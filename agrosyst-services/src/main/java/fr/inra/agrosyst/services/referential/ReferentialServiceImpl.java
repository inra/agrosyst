package fr.inra.agrosyst.services.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielType;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisationImpl;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.managementmode.StrategyType;
import fr.inra.agrosyst.api.entities.referential.AbstractRefMaterielTopiaDao;
import fr.inra.agrosyst.api.entities.referential.BioAgressorParentType;
import fr.inra.agrosyst.api.entities.referential.DestinationContext;
import fr.inra.agrosyst.api.entities.referential.DomainPhytoProductInputDaoSearchResult;
import fr.inra.agrosyst.api.entities.referential.DomainPhytoProductInputSearchResult;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPC;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPCTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaGroupeCultures;
import fr.inra.agrosyst.api.entities.referential.RefActaGroupeCulturesTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActiveTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCategTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefAdventiceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefAnimalTypeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressorTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCattleAnimalTypeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCattleRationAlimentTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAA;
import fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrapeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMMTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefConversionUnitesQSATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTSTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCountryTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCultureMAA;
import fr.inra.agrosyst.api.entities.referential.RefDepartmentShape;
import fr.inra.agrosyst.api.entities.referential.RefDepartmentShapeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinageTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceToVariete;
import fr.inra.agrosyst.api.entities.referential.RefEspeceToVarieteTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrgaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitement;
import fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitementTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverterTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatus;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatusTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMAABiocontrole;
import fr.inra.agrosyst.api.entities.referential.RefMAABiocontroleTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCibleTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMarketingDestination;
import fr.inra.agrosyst.api.entities.referential.RefMarketingDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteurTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutilTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTractionTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefOTEXTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDI;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefOtherInputTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMMTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefPotTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClass;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClassTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSeedUnitsTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalisTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigoTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigoTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSectorTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefStationMeteo;
import fr.inra.agrosyst.api.entities.referential.RefStationMeteoTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLever;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLeverTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenneTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSubstrateTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefTraitSdC;
import fr.inra.agrosyst.api.entities.referential.RefTraitSdCTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgricultureTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGeves;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGevesTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrapeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntity;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.entities.referential.Referentials;
import fr.inra.agrosyst.api.entities.referential.WineValorisation;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.action.HarvestingActionValorisationDto;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.domain.CroppingPlans;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainMineralProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainOrganicProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainOtherProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPotInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedLotInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedSpeciesInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSubstrateInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.search.DomainPhytoProductInputSearchResults;
import fr.inra.agrosyst.api.services.domain.inputStock.search.PhytoProductInputFilter;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.itk.Itk;
import fr.inra.agrosyst.api.services.performance.EquipmentUsageRange;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.api.services.practiced.RefStadeEdiDto;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.api.services.referential.DomainReferentialInputs;
import fr.inra.agrosyst.api.services.referential.GroupeCibleDTO;
import fr.inra.agrosyst.api.services.referential.MaterielDto;
import fr.inra.agrosyst.api.services.referential.MineralProductType;
import fr.inra.agrosyst.api.services.referential.RefAnimalTypeDto;
import fr.inra.agrosyst.api.services.referential.RefCattleAnimalTypeDto;
import fr.inra.agrosyst.api.services.referential.RefCattleRationAlimentDto;
import fr.inra.agrosyst.api.services.referential.RefPotDto;
import fr.inra.agrosyst.api.services.referential.RefSubstrateDto;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.referential.TypeMaterielFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.CacheDiscriminator;
import fr.inra.agrosyst.services.common.CacheService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.common.export.EntityExporter;
import fr.inra.agrosyst.services.common.export.ExportModel;
import fr.inra.agrosyst.services.common.export.ExportModelAndRows;
import fr.inra.agrosyst.services.domain.inputStock.DomainInputBinder;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorVintageTargetIFT;
import fr.inra.agrosyst.services.referential.csv.AllMaterielExport;
import fr.inra.agrosyst.services.referential.csv.AllSpeciesExport;
import fr.inra.agrosyst.services.referential.csv.EuropeanProductDto;
import fr.inra.agrosyst.services.referential.csv.EuropeanProductModel;
import fr.inra.agrosyst.services.referential.csv.RefCiblesAgrosystGroupesCiblesMAAModel;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jetbrains.annotations.NotNull;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportModel;
import org.nuiton.i18n.I18n;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.nuiton.i18n.I18n.l;

@Setter
public class ReferentialServiceImpl extends AbstractAgrosystService implements ReferentialService {

    private static final Log LOGGER = LogFactory.getLog(ReferentialServiceImpl.class);
    protected static final int NB_RESULTS = 20;
    protected static final String REF_ESPECE_VARIETY_KEY_SEPARATOR = "_";
    // Phyto product have there unit converted at import ImportServiceImpl.convertTo_PhytoProductUnit_HA_unit()
    public static final List<PhytoProductUnit> REFERENCE_DOSE_UNIT_ORDER = Arrays.asList(
            PhytoProductUnit.L_HA,
            PhytoProductUnit.KG_HA,
            PhytoProductUnit.G_HA,
            PhytoProductUnit.L_HL,
            PhytoProductUnit.KG_HL,
            PhytoProductUnit.G_HL,
            PhytoProductUnit.L_OU_KG_KG,
            PhytoProductUnit.L_KG_APPAT,
            PhytoProductUnit.G_PALME
    );

    public static final List<PhytoProductUnit> SEEDING_DEFAULT_PHYTO_PRODUCT_UNITS = Arrays.asList(
            PhytoProductUnit.DIFFUSEURS_HA,
            PhytoProductUnit.DS_HA,
            PhytoProductUnit.G_HA,
            PhytoProductUnit.G_KG,
            PhytoProductUnit.G_L,
            PhytoProductUnit.KG_HA,
            PhytoProductUnit.KG_M3,
            PhytoProductUnit.KG_Q,
            PhytoProductUnit.KG_T,
            PhytoProductUnit.KG_UNITE,
            PhytoProductUnit.L_100000_GRAINES,
            PhytoProductUnit.L_1000PLANTS,
            PhytoProductUnit.L_HA,
            PhytoProductUnit.L_HL,
            PhytoProductUnit.L_KG,
            PhytoProductUnit.L_Q,
            PhytoProductUnit.L_T,
            PhytoProductUnit.L_UNITE,
            PhytoProductUnit.L_UNITE_SEMENCES,
            PhytoProductUnit.ML_HA,
            PhytoProductUnit.ML_KG,
            PhytoProductUnit.ML_UNIT,
            PhytoProductUnit.SACHETS_HA);

    public static final int ALL_PERIODS = -1;

    protected CacheService cacheService;
    protected PracticedSystemService practicedSystemService;
    protected EffectiveCropCycleService effectiveCropCycleService;
    protected AgrosystI18nService i18nService;

    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    protected RefActaDosageSPCTopiaDao refActaDosageSPCDao;
    protected RefActaGroupeCulturesTopiaDao refActaGroupeCulturesDao;
    protected RefActaSubstanceActiveTopiaDao refActaSubstanceActiveDao;
    protected RefActaTraitementsProduitsCategTopiaDao refActaTraitementsProduitsCategDao;
    protected RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao;
    protected RefAdventiceTopiaDao refAdventiceDao;
    protected RefAnimalTypeTopiaDao refAnimalTypeDao;
    protected RefBioAgressorTopiaDao refBioAgressorDao;
    protected RefCattleAnimalTypeTopiaDao refCattleAnimalTypeDao;
    protected RefCattleRationAlimentTopiaDao refCattleRationAlimentDao;
    protected RefCiblesAgrosystGroupesCiblesMAATopiaDao refCiblesAgrosystGroupesCiblesMAADao;
    protected RefClonePlantGrapeTopiaDao refClonePlantGrapeDao;
    protected RefCompositionSubstancesActivesParNumeroAMMTopiaDao refCompositionSubstancesActivesParNumeroAMMTopiaDao;
    protected RefConversionUnitesQSATopiaDao refConversionUnitesQSATopiaDao;
    protected RefCorrespondanceMaterielOutilsTSTopiaDao refCorrespondanceMaterielOutilsTSTopiaDao;
    protected RefCountryTopiaDao refCountryTopiaDao;
    protected RefDepartmentShapeTopiaDao refDepartmentShapeDao;
    protected RefDestinationTopiaDao refDestinationDao;
    protected RefElementVoisinageTopiaDao refElementVoisinageDao;
    protected RefEspeceTopiaDao refEspeceDao;
    protected RefEspeceToVarieteTopiaDao refEspeceToVarieteDao;
    protected RefFertiMinUNIFATopiaDao refFertiMinUNIFADao;
    protected RefFertiOrgaTopiaDao refFertiOrgaDao;
    protected RefGroupeCibleTraitementTopiaDao refGroupeCibleTraitementDao;
    protected RefHarvestingPriceTopiaDao refHarvestingPriceDao;
    protected RefInputUnitPriceUnitConverterTopiaDao refInputUnitPriceUnitConverterDao;
    protected RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEDIDao;
    protected RefLegalStatusTopiaDao refLegalStatusDao;
    protected RefLocationTopiaDao refLocationDao;
    protected RefMAABiocontroleTopiaDao refMAABiocontroleDao;
    protected RefMAADosesRefParGroupeCibleTopiaDao refMAADosesRefParGroupeCibleDao;
    protected RefMarketingDestinationTopiaDao refMarketingDestinationDao;
    protected RefMaterielAutomoteurTopiaDao refMaterielAutomoteurDao;
    protected RefMaterielIrrigationTopiaDao refMaterielIrrigationDao;
    protected RefMaterielOutilTopiaDao refMaterielOutilDao;
    protected RefMaterielTopiaDao refMaterielDao;
    protected RefMaterielTractionTopiaDao refMaterielTractionDao;
    protected RefNuisibleEDITopiaDao refNuisibleEDIDao;
    protected RefOtherInputTopiaDao refOtherInputDao;
    protected RefOrientationEDITopiaDao refOrientationEDIDao;
    protected RefOTEXTopiaDao refOTEXDao;
    protected RefParcelleZonageEDITopiaDao refParcelleZonageEDIDao;
    protected RefPhrasesRisqueEtClassesMentionDangerParAMMTopiaDao refPhrasesRisqueEtClassesMentionDangerParAMMTDao;
    protected RefPotTopiaDao refPotDao;
    protected RefQualityCriteriaClassTopiaDao refQualityCriteriaClassDao;
    protected RefQualityCriteriaTopiaDao refQualityCriteriaDao;
    protected RefSeedUnitsTopiaDao refSeedUnitsDao;
    protected RefSolArvalisTopiaDao refSolArvalisDao;
    protected RefSolCaracteristiqueIndigoTopiaDao refSolCaracteristiqueIndigoDao;
    protected RefSolProfondeurIndigoTopiaDao refSolProfondeurIndigoDao;
    protected RefSolTextureGeppaTopiaDao refSolTextureGeppaDao;
    protected RefSpeciesToSectorTopiaDao refSpeciesToSectorDao;
    protected RefStadeEDITopiaDao refStadeEDIDao;
    protected RefStationMeteoTopiaDao refStationMeteoDao;
    protected RefStrategyLeverTopiaDao refStrategyLeverDao;
    protected RefSubstancesActivesCommissionEuropeenneTopiaDao refSubstancesActivesCommissionEuropeennesDao;
    protected RefSubstrateTopiaDao refSubstrateDao;
    protected RefTraitSdCTopiaDao refTraitSdCDao;
    protected RefTypeAgricultureTopiaDao refTypeAgricultureDao;
    protected RefVarieteGevesTopiaDao refVarieteGevesDao;
    protected RefVarietePlantGrapeTopiaDao refVarietePlantGrapeDao;

    protected static final Predicate<RefEspeceToVariete> IS_GEVES = input -> VARIETE_GEVES.equalsIgnoreCase(input.getReferentiel_source());


    protected static final Function<RefEspeceToVariete, Integer> TO_VARIETE_GEVES_SPECIES_ID =
            input -> Integer.valueOf(input.getCode_espece_autre_referentiel());

    protected static final Function<RefActaGroupeCultures, Integer> GET_GROUPE_ID_CULTURE = RefActaGroupeCultures::getId_groupe_culture;

    protected static final Comparator<RefStadeEdiDto> STADE_EDI_DTO_COMPARATOR = Comparator.comparing(RefStadeEdiDto::getLabel);

    protected static final Binder<HarvestingActionValorisation, HarvestingActionValorisation>
            HARVESTING_ACTION_VALORISATION_HARVESTING_ACTION_VALORISATION_BINDER = BinderFactory.newBinder(HarvestingActionValorisation.class);

    public static final int FIRST_TRY = 0;

    protected static final Function<CroppingPlanSpecies, String> GET_REF_ESPECE_VARIETY_KEY = input -> {
        String id = input.getSpecies().getTopiaId();
        if (input.getVariety() != null) {
            id = id + REF_ESPECE_VARIETY_KEY_SEPARATOR + input.getVariety().getTopiaId();
        }
        return id;
    };

    protected void validatePracticedSystemCampaigns(String practicedSystemCampaigns) {
        Preconditions.checkArgument(
                CommonService.getInstance().areCampaignsValids(practicedSystemCampaigns),
                String.format("Les campagnes du synthétisé ne sont pas valides %s", practicedSystemCampaigns)
        );
    }

    private Optional<Set<Integer>> getEntireYears(int beginCampaign, int endingCampaign) {
        Set<Integer> fullYears = null;
        if (endingCampaign - beginCampaign > 1) {
            fullYears = new HashSet<>();
            for (int i = beginCampaign + 1; i < endingCampaign; i++) {
                fullYears.add(i);
            }
        }
        return Optional.ofNullable(fullYears);
    }

    @Override
    public boolean isValidRefFertiMinProduct(RefFertiMinUNIFA product) {
        if (product == null) {
            return false;
        }
        boolean result = product.getCateg() != null && StringUtils.isNotBlank(product.getForme());
        if (!result) {
            return false;
        }
        List<String> productTypes = getRefFertiMinUnifTypeProduitForCateg(product.getCateg());
        result = productTypes.contains(product.getType_produit());

        double totalWeight = 0;
        totalWeight += product.getN();
        totalWeight += product.getP2O5();
        totalWeight += product.getBore();
        totalWeight += product.getCalcium();
        totalWeight += product.getCuivre();
        totalWeight += product.getFer();
        totalWeight += product.getK2O();
        totalWeight += product.getManganese();
        totalWeight += product.getMgO();
        totalWeight += product.getMolybdene();
        totalWeight += product.getOxyde_de_sodium();
        totalWeight += product.getsO3();
        totalWeight += product.getZinc();
        result &= (0 < totalWeight && totalWeight <= 100);
        return result;
    }

    public boolean isValidRefFertiMinProductDto(DomainMineralProductInputDto product) {
        if (product == null) {
            return false;
        }
        List<String> productTypes = getRefFertiMinUnifTypeProduitForCateg(product.getCateg());
        boolean result = productTypes.contains(product.getTradeName().orElse(""));

        double totalWeight = 0;
        totalWeight += product.getN();
        totalWeight += product.getP2o5();
        totalWeight += product.getBore();
        totalWeight += product.getCalcium();
        totalWeight += product.getCuivre();
        totalWeight += product.getFer();
        totalWeight += product.getK2o();
        totalWeight += product.getManganese();
        totalWeight += product.getMgo();
        totalWeight += product.getMolybdene();
        totalWeight += product.getOxyde_de_sodium();
        totalWeight += product.getSo3();
        totalWeight += product.getZinc();
        result &= (0 < totalWeight && totalWeight <= 100);
        return result;
    }

    @Override
    public Map<MaterielType, List<Pair<String, String>>> getWithoutPetitMaterielTypeMateriel1List(boolean filterEdaplos) {
        Language language = getSecurityContext().getLanguage();
        Callable<HashMap<MaterielType, List<Pair<String, String>>>> loader = () -> {
            HashMap<MaterielType, List<Pair<String, String>>> result = new LinkedHashMap<>();
            List<String> tractorTypeMateriel1 = refMaterielTractionDao.findTypeMateriel1WithoutPetitMaterielValues(filterEdaplos);
            result.put(MaterielType.TRACTEUR, refMaterielOutilDao.getMaterlielTraduction("trad_ref_materiel", tractorTypeMateriel1, language));
            List<String> automoteurTypeMateriel1 = refMaterielAutomoteurDao.findTypeMateriel1WithoutPetitMaterielValues(filterEdaplos);
            result.put(MaterielType.AUTOMOTEUR, refMaterielOutilDao.getMaterlielTraduction("trad_ref_materiel", automoteurTypeMateriel1, language));
            List<String> outilTypeMateriel1 = refMaterielOutilDao.findTypeMateriel1WithoutPetitMaterielValues(filterEdaplos);
            result.put(MaterielType.OUTIL, refMaterielOutilDao.getMaterlielTraduction("trad_ref_materiel", outilTypeMateriel1, language));
            List<String> irrigationTypeMateriel1 = refMaterielIrrigationDao.findTypeMateriel1WithoutPetitMaterielValues(filterEdaplos);
            result.put(MaterielType.IRRIGATION, refMaterielOutilDao.getMaterlielTraduction("trad_ref_materiel", irrigationTypeMateriel1, language));
            return result;
        };
        return cacheService.get(CacheDiscriminator.activeTypeMateriel1WithoutManualTools(language, filterEdaplos), loader);
    }

    @Override
    public Map<MaterielType, List<Pair<String, String>>> getPetitMaterielTypeMateriel1List() {
        Language language = getSecurityContext().getLanguage();
        Callable<HashMap<MaterielType, List<Pair<String, String>>>> loader = () -> {
            HashMap<MaterielType, List<Pair<String, String>>> result = new LinkedHashMap<>();
            final List<String> typeMateriel1Values = refMaterielOutilDao.findTypeMateriel1PetitMaterielValues();
            List<Pair<String, String>> typeMateriel1ValuesWithTraductions = refMaterielOutilDao.getMaterlielTraduction("trad_ref_materiel", typeMateriel1Values, language);
            result.put(MaterielType.OUTIL, typeMateriel1ValuesWithTraductions);
            return result;
        };
        return cacheService.get(CacheDiscriminator.activeTypeMateriel1WithManualTools(language), loader);
    }

    @Override
    public List<String> getRefFertiMinUnifTypeProduitForCateg(Integer categ) {
        return refFertiMinUNIFADao.findNotAgrosystUserRefFertiMinUnifTypeProduitForCateg(categ);
    }

    protected AbstractRefMaterielTopiaDao<?> getRefMaterielDao(MaterielType type) {
        return switch (type) {
            case TRACTEUR -> refMaterielTractionDao;
            case AUTOMOTEUR -> refMaterielAutomoteurDao;
            case OUTIL -> refMaterielOutilDao;
            case IRRIGATION -> refMaterielIrrigationDao;
            default -> refMaterielDao;
        };
    }

    @Override
    public List<Pair<String, String>> getTypeMateriel2List(TypeMaterielFilter filter) {
        MaterielType type = filter.getType();
        Preconditions.checkArgument(type != null, "Type de matériel inconnu");
        AbstractRefMaterielTopiaDao<?> dao = getRefMaterielDao(type);

        // declare dao
        Language language = getSecurityContext().getLanguage();
        List<String> result0 = dao.findPropertyValues(
                RefMateriel.PROPERTY_TYPE_MATERIEL2,
                filter.getTypeMateriel1(),
                null,
                null,
                filter.isFilterOnManualTool());
        List<Pair<String, String>> materlielTraduction = refMaterielOutilDao.getMaterlielTraduction("trad_ref_materiel", result0, language);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTypeMateriel2List:");
            LOGGER.debug(filter);
        }

        return materlielTraduction;
    }

    @Override
    public List<Pair<String, String>> getTypeMateriel3List(TypeMaterielFilter filter) {
        MaterielType type = filter.getType();
        Preconditions.checkArgument(type != null, "Type de matériel inconnu");
        AbstractRefMaterielTopiaDao<?> dao = getRefMaterielDao(type);

        Language language = getSecurityContext().getLanguage();
        List<String> result0 = dao.findPropertyValues(
                RefMateriel.PROPERTY_TYPE_MATERIEL3,
                filter.getTypeMateriel1(),
                filter.getTypeMateriel2(),
                null,
                filter.isFilterOnManualTool());
        List<Pair<String, String>> materlielTraduction = refMaterielOutilDao.getMaterlielTraduction("trad_ref_materiel", result0, language);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTypeMateriel3List:");
            LOGGER.debug(filter);
        }

        return materlielTraduction;
    }

    @Override
    public List<Pair<String, String>> getTypeMateriel4List(TypeMaterielFilter filter) {
        MaterielType type = filter.getType();
        Preconditions.checkArgument(type != null, "Type de matériel inconnu");
        AbstractRefMaterielTopiaDao<?> dao = getRefMaterielDao(type);
        Language language = getSecurityContext().getLanguage();
        List<String> result0 = dao.findPropertyValues(
                RefMateriel.PROPERTY_TYPE_MATERIEL4,
                filter.getTypeMateriel1(),
                filter.getTypeMateriel2(),
                filter.getTypeMateriel3(),
                filter.isFilterOnManualTool());
        List<Pair<String, String>> materlielTraduction = refMaterielOutilDao.getMaterlielTraduction("trad_ref_materiel", result0, language);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTypeMateriel4List:");
            LOGGER.debug(filter);
        }

        return materlielTraduction;
    }

    @Override
    public Map<String, List<MaterielDto>> getMaterielUniteMap(TypeMaterielFilter filter) {
        MaterielType type = filter.getType();
        Preconditions.checkArgument(type != null, "Type de matériel inconnu");
        Language language = getSecurityContext().getLanguage();
        AbstractRefMaterielTopiaDao<?> dao = getRefMaterielDao(type);
        Map<String, String[]> result0 = dao.findPropertyValuesAsMap(
                RefMateriel.PROPERTY_UNITE_PAR_AN,
                RefMateriel.PROPERTY_UNITE,
                filter.getTypeMateriel1(),
                filter.getTypeMateriel2(),
                filter.getTypeMateriel3(),
                filter.getTypeMateriel4(),
                filter.isFilterOnManualTool());

        List<String> valueToTranslate = result0.values().stream().map(t -> t[1]).filter(Objects::nonNull).toList();
        List<Pair<String, String>> translatedUnits = refMaterielOutilDao.getMaterlielTraduction("trad_ref_divers", valueToTranslate, language);
        Map<String, String> translatedUnitMap = new HashMap<>();
        translatedUnits.forEach(t -> translatedUnitMap.put(t.getKey(), t.getValue()));

        Map<String, List<MaterielDto>> result = new HashMap<>();
        for (Map.Entry<String, String[]> materielIdToUnits : result0.entrySet()) {
            String materielId = materielIdToUnits.getKey();
            String[] units = materielIdToUnits.getValue();
            double uniteParAn = Double.parseDouble(units[0]);
            String unite = units[1];
            List<MaterielDto> materielDtos = result.computeIfAbsent(materielId, k -> new ArrayList<>());
            MaterielDto materielDto = new MaterielDto();
            materielDto.setMaterielTopiaId(materielId);
            materielDto.setUniteParAn(uniteParAn);
            materielDto.setUnite(unite);
            materielDto.setUniteTranslated(translatedUnitMap.get(unite));
            materielDtos.add(materielDto);
            result.put(materielId, materielDtos);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMaterielUniteMap:");
            LOGGER.debug(filter);
        }
        return result;
    }

    @Override
    public RefMateriel getMateriel(String materielTopiaId) {
        // declare dao
        RefMateriel materiel = refMaterielDao.forTopiaIdEquals(materielTopiaId).findUnique();
        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(getSecurityContext().getLanguage());
        refMaterielDao.fillTranslations(Collections.singletonList(materielTopiaId), translationMap);
        AgrosystI18nService.translateMateriel(materiel, translationMap);
        return materiel;
    }

    @Override
    public Map<Integer, String> getSolArvalisRegions() {
        return refSolArvalisDao.getAllSolArvalisRegions();
    }

    @Override
    public List<RefSolArvalis> getSolArvalis(Integer regionCode) {
        Language language = getSecurityContext().getLanguage();
        List<RefSolArvalis> result;
        if (regionCode == null) {
            result = new ArrayList<>();
        } else {
            result = refSolArvalisDao.findAllForRegion(language, regionCode);
        }
        return result;
    }

    @Override
    public List<RefEspece> findSpecies(String filter) {
        Language language = getSecurityContext().getLanguage();
        return refEspeceDao.findActiveEspeces(language, filter, NB_RESULTS);
    }

    @Override
    public List<RefVariete> getVarietes(String speciesId, String filter) {

        RefEspece species = refEspeceDao.forTopiaIdEquals(speciesId).findUnique();

        List<RefEspeceToVariete> varietes = refEspeceToVarieteDao.forCode_espece_ediEquals(species.getCode_espece_botanique()).findAll();

        return getSpeciesVarietes(varietes, filter, NB_RESULTS);
    }

    @Override
    public boolean validVarietesFromCodeEspeceEdi(RefVariete variete, String codeEspeceBotanique) {
        boolean result;

        Preconditions.checkArgument(variete != null, StringUtils.isNoneBlank(codeEspeceBotanique));

        List<RefEspeceToVariete> varietes = refEspeceToVarieteDao.forCode_espece_ediEquals(codeEspeceBotanique).findAll();

        List<RefVariete> speciesVariety = getSpeciesVarietes(varietes, null, DaoUtils.NO_PAGE_LIMIT);

        result = speciesVariety.contains(variete);

        return result;
    }

    protected List<RefVariete> getSpeciesVarietes(List<RefEspeceToVariete> varietes, String filter, int maxResults) {
        // Variétés Geves
        Set<Integer> varietesGevesNumEspeceBotaniques = varietes.stream()
                .filter(IS_GEVES)
                .map(TO_VARIETE_GEVES_SPECIES_ID)
                .collect(Collectors.toSet());
        List<RefVarieteGeves> varietesGeves = refVarieteGevesDao.findAllActiveVarietes(varietesGevesNumEspeceBotaniques, filter, maxResults);
        List<RefVariete> speciesVariety = new ArrayList<>(varietesGeves);

        maxResults = maxResults == DaoUtils.NO_PAGE_LIMIT ? DaoUtils.NO_PAGE_LIMIT : maxResults - speciesVariety.size();

        // Variétés PlantGrape
        boolean isZMO = varietes.stream().anyMatch(IS_GEVES.negate());
        List<RefVarietePlantGrape> varietesPlantGrappe = refVarietePlantGrapeDao.findAllVarietes(isZMO, filter, maxResults);
        speciesVariety.addAll(varietesPlantGrappe);

        return speciesVariety;
    }

    @Override
    public RefEspece getSpecies(String speciesId) {
        return refEspeceDao.forTopiaIdEquals(speciesId).findUnique();
    }

    @Override
    public List<RefVariete> getGraftSupports(String speciesId, String filter) {

        RefEspece species = refEspeceDao.forTopiaIdEquals(speciesId).findUnique();

        String gnisCode = species.getCode_GNIS();

        List<RefVariete> result = new ArrayList<>();

        // Variétés Geves
        if (!ReferentialService.WINE.equals(species.getCode_espece_botanique())) {

            int graftSupportCodeSection = getConfig().getSpeciesGraftSupportCodeSection();

            List<RefVarieteGeves> gevesVarietes = refVarieteGevesDao.findActiveGraftSupport(
                    filter, graftSupportCodeSection, DaoUtils.NO_PAGE_LIMIT, gnisCode);
            result.addAll(gevesVarietes);

        } else {
            // Variétés PlantGrape
            String speciesGraftSupportUtilisation = getConfig().getSpeciesGraftSupportUtilisation();
            List<RefVarietePlantGrape> plantGrapeVarietes = refVarietePlantGrapeDao.findGraftSupport(
                    filter, speciesGraftSupportUtilisation, DaoUtils.NO_PAGE_LIMIT, gnisCode);
            result.addAll(plantGrapeVarietes);
        }

        return result;
    }

    @Override
    public List<RefClonePlantGrape> getGraftClones(String speciesId, String varietyId, String filter) {
        Preconditions.checkArgument(speciesId != null);
        Preconditions.checkArgument(varietyId != null);

        // it can be something else than an RefVarietePlantGrape that's why it can be null
        RefVarietePlantGrape variety = refVarietePlantGrapeDao.forTopiaIdEquals(varietyId).findUniqueOrNull();

        List<RefClonePlantGrape> result = new ArrayList<>();
        if (variety != null) {
            List<RefClonePlantGrape> gevesVarietes = refClonePlantGrapeDao.findGraftClones(
                    filter, variety.getCodeVar(), NB_RESULTS);
            result.addAll(gevesVarietes);
        }
        return result;
    }

    @Override
    public RefLocation getRefLocation(String refLocationArvalisId) {
        return refLocationDao.forTopiaIdEquals(refLocationArvalisId).findUnique();
    }

    @Override
    public List<RefLocation> getActiveCommunes(String filter) {
        String franceId = refCountryTopiaDao.forTrigramEquals(Locale.FRANCE.getISO3Country().toLowerCase()).findUnique().getTopiaId();
        return refLocationDao.findActiveLocations(filter, franceId, NB_RESULTS);
    }

    @Override
    public List<RefLocation> getActiveCommunes(String filter, String refCountryId) {
        if (refCountryId != null) {
            return refLocationDao.findActiveLocations(filter, refCountryId, NB_RESULTS);
        } else {
            return getActiveCommunes(filter);
        }
    }

    @Override
    public Map<Integer, String> getAllActiveOtex18Code() {
        Language language = getSecurityContext().getLanguage();
        Callable<HashMap<Integer, String>> loader = () -> new HashMap<>(refOTEXDao.findAllActiveOtex18Code(language));
        return cacheService.get(CacheDiscriminator.activeOtex18Code(language), loader);
    }

    @Override
    public Map<Integer, String> getAllActiveCodeOtex70ByOtex18code(Integer otex18code) {
        Language language = getSecurityContext().getLanguage();
        return refOTEXDao.findAllActiveCodeOtex70ByOtex18code(otex18code, language);
    }

    @Override
    public List<RefOrientationEDI> getAllReferentielEDI() {
        Language language = getSecurityContext().getLanguage();
        Callable<LinkedList<RefOrientationEDI>> loader = () -> new LinkedList<>(refOrientationEDIDao.findAllOrientationEdi(language));
        return cacheService.get(CacheDiscriminator.activeRefOrientationEDI(language), loader);

    }

    @Override
    public List<RefInterventionAgrosystTravailEDI> getAllActiveAgrosystActions() {
        return getAllActiveAgrosystActions(null);
    }

    @Override
    public List<RefInterventionAgrosystTravailEDI> getAllActiveAgrosystActions(final AgrosystInterventionType interventionType) {
        Language language = getSecurityContext().getLanguage();
        Callable<LinkedList<RefInterventionAgrosystTravailEDI>> loader = () ->
                new LinkedList<>(refInterventionAgrosystTravailEDIDao.findAllActive(language, interventionType));
        return cacheService.get(CacheDiscriminator.activeAgrosystActions(language), interventionType, loader);
    }

    @Override
    public List<RefParcelleZonageEDI> getAllActiveParcelleZonage() {

        Callable<LinkedList<RefParcelleZonageEDI>> loader = () -> new LinkedList<>(
                refParcelleZonageEDIDao.forActiveEquals(true).findAll());

        return cacheService.get(CacheDiscriminator.activeRefParcelleZonageEDI(), loader);
    }

    @Override
    public List<RefSolTextureGeppa> getAllActiveSolTextures() {

        Callable<LinkedList<RefSolTextureGeppa>> loader = () -> new LinkedList<>(
                refSolTextureGeppaDao.forActiveEquals(true).findAll());

        return cacheService.get(CacheDiscriminator.activeRefSolTextureGeppa(), loader);

    }

    @Override
    public List<RefSolTextureGeppa> getAllNotFrenchActiveSolTextures() {
        Language language = getSecurityContext().getLanguage();
        Callable<LinkedList<RefSolTextureGeppa>> loader = () -> new LinkedList<>(refSolTextureGeppaDao.getNotFrenchTextures(language));
        return cacheService.get(CacheDiscriminator.activeNotFrenchRefSolTextureGeppa(language), loader);
    }

    @Override
    public List<RefSolProfondeurIndigo> getAllActiveSolProfondeurs() {
        Language language = getSecurityContext().getLanguage();
        Callable<LinkedList<RefSolProfondeurIndigo>> loader = () -> new LinkedList<>(refSolProfondeurIndigoDao.getAllActiveProfondeurs(language));
        return cacheService.get(CacheDiscriminator.activeRefSolProfondeurIndigo(language), loader);

    }

    @Override
    public List<RefSolCaracteristiqueIndigo> getAllActiveSolCaracteristiques() {

        Callable<LinkedList<RefSolCaracteristiqueIndigo>> loader = () -> new LinkedList<>(
                refSolCaracteristiqueIndigoDao.forActiveEquals(true).findAll());

        return cacheService.get(CacheDiscriminator.activeRefSolCaracteristiqueIndigo(), loader);

    }

    @Override
    public List<MineralProductType> getAllActiveMineralProductTypes() {
        Language language = getSecurityContext().getLanguage();

        Callable<LinkedList<MineralProductType>> loader = () -> new LinkedList<>(
                refFertiMinUNIFADao.findAllActiveFertiMinProductType(language));

        return cacheService.get(CacheDiscriminator.activeMineralProductType(language), loader);

    }

    @Override
    public List<RefFertiMinUNIFA> getAllActiveRefFertiMinUnifaByCategAndShape(Integer categ, String fertilizerShape) {
        return refFertiMinUNIFADao.findAllActiveRefFertiMinUnifaByCategAndShape(categ, fertilizerShape);
    }

    @Override
    public List<RefFertiOrga> getAllActiveOrganicProductTypes() {
        Language language = getSecurityContext().getLanguage();
        Callable<LinkedList<RefFertiOrga>> loader = () -> new LinkedList<>(
                refFertiOrgaDao.findAll(language, false));
        return cacheService.get(CacheDiscriminator.activeRefFertiOrga(language), loader);
    }

    @Override
    public Object getAllOrganicProductTypes(boolean includeInactive) {

        Language language = getSecurityContext().getLanguage();
        Callable<LinkedList<RefFertiOrga>> loader = () -> new LinkedList<>(
                refFertiOrgaDao.findAll(language, includeInactive));

        LinkedList<RefFertiOrga> refFertiOrgas;
        if (includeInactive) {
            refFertiOrgas = cacheService.get(CacheDiscriminator.inactiveRefFertiOrga(language), loader);
        } else {
            refFertiOrgas = cacheService.get(CacheDiscriminator.activeRefFertiOrga(language), loader);
        }

        return refFertiOrgas;
    }

    @Override
    public List<RefStadeEdiDto> getRefStadesEdi(Integer vegetativeProfileRaw) {
        Language language = getSecurityContext().getLanguage();
        int vegetativeProfile = MoreObjects.firstNonNull(vegetativeProfileRaw, 9999);
        List<RefStadeEDI> entities = refStadeEDIDao.forProperties(
                RefStadeEDI.PROPERTY_PROFIL_VEGETATIF, vegetativeProfile,
                RefStadeEDI.PROPERTY_ACTIVE, true).findAll();
        ReferentialTranslationMap referentialTranslationMap = new ReferentialTranslationMap(language);
        i18nService.fillRefStadeEdiTranslations(
                entities.stream().map(RefStadeEDI::getTopiaId).toList(),
                referentialTranslationMap);
        return entities.stream()
                .map(entity -> Itk.getDtoForRefStadeEdi(entity, referentialTranslationMap))
                .sorted(STADE_EDI_DTO_COMPARATOR)
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, String> getActaTreatementCodesAndNames() {

        Callable<HashMap<String, String>> loader = () -> new HashMap<>(
                refActaTraitementsProduitDao.findAllTreatmentCodesAndNames());

        return cacheService.get(CacheDiscriminator.activeActaTreatementCodesAndNames(), loader);
    }

    @Override
    public Map<AgrosystInterventionType, List<ProductType>> getAllActiveActaTreatmentProductTypes() {

        Callable<LinkedHashMap<AgrosystInterventionType, List<ProductType>>> loader = () ->
                refActaTraitementsProduitsCategDao.findAllActiveActaTreatmentProductType();

        return cacheService.get(CacheDiscriminator.activeActaTreatmentProductTypes(), loader);
    }

    @Override
    public List<RefElementVoisinage> getAllActiveElementVoisinages() {
        Language language = getSecurityContext().getLanguage();
        return refElementVoisinageDao.getAllActiveElementsVoisinage(language);
    }

    @Override
    public List<RefBioAgressor> getTreatmentTargets(BioAgressorType category) {

        Language language = getSecurityContext().getLanguage();

        String key = category.name();
        Optional<List<BioAgressorType>> bioAgressorTypes = Referentials.getTreatmentTargetBioAgressorParentTypes().stream()
                .filter(bapt -> bapt.getParent().equals(category))
                .map(BioAgressorParentType::getBioAgressorTypes)
                .findFirst();

        List<RefBioAgressor> result = null;

        if (bioAgressorTypes.isPresent()) {
            if (BioAgressorType.ADVENTICE.equals(category)) {
                Callable<ArrayList<RefBioAgressor>> loader = () -> {
                    ArrayList<RefBioAgressor> bioAgressors = new ArrayList<>(
                            refAdventiceDao.findActiveRefAdventiceForBioAgressorType(language)
                    );
                    bioAgressors.addAll(
                            refNuisibleEDIDao.findActiveRefNuisibleEDIForBioAgressorType(language, bioAgressorTypes.get(), null)
                    );
                    bioAgressors.sort(Comparator.comparing(refBioAgressor -> {
                        if (refBioAgressor instanceof RefAdventice) {
                            return ((RefAdventice) refBioAgressor).isMain();
                        }
                        return ((RefNuisibleEDI) refBioAgressor).isMain();

                    }).reversed().thenComparing(refBioAgressor -> {
                        if (refBioAgressor instanceof RefAdventice) {
                            return ((RefAdventice) refBioAgressor).getAdventice();
                        }
                        return ((RefNuisibleEDI) refBioAgressor).getReference_label();
                    }));
                    return bioAgressors;
                };
                result = cacheService.get(CacheDiscriminator.activeBioAgressorTreatmentTarget(language), key, loader);

            } else {
                Callable<ArrayList<RefBioAgressor>> loader = () -> new ArrayList<>(
                        refNuisibleEDIDao.findActiveRefNuisibleEDIForBioAgressorType(language, bioAgressorTypes.get(), null)
                );
                result = cacheService.get(CacheDiscriminator.activeNuisibleEDITreatmentTarget(language), key, loader);
            }
        }
        return result;
    }

    @Override
    public List<RefTraitSdC> getAllActiveGrowingSystemCharacteristics() {
        return refTraitSdCDao.forActiveEquals(true)
                .setOrderByArguments(RefTraitSdC.PROPERTY_TYPE_TRAIT_SDC, RefTraitSdC.PROPERTY_NOM_TRAIT)
                .findAll();
    }

    @Override
    public List<RefActaTraitementsProduit> getActaTraitementsProduits(AgrosystInterventionType interventionType, ProductType productType) {
        Callable<ArrayList<RefActaTraitementsProduit>> loader = () -> new ArrayList<>(
                refActaTraitementsProduitDao.findAllActiveTreatmentTypesForProductType(interventionType, productType)
        );

        String key = interventionType.name() + "_" + productType.name();
        return cacheService.get(CacheDiscriminator.activeRefActaTraitementsProduit(), key, loader);
    }

    @Override
    public RefBioAgressor getBioAgressor(String bioAgressorId) {
        return refBioAgressorDao.forTopiaIdEquals(bioAgressorId).findUnique();
    }

    @Override
    public RefDepartmentShape getDepartmentShape(String departmentCode) {
        return refDepartmentShapeDao.forDepartmentEquals(departmentCode).findAnyOrNull();
    }

    @Override
    public Map<String, String> getAllRefStationMeteoMap() {

        Callable<LinkedHashMap<String, String>> loader = () -> {
            final List<RefStationMeteo> list = refStationMeteoDao.newQueryBuilder()
                    .addEquals(RefStationMeteo.PROPERTY_ACTIVE, true)
                    .setOrderByArguments(RefStationMeteo.PROPERTY_COMMUNE_SITE).findAll();
            LinkedHashMap<String, String> result = new LinkedHashMap<>();
            for (RefStationMeteo station : list) {
                result.put(station.getTopiaId(), station.getCommuneSite());
            }
            return result;
        };

        return cacheService.get(CacheDiscriminator.activeAllRefStationMeteo(), loader);
    }

    @Override
    public RefFertiMinUNIFA createOrUpdateRefMineralProductToInput(RefFertiMinUNIFA product) {
        Preconditions.checkState(product != null, isValidRefFertiMinProduct(product));
        RefFertiMinUNIFA result;
        // on retrouve le produit par rapport à sa forme, sa catégorie et sa composition et non pas par rapport à son topiaId
        RefFertiMinUNIFA existingProduct = refFertiMinUNIFADao.forNaturalId(
                product.getCateg(),
                product.getForme(),
                product.getN(),
                product.getP2O5(),
                product.getK2O(),
                product.getBore(),
                product.getCalcium(),
                product.getFer(),
                product.getManganese(),
                product.getMolybdene(),
                product.getMgO(),
                product.getOxyde_de_sodium(),
                product.getsO3(),
                product.getCuivre(),
                product.getZinc()).findUniqueOrNull();
        // product not found so it's a new product added by user.
        if (existingProduct == null) {
            product.setTopiaId(null);
            String source = StringUtils.isNotBlank(product.getSource()) ? product.getSource() : "AGROSYST_USER";
            product.setSource(source);
            product.setActive(true);
            product.setCodeprod(null);
            result = refFertiMinUNIFADao.create(product);
        } else if (!existingProduct.isActive()) {
            // cas ou un produit a été créé par un utilisateur et celui-ci correspond à une entité qui est inactive
            // dans ce cas on la réactive
            existingProduct.setActive(true);
            result = refFertiMinUNIFADao.update(existingProduct);
        } else {
            result = existingProduct;
        }
        return result;
    }

    @Override
    public RefFertiMinUNIFA createOrUpdateRefMineralProductInput(DomainMineralProductInputDto productDto) {
        Preconditions.checkState(productDto != null, isValidRefFertiMinProductDto(productDto));
        RefFertiMinUNIFA result;
        // on retrouve le produit par rapport à sa forme, sa catégorie et sa composition et non pas par rapport à son topiaId
        RefFertiMinUNIFA existingProduct = refFertiMinUNIFADao.forNaturalId(
                productDto.getCateg(),
                productDto.getForme(),
                productDto.getN(),
                productDto.getP2o5(),
                productDto.getK2o(),
                productDto.getBore(),
                productDto.getCalcium(),
                productDto.getFer(),
                productDto.getManganese(),
                productDto.getMolybdene(),
                productDto.getMgo(),
                productDto.getOxyde_de_sodium(),
                productDto.getSo3(),
                productDto.getCuivre(),
                productDto.getZinc()).findUniqueOrNull();
        // product not found so it's a new product added by user.
        if (existingProduct == null) {
            result = refFertiMinUNIFADao.create(
                    RefFertiMinUNIFA.PROPERTY_CATEG, productDto.getCateg(),
                    RefFertiMinUNIFA.PROPERTY_FORME, productDto.getForme(),
                    RefFertiMinUNIFA.PROPERTY_N, productDto.getN(),
                    RefFertiMinUNIFA.PROPERTY_P2_O5, productDto.getP2o5(),
                    RefFertiMinUNIFA.PROPERTY_K2_O, productDto.getK2o(),
                    RefFertiMinUNIFA.PROPERTY_BORE, productDto.getBore(),
                    RefFertiMinUNIFA.PROPERTY_CALCIUM, productDto.getCalcium(),
                    RefFertiMinUNIFA.PROPERTY_FER, productDto.getFer(),
                    RefFertiMinUNIFA.PROPERTY_MANGANESE, productDto.getManganese(),
                    RefFertiMinUNIFA.PROPERTY_MOLYBDENE, productDto.getMolybdene(),
                    RefFertiMinUNIFA.PROPERTY_MG_O, productDto.getMgo(),
                    RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, productDto.getOxyde_de_sodium(),
                    RefFertiMinUNIFA.PROPERTY_S_O3, productDto.getSo3(),
                    RefFertiMinUNIFA.PROPERTY_CUIVRE, productDto.getCuivre(),
                    RefFertiMinUNIFA.PROPERTY_ZINC, productDto.getZinc(),
                    RefFertiMinUNIFA.PROPERTY_SOURCE, "AGROSYST_USER",
                    RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, productDto.getType_produit(),
                    RefFertiMinUNIFA.PROPERTY_ACTIVE, Boolean.TRUE
            );
        } else if (!existingProduct.isActive()) {
            // cas ou un produit a été créé par un utilisateur et celui-ci correspond à une entité qui est inactive
            // dans ce cas on la réactive
            existingProduct.setActive(true);
            result = refFertiMinUNIFADao.update(existingProduct);
        } else {
            result = existingProduct;
        }
        return result;
    }

    @Override
    public Map<String, String> getCroppingPlanSpeciesCodeByRefEspeceAndVarietyKey(Collection<CroppingPlanSpecies> allCroppingPlanSpecies) {
        Map<String, String> speciesKeyToCode = new HashMap<>();
        if (CollectionUtils.isNotEmpty(allCroppingPlanSpecies)) {
            for (CroppingPlanSpecies croppingPlanSpecies : allCroppingPlanSpecies) {
                speciesKeyToCode.put(GET_REF_ESPECE_VARIETY_KEY.apply(croppingPlanSpecies), croppingPlanSpecies.getCode());
            }
        }
        return speciesKeyToCode;
    }

    @Override
    public DestinationContext getDestinationContext(
            Set<String> speciesCodes,
            Set<WineValorisation> wineValorisations,
            String growingSystemId,
            String campaigns,
            String zoneTopiaId) {

        DestinationContext result;

        if (CollectionUtils.isNotEmpty(speciesCodes)) {
            List<Pair<String, String>> codeEspeceBotaniquesCodeQualifiantAEE = getCodeEspeceBotaniquesCodeQualifiantAEE(speciesCodes);

            Map<Pair<String, String>, List<Sector>> sectorsByCodeEspeceBotaniqueCodeQualifiant =
                    getSectorsByCodeEspeceBotaniqueCodeQualifiantAEE(codeEspeceBotaniquesCodeQualifiantAEE);

            List<RefDestination> destinations = getRefDestinations(sectorsByCodeEspeceBotaniqueCodeQualifiant, wineValorisations);

            List<RefQualityCriteria> qualityCriteria = getRefQualityCriteria(sectorsByCodeEspeceBotaniqueCodeQualifiant, wineValorisations);

            List<RefQualityCriteriaClass> qualityCriteriaClasses = getRefQualityCriteriaClassForRefQualityCriteria(qualityCriteria);

            Map<String, List<Pair<String, String>>> allCodeEspeceBotaniqueCodeQualifantBySpeciesCode = null;

            Map<String, List<Sector>> allsectorByCodeEspeceBotaniqueCodeQualifiant = null;

            if (StringUtils.isNotBlank(growingSystemId) && StringUtils.isNotBlank(campaigns) || StringUtils.isNotBlank(zoneTopiaId)) {
                if (StringUtils.isEmpty(zoneTopiaId)) {

                    allCodeEspeceBotaniqueCodeQualifantBySpeciesCode = practicedSystemService
                            .getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(growingSystemId, campaigns);

                    allsectorByCodeEspeceBotaniqueCodeQualifiant = practicedSystemService
                            .getSectorByCodeEspceBotaniqueCodeQualifiant(growingSystemId, campaigns);

                } else {

                    allCodeEspeceBotaniqueCodeQualifantBySpeciesCode = effectiveCropCycleService
                            .getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(zoneTopiaId);

                    allsectorByCodeEspeceBotaniqueCodeQualifiant = effectiveCropCycleService
                            .getSectorByCodeEspceBotaniqueCodeQualifiant(zoneTopiaId);
                }
            }

            result = new DestinationContext(
                    sectorsByCodeEspeceBotaniqueCodeQualifiant,
                    destinations,
                    qualityCriteria,
                    qualityCriteriaClasses,
                    allCodeEspeceBotaniqueCodeQualifantBySpeciesCode,
                    allsectorByCodeEspeceBotaniqueCodeQualifiant);
        } else {
            result = new DestinationContext();
        }

        return result;
    }

    private List<RefQualityCriteriaClass> getRefQualityCriteriaClassForRefQualityCriteria(
            List<RefQualityCriteria> qualityCriteria) {

        return refQualityCriteriaClassDao.findAllForRefQualityCriteria(qualityCriteria);
    }

    @Override
    public Map<String, List<Sector>> getSectorsByCodeEspeceBotanique_CodeQualifiantAEE(
            List<Pair<String, String>> codeEspeceBotaniquesCodeQualifiantAEE) {

        return refSpeciesToSectorDao.loadSectorsByCodeEspeceBotanique_CodeQualifiant(codeEspeceBotaniquesCodeQualifiantAEE);
    }

    @Override
    public Map<Pair<String, String>, List<Sector>> getSectorsByCodeEspeceBotaniqueCodeQualifiantAEE(
            List<Pair<String, String>> codeEspeceBotaniquesCodeQualifiantAEE) {

        return refSpeciesToSectorDao.loadSectorsByCodeEspeceBotaniqueCodeQualifiant(codeEspeceBotaniquesCodeQualifiantAEE);
    }

    protected List<Integer> getPeriods(int from, int to) {
        List<Integer> periods = new ArrayList<>();
        for (int i = from; i <= to; i++) {
            if (i != -1) {
                periods.add(i);
            }
        }

        return periods;
    }

    private static @NotNull Set<Integer> getValorisationCampaigns(int beginCampaign, int endingCampaign, String topiaId) {
        Set<Integer> allCampaigns = new HashSet<>();

        if (CommonService.getInstance().isCampaignValid(beginCampaign) &&
                CommonService.getInstance().isCampaignValid(endingCampaign)) {

            for (int i = beginCampaign; i <= endingCampaign; i++) {
                allCampaigns.add(i);
            }

        } else if (LOGGER.isWarnEnabled()) {
            LOGGER.warn(
                    String.format(
                            "Échec de remonté des début et fin de campagne pour la valorisation avec comme ID: '%s' " +
                                    "les valeurs de début et/ou fin de campagne marketing sont invalides %d -> %d",
                            topiaId,
                            beginCampaign,
                            endingCampaign));
        }

        return allCampaigns;
    }

    protected Set<Integer> getValorisationCampaigns(HarvestingActionValorisationDto valorisationDto) {

        int beginCampaign = valorisationDto.getBeginMarketingPeriodCampaign();

        int endingCampaign = valorisationDto.getEndingMarketingPeriodCampaign();

        String topiaId = valorisationDto.getTopiaId().orElse("new");

        return getValorisationCampaigns(beginCampaign, endingCampaign, topiaId);
    }

    protected Set<Integer> getValorisationCampaigns(HarvestingActionValorisation valorisation) {

        int beginCampaign = valorisation.getBeginMarketingPeriodCampaign();

        int endingCampaign = valorisation.getEndingMarketingPeriodCampaign();

        String topiaId = valorisation.getTopiaId();

        return getValorisationCampaigns(beginCampaign, endingCampaign, topiaId);
    }

    @Override
    public LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> getRefHarvestingPricesForPracticedSystemValorisations(
            Collection<HarvestingActionValorisation> valorisations,
            String practicedSystemCampaigns,
            List<RefHarvestingPrice> refHarvestingPrices,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {

        LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> result = new LinkedHashMap<>();

        if (CollectionUtils.isNotEmpty(valorisations)) {

            validatePracticedSystemCampaigns(practicedSystemCampaigns);

            for (HarvestingActionValorisation valorisation : valorisations) {

                Set<Integer> allCampaigns = getValorisationCampaigns(valorisation);

                if (allCampaigns.isEmpty()) {
                    result.put(valorisation, new ArrayList<>());
                    continue;
                }

                List<RefHarvestingPrice> allValorisationPrices = refHarvestingPrices.stream()
                        .filter(rp -> valorisation.getDestination().getCode_destination_A().equals(rp.getCode_destination_A()))
                        .filter(rp -> valorisation.isIsOrganicCrop() == rp.isOrganic())
                        .filter(rp -> valorisation.getEndingMarketingPeriodCampaign() >= rp.getCampaign())
                        .toList();

                Set<Pair<String, String>> codeEspBotCodeQuali = codeEspBotCodeQualiBySpeciesCode.get(valorisation.getSpeciesCode());

                Optional<Set<RefHarvestingPrice>> optionalValorisationPrices = getPracticedRefHarvestingPricesForValorisation(
                        valorisation,
                        allValorisationPrices,
                        practicedSystemCampaigns,
                        codeEspBotCodeQuali);

                if (optionalValorisationPrices.isPresent()) {
                    Set<RefHarvestingPrice> valorisationPrices = optionalValorisationPrices.get();

                    if (valorisationPrices.isEmpty()) {
                        valorisationPrices = loopOverPracticedPreviousCampaigns(
                                valorisation,
                                allValorisationPrices,
                                practicedSystemCampaigns,
                                codeEspBotCodeQuali);
                    }
                    result.put(valorisation, new ArrayList<>(valorisationPrices));

                } else {
                    result.put(valorisation, new ArrayList<>());
                }

            }

        } else {
            result = new LinkedHashMap<>();
        }

        return result;
    }

    @Override
    public LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> getRefHarvestingPricesForPracticedSystemValorisations(
            Collection<HarvestingActionValorisation> valorisations,
            String practicedSystemCampaigns,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {

        LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> result = new LinkedHashMap<>();

        if (CollectionUtils.isNotEmpty(valorisations)) {

            validatePracticedSystemCampaigns(practicedSystemCampaigns);

            for (HarvestingActionValorisation valorisation : valorisations) {

                Set<Integer> allCampaigns = getValorisationCampaigns(valorisation);

                if (allCampaigns.isEmpty()) {
                    result.put(valorisation, new ArrayList<>());
                    continue;
                }

                Set<Pair<String, String>> codeEspBotCodeQuali = new HashSet<>(codeEspBotCodeQualiBySpeciesCode.get(valorisation.getSpeciesCode()));
                List<RefHarvestingPrice> allValorisationPrices = refHarvestingPriceDao.findRefHarvestingPricesForValorisation(
                        valorisation,
                        codeEspBotCodeQuali);

                Optional<Set<RefHarvestingPrice>> optionalValorisationPrices = getPracticedRefHarvestingPricesForValorisation(
                        valorisation,
                        allValorisationPrices,
                        practicedSystemCampaigns,
                        codeEspBotCodeQuali);

                if (optionalValorisationPrices.isPresent()) {
                    Set<RefHarvestingPrice> valorisationPrices = optionalValorisationPrices.get();

                    if (valorisationPrices.isEmpty()) {
                        valorisationPrices = loopOverPracticedPreviousCampaigns(
                                valorisation,
                                allValorisationPrices,
                                practicedSystemCampaigns,
                                codeEspBotCodeQuali);
                    }
                    result.put(valorisation, new ArrayList<>(valorisationPrices));

                } else {
                    result.put(valorisation, new ArrayList<>());
                }

            }

        } else {
            result = new LinkedHashMap<>();
        }

        return result;
    }

    protected Set<RefHarvestingPrice> loopOverPracticedPreviousCampaigns(
            HarvestingActionValorisation valorisation,
            List<RefHarvestingPrice> allValorisationPrices,
            String practicedSystemCampaigns,
            Set<Pair<String, String>> codeEspBotCodeQuali) {

        // Copy the valorisation to be able to apply change on it without concequences to the original one
        HarvestingActionValorisation valorisationCopy = new HarvestingActionValorisationImpl();
        HARVESTING_ACTION_VALORISATION_HARVESTING_ACTION_VALORISATION_BINDER.copy(valorisation, valorisationCopy);

        Optional<Set<RefHarvestingPrice>> optionalHarvestingPricesForValorisation = Optional.of(new HashSet<>());
        int nbTry = FIRST_TRY;

        while (optionalHarvestingPricesForValorisation.isPresent()
                && optionalHarvestingPricesForValorisation.get().isEmpty()
                && nbTry < 3) {

            final int beginCampaign = valorisationCopy.getBeginMarketingPeriodCampaign() - 1;
            valorisationCopy.setBeginMarketingPeriodCampaign(beginCampaign);
            final int endingCampaign = valorisationCopy.getEndingMarketingPeriodCampaign() - 1;
            valorisationCopy.setEndingMarketingPeriodCampaign(endingCampaign);

            // do not try on invalid campaigns
            if (CommonService.getInstance().isCampaignValid(beginCampaign) &&
                    CommonService.getInstance().isCampaignValid(endingCampaign)) {

                LOGGER.warn("TRY FIND HARVESTING REF PRICE FAILED, trying on campaign -1 " + beginCampaign + "; " + valorisationCopy.getSpeciesCode() + "; " + valorisationCopy.getDestination().getCode_destination_A());

                optionalHarvestingPricesForValorisation =
                        getPracticedRefHarvestingPricesForValorisation(
                                valorisationCopy,
                                allValorisationPrices,
                                practicedSystemCampaigns,
                                codeEspBotCodeQuali);


            } else {

                optionalHarvestingPricesForValorisation = Optional.empty();
            }

            nbTry++;
        }

        return optionalHarvestingPricesForValorisation.orElse(new HashSet<>());
    }

    protected Optional<Set<RefHarvestingPrice>> getPracticedRefHarvestingPricesForValorisation(
            HarvestingActionValorisation valorisation,
            List<RefHarvestingPrice> allValorisationPrices,
            String practicedSystemCampaigns,
            Set<Pair<String, String>> codeEspBotCodeQuali) {

        Set<Integer> allCampaigns = getValorisationCampaigns(valorisation);

        if (allCampaigns.isEmpty()) {
            return Optional.empty();
        }

        Set<RefHarvestingPrice> valorisationPricesResult = getPracticedRefHarvestingPricesFilteredOnPeriod(
                valorisation,
                allValorisationPrices,
                practicedSystemCampaigns);

        Set<RefHarvestingPrice> valorisationPrices = getRefHarvestingPricesFilteredOnSpecies(
                valorisationPricesResult,
                codeEspBotCodeQuali);

        if (valorisationPrices.isEmpty()) {
            valorisationPrices = valorisationPricesResult;
        }

        return Optional.of(valorisationPrices);
    }

    private Set<RefHarvestingPrice> getRefHarvestingPricesFilteredOnSpecies(
            Set<RefHarvestingPrice> valorisationPricesResult,
            Set<Pair<String, String>> valorisationCodeEspeceBotaniqueCodeQualifiantAee) {

        if (CollectionUtils.isEmpty(valorisationCodeEspeceBotaniqueCodeQualifiantAee)) return new HashSet<>();

        Set<RefHarvestingPrice> valorisationPrices = valorisationPricesResult.stream()
                .filter(refHarvestingPrice ->
                        valorisationCodeEspeceBotaniqueCodeQualifiantAee.contains(
                                Pair.of(refHarvestingPrice.getCode_espece_botanique(), refHarvestingPrice.getCode_qualifiant_AEE())
                        )
                )
                .collect(Collectors.toSet());

        valorisationPrices.addAll(valorisationPricesResult.stream()
                .filter(refHarvestingPrice ->
                        valorisationCodeEspeceBotaniqueCodeQualifiantAee.contains(
                                Pair.of(refHarvestingPrice.getCode_espece_botanique(), "")
                        ) || valorisationCodeEspeceBotaniqueCodeQualifiantAee.contains(
                                Pair.of(refHarvestingPrice.getCode_espece_botanique(), null)
                        )
                )
                .collect(Collectors.toSet()));

        return valorisationPrices;
    }

    protected Set<RefHarvestingPrice> getPracticedRefHarvestingPricesFilteredOnPeriod(
            HarvestingActionValorisation valorisation,
            List<RefHarvestingPrice> valorisationPrices,
            String practicedSystemCampaigns) {

        int valorisationBeginCampaign = valorisation.getBeginMarketingPeriodCampaign();
        Pair<Integer, Integer> beginEndingCampaigns = CommonService.GET_CAMPAINGS_RANGE.apply(practicedSystemCampaigns);
        int practicedSystemEndingCampaign = beginEndingCampaigns.getRight();

        int beginMarketingPeriod = valorisation.getBeginMarketingPeriod();
        int endingMarketingPeriod = valorisation.getEndingMarketingPeriod();

        List<Integer> allBeginPeriods = getPeriods(beginMarketingPeriod, endingMarketingPeriod);

        Set<Integer> valorisationCampaigns = new HashSet<>();
        for (int psc = valorisationBeginCampaign; psc <= practicedSystemEndingCampaign; psc++) {
            valorisationCampaigns.add(psc);
        }

        // -> FirstPeriodDecades
        Set<RefHarvestingPrice> valorisationPricesResult = new HashSet<>();

        final int beginPeriodDecade = valorisation.getBeginMarketingPeriodDecade();
        final int endingPeriodDecade = valorisation.getEndingMarketingPeriodDecade();

        if (CollectionUtils.isNotEmpty(allBeginPeriods)) {
            final int beginPeriod = allBeginPeriods.getFirst();

            if (valorisation.getBeginMarketingPeriod() == valorisation.getEndingMarketingPeriod()) {

                valorisationPricesResult.addAll(valorisationPrices.stream()
                        .filter(
                                refHarvestingPrice ->
                                        refHarvestingPrice.getMarketingPeriod() == beginPeriod &&
                                                refHarvestingPrice.getMarketingPeriodDecade() >= beginPeriodDecade &&
                                                valorisationCampaigns.contains(refHarvestingPrice.getCampaign()) &&
                                                refHarvestingPrice.getMarketingPeriodDecade() <= endingPeriodDecade
                        ).toList());
            } else {
                valorisationPricesResult.addAll(valorisationPrices.stream()
                        .filter(
                                refHarvestingPrice ->
                                        refHarvestingPrice.getMarketingPeriod() == beginPeriod &&
                                                refHarvestingPrice.getMarketingPeriodDecade() >= beginPeriodDecade &&
                                                valorisationCampaigns.contains(refHarvestingPrice.getCampaign())
                        )
                        .collect(Collectors.toSet()));
            }
        }

        // -> FirstCampaignCompletePeriods
        if (allBeginPeriods.size() > 2) {
            Set<Integer> begingIntermediatePeriods = new HashSet<>();
            for (int i = 1; i < allBeginPeriods.size() - 1; i++) {
                begingIntermediatePeriods.add(allBeginPeriods.get(i));
            }

            valorisationPricesResult.addAll(valorisationPrices.stream()
                    .filter(
                            refHarvestingPrice ->
                                    begingIntermediatePeriods.contains(refHarvestingPrice.getMarketingPeriod()) &&
                                            valorisationCampaigns.contains(refHarvestingPrice.getCampaign())
                    )
                    .collect(Collectors.toSet()));
        }

        // -> LastPeriodDecades
        if (valorisation.getBeginMarketingPeriod() != valorisation.getEndingMarketingPeriod()) {
            final List<Integer> periods = getPeriods(valorisation.getEndingMarketingPeriod(), valorisation.getEndingMarketingPeriod());
            Integer endingPeriod = periods.getFirst();
            valorisationPricesResult.addAll(valorisationPrices.stream()
                    .filter(
                            refHarvestingPrice ->
                                    refHarvestingPrice.getMarketingPeriod() == endingPeriod &&
                                            refHarvestingPrice.getMarketingPeriodDecade() <= endingPeriodDecade &&
                                            valorisationCampaigns.contains(refHarvestingPrice.getCampaign())
                    )
                    .collect(Collectors.toSet()));
        }

        // -> MatchAllPeriodsPrices
        valorisationPricesResult.addAll(valorisationPrices.stream()
                .filter(
                        refHarvestingPrice ->
                                refHarvestingPrice.getMarketingPeriod() == ALL_PERIODS &&
                                        refHarvestingPrice.getCampaign() >= valorisationBeginCampaign &&//beginCampaignAll_
                                        refHarvestingPrice.getCampaign() <= practicedSystemEndingCampaign//endingCampaignAll_

                )
                .collect(Collectors.toSet()));

        return valorisationPricesResult;
    }

    @Deprecated
    @Override
    public LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> getRefHarvestingPricesForValorisations(
            Collection<HarvestingActionValorisation> valorisations,
            List<RefHarvestingPrice> refHarvestingPrices,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {

        LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> result = new LinkedHashMap<>();
        if (CollectionUtils.isNotEmpty(valorisations)) {

            for (HarvestingActionValorisation valorisation : valorisations) {

                Set<Integer> allCampaigns = getValorisationCampaigns(valorisation);

                if (allCampaigns.isEmpty()) {
                    result.put(valorisation, new ArrayList<>());
                    continue;
                }

                Set<Pair<String, String>> codeEspBotCodeQuali = codeEspBotCodeQualiBySpeciesCode.get(valorisation.getSpeciesCode());
                List<RefHarvestingPrice> allValorisationPrices = refHarvestingPriceDao.findRefHarvestingPricesForValorisation(
                        valorisation,
                        codeEspBotCodeQuali);

                Optional<Set<RefHarvestingPrice>> optionalValorisationPrices = getEffectiveRefHarvestingPricesForValorisation(
                        valorisation,
                        allValorisationPrices,
                        codeEspBotCodeQuali);

                if (optionalValorisationPrices.isEmpty()) {
                    result.put(valorisation, new ArrayList<>());
                    continue;
                }

                Set<RefHarvestingPrice> valorisationPrices = optionalValorisationPrices.get();

                if (valorisationPrices.isEmpty()) {
                    valorisationPrices = loopOverEffectivePreviousCampaigns(
                            valorisation,
                            allValorisationPrices,
                            codeEspBotCodeQuali);
                }

                result.put(valorisation, new ArrayList<>(valorisationPrices));

            }
        }
        return result;
    }

    @Override
    public LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> getRefHarvestingPricesForValorisations(
            Collection<HarvestingActionValorisation> valorisations,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {

        LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> result = new LinkedHashMap<>();
        if (CollectionUtils.isNotEmpty(valorisations)) {

            for (HarvestingActionValorisation valorisation : valorisations) {

                Set<Integer> allCampaigns = getValorisationCampaigns(valorisation);

                if (allCampaigns.isEmpty()) {
                    result.put(valorisation, new ArrayList<>());
                    continue;
                }

                Set<Pair<String, String>> codeEspBotCodeQuali = codeEspBotCodeQualiBySpeciesCode.get(valorisation.getSpeciesCode());
                List<RefHarvestingPrice> allValorisationPrices = refHarvestingPriceDao.findRefHarvestingPricesForValorisation(
                        valorisation,
                        codeEspBotCodeQuali);

                Optional<Set<RefHarvestingPrice>> optionalValorisationPrices = getEffectiveRefHarvestingPricesForValorisation(
                        valorisation,
                        allValorisationPrices,
                        codeEspBotCodeQuali);

                if (optionalValorisationPrices.isEmpty()) {
                    result.put(valorisation, new ArrayList<>());
                    continue;
                }

                Set<RefHarvestingPrice> valorisationPrices = optionalValorisationPrices.get();

                if (valorisationPrices.isEmpty()) {
                    valorisationPrices = loopOverEffectivePreviousCampaigns(
                            valorisation,
                            allValorisationPrices,
                            codeEspBotCodeQuali);
                }

                result.put(valorisation, new ArrayList<>(valorisationPrices));

            }
        }
        return result;
    }

    @Override
    public Map<HarvestingActionValorisationDto, List<RefHarvestingPrice>> getRefHarvestingPricesForValorisationDtos(
            Collection<HarvestingActionValorisationDto> valorisationDtos,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {

        Map<HarvestingActionValorisationDto, List<RefHarvestingPrice>> result = new HashMap<>();
        if (CollectionUtils.isNotEmpty(valorisationDtos)) {

            for (HarvestingActionValorisationDto valorisationDto : valorisationDtos) {

                Set<Integer> allCampaigns = getValorisationCampaigns(valorisationDto);

                if (allCampaigns.isEmpty()) {
                    result.put(valorisationDto, new ArrayList<>());
                    continue;
                }

                Set<Pair<String, String>> codeEspBotCodeQuali = codeEspBotCodeQualiBySpeciesCode.get(valorisationDto.getSpeciesCode());
                List<RefHarvestingPrice> allValorisationPrices = refHarvestingPriceDao.findRefHarvestingPricesForValorisation(
                        valorisationDto,
                        codeEspBotCodeQuali);

                Optional<Set<RefHarvestingPrice>> optionalValorisationPrices = getEffectiveRefHarvestingPricesForValorisation(
                        valorisationDto,
                        allValorisationPrices,
                        codeEspBotCodeQuali);

                if (optionalValorisationPrices.isEmpty()) {
                    result.put(valorisationDto, new ArrayList<>());
                    continue;
                }

                Set<RefHarvestingPrice> valorisationPrices = optionalValorisationPrices.get();

                if (valorisationPrices.isEmpty()) {
                    valorisationPrices = loopOverEffectivePreviousCampaigns(
                            valorisationDto,
                            allValorisationPrices,
                            codeEspBotCodeQuali);
                }

                result.put(valorisationDto, new ArrayList<>(valorisationPrices));

            }
        }
        return result;
    }

    protected Optional<Set<RefHarvestingPrice>> getEffectiveRefHarvestingPricesForValorisation(
            HarvestingActionValorisationDto valorisationDto,
            List<RefHarvestingPrice> allValorisationPrices,
            Set<Pair<String, String>> codeEspBotCodeQuali) {

        Set<Integer> allCampaigns = getValorisationCampaigns(valorisationDto);
        if (allCampaigns.isEmpty()) {
            return Optional.empty();
        }

        Set<RefHarvestingPrice> valorisationPricesResult = getEffectiveRefHarvestingPricesFilteredOnPeriod(
                valorisationDto,
                allValorisationPrices);


        Set<RefHarvestingPrice> valorisationPrices = getRefHarvestingPricesFilteredOnSpecies(
                valorisationPricesResult,
                codeEspBotCodeQuali);

        if (valorisationPrices.isEmpty()) {
            valorisationPrices = valorisationPricesResult;
        }

        return Optional.of(valorisationPrices);
    }

    protected Optional<Set<RefHarvestingPrice>> getEffectiveRefHarvestingPricesForValorisation(
            HarvestingActionValorisation valorisation,
            List<RefHarvestingPrice> allValorisationPrices,
            Set<Pair<String, String>> codeEspBotCodeQuali) {

        Set<Integer> allCampaigns = getValorisationCampaigns(valorisation);
        if (allCampaigns.isEmpty()) {
            return Optional.empty();
        }

        Set<RefHarvestingPrice> valorisationPricesResult = getEffectiveRefHarvestingPricesFilteredOnPeriod(
                valorisation,
                allValorisationPrices);


        Set<RefHarvestingPrice> valorisationPrices = getRefHarvestingPricesFilteredOnSpecies(
                valorisationPricesResult,
                codeEspBotCodeQuali);

        if (valorisationPrices.isEmpty()) {
            valorisationPrices = valorisationPricesResult;
        }

        return Optional.of(valorisationPrices);
    }

    protected @NotNull Set<RefHarvestingPrice> getEffectiveRefHarvestingPricesFilteredOnPeriod(
            List<RefHarvestingPrice> valorisationPrices,
            int beginCampaign,
            int endingCampaign,
            int endingMarketingPeriod1,
            int beginMarketingPeriod,
            int beginMarketingPeriodDecade,
            int endingMarketingPeriodDecade) {

        Optional<Set<Integer>> entireCampaigns = getEntireYears(beginCampaign, endingCampaign);

        int endingMarketingPeriod = entireCampaigns.isPresent() ? 11 : endingMarketingPeriod1;
        List<Integer> allBeginPeriods = getPeriods(beginMarketingPeriod, endingMarketingPeriod);

        // start sub query
        // -> FirstPeriodDecadesPrices
        Set<RefHarvestingPrice> valorisationPricesResult = new HashSet<>();
        if (CollectionUtils.isNotEmpty(allBeginPeriods)) {
            final int beginPeriod = allBeginPeriods.getFirst();

            int beginPeriodDecade = beginMarketingPeriodDecade;

            if (beginCampaign == endingCampaign &&
                    beginMarketingPeriod == endingMarketingPeriod1) {

                valorisationPricesResult.addAll(valorisationPrices.stream()
                        .filter(
                                refHarvestingPrice ->
                                        beginPeriod == refHarvestingPrice.getMarketingPeriod() &&
                                                refHarvestingPrice.getMarketingPeriodDecade() >= beginPeriodDecade &&
                                                refHarvestingPrice.getCampaign() == beginCampaign &&
                                                refHarvestingPrice.getMarketingPeriodDecade() >= beginPeriodDecade &&
                                                refHarvestingPrice.getMarketingPeriodDecade() <= endingMarketingPeriodDecade
                        )
                        .collect(Collectors.toSet()));
            } else {
                valorisationPricesResult.addAll(valorisationPrices.stream()
                        .filter(
                                refHarvestingPrice ->
                                        beginPeriod == refHarvestingPrice.getMarketingPeriod() &&
                                                refHarvestingPrice.getMarketingPeriodDecade() >= beginPeriodDecade &&
                                                refHarvestingPrice.getCampaign() == beginCampaign
                        )
                        .collect(Collectors.toSet()));
            }
        }

        // -> FirstCampaignCompletePeriodsPrices
        if (allBeginPeriods.size() > 2) {

            //      if beginCampaign and endingCampaign is on the same campaign last month must not be include into this query
            //      as it is not a complete month but decade has to be consider.
            //      last month will be manage in getPricesQueryForLastPeriodDecades() methode
            //
            Set<Integer> begingIntermediatePeriods = new HashSet<>();
            int nbMonthRemove = beginCampaign == endingCampaign ? 1 : 0;
            for (int i = 1; i < allBeginPeriods.size() - nbMonthRemove; i++) {
                begingIntermediatePeriods.add(allBeginPeriods.get(i));
            }

            valorisationPricesResult.addAll(valorisationPrices.stream()
                    .filter(
                            refHarvestingPrice ->
                                    begingIntermediatePeriods.contains(refHarvestingPrice.getMarketingPeriod()) &&
                                            refHarvestingPrice.getCampaign() == beginCampaign
                    )
                    .collect(Collectors.toSet()));
        }

        // -> CompleteCampaignsPrices
        entireCampaigns.ifPresent(
                integers -> valorisationPricesResult.addAll(valorisationPrices.stream()
                        .filter(
                                refHarvestingPrice ->
                                        integers
                                                .contains(refHarvestingPrice.getCampaign())
                        )
                        .collect(Collectors.toSet())));

        // -> LastCampaignCompletePeriodsPrices
        int beginningPeriod = beginCampaign == endingCampaign ? beginMarketingPeriod + 1 : 0;
        List<Integer> allEndingPeriods = getPeriods(beginningPeriod, endingMarketingPeriod1);

        if (allEndingPeriods.size() > 2) {

            Set<Integer> endingIntermediatePeriods = new HashSet<>();
            for (int i = 0; i < allEndingPeriods.size() - 1; i++) {
                endingIntermediatePeriods.add(allEndingPeriods.get(i));
            }

            valorisationPricesResult.addAll(valorisationPrices.stream()
                    .filter(
                            refHarvestingPrice ->
                                    endingIntermediatePeriods.contains(refHarvestingPrice.getMarketingPeriod()) &&
                                            refHarvestingPrice.getCampaign() == endingCampaign
                    )
                    .collect(Collectors.toSet()));
        }


        // -> LastPeriodDecadePrices
        if (beginCampaign != endingCampaign ||
                beginMarketingPeriod != endingMarketingPeriod1) {
            Integer endingPeriod = getPeriods(endingMarketingPeriod1, endingMarketingPeriod1).getFirst();
            int lastPeriodDecade = endingMarketingPeriodDecade;
            valorisationPricesResult.addAll(valorisationPrices.stream()
                    .filter(
                            refHarvestingPrice ->
                                    refHarvestingPrice.getMarketingPeriod() == endingPeriod &&
                                            refHarvestingPrice.getMarketingPeriodDecade() <= lastPeriodDecade &&
                                            refHarvestingPrice.getCampaign() == endingCampaign
                    )
                    .collect(Collectors.toSet()));
        }

        // -> MatchAllPeriodsPrices
        valorisationPricesResult.addAll(valorisationPrices.stream()
                .filter(
                        refHarvestingPrice ->
                                refHarvestingPrice.getMarketingPeriod() == ALL_PERIODS &&
                                        refHarvestingPrice.getCampaign() >= beginCampaign &&
                                        refHarvestingPrice.getCampaign() <= endingCampaign
                )
                .collect(Collectors.toSet()));

        return valorisationPricesResult;
    }

    protected Set<RefHarvestingPrice> getEffectiveRefHarvestingPricesFilteredOnPeriod(
            HarvestingActionValorisationDto valorisationDto,
            List<RefHarvestingPrice> valorisationPrices) {

        int beginCampaign = valorisationDto.getBeginMarketingPeriodCampaign();
        int endingCampaign = valorisationDto.getEndingMarketingPeriodCampaign();
        int beginMarketingPeriod = valorisationDto.getBeginMarketingPeriod();
        int endingMarketingPeriod1 = valorisationDto.getEndingMarketingPeriod();
        int beginMarketingPeriodDecade = valorisationDto.getBeginMarketingPeriodDecade();
        int endingMarketingPeriodDecade = valorisationDto.getEndingMarketingPeriodDecade();

        return getEffectiveRefHarvestingPricesFilteredOnPeriod(
                valorisationPrices,
                beginCampaign,
                endingCampaign,
                endingMarketingPeriod1,
                beginMarketingPeriod,
                beginMarketingPeriodDecade,
                endingMarketingPeriodDecade);
    }

    protected Set<RefHarvestingPrice> getEffectiveRefHarvestingPricesFilteredOnPeriod(
            HarvestingActionValorisation valorisation,
            List<RefHarvestingPrice> valorisationPrices) {

        int beginCampaign = valorisation.getBeginMarketingPeriodCampaign();
        int endingCampaign = valorisation.getEndingMarketingPeriodCampaign();
        int beginMarketingPeriod = valorisation.getBeginMarketingPeriod();
        int endingMarketingPeriod1 = valorisation.getEndingMarketingPeriod();
        int beginMarketingPeriodDecade = valorisation.getBeginMarketingPeriodDecade();
        int endingMarketingPeriodDecade = valorisation.getEndingMarketingPeriodDecade();

        return getEffectiveRefHarvestingPricesFilteredOnPeriod(
                valorisationPrices,
                beginCampaign,
                endingCampaign,
                endingMarketingPeriod1,
                beginMarketingPeriod,
                beginMarketingPeriodDecade,
                endingMarketingPeriodDecade);
    }

    protected Set<RefHarvestingPrice> loopOverEffectivePreviousCampaigns(
            HarvestingActionValorisationDto valorisation,
            List<RefHarvestingPrice> allValorisationPrices,
            Set<Pair<String, String>> codeEspBotCodeQuali) {

        // Copy the valorisation to be able to apply change on it without concequences to the original one
        int beginCampaign = valorisation.getBeginMarketingPeriodCampaign();
        int endingCampaign = valorisation.getEndingMarketingPeriodCampaign();

        Optional<Set<RefHarvestingPrice>> optionalHarvestingPricesForValorisation = Optional.of(new HashSet<>());
        int nbTry = FIRST_TRY;

        while (optionalHarvestingPricesForValorisation.isPresent()
                && optionalHarvestingPricesForValorisation.get().isEmpty()
                && nbTry < 10) {

            beginCampaign = beginCampaign - 1;
            endingCampaign = endingCampaign - 1;

            // do not try on invalid campaigns
            if (CommonService.getInstance().isCampaignValid(beginCampaign) &&
                    CommonService.getInstance().isCampaignValid(endingCampaign)) {

                HarvestingActionValorisationDto valorisationCopy = valorisation.toBuilder()
                        .beginMarketingPeriodCampaign(beginCampaign)
                        .endingMarketingPeriodCampaign(endingCampaign)
                        .build();

                optionalHarvestingPricesForValorisation =
                        getEffectiveRefHarvestingPricesForValorisation(
                                valorisationCopy,
                                allValorisationPrices,
                                codeEspBotCodeQuali);

            } else {

                optionalHarvestingPricesForValorisation = Optional.empty();
            }

            nbTry++;
        }

        return optionalHarvestingPricesForValorisation.orElse(new HashSet<>());
    }

    protected Set<RefHarvestingPrice> loopOverEffectivePreviousCampaigns(
            HarvestingActionValorisation valorisation,
            List<RefHarvestingPrice> allValorisationPrices,
            Set<Pair<String, String>> codeEspBotCodeQuali) {

        // Copy the valorisation to be able to apply change on it without concequences to the original one
        HarvestingActionValorisation valorisationCopy = new HarvestingActionValorisationImpl();
        HARVESTING_ACTION_VALORISATION_HARVESTING_ACTION_VALORISATION_BINDER.copy(valorisation, valorisationCopy);

        Optional<Set<RefHarvestingPrice>> optionalHarvestingPricesForValorisation = Optional.of(new HashSet<>());
        int nbTry = FIRST_TRY;

        while (optionalHarvestingPricesForValorisation.isPresent()
                && optionalHarvestingPricesForValorisation.get().isEmpty()
                && nbTry < 10) {

            final int beginCampaign = valorisationCopy.getBeginMarketingPeriodCampaign() - 1;
            valorisationCopy.setBeginMarketingPeriodCampaign(beginCampaign);
            final int endingCampaign = valorisationCopy.getEndingMarketingPeriodCampaign() - 1;
            valorisationCopy.setEndingMarketingPeriodCampaign(endingCampaign);

            // do not try on invalid campaigns
            if (CommonService.getInstance().isCampaignValid(beginCampaign) &&
                    CommonService.getInstance().isCampaignValid(endingCampaign)) {

                optionalHarvestingPricesForValorisation =
                        getEffectiveRefHarvestingPricesForValorisation(
                                valorisationCopy,
                                allValorisationPrices,
                                codeEspBotCodeQuali);

            } else {

                optionalHarvestingPricesForValorisation = Optional.empty();
            }

            nbTry++;
        }

        return optionalHarvestingPricesForValorisation.orElse(new HashSet<>());
    }

    @Override
    public EnumMap<Sector, List<RefMarketingDestination>> getRefMarketingDestinationsBySectorForSectors(Set<Sector> sectors) {
        EnumMap<Sector, List<RefMarketingDestination>> result = new EnumMap<>(Sector.class);

        List<RefMarketingDestination> result_ = refMarketingDestinationDao
                .forSectorIn(sectors)
                .addEquals(RefMarketingDestination.PROPERTY_ACTIVE, true)
                .setOrderByArguments(RefMarketingDestination.PROPERTY_MARKETING_DESTINATION)
                .findAll();

        for (RefMarketingDestination refMarketingDestination : result_) {

            Sector sector = refMarketingDestination.getSector();
            List<RefMarketingDestination> destinationsForSector = result.computeIfAbsent(sector, k -> new ArrayList<>());
            destinationsForSector.add(refMarketingDestination);

        }
        return result;
    }

    @Override
    public List<RefTraitSdC> getTraitSdcForIdsIn(List<String> growingSystemCharacteristicIds) {
        return refTraitSdCDao
                .forTopiaIdIn(growingSystemCharacteristicIds)
                .addEquals(RefTraitSdC.PROPERTY_ACTIVE, true)
                .findAll();
    }

    @Override
    public RefTypeAgriculture getRefTypeAgricultureWithId(String typeAgricultureId) {
        return refTypeAgricultureDao.forTopiaIdEquals(typeAgricultureId).findUnique();
    }

    @Override
    public List<RefTypeAgriculture> getAllTypeAgricultures() {
        Language language = getSecurityContext().getLanguage();
        return refTypeAgricultureDao.findAllTypeAgricultures(language);
    }

    @Override
    public List<RefTypeAgriculture> getIpmWorksTypeAgricultures() {
        Language language = getSecurityContext().getLanguage();
        List<Integer> ipmWorksTypesConduite = getConfig().getIpmWorksTypesConduite();
        return refTypeAgricultureDao.findTypeAgricultures(language, ipmWorksTypesConduite);
    }

    @Override
    public List<RefStrategyLever> loadRefStrategyLeversForTerm(
            Sector sector,
            SectionType sectionType,
            StrategyType strategyType,
            String term
    ) {

        List<RefStrategyLever> result = refStrategyLeverDao.findRefStrategyLevers(sector, sectionType, strategyType, term);
        return result;
    }

    @Override
    public List<RefStrategyLever> loadRefStrategyLevers(
            String growingSystemTopiaId,
            SectionType sectionType,
            StrategyType strategyType) {
        return refStrategyLeverDao.findRefStrategyLevers(growingSystemTopiaId, sectionType, strategyType);
    }

    @Override
    public List<RefAnimalType> getAllActiveRefAnimalTypes() {
        return refAnimalTypeDao.forActiveEquals(true).findAll();
    }

    @Override
    public List<RefAnimalTypeDto> getAllTranslatedActiveRefAnimalTypes() {
        Language language = getSecurityContext().getLanguage();
        return refAnimalTypeDao.findAllActive(language);
    }

    @Override
    public List<RefLegalStatus> getAllRefActiveLegalStatus() {
        return refLegalStatusDao.forActiveEquals(true).findAll();
    }

    @Override
    public List<Pair<String, String>> getAllCodeEspeceBotaniqueCodeQualifantForDomainIds(Set<String> domainIds) {

        return CollectionUtils.isNotEmpty(domainIds) ?
                croppingPlanSpeciesDao.loadCodeEspeceBotaniquesCodeQualifiantAEEForDomainId(domainIds)
                : new ArrayList<>();
    }

    @Override
    public Map<String, List<Pair<String, String>>> getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(Set<String> domainIds) {
        Map<String, List<Pair<String, String>>> result = new HashMap<>();

        if (CollectionUtils.isNotEmpty(domainIds)) {
            List<Object[]> speciesCodeCodeEspeceBotaniqueCodeQualifiants =
                    croppingPlanSpeciesDao.loadSpeciesCodeCodeEspeceBotaniquesCodeQualifiantAEEForDomainId(domainIds);

            for (Object[] speciesCodeCodeEspeceBotaniqueCodeQualifiant : speciesCodeCodeEspeceBotaniqueCodeQualifiants) {
                String speciesCode = (String) speciesCodeCodeEspeceBotaniqueCodeQualifiant[0];
                String codeCodeEspeceBotanique = (String) speciesCodeCodeEspeceBotaniqueCodeQualifiant[1];
                String codeQualifiant = (String) speciesCodeCodeEspeceBotaniqueCodeQualifiant[2];
                List<Pair<String, String>> codeCodeEspeceBotaniquecodeQualifiant = result.computeIfAbsent(speciesCode, k -> new ArrayList<>());
                codeCodeEspeceBotaniquecodeQualifiant.add(Pair.of(codeCodeEspeceBotanique, codeQualifiant));
            }
        }

        return result;
    }

    @Override
    public Map<String, Set<Pair<String, String>>> getCodeEspBotCodeQualiBySpeciesCode(Set<String> speciesCodes) {
        Map<String, Set<Pair<String, String>>> res = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);
        for (String speciesCode : speciesCodes) {
            res.computeIfAbsent(speciesCode, k -> new HashSet<>());
        }
        return res;
    }

    protected List<Pair<String, String>> getCodeEspeceBotaniquesCodeQualifiantAEE(Set<String> speciesCodes) {
        return croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifiantAEEForSpeciesCode(speciesCodes);
    }

    protected List<RefDestination> getRefDestinations(
            Map<Pair<String, String>, List<Sector>> sectorsByCodeEspeceBotaniqueCodeQualifiant,
            Set<WineValorisation> wineValorisations) {
        Language language = getSecurityContext().getLanguage();
        return refDestinationDao.getDestinations(language, sectorsByCodeEspeceBotaniqueCodeQualifiant, wineValorisations);
    }

    protected List<RefQualityCriteria> getRefQualityCriteria(
            Map<Pair<String, String>, List<Sector>> sectorsByCodeEspeceBotaniqueCodeQualifian,
            Set<WineValorisation> wineValorisations) {
        Language language = getSecurityContext().getLanguage();
        return refQualityCriteriaDao.getRefQualityCriteria(
                language,
                sectorsByCodeEspeceBotaniqueCodeQualifian,
                wineValorisations);
    }

    @Override
    public List<RefBioAgressor> getBioAgressors(Collection<BioAgressorType> bioAgressorTypes) {
        return getBioAgressors(bioAgressorTypes, null);
    }

    @Override
    public List<RefBioAgressor> getBioAgressors(Collection<BioAgressorType> bioAgressorTypes, Collection<Sector> sectors) {
        Language language = getSecurityContext().getLanguage();

        List<RefBioAgressor> result = new ArrayList<>();

        List<BioAgressorType> bioAgressorTypes0 = new ArrayList<>(bioAgressorTypes);

        if (bioAgressorTypes0.contains(BioAgressorType.ADVENTICE)) {
            bioAgressorTypes0.remove(BioAgressorType.ADVENTICE);
            result.addAll(refAdventiceDao.findActiveRefAdventiceForBioAgressorType(language));
        }

        if (CollectionUtils.isNotEmpty(bioAgressorTypes0)) {
            result.addAll(refNuisibleEDIDao.findActiveRefNuisibleEDIForBioAgressorType(language, bioAgressorTypes0, sectors));
        }

        result.sort(Comparator.comparing(RefBioAgressor::getLabel, String.CASE_INSENSITIVE_ORDER));

        return result;
    }

    @Override
    public ExportResult exportAllSpeciesXslx() {
        ExportResult result;
        try {
            Language language = getSecurityContext().getLanguage();
            List<Pair<RefEspece, RefVariete>> especeAndVariete = refEspeceDao.findEspeceAndVariete();

            Set<String> speciesIds = especeAndVariete.stream().map(pair -> pair.getKey().getTopiaId()).collect(Collectors.toSet());
            ReferentialTranslationMap translationMap = new ReferentialTranslationMap(language);
            i18nService.fillRefEspeceTranslations(speciesIds, translationMap);

            especeAndVariete = especeAndVariete.stream()
                    .peek(o -> {
                        RefEspece species = o.getKey();
                        CroppingPlans.bindTranslationToRefEspece(translationMap, species);
                    })
                    .sorted(Comparator.comparing(o -> {
                        RefEspece key = o.getKey();
                        RefVariete variety = o.getValue();
                        return (
                                (StringUtils.isNotBlank(key.getLibelle_espece_botanique_Translated()) ? key.getLibelle_espece_botanique_Translated() : "#") +
                                        (StringUtils.isNotBlank(key.getLibelle_qualifiant_AEE_Translated()) ? key.getLibelle_qualifiant_AEE_Translated() : "#") +
                                        (StringUtils.isNotBlank(key.getCode_type_saisonnier_AEE()) ? key.getCode_type_saisonnier_AEE() : "#") +
                                        (StringUtils.isNotBlank(key.getLibelle_destination_AEE_Translated()) ? key.getLibelle_destination_AEE_Translated() : "#") +
                                        (variety != null ? variety.getLabel() : "#")
                        ).toUpperCase();
                    }))
                    .toList();

            List<AllSpeciesExport> entities = especeAndVariete.stream()
                    .map(AllSpeciesExport::new)
                    .distinct()
                    .collect(Collectors.toList());

            ExportModelAndRows<AllSpeciesExport> rows = new ExportModelAndRows<>(new AllSpeciesExportModel(), entities);

            EntityExporter exporter = new EntityExporter();
            result = exporter.exportAsXlsx("Espèces référencées dans Agrosyst", rows);

        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't export", ex);
        }
        return result;
    }

    public static class AllSpeciesExportModel extends ExportModel<AllSpeciesExport> {

        @Override
        public String getTitle() {
            return "Espèces référencées dans Agrosyst";
        }

        public AllSpeciesExportModel() {
            super();
            newColumn(I18n.t("AllSpeciesExportModel.libelle_espece_botanique_qualifiant_aee"), AllSpeciesExport::getLibelle_espece_botanique_qualifiant_aee);
            newColumn(I18n.t("AllSpeciesExportModel.variete_label"), AllSpeciesExport::getVariete_label);
        }
    }

    @Override
    public Collection<EquipmentUsageRange> getEquipmentlUsageRangeForEquipments(Collection<RefMateriel> materiels) {
        return CollectionUtils.isEmpty(materiels) ? new ArrayList<>() : refMaterielTractionDao.findEquipmentsUsages(materiels);

    }

    public <U> Map<Enum<?>, List<PriceUnit>> setPriceUnitsByProductUnit(
            InputPriceCategory categ,
            List<RefInputUnitPriceUnitConverter> productUnitsToPriceUnitsForCateg,
            Class<U> c) {

        Map<Enum<?>, List<PriceUnit>> allPriceUnitsForProductUnits = new HashMap<>();

        Method m;
        try {
            // use introspection to get the correct enum identified by it's class name: PhytoProductUnit -> getPhytoProductUnit
            m = RefInputUnitPriceUnitConverter.class.getMethod("get" + c.getSimpleName());
        } catch (NoSuchMethodException e) {
            LOGGER.error("Failed get Prices Units for " + categ);
            throw new AgrosystTechnicalException("Failed get Prices Units for " + categ, e);
        }

        // get PriceUnits allowed for product
        for (RefInputUnitPriceUnitConverter productUnitToPriceUnitForCateg : productUnitsToPriceUnitsForCateg) {
            try {
                U matchingProductUnitPriceUnits = (U) m.invoke(productUnitToPriceUnitForCateg);
                if (matchingProductUnitPriceUnits != null) {
                    List<PriceUnit> allPriceUnitsForProductUnit =
                            allPriceUnitsForProductUnits
                                    .computeIfAbsent(
                                            (Enum<?>) matchingProductUnitPriceUnits,
                                            k -> new ArrayList<>()
                                    );
                    PriceUnit pu = productUnitToPriceUnitForCateg.getPriceUnit();
                    allPriceUnitsForProductUnit.add(pu);
                }
            } catch (IllegalAccessException | InvocationTargetException e) {
                LOGGER.error("Failed get Prices Units for " + categ);
                throw new AgrosystTechnicalException("Failed get Prices Units for " + categ, e);
            }
        }
        return allPriceUnitsForProductUnits;
    }

    @Override
    public ExportResult exportAllMaterielXlsx() {
        ExportResult result;

        try {
            List<RefMateriel> materiels = refMaterielDao.forActiveEquals(true).findAll();

            Set<String> topiaIds = materiels.stream()
                    .map(TopiaEntity::getTopiaId)
                    .collect(Collectors.toSet());

            ReferentialTranslationMap translationMap = new ReferentialTranslationMap(getSecurityContext().getLanguage());
            refMaterielDao.fillTranslations(topiaIds, translationMap);
            materiels.forEach(m -> AgrosystI18nService.translateMateriel(m, translationMap));

            List<AllMaterielExport> entities = materiels.stream()
                    .map(AllMaterielExport::new)
                    .distinct()
                    .collect(Collectors.toList());

            Map<MaterielType, String> translatedMaterielTypes = i18nService.getEnumTranslationMap(MaterielType.class);

            ExportModelAndRows<AllMaterielExport> data = new ExportModelAndRows<>(new AllMaterielExportModel(translatedMaterielTypes), entities);

            EntityExporter exporter = new EntityExporter();
            result = exporter.exportAsXlsx("Agrosyst export Materiels", data);


        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't export", ex);
        }
        return result;
    }

    public static class AllMaterielExportModel extends ExportModel<AllMaterielExport> {

        protected final Map<MaterielType, String> translatedMaterielTypes;

        @Override
        public String getTitle() {
            return "Export Materiels";
        }

        public AllMaterielExportModel(Map<MaterielType, String> translatedMaterielTypes) {
            super();
            this.translatedMaterielTypes = translatedMaterielTypes;
            newColumn(I18n.t("AllMaterielExportModel.category"), translatedMaterielTypes::get);
            newColumn(I18n.t("AllMaterielExportModel.typeMateriel1"), AllMaterielExport::getTypeMateriel1);
            newColumn(I18n.t("AllMaterielExportModel.typeMateriel2"), AllMaterielExport::getTypeMateriel2);
            newColumn(I18n.t("AllMaterielExportModel.typeMateriel3"), AllMaterielExport::getTypeMateriel3);
            newColumn(I18n.t("AllMaterielExportModel.typeMateriel4"), AllMaterielExport::getTypeMateriel4);
            newColumn(I18n.t("AllMaterielExportModel.manualTool"), AllMaterielExport::isManualTool);
        }


    }

    @Override
    public List<RefCattleAnimalTypeDto> getAllTranslatedActiveRefCattleAnimalTypes() {
        Language language = getSecurityContext().getLanguage();
        return refCattleAnimalTypeDao.findAllActive(language);
    }

    @Override
    public List<RefCattleRationAlimentDto> getAllTranslatedActiveRefCattleRationAliments() {
        Language language = getSecurityContext().getLanguage();
        return refCattleRationAlimentDao.findAllActive(language);
    }

    @Override
    public List<RefSubstrateDto> getAllActiveRefSubstrates() {
        List<RefSubstrateDto> allActiveSubstrates = refSubstrateDao.findAllActiveSubstrates(getSecurityContext().getLanguage());
        return allActiveSubstrates;
    }

    @Override
    public List<RefPotDto> getAllActiveRefPots() {
        List<RefPotDto> allActivePots = refPotDao.findAllActivePots(getSecurityContext().getLanguage());
        return allActivePots;
    }

    @Override
    public List<RefOtherInput> getAllActiveRefOtherInputs() {
        Language language = getSecurityContext().getLanguage();
        List<RefOtherInput> result = refOtherInputDao.findAllActiveOtherInputs(language);
        String emptyString = l(language.getLocale(), DomainInputBinder.class.getName() + "." + "DEFAULT_EMPTY_VIDE");
        for (RefOtherInput refInput : result) {
            if (StringUtils.isEmpty(refInput.getInputType_c0())) {
                refInput.setInputType_c0(emptyString);
            }
            if (StringUtils.isEmpty(refInput.getCaracteristic1())) {
                refInput.setCaracteristic1(emptyString);
            }
            if (StringUtils.isEmpty(refInput.getCaracteristic2())) {
                refInput.setCaracteristic2(emptyString);
            }
            if (StringUtils.isEmpty(refInput.getCaracteristic3())) {
                refInput.setCaracteristic3(emptyString);
            }
        }
        return result;
    }

    @Override
    public ExportResult exportAllActiveRefOtherInputsToXlsx() {

        final List<RefOtherInput> allActiveRefOtherInputs = this.getAllActiveRefOtherInputs();
        ExportResult result;

        try {

            ExportModelAndRows<RefOtherInput> data = new ExportModelAndRows<>(new RefOtherInputModel(getAuthenticatedUser().isAdmin()), allActiveRefOtherInputs);

            EntityExporter exporter = new EntityExporter();
            result = exporter.exportAsXlsx("Intrants autres et petits équipements référencés dans Agrosyst", data);

        } catch (Exception e) {
            throw new AgrosystTechnicalException("Can't export", e);
        }

        return result;
    }

    public static class RefOtherInputModel extends ExportModel<RefOtherInput> {

        @Override
        public String getTitle() {
            return "Intrants autres et petits équipements référencés dans Agrosyst";
        }

        public RefOtherInputModel(boolean isExportForAdmin) {
            if (isExportForAdmin) {
                newColumn("reference_id", RefOtherInput::getReference_id);
                newColumn("reference_code", RefOtherInput::getReference_code);
            }

            newColumn("Type d'intrants", RefOtherInput::getInputType_c0);
            newColumn("Caractéristique 1", RefOtherInput::getCaracteristic1);
            newColumn("Caractéristique 2", RefOtherInput::getCaracteristic2);
            newColumn("Caractéristique 3", RefOtherInput::getCaracteristic3);
            newColumn("Unité", RefOtherInput::getUnit);
            newColumn("Durée de vie", RefOtherInput::getLifetime);
            newColumn("Filières", RefOtherInput::getSector);
            newColumn("Source", RefOtherInput::getSource);

            if (isExportForAdmin) {
                newColumn("Actif", RefOtherInput::isActive);
            }
        }


    }

    @Override
    public ExportResult exportAllActiveRefSubstratesToXlsx() {
        final List<RefSubstrateDto> allActiveRefSubstrates = this.getAllActiveRefSubstrates();
        ExportResult result;

        try {
            ExportModelAndRows<RefSubstrateDto> data = new ExportModelAndRows<>(new RefSubstrateModel(), allActiveRefSubstrates);

            EntityExporter exporter = new EntityExporter();
            result = exporter.exportAsXlsx("Substrats référencés dans Agrosyst", data);
        } catch (Exception e) {
            throw new AgrosystTechnicalException("Can't export", e);
        }

        return result;
    }

    public static class RefSubstrateModel extends ExportModel<RefSubstrateDto> {
        @Override
        public String getTitle() {
            return "Substrats référencés dans Agrosyst";
        }

        public RefSubstrateModel() {
            newColumn("Caractéristique 1", RefSubstrateDto::getCharacteristic1Translated);
            newColumn("Caractéristique 2", RefSubstrateDto::getCharacteristic2Translated);
            newColumn("Unité d'application", RefSubstrateDto::getUnitType);
        }
    }

    @Override
    public ReferenceDoseDTO computeReferenceDoseForIFT(
            String refInputId,
            Collection<String> refEspeceIds) {

        Preconditions.checkArgument(!refEspeceIds.isEmpty() && !StringUtils.isEmpty(refEspeceIds.iterator().next()));

        RefActaTraitementsProduit refActaTraitementsProduit = refActaTraitementsProduitDao.forEquals(TopiaEntity.PROPERTY_TOPIA_ID, refInputId).findUniqueOrNull();
        Collection<RefEspece> refEspeces = getRefEspecesForEspeceIds(refEspeceIds);

        return computeActaReferenceDoseForGivenProductIdAndActaSpecies(refActaTraitementsProduit, refEspeces).orElse(null);
    }

    @Override
    public ReferenceDoseDTO computeLocalizedReferenceDoseForIFTLegacy(
            String refInputId,
            Set<String> refEspeceIds) {

        Preconditions.checkArgument(!refEspeceIds.isEmpty() && !StringUtils.isEmpty(refEspeceIds.iterator().next()));

        RefActaTraitementsProduit refActaTraitementsProduit = refActaTraitementsProduitDao.forTopiaIdEquals(refInputId).findUniqueOrNull();
        Collection<RefEspece> refEspeces = getRefEspecesForEspeceIds(refEspeceIds);

        return computeActaReferenceDoseForGivenProductIdAndActaSpecies(refActaTraitementsProduit, refEspeces).orElse(null);
    }

    protected ReferenceDoseDTO computeReferenceDoseForIFTCible(
            RefActaTraitementsProduit refActaTraitementsProduit,
            Collection<String> refEspeceIds,
            Collection<String> bioAgressorIdentifiantOrReference_id,
            Collection<String> groupesCibles) {

        Preconditions.checkArgument(!refEspeceIds.isEmpty() && !StringUtils.isEmpty(refEspeceIds.iterator().next()));

        Collection<RefEspece> ediToActaSpeciesLink = getRefEspecesForEspeceIds(refEspeceIds);

        Optional<ReferenceDoseDTO> optionalDose = tryFindMaaDoseForProductTargetAndSpecies(
                refActaTraitementsProduit,
                ediToActaSpeciesLink,
                bioAgressorIdentifiantOrReference_id,
                groupesCibles
        );

        if (optionalDose.isEmpty()) {
            optionalDose = computeActaReferenceDoseForGivenProductIdAndActaSpecies(
                    refActaTraitementsProduit, ediToActaSpeciesLink);
        }

        return optionalDose.orElse(null);
    }

    protected Optional<ReferenceDoseDTO> tryFindMaaDoseForProductTargetAndSpecies(
            RefActaTraitementsProduit refActaTraitementsProduit,
            Collection<RefEspece> refEspeces,
            Collection<String> bioAgressorIdentifiantOrReference_id,
            Collection<String> groupesCibles) {

        final Set<String> codeCultureMaas = refEspeces.stream()
                .filter(refEspece -> CollectionUtils.isNotEmpty(refEspece.getCulturesMaa()))
                .flatMap(refEspece -> refEspece.getCulturesMaa().stream())
                .filter(Objects::nonNull)
                .filter(RefCultureMAA::isActive)
                .map(RefCultureMAA::getCode_culture_maa)
                .collect(Collectors.toSet());

        return computeMAAReferenceDoseForGivenProductIdAndMAASpecies(refActaTraitementsProduit, bioAgressorIdentifiantOrReference_id, groupesCibles, codeCultureMaas);
    }

    @Override
    public ReferenceDoseDTO computeReferenceDoseForIFTCibleNonMillesime(
            String refInputId,
            Collection<String> refEspeceIds,
            Collection<String> bioAgressorIdentifiantOrReference_id,
            Collection<String> groupesCibles) {

        if (CollectionUtils.isEmpty(refEspeceIds) || StringUtils.isEmpty(refInputId)) {
            return null;
        }

        RefActaTraitementsProduit refActaTraitementsProduit = refActaTraitementsProduitDao.forEquals(TopiaEntity.PROPERTY_TOPIA_ID, refInputId).findUniqueOrNull();

        return computeReferenceDoseForIFTCible(refActaTraitementsProduit, refEspeceIds, bioAgressorIdentifiantOrReference_id, groupesCibles);
    }

    @Override
    public ReferenceDoseDTO computeLocalizedReferenceDoseForIFCCibleNonMillesime(
            String refInputId,
            Set<String> refEspeceIds,
            Set<String> bioAgressorIdentifiantOrReference_id,
            Set<String> groupesCibles) {

        if (CollectionUtils.isEmpty(refEspeceIds) || StringUtils.isEmpty(refInputId)) {
            return null;
        }

        RefActaTraitementsProduit refActaTraitementsProduit = refActaTraitementsProduitDao.forTopiaIdEquals(refInputId).findUniqueOrNull();

        return computeReferenceDoseForIFTCible(refActaTraitementsProduit, refEspeceIds, bioAgressorIdentifiantOrReference_id, groupesCibles);
    }

    @Override
    public ReferenceDoseDTO computeReferenceDoseForIFCCibleMillesime(
            String refInputId,
            Set<String> refEspeceIds,
            Set<String> bioAgressorIdentifiantOrReference_id,
            Set<String> groupesCibles,
            Integer domainCampaign) {

        ReferenceDoseDTO referenceDoseDTO = null;

        if (CollectionUtils.isEmpty(refEspeceIds) || StringUtils.isEmpty(refInputId)) {
            return referenceDoseDTO;
        }

        RefActaTraitementsProduit refActaTraitementsProduit = refActaTraitementsProduitDao.forTopiaIdEquals(refInputId).findUniqueOrNull();

        Preconditions.checkArgument(!refEspeceIds.isEmpty() && !StringUtils.isEmpty(refEspeceIds.iterator().next()));

        Collection<RefEspece> ediToActaSpeciesLink = getRefEspecesForEspeceIds(refEspeceIds);

        final Set<String> codeCultureMaas = CollectionUtils.emptyIfNull(ediToActaSpeciesLink)
                .stream()
                .map(RefEspece::getCulturesMaa)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(RefCultureMAA::getCode_culture_maa)
                .collect(Collectors.toSet());

        String ammCode = refActaTraitementsProduit.getCode_AMM();
        String maaTreatmentCode = refActaTraitementsProduit.getCode_traitement_maa();

        List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA;
        if (CollectionUtils.isNotEmpty(codeCultureMaas) && StringUtils.isNotBlank(ammCode) && StringUtils.isNotBlank(maaTreatmentCode)) {
            doseRefsFor_CodeAMM_CultureMAA_TraitementMAA =
                    refMAADosesRefParGroupeCibleDao.findByCodeAmmCodeCulturesAndTreatmentCode(
                            ammCode,
                            codeCultureMaas,
                            maaTreatmentCode
                    );
        } else {
            doseRefsFor_CodeAMM_CultureMAA_TraitementMAA = List.of();
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Code traitement MAA non retrouvé");
            }
        }

        IndicatorVintageTargetIFT indicatorVintageTargetIFT = newInstance(IndicatorVintageTargetIFT.class);

        Optional<RefMAADosesRefParGroupeCible> optionalRefDose = indicatorVintageTargetIFT.computeVitageTargetIft(
                doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
                domainCampaign,
                bioAgressorIdentifiantOrReference_id,
                groupesCibles
        );

        if (optionalRefDose.isPresent()) {
            RefMAADosesRefParGroupeCible refDose = optionalRefDose.get();
            referenceDoseDTO = new ReferenceDoseDTO(
                    refDose.getTopiaId(),
                    refDose.getUnit_dose_ref_maa(),
                    refDose.getDose_ref_maa());
        }

        return referenceDoseDTO;
    }

    protected Collection<RefEspece> getRefEspecesForEspeceIds(Collection<String> ediSpeciesIds) {
        return refEspeceDao.forTopiaIdIn(ediSpeciesIds).findAll();
    }

    @Override
    public ReferenceDoseDTO computeReferenceDoseForGivenProductAndActaSpecies(RefActaTraitementsProduit phytoProduct,
                                                                              Collection<RefEspece> refEspeces) {
        ReferenceDoseDTO result = null;
        if (CollectionUtils.isEmpty(refEspeces)) {
            return null;
        }

        if (phytoProduct != null) {
            result = computeActaReferenceDoseForGivenProductIdAndActaSpecies(
                    phytoProduct,
                    refEspeces)
                    .orElse(null);
        }

        return result;
    }

    protected Optional<ReferenceDoseDTO> computeMAAReferenceDoseForGivenProductIdAndMAASpecies(
            RefActaTraitementsProduit traitementsProduit,
            Collection<String> bioAgressorIdentifiantOrReference_id,
            Collection<String> groupesCibles,
            Collection<String> cultureMaaCodes) {

        try {

            Function<Map<String, Boolean>, Collection<RefMAADosesRefParGroupeCible>> doseRefsSupplierByTargetId = groupesCiblesMaaCodesAndGeneric -> {

                List<RefMAADosesRefParGroupeCible> result = new ArrayList<>();

                if (traitementsProduit != null) {

                    List<RefGroupeCibleTraitement> refGroupeCibleTraitements = loadRefGroupeCibleTraitement(
                            traitementsProduit.getId_traitement(),
                            groupesCiblesMaaCodesAndGeneric
                    );
                    List<RefMAADosesRefParGroupeCible> doseRefsMatchingCodeAmmCodeTraitementCodeCultureMaa =
                            loadDosesByTargetsGroup(
                                    traitementsProduit,
                                    refGroupeCibleTraitements,
                                    cultureMaaCodes
                            );
                    result.addAll(doseRefsMatchingCodeAmmCodeTraitementCodeCultureMaa);
                }
                return result;
            };
            //Dose_Ref correspondant au quintuplet Campagne_Max|code_AMM|CultureMAA|GroupesCiblesMAA|traitementMAA
            Optional<RefMAADosesRefParGroupeCible> optionalDoseRef = computeMaaRefDose(bioAgressorIdentifiantOrReference_id, groupesCibles, doseRefsSupplierByTargetId);

            return optionalDoseRef.map(this::getReferenceDoseDTO);

        } catch (Exception eee) { // May happen if speciesId does not match any maaSpecies
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Exception during MAA reference dose computation: " + eee.getMessage());
            } else if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("Exception during MAA reference dose computation", eee);
            }
        }

        return Optional.empty();
    }

    @Override
    public Optional<RefMAADosesRefParGroupeCible> computeMaaRefDose(
            Collection<String> bioAgressorIdentifiantOrReferenceIds,
            Collection<String> groupesCibles,
            Function<Map<String, Boolean>, Collection<RefMAADosesRefParGroupeCible>> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA_SupplierByTarget) {

        Optional<RefMAADosesRefParGroupeCible> result;

        Collection<RefMAADosesRefParGroupeCible> availableDoseRefs = new HashSet<>();

        if (CollectionUtils.isNotEmpty(bioAgressorIdentifiantOrReferenceIds)) {
            for (String bioAgressorEdi : bioAgressorIdentifiantOrReferenceIds) {
                Optional<RefMAADosesRefParGroupeCible> optionalDoseRef = getRefMAADosesRefForTarget(
                        doseRefsFor_CodeAMM_CultureMAA_TraitementMAA_SupplierByTarget,
                        RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_ID,
                        bioAgressorEdi
                );
                optionalDoseRef.ifPresent(availableDoseRefs::add);
            }
        }

        if (CollectionUtils.isNotEmpty(groupesCibles)) {
            for (String groupesCible : groupesCibles) {
                Optional<RefMAADosesRefParGroupeCible> optionalDoseRef = getRefMAADosesRefForTarget(
                        doseRefsFor_CodeAMM_CultureMAA_TraitementMAA_SupplierByTarget,
                        RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CODE_GROUPE_CIBLE_MAA,
                        groupesCible
                );
                optionalDoseRef.ifPresent(availableDoseRefs::add);
            }
        }

        if (!availableDoseRefs.isEmpty()) {
            result = availableDoseRefs.stream()
                    .max(Comparator.comparing(d -> Optional.ofNullable(d.getDose_ref_maa()).orElse(-Double.MAX_VALUE)));

        } else {
            result = getRefMAADosesRefForTarget(
                    doseRefsFor_CodeAMM_CultureMAA_TraitementMAA_SupplierByTarget,
                    RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_ID,
                    null
            );
        }

        return result;
    }

    private Optional<RefMAADosesRefParGroupeCible> getRefMAADosesRefForTarget(
            Function<Map<String, Boolean>, Collection<RefMAADosesRefParGroupeCible>> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA_SupplierByTarget,
            String property,
            String bioAgressorEdiIdsOrGroupesCible) {

        Map<String, Boolean> groupesCiblesMaaCodesAndGeneric = getGroupesCiblesCodesAndGeneric(property, bioAgressorEdiIdsOrGroupesCible);
        // filtre les doses CodeAMM | CultureMAA | TraitementMAA | SupplierByTarget
        Collection<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA = doseRefsFor_CodeAMM_CultureMAA_TraitementMAA_SupplierByTarget.apply(groupesCiblesMaaCodesAndGeneric);

        if (CollectionUtils.isEmpty(doseRefsFor_CodeAMM_CultureMAA_TraitementMAA)) return Optional.empty();

        // Phyto product have there unit converted at import ImportServiceImpl.convertTo_PhytoProductUnit_HA_unit()
        int requestCampaign = doseRefsFor_CodeAMM_CultureMAA_TraitementMAA.stream()
                .map(RefMAADosesRefParGroupeCible::getCampagne)
                .filter(Objects::nonNull)
                .max(Integer::compareTo)
                .orElse(-1);

        return getRefMAADosesRefParGroupeCibleForTargetId(
                groupesCiblesMaaCodesAndGeneric,
                doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
                requestCampaign
        );
    }

    /**
     * @param maxOrDomainCampaign IFT à la cible Campagne maxi, IFT millésimé : campagne du domaine
     */
    private Optional<RefMAADosesRefParGroupeCible> getRefMAADosesRefParGroupeCibleForTargetId(
            Map<String, Boolean> groupesCiblesMaaCodesAndGeneric,
            Collection<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
            int maxOrDomainCampaign) {

        Optional<RefMAADosesRefParGroupeCible> optionalDoseRef = Optional.empty();

        boolean isTargetPresent = MapUtils.isNotEmpty(groupesCiblesMaaCodesAndGeneric);

        if (isTargetPresent) {
            // S’il y a plusieurs valeurs parce que plusieurs cibles (et donc plusieurs GroupeCibleMAA),
            // retenir la dose de référence la plus élevée comme dose de référence (Dose_REF) pour ce traitement en priorisant les non génériques
            // Campagne_Max|code_AMM|CultureMAA| Traitement_MAA dans MAA_Doses_Ref_par_GroupeCibles
            Comparator<RefMAADosesRefParGroupeCible> maxComparator =
                    Comparator.<RefMAADosesRefParGroupeCible, Boolean>comparing(
                                    dose -> groupesCiblesMaaCodesAndGeneric.get(dose.getCode_groupe_cible_maa())
                            )
                            .reversed()
                            .thenComparing(d -> Optional.ofNullable(d.getDose_ref_maa()).orElse(-Double.MAX_VALUE));

            // Campagne | CodeAMM | CultureMAA | GroupesCiblesMAA | TraitementMAA
            optionalDoseRef = doseRefsFor_CodeAMM_CultureMAA_TraitementMAA.stream()
                    .filter(dose -> dose.getCampagne().equals(maxOrDomainCampaign)
                            && groupesCiblesMaaCodesAndGeneric.containsKey(dose.getCode_groupe_cible_maa()))
                    .max(maxComparator);

            // IFT à la cible non millésimé
            // Si on ne trouve pas le quintuplet Campagne_Max|code_AMM|CultureMAA|GroupesCiblesMAA|traitementMAA
            // dans MAA_Doses_Ref_par_GroupeCibles, alors rechercher si on trouve le quadruplet
            // => code_AMM|CultureMAA|GroupesCiblesMAA|traitementMAA,
            // IFT à la cible millésimé
            // alors rechercher si on trouve le quadruplet
            //    Campagne|code_AMM|CultureMAA| Traitement_MAA|    CodeAMM | CultureMAA | GroupesCiblesMAA | TraitementMAA
            // retenir comme Dose_Ref la plus grande des valeurs trouvées en priorisant les non génériques.
            if (optionalDoseRef.isEmpty()) {
                optionalDoseRef = doseRefsFor_CodeAMM_CultureMAA_TraitementMAA.stream()
                        .filter(dose -> groupesCiblesMaaCodesAndGeneric.containsKey(dose.getCode_groupe_cible_maa()))
                        .max(maxComparator);
            }
        }

        Comparator<RefMAADosesRefParGroupeCible> minComparator =
                Comparator.comparing(d -> Optional.ofNullable(d.getDose_ref_maa()).orElse(Double.MAX_VALUE));

        if (optionalDoseRef.isEmpty()) {
            // Si on ne trouve pas le quadruplet code_AMM|CultureMAA|GroupesCiblesMAA| Traitement_MAA dans MAA_Doses_Ref_par_GroupeCibles,
            // alors rechercher si on trouve le quadruplet
            // ==> Campagne_max|code_AMM|CultureMAA| Traitement_MAA,
            // retenir comme Dose_Ref la plus petite des valeurs trouvées.
            optionalDoseRef = doseRefsFor_CodeAMM_CultureMAA_TraitementMAA.stream()
                    .filter(dose -> dose.getCampagne().equals(maxOrDomainCampaign))
                    .min(minComparator);
        }

        // Si on ne trouve pas le quadruplet Campagne_max|code_AMM|CultureMAA| Traitement_MAA dans MAA_Doses_Ref_par_GroupeCibles,
        // alors rechercher si on trouve le triplet
        // ==> code_AMM|CultureMAA| Traitement_MAA,
        // retenir comme Dose_Ref la plus petite des valeurs trouvées.
        if (optionalDoseRef.isEmpty()) {
            optionalDoseRef = doseRefsFor_CodeAMM_CultureMAA_TraitementMAA.stream().min(minComparator);
        }
        return optionalDoseRef;
    }

    private ReferenceDoseDTO getReferenceDoseDTO(RefMAADosesRefParGroupeCible refMAADosesRefParGroupeCible) {
        return new ReferenceDoseDTO(
                refMAADosesRefParGroupeCible.getTopiaId(),
                refMAADosesRefParGroupeCible.getUnit_dose_ref_maa(),
                refMAADosesRefParGroupeCible.getDose_ref_maa());
    }

    private List<RefMAADosesRefParGroupeCible> loadDosesByTargetsGroup(
            RefActaTraitementsProduit traitementsProduit,
            List<RefGroupeCibleTraitement> refGroupeCibleTraitements,
            Collection<String> cultureMaaCodes) {

        Set<String> code_traitement_maas = new HashSet<>();
        code_traitement_maas.add(traitementsProduit.getCode_traitement_maa());
        for (RefGroupeCibleTraitement refGroupeCibleTraitement : refGroupeCibleTraitements) {
            code_traitement_maas.add(refGroupeCibleTraitement.getCode_traitement_maa());
        }

        TopiaQueryBuilderAddCriteriaOrRunQueryStep<RefMAADosesRefParGroupeCible> query =
                refMAADosesRefParGroupeCibleDao.forCode_ammEquals(traitementsProduit.getCode_AMM())
                        .addIn(RefMAADosesRefParGroupeCible.PROPERTY_CODE_TRAITEMENT_MAA, code_traitement_maas)
                        .addEquals(RefMAADosesRefParGroupeCible.PROPERTY_ACTIVE, true);

        if (CollectionUtils.isNotEmpty(cultureMaaCodes)) {
            query = query.addIn(RefMAADosesRefParGroupeCible.PROPERTY_CODE_CULTURE_MAA, cultureMaaCodes);
        }
        return query.findAll();
    }

    private List<RefGroupeCibleTraitement> loadRefGroupeCibleTraitement(int idTraitement,
                                                                        Map<String, Boolean> groupesCiblesMaaCodes) {
        return refGroupeCibleTraitementDao.forId_traitementEquals(idTraitement)
                .addIn(RefGroupeCibleTraitement.PROPERTY_CODE_GROUPE_CIBLE_MAA, MapUtils.emptyIfNull(groupesCiblesMaaCodes).keySet())
                .addEquals(RefGroupeCibleTraitement.PROPERTY_ACTIVE, true)
                .findAll();
    }

    @Override
    public Map<String, Boolean> getGroupesCiblesCodesAndGeneric(String property, String bioAgressorEdiIdsOrGroupesCible) {
        Map<String, Boolean> result = new HashMap<>();

        if (StringUtils.isBlank(bioAgressorEdiIdsOrGroupesCible)) return result;

        List<RefCiblesAgrosystGroupesCiblesMAA> refCiblesAgrosystGroupesCiblesMAAs = getRefCiblesAgrosystGroupesCiblesMAAS(property, bioAgressorEdiIdsOrGroupesCible);

        if ((CollectionUtils.isEmpty(refCiblesAgrosystGroupesCiblesMAAs))) {
            result.put(bioAgressorEdiIdsOrGroupesCible, false);
        } else {
            result = refCiblesAgrosystGroupesCiblesMAAs.stream()
                    .collect(Collectors.toMap(
                            RefCiblesAgrosystGroupesCiblesMAA::getCode_groupe_cible_maa,
                            RefCiblesAgrosystGroupesCiblesMAA::isCible_generique,
                            (currentValue, newValue) -> {
                                if (currentValue != newValue) {
                                    LOGGER.warn("Erreur dans le référentiel RefCiblesAgrosystGroupesCiblesMAA, " +
                                            "des lignes de même code_groupe_cible_maa sont génériques et d'autres non");
                                }
                                return currentValue || newValue;
                            }
                    ));
        }
        return result;
    }

    @Override
    public List<RefCiblesAgrosystGroupesCiblesMAA> getRefCiblesAgrosystGroupesCiblesMAAS(String property, String bioAgressorEdiIdsOrGroupesCible) {
        if (bioAgressorEdiIdsOrGroupesCible != null) {
            return refCiblesAgrosystGroupesCiblesMAADao
                    .forActiveEquals(true)
                    .addEquals(property, bioAgressorEdiIdsOrGroupesCible)
                    .findAll();
        }
        return new ArrayList<>();
    }

    protected Optional<ReferenceDoseDTO> computeActaReferenceDoseForGivenProductIdAndActaSpecies(
            RefActaTraitementsProduit refActaTraitementsProduit,
            Collection<RefEspece> refEspeces) {

        Optional<ReferenceDoseDTO> optionalReferenceDoseDTO = Optional.empty();

        if (CollectionUtils.isEmpty(refEspeces) || refActaTraitementsProduit == null) {
            return optionalReferenceDoseDTO;
        }

        try {

            // do not load RefActaDosageSPC for couple phyto product x Zones cultivées #5566
            final int actaDosageSpcCroppingZonesGroupId = getConfig().getActaDosageSpcCroppingZonesGroupId();

            Set<Integer> actaSpeciesIds = new HashSet<>();

            for (RefEspece refEspece : refEspeces) {

                int idCulture = refEspece.getId_culture_acta();

                if (actaDosageSpcCroppingZonesGroupId != idCulture) {
                    actaSpeciesIds.add(idCulture);
                }

                // Get all related cultures/groups and add to the set
                final List<Integer> gcActaSpeciesIds = refActaGroupeCulturesDao.findGroupCultureActaSpeciesIds(
                        idCulture,
                        actaDosageSpcCroppingZonesGroupId);

                actaSpeciesIds.addAll(gcActaSpeciesIds);
            }

            // Ask Dao for the minimal value over all the expected acta species
            Optional<RefActaDosageSPC> actaRefDoseForSpecies = refActaDosageSPCDao.findMinimalValueBySpeciesIds(
                    refActaTraitementsProduit.getId_produit(),
                    refActaTraitementsProduit.getId_traitement(),
                    actaSpeciesIds);

            // if result is null we look for value for couple phyto product x Zones cultivées #5566
            if (actaRefDoseForSpecies.isEmpty()) {

                actaRefDoseForSpecies = refActaDosageSPCDao.findMinimalValueByGroupId(
                        refActaTraitementsProduit.getId_produit(),
                        refActaTraitementsProduit.getId_traitement(),
                        actaDosageSpcCroppingZonesGroupId);
            }

            if (actaRefDoseForSpecies.isEmpty() && StringUtils.isNotBlank(refActaTraitementsProduit.getCode_AMM())) {

                actaRefDoseForSpecies = refActaDosageSPCDao.findMinimalValueByAMM(
                        refActaTraitementsProduit.getCode_AMM(),
                        refActaTraitementsProduit.getId_traitement(),
                        actaSpeciesIds);

            }

            Optional<ReferenceDoseDTO> actaOptionalReferenceDoseDTO;
            if (actaRefDoseForSpecies.isPresent()) {
                RefActaDosageSPC referenceDose = actaRefDoseForSpecies.get();
                actaOptionalReferenceDoseDTO = Optional.of(
                        new ReferenceDoseDTO(
                                referenceDose.getTopiaId(),
                                referenceDose.getDosage_spc_unite(),
                                referenceDose.getDosage_spc_valeur()));
            } else {
                actaOptionalReferenceDoseDTO = Optional.empty();
            }

            Optional<ReferenceDoseDTO> maaOptionalReferenceDoseDTO;
            if (actaOptionalReferenceDoseDTO.isEmpty() ||
                    actaOptionalReferenceDoseDTO.get().getValue() == null) {
                // Si on ne trouve pas le triplet code_amm | id_traitement | (id_culture + id_culture des groupes culture) dans ACTA_Dosage_SPC_Complet,
                // alors rechercher dans MAA_Doses_Ref_par_GroupeCibles selon le triplet code_AMM | CultureMAA | Traitement_MAA, et retenir comme Dose_Ref la plus petite des valeurs trouvées
                final Set<String> codeCultureMaas = refEspeces.stream()
                        .filter(refEspece -> CollectionUtils.isNotEmpty(refEspece.getCulturesMaa()))
                        .flatMap(refEspece -> refEspece.getCulturesMaa().stream())
                        .filter(Objects::nonNull)
                        .filter(RefCultureMAA::isActive)
                        .map(RefCultureMAA::getCode_culture_maa)
                        .collect(Collectors.toSet());

                List<RefMAADosesRefParGroupeCible> dosesRefParGroupeCibles =
                        refMAADosesRefParGroupeCibleDao.forProperties(
                                        RefMAADosesRefParGroupeCible.PROPERTY_CODE_AMM, refActaTraitementsProduit.getCode_AMM(),
                                        RefMAADosesRefParGroupeCible.PROPERTY_CODE_TRAITEMENT_MAA, refActaTraitementsProduit.getCode_traitement_maa(),
                                        RefMAADosesRefParGroupeCible.PROPERTY_ACTIVE, true
                                ).addIn(RefMAADosesRefParGroupeCible.PROPERTY_CODE_CULTURE_MAA, codeCultureMaas)
                                .addNotNull(RefMAADosesRefParGroupeCible.PROPERTY_UNIT_DOSE_REF_MAA)
                                .findAll();

                Optional<RefMAADosesRefParGroupeCible> result0 = dosesRefParGroupeCibles.stream()
                        .min(Comparator.<RefMAADosesRefParGroupeCible>comparingDouble(
                                        dose -> REFERENCE_DOSE_UNIT_ORDER.indexOf(dose.getUnit_dose_ref_maa()))
                                .thenComparingDouble(
                                        rad -> ObjectUtils.firstNonNull(rad.getDose_ref_maa(), Double.MAX_VALUE)));

                maaOptionalReferenceDoseDTO = result0.map(this::getReferenceDoseDTO);
            } else {
                maaOptionalReferenceDoseDTO = Optional.empty();
            }

            // On a le maaOptionalReferenceDoseDTO qui si l'on a pas un actaOptionalReferenceDoseDTO plus complet (valeur + unité)
            if (maaOptionalReferenceDoseDTO.isPresent()) {
                optionalReferenceDoseDTO = maaOptionalReferenceDoseDTO;
            } else {
                optionalReferenceDoseDTO = actaOptionalReferenceDoseDTO;
            }

        } catch (Exception eee) { // May happen if getActaSpeciesId does not match any actaSpecies
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Exception during reference dose computation", eee);
            }
        }
        return optionalReferenceDoseDTO;
    }

    @Override
    public ExportResult exportPhytoAndLutteBioActaTraitementProduitsXlsx() {
        ExportResult result;
        try {
            Map<AgrosystInterventionType, String> translatedInterventionTypes = i18nService.getEnumTranslationMap(AgrosystInterventionType.class);
            Map<Integer, String> actionTypeByTraitementId = refActaTraitementsProduitsCategDao
                    .forActiveEquals(true)
                    .addIn(RefActaTraitementsProduitsCateg.PROPERTY_ACTION,
                            Arrays.asList(AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, AgrosystInterventionType.LUTTE_BIOLOGIQUE))
                    .findAll().stream()
                    .collect(Collectors.toMap(
                            RefActaTraitementsProduitsCateg::getId_traitement,
                            categ -> translatedInterventionTypes.getOrDefault(categ.getAction(), categ.getAction().toString())
                    ));

            Set<Integer> traitementIds = actionTypeByTraitementId.keySet();
            Language language = getSecurityContext().getLanguage();
            List<RefActaTraitementsProduit> entities = refActaTraitementsProduitDao.findAllForTreatmentIds(traitementIds, language);

            ExportModelAndRows<RefActaTraitementsProduit> domainBeanExportModelAndRows = new ExportModelAndRows<>(new RefActaTraitementsProduitModel(actionTypeByTraitementId), entities);

            EntityExporter exporter = new EntityExporter();
            result = exporter.exportAsXlsx("Agrosyst Acta Traitement Produits", domainBeanExportModelAndRows);

        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't export", ex);
        }
        return result;
    }

    public static class RefActaTraitementsProduitModel extends ExportModel<RefActaTraitementsProduit> {

        @Override
        public String getTitle() {
            return "ACTA Traitements Produits";
        }

        public RefActaTraitementsProduitModel(Map<Integer, String> actionTypeByTraitementId) {
            super();
            newColumn(I18n.t("InputProductsModel.countryTrigram"), r -> r.getRefCountry().getTrigram());
            newColumn(I18n.t("InputProductsModel.actionType"), r -> actionTypeByTraitementId.get(r.getId_traitement()));
            newColumn(I18n.t("InputProductsModel.nom_traitement"), RefActaTraitementsProduit::getNom_traitement);
            newColumn(I18n.t("InputProductsModel.nom_produit"), RefActaTraitementsProduit::getNom_produit);
            newColumn(I18n.t("InputProductsModel.code_AMM"), RefActaTraitementsProduit::getCode_AMM);
            newColumn(I18n.t("InputProductsModel.nodu"), RefActaTraitementsProduit::isNodu);
            newColumn(I18n.t("InputProductsModel.etat_usage"), RefActaTraitementsProduit::getEtat_usage);
            newColumn(I18n.t("InputProductsModel.date_retrait_produit"), RefActaTraitementsProduit::getDate_retrait_produit);
            newColumn(I18n.t("InputProductsModel.date_autorisation_produit"), RefActaTraitementsProduit::getDate_autorisation_produit);
        }
    }

    @Override
    public InputStream exportCiblesGroupesCiblesMaaCSV() {
        InputStream result;
        try {
            List<RefCiblesAgrosystGroupesCiblesMAA> entities = refCiblesAgrosystGroupesCiblesMAADao
                    .forActiveEquals(true)
                    .setOrderByArguments(
                            RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_REFERENCE_PARAM,
                            RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_GROUPE_CIBLE_MAA,
                            RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_GENERIQUE,
                            RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_LABEL
                    )
                    .findAll();

            File tempFile = File.createTempFile("export-all-cibles-groupes-cibles-mass-export-", ".csv");
            tempFile.deleteOnExit();

            Export.exportToFile(
                    new RefCiblesAgrosystGroupesCiblesMAAModel(false),
                    entities,
                    tempFile,
                    StandardCharsets.UTF_8);

            result = new FileInputStream(tempFile);

        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't export", ex);
        }
        return result;
    }

    @Override
    public Collection<String> getCodeAmmBioControle(int campagne) {
        return refMAABiocontroleDao.forActiveEquals(true)
                .addEquals(RefMAABiocontrole.PROPERTY_BIOCONTROL, true)
                .addEquals(RefMAABiocontrole.PROPERTY_CAMPAGNE, campagne)
                .stream()
                .map(RefMAABiocontrole::getCode_amm)
                .collect(Collectors.toSet());
    }

    @Override
    public Collection<String> getCodeAmmBioControle(String campagnes) {
        Pair<Integer, Integer> campaignRange = CommonService.GET_CAMPAINGS_RANGE.apply(campagnes);
        return refMAABiocontroleDao.getActiveRefMaaBioControle(campaignRange.getLeft(), campaignRange.getRight());
    }

    @Override
    public List<GroupeCibleDTO> getGroupesCibles() {
        Language language = getSecurityContext().getLanguage();
        Callable<ArrayList<GroupeCibleDTO>> loader = () -> {
            List<RefCiblesAgrosystGroupesCiblesMAA> allActiveGroupesCibles = refCiblesAgrosystGroupesCiblesMAADao.findAllActive(language);
            Map<String, GroupeCibleDTO> result = new HashMap<>();
            for (RefCiblesAgrosystGroupesCiblesMAA activeGroupeCible : allActiveGroupesCibles) {
                GroupeCibleDTO groupeCibleDTO = result.computeIfAbsent(activeGroupeCible.getCode_groupe_cible_maa(),
                        code_groupe_cible_maa -> new GroupeCibleDTO(
                                code_groupe_cible_maa,
                                activeGroupeCible.getGroupe_cible_maa_Translated(),
                                activeGroupeCible.getReference_param().name()
                        )
                );
                if (!Objects.equals(groupeCibleDTO.getGroupeCibleMaa(), activeGroupeCible.getGroupe_cible_maa())) {
                    LOGGER.warn("[RefCiblesAgrosystGroupesCiblesMAA] Plusieurs libellés pour le meme code " + activeGroupeCible.getCode_groupe_cible_maa() +
                            " : '" + groupeCibleDTO.getGroupeCibleMaa() + "' et '" + activeGroupeCible.getGroupe_cible_maa() + "'");
                }
                groupeCibleDTO.addCibleEdiRefCode(activeGroupeCible.getCible_edi_ref_code());
            }
            return result.values().stream()
                    .sorted(Comparator.comparing(GroupeCibleDTO::getGroupeCibleMaa))
                    .collect(Collectors.toCollection(ArrayList::new));
        };
        return cacheService.get(CacheDiscriminator.activeGroupesCibles(language), loader);
    }

    @Override
    public Map<String, String> getGroupesCiblesParCode() {
        return getGroupesCibles().stream()
                .collect(Collectors.toMap(GroupeCibleDTO::getCodeGroupeCibleMaa, GroupeCibleDTO::getGroupeCibleMaa));
    }

    @Override
    public Map<String, BioAgressorType> getGroupesCiblesReferenceParamParCode() {
        return getGroupesCibles().stream()
                .collect(Collectors.toMap(GroupeCibleDTO::getCodeGroupeCibleMaa, groupeCible -> BioAgressorType.valueOf(groupeCible.getReferenceParam())));
    }

    @Override
    public RefPot loadRefPot(String refPotId) {
        return refPotDao.forTopiaIdEquals(refPotId).findUnique();
    }

    @Override
    public DomainReferentialInputs getDomainReferentialInputs(Collection<DomainInputDto> domainInputDtos_) {
        // we create a new list as we want to modify it's content
        Collection<DomainInputDto> domainInputDtos = new ArrayList<>(domainInputDtos_);
        List<String> refFertiOrgaIds = new ArrayList<>();
        List<String> refActaTraitementsProduitIds = new ArrayList<>();
        List<String> refPotIds = new ArrayList<>();
        List<String> refSubstrateIds = new ArrayList<>();
        List<String> refOtherInputIds = new ArrayList<>();

        List<DomainMineralProductInputDto> domainMineralProductInputDtos = new ArrayList<>();

        domainInputDtos.forEach(inputDto -> {
            InputType inputType = inputDto.getInputType();
            switch (inputType) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                    DomainMineralProductInputDto domainMineralProductInput = (DomainMineralProductInputDto) inputDto;
                    domainMineralProductInputDtos.add(domainMineralProductInput);
                }
                case EPANDAGES_ORGANIQUES -> {
                    DomainOrganicProductInputDto domainOrganicProductInput = (DomainOrganicProductInputDto) inputDto;
                    refFertiOrgaIds.add(domainOrganicProductInput.getRefInputId());
                }
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, LUTTE_BIOLOGIQUE, TRAITEMENT_SEMENCE -> {
                    DomainPhytoProductInputDto domainPhytoProductInputDto = (DomainPhytoProductInputDto) inputDto;
                    refActaTraitementsProduitIds.add(domainPhytoProductInputDto.getRefInputId());
                }
                case CARBURANT, IRRIGATION, MAIN_OEUVRE_MANUELLE, MAIN_OEUVRE_TRACTORISTE -> {
                }
                case SEMIS, PLAN_COMPAGNE -> {
                    DomainSeedLotInputDto domainSeedLotInputDto = (DomainSeedLotInputDto) inputDto;
                    List<String> phytoIds = CollectionUtils.emptyIfNull(domainSeedLotInputDto.getSpeciesInputs()).stream()
                            .map(DomainSeedSpeciesInputDto::getSpeciesPhytoInputDtos)
                            .filter(Objects::nonNull)
                            .flatMap(Collection::stream)
                            .map(DomainPhytoProductInputDto::getRefInputId)
                            .toList();
                    refActaTraitementsProduitIds.addAll(phytoIds);
                }
                case AUTRE -> {
                    DomainOtherProductInputDto domainOtherProductInputDto = (DomainOtherProductInputDto) inputDto;
                    refOtherInputIds.add(domainOtherProductInputDto.getRefInputId());
                }
                case SUBSTRAT -> {
                    DomainSubstrateInputDto domainSubstrateInputDto = (DomainSubstrateInputDto) inputDto;
                    refSubstrateIds.add(domainSubstrateInputDto.getRefInputId());
                }
                case POT -> {
                    DomainPotInputDto domainPotInputDto = (DomainPotInputDto) inputDto;
                    refPotIds.add(domainPotInputDto.getRefInputId());
                }
                default -> throw new IllegalStateException("Unexpected value: " + inputType);
            }
        });

        Map<String, RefFertiMinUNIFA> refFertiMinUnifaByIds = new HashMap<>();
        List<DomainInputDto> domainMineralProductInputWithRefFertiMinUnifaTopiaId = Lists.newArrayListWithCapacity(domainMineralProductInputDtos.size());
        domainMineralProductInputDtos.forEach(
                di -> {
                    DomainMineralProductInputDto domainMineralProductInputDto = di;
                    RefFertiMinUNIFA product = createOrUpdateRefMineralProductInput(domainMineralProductInputDto);
                    refFertiMinUnifaByIds.put(product.getTopiaId(), product);
                    DomainMineralProductInputDto newDto = domainMineralProductInputDto.toBuilder().refInputId(product.getTopiaId()).build();
                    domainMineralProductInputWithRefFertiMinUnifaTopiaId.add(newDto);
                }
        );
        domainInputDtos.removeAll(domainMineralProductInputDtos);
        domainInputDtos.addAll(domainMineralProductInputWithRefFertiMinUnifaTopiaId);

        DomainReferentialInputs domainReferentialInputs = new DomainReferentialInputs(
                domainInputDtos,
                Maps.uniqueIndex(refFertiOrgaDao.forTopiaIdIn(refFertiOrgaIds).findAll(), TopiaEntity::getTopiaId),
                Maps.uniqueIndex(refActaTraitementsProduitDao.forTopiaIdIn(refActaTraitementsProduitIds).findAll(), TopiaEntity::getTopiaId),
                Maps.uniqueIndex(refPotDao.forTopiaIdIn(refPotIds).findAll(), TopiaEntity::getTopiaId),
                Maps.uniqueIndex(refSubstrateDao.forTopiaIdIn(refSubstrateIds).findAll(), TopiaEntity::getTopiaId),
                ImmutableMap.copyOf(refFertiMinUnifaByIds),
                Maps.uniqueIndex(refOtherInputDao.forTopiaIdIn(refOtherInputIds).findAll(), TopiaEntity::getTopiaId)
        );

        return domainReferentialInputs;
    }

    @Override
    public Collection<RefDestination> loadDestinationsForIds(Set<String> destinationIds) {
        return refDestinationDao.forTopiaIdIn(destinationIds).findAll();
    }

    @Override
    public Collection<RefQualityCriteria> loadRefQualityCriteriaForIds(Set<String> qualityCriteriaRefIds) {
        return refQualityCriteriaDao.forTopiaIdIn(qualityCriteriaRefIds).findAll();
    }

    @Override
    public Collection<RefQualityCriteriaClass> loadRefQualityCriteriaClassForIds(Set<String> qualityCriteriaRefQualityCriteriaClassIds) {
        return refQualityCriteriaClassDao.forTopiaIdIn(qualityCriteriaRefQualityCriteriaClassIds).findAll();
    }

    @Override
    public Collection<RefInputUnitPriceUnitConverter> loadAllRefInputUnitPriceUnitConverter() {
        return refInputUnitPriceUnitConverterDao.findAll();
    }

    @Override
    public RefStationMeteo findRefStationMeteoByTopiaId(String topiaId) {
        return refStationMeteoDao.forTopiaIdEquals(topiaId).findUnique();
    }

    @Override
    public DomainPhytoProductInputSearchResults getDomainPhytoProductInputSearchResults(PhytoProductInputFilter phytoProductInputFilter) {

        Language language = getSecurityContext().getLanguage();
        AgrosystInterventionType action = phytoProductInputFilter.getAction();

        // S'il n'y a pas de filtre permettant de retourner un nom limité de donné
        // le code suivant est bien plus performant pour retourner les données souhaitées.
        if (StringUtils.isBlank(phytoProductInputFilter.getNomProduit()) &&
                phytoProductInputFilter.getTypeProduit() == null &&
                StringUtils.isBlank(phytoProductInputFilter.getCodeAMM()) &&
                StringUtils.isBlank(phytoProductInputFilter.getActiveSubstancesTerm()) &&
                StringUtils.isBlank(phytoProductInputFilter.getActiveSubstance())
        ) {

            Callable<DomainPhytoProductInputSearchResults> loader = () -> getDomainPhytoProductInputSearchResultForAction(language, action, phytoProductInputFilter.isIpmworks());

            DomainPhytoProductInputSearchResults result = cacheService.get(CacheDiscriminator.activeDomainPhytoProductInputSearchResults(), loader);

            return result;
        }

        List<DomainPhytoProductInputSearchResult> resulPhytoProductNameAndAmm = refActaTraitementsProduitDao.findDomainPhytoProductInputSearchResults(language, phytoProductInputFilter);

        Set<PhytoProductUnit> phytoProductUnits = new HashSet<>();

        String refActaTraitementsProduitId = null;
        String idProduit = null;
        Integer idTraitement = null;


        final Set<String> productNames = new HashSet<>();
        final Set<String> code_AMMs = new HashSet<>();
        final Set<ProductType> productTypes = new HashSet<>();
        final Set<String> substances = new HashSet<>();

        resulPhytoProductNameAndAmm.stream()
                .filter(Objects::nonNull)
                .forEach(
                        ratp -> {
                            if (StringUtils.isNotEmpty(ratp.nomProduit())) productNames.add(ratp.nomProduit());
                            if (StringUtils.isNotEmpty(ratp.codeAmm())) code_AMMs.add(ratp.codeAmm());
                            if (ratp.productType() != null) productTypes.add(ratp.productType());
                            if (StringUtils.isNotEmpty(ratp.activeSubstanceNomCommun()))
                                substances.add(ratp.activeSubstanceNomCommun());
                        }
                );

        if (StringUtils.isEmpty(phytoProductInputFilter.getActiveSubstance())
                && StringUtils.isEmpty(phytoProductInputFilter.getActiveSubstancesTerm())) {

            substances.addAll(refCompositionSubstancesActivesParNumeroAMMTopiaDao.findDistinctSubtanceActiveNamesForProducts(language, code_AMMs));
        }

        Collection<String> refEspeceIds = phytoProductInputFilter.getRefEspeceIds();

        String key = null;
        if (code_AMMs.size() == 1) {

            final List<DomainPhytoProductInputDaoSearchResult> allPhytoProductsMatchingFilters =
                    refActaTraitementsProduitDao.findAllByPhytoProductInputFilter(phytoProductInputFilter);

            if (CollectionUtils.isNotEmpty(allPhytoProductsMatchingFilters)) {
                final DomainPhytoProductInputDaoSearchResult refActaCategAndProdust = allPhytoProductsMatchingFilters.getFirst();
                final RefActaTraitementsProduit refActaTraitementsProduit = refActaCategAndProdust.product();
                refActaTraitementsProduitId = refActaTraitementsProduit.getTopiaId();
                idProduit = refActaTraitementsProduit.getId_produit();
                idTraitement = refActaTraitementsProduit.getId_traitement();
                key = DomainInputStockUnitService.getPhytoInputKey(refActaTraitementsProduit, PhytoProductUnit.KG_HA);// default unit usage, will be changed at save time
                phytoProductUnits = new HashSet<>();
                if (CollectionUtils.isNotEmpty(refEspeceIds)) {
                    Collection<RefEspece> refEspeces = refEspeceDao.forTopiaIdIn(refEspeceIds).findAll();
                    Optional<ReferenceDoseDTO> rd = computeActaReferenceDoseForGivenProductIdAndActaSpecies(refActaTraitementsProduit, refEspeces);
                    if (rd.isPresent() && rd.get().getUnit() != null) {
                        phytoProductUnits.add(rd.get().getUnit());
                    }
                } else {
                    List<PhytoProductUnit> units = refActaDosageSPCDao.findPhytoProductUnitsForProduct(refActaTraitementsProduit);
                    // Si on ne trouve pas d'unité pour le produit, on tente la recherche avec le code AMM
                    if (CollectionUtils.isEmpty(units)) {
                        units = refActaDosageSPCDao.findPhytoProductUnitsForCodeAmm(refActaTraitementsProduit);
                    }
                    phytoProductUnits.addAll(units);
                    phytoProductUnits.addAll(refMAADosesRefParGroupeCibleDao.findPhytoProductUnitsForProduct(refActaTraitementsProduit));
                }
                // we look for usage units
                if (AgrosystInterventionType.SEMIS == phytoProductInputFilter.getAction() && phytoProductUnits.isEmpty()) {
                    phytoProductUnits.addAll(SEEDING_DEFAULT_PHYTO_PRODUCT_UNITS);
                }
            }
        } else if (!resulPhytoProductNameAndAmm.isEmpty()) {
            phytoProductUnits.addAll(refActaTraitementsProduitDao.findMAADosesPhytoProductUnitsForProducts(language, phytoProductInputFilter));
            phytoProductUnits.addAll(refActaTraitementsProduitDao.findRefActaDosageSpcPhytoProductUnitsForProducts(language, phytoProductInputFilter));
        }

        DomainPhytoProductInputSearchResults result = new DomainPhytoProductInputSearchResults(
                refActaTraitementsProduitId,
                idProduit,
                idTraitement,
                key,
                phytoProductUnits,
                productTypes.stream().sorted().collect(Collectors.toList()),
                productNames.stream().sorted().collect(Collectors.toList()),
                code_AMMs.stream().sorted().collect(Collectors.toList()),
                substances.stream().toList()
        );

        return result;
    }

    private @NotNull DomainPhytoProductInputSearchResults getDomainPhytoProductInputSearchResultForAction(
            Language language,
            AgrosystInterventionType action,
            boolean ipmworks) {

        List<Pair<String, String>> phytoNamesAndAmms = refActaTraitementsProduitDao.findResulPhytoProductNameAndAmm(language, action);

        Set<ProductType> productTypes = refActaTraitementsProduitsCategDao.forActiveEquals(true)
                .addEquals(RefActaTraitementsProduitsCateg.PROPERTY_ACTION, action)
                .stream()
                .distinct()
                .map(RefActaTraitementsProduitsCateg::getType_produit)
                .collect(Collectors.toSet());

        Set<PhytoProductUnit> doseUnits = new LinkedHashSet<>(refActaDosageSPCDao.findRefActaDosageSpcDosageUnits(false, action));

        List<String> substanceActiveNames = refCompositionSubstancesActivesParNumeroAMMTopiaDao.findActiveSubstanceNamesForInterventionType(action, ipmworks);

        DomainPhytoProductInputSearchResults result = new DomainPhytoProductInputSearchResults(
                null,
                null,
                null,
                null,
                doseUnits,
                productTypes.stream().sorted().collect(Collectors.toList()),
                phytoNamesAndAmms.stream().map(Pair::getKey).distinct().collect(Collectors.toList()),
                phytoNamesAndAmms.stream().map(Pair::getValue).distinct().collect(Collectors.toList()),
                substanceActiveNames);
        return result;
    }

    @Override
    public DomainPhytoProductInputSearchResults getDomainBiologicalControlInputsSearchResults(PhytoProductInputFilter phytoProductInputFilter) {

        Language language = getSecurityContext().getLanguage();

        // pour les Produits sans AMM et macroorganismes ce sont les francais qu'on prend et que l'on traduit
        RefCountry france = refCountryTopiaDao.forTrigramEquals(Locale.FRANCE.getISO3Country().toLowerCase()).findUnique();
        phytoProductInputFilter = phytoProductInputFilter.toBuilder()
                .countryTopiaId(france.getTopiaId())
                .codeAMM(null)
                .build();

        final List<DomainPhytoProductInputDaoSearchResult> allPhytoProductsMatchingFilters =
                refActaTraitementsProduitDao.findAllBiologicalControlInputs(language, phytoProductInputFilter);

        Set<PhytoProductUnit> phytoProductUnits = null;

        String refActaTraitementsProduitId = null;
        String idProduit = null;
        Integer idTraitement = null;
        String key = null;

        Set<String> productNames = new HashSet<>();

        allPhytoProductsMatchingFilters.stream()
                .map(DomainPhytoProductInputDaoSearchResult::product)
                .filter(Objects::nonNull)
                .forEach(ratp -> productNames.add(ratp.getNom_produit()));

        Set<ProductType> productTypes = allPhytoProductsMatchingFilters.stream()
                .map(DomainPhytoProductInputDaoSearchResult::categ)
                .filter(Objects::nonNull)
                .map(RefActaTraitementsProduitsCateg::getType_produit)
                .collect(Collectors.toSet());

        if (allPhytoProductsMatchingFilters.size() == 1) {
            // we look for usage units
            final DomainPhytoProductInputDaoSearchResult refActaCategAndProdust = allPhytoProductsMatchingFilters.getFirst();
            final RefActaTraitementsProduit refActaTraitementsProduit = refActaCategAndProdust.product();
            refActaTraitementsProduitId = refActaTraitementsProduit.getTopiaId();
            idProduit = refActaTraitementsProduit.getId_produit();
            idTraitement = refActaTraitementsProduit.getId_traitement();
            phytoProductUnits = new HashSet<>();
            phytoProductUnits.addAll(refActaDosageSPCDao.findPhytoProductUnitsForProduct(refActaTraitementsProduit));
            phytoProductUnits.addAll(refMAADosesRefParGroupeCibleDao.findPhytoProductUnitsForProduct(refActaTraitementsProduit));
            key = DomainInputStockUnitService.getPhytoInputKey(refActaTraitementsProduit, PhytoProductUnit.KG_HA);// default unit usage, will be changed at save time
        }

        DomainPhytoProductInputSearchResults result = new DomainPhytoProductInputSearchResults(
                refActaTraitementsProduitId,
                idProduit,
                idTraitement,
                key,
                phytoProductUnits,
                productTypes.stream().sorted().collect(Collectors.toList()),
                productNames.stream().sorted().collect(Collectors.toList()),
                null,
                null
        );

        return result;
    }

    @Override
    public RefInterventionAgrosystTravailEDI getRefInterventionAgrosystTravailEDI(String topiaId) {
        return refInterventionAgrosystTravailEDIDao.forTopiaIdEquals(topiaId).findUnique();
    }

    @Override
    public RefDestination getDestination(String topiaId) {
        return refDestinationDao.forTopiaIdEquals(topiaId).findUnique();
    }

    @Override
    public RefQualityCriteria loadQualityCriteriaForId(String refQualityCriteriaId) {
        return refQualityCriteriaDao.forTopiaIdEquals(refQualityCriteriaId).findUnique();
    }

    @Override
    public RefDestination loadDestinationForId(String destinationId) {
        return refDestinationDao.forTopiaIdEquals(destinationId).findUnique();
    }


    @Override
    public Map<BioAgressorType, String> getTranslatedBioAgressorType() {
        return i18nService.getEnumTranslationMap(BioAgressorType.class);
    }

    @Override
    public Map<AgrosystInterventionType, String> getAgrosystInterventionTypeTranslationMap() {
        return i18nService.getEnumTranslationMap(AgrosystInterventionType.class);
    }

    public <E extends ReferentialEntity> List<E> getAllActiveReferentialEntities(Class<E> clazz) {
        // get lazy iterator of entities to export
        final TopiaDao<E> classDao = context.getDaoSupplier().getDao(clazz);
        return classDao.forEquals("active", true).findAll();
    }

    /**
     * used RefCompositionSubstancesActivesParNumeroAMM instead of RefActaSubstanceActive
     */
    @Deprecated
    @Override
    public InputStream matchMaaWithFrenchMaa(InputStream contentStream) {
        ImportModel<EuropeanProductDto> csvModel = new EuropeanProductModel();

        int line = 2;
        Set<String> alreadyDone = new HashSet<>();
        List<EuropeanProductDto> outputContent = new ArrayList<>();

        try (Import<EuropeanProductDto> importer = Import.newImport(csvModel, contentStream)) {
            for (EuropeanProductDto productDtoRow : importer) {
                EuropeanProductDto result = new EuropeanProductDto();
                Binder<EuropeanProductDto, EuropeanProductDto> binder = BinderFactory.newBinder(EuropeanProductDto.class);
                binder.copy(productDtoRow, result);
                outputContent.add(result);

                String ammUe = productDtoRow.getAmm_ue();
                if (alreadyDone.add(ammUe)) {
                    List<String> substances = Stream.of(
                                    productDtoRow.getSa_1(),
                                    productDtoRow.getSa_2(),
                                    productDtoRow.getSa_3(),
                                    productDtoRow.getSa_4(),
                                    productDtoRow.getSa_5()
                            ).filter(Objects::nonNull)
                            .toList();

                    List<RefActaSubstanceActive> substancesActives = refActaSubstanceActiveDao.forActiveEquals(true)
                            .addIn(RefActaSubstanceActive.PROPERTY_NOM_COMMUN_SA, substances)
                            .findAll();

                    MultiValuedMap<String, RefActaSubstanceActive> substancesActivesByAmm = new HashSetValuedHashMap<>();
                    substancesActives.forEach(substanceActive -> substancesActivesByAmm.put(substanceActive.getCode_AMM(), substanceActive));

                    Comparator<String> comparator = Comparator.comparingInt(key -> substancesActivesByAmm.get(key).size());
                    List<String> sortedAmms = substancesActivesByAmm.keySet().stream()
                            .sorted(comparator.reversed())
                            .toList();

                    int index = 0;
                    Map<String, Integer> matchesByAmm = new HashMap<>();

                    while (index < sortedAmms.size() && result.getAmm_sa() == null && result.getAmm_probable() == null) {
                        String amm = sortedAmms.get(index);
                        Collection<RefActaSubstanceActive> refActaSubstanceActives = substancesActivesByAmm.get(amm);
                        for (RefActaSubstanceActive substanceActive : refActaSubstanceActives) {
                            if (namesEqual(substanceActive.getNom_produit(), productDtoRow.getNom_produit())) {
                                result.setAmm_sa(amm);
                                result.setRef_data(computeRefData(substancesActivesByAmm, amm));
                                break;
                            }

                            double saConcentration = substanceActive.getConcentration_valeur();
                            String saUnit = substanceActive.getConcentration_unite();

                            if (namesEqual(substanceActive.getNom_commun_sa(), productDtoRow.getSa_1())) {
                                if (productDtoRow.getDose_1() != null) {
                                    checkIfSubstanceOfProduct(matchesByAmm, amm, saConcentration, saUnit, productDtoRow.getDose_1(), productDtoRow.getUnite_dose_1());
                                }
                            } else if (namesEqual(substanceActive.getNom_commun_sa(), productDtoRow.getSa_2())) {
                                if (productDtoRow.getDose_2() != null) {
                                    checkIfSubstanceOfProduct(matchesByAmm, amm, saConcentration, saUnit, productDtoRow.getDose_2(), productDtoRow.getUnite_dose_2());
                                }
                            } else if (namesEqual(substanceActive.getNom_commun_sa(), productDtoRow.getSa_3())) {
                                if (productDtoRow.getDose_3() != null) {
                                    checkIfSubstanceOfProduct(matchesByAmm, amm, saConcentration, saUnit, productDtoRow.getDose_3(), productDtoRow.getUnite_dose_3());
                                }
                            } else if (namesEqual(substanceActive.getNom_commun_sa(), productDtoRow.getSa_4())) {
                                if (productDtoRow.getDose_4() != null) {
                                    checkIfSubstanceOfProduct(matchesByAmm, amm, saConcentration, saUnit, productDtoRow.getDose_4(), productDtoRow.getUnite_dose_4());
                                }
                            } else if (namesEqual(substanceActive.getNom_commun_sa(), productDtoRow.getSa_5())) {
                                if (productDtoRow.getDose_5() != null) {
                                    checkIfSubstanceOfProduct(matchesByAmm, amm, saConcentration, saUnit, productDtoRow.getDose_5(), productDtoRow.getUnite_dose_4());
                                }
                            }
                        }
                        index++;
                    }

                    String nomProduit = productDtoRow.getNom_produit();
                    Set<String> ammPossibles = new LinkedHashSet<>();
                    List<RefActaSubstanceActive> ammForProductNameEquals = refActaSubstanceActiveDao.getAmmForProductNameEquals(nomProduit);
                    if (CollectionUtils.isNotEmpty(ammForProductNameEquals)) {
                        result.setAmm_nom_produit(ammForProductNameEquals.getFirst().getCode_AMM());
                        ammForProductNameEquals.forEach(sa -> {
                            substancesActivesByAmm.put(sa.getCode_AMM(), sa);
                            ammPossibles.add(sa.getCode_AMM());
                        });

                    } else {
                        List<RefActaSubstanceActive> ammForProductNameLike = refActaSubstanceActiveDao.getAmmForProductNameLike(nomProduit);
                        if (CollectionUtils.isNotEmpty(ammForProductNameLike)) {
                            ammForProductNameLike.forEach(sa -> {
                                String codeAmm = sa.getCode_AMM();
                                if (!codeAmm.equals(productDtoRow.getAmm_sa())) {
                                    substancesActivesByAmm.put(codeAmm, sa);
                                    ammPossibles.add(codeAmm);
                                }
                            });

                        } else if (nomProduit.contains(" ")) {
                            String shorterNomProduit = nomProduit;
                            ammForProductNameLike = new ArrayList<>();
                            while (shorterNomProduit.lastIndexOf(' ') > 0 && CollectionUtils.isEmpty(ammForProductNameLike)) {
                                shorterNomProduit = nomProduit.substring(0, shorterNomProduit.lastIndexOf(' '));
                                ammForProductNameLike.addAll(refActaSubstanceActiveDao.getAmmForProductNameEquals(shorterNomProduit));
                                ammForProductNameLike.addAll(refActaSubstanceActiveDao.getAmmForProductNameLike(shorterNomProduit));
                            }
                            if (CollectionUtils.isNotEmpty(ammForProductNameLike)) {
                                ammForProductNameLike.forEach(sa -> {
                                    String codeAmm = sa.getCode_AMM();
                                    if (!codeAmm.equals(productDtoRow.getAmm_sa())) {
                                        substancesActivesByAmm.put(codeAmm, sa);
                                        ammPossibles.add(codeAmm);
                                    }
                                });
                            }
                        }
                    }

                    matchesByAmm.values().stream().max(Comparator.naturalOrder()).ifPresent(max -> {
                        List<String> ammPossiblesSa = matchesByAmm.keySet().stream()
                                .filter(amm -> matchesByAmm.get(amm).equals(max))
                                .toList();
                        ammPossibles.addAll(ammPossiblesSa);
                    });

                    if (!ammPossibles.isEmpty()) {
                        result.setAmm_probable(String.join(", ", ammPossibles));
                        List<String> allRefData = new ArrayList<>();
                        for (String ammPossible : ammPossibles) {
                            String refData = computeRefData(substancesActivesByAmm, ammPossible);
                            allRefData.add(refData);
                        }
                        result.setRef_data(String.join("\n", allRefData));
                    }

                } else {
                    String message = String.format("Valeur en double trouvée (type=%s, L%d): %s", EuropeanProductDto.class, line, ammUe);
                    if (LOGGER.isErrorEnabled()) {
                        LOGGER.error(message);
                    }
                }
                line++;
            }

            File tempFile = File.createTempFile("export-european-amm-", ".csv");
            tempFile.deleteOnExit();

            Export.exportToFile(new EuropeanProductModel(), outputContent, tempFile, StandardCharsets.UTF_8);
            return new FileInputStream(tempFile);

        } catch (Exception ee) {
            throw new AgrosystTechnicalException(String.format("Import Failed on line %d, cause: %s", line, ee.getMessage()), ee);
        }
    }

    @Override
    public List<RefEspece> getAllActiveRefEspeceWithGivenCodeEspeceBotanique(Set<String> codeEspeceBotaniques) {
        List<RefEspece> allSpeciesWithSameCodeEspeceBotaniques = refEspeceDao.forCode_espece_botaniqueIn(codeEspeceBotaniques)
                .findAll()
                .stream()
                .filter(RefEspece::isActive)
                .toList();
        return allSpeciesWithSameCodeEspeceBotaniques;
    }

    @Override
    public List<RefCompositionSubstancesActivesParNumeroAMM> getDomainRefCompositionSubstancesActivesParNumeroAMM(Domain domain) {
        final List<RefCompositionSubstancesActivesParNumeroAMM> activeSubstances = this.refCompositionSubstancesActivesParNumeroAMMTopiaDao.findAllRefCompositionSubstancesActivesParNumeroAMMForDomain(domain);
        return activeSubstances;
    }

    @Override
    public Map<String, Double> getCoeffsConversionVersKgHa() {
        return this.refConversionUnitesQSATopiaDao.findAllConversionsToKgHa();
    }

    @Override
    public MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> getSubstancesActivesCommissionEuropeenneByAmmCodeForDomain(Domain domain) {
        return this.refSubstancesActivesCommissionEuropeennesDao.findSubstancesActivesCommissionEuropeenneByAmmCodeForDomain(domain);
    }

    @Override
    public MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> getRefPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM(Domain domain) {
        return refPhrasesRisqueEtClassesMentionDangerParAMMTDao.findRefPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM(domain);
    }

    @Override
    public Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> findRefCorrespondanceMaterielOutilsTSForTools(Set<RefMateriel> materiels) {
        return refCorrespondanceMaterielOutilsTSTopiaDao.findRefCorrespondanceMaterielOutilsTSForTools(materiels);
    }

    @Override
    public List<SeedPlantUnit> getSeedUnitsForSpecies(List<RefEspece> species) {
        return refSeedUnitsDao.findAllforRefEspeceIn(species);
    }

    /**
     * use RefCompositionSubstancesActivesParNumeroAMM instead of  RefActaSubstanceActive
     */
    @Deprecated
    protected static String computeRefData(MultiValuedMap<String, RefActaSubstanceActive> substancesActivesByAmm, String amm) {
        Collection<RefActaSubstanceActive> substancesPossibles = substancesActivesByAmm.get(amm);
        RefActaSubstanceActive substanceActive = Iterables.get(substancesPossibles, 0);
        String refData = substanceActive.getNom_produit() + "(" + substanceActive.getCode_AMM() + ") : ";
        refData += substancesPossibles.stream()
                .map(substance -> substance.getNom_commun_sa() + " (" + substance.getConcentration_valeur() + " " + substance.getConcentration_unite() + ")")
                .collect(Collectors.joining(", "));
        return refData;
    }

    protected static void checkIfSubstanceOfProduct(Map<String, Integer> matchesByAmm, String amm, double saConcentration, String saUnit, double concentration, String unit) {
        if (!saUnit.equals(unit)) {
            if ("mg".equals(unit)) {
                concentration = concentration / 1000;
            } else if ("mg/kg".equals(unit)) {
                concentration = concentration / 10000;
            }
            if ("mg".equals(saUnit)) {
                saConcentration = saConcentration / 1000;
            } else if ("mg/kg".equals(saUnit)) {
                saConcentration = saConcentration / 10000;
            }
            if (concentration - saConcentration < 0.1d) {
                matchesByAmm.compute(amm, (key, value) -> {
                    if (value == null) {
                        return 1;
                    }
                    return value + 1;
                });
            }
        }
    }

    protected static boolean namesEqual(String n1, String n2) {
        String cleanN1 = n1.replaceAll("\\W", "");
        String cleanN2 = n2.replaceAll("\\W", "");
        return cleanN1.equalsIgnoreCase(cleanN2);
    }

}
