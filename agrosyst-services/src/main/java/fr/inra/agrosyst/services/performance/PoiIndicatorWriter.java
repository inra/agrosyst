package fr.inra.agrosyst.services.performance;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.HarvestingValorisationPriceSummary;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.RefScenariosInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.utils.CurrentPerformanceDto;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.export.ExportUtils;
import fr.inra.agrosyst.services.performance.indicators.EffectiveItkCropCycleScaleKey;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.compress.archivers.zip.Zip64Mode;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math3.util.Precision;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaPersistenceContext;

import java.io.IOException;
import java.io.OutputStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * Writer abstrait que les indicateurs peuvent utiliser qui délègue l'ecriture au feuille de calcul
 * excel.
 * Les onglets sont composés ainsi en réalisé:
 * <ul>
 * <li>Domaine_Exploitation</li>
 * <li>Système de culture</li>
 * <li>Culture &amp; précédent</li>
 * <li>Parcelle</li>
 * <li>Zone</li>
 * <li>Intervention</li>
 * </ul>
 * 
 * Et en pratiqué:
 * <ul>
 * <li>Domaine_Exploitation</li>
 * <li>Système de culture</li>
 * <li>Culture &amp; précédent</li>
 * <li>Intervention</li>
 * </ul>
 * 
 * @author Eric Chatellier
 */
public class PoiIndicatorWriter implements IndicatorWriter {

    private static final Log LOGGER = LogFactory.getLog(PoiIndicatorWriter.class);

    private final static List<String> ALL_CODES_SCENARIOS = List.of(
            "Code_Scenario_Agrosyst_1",
            "Code_Scenario_Agrosyst_2",
            "Code_Scenario_Agrosyst_3",
            "Code_Scenario_Agrosyst_4",
            "Code_Scenario_Agrosyst_5",
            "Code_Scenario_Agrosyst_6",
            "Code_Scenario_Agrosyst_7",
            "Code_Scenario_Agrosyst_8",
            "Code_Scenario_Agrosyst_9",
            "Code_Scenario_Agrosyst_10"
    );
    public static final int MAXIMUM_NOMBER_OF_ROWS = 1048575;

    final DecimalFormat decimalFormat;
    {
        final DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance();
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormat = new DecimalFormat("#.###", decimalFormatSymbols);
        decimalFormat.setRoundingMode(RoundingMode.HALF_UP);
    }

    protected Locale locale;
    protected AgrosystI18nService i18nService;
    protected CurrentPerformanceDto performance;
    protected OutputStream output;
    protected TopiaPersistenceContext persistenceContext;
    protected SXSSFWorkbook workbook;
    protected SXSSFSheet domainSheet;
    protected SXSSFSheet growingSystemSheet;
    protected SXSSFSheet practicedSystemSheet;
    protected SXSSFSheet plotSystemSheet;
    protected SXSSFSheet zoneSheet;
    protected SXSSFSheet practicedCroppingPlanSheet;
    protected SXSSFSheet effectiveCroppingPlanSheet;
    protected SXSSFSheet interventionSheet;
    protected SXSSFSheet inputSheet;
    protected SXSSFSheet pricesSheet;
    
    public final Function<Map<Pair<RefDestination, YealdUnit>, Double>, String> GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION = yealdAverages -> {
        List<String> allPrintablePlotYealdAverage = new ArrayList<>();
        if (yealdAverages != null) {
            for (Map.Entry<Pair<RefDestination, YealdUnit>, Double> pairDoubleEntry : yealdAverages.entrySet()) {
                
                final Pair<RefDestination, YealdUnit> destAndUnit = pairDoubleEntry.getKey();
                final String unit = AgrosystI18nService.getEnumTraduction(getLocale(), destAndUnit.getRight());
                final String destination = destAndUnit.getLeft().getDestination();
                final Double yealdAverage = pairDoubleEntry.getValue();
                
                String printableYeald = (yealdAverage == null ? "?" : yealdAverage) + " (" + destination + "/" + unit + ")";
                allPrintablePlotYealdAverage.add(printableYeald);
            }
        }
        return String.join(", ", allPrintablePlotYealdAverage);
    };
    
    public PoiIndicatorWriter(Language language, CurrentPerformanceDto performance, OutputStream output, AgrosystI18nService i18nService) {
        this.performance = performance;
        this.output = output;
        this.i18nService = i18nService;
        this.persistenceContext = i18nService.getPersistenceContext();
        this.locale = language.getLocale();
        workbook = new SXSSFWorkbook();
        workbook.setZip64Mode(Zip64Mode.Never); // see https://bugs.documentfoundation.org/show_bug.cgi?id=162944
    }
    
    public void init() {
        if (performance.isPracticed()) {
            initPracticed();
        } else {
            initEffective();
        }
    }

    protected void initEffective() {
        domainSheet = workbook.createSheet(l(locale, "IndicatorWriter.domain"));
        addHeaders(
                workbook,
                domainSheet,
                l(locale, "IndicatorWriter.domain"),
                l(locale, "IndicatorWriter.department"),
                l(locale, "IndicatorWriter.typesAgriculture"),
                l(locale, "IndicatorWriter.year"),// first common
                l(locale, "IndicatorWriter.effectiveOrPracticed"),
                l(locale, "IndicatorWriter.indicatorCategory"),
                l(locale, "IndicatorWriter.indicator"),
                l(locale, "IndicatorWriter.value"),
                l(locale, "IndicatorWriter.completionRate"),
                l(locale, "IndicatorWriter.requiredMissingFields"));

        growingSystemSheet = workbook.createSheet(l(locale, "IndicatorWriter.croppingSystems"));
        addHeaders(
                workbook,
                growingSystemSheet,
                l(locale, "IndicatorWriter.domain"),
                l(locale, "IndicatorWriter.itNetworkName"),
                l(locale, "IndicatorWriter.irNetworkName"),
                l(locale, "IndicatorWriter.department"),
                l(locale, "IndicatorWriter.croppingSystem"),
                l(locale, "IndicatorWriter.dephyNumber"),
                l(locale, "IndicatorWriter.typeAgriculture"),
                l(locale, "IndicatorWriter.validatedCroppingSystem"),
                l(locale, "IndicatorWriter.year"),// first common
                l(locale, "IndicatorWriter.effectiveOrPracticed"),
                l(locale, "IndicatorWriter.indicatorCategory"),
                l(locale, "IndicatorWriter.indicator"),
                l(locale, "IndicatorWriter.value"),
                l(locale, "IndicatorWriter.completionRate"),
                l(locale, "IndicatorWriter.requiredMissingFields"));

        plotSystemSheet = workbook.createSheet(l(locale, "IndicatorWriter.plots"));
        addHeaders(
                workbook,
                plotSystemSheet,
                l(locale, "IndicatorWriter.domain"),
                l(locale, "IndicatorWriter.itNetworkName"),
                l(locale, "IndicatorWriter.irNetworkName"),
                l(locale, "IndicatorWriter.department"),
                l(locale, "IndicatorWriter.croppingSystem"),
                l(locale, "IndicatorWriter.dephyNumber"),
                l(locale, "IndicatorWriter.typeAgriculture"),
                l(locale, "IndicatorWriter.plot"),
                l(locale, "IndicatorWriter.year"),// first common
                l(locale, "IndicatorWriter.effectiveOrPracticed"),
                l(locale, "IndicatorWriter.indicatorCategory"),
                l(locale, "IndicatorWriter.indicator"),
                l(locale, "IndicatorWriter.value"),
                l(locale, "IndicatorWriter.yeald"),
                l(locale, "IndicatorWriter.completionRate"),
                l(locale, "IndicatorWriter.requiredMissingFields"));

        zoneSheet = workbook.createSheet(l(locale, "IndicatorWriter.zones"));
        addHeaders(
                workbook,
                zoneSheet,
                l(locale, "IndicatorWriter.domain"),
                l(locale, "IndicatorWriter.itNetworkName"),
                l(locale, "IndicatorWriter.irNetworkName"),
                l(locale, "IndicatorWriter.croppingSystem"),
                l(locale, "IndicatorWriter.dephyNumber"),
                l(locale, "IndicatorWriter.typeAgriculture"),
                l(locale, "IndicatorWriter.plot"),
                l(locale, "IndicatorWriter.zone"),
                l(locale, "IndicatorWriter.species"),
                l(locale, "IndicatorWriter.variety"),
                l(locale, "IndicatorWriter.year"),// first common
                l(locale, "IndicatorWriter.effectiveOrPracticed"),
                l(locale, "IndicatorWriter.indicatorCategory"),
                l(locale, "IndicatorWriter.indicator"),
                l(locale, "IndicatorWriter.value"),
                l(locale, "IndicatorWriter.yeald"),
                l(locale, "IndicatorWriter.completionRate"),
                l(locale, "IndicatorWriter.requiredMissingFields"));

        effectiveCroppingPlanSheet = workbook.createSheet(l(locale, "IndicatorWriter.previousPhases"));
        addHeaders(
                workbook,
                effectiveCroppingPlanSheet,
                l(locale, "IndicatorWriter.domain"),
                l(locale, "IndicatorWriter.itNetworkName"),
                l(locale, "IndicatorWriter.irNetworkName"),
                l(locale, "IndicatorWriter.croppingSystem"),
                l(locale, "IndicatorWriter.dephyNumber"),
                l(locale, "IndicatorWriter.typeAgriculture"),
                l(locale, "IndicatorWriter.plot"),
                l(locale, "IndicatorWriter.cropRank"),
                l(locale, "IndicatorWriter.species"),
                l(locale, "IndicatorWriter.variety"),
                l(locale, "IndicatorWriter.previousPhase"),
                l(locale, "IndicatorWriter.year"),// first common
                l(locale, "IndicatorWriter.effectiveOrPracticed"),
                l(locale, "IndicatorWriter.indicatorCategory"),
                l(locale, "IndicatorWriter.indicator"),
                l(locale, "IndicatorWriter.value"),
                l(locale, "IndicatorWriter.yeald"),
                l(locale, "IndicatorWriter.completionRate"),
                l(locale, "IndicatorWriter.requiredMissingFields"));

        interventionSheet = workbook.createSheet(l(locale, "IndicatorWriter.interventions"));
        addHeaders(
                workbook,
                interventionSheet,
                l(locale, "IndicatorWriter.domain"),
                l(locale, "IndicatorWriter.itNetworkName"),
                l(locale, "IndicatorWriter.irNetworkName"),
                l(locale, "IndicatorWriter.croppingSystem"),
                l(locale, "IndicatorWriter.dephyNumber"),
                l(locale, "IndicatorWriter.typeAgriculture"),
                l(locale, "IndicatorWriter.plot"),
                l(locale, "IndicatorWriter.zone"),
                l(locale, "IndicatorWriter.zoneArea"),
                l(locale, "IndicatorWriter.crop"),
                l(locale, "IndicatorWriter.icAssignment"),
                l(locale, "IndicatorWriter.species"),
                l(locale, "IndicatorWriter.variety"),
                l(locale, "IndicatorWriter.interventionSpecies"),
                l(locale, "IndicatorWriter.interventionVarieties"),
                l(locale, "IndicatorWriter.previousPhase"),
                l(locale, "IndicatorWriter.interventionType"),
                l(locale, "IndicatorWriter.intervention"),
                l(locale, "IndicatorWriter.interventionId"),
                l(locale, "IndicatorWriter.startDate"),
                l(locale, "IndicatorWriter.endDate"),
                l(locale, "IndicatorWriter.actions"),
                l(locale, "IndicatorWriter.spatialFrequency"),
                l(locale, "IndicatorWriter.input.s"),
                l(locale, "IndicatorWriter.inputNb"),
                l(locale, "IndicatorWriter.targetGroups"),
                l(locale, "IndicatorWriter.treatmenTarget"),
                l(locale, "IndicatorWriter.biocontrol"),
                l(locale, "IndicatorWriter.years"),// first common
                l(locale, "IndicatorWriter.effectiveOrPracticed"),
                l(locale, "IndicatorWriter.indicatorCategory"),
                l(locale, "IndicatorWriter.indicator"),
                l(locale, "IndicatorWriter.value"),
                l(locale, "IndicatorWriter.completionRate"),
                l(locale, "IndicatorWriter.requiredMissingFields"),
                l(locale, "IndicatorWriter.referenceDoses"));

        inputSheet = workbook.createSheet(l(locale, "IndicatorWriter.inputs"));
        addHeaders(
                workbook,
                inputSheet,
                l(locale, "IndicatorWriter.domain"),
                l(locale, "IndicatorWriter.itNetworkName"),
                l(locale, "IndicatorWriter.irNetworkName"),
                l(locale, "IndicatorWriter.croppingSystem"),
                l(locale, "IndicatorWriter.dephyNumber"),
                l(locale, "IndicatorWriter.typeAgriculture"),
                l(locale, "IndicatorWriter.plot"),
                l(locale, "IndicatorWriter.zone"),
                l(locale, "IndicatorWriter.zoneArea"),
                l(locale, "IndicatorWriter.crop"),
                l(locale, "IndicatorWriter.icAssignment"),
                l(locale, "IndicatorWriter.species"),
                l(locale, "IndicatorWriter.variety"),
                l(locale, "IndicatorWriter.interventionSpecies"),
                l(locale, "IndicatorWriter.interventionVarieties"),
                l(locale, "IndicatorWriter.previousPhase"),
                l(locale, "IndicatorWriter.interventionType"),
                l(locale, "IndicatorWriter.intervention"),
                l(locale, "IndicatorWriter.interventionId"),
                l(locale, "IndicatorWriter.startDate"),
                l(locale, "IndicatorWriter.endDate"),
                l(locale, "IndicatorWriter.action"),
                l(locale, "IndicatorWriter.input"),
                l(locale, "IndicatorWriter.targetGroup"),
                l(locale, "IndicatorWriter.treatmenTarget"),
                l(locale, "IndicatorWriter.biocontrol"),
                l(locale, "IndicatorWriter.years"),// first common
                l(locale, "IndicatorWriter.effectiveOrPracticed"),
                l(locale, "IndicatorWriter.indicatorCategory"),
                l(locale, "IndicatorWriter.indicator"),
                l(locale, "IndicatorWriter.value"),
                l(locale, "IndicatorWriter.completionRate"),
                l(locale, "IndicatorWriter.requiredMissingFields"),
                l(locale, "IndicatorWriter.referenceDoses"));

        pricesSheet = workbook.createSheet(l(locale, "IndicatorWriter.referencePrices"));
        addHeaders(workbook,
                pricesSheet,
                l(locale, "IndicatorWriter.domain"),
                l(locale, "IndicatorWriter.campaign"),
                l(locale, "IndicatorWriter.type"),
                l(locale, "IndicatorWriter.name"),
                l(locale, "IndicatorWriter.additionalInformation"),
                l(locale, "IndicatorWriter.equipmentLifespan"),
                l(locale, "IndicatorWriter.equipmentInvestment"),
                l(locale, "IndicatorWriter.realComputationPrices"),
                l(locale, "IndicatorWriter.unit"),
                l(locale, "IndicatorWriter.standardComputationPrices"),
                l(locale, "IndicatorWriter.unit"),
                l(locale, "IndicatorWriter.organicPrices"),
                l(locale, "IndicatorWriter.source"),
                l(locale, "IndicatorWriter.prixScenario1"),
                l(locale, "IndicatorWriter.prixScenario2"),
                l(locale, "IndicatorWriter.prixScenario3"),
                l(locale, "IndicatorWriter.prixScenario4"),
                l(locale, "IndicatorWriter.prixScenario5"),
                l(locale, "IndicatorWriter.prixScenario6"),
                l(locale, "IndicatorWriter.prixScenario7"),
                l(locale, "IndicatorWriter.prixScenario8"),
                l(locale, "IndicatorWriter.prixScenario9"),
                l(locale, "IndicatorWriter.prixScenario10")
        );
    }

    protected void initPracticed() {
        domainSheet = workbook.createSheet(l(locale, "IndicatorWriter.domain"));
        addHeaders(workbook,
                domainSheet,
                l(locale, "IndicatorWriter.domain"),
                l(locale, "IndicatorWriter.department"),
                l(locale, "IndicatorWriter.typesAgriculture"),
                l(locale, "IndicatorWriter.years"),// first common
                l(locale, "IndicatorWriter.effectiveOrPracticed"),
                l(locale, "IndicatorWriter.indicatorCategory"),
                l(locale, "IndicatorWriter.indicator"),
                l(locale, "IndicatorWriter.value"),
                l(locale, "IndicatorWriter.completionRate"),
                l(locale, "IndicatorWriter.requiredMissingFields"));

        practicedSystemSheet = workbook.createSheet(l(locale, "IndicatorWriter.practicedSystem"));
        addHeaders(workbook,
                practicedSystemSheet,
                l(locale, "IndicatorWriter.domain"),
                l(locale, "IndicatorWriter.itNetworkName"),
                l(locale, "IndicatorWriter.irNetworkName"),
                l(locale, "IndicatorWriter.department"),
                l(locale, "IndicatorWriter.croppingSystem"),
                l(locale, "IndicatorWriter.dephyNumber"),
                l(locale, "IndicatorWriter.typeAgriculture"),
                l(locale, "IndicatorWriter.validatedCroppingSystem"),
                l(locale, "IndicatorWriter.practicedSystem"),
                l(locale, "IndicatorWriter.validatedPracticedSystem"),
                l(locale, "IndicatorWriter.years"),// first common
                l(locale, "IndicatorWriter.effectiveOrPracticed"),
                l(locale, "IndicatorWriter.indicatorCategory"),
                l(locale, "IndicatorWriter.indicator"),
                l(locale, "IndicatorWriter.value"),
                l(locale, "IndicatorWriter.completionRate"),
                l(locale, "IndicatorWriter.requiredMissingFields"));

        practicedCroppingPlanSheet = workbook.createSheet(l(locale,"IndicatorWriter.previousPhases"));
        addHeaders(workbook,
                practicedCroppingPlanSheet,
                l(locale, "IndicatorWriter.domain"),
                l(locale, "IndicatorWriter.itNetworkName"),
                l(locale, "IndicatorWriter.irNetworkName"),
                l(locale, "IndicatorWriter.croppingSystem"),
                l(locale, "IndicatorWriter.dephyNumber"),
                l(locale, "IndicatorWriter.typeAgriculture"),
                l(locale, "IndicatorWriter.practicedSystem"),
                l(locale, "IndicatorWriter.cropRank"),
                l(locale, "IndicatorWriter.species"),
                l(locale, "IndicatorWriter.variety"),
                l(locale, "IndicatorWriter.previousPhase"),
                l(locale, "IndicatorWriter.persistentCropsRate"),
                l(locale, "IndicatorWriter.cpCoupleFrequency"),
                l(locale, "IndicatorWriter.years"),// first common
                l(locale, "IndicatorWriter.effectiveOrPracticed"),
                l(locale, "IndicatorWriter.indicatorCategory"),
                l(locale, "IndicatorWriter.indicator"),
                l(locale, "IndicatorWriter.value"),
                l(locale, "IndicatorWriter.yeald"),
                l(locale, "IndicatorWriter.completionRate"),
                l(locale, "IndicatorWriter.requiredMissingFields"));

        interventionSheet = workbook.createSheet("Interventions");
        addHeaders(workbook,
                interventionSheet,
                l(locale, "IndicatorWriter.domain"),
                l(locale, "IndicatorWriter.itNetworkName"),
                l(locale, "IndicatorWriter.irNetworkName"),
                l(locale, "IndicatorWriter.croppingSystem"),
                l(locale, "IndicatorWriter.dephyNumber"),
                l(locale, "IndicatorWriter.typeAgriculture"),
                l(locale, "IndicatorWriter.practicedSystem"),
                l(locale, "IndicatorWriter.crop"),
                l(locale, "IndicatorWriter.icAssignment"),
                l(locale, "IndicatorWriter.species"),
                l(locale, "IndicatorWriter.variety"),
                l(locale, "IndicatorWriter.interventionSpecies"),
                l(locale, "IndicatorWriter.interventionVarieties"),
                l(locale, "IndicatorWriter.previousPhase"),
                l(locale, "IndicatorWriter.interventionType"),
                l(locale, "IndicatorWriter.intervention"),
                l(locale, "IndicatorWriter.interventionId"),
                l(locale, "IndicatorWriter.startDate"),
                l(locale, "IndicatorWriter.endDate"),
                l(locale, "IndicatorWriter.actions"),
                l(locale, "IndicatorWriter.spatialFrequency"),
                l(locale, "IndicatorWriter.input.s"),
                l(locale, "IndicatorWriter.inputNb"),
                l(locale, "IndicatorWriter.targetGroups"),
                l(locale, "IndicatorWriter.treatmenTarget"),
                l(locale, "IndicatorWriter.biocontrol"),
                l(locale, "IndicatorWriter.years"),// first common
                l(locale, "IndicatorWriter.effectiveOrPracticed"),
                l(locale, "IndicatorWriter.indicatorCategory"),
                l(locale, "IndicatorWriter.indicator"),
                l(locale, "IndicatorWriter.value"),
                l(locale, "IndicatorWriter.completionRate"),
                l(locale, "IndicatorWriter.requiredMissingFields"),
                l(locale, "IndicatorWriter.referenceDoses"));

        inputSheet = workbook.createSheet(l(locale, "IndicatorWriter.inputs"));
        addHeaders(
                workbook,
                inputSheet,
                l(locale, "IndicatorWriter.domain"),
                l(locale, "IndicatorWriter.itNetworkName"),
                l(locale, "IndicatorWriter.irNetworkName"),
                l(locale, "IndicatorWriter.croppingSystem"),
                l(locale, "IndicatorWriter.dephyNumber"),
                l(locale, "IndicatorWriter.typeAgriculture"),
                l(locale, "IndicatorWriter.practicedSystem"),
                l(locale, "IndicatorWriter.crop"),
                l(locale, "IndicatorWriter.icAssignment"),
                l(locale, "IndicatorWriter.species"),
                l(locale, "IndicatorWriter.variety"),
                l(locale, "IndicatorWriter.interventionSpecies"),
                l(locale, "IndicatorWriter.interventionVarieties"),
                l(locale, "IndicatorWriter.previousPhase"),
                l(locale, "IndicatorWriter.interventionType"),
                l(locale, "IndicatorWriter.intervention"),
                l(locale, "IndicatorWriter.interventionId"),
                l(locale, "IndicatorWriter.startDate"),
                l(locale, "IndicatorWriter.endDate"),
                l(locale, "IndicatorWriter.action"),
                l(locale, "IndicatorWriter.input"),
                l(locale, "IndicatorWriter.targetGroup"),
                l(locale, "IndicatorWriter.treatmenTarget"),
                l(locale, "IndicatorWriter.biocontrol"),
                l(locale, "IndicatorWriter.years"),// first common
                l(locale, "IndicatorWriter.effectiveOrPracticed"),
                l(locale, "IndicatorWriter.indicatorCategory"),
                l(locale, "IndicatorWriter.indicator"),
                l(locale, "IndicatorWriter.value"),
                l(locale, "IndicatorWriter.completionRate"),
                l(locale, "IndicatorWriter.requiredMissingFields"),
                l(locale, "IndicatorWriter.referenceDoses"));
    
        pricesSheet = workbook.createSheet(l(locale, "IndicatorWriter.referencePrices"));
        addHeaders(workbook,
                pricesSheet,
                l(locale, "IndicatorWriter.domain"),
                l(locale, "IndicatorWriter.practicedSystem"),
                l(locale, "IndicatorWriter.campaignSerie"),
                l(locale, "IndicatorWriter.type"),
                l(locale, "IndicatorWriter.name"),
                l(locale, "IndicatorWriter.additionalInformation"),
                l(locale, "IndicatorWriter.equipmentLifespan"),
                l(locale, "IndicatorWriter.equipmentInvestment"),
                l(locale, "IndicatorWriter.realComputationPrices"),
                l(locale, "IndicatorWriter.unit"),
                l(locale, "IndicatorWriter.standardComputationPrices"),
                l(locale, "IndicatorWriter.unit"),
                l(locale, "IndicatorWriter.organicPrices"),
                l(locale, "IndicatorWriter.source"),
                l(locale, "IndicatorWriter.prixScenario1"),
                l(locale, "IndicatorWriter.prixScenario2"),
                l(locale, "IndicatorWriter.prixScenario3"),
                l(locale, "IndicatorWriter.prixScenario4"),
                l(locale, "IndicatorWriter.prixScenario5"),
                l(locale, "IndicatorWriter.prixScenario6"),
                l(locale, "IndicatorWriter.prixScenario7"),
                l(locale, "IndicatorWriter.prixScenario8"),
                l(locale, "IndicatorWriter.prixScenario9"),
                l(locale, "IndicatorWriter.prixScenario10")
        );
    }

    /**
     * Write header line (bold font) in given SpeadSheet.
     * 
     * @param workbook workbook
     * @param sheet sheet
     * @param headers headers
     */
    protected void addHeaders(Workbook workbook, SXSSFSheet sheet, String... headers) {
        // mandatory in SXSSF mode
        //sheet.trackAllColumnsForAutoSizing(); // not working
        for (int i = 0; i < headers.length; i++) {
            sheet.trackColumnForAutoSizing(i);
        }

        Row row0 = sheet.createRow(0);
        CellStyle style = workbook.createCellStyle(); // bold
        Font font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);
        int header = 0;
        for (String displayName : headers) {
            Cell cell0 = row0.createCell(header++);
            cell0.setCellValue(displayName);
            cell0.setCellStyle(style);
        }
    }

    @Override
    public void addComputedIndicators(GenericIndicator... indicators) {}

    @Override
    public void afterAllIndicatorsAreWritten() {}

    @Override
    public void finish() {
        // autosize columns
        for (int s = 0; s < workbook.getNumberOfSheets(); s++) {
            SXSSFSheet sheet = workbook.getSheetAt(s);
            for (Integer column : sheet.getTrackedColumnsForAutoSizing()) {
                sheet.autoSizeColumn(column);
            }
        }
    
        // dump workbook to stream
        try {
            workbook.write(output);
        } catch (IOException ex) {
            throw new AgrosystTechnicalException("Can't write to output stream", ex);
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                LOGGER.error(e);
            }
        }
    }

    @Override
    public void writePracticedSeasonalIntervention(
            String its,
            String irs,
            String campaigns,
            String indicatorCategory,
            String indicatorName,
            Object value,
            Integer reliabilityIndex,
            String comment,
            List<String> referenceDosages,
            Domain domain,
            GrowingSystem growingSystem,
            PracticedSystem practicedSystem,
            CroppingPlanEntry croppingPlanEntry,
            int rank,
            CroppingPlanEntry previousPlanEntry,
            CroppingPlanEntry intermediateCrop,
            PracticedIntervention intervention,
            Collection<AbstractAction> actions,
            Collection<String> codeAmmBioControle,
            Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage,
            Map<String, String> groupesCiblesByCode,
            Class<? extends GenericIndicator> indicatorClass) {
        CroppingPlanEntry interCrop = intervention.isIntermediateCrop() ? intermediateCrop : croppingPlanEntry;
    
        //"Domaine_Exploitation",
        //"Nom du réseau de l'IT", "Nom du réseau de l'IR", "Systèmes de culture", "Numéro DEPHY", "Type de conduite du SdC",
        //"Système synthétisé", "Culture", "Affection CI", "Espèces", "Variétés",
        // "Espèces concernées par l'intervention", "Varietes concernées par l'intervention"
        // "Précédent/Phase", "Type d'intervention", "Intervention", "Action(s)", "Intrant(s)", "Cible(s) de traitement",
        //"Biocontrôle", "Année(s)", "Approche de calcul", "Catégorie d'indicateur", "Indicateur", "Valeur", "Taux de complétion (en %)",
        // "Détails des champs non renseignés requis pour le calcul de l'indicateur", "Doses de référence utilisées pour le calcul IFT culture"
        Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(actions, domain);
        writeToSheet(
                interventionSheet,
                campaigns, //                                                                    "Année(s)",
                //                                                                               "Approche de calcul" :implicite
                //                                                                               Ignore for intervention sheet
                indicatorCategory, //                                                            "Catégorie d'indicateur"
                indicatorName, //                                                                "Indicateur"
                value, //                                                                        "Valeur"
                null,                                                         // Not used for intervention sheet
                reliabilityIndex, //                                                             "Taux de complétion (en %)"
                comment, //                                                           "Détails des champs non renseignés requis pour le calcul de l'indicateur",
                referenceDosages == null ? "" : String.join(", ", referenceDosages), //  "Doses de référence utilisées pour le calcul IFT culture"
                null,
            
                // dynamic cells (first fields displayed)
                getDomainName(domain), //                                                        "Domaine_Exploitation"
                its, irs, //                                                                     "Nom du réseau de l'IT", "Nom du réseau de l'IR"
                getGrowingSystemName(Optional.ofNullable(growingSystem)), //                                           "Systèmes de culture"
                geDephyNumber(Optional.ofNullable(growingSystem)), //                                                 "Numéro DEPHY"
                getTypeDeConduiteSDC(Optional.ofNullable(growingSystem)), //                                          "Type de conduite du SdC"
                practicedSystem.getName(), //                                                    "Système synthétisé"
                l(locale, CROP_NAME_RANK, croppingPlanEntry != null ? croppingPlanEntry.getName() : l(locale, "IndicatorWriter.undefined"), rank + 1), //        "Culture"
                intermediateCrop != null ? intermediateCrop.getName() : "", //                   "Affection CI"
                getCropSpeciesName(interCrop), //                                                "Espèces"
                getCropVarietyName(interCrop), //                                                "Variétés"
                getInterventionSpeciesName(interCrop, intervention), //                          "Espèces concernées par l'intervention",
                getInterventionVarietyName(interCrop, intervention), //                          "Varietes concernées par l'intervention"
                previousPlanEntry != null ? previousPlanEntry.getName() : "", //                 "Précédent/Phase"
                AgrosystI18nService.getEnumTraduction(locale, intervention.getType()), //                "Type d'intervention"
                intervention.getName(), //                                                       "Intervention"
                getShortId(intervention), //                                 "Identifiant de l'intervention",
                intervention.getStartingPeriodDate(), //                                         "Date de début"
                intervention.getEndingPeriodDate(), //                                           "Date de fin"
                getActionsToString(actions), //                                            "Action(s)"
                Double.toString(intervention.getSpatialFrequency()), // fréquence spatiale
                getInputUsagesToString(inputUsages, actions, interCrop),
                getNbInputUsages(inputUsages),
                getGroupesCiblesToString(inputUsages, groupesCiblesByCode),
                getInputUsageTargetsToString(inputUsages),
                getBooleanAsString(isOneInputUsageBiocontrole(inputUsages, codeAmmBioControle))
        );
    
    }


    @Override
    public void writeInputUsage(
            String its,
            String irs,
            String indicatorCategory,
            String indicatorName,
            AbstractInputUsage usage,
            AbstractAction action,
            EffectiveIntervention effectiveIntervention,
            PracticedIntervention practicedIntervention,
            Collection<String> codeAmmBioControle,
            Domain domain,
            GrowingSystem growingSystem,
            Plot plot,
            Zone zone,
            PracticedSystem practicedSystem,
            CroppingPlanEntry croppingPlanEntry,
            PracticedCropCyclePhase practicedCropCyclePhase,
            Double solOccupationPercent,
            EffectiveCropCyclePhase effectiveCropCyclePhase,
            Integer rank,
            CroppingPlanEntry previousCrop,
            CroppingPlanEntry intermediateCrop0,
            Class<? extends GenericIndicator> indicatorClass,
            Object value,
            Integer reliabilityIndexForInputId,
            String reliabilityCommentForInputId,
            String iftRefDoseUserInfos,
            String iftRefDoseValue,
            String iftRefDoseUnit,
            Map<String, String> groupesCiblesByCode) {


        final Optional<Boolean> maybeInputUsageBiocontrole = isInputUsageBiocontrole(usage, codeAmmBioControle);
        final String biocontrole = maybeInputUsageBiocontrole.map(this::getBooleanAsString).orElse(null);

        Pair<String, String> speciesAndVarietyNames = getSpeciesAndVarietyName(usage, action, croppingPlanEntry);
        String speciesNames = speciesAndVarietyNames.getLeft();
        String varietyNames = speciesAndVarietyNames.getRight();

        if (practicedSystem != null) {
        
            String precedentOrPhaseType;
            if (practicedCropCyclePhase != null) {
                precedentOrPhaseType = AgrosystI18nService.getEnumTraduction(locale, practicedCropCyclePhase.getType());
            } else if (previousCrop != null) {
                precedentOrPhaseType = previousCrop.getName();
            } else {
                precedentOrPhaseType = "";
            }

            CroppingPlanEntry intermediateCrop = null;
            if (practicedIntervention.isIntermediateCrop()) {
                intermediateCrop = intermediateCrop0;
            }
        
            String cultureName;
            if (rank != null) {
                cultureName = l(locale, CROP_NAME_RANK, croppingPlanEntry != null ? croppingPlanEntry.getName() : l(locale, "IndicatorWriter.undefined"), rank + 1);
            } else {
                cultureName = croppingPlanEntry.getName();
            }
        
            writeToSheet(
                    inputSheet,
                    practicedSystem.getCampaigns(),
                    indicatorCategory,
                    indicatorName,
                    value,
                    null,
                    reliabilityIndexForInputId,
                    reliabilityCommentForInputId,
                    iftRefDoseUserInfos,
                    // dynamic cells (first fields displayed)
                    null,// end of common lignes
                    getDomainName(domain),                                             //"Domaine_Exploitation",
                    its,                                                                        //"Nom du réseau de l'IT",
                    irs,                                                                        //"Nom du réseau de l'IR",
                    getGrowingSystemName((Optional.ofNullable(growingSystem))),                         //"Systèmes de culture",
                    geDephyNumber((Optional.ofNullable(growingSystem))),                                //"Numéro DEPHY",
                    getTypeDeConduiteSDC((Optional.ofNullable(growingSystem))),                         //"Type de conduite du SdC",
                    practicedSystem.getName(),                                                  //"Système synthétisé",
                    cultureName,                                                //"Culture",
                    intermediateCrop != null ? intermediateCrop.getName() : "",                // "Affection CI"
                    getCropSpeciesName(croppingPlanEntry),                      //"Espèces",
                    getCropVarietyName(croppingPlanEntry),                      //"Variétés",
                    speciesNames,//"Espèces concernées par l'intervention",
                    varietyNames,//"Variétés concernées par l'intervention",
                    precedentOrPhaseType,                     //"Précédent/Phase",
                    AgrosystI18nService.getEnumTraduction(locale, practicedIntervention.getType()),              //"Type d'intervention",
                    practicedIntervention.getName(),                                                     //"Intervention",
                    getShortId(practicedIntervention),                               //"Identifiant de l'intervention",
                    practicedIntervention.getStartingPeriodDate(),                                       //"Date de début",
                    practicedIntervention.getEndingPeriodDate(),                                         //"Date de fin",
                    getActionToString(action),                                //"Action",
                    getInputUsageProductName(usage, action, intermediateCrop, croppingPlanEntry, true),//"Intrant
                    getGroupesCiblesToString(usage, groupesCiblesByCode),
                    getTargetsToString(usage),                                 //"Cible(s) de traitement"
                    biocontrole //"Biocontrôle"
            );
        } else {
        
            String precedentOrPhaseType;
            if (effectiveCropCyclePhase != null) {
                precedentOrPhaseType = AgrosystI18nService.getEnumTraduction(locale, effectiveCropCyclePhase.getType());
            } else if (previousCrop != null) {
                precedentOrPhaseType = previousCrop.getName();
            } else {
                precedentOrPhaseType = "";
            }

            CroppingPlanEntry intermediateCrop = null;
            if (effectiveIntervention.isIntermediateCrop()) {
                intermediateCrop = intermediateCrop0;
            }
        
            writeToSheet(
                    inputSheet,
                    String.valueOf(domain.getCampaign()),
                    indicatorCategory,
                    indicatorName,
                    value,
                    null,
                    reliabilityIndexForInputId,
                    reliabilityCommentForInputId,
                    iftRefDoseUserInfos,
                    // dynamic cells (first fields displayed)
                    null,// end of common lignes
                    getDomainName(domain),                                             //"Domaine_Exploitation",
                    its,                                                                        //"Nom du réseau de l'IT",
                    irs,                                                                        //"Nom du réseau de l'IR",
                    getGrowingSystemName((Optional.ofNullable(growingSystem))),                         //"Systèmes de culture",
                    geDephyNumber((Optional.ofNullable(growingSystem))),                                //"Numéro DEPHY",
                    getTypeDeConduiteSDC((Optional.ofNullable(growingSystem))),                         //"Type de conduite du SdC",
                    plot.getName(),                                                             //"Parcelle",
                    zone.getName(),                                                             //"Zone",
                    String.valueOf(zone.getArea()),                                             //"Surface de la zone (en ha)",
                    croppingPlanEntry.getName(),                                                //"Culture"
                    intermediateCrop != null ? intermediateCrop.getName() : "",                 //"Affection CI"
                    getCropSpeciesName(croppingPlanEntry),                      //"Espèces",
                    getCropVarietyName(croppingPlanEntry),                      //"Variétés",
                    speciesNames,          //"Espèces concernées par l'intervention",
                    varietyNames,          //"Variétés concernées par l'intervention",
                    precedentOrPhaseType,                     //"Précédent/Phase",
                    AgrosystI18nService.getEnumTraduction(locale, effectiveIntervention.getType()),     //"Type d'intervention",
                    effectiveIntervention.getName(),                                            //"Intervention",
                    getShortId(effectiveIntervention),                      //"Identifiant de l'intervention",
                    INTERVENTION_DATE_FORMAT_DDMMYYYY.format(effectiveIntervention.getStartInterventionDate()),//"Date de début",
                    INTERVENTION_DATE_FORMAT_DDMMYYYY.format(effectiveIntervention.getEndInterventionDate()),//"Date de fin",
                    getActionToString(action),                                  //"Action",
                    getInputUsageProductName(usage, action, intermediateCrop, croppingPlanEntry, true),//"Intrant",
                    getGroupesCiblesToString(usage, groupesCiblesByCode),
                    getTargetsToString(usage),                                  //"Cible(s) de traitement"
                    biocontrole //"Biocontrôle"
            );
        }
    }
    
    @Override
    public void writePracticedPerennialIntervention(
            String its,
            String irs,
            String campaigns,
            String indicatorCategory,
            String indicatorName,
            Object value,
            Integer reliabilityIndex,
            String comment,
            List<String> referenceDosages,
            Domain domain,
            GrowingSystem growingSystem,
            PracticedSystem practicedSystem,
            CroppingPlanEntry croppingPlanEntry,
            PracticedCropCyclePhase phase,
            Double perennialCropCyclePercent,
            PracticedIntervention intervention,
            Collection<AbstractAction> actions,
            Collection<String> codeAmmBioControle,
            Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage,
            Map<String, String> groupesCiblesByCode,
            Class<? extends GenericIndicator> indicatorClass) {
    
        //"Domaine_Exploitation",
        //"Nom du réseau de l'IT", "Nom du réseau de l'IR", "Systèmes de culture", "Numéro DEPHY", "Type de conduite du SdC",
        //"Système synthétisé", "Culture", "Affection CI", "Espèces", "Variétés",
        //   "Espèces concernées par l'intervention", "Varietes concernées par l'intervention"
        // "Précédent/Phase", "Type d'intervention", "Intervention", "Identifiant de l'intervention", "Date de début", "Date de fin", "Action(s)", "Intrant(s)",
        // "Cible(s) de traitement",
    
        //"Année(s)",
        //"Approche de calcul",
        //"Catégorie d'indicateur", "Indicateur", "Valeur",
        //"Taux de complétion (en %)",
        //"Détails des champs non renseignés requis pour le calcul de l'indicateur",
        //"Doses de référence utilisées pour le calcul IFT culture"
        Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(actions, domain);
        writeToSheet(
                interventionSheet,
                campaigns,
                indicatorCategory,
                indicatorName,
                value,
                "", reliabilityIndex, comment,
                referenceDosages == null ? "" : String.join(", ", referenceDosages),
            
                // dynamic cells (first fields displayed)
                null, getDomainName(domain),
                its, irs,
                getGrowingSystemName((Optional.ofNullable(growingSystem))),
                geDephyNumber((Optional.ofNullable(growingSystem))),
                getTypeDeConduiteSDC((Optional.ofNullable(growingSystem))),
                practicedSystem.getName(),
                croppingPlanEntry.getName(),
                "", // "Affection CI" non applicable for perennial
                getCropSpeciesName(croppingPlanEntry),
                getCropVarietyName(croppingPlanEntry),
                getInterventionSpeciesName(croppingPlanEntry, intervention),
                getInterventionVarietyName(croppingPlanEntry, intervention),
                AgrosystI18nService.getEnumTraduction(locale, phase.getType()),
                AgrosystI18nService.getEnumTraduction(locale, intervention.getType()),
                intervention.getName(),
                getShortId(intervention),
                intervention.getStartingPeriodDate(),
                intervention.getEndingPeriodDate(),
                getActionsToString(actions),
                Double.toString(intervention.getSpatialFrequency()), // fréquence spatiale
                getInputUsagesToString(inputUsages, actions, croppingPlanEntry),
                getNbInputUsages(inputUsages),
                getGroupesCiblesToString(inputUsages, groupesCiblesByCode),
                getInputUsageTargetsToString(inputUsages),
                getBooleanAsString(isOneInputUsageBiocontrole(inputUsages, codeAmmBioControle))
        );
    }
    
    @Override
    public void writePracticedSeasonalCrop(String its,
                                           String irs,
                                           String campaigns,
                                           Double connexionFrequency,
                                           String indicatorCategory,
                                           String indicatorName,
                                           Object value,
                                           Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                           Integer reliabilityIndex,
                                           String comment,
                                           Integer rank,
                                           Domain domain,
                                           GrowingSystem growingSystem,
                                           PracticedSystem practicedSystem,
                                           CroppingPlanEntry croppingPlanEntry,
                                           CroppingPlanEntry previousPlanEntry,
                                           String culturePrecedentRangId,
                                           Class<? extends GenericIndicator> indicatorClass,
                                           PracticedCropCycleConnection practicedCropCycleConnection) {
    
        //"Domaine_Exploitation", "Nom du réseau de l'IT", "Nom du réseau de l'IR",
        //"Systèmes de culture", "Numéro DEPHY",
        //"Type de conduite du SdC", "Système synthétisé", "Culture (rang)", "Espèces", "Variétés", "Précédent/Phase", "% des cultures pérennes",
        //"Fréquence des couples C-P %",
    
        //"Année(s)", "Approche de calcul", "Catégorie d'indicateur", "Indicateur", "Valeur",  "Rendement", "Taux de complétion (en %)",
        // "Détails des champs non renseignés requis pour le calcul de l'indicateur"
    
        //String years, String indicatorCategory, String indicatorName, Double value, String printableYealdAverages, Integer reliabilityIndex, String comment,
        // String referenceDosages, String... columns
        writeToSheet(
                practicedCroppingPlanSheet,
                campaigns,
                indicatorCategory,
                indicatorName,
                value,
                GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(cropYealdAverage),
                reliabilityIndex,
                comment,
                null,
                connexionFrequency,
        
                // dynamic cells
                domain.getName(),
                its, irs,
                getGrowingSystemName((Optional.ofNullable(growingSystem))),
                geDephyNumber((Optional.ofNullable(growingSystem))),
                getTypeDeConduiteSDC((Optional.ofNullable(growingSystem))),
                practicedSystem.getName(),
                l(locale, CROP_NAME_RANK, croppingPlanEntry != null ? croppingPlanEntry.getName() : l(locale, "IndicatorWriter.undefined"), rank + 1),
                getCropSpeciesName(croppingPlanEntry),
                getCropVarietyName(croppingPlanEntry),
                previousPlanEntry != null ? previousPlanEntry.getName() : "", //  "Précédent/Phase"
                ""// "% des cultures pérennes" not applicable for seasonal
        );
    }
    
    @Override
    public void writePracticedPerennialCop(String its,
                                           String irs,
                                           String campaigns,
                                           String indicatorCategory,
                                           String indicatorName,
                                           Object value,
                                           Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                           Integer reliabilityIndex,
                                           String comment,
                                           Domain domain,
                                           GrowingSystem growingSystem,
                                           PracticedSystem practicedSystem,
                                           CroppingPlanEntry croppingPlanEntry,
                                           PracticedCropCyclePhase phase,
                                           Double perennialCropCyclePercent,
                                           Class<? extends GenericIndicator> indicatorClass) {
    
        //"Domaine_Exploitation", "Nom du réseau de l'IT", "Nom du réseau de l'IR",
        //"Systèmes de culture", "Numéro DEPHY",
        //"Type de conduite du SdC", "Système synthétisé", "Culture (rang)", "Espèces", "Variétés", "Précédent/Phase", "% des cultures pérennes",
        //"Fréquence des couples C-P %",
    
        //"Année(s)", "Approche de calcul", "Catégorie d'indicateur", "Indicateur", "Valeur",  "Rendement", "Taux de complétion (en %)",
        // "Détails des champs non renseignés requis pour le calcul de l'indicateur"
    
        //String years, String indicatorCategory, String indicatorName, Double value, String printableYealdAverages,
        // Integer reliabilityIndex, String comment, String referenceDosages, String... columns
        writeToSheet(practicedCroppingPlanSheet,
                campaigns,
                indicatorCategory,
                indicatorName,
                value,
                GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(cropYealdAverage),
                reliabilityIndex,
                comment,
                null,
                null,
                // dynamic cells
                domain.getName(),
                its, irs,
                getGrowingSystemName((Optional.ofNullable(growingSystem))),
                geDephyNumber((Optional.ofNullable(growingSystem))),
                getTypeDeConduiteSDC((Optional.ofNullable(growingSystem))),
                practicedSystem.getName(),
                croppingPlanEntry.getName(),
                getCropSpeciesName(croppingPlanEntry),
                getCropVarietyName(croppingPlanEntry),
                AgrosystI18nService.getEnumTraduction(locale, phase.getType()),
                perennialCropCyclePercent != null ? perennialCropCyclePercent.toString() : l(locale, "undefined")// "% des cultures pérennes"
        );
    }
    
    @Override
    public void writePracticedSystem(String its,
                                     String irs,
                                     String campaigns,
                                     String indicatorCategory,
                                     String indicatorName,
                                     Object value,
                                     Integer reliabilityIndex,
                                     String comment,
                                     Domain domain,
                                     GrowingSystem growingSystem,
                                     PracticedSystem practicedSystem,
                                     Class<? extends GenericIndicator> indicatorClass) {
        // Order change since #10412
        //"Domaine_Exploitation", "Nom du réseau de l'IT", "Nom du réseau de l'IR", "Departement", "Systèmes de culture",
        //   "Numéro DEPHY", "Type de conduite du SdC", "SDC validé",
        //"Système synthétisé", "Synthétisé validé",
        //"Année(s)", "Approche de calcul", "Catégorie d'indicateur", "Indicateur", "Valeur", "Taux de complétion (en %)",
        //   "Détails des champs non renseignés requis pour le calcul de l'indicateur"
    
        //String years, String indicatorCategory, String indicatorName, Double value, String printableYealdAverages, Integer reliabilityIndex,
        //   String comment, String referenceDosages, Double practicedCroppingPlanSheetConnexionFrequency, String... columns
        writeToSheet(
                practicedSystemSheet,
                campaigns,
                indicatorCategory,
                indicatorName,
                value,
                null,
                reliabilityIndex,
                comment,
                null,
                null,
                // dynamic cells
                domain.getName(),
                its, irs,
                domain.getLocation().getDepartement(),
                getGrowingSystemName((Optional.ofNullable(growingSystem))),
                geDephyNumber((Optional.ofNullable(growingSystem))),
                getTypeDeConduiteSDC((Optional.ofNullable(growingSystem))),
                getApprovedGrowingSystem((Optional.ofNullable(growingSystem))),
                practicedSystem.getName(),
                getBooleanAsString(practicedSystem.isValidated())
        );
    }
    
    @Override
    public void writePracticedDomain(String campaigns,
                                     String indicatorCategory,
                                     String indicatorName,
                                     Object value,
                                     Integer reliabilityIndex,
                                     String comment,
                                     Domain domain,
                                     String growingSystemsTypeAgricultureLabels) {
        //"Domaine_Exploitation", "Département", "Types de conduite des SdC",
        //"Année(s)", "Approche de calcul", "Catégorie d'indicateur", "Indicateur", "Valeur", "Taux de complétion (en %)",
        //   "Détails des champs non renseignés requis pour le calcul de l'indicateur"
    
        //String years, String indicatorCategory, String indicatorName, Double value, String printableYealdAverages, Integer reliabilityIndex, String comment,
        // String referenceDosages, Double practicedCroppingPlanSheetConnexionFrequency, String... columns
        writeToSheet(domainSheet, campaigns, indicatorCategory, indicatorName, value, null, reliabilityIndex, comment, null,
                null, domain.getName(), domain.getLocation().getDepartement(), growingSystemsTypeAgricultureLabels);
    }

    @Override
    public void writePrices(
            RefCampaignsInputPricesByDomainInput domainInputPriceRefPrices,
            List<HarvestingPrice> harvestingPrices,
            Map<String, HarvestingValorisationPriceSummary> refHarvestingPricesSummaries,
            List<Equipment> equipments,
            Domain domain,
            PracticedSystem practicedSystem,
            RefScenariosInputPricesByDomainInput refScenariosInputPricesByDomainInput,
            MultiKeyMap<String, List<RefHarvestingPrice>> refHarvestingPricesByCodeEspeceAndCodeDestination,
            List<CroppingPlanSpecies> croppingPlanSpecies) {

        final Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> allDomainRefPrices = new HashMap<>();
        allDomainRefPrices.putAll(domainInputPriceRefPrices.mineralRefPriceForInput());
        allDomainRefPrices.putAll(domainInputPriceRefPrices.speciesRefPriceForInput());
        allDomainRefPrices.putAll(domainInputPriceRefPrices.fertiOrgaRefPriceForInput());
        allDomainRefPrices.putAll(domainInputPriceRefPrices.phytoRefPriceForInput());
        allDomainRefPrices.putAll(domainInputPriceRefPrices.irrigRefPriceForInput());
        allDomainRefPrices.putAll(domainInputPriceRefPrices.fuelRefPriceForInput());
        allDomainRefPrices.putAll(domainInputPriceRefPrices.otherRefPriceForInput());
        allDomainRefPrices.putAll(domainInputPriceRefPrices.potRefPriceForInput());
        allDomainRefPrices.putAll(domainInputPriceRefPrices.substrateRefPriceForInput());

        final Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> allScenariosRefPrices = new HashMap<>();
        allScenariosRefPrices.putAll(refScenariosInputPricesByDomainInput.mineralRefPriceByScenarioForInput());
        allScenariosRefPrices.putAll(refScenariosInputPricesByDomainInput.speciesRefPriceByScenarioForInput());
        allScenariosRefPrices.putAll(refScenariosInputPricesByDomainInput.fertiOrgaRefPriceByScenarioForInput());
        allScenariosRefPrices.putAll(refScenariosInputPricesByDomainInput.phytoRefPriceByScenarioForInput());
        allScenariosRefPrices.putAll(refScenariosInputPricesByDomainInput.irrigRefPriceByScenarioForInput());
        allScenariosRefPrices.putAll(refScenariosInputPricesByDomainInput.fuelRefPriceByScenarioForInput());
        allScenariosRefPrices.putAll(refScenariosInputPricesByDomainInput.otherRefPriceByScenarioForInput());
        allScenariosRefPrices.putAll(refScenariosInputPricesByDomainInput.potRefPriceByScenarioForInput());
        allScenariosRefPrices.putAll(refScenariosInputPricesByDomainInput.substrateRefPriceByScenarioForInput());

        for (Map.Entry<AbstractDomainInputStockUnit, Optional<InputRefPrice>> domainInputOptionalRefPrice : allDomainRefPrices.entrySet()) {
            writeDomainInputPriceRow(domainInputOptionalRefPrice, domain, practicedSystem, allScenariosRefPrices);
        }

        if (CollectionUtils.isNotEmpty(harvestingPrices)) {
            for (HarvestingPrice harvestingPrice : harvestingPrices) {
                String valorisationId = harvestingPrice.getHarvestingActionValorisation().getTopiaId();
                HarvestingValorisationPriceSummary harvestingValorisationPriceSummary = refHarvestingPricesSummaries.get(valorisationId);

                Optional<CroppingPlanSpecies> maybeCps = Optional.empty();
                if (croppingPlanSpecies != null) {
                    maybeCps = croppingPlanSpecies
                            .stream()
                            .filter(cps -> cps.getCode().equals(harvestingPrice.getHarvestingActionValorisation().getSpeciesCode()))
                            .findFirst();
                }

                String codeEspece;
                String codeDestination;
                String codeQualifiantAee;
                if (maybeCps.isPresent()) {
                    CroppingPlanSpecies cps = maybeCps.get();
                    // si trouvé alors code_espece_botanique, code_qualifiant_aee ne doit pas être null
                    codeEspece = cps.getSpecies().getCode_espece_botanique();
                    codeQualifiantAee = cps.getSpecies().getCode_qualifiant_AEE();
                    // codeDestination ne peut être null à cet endroit
                    codeDestination = harvestingPrice.getHarvestingActionValorisation().getDestination().getCode_destination_A();

                } else {
                    codeEspece = null;
                    codeDestination = null;
                    codeQualifiantAee = null;
                }

                List<RefHarvestingPrice> refHarvestingPricesForScenarios = ListUtils.emptyIfNull(refHarvestingPricesByCodeEspeceAndCodeDestination.get(codeEspece, codeDestination));

                refHarvestingPricesForScenarios = refHarvestingPricesForScenarios
                        .stream()
                        .filter(rhp -> rhp.getCode_qualifiant_AEE().equals(codeQualifiantAee))
                        .toList();


                writeHarvestingPriceRow(harvestingPrice, harvestingValorisationPriceSummary, domain, practicedSystem, refHarvestingPricesForScenarios);
            }
        }

        MultiValuedMap<RefMateriel, Equipment> equipmentsByMateriel = new ArrayListValuedHashMap<>();
        for (Equipment equipment : equipments) {
            equipmentsByMateriel.put(equipment.getRefMateriel(), equipment);
        }
        equipmentsByMateriel.keySet().stream()
                .filter(Objects::nonNull)
                .sorted(Comparator.comparing(materiel -> materiel.getClass().getName()
                        + equipmentsByMateriel.get(materiel).stream()
                        .map(Equipment::getName)
                        .filter(Objects::nonNull)
                        .min(Comparator.naturalOrder())
                        .orElse("")))
                .forEachOrdered(materiel -> writeEquipmentRow(materiel, equipmentsByMateriel.get(materiel), practicedSystem));

    }

    @Override
    public void writeEffectiveSeasonalIntervention(
            String its,
            String irs,
            int campaign,
            String indicatorCategory,
            String indicatorName,
            Object value,
            Integer reliabilityIndex,
            String reliabilityComment,
            List<String> referenceDosages,
            Domain domain,
            Optional<GrowingSystem> optionalGrowingSystem,
            Plot plot,
            Zone zone,
            CroppingPlanEntry croppingPlanEntry,
            CroppingPlanEntry intermediateCrop,
            int rank,
            CroppingPlanEntry previousPlanEntry,
            EffectiveIntervention intervention,
            Collection<AbstractAction> actions,
            Collection<String> codeAmmBioControle,
            Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverages,
            Map<String, String> groupesCiblesByCode,
            Class<? extends GenericIndicator> indicatorClass) {

        CroppingPlanEntry interCrop = intervention.isIntermediateCrop() ? intermediateCrop : croppingPlanEntry;
    
        Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(actions, domain);
    
        //"Domaine_Exploitation",
        //"Nom du réseau de l'IT", "Nom du réseau de l'IR", "Systèmes de culture", "Numéro DEPHY", "Type de conduite du SdC",
        //"Parcelle", "Zone", "Surface de la zone (en ha)",
        //"Culture", "Affection CI", "Espèces", "Variétés", "Espèces concernées par l'intervention", "Varietes concernées par l'intervention",
        //"Précédent/Phase", "Type d'intervention", "Intervention", "Identifiant de l'intervention", "Date de début",
        // "Date de fin", "Action(s)", "Intrant(s)", "Cible(s) de traitement",
        //"Nom du réseau de l'IT", "Nom du réseau de l'IR", "Année(s)", "Approche de calcul",
        //"Catégorie d'indicateur", "Indicateur", "Valeur", "Taux de complétion (en %)",
        //"Détails des champs non renseignés requis pour le calcul de l'indicateur",
        //"Doses de référence utilisées pour le calcul IFT culture"
        writeToSheet(interventionSheet,
                String.valueOf(campaign),
                indicatorCategory,
                indicatorName,
                value,
                "", reliabilityIndex, reliabilityComment,
                referenceDosages == null ? "" : String.join(", ", referenceDosages),
                // dynamic cells
                null, getDomainName(domain),
                its, irs,
                getGrowingSystemName(optionalGrowingSystem),
                geDephyNumber(optionalGrowingSystem),
                getTypeDeConduiteSDC(optionalGrowingSystem),
                plot.getName(),
                zone.getName(),
                String.valueOf(zone.getArea()),
                l(locale, CROP_NAME_RANK, croppingPlanEntry.getName(), rank + 1),
                intermediateCrop != null ? intermediateCrop.getName() : "",
                getCropSpeciesName(interCrop),
                getCropVarietyName(interCrop),
                getInterventionSpeciesName(intervention),
                getInterventionVarietyName(intervention),
                previousPlanEntry != null ? previousPlanEntry.getName() : l(locale, NO_PREVIOUS_CROP),
                AgrosystI18nService.getEnumTraduction(locale, intervention.getType()),
                intervention.getName(),
                getShortId(intervention),
                INTERVENTION_DATE_FORMAT_DDMMYYYY.format(intervention.getStartInterventionDate()),
                INTERVENTION_DATE_FORMAT_DDMMYYYY.format(intervention.getEndInterventionDate()),
                getActionsToString(actions),
                Double.toString(intervention.getSpatialFrequency()), // fréquence spatiale
                getInputUsagesToString(inputUsages, actions, interCrop),
                getNbInputUsages(inputUsages),
                getGroupesCiblesToString(inputUsages, groupesCiblesByCode),
                getInputUsageTargetsToString(inputUsages),
                getBooleanAsString(isOneInputUsageBiocontrole(inputUsages, codeAmmBioControle))
        );
    }
    
    @Override
    public void writeEffectivePerennialIntervention(
            String its,
            String irs,
            String indicatorCategory,
            String indicatorName,
            Object value,
            Integer reliabilityIndex,
            String comment,
            List<String> referenceDosages,
            Domain domain,
            Optional<GrowingSystem> optionalGrowingSystem,
            Plot plot,
            Zone zone,
            CroppingPlanEntry croppingPlanEntry,
            EffectiveCropCyclePhase phase,
            EffectiveIntervention intervention,
            Collection<AbstractAction> actions,
            Collection<String> codeAmmBioControle,
            Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage,
            Map<String, String> groupesCiblesByCode,
            Class<? extends GenericIndicator> indicatorClass) {
        
        Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(actions, domain);
        writeToSheet(
                interventionSheet,
                String.valueOf(domain.getCampaign()),
                indicatorCategory,
                indicatorName,
                value,
                "", // not display for intervention
                reliabilityIndex, comment,
                referenceDosages == null ? "" : String.join(", ", referenceDosages),
            
                // dynamic cells:
                null, getDomainName(domain),
                its, irs,
                getGrowingSystemName(optionalGrowingSystem),
                geDephyNumber(optionalGrowingSystem),
                getTypeDeConduiteSDC(optionalGrowingSystem),
                plot.getName(),
                zone.getName(),
                String.valueOf(zone.getArea()),
                croppingPlanEntry.getName(),
                "",// "Affection CI"
                getCropSpeciesName(croppingPlanEntry),
                getCropVarietyName(croppingPlanEntry),
                getInterventionSpeciesName(intervention),
                getInterventionVarietyName(intervention),
                AgrosystI18nService.getEnumTraduction(locale, phase.getType()),
                AgrosystI18nService.getEnumTraduction(locale, intervention.getType()),
                intervention.getName(),
                getShortId(intervention),
                INTERVENTION_DATE_FORMAT_DDMMYYYY.format(intervention.getStartInterventionDate()),
                INTERVENTION_DATE_FORMAT_DDMMYYYY.format(intervention.getEndInterventionDate()),
                getActionsToString(actions),
                Double.toString(intervention.getSpatialFrequency()), // fréquence spatiale
                getInputUsagesToString(inputUsages, actions, croppingPlanEntry),
                getNbInputUsages(inputUsages),
                getGroupesCiblesToString(inputUsages, groupesCiblesByCode),
                getInputUsageTargetsToString(inputUsages),
                getBooleanAsString(isOneInputUsageBiocontrole(inputUsages, codeAmmBioControle))
        );
    }
    
    @Override
    public void writeEffectiveSeasonalCrop(String its,
                                           String irs,
                                           int campaign,
                                           String indicatorCategory,
                                           String indicatorName,
                                           Object value,
                                           Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                           Integer reliabilityIndex,
                                           String comment,
                                           Domain domain,
                                           Optional<GrowingSystem> optionalGrowingSystem,
                                           Plot plot,
                                           CroppingPlanEntry croppingPlanEntry,
                                           Integer rank,
                                           CroppingPlanEntry previousPlanEntry,
                                           Class<? extends GenericIndicator> indicatorClass) {
    
        // "Domaine_Exploitation", "Nom du réseau de l'IT", "Nom du réseau de l'IR", "Systèmes de culture", "Numéro DEPHY", "Type de conduite du SdC",
        // "Parcelle", "Culture (rang)", "Espèces", "Variétés", "Précédent/Phase",
        //  "Année(s)", "Approche de calcul", "Catégorie d'indicateur", "Indicateur", "Valeur",  "Rendement", "Taux de complétion (en %)",
        //  "Détails des champs non renseignés requis pour le calcul de l'indicateur"
    
        writeToSheet(
                effectiveCroppingPlanSheet,
                String.valueOf(campaign),
                indicatorCategory,
                indicatorName,
                value,
                GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(cropYealdAverage),
                reliabilityIndex,
                comment, null,
                // dynamic cells
                null, domain.getName(),
                its, irs,
                getGrowingSystemName(optionalGrowingSystem),
                geDephyNumber(optionalGrowingSystem),
                getTypeDeConduiteSDC(optionalGrowingSystem),
                plot.getName(),
                l(locale, CROP_NAME_RANK, croppingPlanEntry.getName(), rank + 1),
                getCropSpeciesName(croppingPlanEntry),
                getCropVarietyName(croppingPlanEntry),
                previousPlanEntry != null ? previousPlanEntry.getName() : l(locale, NO_PREVIOUS_CROP));
    }
    
    @Override
    public void writeEffectivePerennialCrop(String its,
                                            String irs,
                                            int campaign,
                                            String indicatorCategory,
                                            String indicatorName,
                                            Object value,
                                            Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverages,
                                            Integer reliabilityIndex,
                                            String comment,
                                            Domain domain,
                                            Optional<GrowingSystem> optionalGrowingSystem,
                                            Plot plot,
                                            CroppingPlanEntry croppingPlanEntry,
                                            EffectiveCropCyclePhase phase,
                                            Class<? extends GenericIndicator> indicatorClass) {
    
        // "Domaine_Exploitation", "Nom du réseau de l'IT", "Nom du réseau de l'IR", "Systèmes de culture", "Numéro DEPHY", "Type de conduite du SdC",
        // "Parcelle", "Culture (rang)", "Espèces", "Variétés", "Précédent/Phase",
        //  "Année(s)", "Approche de calcul", "Catégorie d'indicateur", "Indicateur", "Valeur",  "Rendement", "Taux de complétion (en %)",
        //  "Détails des champs non renseignés requis pour le calcul de l'indicateur"
    
        writeToSheet(
                effectiveCroppingPlanSheet,
                String.valueOf(campaign),
                indicatorCategory,
                indicatorName,
                value,
                GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(cropYealdAverages),
                reliabilityIndex,
                comment, null,
                // dynamic cells
                null, domain.getName(),
                its, irs,
                getGrowingSystemName(optionalGrowingSystem),
                geDephyNumber(optionalGrowingSystem),
                getTypeDeConduiteSDC(optionalGrowingSystem),
                plot.getName(),
                croppingPlanEntry.getName(),
                getCropSpeciesName(croppingPlanEntry),
                getCropVarietyName(croppingPlanEntry),
                AgrosystI18nService.getEnumTraduction(locale, phase.getType()));
    }
    
    @Override
    public void writeEffectiveZone(String its,
                                   String irs,
                                   String indicatorCategory,
                                   String indicatorName,
                                   Object value,
                                   Map<Pair<RefDestination, YealdUnit>, Double> zoneAverageYeald,
                                   Integer reliabilityIndex,
                                   String comment,
                                   Domain anonymiseDomain,
                                   List<CroppingPlanSpecies> domainCroppingPlanSpecies,
                                   Optional<GrowingSystem> optionalGrowingSystem,
                                   Plot plot,
                                   Zone zone,
                                   String speciesNames,
                                   String varietyNames,
                                   Class<? extends GenericIndicator> indicatorClass) {
    
        //"Domaine_Exploitation", "Nom du réseau de l'IT", "Nom du réseau de l'IR", "Systèmes de culture", "Numéro DEPHY",
        // "Type de conduite du SdC", "Parcelle", "Zone", "Espèces", "Variétés",
        //"Année(s)", "Approche de calcul", "Catégorie d'indicateur", "Indicateur", "Valeur",  "Rendement", "Taux de complétion (en %)",
        // "Détails des champs non renseignés requis pour le calcul de l'indicateur"
        writeToSheet(
                zoneSheet,
                String.valueOf(anonymiseDomain.getCampaign()),
                indicatorCategory,
                indicatorName,
                value,
                GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(zoneAverageYeald),
                reliabilityIndex,
                comment,
                null,
                // dynamic cells
                null,
                anonymiseDomain.getName(),
                its,
                irs,
                getGrowingSystemName(optionalGrowingSystem),
                geDephyNumber(optionalGrowingSystem),
                getTypeDeConduiteSDC(optionalGrowingSystem),
                plot.getName(),
                zone.getName(),
                speciesNames,
                varietyNames);
    }
    
    @Override
    public void writeEffectivePlot(String its,
                                   String irs,
                                   String indicatorCategory,
                                   String indicatorName,
                                   Object value,
                                   Integer reliabilityIndex,
                                   String comment,
                                   Domain domain,
                                   Collection<CroppingPlanSpecies> domainCroppingPlanSpecies,
                                   Optional<GrowingSystem> optionalGrowingSystem,
                                   Plot plot,
                                   Map<Pair<RefDestination, YealdUnit>, Double> plotYealdAverage,
                                   Class<? extends GenericIndicator> indicatorClass,
                                   String zoneIds) {
        //"Domaine_Exploitation", "Nom du réseau de l'IT", "Nom du réseau de l'IR", "Departement", "Systèmes de culture", "Numéro DEPHY",
        // "Type de conduite du SdC", "Parcelle",
        //"Année(s)", "Approche de calcul", "Catégorie d'indicateur", "Indicateur", "Valeur", "Rendement", "Taux de complétion (en %)",
        // "Détails des champs non renseignés requis pour le calcul de l'indicateur"
        writeToSheet(
                plotSystemSheet,
                String.valueOf(domain.getCampaign()),
                indicatorCategory,
                indicatorName,
                value,
                GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(plotYealdAverage),
                reliabilityIndex,
                comment, null,
                // dynamic cells
                null,
                domain.getName(),
                its, irs,
                domain.getLocation().getDepartement(),
                getGrowingSystemName(optionalGrowingSystem),
                geDephyNumber(optionalGrowingSystem),
                getTypeDeConduiteSDC(optionalGrowingSystem),
                plot.getName());
    }
    
    @Override
    public void writeEffectiveGrowingSystem(String its,
                                            String irs,
                                            String indicatorCategory,
                                            String indicatorName,
                                            Object value,
                                            Integer reliabilityIndex,
                                            String comment,
                                            Domain domain,
                                            Optional<GrowingSystem> optionalGrowingSystem,
                                            Class<? extends GenericIndicator> indicatorClass,
                                            Collection<CroppingPlanSpecies> agrosystSpecies) {
        //"Domaine_Exploitation",  "Nom du réseau de l'IT", "Nom du réseau de l'IR", "Departement", "Systèmes de culture",
        // "Numéro DEPHY", "Type de conduite du SdC", "SDC validé",
        //"Année(s)", "Approche de calcul", "Catégorie d'indicateur", "Indicateur", "Valeur", "Taux de complétion (en %)",
        // "Détails des champs non renseignés requis pour le calcul de l'indicateur"
        final String campaign = String.valueOf(domain.getCampaign());
        final String departement = domain.getLocation().getDepartement();
    
        writeToSheet(
                growingSystemSheet,
                campaign,
                indicatorCategory,
                indicatorName,
                value,
                "",
                reliabilityIndex,
                comment,
                null,
                // dinamic cells
                null, domain.getName(),
                its,
                irs,
                departement,
                getGrowingSystemName(optionalGrowingSystem),
                geDephyNumber(optionalGrowingSystem),
                getTypeDeConduiteSDC(optionalGrowingSystem),
                getApprovedGrowingSystem(optionalGrowingSystem)
        );
    }
    
    @Override
    public void writeEffectiveDomain(String indicatorCategory,
                                     String indicatorName,
                                     Object value,
                                     Integer reliabilityIndex,
                                     String comment,
                                     Domain domain,
                                     String growingSystemsTypeAgricultureLabels) {
        //"Domaine_Exploitation", Departement", "Types de conduite des SdC",
        //"Année(s)", "Approche de calcul", "Catégorie d'indicateur", "Indicateur", "Valeur", "Taux de complétion (en %)",
        // "Détails des champs non renseignés requis pour le calcul de l'indicateur"
        writeToSheet(
                domainSheet,
                String.valueOf(domain.getCampaign()),
                indicatorCategory,
                indicatorName,
                value,
                "",
                reliabilityIndex,
                comment,
                null,
                null,
                domain.getName(),
                domain.getLocation().getDepartement(),
                growingSystemsTypeAgricultureLabels);
    }

    @Override
    public void writeEffectiveItk(
            String its,
            String irs,
            String indicatorCategory,
            String indicatorLabel,
            Object itkValue,
            Integer effectiveItkCropPrevCropPhaseReliabilityIndex,
            String effectiveItkCropPrevCropPhaseComments,
            Domain anonymiseDomain,
            Optional<GrowingSystem> optionalAnoGrowingSystem,
            Plot plot,
            Zone anonymizeZone,
            Class<? extends GenericIndicator> aClass,
            EffectiveItkCropCycleScaleKey key) {
        // TODO
    }
    
    // columns: colonnes spécifiques à chaque feuille
    protected void writeToSheet(Sheet sheet,
                                String years,
                                String indicatorCategory,
                                String indicatorName,
                                Object value,
                                String printableYealdAverages,
                                Integer reliabilityIndex,
                                String comment,
                                String iftReferenceDosages,
                                Double practicedCroppingPlanSheetConnexionFrequency,
                                String... columns) {
        
        // new line
        int line = sheet.getPhysicalNumberOfRows();
        if (line > MAXIMUM_NOMBER_OF_ROWS) {
            return;
        }
        Row row = sheet.createRow(line);
        
        int column = 0;
        
        Cell cell;
        // dynamic cell
        for (String dynColumn : columns) {
            cell = row.createCell(column++);
            cell.setCellValue(dynColumn);
        }
        
        // "Fréquence des couples C-P %" uniquement sur le synthétisé
        if (sheet == practicedCroppingPlanSheet) {
            cell = row.createCell(column++);
            if (practicedCroppingPlanSheetConnexionFrequency != null) {
                // only seasonal
                cell.setCellValue(practicedCroppingPlanSheetConnexionFrequency);
            } else {
                cell.setCellValue("");
            }
        }
        
        // "Année(s)"
        cell = row.createCell(column++);
        cell.setCellValue(years);
        
        
        // "Approche de calcul"
        cell = row.createCell(column++);
        String practicedOrEffective = l(locale, performance.isPracticed() ? "IndicatorWriter.practiced" : "IndicatorWriter.effective");
        cell.setCellValue(practicedOrEffective);
        
        // "Catégorie d'indicateur"
        cell = row.createCell(column++);
        cell.setCellValue(l(locale, indicatorCategory));
        
        // "Indicateur"
        cell = row.createCell(column++);
        cell.setCellValue(indicatorName);
    
    
        // "Valeur"
        cell = row.createCell(column++);
        if (value == null) {
            cell.setCellValue("");
        } else {
            if (value instanceof Double) {
                cell.setCellValue(Double.parseDouble(decimalFormat.format(value)));
            } else {
                cell.setCellValue((String) value);
            }
        }
    
        if (sheet == effectiveCroppingPlanSheet ||
                sheet == practicedCroppingPlanSheet ||
                sheet == zoneSheet ||
                sheet == plotSystemSheet) {
            // "Rendement"
            cell = row.createCell(column++);
            cell.setCellValue(printableYealdAverages);
        }
    
        // "Taux de complétion (en %)"
        cell = row.createCell(column++);
        cell.setCellValue(reliabilityIndex);
    
        // "Détails des champs non renseignés requis pour le calcul de l'indicateur"
        cell = row.createCell(column++);
        cell.setCellValue(comment);
        
        // "Doses de référence utilisées pour le calcul IFT culture"
        if (iftReferenceDosages != null) {
            cell = row.createCell(column);
            cell.setCellValue(iftReferenceDosages);
        }
    }

    private void writeHarvestingPriceRow(
            HarvestingPrice harvestingPrice,
            HarvestingValorisationPriceSummary harvestingValorisationPriceSummary,
            Domain domain,
            PracticedSystem practicedSystem,
            List<RefHarvestingPrice> refHarvestingPricesForScenarios) {
        int line = pricesSheet.getPhysicalNumberOfRows();
        Row row = pricesSheet.createRow(line);

        int column = 0;

        // Domaine
        Cell cell = row.createCell(column++);
        if (domain != null) {
            cell.setCellValue(domain.getName());
        }
        if (practicedSystem != null) {
            // Système synthétisé
            cell = row.createCell(column++);
            cell.setCellValue(practicedSystem.getName());
        }
        // Campagne
        cell = row.createCell(column++);

        if (practicedSystem == null && domain != null) {
            cell.setCellValue(domain.getCampaign());
        } else if (practicedSystem != null) {
            cell.setCellValue(practicedSystem.getCampaigns());
        }

        // Type
        String type = AgrosystI18nService.getEnumTraduction(locale, InputPriceCategory.HARVESTING_ACTION);
        row.createCell(column++).setCellValue(type);
        // Nom
        String libelle;
        final String startingFormattingPattern = "<span class=\"warning-label\"><i class=\"fa fa-warning\" aria-hidden=\"true\"></i>";
        final String endingFormattingPattern = "</span>";
        libelle = Strings.nullToEmpty(harvestingPrice.getDisplayName());
        libelle = StringUtils.replace(libelle, startingFormattingPattern, "/!\\ ");
        libelle = StringUtils.replace(libelle, endingFormattingPattern, "");
        row.createCell(column++).setCellValue(libelle);
        // Complément information
        String information = Optional.of(harvestingPrice)
                .map(HarvestingPrice::getHarvestingActionValorisation)
                .map(HarvestingActionValorisation::getDestination)
                .map(RefDestination::getDestination)
                .orElse(null);

        cell = row.createCell(column++);
        if (information != null) {
            cell.setCellValue(information);
        }
        // Durée de vie théorique des matériels (années)
        row.createCell(column++);
        // Investissement matériels (euro)
        row.createCell(column++);

        Double refInputPriceValue = null;
        PriceUnit refInputPriceUnit = null;
        String refInputPriceSource = null;

        Boolean isOrganic = null;

        final HarvestingActionValorisation harvestingActionValorisation = harvestingPrice.getHarvestingActionValorisation();
        if (harvestingActionValorisation != null) {
            if (harvestingValorisationPriceSummary != null) {
                refInputPriceValue = harvestingValorisationPriceSummary.getAveragePrice();
                if (refInputPriceValue != null) {
                    isOrganic = harvestingActionValorisation.isIsOrganicCrop();
                    refInputPriceUnit = harvestingValorisationPriceSummary.getPriceUnit();
                    refInputPriceSource = harvestingValorisationPriceSummary.getSources().entrySet().stream()
                            .sorted(Map.Entry.comparingByKey())
                            .map(Map.Entry::getValue)
                            .collect(Collectors.joining(", "));
                }
            }
        }

        // Prix utilisés pour calculs en réel
        cell = row.createCell(column++);
        Double priceValue = harvestingPrice.getPrice();
        Double value;
        if (priceValue != null) {
            value = priceValue;
        } else {
            value = refInputPriceValue;
        }
        if (value != null) {
            cell.setCellValue(Precision.round(value, 2, RoundingMode.HALF_UP.ordinal()));
        } else {
            cell.setCellValue(ExportUtils.NO_VALUE);
        }
        // Unité
        String priceUnit;
        if (harvestingPrice.getPrice() != null && harvestingPrice.getPriceUnit() != null) {
            priceUnit = AgrosystI18nService.getEnumTraduction(locale, harvestingPrice.getPriceUnit());
        } else if (refInputPriceUnit != null) {
            priceUnit = AgrosystI18nService.getEnumTraduction(locale, refInputPriceUnit);
        } else {
            priceUnit = ExportUtils.NO_VALUE;
        }
        row.createCell(column++).setCellValue(priceUnit);
        // Prix pour mode de calcul Standardisé - Millésimé
        cell = row.createCell(column++);
        if (refInputPriceValue != null) {
            cell.setCellValue(Precision.round(refInputPriceValue, 2, RoundingMode.HALF_UP.ordinal()));
        } else {
            cell.setCellValue(ExportUtils.NO_VALUE);
        }
        // Unité
        String refPriceUnit = refInputPriceUnit != null ? AgrosystI18nService.getEnumTraduction(locale, refInputPriceUnit) : ExportUtils.NO_VALUE;
        row.createCell(column++).setCellValue(refPriceUnit);
        // Prix AB
        String isOrganicResult = null;
        if (isOrganic != null) {
            isOrganicResult = getBooleanAsString(isOrganic);
        }
        row.createCell(column++).setCellValue(Objects.toString(isOrganicResult, ExportUtils.NO_VALUE));
        // source
        row.createCell(column++).setCellValue(Objects.toString(refInputPriceSource, ExportUtils.NULL_SOURCE_PLACEHOLDER));

        final Set<String> availableScenarios = refHarvestingPricesForScenarios.stream().map(RefHarvestingPrice::getCode_scenario).collect(Collectors.toSet());

        for (String codeScenario : ALL_CODES_SCENARIOS) {
            cell = row.createCell(column++);

            final Optional<RefHarvestingPrice> refHarvestingPrice = refHarvestingPricesForScenarios
                    .stream()
                    .filter(rhp -> rhp.getCode_scenario().equals(codeScenario))
                    .findFirst();

            if (refHarvestingPrice.isEmpty()) {
                // laisser la case vide quand le scénario n'est pas pris en compte dans la simulation,
                if (availableScenarios.contains(codeScenario)) {
                    cell.setCellValue(ExportUtils.NO_VALUE);
                }

            } else {
                cell.setCellValue(Precision.round(refHarvestingPrice.get().getPrice(), 2, RoundingMode.HALF_UP.ordinal()));
            }
        }
    }

    private void writeDomainInputPriceRow(
            Map.Entry<AbstractDomainInputStockUnit, Optional<InputRefPrice>> domainInputOptionalRefPrice,
            Domain domain,
            PracticedSystem practicedSystem,
            Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> scenariosInputsPrices) {

        AbstractDomainInputStockUnit domainInput = domainInputOptionalRefPrice.getKey();
        InputPrice inputPrice = domainInput.getInputPrice();
        if (inputPrice == null) {
            return;
        }

        // new line
        int line = pricesSheet.getPhysicalNumberOfRows();
        Row row = pricesSheet.createRow(line);

        int column = 0;

        // Domaine
        Cell cell = row.createCell(column++);
        if (domain != null) {
            cell.setCellValue(domain.getName());
        }
        if (practicedSystem != null) {
            // Système synthétisé
            cell = row.createCell(column++);
            cell.setCellValue(practicedSystem.getName());
        }
        // Campagne
        cell = row.createCell(column++);

        if (practicedSystem == null && domain != null) {
            cell.setCellValue(domain.getCampaign());
        } else if (practicedSystem != null) {
            cell.setCellValue(practicedSystem.getCampaigns());
        }

        Optional<InputRefPrice> optionalRefPrice = domainInputOptionalRefPrice.getValue();

        InputType inputType = domainInput.getInputType();

        // Type
        InputPriceCategory category = InputPriceService.INPUT_TYPE_TO_PRICE_TYPE.apply(inputType);
        String type = AgrosystI18nService.getEnumTraduction(locale, category);
        row.createCell(column++).setCellValue(type);
        // Nom
        String libelle;
        final String startingFormattingPattern = "<span class=\"warning-label\"><i class=\"fa fa-warning\" aria-hidden=\"true\"></i>";
        final String endingFormattingPattern = "</span>";
        libelle = Strings.nullToEmpty(StringUtils.firstNonBlank(inputPrice.getDisplayName(), domainInput.getInputName()));
        libelle = StringUtils.replace(libelle, startingFormattingPattern, "/!\\ ");
        libelle = StringUtils.replace(libelle, endingFormattingPattern, "");
        row.createCell(column++).setCellValue(libelle);
        // information complémentaire
        row.createCell(column++);
        // Durée de vie théorique des matériels (années)
        row.createCell(column++);
        // Investissement matériels (euro)
        row.createCell(column++);

        Double refInputPriceValue = null;
        PriceUnit refInputPriceUnit = null;
        String refInputPriceSource = null;

        if (optionalRefPrice.isPresent()) {
            InputRefPrice refInputPrice = optionalRefPrice.get();
            refInputPriceValue = refInputPrice.averageRefPrice().value();
            refInputPriceUnit =  refInputPrice.averageRefPrice().unit();
        }

        // Prix utilisés pour calculs en réel
        cell = row.createCell(column++);
        Double priceValue = inputPrice.getPrice();
        Double value;
        if (priceValue != null) {
            value = priceValue;
        } else {
            value = refInputPriceValue;
        }
        if (value != null) {
            cell.setCellValue(Precision.round(value, 2, RoundingMode.HALF_UP.ordinal()));
        } else {
            cell.setCellValue(ExportUtils.NO_VALUE);
        }
        // Unité
        String priceUnit;
        if (inputPrice.getPrice() != null && inputPrice.getPriceUnit() != null) {
            priceUnit = AgrosystI18nService.getEnumTraduction(locale, inputPrice.getPriceUnit());
        } else if (refInputPriceUnit != null) {
            priceUnit = AgrosystI18nService.getEnumTraduction(locale, refInputPriceUnit);
        } else {
            priceUnit = ExportUtils.NO_VALUE;
        }
        row.createCell(column++).setCellValue(priceUnit);
        // Prix pour mode de calcul Standardisé - Millésimé
        cell = row.createCell(column++);
        if (refInputPriceValue != null) {
            cell.setCellValue(Precision.round(refInputPriceValue, 2, RoundingMode.HALF_UP.ordinal()));
        } else {
            cell.setCellValue(ExportUtils.NO_VALUE);
        }
        // Unité
        String refPriceUnit = refInputPriceUnit != null ? AgrosystI18nService.getEnumTraduction(locale, refInputPriceUnit) : ExportUtils.NO_VALUE;
        row.createCell(column++).setCellValue(refPriceUnit);
        // organic
        row.createCell(column++).setCellValue(ExportUtils.NO_VALUE);
        // source
        row.createCell(column++).setCellValue(Objects.toString(refInputPriceSource, ExportUtils.NULL_SOURCE_PLACEHOLDER));

        // ajouter les prix pour les scénarios mobilisés.
        final Map<String, Optional<InputRefPrice>> scenariosPrices = scenariosInputsPrices.get(domainInputOptionalRefPrice.getKey());

        // laisser les cases des prix des scénarios vides quand on n'a pas les prix
        if (scenariosPrices == null) {
            return;
        }

        for (String codeScenario : ALL_CODES_SCENARIOS) {
            cell = row.createCell(column++);

            final Optional<InputRefPrice> scenarioRefPrice = scenariosPrices.get(codeScenario);

            // Si le scénario n'est pas pris en compte, laisser la case vide.
            // S'il n'y a pas de prix pour le scénario, indiquer qu'il n'y a pas de valeur associée.
            if (scenarioRefPrice != null) {
                if (scenarioRefPrice.isEmpty()) {
                    cell.setCellValue(ExportUtils.NO_VALUE);
                } else {
                    double scenarioPriceValue = scenarioRefPrice.get().averageRefPrice().value();
                    cell.setCellValue(Precision.round(scenarioPriceValue, 2, RoundingMode.HALF_UP.ordinal()));
                }
            }
        }
    }

    protected void writeEquipmentRow(RefMateriel refMateriel,
                                     Collection<Equipment> equipments,
                                     PracticedSystem practicedSystem) {
        long nbDomains = equipments.stream()
                .map(Equipment::getDomain)
                .map(Domain::getCode)
                .distinct()
                .count();
        Validate.isTrue(nbDomains == 1);
        
        // new line
        int line = pricesSheet.getPhysicalNumberOfRows();
        Row row = pricesSheet.createRow(line);
        
        int column = 0;
        
        Domain domain = equipments.stream()
                .map(Equipment::getDomain)
                .findFirst()
                .orElse(null);
        
        String campaigns = equipments.stream()
                .map(Equipment::getDomain)
                .map(Domain::getCampaign)
                .distinct()
                .sorted()
                .map(Object::toString)
                .collect(Collectors.joining(", "));
        
        String equipmentNames = equipments.stream()
                .map(Equipment::getName)
                .distinct()
                .collect(Collectors.joining(", "));
        
        // Domaine
        Cell cell = row.createCell(column++);
        if (domain != null) {
            cell.setCellValue(domain.getName());
        }
        if (practicedSystem != null) {
            // Système synthétisé
            cell = row.createCell(column++);
            cell.setCellValue(practicedSystem.getName());
        }
        // Campagne
        cell = row.createCell(column++);
        cell.setCellValue(campaigns);
        // Type
        Class<?> genericClass = switch (refMateriel) {
            case RefMaterielAutomoteur ignored -> RefMaterielAutomoteur.class;
            case RefMaterielIrrigation ignored -> RefMaterielIrrigation.class;
            case RefMaterielOutil ignored -> RefMaterielOutil.class;
            case RefMaterielTraction ignored -> RefMaterielTraction.class;
            case null, default -> RefMateriel.class;
        };
        row.createCell(column++).setCellValue(l(locale, genericClass.getName()));
        // Nom
        row.createCell(column++).setCellValue(equipmentNames);
        
        if (refMateriel != null) {
            // Complément information
            row.createCell(column++).setCellValue(refMateriel.getTypeMateriel1());
            // Durée de vie théorique des matériels (années)
            row.createCell(column++).setCellValue(refMateriel.getDuree_vie_theorique());
            // Investissement matériels (euro)
            row.createCell(column).setCellValue(refMateriel.getPrixMoyenAchat());
        }
    }

    protected <E extends TopiaEntity> String getShortId(E entity) {
        return Entities.getShortId(entity, persistenceContext);
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

}
