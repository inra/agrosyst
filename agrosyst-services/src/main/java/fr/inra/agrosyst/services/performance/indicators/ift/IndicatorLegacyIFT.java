package fr.inra.agrosyst.services.performance.indicators.ift;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, Codelutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.Ift;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.performance.TraitementProduitWithCroppingPlanEntry;
import fr.inra.agrosyst.api.services.performance.TraitementProduitWithSpecies;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingFieldMessage;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import fr.inra.agrosyst.services.referential.csv.RefActaDosageSPCModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static org.nuiton.i18n.I18n.l;

public class IndicatorLegacyIFT extends AbstractIndicator {

    private static final Log LOGGER = LogFactory.getLog(IndicatorLegacyIFT.class);

    /**
     * the corresponding indicator fields
     */
    protected final String[] fields = new String[]{
            "ift_a_l_ancienne_ift_chimique_total",    //  0
            "ift_a_l_ancienne_ift_chimique_tot_hts",  //  1
            "ift_a_l_ancienne_ift_h",                 //  2
            "ift_a_l_ancienne_ift_f",                 //  3
            "ift_a_l_ancienne_ift_i",                 //  4
            "ift_a_l_ancienne_ift_ts",                //  5
            "ift_a_l_ancienne_ift_a",                 //  6
            "ift_a_l_ancienne_ift_hh",                //  7
            "ift_a_l_ancienne_ift_biocontrole"        //  8
    };

    protected final String[] indicators = new String[]{
            "Indicator.label.ift_a_l_ancienne_ift_chimique_total",      // 0 IFT phytosanitaire chimique (ex total, refs #10576)
            "Indicator.label.ift_a_l_ancienne_ift_chimique_tot_hts",    // 1 IFT phytosanitaire chimique hors traitement de semence
            "Indicator.label.ift_a_l_ancienne_ift_h",                   // 2 IFT herbicide
            "Indicator.label.ift_a_l_ancienne_ift_f",                   // 3 IFT fongicide
            "Indicator.label.ift_a_l_ancienne_ift_i",                   // 4 IFT insecticide
            "Indicator.label.ift_a_l_ancienne_ift_ts",                  // 5 IFT traitement de semence
            "Indicator.label.ift_a_l_ancienne_ift_a",                   // 6 IFT phytosanitaire autres
            "Indicator.label.ift_a_l_ancienne_ift_hh",                  // 7 IFT phytosanitaire hors herbicide
            "Indicator.label.ift_a_l_ancienne_ift_biocontrole"          // 8 IFT vert
    };

    protected Boolean[] iftsToDisplay = new Boolean[]{
            true,  // 0 IFT phytosanitaire total
            true,  // 1 IFT phytosanitaire total hors traitement de semence
            true,  // 2 IFT herbicide
            true,  // 3 IFT fongicide
            true,  // 4 IFT insecticide
            true,  // 5 IFT traitement de semence
            true,  // 6 IFT phytosanitaire autres
            true,  // 7 IFT phytosanitaire hors herbicide
            true,  // 8 IFT vert
    };

    /**
     * Constructeur initialisant la map des champs optionnels
     */
    public IndicatorLegacyIFT() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, "ift_a_l_ancienne_tx_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, "ift_a_l_ancienne_detail_champs_non_renseig");
        extraFields.put(OptionalExtraColumnEnumKey.DOSE_REFERENCE, "ift_a_l_ancienne_dose_reference");
        extraFields.put(OptionalExtraColumnEnumKey.DOSE_REFERENCE_UNIT, "ift_a_l_ancienne_dose_reference_unite");
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_LEGACY_IFT);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, indicators[i]);
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {

        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();

        for (int i = 0; i < indicators.length; i++) {
            String indicatorLabel = getIndicatorLabel(i);
            String columnName = fields[i];
            indicatorNameToColumnName.put(indicatorLabel, columnName);
        }
        return indicatorNameToColumnName;
    }

    @Override
    public boolean isDisplayed(ExportLevel atLevel, int i) {
        return iftsToDisplay[i];
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return true;//display at all level
    }

    /**
     * Formule de calcul:
     *
     * <pre>
     *
     *              DA (dose appliquée)
     * IFTi = sum ( -- * PSCi )
     *              DH (Dose homologuée)
     * </pre>
     */
    @Override
    public Double[] manageIntervention(
            WriterContext writerContext,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedCropExecutionContext cropContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            PracticedCropCyclePhase phase) {

        Double[] result = newArray(indicators.length, 0.0);

        Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();

        if (interventionContext.isFictive()) return result;

        final PracticedIntervention intervention = interventionContext.getIntervention();
        final String interventionId = intervention.getTopiaId();

        double toolsPsci = getToolPSCi(intervention);

        Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement =
                interventionContext.getTraitementProduitCategByIdTraitement();

        // calcul de l'IFT pour l'action de semis et ses intrants si nécessaire
        Double[] seedingInputResult = computeSeedingInputsIFT(
                writerContext,
                traitementProduitCategByIdTraitement,
                optionalSeedingActionUsage,
                toolsPsci,
                interventionId);

        result = sum(result, seedingInputResult);


        // le cas des intrants de type semis est géré dans la méthode computeSeedingActionIFT()
        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionContext.getOptionalBiologicalControlAction();
        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction();

        if (optionalBiologicalControlAction.isPresent()) {
            BiologicalControlAction treatmentAction = optionalBiologicalControlAction.get();
            Collection<BiologicalProductInputUsage> biologicalProductInputUsages = treatmentAction.getBiologicalProductInputUsages();

            if (CollectionUtils.isNotEmpty(biologicalProductInputUsages)) {
                result = sum(result,
                        computeTreatmentIftAndWriteInput(
                                writerContext,
                                interventionContext,
                                traitementProduitCategByIdTraitement,
                                treatmentAction,
                                biologicalProductInputUsages));
            }
        }

        if (optionalPesticidesSpreadingAction.isPresent()) {
            PesticidesSpreadingAction treatmentAction = optionalPesticidesSpreadingAction.get();
            Collection<PesticideProductInputUsage> biologicalProductInputUsages = treatmentAction.getPesticideProductInputUsages();

            if (CollectionUtils.isNotEmpty(biologicalProductInputUsages)) {
                result = sum(result,
                        computeTreatmentIftAndWriteInput(
                                writerContext,
                                interventionContext,
                                traitementProduitCategByIdTraitement,
                                treatmentAction,
                                biologicalProductInputUsages
                        ));
            }
        }


        return newResult(result);
    }

    private Double[] computeTreatmentIftAndWriteInput(
            WriterContext writerContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement,
            AbstractAction treatmentAction,
            Collection<? extends AbstractPhytoProductInputUsage> phytoProductInputUsages) {

        Double[] result = newArray(indicators.length, 0.0);

        EffectiveIntervention intervention = interventionContext.getIntervention();
        String interventionId = intervention.getTopiaId();
        String interventionName = intervention.getName();
        final boolean isSpecies = CollectionUtils.isNotEmpty(interventionContext.getInterventionSpecies());

        for (AbstractPhytoProductInputUsage phytoProductInputUsage : phytoProductInputUsages) {

            final String inputTopiaId = phytoProductInputUsage.getTopiaId();

            // espèces
            incrementAngGetTotalFieldCounterForTargetedId(inputTopiaId);
            if (!isSpecies) {
                addMissingFieldMessage(
                        interventionId,
                        messageBuilder.getMissingInterventionSpeciesMessage());
            }

            // L’onglet «ACTA_traitements_produits» indique quels produits sont concernés par le calcul de l’IFT biocontrôle (colonne F).
            final RefActaTraitementsProduit phytoProduct = phytoProductInputUsage.getDomainPhytoProductInput().getRefInput();

            Double dh = null;
            String refDoseValue = null;
            String refDoseUnit = "N/A";
            String refDoseUi = "";

            if (isSpecies && phytoProduct != null) {

                ReferenceDoseDTO referenceDose = getProductDoseHomologue(
                        phytoProduct,
                        interventionContext);


                dh = computeDh(inputTopiaId, phytoProduct, referenceDose);

                refDoseUi = getReferencesDosageUI(phytoProduct, referenceDose);
                interventionContext.addLegacyReferencesDosagesUserInfos(refDoseUi);

                if (referenceDose != null) {
                    refDoseValue = referenceDose.getValue() != null ? referenceDose.getValue().toString() : null;
                    refDoseUnit = RefActaDosageSPCModel.PHYTO_PRODUCT_UNIT_FORMATTER.format(referenceDose.getUnit());
                }

            }

            final double inputPSCi = getPesticideOrBiologicalControlActionPSCi(intervention, treatmentAction);

            Double[] inputResult = computeIFT(
                    treatmentAction,
                    traitementProduitCategByIdTraitement,
                    phytoProductInputUsage,
                    inputPSCi,
                    dh,
                    interventionName);

            IndicatorWriter writer = writerContext.getWriter();

            String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(inputTopiaId, MissingMessageScope.INPUT);
            Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(inputTopiaId);

            for (int i = 0; i < inputResult.length; i++) {

                boolean isDisplayed = isDisplayed(ExportLevel.INPUT, i);

                if (isDisplayed) {
                    // write input sheet
                    writer.writeInputUsage(
                            writerContext.getIts(),
                            writerContext.getIrs(),
                            getIndicatorCategory(),
                            getIndicatorLabel(i),
                            phytoProductInputUsage,
                            treatmentAction,
                            writerContext.getEffectiveIntervention(),
                            writerContext.getPracticedIntervention(),
                            writerContext.getCodeAmmBioControle(),
                            writerContext.getAnonymizeDomain(),
                            writerContext.getAnonymizeGrowingSystem(),
                            writerContext.getPlot(),
                            writerContext.getZone(),
                            writerContext.getPracticedSystem(),
                            writerContext.getCroppingPlanEntry(),
                            writerContext.getPracticedPhase(),
                            writerContext.getSolOccupationPercent(),
                            writerContext.getEffectivePhase(),
                            writerContext.getRank(),
                            writerContext.getPreviousPlanEntry(),
                            writerContext.getIntermediateCrop(),
                            this.getClass(),
                            inputResult[i],
                            reliabilityIndexForInputId,
                            reliabilityCommentForInputId,
                            refDoseUi,
                            refDoseValue,
                            refDoseUnit,
                            writerContext.getGroupesCiblesByCode());
                }
            }

            result = sum(result, inputResult);
        }
        return result;
    }

    protected Double[] computeTreatmentIftAndWriteInput(
            WriterContext writerContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement,
            AbstractAction treatmentAction,
            Collection<? extends AbstractPhytoProductInputUsage> phytoProductInputUsages) {

        Double[] result = newArray(indicators.length, 0.0);

        PracticedIntervention intervention = interventionContext.getIntervention();
        String interventionId = intervention.getTopiaId();
        String interventionName = intervention.getName();
        final boolean isSpecies = CollectionUtils.isNotEmpty(interventionContext.getInterventionSpecies());

        for (AbstractPhytoProductInputUsage phytoProductInputUsage : phytoProductInputUsages) {

            final String inputTopiaId = phytoProductInputUsage.getTopiaId();

            // espèces
            incrementAngGetTotalFieldCounterForTargetedId(inputTopiaId);
            if (!isSpecies) {
                addMissingFieldMessage(
                        interventionId,
                        messageBuilder.getMissingInterventionSpeciesMessage());
            }

            // L’onglet «ACTA_traitements_produits» indique quels produits sont concernés par le calcul de l’IFT biocontrôle (colonne F).
            final RefActaTraitementsProduit phytoProduct = phytoProductInputUsage.getDomainPhytoProductInput().getRefInput();

            Double dh = null;
            String refDoseValue = null;
            String refDoseUnit = "N/A";
            String refDoseUi = "";

            if (isSpecies && phytoProduct != null) {

                ReferenceDoseDTO referenceDose = getProductDoseHomologue(
                        phytoProduct,
                        interventionContext);


                dh = computeDh(inputTopiaId, phytoProduct, referenceDose);

                refDoseUi = getReferencesDosageUI(phytoProduct, referenceDose);
                interventionContext.addLegacyReferencesDosagesUserInfos(refDoseUi);

                if (referenceDose != null) {
                    refDoseValue = referenceDose.getValue() != null ? referenceDose.getValue().toString() : null;
                    refDoseUnit = RefActaDosageSPCModel.PHYTO_PRODUCT_UNIT_FORMATTER.format(referenceDose.getUnit());
                }

            }

            final double inputPSCi = getPesticideOrBiologicalControlActionPSCi(intervention, treatmentAction);

            Double[] inputResult = computeIFT(
                    treatmentAction,
                    traitementProduitCategByIdTraitement,
                    phytoProductInputUsage,
                    inputPSCi,
                    dh,
                    interventionName);

            IndicatorWriter writer = writerContext.getWriter();

            String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(inputTopiaId, MissingMessageScope.INPUT);
            Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(inputTopiaId);

            for (int i = 0; i < inputResult.length; i++) {

                boolean isDisplayed = isDisplayed(ExportLevel.INPUT, i);

                if (isDisplayed) {
                    // write input sheet
                    writer.writeInputUsage(
                            writerContext.getIts(),
                            writerContext.getIrs(),
                            getIndicatorCategory(),
                            getIndicatorLabel(i),
                            phytoProductInputUsage,
                            treatmentAction,
                            writerContext.getEffectiveIntervention(),
                            writerContext.getPracticedIntervention(),
                            writerContext.getCodeAmmBioControle(),
                            writerContext.getAnonymizeDomain(),
                            writerContext.getAnonymizeGrowingSystem(),
                            writerContext.getPlot(),
                            writerContext.getZone(),
                            writerContext.getPracticedSystem(),
                            writerContext.getCroppingPlanEntry(),
                            writerContext.getPracticedPhase(),
                            writerContext.getSolOccupationPercent(),
                            writerContext.getEffectivePhase(),
                            writerContext.getRank(),
                            writerContext.getPreviousPlanEntry(),
                            writerContext.getIntermediateCrop(),
                            this.getClass(),
                            inputResult[i],
                            reliabilityIndexForInputId,
                            reliabilityCommentForInputId,
                            refDoseUi,
                            refDoseValue,
                            refDoseUnit,
                            writerContext.getGroupesCiblesByCode());
                }
            }

            result = sum(result, inputResult);
        }
        return result;
    }


    private Double[] computeIFT(
            AbstractAction action,
            Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement,
            AbstractPhytoProductInputUsage phytoProductInputUsage,
            double inputPSCi,
            Double dh,
            String interventionName) {

        Double[] result = newArray(indicators.length, 0.0);
        final String inputTopiaId = phytoProductInputUsage.getTopiaId();

        incrementAngGetTotalFieldCounterForTargetedId(inputTopiaId);// PhytoProduct
        incrementAngGetTotalFieldCounterForTargetedId(inputTopiaId);// phytoProductUnit
        incrementAngGetTotalFieldCounterForTargetedId(inputTopiaId);// QtAvg

        final DomainPhytoProductInput domainPhytoProductInput = phytoProductInputUsage.getDomainPhytoProductInput();
        RefActaTraitementsProduit phytoProduct = domainPhytoProductInput.getRefInput();
        if (phytoProduct == null) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Can't find phyto product for input " + phytoProductInputUsage.getDomainPhytoProductInput().getInputName());
            }

            addMissingFieldMessage(
                    inputTopiaId,
                    messageBuilder.getMissingInputProductMessage(phytoProductInputUsage.getInputType()));
            return result;
        }

        // L’onglet « ACTA_traitements_prod_categ » indique quels traitements sont concernés par quelles catégories de l’IFT (colonnes G à O).
        RefActaTraitementsProduitsCateg traitementProduitCateg = traitementProduitCategByIdTraitement.get(phytoProduct);

        if (traitementProduitCateg == null) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Aucune donnée dans le référentiel ACTA Traitements Produits (Catégories) pour le produit " +
                        phytoProduct.getNom_produit() + " idProduit: '" +
                        phytoProduct.getId_produit() + "' idTraitement: '" +
                        phytoProduct.getId_traitement() + "'");

            }
            return result;
        }

        boolean nodu = phytoProduct.isNodu();//Nombre de Doses Unité

        double ift;
        // DA : dose appliquée de chaque produit appliqué lors d’une intervention, exprimée en g ou kg par ha
        // ou hl. Données saisies par l’utilisateur (l’utilisateur entre la dose appliquée et
        // sélectionne l’unité dans une liste de valeurs). Si la dose est exprimée par hl, elle
        // doit être multipliée par le volume de bouillie épandu par ha (donnée saisie par l’utilisateur).
        Double da = phytoProductInputUsage.getQtAvg();
        PhytoProductUnit productUnit = domainPhytoProductInput.getUsageUnit();

        addMissingDeprecatedInputQtyOrUnitIfRequired(phytoProductInputUsage);

        if (da == null || productUnit == null) {
            // #10124
            // - si le champ 'dose appliquée' n'est pas renseignée pour un intrant de type 'produit phytosanitaire',
            //   alors la valeur par défaut est la dose de référence pour ce produit phytosanitaire sur la culture concernée (ce qui donne un IFT de 1,
            //   modulo PSCI);
            // - si Agrosyst ne trouve pas de dose de référence pour ce produit phytosanitaire sur la culture concernée,
            //   alors l'IFT associée à l'intrant est 1*PSCI (comme c'est déjà le cas)
            MissingFieldMessage message;
            if (da == null) {
                message = messageBuilder.getMissingDoseMessage(phytoProductInputUsage.getInputType(), MissingMessageScope.INPUT);
            } else {
                message = messageBuilder.getMissingDoseUnitMessage(phytoProductInputUsage.getInputType());
            }
            addMissingFieldMessage(inputTopiaId, message);
            ift = DEFAULT_IFT * inputPSCi;

        } else {
            // DH : dose homologuée du produit appliqué, exprimée en g ou kg par ha ou hl (l’unité doit
            // être identique à celle saisie par l’utilisateur pour DA). Donnée de référence. Si la dose
            // appliquée est exprimée par hl, elle doit automatiquement être multipliée par 10 pour l’exprimer
            // par ha (les doses d’homologation sont calculées sur une base d’un traitement à 10 hl de bouillie par hectare).
            if (dh != null) {// dh est calculé si des espèces sont présentes
                // calcul de la valeur de l'ift
                da = da * computeBoiledQuantityFactor(action, productUnit);
                ift = dh == 0.0d ? inputPSCi : inputPSCi * (da / dh);

            } else {
                // pour les espèces sélectionnées
                if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn("No species selected for intervention " + interventionName);
                }

                // refs #9601:  quand on n'a pas d'espèce, on n'a pas de dose de référence identifiée mais pour autant,
                //   il faut effectivement appliquer la même règle que lorsque l'on ne trouve pas de DR : IFT = 1 quelque soit la dose d'application déclarée
                ift = inputPSCi;

            }
        }

        // Par exemple, pour le calcul de l'IFT Chimique, l'algorithme regardera dans la table
        // ACTA_traitements_produits si le produit est dans la liste NODU (Nombre de Doses Unités, colonne NODU O/N)
        // Si ce n'est pas le cas, il ira vérifier dans ACTA_traitement_prod_categ si le traitement
        // entre dans le calcul de l'IFT chimique total (colonne IFT_chimique_total O/N). Si toutes les conditions
        // sont respectées, alors le produit rentre dans le calcul de l'IFT.

        // affectation de l'IFT au catégories concernées
        if (traitementProduitCateg.isIft_chimique_total() && !nodu) {
            result[0] = ift; // "IFT Chimique"
        }
        if (traitementProduitCateg.isIft_chimique_tot_hts() && !nodu) {
            result[1] = ift; // "IFT tot hts"
        }
        if (traitementProduitCateg.isIft_h() && !nodu) {
            result[2] = ift; // "IFT h"
        }
        if (traitementProduitCateg.isIft_f() && !nodu) {
            result[3] = ift; // "IFT f"
        }
        if (traitementProduitCateg.isIft_i() && !nodu) {
            result[4] = ift; // "IFT i"
        }
        if (traitementProduitCateg.isIft_ts() && !nodu) {
            result[5] = ift; // "IFT ts"
        }
        if (traitementProduitCateg.isIft_a() && !nodu) {
            result[6] = ift; // "IFT a"
        }
        if (traitementProduitCateg.isIft_hh() && !nodu) {
            result[7] = ift; // "IFT hh (ts inclus)"
        }
        if (nodu) {
            result[8] = ift; // "IFT biocontrole"
        }

        return result;

    }

    @Override
    public Double[] manageIntervention(
            WriterContext writerContext,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveCropExecutionContext cropContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {

        Double[] result = newArray(indicators.length, 0.0);

        Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();

        final EffectiveIntervention intervention = interventionContext.getIntervention();
        final String interventionId = intervention.getTopiaId();

        double toolsPsci = getToolPSCi(intervention);

        Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement =
                interventionContext.getTraitementProduitCategByIdTraitement();

        // calcul de l'IFT pour l'action de semis et ses intrants si nécessaire
        Double[] seedingInputResult = computeSeedingInputsIFT(
                writerContext,
                traitementProduitCategByIdTraitement,
                optionalSeedingActionUsage,
                toolsPsci,
                interventionId);

        result = sum(result, seedingInputResult);

        // le cas des intrants de type semis est géré dans la méthode computeSeedingActionIFT()
        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionContext.getOptionalBiologicalControlAction();
        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction();

        if (optionalBiologicalControlAction.isPresent()) {
            BiologicalControlAction treatmentAction = optionalBiologicalControlAction.get();
            Collection<BiologicalProductInputUsage> biologicalProductInputUsages = treatmentAction.getBiologicalProductInputUsages();

            if (CollectionUtils.isNotEmpty(biologicalProductInputUsages)) {
                result = sum(result,
                        computeTreatmentIftAndWriteInput(
                                writerContext,
                                interventionContext,
                                traitementProduitCategByIdTraitement,
                                treatmentAction,
                                biologicalProductInputUsages));
            }
        }

        if (optionalPesticidesSpreadingAction.isPresent()) {
            PesticidesSpreadingAction treatmentAction = optionalPesticidesSpreadingAction.get();
            Collection<PesticideProductInputUsage> pesticideProductInputs = treatmentAction.getPesticideProductInputUsages();

            if (CollectionUtils.isNotEmpty(pesticideProductInputs)) {
                result = sum(result,
                        computeTreatmentIftAndWriteInput(
                                writerContext,
                                interventionContext,
                                traitementProduitCategByIdTraitement,
                                treatmentAction,
                                pesticideProductInputs));
            }
        }

        return newResult(result);
    }

    public static ReferenceDoseDTO getProductDoseHomologue(
            RefActaTraitementsProduit phytoProduct,
            PerformancePracticedInterventionExecutionContext practicedInterventionContext) {

        Map<TraitementProduitWithSpecies, ReferenceDoseDTO> refDosageSPCForPhytoInputs =
                practicedInterventionContext.getLegacyRefDosageForPhytoInputs();

        TraitementProduitWithSpecies traitementProduitWithSpecies =
                new TraitementProduitWithSpecies(phytoProduct, practicedInterventionContext.getCropWithSpecies());

        ReferenceDoseDTO referenceDose = refDosageSPCForPhytoInputs.get(traitementProduitWithSpecies);

        return referenceDose;
    }

    public static ReferenceDoseDTO getProductDoseHomologue(
            RefActaTraitementsProduit phytoProduct,
            PerformanceEffectiveInterventionExecutionContext effectiveInterventionExecutionContext) {

        Map<TraitementProduitWithCroppingPlanEntry, ReferenceDoseDTO> refDosageSPCForPhytoInputs =
                effectiveInterventionExecutionContext.getLegacyRefDosageForPhytoInputs();

        TraitementProduitWithCroppingPlanEntry traitementProduitWithCroppingPlanEntry =
                new TraitementProduitWithCroppingPlanEntry(phytoProduct, effectiveInterventionExecutionContext.getCroppingPlanEntry());
        ReferenceDoseDTO referenceDose = refDosageSPCForPhytoInputs.get(traitementProduitWithCroppingPlanEntry);

        return referenceDose;
    }

    protected double computeDh(
            String inputId,
            RefActaTraitementsProduit phytoProduct,
            ReferenceDoseDTO referenceDose) {

        // referenceDose
        incrementAngGetTotalFieldCounterForTargetedId(inputId);

        double dh = 0.0;

        if (referenceDose != null && referenceDose.getValue() != null) {

            PhytoProductUnit unit = referenceDose.getUnit();

            dh = referenceDose.getValue();

            // Si la dose appliquée est exprimée par hl, elle doit automatiquement être multipliée par 10 pour l’exprimer
            // par ha (les doses d’homologation sont calculées sur une base d’un traitement à 10 hl de bouillie par hectare).
            if (unit == PhytoProductUnit.G_HL ||
                    unit == PhytoProductUnit.KG_HL ||
                    unit == PhytoProductUnit.L_HL) {
                dh *= 10;
            }
        } else {
            addMissingRefDosageMessageMessage(inputId);
            if (LOGGER.isWarnEnabled()) {
                String warningMessage;
                if (referenceDose == null) {
                    warningMessage = "Can't find reference dose for %s/%s";
                } else {
                    warningMessage = "Reference dose value is null for %s/%s";
                }
                LOGGER.warn(String.format(warningMessage, phytoProduct.getNom_produit(), phytoProduct.getNom_traitement()));
            }
        }

        return dh;
    }

    /**
     * @param phytoProduct  required
     * @param referenceDose optional
     * @return user readable ref dose
     */
    protected String getReferencesDosageUI(
            RefActaTraitementsProduit phytoProduct,
            ReferenceDoseDTO referenceDose) {

        String referencesDosageUI = "Dose pour '(%s-%s):%s %s'";
        if (referenceDose != null &&
                referenceDose.getValue() != null) {

            PhytoProductUnit unit = referenceDose.getUnit();
            referencesDosageUI = String.format(
                    referencesDosageUI,
                    phytoProduct.getNom_produit(),
                    phytoProduct.getNom_traitement(),
                    referenceDose.getValue().toString(),
                    RefActaDosageSPCModel.PHYTO_PRODUCT_UNIT_FORMATTER.format(unit));

        } else {
            referencesDosageUI = String.format(referencesDosageUI, phytoProduct.getNom_produit(), phytoProduct.getNom_traitement(), "#NA", "");
        }

        return referencesDosageUI;
    }

    /**
     * fait référence au ticket: #10685
     * <p>
     * Dans le cas des traitements de semence, on peut se retrouver avec les cas de figure suivants :
     * <p>
     * 1.1 . Case 'Traitement chimique des semences / plants' cochée et champ 'nodu = false'
     * dans le ref RefActaTraitementsProduit pour le couple produit*id_traitement = TS chimique
     * 1.2. Case 'Traitement chimique des semences / plants' cochée et champ 'nodu = true'
     * dans le ref RefActaTraitementsProduit pour le couple produit*id_traitement = TS chimique + NODU
     * <p>
     * 2.1 . Case 'Inoculation biologique des semences / plants' cochée et champ 'nodu = false'
     * dans le ref RefActaTraitementsProduit pour le couple produit*id_traitement = TS lutte bio
     * 2.2. Case 'Inoculation biologique des semences / plants' cochée et champ 'nodu = true'
     * dans le ref RefActaTraitementsProduit pour le couple produit*id_traitement = TS lutte bio + NODU
     * <p>
     * Les méthodes de calcul des IFT sont les suivantes :
     * <p>
     * 1.1 :
     * on calcule un ift_ts, qui est comptabilisé dans l'ift_chimique_total et décompté de l'ift_chimique_tot_hts
     * avec
     * ift_ts = PSCi*1
     * -> Calcul valide actuellement
     * <p>
     * 1.2 :
     * ift_chimique_total = ift_chimique_tot_hts = ift_h = ift_f = ift_i = ift_ts = ift_a = ift_hh = 0
     * avec
     * ift_biocontrole = PSCi*1
     * -> Calcul actuel A MODIFIER
     * <p>
     * 2.1 :
     * Pas de calcul d'IFT, mais on calcule un indicateur de Recours à des moyens biologiques sans AMM
     * avec
     * Recours à des moyens biologiques sans AMM = PSCi*1
     * -> Calcul valide actuellement
     * <p>
     * 2.2 :
     * On calcule un indicateur de Recours à des moyens biologiques sans AMM ET un IFT biocontrôle
     * avec
     * Recours à des moyens biologiques sans AMM = PSCi*1
     * ift_biocontrole = PSCi*1
     * -> Calcul valide actuellement
     * <p>
     * avec, dans tous les cas, PSCi = fréquence spatiale * fréquence temporelle
     * (pour les actions de semis, il n'y a pas de notion de Proportion de Surface Traitée).
     **/
    protected Double[] computeSeedingInputsIFT(
            WriterContext writerContext,
            Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement,
            Optional<SeedingActionUsage> optionalSeedingActionUsage,
            double toolsPsci,
            String interventionId) {

        Double[] result = newArray(indicators.length, 0.0);

        if (optionalSeedingActionUsage.isPresent()) {
            SeedingActionUsage seedingActionUsage = optionalSeedingActionUsage.get();
            Collection<SeedLotInputUsage> seedLotInputUsages = seedingActionUsage.getSeedLotInputUsage();

            boolean isTreatment = seedLotInputUsages.stream()
                    .map(SeedLotInputUsage::getDomainSeedLotInput)
                    .map(DomainSeedLotInput::getDomainSeedSpeciesInput)
                    .flatMap(Collection::stream)
                    .anyMatch(DomainSeedSpeciesInput::isChemicalTreatment);
            boolean isBiologicalSeedInoculation = seedLotInputUsages.stream()
                    .map(SeedLotInputUsage::getDomainSeedLotInput)
                    .map(DomainSeedLotInput::getDomainSeedSpeciesInput)
                    .flatMap(Collection::stream)
                    .anyMatch(DomainSeedSpeciesInput::isBiologicalSeedInoculation);

            Collection<SeedProductInputUsage> seedingProductInputUsages = new HashSet<>();
            seedLotInputUsages.forEach(
                    seedLotInputUsage -> {
                        final Collection<SeedSpeciesInputUsage> seedingSpecies = CollectionUtils.emptyIfNull(seedLotInputUsage.getSeedingSpecies());
                        seedingSpecies.forEach(
                                seedSpeciesInputUsage -> seedingProductInputUsages.addAll(seedSpeciesInputUsage.getSeedProductInputUsages())
                        );
                    }
            );
            // isTreatment
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            // isBiologicalSeedInoculation
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);

            if (CollectionUtils.isEmpty(seedingProductInputUsages)) {
                int ift_ts = isTreatment ? 1 : 0;
                int ift_biocontrole = 0;

                Double[] inputSeedingResult = feedUpSeedingIft(toolsPsci, ift_ts, ift_biocontrole);
                result = sum(result, inputSeedingResult);
            }

            for (SeedProductInputUsage phytoProductInputUsage : seedingProductInputUsages) {
                int ift_ts = 0;
                int ift_biocontrole = 0;

                RefActaTraitementsProduit refActaTraitementsProduit = phytoProductInputUsage.getDomainPhytoProductInput().getRefInput();

                // PhytoProduct
                final String inputTopiaId = phytoProductInputUsage.getTopiaId();
                incrementAngGetTotalFieldCounterForTargetedId(inputTopiaId);

                if (refActaTraitementsProduit == null) {
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn("Can't find phyto product for input " + phytoProductInputUsage.getDomainPhytoProductInput().getInputName());
                    }

                    addMissingFieldMessage(
                            inputTopiaId,
                            messageBuilder.getMissingInputProductMessage(phytoProductInputUsage.getInputType()));
                    return result;
                }

                // L’onglet « ACTA_traitements_prod_categ » indique quels traitements sont concernés par quelles catégories de l’IFT (colonnes G à O).
                RefActaTraitementsProduitsCateg traitementProduitCateg = traitementProduitCategByIdTraitement.get(refActaTraitementsProduit);

                if (traitementProduitCateg == null) {
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn("Aucune donnée dans le référentiel ACTA Traitements Produits (Catégories) pour le produit " +
                                refActaTraitementsProduit.getNom_produit() + " idProduit: '" +
                                refActaTraitementsProduit.getId_produit() + "' idTraitement: '" +
                                refActaTraitementsProduit.getId_traitement() + "'");
                    }
                    continue;
                }

                // ift_ts = isTreatment ? ift_ts + 1 : ift_ts;
                // ift_bio = isBiologicalSeedInoculation ? ift_bio + 1 : ift_bio;
                boolean nodu = refActaTraitementsProduit.isNodu();//Nombre de Doses Unité

                // Case 'Traitement chimique des semences / plants' cochée et champ 'nodu = false'
                // dans le ref RefActaTraitementsProduit pour le couple produit*id_traitement = TS chimique
                // on calcule un ift_ts, qui est comptabilisé dans l'ift_chimique_total et décompté de l'ift_chimique_tot_hts avec
                // ift_ts = PSCi*1
                if (isTreatment && traitementProduitCateg.isIft_ts() && !nodu) {
                    ift_ts = ift_ts + 1;
                }

                //  Case 'Traitement chimique des semences / plants' cochée et champ 'nodu = true'
                // ou
                // Case 'Inoculation biologique des semences / plants' cochée et champ 'nodu = true'
                // dans le ref RefActaTraitementsProduit pour le couple produit*id_traitement = TS lutte bio + NODU
                // On calcule un indicateur de Recours à des moyens biologiques sans AMM ET un IFT biocontrôle avec
                //  Recours à des moyens biologiques sans AMM = PSCi*1
                //  ift_biocontrole = PSCi*1
                if ((isTreatment || isBiologicalSeedInoculation) && nodu) {
                    ift_biocontrole = ift_biocontrole + 1;
                }

                Double[] inputSeedingResult = feedUpSeedingIft(toolsPsci, ift_ts, ift_biocontrole);

                IndicatorWriter writer = writerContext.getWriter();

                String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(inputTopiaId, MissingMessageScope.INPUT);
                Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(inputTopiaId);

                for (int i = 0; i < inputSeedingResult.length; i++) {

                    boolean isDisplayed = isDisplayed(ExportLevel.INPUT, i);

                    if (isDisplayed) {
                        // write intervention sheet
                        writer.writeInputUsage(
                                writerContext.getIts(),
                                writerContext.getIrs(),
                                getIndicatorCategory(),
                                getIndicatorLabel(i),
                                phytoProductInputUsage,
                                seedingActionUsage,
                                writerContext.getEffectiveIntervention(),
                                writerContext.getPracticedIntervention(),
                                writerContext.getCodeAmmBioControle(),
                                writerContext.getAnonymizeDomain(),
                                writerContext.getAnonymizeGrowingSystem(),
                                writerContext.getPlot(),
                                writerContext.getZone(),
                                writerContext.getPracticedSystem(),
                                writerContext.getCroppingPlanEntry(),
                                writerContext.getPracticedPhase(),
                                writerContext.getSolOccupationPercent(),
                                writerContext.getEffectivePhase(),
                                writerContext.getRank(),
                                writerContext.getPreviousPlanEntry(),
                                writerContext.getIntermediateCrop(),
                                this.getClass(),
                                inputSeedingResult[i],
                                reliabilityIndexForInputId,
                                reliabilityCommentForInputId,
                                null,
                                null,
                                null,
                                writerContext.getGroupesCiblesByCode());
                    }
                }
                result = sum(result, inputSeedingResult);
            }
        }

        return result;
    }

    protected Double[] feedUpSeedingIft(double toolsPsci, int ift_ts, int ift_biocontrole) {
        Double[] result = newArray(indicators.length, 0.0);
        result[0] += ift_ts * toolsPsci; // "IFT Chimique"
        result[1] += 0; // "IFT tot hts"
        result[2] += 0; // "IFT h"
        result[3] += 0; // "IFT f"
        result[4] += 0; // "IFT i"
        result[5] += ift_ts * toolsPsci; // "IFT ts"
        result[6] += 0; // "IFT a"
        result[7] += ift_ts * toolsPsci; // "IFT hh (ts inclus)"
        result[8] += ift_biocontrole * toolsPsci; // "IFT biocontrole"
        return result;
    }

    @Override
    protected List<String> getReferencesDosagesUserInfos(PerformanceInterventionContext interventionContext) {
        return new ArrayList<>(interventionContext.getLegacyReferencesDosagesUserInfos().stream().filter(Objects::nonNull).toList());
    }

    public void init(Collection<Ift> iftsToDisplay) {

        resetIftToDisplay();

        for (Ift ift : iftsToDisplay) {
            switch (ift) {
                case TOTAL -> this.iftsToDisplay[0] = true;
                case SUM_HTS -> this.iftsToDisplay[1] = true;
                case H -> this.iftsToDisplay[2] = true;
                case F -> this.iftsToDisplay[3] = true;
                case I -> this.iftsToDisplay[4] = true;
                case TS -> this.iftsToDisplay[5] = true;
                case A -> this.iftsToDisplay[6] = true;
                case HH -> this.iftsToDisplay[7] = true;
                case BIO_CONTROL -> this.iftsToDisplay[8] = true;
            }
        }
    }

    protected void resetIftToDisplay() {
        iftsToDisplay = new Boolean[]{
                false,  // IFT phytosanitaire total
                false,  // IFT phytosanitaire total hors traitement de semence
                false,  // IFT herbicide
                false,  // IFT fongicide
                false,  // IFT insecticide
                false,  // IFT traitement de semence
                false,  // IFT phytosanitaire autres
                false,  // IFT phytosanitaire hors herbicide
                false,  // IFT vert
                false,  // anciennement IFT moyens biologiques
        };
    }
}
