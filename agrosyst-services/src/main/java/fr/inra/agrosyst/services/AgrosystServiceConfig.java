package fr.inra.agrosyst.services;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.services.common.CommonService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.cfg.Environment;
import org.hibernate.dialect.H2Dialect;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.topia.flyway.TopiaFlywayService;
import org.nuiton.topia.flyway.TopiaFlywayServiceImpl;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

/**
 * Service module configuration.
 *
 * @author Eric Chatellier
 */
public class AgrosystServiceConfig {

    private static final Log LOGGER = LogFactory.getLog(AgrosystServiceConfig.class);

    /**
     * Configuration filename.
     */
    protected static final String AGROSYST_DEFAULT_CONF_FILENAME = "agrosyst-default.properties";

    protected static final String URL_SEPARATOR = "/";

    protected final String configFileName;

    /**
     * Delegate application config.
     */
    protected ApplicationConfig config;

    public AgrosystServiceConfig(String configFileName) {
        this.configFileName = configFileName;
        reloadConfiguration();
    }

    protected void reloadConfiguration() {
        try {
            ApplicationConfig defaultConfig = new ApplicationConfig(AGROSYST_DEFAULT_CONF_FILENAME);
            defaultConfig.loadDefaultOptions(ServiceConfigOption.values());
            defaultConfig.parse();
            if (StringUtils.isNotBlank(this.configFileName)) {
                Properties flatOptions = defaultConfig.getFlatOptions(false);
                config = new ApplicationConfig(flatOptions, this.configFileName);
                config.parse();

                if (LOGGER.isInfoEnabled()) {
                    LOGGER.info("load properties from " + this.configFileName);
                }
            } else {
                if (LOGGER.isInfoEnabled()) {
                    LOGGER.info("No specific configuration provided, using the default one");
                }
                config = defaultConfig;
            }
            CommonService.createInstance(this.getLowerCampaignBound(), this.getUpperCampaignBound());
        } catch (ArgumentsParserException ex) {
            throw new AgrosystTechnicalException("Can't read configuration", ex);
        }
    }


    public Properties getFlatOptions() {
        return config.getFlatOptions();
    }

    public String getApplicationVersion() {
        return config.getOption(ServiceConfigOption.APPLICATION_VERSION.key);
    }

    public String getApplicationBuildDate() {
        return config.getOption(ServiceConfigOption.APPLICATION_BUILD_DATE.key);
    }

    public String getApplicationRevision() {
        return config.getOption(ServiceConfigOption.APPLICATION_REVISION.key);
    }

    public String getApplicationBaseUrl() {
        String result = config.getOption(ServiceConfigOption.APPLICATION_BASE_URL.key).trim();
        if (result.endsWith("/")) {
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }

    public boolean isDemoModeEnabled() {
        return config.getOptionAsBoolean(ServiceConfigOption.DEMO_MODE.key);
    }

    public boolean isGenerateBuiltinReferentials() {
        return config.getOptionAsBoolean(ServiceConfigOption.GENERATE_BUILTIN_REFERENTIALS.key);
    }

    public boolean isGenerateBuiltinUsers() {
        return config.getOptionAsBoolean(ServiceConfigOption.GENERATE_BUILTIN_USERS.key);
    }

    public int getSpeciesGraftSupportCodeSection() {
        return config.getOptionAsInt(ServiceConfigOption.SPECIES_GRAFTSUPPORT_CODESECTION.key);
    }

    public String getSpeciesGraftSupportUtilisation() {
        return config.getOption(ServiceConfigOption.SPECIES_GRAFTSUPPORT_UTILISATION.key);
    }

    public String getSpeciesGraftCloneTypeVarietal() {
        return config.getOption(ServiceConfigOption.SPECIES_GRAFTCLONE_TYPEVARIETAL.key);
    }

    public boolean isTokenStorageEnabled() {
        return config.getOptionAsBoolean(ServiceConfigOption.TOKEN_STORAGE_ENABLED.key);
    }

    public boolean isBusinessCachingEnabled() {
        return config.getOptionAsBoolean(ServiceConfigOption.BUSINESS_CACHING_ENABLED.key);
    }

    public void setBusinessCachingEnabled(boolean value) {
        config.setOption(ServiceConfigOption.BUSINESS_CACHING_ENABLED.key, String.valueOf(value));
    }

    public long getBusinessCachingDuration() {
        return config.getOptionAsLong(ServiceConfigOption.BUSINESS_CACHING_DURATION.key);
    }

    public long getBusinessCachingShortDuration() {
        return config.getOptionAsLong(ServiceConfigOption.BUSINESS_CACHING_SHORT_DURATION.key);
    }

    public int getActaDosageSpcCroppingZonesGroupId() {
        return config.getOptionAsInt(ServiceConfigOption.ACTA_DOSAGE_SPC_CROPPING_ZONES_GROUP_ID.key);
    }
    
    public int getLowerCampaignBound() {
        return config.getOptionAsInt(ServiceConfigOption.LOWER_CAMPAIGN_BOUND.key);
    }
    
    public int getUpperCampaignBound() {
        return config.getOptionAsInt(ServiceConfigOption.UPPER_CAMPAIGN_BOUND.key);
    }

    public String getFileEncoding() {
        String option = config.getOption(ServiceConfigOption.FILE_ENCODING.key);
        return option;
    }

    public boolean isEmailEnabled() {
        return config.getOptionAsBoolean(ServiceConfigOption.EMAIL_ENABLED.key);
    }

    public String getSmtpHost() {
        return config.getOption(ServiceConfigOption.EMAIL_SMPT_HOST.key);
    }

    public int getSmtpPort() {
        return config.getOptionAsInt(ServiceConfigOption.EMAIL_SMPT_PORT.key);
    }

    public String getEmailFrom() {
        return config.getOption(ServiceConfigOption.EMAIL_FROM.key);
    }
    
    public String getFeedbackEmail() {
        return config.getOption(ServiceConfigOption.USER_FEEDBACK_EMAIL.key);
    }

    public String getSupportEmail() {
        return config.getOption(ServiceConfigOption.USER_SUPPORT_EMAIL.key);
    }
 
    public String getLogFileLocation() {
        return config.getOption(ServiceConfigOption.LOG_FILE_LOCATION.key);
    }

    public String getDatabaseConnectionProvider() {
        return config.getOption(ServiceConfigOption.DATABASE_CONNECTION_PROVIDER.key);
    }

    public String getDephyGraphAPIUrl() {
        String result = config.getOption(ServiceConfigOption.DEPHYGRAPH_API_URL.getKey());
        if (result != null && !result.endsWith(URL_SEPARATOR)) {
            result += URL_SEPARATOR;
        }
        return result;
    }
    
    public String getDephyGraphRedirectionUrl() {
        String result = config.getOption(ServiceConfigOption.DEPHYGRAPH_REDIRECT_URL.getKey());
        if (result != null && !result.endsWith(URL_SEPARATOR)) {
            result += URL_SEPARATOR;
        }
        return result;
    }

    public String getMAAReferentialUrl() {
        return config.getOption(ServiceConfigOption.MAA_REFERENTIAL_URL.getKey());
    }

    public int getMAARefenrentialNbElementLoaded() {
        return config.getOptionAsInt(ServiceConfigOption.MAA_REFERENTIAL_ELEMENT_SIZE.key);
    }

    public int getMAARefenrentialNbPageToLoad() {
        return config.getOptionAsInt(ServiceConfigOption.MAA_REFERENTIAL_NB_PAGE_TO_LOAD.key);
    }

    public String getRefMaterielDefaultTractorCode() {
        return config.getOption(ServiceConfigOption.GENERIC_TRACTOR_MATERIAL_CODE.key);
    }
    
    public String getRefMaterielDefaultIrrigationCode() {
        return config.getOption(ServiceConfigOption.GENERIC_IRRIGATION_MATERIAL_CODE.key);
    }
    
    public String getRefMaterielDefaultEquipmentCode() {
        return config.getOption(ServiceConfigOption.GENERIC_EQUIPMENT_MATERIAL_CODE.key);
    }
    
    public String getRefMaterielDefaultAutomoteurCode() {
        return config.getOption(ServiceConfigOption.GENERIC_AUTOMOTEUR_MATERIAL_CODE.key);
    }

    public String getDefaultArboAdventiceIdForReport() {
        return config.getOption(ServiceConfigOption.DEFAULT_ARBO_ADVENTICE_ID_FOR_REPORT.key);
    }
    
    public int getAsyncImmediatePoolSize() {
        return config.getOptionAsInt(ServiceConfigOption.ASYNC_IMMEDIATE_POOL_SIZE.key);
    }

    public boolean isMaintenanceModeActive() {
        return config.getOptionAsBoolean(ServiceConfigOption.MAINTENANCE_MODE_ACTIVE.key);
    }

    public String getMaintenanceModeMessage() {
        return config.getOption(ServiceConfigOption.MAINTENANCE_MODE_MESSAGE.key);
    }

    public boolean isMaintenanceModeDisconnectAllUsers() {
        return config.getOptionAsBoolean(ServiceConfigOption.MAINTENANCE_MODE_DISCONNECT_ALL_USERS.key);
    }

    public boolean isCronJobsEnabled() {
        return config.getOptionAsBoolean(ServiceConfigOption.CRON_JOBS_ENABLED.key);
    }

    public void setMaintenanceMode(boolean active, String message, boolean disconnectAllUsers) {
        config.setOption(ServiceConfigOption.MAINTENANCE_MODE_ACTIVE.key, String.valueOf(active));
        config.setOption(ServiceConfigOption.MAINTENANCE_MODE_MESSAGE.key, message);
        config.setOption(ServiceConfigOption.MAINTENANCE_MODE_DISCONNECT_ALL_USERS.key, String.valueOf(disconnectAllUsers));
        config.saveForUser();
    }

    public Optional<String> getJmsUrl() {
        String value = config.getOption(ServiceConfigOption.JMS_URL.key);
        Optional<String> result = Optional.ofNullable(StringUtils.trimToNull(value));
        return result;
    }

    public int getDownloadableRetentionDays() {
        return config.getOptionAsInt(ServiceConfigOption.DOWNLOADABLE_RETENTION_DAYS.key);
    }

    public List<Integer> getIpmWorksTypesConduite() {
        return config.getOptionAsList(ServiceConfigOption.IPMWORKS_TYPE_CONDUITE_IDS.key).getOptionAsInt();
    }

    public int getPerformanceSlowIndicatorThreshold() {
        return config.getOptionAsInt(ServiceConfigOption.PERFORMANCE_SLOW_INDICATOR_COMPUTE_THRESHOLD.key);
    }

    public int getPerformanceSlowContextThreshold() {
        return config.getOptionAsInt(ServiceConfigOption.PERFORMANCE_SLOW_CONTEXT_COMPUTE_THRESHOLD.key);
    }

    public void reload() {
        if (LOGGER.isWarnEnabled()) {
            LOGGER.warn("Reloading configuration...");
        }
        this.reloadConfiguration();
    }

    public enum ServiceConfigOption implements ConfigOptionDef {

//        FILENAME(ApplicationConfig.CONFIG_FILE_NAME, AGROSYST_DEFAULT_CONF_FILENAME),

        APPLICATION_VERSION("agrosyst.services.application.version", null),
        APPLICATION_BUILD_DATE("agrosyst.services.application.buildDate", null),
        APPLICATION_REVISION("agrosyst.services.application.revision", null),
        APPLICATION_BASE_URL("agrosyst.services.application.base_url", "https://agrosyst-latest.demo.codelutin.com"),
    
        LOWER_CAMPAIGN_BOUND("agrosyst.services.lowerCampaignBound", "2000"),
        UPPER_CAMPAIGN_BOUND( "agrosyst.services.upperCampaignBound","2999"),
        
        ASYNC_IMMEDIATE_POOL_SIZE( "agrosyst.services.asyncImmediatePoolSize","8"),
        
        DATABASE_DIALECT(Environment.DIALECT, H2Dialect.class.getName()),
        DATABASE_DRIVER(Environment.JAKARTA_JDBC_DRIVER, "org.h2.Driver"),
        DATABASE_URL(Environment.JAKARTA_JDBC_URL, "jdbc:h2:file:/tmp/agrosyst/h2data-web"),
        DATABASE_USER(Environment.JAKARTA_JDBC_USER , "sa"),
        DATABASE_PASS(Environment.JAKARTA_JDBC_USER, ""),
        //C3P0 POOL
        DATABASE_C3P0_MIN_SIZE(Environment.C3P0_MIN_SIZE, "5"),
        DATABASE_C3P0_MAX_SIZE(Environment.C3P0_MAX_SIZE, "20"),
        DATABASE_C3P0_TIMEOUT(Environment.C3P0_TIMEOUT, "600"),
        DATABASE_C3P0_MAX_STATEMENTS(Environment.C3P0_MAX_STATEMENTS, "50"),
        //HIKARICP POOL
        DATABASE_CONNECTION_PROVIDER(Environment.CONNECTION_PROVIDER, "org.hibernate.hikaricp.internal.HikariCPConnectionProvider"),
        DATABASE_HIKARICP_MIN_SIZE("hibernate.hikari.minimumIdle", "5"),
        DATABASE_HIKARICP_MAX_SIZE("hibernate.hikari.maximumPoolSize", "20"),
        DATABASE_HIKARICP_TIMEOUT("hibernate.hikari.idleTimeout", "600000"),
        //MIGRATION
        FLYWAY_SERVICE("topia.service.migration",TopiaFlywayServiceImpl.class.getName()),
        FLYWAY_IS_MODEL_VERSION_USED("topia.service.migration." + TopiaFlywayService.USE_MODEL_VERSION, "false"),
        FLYWAY_TABLE("topia.service.migration.flyway.table", "schema_version"),

        EMAIL_ENABLED("agrosyst.services.email.enabled", "true"),
        EMAIL_SMPT_HOST("agrosyst.services.email.smtp_host", "localhost"),
        EMAIL_SMPT_PORT("agrosyst.services.email.smtp_port", "25"),
        EMAIL_FROM("agrosyst.services.email.from", "no-reply+agrosyst@inrae.fr"),
        USER_FEEDBACK_EMAIL("agrosyst.services.email.feedback", "agrosyst-commits@list.forge.codelutin.com"),
        USER_SUPPORT_EMAIL("agrosyst.services.email.support", "agrosyst-support@inrae.fr"),

        DEMO_MODE("agrosyst.services.demoMode", "false"),
        GENERATE_BUILTIN_REFERENTIALS("agrosyst.services.generateBuiltinReferentials", "true"),
        GENERATE_BUILTIN_USERS("agrosyst.services.generateBuiltinUsers", "true"),
        SPECIES_GRAFTSUPPORT_CODESECTION("agrosyst.services.species.graftsupport.codesection", "26"),
        SPECIES_GRAFTSUPPORT_UTILISATION("agrosyst.services.species.graftsupport.utilisation", "PG"),
        SPECIES_GRAFTCLONE_TYPEVARIETAL("agrosyst.services.species.graftslone.typevarietal", "CLO"),
        TOKEN_STORAGE_ENABLED("agrosyst.services.security.tokenStorageEnabled", "false"),
        FILE_ENCODING("agrosyst.services.file.encoding", StandardCharsets.UTF_8.name()),
        LOG_FILE_LOCATION("agrosyst.services.log.file.location", null),
        BUSINESS_CACHING_ENABLED("agrosyst.services.businessCaching.enabled", "true"),
        BUSINESS_CACHING_DURATION("agrosyst.services.businessCaching.duration", "5"), // minutes
        BUSINESS_CACHING_SHORT_DURATION("agrosyst.services.businessCaching.shortDuration", "30"), // seconds

        ACTA_DOSAGE_SPC_CROPPING_ZONES_GROUP_ID("agrosyst.services.referential.acta_dosage_spc_complet.id_groupe_culture_zones_cultivees", "428"),
    
        DEPHYGRAPH_API_URL("agrosyst.web.dephyGraphApiUrl", null),
        DEPHYGRAPH_REDIRECT_URL("agrosyst.web.dephyGraphUrl", null),
        MAA_REFERENTIAL_URL("agrosyst.web.maaReferentialUrl", "https://alim.api.agriculture.gouv.fr/ift/v5/api/produits-doses-reference"),
        MAA_REFERENTIAL_ELEMENT_SIZE("agrosyst.web.maaReferentialNbElementsPerPageLoaded", "2000"),
        MAA_REFERENTIAL_NB_PAGE_TO_LOAD("agrosyst.web.maaReferentialNbPageToLoaded", "-1"),

        GENERIC_EQUIPMENT_MATERIAL_CODE("agrosyst.services.referential.ref_materiel_outil.default_code_value", "OUTI999999999"),
        GENERIC_TRACTOR_MATERIAL_CODE("agrosyst.services.referential.ref_materiel_traction.default_code_value", "TRAC999999999"),
        GENERIC_IRRIGATION_MATERIAL_CODE("agrosyst.services.referential.ref_materiel_irrigation.default_code_value", "IRRI999999999"),
        GENERIC_AUTOMOTEUR_MATERIAL_CODE("agrosyst.services.referential.ref_materiel_automoteur.default_code_value", "AUTO999999999"),

        DEFAULT_ARBO_ADVENTICE_ID_FOR_REPORT("agrosyst.services.referential.ref_adventice.default_arbo_adventice_id_for_report", "000000AEE504"),

        MAINTENANCE_MODE_ACTIVE("agrosyst.services.maintenanceMode.active", "false"),
        MAINTENANCE_MODE_MESSAGE("agrosyst.services.maintenanceMode.message", null),
        MAINTENANCE_MODE_DISCONNECT_ALL_USERS("agrosyst.services.maintenanceMode", "false"),

        JMS_URL("agrosyst.services.jmsUrl", null),

        DOWNLOADABLE_RETENTION_DAYS("agrosyst.services.downloadableRetentionDays", "7"),

        CRON_JOBS_ENABLED("agrosyst.services.cronJobsEnabled", "true"),

        IPMWORKS_TYPE_CONDUITE_IDS("ipmworks.typeConduitesIds", "1,19948"),

        PERFORMANCE_SLOW_INDICATOR_COMPUTE_THRESHOLD("fr.inra.agrosyst.services.performance.monitoring.slowIndicatorComputeThreshold", "1000"),

        PERFORMANCE_SLOW_CONTEXT_COMPUTE_THRESHOLD("fr.inra.agrosyst.services.performance.monitoring.slowContextComputeThreshold", "1000");

        private final String key;
        private final String defaultValue;

        ServiceConfigOption(String key, String defaultValue) {
            this.key = key;
            this.defaultValue = defaultValue;
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public Class<?> getType() {
            return null;
        }

        @Override
        public String getDescription() {
            return null;
        }

        @Override
        public boolean isTransient() {
            return false;
        }

        @Override
        public boolean isFinal() {
            return false;
        }

        @Override
        public void setDefaultValue(String defaultValue) {

        }

        @Override
        public void setTransient(boolean isTransient) {

        }

        @Override
        public void setFinal(boolean isFinal) {

        }
    }
}
