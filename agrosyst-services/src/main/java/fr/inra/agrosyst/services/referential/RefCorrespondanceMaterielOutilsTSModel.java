package fr.inra.agrosyst.services.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.TypeTravailSol;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTSImpl;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.referential.csv.AbstractAgrosystModel;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

public class RefCorrespondanceMaterielOutilsTSModel extends AbstractAgrosystModel<RefCorrespondanceMaterielOutilsTS> implements ExportModel<RefCorrespondanceMaterielOutilsTS> {

    private static final ValueParser<TypeTravailSol> TYPE_TRAVAIL_SOL_VALUE_PARSER = value ->
        (TypeTravailSol) getGenericEnumParser(TypeTravailSol.class, value.toUpperCase())
    ;

    private static final ValueFormatter<TypeTravailSol> TYPE_TRAVAIL_SOL_VALUE_FORMATTER = value -> {
        if (value == TypeTravailSol.LABOUR) {
            return "Labour";
        }
        return GENERIC_ENUM_FORMATTER.format(value);
    };


    protected RefCorrespondanceMaterielOutilsTSModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("type_materiel_1", RefCorrespondanceMaterielOutilsTS.PROPERTY_TYPE_MATERIEL_1);
        newMandatoryColumn("type_travail_sol", RefCorrespondanceMaterielOutilsTS.PROPERTY_TYPE_TRAVAIL_SOL, TYPE_TRAVAIL_SOL_VALUE_PARSER);
        newMandatoryColumn("desherbage_mecanique", RefCorrespondanceMaterielOutilsTS.PROPERTY_DESHERBAGE_MECANIQUE, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("source", RefCorrespondanceMaterielOutilsTS.PROPERTY_SOURCE);
        newMandatoryColumn("active", RefCorrespondanceMaterielOutilsTS.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefCorrespondanceMaterielOutilsTS, Object>> getColumnsForExport() {
        ModelBuilder<RefCorrespondanceMaterielOutilsTS> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("type_materiel_1", RefCorrespondanceMaterielOutilsTS.PROPERTY_TYPE_MATERIEL_1);
        modelBuilder.newColumnForExport("type_travail_sol", RefCorrespondanceMaterielOutilsTS.PROPERTY_TYPE_TRAVAIL_SOL, TYPE_TRAVAIL_SOL_VALUE_FORMATTER);
        modelBuilder.newColumnForExport("desherbage_mecanique", RefCorrespondanceMaterielOutilsTS.PROPERTY_DESHERBAGE_MECANIQUE, O_N_FORMATTER);
        modelBuilder.newColumnForExport("source", RefCorrespondanceMaterielOutilsTS.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport("active", RefCorrespondanceMaterielOutilsTS.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefCorrespondanceMaterielOutilsTS newEmptyInstance() {
        final RefCorrespondanceMaterielOutilsTSImpl ref = new RefCorrespondanceMaterielOutilsTSImpl();
        ref.setActive(true);
        return ref;
    }
}
