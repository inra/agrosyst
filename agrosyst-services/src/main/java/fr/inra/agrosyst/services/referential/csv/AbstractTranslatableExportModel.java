package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ext.AbstractExportModel;

import java.util.Map;

public abstract class AbstractTranslatableExportModel<E> extends AbstractExportModel<E> {

    public static final String COMMON_YES_ABBR = "common-yes-abbr";
    public static final String COMMON_NO_ABBR = "common-no-abbr";
    protected final Map<String, String> translations;

    public AbstractTranslatableExportModel(Map<String, String> translations) {
        super(AbstractAgrosystModel.CSV_SEPARATOR);
        this.translations = translations;
    }

    @Override
    public ExportableColumn<E, String> newColumnForExport(String property) {
        return newColumnForExport(translateColumnName(property), property);
    }

    @Override
    public <T> ExportableColumn<E, T> newColumnForExport(String property, ValueFormatter<T> formatter) {
        return newColumnForExport(translateColumnName(property), property, formatter);
    }

    public ValueFormatter<Boolean> getTranslated_O_N_Formatter() {
        return value -> value ? translations.get(COMMON_YES_ABBR) : translations.get(COMMON_NO_ABBR);
    }

    public String translateColumnName(String column) {
        return translations.get(getClass().getSimpleName() + "." + column);
    }

    public Map<String, String> getTranslations() {
        return translations;
    }
}
