package fr.inra.agrosyst.services.input;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.action.AbstractAbstractInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractAbstractPhytoProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.IrrigationInputUsage;
import fr.inra.agrosyst.api.entities.action.IrrigationInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.PhytoProductTarget;
import fr.inra.agrosyst.api.entities.action.PhytoProductTargetTopiaDao;
import fr.inra.agrosyst.api.entities.action.PotInputUsage;
import fr.inra.agrosyst.api.entities.action.PotInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SubstrateInputUsage;
import fr.inra.agrosyst.api.entities.action.SubstrateInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressorTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.domain.CroppingPlanEntryDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlans;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainIrrigationInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainMineralProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainOrganicProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainOtherProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPotInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedLotInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedSpeciesInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSubstrateInputDto;
import fr.inra.agrosyst.api.services.input.AbstractInputUsageDto;
import fr.inra.agrosyst.api.services.input.IrrigationInputUsageDto;
import fr.inra.agrosyst.api.services.input.MineralProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.OrganicProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.OtherProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.PhytoProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.PhytoProductTargetDto;
import fr.inra.agrosyst.api.services.input.PotInputUsageDto;
import fr.inra.agrosyst.api.services.input.SeedLotInputUsageDto;
import fr.inra.agrosyst.api.services.input.SeedSpeciesInputUsageDto;
import fr.inra.agrosyst.api.services.input.SubstrateInputUsageDto;
import fr.inra.agrosyst.api.services.practiced.PracticedDuplicateCropCyclesContext;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.domain.inputStock.DomainInputBinder;
import fr.inra.agrosyst.services.domain.inputStock.DomainInputStockUnitServiceImpl;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Setter
public class InputUsageServiceImpl extends AbstractAgrosystService implements AgrosystService {

    protected static final Log LOGGER = LogFactory.getLog(InputUsageServiceImpl.class);

    protected static final String VALIDATE_USAGE_ERROR_MESSAGE = "- Intrant du domaine manquant pour l'intrant '%s' de l'action de type '%s'";
    protected BiologicalProductInputUsageTopiaDao biologicalProductInputUsageDao;
    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    protected IrrigationInputUsageTopiaDao irrigationInputUsageDao;
    protected MineralProductInputUsageTopiaDao mineralProductInputUsageDao;
    protected OrganicProductInputUsageTopiaDao organicProductInputUsageDao;
    protected OtherProductInputUsageTopiaDao otherProductInputUsageDao;
    protected PesticideProductInputUsageTopiaDao pesticideProductInputUsageDao;
    protected PhytoProductTargetTopiaDao phytoProductTargetDao;
    protected PotInputUsageTopiaDao potInputUsageDao;
    protected RefBioAgressorTopiaDao refBioAgressorDao;
    protected RefEspeceTopiaDao refEspeceDao;
    protected SeedLotInputUsageTopiaDao seedLotInputUsageDao;
    protected SeedProductInputUsageTopiaDao seedProductInputUsageDao;
    protected SeedSpeciesInputUsageTopiaDao seedSpeciesInputUsageDao;
    protected SubstrateInputUsageTopiaDao substrateInputUsageDao;

    protected DomainInputStockUnitService domainInputStockUnitService;
    protected AgrosystI18nService i18nService;

    private DomainInputDto tryFindDomainInputByCodeByKeyOrForceNewOne(
            Map<String, DomainInputDto> domainInputDtoByCode,
            DomainInputDto domainInputDto,
            String toDomainId) {

        final String domainInputCode = domainInputDto.getCode();
        DomainInputDto targetedDomainInputDto = domainInputDtoByCode.get(domainInputCode);
        if (targetedDomainInputDto == null) {
            String key = domainInputDto.getKey();
            Optional<DomainInputDto> similarDomainInput = domainInputDtoByCode.values().stream().filter(di -> key.equals(di.getKey())).findAny();
            if (similarDomainInput.isPresent()) {
                targetedDomainInputDto = similarDomainInput.get();
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(String.format("Replacement domainPhytoProductInputDto with id %s and key %s with similar one %s",
                            targetedDomainInputDto.getTopiaId(),
                            targetedDomainInputDto.getTopiaId(),
                            key
                    ));
                }
            }

        }
        if (targetedDomainInputDto == null && domainInputDto.getTopiaId().isPresent()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format("Domain input with code %s not found on domain with id %s, will create new one", domainInputCode, toDomainId));
            }
            targetedDomainInputDto = domainInputDto.toBuilder()
                    .topiaId(null)
                    .toCopyFromInputId(domainInputDto.getTopiaId().get())
                    .toCopyToDomainId(toDomainId)
                    .build();
            domainInputDtoByCode.put(domainInputCode, targetedDomainInputDto);
        }

        return targetedDomainInputDto;
    }

    private String getEffectiveOrPracticedCropIdentifier(SeedLotInputUsageDto seedLotInputUsageDto, DomainSeedLotInputDto domainInputDto) {
        String croppingPlanEntryIdentifier = seedLotInputUsageDto.getCroppingPlanEntryIdentifier().contains(CroppingPlanEntry.class.getName()) ?
                domainInputDto.getCropSeedDto().getTopiaId() :
                domainInputDto.getCropSeedDto().getCode();
        return croppingPlanEntryIdentifier;
    }

    private String getEffectiveOrPracticedSpeciesIdentifier(SeedSpeciesInputUsageDto seedSpeciesInputUsageDto, DomainSeedSpeciesInputDto clonedDomainSeedSpeciesInputDto) {
        String croppingPlanEntryIdentifier = seedSpeciesInputUsageDto.getSpeciesIdentifier().contains(CroppingPlanSpecies.class.getName()) ?
                clonedDomainSeedSpeciesInputDto.getSpeciesSeedDto().getTopiaId() :
                clonedDomainSeedSpeciesInputDto.getSpeciesSeedDto().getCode();
        return croppingPlanEntryIdentifier;
    }

    protected DomainSeedLotInputDto tryDuplicateNewLotFromGivenOne(
            String toDomainId,
            DomainSeedLotInputDto domainSeedLotInputDto) {

        DomainSeedLotInputDto domainInputDto = null;

        String cropCode = domainSeedLotInputDto.getCropSeedDto().getCode();
        CroppingPlanEntry sameCrop = croppingPlanEntryDao.forProperties(
                        CroppingPlanEntry.PROPERTY_DOMAIN + "." + TopiaEntity.PROPERTY_TOPIA_ID, toDomainId,
                        CroppingPlanEntry.PROPERTY_CODE, cropCode)
                .findAnyOrNull();

        Optional<DomainSeedLotInput> fromDomainSeedLotEntity = Optional.empty();
        Optional<String> optionalLotId = domainSeedLotInputDto.getTopiaId();
        if (optionalLotId.isPresent()) {
            fromDomainSeedLotEntity = domainInputStockUnitService.getLotForId(optionalLotId.get());
        }

        if (sameCrop != null && fromDomainSeedLotEntity.isPresent()) {

            Map<String, CroppingPlanEntry> extendCroppingPlanByCodes = new HashMap<>();
            Map<String, CroppingPlanSpecies> extendCroppingPlanSpeciesByCodes = new HashMap<>();
            extendCroppingPlanByCodes.put(sameCrop.getCode(), sameCrop);
            CollectionUtils.emptyIfNull(sameCrop.getCroppingPlanSpecies()).forEach(cps -> extendCroppingPlanSpeciesByCodes.put(cps.getCode(), cps));

            DomainSeedLotInput clonedDomainInputEntity = domainInputStockUnitService.cloneAndPersistDomainSeedLotInput(
                    fromDomainSeedLotEntity.get().getDomain(),
                    sameCrop.getDomain(),
                    fromDomainSeedLotEntity.get(),
                    extendCroppingPlanByCodes,
                    extendCroppingPlanSpeciesByCodes
            );

            ReferentialTranslationMap translationMap = i18nService.fillCroppingPlanEntryTranslationMaps(List.of(sameCrop));
            i18nService.fillTranslationMap(List.of(clonedDomainInputEntity), translationMap);
            CroppingPlanEntryDto dtoForCroppingPlanEntry = CroppingPlans.getDtoForCroppingPlanEntry(sameCrop, translationMap);

            domainInputDto = (DomainSeedLotInputDto) DomainInputBinder.bindToDto(clonedDomainInputEntity, List.of(dtoForCroppingPlanEntry), translationMap);
        }
        return domainInputDto;
    }

    protected List<SeedSpeciesInputUsageDto> cloneSeedSpeciesInputUsageDtos(
            DomainSeedLotInputDto domainInputDto,
            Optional<List<SeedSpeciesInputUsageDto>> optionalSeedingSpeciesUsageDtos,
            String toDomainId) {

        if (optionalSeedingSpeciesUsageDtos.isPresent()) {

            Map<String, DomainInputDto> domainSeedSpeciesInputDtoByCodes = CollectionUtils.emptyIfNull(domainInputDto.getSpeciesInputs())
                    .stream().collect(Collectors.toMap(DomainInputDto::getCode, Function.identity()));

            List<SeedSpeciesInputUsageDto> seedingSpeciesDtos = new ArrayList<>();
            final List<SeedSpeciesInputUsageDto> seedSpeciesInputUsageDtos = optionalSeedingSpeciesUsageDtos.get();
            seedSpeciesInputUsageDtos.forEach(
                    seedSpeciesInputUsageDto -> {
                        DomainInputDto domainSeedSpeciesInputDto = seedSpeciesInputUsageDto.getDomainSeedSpeciesInputDto();
                        final DomainSeedSpeciesInputDto cloneDomainSeedSpeciesInputDto = (DomainSeedSpeciesInputDto) tryFindDomainInputByCodeByKeyOrForceNewOne(
                                domainSeedSpeciesInputDtoByCodes,
                                domainSeedSpeciesInputDto,
                                toDomainId);
                        if (cloneDomainSeedSpeciesInputDto != null) {

                            Optional<Collection<PhytoProductInputUsageDto>> seedProductInputDtos = seedSpeciesInputUsageDto.getSeedProductInputDtos();

                            List<PhytoProductInputUsageDto> phytoProductInputUsageDtos = cloneSpeciesPhytoProductInputUsageDtos(
                                    toDomainId,
                                    seedProductInputDtos,
                                    cloneDomainSeedSpeciesInputDto);

                            String speciesIdentifier = getEffectiveOrPracticedSpeciesIdentifier(seedSpeciesInputUsageDto, cloneDomainSeedSpeciesInputDto);
                            final SeedSpeciesInputUsageDto speciesInputUsageDto = seedSpeciesInputUsageDto.toBuilder()
                                    .speciesIdentifier(speciesIdentifier)
                                    .inputUsageTopiaId(null)
                                    .domainSeedSpeciesInputDto(cloneDomainSeedSpeciesInputDto)
                                    .seedProductInputDtos(phytoProductInputUsageDtos)
                                    .build();
                            seedingSpeciesDtos.add(speciesInputUsageDto);
                        }
                    }
            );
            return seedingSpeciesDtos;
        }
        return null;
    }

    protected List<PhytoProductInputUsageDto> cloneSpeciesPhytoProductInputUsageDtos(
            String toDomainId,
            Optional<Collection<PhytoProductInputUsageDto>> seedProductInputDtos,
            DomainSeedSpeciesInputDto cloneDomainSeedSpeciesInputDto) {

        List<PhytoProductInputUsageDto> phytoProductInputUsageDtos;
        if (seedProductInputDtos.isPresent()) {
            phytoProductInputUsageDtos = new ArrayList<>();
            seedProductInputDtos.get().forEach(phytoProductInputUsageDto -> {
                List<PhytoProductTargetDto> newPhytoProductTargetDtos = null;
                if (phytoProductInputUsageDto.getTargets().isPresent()) {
                    newPhytoProductTargetDtos = new ArrayList<>();
                    List<PhytoProductTargetDto> phytoProductTargetDtos = phytoProductInputUsageDto.getTargets().get();
                    for (PhytoProductTargetDto phytoProductTargetDto : phytoProductTargetDtos) {
                        newPhytoProductTargetDtos.add(phytoProductTargetDto.toBuilder()
                                .topiaId(null)
                                .build());
                    }
                }
                DomainPhytoProductInputDto domainPhytoProductInputDto = phytoProductInputUsageDto.getDomainPhytoProductInputDto();

                Collection<DomainPhytoProductInputDto> domainPhytoProductInputDtos = CollectionUtils.emptyIfNull(cloneDomainSeedSpeciesInputDto.getSpeciesPhytoInputDtos());
                Map<String, DomainInputDto> domainPhytoProductInputDtoByCodes = domainPhytoProductInputDtos.stream().collect(Collectors.toMap(DomainInputDto::getCode, Function.identity()));
                DomainPhytoProductInputDto phytoDomainInputDto = (DomainPhytoProductInputDto) tryFindDomainInputByCodeByKeyOrForceNewOne(domainPhytoProductInputDtoByCodes, domainPhytoProductInputDto, toDomainId);

                if (phytoDomainInputDto != null) {
                    PhytoProductInputUsageDto newTargetDto = phytoProductInputUsageDto.toBuilder()
                            .inputUsageTopiaId(null)
                            .domainPhytoProductInputDto(phytoDomainInputDto)
                            .targets(newPhytoProductTargetDtos)
                            .build();
                    phytoProductInputUsageDtos.add(newTargetDto);
                }
            });
        } else {
            phytoProductInputUsageDtos = null;
        }
        return phytoProductInputUsageDtos;
    }

    protected ImmutableList<PhytoProductTargetDto> getPhytoProductTargetDtos(Optional<List<PhytoProductTargetDto>> optionalTargets) {

        ImmutableList<PhytoProductTargetDto> clonedTargets = null;
        if (optionalTargets.isPresent()) {

            final List<PhytoProductTargetDto> clonedTargets_ = new ArrayList<>();
            final List<PhytoProductTargetDto> phytoProductTargetDtos = optionalTargets.get();
            phytoProductTargetDtos.forEach(phytoProductTargetDto -> {
                final PhytoProductTargetDto clonedPhytoProductTargetDto = phytoProductTargetDto.toBuilder()
                        .topiaId(null)
                        .build();
                clonedTargets_.add(clonedPhytoProductTargetDto);
            });

            clonedTargets = ImmutableList.copyOf(clonedTargets_);
        }

        return clonedTargets;

    }

    public void addExistingOrNewOtherProductInputDomainInput(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Optional<Collection<OtherProductInputUsageDto>> optionalOtherProductInputUsageDtos,
            String toDomainId) {

        optionalOtherProductInputUsageDtos.ifPresent(otherProductInputUsageDtos -> otherProductInputUsageDtos.forEach(
                otherProductInputUsageDto -> tryFindDomainInputByCodeByKeyOrForceNewOne(
                        domainInputDtoByCode,
                        otherProductInputUsageDto.getDomainOtherProductInputDto(), toDomainId)));
    }

    public void addExistingOrNewMineralProductInputUsageDomainInput(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Optional<Collection<MineralProductInputUsageDto>> optionalMineralProductInputUsageDtos,
            String toDomainId) {
        optionalMineralProductInputUsageDtos.ifPresent(mineralProductInputUsageDtos -> mineralProductInputUsageDtos.forEach(
                mineralProductInputUsageDto -> tryFindDomainInputByCodeByKeyOrForceNewOne(
                        domainInputDtoByCode,
                        mineralProductInputUsageDto.getDomainMineralProductInputDto(),
                        toDomainId)
        ));
    }

    public void addExistingOrNewPhytoProductInputUsageDomainInput(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Optional<Collection<PhytoProductInputUsageDto>> optionalLotProductInputDtos,
            String toDomainId) {

        optionalLotProductInputDtos.ifPresent(phytoProductInputUsageDtos -> phytoProductInputUsageDtos.forEach(
                phytoProductInputUsageDto -> tryFindDomainInputByCodeByKeyOrForceNewOne(
                        domainInputDtoByCode,
                        phytoProductInputUsageDto.getDomainPhytoProductInputDto(),
                        toDomainId)
        ));
    }

    public void addExistingOrNewPotInputUsageDomainInputDtos(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Optional<Collection<PotInputUsageDto>> optionalPotInputUsageDtos,
            String toDomainId) {

        optionalPotInputUsageDtos.ifPresent(potInputUsageDtos -> potInputUsageDtos.forEach(
                potInputUsageDto -> tryFindDomainInputByCodeByKeyOrForceNewOne(
                        domainInputDtoByCode,
                        potInputUsageDto.getDomainPotInputDto(),
                        toDomainId)));
    }

    public void addExistingOrNewSubstrateInputUsageDomainInput(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Optional<Collection<SubstrateInputUsageDto>> optionalSubstrateInputUsageDtos,
            String toDomainId) {
        optionalSubstrateInputUsageDtos.ifPresent(substrateInputUsageDtos -> substrateInputUsageDtos.forEach(
                substrateInputUsageDto -> tryFindDomainInputByCodeByKeyOrForceNewOne(
                        domainInputDtoByCode,
                        substrateInputUsageDto.getDomainSubstrateInputDto(),
                        toDomainId)));
    }

    public void addExistingOrNewOrganicProductInputUsageDomainInput(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Optional<Collection<OrganicProductInputUsageDto>> optionalOrganicProductInputUsageDtos,
            String toDomainId) {
        optionalOrganicProductInputUsageDtos.ifPresent(organicProductInputUsageDtos -> organicProductInputUsageDtos.forEach(
                organicProductInputUsageDto -> tryFindDomainInputByCodeByKeyOrForceNewOne(
                        domainInputDtoByCode,
                        organicProductInputUsageDto.getDomainOrganicProductInputDto(),
                        toDomainId)));
    }

    public void addExistingOrNewSeedLotInputInputUsageDtos(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Collection<SeedLotInputUsageDto> seedLotInputUsageDtos,
            String toDomainId) {

        for (SeedLotInputUsageDto seedLotInputUsageDto : seedLotInputUsageDtos) {
            DomainSeedLotInputDto domainSeedLotInputDto = seedLotInputUsageDto.getDomainSeedLotInputDto();

            final String domainInputCode = domainSeedLotInputDto.getCode();
            // best case
            DomainSeedLotInputDto domainInputDto = (DomainSeedLotInputDto) domainInputDtoByCode.get(domainInputCode);
            if (domainInputDto == null) {
                domainInputDto = DomainInputStockUnitServiceImpl.tryFindMatchingDomainSeedLotInput(domainSeedLotInputDto, domainInputDtoByCode.values());
            }
            // in case there is no lot for targeted Domain we try to create One
            if (domainInputDto == null &&
                    domainSeedLotInputDto.getTopiaId().isPresent() &&
                    domainSeedLotInputDto.getDomainId().isPresent() &&
                    toDomainId != null) {

                try {
                    domainInputDto = tryDuplicateNewLotFromGivenOne(toDomainId, domainSeedLotInputDto);

                    if (domainInputDto != null) domainInputDtoByCode.put(domainInputDto.getCode(), domainInputDto);

                } catch (Exception e) {
                    LOGGER.error("Trying to duplicate a seed lot failed with the following error:", e);
                }
            }

            if (domainInputDto == null) continue;

            // add species code as there are not into domainInputDtoByCode
            Map<String, DomainInputDto> domainSeedSpeciesInputDtoByCode = CollectionUtils.emptyIfNull(domainInputDto.getSpeciesInputs())
                    .stream().collect(Collectors.toMap(DomainSeedSpeciesInputDto::getCode, Function.identity()));
            domainInputDtoByCode.putAll(domainSeedSpeciesInputDtoByCode);

            CollectionUtils.emptyIfNull(domainInputDto.getSpeciesInputs())
                    .stream()
                    .map(DomainSeedSpeciesInputDto::getSpeciesPhytoInputDtos)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .forEach(dto -> domainInputDtoByCode.put(dto.getCode(), dto));

        }
    }

    public void addExistingOrNewIrrigationInputUsageDomainInput(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Optional<IrrigationInputUsageDto> optionalIrrigationInputUsageDto,
            String toDomainId) {
        optionalIrrigationInputUsageDto.ifPresent(irrigationInputUsageDto -> tryFindDomainInputByCodeByKeyOrForceNewOne(
                domainInputDtoByCode,
                irrigationInputUsageDto.getDomainIrrigationInputDto(),
                toDomainId));
    }

    public Collection<PotInputUsageDto> clonePotInputUsageDto(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Optional<Collection<PotInputUsageDto>> optionalPotInputUsageDtos) {

        if (optionalPotInputUsageDtos.isPresent()) {
            final Collection<PotInputUsageDto> clonedPotInputUsageDtos = new ArrayList<>();
            optionalPotInputUsageDtos.get().forEach(
                    potInputUsageDto -> {
                        final DomainInputDto domainInputDto = domainInputDtoByCode.get(potInputUsageDto.getDomainPotInputDto().getCode());
                        if (domainInputDto != null) {
                            final PotInputUsageDto clonedPotInputUsageDto = potInputUsageDto.toBuilder()
                                    .inputUsageTopiaId(null)
                                    .domainPotInputDto((DomainPotInputDto) domainInputDto)
                                    .build();
                            clonedPotInputUsageDtos.add(clonedPotInputUsageDto);
                        }
                    });
            return clonedPotInputUsageDtos;
        }
        return null;
    }

    public Collection<SubstrateInputUsageDto> cloneSubstrateInputUsageDto(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Optional<Collection<SubstrateInputUsageDto>> optionalSubstrateInputUsageDtos) {
        if (optionalSubstrateInputUsageDtos.isPresent()) {
            Collection<SubstrateInputUsageDto> clonedSubstrateInputUsageDtos = new ArrayList<>();

            optionalSubstrateInputUsageDtos.get().forEach(
                    substrateInputUsageDto -> {
                        final DomainInputDto domainInputDto = domainInputDtoByCode.get(substrateInputUsageDto.getDomainSubstrateInputDto().getCode());
                        if (domainInputDto != null) {
                            final SubstrateInputUsageDto clonedSubstrateInputUsageDto = substrateInputUsageDto.toBuilder()
                                    .inputUsageTopiaId(null)
                                    .domainSubstrateInputDto((DomainSubstrateInputDto) domainInputDto)
                                    .build();
                            clonedSubstrateInputUsageDtos.add(clonedSubstrateInputUsageDto);
                        }
                    });
            return clonedSubstrateInputUsageDtos;
        }
        return null;
    }

    public Collection<MineralProductInputUsageDto> cloneMineralProductInputUsageDtos(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Optional<Collection<MineralProductInputUsageDto>> optionalMineralProductInputUsageDtos) {

        List<MineralProductInputUsageDto> clonedMineralProductInputUsageDtos = null;
        if (optionalMineralProductInputUsageDtos.isPresent()) {
            clonedMineralProductInputUsageDtos = new ArrayList<>();
            for (MineralProductInputUsageDto mineralProductInputUsageDto : optionalMineralProductInputUsageDtos.get()) {
                final DomainInputDto domainInputDto = domainInputDtoByCode.get(mineralProductInputUsageDto.getDomainMineralProductInputDto().getCode());
                if (domainInputDto != null) {
                    final MineralProductInputUsageDto clonedMineralProductInputUsageDto = mineralProductInputUsageDto.toBuilder()
                            .inputUsageTopiaId(null)
                            .domainMineralProductInputDto((DomainMineralProductInputDto) domainInputDto)
                            .build();
                    clonedMineralProductInputUsageDtos.add(clonedMineralProductInputUsageDto);
                }
            }
        }
        return clonedMineralProductInputUsageDtos;
    }

    public Collection<PhytoProductInputUsageDto> clonePhytoProductInputUsageDtos(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Optional<Collection<PhytoProductInputUsageDto>> optionalLotProductInputDtos) {

        if (optionalLotProductInputDtos.isPresent()) {
            Collection<PhytoProductInputUsageDto> clonedPhytoProductInputUsageDtos = new ArrayList<>();
            optionalLotProductInputDtos.get().forEach(
                    phytoProductInputUsageDto -> {
                        final ImmutableList<PhytoProductTargetDto> clonedTargets = getPhytoProductTargetDtos(phytoProductInputUsageDto.getTargets());
                        final DomainInputDto domainInputDto = domainInputDtoByCode.get(phytoProductInputUsageDto.getDomainPhytoProductInputDto().getCode());
                        if (domainInputDto != null) {
                            final PhytoProductInputUsageDto productInputUsageDto = phytoProductInputUsageDto.toBuilder()
                                    .inputUsageTopiaId(null)
                                    .targets(clonedTargets)
                                    .domainPhytoProductInputDto((DomainPhytoProductInputDto) domainInputDto)
                                    .build();
                            clonedPhytoProductInputUsageDtos.add(productInputUsageDto);
                        }
                    }
            );
            return clonedPhytoProductInputUsageDtos;
        }
        return null;
    }

    public Collection<OrganicProductInputUsageDto> cloneOrganicProductInputUsageDtos(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Optional<Collection<OrganicProductInputUsageDto>> optionalOrganicProductInputUsageDtos) {

        if (optionalOrganicProductInputUsageDtos.isPresent()) {
            final Collection<OrganicProductInputUsageDto> clonedOrganicProductInputUsageDtos = new ArrayList<>();
            optionalOrganicProductInputUsageDtos.get().forEach(
                    organicProductInputUsageDto -> {
                        final DomainInputDto domainInputDto = domainInputDtoByCode.get(organicProductInputUsageDto.getDomainOrganicProductInputDto().getCode());
                        if (domainInputDto != null) {
                            final OrganicProductInputUsageDto clonedOrganicProductInputUsageDto = organicProductInputUsageDto.toBuilder()
                                    .inputUsageTopiaId(null)
                                    .domainOrganicProductInputDto((DomainOrganicProductInputDto) domainInputDto)
                                    .build();
                            clonedOrganicProductInputUsageDtos.add(clonedOrganicProductInputUsageDto);
                        }
                    });
            return clonedOrganicProductInputUsageDtos;
        }
        return null;
    }

    public IrrigationInputUsageDto cloneIrrigationInputUsageDto(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Optional<IrrigationInputUsageDto> optionalIrrigationInputUsageDto
    ) {
        IrrigationInputUsageDto clonedIrrigationInputUsageDto = null;
        if (optionalIrrigationInputUsageDto.isPresent()) {
            final IrrigationInputUsageDto irrigationInputUsageDto = optionalIrrigationInputUsageDto.get();
            final DomainInputDto domainInputDto = domainInputDtoByCode.get(irrigationInputUsageDto.getDomainIrrigationInputDto().getCode());
            if (domainInputDto != null) {
                clonedIrrigationInputUsageDto = irrigationInputUsageDto.toBuilder()
                        .inputUsageTopiaId(null)
                        .domainIrrigationInputDto((DomainIrrigationInputDto) domainInputDto)
                        .build();
            }
        }
        return clonedIrrigationInputUsageDto;
    }

    public Collection<OtherProductInputUsageDto> cloneOtherProductInputUsageDtos(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Optional<Collection<OtherProductInputUsageDto>> optionalOtherProductInputUsageDtos) {

        final Collection<OtherProductInputUsageDto> clonedOtherProductInputUsageDtos;
        if (optionalOtherProductInputUsageDtos.isPresent()) {
            clonedOtherProductInputUsageDtos = new ArrayList<>();

            for (OtherProductInputUsageDto otherProductInputUsageDto : optionalOtherProductInputUsageDtos.get()) {
                DomainInputDto domainInputDto = domainInputDtoByCode.get(otherProductInputUsageDto.getDomainOtherProductInputDto().getCode());

                if (domainInputDto != null) {
                    domainInputDtoByCode.put(domainInputDto.getCode(), domainInputDto);
                    final OtherProductInputUsageDto productInputUsageDto = otherProductInputUsageDto.toBuilder()
                            .inputUsageTopiaId(null)
                            .domainOtherProductInputDto((DomainOtherProductInputDto) domainInputDto)
                            .build();
                    clonedOtherProductInputUsageDtos.add(productInputUsageDto);
                }
            }

            return clonedOtherProductInputUsageDtos;
        }

        return null;
    }

    public Collection<SeedLotInputUsageDto> cloneSeedLotInputUsageDtos(
            Map<String, DomainInputDto> domainInputDtoByCode,
            Collection<SeedLotInputUsageDto> seedLotInputUsageDtos,
            String toDomainId) {

        Collection<SeedLotInputUsageDto> clonedSeedLotInputUsageDtos = new ArrayList<>();

        for (SeedLotInputUsageDto seedLotInputUsageDto : seedLotInputUsageDtos) {
            DomainSeedLotInputDto domainSeedLotInputDto = seedLotInputUsageDto.getDomainSeedLotInputDto();

            final String domainInputCode = domainSeedLotInputDto.getCode();
            // best case
            DomainSeedLotInputDto domainInputDto = (DomainSeedLotInputDto) domainInputDtoByCode.get(domainInputCode);
            if (domainInputDto == null) {
                domainInputDto = DomainInputStockUnitServiceImpl.tryFindMatchingDomainSeedLotInput(domainSeedLotInputDto, domainInputDtoByCode.values());
            }

            if (domainInputDto == null) continue;

            // add species code as there are not into domainInputDtoByCode
            Map<String, DomainInputDto> domainSeedSpeciesInputDtoByCode = CollectionUtils.emptyIfNull(domainInputDto.getSpeciesInputs())
                    .stream().collect(Collectors.toMap(DomainSeedSpeciesInputDto::getCode, Function.identity()));
            domainInputDtoByCode.putAll(domainSeedSpeciesInputDtoByCode);

            CollectionUtils.emptyIfNull(domainInputDto.getSpeciesInputs())
                    .stream()
                    .map(DomainSeedSpeciesInputDto::getSpeciesPhytoInputDtos)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .forEach(dto -> domainInputDtoByCode.put(dto.getCode(), dto));

            final List<SeedSpeciesInputUsageDto> clonedSeedSpeciesInputUsageDtos = cloneSeedSpeciesInputUsageDtos(
                    domainInputDto,
                    seedLotInputUsageDto.getSeedingSpeciesDtos(),
                    toDomainId);

            String croppingPlanEntryIdentifier = getEffectiveOrPracticedCropIdentifier(seedLotInputUsageDto, domainInputDto);

            final SeedLotInputUsageDto clonedSeedLotInputUsageDto = seedLotInputUsageDto.toBuilder()
                    .inputUsageTopiaId(null)
                    .croppingPlanEntryIdentifier(croppingPlanEntryIdentifier)
                    .domainSeedLotInputDto(domainInputDto)
                    .seedingSpeciesDtos(clonedSeedSpeciesInputUsageDtos)
                    .build();

            clonedSeedLotInputUsageDtos.add(clonedSeedLotInputUsageDto);

        }
        return clonedSeedLotInputUsageDtos;
    }

    public Optional<InputPersistanceResults<OtherProductInputUsage>> persistOrDeleteOtherProductInputUsages(Optional<Collection<OtherProductInputUsageDto>> optInputUsages, Collection<OtherProductInputUsage> actionInputUsages, Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        return persistOrDeleteInputUsages(optInputUsages, actionInputUsages, domainInputStockUnitByIds, otherProductInputUsageDao);
    }

    public Optional<InputPersistanceResults<MineralProductInputUsage>> persistOrDeleteMineralProductInputUsages(Optional<Collection<MineralProductInputUsageDto>> dtos, Collection<MineralProductInputUsage> entities, Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        return persistOrDeleteInputUsages(dtos, entities, domainInputStockUnitByIds, mineralProductInputUsageDao);
    }

    public Optional<InputPersistanceResults<SubstrateInputUsage>> persistOrDeleteSubstrateInputUsages(Optional<Collection<SubstrateInputUsageDto>> dtos, Collection<SubstrateInputUsage> entities, Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        return persistOrDeleteInputUsages(dtos, entities, domainInputStockUnitByIds, substrateInputUsageDao);
    }

    public Optional<InputPersistanceResults<PotInputUsage>> persistOrDeletePotInputUsages(Optional<Collection<PotInputUsageDto>> dtos, Collection<PotInputUsage> entities, Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        return persistOrDeleteInputUsages(dtos, entities, domainInputStockUnitByIds, potInputUsageDao);
    }

    public Optional<InputPersistanceResults<OrganicProductInputUsage>> persistOrDeleteOrganicProductInputUsages(Optional<Collection<OrganicProductInputUsageDto>> dtos, Collection<OrganicProductInputUsage> entities, Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        return persistOrDeleteInputUsages(dtos, entities, domainInputStockUnitByIds, organicProductInputUsageDao);
    }

    public IrrigationInputUsage persistOrDeleteIrrigationInputUsages(
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            IrrigationAction irrigationAction,
            Domain domain) {

        IrrigationInputUsage persistedIrrigUsage = irrigationAction.getIrrigationInputUsage();

        IrrigationInputUsage irrigationInputUsage;

        if (persistedIrrigUsage == null) {

            irrigationInputUsage = irrigationInputUsageDao.newInstance();
            irrigationInputUsage.setInputType(InputType.IRRIGATION);

        } else {
            irrigationInputUsage = persistedIrrigUsage;
        }

        Optional<AbstractDomainInputStockUnit> optionalAbstractDomainInputStockUnit = domainInputStockUnitByIds.values().stream()
                .filter(adis -> InputType.IRRIGATION.equals(adis.getInputType())).findAny();

        DomainIrrigationInput domainIrrigationInput = optionalAbstractDomainInputStockUnit.map(abstractDomainInputStockUnit ->
                (DomainIrrigationInput) abstractDomainInputStockUnit).orElseGet(() -> domainInputStockUnitService.newDomainIrrigationInput(domain));
        irrigationInputUsage.setDomainIrrigationInput(domainIrrigationInput);

        irrigationInputUsage.setComment(irrigationAction.getComment());
        irrigationInputUsage.setQtMin(irrigationAction.getWaterQuantityMin());
        irrigationInputUsage.setQtAvg(irrigationAction.getWaterQuantityAverage());
        irrigationInputUsage.setQtMed(irrigationAction.getWaterQuantityMedian());
        irrigationInputUsage.setQtMax(irrigationAction.getWaterQuantityMax());

        if (irrigationInputUsage.isPersisted()) {
            irrigationInputUsageDao.update(irrigationInputUsage);
        } else {
            irrigationInputUsageDao.create(irrigationInputUsage);
        }

        return irrigationInputUsage;
    }

    public Optional<InputPersistanceResults<PesticideProductInputUsage>> persistOrDeleteAbstractPhytoProductUsageInputUsages(Optional<Collection<PhytoProductInputUsageDto>> phytoProductInputUsageDtos, Collection<PesticideProductInputUsage> phytoProductInputUsages, Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        return persistOrDeleteAbstractPhytoProductUsageInputUsages(phytoProductInputUsageDtos, phytoProductInputUsages, domainInputStockUnitByIds, pesticideProductInputUsageDao);
    }

    /**/
    protected <PIU extends AbstractPhytoProductInputUsage> Optional<InputPersistanceResults<PIU>> persistOrDeleteAbstractPhytoProductUsageInputUsages(
            Optional<Collection<PhytoProductInputUsageDto>> dtos,
            Collection<PIU> entities,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            AbstractAbstractPhytoProductInputUsageTopiaDao<PIU> dao) {

        if (dtos.isEmpty()) {

            if (entities != null) {
                InputPersistanceResults<PIU> r = new InputPersistanceResults<>(
                        ImmutableList.of(),
                        ImmutableList.of(),
                        ImmutableList.copyOf(entities)
                );
                return Optional.of(r);
            }

            return Optional.empty();
        }

        ArrayList<PIU> inputUsagesCreated = new ArrayList<>();
        ArrayList<PIU> inputUsagesUpdated = new ArrayList<>();

        Map<String, PIU> persisterInputUsagesByIds =
                CollectionUtils.emptyIfNull(entities).stream()
                        .collect(Collectors.toMap(TopiaEntity::getTopiaId, Function.identity()));

        Collection<PhytoProductInputUsageDto> inputUsageDtos = dtos.get();
        inputUsageDtos.forEach(usageDto -> {

            PIU entity;
            if (usageDto.getInputUsageTopiaId().isPresent() && !usageDto.getInputUsageTopiaId().get().startsWith("NEW-USAGE")) {
                entity = persisterInputUsagesByIds.remove(usageDto.getInputUsageTopiaId().get());
                inputUsagesUpdated.add(entity);

                Preconditions.checkNotNull(entity, String.format("Requested PhytoProductInputUsage with id '%s' is not present in database", usageDto.getInputUsageTopiaId().get()));

                Map<String, PhytoProductTarget> targetByIds = CollectionUtils.emptyIfNull(entity.getTargets()).stream().collect(Collectors.toMap(TopiaEntity::getTopiaId, Function.identity()));
                if (usageDto.getTargets().isPresent()) {
                    List<PhytoProductTargetDto> targetDtos = usageDto.getTargets().get();

                    Map<String, RefBioAgressor> refBioAgressorByIds = Maps.uniqueIndex(refBioAgressorDao
                            .forTopiaIdIn(
                                    targetDtos.stream().map(PhytoProductTargetDto::getRefBioAgressorTargetId).collect(Collectors.toSet())
                            ).findAll(), TopiaEntity::getTopiaId);

                    persistPhytoTargrets(targetDtos, targetByIds, entity, refBioAgressorByIds);
                }

                targetByIds.values().forEach(entity::removeTargets);

            } else {
                entity = newEntityUsageInstance(dao);
                inputUsagesCreated.add(entity);

                entity.setInputType(usageDto.getInputType());


                if (usageDto.getTargets().isPresent()) {
                    List<PhytoProductTargetDto> targetDtos = usageDto.getTargets().get();

                    Map<String, RefBioAgressor> refBioAgressorByIds = Maps.uniqueIndex(refBioAgressorDao
                            .forTopiaIdIn(
                                    targetDtos.stream().map(PhytoProductTargetDto::getRefBioAgressorTargetId).collect(Collectors.toSet())
                            ).findAll(), TopiaEntity::getTopiaId);

                    Map<String, PhytoProductTarget> targetByIds = new HashMap<>();
                    persistPhytoTargrets(targetDtos, targetByIds, entity, refBioAgressorByIds);
                }

            }

            AbstractDomainInputStockUnit domainInput = getOrCreateDomainInputStockUnit(domainInputStockUnitByIds, usageDto.getDomainPhytoProductInputDto());

            entity.setDomainPhytoProductInput((DomainPhytoProductInput) domainInput);

            shallowBindAbstractInputUsageDtoToEntity(usageDto, entity);

        });

        Collection<PIU> removedPersistedInputUsages = persisterInputUsagesByIds.values();
        if (CollectionUtils.isNotEmpty(removedPersistedInputUsages)) {
            dao.deleteAll(removedPersistedInputUsages);
        }
        if (CollectionUtils.isNotEmpty(inputUsagesCreated)) {
            dao.createAll(inputUsagesCreated);
        }
        if (CollectionUtils.isNotEmpty(inputUsagesUpdated)) {
            dao.updateAll(inputUsagesUpdated);
        }

        InputPersistanceResults<PIU> result0 = new InputPersistanceResults<>(
                ImmutableList.copyOf(inputUsagesCreated),
                ImmutableList.copyOf(inputUsagesUpdated),
                ImmutableList.copyOf(removedPersistedInputUsages)
        );

        return Optional.of(result0);
    }

    private AbstractDomainInputStockUnit getOrCreateDomainInputStockUnit(
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            DomainInputDto inputDto) {

        String identifier = "?";
        AbstractDomainInputStockUnit domainInput = null;
        Optional<String> optDomainInputId = inputDto.getTopiaId();
        if (optDomainInputId.isPresent()) {
            identifier = optDomainInputId.get();
            domainInput = domainInputStockUnitByIds.get(identifier);
        }

        if (domainInput == null) throw new AgrosystTechnicalException(
                String.format("Input '%s', '%s' with id '%s' doesn't exists", inputDto.getInputType(), inputDto.getInputName(), identifier));
        return domainInput;
    }

    public SeedProductInputUsage createNewSeedProductInputUsage(DomainPhytoProductInput domainInput) {
        SeedProductInputUsage entity = seedProductInputUsageDao.newInstance();
        entity.setInputType(domainInput.getInputType());
        entity.setDomainPhytoProductInput(domainInput);
        seedProductInputUsageDao.create(entity);
        return entity;
    }

    public void updateSeedSpeciesInputUsages(Collection<SeedSpeciesInputUsage> updatedSeedSpeciesInputUsage) {
        seedSpeciesInputUsageDao.updateAll(updatedSeedSpeciesInputUsage);
    }

    private <PIU extends AbstractPhytoProductInputUsage> void persistPhytoTargrets(
            List<PhytoProductTargetDto> targetDtos,
            Map<String, PhytoProductTarget> targetByIds,
            PIU inputUsage,
            Map<String, RefBioAgressor> refBioAgressorByIds) {

        for (PhytoProductTargetDto targetDto : targetDtos) {
            PhytoProductTarget target;
            if (targetDto.getTopiaId().isPresent()) {
                target = targetByIds.remove(targetDto.getTopiaId().get());
                Preconditions.checkNotNull(target, String.format("Requested PhytoProductTarget with id '%s' is not present in database", targetDto.getTopiaId().get()));
            } else {
                target = phytoProductTargetDao.newInstance();
                inputUsage.addTargets(target);
            }
            if (Objects.nonNull(targetDto.getRefBioAgressorTargetId())) {
                RefBioAgressor refBioAgressor = refBioAgressorByIds.get(targetDto.getRefBioAgressorTargetId());
                Preconditions.checkNotNull(refBioAgressor, String.format("Requested RefBioAgressor with id '%s' is not present in database", targetDto.getRefBioAgressorTargetId()));
                target.setTarget(refBioAgressor);
                if (Objects.isNull(targetDto.getCategory())) {
                    target.setCategory(refBioAgressor.getReferenceParam());
                }
            }
            target.setCategory(targetDto.getCategory());
            target.setCodeGroupeCibleMaa(targetDto.getCodeGroupeCibleMaa());
        }

        if (CollectionUtils.isNotEmpty(inputUsage.getTargets())) {
            inputUsage.getTargets().stream().filter(t -> !t.isPersisted()).forEach(t -> phytoProductTargetDao.create(t));
            inputUsage.getTargets().stream().filter(TopiaEntity::isPersisted).forEach(t -> phytoProductTargetDao.update(t));
        }
    }

    public PhytoProductInputUsageDto bindPhytoProductInputUsageToDTO(AbstractPhytoProductInputUsage entity) {

        final DomainPhytoProductInput domainInput = entity.getDomainPhytoProductInput();
        DomainPhytoProductInputDto domainInputDto = DomainInputBinder.domainPhytoProductInputDtoBinder(domainInput);

        final PhytoProductInputUsageDto.PhytoProductInputUsageDtoBuilder<?, ?> builder = PhytoProductInputUsageDto.builder();
        bindAbstractInputUsageEntityToDTO(entity, builder, domainInput.getInputName(), domainInput.getCode());

        Collection<PhytoProductTarget> targets = CollectionUtils.emptyIfNull(entity.getTargets());

        Collection<PhytoProductTargetDto> targetDtos = targets.stream().map(t -> {
            RefBioAgressor refTarget = t.getTarget();
            PhytoProductTargetDto dto;
            if (refTarget != null) {
                String identifiantOrReference_code;
                if (refTarget.getTopiaId().startsWith(RefAdventice.class.getName())) {
                    identifiantOrReference_code = ((RefAdventice) refTarget).getIdentifiant();
                } else {
                    identifiantOrReference_code = ((RefNuisibleEDI) refTarget).getReference_code();
                }
                String targetId;
                if (refTarget.getTopiaId().startsWith(RefAdventice.class.getName())) {
                    targetId = ((RefAdventice) refTarget).getIdentifiant();
                } else {
                    targetId = String.valueOf(((RefNuisibleEDI) refTarget).getReference_id());
                }
                dto = PhytoProductTargetDto.builder()
                        .topiaId(t.getTopiaId())
                        .codeGroupeCibleMaa(t.getCodeGroupeCibleMaa())
                        .refBioAgressorTargetId(refTarget.getTopiaId())
                        .category(t.getCategory())
                        .refBioAggressorLabel(refTarget.getLabel())
                        .refBioAggressorReferenceParam(refTarget.getReferenceParam())
                        .identifiantOrReference_code(identifiantOrReference_code)
                        .targetId(targetId)
                        .build();
            } else {
                dto = PhytoProductTargetDto.builder()
                        .topiaId(t.getTopiaId())
                        .codeGroupeCibleMaa(t.getCodeGroupeCibleMaa())
                        .category(t.getCategory())
                        .build();
            }
            return dto;
        }).toList();

        PhytoProductInputUsageDto dto = builder
                .domainPhytoProductInputDto(domainInputDto)
                .targets(ImmutableList.copyOf(targetDtos))
                .build();

        return dto;
    }

    public MineralProductInputUsageDto bindMineralProductInputUsageEntityToDTO(MineralProductInputUsage entity) {

        final DomainMineralProductInput domainInput = entity.getDomainMineralProductInput();
        DomainMineralProductInputDto domainInputDto = DomainInputBinder.domainMineralProductInputEntityToDtoBinder(domainInput);

        final MineralProductInputUsageDto.MineralProductInputUsageDtoBuilder<?, ?> builder = MineralProductInputUsageDto.builder();
        bindAbstractInputUsageEntityToDTO(entity, builder, domainInput.getInputName(), domainInput.getCode());

        MineralProductInputUsageDto dto = builder
                .domainMineralProductInputDto(domainInputDto)
                .build();

        return dto;
    }

    public OtherProductInputUsageDto bindOtherProductInputUsageEntityToDTO(OtherProductInputUsage entity) {

        final DomainOtherInput domainInput = entity.getDomainOtherInput();
        DomainOtherProductInputDto domainInputDto = DomainInputBinder.domainOtherProductInputDtoBinder(domainInput);

        final OtherProductInputUsageDto.OtherProductInputUsageDtoBuilder<?, ?> builder = OtherProductInputUsageDto.builder();
        bindAbstractInputUsageEntityToDTO(entity, builder, domainInput.getInputName(), domainInput.getCode());

        OtherProductInputUsageDto dto = builder
                .domainOtherProductInputDto(domainInputDto)
                .build();

        return dto;
    }

    public SeedLotInputUsageDto bindSeedLotInputUsageEntityToDTO(SeedLotInputUsage entity, boolean isPracticed) {

        DomainSeedLotInput domainInput = entity.getDomainSeedLotInput();

        CroppingPlanEntry cpe = domainInput.getCropSeed();


        ReferentialTranslationMap translationMap = i18nService.fillCroppingPlanEntryTranslationMaps(List.of(cpe));

        DomainSeedLotInputDto domainSeedLotInputDto = DomainInputBinder.bindToSeedingDto(
                domainInput, Lists.newArrayList(CroppingPlans.getDtoForCroppingPlanEntry(cpe, translationMap)));

        Map<String, DomainSeedSpeciesInputDto> domainSeedSpeciesInputDtoByIds = domainSeedLotInputDto.getSpeciesInputs()
                .stream()
                .filter(dto -> dto.getTopiaId().isPresent())
                .collect(Collectors.toMap((dto -> dto.getTopiaId().get()), Function.identity()));

        Collection<SeedSpeciesInputUsageDto> seedingSpeciesDtos = CollectionUtils.emptyIfNull(entity.getSeedingSpecies())
                .stream().map(
                        e -> seedSpeciesInputUsageEntityToDTO(e, domainSeedSpeciesInputDtoByIds, domainInput.getUsageUnit(), isPracticed))
                .filter(Objects::nonNull).toList();

        final SeedLotInputUsageDto.SeedLotInputUsageDtoBuilder<?, ?> builder = SeedLotInputUsageDto.builder();
        bindAbstractInputUsageEntityToDTO(entity, builder, domainInput.getInputName(), domainInput.getCode());
        SeedLotInputUsageDto dto = builder
                .domainSeedLotInputDto(domainSeedLotInputDto)
                .deepness(entity.getDeepness())
                .croppingPlanEntryIdentifier(isPracticed ? cpe.getCode() : cpe.getTopiaId())
                .seedingSpeciesDtos(ImmutableList.copyOf(seedingSpeciesDtos))
                .build();

        return dto;
    }

    private SeedSpeciesInputUsageDto seedSpeciesInputUsageEntityToDTO(
            SeedSpeciesInputUsage entity,
            Map<String, DomainSeedSpeciesInputDto> domainSeedSpeciesInputDtoByIds,
            SeedPlantUnit usageUnit,
            boolean isPracticed) {

        DomainSeedSpeciesInputDto domainSeedSpeciesInputDto = domainSeedSpeciesInputDtoByIds.get(
                entity.getDomainSeedSpeciesInput().getTopiaId());

        if (domainSeedSpeciesInputDto == null) {
            return null;// non valid
        }
        final CroppingPlanSpeciesDto speciesSeedDto = domainSeedSpeciesInputDto.getSpeciesSeedDto();
        String identifier = isPracticed ? speciesSeedDto.getCode() : speciesSeedDto.getTopiaId();

        Collection<SeedProductInputUsage> seedProductInputUsages = CollectionUtils.emptyIfNull(entity.getSeedProductInputUsages());
        Collection<PhytoProductInputUsageDto> seedProductInputUsageDtos = seedProductInputUsages.stream().map(
                this::bindPhytoProductInputUsageToDTO).toList();

        final SeedSpeciesInputUsageDto.SeedSpeciesInputUsageDtoBuilder<?, ?> builder = SeedSpeciesInputUsageDto.builder();
        bindAbstractInputUsageEntityToDTO(entity, builder, domainSeedSpeciesInputDto.getInputName(), domainSeedSpeciesInputDto.getCode());
        SeedSpeciesInputUsageDto dto = builder
                .domainSeedSpeciesInputDto(domainSeedSpeciesInputDto)
                .usageUnit(usageUnit)
                .speciesIdentifier(identifier)
                .seedProductInputDtos(ImmutableList.copyOf(seedProductInputUsageDtos))
                .deepness(entity.getDeepness())
                .build();
        return dto;
    }

    public IrrigationInputUsageDto bindIrrigationActionEntityToDTO(IrrigationInputUsage entity) {

        DomainIrrigationInput domainInput = entity.getDomainIrrigationInput();
        DomainIrrigationInputDto domainIrrigationInputDto = DomainInputBinder.domainIrrigationInputDtoBinder(domainInput);

        final IrrigationInputUsageDto.IrrigationInputUsageDtoBuilder<?, ?> builder = IrrigationInputUsageDto.builder();
        bindAbstractInputUsageEntityToDTO(entity, builder, domainInput.getInputName(), domainInput.getCode());
        IrrigationInputUsageDto dto = builder
                .domainIrrigationInputDto(domainIrrigationInputDto)
                .build();

        return dto;
    }

    public OrganicProductInputUsageDto bindOrganicProductInputUsageEntityToDTO(OrganicProductInputUsage entity) {

        final DomainOrganicProductInput domainInput = entity.getDomainOrganicProductInput();
        DomainOrganicProductInputDto domainInputDto = DomainInputBinder.domainOrganicProductInputDtoBinder(domainInput);

        final OrganicProductInputUsageDto.OrganicProductInputUsageDtoBuilder<?, ?> builder = OrganicProductInputUsageDto.builder();
        bindAbstractInputUsageEntityToDTO(entity, builder, domainInput.getInputName(), domainInput.getCode());
        OrganicProductInputUsageDto dto = builder.domainOrganicProductInputDto(domainInputDto)
                .build();

        return dto;
    }

    public SubstrateInputUsageDto bindSubstrateInputUsageEntityToDTO(AbstractAction action, SubstrateInputUsage entity) {

        final DomainSubstrateInput domainInput = entity.getDomainSubstrateInput();
        DomainSubstrateInputDto domainInputDto = DomainInputBinder.domainSubstrateInputDtoBinder(domainInput);

        final SubstrateInputUsageDto.SubstrateInputUsageDtoBuilder<?, ?> builder = SubstrateInputUsageDto.builder();
        bindAbstractInputUsageEntityToDTO(entity, builder, domainInput.getInputName(), domainInput.getCode());

        SubstrateInputUsageDto dto = builder.domainSubstrateInputDto(domainInputDto)
                .actionId(action.getTopiaId())
                .build();

        return dto;
    }

    public PotInputUsageDto bindPotInputUsageToDTO(PotInputUsage entity) {

        final DomainPotInput domainInput = entity.getDomainPotInput();
        DomainPotInputDto domainInputDto = DomainInputBinder.domainPotInputDtoBinder(domainInput);

        final PotInputUsageDto.PotInputUsageDtoBuilder<?, ?> builder = PotInputUsageDto.builder();
        bindAbstractInputUsageEntityToDTO(entity, builder, domainInput.getInputName(), domainInput.getCode());

        PotInputUsageDto dto = builder.domainPotInputDto(domainInputDto)
                .build();

        return dto;
    }

    public <E extends AbstractInputUsage> void bindAbstractInputUsageEntityToDTO(
            E entity,
            AbstractInputUsageDto.AbstractInputUsageDtoBuilder<?, ?> builder,
            String inputName,
            String inputCode) {

        builder.inputUsageTopiaId(entity.getTopiaId())
                .inputType(entity.getInputType())
                .comment(entity.getComment())
                .qtMin(entity.getQtMin())
                .qtAvg(entity.getQtAvg())
                .qtMed(entity.getQtMed())
                .qtMax(entity.getQtMax())
                .code(inputCode)
                .productName(inputName);
    }

    protected <IU extends AbstractInputUsage> IU newEntityUsageInstance(AbstractAbstractInputUsageTopiaDao<IU> dao) {
        IU entity = dao.newInstance();
        return entity;
    }

    protected <IU extends AbstractInputUsage, DTO extends AbstractInputUsageDto> Optional<InputPersistanceResults<IU>> persistOrDeleteInputUsages(
            Optional<Collection<DTO>> dtos,
            Collection<IU> entities,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            AbstractAbstractInputUsageTopiaDao<IU> dao) {

        if (dtos.isEmpty()) {

            if (CollectionUtils.isNotEmpty(entities)) {
                InputPersistanceResults<IU> r = new InputPersistanceResults<>(
                        ImmutableList.of(),
                        ImmutableList.of(),
                        ImmutableList.copyOf(entities)
                );
                dao.deleteAll(entities);
                return Optional.of(r);
            }

            return Optional.empty();
        }

        Collection<DTO> inputUsageDtos = dtos.get();

        ArrayList<IU> inputUsagesCreated = new ArrayList<>();
        ArrayList<IU> inputUsagesUpdated = new ArrayList<>();

        Map<String, IU> persisterInputUsagesByIds = CollectionUtils.emptyIfNull(entities).stream().collect(Collectors.toMap(TopiaEntity::getTopiaId, Function.identity()));

        inputUsageDtos.forEach(dto -> {

            IU entity = dto.getInputUsageTopiaId().isPresent() && !dto.getInputUsageTopiaId().get().startsWith("NEW-USAGE") ? persisterInputUsagesByIds.remove(dto.getInputUsageTopiaId().get()) : newEntityUsageInstance(dao);

            if (entity == null) {
                throw new AgrosystTechnicalException(String.format("Requested InputUsage with id '%s' is not present in database", dto.getInputUsageTopiaId().get()));
            }

            if (entity.isPersisted()) {
                inputUsagesUpdated.add(entity);
            } else {
                inputUsagesCreated.add(entity);
            }

            bindInputUsageDtoToEntityFunction.apply(Pair.of(dto, entity), domainInputStockUnitByIds);
        });

        Collection<IU> deletedInputUsages = persisterInputUsagesByIds.values();
        if (CollectionUtils.isNotEmpty(deletedInputUsages)) {
            dao.deleteAll(deletedInputUsages);
        }
        Set<IU> newInputUsages = inputUsagesCreated.stream().filter(input -> !input.isPersisted()).collect(Collectors.toSet());
        if (CollectionUtils.isNotEmpty(newInputUsages)) {
            dao.createAll(newInputUsages);
        }
        Set<IU> updatedInputUsages = inputUsagesCreated.stream().filter(TopiaEntity::isPersisted).collect(Collectors.toSet());
        if (CollectionUtils.isNotEmpty(updatedInputUsages)) {
            dao.updateAll(updatedInputUsages);
        }

        InputPersistanceResults<IU> result = new InputPersistanceResults<>(
                ImmutableList.copyOf(inputUsagesCreated),
                ImmutableList.copyOf(inputUsagesUpdated),
                ImmutableList.copyOf(deletedInputUsages)
        );

        return Optional.of(result);
    }

    protected BiFunction<Pair<? extends AbstractInputUsageDto, ? extends AbstractInputUsage>, Map<String, AbstractDomainInputStockUnit>, ? extends AbstractInputUsage> bindInputUsageDtoToEntityFunction =
            (mdto_inputUsage, domainInputStockUnitByIds) -> {
                AbstractInputUsageDto dto = mdto_inputUsage.getLeft();
                AbstractInputUsage entity = mdto_inputUsage.getRight();
                InputType inputType = dto.getInputType();
                switch (inputType) {
                    case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                        return bindMineralProductInputUsageDtoToEntity((MineralProductInputUsageDto) dto, (MineralProductInputUsage) entity, domainInputStockUnitByIds);
                    }
                    case AUTRE -> {
                        return bindOtherProductInputUsageDtoToEntity((OtherProductInputUsageDto) dto, (OtherProductInputUsage) entity, domainInputStockUnitByIds);
                    }
                    case EPANDAGES_ORGANIQUES -> {
                        return bindOrganicProductInputUsageToEntity((OrganicProductInputUsageDto) dto, (OrganicProductInputUsage) entity, domainInputStockUnitByIds);
                    }
                    case IRRIGATION -> {
                        return bindIrrigationInputUsageToEntity((IrrigationInputUsageDto) dto, (IrrigationInputUsage) entity, domainInputStockUnitByIds);
                    }
                    case POT -> {
                        return bindPotInputUsageDtoToEntity((PotInputUsageDto) dto, (PotInputUsage) entity, domainInputStockUnitByIds);
                    }
                    case SUBSTRAT -> {
                        return bindSubstrateInputUsageDtoToEntity((SubstrateInputUsageDto) dto, (SubstrateInputUsage) entity, domainInputStockUnitByIds);
                    }
                    default -> throw new IllegalStateException("Unexpected value: " + inputType);
                }
            };

    protected void shallowBindAbstractInputUsageDtoToEntity(AbstractInputUsageDto mdto, AbstractInputUsage inputUsage) {
        inputUsage.setComment(mdto.getComment());
        inputUsage.setQtMin(mdto.getQtMin());
        inputUsage.setQtAvg(mdto.getQtAvg());
        inputUsage.setQtMed(mdto.getQtMed());
        inputUsage.setQtMax(mdto.getQtMax());
    }

    protected void shallowBindAbstractInputUsage(AbstractInputUsage from, AbstractInputUsage to) {
        to.setComment(from.getComment());
        to.setQtMin(from.getQtMin());
        to.setQtAvg(from.getQtAvg());
        to.setQtMed(from.getQtMed());
        to.setQtMax(from.getQtMax());
    }

    protected MineralProductInputUsage bindMineralProductInputUsageDtoToEntity(
            MineralProductInputUsageDto dto,
            MineralProductInputUsage entity,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {

        AbstractDomainInputStockUnit domainMineralProductInput = getOrCreateDomainInputStockUnit(domainInputStockUnitByIds, dto.getDomainMineralProductInputDto());

        entity.setDomainMineralProductInput((DomainMineralProductInput) domainMineralProductInput);
        entity.setInputType(InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);

        shallowBindAbstractInputUsageDtoToEntity(dto, entity);

        return entity;
    }

    protected OtherProductInputUsage bindOtherProductInputUsageDtoToEntity(
            OtherProductInputUsageDto dto,
            OtherProductInputUsage entity,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {

        AbstractDomainInputStockUnit domainInput = getOrCreateDomainInputStockUnit(domainInputStockUnitByIds, dto.getDomainOtherProductInputDto());
        entity.setDomainOtherInput((DomainOtherInput) domainInput);
        entity.setInputType(InputType.AUTRE);

        shallowBindAbstractInputUsageDtoToEntity(dto, entity);

        return entity;
    }


    protected SubstrateInputUsage bindSubstrateInputUsageDtoToEntity(SubstrateInputUsageDto dto, SubstrateInputUsage entity, Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {

        AbstractDomainInputStockUnit domainInput = getOrCreateDomainInputStockUnit(domainInputStockUnitByIds, dto.getDomainSubstrateInputDto());
        entity.setDomainSubstrateInput((DomainSubstrateInput) domainInput);
        entity.setInputType(InputType.SUBSTRAT);

        shallowBindAbstractInputUsageDtoToEntity(dto, entity);
        return entity;
    }

    protected PotInputUsage bindPotInputUsageDtoToEntity(PotInputUsageDto dto, PotInputUsage entity, Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {

        AbstractDomainInputStockUnit domainInput = getOrCreateDomainInputStockUnit(domainInputStockUnitByIds, dto.getDomainPotInputDto());
        entity.setDomainPotInput((DomainPotInput) domainInput);
        entity.setInputType(InputType.POT);

        shallowBindAbstractInputUsageDtoToEntity(dto, entity);
        return entity;
    }

    private OrganicProductInputUsage bindOrganicProductInputUsageToEntity(OrganicProductInputUsageDto dto, OrganicProductInputUsage entity, Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {

        AbstractDomainInputStockUnit domainInput = getOrCreateDomainInputStockUnit(domainInputStockUnitByIds, dto.getDomainOrganicProductInputDto());
        entity.setDomainOrganicProductInput((DomainOrganicProductInput) domainInput);
        entity.setInputType(InputType.EPANDAGES_ORGANIQUES);

        shallowBindAbstractInputUsageDtoToEntity(dto, entity);
        return entity;
    }

    private IrrigationInputUsage bindIrrigationInputUsageToEntity(IrrigationInputUsageDto dto, IrrigationInputUsage entity, Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {

        Optional<String> optDomainInputId = dto.getDomainIrrigationInputDto().getTopiaId();
        if (optDomainInputId.isEmpty()) {
            throw new AgrosystTechnicalException("DomainIrrigationInput must exist");
        }
        String identifier = optDomainInputId.get();
        DomainIrrigationInput domainInput = (DomainIrrigationInput) domainInputStockUnitByIds.get(identifier);
        if (domainInput == null) {
            throw new AgrosystTechnicalException(String.format("DomainIrrigationInput with id '%s' doesn't exists", identifier));
        }
        entity.setDomainIrrigationInput(domainInput);
        entity.setInputType(InputType.IRRIGATION);

        shallowBindAbstractInputUsageDtoToEntity(dto, entity);
        return entity;
    }

    public Optional<InputPersistanceResults<BiologicalProductInputUsage>> persistOrDeleteBiologicalProductInputUsages(Optional<Collection<PhytoProductInputUsageDto>> actionInputUsages, Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds, Collection<BiologicalProductInputUsage> biologicalProductInputs) {
        return persistOrDeleteAbstractPhytoProductUsageInputUsages(actionInputUsages, biologicalProductInputs, domainInputStockUnitByIds, biologicalProductInputUsageDao);
    }

    /**
     * @param seedLotInputUsageDtoToPersists The seed lot to persist
     * @param persistedSeedLotInputUsage     Persisted action seed lot usage
     * @param domainInputStockUnitByIds      The related domain input stocks
     * @param cropIdentifier                 Practiced: crop code, Effective crop topiaId
     * @param speciesIdentifiers             CroppingPanSpecies: Practiced: code, Effective Crop topiaId
     */
    public void persistOrDeleteSeedLotInputUsages(
            Collection<SeedLotInputUsageDto> seedLotInputUsageDtoToPersists,
            Collection<SeedLotInputUsage> persistedSeedLotInputUsage,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            String cropIdentifier,
            Set<String> speciesIdentifiers) {

        Map<String, SeedLotInputUsage> persisterInputUsagesByIds =
                persistedSeedLotInputUsage.stream()
                        .collect(Collectors.toMap(TopiaEntity::getTopiaId, Function.identity()));

        seedLotInputUsageDtoToPersists.forEach(dto -> {

            SeedLotInputUsage inputUsage;
            if (dto.getInputUsageTopiaId().isPresent() && !dto.getInputUsageTopiaId().get().startsWith("NEW-USAGE")) {
                inputUsage = persisterInputUsagesByIds.remove(dto.getInputUsageTopiaId().get());
            } else {
                inputUsage = newEntityUsageInstance(seedLotInputUsageDao);
                persistedSeedLotInputUsage.add(inputUsage);
            }

            if (inputUsage == null) {
                throw new AgrosystTechnicalException(String.format("Requested seedLotInputUsage with id '%s' is not present in database", dto.getInputUsageTopiaId().get()));
            }

            try {
                validateSeedLotInputUsageDto(dto, cropIdentifier, speciesIdentifiers);
                bindSeedLotInputUsageDtoToEntity(dto, inputUsage, domainInputStockUnitByIds);
            } catch (Exception e) {
                LOGGER.error(e);
            }

        });

        final Collection<SeedLotInputUsage> seedLotInputUsageToRemoves = persisterInputUsagesByIds.values();
        if (CollectionUtils.isNotEmpty(seedLotInputUsageToRemoves)) {
            for (SeedLotInputUsage seedLotInputUsageToRemove : seedLotInputUsageToRemoves) {
                final Collection<SeedSpeciesInputUsage> seedingSpecies = seedLotInputUsageToRemove.getSeedingSpecies();
                for (SeedSpeciesInputUsage seedingSpecy : seedingSpecies) {
                    seedingSpecy.clearSeedProductInputUsages();
                }
                seedLotInputUsageToRemove.clearSeedingSpecies();
            }
            seedLotInputUsageDao.deleteAll(seedLotInputUsageToRemoves);
        }
        persistedSeedLotInputUsage.removeAll(seedLotInputUsageToRemoves);
        Set<SeedLotInputUsage> newInputUsages = persistedSeedLotInputUsage.stream().filter(input -> !input.isPersisted()).collect(Collectors.toSet());
        if (CollectionUtils.isNotEmpty(newInputUsages)) {
            seedLotInputUsageDao.createAll(newInputUsages);
        }

        Set<SeedLotInputUsage> updatedInputUsages = persistedSeedLotInputUsage.stream().filter(TopiaEntity::isPersisted).collect(Collectors.toSet());
        if (CollectionUtils.isNotEmpty(updatedInputUsages)) {
            seedLotInputUsageDao.updateAll(updatedInputUsages);
        }
    }

    /**
     * Valid that crop and species exists on intervention
     *
     * @param dto                the dto to validate
     * @param cropIdentifier     Practiced: crop code, Effective crop topiaId
     * @param speciesIdentifiers CroppingPanSpecies: Practiced: code, Effective Crop topiaId
     */
    protected void validateSeedLotInputUsageDto(SeedLotInputUsageDto dto, String cropIdentifier, Set<String> speciesIdentifiers) {
        Preconditions.checkArgument(cropIdentifier.equals(dto.getCroppingPlanEntryIdentifier()), String.format("Intervention crop identifier %s doesn't match usage crop identifier %s", dto.getCroppingPlanEntryIdentifier(), cropIdentifier));
        if (dto.getSeedingSpeciesDtos().isPresent()) {
            for (SeedSpeciesInputUsageDto seedSpeciesInputUsageDto : dto.getSeedingSpeciesDtos().get()) {
                final String speciesIdentifier = seedSpeciesInputUsageDto.getSpeciesIdentifier();
                Preconditions.checkArgument(
                        speciesIdentifiers.contains(speciesIdentifier),
                        String.format("Intervention species '%s' not available on intervention species : %s",
                                speciesIdentifier,
                                String.join(", ", speciesIdentifiers)));
            }
        }
    }

    protected void bindSeedLotInputUsageDtoToEntity(
            SeedLotInputUsageDto dto,
            SeedLotInputUsage seedLotUsage,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {

        if (seedLotUsage.isPersisted()) {
            bindSeedLotInputUsageDtoToPersistedEntity(dto, seedLotUsage, domainInputStockUnitByIds);
        } else {
            bindSeedLotInputUsageDtoToNewEntity(dto, seedLotUsage, domainInputStockUnitByIds);
        }
    }

    protected void bindSeedLotInputUsageDtoToNewEntity(
            SeedLotInputUsageDto lotUsageDto,
            SeedLotInputUsage lotUsageEntity,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {

        final Optional<String> optionalTopiaId = lotUsageDto.getDomainSeedLotInputDto().getTopiaId();
        if (optionalTopiaId.isEmpty())
            throw new AgrosystTechnicalException("Unexpected not persisted domainSeedLotInput");

        final String domainSeedLotIdentifier = optionalTopiaId.get();
        final DomainSeedLotInput domainSeedLotInput = (DomainSeedLotInput) domainInputStockUnitByIds.get(domainSeedLotIdentifier);

        Preconditions.checkNotNull(domainSeedLotInput, String.format("Missing DomainSeedLotInput with id '%s'", domainSeedLotIdentifier));

        lotUsageEntity.setDomainSeedLotInput(domainSeedLotInput);
        lotUsageEntity.setDeepness(lotUsageDto.getDeepness());
        lotUsageEntity.setInputType(InputType.SEMIS);

        shallowBindAbstractInputUsageDtoToEntity(lotUsageDto, lotUsageEntity);

        double lotQtyUsed = 0;
        if (lotUsageDto.getSeedingSpeciesDtos().isPresent()) {

            Collection<DomainSeedSpeciesInput> domainSeedSpeciesInputs = CollectionUtils.emptyIfNull(domainSeedLotInput.getDomainSeedSpeciesInput());
            Map<String, DomainSeedSpeciesInput> domainSeedSpeciesInputByIds = Maps.uniqueIndex(domainSeedSpeciesInputs, TopiaEntity::getTopiaId);

            for (SeedSpeciesInputUsageDto speciesInputUsageDto : lotUsageDto.getSeedingSpeciesDtos().get()) {
                SeedSpeciesInputUsage speciesInputUsage = newEntityUsageInstance(seedSpeciesInputUsageDao);
                lotUsageEntity.addSeedingSpecies(speciesInputUsage);
                if (speciesInputUsageDto.getQtAvg() != null) {
                    lotQtyUsed += speciesInputUsageDto.getQtAvg();
                }
                speciesInputUsage.setInputType(InputType.SEMIS);

                DomainSeedSpeciesInputDto domainSeedSpeciesInputDto = speciesInputUsageDto.getDomainSeedSpeciesInputDto();
                final Optional<String> optionalSeedSpeciesInputTopiaId = domainSeedSpeciesInputDto.getTopiaId();
                if (optionalSeedSpeciesInputTopiaId.isEmpty())
                    throw new AgrosystTechnicalException("Missing topiaId to DomainSeedSpeciesInput Id for speciesInputUsage");
                final String identifier = optionalSeedSpeciesInputTopiaId.get();
                DomainSeedSpeciesInput domainSeedSpeciesInput = domainSeedSpeciesInputByIds.get(identifier);
                Preconditions.checkNotNull(domainSeedSpeciesInput, String.format("Expecting DomainSeedSpeciesInput with id '%s' is missing", identifier));

                speciesInputUsage.setDomainSeedSpeciesInput(domainSeedSpeciesInput);
                speciesInputUsage.setDeepness(speciesInputUsageDto.getDeepness());

                shallowBindAbstractInputUsageDtoToEntity(speciesInputUsageDto, speciesInputUsage);

                Optional<InputPersistanceResults<SeedProductInputUsage>> seedProductInputUsageEntities = persistOrDeleteAbstractPhytoProductUsageInputUsages(
                        speciesInputUsageDto.getSeedProductInputDtos(),
                        speciesInputUsage.getSeedProductInputUsages(),
                        domainInputStockUnitByIds,
                        seedProductInputUsageDao);
                seedProductInputUsageEntities.ifPresent(r -> speciesInputUsage.setSeedProductInputUsages(r.getInputUsagesCreated()));

            }
            if (CollectionUtils.isNotEmpty(lotUsageEntity.getSeedingSpecies())) {
                seedSpeciesInputUsageDao.createAll(lotUsageEntity.getSeedingSpecies());
            }
        }

        lotUsageEntity.setQtAvg(lotQtyUsed);

    }

    private void bindSeedLotInputUsageDtoToPersistedEntity(
            SeedLotInputUsageDto lotUsageDto,
            SeedLotInputUsage lotUsageEntity,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {

        final Optional<String> optionalTopiaId = lotUsageDto.getDomainSeedLotInputDto().getTopiaId();
        if (optionalTopiaId.isEmpty())
            throw new AgrosystTechnicalException("Unexpected not persisted domainSeedLotInput");

        final String domainSeedLotIdentifier = optionalTopiaId.get();
        final DomainSeedLotInput domainSeedLotInput = (DomainSeedLotInput) domainInputStockUnitByIds.get(domainSeedLotIdentifier);

        Preconditions.checkNotNull(domainSeedLotInput, String.format("Missing DomainSeedLotInput with id '%s'", domainSeedLotIdentifier));

        lotUsageEntity.setDomainSeedLotInput(domainSeedLotInput);

        Map<String, DomainSeedSpeciesInput> domainSeedSpeciesInputStockUnitByIds = new HashMap<>();
        CollectionUtils.emptyIfNull(domainSeedLotInput.getDomainSeedSpeciesInput())
                .forEach(dss -> domainSeedSpeciesInputStockUnitByIds.put(dss.getTopiaId(), dss));

        lotUsageEntity.setDeepness(lotUsageDto.getDeepness());

        shallowBindAbstractInputUsageDtoToEntity(lotUsageDto, lotUsageEntity);

        Map<String, SeedSpeciesInputUsage> seedSpeciesInputUsageByIds = CollectionUtils.emptyIfNull(lotUsageEntity.getSeedingSpecies()).stream()
                .collect(Collectors.toMap(TopiaEntity::getTopiaId, Function.identity()));

        double lotQtyUsed = 0;
        if (lotUsageDto.getSeedingSpeciesDtos().isPresent()) {
            for (SeedSpeciesInputUsageDto speciesInputUsageDto : lotUsageDto.getSeedingSpeciesDtos().get()) {

                DomainSeedSpeciesInput domainInputStockUnit = domainSeedSpeciesInputStockUnitByIds.get(speciesInputUsageDto.getDomainSeedSpeciesInputDto().getTopiaId().orElse(null));
                if (domainInputStockUnit == null) {
                    continue;
                }

                final Optional<String> inputUsageTopiaId = speciesInputUsageDto.getInputUsageTopiaId();
                SeedSpeciesInputUsage speciesInputUsage;
                if (inputUsageTopiaId.isPresent()) {
                    speciesInputUsage = seedSpeciesInputUsageByIds.remove(inputUsageTopiaId.get());
                } else {
                    speciesInputUsage = seedSpeciesInputUsageDao.newInstance();
                }

                speciesInputUsage.setDomainSeedSpeciesInput(domainInputStockUnit);
                speciesInputUsage.setInputType(InputType.SEMIS);

                Preconditions.checkNotNull(speciesInputUsage, "SeedSpeciesInputUsage not found");

                lotQtyUsed += speciesInputUsageDto.getQtAvg();
                speciesInputUsage.setDeepness(speciesInputUsageDto.getDeepness());

                shallowBindAbstractInputUsageDtoToEntity(speciesInputUsageDto, speciesInputUsage);

                Optional<InputPersistanceResults<SeedProductInputUsage>> seedProductInputs = persistOrDeleteAbstractPhytoProductUsageInputUsages(
                        speciesInputUsageDto.getSeedProductInputDtos(),
                        speciesInputUsage.getSeedProductInputUsages(),
                        domainInputStockUnitByIds,
                        seedProductInputUsageDao);

                seedProductInputs.ifPresent(r -> {
                    r.inputUsagesRemoved.forEach(speciesInputUsage::removeSeedProductInputUsages);
                    r.getInputUsagesCreated().forEach(speciesInputUsage::addSeedProductInputUsages);
                });

                if (speciesInputUsage.isPersisted()) {
                    seedSpeciesInputUsageDao.update(speciesInputUsage);
                } else {
                    seedSpeciesInputUsageDao.create(speciesInputUsage);
                    lotUsageEntity.addSeedingSpecies(speciesInputUsage);
                }
            }
        }
        lotUsageEntity.setQtAvg(lotQtyUsed);

        seedSpeciesInputUsageByIds.values().forEach(lotUsageEntity::removeSeedingSpecies);
        seedSpeciesInputUsageDao.deleteAll(seedSpeciesInputUsageByIds.values());
    }

    public SeedLotInputUsage cloneSeedLotInputUsageEntityToNewEntity(SeedLotInputUsage fromLotUsed, PracticedDuplicateCropCyclesContext duplicateContext) {

        SeedLotInputUsage clonedLotUsed = seedLotInputUsageDao.newInstance();
        Map<String, AbstractDomainInputStockUnit> clonedDomainInputStockUnitByCodes = duplicateContext.getDomainInputStockUnitByCodes();
        DomainSeedLotInput fromDomainSeedLotInput = fromLotUsed.getDomainSeedLotInput();
        String fromDomainSeedLotCode = fromDomainSeedLotInput.getCode();
        DomainSeedLotInput clonedDomainSeedLotInput = (DomainSeedLotInput) clonedDomainInputStockUnitByCodes.get(fromDomainSeedLotCode);

        if (clonedDomainSeedLotInput == null) {
            Map<String, Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>>> toSpeciesByCropCode = duplicateContext.getToSpeciesByCropCode();
            String cropCode = fromDomainSeedLotInput.getCropSeed().getCode();
            Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>> croppingPlanEntryMapPair = toSpeciesByCropCode.get(cropCode);
            if (croppingPlanEntryMapPair != null) {
                CroppingPlanEntry croppingPlanEntry = croppingPlanEntryMapPair.getLeft();
                Map<String, CroppingPlanEntry> croppingPlanEntryByCode = new HashMap<>();
                Map<String, CroppingPlanSpecies> croppingPlanSpeciesByCodes = croppingPlanEntryMapPair.getRight();

                croppingPlanEntryByCode.put(croppingPlanEntry.getCode(), croppingPlanEntry);

                clonedDomainSeedLotInput = domainInputStockUnitService.cloneDomainSeedLotInput(
                        fromDomainSeedLotInput.getDomain(),
                        duplicateContext.getToGrowingSystem().getGrowingPlan().getDomain(),
                        fromDomainSeedLotInput,
                        croppingPlanEntryByCode,
                        croppingPlanSpeciesByCodes);
                clonedDomainInputStockUnitByCodes.put(fromDomainSeedLotCode, clonedDomainSeedLotInput);
            }

        }

        clonedLotUsed.setDomainSeedLotInput(clonedDomainSeedLotInput);
        clonedLotUsed.setDeepness(fromLotUsed.getDeepness());
        clonedLotUsed.setInputType(InputType.SEMIS);

        shallowBindAbstractInputUsage(fromLotUsed, clonedLotUsed);

        Collection<SeedSpeciesInputUsage> seedingSpeciesSources = fromLotUsed.getSeedingSpecies();
        if (clonedDomainSeedLotInput != null && CollectionUtils.isNotEmpty(seedingSpeciesSources)) {

            Collection<DomainSeedSpeciesInput> clonedDomainSeedSpeciesInputs = CollectionUtils.emptyIfNull(clonedDomainSeedLotInput.getDomainSeedSpeciesInput());
            Map<String, DomainPhytoProductInput> speciesSeedDomainPhytoProductInputByCodes = clonedDomainSeedSpeciesInputs.stream()
                    .map(DomainSeedSpeciesInput::getSpeciesPhytoInputs)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toMap(DomainPhytoProductInput::getCode, Function.identity()));
            Map<String, DomainSeedSpeciesInput> clonedDomainSeedSpeciesInputByCodes = Maps.uniqueIndex(clonedDomainSeedSpeciesInputs, AbstractDomainInputStockUnit::getCode);
            duplicateContext.getDomainInputStockUnitByCodes().putAll(speciesSeedDomainPhytoProductInputByCodes);

            for (SeedSpeciesInputUsage speciesInputUsageSource : seedingSpeciesSources) {
                SeedSpeciesInputUsage clonedSpeciesInputUsage = newEntityUsageInstance(seedSpeciesInputUsageDao);
                clonedLotUsed.addSeedingSpecies(clonedSpeciesInputUsage);
                clonedSpeciesInputUsage.setInputType(InputType.SEMIS);

                DomainSeedSpeciesInput domainSeedSpeciesInputSource = speciesInputUsageSource.getDomainSeedSpeciesInput();
                final String domainSeedSpeciesInputCode = domainSeedSpeciesInputSource.getCode();
                DomainSeedSpeciesInput clonedDomainSeedSpeciesInput = clonedDomainSeedSpeciesInputByCodes.get(domainSeedSpeciesInputCode);
                Preconditions.checkNotNull(clonedDomainSeedSpeciesInput, String.format("Expecting DomainSeedSpeciesInput with code '%s' is missing", domainSeedSpeciesInputCode));

                clonedSpeciesInputUsage.setDomainSeedSpeciesInput(clonedDomainSeedSpeciesInput);
                clonedSpeciesInputUsage.setDeepness(speciesInputUsageSource.getDeepness());

                shallowBindAbstractInputUsage(speciesInputUsageSource, clonedSpeciesInputUsage);

                Collection<SeedProductInputUsage> clonedPhytoProductUsageInputUsages = cloneSeedProductInputUsages(
                        clonedDomainSeedSpeciesInput,
                        speciesInputUsageSource.getSeedProductInputUsages(),
                        duplicateContext,
                        seedProductInputUsageDao);


                clonedSpeciesInputUsage.setSeedProductInputUsages(clonedPhytoProductUsageInputUsages);

            }

            Collection<SeedSpeciesInputUsage> seedingSpecies = clonedLotUsed.getSeedingSpecies();
            if (CollectionUtils.isNotEmpty(seedingSpecies)) {
                seedSpeciesInputUsageDao.createAll(seedingSpecies);
            }
        }

        return clonedLotUsed;
    }

    protected Collection<SeedProductInputUsage> cloneSeedProductInputUsages(
            DomainSeedSpeciesInput clonedDomainSeedSpeciesInput,
            Collection<SeedProductInputUsage> origins,
            PracticedDuplicateCropCyclesContext duplicateCropCyclesContext,
            SeedProductInputUsageTopiaDao dao) {

        if (CollectionUtils.isEmpty(origins)) {

            return CollectionUtils.emptyCollection();
        }

        Domain toDomain = duplicateCropCyclesContext.getToGrowingSystem().getGrowingPlan().getDomain();
        ArrayList<SeedProductInputUsage> result = new ArrayList<>();
        Map<String, DomainPhytoProductInput> domainPhytoProductInputByCode = CollectionUtils.emptyIfNull(clonedDomainSeedSpeciesInput.getSpeciesPhytoInputs())
                .stream()
                .collect(Collectors.toMap(DomainPhytoProductInput::getCode, Function.identity()));


        origins.forEach(originInputUsage -> {

            SeedProductInputUsage clone = newEntityUsageInstance(dao);
            result.add(clone);

            DomainPhytoProductInput domainPhytoProductInput = originInputUsage.getDomainPhytoProductInput();
            String identifier = domainPhytoProductInput.getCode();
            DomainPhytoProductInput domainInput = domainPhytoProductInputByCode.get(identifier);

            if (domainInput == null) {

                final String phytoSeedKey = domainPhytoProductInput.getInputKey();
                Optional<DomainPhytoProductInput> optionalDomainInputWithSameKey = domainPhytoProductInputByCode.values().stream().filter(dis -> phytoSeedKey.equals(dis.getInputKey())).findAny();
                if (optionalDomainInputWithSameKey.isPresent()) {
                    domainInput = optionalDomainInputWithSameKey.get();
                }

                if (domainInput == null) {
                    LOGGER.debug("No DomainPhytoProductInput found for identifier " + identifier);

                    domainInput = domainInputStockUnitService.duplicateAndAddPhytoProductToDomainSeedSpecies(
                            toDomain,
                            clonedDomainSeedSpeciesInput,
                            domainPhytoProductInput,
                            duplicateCropCyclesContext.isSameGrowingSystemCode());
                }

            }
            clone.setDomainPhytoProductInput(domainInput);
            clone.setInputType(originInputUsage.getInputType());

            if (CollectionUtils.isNotEmpty(originInputUsage.getTargets())) {
                Collection<PhytoProductTarget> originTargets = originInputUsage.getTargets();
                clonePhytoTargrets(originTargets, clone);
            }

            shallowBindAbstractInputUsage(originInputUsage, clone);

        });

        if (CollectionUtils.isNotEmpty(result)) {
            dao.createAll(result);
        }


        return result;
    }

    protected <PIU extends AbstractPhytoProductInputUsage> Collection<PIU> clonePhytoProductUsageInputUsages(
            Collection<PIU> origins,
            PracticedDuplicateCropCyclesContext duplicateCropCyclesContext,
            AbstractAbstractPhytoProductInputUsageTopiaDao<PIU> dao) {

        if (CollectionUtils.isEmpty(origins)) {

            return CollectionUtils.emptyCollection();
        }

        Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByCodes = duplicateCropCyclesContext.getDomainInputStockUnitByCodes();
        Domain toDomain = duplicateCropCyclesContext.getToGrowingSystem().getGrowingPlan().getDomain();
        ArrayList<PIU> result = new ArrayList<>();

        origins.forEach(originInputUsage -> {

            PIU clone = newEntityUsageInstance(dao);
            result.add(clone);

            String identifier = originInputUsage.getDomainPhytoProductInput().getCode();
            DomainPhytoProductInput domainInput = (DomainPhytoProductInput) domainInputStockUnitByCodes.get(identifier);
            if (domainInput == null) {
                LOGGER.debug("No DomainPhytoProductInput found for identifier " + identifier);
                // domain input must be created
                domainInput = (DomainPhytoProductInput) domainInputStockUnitService.cloneDomainInputExceptSeedLot(
                        toDomain,
                        originInputUsage.getInputType(),
                        originInputUsage.getDomainPhytoProductInput(),
                        duplicateCropCyclesContext.isSameGrowingSystemCode());
                domainInputStockUnitByCodes.put(identifier, domainInput);
            }
            clone.setDomainPhytoProductInput(domainInput);
            clone.setInputType(originInputUsage.getInputType());

            if (CollectionUtils.isNotEmpty(originInputUsage.getTargets())) {
                Collection<PhytoProductTarget> originTargets = originInputUsage.getTargets();
                clonePhytoTargrets(originTargets, clone);
            }

            shallowBindAbstractInputUsage(originInputUsage, clone);

        });

        if (CollectionUtils.isNotEmpty(result)) {
            dao.createAll(result);
        }


        return result;
    }

    protected <PIU extends AbstractPhytoProductInputUsage> void clonePhytoTargrets(
            Collection<PhytoProductTarget> originTargets,
            PIU inputUsage) {

        for (PhytoProductTarget originTarget : originTargets) {
            PhytoProductTarget target;

            target = phytoProductTargetDao.newInstance();
            inputUsage.addTargets(target);

            target.setTarget(originTarget.getTarget());
            target.setCategory(originTarget.getCategory());
            target.setCodeGroupeCibleMaa(originTarget.getCodeGroupeCibleMaa());
        }

        inputUsage.getTargets().forEach(t -> phytoProductTargetDao.create(t));
    }

    public Collection<OtherProductInputUsage> cloneOtherProductInputUsages(Collection<OtherProductInputUsage> originInputUsages, PracticedDuplicateCropCyclesContext duplicateCropCyclesContext) {
        return cloneInputUsages(originInputUsages, duplicateCropCyclesContext, otherProductInputUsageDao);
    }

    public Collection<MineralProductInputUsage> cloneMineralProductInputUsages(Collection<MineralProductInputUsage> originInputUsages, PracticedDuplicateCropCyclesContext duplicateCropCyclesContext) {
        return cloneInputUsages(originInputUsages, duplicateCropCyclesContext, mineralProductInputUsageDao);
    }

    public Collection<SubstrateInputUsage> cloneSubstrateInputUsageDtos(Collection<SubstrateInputUsage> originInputUsages, PracticedDuplicateCropCyclesContext duplicateCropCyclesContext) {
        return cloneInputUsages(originInputUsages, duplicateCropCyclesContext, substrateInputUsageDao);
    }

    public Collection<PotInputUsage> clonePotInputUsages(Collection<PotInputUsage> originInputUsages, PracticedDuplicateCropCyclesContext duplicateCropCyclesContext) {
        return cloneInputUsages(originInputUsages, duplicateCropCyclesContext, potInputUsageDao);
    }

    public Collection<OrganicProductInputUsage> cloneOrganicProductInputUsages(Collection<OrganicProductInputUsage> originInputUsages, PracticedDuplicateCropCyclesContext duplicateCropCyclesContext) {
        return cloneInputUsages(originInputUsages, duplicateCropCyclesContext, organicProductInputUsageDao);
    }

    public Collection<BiologicalProductInputUsage> cloneBiologicalProductInputUsage(Collection<BiologicalProductInputUsage> originInputUsages, PracticedDuplicateCropCyclesContext duplicateCropCyclesContext) {
        return clonePhytoProductUsageInputUsages(originInputUsages, duplicateCropCyclesContext, biologicalProductInputUsageDao);
    }

    public Collection<PesticideProductInputUsage> clonePesticideProductInputUsages(Collection<PesticideProductInputUsage> originInputUsages, PracticedDuplicateCropCyclesContext duplicateCropCyclesContext) {
        return clonePhytoProductUsageInputUsages(originInputUsages, duplicateCropCyclesContext, pesticideProductInputUsageDao);
    }

    public IrrigationInputUsage cloneIrrigationInputUsages(IrrigationInputUsage originInputUsage, PracticedDuplicateCropCyclesContext duplicateCropCyclesContext) {

        if (originInputUsage == null) return null;

        IrrigationInputUsage result = null;

        Collection<IrrigationInputUsage> irrigationInputUsages = cloneInputUsages(Lists.newArrayList(originInputUsage), duplicateCropCyclesContext, irrigationInputUsageDao);

        if (CollectionUtils.isNotEmpty(irrigationInputUsages)) {
            result = irrigationInputUsages.iterator().next();
        }

        return result;

    }

    protected <IU extends AbstractInputUsage> Collection<IU> cloneInputUsages(
            Collection<IU> originInputUsages,
            PracticedDuplicateCropCyclesContext duplicateCropCyclesContext,
            AbstractAbstractInputUsageTopiaDao<IU> dao) {

        if (CollectionUtils.isEmpty(originInputUsages)) {

            return CollectionUtils.emptyCollection();
        }

        ArrayList<IU> inputUsagesCreated = new ArrayList<>();

        originInputUsages.forEach(originInputUsage -> {

            IU entity = newEntityUsageInstance(dao);

            inputUsagesCreated.add(entity);

            bindInputUsageFunction.apply(Pair.of(originInputUsage, entity), duplicateCropCyclesContext);
        });

        if (CollectionUtils.isNotEmpty(inputUsagesCreated)) {
            dao.createAll(inputUsagesCreated);
        }

        return inputUsagesCreated;
    }

    protected BiFunction<
            Pair<? extends AbstractInputUsage, ? extends AbstractInputUsage>, PracticedDuplicateCropCyclesContext,
            ? extends AbstractInputUsage> bindInputUsageFunction =
            (originToClone, duplicateCropCyclesContext) -> {
                Domain toDomain = duplicateCropCyclesContext.getToGrowingSystem().getGrowingPlan().getDomain();
                Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByCodes = duplicateCropCyclesContext.getDomainInputStockUnitByCodes();
                AbstractInputUsage origin = originToClone.getLeft();
                AbstractInputUsage clone = originToClone.getRight();
                InputType inputType = origin.getInputType();

                boolean isSameDomainCode = duplicateCropCyclesContext.isSameGrowingSystemCode();
                switch (inputType) {
                    case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                        return bindMineralProductInputUsage((MineralProductInputUsage) origin, (MineralProductInputUsage) clone, domainInputStockUnitByCodes, toDomain, isSameDomainCode);
                    }
                    case AUTRE -> {
                        return bindOtherProductInputUsage((OtherProductInputUsage) origin, (OtherProductInputUsage) clone, domainInputStockUnitByCodes, toDomain, isSameDomainCode);
                    }
                    case EPANDAGES_ORGANIQUES -> {
                        return bindOrganicProductInputUsage((OrganicProductInputUsage) origin, (OrganicProductInputUsage) clone, domainInputStockUnitByCodes, toDomain, isSameDomainCode);
                    }
                    case IRRIGATION -> {
                        return bindIrrigationInputUsage((IrrigationInputUsage) origin, (IrrigationInputUsage) clone, domainInputStockUnitByCodes, toDomain, isSameDomainCode);
                    }
                    case POT -> {
                        return bindPotInputUsage((PotInputUsage) origin, (PotInputUsage) clone, domainInputStockUnitByCodes, toDomain, isSameDomainCode);
                    }
                    case SUBSTRAT -> {
                        return bindSubstrateInputUsage((SubstrateInputUsage) origin, (SubstrateInputUsage) clone, domainInputStockUnitByCodes, toDomain, isSameDomainCode);
                    }
                    // others inputTypes are done in  clonePhytoProductUsageInputUsages() or cloneSeedLotInputUsageEntityToNewEntity()
                    default -> throw new IllegalStateException("Unexpected value: " + inputType);
                }

            };

    protected MineralProductInputUsage bindMineralProductInputUsage(
            MineralProductInputUsage origin,
            MineralProductInputUsage clone,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByCodes,
            Domain toDomain,
            boolean isSameDomainCode) {

        String domainInputCode = origin.getDomainMineralProductInput().getCode();
        DomainMineralProductInput domainMineralProductInput = (DomainMineralProductInput) domainInputStockUnitByCodes.get(domainInputCode);
        if (domainMineralProductInput == null) {
            domainMineralProductInput = (DomainMineralProductInput) domainInputStockUnitService.cloneDomainInputExceptSeedLot(
                    toDomain, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX, origin.getDomainMineralProductInput(), isSameDomainCode);
            domainInputStockUnitByCodes.put(domainInputCode, domainMineralProductInput);
        }
        clone.setDomainMineralProductInput(domainMineralProductInput);
        clone.setInputType(InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);

        shallowBindAbstractInputUsage(origin, clone);

        return clone;
    }

    protected OtherProductInputUsage bindOtherProductInputUsage(
            OtherProductInputUsage origin,
            OtherProductInputUsage clone,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByCodes,
            Domain toDomain,
            boolean isSameDomainCode) {

        String domainInputCode = origin.getDomainOtherInput().getCode();
        DomainOtherInput domainInput = (DomainOtherInput) domainInputStockUnitByCodes.get(domainInputCode);
        if (domainInput == null) {
            domainInput = (DomainOtherInput) domainInputStockUnitService.cloneDomainInputExceptSeedLot(
                    toDomain, InputType.AUTRE, origin.getDomainOtherInput(), isSameDomainCode);
            domainInputStockUnitByCodes.put(domainInputCode, domainInput);
        }
        clone.setDomainOtherInput(domainInput);
        clone.setInputType(InputType.AUTRE);

        shallowBindAbstractInputUsage(origin, clone);

        return clone;
    }

    protected SubstrateInputUsage bindSubstrateInputUsage(
            SubstrateInputUsage dto,
            SubstrateInputUsage entity,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByCodes,
            Domain toDomain,
            boolean isSameDomainCode) {

        String domainInputCode = dto.getDomainSubstrateInput().getCode();
        DomainSubstrateInput domainInput = (DomainSubstrateInput) domainInputStockUnitByCodes.get(domainInputCode);
        if (domainInput == null) {
            domainInput = (DomainSubstrateInput) domainInputStockUnitService.cloneDomainInputExceptSeedLot(
                    toDomain,
                    InputType.SUBSTRAT,
                    dto.getDomainSubstrateInput(),
                    isSameDomainCode);
            domainInputStockUnitByCodes.put(domainInputCode, domainInput);
        }
        entity.setDomainSubstrateInput(domainInput);
        entity.setInputType(InputType.SUBSTRAT);

        shallowBindAbstractInputUsage(dto, entity);
        return entity;
    }

    protected PotInputUsage bindPotInputUsage(
            PotInputUsage dto,
            PotInputUsage entity,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByCodes,
            Domain toDomain,
            boolean isSameDomainCode) {

        String domainInputCode = dto.getDomainPotInput().getCode();
        DomainPotInput domainPotInput = (DomainPotInput) domainInputStockUnitByCodes.get(domainInputCode);
        if (domainPotInput == null) {
            domainPotInput = (DomainPotInput) domainInputStockUnitService.cloneDomainInputExceptSeedLot(
                    toDomain,
                    InputType.POT,
                    dto.getDomainPotInput(),
                    isSameDomainCode);
            domainInputStockUnitByCodes.put(domainInputCode, domainPotInput);
        }
        entity.setDomainPotInput(domainPotInput);
        entity.setInputType(InputType.POT);

        shallowBindAbstractInputUsage(dto, entity);
        return entity;
    }

    private OrganicProductInputUsage bindOrganicProductInputUsage(
            OrganicProductInputUsage dto,
            OrganicProductInputUsage entity,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByCodes,
            Domain toDomain,
            boolean isSameDomainCode) {

        DomainOrganicProductInput domainOrganicProductInput = dto.getDomainOrganicProductInput();
        String domainInputCode = domainOrganicProductInput.getCode();
        DomainOrganicProductInput domainInput = (DomainOrganicProductInput) domainInputStockUnitByCodes.get(domainInputCode);
        if (domainInput == null) {
            domainInput = (DomainOrganicProductInput) domainInputStockUnitService.cloneDomainInputExceptSeedLot(
                    toDomain,
                    InputType.EPANDAGES_ORGANIQUES,
                    domainOrganicProductInput,
                    isSameDomainCode);
            domainInputStockUnitByCodes.put(domainInputCode, domainInput);
        }
        entity.setDomainOrganicProductInput(domainInput);
        entity.setInputType(InputType.EPANDAGES_ORGANIQUES);

        shallowBindAbstractInputUsage(dto, entity);
        return entity;
    }

    private IrrigationInputUsage bindIrrigationInputUsage(
            IrrigationInputUsage dto,
            IrrigationInputUsage entity,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByCodes,
            Domain toDomain,
            boolean isSameDomainCode) {

        String domainInputCode = dto.getDomainIrrigationInput().getCode();
        DomainIrrigationInput domainInput = (DomainIrrigationInput) domainInputStockUnitByCodes.get(domainInputCode);
        if (domainInput == null) {
            domainInput = (DomainIrrigationInput) domainInputStockUnitService.cloneDomainInputExceptSeedLot(
                    toDomain,
                    InputType.IRRIGATION,
                    dto.getDomainIrrigationInput(),
                    isSameDomainCode);
            domainInputStockUnitByCodes.put(domainInputCode, domainInput);
        }
        entity.setDomainIrrigationInput(domainInput);
        entity.setInputType(InputType.IRRIGATION);

        shallowBindAbstractInputUsage(dto, entity);
        return entity;
    }


    public Collection<String> validateOtherInputUsages(
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Optional<Collection<OtherProductInputUsageDto>> optionalOtherProductInputUsageDtos,
            RefInterventionAgrosystTravailEDI mainAction) {

        Collection<String> errorMessages = validateProductInputUsage(
                domainInputStockUnitByIds, optionalOtherProductInputUsageDtos, mainAction, OtherProductInputUsageDto::getDomainOtherProductInputDto);

        return errorMessages;
    }

    public Collection<String> validateSubstrateInputUsages(
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Optional<Collection<SubstrateInputUsageDto>> optionalSubstrateInputUsageDtos,
            RefInterventionAgrosystTravailEDI mainAction) {

        Collection<String> errorMessages = validateProductInputUsage(
                domainInputStockUnitByIds, optionalSubstrateInputUsageDtos, mainAction, SubstrateInputUsageDto::getDomainSubstrateInputDto);

        return errorMessages;
    }

    public Collection<String> validatePotInputUsages(
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Optional<Collection<PotInputUsageDto>> optionalPotInputUsageDtos,
            RefInterventionAgrosystTravailEDI mainAction) {

        Collection<String> errorMessages = validateProductInputUsage(
                domainInputStockUnitByIds, optionalPotInputUsageDtos, mainAction, PotInputUsageDto::getDomainPotInputDto);

        return errorMessages;
    }


    public Collection<String> validatePytoProductInputUsages(
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Optional<Collection<PhytoProductInputUsageDto>> optionalPhytoProductInputUsageDtos,
            RefInterventionAgrosystTravailEDI mainAction) {

        Collection<String> errorMessages = validateProductInputUsage(
                domainInputStockUnitByIds, optionalPhytoProductInputUsageDtos, mainAction, PhytoProductInputUsageDto::getDomainPhytoProductInputDto);

        return errorMessages;
    }

    private void validDomainInputRelation(
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            List<String> errorMessages,
            String actionName,
            DomainInputDto domainInputDto) {

        Optional<String> domainInputId = domainInputDto.getTopiaId();
        if (domainInputId.isEmpty()) {
            String inputTypeName = AgrosystI18nService.getEnumTraductionWithDefaultLocale(domainInputDto.getInputType());
            errorMessages.add(String.format(VALIDATE_USAGE_ERROR_MESSAGE, inputTypeName, actionName));
        } else if (domainInputStockUnitByIds.get(domainInputId.get()) == null) {
            String inputTypeName = AgrosystI18nService.getEnumTraductionWithDefaultLocale(domainInputDto.getInputType());
            errorMessages.add(String.format(VALIDATE_USAGE_ERROR_MESSAGE, inputTypeName, actionName));
        }
    }


    public Collection<String> validateSeedLotInputUsages(
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            SeedLotInputUsageDto seedLotInputUsageDto,
            RefInterventionAgrosystTravailEDI mainAction,
            Set<String> itkSpeciesCodes) {

        List<String> errorMessages = new ArrayList<>();
        String actionName = getActionName(mainAction);
        DomainSeedLotInputDto domainInputDto = seedLotInputUsageDto.getDomainSeedLotInputDto();
        validDomainInputRelation(domainInputStockUnitByIds, errorMessages, actionName, domainInputDto);

        Optional<List<SeedSpeciesInputUsageDto>> seedingSpeciesUsageDtos = seedLotInputUsageDto.getSeedingSpeciesDtos();
        if (seedingSpeciesUsageDtos.isPresent() && CollectionUtils.isNotEmpty(seedingSpeciesUsageDtos.get())) {
            for (SeedSpeciesInputUsageDto speciesInputUsageDto : seedingSpeciesUsageDtos.get()) {
                String speciesCode = speciesInputUsageDto.getDomainSeedSpeciesInputDto().getSpeciesSeedDto().getCode();
                if (!itkSpeciesCodes.contains(speciesCode)) {
                    errorMessages.add(String.format("Species with code '%s' not part of the itk.",
                            speciesCode));
                }

                Optional<Collection<PhytoProductInputUsageDto>> seedProductInputUsageDtos = speciesInputUsageDto.getSeedProductInputDtos();
                if (seedProductInputUsageDtos.isPresent() && CollectionUtils.isNotEmpty(seedProductInputUsageDtos.get())) {
                    for (PhytoProductInputUsageDto speciesDomainInputUsageDto : seedProductInputUsageDtos.get()) {
                        validDomainInputRelation(domainInputStockUnitByIds, errorMessages, actionName, speciesDomainInputUsageDto.getDomainPhytoProductInputDto());
                    }
                }
            }
        }

        return errorMessages;
    }


    public Collection<String> validateMineralProductInputUsage(
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Optional<Collection<MineralProductInputUsageDto>> optionalMineralProductInputUsageDtos,
            RefInterventionAgrosystTravailEDI mainAction) {

        Collection<String> errorMessages = validateProductInputUsage(
                domainInputStockUnitByIds, optionalMineralProductInputUsageDtos, mainAction, MineralProductInputUsageDto::getDomainMineralProductInputDto);

        return errorMessages;
    }


    public Collection<String> validateOrganicProductInputUsage(
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Optional<Collection<OrganicProductInputUsageDto>> organicProductInputUsageDtos,
            RefInterventionAgrosystTravailEDI mainAction) {

        Collection<String> errorMessages = validateProductInputUsage(
                domainInputStockUnitByIds, organicProductInputUsageDtos, mainAction, OrganicProductInputUsageDto::getDomainOrganicProductInputDto);

        return errorMessages;
    }

    public <IU_DTO extends AbstractInputUsageDto, DI_DTO extends DomainInputDto> Collection<String> validateProductInputUsage(
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Optional<Collection<IU_DTO>> optionalInputUsageDtos,
            RefInterventionAgrosystTravailEDI mainAction,
            Function<IU_DTO, DI_DTO> getDomainInputDto
    ) {
        List<String> errorMessages = new ArrayList<>();
        if (optionalInputUsageDtos.isPresent()) {
            String actionName = getActionName(mainAction);
            Collection<IU_DTO> inputUsageDtos = optionalInputUsageDtos.get();
            for (IU_DTO inputUsageDto : inputUsageDtos) {
                DomainInputDto domainInputDto = getDomainInputDto.apply(inputUsageDto);
                validDomainInputRelation(domainInputStockUnitByIds, errorMessages, actionName, domainInputDto);
            }

        }
        return errorMessages;
    }

    private String getActionName(RefInterventionAgrosystTravailEDI mainAction) {
        AgrosystInterventionType interventionType = mainAction.getIntervention_agrosyst();
        final String mainActionToActionSeparator = " / ";
        final String pattern = "%s %s %s";
        String mainActionName = AgrosystI18nService.getEnumTraductionWithDefaultLocale(interventionType);
        final String actionName = mainAction.getReference_label();
        return String.format(pattern, mainActionName, mainActionToActionSeparator, actionName);
    }

    public void deleteIrrigationInputUsage(IrrigationInputUsage entity) {
        irrigationInputUsageDao.delete(entity);
    }

    public Collection<SeedLotInputUsage> getLotInputUsages(List<DomainSeedLotInput> persistedLotsWithNewDomainSeedInputProducts) {
        List<SeedLotInputUsage> seedLotInputUsages = seedLotInputUsageDao.forDomainSeedLotInputIn(persistedLotsWithNewDomainSeedInputProducts).findAll();
        return seedLotInputUsages;
    }

    public void removeSeedingProductUsageForDomainSeedingProduct(
            DomainPhytoProductInput domainPhytoProductInputToRemove,
            MultiValuedMap<DomainPhytoProductInput, SeedingProductUsageToRemoveForDomainSeedingProduct> usageToRemovesForDomainSeedProduct) {

        if (domainPhytoProductInputToRemove != null) {
            Collection<SeedingProductUsageToRemoveForDomainSeedingProduct> seedingProductUsageToRemoveForDomainSeedingProducts = usageToRemovesForDomainSeedProduct.get(domainPhytoProductInputToRemove);
            for (SeedingProductUsageToRemoveForDomainSeedingProduct seedingProductUsageToRemoveForDomainSeedingProduct : seedingProductUsageToRemoveForDomainSeedingProducts) {
                SeedProductInputUsage seedProductInputUsage = seedingProductUsageToRemoveForDomainSeedingProduct.seedProductInputUsage();
                SeedSpeciesInputUsage seedSpeciesInputUsage = seedingProductUsageToRemoveForDomainSeedingProduct.seedSpeciesInputUsage();
                seedSpeciesInputUsage.removeSeedProductInputUsages(seedProductInputUsage);
                seedSpeciesInputUsageDao.update(seedSpeciesInputUsage);
                seedProductInputUsageDao.delete(seedProductInputUsage);
            }

        }
    }

    public List<SeedingProductUsageToRemoveForDomainSeedingProduct> getSeedingProductUsageToRemoveForDomainSeedingProduct(
            DomainSeedSpeciesInput speciesInputEntity,
            DomainPhytoProductInput domainPhytoProductInputToRemove) {

        List<SeedingProductUsageToRemoveForDomainSeedingProduct> result = new ArrayList<>();
        if (speciesInputEntity != null && domainPhytoProductInputToRemove != null) {
            List<SeedProductInputUsage> seedProductInputUsages = seedProductInputUsageDao.forDomainPhytoProductInputEquals(domainPhytoProductInputToRemove).findAll();
            if (CollectionUtils.isNotEmpty(seedProductInputUsages)) {
                List<SeedSpeciesInputUsage> seedSpeciesInputUsages = seedSpeciesInputUsageDao.forDomainSeedSpeciesInputEquals(speciesInputEntity).findAll();
                for (SeedSpeciesInputUsage seedSpeciesInputUsage : seedSpeciesInputUsages) {
                    for (SeedProductInputUsage seedProductInputUsage : seedProductInputUsages) {
                        if (seedSpeciesInputUsage.containsSeedProductInputUsages(seedProductInputUsage)) {
                            result.add(new SeedingProductUsageToRemoveForDomainSeedingProduct(seedProductInputUsage, seedSpeciesInputUsage));
                        }
                    }
                }
            }
        }
        return result;
    }

    public record SeedingProductUsageToRemoveForDomainSeedingProduct(SeedProductInputUsage seedProductInputUsage,
                                                                     SeedSpeciesInputUsage seedSpeciesInputUsage) {
    }
}
