package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMin;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMinImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefPrixFertiMinModel extends AbstractDestinationAndPriceModel<RefPrixFertiMin> implements ExportModel<RefPrixFertiMin> {

    public RefPrixFertiMinModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("CATEG", RefPrixFertiMin.PROPERTY_CATEG, INT_PARSER);
        newMandatoryColumn("FORME", RefPrixFertiMin.PROPERTY_FORME);
        newMandatoryColumn("Élement", RefPrixFertiMin.PROPERTY_ELEMENT, FERTI_MIN_ELEMENT_PARSER);
        newMandatoryColumn("Prix", RefPrixFertiMin.PROPERTY_PRICE, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("Unité", RefPrixFertiMin.PROPERTY_UNIT, PRICE_UNIT_PARSER);
        newMandatoryColumn("Code scénario", RefPrixFertiMin.PROPERTY_CODE_SCENARIO);
        newMandatoryColumn("Scénario", RefPrixFertiMin.PROPERTY_SCENARIO);
        newMandatoryColumn("Campagne", RefPrixFertiMin.PROPERTY_CAMPAIGN, INTEGER_WITH_NULL_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefPrixFertiMin.PROPERTY_ACTIVE, ACTIVE_PARSER);
        newOptionalColumn("Source", RefPrixFertiMin.PROPERTY_SOURCE);
    }

    @Override
    public Iterable<ExportableColumn<RefPrixFertiMin, Object>> getColumnsForExport() {
        ModelBuilder<RefPrixFertiMin> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("CATEG", RefPrixFertiMin.PROPERTY_CATEG, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("FORME", RefPrixFertiMin.PROPERTY_FORME);
        modelBuilder.newColumnForExport("Élement", RefPrixFertiMin.PROPERTY_ELEMENT, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Prix", RefPrixFertiMin.PROPERTY_PRICE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Unité", RefPrixFertiMin.PROPERTY_UNIT, PRICE_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("Code scénario", RefPrixFertiMin.PROPERTY_CODE_SCENARIO);
        modelBuilder.newColumnForExport("Scénario", RefPrixFertiMin.PROPERTY_SCENARIO);
        modelBuilder.newColumnForExport("Campagne", RefPrixFertiMin.PROPERTY_CAMPAIGN, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefPrixFertiMin.PROPERTY_ACTIVE, T_F_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefPrixFertiMin.PROPERTY_SOURCE);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefPrixFertiMin newEmptyInstance() {
        RefPrixFertiMin result = new RefPrixFertiMinImpl();
        result.setActive(true);
        return result;
    }
}
