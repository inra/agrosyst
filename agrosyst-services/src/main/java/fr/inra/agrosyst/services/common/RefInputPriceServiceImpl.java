package fr.inra.agrosyst.services.common;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.OtherProductInputUnit;
import fr.inra.agrosyst.api.entities.PotInputUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.SeedPrice;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.referential.FertiMinElement;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefInputPrice;
import fr.inra.agrosyst.api.entities.referential.RefInputPriceImpl;
import fr.inra.agrosyst.api.entities.referential.RefInputPriceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixAutre;
import fr.inra.agrosyst.api.entities.referential.RefPrixAutreTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbu;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbuTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspece;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMin;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMinTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiOrgaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixIrrig;
import fr.inra.agrosyst.api.entities.referential.RefPrixIrrigTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhyto;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhytoTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixPotTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefPrixSubstrateTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.common.RefInputPriceService;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInputAndCampaigns;
import fr.inra.agrosyst.api.services.performance.RefScenariosInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.Scenario;
import fr.inra.agrosyst.api.services.performance.ScenarioFilter;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.util.pagination.PaginationResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static fr.inra.agrosyst.api.FormatUtils.parseIdTraitementIdProduit;
import static java.util.function.Predicate.not;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Setter
@Getter
public class RefInputPriceServiceImpl extends AbstractAgrosystService implements RefInputPriceService {
    
    protected static final PriceUnit STANDARDIZED_DEFAULT_PRICE_UNIT = PriceUnit.EURO_HA;

    protected RefInputPriceTopiaDao refInputPriceDao;
    
    protected RefPrixCarbuTopiaDao refPrixCarbuDao;
    protected RefPrixFertiMinTopiaDao refPrixFertiMinDao;
    protected RefPrixFertiOrgaTopiaDao refPrixFertiOrgaDao;
    protected RefPrixPhytoTopiaDao refPrixPhytoDao;
    protected RefPrixIrrigTopiaDao refPrixIrrigDao;
    protected RefPrixPotTopiaDao refPrixPotDao;
    protected RefPrixSubstrateTopiaDao refPrixSubstrateDao;
    protected RefPrixAutreTopiaDao refPrixAutreDao;
    protected RefPrixEspeceTopiaDao refPrixEspeceDao;
    
    final protected RefFertiMinInputPriceServiceImpl refFertiMinInputPriceService;
    final protected RefPhytoInputPriceServiceImpl refPhytoInputPriceService;
    final protected RefOrganicInputPriceServiceImpl refOrganicInputPriceService;
    protected DomainService domainService;
    
    public RefInputPriceServiceImpl() {
        this.refPhytoInputPriceService = new RefPhytoInputPriceServiceImpl();
        this.refFertiMinInputPriceService = new RefFertiMinInputPriceServiceImpl();
        this.refOrganicInputPriceService = new RefOrganicInputPriceServiceImpl();
    }
    
    @Override
    public RefCampaignsInputPricesByDomainInputAndCampaigns getRefInputPricesForCampaignsByInput(
            Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStocksByTypes,
            Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters,
            Collection<Integer> campaigns) {
    
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> mineralInputRefPriceByCampaignForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixEspeceByCampaignForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixFertiOrgaByCampaignForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixPhytoByCampaignForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixIrrigByCampaignForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixCarbuByCampaignForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixAutreByCampaignForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixPotByCampaignForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixSubstrateByCampaignForInput = new HashMap<>();
        
        for (Map.Entry<InputType, List<AbstractDomainInputStockUnit>> domainInputForInputType : domainInputStocksByTypes.entrySet()) {
            InputType inputType = domainInputForInputType.getKey();
            List<AbstractDomainInputStockUnit> inputs = domainInputForInputType.getValue();
            switch (inputType) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
    
                    Collection<RefPrixFertiMin> refPrixForFertiMinElements = loadAllCampaignsDomainFertiElementsRefPrices(campaigns, inputs);
                    mineralInputRefPriceByCampaignForInput.putAll(refFertiMinInputPriceService.getRefInputPriceForCampaigns(
                            campaigns,
                            refInputUnitPriceUnitConverters,
                            inputs,
                            refPrixForFertiMinElements));
                }
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, LUTTE_BIOLOGIQUE -> {
    
                    Collection<DomainPhytoProductInput> phytoInputs = new HashSet<>();
                    inputs.forEach(i -> phytoInputs.add((DomainPhytoProductInput)i));
    
                    Collection<RefPrixPhyto> phytoPricesResult = loadRefPrixPhytoForCampaigns(campaigns, phytoInputs);
    
                    // group ref prices by scenario codes and object ids
                    Map<String, Map<Integer, List<RefPrixPhyto>>> refPhytoPricesByObjectIdAndByCampaigns = groupRefPhytoPricesByObjectIdsAndCampaigns(phytoPricesResult);
                    Map<? extends AbstractDomainInputStockUnit, ? extends Map<Integer, Optional<InputRefPrice>>> refInputPriceForCampaigns = refPhytoInputPriceService.getRefInputPriceForCampaigns(
                            campaigns,
                            refInputUnitPriceUnitConverters,
                            phytoInputs,
                            refPhytoPricesByObjectIdAndByCampaigns);
                    refPrixPhytoByCampaignForInput.putAll(refInputPriceForCampaigns);
    
                }
                case AUTRE -> {
                    Collection<DomainOtherInput> domainOtherInputs = new HashSet<>();
                    inputs.forEach(i -> domainOtherInputs.add((DomainOtherInput)i));
    
                    Map<RefPrixAutreCampaignKey, List<RefPrixAutre>> refPrixAutreByKeys = loadDomainOtherInputRefPricesForCampaigns(campaigns, domainOtherInputs);
    
                    List<RefInputUnitPriceUnitConverter> reOtherConverters = refInputUnitPriceUnitConverters.stream()
                            .filter(c -> c.getOtherProductInputUnit() != null)
                            .toList();
    
                    for (DomainOtherInput domainOtherInput : domainOtherInputs) {
                        final RefOtherInput refInput = domainOtherInput.getRefInput();
                        String inputType_c0 = refInput.getInputType_c0();
                        String caracteristic1 = refInput.getCaracteristic1();
                        String caracteristic2 = refInput.getCaracteristic2();
                        String caracteristic3 = refInput.getCaracteristic3();
                        OtherProductInputUnit usageUnit = domainOtherInput.getUsageUnit();
                        for (Integer campaign : campaigns) {
                            RefPrixAutreCampaignKey key = new RefPrixAutreCampaignKey(campaign, inputType_c0, caracteristic1, caracteristic2, caracteristic3);
            
                            List<RefPrixAutre> domainInputRefPrixAutres = refPrixAutreByKeys.get(key);
            
                            List<RefInputUnitPriceUnitConverter> reConvertersWithInputUnit = reOtherConverters.stream()
                                    .filter(rpc -> rpc.getOtherProductInputUnit() == usageUnit)
                                    .toList();
            
                            Optional<PriceAndUnit> refPrice = getAverageRefPrice(domainInputRefPrixAutres, reConvertersWithInputUnit, domainOtherInput);
                            InputRefPrice inputRefPrice = refPrice.map(rp -> RefInputPriceServiceImpl.getCampaignInputRePrice(domainOtherInput, campaign, rp, false)).orElse(null);
            
                            Map<Integer, Optional<InputRefPrice>> rePriceMap = refPrixAutreByCampaignForInput.computeIfAbsent(domainOtherInput, k -> new HashMap<>());
                            rePriceMap.put(campaign, Optional.ofNullable(inputRefPrice));
                        }
                    }
    
                }
                case CARBURANT -> {
                    //refPrixCarbuDao
                    Collection<RefPrixCarbu> refPrixCarbus = getFuelRefPriceForCampaigns(campaigns);
                    refPrixCarbuByCampaignForInput.putAll(getIrrigOrFuelRefPricesForCampaigns(campaigns, inputs, refPrixCarbus));
                }
                case IRRIGATION -> {
                    List<? extends RefInputPrice> refPrixIrrigs = loadIrrigationScenarioPricesForCampaigns(campaigns);
                    refPrixIrrigByCampaignForInput.putAll(getIrrigOrFuelRefPricesForCampaigns(campaigns, inputs, refPrixIrrigs));
                }
                case EPANDAGES_ORGANIQUES -> {
                    Collection<DomainOrganicProductInput> organicInputs = new HashSet<>();
                    inputs.forEach(i -> organicInputs.add((DomainOrganicProductInput)i));
    
                    Set<String> idTypeEffluents = organicInputs.stream()
                            .map(organicProductInput -> organicProductInput.getRefInput().getIdtypeeffluent())
                            .collect(Collectors.toSet());
    
                    List<RefPrixFertiOrga> refPrixFertiOrgas = loadOrganicScenarioPricesForCampaigns(campaigns, idTypeEffluents);
    
                    refPrixFertiOrgaByCampaignForInput.putAll(refOrganicInputPriceService.getRefInputPriceForCampaigns(
                            campaigns,
                            refInputUnitPriceUnitConverters,
                            organicInputs,
                            refPrixFertiOrgas));
                }
                case POT -> {
                    Collection<DomainPotInput> domainPotInputs = new HashSet<>();
                    inputs.forEach(i -> domainPotInputs.add((DomainPotInput)i));
                    Set<String> caracteristic1s = domainPotInputs.stream().map(DomainPotInput::getRefInput).map(RefPot::getCaracteristic1).collect(Collectors.toSet());
                    List<RefPrixPot> refPrixPots = loadPotScenarioPricesCampaigns(campaigns, caracteristic1s);
    
                    List<RefInputUnitPriceUnitConverter> rePotConverters = refInputUnitPriceUnitConverters.stream()
                            .filter(c -> c.getPotInputUnit() != null)
                            .toList();
    
                    MultiKeyMap<Object, List<RefPrixPot>> refPrixPotByCharacteristic1AndCampaigns = new MultiKeyMap<>();
                    refPrixPots.forEach(
                            refPrixPot -> {
                                String caracteristic1 = refPrixPot.getCaracteristic1();
                                Integer campaign = refPrixPot.getCampaign();
                                MultiKey<Object> mk = new MultiKey<>(caracteristic1, campaign);
                                refPrixPotByCharacteristic1AndCampaigns.merge(mk, Lists.newArrayList(refPrixPot), this::mergeRefInputPrices);
                            }
                    );
                    
                    for (DomainPotInput domainPotInput : domainPotInputs) {
                        String caracteristic1 = domainPotInput.getRefInput().getCaracteristic1();
                        for (Integer campaign : campaigns) {
                            List<RefPrixPot> domainInputRefPrixPots = refPrixPotByCharacteristic1AndCampaigns.get(caracteristic1, campaign);
            
                            PotInputUnit usageUnit = domainPotInput.getUsageUnit();
            
                            List<RefInputUnitPriceUnitConverter> rePotConvertersWithInputUnit = rePotConverters.stream()
                                    .filter(rpc -> rpc.getPotInputUnit() == usageUnit)
                                    .toList();
            
                            Optional<PriceAndUnit> refPrice = getAverageRefPrice(domainInputRefPrixPots, rePotConvertersWithInputUnit, domainPotInput);
                            InputRefPrice inputRefPrice = refPrice.map(priceAndUnit -> RefInputPriceServiceImpl.getCampaignInputRePrice(
                                    domainPotInput,
                                    campaign,
                                    priceAndUnit, false)).orElse(null);
            
                            Map<Integer, Optional<InputRefPrice>> rePriceMap = refPrixPotByCampaignForInput.computeIfAbsent(domainPotInput, k -> new HashMap<>());
                            rePriceMap.put(campaign, Optional.ofNullable(inputRefPrice));
                        }
                    }
                }
                case SEMIS, PLAN_COMPAGNE -> {

                    List<RefInputUnitPriceUnitConverter> seedConverters = refInputUnitPriceUnitConverters.stream()
                            .filter(c -> c.getSeedPlantUnit() != null)
                            .toList();

                    Collection<DomainSeedLotInput> domainSeedLotInputs = new HashSet<>();
                    inputs.forEach(i -> domainSeedLotInputs.add((DomainSeedLotInput)i));
    
                    Map<RefPrixSemisCampaignKey, List<RefPrixEspece>> refPrixSemisByKey = loadSeedInputRefPricesForCampaigns(campaigns, domainSeedLotInputs);

                    Map<String, List<RefInputUnitPriceUnitConverter>> convertersByUsageUnits = getConverteursByUsageUnits(seedConverters);

                    for (DomainSeedLotInput domainSeedLotInput : domainSeedLotInputs) {

                        SeedPrice cropPrice = domainSeedLotInput.getSeedPrice();
                        PriceUnit cropPriceUnit = cropPrice != null ? cropPrice.getPriceUnit() : null;

                        boolean isLotIncludedTreatment = domainSeedLotInput.isSeedCoatedTreatment();
                        boolean isOrganic = domainSeedLotInput.isOrganic();
                        SeedPlantUnit usageUnit = domainSeedLotInput.getUsageUnit();

                        List<RefInputUnitPriceUnitConverter> usageUnitConverters = convertersByUsageUnits.get(usageUnit.name());
                        Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> usageUnitConvertersByPriceUnits = getConvertersByPriceUnits(usageUnitConverters);

                        Collection<DomainSeedSpeciesInput> domainSeedSpeciesInput = CollectionUtils.emptyIfNull(domainSeedLotInput.getDomainSeedSpeciesInput());
        
                        Map<Integer, Optional<InputRefPrice>> rePriceMap = refPrixEspeceByCampaignForInput.computeIfAbsent(domainSeedLotInput, k -> new HashMap<>());

                        // there is no ref price for crop -> comput for species
                        for (Integer campaign : campaigns) {
                            rePriceMap.put(campaign, Optional.empty());
                        }

                        List<DomainPhytoProductInput> domainPhytoProductInputs = new ArrayList<>();
        
                        for (DomainSeedSpeciesInput seedSpeciesInput : domainSeedSpeciesInput) {

                            SeedPrice seedPrice = seedSpeciesInput.getSeedPrice();
                            Optional<PriceUnit> optionalSeedPriceUnit = Optional.ofNullable(seedPrice!= null ? seedPrice.getPriceUnit() : cropPriceUnit);

                            Map<Integer, Optional<InputRefPrice>> speciesRePrice = refPrixEspeceByCampaignForInput.computeIfAbsent(seedSpeciesInput, k -> new HashMap<>());
            
                            SeedType seedType = seedSpeciesInput.getSeedType();
                            // #8507 Par défaut, si un traitement est déclaré dans l'action de semis, on prend le prix de la semence traitée.
                            // Mais si la case traitement inclus n'a pas été cochée sur le prix,
                            // on prend le prix de la semence non traitée.
                            // car le prix de traitement est déclaré indépendant
                            final RefEspece species = seedSpeciesInput.getSpeciesSeed().getSpecies();
                            String code_espece_botanique = species.getCode_espece_botanique();
                            String codeQualifiantAee = species.getCode_qualifiant_AEE();

                            if (seedSpeciesInput.getSpeciesPhytoInputs() != null && !isLotIncludedTreatment) {
                                domainPhytoProductInputs.addAll(seedSpeciesInput.getSpeciesPhytoInputs());
                            }
            
                            for (Integer campaign : campaigns) {

                                final boolean fallbackRefPrice;
                                RefPrixSemisCampaignKey key = new RefPrixSemisCampaignKey(
                                        campaign,
                                        code_espece_botanique,
                                        codeQualifiantAee,
                                        seedType,
                                        isLotIncludedTreatment,
                                        isOrganic
                                );
                
                                List<RefPrixEspece> domainInputSeedRefPrice = refPrixSemisByKey.get(key);

                                if (CollectionUtils.isEmpty(domainInputSeedRefPrice)) {
                                    RefPrixSemisCampaignKey fallbackKey = new RefPrixSemisCampaignKey(
                                            campaign,
                                            code_espece_botanique,
                                            codeQualifiantAee,
                                            seedType,
                                            isLotIncludedTreatment,
                                            !isOrganic
                                    );
                                    domainInputSeedRefPrice = refPrixSemisByKey.get(fallbackKey);

                                    fallbackRefPrice = true;

                                } else {
                                    fallbackRefPrice = false;
                                }

                                InputRefPrice inputRefPrice = null;
                                Collection<PriceAndUnit> averageRefPriceAndUnits = getAverageRefPriceAndUnit(usageUnitConvertersByPriceUnits, domainInputSeedRefPrice, optionalSeedPriceUnit);
                                for (PriceAndUnit averageRefPriceAndUnit : CollectionUtils.emptyIfNull(averageRefPriceAndUnits)) {
                                    Optional<PriceAndUnit> refPrice = Optional.ofNullable(averageRefPriceAndUnit);
                                    // use convertersByPriceUnits ?
                                    Optional<Double> conversionRate = getSeedingSpeciesConversionRate(usageUnitConvertersByPriceUnits, refPrice, usageUnit);

                                    if (!conversionRate.orElse(0d).equals(0d)) {
                                        // retourne un résultat uniquement pour le réalisé
                                        InputRefPrice inputRefPrice0 = refPrice.map(priceAndUnit -> RefInputPriceServiceImpl.getCampaignInputRePrice(
                                                domainSeedLotInput,
                                                campaign,
                                                priceAndUnit,
                                                fallbackRefPrice)).orElse(null);

                                        if (inputRefPrice0 != null) {
                                            double price = inputRefPrice0.averageRefPrice().value() * conversionRate.get();
                                            inputRefPrice = new InputRefPrice(
                                                    inputRefPrice0.domainInputId(),
                                                    inputRefPrice0.scenarioCode(),
                                                    inputRefPrice0.campaign(),
                                                    inputRefPrice0.practicedSystemCampaigns(),
                                                    new PriceAndUnit(price, refPrice.get().unit()),
                                                    inputRefPrice0.fallbackRefPrice()
                                            );
                                            break;
                                        }
                                    }
                                }
                                speciesRePrice.put(campaign, Optional.ofNullable(inputRefPrice));

                            }
                        }

                        Collection<RefPrixPhyto> phytoPricesResult = loadRefPrixPhytoForCampaigns(campaigns, domainPhytoProductInputs);


                        Map<String, Map<Integer, List<RefPrixPhyto>>> refPhytoPricesByObjectIdAndByCampaigns = groupRefPhytoPricesByObjectIdsAndCampaigns(phytoPricesResult);
                        //speciesRefPriceForInput
                        Map<? extends AbstractDomainInputStockUnit, ? extends Map<Integer, Optional<InputRefPrice>>> refInputPriceForCampaigns = refPhytoInputPriceService.getRefInputPriceForCampaigns(
                                campaigns,
                                refInputUnitPriceUnitConverters,
                                domainPhytoProductInputs,
                                refPhytoPricesByObjectIdAndByCampaigns);
                        refPrixPhytoByCampaignForInput.putAll(refInputPriceForCampaigns);

                    }
                }
                case SUBSTRAT -> {
                    Collection<DomainSubstrateInput> domainSubstrateInputs = new HashSet<>();
                    inputs.forEach(i -> domainSubstrateInputs.add((DomainSubstrateInput)i));
                    Set<Pair<String,String>> caracteristics = domainSubstrateInputs.stream()
                            .map(DomainSubstrateInput::getRefInput)
                            .map(rs -> Pair.of(rs.getCaracteristic1(), rs.getCaracteristic2()))
                            .collect(Collectors.toSet());
                    List<RefPrixSubstrate> refPrixSubstrates = loadSubstrateScenarioPricesForCampaigns(campaigns, caracteristics);
    
                    List<RefInputUnitPriceUnitConverter> reSubstrateConverters = refInputUnitPriceUnitConverters.stream()
                            .filter(c -> c.getSubstrateInputUnit() != null)
                            .toList();
    
                    MultiKeyMap<Object, List<RefPrixSubstrate>> refPrixSubstrateByCharacteristicsAndCampaigns = new MultiKeyMap<>();
                    refPrixSubstrates.forEach(
                            refPrixSubstrate -> {
                                String caracteristic1 = refPrixSubstrate.getCaracteristic1();
                                String caracteristic2 = refPrixSubstrate.getCaracteristic2();
                                Integer campaign = refPrixSubstrate.getCampaign();
                                MultiKey<Object> mk = new MultiKey<>(caracteristic1, caracteristic2, campaign);
                                refPrixSubstrateByCharacteristicsAndCampaigns.merge(mk, Lists.newArrayList(refPrixSubstrate), this::mergeRefInputPrices);
                            }
                    );
                    for (DomainSubstrateInput domainSubstrateInput : domainSubstrateInputs) {
                        final RefSubstrate refInput = domainSubstrateInput.getRefInput();
                        String caracteristic1 = refInput.getCaracteristic1();
                        String caracteristic2 = refInput.getCaracteristic2();
                        SubstrateInputUnit usageUnit = domainSubstrateInput.getUsageUnit();
                        for (Integer campaign : campaigns) {
                            MultiKey<Object> mk = new MultiKey<>(caracteristic1, caracteristic2, campaign);
                            List<RefPrixSubstrate> domainInputRefPrixSubstrates = refPrixSubstrateByCharacteristicsAndCampaigns.get(mk);
            
                            List<RefInputUnitPriceUnitConverter> reSubstrateConvertersWithInputUnit = reSubstrateConverters.stream()
                                    .filter(rpc -> rpc.getSubstrateInputUnit() == usageUnit)
                                    .toList();
            
                            Optional<PriceAndUnit> refPrice = getAverageRefPrice(domainInputRefPrixSubstrates, reSubstrateConvertersWithInputUnit, domainSubstrateInput);
                            InputRefPrice inputRefPrice = refPrice.map(doseAndUnit -> RefInputPriceServiceImpl.getCampaignInputRePrice(
                                    domainSubstrateInput,
                                    campaign,
                                    doseAndUnit,
                                    false)).orElse(null);
            
                            Map<Integer, Optional<InputRefPrice>> rePriceMap = refPrixSubstrateByCampaignForInput.computeIfAbsent(domainSubstrateInput, k -> new HashMap<>());
                            rePriceMap.put(campaign, Optional.ofNullable(inputRefPrice));
                        }
                    }
                }
                case TRAITEMENT_SEMENCE -> {
                }
            }
        }
        RefCampaignsInputPricesByDomainInputAndCampaigns result = new RefCampaignsInputPricesByDomainInputAndCampaigns(
                mineralInputRefPriceByCampaignForInput,
                refPrixEspeceByCampaignForInput,
                refPrixFertiOrgaByCampaignForInput,
                refPrixPhytoByCampaignForInput,
                refPrixIrrigByCampaignForInput,
                refPrixCarbuByCampaignForInput,
                refPrixAutreByCampaignForInput,
                refPrixPotByCampaignForInput,
                refPrixSubstrateByCampaignForInput
        );
    
        return result;
    }

    protected static Map<String, List<RefInputUnitPriceUnitConverter>> getConverteursByUsageUnits(List<RefInputUnitPriceUnitConverter> converters) {
        Map<String, List<RefInputUnitPriceUnitConverter>> convertersByUsageUnit = new HashMap<>();

        converters.forEach(
                ripc -> {

                    String productUnit = StringUtils.firstNonBlank(
                            ripc.getCapacityUnit() != null ? ripc.getCapacityUnit().name() : "",
                            ripc.getMineralProductUnit() != null ? ripc.getMineralProductUnit().name() : "",
                            ripc.getOrganicProductUnit() != null ? ripc.getOrganicProductUnit().name() : "",
                            ripc.getOtherProductInputUnit() != null ? ripc.getOtherProductInputUnit().name() : "",
                            ripc.getPhytoProductUnit() != null ? ripc.getPhytoProductUnit().name() : "",
                            ripc.getPotInputUnit() != null ? ripc.getPotInputUnit().name() : "",
                            ripc.getSeedPlantUnit() != null ? ripc.getSeedPlantUnit().name() : "",
                            ripc.getSubstrateInputUnit() != null ? ripc.getSubstrateInputUnit().name()  : "");
                    List<RefInputUnitPriceUnitConverter> convertersForProductUnit = convertersByUsageUnit.computeIfAbsent(productUnit, k -> new ArrayList<>());
                    convertersForProductUnit.add(ripc);
                });
        return convertersByUsageUnit;
    }

    protected Optional<Double> getSeedingSpeciesConversionRate(Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> convertersByPriceUnit,
                                                               Optional<PriceAndUnit> optionalPriceAndUnit,
                                                               SeedPlantUnit usageUnit) {
        if (optionalPriceAndUnit.isPresent()) {
            PriceAndUnit priceAndUnit = optionalPriceAndUnit.get();
            Double conversionRate = null;
            Pair<SeedPlantUnit, Double> conversionRateForDoseUnit = getDoseUnitToPriceUnitConversionRate(
                    convertersByPriceUnit,
                    priceAndUnit.unit(),
                    usageUnit);

            if (conversionRateForDoseUnit != null &&
                    conversionRateForDoseUnit.getRight() != null) {
                conversionRate = conversionRateForDoseUnit.getRight();
            }
            return Optional.ofNullable(conversionRate);
        }

        return Optional.empty();
    }

    // TODO factoriser as RefOrganicInputPriceServiceImpl.getDoseUnitToPriceUnitConversionRate() RefInputPriceServiceImpl.getDoseUnitToPriceUnitConversionRate()
    protected Pair<SeedPlantUnit, Double> getDoseUnitToPriceUnitConversionRate(
            Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> doseUnitToPriceUnitConverterByPriceUnit,
            PriceUnit priceUnit,
            SeedPlantUnit usageUnit) {

        Pair<SeedPlantUnit, Double> conversionRateForDoseUnit = null;

        if (doseUnitToPriceUnitConverterByPriceUnit != null) {

            List<RefInputUnitPriceUnitConverter> doseUnitToPriceUnitConverters = doseUnitToPriceUnitConverterByPriceUnit.get(priceUnit);

            if (CollectionUtils.isNotEmpty(doseUnitToPriceUnitConverters)) {

                conversionRateForDoseUnit = getConversionRateForSeedingSpecies(usageUnit, doseUnitToPriceUnitConverters);
            }

            if (conversionRateForDoseUnit == null || conversionRateForDoseUnit.getValue() == null) {
                // si pas trouver, essayer avec une unité cohérente
                Double conversionRate =null;
                Iterator<Map.Entry<PriceUnit, List<RefInputUnitPriceUnitConverter>>> iter = doseUnitToPriceUnitConverterByPriceUnit.entrySet().iterator();
                while (iter.hasNext() && conversionRate == null) {
                    Map.Entry<PriceUnit, List<RefInputUnitPriceUnitConverter>> priceUnitListEntry = iter.next();
                    PriceUnit to = priceUnitListEntry.getKey();
                    doseUnitToPriceUnitConverters = doseUnitToPriceUnitConverterByPriceUnit.get(to);
                    Double priceUnitConversionRate = RefInputPriceImpl.GET_PRICE_UNIT_TO_OTHER_PRICE_UNIT_CONVERSION_RATE.apply(Pair.of(priceUnit, to));
                    if (priceUnitConversionRate != null && priceUnitConversionRate != 0 && CollectionUtils.isNotEmpty(doseUnitToPriceUnitConverters)) {
                        conversionRateForDoseUnit = getConversionRateForSeedingSpecies(usageUnit, doseUnitToPriceUnitConverters);
                        final Double optionalValue = conversionRateForDoseUnit.getRight();
                        conversionRate = optionalValue != null ? optionalValue * priceUnitConversionRate : null;
                        conversionRateForDoseUnit = Pair.of(conversionRateForDoseUnit.getLeft(), conversionRate);
                    }
                }
            }
        }

        return conversionRateForDoseUnit;
    }

    protected Pair<SeedPlantUnit,Double> getConversionRateForSeedingSpecies(
            SeedPlantUnit usageUnit, List<RefInputUnitPriceUnitConverter> doseUnitToPriceUnitConverters) {

        Double conversionRate = doseUnitToPriceUnitConverters.stream()
                .filter(obj -> obj.getSeedPlantUnit() != null && obj.getSeedPlantUnit().equals(usageUnit))
                .toList()
                .stream()
                .map(RefInputUnitPriceUnitConverter::getConvertionRate).findAny().orElse(null);

        Pair<SeedPlantUnit, Double> conversionRateForDoseUnit = Pair.of(usageUnit, conversionRate);

        return conversionRateForDoseUnit;
    }


    @Override
    public RefCampaignsInputPricesByDomainInput getRefInputPricesForCampaignByInput(
            Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStocksByTypes,
            Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters,
            Integer campaign) {
        
        RefCampaignsInputPricesByDomainInputAndCampaigns refInputPricesForCampaignsByInput = getRefInputPricesForCampaignsByInput(
                domainInputStocksByTypes,
                refInputUnitPriceUnitConverters,
                Sets.newHashSet(campaign));
        
        RefCampaignsInputPricesByDomainInput result = getRefInputPricesForCampaignByInput(refInputPricesForCampaignsByInput, campaign);
        
        return result;
    }
    
    @Override
    public RefCampaignsInputPricesByDomainInput getRefInputPricesForCampaignByInput(
            RefCampaignsInputPricesByDomainInputAndCampaigns refInputPricesForCampaignsByInput,
            Integer campaign
    ) {
        
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> mineralInputRefPriceByCampaignForInput = refInputPricesForCampaignsByInput.mineralRefPriceByCampaignForInput();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixEspeceByCampaignForInput = refInputPricesForCampaignsByInput.speciesRefPriceByCampaignForInput();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixFertiOrgaByCampaignForInput = refInputPricesForCampaignsByInput.fertiOrgaRefPriceByCampaignForInput();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixPhytoByCampaignForInput = refInputPricesForCampaignsByInput.phytoRefPriceByCampaignForInput();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixIrrigByCampaignForInput = refInputPricesForCampaignsByInput.irrigRefPriceByCampaignForInput();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixCarbuByCampaignForInput = refInputPricesForCampaignsByInput.fuelRefPriceByCampaignForInput();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixAutreByCampaignForInput = refInputPricesForCampaignsByInput.otherRefPriceByCampaignForInput();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixPotByCampaignForInput = refInputPricesForCampaignsByInput.potRefPriceByCampaignForInput();
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixSubstrateByCampaignForInput = refInputPricesForCampaignsByInput.substrateRefPriceByCampaignForInput();
        
        Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> mineralInputRefPriceForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> refPrixEspeceForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> refPrixFertiOrgaForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> refPrixPhytoForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> refPrixIrrigForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> refPrixCarbuForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> refPrixAutreForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> refPrixPotForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> refPrixSubstrateForInput = new HashMap<>();
        
        mineralInputRefPriceByCampaignForInput.forEach(
                (domainInput, refPriceByCampaigns) -> mineralInputRefPriceForInput.put(domainInput, refPriceByCampaigns.get(campaign)));
        refPrixEspeceByCampaignForInput.forEach(
                (domainInput, refPriceByCampaigns) -> refPrixEspeceForInput.put(domainInput, refPriceByCampaigns.get(campaign)));
        refPrixFertiOrgaByCampaignForInput.forEach(
                (domainInput, refPriceByCampaigns) -> refPrixFertiOrgaForInput.put(domainInput, refPriceByCampaigns.get(campaign)));
        refPrixPhytoByCampaignForInput.forEach(
                (domainInput, refPriceByCampaigns) -> refPrixPhytoForInput.put(domainInput, refPriceByCampaigns.get(campaign)));
        refPrixIrrigByCampaignForInput.forEach(
                (domainInput, refPriceByCampaigns) -> refPrixIrrigForInput.put(domainInput, refPriceByCampaigns.get(campaign)));
        refPrixCarbuByCampaignForInput.forEach(
                (domainInput, refPriceByCampaigns) -> refPrixCarbuForInput.put(domainInput, refPriceByCampaigns.get(campaign)));
        refPrixAutreByCampaignForInput.forEach(
                (domainInput, refPriceByCampaigns) -> refPrixAutreForInput.put(domainInput, refPriceByCampaigns.get(campaign)));
        refPrixPotByCampaignForInput.forEach(
                (domainInput, refPriceByCampaigns) -> refPrixPotForInput.put(domainInput, refPriceByCampaigns.get(campaign)));
        refPrixSubstrateByCampaignForInput.forEach(
                (domainInput, refPriceByCampaigns) -> refPrixSubstrateForInput.put(domainInput, refPriceByCampaigns.get(campaign)));
        
        RefCampaignsInputPricesByDomainInput result = new RefCampaignsInputPricesByDomainInput(
                mineralInputRefPriceForInput,
                refPrixEspeceForInput,
                refPrixFertiOrgaForInput,
                refPrixPhytoForInput,
                refPrixIrrigForInput,
                refPrixCarbuForInput,
                refPrixAutreForInput,
                refPrixPotForInput,
                refPrixSubstrateForInput
        );
        
        return result;
    }
    
    @Override
    public RefCampaignsInputPricesByDomainInput getRefInputPricesForCampaignsByInput(
            RefCampaignsInputPricesByDomainInputAndCampaigns refInputPricesForCampaignsByInput,
            Collection<Integer> campaigns) {
        
        RefCampaignsInputPricesByDomainInput result = new RefCampaignsInputPricesByDomainInput(
                averageRefPriceByInputForGivenCampaigns(campaigns, refInputPricesForCampaignsByInput.mineralRefPriceByCampaignForInput()),
                averageRefPriceByInputForGivenCampaigns(campaigns, refInputPricesForCampaignsByInput.speciesRefPriceByCampaignForInput()),
                averageRefPriceByInputForGivenCampaigns(campaigns, refInputPricesForCampaignsByInput.fertiOrgaRefPriceByCampaignForInput()),
                averageRefPriceByInputForGivenCampaigns(campaigns, refInputPricesForCampaignsByInput.phytoRefPriceByCampaignForInput()),
                averageRefPriceByInputForGivenCampaigns(campaigns, refInputPricesForCampaignsByInput.irrigRefPriceByCampaignForInput()),
                averageRefPriceByInputForGivenCampaigns(campaigns, refInputPricesForCampaignsByInput.fuelRefPriceByCampaignForInput()),
                averageRefPriceByInputForGivenCampaigns(campaigns, refInputPricesForCampaignsByInput.otherRefPriceByCampaignForInput()),
                averageRefPriceByInputForGivenCampaigns(campaigns, refInputPricesForCampaignsByInput.potRefPriceByCampaignForInput()),
                averageRefPriceByInputForGivenCampaigns(campaigns, refInputPricesForCampaignsByInput.substrateRefPriceByCampaignForInput())
        );
    
        return result;
    }
    
    private Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> averageRefPriceByInputForGivenCampaigns(
            Collection<Integer> campaigns,
            Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> inputRePrices) {
        
        Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> averageRePriceByInput = new HashMap<>();
        inputRePrices.forEach(
                (domainInput, refPriceByCampaignForInput) -> {
                    List<Optional<InputRefPrice>> refPriceForGivenCampaigns = new ArrayList<>();
                    for (Map.Entry<Integer, Optional<InputRefPrice>> campaignToRefPrice : refPriceByCampaignForInput.entrySet()) {
                        if (campaigns.contains(campaignToRefPrice.getKey())) {
                            refPriceForGivenCampaigns.add(campaignToRefPrice.getValue());
                        }
                    }
                    InputRefPrice irp = null;
                    if (CollectionUtils.isNotEmpty(refPriceForGivenCampaigns)) {
                        Optional<PriceAndUnit> optionalAverageRefPrice = getAverageInputRefPriceAndUnit(refPriceForGivenCampaigns);
                        if (optionalAverageRefPrice.isPresent()) {
                            PriceAndUnit averagePrice = optionalAverageRefPrice.get();
                            irp = new InputRefPrice(
                                    domainInput.getTopiaId(),
                                    Optional.empty(),
                                    Optional.empty(),
                                    Optional.ofNullable(campaigns),
                                    averagePrice
                                    ,false);
                        }
                    }
                    averageRePriceByInput.put(domainInput, Optional.ofNullable(irp));
                });
        return averageRePriceByInput;
    }
    
    @Override
    public RefScenariosInputPricesByDomainInput getRefInputPricesForScenariosByInput(
            Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStocksByTypes,
            Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters,
            Collection<String> scenarioCodes) {
        
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> mineralInputRefPriceByScenarioForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> refPrixEspeceByScenarioForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> refPrixFertiOrgaByScenarioForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> refPrixPhytoByScenarioForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> refPrixIrrigByScenarioForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> refPrixCarbuByScenarioForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> refPrixAutreByScenarioForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> refPrixPotByScenarioForInput = new HashMap<>();
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> refPrixSubstrateByScenarioForInput = new HashMap<>();
        
        for (Map.Entry<InputType, List<AbstractDomainInputStockUnit>> domainInputForInputType : domainInputStocksByTypes.entrySet()) {
            InputType inputType = domainInputForInputType.getKey();
            Collection<AbstractDomainInputStockUnit> inputs = domainInputForInputType.getValue();
            switch (inputType) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                    // load all element refPrice used into the domain inputs
                    Collection<RefPrixFertiMin> refPrixForFertiMinElements = this.loadAllScenariosDomainFertiElementsRefPrices(scenarioCodes, inputs);
                    // compute the refPrice by input, scenario and element
                    mineralInputRefPriceByScenarioForInput.putAll(refFertiMinInputPriceService.getRefInputPriceForScenarios(
                            scenarioCodes,
                            refInputUnitPriceUnitConverters,
                            inputs,
                            refPrixForFertiMinElements));
                }
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, LUTTE_BIOLOGIQUE, TRAITEMENT_SEMENCE -> {
                    
                    Collection<DomainPhytoProductInput> phytoInputs = new HashSet<>();
                    inputs.forEach(i -> phytoInputs.add((DomainPhytoProductInput)i));
                    
                    Collection<RefPrixPhyto> phytoPricesResult = loadRefPrixPhytoForScenarioCodes(new HashSet<>(scenarioCodes), phytoInputs);
    
                    // group ref prices by scenario codes and object ids
                    Map<String, Map<String, List<RefPrixPhyto>>> refPhytoScenarioPricesByObjectIdAndByScenarioCodes = groupRefPhytoPricesByObjectIdsAndScenarios(phytoPricesResult);
                    
                    refPrixPhytoByScenarioForInput.putAll(refPhytoInputPriceService.getRefInputPriceForScenarios(
                            scenarioCodes,
                            refInputUnitPriceUnitConverters,
                            phytoInputs,
                            refPhytoScenarioPricesByObjectIdAndByScenarioCodes));
                    
                }
                case AUTRE -> {
                    Collection<DomainOtherInput> domainOtherInputs = new HashSet<>();
                    inputs.forEach(i -> domainOtherInputs.add((DomainOtherInput)i));
    
                    Map<RefPrixAutreScenarioKey, List<RefPrixAutre>> refPrixAutreByKeys = loadOtherInputScenarioPricesForScenarioCodes(scenarioCodes, domainOtherInputs);
    
                    List<RefInputUnitPriceUnitConverter> reOtherConverters = refInputUnitPriceUnitConverters.stream()
                            .filter(c -> c.getSubstrateInputUnit() != null)
                            .toList();
                    
                    for (DomainOtherInput domainOtherInput : domainOtherInputs) {
                        final RefOtherInput refInput = domainOtherInput.getRefInput();
                        String inputType_c0 = refInput.getInputType_c0();
                        String caracteristic1 = refInput.getCaracteristic1();
                        String caracteristic2 = refInput.getCaracteristic2();
                        String caracteristic3 = refInput.getCaracteristic3();
                        OtherProductInputUnit usageUnit = domainOtherInput.getUsageUnit();
                        for (String scenarioCode : scenarioCodes) {
                            RefPrixAutreScenarioKey key = new RefPrixAutreScenarioKey(scenarioCode, inputType_c0, caracteristic1, caracteristic2, caracteristic3);
                            
                            List<RefPrixAutre> domainInputRefPrixAutres = refPrixAutreByKeys.get(key);
                            
                            List<RefInputUnitPriceUnitConverter> reConvertersWithInputUnit = reOtherConverters.stream()
                                    .filter(rpc -> rpc.getOtherProductInputUnit() == usageUnit)
                                    .toList();
                            
                            Optional<PriceAndUnit> refPrice = getAverageRefPrice(domainInputRefPrixAutres, reConvertersWithInputUnit, domainOtherInput);
                            InputRefPrice inputRefPrice = refPrice.map(priceAndUnit -> RefInputPriceServiceImpl.getScenarioInputRePrice(
                                    domainOtherInput,
                                    scenarioCode,
                                    priceAndUnit
                            )).orElse(null);
                            
                            Map<String, Optional<InputRefPrice>> rePriceMap = refPrixAutreByScenarioForInput.computeIfAbsent(domainOtherInput, k -> new HashMap<>());
                            rePriceMap.put(scenarioCode, Optional.ofNullable(inputRefPrice));
                        }
                    }
                    
                }
                case CARBURANT -> {
                    //refPrixCarbuDao
                    List<RefPrixCarbu> refPrixCarbus = loadCarbuScenarioPricesForScenarioCodes(scenarioCodes);
                    refPrixCarbuByScenarioForInput.putAll(getIrrigOrFuelRefPricesForScenarios(scenarioCodes, inputs, refPrixCarbus));
                }
                case IRRIGATION -> {
        
                    List<? extends RefInputPrice> refPrixIrrigs = loadIrrigationScenarioPricesForScenarioCodes(scenarioCodes);
                    refPrixIrrigByScenarioForInput.putAll(getIrrigOrFuelRefPricesForScenarios(scenarioCodes, inputs, refPrixIrrigs));
        
                }
                case EPANDAGES_ORGANIQUES -> {
                    
                    Collection<DomainOrganicProductInput> organicInputs = new HashSet<>();
                    inputs.forEach(i -> organicInputs.add((DomainOrganicProductInput)i));
    
                    Set<String> idTypeEffluents = organicInputs.stream()
                            .map(organicProductInput -> organicProductInput.getRefInput().getIdtypeeffluent())
                            .collect(Collectors.toSet());
    
                    List<RefPrixFertiOrga> refPrixFertiOrgas = loadOrganicScenarioPricesForScenarioCodes(scenarioCodes, idTypeEffluents);
    
                    refPrixFertiOrgaByScenarioForInput.putAll(refOrganicInputPriceService.getRefInputPriceForScenario(
                            scenarioCodes,
                            refInputUnitPriceUnitConverters,
                            organicInputs,
                            refPrixFertiOrgas));
                }
                case POT -> {
                    Collection<DomainPotInput> domainPotInputs = new HashSet<>();
                    inputs.forEach(i -> domainPotInputs.add((DomainPotInput)i));
                    Set<String> caracteristic1s = domainPotInputs.stream().map(DomainPotInput::getRefInput).map(RefPot::getCaracteristic1).collect(Collectors.toSet());
                    List<RefPrixPot> refPrixPots = loadPotScenarioPricesForScenarioCodes(scenarioCodes, caracteristic1s);
    
                    List<RefInputUnitPriceUnitConverter> rePotConverters = refInputUnitPriceUnitConverters.stream()
                            .filter(c -> c.getPotInputUnit() != null)
                            .toList();
                    
                    MultiKeyMap<Object, List<RefPrixPot>> refPrixPotByCharacteristic1AndScenarioCodes = new MultiKeyMap<>();
                    refPrixPots.forEach(
                            refPrixPot -> {
                                String caracteristic1 = refPrixPot.getCaracteristic1();
                                String scenarioCode = refPrixPot.getCode_scenario();
                                MultiKey<String> mk = new MultiKey<>(caracteristic1, scenarioCode);
                                refPrixPotByCharacteristic1AndScenarioCodes.merge(mk, Lists.newArrayList(refPrixPot), this::mergeRefInputPrices);
                            }
                    );
                    for (DomainPotInput domainPotInput : domainPotInputs) {
                        String caracteristic1 = domainPotInput.getRefInput().getCaracteristic1();
                        for (String scenarioCode : scenarioCodes) {
                            List<RefPrixPot> domainInputRefPrixPots = refPrixPotByCharacteristic1AndScenarioCodes.get(caracteristic1, scenarioCode);
    
                            PotInputUnit usageUnit = domainPotInput.getUsageUnit();
                            
                            List<RefInputUnitPriceUnitConverter> rePotConvertersWithInputUnit = rePotConverters.stream()
                                    .filter(rpc -> rpc.getPotInputUnit() == usageUnit)
                                    .toList();
                            
                            Optional<PriceAndUnit> refPrice = getAverageRefPrice(domainInputRefPrixPots, rePotConvertersWithInputUnit, domainPotInput);
                            InputRefPrice inputRefPrice = refPrice.map(rp -> RefInputPriceServiceImpl.getScenarioInputRePrice(domainPotInput, scenarioCode, rp)).orElse(null);
                            
                            Map<String, Optional<InputRefPrice>> rePriceMap = refPrixPotByScenarioForInput.computeIfAbsent(domainPotInput, k -> new HashMap<>());
                            rePriceMap.put(scenarioCode, Optional.ofNullable(inputRefPrice));
                        }
                    }
                    
                }
                case SEMIS, PLAN_COMPAGNE -> {
                    Collection<DomainSeedLotInput> domainSeedLotInputs = new HashSet<>();
                    inputs.forEach(i -> domainSeedLotInputs.add((DomainSeedLotInput)i));
    
                    Map<RefPrixSemisScenarioKey, List<RefPrixEspece>> refPrixSemisScenarioByKey = loadSeedInputScenarioPricesForScenarioCodes(scenarioCodes, domainSeedLotInputs);
                    
                    for (DomainSeedLotInput domainSeedLotInput : domainSeedLotInputs) {
                        Collection<DomainSeedSpeciesInput> domainSeedSpeciesInput = CollectionUtils.emptyIfNull(domainSeedLotInput.getDomainSeedSpeciesInput());

                        Map<String, Optional<InputRefPrice>> rePriceMap = refPrixEspeceByScenarioForInput.computeIfAbsent(domainSeedLotInput, k -> new HashMap<>());

                        // no ref price on crop
                        if (CollectionUtils.isEmpty(domainSeedSpeciesInput)) {
                            for (String scenarioCode : scenarioCodes) {
                                rePriceMap.put(scenarioCode, Optional.empty());
                            }
                            continue;
                        }

                        SeedPrice cropPrice = domainSeedLotInput.getSeedPrice();
                        PriceUnit cropPriceUnit = cropPrice != null ? cropPrice.getPriceUnit() : null;
                        Boolean isIncludedTreatment = cropPrice != null ? cropPrice.isIncludedTreatment() : null;
                        boolean isOrganic = domainSeedLotInput.isOrganic();
    
                        for (DomainSeedSpeciesInput seedSpeciesInput : domainSeedSpeciesInput) {

                            SeedPrice seedPrice = seedSpeciesInput.getSeedPrice();
                            Optional<PriceUnit> optionalSeedPriceUnit = Optional.ofNullable(seedPrice!= null ? seedPrice.getPriceUnit() : cropPriceUnit);

                            Map<String, Optional<InputRefPrice>> refSpeciesPriceMap = refPrixEspeceByScenarioForInput.computeIfAbsent(seedSpeciesInput, k -> new HashMap<>());

                            SeedType seedType = seedSpeciesInput.getSeedType();
                            final RefEspece species = seedSpeciesInput.getSpeciesSeed().getSpecies();
                            String code_espece_botanique = species.getCode_espece_botanique();
                            String codeQualifiantAee = species.getCode_qualifiant_AEE();

                            boolean isTreatment = isIncludedTreatment != null ? isIncludedTreatment : seedSpeciesInput.isChemicalTreatment() || seedSpeciesInput.isBiologicalSeedInoculation();

                            for (String scenarioCode : scenarioCodes) {
    
                                RefPrixSemisScenarioKey key = new RefPrixSemisScenarioKey(
                                        scenarioCode,
                                        code_espece_botanique,
                                        codeQualifiantAee,
                                        seedType,
                                        isTreatment,// if treatment we look for ref price with treatment
                                        isOrganic
                                );
                                
                                List<RefPrixEspece> domainInputSeedRefPrice = refPrixSemisScenarioByKey.get(key);

                                Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> convertersByPriceUnits = getConvertersByPriceUnits(refInputUnitPriceUnitConverters);

                                InputRefPrice inputRefPrice = null;
                                Collection<PriceAndUnit> averageRefPriceAndUnits = getAverageRefPriceAndUnit(convertersByPriceUnits, domainInputSeedRefPrice, optionalSeedPriceUnit);
                                for (PriceAndUnit averageRefPriceAndUnit : CollectionUtils.emptyIfNull(averageRefPriceAndUnits)) {
                                    Optional<PriceAndUnit> refPrice = Optional.ofNullable(averageRefPriceAndUnit);
                                    inputRefPrice = refPrice.map(priceAndUnit -> RefInputPriceServiceImpl.getScenarioInputRePrice(domainSeedLotInput, scenarioCode, priceAndUnit)).orElse(null);
                                    break;
                                }
                                refSpeciesPriceMap.put(scenarioCode, Optional.ofNullable(inputRefPrice));

                            }

                            Collection<DomainPhytoProductInput> speciesPhytoInputs = CollectionUtils.emptyIfNull(seedSpeciesInput.getSpeciesPhytoInputs());
                            Collection<DomainPhytoProductInput> phytoInputs = new HashSet<>();
                            inputs.forEach(i -> phytoInputs.addAll(speciesPhytoInputs));

                            Collection<RefPrixPhyto> phytoPricesResult = loadRefPrixPhytoForScenarioCodes(new HashSet<>(scenarioCodes), phytoInputs);

                            // group ref prices by scenario codes and object ids
                            Map<String, Map<String, List<RefPrixPhyto>>> refPhytoScenarioPricesByObjectIdAndByScenarioCodes = groupRefPhytoPricesByObjectIdsAndScenarios(phytoPricesResult);

                            refPrixPhytoByScenarioForInput.putAll(refPhytoInputPriceService.getRefInputPriceForScenarios(
                                    scenarioCodes,
                                    refInputUnitPriceUnitConverters,
                                    phytoInputs,
                                    refPhytoScenarioPricesByObjectIdAndByScenarioCodes));

                        }

                    }
                }
                case SUBSTRAT -> {
                    Collection<DomainSubstrateInput> domainSubstrateInputs = new HashSet<>();
                    inputs.forEach(i -> domainSubstrateInputs.add((DomainSubstrateInput)i));
                    Set<Pair<String,String>> caracteristic1s = domainSubstrateInputs.stream()
                            .map(DomainSubstrateInput::getRefInput)
                            .map(rs -> Pair.of(rs.getCaracteristic1(), rs.getCaracteristic2()))
                                    .collect(Collectors.toSet());
                    List<RefPrixSubstrate> refPrixSubstrates = loadSubstrateScenarioPricesForScenarioCodes(scenarioCodes, caracteristic1s);
    
                    List<RefInputUnitPriceUnitConverter> reSubstrateConverters = refInputUnitPriceUnitConverters.stream()
                            .filter(c -> c.getSubstrateInputUnit() != null)
                            .toList();
    
                    MultiKeyMap<Object, List<RefPrixSubstrate>> refPrixPotByCharacteristic1AndScenarioCodes = new MultiKeyMap<>();
                    refPrixSubstrates.forEach(
                            refPrixSubstrate -> {
                                String caracteristic1 = refPrixSubstrate.getCaracteristic1();
                                String caracteristic2 = refPrixSubstrate.getCaracteristic2();
                                String scenarioCode = refPrixSubstrate.getCode_scenario();
                                MultiKey<String> mk = new MultiKey<>(caracteristic1, caracteristic2, scenarioCode);
                                refPrixPotByCharacteristic1AndScenarioCodes.merge(mk, Lists.newArrayList(refPrixSubstrate), this::mergeRefInputPrices);
                            }
                    );
                    for (DomainSubstrateInput domainSubstrateInput : domainSubstrateInputs) {
                        final RefSubstrate refInput = domainSubstrateInput.getRefInput();
                        String caracteristic1 = refInput.getCaracteristic1();
                        String caracteristic2 = refInput.getCaracteristic2();
                        SubstrateInputUnit usageUnit = domainSubstrateInput.getUsageUnit();
                        for (String scenarioCode : scenarioCodes) {
                            List<RefPrixSubstrate> domainInputRefPrixSubstrates = refPrixPotByCharacteristic1AndScenarioCodes.get(caracteristic1, caracteristic2, scenarioCode);
            
                            List<RefInputUnitPriceUnitConverter> reSubstrateConvertersWithInputUnit = reSubstrateConverters.stream()
                                    .filter(rpc -> rpc.getSubstrateInputUnit() == usageUnit)
                                    .toList();
                            
                            Optional<PriceAndUnit> refPrice = getAverageRefPrice(domainInputRefPrixSubstrates, reSubstrateConvertersWithInputUnit, domainSubstrateInput);
                            InputRefPrice inputRefPrice = refPrice.map(rp -> RefInputPriceServiceImpl.getScenarioInputRePrice(
                                    domainSubstrateInput,
                                    scenarioCode,
                                    rp
                            )).orElse(null);
            
                            Map<String, Optional<InputRefPrice>> rePriceMap = refPrixSubstrateByScenarioForInput.computeIfAbsent(domainSubstrateInput, k -> new HashMap<>());
                            rePriceMap.put(scenarioCode, Optional.ofNullable(inputRefPrice));
                        }
                    }
    
                }
            }
        }
        
        RefScenariosInputPricesByDomainInput result = new RefScenariosInputPricesByDomainInput(
                mineralInputRefPriceByScenarioForInput,
                refPrixEspeceByScenarioForInput,
                refPrixFertiOrgaByScenarioForInput,
                refPrixPhytoByScenarioForInput,
                refPrixIrrigByScenarioForInput,
                refPrixCarbuByScenarioForInput,
                refPrixAutreByScenarioForInput,
                refPrixPotByScenarioForInput,
                refPrixSubstrateByScenarioForInput
        );
        
        return result;
    }

    @Override
    public Optional<RefPrixCarbu> getFuelRefPriceForCampaign(int campaign) {
        return refPrixCarbuDao.forCampaignEquals(campaign)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .stream().findAny();
    }
    
    @Override
    public Collection<RefPrixCarbu> getFuelRefPriceForCampaigns(Collection<Integer> campaigns) {
        return refPrixCarbuDao.forCampaignIn(campaigns)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .findAll();
    }
    
    private Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> getIrrigOrFuelRefPricesForScenarios(
            Collection<String> scenarioCodes,
            Collection<AbstractDomainInputStockUnit> inputs,
            Collection<? extends RefInputPrice> refInputPrices) {
        
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> results = new HashMap<>();
        Map<String, RefInputPrice> refScenarioPricesByScenarioCode =
                refInputPrices
                        .stream()
                        .collect(
                                Collectors.toMap(RefInputPrice::getCode_scenario, Function.identity())
                        );
        
        inputs.forEach(input -> scenarioCodes.forEach(scenarioCode -> {
            final RefInputPrice refInputPrice = refScenarioPricesByScenarioCode.get(scenarioCode);
            Map<String, Optional<InputRefPrice>> result = results.computeIfAbsent(input, k -> new HashMap<>());
            InputRefPrice inputRefPrice = null;
            if (refInputPrice != null) {
                inputRefPrice = RefInputPriceServiceImpl.getScenarioInputRePrice(input, scenarioCode, new PriceAndUnit(refInputPrice.getPrice(), refInputPrice.getUnit()));
            }
            result.put(scenarioCode, Optional.ofNullable(inputRefPrice));
        }));
        return results;
    }
    
    private Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> getIrrigOrFuelRefPricesForCampaigns(
            Collection<Integer> campaigns,
            Collection<AbstractDomainInputStockUnit> inputs,
            Collection<? extends RefInputPrice> refInputPrices) {
        
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> results = new HashMap<>();
        Map<Integer, RefInputPrice> refPricesByCampaings =
                refInputPrices
                        .stream()
                        .collect(
                                Collectors.toMap(RefInputPrice::getCampaign, Function.identity())
                        );
        
        inputs.forEach(input -> campaigns.forEach(campaign -> {
            final RefInputPrice refInputPrice = refPricesByCampaings.get(campaign);
            InputRefPrice inputRefPrice = null;
            Map<Integer, Optional<InputRefPrice>> result = results.computeIfAbsent(input, m -> new HashMap<>());
            if (refInputPrice != null) {
                inputRefPrice = RefInputPriceServiceImpl.getCampaignInputRePrice(input, campaign, new PriceAndUnit(refInputPrice.getPrice(), refInputPrice.getUnit()), false);
            }
            result.put(campaign, Optional.ofNullable(inputRefPrice));
        }));
        return results;
    }

    protected static <RIP extends RefInputPrice> Optional<PriceAndUnit> getAverageRefPrice(
            final Collection<RIP> domainInputRefPrice,
            final Collection<RefInputUnitPriceUnitConverter> doseUnitToPriceUnitConverterByPriceUnit,
            AbstractDomainInputStockUnit domainInputStockUnit) {
    
        Double avgPrice = null;
        PriceUnit priceUnit = null;
    
        final InputPrice inputPrice = domainInputStockUnit.getInputPrice();
        final Optional<PriceUnit> optionalDomainInputPriceUnit = inputPrice != null ? Optional.of(inputPrice.getPriceUnit()) : Optional.empty();
    
        Collection<RefInputUnitPriceUnitConverter> currentConverters = null;
        if (optionalDomainInputPriceUnit.isPresent()) {
            final PriceUnit domainInputPriceUnit = optionalDomainInputPriceUnit.get();
            currentConverters = CollectionUtils.emptyIfNull(doseUnitToPriceUnitConverterByPriceUnit).stream()
                    .filter(c -> domainInputPriceUnit.equals(c.getPriceUnit()))
                    .toList();
        }
        
        if (CollectionUtils.isEmpty(currentConverters) && CollectionUtils.isNotEmpty(doseUnitToPriceUnitConverterByPriceUnit)) {
            currentConverters = Lists.newArrayList(doseUnitToPriceUnitConverterByPriceUnit);
        }
    
        Iterator<RefInputUnitPriceUnitConverter> converterIterator = CollectionUtils.emptyIfNull(currentConverters).iterator();
        while (converterIterator.hasNext() && (avgPrice == null || avgPrice == 0)) {
            RefInputUnitPriceUnitConverter aConverter = converterIterator.next();

            Collection<RIP> rips = CollectionUtils.emptyIfNull(domainInputRefPrice);
            List<RIP> refInputPrices = Lists.newArrayList(rips.stream()
                    .filter(rip -> rip.getUnit().equals(aConverter.getPriceUnit()))
                    .filter(rip -> rip.getPrice() != null)
                    .filter(rip -> rip.getPrice() > 0)
                    .toList());

            double transformablePriceUnitConversionRate = 1;
            PriceUnit compatiblePriceUnit = null;
            if (CollectionUtils.isEmpty(refInputPrices)) {
                for (RIP rip : rips) {
                    Double priceUnitConversionRate = RefInputPriceImpl.GET_PRICE_UNIT_TO_OTHER_PRICE_UNIT_CONVERSION_RATE.apply(Pair.of(rip.getUnit(), aConverter.getPriceUnit()));
                    if (priceUnitConversionRate != null && priceUnitConversionRate != 0) {
                        transformablePriceUnitConversionRate = priceUnitConversionRate;
                        refInputPrices.add(rip);
                        compatiblePriceUnit = aConverter.getPriceUnit();
                        break;
                    }
                }

            }
    
            final boolean notEmpty = CollectionUtils.isNotEmpty(refInputPrices);
            avgPrice = notEmpty ?
                    refInputPrices.stream().mapToDouble(RIP::getPrice).average().orElse(0d)
                            * aConverter.getConvertionRate()
                            * transformablePriceUnitConversionRate
                    : null;
            
            priceUnit = compatiblePriceUnit != null ?  compatiblePriceUnit : notEmpty ? refInputPrices.iterator().next().getUnit() : null;
        }
        
        Optional<PriceAndUnit> refPrice = avgPrice != null ? Optional.of(new PriceAndUnit(avgPrice, priceUnit)) : Optional.empty();
        return refPrice;
    }

    protected static Collection<PriceAndUnit> getAverageRefPriceAndUnit(
            Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> converters,
            Collection<? extends RefInputPrice> refInputPrices,
            Optional<PriceUnit> optionalInputPriceUnit) {


        // Sur l'ensemble des prix de référence remontés, on ne garde que ceux ayant la même unité.
        // 1 même unité que celle choisie par l'utilisateur.
        // 2 prix ayant une unité compatible
        // 3 prix ayant l'unité par défaut
        // n'importe quel autre prix
        if (CollectionUtils.isNotEmpty(refInputPrices)) {
            Collection<PriceAndUnit> results = null;

            if (optionalInputPriceUnit.isPresent()) {
                Map<PriceUnit, List<RefInputPrice>> refPricesByPriceUnits = filterRefPricesOnUserUnit(refInputPrices, optionalInputPriceUnit.get());
                results = getAverageRefPrices(refPricesByPriceUnits);
            }

            if (CollectionUtils.isEmpty(results)) {
                Map<PriceUnit, List<RefInputPrice>> refPricesByPriceUnits = filterRefPricesOnNonDefaultPriceUnit(converters, refInputPrices);
                results = getAverageRefPrices(refPricesByPriceUnits);
            }

            if (CollectionUtils.isEmpty(results)) {
                Map<PriceUnit, List<RefInputPrice>> refPricesByPriceUnits = filterRefPricesOnConvertibleDefaultPriceUnit(converters, refInputPrices);
                results = getAverageRefPrices(refPricesByPriceUnits);
            }

            if (CollectionUtils.isEmpty(results)) {
                Map<PriceUnit, List<RefInputPrice>> refPricesByPriceUnits = filterRefPricesOnDefaultPriceUnit(refInputPrices);
                results = getAverageRefPrices(refPricesByPriceUnits);
            }

            if (CollectionUtils.isEmpty(results)) {
                Map<PriceUnit, List<RefInputPrice>> refPricesByPriceUnits = filterRefPricesOnAnyPriceUnit(refInputPrices);
                results = getAverageRefPrices(refPricesByPriceUnits);
            }

            return results;
        } else {
            return null;
        }
    }


    private static Map<PriceUnit, List<RefInputPrice>> filterRefPricesOnUserUnit(Collection<? extends RefInputPrice> refInputPrices, PriceUnit userPriceUnit) {
        Map<PriceUnit, List<RefInputPrice>> refPricesByPriceUnits = new HashMap<>();

        List<RefInputPrice> filteredUnitRefPrices0 = refInputPrices.stream()
                .filter(refPrice -> userPriceUnit.equals(refPrice.getUnit()))
                .filter(refPrice -> refPrice.getPrice() != null && refPrice.getPrice() > 0D)
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(filteredUnitRefPrices0)) {
            refPricesByPriceUnits.put(userPriceUnit, filteredUnitRefPrices0);
        }

        return refPricesByPriceUnits;
    }

    private static Map<PriceUnit, List<RefInputPrice>> filterRefPricesOnNonDefaultPriceUnit(Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> converters, Collection<? extends RefInputPrice> refInputPrices) {
        Map<PriceUnit, List<RefInputPrice>> refPricesByPriceUnits = new HashMap<>();
        List<? extends RefInputPrice> refInputPrices0 = refInputPrices.stream().filter(rip -> !RefInputPriceServiceImpl.STANDARDIZED_DEFAULT_PRICE_UNIT.equals(rip.getUnit()))
                .filter(rip -> CollectionUtils.isNotEmpty(converters.get(rip.getUnit())))
                .filter(refPrice -> refPrice.getPrice() != null && refPrice.getPrice() > 0D)
                .toList();

        groupRefPricesByUnits(refInputPrices0, refPricesByPriceUnits);
        return refPricesByPriceUnits;
    }

    private static Map<PriceUnit, List<RefInputPrice>> filterRefPricesOnConvertibleDefaultPriceUnit(
            Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> converters,
            Collection<? extends RefInputPrice> refInputPrices) {

        Map<PriceUnit, List<RefInputPrice>> refPricesByPriceUnits = new HashMap<>();
        List<? extends RefInputPrice> refInputPrices0 = refInputPrices.stream().filter(rip -> RefInputPriceServiceImpl.STANDARDIZED_DEFAULT_PRICE_UNIT.equals(rip.getUnit()))
                .filter(rip -> CollectionUtils.isNotEmpty(converters.get(rip.getUnit())))
                .filter(refPrice -> refPrice.getPrice() != null && refPrice.getPrice() > 0D)
                .toList();

        groupRefPricesByUnits(refInputPrices0, refPricesByPriceUnits);
        return refPricesByPriceUnits;
    }

    private static void groupRefPricesByUnits(List<? extends RefInputPrice> refInputPrices0, Map<PriceUnit, List<RefInputPrice>> refPricesByPriceUnits) {
        if (CollectionUtils.isNotEmpty(refInputPrices0)) {
            refInputPrices0.forEach(refInputPrice -> {
                List<RefInputPrice> refInputPrices1 = refPricesByPriceUnits.computeIfAbsent(refInputPrice.getUnit(), k -> new ArrayList<>());
                refInputPrices1.add(refInputPrice);
            });
        }
    }

    private static Map<PriceUnit, List<RefInputPrice>> filterRefPricesOnDefaultPriceUnit(Collection<? extends RefInputPrice> refInputPrices) {
        Map<PriceUnit, List<RefInputPrice>> refPricesByPriceUnits = new HashMap<>();
        List<RefInputPrice> filteredUnitRefPrices0 = refInputPrices.stream()
                .filter(productPrice -> RefInputPriceServiceImpl.STANDARDIZED_DEFAULT_PRICE_UNIT.equals(productPrice.getUnit()))
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(filteredUnitRefPrices0)) {
            refPricesByPriceUnits.put(RefInputPriceServiceImpl.STANDARDIZED_DEFAULT_PRICE_UNIT, filteredUnitRefPrices0);
        }
        return refPricesByPriceUnits;
    }

    private static Map<PriceUnit, List<RefInputPrice>> filterRefPricesOnAnyPriceUnit(Collection<? extends RefInputPrice> refInputPrices) {
        Map<PriceUnit, List<RefInputPrice>> refPricesByPriceUnits = new HashMap<>();
        refInputPrices.forEach(refInputPrice -> {
            List<RefInputPrice> refInputPrices1 = refPricesByPriceUnits.computeIfAbsent(refInputPrice.getUnit(), k -> new ArrayList<>());
            refInputPrices1.add(refInputPrice);
        });
        return refPricesByPriceUnits;
    }

    private static Collection<PriceAndUnit> getAverageRefPrices(Map<PriceUnit, List<RefInputPrice>> refPricesByPriceUnits) {
        Collection<PriceAndUnit> results = new ArrayList<>();

        for (Map.Entry<PriceUnit, List<RefInputPrice>> refPriceFoPriceUnit : refPricesByPriceUnits.entrySet()) {
            final PriceUnit refPriceUnit = refPriceFoPriceUnit.getKey();
            List<RefInputPrice> filteredUnitRefPrices = refPriceFoPriceUnit.getValue();

            double priceValue = filteredUnitRefPrices.stream()
                    .filter(refInputPrice0 -> Objects.nonNull(refInputPrice0) && Objects.nonNull(refInputPrice0.getPrice()))
                    .map(RefInputPrice::getPrice)
                    .filter(Objects::nonNull)
                    .mapToDouble(Double::doubleValue)
                    .average()
                    .orElse(0d);

            results.add(new PriceAndUnit(priceValue, refPriceUnit));
        }
        return results;
    }

    protected static Optional<PriceAndUnit> getAverageInputRefPriceAndUnit(Collection<Optional<InputRefPrice>> inputRePrices) {

        if (inputRePrices.stream().anyMatch(Optional::isPresent)) {
            // Sur l'ensemble des prix de référence remontés on ne garde que ceux ayant la même unité. La première unité sera celle de référence.
            final PriceUnit refPriceUnit = inputRePrices.stream()
                    .filter(Optional::isPresent)
                    .filter(productPrice -> !RefInputPriceServiceImpl.STANDARDIZED_DEFAULT_PRICE_UNIT.equals(productPrice.get().averageRefPrice().unit()))
                    .findAny()
                    .filter(Optional::isPresent)
                    .map(opu -> opu.get().averageRefPrice().unit()).orElse(RefInputPriceServiceImpl.STANDARDIZED_DEFAULT_PRICE_UNIT);

            final List<Optional<InputRefPrice>> refPrices = inputRePrices.stream()
                    .filter(Optional::isPresent)
                    .filter(refPrice -> refPriceUnit.equals(refPrice.get().averageRefPrice().unit())).toList();

            double priceValue = refPrices.stream()
                    .filter(Optional::isPresent)
                    .map(orp -> orp.get().averageRefPrice().value())
                    .mapToDouble(Double::doubleValue)
                    .average()
                    .orElse(0d);

            return Optional.of(new PriceAndUnit(priceValue, refPriceUnit));
        }
        
        return Optional.empty();
    }
    
    protected <RIP extends RefInputPrice> List<RIP> mergeRefInputPrices(List<RIP> previousValues, List<RIP> newValues) {
        if (previousValues == null) {
            return newValues;
        } else {
            previousValues.addAll(newValues);
            return previousValues;
        }
    }
    
    private List<RefPrixCarbu> loadCarbuScenarioPricesForScenarioCodes(Collection<String> scenarioCodes) {
        return refPrixCarbuDao.forCode_scenarioIn(scenarioCodes)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .findAll();
    }
    
    protected Collection<RefPrixFertiMin> loadAllScenariosDomainFertiElementsRefPrices(Collection<String> scenarioCodes, Collection<AbstractDomainInputStockUnit> inputs) {
        Set<FertiMinElement> fertiMinElements = inputs.stream()
                .map(i -> RefFertiMinInputPriceServiceImpl.getProductElements(((DomainMineralProductInput)i).getRefInput()))
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        Collection<RefPrixFertiMin> refPrixFertiMins = loadMineralScenarioPricesForScenarioCodes(scenarioCodes, fertiMinElements);
        return refPrixFertiMins;
    }
    
    protected Collection<RefPrixFertiMin> loadAllCampaignsDomainFertiElementsRefPrices(Collection<Integer> campaigns, Collection<AbstractDomainInputStockUnit> inputs) {
        Set<FertiMinElement> fertiMinElements = inputs.stream()
                .map(i -> RefFertiMinInputPriceServiceImpl.getProductElements(((DomainMineralProductInput)i).getRefInput()))
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        Collection<RefPrixFertiMin> refPrixFertiMins = loadMineralScenarioPricesForCampaigns(campaigns, fertiMinElements);
        return refPrixFertiMins;
    }
    
    protected Map<String, Map<String, List<RefPrixPhyto>>> groupRefPhytoPricesByObjectIdsAndScenarios(Collection<RefPrixPhyto> phytoPricesResult) {
        Map<String, Map<String, List<RefPrixPhyto>>> refPhytoScenarioPricesByObjectIdAndByScenarioCodes = new HashMap<>();
        for (RefPrixPhyto scenarioPrixPhyto : phytoPricesResult) {
            String objectId = scenarioPrixPhyto.getObjectId();
            Map<String, List<RefPrixPhyto>> scenarioPrixPhytoByScenarioCode =
                    refPhytoScenarioPricesByObjectIdAndByScenarioCodes.computeIfAbsent(objectId, k -> new HashMap<>());
            List<RefPrixPhyto> refProductPrices =
                    scenarioPrixPhytoByScenarioCode.computeIfAbsent(scenarioPrixPhyto.getCode_scenario(), k -> new ArrayList<>());
            refProductPrices.add(scenarioPrixPhyto);
        }
        return refPhytoScenarioPricesByObjectIdAndByScenarioCodes;
    }
    
    protected Map<String, Map<Integer, List<RefPrixPhyto>>> groupRefPhytoPricesByObjectIdsAndCampaigns(Collection<RefPrixPhyto> phytoPricesResult) {
        Map<String, Map<Integer, List<RefPrixPhyto>>> refPhytoPricesByObjectIdAndByCampaigns = new HashMap<>();
        for (RefPrixPhyto refPrixPhyto : phytoPricesResult) {
            String objectId = refPrixPhyto.getObjectId();
            Map<Integer, List<RefPrixPhyto>> refPrixPhytoByObjectIds =
                    refPhytoPricesByObjectIdAndByCampaigns.computeIfAbsent(objectId, k -> new HashMap<>());
            List<RefPrixPhyto> refProductPrices =
                    refPrixPhytoByObjectIds.computeIfAbsent(refPrixPhyto.getCampaign(), k -> new ArrayList<>());
            refProductPrices.add(refPrixPhyto);
        }
        return refPhytoPricesByObjectIdAndByCampaigns;
    }
    
    protected List<RefPrixFertiMin> loadMineralScenarioPricesForScenarioCodes(Collection<String> scenarioCodes,
                                                                              Collection<FertiMinElement> fertiMinElements) {
        return refPrixFertiMinDao.forCode_scenarioIn(scenarioCodes)
                .addIn(RefPrixFertiMin.PROPERTY_ELEMENT, fertiMinElements)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .findAll();
    }
    
    protected List<RefPrixFertiMin> loadMineralScenarioPricesForCampaigns(Collection<Integer> campaigns,
                                                                         Collection<FertiMinElement> fertiMinElements) {
        return refPrixFertiMinDao.forCampaignIn(campaigns)
                .addIn(RefPrixFertiMin.PROPERTY_ELEMENT, fertiMinElements)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .findAll();
    }
    
    protected List<RefPrixFertiOrga> loadOrganicScenarioPricesForScenarioCodes(Collection<String> scenarioCodes, Collection<String> idTypeEffluents) {
        return refPrixFertiOrgaDao.forCode_scenarioIn(scenarioCodes)
                .addIn(RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT, idTypeEffluents)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .findAll();
    }
    
    protected List<RefPrixFertiOrga> loadOrganicScenarioPricesForCampaigns(Collection<Integer> campaigns, Collection<String> idTypeEffluents) {
        return refPrixFertiOrgaDao.forCampaignIn(campaigns)
                .addIn(RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT, idTypeEffluents)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .findAll();
    }
    
    protected Collection<RefPrixPhyto> loadRefPrixPhytoForScenarioCodes(HashSet<String> scenarioCodes, Collection<DomainPhytoProductInput> phytoInputs) {
        Set<String> phytoObjectIds = phytoInputs.stream().map(pi -> InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(pi.getRefInput())).collect(Collectors.toSet());
        List<RefPrixPhyto> result = refPrixPhytoDao.forCode_scenarioIn(scenarioCodes)
                .addIn(RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, phytoObjectIds)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .findAll();
        Set<String> phytoObjectIdsWithPrice = result.stream().map(RefPrixPhyto::getPhytoObjectId).collect(Collectors.toSet());
        Set<String> phytoObjectIdsWithoutPrice = phytoObjectIds.stream().filter(not(phytoObjectIdsWithPrice::contains)).collect(Collectors.toSet());
        if (!phytoObjectIdsWithoutPrice.isEmpty()) {
            var refCountry = domainService.getCountry();
            scenarioCodes.forEach(scenarioCode -> phytoObjectIdsWithoutPrice.forEach(phytoObjectId -> {
                var pair = parseIdTraitementIdProduit(phytoObjectId);
                List<RefPrixPhyto> refPrixPhytoForCodeAMM = refInputPriceDao.findRefPrixPhytoWithCodeAMM(pair.getRight(), pair.getLeft(), scenarioCode, refCountry);
                result.addAll(refPrixPhytoForCodeAMM);
            }));
        }
        return result;
    }

    protected Collection<RefPrixPhyto> loadRefPrixPhytoForCampaigns(Collection<Integer> campaigns, Collection<DomainPhytoProductInput> phytoInputs) {
        Set<String> phytoObjectIds = phytoInputs.stream().map(pi -> InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(pi.getRefInput())).collect(Collectors.toSet());
        List<RefPrixPhyto> result =  refPrixPhytoDao.forCampaignIn(campaigns)
                .addIn(RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, phytoObjectIds)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .findAll();
        Set<String> phytoObjectIdsWithPrice = result.stream().map(RefPrixPhyto::getPhytoObjectId).collect(Collectors.toSet());
        Set<String> phytoObjectIdsWithoutPrice = phytoObjectIds.stream().filter(not(phytoObjectIdsWithPrice::contains)).collect(Collectors.toSet());
        if (!phytoObjectIdsWithoutPrice.isEmpty()) {
            var refCountry = domainService.getCountry();
            campaigns.forEach(campaign -> phytoObjectIdsWithoutPrice.forEach(phytoObjectId -> {
                var pair = parseIdTraitementIdProduit(phytoObjectId);
                List<RefPrixPhyto> refPrixPhytoForCodeAMM = refInputPriceDao.findRefPrixPhytoWithCodeAMM(pair.getRight(), pair.getLeft(), campaign, refCountry);
                result.addAll(refPrixPhytoForCodeAMM);
            }));
        }
        return result;
    }
    
    protected List<RefPrixIrrig> loadIrrigationScenarioPricesForScenarioCodes(Collection<String> scenarioCodes) {
        return refPrixIrrigDao.forCode_scenarioIn(scenarioCodes)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .addEquals(RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_M3)
                .findAll();
    }
    
    protected List<RefPrixIrrig> loadIrrigationScenarioPricesForCampaigns(Collection<Integer> campaigns) {
        return refPrixIrrigDao.forCampaignIn(campaigns)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .addEquals(RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_M3)
                .findAll();
    }
    
    protected List<RefPrixPot> loadPotScenarioPricesForScenarioCodes(Collection<String> scenarioCodes, Collection<String> caracteristic1s) {
        return refPrixPotDao.forCode_scenarioIn(scenarioCodes)
                .addIn(RefPrixPot.PROPERTY_CARACTERISTIC1, caracteristic1s)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .findAll();
    }
    
    protected List<RefPrixPot> loadPotScenarioPricesCampaigns(Collection<Integer> campaigns, Collection<String> caracteristic1s) {
        return refPrixPotDao.forCampaignIn(campaigns)
                .addIn(RefPrixPot.PROPERTY_CARACTERISTIC1, caracteristic1s)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .findAll();
    }
    
    protected List<RefPrixSubstrate> loadSubstrateScenarioPricesForScenarioCodes(Collection<String> scenarioCodes, Set<Pair<String, String>> caracteristics) {
        Set<String> caracteristic1s = caracteristics.stream().map(Pair::getKey).collect(Collectors.toSet());
    
        List<RefPrixSubstrate> refPrixSubstrates = refPrixSubstrateDao.forCode_scenarioIn(scenarioCodes)
                .addIn(RefPrixPot.PROPERTY_CARACTERISTIC1, caracteristic1s)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .findAll();
        refPrixSubstrates.removeIf(rs -> !caracteristics.contains(Pair.of(rs.getCaracteristic1(), rs.getCaracteristic2())));
        return refPrixSubstrates;
    }
    
    protected List<RefPrixSubstrate> loadSubstrateScenarioPricesForCampaigns(Collection<Integer> campaigns, Set<Pair<String, String>> caracteristics) {
        Set<String> caracteristic1s = caracteristics.stream().map(Pair::getKey).collect(Collectors.toSet());
        
        List<RefPrixSubstrate> refPrixSubstrates = refPrixSubstrateDao.forCampaignIn(campaigns)
                .addIn(RefPrixSubstrate.PROPERTY_CARACTERISTIC1, caracteristic1s)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .findAll();
        refPrixSubstrates.removeIf(rps -> !caracteristics.contains(Pair.of(rps.getCaracteristic1(), rps.getCaracteristic2())));
        return refPrixSubstrates;
    }
    
    protected Map<RefPrixAutreScenarioKey, List<RefPrixAutre>> loadOtherInputScenarioPricesForScenarioCodes(
            Collection<String> scenarioCodes,
            Collection<DomainOtherInput> domainOtherInputs) {
        
        Set<String> inputType_c0 = new HashSet<>();
        Set<RefPrixAutreKey> domainOtherInputKeys = new HashSet<>();
        
        domainOtherInputs.forEach(
                doi ->
                {
                    final RefOtherInput refInput = doi.getRefInput();
                    final RefPrixAutreKey key = new RefPrixAutreKey(
                            refInput.getInputType_c0(), refInput.getCaracteristic1(), refInput.getCaracteristic2(), refInput.getCaracteristic3());
                    domainOtherInputKeys.add(key);
                    inputType_c0.add(refInput.getInputType_c0());
                });
        
        List<RefPrixAutre> refPrixAutres = refPrixAutreDao.forCode_scenarioIn(scenarioCodes)
                .addIn(RefPrixAutre.PROPERTY_INPUT_TYPE_C0, inputType_c0)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .findAll();
    
        Map<RefPrixAutreScenarioKey, List<RefPrixAutre>> result = new HashMap<>();
        refPrixAutres.stream()
                .filter(refPrixAutre -> {
                    final RefPrixAutreKey key = new RefPrixAutreKey(
                            refPrixAutre.getInputType_c0(),
                            refPrixAutre.getCaracteristic1(),
                            refPrixAutre.getCaracteristic2(),
                            refPrixAutre.getCaracteristic3());
                    return domainOtherInputKeys.contains(key);
                })
                .forEach(refPrixAutre ->
                        result.merge(
                                new RefPrixAutreScenarioKey(
                                        refPrixAutre.getCode_scenario(),
                                        refPrixAutre.getInputType_c0(),
                                        refPrixAutre.getCaracteristic1(),
                                        refPrixAutre.getCaracteristic2(),
                                        refPrixAutre.getCaracteristic3()),
                                Lists.newArrayList(refPrixAutre), this::mergeRefInputPrices));
        return result;
    }
    
    protected Map<RefPrixAutreCampaignKey, List<RefPrixAutre>> loadDomainOtherInputRefPricesForCampaigns(
            Collection<Integer> campaigns,
            Collection<DomainOtherInput> domainOtherInputs) {
        
        Set<String> inputType_c0 = new HashSet<>();
        Set<RefPrixAutreKey> domainOtherInputKeys = new HashSet<>();
        
        domainOtherInputs.forEach(
                doi ->
                {
                    final RefOtherInput refInput = doi.getRefInput();
                    final RefPrixAutreKey key = new RefPrixAutreKey(
                            refInput.getInputType_c0(), refInput.getCaracteristic1(), refInput.getCaracteristic2(), refInput.getCaracteristic3());
                    domainOtherInputKeys.add(key);
                    inputType_c0.add(refInput.getInputType_c0());
                });
        
        List<RefPrixAutre> refPrixAutres = refPrixAutreDao.forCampaignIn(campaigns)
                .addIn(RefPrixAutre.PROPERTY_INPUT_TYPE_C0, inputType_c0)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .findAll();
        
        Map<RefPrixAutreCampaignKey, List<RefPrixAutre>> result = new HashMap<>();
        refPrixAutres.stream()
                .filter(refPrixAutre -> {
                    final RefPrixAutreKey key = new RefPrixAutreKey(
                            refPrixAutre.getInputType_c0(),
                            refPrixAutre.getCaracteristic1(),
                            refPrixAutre.getCaracteristic2(),
                            refPrixAutre.getCaracteristic3());
                    return domainOtherInputKeys.contains(key);
                })
                .forEach(refPrixAutre ->
                        result.merge(
                                new RefPrixAutreCampaignKey(
                                        refPrixAutre.getCampaign(),
                                        refPrixAutre.getInputType_c0(),
                                        refPrixAutre.getCaracteristic1(),
                                        refPrixAutre.getCaracteristic2(),
                                        refPrixAutre.getCaracteristic3()),
                                Lists.newArrayList(refPrixAutre), this::mergeRefInputPrices));
        return result;
    }
    
    protected record RefPrixAutreKey (String inputType_c0, String caracteristic1, String caracteristic2, String caracteristic3) {}
    
    protected record RefPrixAutreScenarioKey (String scenario, String inputType_c0, String caracteristic1, String caracteristic2, String caracteristic3) {}
    
    protected record RefPrixAutreCampaignKey (Integer campaign, String inputType_c0, String caracteristic1, String caracteristic2, String caracteristic3) {}
    
    protected Map<RefPrixSemisScenarioKey, List<RefPrixEspece>> loadSeedInputScenarioPricesForScenarioCodes(
            Collection<String> scenarioCodes,
            Collection<DomainSeedLotInput> domainSeedLotInputs) {
        
        Set<String> code_espece_botanique = new HashSet<>();
        Set<RefPrixSemisKey> domainSpeciesInputKeys = new HashSet<>();
        
        domainSeedLotInputs.stream()
                .map(DomainSeedLotInput::getDomainSeedSpeciesInput)
                .filter(CollectionUtils::isNotEmpty)
                .flatMap(Collection::stream)
                .forEach(seedSpeciesInput -> {
                    final SeedPrice seedPrice = seedSpeciesInput.getSeedPrice();
                    boolean isPriceIncludeTreatment = seedPrice != null ?
                            seedPrice.isIncludedTreatment() :
                            seedSpeciesInput.isChemicalTreatment() || seedSpeciesInput.isBiologicalSeedInoculation();

                    final RefEspece refEspece = seedSpeciesInput.getSpeciesSeed().getSpecies();
                    code_espece_botanique.add(refEspece.getCode_espece_botanique());
                    RefPrixSemisKey key = new RefPrixSemisKey(
                            refEspece.getCode_espece_botanique(),
                            refEspece.getCode_qualifiant_AEE(),
                            seedSpeciesInput.getSeedType(),
                            isPriceIncludeTreatment,
                            seedSpeciesInput.isOrganic()
                    );
                    domainSpeciesInputKeys.add(key);
                });
    
        List<RefPrixEspece> refPrixEspeces = refPrixEspeceDao.forCode_scenarioIn(scenarioCodes)
                .addIn(RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, code_espece_botanique)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .findAll();
    
    
        Map<RefPrixSemisScenarioKey, List<RefPrixEspece>> result = new HashMap<>();
        refPrixEspeces.stream()
                .filter(refPrixEspece -> {
                    final RefPrixSemisKey key = new RefPrixSemisKey(
                            refPrixEspece.getCode_espece_botanique(),
                            refPrixEspece.getCode_qualifiant_AEE(),
                            refPrixEspece.getSeedType(),
                            refPrixEspece.isTreatment(),
                            refPrixEspece.isOrganic());
                    return domainSpeciesInputKeys.contains(key);
                })
                .forEach(refPrixEspece ->
                        result.merge(
                                new RefPrixSemisScenarioKey(
                                        refPrixEspece.getCode_scenario(),
                                        refPrixEspece.getCode_espece_botanique(),
                                        refPrixEspece.getCode_qualifiant_AEE(),
                                        refPrixEspece.getSeedType(),
                                        refPrixEspece.isTreatment(),
                                        refPrixEspece.isOrganic()),
                                Lists.newArrayList(refPrixEspece), this::mergeRefInputPrices));
        
        return result;
    }
    
    protected Map<RefPrixSemisCampaignKey, List<RefPrixEspece>> loadSeedInputRefPricesForCampaigns(
            Collection<Integer> campaigns,
            Collection<DomainSeedLotInput> domainSeedLotInputs) {
        
        Set<String> code_espece_botanique = new HashSet<>();
        Set<RefPrixSemisKey> domainSpeciesInputKeys = new HashSet<>();

        // Par défaut, si un traitement est déclaré dans l'action de semis, on prend le prix de la semence traitée.
        // Mais si la case traitement inclus n'a pas été cochée,
        // on prend le prix de la semence non traitée.
        // car le prix de traitement est déclaré indépendant
        domainSeedLotInputs.stream().filter(dsl -> CollectionUtils.isNotEmpty(dsl.getDomainSeedSpeciesInput()))
            .forEach(
                dsl -> {
                    final boolean isLotIncludedTreatment = dsl.isSeedCoatedTreatment();
                    boolean isOrganic = dsl.isOrganic();

                    dsl.getDomainSeedSpeciesInput()
                    .forEach(seedSpeciesInput -> {
                        final RefEspece refEspece = seedSpeciesInput.getSpeciesSeed().getSpecies();
                        code_espece_botanique.add(refEspece.getCode_espece_botanique());
                        domainSpeciesInputKeys.add(
                            new RefPrixSemisKey(
                                    refEspece.getCode_espece_botanique(),
                                    refEspece.getCode_qualifiant_AEE(),
                                    seedSpeciesInput.getSeedType(),
                                    isLotIncludedTreatment,
                                    isOrganic
                            ));
                        // fallbackKey
                        if (!isOrganic) {
                            domainSpeciesInputKeys.add(
                                new RefPrixSemisKey(
                                        refEspece.getCode_espece_botanique(),
                                        refEspece.getCode_qualifiant_AEE(),
                                        seedSpeciesInput.getSeedType(),
                                        isLotIncludedTreatment,
                                        false
                                ));
                        }

                    });
            }
        );

        List<RefPrixEspece> refPrixEspeces = refPrixEspeceDao.forCode_espece_botaniqueIn(code_espece_botanique)
                .addEquals(RefInputPrice.PROPERTY_ACTIVE, true)
                .addNotNull(RefInputPrice.PROPERTY_PRICE)
                .findAll();

        List<RefPrixEspece> sameCampaignRefPrices = refPrixEspeces.stream().filter(rpe -> campaigns.contains(rpe.getCampaign())).toList();

        Map<RefPrixSemisCampaignKey, List<RefPrixEspece>> refPriceForSameCampaign = mergeRefSpeciesPricesResult(domainSpeciesInputKeys, sameCampaignRefPrices);

        Set<RefPrixSemisKey> keyFounds = new HashSet<>();

        // String code_espece_botanique, String code_qualifiant_AEE, SeedType seedType, boolean treatment, boolean organic
        refPriceForSameCampaign.keySet().forEach(rpsck -> keyFounds.add(new RefPrixSemisKey(rpsck.code_espece_botanique, rpsck.code_qualifiant_AEE, rpsck.seedType, rpsck.treatment, rpsck.organic)));
        domainSpeciesInputKeys.removeAll(keyFounds);
        for (RefPrixSemisKey domainSpeciesInputKey : domainSpeciesInputKeys) {
            List<RefPrixEspece> list = refPrixEspeces.stream()
                    .filter(rpe -> rpe.getCode_espece_botanique().contentEquals(domainSpeciesInputKey.code_espece_botanique))
                    .filter(rpe -> rpe.getCode_qualifiant_AEE().contentEquals(domainSpeciesInputKey.code_qualifiant_AEE))
                    .filter(rpe -> rpe.getSeedType().equals(domainSpeciesInputKey.seedType))
                    .filter(rpe -> rpe.isTreatment() == domainSpeciesInputKey.treatment)
                    .filter(rpe -> rpe.isOrganic() == domainSpeciesInputKey.organic)
                    .toList();
            if (list.isEmpty()) {
                list = refPrixEspeces.stream()
                        .filter(rpe -> rpe.getCode_espece_botanique().contentEquals(domainSpeciesInputKey.code_espece_botanique))
                        .filter(rpe -> rpe.getCode_qualifiant_AEE().contentEquals(domainSpeciesInputKey.code_qualifiant_AEE))
                        .filter(rpe -> rpe.getSeedType().equals(domainSpeciesInputKey.seedType))
                        .filter(rpe -> rpe.isTreatment() == domainSpeciesInputKey.treatment)
                        .filter(rpe -> rpe.isOrganic() != domainSpeciesInputKey.organic)
                        .toList();
            }
            if (!list.isEmpty()) {
                int maxCampaign = list
                        .stream()
                        .map(RefPrixEspece::getCampaign)
                        .filter(Objects::nonNull)
                        .max(Integer::compareTo).orElse(Integer.MAX_VALUE);
                list = list
                        .stream()
                        .filter(refPrixEspece ->
                                refPrixEspece.getCampaign() != null
                                        && maxCampaign == refPrixEspece.getCampaign())
                        .collect(Collectors.toList());
                refPriceForSameCampaign.putAll(mergeRefSpeciesPricesResult(domainSpeciesInputKeys, list));
            }

        }
        return refPriceForSameCampaign;
    }

    private Map<RefPrixSemisCampaignKey, List<RefPrixEspece>>  mergeRefSpeciesPricesResult(
            Set<RefPrixSemisKey> domainSpeciesInputKeys,
            List<RefPrixEspece> refPrixEspeces) {
        Map<RefPrixSemisCampaignKey, List<RefPrixEspece>> result = new HashMap<>();
        refPrixEspeces.stream()
                .filter(refPrixEspece -> {
                    final RefPrixSemisKey key = new RefPrixSemisKey(
                            refPrixEspece.getCode_espece_botanique(),
                            refPrixEspece.getCode_qualifiant_AEE(),
                            refPrixEspece.getSeedType(),
                            refPrixEspece.isTreatment(),
                            refPrixEspece.isOrganic());
                    return domainSpeciesInputKeys.contains(key);
                })
                .forEach(refPrixEspece ->
                        result.merge(
                                new RefPrixSemisCampaignKey(
                                        refPrixEspece.getCampaign(),
                                        refPrixEspece.getCode_espece_botanique(),
                                        refPrixEspece.getCode_qualifiant_AEE(),
                                        refPrixEspece.getSeedType(),
                                        refPrixEspece.isTreatment(),
                                        refPrixEspece.isOrganic()),
                                Lists.newArrayList(refPrixEspece), this::mergeRefInputPrices));
        return result;
    }

    protected record RefPrixSemisKey (String code_espece_botanique, String code_qualifiant_AEE, SeedType seedType, boolean treatment, boolean organic) {}
    
    protected record RefPrixSemisScenarioKey (String scenario, String code_espece_botanique, String code_qualifiant_AEE, SeedType seedType, boolean treatment, boolean organic) {}
    
    protected record RefPrixSemisCampaignKey (Integer campaign, String code_espece_botanique, String code_qualifiant_AEE, SeedType seedType, boolean treatment, boolean organic) {}
    
    protected static InputRefPrice getScenarioInputRePrice(
            AbstractDomainInputStockUnit input,
            String scenarioCode,
            PriceAndUnit refPrice) {
        
        InputRefPrice inputRefPrice = new InputRefPrice(
                input.getTopiaId(),
                Optional.of(scenarioCode),
                Optional.empty(),
                Optional.empty(),
                refPrice,
                false);
        
        return inputRefPrice;
    }
    
    protected static InputRefPrice getCampaignInputRePrice(
            AbstractDomainInputStockUnit input,
            Integer campaign,
            PriceAndUnit refPrice,
            boolean fallbackRefPrice) {
        
        InputRefPrice inputRefPrice = new InputRefPrice(
                input.getTopiaId(),
                Optional.empty(),
                Optional.of(campaign),
                Optional.empty(),
                refPrice,
                fallbackRefPrice);
        
        return inputRefPrice;
    }
    
    protected static Map<OrganicProductUnit, List<RefInputUnitPriceUnitConverter>> getDoseUnitToPriceUnitConverterByProductUnit(
            Collection<RefInputUnitPriceUnitConverter> doseUnitConverters) {

        Map<OrganicProductUnit, List<RefInputUnitPriceUnitConverter>> doseUnitToPriceUnitConverterByPriceUnit = new HashMap<>();
        CollectionUtils.emptyIfNull(doseUnitConverters).stream()
                .filter(duc -> duc.getOrganicProductUnit() != null)
                .forEach(doseUnitConverter -> {
                    OrganicProductUnit organicProductUnit = doseUnitConverter.getOrganicProductUnit();
                    List<RefInputUnitPriceUnitConverter> doseUnitConvertersForInputUsageUnit = doseUnitToPriceUnitConverterByPriceUnit.computeIfAbsent(organicProductUnit, k -> new ArrayList<>());
                    doseUnitConvertersForInputUsageUnit.add(doseUnitConverter);
        });

        return doseUnitToPriceUnitConverterByPriceUnit;
    }

    protected static Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> getConvertersByPriceUnits(Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters) {
        Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> convertersByPriceUnits = new HashMap<>();
        CollectionUtils.emptyIfNull(refInputUnitPriceUnitConverters).forEach(c -> {
            PriceUnit priceUnit = c.getPriceUnit();
            List<RefInputUnitPriceUnitConverter> convertersForPriceUnit = convertersByPriceUnits.computeIfAbsent(priceUnit, k -> new ArrayList<>());
            convertersForPriceUnit.add(c);
        });
        return convertersByPriceUnits;
    }
    
    @Override
    public PaginationResult<Scenario> getScenarioLabelByScenarioCode(ScenarioFilter scenarioFilter) {
        return refInputPriceDao.findAllScenariosByScenarioCodeForName(scenarioFilter);
    }
    
    @Override
    public Collection<Scenario> getScenariosForCodes(Collection<String> scenarioCodes) {
        return refInputPriceDao.findAllScenariosForCodes(scenarioCodes);
    }
}
