package fr.inra.agrosyst.services.common.export;

/*
 * #%L
 * SISPEA Services
 * %%
 * Copyright (C) 2014 - 2019 AFB
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import org.nuiton.util.DateUtil;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public final class AgrosystDateUtils {

    private AgrosystDateUtils() {
    }

    /**
     * Méthode permettant de savoir si la {@code date1} est antérieure ou égale à la {@code date2}
     */
    public static boolean isBeforeOrEquals(Date date1, Date date2) {
        // Écriture des tests par la négation pour gérer les cas d'egalité parfaite entre les 2 dates
        return !date1.after(date2);
    }

    /**
     * Méthode permettant de savoir si la {@code date1} est postérieure ou égale à la {@code date2}
     */
    public static boolean isAfterOrEquals(Date date1, Date date2) {
        // Écriture des tests par la négation pour gérer les cas d'egalité parfaite entre les 2 dates
        return !date1.before(date2);
    }

    public static Predicate<Date> isAfterPredicate(final Date when) {
        return input -> input.after(when);
    }

    /**
     * Créé une date normée à 12h00 le jour indiqué. Pour éviter les problèmes on réutilise la méthode
     * {@link #createNoonDate(int, int, int)}
     *
     * @see #createNoonDate(int, int, int)
     */
    public static Date createNoonDate(Date source) {
        if (source == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(source);
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH) + 1;
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        Date result = createNoonDate(dd, mm, yy);
        return result;
    }

    /**
     * Créé une date normée à 12h00 le jour indiqué
     *
     * @see DateUtil#createDate(int, int, int, int, int, int)
     */
    public static Date createNoonDate(int dd, int mm, int yy) {
        return DateUtil.createDate(0, 0, 12, dd, mm, yy);
    }

    public static Calendar toCalendarOrNull(Date date) {
        Calendar result = null;
        if (date != null) {
            result = org.apache.commons.lang3.time.DateUtils.toCalendar(date);
        }
        return result;
    }

    public static Optional<Calendar> toCalendar(Optional<Date> date) {
        return Optional.fromNullable(toCalendarOrNull(date.orNull()));
    }

    public static LocalDateTime toLocalDateTimeOrNull(Date date) {
        return toLocalDateTime(date).orNull();
    }

    public static Optional<LocalDateTime> toLocalDateTime(Date date) {
        Optional<LocalDateTime> result = Optional.fromNullable(date)
                .transform(Date::toInstant)
                .transform(instant -> LocalDateTime.ofInstant(instant, ZoneId.systemDefault()));
        return result;
    }

    public static Date toDate(LocalDateTime localDateTime) {
        Preconditions.checkNotNull(localDateTime);
        Date result = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        return result;
    }

    public static String formatDuration(LocalDateTime from, LocalDateTime to) {
        long s = ChronoUnit.SECONDS.between(from, to);
        long m = ChronoUnit.MINUTES.between(from, to);
        long h = ChronoUnit.HOURS.between(from, to);
        long d = ChronoUnit.DAYS.between(from, to);
        String result = String.format("%ds", (s % 60));
        if (m > 0) {
            result = String.format("%dm", (m % 60)) + result;
        }
        if (h > 0) {
            result = String.format("%dh", (h % 24)) + result;
        }
        if (d > 0) {
            result = String.format("%dd", d) + result;
        }
        return result;
    }

}
