package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutilImpl;
import fr.inra.agrosyst.services.common.CommonService;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

import java.util.List;

/**
 * Automoteur import model.
 * 
 * Columns:
 * <ul>
 * <li>Type materiel 1
 * <li>type materiel 2
 * <li>Type materiel 3
 * <li>Type materiel 4
 * <li>idtypemateriel
 * <li>idsoustypemateriel
 * <li>commentaire sur materiel
 * <li>Millésime
 * <li>codetype
 * <li>Coderef
 * <li>prix neuf € unité
 * <li>prix moyen achat
 * <li>unité
 * <li>unité / an
 * <li>charges fixes annuelle unité
 * <li>charges fixes €/an
 * <li>charges fixes €/unité de volume de travail annuel unité
 * <li>charges fixes €/unité de volume de travail annuel
 * <li>Réparations unité
 * <li>Réparations €/unité de travail annuel
 * <li>coût total unité
 * <li>coût total € / unité de travail annuel
 * <li>performance unité
 * <li>performance
 * <li>performance coût total unité
 * <li>performance coût total €
 * <li>données puissance 1 adéquate unité
 * <li>données puissance 1 adéquate
 * <li>données taux de charge moteur
 * <li>donnees amortissement 1
 * <li>donnees amortissement 2
 * <li>donnees transport 1 unite
 * <li>donnees transport 1
 * <li>donnees transport 2 unite
 * <li>donnees transport 2
 * <li>donnees transport 3 unite
 * <li>donnees transport 3
 * </ul>
 * 
 * @author Eric Chatellier
 */
public class RefMaterielOutilModel extends AbstractAgrosystModel<RefMaterielOutil> implements ExportModel<RefMaterielOutil> {

    public RefMaterielOutilModel() {
        super(CSV_SEPARATOR);
    }

    @Override
    public void pushCsvHeaderNames(List<String> headerNames) {
        super.pushCsvHeaderNames(headerNames);

        newMandatoryColumn("Type materiel 1", RefMaterielOutil.PROPERTY_TYPE_MATERIEL1, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("Type materiel 2", RefMaterielOutil.PROPERTY_TYPE_MATERIEL2, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("Type materiel 3", RefMaterielOutil.PROPERTY_TYPE_MATERIEL3, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("Type materiel 4", RefMaterielOutil.PROPERTY_TYPE_MATERIEL4, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("idtypemateriel", RefMaterielOutil.PROPERTY_IDTYPEMATERIEL);
        newMandatoryColumn("idsoustypemateriel", RefMaterielOutil.PROPERTY_IDSOUSTYPEMATERIEL);
        newMandatoryColumn("commentaire sur materiel", RefMaterielOutil.PROPERTY_COMMENTAIRE_SUR_MATERIEL);
        newMandatoryColumn("petitmateriel", RefMaterielOutil.PROPERTY_PETIT_MATERIEL, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("Millésime", RefMaterielOutil.PROPERTY_MILLESIME, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("codetype", RefMaterielOutil.PROPERTY_CODETYPE);
        newMandatoryColumn("Coderef", RefMaterielOutil.PROPERTY_CODE_REF);
        newMandatoryColumn("prix neuf € unité", RefMaterielOutil.PROPERTY_PRIX_NEUF_UNITE);
        newMandatoryColumn("prix moyen achat", RefMaterielOutil.PROPERTY_PRIX_MOYEN_ACHAT, DOUBLE_PARSER);
        newMandatoryColumn("unité", RefMaterielOutil.PROPERTY_UNITE);
        newMandatoryColumn("unité / an", RefMaterielOutil.PROPERTY_UNITE_PAR_AN, DOUBLE_PARSER);
        newMandatoryColumn("charges fixes annuelle unité", RefMaterielOutil.PROPERTY_CHARGES_FIXES_ANNUELLE_UNITE);
        newMandatoryColumn("charges fixes €/an", RefMaterielOutil.PROPERTY_CHARGES_FIXES_PAR_AN, DOUBLE_PARSER);
        newMandatoryColumn("charges fixes €/unité de volume de travail annuel unité", RefMaterielOutil.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL_UNITE);
        newMandatoryColumn("charges fixes €/unité de volume de travail annuel", RefMaterielOutil.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL, DOUBLE_PARSER);
        newMandatoryColumn("Réparations unité", RefMaterielOutil.PROPERTY_REPARATIONS_UNITE);
        newMandatoryColumn("Réparations €/unité de travail annuel", RefMaterielOutil.PROPERTY_REPARATIONS_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_PARSER);
        newMandatoryColumn("coût total unité", RefMaterielOutil.PROPERTY_COUT_TOTAL_UNITE);
        newMandatoryColumn("coût total € / unité de travail annuel", RefMaterielOutil.PROPERTY_COUT_TOTAL_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_PARSER);
        newMandatoryColumn("performance unité", RefMaterielOutil.PROPERTY_PERFORMANCE_UNITE, AGROSYST_MATERIEL_WORK_RATE_UNIT_PARSER);
        newMandatoryColumn("performance", RefMaterielOutil.PROPERTY_PERFORMANCE, DOUBLE_PARSER);
        newMandatoryColumn("performance coût total unité", RefMaterielOutil.PROPERTY_PERFORMANCE_COUT_TOTAL_UNITE);
        newMandatoryColumn("performance coût total €", RefMaterielOutil.PROPERTY_PERFORMANCE_COUT_TOTAL, DOUBLE_PARSER);
        newMandatoryColumn("données puissance 1 adéquate unité", RefMaterielOutil.PROPERTY_DONNEES_PUISSANCE1_ADEQUATE_UNITE);
        newMandatoryColumn("données puissance 1 adéquate", RefMaterielOutil.PROPERTY_DONNEES_PUISSANCE1_ADEQUATE, DOUBLE_PARSER);
        newMandatoryColumn("données taux de charge moteur", RefMaterielOutil.PROPERTY_DONNEES_TAUX_DE_CHARGE_MOTEUR, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("donnees amortissement 1", RefMaterielOutil.PROPERTY_DONNEES_AMORTISSEMENT1, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("donnees amortissement 2", RefMaterielOutil.PROPERTY_DONNEES_AMORTISSEMENT2, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("donnees transport 1 unite", RefMaterielOutil.PROPERTY_DONNEES_TRANSPORT1_UNITE);
        newMandatoryColumn("donnees transport 1", RefMaterielOutil.PROPERTY_DONNEES_TRANSPORT1);
        newMandatoryColumn("donnees transport 2 unite", RefMaterielOutil.PROPERTY_DONNEES_TRANSPORT2_UNITE, AGROSYST_MATERIEL_TRANSPORT_UNIT_PARSER);
        newMandatoryColumn("donnees transport 2", RefMaterielOutil.PROPERTY_DONNEES_TRANSPORT2);
        newMandatoryColumn("donnees transport 3 unite", RefMaterielOutil.PROPERTY_DONNEES_TRANSPORT3_UNITE);
        newMandatoryColumn("donnees transport 3", RefMaterielOutil.PROPERTY_DONNEES_TRANSPORT3);
        newMandatoryColumn("code materiel GES'TIM", RefMaterielOutil.PROPERTY_CODE_MATERIEL__GESTIM);
        newMandatoryColumn("masse (kg)", RefMaterielOutil.PROPERTY_MASSE, DOUBLE_PARSER);
        newMandatoryColumn("durée de vie théorique -obsolescence constructeur- (années)", RefMaterielOutil.PROPERTY_DUREE_VIE_THEORIQUE, INT_PARSER);
        newMandatoryColumn("Code EDI", RefMaterielOutil.PROPERTY_CODE_EDI);
        newMandatoryColumn("source", RefMaterielOutil.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefMaterielOutil.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefMaterielOutil, Object>> getColumnsForExport() {
        ModelBuilder<RefMaterielOutil> modelBuilder = new ModelBuilder<>();
        
        modelBuilder.newColumnForExport("Type materiel 1", RefMaterielOutil.PROPERTY_TYPE_MATERIEL1);
        modelBuilder.newColumnForExport("Type materiel 2", RefMaterielOutil.PROPERTY_TYPE_MATERIEL2);
        modelBuilder.newColumnForExport("Type materiel 3", RefMaterielOutil.PROPERTY_TYPE_MATERIEL3);
        modelBuilder.newColumnForExport("Type materiel 4", RefMaterielOutil.PROPERTY_TYPE_MATERIEL4);
        modelBuilder.newColumnForExport("idtypemateriel", RefMaterielOutil.PROPERTY_IDTYPEMATERIEL);
        modelBuilder.newColumnForExport("idsoustypemateriel", RefMaterielOutil.PROPERTY_IDSOUSTYPEMATERIEL);
        modelBuilder.newColumnForExport("commentaire sur materiel", RefMaterielOutil.PROPERTY_COMMENTAIRE_SUR_MATERIEL);
        modelBuilder.newColumnForExport("petitmateriel", RefMaterielOutil.PROPERTY_PETIT_MATERIEL, T_F_FORMATTER);
        modelBuilder.newColumnForExport("Millésime", RefMaterielOutil.PROPERTY_MILLESIME, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("codetype", RefMaterielOutil.PROPERTY_CODETYPE);
        modelBuilder.newColumnForExport("Coderef", RefMaterielOutil.PROPERTY_CODE_REF);
        modelBuilder.newColumnForExport("prix neuf € unité", RefMaterielOutil.PROPERTY_PRIX_NEUF_UNITE);
        modelBuilder.newColumnForExport("prix moyen achat", RefMaterielOutil.PROPERTY_PRIX_MOYEN_ACHAT, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("unité", RefMaterielOutil.PROPERTY_UNITE);
        modelBuilder.newColumnForExport("unité / an", RefMaterielOutil.PROPERTY_UNITE_PAR_AN, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("charges fixes annuelle unité", RefMaterielOutil.PROPERTY_CHARGES_FIXES_ANNUELLE_UNITE);
        modelBuilder.newColumnForExport("charges fixes €/an", RefMaterielOutil.PROPERTY_CHARGES_FIXES_PAR_AN, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("charges fixes €/unité de volume de travail annuel unité", RefMaterielOutil.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL_UNITE);
        modelBuilder.newColumnForExport("charges fixes €/unité de volume de travail annuel", RefMaterielOutil.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Réparations unité", RefMaterielOutil.PROPERTY_REPARATIONS_UNITE);
        modelBuilder.newColumnForExport("Réparations €/unité de travail annuel", RefMaterielOutil.PROPERTY_REPARATIONS_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("coût total unité", RefMaterielOutil.PROPERTY_COUT_TOTAL_UNITE);
        modelBuilder.newColumnForExport("coût total € / unité de travail annuel", RefMaterielOutil.PROPERTY_COUT_TOTAL_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("performance unité", RefMaterielOutil.PROPERTY_PERFORMANCE_UNITE, AGROSYST_MATERIEL_WORK_RATE_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("performance", RefMaterielOutil.PROPERTY_PERFORMANCE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("performance coût total unité", RefMaterielOutil.PROPERTY_PERFORMANCE_COUT_TOTAL_UNITE);
        modelBuilder.newColumnForExport("performance coût total €", RefMaterielOutil.PROPERTY_PERFORMANCE_COUT_TOTAL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("données puissance 1 adéquate unité", RefMaterielOutil.PROPERTY_DONNEES_PUISSANCE1_ADEQUATE_UNITE);
        modelBuilder.newColumnForExport("données puissance 1 adéquate", RefMaterielOutil.PROPERTY_DONNEES_PUISSANCE1_ADEQUATE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("données taux de charge moteur", RefMaterielOutil.PROPERTY_DONNEES_TAUX_DE_CHARGE_MOTEUR, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("donnees amortissement 1", RefMaterielOutil.PROPERTY_DONNEES_AMORTISSEMENT1, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("donnees amortissement 2", RefMaterielOutil.PROPERTY_DONNEES_AMORTISSEMENT2, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("donnees transport 1 unite", RefMaterielOutil.PROPERTY_DONNEES_TRANSPORT1_UNITE);
        modelBuilder.newColumnForExport("donnees transport 1", RefMaterielOutil.PROPERTY_DONNEES_TRANSPORT1);
        modelBuilder.newColumnForExport("donnees transport 2 unite", RefMaterielOutil.PROPERTY_DONNEES_TRANSPORT2_UNITE, AGROSYST_MATERIEL_TRANSPORT_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("donnees transport 2", RefMaterielOutil.PROPERTY_DONNEES_TRANSPORT2);
        modelBuilder.newColumnForExport("donnees transport 3 unite", RefMaterielOutil.PROPERTY_DONNEES_TRANSPORT3_UNITE);
        modelBuilder.newColumnForExport("donnees transport 3", RefMaterielOutil.PROPERTY_DONNEES_TRANSPORT3);
        modelBuilder.newColumnForExport("code materiel GES'TIM", RefMaterielOutil.PROPERTY_CODE_MATERIEL__GESTIM);
        modelBuilder.newColumnForExport("masse (kg)", RefMaterielOutil.PROPERTY_MASSE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("durée de vie théorique -obsolescence constructeur- (années)", RefMaterielOutil.PROPERTY_DUREE_VIE_THEORIQUE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Code EDI", RefMaterielOutil.PROPERTY_CODE_EDI);
        modelBuilder.newColumnForExport("source", RefMaterielOutil.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefMaterielOutil.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefMaterielOutil newEmptyInstance() {
        RefMaterielOutil refMaterielOutil = new RefMaterielOutilImpl();
        refMaterielOutil.setActive(true);
        return refMaterielOutil;
    }

}
