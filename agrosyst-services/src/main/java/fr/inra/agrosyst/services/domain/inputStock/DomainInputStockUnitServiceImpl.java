package fr.inra.agrosyst.services.domain.inputStock;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnitTopiaDao;
import fr.inra.agrosyst.api.entities.CompagneType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainFuelInput;
import fr.inra.agrosyst.api.entities.DomainFuelInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.DomainIrrigationInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainManualWorkforceInput;
import fr.inra.agrosyst.api.entities.DomainManualWorkforceInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainMechanizedWorkforceInput;
import fr.inra.agrosyst.api.entities.DomainMechanizedWorkforceInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.DomainOtherInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainPotInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInputImpl;
import fr.inra.agrosyst.api.entities.DomainSeedLotInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceTopiaDao;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.IrrigationUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.SeedPrice;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrgaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefOtherInputTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPotTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSubstrateTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.domain.CropPersistResult;
import fr.inra.agrosyst.api.services.domain.CroppingPlanEntryDto;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedLotInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedSpeciesInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.InputPriceDto;
import fr.inra.agrosyst.api.services.referential.DomainReferentialInputs;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.EntityUsageService;
import fr.inra.agrosyst.services.input.InputUsageServiceImpl;
import lombok.NonNull;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MultiMapUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Setter
public class DomainInputStockUnitServiceImpl extends AbstractAgrosystService implements DomainInputStockUnitService {

    private static final Log LOGGER = LogFactory.getLog(DomainInputStockUnitServiceImpl.class);
    public static final String DELIMITER = ", ";

    protected AgrosystI18nService i18nService;
    protected BusinessAuthorizationService authorizationService;
    protected DomainService domainService;
    protected InputPriceService inputPriceService;
    protected InputUsageServiceImpl inputUsageService;
    protected EntityUsageService entityUsageService;
    protected ReferentialService referentialService;

    protected AbstractDomainInputStockUnitTopiaDao domainInputDao;

    protected DomainFuelInputTopiaDao domainFuelDao;
    protected DomainManualWorkforceInputTopiaDao domainManualWorkforceDao;
    protected DomainMechanizedWorkforceInputTopiaDao domainMechanizedWorkforceDao;
    protected DomainIrrigationInputTopiaDao domainIrrigationDao;
    protected DomainMineralProductInputTopiaDao domainMineralProductInputDao;
    protected DomainOrganicProductInputTopiaDao domainOrganicProductInputDao;
    protected DomainOtherInputTopiaDao domainOtherInputDao;
    protected DomainPhytoProductInputTopiaDao domainPhytoProductInputDao;
    protected DomainPotInputTopiaDao domainPotInputDao;
    protected DomainSeedLotInputTopiaDao domainSeedLotInputDao;
    protected DomainSeedSpeciesInputTopiaDao domainSeedSpeciesInputDao;
    protected DomainSubstrateInputTopiaDao domainSubstrateInputDao;
    protected InputPriceTopiaDao inputPriceDao;
    protected RefEspeceTopiaDao refEspeceDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;

    protected DomainTopiaDao domainDao;

    protected RefFertiMinUNIFATopiaDao refFertiMinUNIFADao;
    protected RefFertiOrgaTopiaDao refFertiOrgaDao;
    protected RefPotTopiaDao refPotDao;
    protected RefSubstrateTopiaDao refSubstrateDao;
    protected RefOtherInputTopiaDao refOtherInputDao;
    private boolean ifInputIsNotUsedAndRemovedPhyto;


    protected static final Binder<DomainFuelInput, DomainFuelInput> domainFuelInputBinder = BinderFactory.newBinder(DomainFuelInput.class);
    protected static final Binder<DomainManualWorkforceInput, DomainManualWorkforceInput> domainManualWorkforceInputBinder = BinderFactory.newBinder(DomainManualWorkforceInput.class);
    protected static final Binder<DomainMechanizedWorkforceInput, DomainMechanizedWorkforceInput> domainMechanizedWorkforceInputBinder = BinderFactory.newBinder(DomainMechanizedWorkforceInput.class);
    protected static final Binder<DomainIrrigationInput, DomainIrrigationInput> domainIrrigationInputBinder = BinderFactory.newBinder(DomainIrrigationInput.class);
    protected static final Binder<DomainMineralProductInput, DomainMineralProductInput> domainMineralProductInputBinder = BinderFactory.newBinder(DomainMineralProductInput.class);
    protected static final Binder<DomainOrganicProductInput, DomainOrganicProductInput> domainOrganicProductInputBinder = BinderFactory.newBinder(DomainOrganicProductInput.class);
    protected static final Binder<DomainOtherInput, DomainOtherInput> domainOtherInputBinder = BinderFactory.newBinder(DomainOtherInput.class);
    protected static final Binder<DomainPhytoProductInput, DomainPhytoProductInput> domainPhytoProductInputBinder = BinderFactory.newBinder(DomainPhytoProductInput.class);
    protected static final Binder<DomainPotInput, DomainPotInput> inputDomainPotInputBinder = BinderFactory.newBinder(DomainPotInput.class);
    protected static final Binder<DomainSeedLotInput, DomainSeedLotInput> domainSeedLotInputBinder = BinderFactory.newBinder(DomainSeedLotInput.class);
    protected static final Binder<DomainSeedSpeciesInput, DomainSeedSpeciesInput> domainSeedSpeciesInputBinder = BinderFactory.newBinder(DomainSeedSpeciesInput.class);
    protected static final Binder<DomainSubstrateInput, DomainSubstrateInput> domainSubstrateInputBinder = BinderFactory.newBinder(DomainSubstrateInput.class);

    @Override
    public DomainInputDto validateDomainInputStockUnit(AbstractDomainInputStockUnit domainInput) {
        // TODO
        return null;
    }

    @Override
    public void createOrUpdateDomainInputStock(
            Domain domain,
            Collection<DomainInputDto> domainInputDtos_,
            Map<String, CropPersistResult> cropsByOriginalIds) {

        if (domainInputDtos_ == null) {
            return;
        }
        long startTime = System.currentTimeMillis();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Start Lot Persistance of domain " + domain.getTopiaId());
        }

        DomainReferentialInputs domainInputReferential = referentialService.getDomainReferentialInputs(domainInputDtos_);
        Collection<DomainInputDto> domainInputDtos = domainInputReferential.domainInputDtos();

        createOrUpdateSeedLotInputs(
                domain,
                cropsByOriginalIds,
                domainInputDtos,
                domainInputReferential);


        createOrUpdateDomainInputsExceptSeedLot(
                domain,
                domainInputDtos,
                domainInputReferential);


        long endTime = System.currentTimeMillis();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Ending Lot Persistance of domain " + domain.getTopiaId() + " in " + ((startTime - endTime) / 1000.0) + "s");
        }
    }

    protected void createOrUpdateDomainInputsExceptSeedLot(
            Domain domain,
            Collection<DomainInputDto> domainInputDtos,
            DomainReferentialInputs domainInputReferential) {

        Collection<AbstractDomainInputStockUnit> allPersistedDomainInputs = domainInputDao
                .forDomainEquals(domain)
                .addNotEquals(AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS)
                .addNotEquals(AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.PLAN_COMPAGNE)
                .addNotEquals(AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE)
                .findAll();

        Map<String, AbstractDomainInputStockUnit> persistedDomainInputExceptSeedLotByIds = getDomainInputStockUnitsExceptSeedLotAndRelatedEntities(allPersistedDomainInputs)
                .stream()
                .collect(Collectors.toMap((TopiaEntity::getTopiaId), Function.identity()));

        // we look up for usage as if the input is used modification are limited
        Collection<AbstractDomainInputStockUnit> persistedDomainInputExceptSeedLot = persistedDomainInputExceptSeedLotByIds.values();
        Map<String, Boolean> isDomainInputUsedByDomainInputId = entityUsageService.getDomainInputUsageMap(persistedDomainInputExceptSeedLot);

        Collection<AbstractDomainInputStockUnit> inputsToCreateOrUpdate = createOrUpdateDomainInputsExceptLots(
                domain,
                domainInputDtos,
                persistedDomainInputExceptSeedLotByIds,
                isDomainInputUsedByDomainInputId,
                domainInputReferential);

        inputsToCreateOrUpdate.forEach(this::createOrUpdateDomainInput_);

        deleteDomainInputs(persistedDomainInputExceptSeedLotByIds, isDomainInputUsedByDomainInputId);

    }

    protected void createOrUpdateSeedLotInputs(
            Domain domain,
            Map<String, CropPersistResult> cropsByOriginalIds,
            Collection<DomainInputDto> domainInputDtos,
            DomainReferentialInputs domainInputReferential) {

        Collection<DomainSeedLotInput> allPersistedDomainSeedLot = domainSeedLotInputDao.forDomainEquals(domain).findAll();
        Map<String, DomainSeedLotInput> persistedSeedLotByIds = allPersistedDomainSeedLot.stream().collect(Collectors.toMap((TopiaEntity::getTopiaId), Function.identity()));

        // we look up for usage as if the input is used modification are limited
        Collection<DomainSeedLotInput> seedLotPersistedDomainInputs = persistedSeedLotByIds.values();
        Map<String, Boolean> isSeedLotAndRelatedUsedByDomainInputId = entityUsageService.getDomainSeedLotInputUsageMap(seedLotPersistedDomainInputs);

        MultiValuedMap<DomainPhytoProductInput, InputUsageServiceImpl.SeedingProductUsageToRemoveForDomainSeedingProduct> usageToRemovesForDomainSeedProduct =
                getUsagesToRemoveWhenRemovinSeedinProducts(
                        domainInputDtos,
                        allPersistedDomainSeedLot,
                        isSeedLotAndRelatedUsedByDomainInputId);

        MultiValuedMap<String, SeedLotInputUsage> persistedUsagesToAddNewPhytolsLotId = getPersistedUsagesToAddNewPhytolsLotId(
                domainInputDtos,
                persistedSeedLotByIds);

        Collection<DomainSeedLotInput> domainInputsSeedLots = createOrUpdateDomainInputsSeedLots(
                domain,
                domainInputDtos,
                cropsByOriginalIds,
                persistedSeedLotByIds,
                isSeedLotAndRelatedUsedByDomainInputId,
                usageToRemovesForDomainSeedProduct,
                domainInputReferential);

        createNewUsageFromNewSeedingSpeciesProducts(domainInputsSeedLots, persistedUsagesToAddNewPhytolsLotId);

        deleteSeedDomainInputs(persistedSeedLotByIds, isSeedLotAndRelatedUsedByDomainInputId);
    }

    protected MultiValuedMap<DomainPhytoProductInput, InputUsageServiceImpl.SeedingProductUsageToRemoveForDomainSeedingProduct> getUsagesToRemoveWhenRemovinSeedinProducts(
            Collection<DomainInputDto> domainInputDtos,
            Collection<DomainSeedLotInput> allPersistedDomainSeedLot,
            Map<String, Boolean> isSeedLotAndRelatedUsedByDomainInputId) {

        Set<String> removedPhytoIds = getRemovedPhytoIds(domainInputDtos, allPersistedDomainSeedLot);

        List<DomainSeedSpeciesInput> domainSeedSpeciesInputs = allPersistedDomainSeedLot.stream()
                .map(DomainSeedLotInput::getDomainSeedSpeciesInput)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .toList();

        MultiValuedMap<DomainPhytoProductInput, InputUsageServiceImpl.SeedingProductUsageToRemoveForDomainSeedingProduct> usageToRemovesForDomainSeddProduct = new HashSetValuedHashMap<>();
        for (Map.Entry<String, Boolean> isInputUsed : isSeedLotAndRelatedUsedByDomainInputId.entrySet()) {
            Boolean isUsed = isInputUsed.getValue();
            String inputId = isInputUsed.getKey();
            ifInputIsNotUsedAndRemovedPhyto = !isUsed && removedPhytoIds.contains(inputId);
            if (ifInputIsNotUsedAndRemovedPhyto) {
                // try find the phyto into the seedingSpecies
                for (DomainSeedSpeciesInput domainSeedSpeciesInput : domainSeedSpeciesInputs) {
                    DomainPhytoProductInput domainPhytoProductInput = domainSeedSpeciesInput.getSpeciesPhytoInputsByTopiaId(inputId);
                    if (domainPhytoProductInput != null) {
                        List<InputUsageServiceImpl.SeedingProductUsageToRemoveForDomainSeedingProduct> seedingProductUsageToRemoveForDomainSeedingProduct =
                                inputUsageService.getSeedingProductUsageToRemoveForDomainSeedingProduct(domainSeedSpeciesInput, domainPhytoProductInput);
                        seedingProductUsageToRemoveForDomainSeedingProduct.forEach(r0 ->
                                usageToRemovesForDomainSeddProduct.put(domainPhytoProductInput, r0));
                    }

                }
            }
        }
        return usageToRemovesForDomainSeddProduct;
    }

    protected void recursivelyApplyPersistanceOnSeedLot(Collection<DomainSeedLotInput> seedLotInputsToCreateOrUpdate) {

        seedLotInputsToCreateOrUpdate.stream().map(DomainSeedLotInput::getDomainSeedSpeciesInput)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(DomainSeedSpeciesInput::getSpeciesPhytoInputs)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .forEach(this::createOrUpdateDomainInput_);

        seedLotInputsToCreateOrUpdate.stream().map(DomainSeedLotInput::getDomainSeedSpeciesInput)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .forEach(this::createOrUpdateDomainInput_);

        seedLotInputsToCreateOrUpdate.forEach(this::createOrUpdateDomainInput_);
    }

    protected void createNewUsageFromNewSeedingSpeciesProducts(
            Collection<DomainSeedLotInput> domainSeedLotInputs,
            MultiValuedMap<String, SeedLotInputUsage> persistedUsagesToAddNewPhytolsLotId) {

        Collection<SeedSpeciesInputUsage> updatedSeedSpeciesInputUsage = new HashSet<>();
        for (DomainSeedLotInput seedLotInput : domainSeedLotInputs) {
            Collection<SeedLotInputUsage> seedLotInputUsagesToAddNexPhyto = persistedUsagesToAddNewPhytolsLotId.get(seedLotInput.getTopiaId());
            if (CollectionUtils.isNotEmpty(seedLotInputUsagesToAddNexPhyto)) {

                Collection<DomainSeedSpeciesInput> domainSeedSpeciesInputs = CollectionUtils.emptyIfNull(seedLotInput.getDomainSeedSpeciesInput());

                for (SeedLotInputUsage seedLotInputUsage : seedLotInputUsagesToAddNexPhyto) {

                    Collection<SeedSpeciesInputUsage> seedSpeciesInputUsages = seedLotInputUsage.getSeedingSpecies();
                    Map<DomainSeedSpeciesInput, SeedSpeciesInputUsage> seedingSpecieUsagesByDomainInput = seedSpeciesInputUsages.stream()
                            .collect(Collectors.toMap(SeedSpeciesInputUsage::getDomainSeedSpeciesInput, Function.identity()));

                    seedSpeciesInputUsages.forEach(
                            seedSpeciesInputUsage -> {
                                Collection<SeedProductInputUsage> seedProductInputUsages = seedSpeciesInputUsage.getSeedProductInputUsages();

                                DomainSeedSpeciesInput usageDomainSeedSpeciesInput = seedSpeciesInputUsage.getDomainSeedSpeciesInput();
                                SeedSpeciesInputUsage seedingSpecieUsage = seedingSpecieUsagesByDomainInput.get(usageDomainSeedSpeciesInput);
                                Map<DomainPhytoProductInput, SeedProductInputUsage> previouslyPersistedProductUsageByDomainInputs = CollectionUtils.emptyIfNull(seedProductInputUsages)
                                        .stream().collect(Collectors.toMap(SeedProductInputUsage::getDomainPhytoProductInput, Function.identity()));

                                Optional<DomainSeedSpeciesInput> optionalDomainSeedSpeciesInput = domainSeedSpeciesInputs
                                        .stream()
                                        .filter(dssi -> dssi.getTopiaId().contentEquals(seedingSpecieUsage.getDomainSeedSpeciesInput().getTopiaId())).findAny();

                                if (optionalDomainSeedSpeciesInput.isPresent()) {

                                    DomainSeedSpeciesInput domainSeedSpeciesInput = optionalDomainSeedSpeciesInput.get();
                                    seedingSpecieUsage.setDomainSeedSpeciesInput(domainSeedSpeciesInput);

                                    Collection<DomainPhytoProductInput> speciesPhytoInputs = domainSeedSpeciesInput.getSpeciesPhytoInputs();
                                    new ArrayList<>(speciesPhytoInputs).forEach(
                                            dspi -> {
                                                SeedProductInputUsage seedProductInputUsage = previouslyPersistedProductUsageByDomainInputs.get(dspi);
                                                if (seedProductInputUsage == null) {
                                                    SeedProductInputUsage newSeedProductInputUsage = inputUsageService.createNewSeedProductInputUsage(dspi);
                                                    seedProductInputUsages.add(newSeedProductInputUsage);
                                                } else {
                                                    seedProductInputUsage.setDomainPhytoProductInput(dspi);
                                                }
                                                updatedSeedSpeciesInputUsage.add(seedSpeciesInputUsage);
                                            }
                                    );

                                }
                            }
                    );
                }

            }
        }
        inputUsageService.updateSeedSpeciesInputUsages(updatedSeedSpeciesInputUsage);
    }

    protected static Set<String> getRemovedPhytoIds(Collection<DomainInputDto> domainInputDtos, Collection<DomainSeedLotInput> persistedDomainSeedLot) {
        List<String> phytoDtoIds = domainInputDtos.stream()
                .filter(domainInputDto -> InputType.SEMIS.equals(domainInputDto.getInputType()) ||
                        InputType.PLAN_COMPAGNE.equals(domainInputDto.getInputType()))
                .filter(domainInputDto -> domainInputDto.getTopiaId().isPresent())
                .map(domainInputDto -> ((DomainSeedLotInputDto) domainInputDto).getSpeciesInputs())
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(DomainSeedSpeciesInputDto::getSpeciesPhytoInputDtos)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(DomainPhytoProductInputDto::getTopiaId)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();

        Set<String> phytoIds = persistedDomainSeedLot.stream()
                .map(DomainSeedLotInput::getDomainSeedSpeciesInput)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(DomainSeedSpeciesInput::getSpeciesPhytoInputs)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(DomainPhytoProductInput::getTopiaId)
                .filter(topiaId -> !phytoDtoIds.contains(topiaId))
                .collect(Collectors.toSet());

        return phytoIds;
    }

    protected MultiValuedMap<String, SeedLotInputUsage> getPersistedUsagesToAddNewPhytolsLotId(
            Collection<DomainInputDto> domainInputDtos,
            Map<String, DomainSeedLotInput> domainSeedLotByIds) {

        List<DomainInputDto> lotsWithNewDomainSeedInputProductsDtos = getLotsWithNewDomainSeedInputProductsDtos(domainInputDtos);

        MultiValuedMap<String, SeedLotInputUsage> persistedLotsWithNewDomainSeedInputProductsDtoByIds = new HashSetValuedHashMap<>();
        if (!lotsWithNewDomainSeedInputProductsDtos.isEmpty()) {
            List<DomainSeedLotInput> persistedLotsWithNewDomainSeedInputProductsDtos = new ArrayList<>();
            lotsWithNewDomainSeedInputProductsDtos.stream()
                    .filter(lotDto -> lotDto.getTopiaId().isPresent())
                    .forEach(
                            lotsWithNewDomainSeedInputProductsDto -> {
                                String lotId = lotsWithNewDomainSeedInputProductsDto.getTopiaId().get();
                                DomainSeedLotInput persistedLot = domainSeedLotByIds.get(lotId);
                                persistedLotsWithNewDomainSeedInputProductsDtos.add(persistedLot);
                            }
                    );
            Collection<SeedLotInputUsage> persistedLotUsages = inputUsageService.getLotInputUsages(persistedLotsWithNewDomainSeedInputProductsDtos);
            persistedLotUsages.forEach(plu -> persistedLotsWithNewDomainSeedInputProductsDtoByIds.put(plu.getDomainSeedLotInput().getTopiaId(), plu));
        }

        return persistedLotsWithNewDomainSeedInputProductsDtoByIds;
    }

    protected void deleteDomainInputs(Map<String, AbstractDomainInputStockUnit> rootLevePersistedDomainInputByIds, Map<String, Boolean> allInputUsageByIds) {
        List<AbstractDomainInputStockUnit> inputsToRemove = rootLevePersistedDomainInputByIds
                .values()
                .stream()
                .filter(
                        domainInputStockToRemove -> !allInputUsageByIds.get(domainInputStockToRemove.getTopiaId()))
                .collect(Collectors.toList());
        domainInputDao.deleteAll(inputsToRemove);
    }

    protected void deleteSeedDomainInputs(
            Map<String, DomainSeedLotInput> persistedSeedLotByIds,
            Map<String, Boolean> allInputUsageByIds) {

        List<DomainSeedLotInput> inputsToRemove = persistedSeedLotByIds
                .values()
                .stream()
                .filter(
                        domainInputStockToRemove -> !allInputUsageByIds.get(domainInputStockToRemove.getTopiaId()))
                .collect(Collectors.toList());
        domainSeedLotInputDao.deleteAll(inputsToRemove);
    }

    protected Collection<AbstractDomainInputStockUnit> createOrUpdateDomainInputsExceptLots(
            Domain domain,
            Collection<DomainInputDto> domainInputDtos,
            Map<String, AbstractDomainInputStockUnit> rootLevePersistedDomainInputByIds,
            Map<String, Boolean> allInputUsageByIds,
            DomainReferentialInputs domainInputReferential) {
        Collection<AbstractDomainInputStockUnit> inputsToCreateOrUpdate = new ArrayList<>();

        List<DomainInputDto> domainInputDtoWithoutSeedingInputs = domainInputDtos.stream()
                .filter(domainInputDto -> !InputType.SEMIS.equals(domainInputDto.getInputType()) &&
                        !InputType.PLAN_COMPAGNE.equals(domainInputDto.getInputType()) &&
                        !InputType.TRAITEMENT_SEMENCE.equals(domainInputDto.getInputType()))
                .toList();

        domainInputDtoWithoutSeedingInputs.forEach(domainInputDto -> {

            AbstractDomainInputStockUnit inputEntity = getOrCreateInstance(rootLevePersistedDomainInputByIds, domainInputDto, domainInputDto.getInputType());

            boolean isInputUsed = inputEntity.isPersisted() && allInputUsageByIds.get(inputEntity.getTopiaId());
            DomainInputBinder.bindDtoToEntity(domain, domainInputDto, inputEntity, referentialService, domainInputReferential, isInputUsed);
            createUpdateOrDeletePrice(domain, domainInputDto, inputEntity);

            inputsToCreateOrUpdate.add(inputEntity);
        });
        return inputsToCreateOrUpdate;
    }

    protected Collection<DomainSeedLotInput> createOrUpdateDomainInputsSeedLots(
            Domain domain,
            Collection<DomainInputDto> domainInputDtos,
            Map<String, CropPersistResult> cropsByOriginalIds,
            Map<String, DomainSeedLotInput> persistedSeedLotByIds,
            Map<String, Boolean> isSeedLotAndRelatedUsedByDomainInputId,
            MultiValuedMap<DomainPhytoProductInput, InputUsageServiceImpl.SeedingProductUsageToRemoveForDomainSeedingProduct> usageToRemovesForDomainSeedProduct,
            DomainReferentialInputs domainInputReferential) {

        Collection<DomainSeedLotInput> inputsToCreateOrUpdate = new ArrayList<>();

        List<DomainInputDto> domainSeedLotInputDtos = domainInputDtos.stream()
                .filter(domainInputDto -> InputType.SEMIS.equals(domainInputDto.getInputType()) ||
                        InputType.PLAN_COMPAGNE.equals(domainInputDto.getInputType()))
                .toList();

        if (CollectionUtils.isNotEmpty(domainSeedLotInputDtos)) {
            Map<SeedType, String> seedTypeTranslations = i18nService.getEnumTranslationMap(SeedType.class);

            domainSeedLotInputDtos.forEach(domainSeedLotDto -> {

                DomainSeedLotInput inputEntity = (DomainSeedLotInput) getOrCreateInstance(persistedSeedLotByIds, domainSeedLotDto, domainSeedLotDto.getInputType());

                boolean isInputUsed = inputEntity.isPersisted() && isSeedLotAndRelatedUsedByDomainInputId.get(inputEntity.getTopiaId());

                bindSeedLotInputDtoToEntity(
                        domain,
                        isSeedLotAndRelatedUsedByDomainInputId,
                        usageToRemovesForDomainSeedProduct,
                        cropsByOriginalIds,
                        domainSeedLotDto,
                        isInputUsed,
                        inputEntity,
                        domainInputReferential,
                        seedTypeTranslations);

                inputsToCreateOrUpdate.add(inputEntity);

                recursivelyApplyPersistanceOnSeedLot(Lists.newArrayList(inputEntity));
            });
        }
        return inputsToCreateOrUpdate;
    }

    protected static List<DomainInputDto> getLotsWithNewDomainSeedInputProductsDtos(Collection<DomainInputDto> domainInputDtos) {
        List<DomainInputDto> lotsWithNewDomainSeedInputProductsDtos = domainInputDtos.stream()
                .filter(domainInputDto -> InputType.SEMIS.equals(domainInputDto.getInputType()) ||
                        InputType.PLAN_COMPAGNE.equals(domainInputDto.getInputType()))
                .filter(domainInputDto -> domainInputDto.getTopiaId().isPresent())
                .filter(domainInputDto -> {
                    Collection<DomainSeedSpeciesInputDto> speciesInputs = CollectionUtils.emptyIfNull(((DomainSeedLotInputDto) domainInputDto).getSpeciesInputs());
                    return speciesInputs.stream()
                            .map(DomainSeedSpeciesInputDto::getSpeciesPhytoInputDtos)
                            .filter(Objects::nonNull)
                            .flatMap(Collection::stream)
                            .anyMatch(sp -> sp.getTopiaId().isEmpty());
                })
                .toList();
        return lotsWithNewDomainSeedInputProductsDtos;
    }

    private static Collection<AbstractDomainInputStockUnit> getDomainInputStockUnitsExceptSubSeedLotEntities(Collection<AbstractDomainInputStockUnit> allPersistedDomainInputs) {
        Collection<AbstractDomainInputStockUnit> persistedDomainInputs = allPersistedDomainInputs.stream()
                .filter(di -> !(di instanceof DomainSeedSpeciesInput))
                .filter(di -> !(di instanceof DomainPhytoProductInput domainPhytoProductInput &&
                        InputType.TRAITEMENT_SEMENCE.equals(domainPhytoProductInput.getInputType())))
                .toList();
        return persistedDomainInputs;
    }

    protected static Collection<AbstractDomainInputStockUnit> getDomainInputStockUnitsExceptSeedLotAndRelatedEntities(Collection<AbstractDomainInputStockUnit> allPersistedDomainInputs) {
        Collection<AbstractDomainInputStockUnit> persistedDomainInputs = allPersistedDomainInputs.stream()
                .filter(di -> !(di instanceof DomainSeedLotInput))
                .filter(di -> !(di instanceof DomainSeedSpeciesInput))
                .filter(di -> !(di instanceof DomainPhytoProductInput domainPhytoProductInput &&
                        InputType.TRAITEMENT_SEMENCE.equals(domainPhytoProductInput.getInputType())))
                .toList();
        return persistedDomainInputs;
    }

    @Override
    public Collection<DomainInputDto> loadDomainInputStockDtos(String domainId) {

        Collection<AbstractDomainInputStockUnit> domainInputs = getDomainInputStockUnitsExceptSubSeedLotEntities(
                domainInputDao.forDomainEquals(domainDao.forTopiaIdEquals(domainId).findUnique()).findAll());

        List<CroppingPlanEntryDto> cropDtos = domainService.getCroppingPlanDtos(domainId);
        Collection<DomainInputDto> domainInputDtos = new ArrayList<>();

        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(getSecurityContext().getLanguage());
        i18nService.fillTranslationMap(domainInputs, translationMap);

        domainInputs.forEach(domainInput -> domainInputDtos.add(DomainInputBinder.bindToDto(domainInput, cropDtos, translationMap)));
        return ImmutableList.copyOf(domainInputDtos.stream().filter(Objects::nonNull).toList());
    }

    @Override
    public List<DomainInputDto> loadDomainInputStockDtos(String domainId, InputType inputType) {

        Domain d = domainDao.forTopiaIdEquals(domainId).findUnique();
        Collection<AbstractDomainInputStockUnit> domainInputs = new ArrayList<>();

        if (InputType.SEMIS == inputType || InputType.PLAN_COMPAGNE == inputType) {

            List<DomainSeedLotInput> domainSeedLotInputs = domainSeedLotInputDao.forProperties(
                            AbstractDomainInputStockUnit.PROPERTY_DOMAIN, d)
                    .findAll()
                    .stream()
                    .toList();
            domainInputs.addAll(domainSeedLotInputs);

        } else {

            domainInputs = domainInputDao.forProperties(
                            AbstractDomainInputStockUnit.PROPERTY_DOMAIN, d,
                            AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, inputType)
                    .findAll()
                    .stream()
                    .filter(di -> !(di instanceof DomainPhytoProductInput domainPhytoProductInput &&
                            InputType.TRAITEMENT_SEMENCE.equals(domainPhytoProductInput.getInputType())))
                    .toList();
        }

        List<CroppingPlanEntryDto> cropDtos = domainService.getCroppingPlanDtos(domainId);
        Collection<DomainInputDto> domainInputDtos = new ArrayList<>();

        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(getSecurityContext().getLanguage());
        i18nService.fillTranslationMap(domainInputs, translationMap);

        domainInputs.stream().filter(i -> !(i instanceof DomainOtherInput) || !((DomainOtherInput) i).getRefInput()
                .getTopiaId()
                .contentEquals(DEPRECATED_REF_OTHER_INPUT_ID))
                .forEach(domainInput -> domainInputDtos.add(DomainInputBinder.bindToDto(domainInput, cropDtos, translationMap)));

        return ImmutableList.copyOf(domainInputDtos.stream().filter(Objects::nonNull).toList());
    }

    @Override
    public void duplicateDomainInputStocks(
            Domain fromDomain,
            Domain toDomain,
            Collection<CroppingPlanEntry> extendCroppingPlans) {

        Map<String, CroppingPlanEntry> extendCroppingPlanByCodes = CollectionUtils.emptyIfNull(extendCroppingPlans).stream().collect(
                Collectors.toMap(CroppingPlanEntry::getCode, Function.identity()));

        Map<String, CroppingPlanSpecies> extendCroppingPlanSpeciesByCodes = CollectionUtils.emptyIfNull(extendCroppingPlans).stream()
                .map(CroppingPlanEntry::getCroppingPlanSpecies)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .collect(Collectors.toMap(CroppingPlanSpecies::getCode, Function.identity()));

        Map<InputType, List<AbstractDomainInputStockUnit>> sourceDomainInputStocks = loadDomainInputStock(fromDomain);

        authorizationService.checkCreateOrUpdateDomain(toDomain.getTopiaId());

        boolean isSameDomainCode = toDomain.getCode().contentEquals(fromDomain.getCode());

        sourceDomainInputStocks = filterOtherDeprecatedInputs(sourceDomainInputStocks);

        Collection<AbstractDomainInputStockUnit> domainInputStockUnitsToPersist = new ArrayList<>();
        for (Map.Entry<InputType, List<AbstractDomainInputStockUnit>> typeToInputs : sourceDomainInputStocks.entrySet()) {

            final List<AbstractDomainInputStockUnit> sourceInputStockUnits = typeToInputs.getValue();

            final InputType inputType = typeToInputs.getKey();

            if (InputType.SEMIS.equals(inputType) || InputType.PLAN_COMPAGNE.equals(inputType) || InputType.TRAITEMENT_SEMENCE.equals(inputType)) {

                if (InputType.TRAITEMENT_SEMENCE.equals(inputType)) {
                    continue;// done into SEMIS ou PLAN_COMPAGNE
                }

                Collection<DomainSeedLotInput> duplicatedSeedingInputs = bindDomainSeedLot(
                        fromDomain,
                        toDomain,
                        extendCroppingPlanByCodes,
                        extendCroppingPlanSpeciesByCodes,
                        sourceInputStockUnits
                );
                if (CollectionUtils.isNotEmpty(duplicatedSeedingInputs)) {
                    domainInputStockUnitsToPersist.addAll(duplicatedSeedingInputs);
                }

            } else {

                Collection<AbstractDomainInputStockUnit> extendInputStockUnitDomainInputS = copyDomainInputsExceptSeedLots(
                        toDomain,
                        sourceInputStockUnits,
                        isSameDomainCode);

                domainInputStockUnitsToPersist.addAll(extendInputStockUnitDomainInputS);

            }
        }
        
        for (AbstractDomainInputStockUnit domainInputStockUnit : domainInputStockUnitsToPersist) {
            createOrUpdateDomainInput_(domainInputStockUnit);
        }
    }

    @Override
    public DomainSeedLotInput cloneDomainSeedLotInput(
            Domain fromDomain,
            Domain toDomain,
            final DomainSeedLotInput domainSeedLotInput,
            Map<String, CroppingPlanEntry> extendCroppingPlanByCodes,
            Map<String, CroppingPlanSpecies> extendCroppingPlanSpeciesByCodes) {

        DomainSeedLotInput result = null;
        List<AbstractDomainInputStockUnit> sourceInputStockUnits = new ArrayList<>();
        sourceInputStockUnits.add(domainSeedLotInput);

        List<DomainSeedLotInput> duplicatedSeedingInputs = bindDomainSeedLot(
                fromDomain,
                toDomain,
                extendCroppingPlanByCodes,
                extendCroppingPlanSpeciesByCodes,
                sourceInputStockUnits
        );
        if (CollectionUtils.isNotEmpty(duplicatedSeedingInputs)) {
            // there is only one
            result = duplicatedSeedingInputs.getFirst();
            createOrUpdateDomainInput_(result);
        }
        return result;
    }

    protected boolean isDuplicatedInputCode(
            Domain toDomain,
            AbstractDomainInputStockUnit domainInputStockUnit) {

        return domainSeedLotInputDao.forProperties(
                AbstractDomainInputStockUnit.PROPERTY_CODE, domainInputStockUnit.getCode(),
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, toDomain).exists();
    }

    @Override
    public DomainSeedLotInput cloneAndPersistDomainSeedLotInput(Domain fromDomain, Domain toDomain, DomainSeedLotInput domainSeedLotInput, Map<String, CroppingPlanEntry> extendCroppingPlanByCodes, Map<String, CroppingPlanSpecies> extendCroppingPlanSpeciesByCodes) {

        DomainSeedLotInput entity = cloneDomainSeedLotInput(fromDomain, toDomain, domainSeedLotInput, extendCroppingPlanByCodes, extendCroppingPlanSpeciesByCodes);
        domainSeedLotInputDao.create(entity);

        return entity;
    }

    @Override
    public AbstractDomainInputStockUnit cloneDomainInputExceptSeedLot(Domain toDomain,
                                                                      InputType inputType,
                                                                      AbstractDomainInputStockUnit sourceInputStockUnit,
                                                                      boolean isSameDomainCode) {
        if (isSameDomainCode && isDuplicatedInputCode(toDomain, sourceInputStockUnit)) {
            isSameDomainCode = false; // TO force create new one
        }
        AbstractDomainInputStockUnit domainInputStockUnit = bindDomainInputExceptSeedLot(toDomain, inputType, sourceInputStockUnit, isSameDomainCode);
        domainInputStockUnit = createOrUpdateDomainInput_(domainInputStockUnit);
        return domainInputStockUnit;
    }

    protected AbstractDomainInputStockUnit bindDomainInputExceptSeedLot(
            Domain toDomain,
            InputType inputType,
            AbstractDomainInputStockUnit sourceInputStockUnit,
            boolean isSameDomainCode) {

        AbstractDomainInputStockUnit extendInputStockUnit = getAbstractDomainInputStockUnitNewInstance(sourceInputStockUnit.getInputType());

        boolean preserveDomainCode = isSameDomainCode && checkInputCodeNotUsed(toDomain, sourceInputStockUnit);

        shallowBindDomainInputExceptSeedLot(
                inputType,
                sourceInputStockUnit,
                extendInputStockUnit,
                preserveDomainCode);

        extendInputStockUnit.setDomain(toDomain);

        final InputPrice sourceInputPrice = sourceInputStockUnit.getInputPrice();
        if (sourceInputPrice != null) {
            InputPrice inputPrice = inputPriceService.bindToNewPrice(
                    toDomain,
                    sourceInputStockUnit.getInputPrice(),
                    sourceInputPrice.getObjectId(),
                    Optional.empty(),
                    Optional.empty());
            inputPrice = inputPriceService.persistInputPrice(inputPrice);
            extendInputStockUnit.setInputPrice(inputPrice);
        }

        extendInputStockUnit.setDomain(toDomain);

        return extendInputStockUnit;
    }

    protected static void shallowBindDomainInputExceptSeedLot(
            InputType inputType,
            AbstractDomainInputStockUnit sourceInputStockUnit,
            AbstractDomainInputStockUnit extendInputStockUnit,
            boolean preserveInputCode) {

        String[] commonExcludeProperties = new String[]{
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                AbstractDomainInputStockUnit.PROPERTY_CODE,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE};

        switch (inputType) {
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX ->
                    domainMineralProductInputBinder.copyExcluding((DomainMineralProductInput) sourceInputStockUnit, (DomainMineralProductInput) extendInputStockUnit, commonExcludeProperties);
            case EPANDAGES_ORGANIQUES ->
                    domainOrganicProductInputBinder.copyExcluding((DomainOrganicProductInput) sourceInputStockUnit, (DomainOrganicProductInput) extendInputStockUnit, commonExcludeProperties);
            case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, LUTTE_BIOLOGIQUE ->
                    domainPhytoProductInputBinder.copyExcluding((DomainPhytoProductInput) sourceInputStockUnit, (DomainPhytoProductInput) extendInputStockUnit, commonExcludeProperties);
            case CARBURANT ->
                    domainFuelInputBinder.copyExcluding((DomainFuelInput) sourceInputStockUnit, (DomainFuelInput) extendInputStockUnit, commonExcludeProperties);
            case MAIN_OEUVRE_MANUELLE ->
                    domainManualWorkforceInputBinder.copyExcluding((DomainManualWorkforceInput) sourceInputStockUnit, (DomainManualWorkforceInput) extendInputStockUnit, commonExcludeProperties);
            case MAIN_OEUVRE_TRACTORISTE ->
                    domainMechanizedWorkforceInputBinder.copyExcluding((DomainMechanizedWorkforceInput) sourceInputStockUnit, (DomainMechanizedWorkforceInput) extendInputStockUnit, commonExcludeProperties);
            case IRRIGATION ->
                    domainIrrigationInputBinder.copyExcluding((DomainIrrigationInput) sourceInputStockUnit, (DomainIrrigationInput) extendInputStockUnit, commonExcludeProperties);
            case AUTRE ->
                    domainOtherInputBinder.copyExcluding((DomainOtherInput) sourceInputStockUnit, (DomainOtherInput) extendInputStockUnit, commonExcludeProperties);
            case SUBSTRAT ->
                    domainSubstrateInputBinder.copyExcluding((DomainSubstrateInput) sourceInputStockUnit, (DomainSubstrateInput) extendInputStockUnit, commonExcludeProperties);
            case POT ->
                    inputDomainPotInputBinder.copyExcluding((DomainPotInput) sourceInputStockUnit, (DomainPotInput) extendInputStockUnit, commonExcludeProperties);
            case SEMIS, PLAN_COMPAGNE, TRAITEMENT_SEMENCE -> {
                if (LOGGER.isTraceEnabled())
                    LOGGER.trace("shallowBindDomainInputExceptSeedLot ne gère pas le cas des semis");
            }
            default -> throw new IllegalStateException("Unexpected value: " + inputType);
        }
        String inputCode = preserveInputCode ? sourceInputStockUnit.getCode() : UUID.randomUUID().toString();
        extendInputStockUnit.setCode(inputCode);
    }

    @Override
    public List<String> copyInputStocks(String fromDomainId, Collection<String> inputIds, Collection<String> toDomainIds, boolean forceCopy) {
        // List of "domain name - campaign"
        List<String> result = new ArrayList<>(toDomainIds.size());

        toDomainIds.forEach(domainId -> authorizationService.checkCreateOrUpdateDomain(domainId));

        Domain fromDomain = domainDao.forTopiaIdEquals(fromDomainId).findUnique();

        Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStocksByTypesToCopy = loadDomainInputStockWithIds(inputIds);

        domainInputStocksByTypesToCopy = filterOtherDeprecatedInputs(domainInputStocksByTypesToCopy);

        List<Domain> toDomains = domainDao.forTopiaIdIn(toDomainIds).findAll();
        for (Domain toDomain : toDomains) {
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info(String.format("Copy inputs from %s (%s) to %s (%s)", fromDomain.getName(), fromDomain.getTopiaId(), toDomain.getName(), toDomain.getTopiaId()));
            }

            if (!forceCopy) {
                domainInputStocksByTypesToCopy = filterOnNotExistingInputs(toDomain, domainInputStocksByTypesToCopy);
            }

            domainInputStocksByTypesToCopy = filterOnNotExistingWorkforceAndOperationInputs(toDomain, domainInputStocksByTypesToCopy);

            boolean isSameDomainCode = !forceCopy && toDomain.getCode().contentEquals(fromDomain.getCode());

            Collection<AbstractDomainInputStockUnit> domainInputStockUnitsToPersist = new ArrayList<>();

            for (Map.Entry<InputType, List<AbstractDomainInputStockUnit>> typeToInputs : domainInputStocksByTypesToCopy.entrySet()) {

                final InputType inputType = typeToInputs.getKey();
                final List<AbstractDomainInputStockUnit> domainInputStocksToCopy = typeToInputs.getValue();

                if (InputType.SEMIS.equals(inputType) ||
                        InputType.PLAN_COMPAGNE.equals(inputType) ||
                        InputType.TRAITEMENT_SEMENCE.equals(inputType)) {

                    if (InputType.TRAITEMENT_SEMENCE.equals(inputType)) {
                        continue;// done into SEMIS ou PLAN_COMPAGNE
                    }

                    domainInputStockUnitsToPersist.addAll(
                            copySeedLots(
                                    fromDomain,
                                    toDomain,
                                    domainInputStocksToCopy,
                                    isSameDomainCode));

                } else {
                    domainInputStockUnitsToPersist.addAll(
                            copyDomainInputsExceptSeedLots(
                                    toDomain,
                                    domainInputStocksToCopy,
                                    isSameDomainCode
                            ));

                }

            }

            for (AbstractDomainInputStockUnit domainInputStockUnit : domainInputStockUnitsToPersist) {
                createOrUpdateDomainInput_(domainInputStockUnit);
            }

            result.add(toDomain.getName() + " (" + toDomain.getCampaign() + ")");
        }
        return result;
    }

    protected Map<InputType, List<AbstractDomainInputStockUnit>> filterOtherDeprecatedInputs(Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStocksByTypesToCopy) {
        Map<InputType, List<AbstractDomainInputStockUnit>> result;
        List<AbstractDomainInputStockUnit> otherDomainInputToCopies = domainInputStocksByTypesToCopy.get(InputType.AUTRE);
        if (CollectionUtils.isNotEmpty(otherDomainInputToCopies)) {
            result = new HashMap<>(domainInputStocksByTypesToCopy);
            result.put(InputType.AUTRE, otherDomainInputToCopies
                    .stream()
                    .filter(oi -> !((DomainOtherInput)oi).getRefInput()
                            .getTopiaId()
                            .contentEquals(DEPRECATED_REF_OTHER_INPUT_ID)).toList());
        } else {
            result = domainInputStocksByTypesToCopy;
        }
        return result;
    }

    protected Collection<AbstractDomainInputStockUnit> copyDomainInputsExceptSeedLots(
            Domain toDomain,
            List<AbstractDomainInputStockUnit> domainInputStocksToCopy,
            boolean isSameDomainCode) {

        Collection<AbstractDomainInputStockUnit> domainInputStockUnitsToPersist = new ArrayList<>();
        for (AbstractDomainInputStockUnit sourceInputStockUnit : domainInputStocksToCopy) {

            AbstractDomainInputStockUnit extendInputStockUnit = null;

            if (InputType.CARBURANT.equals(sourceInputStockUnit.getInputType())) {
                extendInputStockUnit = domainFuelDao.forDomainEquals(toDomain).findUniqueOrNull();
            } else if (InputType.IRRIGATION.equals(sourceInputStockUnit.getInputType())) {
                extendInputStockUnit = domainIrrigationDao.forDomainEquals(toDomain).findUniqueOrNull();
            } else if (InputType.MAIN_OEUVRE_MANUELLE.equals(sourceInputStockUnit.getInputType())) {
                extendInputStockUnit = domainManualWorkforceDao.forDomainEquals(toDomain).findUniqueOrNull();
            } else if (InputType.MAIN_OEUVRE_TRACTORISTE.equals(sourceInputStockUnit.getInputType())) {
                extendInputStockUnit = domainMechanizedWorkforceDao.forDomainEquals(toDomain).findUniqueOrNull();
            }

            if (extendInputStockUnit == null) {
                extendInputStockUnit = getAbstractDomainInputStockUnitNewInstance(sourceInputStockUnit.getInputType());
            }

            boolean preserveDomainCode = isSameDomainCode && checkInputCodeNotUsed(toDomain, sourceInputStockUnit);

            shallowBindDomainInputExceptSeedLot(
                    sourceInputStockUnit.getInputType(),
                    sourceInputStockUnit,
                    extendInputStockUnit,
                    preserveDomainCode);

            final String inputCode = isSameDomainCode ? sourceInputStockUnit.getCode() : UUID.randomUUID().toString();
            extendInputStockUnit.setCode(inputCode);
            extendInputStockUnit.setDomain(toDomain);

            final InputPrice sourceInputPrice = sourceInputStockUnit.getInputPrice();
            if (sourceInputPrice != null) {
                InputPrice persistedPrice = extendInputStockUnit.getInputPrice();
                InputPrice inputPrice;
                if (persistedPrice == null) {
                    inputPrice = inputPriceService.bindToNewPrice(
                            toDomain,
                            sourceInputStockUnit.getInputPrice(),
                            sourceInputPrice.getObjectId(),
                            Optional.empty(),
                            Optional.empty());
                } else {
                    // cas de Main d’œuvre et fonctionnement
                    inputPrice = inputPriceService.bindDtoToPrice(
                            toDomain,
                            DomainInputPriceBinder.bindPriceToDto(sourceInputPrice),
                            persistedPrice,
                            sourceInputPrice.getObjectId(),
                            Optional.empty(),
                            Optional.empty());
                }
                inputPrice = inputPriceService.persistInputPrice(inputPrice);
                extendInputStockUnit.setInputPrice(inputPrice);

            }
            extendInputStockUnit.setDomain(toDomain);

            domainInputStockUnitsToPersist.add(extendInputStockUnit);

        }
        return domainInputStockUnitsToPersist;
    }

    @Override
    public AbstractDomainInputStockUnit copyInputStock(String fromDomainId, String inputId, String toDomainId) {

        authorizationService.checkCreateOrUpdateDomain(toDomainId);

        AbstractDomainInputStockUnit result = null;

        Domain fromDomain = domainDao.forTopiaIdEquals(fromDomainId).findUnique();
        Domain toDomain = domainDao.forTopiaIdEquals(toDomainId).findUnique();

        boolean isSameDomainCode = fromDomain.getCode().contentEquals(toDomain.getCode());

        AbstractDomainInputStockUnit input = domainInputDao.forTopiaIdEquals(inputId).findUniqueOrNull();

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info(String.format("Copy inputs from %s (%s) to %s (%s)", fromDomain.getName(), fromDomain.getTopiaId(), toDomain.getName(), toDomain.getTopiaId()));
        }

        final InputType inputType = input.getInputType();

        if (InputType.SEMIS.equals(inputType) ||
                InputType.PLAN_COMPAGNE.equals(inputType)) {

            Collection<CroppingPlanEntry> targetedCroppingPlanEntries = CollectionUtils.emptyIfNull(domainService.getCroppingPlanEntriesForDomain(toDomain));

            if (CollectionUtils.isEmpty(targetedCroppingPlanEntries)) {
                if (LOGGER.isInfoEnabled()) {
                    LOGGER.info(String.format("Copy domainSeedLot from domain %s (%s) is not possible due to missing crop on domain %s (%s)",
                            fromDomain.getName(), fromDomain.getTopiaId(), toDomain.getName(), toDomain.getTopiaId()));
                }
                return null;
            }
            Map<CroppingPlanEntryComparable, CroppingPlanEntry> comparableCropToCrops = new HashMap<>();
            targetedCroppingPlanEntries.forEach(tce -> comparableCropToCrops.put(new CroppingPlanEntryComparable(tce), tce));

            result = copySeedLot(
                    fromDomain,
                    toDomain,
                    isSameDomainCode,
                    (DomainSeedLotInput) input,
                    comparableCropToCrops);


        } else if (InputType.TRAITEMENT_SEMENCE.equals(inputType)) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("TRAITEMENT_SEMENCE is done within SEMIS or PLAN_COMPAGNE");
            }
        } else {

            Collection<AbstractDomainInputStockUnit> extendInputStockUnitDomainInputS = copyDomainInputsExceptSeedLots(
                    toDomain,
                    List.of(input),
                    isSameDomainCode);
            result = createOrUpdateDomainInput_(extendInputStockUnitDomainInputS.iterator().next());

        }

        return result;
    }

    protected Collection<AbstractDomainInputStockUnit> copySeedLots(
            Domain fromDomain,
            Domain toDomain,
            List<AbstractDomainInputStockUnit> domainInputStocksToCopy,
            boolean preserveInputCode) {

        Collection<AbstractDomainInputStockUnit> domainInputStockUnitsToPersist = new ArrayList<>();

        Collection<CroppingPlanEntry> targetedCroppingPlanEntries = CollectionUtils.emptyIfNull(domainService.getCroppingPlanEntriesForDomain(toDomain));

        if (CollectionUtils.isEmpty(targetedCroppingPlanEntries)) {
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info(String.format("Copy domainSeedLot from domain %s (%s) is not possible due to missing crop on domain %s (%s)",
                        fromDomain.getName(), fromDomain.getTopiaId(), toDomain.getName(), toDomain.getTopiaId()));
            }
            return domainInputStockUnitsToPersist;
        }
        Map<CroppingPlanEntryComparable, CroppingPlanEntry> comparableCropToCrops = new HashMap<>();
        targetedCroppingPlanEntries.forEach(tce -> comparableCropToCrops.put(new CroppingPlanEntryComparable(tce), tce));

        final Set<AbstractDomainInputStockUnit> domainSeedLots = domainInputStocksToCopy.stream().filter(distc -> distc instanceof DomainSeedLotInputImpl).collect(Collectors.toSet());
        for (AbstractDomainInputStockUnit domainInputStockUnit : domainSeedLots) {
            DomainSeedLotInput domainSeedLotInput = copySeedLot(
                    fromDomain,
                    toDomain,
                    preserveInputCode,
                    (DomainSeedLotInput) domainInputStockUnit,
                    comparableCropToCrops
            );
            if (domainSeedLotInput != null) {
                domainInputStockUnitsToPersist.add(domainSeedLotInput);
            }
        }
        return domainInputStockUnitsToPersist;
    }

    protected DomainSeedLotInput copySeedLot(
            Domain fromDomain,
            Domain toDomain,
            boolean isSameDomaineCode,
            DomainSeedLotInput domainInputStockUnit,
            Map<CroppingPlanEntryComparable, CroppingPlanEntry> comparableCropToCrops) {

        DomainSeedLotInput sourceDomainSeedLotInput = domainInputStockUnit;
        final CroppingPlanEntry cropSeed = sourceDomainSeedLotInput.getCropSeed();
        final CroppingPlanEntryComparable croppingPlanEntryComparable = new CroppingPlanEntryComparable(cropSeed);
        final CroppingPlanEntry croppingPlanEntry = comparableCropToCrops.get(croppingPlanEntryComparable);
        if (croppingPlanEntry != null) {
            Map<SpeciesComparable, CroppingPlanSpecies> speciesComparableToCroppingPlanSpecies = new HashMap<>();
            CollectionUtils.emptyIfNull(croppingPlanEntry.getCroppingPlanSpecies()).forEach(
                    cps -> speciesComparableToCroppingPlanSpecies.put(new SpeciesComparable(cps), cps)
            );

            boolean preserveInputCode = isSameDomaineCode && checkInputCodeNotUsed(toDomain, sourceDomainSeedLotInput);

            DomainSeedLotInput copiedSeedLotInput = bindDomainSeedLotInput(
                    toDomain,
                    sourceDomainSeedLotInput,
                    preserveInputCode,
                    croppingPlanEntry);

            final InputPrice newPrice = inputPriceService.bindToNewPrice(
                    toDomain,
                    sourceDomainSeedLotInput.getSeedPrice(),
                    InputPriceService.GET_CROP_SEED_OBJECT_ID.apply(croppingPlanEntry, copiedSeedLotInput.isOrganic()),
                    Optional.empty(),
                    Optional.empty());
            SeedPrice extendedSeedPrice = (SeedPrice) inputPriceService.persistInputPrice(newPrice);
            copiedSeedLotInput.setSeedPrice(extendedSeedPrice);

            // species from lot
            final Collection<DomainSeedSpeciesInput> sourceDomainSeedSpeciesInput = sourceDomainSeedLotInput.getDomainSeedSpeciesInput();
            if (CollectionUtils.isNotEmpty(sourceDomainSeedSpeciesInput)) {

                for (DomainSeedSpeciesInput sourceSeedSpeciesInput : sourceDomainSeedSpeciesInput) {

                    // add species to species input
                    final CroppingPlanSpecies sourceSpeciesSeed = sourceSeedSpeciesInput.getSpeciesSeed();
                    final CroppingPlanSpecies speciesSeed = speciesComparableToCroppingPlanSpecies.get(new SpeciesComparable(sourceSpeciesSeed));
                    if (speciesSeed == null) {
                        Locale locale = getSecurityContext().getLocale();
                        String varietyName = sourceSpeciesSeed.getVariety() != null ? " (" + sourceSpeciesSeed.getVariety().getLabel() + ")" : "";
                        LOGGER.warn(String.format(l(locale, "fr.inra.agrosyst.services.domain.inputStock.DomainInputStockUnitServiceImpl.duplicateMissingCropError"),
                                sourceSpeciesSeed.getSpecies().getLibelle_espece_botanique_Translated() + varietyName,
                                toDomain.getName() + " (" + toDomain.getCampaign() + ")",
                                fromDomain.getName() + " (" + fromDomain.getCampaign() + ")"));
                        continue;
                    }

                    // copy species parameters
                    DomainSeedSpeciesInput copiedDomainSeedSpeciesInput = bindDomainSeedSpeciesInput(
                            toDomain,
                            sourceSeedSpeciesInput,
                            preserveInputCode,
                            speciesSeed,
                            copiedSeedLotInput.getUsageUnit());

                    copiedSeedLotInput.addDomainSeedSpeciesInput(copiedDomainSeedSpeciesInput);

                    // species seed price
                    final SeedPrice speciesSeedPrice = sourceSeedSpeciesInput.getSeedPrice();
                    final String speciesPriceOpjectId = InputPriceService.GET_SPECIES_SEED_OBJECT_ID.apply(speciesSeed.getSpecies(), speciesSeed.getVariety());
                    if (sourceSeedSpeciesInput.getSeedPrice() != null &&
                            sourceSeedSpeciesInput.getSeedPrice().getPriceUnit() != PriceUnit.EURO_HA &&
                            sourceSeedSpeciesInput.getSeedPrice().getPriceUnit() != PriceUnit.EURO_M3) {
                        final InputPrice newSpeciesSeedPrice = inputPriceService.bindToNewPrice(
                                toDomain,
                                speciesSeedPrice,
                                speciesPriceOpjectId,
                                Optional.empty(),
                                Optional.empty());
                        final InputPrice copiedSeedPrice = inputPriceService.persistInputPrice(newSpeciesSeedPrice);
                        copiedDomainSeedSpeciesInput.setSeedPrice((SeedPrice) copiedSeedPrice);
                    }

                    // species phyto product
                    final Collection<DomainPhytoProductInput> sourceDomainPhytoProductInputs = CollectionUtils.emptyIfNull(sourceSeedSpeciesInput.getSpeciesPhytoInputs());
                    for (DomainPhytoProductInput sourceDomainPhytoProductInput : sourceDomainPhytoProductInputs) {

                        // bind phyto parameters
                        DomainPhytoProductInput extendDomainPhytoProductInput = bindSeedingPytoProduct(
                                toDomain,
                                sourceDomainPhytoProductInput,
                                preserveInputCode);
                        // add product to species
                        copiedDomainSeedSpeciesInput.addSpeciesPhytoInputs(extendDomainPhytoProductInput);
                    }

                }
            }

            return copiedSeedLotInput;
        }

        LOGGER.warn(String.format("Copy domain seed lot with crop %s (%s) from domain %s (%s) to domain %s (%s) can not be performed because of missing similar crop as %s",
                cropSeed.getName(), cropSeed.getTopiaId(), fromDomain.getName(), fromDomain.getTopiaId(), toDomain.getName(), toDomain.getTopiaId(), croppingPlanEntryComparable));

        return null;
    }

    private DomainSeedLotInput bindDomainSeedLotInput(Domain toDomain, DomainSeedLotInput sourceDomainSeedLotInput, boolean preserveInputCode, CroppingPlanEntry croppingPlanEntry) {
        DomainSeedLotInput copiedSeedLotInput = domainSeedLotInputDao.newInstance();
        domainSeedLotInputBinder.copyExcluding(sourceDomainSeedLotInput, copiedSeedLotInput,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                AbstractDomainInputStockUnit.PROPERTY_CODE,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY,
                DomainSeedLotInput.PROPERTY_DOMAIN_SEED_SPECIES_INPUT,
                DomainSeedLotInput.PROPERTY_SEED_PRICE
        );
        final String seedLotCode = preserveInputCode ? sourceDomainSeedLotInput.getCode() : UUID.randomUUID().toString();
        copiedSeedLotInput.setDomain(toDomain);
        copiedSeedLotInput.setCode(seedLotCode);
        copiedSeedLotInput.setCropSeed(croppingPlanEntry);
        String lotCropSeedInputKey = DomainInputStockUnitService.getLotCropSeedInputKey(croppingPlanEntry, copiedSeedLotInput.isOrganic(), copiedSeedLotInput.getUsageUnit());
        copiedSeedLotInput.setInputKey(lotCropSeedInputKey);
        return copiedSeedLotInput;
    }

    private DomainPhytoProductInput bindSeedingPytoProduct(
            Domain toDomain,
            DomainPhytoProductInput sourceDomainPhytoProductInput,
            boolean preserveInputCode) {

        // bind species phyto parameters
        DomainPhytoProductInput extendDomainPhytoProductInput = domainPhytoProductInputDao.newInstance();
        domainPhytoProductInputBinder.copyExcluding(sourceDomainPhytoProductInput, extendDomainPhytoProductInput,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE
        );
        if (!preserveInputCode) {
            extendDomainPhytoProductInput.setCode(UUID.randomUUID().toString());
        }
        extendDomainPhytoProductInput.setDomain(toDomain);
        extendDomainPhytoProductInput.setInputKey(DomainInputStockUnitService.getPhytoInputKey(extendDomainPhytoProductInput.getRefInput(), extendDomainPhytoProductInput.getUsageUnit()));

        // bind phyto price to product
        final InputPrice sourceSpeciesPhytoInputPrice = sourceDomainPhytoProductInput.getInputPrice();
        if (sourceSpeciesPhytoInputPrice != null) {
            InputPrice inputPrice = inputPriceService.bindToNewPrice(
                    toDomain,
                    sourceSpeciesPhytoInputPrice,
                    sourceSpeciesPhytoInputPrice.getObjectId(),
                    Optional.empty(),
                    Optional.empty());
            inputPrice = inputPriceService.persistInputPrice(inputPrice);
            extendDomainPhytoProductInput.setInputPrice(inputPrice);
        }
        return extendDomainPhytoProductInput;
    }

    @Override
    public DomainPhytoProductInput duplicateAndAddPhytoProductToDomainSeedSpecies(
            Domain domain,
            DomainSeedSpeciesInput domainSeedSpeciesInput,
            DomainPhytoProductInput phytoInputModel,
            boolean preserveInputCode) {

        DomainPhytoProductInput domainPhytoProductInput = bindSeedingPytoProduct(
                domain,
                phytoInputModel,
                preserveInputCode);

        domainPhytoProductInputDao.create(domainPhytoProductInput);

        domainSeedSpeciesInput.addSpeciesPhytoInputs(domainPhytoProductInput);

        domainSeedSpeciesInputDao.update(domainSeedSpeciesInput);

        return domainPhytoProductInput;

    }

    protected Map<InputType, List<AbstractDomainInputStockUnit>> filterOnNotExistingInputs(Domain toDomain, Map<InputType, List<AbstractDomainInputStockUnit>> fromDomainInputByTypes) {
        Map<InputType, List<AbstractDomainInputStockUnit>> filteredDomainInputStocksByTypesToCopy = new HashMap<>();
        // remove existing input
        Map<InputType, List<AbstractDomainInputStockUnit>> targetDomainInputStocks = loadDomainInputStock(toDomain);
        final Set<String> targetedKey = targetDomainInputStocks.values().stream()
                .flatMap(Collection::stream)
                .map(AbstractDomainInputStockUnit::getInputKey)
                .collect(Collectors.toSet());

        final boolean isExistingDomainInputsOnTargetedDomain = !targetedKey.isEmpty();
        if (isExistingDomainInputsOnTargetedDomain) {
            for (Map.Entry<InputType, List<AbstractDomainInputStockUnit>> domainInputToCopy : fromDomainInputByTypes.entrySet()) {
                // for some inputType we can use the key to find du
                final List<AbstractDomainInputStockUnit> domainInputToCopyValue = domainInputToCopy.getValue();
                final InputType inputType = domainInputToCopy.getKey();
                switch (inputType) {
                    case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                            EPANDAGES_ORGANIQUES,
                            APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                            LUTTE_BIOLOGIQUE,
                            CARBURANT,
                            MAIN_OEUVRE_MANUELLE,
                            MAIN_OEUVRE_TRACTORISTE,
                            IRRIGATION,
                            POT,
                            SUBSTRAT,
                            AUTRE,
                            TRAITEMENT_SEMENCE -> {
                        final List<AbstractDomainInputStockUnit> collectInputs = domainInputToCopyValue.stream().filter(di -> !targetedKey.contains(di.getInputKey())).toList();
                        filteredDomainInputStocksByTypesToCopy.put(inputType, collectInputs);
                    }
                    case PLAN_COMPAGNE, SEMIS -> {
                        List<AbstractDomainInputStockUnit> collectInputs = getNotExistingSeedLots(targetDomainInputStocks, domainInputToCopyValue);
                        filteredDomainInputStocksByTypesToCopy.put(inputType, collectInputs);
                    }
                }
            }
        } else {
            filteredDomainInputStocksByTypesToCopy = fromDomainInputByTypes;
        }
        return filteredDomainInputStocksByTypesToCopy;
    }

    protected Map<InputType, List<AbstractDomainInputStockUnit>> filterOnNotExistingWorkforceAndOperationInputs(
            Domain toDomain,
            Map<InputType, List<AbstractDomainInputStockUnit>> fromDomainInputByTypes) {

        Map<InputType, List<AbstractDomainInputStockUnit>> filteredDomainInputStocksByTypesToCopy = new HashMap<>(fromDomainInputByTypes);
        if (CollectionUtils.isNotEmpty(fromDomainInputByTypes.get(InputType.IRRIGATION))) {
            com.google.common.base.Optional<DomainIrrigationInput> domainIrrigationInputOptional = domainIrrigationDao.forDomainEquals(toDomain).tryFindUnique();
            if (domainIrrigationInputOptional.isPresent() && domainIrrigationInputOptional.get().getInputPrice() != null && domainIrrigationInputOptional.get().getInputPrice().getPrice() != null) {
                filteredDomainInputStocksByTypesToCopy.remove(InputType.IRRIGATION);
            }
        }
        if (CollectionUtils.isNotEmpty(fromDomainInputByTypes.get(InputType.CARBURANT))) {
            com.google.common.base.Optional<DomainFuelInput> domainFuelInputOptional = domainFuelDao.forDomainEquals(toDomain).tryFindUnique();
            if (domainFuelInputOptional.isPresent() && domainFuelInputOptional.get().getInputPrice() != null && domainFuelInputOptional.get().getInputPrice().getPrice() != null) {
                filteredDomainInputStocksByTypesToCopy.remove(InputType.CARBURANT);
            }
        }
        if (CollectionUtils.isNotEmpty(fromDomainInputByTypes.get(InputType.MAIN_OEUVRE_MANUELLE))) {
            com.google.common.base.Optional<DomainManualWorkforceInput> domainManualWorkforceInputOptional = domainManualWorkforceDao.forDomainEquals(toDomain).tryFindUnique();
            if (domainManualWorkforceInputOptional.isPresent() && domainManualWorkforceInputOptional.get().getInputPrice() != null && domainManualWorkforceInputOptional.get().getInputPrice() .getPrice() != null) {
                filteredDomainInputStocksByTypesToCopy.remove(InputType.MAIN_OEUVRE_MANUELLE);
            }
        }
        if (CollectionUtils.isNotEmpty(fromDomainInputByTypes.get(InputType.MAIN_OEUVRE_TRACTORISTE))) {
            com.google.common.base.Optional<DomainMechanizedWorkforceInput> domainMechanizedWorkforceInputOptional = domainMechanizedWorkforceDao.forDomainEquals(toDomain).tryFindUnique();
            if (domainMechanizedWorkforceInputOptional.isPresent() && domainMechanizedWorkforceInputOptional.get().getInputPrice() != null && domainMechanizedWorkforceInputOptional.get().getInputPrice() .getPrice() != null) {
                filteredDomainInputStocksByTypesToCopy.remove(InputType.MAIN_OEUVRE_TRACTORISTE);
            }
        }

        return filteredDomainInputStocksByTypesToCopy;
    }

    protected static List<AbstractDomainInputStockUnit> getNotExistingSeedLots(Map<InputType, List<AbstractDomainInputStockUnit>> targetDomainInputStocks, List<AbstractDomainInputStockUnit> domainInputToCopyValue) {
        Collection<AbstractDomainInputStockUnit> targetedDomainSeedInputs = new ArrayList<>(CollectionUtils.emptyIfNull(targetDomainInputStocks.get(InputType.PLAN_COMPAGNE)));
        targetedDomainSeedInputs.addAll(CollectionUtils.emptyIfNull(targetDomainInputStocks.get(InputType.SEMIS)));

        List<AbstractDomainInputStockUnit> collectInputs = Lists.newArrayList(domainInputToCopyValue);

        if (CollectionUtils.isNotEmpty(targetedDomainSeedInputs)) {
            Set<SeedLotComparable> targetedDomainSeedInputKeys = new HashSet<>();

            // we consider identical lot if there have same refEspece and product input
            final List<AbstractDomainInputStockUnit> targetedDomainSeedLots = targetedDomainSeedInputs.stream()
                    .filter(tsl -> tsl instanceof DomainSeedLotInput).toList();

            targetedDomainSeedLots.forEach(tsl -> {
                final DomainSeedLotInput tsl1 = (DomainSeedLotInput) tsl;
                SeedLotComparable seedLotComparable = new SeedLotComparable(tsl1);
                targetedDomainSeedInputKeys.add(seedLotComparable);
            });

            for (AbstractDomainInputStockUnit domainInputStockUnit : domainInputToCopyValue) {
                if (domainInputStockUnit instanceof DomainSeedLotInput domainSeedLotInputToCopy) {
                    SeedLotComparable seedLotComparable = new SeedLotComparable(domainSeedLotInputToCopy);
                    if (targetedDomainSeedInputKeys.contains(seedLotComparable)) {
                        collectInputs.remove(domainInputStockUnit);
                    }
                }
            }

        }
        return collectInputs;
    }

    protected Map<InputType, List<AbstractDomainInputStockUnit>> loadDomainInputStockWithIds(Collection<String> inputIds) {
        Collection<AbstractDomainInputStockUnit> inputs = domainInputDao.forTopiaIdIn(inputIds).findAll();
        Map<InputType, List<AbstractDomainInputStockUnit>> result = inputs.stream().collect(Collectors.groupingBy(AbstractDomainInputStockUnit::getInputType));
        return result;
    }

    protected List<DomainSeedLotInput> bindDomainSeedLot(
            Domain fromDomain,
            Domain toDomain,
            Map<String, CroppingPlanEntry> extendCroppingPlanByCodes,
            Map<String, CroppingPlanSpecies> extendCroppingPlanSpeciesByCodes,
            List<AbstractDomainInputStockUnit> sourceInputStockUnits) {

        boolean isSameDomaineCode = !fromDomain.equals(toDomain) && fromDomain.getCode().contentEquals(toDomain.getCode());

        List<DomainSeedLotInput> result = new ArrayList<>();
        for (AbstractDomainInputStockUnit sourceSeedingInputStockUnit : sourceInputStockUnits) {
            if (sourceSeedingInputStockUnit instanceof DomainSeedLotInput sourceDomainSeedLotInput) {

                final CroppingPlanEntry sourceCropSeed = sourceDomainSeedLotInput.getCropSeed();
                final CroppingPlanEntry extendCropSeed = extendCroppingPlanByCodes.get(sourceCropSeed.getCode());

                if (Objects.isNull(extendCropSeed)) {
                    Locale locale = getSecurityContext().getLocale();
                    var message = String.format(l(locale, "fr.inra.agrosyst.services.domain.inputStock.DomainInputStockUnitServiceImpl.duplicatedMissingCropError"),
                            sourceCropSeed.getName(),
                            toDomain.getName() + " (" + toDomain.getCampaign() + ")",
                            fromDomain.getName() + " (" + fromDomain.getCampaign() + ")");
                    throw new NullPointerException(message);
                }

                boolean preserveInputCode = isSameDomaineCode && checkInputCodeNotUsed(toDomain, sourceDomainSeedLotInput);

                final DomainSeedLotInput extendSeedLotInput = bindDomainSeedLotInput(
                        toDomain,
                        sourceDomainSeedLotInput,
                        preserveInputCode,
                        extendCropSeed);

                final String lotPriceOpjectId = InputPriceService.GET_CROP_SEED_OBJECT_ID.apply(extendCropSeed, extendSeedLotInput.isOrganic());
                if (sourceDomainSeedLotInput.getSeedPrice() != null &&
                        sourceDomainSeedLotInput.getSeedPrice().getPriceUnit() != PriceUnit.EURO_HA &&
                        sourceDomainSeedLotInput.getSeedPrice().getPriceUnit() != PriceUnit.EURO_M3) {
                    final InputPrice newLotSeedPrice = inputPriceService.bindToNewPrice(
                            toDomain,
                            sourceDomainSeedLotInput.getSeedPrice(),
                            lotPriceOpjectId,
                            Optional.empty(),
                            Optional.empty());
                    final InputPrice extendedLotSeedPrice = inputPriceService.persistInputPrice(newLotSeedPrice);
                    extendSeedLotInput.setSeedPrice((SeedPrice) extendedLotSeedPrice);
                }

                // species from lot
                final Collection<DomainSeedSpeciesInput> sourceDomainSeedSpeciesInput = sourceDomainSeedLotInput.getDomainSeedSpeciesInput();
                if (CollectionUtils.isNotEmpty(sourceDomainSeedSpeciesInput)) {

                    for (DomainSeedSpeciesInput sourceSeedSpeciesInput : sourceDomainSeedSpeciesInput) {
                        // add species to species input
                        final CroppingPlanSpecies sourceSpeciesSeed = sourceSeedSpeciesInput.getSpeciesSeed();
                        final String speciesSeedCode = sourceSpeciesSeed.getCode();

                        CroppingPlanSpecies extendedSpeciesSeed = extendCroppingPlanSpeciesByCodes.get(speciesSeedCode);
                        RefEspece refSpecies = sourceSpeciesSeed.getSpecies();
                        RefVariete refVariety = sourceSpeciesSeed.getVariety();

                        if (extendedSpeciesSeed == null) {
                            Optional<CroppingPlanSpecies> similarSpecies = CollectionUtils.emptyIfNull(extendCropSeed.getCroppingPlanSpecies()).stream()
                                    .filter(targetedCPS -> targetedCPS.getSpecies().equals(refSpecies) &&
                                            targetedCPS.getVariety().equals(refVariety)).findAny();
                            if (similarSpecies.isPresent()) {
                                extendedSpeciesSeed = similarSpecies.get();
                            }
                        }

                        if (extendedSpeciesSeed == null) {
                            Locale locale = getSecurityContext().getLocale();
                            String varietyName = refVariety != null ? " (" + refVariety.getLabel() + ")" : "";
                            var message = String.format(l(locale, "fr.inra.agrosyst.services.domain.inputStock.DomainInputStockUnitServiceImpl.duplicatedMissingSpeciesError"),
                                    refSpecies.getLibelle_espece_botanique_Translated() + varietyName,
                                    sourceCropSeed.getName(),
                                    toDomain.getName() + " (" + toDomain.getCampaign() + ")",
                                    fromDomain.getName() + " (" + fromDomain.getCampaign() + ")");
                            throw new NullPointerException(message);
                        }

                        // copy species parameters
                        DomainSeedSpeciesInput extendedDomainSeedSpeciesInput = bindDomainSeedSpeciesInput(
                                toDomain,
                                sourceSeedSpeciesInput,
                                preserveInputCode,
                                extendedSpeciesSeed,
                                extendSeedLotInput.getUsageUnit());

                        extendSeedLotInput.addDomainSeedSpeciesInput(extendedDomainSeedSpeciesInput);

                        // species seed price
                        final String speciesPriceOpjectId = InputPriceService.GET_SPECIES_SEED_OBJECT_ID.apply(refSpecies, sourceSpeciesSeed.getVariety());
                        final InputPrice newSpeciesSeedPrice = inputPriceService.bindToNewPrice(
                                toDomain,
                                sourceSeedSpeciesInput.getSeedPrice(),
                                speciesPriceOpjectId,
                                Optional.empty(),
                                Optional.empty());
                        final InputPrice extendedSpeciesSeedPrice = inputPriceService.persistInputPrice(newSpeciesSeedPrice);
                        extendedDomainSeedSpeciesInput.setSeedPrice((SeedPrice) extendedSpeciesSeedPrice);

                        // species phyto product
                        final Collection<DomainPhytoProductInput> sourceDomainPhytoProductInputs = CollectionUtils.emptyIfNull(sourceSeedSpeciesInput.getSpeciesPhytoInputs());
                        for (DomainPhytoProductInput sourceDomainPhytoProductInput : sourceDomainPhytoProductInputs) {

                            // bind species phyto parameters
                            DomainPhytoProductInput extendDomainPhytoProductInput = bindSeedingPytoProduct(
                                    toDomain,
                                    sourceDomainPhytoProductInput,
                                    preserveInputCode);

                            // add product to species
                            extendedDomainSeedSpeciesInput.addSpeciesPhytoInputs(extendDomainPhytoProductInput);

                        }

                    }
                }

                result.add(extendSeedLotInput);
            }
        }
        return result;
    }

    private DomainSeedSpeciesInput bindDomainSeedSpeciesInput(
            Domain toDomain,
            DomainSeedSpeciesInput sourceSeedSpeciesInput,
            boolean preserveInputCode,
            CroppingPlanSpecies extendedSpeciesSeed,
            SeedPlantUnit usageUnit) {

        DomainSeedSpeciesInput extendedDomainSeedSpeciesInput = domainSeedSpeciesInputDao.newInstance();
        domainSeedSpeciesInputBinder.copyExcluding(sourceSeedSpeciesInput, extendedDomainSeedSpeciesInput,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY,
                DomainSeedSpeciesInput.PROPERTY_CODE,
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED,
                DomainSeedSpeciesInput.PROPERTY_SEED_PRICE,
                DomainSeedSpeciesInput.PROPERTY_SPECIES_PHYTO_INPUTS
        );

        String speciesInputCode = preserveInputCode ? sourceSeedSpeciesInput.getCode() : UUID.randomUUID().toString();
        extendedDomainSeedSpeciesInput.setCode(speciesInputCode);
        extendedDomainSeedSpeciesInput.setDomain(toDomain);
        extendedDomainSeedSpeciesInput.setSpeciesSeed(extendedSpeciesSeed);
        updateSeedSpeciesInputKey(extendedSpeciesSeed, usageUnit, extendedDomainSeedSpeciesInput);

        return extendedDomainSeedSpeciesInput;
    }

    private static void updateSeedSpeciesInputKey(
            CroppingPlanSpecies extendedSpeciesSeed,
            SeedPlantUnit usageUnit,
            DomainSeedSpeciesInput extendedDomainSeedSpeciesInput) {

        RefEspece species = extendedSpeciesSeed.getSpecies();
        String vLabel = extendedSpeciesSeed.getVariety() != null ? extendedSpeciesSeed.getVariety().getLabel() : "";
        String lotSpeciesInputKey = DomainInputStockUnitService.getLotSpeciesInputKey(species.getCode_espece_botanique(),
                species.getCode_qualifiant_AEE(),
                extendedDomainSeedSpeciesInput.isBiologicalSeedInoculation(),
                extendedDomainSeedSpeciesInput.isChemicalTreatment(),
                extendedDomainSeedSpeciesInput.isOrganic(),
                vLabel,
                usageUnit,
                extendedDomainSeedSpeciesInput.getSeedType());
        extendedDomainSeedSpeciesInput.setInputKey(lotSpeciesInputKey);
    }

    private boolean checkInputCodeNotUsed(Domain toDomain, AbstractDomainInputStockUnit domainInputStockUnit) {
        if (isDuplicatedInputCode(toDomain, domainInputStockUnit)) {

            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn(String.format("La copie vers un même domaine '%s'(%d) '%s' d'un intrant '%s' ayant le même code '%s' n'est pas authorisé",
                        toDomain.getName(),
                        toDomain.getCampaign(),
                        toDomain.getTopiaId(),
                        domainInputStockUnit.getTopiaId(),
                        domainInputStockUnit.getCode()));
            }
            return false;
        }
        return true;
    }

    @Override
    public void deleteDomainInputStockUnit(Domain domain) {
        //List<InputPrice> inputPrices = inputPriceDao.forDomainEquals(domain).findAll();
        domainFuelDao.deleteAll(domainFuelDao.forDomainEquals(domain).findAll());
        domainIrrigationDao.deleteAll(domainIrrigationDao.forDomainEquals(domain).findAll());
        domainMineralProductInputDao.deleteAll(domainMineralProductInputDao.forDomainEquals(domain).findAll());
        domainOrganicProductInputDao.deleteAll(domainOrganicProductInputDao.forDomainEquals(domain).findAll());
        domainOtherInputDao.deleteAll(domainOtherInputDao.forDomainEquals(domain).findAll());
        domainPhytoProductInputDao.deleteAll(domainPhytoProductInputDao.forDomainEquals(domain).findAll());
        domainSubstrateInputDao.deleteAll(domainSubstrateInputDao.forDomainEquals(domain).findAll());
        domainPotInputDao.deleteAll(domainPotInputDao.forDomainEquals(domain).findAll());
        //domainSeedSpeciesInputDao.deleteAll(domainSeedSpeciesInputDao.forDomainEquals(domain).findAll());
        domainSeedLotInputDao.deleteAll(domainSeedLotInputDao.forDomainEquals(domain).findAll());
        //inputPriceDao.deleteAll(inputPrices);
    }

    @Override
    public Map<InputType, List<AbstractDomainInputStockUnit>> loadDomainInputStock(Domain domain) {
        Collection<AbstractDomainInputStockUnit> inputs = domainInputDao.forDomainEquals(domain).findAll();
        inputs = inputs.stream()
                .filter(di -> !(di instanceof DomainSeedSpeciesInput))
                .filter(di -> !(di instanceof DomainPhytoProductInput domainPhytoProductInput &&
                        InputType.TRAITEMENT_SEMENCE.equals(domainPhytoProductInput.getInputType())))
                .toList();
        Map<InputType, List<AbstractDomainInputStockUnit>> result = inputs.stream().collect(Collectors.groupingBy(AbstractDomainInputStockUnit::getInputType));
        return result;
    }

    @Override
    public Map<String, AbstractDomainInputStockUnit> loadDomainInputStockByIds(Domain domain) {
        Collection<AbstractDomainInputStockUnit> inputs = domainInputDao.forDomainEquals(domain).findAll();
        Map<String, AbstractDomainInputStockUnit> result = inputs.stream()
                .collect(
                        Collectors.toMap(
                                Entities.GET_TOPIA_ID, Function.identity()));
        return result;
    }

    @Override
    public Map<String, List<AbstractDomainInputStockUnit>> loadDomainInputStockByIKeys(Domain domain) {
        Collection<AbstractDomainInputStockUnit> inputs = domainInputDao.forDomainEquals(domain).findAll();
        Map<String, List<AbstractDomainInputStockUnit>> result = new HashMap<>();

        inputs.forEach(
                aisu -> {
                    List<AbstractDomainInputStockUnit> abstractDomainInputStockUnits = result.computeIfAbsent(aisu.getInputKey(), k -> new ArrayList<>());
                    abstractDomainInputStockUnits.add(aisu);
                });
        return result;
    }

    @Override
    public Map<String, AbstractDomainInputStockUnit> loadDomainInputStockByCodes(Domain domain) {
        Collection<AbstractDomainInputStockUnit> inputs = domainInputDao.forDomainEquals(domain).findAll();
        Map<String, AbstractDomainInputStockUnit> result = inputs.stream()
                .collect(
                        Collectors.toMap(
                                AbstractDomainInputStockUnit::getCode, Function.identity()));
        return result;
    }

    @Override
    public Map<String, DomainInputDto> loadDomainInputStockDtoByCodes(String domainId) {
        Collection<DomainInputDto> domainInputDtos = loadDomainInputStockDtos(domainId);
        Map<String, DomainInputDto> result = domainInputDtos.stream()
                .collect(
                        Collectors.toMap(
                                DomainInputDto::getCode, Function.identity(), (d1, d2) -> d1));
        return result;
    }

    @Override
    public <I extends AbstractDomainInputStockUnit> I createOrUpdateDomainInput(I input) {
        AbstractDomainInputStockUnit result = createOrUpdateDomainInput_(input);
        return (I) result;
    }

    @Override
    public DomainIrrigationInput newDomainIrrigationInput(Domain domain) {
        DomainIrrigationInput entity = domainIrrigationDao.newInstance();
        entity.setDomain(domain);
        entity.setInputType(InputType.IRRIGATION);
        entity.setUsageUnit(IrrigationUnit.MM);
        entity.setCode(UUID.randomUUID().toString());
        entity.setInputKey(DomainInputStockUnitService.getIrrigInputKey(IrrigationUnit.MM));
        entity.setInputName(IRRIGATION_INPUT_NAME);
        DomainIrrigationInput domainIrrigationInput = domainIrrigationDao.create(entity);
        return domainIrrigationInput;
    }

    protected void createUpdateOrDeletePrice(Domain domain, DomainInputDto domainInputDto, AbstractDomainInputStockUnit inputEntity) {
        Optional<InputPriceDto> domainInputPriceDto = domainInputDto.getPrice();
        if (domainInputPriceDto.isPresent() && domainInputPriceDto.get().getPrice() != null) {
            final InputPriceDto inputPriceDto = domainInputPriceDto.get();
            InputPrice inputPriceEntity = inputEntity.getInputPrice() == null ? inputPriceService.getNewPriceInstance(inputPriceDto.getCategory()) : inputEntity.getInputPrice();
            String objectId = InputPriceService.GET_INPUT_OBJECT_ID.apply(inputEntity);
            inputPriceService.bindDtoToPrice(domain, inputPriceDto, inputPriceEntity, objectId, Optional.empty(), Optional.empty());
            inputPriceEntity = inputPriceService.persistInputPrice(inputPriceEntity);
            inputEntity.setInputPrice(inputPriceEntity);

        } else if (inputEntity.isPersisted() && inputEntity.getInputPrice() != null) {
            inputEntity.setInputPrice(null);
            //inputPriceService.deleteInputPrice(inputEntity.getInputPrice());
        }
    }

    private AbstractDomainInputStockUnit createOrUpdateDomainInput_(AbstractDomainInputStockUnit entity) {
        InputType inputType = entity.getInputType();
        switch (inputType) {
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                if (entity.isPersisted()) {
                    entity = domainMineralProductInputDao.update((DomainMineralProductInput) entity);
                } else {
                    entity = domainMineralProductInputDao.create((DomainMineralProductInput) entity);
                }
            }
            case EPANDAGES_ORGANIQUES -> {
                if (entity.isPersisted()) {
                    entity = domainOrganicProductInputDao.update((DomainOrganicProductInput) entity);
                } else {
                    entity = domainOrganicProductInputDao.create((DomainOrganicProductInput) entity);
                }
            }
            case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, LUTTE_BIOLOGIQUE, TRAITEMENT_SEMENCE -> {
                if (entity.isPersisted()) {
                    entity = domainPhytoProductInputDao.update((DomainPhytoProductInput) entity);
                } else {
                    entity = domainPhytoProductInputDao.create((DomainPhytoProductInput) entity);
                }
            }
            case CARBURANT -> {
                if (entity.isPersisted()) {
                    entity = domainFuelDao.update((DomainFuelInput) entity);
                } else {
                    entity = domainFuelDao.create((DomainFuelInput) entity);
                }
            }
            case MAIN_OEUVRE_MANUELLE -> {
                if (entity.isPersisted()) {
                    entity = domainManualWorkforceDao.update((DomainManualWorkforceInput) entity);
                } else {
                    entity = domainManualWorkforceDao.create((DomainManualWorkforceInput) entity);
                }
            }
            case MAIN_OEUVRE_TRACTORISTE -> {
                if (entity.isPersisted()) {
                    entity = domainMechanizedWorkforceDao.update((DomainMechanizedWorkforceInput) entity);
                } else {
                    entity = domainMechanizedWorkforceDao.create((DomainMechanizedWorkforceInput) entity);
                }
            }
            case IRRIGATION -> {
                if (entity.isPersisted()) {
                    entity = domainIrrigationDao.update((DomainIrrigationInput) entity);
                } else {
                    entity = domainIrrigationDao.create((DomainIrrigationInput) entity);
                }
            }
            case SEMIS, PLAN_COMPAGNE -> {
                if (entity instanceof DomainSeedLotInput domainSeedLotInput) {
                    if (entity.isPersisted()) {
                        entity = domainSeedLotInputDao.update(domainSeedLotInput);
                    } else {
                        entity = domainSeedLotInputDao.create(domainSeedLotInput);
                    }
                }
            }
            case AUTRE -> {
                if (entity.isPersisted()) {
                    entity = domainOtherInputDao.update((DomainOtherInput) entity);
                } else {
                    entity = domainOtherInputDao.create((DomainOtherInput) entity);
                }
            }
            case SUBSTRAT -> {
                if (entity.isPersisted()) {
                    entity = domainSubstrateInputDao.update((DomainSubstrateInput) entity);
                } else {
                    entity = domainSubstrateInputDao.create((DomainSubstrateInput) entity);
                }
            }
            case POT -> {
                if (entity.isPersisted()) {
                    entity = domainPotInputDao.update((DomainPotInput) entity);
                } else {
                    entity = domainPotInputDao.create((DomainPotInput) entity);
                }
            }
            default -> throw new IllegalStateException("Unexpected value: " + inputType);
        }
        return entity;
    }

    private AbstractDomainInputStockUnit getOrCreateInstance(
            Map<String, ? extends AbstractDomainInputStockUnit> mutablePersistedDomainInputByIds,
            DomainInputDto domainInputDto,
            InputType domainInputType) {

        AbstractDomainInputStockUnit entity;
        if (domainInputDto.getTopiaId().isPresent() &&
                mutablePersistedDomainInputByIds.containsKey(domainInputDto.getTopiaId().get())) {
            entity = mutablePersistedDomainInputByIds.remove(domainInputDto.getTopiaId().get());
        } else {
            entity = getAbstractDomainInputStockUnitNewInstance(domainInputType);
        }
        return entity;
    }

    protected AbstractDomainInputStockUnit getAbstractDomainInputStockUnitNewInstance(InputType domainInputType) {
        AbstractDomainInputStockUnit entity;
        switch (domainInputType) {
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> entity = domainMineralProductInputDao.newInstance();
            case EPANDAGES_ORGANIQUES -> entity = domainOrganicProductInputDao.newInstance();
            case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, LUTTE_BIOLOGIQUE, TRAITEMENT_SEMENCE ->
                    entity = domainPhytoProductInputDao.newInstance();
            case CARBURANT -> entity = domainFuelDao.newInstance();
            case MAIN_OEUVRE_MANUELLE -> entity = domainManualWorkforceDao.newInstance();
            case MAIN_OEUVRE_TRACTORISTE -> entity = domainMechanizedWorkforceDao.newInstance();
            case IRRIGATION -> entity = domainIrrigationDao.newInstance();
            case SEMIS, PLAN_COMPAGNE -> entity = domainSeedLotInputDao.newInstance();
            case AUTRE -> entity = domainOtherInputDao.newInstance();
            case SUBSTRAT -> entity = domainSubstrateInputDao.newInstance();
            case POT -> entity = domainPotInputDao.newInstance();
            default -> throw new IllegalStateException("Unexpected value: " + domainInputType);
        }
        entity.setCode(UUID.randomUUID().toString());
        return entity;
    }

    protected void bindSeedLotInputDtoToEntity(
            Domain domain,
            Map<String, Boolean> inputUsageByIds,
            MultiValuedMap<DomainPhytoProductInput, InputUsageServiceImpl.SeedingProductUsageToRemoveForDomainSeedingProduct> usageToRemovesForDomainSeedProduct,
            Map<String, CropPersistResult> cropsByTmpIds,
            DomainInputDto domainInputDto,
            boolean isInputUsed,
            DomainSeedLotInput seedLotInput,
            DomainReferentialInputs domainInputReferential,
            Map<SeedType, String> seedTypeTranslations) {

        DomainSeedLotInputDto domainSeedLotInputDto = (DomainSeedLotInputDto) domainInputDto;

        // in case the user couldn't change the the status of SeedCoatedTreatment because switch was no more available
        if (CollectionUtils.emptyIfNull(domainSeedLotInputDto.getSpeciesInputs())
                .stream()
                .noneMatch(dss -> dss.isBiologicalSeedInoculation() || dss.isChemicalTreatment())
                && domainSeedLotInputDto.isSeedCoatedTreatment()) {
            domainSeedLotInputDto = domainSeedLotInputDto.toBuilder().seedCoatedTreatment(false).build();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String .format("Sur le domaine '%s', intrant '%s', pas de traitement déclaré sur le lot on force SeedCoatedTreatment à false",
                        domain.getTopiaId(),
                        domainSeedLotInputDto.getTopiaId().orElse("Nouveau")));
            }
        }

        DomainInputBinder.shallowBindDomainSeedingLotDtoToEntity(domain, domainSeedLotInputDto, seedLotInput, cropsByTmpIds, isInputUsed);

        createOrUpdateSeedLotSpeciesInput(
                domain,
                inputUsageByIds,
                seedLotInput,
                domainSeedLotInputDto,
                domainInputReferential,
                cropsByTmpIds,
                seedTypeTranslations,
                usageToRemovesForDomainSeedProduct
        );

        createOrUpdateSeedLotPrice(
                domain,
                domainSeedLotInputDto,
                seedLotInput);
    }

    protected void createOrUpdateSeedLotSpeciesInput(Domain domain,
                                                     Map<String, Boolean> inputUsageByIds,
                                                     DomainSeedLotInput seedLotInput,
                                                     DomainSeedLotInputDto domainSeedLotInputDto,
                                                     DomainReferentialInputs domainInputReferential,
                                                     Map<String, CropPersistResult> cropsByIds,
                                                     Map<SeedType, String> seedTypeTranslations,
                                                     MultiValuedMap<DomainPhytoProductInput, InputUsageServiceImpl.SeedingProductUsageToRemoveForDomainSeedingProduct> usageToRemovesForDomainSeedProduct) {

        final Collection<DomainSeedSpeciesInputDto> speciesInputsDto = domainSeedLotInputDto.getSpeciesInputs();
        final @NonNull InputType inputType = domainSeedLotInputDto.getInputType();

        if (CollectionUtils.isNotEmpty(speciesInputsDto)) {
            final CroppingPlanEntryDto cropSeedDto = domainSeedLotInputDto.getCropSeedDto();
            final CropPersistResult cropPersistResult = cropsByIds.get(cropSeedDto.getTopiaId());
            final Map<String, CroppingPlanSpecies> speciesSeedsByIds = cropPersistResult.cropSpeciesByTmpId();
            final SeedPlantUnit usageUnit = seedLotInput.getUsageUnit();

            Collection<DomainSeedSpeciesInput> domainSeedSpeciesInputs = CollectionUtils.emptyIfNull(seedLotInput.getDomainSeedSpeciesInput());
            Map<String, DomainSeedSpeciesInput> domainSeedSpeciesInputById = new HashMap<>(Maps.uniqueIndex(domainSeedSpeciesInputs, TopiaEntity::getTopiaId));

            if (domainSeedLotInputDto.isSeedCoatedTreatment() != isLotPriceIncludeTreatment(domainSeedLotInputDto)) {
                LOGGER.debug(String .format("Sur le domaine '%s', intrant '%s', incohérence entre les donnés du lot et les prix déclarés sur le lot",
                        domain.getTopiaId(),
                        domainSeedLotInputDto.getTopiaId().orElse("Nouveau")));
            }

            List<DomainSeedSpeciesInputDto> speciesInputsDtos = removeSeedingNonValideProductPricesFromSeedSpeciesDtos(speciesInputsDto, domainSeedLotInputDto.isSeedCoatedTreatment());

            for (DomainSeedSpeciesInputDto domainSeedSpeciesInputDto : speciesInputsDtos) {

                boolean isSpeciesUsed = domainSeedSpeciesInputDto.getTopiaId().isPresent() &&
                        inputUsageByIds.containsKey(domainSeedSpeciesInputDto.getTopiaId().get()) ?
                        inputUsageByIds.get(domainSeedSpeciesInputDto.getTopiaId().get()) : false;

                DomainSeedSpeciesInput speciesInputEntity = getOrCreateDomainSeedSpeciesInput(
                        seedLotInput,
                        domainSeedSpeciesInputDto,
                        domainSeedSpeciesInputById);

                shallowBindDomainSeedSpeciesInputDtoToEntity(
                        domain,
                        domainSeedSpeciesInputDto,
                        speciesInputEntity,
                        speciesSeedsByIds,
                        isSpeciesUsed,
                        inputType,
                        usageUnit,
                        seedTypeTranslations);

                createOrUpdateSpeciesPhytoProductInput(
                        domain,
                        domainSeedSpeciesInputDto.getSpeciesPhytoInputDtos(),
                        speciesInputEntity,
                        domainInputReferential,
                        inputUsageByIds,
                        usageToRemovesForDomainSeedProduct);

                bindSpeciesSeedPrice(
                        domain,
                        domainSeedSpeciesInputDto,
                        speciesInputEntity);
            }

            domainSeedSpeciesInputById.values()
                    .stream()
                    .filter(
                            speciesSeed -> isSpeciesNotUsed(inputUsageByIds, speciesSeed))
                    .forEach(seedLotInput::removeDomainSeedSpeciesInput);

        } else {
            removeSeedingSpeciesInputs(inputUsageByIds, seedLotInput);
        }
    }

    protected DomainSeedSpeciesInput getOrCreateDomainSeedSpeciesInput(DomainSeedLotInput seedLotInput, DomainSeedSpeciesInputDto domainSeedSpeciesInputDto, Map<String, DomainSeedSpeciesInput> domainSeedSpeciesInputById) {
        DomainSeedSpeciesInput speciesInputEntity;
        Optional<String> speciesId = domainSeedSpeciesInputDto.getTopiaId();
        if (speciesId.isEmpty()) {
            speciesInputEntity = domainSeedSpeciesInputDao.newInstance();
            speciesInputEntity.setCode(UUID.randomUUID().toString());
            seedLotInput.addDomainSeedSpeciesInput(speciesInputEntity);
        } else {
            speciesInputEntity = domainSeedSpeciesInputById.remove(speciesId.get());
        }
        return speciesInputEntity;
    }

    protected static List<DomainSeedSpeciesInputDto> removeSeedingNonValideProductPricesFromSeedSpeciesDtos(Collection<DomainSeedSpeciesInputDto> speciesInputsDto, boolean lotPriceIncludeTreatment) {
        List<DomainSeedSpeciesInputDto> speciesInputsDtos = new ArrayList<>();
        for (DomainSeedSpeciesInputDto domainSeedSpeciesInputDto : speciesInputsDto) {
            Collection<DomainPhytoProductInputDto> domainPhytoProductInputDtos = removeSeedingNonValideProductPricesFromDtos(domainSeedSpeciesInputDto.getSpeciesPhytoInputDtos(), lotPriceIncludeTreatment);
            speciesInputsDtos.add(domainSeedSpeciesInputDto.toBuilder().speciesPhytoInputDtos(domainPhytoProductInputDtos).build());
        }
        return speciesInputsDtos;
    }

    protected boolean isLotPriceIncludeTreatment(DomainSeedLotInputDto domainSeedLotInputDto) {
        boolean lotPriceIncludeTreatment = domainSeedLotInputDto.getPrice().isEmpty() ?
                CollectionUtils.emptyIfNull(domainSeedLotInputDto.getSpeciesInputs())
                        .stream()
                        .filter(dssi -> dssi.getSeedPrice().isPresent())
                        .map(dssi -> dssi.getSeedPrice().get().isIncludedTreatment())
                        .findAny()
                        .orElse(false) :
                domainSeedLotInputDto.getPrice().get()
                        .isIncludedTreatment();
        return lotPriceIncludeTreatment;
    }

    public void shallowBindDomainSeedSpeciesInputDtoToEntity(
            Domain domain,
            DomainSeedSpeciesInputDto domainSeedSpeciesInputDto,
            DomainSeedSpeciesInput speciesInputEntity,
            Map<String, CroppingPlanSpecies> speciesSeedsByIds,
            Boolean speciesUsed,
            @NonNull InputType inputType,
            @NonNull SeedPlantUnit usageUnit,
            Map<SeedType, String> seedTypeTranslations) {

        CroppingPlanSpecies cps = speciesSeedsByIds.get(domainSeedSpeciesInputDto.getSpeciesSeedDto().getTopiaId());
        speciesInputEntity.setInputType(inputType);

        final RefVariete variety = cps.getVariety();
        final RefEspece species = cps.getSpecies();

        Locale locale = getSecurityContext().getLocale();
        boolean biologicalSeedInoculation = domainSeedSpeciesInputDto.isBiologicalSeedInoculation();
        boolean chemicalTreatment = domainSeedSpeciesInputDto.isChemicalTreatment();
        SeedType seedType = domainSeedSpeciesInputDto.getSeedType();

        String speciesInputName = getDomainSeedSpeciesInputName(
                cps,
                seedType,
                seedTypeTranslations,
                biologicalSeedInoculation,
                chemicalTreatment,
                locale);

        String vLabel = variety != null ? variety.getLabel() : "";

        String lotSpeciesInputKey = DomainInputStockUnitService.getLotSpeciesInputKey(
                species.getCode_espece_botanique(),
                species.getCode_qualifiant_AEE(),
                biologicalSeedInoculation,
                chemicalTreatment,
                domainSeedSpeciesInputDto.isOrganic(),
                vLabel,
                usageUnit,
                seedType);

        DomainInputBinder.shallowBindDomainSeedSpeciesInputDtoToEntity(
                domain,
                domainSeedSpeciesInputDto,
                speciesInputEntity,
                cps,
                speciesUsed,
                speciesInputName,
                lotSpeciesInputKey,
                inputType
        );
    }

    @Override
    public String getDomainSeedSpeciesInputName(
            CroppingPlanSpecies cps,
            SeedType seedType,
            Map<SeedType, String> seedTypeTranslations,
            boolean biologicalSeedInoculation,
            boolean chemicalTreatment,
            Locale locale) {

        final RefVariete variety = cps.getVariety();
        final RefEspece species = cps.getSpecies();

        String speciesInputName = getLotSpeciesInputName(species, variety, seedType, seedTypeTranslations);
        List<String> suffixes = getCropSuffixes(biologicalSeedInoculation, chemicalTreatment, locale);

        if (!suffixes.isEmpty()) {
            speciesInputName += ", " + String.join(DELIMITER, suffixes);
        }

        return speciesInputName;
    }

    public static List<String> getCropSuffixes(
            boolean biologicalSeedInoculation,
            boolean chemicalTreatment,
            Locale locale) {

        List<String> suffixes = new ArrayList<>();

        if (biologicalSeedInoculation) {
            suffixes.add(l(locale, "domain-edit-inputs-semis-species-input-name-organic-treatment"));
        }
        if (chemicalTreatment) {
            suffixes.add(l(locale, "domain-edit-inputs-semis-species-input-name-chemical-treatment"));
        }
        if (!biologicalSeedInoculation && !chemicalTreatment) {
            suffixes.add(l(locale, "domain-edit-inputs-semis-species-input-name-no-treatment"));
        }
        return suffixes;
    }

    public static String getLotSpeciesInputName(
            RefEspece species,
            RefVariete variety,
            SeedType seedType,
            Map<SeedType, String> seedTypeTranslations) {

        String speciesDetails = computeDetails(species, variety);
        return species.getLibelle_espece_botanique_Translated() + " " + speciesDetails + " " + seedTypeTranslations.get(seedType);
    }

    @Override
    public String getDomainSeedLotName(
            CroppingPlanEntry croppingPlanEntry,
            SeedType seedType,
            Map<SeedType, String> seedTypeTranslations,
            boolean biologicalSeedInoculation,
            boolean chemicalTreatment,
            Locale locale) {

        List<String> names = new ArrayList<>();
        names.add(croppingPlanEntry.getName());

        CollectionUtils.emptyIfNull(croppingPlanEntry.getCroppingPlanSpecies()).forEach(
                cps -> {
                    final RefVariete variety = cps.getVariety();
                    final RefEspece species = cps.getSpecies();
                    names.add(getLotSpeciesInputName(species, variety, seedType, seedTypeTranslations));
                }
        );
        names.addAll(getCropSuffixes(biologicalSeedInoculation, chemicalTreatment, locale));
        String lotName = String.join(DELIMITER, names);

        return lotName;
    }

    @Override
    public Optional<DomainSeedLotInput> getLotForId(String lotId) {
        return lotId == null ? Optional.empty() : domainSeedLotInputDao.forTopiaIdEquals(lotId).stream().findAny();
    }

    @Override
    public Collection<SeedPlantUnit> getSeedPlanUnits(List<String> speciesIds) {
        List<SeedPlantUnit> seedUnitsForSpecies = null;
        // GRAINES_PAR_HA et UNITE_PAR_HA ne doivent pas être proposées
        ArrayList<SeedPlantUnit> defaultSeedPlanUnits = Lists.newArrayList(SeedPlantUnit.KG_PAR_HA,
                SeedPlantUnit.PLANTS_PAR_HA,
                SeedPlantUnit.PLANTS_PAR_M2,
                SeedPlantUnit.UNITE_PAR_HA,
                SeedPlantUnit.UNITE_PAR_M2);

        if (CollectionUtils.isNotEmpty(speciesIds)) {
            List<CroppingPlanSpecies> croppingPlanSpecies = croppingPlanSpeciesDao.forTopiaIdIn(speciesIds).findAll();
            seedUnitsForSpecies = referentialService.getSeedUnitsForSpecies(croppingPlanSpecies.stream().map(CroppingPlanSpecies::getSpecies).toList());
        }

        if (CollectionUtils.isEmpty(seedUnitsForSpecies)) {
            seedUnitsForSpecies = defaultSeedPlanUnits;
        }

        return seedUnitsForSpecies;
    }

    public static String computeDetails(RefEspece species, RefVariete variety) {
        List<String> details = new ArrayList<>();
        details.add(StringUtils.trimToNull(species.getLibelle_qualifiant_AEE_Translated()));
        details.add(StringUtils.trimToNull(species.getLibelle_type_saisonnier_AEE_Translated()));
        details.add(StringUtils.trimToNull(species.getLibelle_destination_AEE_Translated()));
        final String varietyName = variety != null ? " (" + variety.getLabel() + ")" : null;
        details.add(varietyName);
        String speciesName = StringUtils.trimToNull(details.stream().filter(Objects::nonNull).collect(Collectors.joining(", ")));
        speciesName = speciesName != null ? "(" + speciesName + ")" : "";
        return speciesName;
    }

    public static DomainSeedLotInputDto tryFindMatchingDomainSeedLotInput(
            DomainSeedLotInputDto fromDomainSeedLotInputDto,
            Collection<DomainInputDto> toDomainInputs) {

        DomainSeedLotInputDto toDomainSeedLotInputDto = null;

        List<DomainInputDto> toDomainSeedLotDtos = CollectionUtils.emptyIfNull(toDomainInputs)
                .stream()
                .filter(dsl -> InputType.SEMIS.equals(dsl.getInputType()) || InputType.PLAN_COMPAGNE.equals(dsl.getInputType()))
                .toList();

        MultiValuedMap<String, DomainInputDto> domainInputDtoByKeys = MultiMapUtils.newSetValuedHashMap();
        toDomainSeedLotDtos.forEach(dto -> domainInputDtoByKeys.put(dto.getKey(), dto));

        Collection<DomainInputDto> matchingDomainInputDtosByKey = domainInputDtoByKeys.get(fromDomainSeedLotInputDto.getKey());
        if (CollectionUtils.isNotEmpty(matchingDomainInputDtosByKey)) {

            Iterator<DomainInputDto> domainInputDtoIterator = matchingDomainInputDtosByKey.iterator();
            if (matchingDomainInputDtosByKey.size() == 1) {
                toDomainSeedLotInputDto = (DomainSeedLotInputDto) domainInputDtoIterator.next();
            } else {
                int lotScore;
                Map<Integer, DomainSeedLotInputDto> targetedLotDtoByScore = new HashMap<>();

                boolean bestScoreFound = false;
                while (!bestScoreFound && domainInputDtoIterator.hasNext()) {
                    DomainInputDto toDomainSeedLotDto = domainInputDtoIterator.next();
                    DomainSeedLotInputDto currentToDomainSeedLotInput = (DomainSeedLotInputDto) toDomainSeedLotDto;

                    final Collection<DomainSeedSpeciesInputDto> toDomainSeedSpeciesInputs = currentToDomainSeedLotInput.getSpeciesInputs();

                    List<DomainSeedSpeciesInputDto> fromSpeciesInputDtos = fromDomainSeedLotInputDto.getSpeciesInputs();

                    if (CollectionUtils.isEmpty(fromSpeciesInputDtos) && CollectionUtils.isEmpty(toDomainSeedSpeciesInputs)) {
                        bestScoreFound = true;
                        targetedLotDtoByScore.put(Integer.MAX_VALUE, currentToDomainSeedLotInput);
                    } else if (CollectionUtils.isNotEmpty(fromSpeciesInputDtos)) {
                        // look for the lot that best match the original one
                        AtomicInteger bestScore = computeBestDomainSeedSpeciesInputDtoScore(fromSpeciesInputDtos);
                        lotScore = computeScoreForLotDto(fromSpeciesInputDtos, getDomainSeedSpeciesInputDtoByKeys(toDomainSeedSpeciesInputs));
                        if (lotScore == bestScore.get()) {
                            bestScoreFound = true;
                        }
                        targetedLotDtoByScore.put(lotScore, currentToDomainSeedLotInput);
                    }
                }
                final Integer max = targetedLotDtoByScore.keySet().stream().max(Comparator.comparingInt(Integer::valueOf)).orElse(0);
                toDomainSeedLotInputDto = targetedLotDtoByScore.get(max);
            }
        }

        return toDomainSeedLotInputDto;
    }

    protected static int computeScoreForLotDto(
            List<DomainSeedSpeciesInputDto> fromSpeciesInputDtos,
            Map<String, DomainInputDto> toDomainSeedSpeciesInputByKeys) {

        int lotScore = 0;
        for (DomainSeedSpeciesInputDto fromSpeciesInputDto : fromSpeciesInputDtos) {
            final DomainSeedSpeciesInputDto toDomainSeedSpeciesInput = (DomainSeedSpeciesInputDto) toDomainSeedSpeciesInputByKeys.get(fromSpeciesInputDto.getKey());
            if (toDomainSeedSpeciesInput != null) {
                lotScore += 1;
                final Collection<DomainPhytoProductInputDto> toSpeciesPhytoInputs = new ArrayList<>(CollectionUtils.emptyIfNull(toDomainSeedSpeciesInput.getSpeciesPhytoInputDtos()));
                final Collection<DomainPhytoProductInputDto> speciesPhytoInputDtos = fromSpeciesInputDto.getSpeciesPhytoInputDtos();
                if (CollectionUtils.isEmpty(speciesPhytoInputDtos) && CollectionUtils.isEmpty(toSpeciesPhytoInputs)) {
                    lotScore += 1;
                } else if (CollectionUtils.isEmpty(speciesPhytoInputDtos)) {
                    for (DomainPhytoProductInputDto speciesPhytoInputDto : speciesPhytoInputDtos) {
                        if (toDomainSeedSpeciesInputByKeys.get(speciesPhytoInputDto.getKey()) != null) {
                            lotScore += 1;
                        }
                    }
                }
            }

        }
        return lotScore;
    }

    protected static Map<String, DomainInputDto> getDomainSeedSpeciesInputDtoByKeys(Collection<DomainSeedSpeciesInputDto> toDomainSeedSpeciesInputs) {
        final Map<String, DomainInputDto> toDomainSeedSpeciesInputByKeys = new HashMap<>();
        toDomainSeedSpeciesInputs.forEach(dssi -> {
            toDomainSeedSpeciesInputByKeys.put(dssi.getKey(), dssi);
            dssi.getSpeciesPhytoInputDtos().forEach(
                    dspi -> toDomainSeedSpeciesInputByKeys.put(dspi.getKey(), dspi));
        });
        return toDomainSeedSpeciesInputByKeys;
    }

    protected static AtomicInteger computeBestDomainSeedSpeciesInputDtoScore(List<DomainSeedSpeciesInputDto> fromSpeciesInputDtos) {
        AtomicInteger bestScore = new AtomicInteger();
        if (CollectionUtils.isNotEmpty(fromSpeciesInputDtos)) {
            fromSpeciesInputDtos.forEach(fsi -> {
                bestScore.addAndGet(1);
                CollectionUtils.emptyIfNull(
                                fsi.getSpeciesPhytoInputDtos()).
                        forEach(spi -> bestScore.addAndGet(1)
                        );
            });
        } else {
            bestScore.addAndGet(1);
        }
        return bestScore;
    }

    public static Map<Integer, DomainSeedLotInput> tryFindToDomainSeedLotInput(
            Map<String, List<AbstractDomainInputStockUnit>> toDomainInputStockUnitByKeys,
            DomainSeedLotInputDto fromDomainSeedLotInputDto) {

        final List<AbstractDomainInputStockUnit> toLotWithSameKey = ListUtils.emptyIfNull(toDomainInputStockUnitByKeys.get(fromDomainSeedLotInputDto.getKey()));
        final List<DomainSeedSpeciesInputDto> fromSpeciesInputDtos = fromDomainSeedLotInputDto.getSpeciesInputs();

        Map<Integer, DomainSeedLotInput> targetedLotByScore = new HashMap<>();
        DomainSeedLotInput toDomainSeedLotInput = null;
        Iterator<AbstractDomainInputStockUnit> toSeedLotIterator = toLotWithSameKey.iterator();
        int lotScore;
        while (toDomainSeedLotInput == null && toSeedLotIterator.hasNext()) {

            DomainSeedLotInput currentToDomainSeedLotInput = (DomainSeedLotInput) toSeedLotIterator.next();
            // if both the DomainSeedLotInput match by key and do not have species then they match together
            final Collection<DomainSeedSpeciesInput> toDomainSeedSpeciesInputs = currentToDomainSeedLotInput.getDomainSeedSpeciesInput();

            if (CollectionUtils.isEmpty(fromSpeciesInputDtos) && CollectionUtils.isEmpty(toDomainSeedSpeciesInputs)) {
                toDomainSeedLotInput = currentToDomainSeedLotInput;
                targetedLotByScore.put(Integer.MAX_VALUE, currentToDomainSeedLotInput);
            } else if (CollectionUtils.isNotEmpty(fromSpeciesInputDtos)) {
                // look for the lot that best match the original one
                AtomicInteger bestScore = computeBestScore(fromSpeciesInputDtos);
                lotScore = computeScoreForLot(fromSpeciesInputDtos, getDomainSeedSpeciesInputByKeys(toDomainSeedSpeciesInputs));
                if (lotScore == bestScore.get()) {
                    toDomainSeedLotInput = currentToDomainSeedLotInput;
                }
                targetedLotByScore.put(lotScore, currentToDomainSeedLotInput);
            }
        }
        return targetedLotByScore;
    }

    protected static int computeScoreForLot(
            List<DomainSeedSpeciesInputDto> fromSpeciesInputDtos,
            Map<String, AbstractDomainInputStockUnit> toDomainSeedSpeciesInputByKeys) {

        int lotScore = 0;
        for (DomainSeedSpeciesInputDto fromSpeciesInputDto : fromSpeciesInputDtos) {
            final DomainSeedSpeciesInput toDomainSeedSpeciesInput = (DomainSeedSpeciesInput) toDomainSeedSpeciesInputByKeys.get(fromSpeciesInputDto.getKey());
            if (toDomainSeedSpeciesInput != null) {
                lotScore += 1;
                final Collection<DomainPhytoProductInput> toSpeciesPhytoInputs = new ArrayList<>(CollectionUtils.emptyIfNull(toDomainSeedSpeciesInput.getSpeciesPhytoInputs()));
                final Collection<DomainPhytoProductInputDto> speciesPhytoInputDtos = fromSpeciesInputDto.getSpeciesPhytoInputDtos();
                if (CollectionUtils.isEmpty(speciesPhytoInputDtos) && CollectionUtils.isEmpty(toSpeciesPhytoInputs)) {
                    lotScore += 1;
                } else if (CollectionUtils.isEmpty(speciesPhytoInputDtos)) {
                    for (DomainPhytoProductInputDto speciesPhytoInputDto : speciesPhytoInputDtos) {
                        if (toDomainSeedSpeciesInputByKeys.get(speciesPhytoInputDto.getKey()) != null) {
                            lotScore += 1;
                        }
                    }
                }
            }

        }
        return lotScore;
    }

    public static AtomicInteger computeBestScore(List<DomainSeedSpeciesInputDto> fromSpeciesInputDtos) {
        AtomicInteger bestScore = new AtomicInteger();
        if (CollectionUtils.isNotEmpty(fromSpeciesInputDtos)) {
            fromSpeciesInputDtos.forEach(fsi -> {
                bestScore.addAndGet(1);
                CollectionUtils.emptyIfNull(fsi.getSpeciesPhytoInputDtos()).forEach(
                        spi -> bestScore.addAndGet(1)
                );
            });
        } else {
            bestScore.addAndGet(1);
        }
        return bestScore;
    }

    public static Map<String, AbstractDomainInputStockUnit> getDomainSeedSpeciesInputByKeys(Collection<DomainSeedSpeciesInput> toDomainSeedSpeciesInputs) {
        final Map<String, AbstractDomainInputStockUnit> toDomainSeedSpeciesInputByKeys = new HashMap<>();
        toDomainSeedSpeciesInputs.forEach(dssi -> {
            toDomainSeedSpeciesInputByKeys.put(dssi.getInputKey(), dssi);
            CollectionUtils.emptyIfNull(dssi.getSpeciesPhytoInputs()).forEach(
                    dspi -> toDomainSeedSpeciesInputByKeys.put(dspi.getInputKey(), dspi));
        });
        return toDomainSeedSpeciesInputByKeys;
    }

    protected static boolean isSpeciesNotUsed(Map<String, Boolean> inputUsageByIds, DomainSeedSpeciesInput speciesSeed) {
        return !inputUsageByIds.get(speciesSeed.getTopiaId());
    }

    public void createOrUpdateSpeciesPhytoProductInput(
            Domain domain,
            Collection<DomainPhytoProductInputDto> speciesPhytoInputDtos,
            DomainSeedSpeciesInput speciesInputEntity,
            DomainReferentialInputs domainInputReferential,
            Map<String, Boolean> seedingSpeciesAndProductsUsages,
            MultiValuedMap<DomainPhytoProductInput, InputUsageServiceImpl.SeedingProductUsageToRemoveForDomainSeedingProduct> usageToRemovesForDomainSeedProduct) {

        Collection<DomainPhytoProductInput> domainPhytoProductInputs = Lists.newArrayList(CollectionUtils.emptyIfNull(speciesInputEntity.getSpeciesPhytoInputs()));

        if (CollectionUtils.isNotEmpty(speciesPhytoInputDtos)) {

            // if user unselect biological inoculation, then remove it's corresponding products
            if (!speciesInputEntity.isBiologicalSeedInoculation()) {
                speciesPhytoInputDtos = new ArrayList<>(speciesPhytoInputDtos.stream()
                        .filter(spp -> !ProductType.SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION.equals(spp.getProductType()))
                        .toList());
            }
            // if user unselect chemical treatment, then remove it's corresponding products
            if (!speciesInputEntity.isChemicalTreatment()) {
                speciesPhytoInputDtos = new ArrayList<>(speciesPhytoInputDtos.stream()
                        .filter(spp -> ProductType.SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION.equals(spp.getProductType()))
                        .toList());
            }

            Map<String, DomainPhytoProductInput> domainPhytoProductInputByIds = new HashMap<>(Maps.uniqueIndex(domainPhytoProductInputs, TopiaEntity::getTopiaId));

            speciesPhytoInputDtos.forEach(speciesPhytoInputDto -> {
                DomainPhytoProductInput domainPhytoProductInput = getOrCreateDomainPhytoProductInput(
                        speciesInputEntity,
                        speciesPhytoInputDto,
                        domainPhytoProductInputByIds);

                if (seedingSpeciesAndProductsUsages.get(domainPhytoProductInput.getTopiaId()) == null ||
                        !seedingSpeciesAndProductsUsages.get(domainPhytoProductInput.getTopiaId())) {
                    DomainInputBinder.bindDtoToEntity(domain, speciesPhytoInputDto, domainPhytoProductInput, referentialService, domainInputReferential, false);
                }

                createUpdateOrDeletePrice(domain, speciesPhytoInputDto, domainPhytoProductInput);
            });

            final Collection<DomainPhytoProductInput> phytoProductInputsToRemove = domainPhytoProductInputByIds.values();
            removePhytoProductsIfNotUsed(speciesInputEntity, seedingSpeciesAndProductsUsages, phytoProductInputsToRemove, usageToRemovesForDomainSeedProduct);

        } else {
            removePhytoProductsIfNotUsed(speciesInputEntity, seedingSpeciesAndProductsUsages, domainPhytoProductInputs, usageToRemovesForDomainSeedProduct);
        }

        // make sure switch status is correct
        Collection<DomainPhytoProductInput> speciesPhytoInputs = CollectionUtils.emptyIfNull(speciesInputEntity.getSpeciesPhytoInputs());
        if (!speciesInputEntity.isBiologicalSeedInoculation()) {
            speciesInputEntity.setBiologicalSeedInoculation(speciesPhytoInputs.stream().anyMatch(pp -> ProductType.SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION.equals(pp.getProductType())));
        }
        if (!speciesInputEntity.isChemicalTreatment()) {
            speciesInputEntity.setChemicalTreatment(speciesPhytoInputs.stream().anyMatch(pp -> !ProductType.SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION.equals(pp.getProductType())));
        }
    }

    protected DomainPhytoProductInput getOrCreateDomainPhytoProductInput(
            DomainSeedSpeciesInput speciesInputEntity,
            DomainPhytoProductInputDto speciesPhytoInputDto,
            Map<String, DomainPhytoProductInput> domainPhytoProductInputByIds) {

        DomainPhytoProductInput domainPhytoProductInput;
        if (speciesPhytoInputDto.getTopiaId().isPresent()) {
            domainPhytoProductInput = domainPhytoProductInputByIds.remove(speciesPhytoInputDto.getTopiaId().get());
        } else {
            domainPhytoProductInput = domainPhytoProductInputDao.newInstance();
            domainPhytoProductInput.setCode(UUID.randomUUID().toString());
            speciesInputEntity.addSpeciesPhytoInputs(domainPhytoProductInput);
        }
        return domainPhytoProductInput;
    }

    protected void removePhytoProductsIfNotUsed(
            DomainSeedSpeciesInput speciesInputEntity,
            Map<String, Boolean> seedingSpeciesAndProductsUsages,
            Collection<DomainPhytoProductInput> domainPhytoProductInputs,
            MultiValuedMap<DomainPhytoProductInput, InputUsageServiceImpl.SeedingProductUsageToRemoveForDomainSeedingProduct> usageToRemovesForDomainSeddProduct) {

        if (CollectionUtils.isNotEmpty(domainPhytoProductInputs)) {
            for (DomainPhytoProductInput domainPhytoProductInputToRemove : domainPhytoProductInputs) {
                if (seedingSpeciesAndProductsUsages.get(domainPhytoProductInputToRemove.getTopiaId()) == null ||
                        !seedingSpeciesAndProductsUsages.get(domainPhytoProductInputToRemove.getTopiaId())) {
                    inputUsageService.removeSeedingProductUsageForDomainSeedingProduct(domainPhytoProductInputToRemove, usageToRemovesForDomainSeddProduct);
                    speciesInputEntity.removeSpeciesPhytoInputs(domainPhytoProductInputToRemove);
                }
            }
        }
    }

    /**
     * Ensure inputs from front does not contain gost prices and recreate DTO if so.
     * For seeding, it's product prices although the crop is declare as product price part of crop price
     */
    protected static Collection<DomainPhytoProductInputDto> removeSeedingNonValideProductPricesFromDtos(
            Collection<DomainPhytoProductInputDto> speciesPhytoInputDtos,
            boolean lotPriceIncludeTreatment) {

        if (lotPriceIncludeTreatment) {
            List<DomainPhytoProductInputDto> inputWithPricesToRemoves = CollectionUtils.emptyIfNull(speciesPhytoInputDtos).stream().filter(spi -> spi.getPrice().isPresent()).filter(spi -> spi.getPrice().get().getPrice() != null).toList();
            if (!inputWithPricesToRemoves.isEmpty()) {
                List<DomainPhytoProductInputDto> newSpeciesPhytoInputDtos = ListUtils.subtract(new ArrayList<>(speciesPhytoInputDtos), inputWithPricesToRemoves);
                for (DomainPhytoProductInputDto inputWithPricesToRemove : inputWithPricesToRemoves) {
                    newSpeciesPhytoInputDtos.add(inputWithPricesToRemove.toBuilder().price(null).build());
                }
                speciesPhytoInputDtos = newSpeciesPhytoInputDtos;
            }
        }
        return speciesPhytoInputDtos;
    }

    protected void removeSeedingSpeciesInputs(Map<String, Boolean> inputUsage, DomainSeedLotInput seedLotInput) {
        final Collection<DomainSeedSpeciesInput> domainSeedSpeciesInputs = seedLotInput.getDomainSeedSpeciesInput();
        if (CollectionUtils.isNotEmpty(domainSeedSpeciesInputs)) {
            // remove not used species
            final ArrayList<DomainSeedSpeciesInput> copiedDomainSeedSpeciesInputs = new ArrayList<>(domainSeedSpeciesInputs);
            for (DomainSeedSpeciesInput domainSeedSpeciesInput : copiedDomainSeedSpeciesInputs) {
                if (isSpeciesNotUsed(inputUsage, domainSeedSpeciesInput)) {
                    seedLotInput.removeDomainSeedSpeciesInput(domainSeedSpeciesInput);
                }
            }

        }
    }

    protected void bindSpeciesSeedPrice(
            Domain domain,
            DomainSeedSpeciesInputDto domainSeedSpeciesInputDto,
            DomainSeedSpeciesInput speciesInputEntity) {

        Optional<InputPriceDto> optionalSpeciesPriceDto = domainSeedSpeciesInputDto.getSeedPrice();
        SeedPrice speciesPrice = null;
        if (optionalSpeciesPriceDto.isPresent() && optionalSpeciesPriceDto.get().getPrice() != null) {

            InputPriceDto seedPriceDto = optionalSpeciesPriceDto.get();
            seedPriceDto = seedPriceDto.toBuilder()
                    .biologicalSeedInoculation(domainSeedSpeciesInputDto.isBiologicalSeedInoculation())
                    .chemicalTreatment(domainSeedSpeciesInputDto.isChemicalTreatment())
                    .organic(domainSeedSpeciesInputDto.isOrganic())
                    .seedType(domainSeedSpeciesInputDto.getSeedType())
                    .build();

            final SeedPrice seedPrice = speciesInputEntity.getSeedPrice();
            final String objectId = InputPriceService.GET_SPECIES_DTO_SEED_OBJECT_ID.apply(speciesInputEntity.getSpeciesSeed(), seedPriceDto);
            speciesPrice = persistSeedPrice(
                    seedPrice,
                    domain,
                    seedPriceDto,
                    objectId);
            speciesInputEntity.setSeedPrice(speciesPrice);
        } else if (speciesInputEntity.getSeedPrice() != null) {
            speciesInputEntity.setSeedPrice(null);
            //inputPriceService.deleteInputPrice(speciesInputEntity.getSeedPrice());
        }
        speciesInputEntity.setSeedPrice(speciesPrice);
    }

    protected void createOrUpdateSeedLotPrice(
            Domain domain,
            DomainSeedLotInputDto domainSeedLotInputDto,
            DomainSeedLotInput seedLotInputEntity) {

        // if price is declare by species, there must not be price for lot
        boolean priceBySpecies = CollectionUtils.emptyIfNull(domainSeedLotInputDto.getSpeciesInputs())
                .stream()
                .filter(dss -> dss.getSeedPrice().isPresent())
                .map(p -> p.getSeedPrice().get()).anyMatch(p -> p.getPrice() != null);

        Optional<InputPriceDto> optionalLotPriceDto = priceBySpecies ? Optional.empty() : domainSeedLotInputDto.getPrice();
        SeedPrice lotPrice = null;
        if (optionalLotPriceDto.isPresent() && optionalLotPriceDto.get().getPrice() != null) {
            InputPriceDto inputPriceDto = optionalLotPriceDto.get();
            String objectId = InputPriceService.GET_CROP_SEED_OBJECT_ID.apply(seedLotInputEntity.getCropSeed(), domainSeedLotInputDto.isOrganic());
            lotPrice = persistSeedPrice(
                    seedLotInputEntity.getSeedPrice(),
                    domain,
                    inputPriceDto,
                    objectId);
        } else if (seedLotInputEntity.isPersisted() && seedLotInputEntity.getSeedPrice() != null) {
            inputPriceService.deleteInputPrice(seedLotInputEntity.getSeedPrice());
        }
        seedLotInputEntity.setSeedPrice(lotPrice);

    }

    private SeedPrice persistSeedPrice(
            SeedPrice seedPriceToSave,
            Domain domain,
            InputPriceDto inputPriceDto,
            String objectId) {

        SeedPrice perssitedSeedPrice = null;
        Optional<SeedPrice> optionalSeedPrice = bindSeedPrice(domain, seedPriceToSave, inputPriceDto, objectId);
        if (optionalSeedPrice.isPresent() && optionalSeedPrice.get().getPrice() != null) {
            perssitedSeedPrice = (SeedPrice) inputPriceService.persistInputPrice(optionalSeedPrice.get());
        } else if (seedPriceToSave != null && seedPriceToSave.isPersisted()) {
            inputPriceService.deleteInputPrice(seedPriceToSave);
        }
        return perssitedSeedPrice;
    }

    private Optional<SeedPrice> bindSeedPrice(
            Domain domain,
            SeedPrice seedPrice,
            InputPriceDto priceDto,
            String priceObjectId) {

        if (priceDto != null) {
            seedPrice = seedPrice == null ? (SeedPrice) inputPriceService.getNewPriceInstance(priceDto.getCategory()) : seedPrice;
            inputPriceService.bindDtoToPrice(domain, priceDto, seedPrice, priceObjectId, Optional.empty(), Optional.empty());
        }
        return Optional.of(seedPrice);
    }

    protected static class SeedLotComparable {
        final boolean organic;
        final Set<SeedSpeciesComparable> seedSpeciesComparables;
        final CroppingPlanEntryComparable cropComparable;

        public SeedLotComparable(DomainSeedLotInput seedLotInput) {
            organic = seedLotInput.isOrganic();

            final Collection<DomainSeedSpeciesInput> domainSeedSpeciesInputs = CollectionUtils.emptyIfNull(seedLotInput.getDomainSeedSpeciesInput());
            seedSpeciesComparables = domainSeedSpeciesInputs.stream().map(SeedSpeciesComparable::new).collect(Collectors.toSet());
            final CroppingPlanEntry cropSeed = seedLotInput.getCropSeed();
            cropComparable = new CroppingPlanEntryComparable(cropSeed);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SeedLotComparable that = (SeedLotComparable) o;
            return organic == that.organic &&
                    Objects.equals(seedSpeciesComparables, that.seedSpeciesComparables) &&
                    Objects.equals(cropComparable, that.cropComparable);
        }

        @Override
        public int hashCode() {
            return Objects.hash(organic, seedSpeciesComparables, cropComparable);
        }

        @Override
        public String toString() {
            return "SeedLotComparable{" +
                    "organic=" + organic +
                    ", seedSpeciesComparables=" + seedSpeciesComparables +
                    ", cropComparables=" + cropComparable +
                    '}';
        }
    }

    protected static class CroppingPlanEntryComparable {
        final boolean mixSpecies;
        final boolean mixVariety;
        final Collection<SpeciesComparable> speciesComparables;

        public CroppingPlanEntryComparable(CroppingPlanEntry crop) {
            mixSpecies = crop.isMixSpecies();
            mixVariety = crop.isMixVariety();

            final Collection<CroppingPlanSpecies> croppingPlanSpecies = CollectionUtils.emptyIfNull(crop.getCroppingPlanSpecies());
            speciesComparables = new ArrayList<>();
            croppingPlanSpecies.forEach(cps -> speciesComparables.add(new SpeciesComparable(cps)));
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CroppingPlanEntryComparable that = (CroppingPlanEntryComparable) o;
            return mixSpecies == that.mixSpecies &&
                    mixVariety == that.mixVariety &&
                    speciesComparables.equals(that.speciesComparables);
        }

        @Override
        public int hashCode() {
            return Objects.hash(mixSpecies, mixVariety, speciesComparables);
        }

        @Override
        public String toString() {
            return "CropComparable{" +
                    "mixSpecies=" + mixSpecies +
                    ", mixVariety=" + mixVariety +
                    ", speciesComparables=" + speciesComparables +
                    '}';
        }
    }

    protected static class RefEspeceComparable {
        final String code_espece_botanique;
        final String code_qualifiant_AEE;
        final String code_type_saisonnier_AEE;
        final String code_destination_AEE;

        public RefEspeceComparable(RefEspece refEspece) {
            code_espece_botanique = refEspece.getCode_espece_botanique();
            code_qualifiant_AEE = refEspece.getCode_qualifiant_AEE();
            code_type_saisonnier_AEE = refEspece.getCode_type_saisonnier_AEE();
            code_destination_AEE = refEspece.getCode_destination_AEE();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            RefEspeceComparable that = (RefEspeceComparable) o;
            return code_espece_botanique.equals(that.code_espece_botanique) &&
                    code_qualifiant_AEE.equals(that.code_qualifiant_AEE) &&
                    code_type_saisonnier_AEE.equals(that.code_type_saisonnier_AEE) &&
                    code_destination_AEE.equals(that.code_destination_AEE);
        }

        @Override
        public int hashCode() {
            return Objects.hash(code_espece_botanique, code_qualifiant_AEE, code_type_saisonnier_AEE, code_destination_AEE);
        }

        @Override
        public String toString() {
            return "RefEspeceComparable{" +
                    "code_espece_botanique='" + code_espece_botanique + '\'' +
                    ", code_qualifiant_AEE='" + code_qualifiant_AEE + '\'' +
                    ", code_type_saisonnier_AEE='" + code_type_saisonnier_AEE + '\'' +
                    ", code_destination_AEE='" + code_destination_AEE + '\'' +
                    '}';
        }
    }

    protected static class RefVarieteComparable {
        final String label;

        public RefVarieteComparable(RefVariete refVariete) {
            label = refVariete != null ? refVariete.getLabel() : null;
        }

        @Override
        public final boolean equals(Object o) {
            if (!(o instanceof RefVarieteComparable that)) return false;

            return Objects.equals(label, that.label);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(label);
        }

        @Override
        public String toString() {
            return "RefVarieteComparable{" +
                    "label='" + Strings.nullToEmpty(label) +
                    '}';
        }
    }

    protected static class SpeciesComparable {

        final CompagneType compagne;

        final RefEspeceComparable refEspeceComparable;
        final RefVarieteComparable refVarieteComparable;

        public SpeciesComparable(CroppingPlanSpecies croppingPlanSpecies) {
            compagne = croppingPlanSpecies.getCompagne();
            final RefEspece refEspece = croppingPlanSpecies.getSpecies();
            refEspeceComparable = new RefEspeceComparable(refEspece);
            refVarieteComparable = new RefVarieteComparable(croppingPlanSpecies.getVariety());
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SpeciesComparable that = (SpeciesComparable) o;
            return compagne == that.compagne &&
                    refEspeceComparable.equals(that.refEspeceComparable) &&
                    refVarieteComparable.equals(that.refVarieteComparable);
        }

        @Override
        public int hashCode() {
            return Objects.hash(compagne, refEspeceComparable);
        }

        @Override
        public String toString() {
            return "SpeciesComparable{" +
                    "compagne=" + compagne +
                    ", refEspeceComparable=" + refEspeceComparable +
                    '}';
        }
    }

    protected static class SeedSpeciesComparable {
        final boolean biologicalSeedInoculation;
        final boolean chemicalTreatment;
        final boolean organic;
        final RefEspeceComparable refEspeceComparable;
        final Set<String> refActaTraitementProductId;

        public SeedSpeciesComparable(DomainSeedSpeciesInput domainSeedSpeciesInput) {
            biologicalSeedInoculation = domainSeedSpeciesInput.isBiologicalSeedInoculation();
            chemicalTreatment = domainSeedSpeciesInput.isChemicalTreatment();
            organic = domainSeedSpeciesInput.isOrganic();
            refEspeceComparable = new RefEspeceComparable(domainSeedSpeciesInput.getSpeciesSeed().getSpecies());
            final Collection<DomainPhytoProductInput> speciesPhytoInputs = CollectionUtils.emptyIfNull(domainSeedSpeciesInput.getSpeciesPhytoInputs());
            refActaTraitementProductId = speciesPhytoInputs.stream().map(spi -> spi.getRefInput().getTopiaId()).collect(Collectors.toSet());
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SeedSpeciesComparable that = (SeedSpeciesComparable) o;
            return biologicalSeedInoculation == that.biologicalSeedInoculation &&
                    chemicalTreatment == that.chemicalTreatment &&
                    organic == that.organic &&
                    refEspeceComparable.equals(that.refEspeceComparable) &&
                    Objects.equals(refActaTraitementProductId, that.refActaTraitementProductId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(biologicalSeedInoculation, chemicalTreatment, organic, refEspeceComparable, refActaTraitementProductId);
        }

        @Override
        public String toString() {
            return "SeedSpeciesComparable{" +
                    "biologicalSeedInoculation=" + biologicalSeedInoculation +
                    ", chemicalTreatment=" + chemicalTreatment +
                    ", organic=" + organic +
                    ", refEspeceComparable=" + refEspeceComparable +
                    ", refActaTraitementProductId=" + refActaTraitementProductId +
                    '}';
        }
    }

}
