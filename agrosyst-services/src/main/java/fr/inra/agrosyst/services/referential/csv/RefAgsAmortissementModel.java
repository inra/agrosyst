package fr.inra.agrosyst.services.referential.csv;

import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement;
import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissementImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;


/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * @author David Cossé
 */
public class RefAgsAmortissementModel  extends AbstractAgrosystModel<RefAgsAmortissement> implements ExportModel<RefAgsAmortissement> {
    
    public RefAgsAmortissementModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("Type_Materiel", RefAgsAmortissement.PROPERTY_TYPE__MATERIEL, MATERIAL_TYPE_PARSER);
        newMandatoryColumn("DonneesAmortissement1", RefAgsAmortissement.PROPERTY_DONNEES_AMORTISSEMENT1, DOUBLE_PARSER);
        newMandatoryColumn("DonneesAmortissement2", RefAgsAmortissement.PROPERTY_DONNEES_AMORTISSEMENT2, DOUBLE_PARSER);
        newMandatoryColumn("TauxGlobal", RefAgsAmortissement.PROPERTY_TAUX_GLOBAL, DOUBLE_PARSER);
        newMandatoryColumn("Commentaire", RefAgsAmortissement.PROPERTY_COMMENTAIRE);
        newOptionalColumn("Actif", RefAgsAmortissement.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }
    
    @Override
    public Iterable<ExportableColumn<RefAgsAmortissement, Object>> getColumnsForExport() {
        ModelBuilder<RefAgsAmortissement> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Type_Materiel", RefAgsAmortissement.PROPERTY_TYPE__MATERIEL, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("DonneesAmortissement1", RefAgsAmortissement.PROPERTY_DONNEES_AMORTISSEMENT1, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("DonneesAmortissement2", RefAgsAmortissement.PROPERTY_DONNEES_AMORTISSEMENT2, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("TauxGlobal", RefAgsAmortissement.PROPERTY_TAUX_GLOBAL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Commentaire", RefAgsAmortissement.PROPERTY_COMMENTAIRE);
        modelBuilder.newColumnForExport("Actif", RefAgsAmortissement.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefAgsAmortissement newEmptyInstance() {
        RefAgsAmortissement entity = new RefAgsAmortissementImpl();
        entity.setActive(true);
        return entity;
    }
}
