package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefGesSemence;
import fr.inra.agrosyst.api.entities.referential.RefGesSemenceImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * code culture;libelle culture;GES (kq eq-CO2/kg);source
 * 
 * @author Eric Chatellier
 */
public class RefGesSemenceModel extends AbstractAgrosystModel<RefGesSemence> implements ExportModel<RefGesSemence> {

    public RefGesSemenceModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("code culture", RefGesSemence.PROPERTY_CODE_CULTURE);
        newMandatoryColumn("libelle culture", RefGesSemence.PROPERTY_LIBELLE_CULTURE);
        newMandatoryColumn("GES (kq eq-CO2/kg)", RefGesSemence.PROPERTY_GES, DOUBLE_PARSER);
        newMandatoryColumn("source", RefGesSemence.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefGesSemence.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefGesSemence, Object>> getColumnsForExport() {
        ModelBuilder<RefGesSemence> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Intitulé", RefGesSemence.PROPERTY_CODE_CULTURE);
        modelBuilder.newColumnForExport("libelle culture", RefGesSemence.PROPERTY_LIBELLE_CULTURE);
        modelBuilder.newColumnForExport("GES (kq eq-CO2/kg)", RefGesSemence.PROPERTY_GES, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("source", RefGesSemence.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefGesSemence.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefGesSemence newEmptyInstance() {
        RefGesSemence refGesSemence = new RefGesSemenceImpl();
        refGesSemence.setActive(true);
        return refGesSemence;
    }
}
