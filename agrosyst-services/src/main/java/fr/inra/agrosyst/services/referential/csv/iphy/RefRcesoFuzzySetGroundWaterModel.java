package fr.inra.agrosyst.services.referential.csv.iphy;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesoFuzzySetGroundWater;
import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesoFuzzySetGroundWaterImpl;
import fr.inra.agrosyst.services.referential.csv.AbstractAgrosystModel;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * CaseNumber;Variables;FussySet;sigma;c.
 * 
 * @author Eric Chatellier
 */
public class RefRcesoFuzzySetGroundWaterModel extends AbstractAgrosystModel<RefRcesoFuzzySetGroundWater> implements ExportModel<RefRcesoFuzzySetGroundWater> {

    public RefRcesoFuzzySetGroundWaterModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("CaseNumber", RefRcesoFuzzySetGroundWater.PROPERTY_CASE_NUMBER, INT_PARSER);
        newMandatoryColumn("Variables", RefRcesoFuzzySetGroundWater.PROPERTY_VARIABLES);
        newMandatoryColumn("FussySet", RefRcesoFuzzySetGroundWater.PROPERTY_FUZZY_SET);
        newMandatoryColumn("sigma", RefRcesoFuzzySetGroundWater.PROPERTY_SIGMA, DOUBLE_PARSER);
        newMandatoryColumn("c", RefRcesoFuzzySetGroundWater.PROPERTY_C, DOUBLE_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefRcesoFuzzySetGroundWater.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public RefRcesoFuzzySetGroundWater newEmptyInstance() {
        RefRcesoFuzzySetGroundWater result = new RefRcesoFuzzySetGroundWaterImpl();
        result.setActive(true);
        return result;
    }

    @Override
    public Iterable<ExportableColumn<RefRcesoFuzzySetGroundWater, Object>> getColumnsForExport() {
        ModelBuilder<RefRcesoFuzzySetGroundWater> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("CaseNumber", RefRcesoFuzzySetGroundWater.PROPERTY_CASE_NUMBER, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Variables", RefRcesoFuzzySetGroundWater.PROPERTY_VARIABLES);
        modelBuilder.newColumnForExport("FussySet", RefRcesoFuzzySetGroundWater.PROPERTY_FUZZY_SET);
        modelBuilder.newColumnForExport("sigma", RefRcesoFuzzySetGroundWater.PROPERTY_SIGMA, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("c", RefRcesoFuzzySetGroundWater.PROPERTY_C, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefRcesoFuzzySetGroundWater.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
}
