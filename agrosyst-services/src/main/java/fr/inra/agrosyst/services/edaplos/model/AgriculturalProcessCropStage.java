package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public class AgriculturalProcessCropStage implements AgroEdiObject {

    protected String typeCode;

    protected String startCropStage;

    protected String endCropStage;

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getStartCropStage() {
        return startCropStage;
    }

    public void setStartCropStage(String startCropStage) {
        this.startCropStage = startCropStage;
    }

    public String getEndCropStage() {
        return endCropStage;
    }

    public void setEndCropStage(String endCropStage) {
        this.endCropStage = endCropStage;
    }

    @Override
    public String getLocalizedIdentifier() {
        return "stade '" + typeCode + "'";
    }
}
