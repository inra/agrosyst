package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * {@code
 * <ram:AgriculturalArea>
 * <ram:TypeCode>A17</ram:TypeCode>
 * <ram:Description>Surface de la parcelle culturale</ram:Description>
 * <ram:ActualMeasure unitCode="HAR">14.79</ram:ActualMeasure>
 * </ram:AgriculturalArea>
 * }
 * </pre>
 */
public class AgriculturalArea implements AgroEdiObject {

    /** Surface de la parcelle culturale. */
    public static final String TYPE_SURFACE_PARCELLE_CULTURALE = "A17";

    /** Surface de la parcelle pérenne. */
    public static final String TYPE_SURFACE_PARCELLE_PERENNE = "ABE05";

    public static final String TYPE_SURFACE_TRAVAILLEE = "ABE02";

    protected String typeCode;

    protected String description;

    protected String actualMeasure;

    protected String spatialFrequency;

    protected List<SpecifiedGeographicalCoordinate> specifiedGeographicalCoordinates = new ArrayList<>();

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getActualMeasure() {
        return actualMeasure;
    }

    public void setActualMeasure(String actualMeasure) {
        this.actualMeasure = actualMeasure;
    }

    public String getSpatialFrequency() {
        return spatialFrequency;
    }

    public void setSpatialFrequency(String spatialFrequency) {
        this.spatialFrequency = spatialFrequency;
    }

    public List<SpecifiedGeographicalCoordinate> getSpecifiedGeographicalCoordinates() {
        return specifiedGeographicalCoordinates;
    }

    public void setSpecifiedGeographicalCoordinates(List<SpecifiedGeographicalCoordinate> specifiedGeographicalCoordinates) {
        this.specifiedGeographicalCoordinates = specifiedGeographicalCoordinates;
    }

    public void addSpecifiedGeographicalCoordinate(SpecifiedGeographicalCoordinate specifiedGeographicalCoordinate) {
        specifiedGeographicalCoordinates.add(specifiedGeographicalCoordinate);
    }

    @Override
    public String getLocalizedIdentifier() {
        return "surface '" + typeCode + "'";
    }
}
