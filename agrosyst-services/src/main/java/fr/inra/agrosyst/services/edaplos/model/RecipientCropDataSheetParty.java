package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2017 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public class RecipientCropDataSheetParty implements AgroEdiObject {

    protected String identification;

    protected String name;

    protected String description;

    protected PartyContact definedPartyContact;

    protected UnstructuredAddress specifiedUnstructuredAddress;

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PartyContact getDefinedPartyContact() {
        return definedPartyContact;
    }

    public void setDefinedPartyContact(PartyContact definedPartyContact) {
        this.definedPartyContact = definedPartyContact;
    }

    public UnstructuredAddress getSpecifiedUnstructuredAddress() {
        return specifiedUnstructuredAddress;
    }

    public void setSpecifiedUnstructuredAddress(UnstructuredAddress specifiedUnstructuredAddress) {
        this.specifiedUnstructuredAddress = specifiedUnstructuredAddress;
    }

    @Override
    public String getLocalizedIdentifier() {
        return "destinataire '" + identification + "'";
    }
}
