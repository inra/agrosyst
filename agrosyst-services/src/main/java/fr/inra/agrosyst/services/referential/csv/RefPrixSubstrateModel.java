package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefPrixSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefPrixSubstrateImpl;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefPrixSubstrateModel extends AbstractDestinationAndPriceModel<RefPrixSubstrate> implements ExportModel<RefPrixSubstrate> {

    public RefPrixSubstrateModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("Prix", RefPrixSubstrate.PROPERTY_PRICE, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("Unité", RefPrixSubstrate.PROPERTY_UNIT, PRICE_UNIT_PARSER);
        newMandatoryColumn("Code scénario", RefPrixSubstrate.PROPERTY_CODE_SCENARIO);
        newMandatoryColumn("Scénario", RefPrixSubstrate.PROPERTY_SCENARIO);
        newMandatoryColumn("Campagne", RefPrixSubstrate.PROPERTY_CAMPAIGN, INTEGER_WITH_NULL_PARSER);
        newOptionalColumn("Source", RefPrixSubstrate.PROPERTY_SOURCE);
        newMandatoryColumn("Caracteristique 1", RefPrixSubstrate.PROPERTY_CARACTERISTIC1);
        newMandatoryColumn("Caracteristique 2", RefPrixSubstrate.PROPERTY_CARACTERISTIC2);
        newOptionalColumn(COLUMN_ACTIVE, RefPrixSubstrate.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefPrixSubstrate, Object>> getColumnsForExport() {
        ModelBuilder<RefSubstrate> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Prix", RefPrixSubstrate.PROPERTY_PRICE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Unité", RefPrixSubstrate.PROPERTY_UNIT, PRICE_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("Code scénario", RefPrixSubstrate.PROPERTY_CODE_SCENARIO);
        modelBuilder.newColumnForExport("Scénario", RefPrixSubstrate.PROPERTY_SCENARIO);
        modelBuilder.newColumnForExport("Campagne", RefPrixSubstrate.PROPERTY_CAMPAIGN, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefPrixSubstrate.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport("Caracteristique 1", RefPrixSubstrate.PROPERTY_CARACTERISTIC1);
        modelBuilder.newColumnForExport("Caracteristique 2", RefPrixSubstrate.PROPERTY_CARACTERISTIC2);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefPrixSubstrate.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefPrixSubstrate newEmptyInstance() {
        RefPrixSubstrate entity = new RefPrixSubstrateImpl();
        entity.setActive(true);
        return entity;
    }
}
