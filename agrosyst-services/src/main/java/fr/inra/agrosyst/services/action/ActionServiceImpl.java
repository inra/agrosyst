package fr.inra.agrosyst.services.action;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainIrrigationInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainMineralProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainOtherInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainPotInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInputTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.LivestockUnit;
import fr.inra.agrosyst.api.entities.LivestockUnitTopiaDao;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.BiologicalControlActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.CarriageAction;
import fr.inra.agrosyst.api.entities.action.CarriageActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisationTopiaDao;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.IrrigationActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.IrrigationInputUsage;
import fr.inra.agrosyst.api.entities.action.MaintenancePruningVinesAction;
import fr.inra.agrosyst.api.entities.action.MaintenancePruningVinesActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherAction;
import fr.inra.agrosyst.api.entities.action.OtherActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.PotInputUsage;
import fr.inra.agrosyst.api.entities.action.QualityCriteria;
import fr.inra.agrosyst.api.entities.action.QualityCriteriaTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SubstrateInputUsage;
import fr.inra.agrosyst.api.entities.action.TillageAction;
import fr.inra.agrosyst.api.entities.action.TillageActionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.QualityAttributeType;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClass;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSectorTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntityTranslation;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.entities.referential.WineValorisation;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.action.ActionService;
import fr.inra.agrosyst.api.services.action.BiologicalControlActionDto;
import fr.inra.agrosyst.api.services.action.CarriageActionDto;
import fr.inra.agrosyst.api.services.action.HarvestingActionDto;
import fr.inra.agrosyst.api.services.action.HarvestingActionValorisationDto;
import fr.inra.agrosyst.api.services.action.HarvestingPriceDto;
import fr.inra.agrosyst.api.services.action.IrrigationActionDto;
import fr.inra.agrosyst.api.services.action.MaintenancePruningVinesActionDto;
import fr.inra.agrosyst.api.services.action.MineralFertilizersSpreadingActionDto;
import fr.inra.agrosyst.api.services.action.OrganicFertilizersSpreadingActionDto;
import fr.inra.agrosyst.api.services.action.OtherActionDto;
import fr.inra.agrosyst.api.services.action.PesticidesSpreadingActionDto;
import fr.inra.agrosyst.api.services.action.QualityCriteriaDto;
import fr.inra.agrosyst.api.services.action.SeedingActionUsageDto;
import fr.inra.agrosyst.api.services.action.TillageActionDto;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlans;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedLotInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedSpeciesInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSubstrateInputDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.input.IrrigationInputUsageDto;
import fr.inra.agrosyst.api.services.input.MineralProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.OrganicProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.OtherProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.PhytoProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.PotInputUsageDto;
import fr.inra.agrosyst.api.services.input.SeedLotInputUsageDto;
import fr.inra.agrosyst.api.services.input.SeedSpeciesInputUsageDto;
import fr.inra.agrosyst.api.services.input.SubstrateInputUsageDto;
import fr.inra.agrosyst.api.services.itk.SpeciesStadeDto;
import fr.inra.agrosyst.api.services.practiced.PracticedDuplicateCropCyclesContext;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.domain.inputStock.DomainInputBinder;
import fr.inra.agrosyst.services.domain.inputStock.DomainInputStockUnitServiceImpl;
import fr.inra.agrosyst.services.input.InputPersistanceResults;
import fr.inra.agrosyst.services.input.InputUsageServiceImpl;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author David Cossé
 */
@Setter
public class ActionServiceImpl extends AbstractAgrosystService implements ActionService {

    protected static final Log LOGGER = LogFactory.getLog(ActionServiceImpl.class);
    public static final String PASTURE = "W25";// pâturage

    // lié aux listes TYPES_ALLOWING_3_ACTIONS dans le front (itk.js et domain-tools-coupling-edition.js)
    public static final List<AgrosystInterventionType> TYPES_ALLOWING_3_ACTIONS = List.of(
            AgrosystInterventionType.ENTRETIEN_TAILLE_VIGNE_ET_VERGER,
            AgrosystInterventionType.AUTRE,
            AgrosystInterventionType.TRAVAIL_DU_SOL
    );

    protected AgrosystI18nService i18nService;
    protected InputUsageServiceImpl inputUsageService;
    protected InputPriceService inputPriceService;
    protected ReferentialService referentialService;
    protected PricesService priceService;

    protected AbstractActionTopiaDao abstractActionDao;
    protected BiologicalControlActionTopiaDao biologicalControlActionDao;
    protected CarriageActionTopiaDao carriageActionDao;
    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    protected EffectiveCropCycleConnectionTopiaDao effectiveCropCycleConnectionDao;
    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDao;
    protected HarvestingActionTopiaDao harvestingActionDao;
    protected HarvestingActionValorisationTopiaDao harvestingActionValorisationDao;
    protected IrrigationActionTopiaDao irrigationActionDao;
    protected LivestockUnitTopiaDao livestockUnitDao;
    protected MaintenancePruningVinesActionTopiaDao maintenancePruningVinesActionDao;
    protected MineralFertilizersSpreadingActionTopiaDao mineralFertilizersSpreadingActionDao;
    protected OrganicFertilizersSpreadingActionTopiaDao organicFertilizersSpreadingActionDao;
    protected OtherActionTopiaDao otherActionDao;
    protected PesticidesSpreadingActionTopiaDao pesticidesSpreadingActionDao;
    protected QualityCriteriaTopiaDao qualityCriteriaDao;
    protected RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEdiDao;
    protected RefDestinationTopiaDao refDestinationDao;
    protected SeedingActionUsageTopiaDao seedingActionUsageDao;
    protected TillageActionTopiaDao tillageActionDao;

    // from domain input
    protected DomainPhytoProductInputTopiaDao domainPhytoProductInputDao;
    protected DomainIrrigationInputTopiaDao domainIrrigationInputDao;
    protected DomainMineralProductInputTopiaDao domainMineralProductInputDao;
    protected DomainOrganicProductInputTopiaDao domainOrganicProductInputDao;
    protected DomainOtherInputTopiaDao domainOtherInputDao;
    protected DomainPotInputTopiaDao domainPotInputDao;
    protected DomainSeedLotInputTopiaDao domainSeedLotInputDao;
    protected DomainSubstrateInputTopiaDao domainSubstrateInputDao;

    public static int getEffectiveDecade(LocalDate localDate) {
        int day = localDate.getDayOfMonth();
        return getDecade(day);
    }

    protected static int getDecade(int day) {
        final int result;
        if (day < 11) {
            result = 1;
        } else if (day < 21) {
            result = 2;
        } else {
            result = 3;
        }
        return result;
    }

    @Override
    public Map<HarvestingActionValorisation, String> getHarvestingActionValorisationsActions(Collection<HarvestingActionValorisation> valorisations) {
        Map<HarvestingActionValorisation, String> valorisationToActionId;
        if (CollectionUtils.isNotEmpty(valorisations)) {
            valorisationToActionId = harvestingActionDao.findValorisationToActionId(valorisations);
        } else {
            valorisationToActionId = new HashMap<>();
        }
        return valorisationToActionId;
    }

    @Override
    public void removeActionsAndUsagesDeps(Collection<AbstractAction> actionsToRemove) {
        for (AbstractAction actionToRemove : actionsToRemove) {
            AgrosystInterventionType agrosystInterventionType = actionToRemove.getMainAction().getIntervention_agrosyst();
            switch (agrosystInterventionType) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                    ((MineralFertilizersSpreadingAction) actionToRemove).clearMineralProductInputUsages();
                    ((MineralFertilizersSpreadingAction) actionToRemove).clearOtherProductInputUsages();
                }
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES -> {
                    ((PesticidesSpreadingAction) actionToRemove).clearPesticideProductInputUsages();
                    ((PesticidesSpreadingAction) actionToRemove).clearOtherProductInputUsages();
                }
                case AUTRE -> {
                    ((OtherAction) actionToRemove).clearPotInputUsages();
                    ((OtherAction) actionToRemove).clearSubstrateInputUsages();
                    ((OtherAction) actionToRemove).clearOtherProductInputUsages();
                }
                case ENTRETIEN_TAILLE_VIGNE_ET_VERGER ->
                        ((MaintenancePruningVinesAction) actionToRemove).clearOtherProductInputUsages();
                case EPANDAGES_ORGANIQUES -> {
                    ((OrganicFertilizersSpreadingAction) actionToRemove).clearOrganicProductInputUsages();
                    ((OrganicFertilizersSpreadingAction) actionToRemove).clearOtherProductInputUsages();
                }
                case IRRIGATION -> {
                    IrrigationAction irrigationAction = (IrrigationAction) actionToRemove;
                    inputUsageService.deleteIrrigationInputUsage(irrigationAction.getIrrigationInputUsage());
                    irrigationAction.clearOtherProductInputUsages();
                    irrigationAction.setIrrigationInputUsage(null);
                }
                case LUTTE_BIOLOGIQUE -> {
                    ((BiologicalControlAction) actionToRemove).clearBiologicalProductInputUsages();
                    ((BiologicalControlAction) actionToRemove).clearOtherProductInputUsages();
                }
                case RECOLTE -> {
                    HarvestingAction harvestingAction = (HarvestingAction) actionToRemove;
                    Collection<HarvestingActionValorisation> valorisationToRemoves = harvestingAction.getValorisations();
                    priceService.removeValorisationPrices(valorisationToRemoves);
                    harvestingAction.clearValorisations();
                    harvestingAction.clearWineValorisations();
                    harvestingAction.clearOtherProductInputUsages();
                    harvestingActionValorisationDao.deleteAll(valorisationToRemoves);
                }
                case SEMIS -> {
                    if (actionToRemove instanceof SeedingActionUsage) {
                        ((SeedingActionUsage) actionToRemove).clearSeedLotInputUsage();
                        ((SeedingActionUsage) actionToRemove).clearOtherProductInputUsages();
                    }
                }
                case TRANSPORT, TRAVAIL_DU_SOL -> {
                }
            }
        }

        abstractActionDao.deleteAll(actionsToRemove);
    }

    protected <IU extends AbstractInputUsage> void removeGivenActionInputUsages(InputPersistanceResults<IU> inputUsagesPersistanceResult, Consumer<IU> functionToProcess) {
        if (CollectionUtils.isNotEmpty(inputUsagesPersistanceResult.getInputUsagesRemoved())) {
            inputUsagesPersistanceResult.getInputUsagesRemoved().forEach(
                    functionToProcess
            );
        }
    }

    protected List<Sector> getSectorForCodeEspeceBotaniqueCodeQualifiant(Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant, Pair<String, String> valideCodeEspeceBotaniqueCodeQualifiantAee) {

        // add sector common to all codeEspeceBotanique
        String forAllSameCodeEspeceBotanique = valideCodeEspeceBotaniqueCodeQualifiantAee.getLeft() + "_" + null;
        List<Sector> sectors = sectorByCodeEspceBotaniqueCodeQualifiant.get(forAllSameCodeEspeceBotanique);
        ArrayList<Sector> sectorsForSpeces = sectors != null ? new ArrayList<>(sectors) : new ArrayList<>();

        // add specific sector to codeEspeceBotanique, code Qualifiant
        List<Sector> sectorsForSpecesAndQalifiant = sectorByCodeEspceBotaniqueCodeQualifiant.get(
                valideCodeEspeceBotaniqueCodeQualifiantAee.getLeft() + "_" + Strings.emptyToNull(valideCodeEspeceBotaniqueCodeQualifiantAee.getRight()));

        if (sectorsForSpecesAndQalifiant != null) {
            sectorsForSpeces.addAll(sectorsForSpecesAndQalifiant);
        }

        return sectorsForSpeces;
    }

    protected Set<Sector> getSectorsForSpecies(Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant, List<Pair<String, String>> valideCodeEspeceBotaniqueCodeQualifiantAees) {
        Set<Sector> sectors = Sets.newHashSet();
        for (Pair<String, String> valideCodeEspeceBotaniqueCodeQualifiantAee : valideCodeEspeceBotaniqueCodeQualifiantAees) {

            List<Sector> sectorsForSpeces = getSectorForCodeEspeceBotaniqueCodeQualifiant(sectorByCodeEspceBotaniqueCodeQualifiant, valideCodeEspeceBotaniqueCodeQualifiantAee);

            if (CollectionUtils.isNotEmpty(sectorsForSpeces)) {
                sectors.addAll(sectorsForSpeces);
            }

        }
        return sectors;
    }

    public HarvestingActionValorisationDto validateQualityCriteria(
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant,
            HarvestingActionValorisationDto valorisationDto,
            RefDestination destination,
            Optional<Collection<WineValorisation>> wineValorisations) {

        // pour valider une destination ou un critère de qualité
        // les destinations et critères de qualité portent sur
        // code_espece_botanique, code_qualifiant_AEE, wineValorisation -> RefDestination
        // code_espece_botanique, code_qualifiant_AEE, wineValorisation -> RefQualityCriteria
        // HarvestingActionValorisation -> speciesCode
        // HarvestingActionValorisation -> HarvestingAction -> WineValorisation
        // (speciesCode + campaing) -> RefEspece -> code_espece_botanique, code_qualifiant_AEE

        List<QualityCriteriaDto> nonValideQualityCriteria = new ArrayList<>();
        Optional<Collection<QualityCriteriaDto>> optQualityCriteriaDtos = valorisationDto.getQualityCriteriaDtos();

        if (optQualityCriteriaDtos.isPresent()) {
            Collection<QualityCriteriaDto> qualityCriteriaDtos = optQualityCriteriaDtos.get();
            for (QualityCriteriaDto qualityCriterion : qualityCriteriaDtos) {
                String refQualityCriteriaId = qualityCriterion.getRefQualityCriteriaId();
                RefQualityCriteria refQualityCriteria = referentialService.loadQualityCriteriaForId(refQualityCriteriaId);
                String qcCode_espece_botanique = refQualityCriteria.getCode_espece_botanique();
                String qcQualifiant_AEE = Strings.nullToEmpty(refQualityCriteria.getCode_qualifiant_AEE());
                Sector qcSector = refQualityCriteria.getSector();
                WineValorisation qcWineValorisation = destination.getWineValorisation();

                // can be null if campaigns where not valides or zone is null
                if (speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee != null && sectorByCodeEspeceBotaniqueCodeQualifiant != null) {

                    List<Pair<String, String>> valideCodeEspeceBotaniqueCodeQualifiantAees = getValideCodeEspeceBotaniqueCodeQualifiantAees(
                            speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                            valorisationDto,
                            qcCode_espece_botanique,
                            qcQualifiant_AEE);

                    boolean isValidSector = getSectorsForSpecies(
                            sectorByCodeEspeceBotaniqueCodeQualifiant,
                            valideCodeEspeceBotaniqueCodeQualifiantAees).contains(qcSector);

                    // code_espece_botanique can be null if destination target all species for it's sector
                    boolean isValideCodeEspeceBotaniqueCodeQualifiantAees = StringUtils.isBlank(qcCode_espece_botanique) || valideCodeEspeceBotaniqueCodeQualifiantAees.contains(Pair.of(qcCode_espece_botanique, qcQualifiant_AEE));

                    boolean isWineValorisationValid = !ReferentialService.WINE.equals(qcCode_espece_botanique) || (wineValorisations.isPresent() && wineValorisations.get().contains(qcWineValorisation));

                    if (!isValidSector || !isValideCodeEspeceBotaniqueCodeQualifiantAees || !isWineValorisationValid) {
                        nonValideQualityCriteria.add(qualityCriterion);
                        if (LOGGER.isInfoEnabled()) {
                            LOGGER.info(String.format("- Le critère de qualité %s n'est pas valide et a été retirée", valorisationDto.getDestinationId()));
                        }
                    }
                }
            }
            qualityCriteriaDtos.removeAll(nonValideQualityCriteria);
            valorisationDto.toBuilder().qualityCriteriaDtos(qualityCriteriaDtos).build();
        }
        return valorisationDto;
    }

    public HarvestingActionValorisationDto removeValorisationDtoYealdAverageOnNoneValidDestinations(
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant,
            HarvestingActionValorisationDto valorisationDto,
            RefDestination destination,
            Optional<Collection<WineValorisation>> wineValorisations) {
        // pour valider une destination ou un critère de qualité
        // les destinations et critères de qualité portent sur
        // code_espece_botanique, code_qualifiant_AEE, wineValorisation -> RefDestination
        // code_espece_botanique, code_qualifiant_AEE, wineValorisation -> RefQualityCriteria
        // HarvestingActionValorisation -> speciesCode
        // HarvestingActionValorisation -> HarvestingAction -> WineValorisation
        // (speciesCode + campaing) -> RefEspece -> code_espece_botanique, code_qualifiant_AEE

        // can be null if campaigns where not valides or zone is null
        if (speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee != null &&
                sectorByCodeEspeceBotaniqueCodeQualifiant != null &&
                valorisationDto.getYealdAverage() != 0) {

            String destCode_espece_botanique = destination.getCode_espece_botanique();
            String destCode_qualifiant_AEE = Strings.nullToEmpty(destination.getCode_qualifiant_AEE());
            Sector destSector = destination.getSector();
            WineValorisation destWineValorisation = destination.getWineValorisation();

            List<Pair<String, String>> valideCodeEspeceBotaniqueCodeQualifiantAees = getValideCodeEspeceBotaniqueCodeQualifiantAees(
                    speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee, valorisationDto, destCode_espece_botanique, destCode_qualifiant_AEE);

            boolean isValidSector = getSectorsForSpecies(sectorByCodeEspeceBotaniqueCodeQualifiant, valideCodeEspeceBotaniqueCodeQualifiantAees).contains(destSector);

            // code_espece_botanique can be null if destination target all species for it's sector
            boolean isValideCodeEspeceBotaniqueCodeQualifiantAees =
                    StringUtils.isBlank(destCode_espece_botanique) ||
                            valideCodeEspeceBotaniqueCodeQualifiantAees.contains(Pair.of(destCode_espece_botanique, destCode_qualifiant_AEE));

            boolean isWineValorisationValid = !ReferentialService.WINE.equals(destCode_espece_botanique) ||
                    (wineValorisations.isPresent() && CollectionUtils.isNotEmpty(wineValorisations.get()) && wineValorisations.get().contains(destWineValorisation));

            if (!isValidSector || !isValideCodeEspeceBotaniqueCodeQualifiantAees || !isWineValorisationValid) {
                if (LOGGER.isInfoEnabled()) {
                    LOGGER.info(String.format("- La destination %s (%s, %s) code:'%s' de la valorisation id:'%s' portant sur l'espèce au code '%s' n'est pas valide sont rendement passe de %f à 0",
                            destination.getDestination(), destCode_espece_botanique, destCode_qualifiant_AEE, destination.getCode_destination_A(),
                            valorisationDto.getTopiaId(), valorisationDto.getSpeciesCode(), valorisationDto.getYealdAverage()));
                }
                valorisationDto = valorisationDto.toBuilder().yealdAverage(0d).build();
            }

            valorisationDto = valorisationDto.toBuilder().build();

        }

        return valorisationDto;
    }

    protected void computeGlobalValorisationForDestination(
            HarvestingActionValorisationDto valorisationDto,
            Map<String, Pair<Double, List<HarvestingActionValorisationDto>>> yealdAverageByDestination) {

        Pair<Double, List<HarvestingActionValorisationDto>> yealAverageForDestination =
                yealdAverageByDestination.computeIfAbsent(
                        valorisationDto.getDestinationId(), k -> Pair.of(valorisationDto.getYealdAverage(), Lists.newArrayList()));

        Double globalYealdAverage = yealAverageForDestination.getLeft() + valorisationDto.getYealdAverage();

        List<HarvestingActionValorisationDto> valorisations = yealAverageForDestination.getRight();
        valorisations.add(valorisationDto);

        yealdAverageByDestination.replace(valorisationDto.getDestinationId(), Pair.of(globalYealdAverage, valorisations));
    }

    protected List<Pair<String, String>> getValideCodeEspeceBotaniqueCodeQualifiantAees(
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            HarvestingActionValorisationDto valorisationDto,
            String destin_code_espece_botanique,
            String destin_code_qualifiant_AEE) {

        List<Pair<String, String>> valideCodeEspeceBotaniqueCodeQualifiantAees = ObjectUtils.firstNonNull(
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee.get(valorisationDto.getSpeciesCode()), new ArrayList<>());

        return getValideCodeEspeceBotaniqueCodeQualifiantAees(
                destin_code_espece_botanique,
                destin_code_qualifiant_AEE,
                valideCodeEspeceBotaniqueCodeQualifiantAees);
    }

    private List<Pair<String, String>> getValideCodeEspeceBotaniqueCodeQualifiantAees(
            String destin_code_espece_botanique,
            String destin_code_qualifiant_AEE,
            List<Pair<String, String>> valideCodeEspeceBotaniqueCodeQualifiantAees) {
        // to be able to validate Destination or Quality Criteria that match all same codeEspeceBotanique
        if (StringUtils.isNotBlank(destin_code_espece_botanique) && StringUtils.isBlank(destin_code_qualifiant_AEE)) {
            Set<Pair<String, String>> valideCodeEspeceBotaniqueCodeQualifiantAeesSet = Sets.newHashSet(valideCodeEspeceBotaniqueCodeQualifiantAees);
            for (Pair<String, String> valideCodeEspeceBotaniqueCodeQualifiantAee : valideCodeEspeceBotaniqueCodeQualifiantAees) {
                valideCodeEspeceBotaniqueCodeQualifiantAeesSet.add(Pair.of(valideCodeEspeceBotaniqueCodeQualifiantAee.getLeft(), ""));
            }
            valideCodeEspeceBotaniqueCodeQualifiantAees = Lists.newArrayList(valideCodeEspeceBotaniqueCodeQualifiantAeesSet);
        }
        return valideCodeEspeceBotaniqueCodeQualifiantAees;
    }


    public List<HarvestingActionValorisationDto> getValorisationsDtoWhereGlobalYealdAverageIsZero(
            Map<String, Pair<Double, List<HarvestingActionValorisationDto>>> yealdAverageByDestination) {

        List<HarvestingActionValorisationDto> nonValidatedValorisations = new ArrayList<>();
        for (Map.Entry<String, Pair<Double, List<HarvestingActionValorisationDto>>> yeladAverageForDestination : yealdAverageByDestination.entrySet()) {
            Double globalYealAverage = yeladAverageForDestination.getValue().getLeft();
            if (globalYealAverage == 0) {
                nonValidatedValorisations.addAll(yeladAverageForDestination.getValue().getValue());
            }
        }
        return nonValidatedValorisations;
    }

    protected ImmutableList<String> getCropCycleToolsCouplingCodes(
            PracticedIntervention practicedIntervention,
            EffectiveIntervention effectiveIntervention) {

        ImmutableList<String> toolsCouplingCodes;
        if (practicedIntervention != null) {
            toolsCouplingCodes = ImmutableList.copyOf(CollectionUtils.emptyIfNull(practicedIntervention.getToolsCouplingCodes()));
        } else {
            if (effectiveIntervention.getToolCouplings() != null) {
                List<ToolsCoupling> toolsCouplings = Lists.newArrayList(effectiveIntervention.getToolCouplings());
                toolsCouplingCodes = ImmutableList.copyOf(toolsCouplings.stream().map(EffectiveCropCycleService.GET_TOOLS_COUPLING_BY_CODE).collect(Collectors.toList()));
            } else {
                toolsCouplingCodes = ImmutableList.of();
            }
        }
        return toolsCouplingCodes;
    }

    public void duplicatePracticedActionsAndUsage(
            PracticedDuplicateCropCyclesContext duplicateContext,
            PracticedIntervention practicedIntervention,
            PracticedIntervention practicedInterventionClone) {

        Preconditions.checkArgument(practicedIntervention != null);
        Preconditions.checkArgument(practicedInterventionClone != null);

        Collection<AbstractAction> actions = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).findAll();

        for (AbstractAction originalAction : actions) {
            duplicatePracticedActionAndUsages(
                    duplicateContext,
                    originalAction,
                    practicedInterventionClone
            );
        }

    }

    /**
     * @param duplicateContext           all data needed for duplication
     * @param originalAction             action to clone
     * @param practicedInterventionClone practiced intervention the cloned action belong to
     */
    public void duplicatePracticedActionAndUsages(
            PracticedDuplicateCropCyclesContext duplicateContext,
            AbstractAction originalAction,
            PracticedIntervention practicedInterventionClone) {

        switch (originalAction) {
            case HarvestingAction harvestingAction ->
                    duplicateHarvestingActionAndUsages(duplicateContext, practicedInterventionClone, harvestingAction);
            case SeedingActionUsage seedingActionUsageSource ->
                    duplicateSeedingActionAndUsages(duplicateContext, practicedInterventionClone, seedingActionUsageSource);
            case OtherAction otherAction ->
                    duplicateOtherActionAndUsages(duplicateContext, practicedInterventionClone, otherAction);
            case IrrigationAction irrigationAction ->
                    duplicateIrrigationActionAndUsages(duplicateContext, practicedInterventionClone, irrigationAction);
            case TillageAction tillageAction ->
                    duplicateTillageActionAndUsages(practicedInterventionClone, tillageAction);
            case MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction ->
                    duplicateMineralFertilizersSpreadingActionAndUsages(duplicateContext, practicedInterventionClone, mineralFertilizersSpreadingAction);
            case OrganicFertilizersSpreadingAction organicFertilizersSpreadingAction ->
                    duplicateOrganicFertilizersActionAndUsages(duplicateContext, practicedInterventionClone, organicFertilizersSpreadingAction);
            case MaintenancePruningVinesAction maintenancePruningVinesAction ->
                    duplicateMaintenancePruningVinesActionAndUsages(duplicateContext, practicedInterventionClone, maintenancePruningVinesAction);
            case BiologicalControlAction biologicalControlAction ->
                    duplicateBiologicalControlActionAndUsages(duplicateContext, practicedInterventionClone, biologicalControlAction);
            case PesticidesSpreadingAction pesticidesSpreadingAction ->
                    duplicatePesticidesSpreadingActionAndUsages(duplicateContext, practicedInterventionClone, pesticidesSpreadingAction);
            case CarriageAction carriageAction ->
                    duplicateCarriageActionAndUsages(practicedInterventionClone, carriageAction);
            case null, default -> throw new UnsupportedOperationException("Unsupported action type");
        }

    }

    private void duplicateCarriageActionAndUsages(PracticedIntervention practicedInterventionClone, CarriageAction carriageAction) {
        CarriageAction clonedCarriageAction = carriageActionDao.newInstance();
        shallowBindAbstractAction(carriageAction, clonedCarriageAction, practicedInterventionClone);

        clonedCarriageAction.setLoadCapacity(carriageAction.getLoadCapacity());
        clonedCarriageAction.setTripFrequency(carriageAction.getTripFrequency());

        carriageActionDao.create(clonedCarriageAction);

    }

    private void duplicatePesticidesSpreadingActionAndUsages(
            PracticedDuplicateCropCyclesContext duplicateContext,
            PracticedIntervention practicedInterventionClone,
            PesticidesSpreadingAction pesticidesSpreadingAction) {

        PesticidesSpreadingAction clonedPesticidesSpreadingAction = pesticidesSpreadingActionDao.newInstance();
        shallowBindAbstractAction(pesticidesSpreadingAction, clonedPesticidesSpreadingAction, practicedInterventionClone);

        clonedPesticidesSpreadingAction.setBoiledQuantity(pesticidesSpreadingAction.getBoiledQuantity());
        clonedPesticidesSpreadingAction.setBoiledQuantityPerTrip(pesticidesSpreadingAction.getBoiledQuantityPerTrip());
        clonedPesticidesSpreadingAction.setTripFrequency(pesticidesSpreadingAction.getTripFrequency());
        clonedPesticidesSpreadingAction.setAntiDriftNozzle(pesticidesSpreadingAction.isAntiDriftNozzle());
        clonedPesticidesSpreadingAction.setProportionOfTreatedSurface(pesticidesSpreadingAction.getProportionOfTreatedSurface());

        Collection<PesticideProductInputUsage> pesticideProductInputUsages = pesticidesSpreadingAction.getPesticideProductInputUsages();
        if (CollectionUtils.isNotEmpty(pesticideProductInputUsages)) {
            Collection<PesticideProductInputUsage> clonedPesticideProductInputUsages = inputUsageService.clonePesticideProductInputUsages(pesticideProductInputUsages, duplicateContext);
            clonedPesticidesSpreadingAction.addAllPesticideProductInputUsages(clonedPesticideProductInputUsages);
        }

        Collection<OtherProductInputUsage> otherProductInputUsages = pesticidesSpreadingAction.getOtherProductInputUsages();
        if (CollectionUtils.isNotEmpty(otherProductInputUsages)) {
            Collection<OtherProductInputUsage> clonedOtherProductInputUsages = inputUsageService.cloneOtherProductInputUsages(otherProductInputUsages, duplicateContext);
            clonedPesticidesSpreadingAction.addAllOtherProductInputUsages(clonedOtherProductInputUsages);
        }

        pesticidesSpreadingActionDao.create(clonedPesticidesSpreadingAction);

    }

    private void duplicateBiologicalControlActionAndUsages(
            PracticedDuplicateCropCyclesContext duplicateContext,
            PracticedIntervention practicedInterventionClone,
            BiologicalControlAction biologicalControlAction) {

        BiologicalControlAction clonedBiologicalControlAction = biologicalControlActionDao.newInstance();
        shallowBindAbstractAction(biologicalControlAction, clonedBiologicalControlAction, practicedInterventionClone);

        clonedBiologicalControlAction.setBoiledQuantity(biologicalControlAction.getBoiledQuantity());
        clonedBiologicalControlAction.setBoiledQuantityPerTrip(biologicalControlAction.getBoiledQuantityPerTrip());
        clonedBiologicalControlAction.setTripFrequency(biologicalControlAction.getTripFrequency());
        clonedBiologicalControlAction.setProportionOfTreatedSurface(biologicalControlAction.getProportionOfTreatedSurface());

        Collection<BiologicalProductInputUsage> biologicalProductInputUsages = biologicalControlAction.getBiologicalProductInputUsages();
        if (CollectionUtils.isNotEmpty(biologicalProductInputUsages)) {
            Collection<BiologicalProductInputUsage> clonedBiologicalProductInputUsages = inputUsageService.cloneBiologicalProductInputUsage(biologicalProductInputUsages, duplicateContext);
            clonedBiologicalControlAction.addAllBiologicalProductInputUsages(clonedBiologicalProductInputUsages);
        }

        Collection<OtherProductInputUsage> otherProductInputUsages = biologicalControlAction.getOtherProductInputUsages();
        if (CollectionUtils.isNotEmpty(otherProductInputUsages)) {
            Collection<OtherProductInputUsage> clonedOtherProductInputUsages = inputUsageService.cloneOtherProductInputUsages(otherProductInputUsages, duplicateContext);
            clonedBiologicalControlAction.addAllOtherProductInputUsages(clonedOtherProductInputUsages);
        }
        biologicalControlActionDao.create(clonedBiologicalControlAction);
    }

    private void duplicateMaintenancePruningVinesActionAndUsages(
            PracticedDuplicateCropCyclesContext duplicateContext,
            PracticedIntervention practicedInterventionClone,
            MaintenancePruningVinesAction maintenancePruningVinesAction) {

        MaintenancePruningVinesAction clonedMaintenancePruningVinesAction = maintenancePruningVinesActionDao.newInstance();
        shallowBindAbstractAction(maintenancePruningVinesAction, clonedMaintenancePruningVinesAction, practicedInterventionClone);

        Collection<OtherProductInputUsage> otherProductInputUsages = maintenancePruningVinesAction.getOtherProductInputUsages();
        if (CollectionUtils.isNotEmpty(otherProductInputUsages)) {
            Collection<OtherProductInputUsage> clonedOtherProductInputUsages = inputUsageService.cloneOtherProductInputUsages(otherProductInputUsages, duplicateContext);
            clonedMaintenancePruningVinesAction.addAllOtherProductInputUsages(clonedOtherProductInputUsages);
        }
        maintenancePruningVinesActionDao.create(clonedMaintenancePruningVinesAction);
    }

    private void duplicateOrganicFertilizersActionAndUsages(
            PracticedDuplicateCropCyclesContext duplicateContext,
            PracticedIntervention practicedInterventionClone,
            OrganicFertilizersSpreadingAction organicFertilizersSpreadingAction) {

        OrganicFertilizersSpreadingAction clonedOrganicFertilizersSpreadingAction = organicFertilizersSpreadingActionDao.newInstance();
        shallowBindAbstractAction(organicFertilizersSpreadingAction, clonedOrganicFertilizersSpreadingAction, practicedInterventionClone);

        clonedOrganicFertilizersSpreadingAction.setLandfilledWaste(organicFertilizersSpreadingAction.isLandfilledWaste());
        Collection<OrganicProductInputUsage> organicProductInputUsages = organicFertilizersSpreadingAction.getOrganicProductInputUsages();
        if (CollectionUtils.isNotEmpty(organicProductInputUsages)) {
            Collection<OrganicProductInputUsage> clonedOrganicProductInputUsages = inputUsageService.cloneOrganicProductInputUsages(organicProductInputUsages, duplicateContext);
            clonedOrganicFertilizersSpreadingAction.addAllOrganicProductInputUsages(clonedOrganicProductInputUsages);
        }
        Collection<OtherProductInputUsage> otherProductInputUsages = organicFertilizersSpreadingAction.getOtherProductInputUsages();
        if (CollectionUtils.isNotEmpty(otherProductInputUsages)) {
            Collection<OtherProductInputUsage> clonedOtherProductInputUsages = inputUsageService.cloneOtherProductInputUsages(otherProductInputUsages, duplicateContext);
            clonedOrganicFertilizersSpreadingAction.addAllOtherProductInputUsages(clonedOtherProductInputUsages);
        }
        organicFertilizersSpreadingActionDao.create(clonedOrganicFertilizersSpreadingAction);
    }

    private void duplicateMineralFertilizersSpreadingActionAndUsages(
            PracticedDuplicateCropCyclesContext duplicateContext,
            PracticedIntervention practicedInterventionClone,
            MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction) {

        MineralFertilizersSpreadingAction clonedMineralFertilizersSpreadingAction = mineralFertilizersSpreadingActionDao.newInstance();
        shallowBindAbstractAction(mineralFertilizersSpreadingAction, clonedMineralFertilizersSpreadingAction, practicedInterventionClone);

        clonedMineralFertilizersSpreadingAction.setBurial(mineralFertilizersSpreadingAction.isBurial());
        clonedMineralFertilizersSpreadingAction.setLocalizedSpreading(mineralFertilizersSpreadingAction.isLocalizedSpreading());

        Collection<MineralProductInputUsage> mineralProductInputUsages = mineralFertilizersSpreadingAction.getMineralProductInputUsages();
        if (CollectionUtils.isNotEmpty(mineralProductInputUsages)) {
            Collection<MineralProductInputUsage> clonedMineralProductInputUsages = inputUsageService.cloneMineralProductInputUsages(mineralProductInputUsages, duplicateContext);
            clonedMineralFertilizersSpreadingAction.addAllMineralProductInputUsages(clonedMineralProductInputUsages);
        }

        Collection<OtherProductInputUsage> otherProductInputUsages = mineralFertilizersSpreadingAction.getOtherProductInputUsages();
        if (CollectionUtils.isNotEmpty(otherProductInputUsages)) {
            Collection<OtherProductInputUsage> clonedOtherProductInputUsages = inputUsageService.cloneOtherProductInputUsages(otherProductInputUsages, duplicateContext);
            clonedMineralFertilizersSpreadingAction.addAllOtherProductInputUsages(clonedOtherProductInputUsages);
        }
        mineralFertilizersSpreadingActionDao.create(clonedMineralFertilizersSpreadingAction);
    }

    private void duplicateTillageActionAndUsages(PracticedIntervention practicedInterventionClone, TillageAction tillageAction) {
        TillageAction clonedTillageAction = tillageActionDao.newInstance();
        shallowBindAbstractAction(tillageAction, clonedTillageAction, practicedInterventionClone);

        clonedTillageAction.setTillageDepth(clonedTillageAction.getTillageDepth());
        clonedTillageAction.setOtherSettingTool(clonedTillageAction.getOtherSettingTool());

        tillageActionDao.create(clonedTillageAction);
    }

    private void duplicateIrrigationActionAndUsages(PracticedDuplicateCropCyclesContext duplicateContext, PracticedIntervention practicedInterventionClone, IrrigationAction irrigationAction) {
        IrrigationAction clonedIrrigationAction = irrigationActionDao.newInstance();
        shallowBindAbstractAction(irrigationAction, clonedIrrigationAction, practicedInterventionClone);

        clonedIrrigationAction.setWaterQuantityMin(irrigationAction.getWaterQuantityMin());
        clonedIrrigationAction.setWaterQuantityMax(irrigationAction.getWaterQuantityMax());
        clonedIrrigationAction.setWaterQuantityAverage(irrigationAction.getWaterQuantityAverage());
        clonedIrrigationAction.setWaterQuantityMedian(irrigationAction.getWaterQuantityMedian());
        clonedIrrigationAction.setAzoteQuantity(irrigationAction.getAzoteQuantity());

        Collection<OtherProductInputUsage> otherProductInputUsages = inputUsageService.cloneOtherProductInputUsages(irrigationAction.getOtherProductInputUsages(), duplicateContext);
        clonedIrrigationAction.setOtherProductInputUsages(otherProductInputUsages);

        IrrigationInputUsage irrigationInputUsage = inputUsageService.cloneIrrigationInputUsages(irrigationAction.getIrrigationInputUsage(), duplicateContext);
        clonedIrrigationAction.setIrrigationInputUsage(irrigationInputUsage);

        irrigationActionDao.create(clonedIrrigationAction);
    }

    private void duplicateOtherActionAndUsages(
            PracticedDuplicateCropCyclesContext duplicateContext,
            PracticedIntervention practicedInterventionClone,
            OtherAction otherAction) {

        OtherAction clonedOtherAction = otherActionDao.newInstance();
        shallowBindAbstractAction(otherAction, clonedOtherAction, practicedInterventionClone);

        Collection<OtherProductInputUsage> otherProductInputUsages = otherAction.getOtherProductInputUsages();
        if (CollectionUtils.isNotEmpty(otherProductInputUsages)) {
            Collection<OtherProductInputUsage> clonedOtherProductInputUsages = inputUsageService.cloneOtherProductInputUsages(otherProductInputUsages, duplicateContext);
            clonedOtherAction.addAllOtherProductInputUsages(clonedOtherProductInputUsages);
        }
        Collection<SubstrateInputUsage> substrateInputUsages = otherAction.getSubstrateInputUsages();
        if (CollectionUtils.isNotEmpty(substrateInputUsages)) {
            Collection<SubstrateInputUsage> clonedSubstrateInputUsages = inputUsageService.cloneSubstrateInputUsageDtos(substrateInputUsages, duplicateContext);
            clonedOtherAction.addAllSubstrateInputUsages(clonedSubstrateInputUsages);
        }
        Collection<PotInputUsage> potInputUsages = otherAction.getPotInputUsages();
        if (CollectionUtils.isNotEmpty(potInputUsages)) {
            Collection<PotInputUsage> clonedPotInputUsages = inputUsageService.clonePotInputUsages(potInputUsages, duplicateContext);
            clonedOtherAction.addAllPotInputUsages(clonedPotInputUsages);
        }
        otherActionDao.create(clonedOtherAction);
    }

    private void duplicateSeedingActionAndUsages(
            PracticedDuplicateCropCyclesContext duplicateContext,
            PracticedIntervention practicedInterventionClone,
            SeedingActionUsage seedingActionUsageSource) {

        SeedingActionUsage clonedSeedingActionUsage = seedingActionUsageDao.newInstance();
        shallowBindAbstractAction(seedingActionUsageSource, clonedSeedingActionUsage, practicedInterventionClone);

        clonedSeedingActionUsage.setDeepness(seedingActionUsageSource.getDeepness());
        clonedSeedingActionUsage.setYealdTarget(seedingActionUsageSource.getYealdTarget());
        clonedSeedingActionUsage.setYealdUnit(seedingActionUsageSource.getYealdUnit());
        clonedSeedingActionUsage.setSeedType(seedingActionUsageSource.getSeedType());

        Collection<OtherProductInputUsage> otherProductInputUsages = seedingActionUsageSource.getOtherProductInputUsages();
        if (CollectionUtils.isNotEmpty(otherProductInputUsages)) {
            Collection<OtherProductInputUsage> clonedOtherProductInputUsages = inputUsageService.cloneOtherProductInputUsages(otherProductInputUsages, duplicateContext);
            clonedSeedingActionUsage.addAllOtherProductInputUsages(clonedOtherProductInputUsages);
        }
        Collection<SubstrateInputUsage> substrateInputUsages = seedingActionUsageSource.getSubstrateInputUsage();
        if (CollectionUtils.isNotEmpty(substrateInputUsages)) {
            Collection<SubstrateInputUsage> clonedSubstrateInputUsages = inputUsageService.cloneSubstrateInputUsageDtos(substrateInputUsages, duplicateContext);
            clonedSeedingActionUsage.addAllSubstrateInputUsage(clonedSubstrateInputUsages);
        }

        Collection<SeedLotInputUsage> seedLotInputUsageSources = seedingActionUsageSource.getSeedLotInputUsage();
        for (SeedLotInputUsage seedLotInputUsageSource : seedLotInputUsageSources) {
            SeedLotInputUsage clonedSeedLotInputUsage = inputUsageService.cloneSeedLotInputUsageEntityToNewEntity(
                    seedLotInputUsageSource,
                    duplicateContext);
            clonedSeedingActionUsage.addSeedLotInputUsage(clonedSeedLotInputUsage);
        }
        seedingActionUsageDao.create(clonedSeedingActionUsage);
    }



    private void duplicateHarvestingActionAndUsages(
            PracticedDuplicateCropCyclesContext duplicateContext,
            PracticedIntervention practicedInterventionClone,
            HarvestingAction harvestingAction) {

        final Map<String, Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>>> toSpeciesByCropCode = duplicateContext.getToSpeciesByCropCode();
        final String targetedCropCode = duplicateContext.getTargetedCropCode();
        final Map<String, CroppingPlanSpecies> cpsByCode = toSpeciesByCropCode.get(targetedCropCode) != null ? toSpeciesByCropCode.get(targetedCropCode).getValue() : Collections.emptyMap();

        HarvestingAction clonedHarvestingAction = harvestingActionDao.createByNotNull(harvestingAction.getMainAction());
        shallowBindAbstractAction(harvestingAction, clonedHarvestingAction, practicedInterventionClone);

        Set<HarvestingActionValorisationTopiaDao.UniqueKeyValorisation> uniqueKeyValorisations = new HashSet<>();
        Collection<HarvestingActionValorisation> valorisations = CollectionUtils.emptyIfNull(harvestingAction.getValorisations());
        if (CollectionUtils.isNotEmpty(valorisations)) {
            Binder<HarvestingActionValorisation, HarvestingActionValorisation> binder = BinderFactory.newBinder(HarvestingActionValorisation.class);
            Binder<QualityCriteria, QualityCriteria> qualityCriteriaBinder = BinderFactory.newBinder(QualityCriteria.class);
            for (HarvestingActionValorisation valorisation : valorisations.stream().filter(Objects::nonNull).toList()) {

                HarvestingActionValorisationTopiaDao.UniqueKeyValorisation uniqueKeyValorisation = new HarvestingActionValorisationTopiaDao.UniqueKeyValorisation(
                        clonedHarvestingAction,
                        valorisation.getSpeciesCode(),
                        valorisation.getDestination());

                if (uniqueKeyValorisations.contains(uniqueKeyValorisation)) {
                    if (LOGGER.isErrorEnabled()) {
                        LOGGER.error(String.format("Valorisation en double remontée à la duplication du synthétisé %s vers le domain %s code %s vers le synthétisé %s et intervention %s %s %s",
                                duplicateContext.getPracticedSystem().getTopiaId(),
                                duplicateContext.getToDomainName(),
                                duplicateContext.getToDomainCode(),
                                duplicateContext.getPracticedSystemClone().getTopiaId(),
                                practicedInterventionClone.getTopiaId(),
                                practicedInterventionClone.getName(),
                                practicedInterventionClone.getStartingPeriodDate()
                        ));
                    }
                    continue;
                }

                uniqueKeyValorisations.add(uniqueKeyValorisation);

                HarvestingActionValorisation valorisationClone = harvestingActionValorisationDao.newInstance();

                Collection<QualityCriteria> qualityCriterias = valorisation.getQualityCriteria();
                if (CollectionUtils.isNotEmpty(qualityCriterias)) {
                    for (QualityCriteria qualityCriteria : qualityCriterias) {
                        QualityCriteria qualityCriteriaClone = qualityCriteriaDao.newInstance();
                        shallowBindQualityCreteria(qualityCriteriaBinder, qualityCriteria, qualityCriteriaClone);
                        valorisationClone.addQualityCriteria(qualityCriteriaClone);
                    }
                }

                shallowBindHarvestingActionValorisation(binder, valorisation, valorisationClone);

                valorisationClone = harvestingActionValorisationDao.create(valorisationClone);

                HarvestingPrice price = priceService.getPriceForValorisation(valorisation);

                if (price != null) {
                    final CroppingPlanSpecies croppingPlanSpecies = cpsByCode.get(valorisationClone.getSpeciesCode());

                    if (croppingPlanSpecies != null) {

                        final String objectId = PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(croppingPlanSpecies, valorisation.getDestination());

                        final Domain domain;
                        String zoneId = null;
                        String zoneName = null;
                        String growingSystemName;
                        String campaign;
                        final PracticedSystem practicedSystemClone = duplicateContext.getPracticedSystemClone();
                        String practicedSystemId = practicedSystemClone.getTopiaId();
                        String practicedSystemName = practicedSystemClone.getName();
                        domain = practicedSystemClone.getGrowingSystem().getGrowingPlan().getDomain();
                        growingSystemName = practicedSystemClone.getGrowingSystem().getName();
                        campaign = practicedSystemClone.getCampaigns();

                        HarvestingPriceDto priceDto = HarvestingPriceDto.builder()
                                .actionId(clonedHarvestingAction.getTopiaId())
                                .domainId(domain.getTopiaId())
                                .practicedSystemId(practicedSystemId)
                                .zoneId(zoneId)
                                .objectId(objectId)
                                .displayName(price.getDisplayName())
                                .sourceUnit(price.getSourceUnit())
                                .price(price.getPrice())
                                .priceUnit(price.getPriceUnit())
                                .category(InputPriceCategory.HARVESTING_ACTION)
                                .valorisationId(valorisationClone.getTopiaId())
                                .growingSystemName(growingSystemName)
                                .practicedSystemName(practicedSystemName)
                                .zoneName(zoneName)
                                .campaign(campaign)
                                .build();

                        HarvestingPrice priceClone = priceService.bindDtoToPrice(
                                domain,
                                valorisationClone,
                                priceDto,
                                priceService.newHarvestingPriceInstance(),
                                objectId,
                                Optional.of(practicedSystemClone),
                                Optional.empty()
                        );

                        priceService.persistPrice(priceClone);
                        valorisationClone = harvestingActionValorisationDao.update(valorisationClone);
                    }
                }
                clonedHarvestingAction.addValorisations(valorisationClone);
            }
        }

        if (harvestingAction.getWineValorisations() != null) {
            List<WineValorisation> wineValorisations = Lists.newArrayList(harvestingAction.getWineValorisations());
            clonedHarvestingAction.setWineValorisations(wineValorisations);
        }

        Collection<OtherProductInputUsage> otherProductInputUsages = harvestingAction.getOtherProductInputUsages();
        if (CollectionUtils.isNotEmpty(otherProductInputUsages)) {
            Collection<OtherProductInputUsage> clonedOtherProductInputUsages = inputUsageService.cloneOtherProductInputUsages(
                    otherProductInputUsages,
                    duplicateContext);
            clonedHarvestingAction.addAllOtherProductInputUsages(clonedOtherProductInputUsages);
        }

        // apply the modifications on to the action
        shallowBindAbstractAction(harvestingAction, clonedHarvestingAction, practicedInterventionClone);
        clonedHarvestingAction.setCattleCode(harvestingAction.getCattleCode());
        clonedHarvestingAction.setPastureLoad(harvestingAction.getPastureLoad());
        clonedHarvestingAction.setPasturingAtNight(harvestingAction.isPasturingAtNight());

        harvestingActionDao.update(clonedHarvestingAction);
    }

    protected void shallowBindAbstractAction(AbstractAction origin, AbstractAction clone, PracticedIntervention practicedInterventionClone) {
        clone.setMainAction(origin.getMainAction());
        clone.setComment(origin.getComment());
        clone.setToolsCouplingCode(origin.getToolsCouplingCode());
        clone.setPracticedIntervention(practicedInterventionClone);
        clone.setEffectiveIntervention(null);
    }

    private void shallowBindHarvestingActionValorisation(Binder<HarvestingActionValorisation, HarvestingActionValorisation> binder, HarvestingActionValorisation valorisation, HarvestingActionValorisation valorisationClone) {
        binder.copyExcluding(valorisation, valorisationClone,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                HarvestingActionValorisation.PROPERTY_QUALITY_CRITERIA);
    }

    private void shallowBindQualityCreteria(Binder<QualityCriteria, QualityCriteria> qualityCriteriaBinder, QualityCriteria qualityCriteria, QualityCriteria qualityCriteriaClone) {
        qualityCriteriaBinder.copyExcluding(qualityCriteria, qualityCriteriaClone,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION);
    }

    @Override
    public Map<AbstractActionDto, Boolean> migrateEffectiveActionsSpeciesToTargetedSpecies(
            Collection<AbstractActionDto> actionDtos,
            Map<String, CroppingPlanSpecies> targetedSpeciesByCode,
            Map<String, String> fromSpeciesCodeToSpeciesCode,
            String fromCropCode, String toCropCode,
            Boolean interventionIntermediateStatusChange,
            List<SpeciesStadeDto> targetedSpeciesStadeDtos,
            Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant,
            Map<String, List<AbstractDomainInputStockUnit>> domainInputStockUnitByKeys,
            List<CroppingPlanSpecies> fromZoneSpecies) {

        // CP -> CP || CI -> CI
        // Copier sur la CP. Si la CP cible a le même code que la CP source,
        // cocher les espèces cibles qui sont les mêmes que les espèces sources (même refespece)
        // sinon cocher toutes les espèces de la CP cible.

        Map<AbstractActionDto, Boolean> migratedAbstractActionDto = new HashMap<>();

        for (AbstractActionDto actionDto : actionDtos) {
            final AgrosystInterventionType interventionAgrosyst = actionDto.getMainActionInterventionAgrosyst();
            if (AgrosystInterventionType.SEMIS.equals(interventionAgrosyst)) {
                Pair<SeedingActionUsageDto, Boolean> seedingActionUsageMigrationResult = migrateEffectiveSeedingActionToTargetedSpecies(
                        targetedSpeciesByCode,
                        fromSpeciesCodeToSpeciesCode,
                        (SeedingActionUsageDto) actionDto,
                        domainInputStockUnitByKeys
                );

                migratedAbstractActionDto.put(seedingActionUsageMigrationResult.getLeft(), seedingActionUsageMigrationResult.getRight());
            } else if (AgrosystInterventionType.RECOLTE.equals(interventionAgrosyst)) {
                Pair<HarvestingActionDto, Boolean> harvestingActionDtoMigrationResult = migrateEffectiveHarvestingActionToTargetedSpecies(
                        targetedSpeciesByCode,
                        fromSpeciesCodeToSpeciesCode,
                        targetedSpeciesStadeDtos,
                        (HarvestingActionDto) actionDto,
                        sectorByCodeEspeceBotaniqueCodeQualifiant);

                migratedAbstractActionDto.put(harvestingActionDtoMigrationResult.getLeft(), harvestingActionDtoMigrationResult.getRight());
            }

        }
        return migratedAbstractActionDto;
    }

    protected Pair<HarvestingActionDto, Boolean> migrateEffectiveHarvestingActionToTargetedSpecies(
            Map<String, CroppingPlanSpecies> targetedSpeciesByCode,
            Map<String, String> fromSpeciesCodeToSpeciesCode,
            List<SpeciesStadeDto> targetedSpeciesStadeDtos,
            HarvestingActionDto harvestingActionDto,
            Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant) {

        final int nbValoisations = harvestingActionDto.getValorisationDtos().size();

        // this valorisation list will replace the existing one

        // in case all from species match targeted Species
        // do an exact copy of valorisations only replacing the scpecies code
        if (fromSpeciesCodeToSpeciesCode.size() == targetedSpeciesByCode.size()) {
            final List<HarvestingActionValorisationDto> harvestingActionValorisationDtos = doEffectiveSimpleValorisationsSpeciesMigration(
                    targetedSpeciesByCode,
                    fromSpeciesCodeToSpeciesCode,
                    harvestingActionDto.getValorisationDtos()
            );
            harvestingActionDto = harvestingActionDto.toBuilder()
                    .topiaId(null)
                    .valorisationDtos(harvestingActionValorisationDtos)
                    .build();

        } else {
            harvestingActionDto = doEffectiveMatchingSpeciesValorisationsMigration(
                    targetedSpeciesByCode,
                    targetedSpeciesStadeDtos,
                    sectorByCodeEspeceBotaniqueCodeQualifiant,
                    harvestingActionDto
            );
        }

        return Pair.of(harvestingActionDto, harvestingActionDto.getValorisationDtos().size() == nbValoisations);
    }

    protected HarvestingActionDto doEffectiveMatchingSpeciesValorisationsMigration(
            Map<String, CroppingPlanSpecies> targetedSpeciesByCode,
            List<SpeciesStadeDto> targetedSpeciesStadeDtos,
            Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant,
            HarvestingActionDto harvestingActionDto) {


        // import valorisations for same libelle espece botanique

        List<HarvestingActionValorisationDto> newValorisationDtos = new ArrayList<>();

        Map<String, CroppingPlanSpecies> targetedSpeciesStadesSpeciesCodes = getTargetedSpeciesStadesSpeciesCodes(targetedSpeciesByCode, targetedSpeciesStadeDtos);

        boolean wineSpeciesFound = ActionService.IS_WINE_SPECIES_FOUND(targetedSpeciesStadesSpeciesCodes.values());

        Collection<WineValorisation> actionWineValorisations = wineSpeciesFound ? harvestingActionDto.getWineValorisations().orElse(new ArrayList<>()) : new ArrayList<>();
        Set<WineValorisation> wineValorisations = Sets.newHashSet(actionWineValorisations);

        Map<String, Pair<HarvestingActionValorisationDto, Pair<Integer, Double>>> valorisationDestinationGlobalYealAverages = getValorisationYealdInfosByRefDestination(harvestingActionDto.getValorisationDtos());
        Map<String, RefDestination> refDestinationByIds = Maps.uniqueIndex(referentialService.loadDestinationsForIds(valorisationDestinationGlobalYealAverages.keySet()), TopiaEntity::getTopiaId);

        Map<String, List<HarvestingActionValorisationDto>> sameValorisationDispachedOnSpecies = new HashMap<>();
        for (Map.Entry<String, Pair<HarvestingActionValorisationDto, Pair<Integer, Double>>> refDestinationPairEntry : valorisationDestinationGlobalYealAverages.entrySet()) {

            List<HarvestingActionValorisationDto> allSpeciesValorisation = new ArrayList<>();
            final String destinationId = refDestinationPairEntry.getKey();
            sameValorisationDispachedOnSpecies.put(destinationId, allSpeciesValorisation);

            for (Map.Entry<String, CroppingPlanSpecies> targetedSpeciesStadesSpeciesByCode : targetedSpeciesStadesSpeciesCodes.entrySet()) {
                CroppingPlanSpecies targetedSpecies = targetedSpeciesStadesSpeciesByCode.getValue();
                RefEspece targetedRefEspece = targetedSpecies.getSpecies();
                String key = targetedRefEspece.getCode_espece_botanique() + RefSpeciesToSectorTopiaDao.SEPARATOR + Strings.emptyToNull(targetedRefEspece.getCode_qualifiant_AEE());
                List<Sector> sectorsForTargetedSpecies = sectorByCodeEspeceBotaniqueCodeQualifiant.get(key);

                Pair<HarvestingActionValorisationDto, Pair<Integer, Double>> harvestingActionValorisationToYealdAverage = refDestinationPairEntry.getValue();
                HarvestingActionValorisationDto fromValorisation = harvestingActionValorisationToYealdAverage.getLeft();

                final RefDestination destination = refDestinationByIds.get(fromValorisation.getDestinationId());
                if (destination == null) {
                    continue;
                }

                boolean valid = fromValorisation.getYealdAverage() > 0 && CollectionUtils.isNotEmpty(sectorsForTargetedSpecies) && sectorsForTargetedSpecies.contains(destination.getSector());
                valid &= destination.getCode_espece_botanique() == null || destination.getCode_espece_botanique().equals(targetedRefEspece.getCode_espece_botanique());
                valid &= destination.getCode_qualifiant_AEE() == null || destination.getCode_qualifiant_AEE().equals(targetedRefEspece.getCode_qualifiant_AEE());

                if (!valid) {
                    continue;
                }

                final HarvestingActionValorisationDto newHarvestingActionValorisationDto = fromValorisation.toBuilder()
                        .topiaId(null)
                        .speciesCode(targetedSpeciesStadesSpeciesByCode.getValue().getCode())
                        .yealdAverage(harvestingActionValorisationToYealdAverage.getRight().getRight())
                        .build();

                allSpeciesValorisation.add(newHarvestingActionValorisationDto);
            }
        }
        // dispatch yeald average on species
        for (Map.Entry<String, List<HarvestingActionValorisationDto>> destinationValorisations : sameValorisationDispachedOnSpecies.entrySet()) {
            List<HarvestingActionValorisationDto> speciesValorisation = destinationValorisations.getValue();
            int nbValidSpeciesForDestination = speciesValorisation.size();
            for (HarvestingActionValorisationDto valorisation : speciesValorisation) {
                valorisation = valorisation.toBuilder()
                        .topiaId(null)
                        .yealdAverage(valorisation.getYealdAverage() / nbValidSpeciesForDestination)
                        .build();
                newValorisationDtos.add(valorisation);
            }
        }

        final HarvestingActionDto result = harvestingActionDto.toBuilder()
                .topiaId(null)
                .wineValorisations(wineValorisations)
                .valorisationDtos(newValorisationDtos)
                .build();

        return result;
    }

    private Map<String, CroppingPlanSpecies> getTargetedSpeciesStadesSpeciesCodes(Map<String, CroppingPlanSpecies> targetedSpeciesByCode, List<SpeciesStadeDto> targetedSpeciesStadeDtos) {
        Map<String, CroppingPlanSpecies> targetedSpeciesStadesSpeciesCodes = new HashMap<>();

        Map<String, SpeciesStadeDto> speciesStadesBySpeciesCodes = null;
        if (targetedSpeciesStadeDtos != null) {
            speciesStadesBySpeciesCodes = Maps.uniqueIndex(targetedSpeciesStadeDtos, SpeciesStadeDto::getSpeciesCode);
        }

        for (Map.Entry<String, CroppingPlanSpecies> targetedSpeciesForCode : targetedSpeciesByCode.entrySet()) {
            if (speciesStadesBySpeciesCodes != null) {
                SpeciesStadeDto speciesStadeDto = speciesStadesBySpeciesCodes.get(targetedSpeciesForCode.getKey());
                if (speciesStadeDto != null) {
                    targetedSpeciesStadesSpeciesCodes.put(targetedSpeciesForCode.getKey(), targetedSpeciesForCode.getValue());
                }
            } else {
                // in case of no species stades are defined for the intervention, all species are selected
                targetedSpeciesStadesSpeciesCodes.put(targetedSpeciesForCode.getKey(), targetedSpeciesForCode.getValue());
            }
        }
        return targetedSpeciesStadesSpeciesCodes;
    }

    protected List<HarvestingActionValorisationDto> doEffectiveSimpleValorisationsSpeciesMigration(
            Map<String, CroppingPlanSpecies> targetedSpeciesByCode,
            Map<String, String> fromSpeciesCodeToSpeciesCode,
            Collection<HarvestingActionValorisationDto> valorisationDtos) {

        // the folowings maps are used to compute yeald average change
        Map<String, HarvestingActionValorisationDto> removedValorisationByDestinationIds = new HashMap<>();
        Map<String, List<HarvestingActionValorisationDto>> speciesValorisationByDestinationIds = new HashMap<>();

        for (HarvestingActionValorisationDto valorisationDto : valorisationDtos) {
            // only add main valorisation or the new species valorisations that match the targeted species
            // first try to find according species code but if code does not match
            // try find using refEspece matching species

            // if some valorisations are removed the removed averageYeald is evenly distribute
            // between same destination valorisations
            // ! Attention dans le cas de changement de culture, il faut appliquer la deuxième règle
            CroppingPlanSpecies toCroppingPlanSpecies = targetedSpeciesByCode.get(valorisationDto.getSpeciesCode());

            if (toCroppingPlanSpecies != null) {

                valorisationDto = valorisationDto
                        .toBuilder()
                        .topiaId(null)
                        .speciesCode(toCroppingPlanSpecies.getCode())
                        .build();

                // same species, code valorisation is valid
                addToValorisationsForDestinationId(speciesValorisationByDestinationIds, valorisationDto);

            } else {
                String toSpeciesCode = fromSpeciesCodeToSpeciesCode.get(valorisationDto.getSpeciesCode());
                if (StringUtils.isNotBlank(toSpeciesCode)) {
                    // species code change but matching species found, validation is valid and new species code set
                    valorisationDto = valorisationDto.toBuilder()
                            .topiaId(null)
                            .speciesCode(toSpeciesCode)
                            .build();

                    // add valorisation to the new valorisations list
                    addToValorisationsForDestinationId(speciesValorisationByDestinationIds, valorisationDto);
                } else {
                    // valorisation species is not found so the valorisation is not valid and has to be removed
                    removedValorisationByDestinationIds.put(valorisationDto.getDestinationId(), valorisationDto);
                }
            }
        }

        final List<HarvestingActionValorisationDto> harvestingActionValorisationDtos =
                computeNewSpeciesDestinationYealdAverage(removedValorisationByDestinationIds, speciesValorisationByDestinationIds);

        return harvestingActionValorisationDtos;
    }

    public void addNewSubstrateInputUsageDto(
            Collection<SubstrateInputUsageDto> newSubstrateInputUsageDtos,
            Map<String, AbstractDomainInputStockUnit> domainInputDtoByCode,
            Optional<Collection<SubstrateInputUsageDto>> optionalSubstrateInputUsageDtos) {
        optionalSubstrateInputUsageDtos.ifPresent(
                substrateInputUsageDtos -> substrateInputUsageDtos.forEach(
                        substrateInputUsageDto -> {
                            final AbstractDomainInputStockUnit domainInputDto = domainInputDtoByCode.get(substrateInputUsageDto.getDomainSubstrateInputDto().getCode());
                            if (domainInputDto != null) {
                                DomainSubstrateInputDto domainSubstrateInputDto = DomainInputBinder.domainSubstrateInputDtoBinder(domainInputDto, null);
                                final SubstrateInputUsageDto clonedSubstrateInputUsageDto = substrateInputUsageDto.toBuilder()
                                        .inputUsageTopiaId(null)
                                        .domainSubstrateInputDto(domainSubstrateInputDto)
                                        .build();
                                newSubstrateInputUsageDtos.add(clonedSubstrateInputUsageDto);
                            }
                        }));
    }

    protected Pair<SeedingActionUsageDto, Boolean> migrateEffectiveSeedingActionToTargetedSpecies(
            Map<String, CroppingPlanSpecies> targetedSpeciesByCode,
            Map<String, String> fromSpeciesCodeToSpeciesCode,
            SeedingActionUsageDto actionDto,
            Map<String, List<AbstractDomainInputStockUnit>> toDomainInputStockUnitByKeys) {

        List<SeedLotInputUsageDto> newSeedingLotInputUsages = new ArrayList<>();
        Collection<SubstrateInputUsageDto> newSubstrateInputUsageDtos = new ArrayList<>();
        SeedingActionUsageDto migratedSeedingActionUsage = actionDto.toBuilder()
                .seedLotInputUsageDtos(newSeedingLotInputUsages)
                .substrateInputUsageDtos(newSubstrateInputUsageDtos)
                .build();

        final List<AbstractDomainInputStockUnit> toDomainInputStockUnits = toDomainInputStockUnitByKeys.values().stream().flatMap(Collection::stream).toList();
        Map<String, AbstractDomainInputStockUnit> toDomainInputStockUnitByCodes = Maps.uniqueIndex(toDomainInputStockUnits, AbstractDomainInputStockUnit::getCode);

        addNewSubstrateInputUsageDto(
                newSubstrateInputUsageDtos,
                toDomainInputStockUnitByCodes,
                actionDto.getSubstrateInputUsageDtos());

        if (fromSpeciesCodeToSpeciesCode.isEmpty()) {
            // no species match
            return Pair.of(migratedSeedingActionUsage, false);
        }

        final Collection<SeedLotInputUsageDto> fromSeedLotInputUsageDtos = actionDto.getSeedLotInputUsageDtos();

        boolean fullSeedingActionMigrated = true;

        for (SeedLotInputUsageDto fromSeedLotInputUsageDto : fromSeedLotInputUsageDtos) {
            final DomainSeedLotInputDto fromDomainSeedLotInputDto = fromSeedLotInputUsageDto.getDomainSeedLotInputDto();
            final String seedLotInputCode = fromDomainSeedLotInputDto.getCode();

            // 1st try find with same Code
            DomainSeedLotInput toDomainSeedLotInput = (DomainSeedLotInput) toDomainInputStockUnitByCodes.get(seedLotInputCode);
            // 2nt try find from key

            Map<Integer, DomainSeedLotInput> targetedLotByScore;
            if (toDomainSeedLotInput == null) {
                final List<DomainSeedSpeciesInputDto> fromSpeciesInputDtos = fromDomainSeedLotInputDto.getSpeciesInputs();

                AtomicInteger bestScore = DomainInputStockUnitServiceImpl.computeBestScore(fromSpeciesInputDtos);
                // try finding a lot base on same crop, organic and usageUnit
                targetedLotByScore = DomainInputStockUnitServiceImpl.tryFindToDomainSeedLotInput(toDomainInputStockUnitByKeys, fromDomainSeedLotInputDto);

                final Integer max = targetedLotByScore.keySet().stream().max(Comparator.comparingInt(Integer::valueOf)).orElse(0);
                toDomainSeedLotInput = targetedLotByScore.get(max);
                fullSeedingActionMigrated = bestScore.get() == max;
            }

            if (toDomainSeedLotInput != null) {

                List<SeedSpeciesInputUsageDto> seedSpeciesInputUsageDtos = toNewSeedSpeciesInputUsageDtos(
                        targetedSpeciesByCode,
                        fromSeedLotInputUsageDto.getSeedingSpeciesDtos(),
                        toDomainSeedLotInput.getDomainSeedSpeciesInput(),
                        DomainInputStockUnitServiceImpl.getDomainSeedSpeciesInputByKeys(toDomainSeedLotInput.getDomainSeedSpeciesInput()));

                if (CollectionUtils.isNotEmpty(seedSpeciesInputUsageDtos)) {

                    SeedLotInputUsageDto toSeedLotInputUsageDto = fromSeedLotInputUsageDto.toBuilder()
                            .inputUsageTopiaId(null)
                            .croppingPlanEntryIdentifier(toDomainSeedLotInput.getCropSeed().getTopiaId())
                            .seedingSpeciesDtos(seedSpeciesInputUsageDtos)
                            .build();
                    newSeedingLotInputUsages.add(toSeedLotInputUsageDto);
                }
            }
        }

        return Pair.of(migratedSeedingActionUsage, fullSeedingActionMigrated && !newSeedingLotInputUsages.isEmpty());
    }

    protected List<SeedSpeciesInputUsageDto> toNewSeedSpeciesInputUsageDtos(
            Map<String, CroppingPlanSpecies> targetedSpeciesByCode,
            Optional<List<SeedSpeciesInputUsageDto>> fromSeedingSpeciesDtos,
            Collection<DomainSeedSpeciesInput> domainSeedSpeciesInput,
            Map<String, AbstractDomainInputStockUnit> toDomainSeedSpeciesInputAndPhytoByKeys) {

        List<SeedSpeciesInputUsageDto> seedSpeciesInputUsageDtos = null;
        if (fromSeedingSpeciesDtos.isPresent() && CollectionUtils.isNotEmpty(domainSeedSpeciesInput)) {
            seedSpeciesInputUsageDtos = new ArrayList<>();
            final List<SeedSpeciesInputUsageDto> fromSeedSpeciesInputUsageDtos = fromSeedingSpeciesDtos.get();
            for (SeedSpeciesInputUsageDto fromSeedSpeciesInputUsageDto : fromSeedSpeciesInputUsageDtos) {
                SeedSpeciesInputUsageDto newSeedSpeciesInputUsageDto = toNewSeedSpeciesInputUsageDto(
                        targetedSpeciesByCode,
                        toDomainSeedSpeciesInputAndPhytoByKeys,
                        fromSeedSpeciesInputUsageDto);
                if (newSeedSpeciesInputUsageDto != null) {
                    seedSpeciesInputUsageDtos.add(newSeedSpeciesInputUsageDto);
                }
            }
        }
        return seedSpeciesInputUsageDtos;
    }

    protected SeedSpeciesInputUsageDto toNewSeedSpeciesInputUsageDto(
            Map<String, CroppingPlanSpecies> targetedSpeciesByCode,
            Map<String, AbstractDomainInputStockUnit> toDomainSeedSpeciesInputAndPhytoByKeys,
            SeedSpeciesInputUsageDto fromSeedSpeciesInputUsageDto) {

        SeedSpeciesInputUsageDto toSeedSpeciesInputUsageDto = null;
        String domainSpeciesKey = fromSeedSpeciesInputUsageDto.getDomainSeedSpeciesInputDto().getKey();
        DomainSeedSpeciesInput toDomainSeedSpeciesInput = (DomainSeedSpeciesInput) toDomainSeedSpeciesInputAndPhytoByKeys.get(domainSpeciesKey);
        if (toDomainSeedSpeciesInput != null) {
            CroppingPlanSpecies speciesSeed = toDomainSeedSpeciesInput.getSpeciesSeed();
            if (targetedSpeciesByCode.get(speciesSeed.getCode()) != null) {
                List<PhytoProductInputUsageDto> seedProductInputDtos = null;

                Optional<Collection<PhytoProductInputUsageDto>> optionalSeedProductInputDtos = fromSeedSpeciesInputUsageDto.getSeedProductInputDtos();
                if (optionalSeedProductInputDtos.isPresent()) {
                    seedProductInputDtos = new ArrayList<>();
                    Collection<PhytoProductInputUsageDto> phytoProductInputUsageDtos = optionalSeedProductInputDtos.get();
                    for (PhytoProductInputUsageDto phytoProductInputUsageDto : phytoProductInputUsageDtos) {
                        toNewSeedSpeciesPhytoUsage(
                                toDomainSeedSpeciesInputAndPhytoByKeys,
                                seedProductInputDtos,
                                phytoProductInputUsageDto);
                    }
                }

                toSeedSpeciesInputUsageDto = toNewSeedSpeciesInputUsageDto(
                        fromSeedSpeciesInputUsageDto,
                        toDomainSeedSpeciesInput,
                        seedProductInputDtos);
            }
        }
        return toSeedSpeciesInputUsageDto;
    }

    protected SeedSpeciesInputUsageDto toNewSeedSpeciesInputUsageDto(
            SeedSpeciesInputUsageDto fromSeedSpeciesInputUsageDto,
            DomainSeedSpeciesInput toDomainSeedSpeciesInput,
            List<PhytoProductInputUsageDto> seedProductInputDtos) {

        CroppingPlanSpecies speciesSeed = toDomainSeedSpeciesInput.getSpeciesSeed();
        CroppingPlanSpeciesDto croppingPlanSpeciesDto = CroppingPlans.getMinimalDtoForCroppingPlanSpecies(speciesSeed);
        DomainSeedSpeciesInputDto domainSeedSpeciesInputDto = DomainInputBinder.getDomainSeedSpeciesInputDto(toDomainSeedSpeciesInput, croppingPlanSpeciesDto);
        SeedSpeciesInputUsageDto toSeedSpeciesInputUsageDto = fromSeedSpeciesInputUsageDto.toBuilder()
                .inputUsageTopiaId(null)
                .domainSeedSpeciesInputDto(domainSeedSpeciesInputDto)
                .speciesIdentifier(speciesSeed.getTopiaId())
                .seedProductInputDtos(seedProductInputDtos)
                .build();

        return toSeedSpeciesInputUsageDto;
    }

    protected void toNewSeedSpeciesPhytoUsage(Map<String, AbstractDomainInputStockUnit> toDomainSeedSpeciesInputAndPhytoByKeys, List<PhytoProductInputUsageDto> seedProductInputDtos, PhytoProductInputUsageDto phytoProductInputUsageDto) {
        DomainPhytoProductInputDto domainPhytoProductInputDto = phytoProductInputUsageDto.getDomainPhytoProductInputDto();
        DomainPhytoProductInput toDomainPhytoProductInput = (DomainPhytoProductInput) toDomainSeedSpeciesInputAndPhytoByKeys.get(domainPhytoProductInputDto.getKey());
        if (toDomainPhytoProductInput != null) {
            DomainPhytoProductInputDto toDomainPhytoProductInputDto = DomainInputBinder.domainPhytoProductInputDtoBinder(toDomainPhytoProductInput);
            PhytoProductInputUsageDto toProductInputUsageDto = phytoProductInputUsageDto.toBuilder()
                    .inputUsageTopiaId(null)
                    .domainPhytoProductInputDto(toDomainPhytoProductInputDto)
                    .build();

            seedProductInputDtos.add(toProductInputUsageDto);
        }
    }

    protected Map<String, Pair<HarvestingActionValorisationDto, Pair<Integer, Double>>> getValorisationYealdInfosByRefDestination(Collection<HarvestingActionValorisationDto> valorisationDtos) {
        Map<String, Pair<HarvestingActionValorisationDto, Pair<Integer, Double>>> valorisationDestinationGlobalYealAverages = new HashMap<>();
        for (HarvestingActionValorisationDto valorisationDto : valorisationDtos) {
            if (valorisationDto.getYealdAverage() != 0) {
                Pair<HarvestingActionValorisationDto, Pair<Integer, Double>> valorisationDtoForDestination = valorisationDestinationGlobalYealAverages.get(valorisationDto.getDestinationId());

                double globalYealdAverageForDestination;
                int nbSpeciesForDestination;
                if (valorisationDtoForDestination == null) {
                    globalYealdAverageForDestination = valorisationDto.getYealdAverage();
                    nbSpeciesForDestination = 1;
                } else {
                    Pair<Integer, Double> p0 = valorisationDtoForDestination.getValue();
                    globalYealdAverageForDestination = p0.getRight() + valorisationDto.getYealdAverage();
                    nbSpeciesForDestination = p0.getLeft() + 1;
                    valorisationDto = mergeQualityCriteria(valorisationDtoForDestination, valorisationDto);
                }
                Pair<Integer, Double> nbSpeciesToGlobalYealdAvergage = Pair.of(nbSpeciesForDestination, globalYealdAverageForDestination);
                valorisationDtoForDestination = Pair.of(valorisationDto, nbSpeciesToGlobalYealdAvergage);
                valorisationDestinationGlobalYealAverages.put(valorisationDto.getDestinationId(), valorisationDtoForDestination);
            }
        }
        return valorisationDestinationGlobalYealAverages;
    }

    protected HarvestingActionValorisationDto mergeQualityCriteria(Pair<HarvestingActionValorisationDto, Pair<Integer, Double>> valorisationForDestination, HarvestingActionValorisationDto valorisationDto) {
        Collection<QualityCriteriaDto> currentQualityCriteria = valorisationDto.getQualityCriteriaDtos().orElse(new ArrayList<>());

        if (currentQualityCriteria.isEmpty()) return valorisationDto;

        List<QualityCriteriaDto> newQualityCriteriaDto = new ArrayList<>();
        HarvestingActionValorisationDto prevVal = valorisationForDestination.getLeft();
        Collection<QualityCriteriaDto> prevQualityCriteriaDto = prevVal.getQualityCriteriaDtos().orElse(new ArrayList<>());
        Map<String, QualityCriteriaDto> qualityCriteriaByRef = Maps.uniqueIndex(prevQualityCriteriaDto, QualityCriteriaDto::getRefQualityCriteriaId);


        final Collection<RefQualityCriteria> refQualityCriteries = !qualityCriteriaByRef.isEmpty() ? referentialService.loadRefQualityCriteriaForIds(qualityCriteriaByRef.keySet()) : new ArrayList<>();
        Map<String, RefQualityCriteria> refQualityCriteriaByIds = Maps.uniqueIndex(refQualityCriteries, TopiaEntity::getTopiaId);

        for (QualityCriteriaDto currentQualityCriterion : currentQualityCriteria) {
            QualityCriteriaDto prevQualityCriterion = qualityCriteriaByRef.get(currentQualityCriterion.getRefQualityCriteriaId());
            if (prevQualityCriterion == null) {
                newQualityCriteriaDto.add(currentQualityCriterion.toBuilder().topiaId(null).build());
            } else {

                final String refQualityCriteriaId = prevQualityCriterion.getRefQualityCriteriaId();
                final RefQualityCriteria refQualityCriteria1 = refQualityCriteriaByIds.get(refQualityCriteriaId);
                QualityAttributeType type = Objects.requireNonNull(refQualityCriteria1).getQualityAttributeType();

                boolean binaryValue = false;
                double quantitativeValue = 0;
                switch (type) {
                    case BINAIRE ->
                        // si le critère a été à la fois vrai et faux, faux l'emporte
                            binaryValue = prevQualityCriterion.getBinaryValue().equals(currentQualityCriterion.getBinaryValue()) ? currentQualityCriterion.getBinaryValue() : false;
                    case QUANTITATIVE ->
                            quantitativeValue = prevQualityCriterion.getQuantitativeValue() + currentQualityCriterion.getQuantitativeValue();
                    case QUALITATIVE -> {
                    }
                    // difficile de choisir une classe de critère de qualité plutôt qu'une autre, il n'y a pas de poid qui permet de distinger une classe plus importante que l'aute
                    // on garde celle d'origine.
                }
                newQualityCriteriaDto.add(prevQualityCriterion.toBuilder()
                        .topiaId(null)
                        .binaryValue(binaryValue)
                        .quantitativeValue(quantitativeValue)
                        .build());
            }
        }

        final HarvestingActionValorisationDto result = valorisationDto.toBuilder()
                .topiaId(null)
                .qualityCriteriaDtos(newQualityCriteriaDto)
                .build();
        return result;
    }

    protected List<HarvestingActionValorisationDto> computeNewSpeciesDestinationYealdAverage(
            Map<String, HarvestingActionValorisationDto> removedValorisationByDestinationIds,
            Map<String, List<HarvestingActionValorisationDto>> speciesValorisationByDestinationIds_) {

        final HashMap<String, List<HarvestingActionValorisationDto>> speciesValorisationByDestinationIds = new HashMap<>(speciesValorisationByDestinationIds_);
        List<HarvestingActionValorisationDto> newValorisationDtos = new ArrayList<>();
        for (Map.Entry<String, HarvestingActionValorisationDto> entry : removedValorisationByDestinationIds.entrySet()) {
            String destinationId = entry.getKey();
            HarvestingActionValorisationDto removedValorisation = entry.getValue();
            double removedYealdAverage = removedValorisation.getYealdAverage();

            List<HarvestingActionValorisationDto> speciesValorisationsForDestination = speciesValorisationByDestinationIds.remove(destinationId);

            if (CollectionUtils.isNotEmpty(speciesValorisationsForDestination)) {
                double part = removedYealdAverage / speciesValorisationsForDestination.size();

                for (HarvestingActionValorisationDto valorisation : speciesValorisationsForDestination) {
                    newValorisationDtos.add(valorisation.toBuilder()
                            .topiaId(null)
                            .yealdAverage(valorisation.getYealdAverage() + part)
                            .build());
                }
            }
        }
        newValorisationDtos.addAll(speciesValorisationByDestinationIds.values().stream().flatMap(Collection::stream).toList());
        return newValorisationDtos;
    }

    protected void addToValorisationsForDestinationId(Map<String, List<HarvestingActionValorisationDto>> map, HarvestingActionValorisationDto valorisation) {
        List<HarvestingActionValorisationDto> speciesValorisationsForDestinationId = map.computeIfAbsent(valorisation.getDestinationId(), k -> Lists.newArrayList());
        speciesValorisationsForDestinationId.add(valorisation);
    }

    @Override
    public Collection<HarvestingAction> getHarvestingActionForGrowingSystem(GrowingSystem growingSystem) {
        return harvestingActionDao.findAllHarvestingActionForGrowingSystem(growingSystem);
    }

    @Override
    public void updateAllValorisations(Collection<HarvestingAction> harvestingActions) {
        Set<HarvestingActionValorisation> allValorisations = new HashSet<>();
        Set<HarvestingActionValorisationTopiaDao.UniqueKeyValorisation> uniqueKeyValorisations = new HashSet<>();
        harvestingActions.forEach(ha -> {
            Collection<HarvestingActionValorisation> valorisations = CollectionUtils.emptyIfNull(ha.getValorisations());
            valorisations.forEach(
                    val -> {
                        HarvestingActionValorisationTopiaDao.UniqueKeyValorisation key = new HarvestingActionValorisationTopiaDao.UniqueKeyValorisation(
                                ha,
                                val.getSpeciesCode(),
                                val.getDestination()
                        );
                        if (!uniqueKeyValorisations.contains(key)) {
                            allValorisations.add(val);
                            uniqueKeyValorisations.add(key);
                        } else {
                            if (LOGGER.isErrorEnabled()) {
                                LOGGER.error(String.format("Valorisation en double remontée sur l'action %s %s", ha.getTopiaId(), key));
                            }
                        }
                    }
            );
        });
        harvestingActionValorisationDao.updateAll(allValorisations);
    }

    @Override
    public void createOrUpdatePracticedInterventionActionAndUsages(
            PracticedIntervention intervention,
            Collection<AbstractActionDto> actionDtos,
            Collection<AbstractDomainInputStockUnit> domainInputStockUnits,
            PracticedSystem practicedSystem,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant,
            List<SpeciesStadeDto> speciesStadesDtos) {

        if (actionDtos == null) return;

        Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds = Maps.uniqueIndex(CollectionUtils.emptyIfNull(domainInputStockUnits), TopiaEntity::getTopiaId);

        final String cropCode = getPracticedInterventionCropCode(intervention);

        String globalContext = String.format("Système synthétisé: %s (%s, %d), Culture code %s, Intervention (%s) %s :",
                practicedSystem.getTopiaId(),
                practicedSystem.getName(),
                practicedSystem.getGrowingSystem().getGrowingPlan().getDomain().getCampaign(),
                cropCode,
                ObjectUtils.firstNonNull(intervention.getTopiaId(), "?"),
                intervention.getName());

        actionDtos = validActionDtosAndUsages0(
                globalContext,
                actionDtos,
                domainInputStockUnitByIds,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspeceBotaniqueCodeQualifiant,
                speciesStadesDtos,
                intervention.isIntermediateCrop());

        // remaining Action will be removed
        Map<String, AbstractAction> persistedActionByIds = new HashMap<>(Maps.uniqueIndex(abstractActionDao.forPracticedInterventionEquals(intervention).findAll(), TopiaEntity::getTopiaId));
        ImmutableList<String> toolsCouplingCodes = getCropCycleToolsCouplingCodes(intervention, null);
        Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds = Maps.uniqueIndex(refInterventionAgrosystTravailEdiDao.findAll(), TopiaEntity::getTopiaId);

        Set<String> speciesCodes = CollectionUtils.emptyIfNull(intervention.getSpeciesStades()).stream().map(PracticedSpeciesStade::getSpeciesCode).collect(Collectors.toSet());
        Collection<CroppingPlanSpecies> croppingPlanSpecies = croppingPlanSpeciesDao.forCodeIn(speciesCodes).findAll();

        // à valider si pas redondant avec speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
        // probablement nécessaire dans le cas ou la culture n'est pas directement liée au domaine du synthétisé
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(croppingPlanSpecies.stream().map(CroppingPlanSpecies::getCode).collect(Collectors.toSet()));

        Domain domain = practicedSystem.getGrowingSystem().getGrowingPlan().getDomain();

        for (AbstractActionDto actionDto : actionDtos) {

            RefInterventionAgrosystTravailEDI mainAction = mainActionByIds.get(actionDto.getMainActionId());
            if (mainAction == null) {
                throw new AgrosystTechnicalException(String.format("MainAction with id '%s' doesn't exist", actionDto.getMainActionId()));
            }

            AgrosystInterventionType agrosystInterventionType = mainAction.getIntervention_agrosyst();
            switch (agrosystInterventionType) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX ->
                        createOrUpdateMineralFertilizersSpreadingAction(actionDto, persistedActionByIds, intervention, null, mainActionByIds, domainInputStockUnitByIds, toolsCouplingCodes);
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES ->
                        createOrUpdatePesticidesSpreadingAction(actionDto, persistedActionByIds, intervention, null, mainActionByIds, domainInputStockUnitByIds, toolsCouplingCodes);
                case AUTRE ->
                        createOrUpdateOtherAction(actionDto, persistedActionByIds, intervention, null, mainActionByIds, domainInputStockUnitByIds, toolsCouplingCodes);
                case ENTRETIEN_TAILLE_VIGNE_ET_VERGER ->
                        createOrUpdateMaintenancePruningVinesAction(actionDto, persistedActionByIds, intervention, null, mainActionByIds, domainInputStockUnitByIds, toolsCouplingCodes);
                case EPANDAGES_ORGANIQUES ->
                        createOrUpdateOrganicFertilizersSpreadingAction(actionDto, persistedActionByIds, intervention, null, mainActionByIds, domainInputStockUnitByIds, toolsCouplingCodes);
                case IRRIGATION ->
                        createOrUpdateIrrigAction(actionDto, persistedActionByIds, intervention, null, mainActionByIds, domainInputStockUnitByIds, toolsCouplingCodes, domain);
                case LUTTE_BIOLOGIQUE ->
                        createOrUpdateBiologicalControlAction(actionDto, persistedActionByIds, intervention, null, mainActionByIds, domainInputStockUnitByIds, toolsCouplingCodes);
                case RECOLTE ->
                        createOrUpdatePracticedHarvestingAction(actionDto, persistedActionByIds, intervention, croppingPlanSpecies, mainActionByIds, practicedSystem, domainInputStockUnitByIds, codeEspBotCodeQualiBySpeciesCode, toolsCouplingCodes);
                case SEMIS ->
                        createOrUpdateSeedingActionUsage(actionDto, persistedActionByIds, intervention, null, cropCode, speciesCodes, mainActionByIds, toolsCouplingCodes, domainInputStockUnitByIds);
                case TRANSPORT ->
                        createOrUpdateCarriageAction(actionDto, persistedActionByIds, intervention, null, mainActionByIds, toolsCouplingCodes);
                case TRAVAIL_DU_SOL ->
                        createOrUpdateTillageAction(actionDto, persistedActionByIds, intervention, null, mainActionByIds, toolsCouplingCodes);
            }
        }

        removeActionsAndUsagesDeps(persistedActionByIds.values());

    }

    @Override
    public void createOrUpdateEffectiveInterventionActionAndUsages(
            EffectiveIntervention intervention,
            Collection<AbstractActionDto> actionDtos,
            Collection<AbstractDomainInputStockUnit> domainInputStockUnits,
            Zone zone,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            List<SpeciesStadeDto> speciesStadesDtos,
            CroppingPlanEntry interventionCrop,
            boolean isIntermediateIntervention) {

        if (actionDtos == null) return;

        Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds = Maps.uniqueIndex(CollectionUtils.emptyIfNull(domainInputStockUnits), TopiaEntity::getTopiaId);

        String globalContext = String.format("Zone: %s (%s), Culture (%s) %s, Intervention (%s) %s :",
                zone.getTopiaId(),
                zone.getName(),
                interventionCrop.getTopiaId(),
                interventionCrop.getName(),
                ObjectUtils.firstNonNull(intervention.getTopiaId(), "?"),
                intervention.getName());

        actionDtos = validActionDtosAndUsages0(
                globalContext,
                actionDtos,
                domainInputStockUnitByIds,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspceBotaniqueCodeQualifiant,
                speciesStadesDtos,
                isIntermediateIntervention);

        // remaining Action will be removed
        Map<String, AbstractAction> persistedActionByIds = new HashMap<>(Maps.uniqueIndex(abstractActionDao.forEffectiveInterventionEquals(intervention).findAll(), TopiaEntity::getTopiaId));
        ImmutableList<String> toolsCouplingCodes = getCropCycleToolsCouplingCodes(null, intervention);
        Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds = Maps.uniqueIndex(refInterventionAgrosystTravailEdiDao.findAll(), TopiaEntity::getTopiaId);

        Set<String> speciesIds = CollectionUtils.emptyIfNull(
                        intervention.getSpeciesStades()).stream()
                .map(ess -> ess.getCroppingPlanSpecies().getTopiaId()).collect(Collectors.toSet());
        Collection<CroppingPlanSpecies> croppingPlanSpecies = croppingPlanSpeciesDao.forTopiaIdIn(speciesIds).findAll();

        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(croppingPlanSpecies.stream().map(CroppingPlanSpecies::getCode).collect(Collectors.toSet()));

        final String cropId = interventionCrop.getTopiaId();

        Domain domain = zone.getPlot().getDomain();

        for (AbstractActionDto actionDto : actionDtos) {

            RefInterventionAgrosystTravailEDI mainAction = mainActionByIds.get(actionDto.getMainActionId());
            if (mainAction == null) {
                throw new AgrosystTechnicalException(String.format("MainAction with id '%s' doesn't exist", actionDto.getMainActionId()));
            }

            AgrosystInterventionType agrosystInterventionType = mainAction.getIntervention_agrosyst();
            switch (agrosystInterventionType) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX ->
                        createOrUpdateMineralFertilizersSpreadingAction(actionDto, persistedActionByIds, null, intervention, mainActionByIds, domainInputStockUnitByIds, toolsCouplingCodes);
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES ->
                        createOrUpdatePesticidesSpreadingAction(actionDto, persistedActionByIds, null, intervention, mainActionByIds, domainInputStockUnitByIds, toolsCouplingCodes);
                case AUTRE ->
                        createOrUpdateOtherAction(actionDto, persistedActionByIds, null, intervention, mainActionByIds, domainInputStockUnitByIds, toolsCouplingCodes);
                case ENTRETIEN_TAILLE_VIGNE_ET_VERGER ->
                        createOrUpdateMaintenancePruningVinesAction(actionDto, persistedActionByIds, null, intervention, mainActionByIds, domainInputStockUnitByIds, toolsCouplingCodes);
                case EPANDAGES_ORGANIQUES ->
                        createOrUpdateOrganicFertilizersSpreadingAction(actionDto, persistedActionByIds, null, intervention, mainActionByIds, domainInputStockUnitByIds, toolsCouplingCodes);
                case IRRIGATION ->
                        createOrUpdateIrrigAction(actionDto, persistedActionByIds, null, intervention, mainActionByIds, domainInputStockUnitByIds, toolsCouplingCodes, domain);
                case LUTTE_BIOLOGIQUE ->
                        createOrUpdateBiologicalControlAction(actionDto, persistedActionByIds, null, intervention, mainActionByIds, domainInputStockUnitByIds, toolsCouplingCodes);
                case RECOLTE ->
                        createOrUpdateEffectiveHarvestingAction(actionDto, persistedActionByIds, intervention, croppingPlanSpecies, mainActionByIds, zone, domainInputStockUnitByIds, codeEspBotCodeQualiBySpeciesCode, toolsCouplingCodes);
                case SEMIS ->
                        createOrUpdateSeedingActionUsage(actionDto, persistedActionByIds, null, intervention, cropId, speciesIds, mainActionByIds, toolsCouplingCodes, domainInputStockUnitByIds);
                case TRANSPORT ->
                        createOrUpdateCarriageAction(actionDto, persistedActionByIds, null, intervention, mainActionByIds, toolsCouplingCodes);
                case TRAVAIL_DU_SOL ->
                        createOrUpdateTillageAction(actionDto, persistedActionByIds, null, intervention, mainActionByIds, toolsCouplingCodes);
            }
        }

        Collection<AbstractAction> actionToRemove = persistedActionByIds.values();

        removeActionsAndUsagesDeps(actionToRemove);

    }

    protected String getPracticedInterventionCropCode(PracticedIntervention intervention) {
        final String cropCode;
        PracticedCropCycleConnection connection = intervention.getPracticedCropCycleConnection();
        PracticedCropCyclePhase phase = intervention.getPracticedCropCyclePhase();
        if (intervention.isIntermediateCrop()) {
            cropCode = intervention.getPracticedCropCycleConnection().getIntermediateCroppingPlanEntryCode();
        } else if (phase != null) {
            cropCode = phase.getPracticedPerennialCropCycle().getCroppingPlanEntryCode();
        } else {
            cropCode = connection.getTarget().getCroppingPlanEntryCode();
        }
        return cropCode;
    }

    @Deprecated
    protected String getEffectiveInterventionCropId(EffectiveIntervention intervention) {
        final Collection<EffectiveSpeciesStade> speciesStades = CollectionUtils.emptyIfNull(intervention.getSpeciesStades());
        if (CollectionUtils.isNotEmpty(speciesStades)) {
            return speciesStades.stream()
                    .findFirst()
                    .map(EffectiveSpeciesStade::getCroppingPlanSpecies)
                    .map(CroppingPlanSpecies::getCroppingPlanEntry)
                    .map(CroppingPlanEntry::getTopiaId)
                    .orElse(null);
        } else if (intervention.getEffectiveCropCycleNode() != null) {
            return intervention.getEffectiveCropCycleNode().getCroppingPlanEntry().getTopiaId();
        } else {
            intervention.getEffectiveCropCyclePhase();
            return null;
        }
    }

    @Override
    public Collection<AbstractActionDto> loadPracticedActionsAndUsages(PracticedIntervention intervention) {
        List<AbstractAction> actions = abstractActionDao.forPracticedInterventionEquals(intervention).findAll();
        return transformeEntitiesToDtos(actions);
    }

    protected SeedingActionUsageDto bindSeedingActionEntityToDTO(AbstractAction action) {
        SeedingActionUsage entity = (SeedingActionUsage) action;

        final boolean isPracticed = action.getPracticedIntervention() != null;
        Collection<SeedLotInputUsage> seedLotInputUsages = entity.getSeedLotInputUsage();
        Collection<SeedLotInputUsageDto> seedLotInputUsageDtos = seedLotInputUsages.stream().map(
                seedLotInputUsage -> inputUsageService.bindSeedLotInputUsageEntityToDTO(seedLotInputUsage, isPracticed)).toList();

        Collection<OtherProductInputUsageDto> otherProductInputDtos = getOtherProductInputUsageDtos(entity.getOtherProductInputUsages());

        Collection<SubstrateInputUsageDto> substrateInputUsageDtos = CollectionUtils.emptyIfNull(entity.getSubstrateInputUsage()).stream().map(
                si -> inputUsageService.bindSubstrateInputUsageEntityToDTO(action, si)
        ).toList();

        final SeedingActionUsageDto.SeedingActionUsageDtoBuilder<?, ?> builder = SeedingActionUsageDto.builder();
        bindAbstractActionEntityToDTO(entity, builder);

        SeedingActionUsageDto dto = builder
                .deepness(entity.getDeepness())
                .yealdTarget(entity.getYealdTarget())
                .seedType(entity.getSeedType())
                .yealdUnit(entity.getYealdUnit())
                .seedLotInputUsageDtos(seedLotInputUsageDtos)
                .substrateInputUsageDtos(substrateInputUsageDtos)
                .otherProductInputUsageDtos(otherProductInputDtos)
                .build();

        return dto;
    }

    protected TillageActionDto bindTillageActionEntityToDTO(AbstractAction action) {
        TillageAction entity = (TillageAction) action;

        final TillageActionDto.TillageActionDtoBuilder<?, ?> builder = TillageActionDto.builder();
        bindAbstractActionEntityToDTO(entity, builder);

        TillageActionDto dto = builder
                .tillageDepth(entity.getTillageDepth())
                .otherSettingTool(entity.getOtherSettingTool())
                .build();

        return dto;
    }

    protected CarriageActionDto bindCarriageActionEntityToDTO(AbstractAction action) {
        CarriageAction entity = (CarriageAction) action;

        final CarriageActionDto.CarriageActionDtoBuilder<?, ?> builder = CarriageActionDto.builder();
        bindAbstractActionEntityToDTO(entity, builder);

        CarriageActionDto dto = builder
                .loadCapacity(entity.getLoadCapacity())
                .tripFrequency(entity.getTripFrequency())
                .capacityUnit(entity.getCapacityUnit())
                .build();
        return dto;
    }


    protected HarvestingActionDto bindHarvestingActionEntityToDTO(AbstractAction action) {

        HarvestingAction entity = (HarvestingAction) action;

        Collection<HarvestingActionValorisation> valorisations = CollectionUtils.emptyIfNull(entity.getValorisations());

        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(getSecurityContext().getLanguage());
        Set<String> destinationIds = valorisations.stream().filter(Objects::nonNull).map(val -> val.getDestination().getTopiaId()).collect(Collectors.toSet());
        refDestinationDao.fillTranslations(destinationIds, translationMap);

        List<HarvestingActionValorisationDto> valorisationDtos = new ArrayList<>(valorisations.stream()
                .filter(Objects::nonNull).map(
                        val -> bindHarvestingActionValorisationEntityToDTO(val, translationMap)).toList());

        final HarvestingActionDto.HarvestingActionDtoBuilder<?, ?> builder = HarvestingActionDto.builder();

        Collection<OtherProductInputUsageDto> otherProductInputDtos = getOtherProductInputUsageDtos(entity.getOtherProductInputUsages());

        bindAbstractActionEntityToDTO(entity, builder);

        HarvestingActionDto dto = builder
                .cattleCode(entity.getCattleCode())
                .pastureLoad(entity.getPastureLoad())
                .pasturingAtNight(entity.isPasturingAtNight())
                .wineValorisations(entity.getWineValorisations())
                .pastureType(entity.getPastureType())
                .otherProductInputUsageDtos(otherProductInputDtos)
                .valorisationDtos(valorisationDtos)
                .build();

        return dto;
    }

    protected HarvestingActionValorisationDto bindHarvestingActionValorisationEntityToDTO(HarvestingActionValorisation entity, ReferentialTranslationMap traductions) {

        ReferentialEntityTranslation entityTranslation = traductions.getEntityTranslation(entity.getDestination().getTopiaId());
        String destinationTraduction = entityTranslation.getPropertyTranslation(RefDestination.PROPERTY_DESTINATION, entity.getDestination().getDestination());
        entity.getDestination().setDestination_Translated(destinationTraduction);

        HarvestingActionValorisationDto dto = HarvestingActionValorisationDto.builder()
                .topiaId(entity.getTopiaId())
                .speciesCode(entity.getSpeciesCode())
                .salesPercent(entity.getSalesPercent())
                .selfConsumedPersent(entity.getSelfConsumedPersent())
                .noValorisationPercent(entity.getNoValorisationPercent())
                .yealdMin(entity.getYealdMin())
                .yealdMax(entity.getYealdMax())
                .yealdAverage(entity.getYealdAverage())
                .beginMarketingPeriod(entity.getBeginMarketingPeriod())
                .beginMarketingPeriodDecade(entity.getBeginMarketingPeriodDecade())
                .beginMarketingPeriodCampaign(entity.getBeginMarketingPeriodCampaign())
                .endingMarketingPeriod(entity.getEndingMarketingPeriod())
                .endingMarketingPeriodDecade(entity.getEndingMarketingPeriodDecade())
                .endingMarketingPeriodCampaign(entity.getEndingMarketingPeriodCampaign())
                .organicCrop(entity.isIsOrganicCrop())
                .yealdUnit(entity.getYealdUnit())
                .destinationId(entity.getDestination().getTopiaId())
                .destinationName(entity.getDestination().getDestination_Translated())
                .qualityCriteriaDtos(
                        new ArrayList<>(
                                CollectionUtils.emptyIfNull(
                                                entity.getQualityCriteria())
                                        .stream()
                                        .map(qc -> QualityCriteriaDto.builder()
                                                .topiaId(qc.getTopiaId())
                                                .quantitativeValue(qc.getQuantitativeValue())
                                                .binaryValue(qc.getBinaryValue())
                                                .refQualityCriteriaId(qc.getRefQualityCriteria().getTopiaId())

                                                .refQualityCriteriaClassId(
                                                        qc.getRefQualityCriteriaClass() != null ? qc.getRefQualityCriteriaClass().getTopiaId() : null)
                                                .build())
                                        .collect(Collectors.toList())))
                .build();
        return dto;
    }

    protected BiologicalControlActionDto bindBiologicalControlActionEntityToDTO(AbstractAction action) {

        BiologicalControlAction entity = (BiologicalControlAction) action;

        Collection<BiologicalProductInputUsage> biologicalProductInputUsages = CollectionUtils.emptyIfNull(entity.getBiologicalProductInputUsages());
        Collection<PhytoProductInputUsageDto> biologicalProductInputDtos = biologicalProductInputUsages.stream().map(
                i -> inputUsageService.bindPhytoProductInputUsageToDTO(i)).toList();

        Collection<OtherProductInputUsageDto> otherProductInputDtos = getOtherProductInputUsageDtos(entity.getOtherProductInputUsages());

        final BiologicalControlActionDto.BiologicalControlActionDtoBuilder<?, ?> builder = BiologicalControlActionDto.builder();
        bindAbstractActionEntityToDTO(entity, builder);

        BiologicalControlActionDto dto = builder
                .boiledQuantity(entity.getBoiledQuantity())
                .boiledQuantityPerTrip(entity.getBoiledQuantityPerTrip())
                .tripFrequency(entity.getTripFrequency())
                .proportionOfTreatedSurface(entity.getProportionOfTreatedSurface())
                .phytoProductInputUsageDtos(biologicalProductInputDtos)
                .otherProductInputUsageDtos(otherProductInputDtos)
                .build();

        return dto;
    }

    protected IrrigationActionDto bindIrrigActionEntityToDTO(AbstractAction action) {
        IrrigationAction entity = (IrrigationAction) action;

        IrrigationInputUsageDto irrigationInputUsageDto = entity.getIrrigationInputUsage() != null ?
                inputUsageService.bindIrrigationActionEntityToDTO(entity.getIrrigationInputUsage())
                : null;

        Collection<OtherProductInputUsageDto> otherProductInputDtos = getOtherProductInputUsageDtos(entity.getOtherProductInputUsages());

        final IrrigationActionDto.IrrigationActionDtoBuilder<?, ?> builder = IrrigationActionDto.builder();
        bindAbstractActionEntityToDTO(entity, builder);

        IrrigationActionDto dto = builder
                .waterQuantityAverage(entity.getWaterQuantityAverage())
                .waterQuantityMax(entity.getWaterQuantityMax())
                .waterQuantityAverage(entity.getWaterQuantityAverage())
                .waterQuantityMedian(entity.getWaterQuantityMedian())
                .azoteQuantity(entity.getAzoteQuantity())
                .irrigationInputUsageDto(irrigationInputUsageDto)
                .otherProductInputUsageDtos(otherProductInputDtos)
                .build();

        return dto;
    }

    protected OrganicFertilizersSpreadingActionDto bindOrganicFertilizersSpreadingActionEntityToDTO(AbstractAction action) {

        OrganicFertilizersSpreadingAction entity = (OrganicFertilizersSpreadingAction) action;


        Collection<OrganicProductInputUsage> organicProductInputs = CollectionUtils.emptyIfNull(entity.getOrganicProductInputUsages());
        Collection<OrganicProductInputUsageDto> organicProductInputDtos = organicProductInputs.stream().map(
                i -> inputUsageService.bindOrganicProductInputUsageEntityToDTO(i)).toList();

        Collection<OtherProductInputUsageDto> otherProductInputDtos = getOtherProductInputUsageDtos(entity.getOtherProductInputUsages());

        final OrganicFertilizersSpreadingActionDto.OrganicFertilizersSpreadingActionDtoBuilder<?, ?> builder = OrganicFertilizersSpreadingActionDto.builder();
        bindAbstractActionEntityToDTO(entity, builder);

        OrganicFertilizersSpreadingActionDto dto =
                builder
                        .landfilledWaste(entity.isLandfilledWaste())
                        .otherProductInputUsageDtos(otherProductInputDtos)
                        .organicProductInputUsageDtos(organicProductInputDtos)
                        .build();

        return dto;
    }

    protected MaintenancePruningVinesActionDto bindMaintenancePruningVinesActionEntityToDTO(AbstractAction action) {

        MaintenancePruningVinesAction entity = (MaintenancePruningVinesAction) action;

        Collection<OtherProductInputUsageDto> otherProductInputDtos = getOtherProductInputUsageDtos(entity.getOtherProductInputUsages());

        final MaintenancePruningVinesActionDto.MaintenancePruningVinesActionDtoBuilder<?, ?> builder = MaintenancePruningVinesActionDto.builder();
        bindAbstractActionEntityToDTO(entity, builder);

        MaintenancePruningVinesActionDto dto = builder
                .quantityUnitEDI(entity.getQuantityUnitEDI())
                .otherProductInputUsageDtos(otherProductInputDtos)
                .build();

        return dto;
    }

    protected OtherActionDto bindOtherActionEntityToDTO(AbstractAction action) {
        OtherAction entity = (OtherAction) action;

        Collection<OtherProductInputUsageDto> otherProductInputDtos = getOtherProductInputUsageDtos(entity.getOtherProductInputUsages());

        Collection<SubstrateInputUsage> substrateInputUsages = CollectionUtils.emptyIfNull(entity.getSubstrateInputUsages());
        Collection<SubstrateInputUsageDto> substrateInputUsageDtos = substrateInputUsages.stream().map(
                i -> inputUsageService.bindSubstrateInputUsageEntityToDTO(action, i)).toList();

        Collection<PotInputUsage> potInputUsages = CollectionUtils.emptyIfNull(entity.getPotInputUsages());
        Collection<PotInputUsageDto> potInputDtos = potInputUsages.stream().map(
                i -> inputUsageService.bindPotInputUsageToDTO(i)).toList();

        final OtherActionDto.OtherActionDtoBuilder<?, ?> builder = OtherActionDto.builder();
        bindAbstractActionEntityToDTO(entity, builder);

        OtherActionDto dto = builder
                .otherProductInputUsageDtos(otherProductInputDtos)
                .substrateInputUsageDtos(substrateInputUsageDtos)
                .potInputUsageDtos(potInputDtos)
                .build();

        return dto;
    }

    protected Collection<OtherProductInputUsageDto> getOtherProductInputUsageDtos(Collection<OtherProductInputUsage> entity) {
        Collection<OtherProductInputUsage> otherProductInputUsages = CollectionUtils.emptyIfNull(entity);
        Collection<OtherProductInputUsageDto> otherProductInputDtos = otherProductInputUsages.stream().map(
                i -> inputUsageService.bindOtherProductInputUsageEntityToDTO(i)).toList();
        return otherProductInputDtos;
    }

    public <E extends AbstractAction> void bindAbstractActionEntityToDTO(E entity, AbstractActionDto.AbstractActionDtoBuilder<?, ?> builder) {
        builder
                .mainActionId(entity.getMainAction().getTopiaId())
                .mainActionReference_code(entity.getMainAction().getReference_code())
                .mainActionInterventionAgrosyst(entity.getMainAction().getIntervention_agrosyst())
                .topiaId(entity.getTopiaId())
                .mainActionReference_label(entity.getMainAction().getReference_label_Translated())
                .comment(entity.getComment())
                .toolsCouplingCode(entity.getToolsCouplingCode())
                .practicedInterventionId(entity.getPracticedIntervention() != null ? entity.getPracticedIntervention().getTopiaId() : null)
                .effectiveInterventionId(entity.getEffectiveIntervention() != null ? entity.getEffectiveIntervention().getTopiaId() : null);
    }

    protected PesticidesSpreadingActionDto bindPesticidesSpreadingActionEntityToDTO(AbstractAction action) {
        PesticidesSpreadingAction entity = (PesticidesSpreadingAction) action;

        Collection<PesticideProductInputUsage> pesticideProductInputUsages = CollectionUtils.emptyIfNull(entity.getPesticideProductInputUsages());
        Collection<PhytoProductInputUsageDto> pesticideProductInputDtos = pesticideProductInputUsages.stream().map(
                i -> inputUsageService.bindPhytoProductInputUsageToDTO(i)).toList();

        Collection<OtherProductInputUsageDto> otherProductInputDtos = getOtherProductInputUsageDtos(entity.getOtherProductInputUsages());

        final PesticidesSpreadingActionDto.PesticidesSpreadingActionDtoBuilder<?, ?> builder = PesticidesSpreadingActionDto.builder();

        bindAbstractActionEntityToDTO(entity, builder);

        PesticidesSpreadingActionDto dto = builder
                .boiledQuantity(entity.getBoiledQuantity())
                .boiledQuantityPerTrip(entity.getBoiledQuantityPerTrip())
                .tripFrequency(entity.getTripFrequency())
                .antiDriftNozzle(entity.isAntiDriftNozzle())
                .proportionOfTreatedSurface(entity.getProportionOfTreatedSurface())
                .phytoProductInputUsageDtos(pesticideProductInputDtos)
                .otherProductInputUsageDtos(otherProductInputDtos)
                .build();

        return dto;
    }

    protected AbstractActionDto bindMineralFertilizersSpreadingActionEntityToDTO(AbstractAction action) {

        MineralFertilizersSpreadingAction entity = (MineralFertilizersSpreadingAction) action;
        MineralFertilizersSpreadingActionDto.MineralFertilizersSpreadingActionDtoBuilder<?, ?> builder = MineralFertilizersSpreadingActionDto.builder()
                .burial(entity.isBurial())
                .localizedSpreading(entity.isLocalizedSpreading());

        Collection<MineralProductInputUsageDto> mineralProductInputUsageDtos = CollectionUtils.emptyIfNull(entity.getMineralProductInputUsages())
                .stream().map(i -> inputUsageService.bindMineralProductInputUsageEntityToDTO(i)).toList();

        Collection<OtherProductInputUsageDto> otherProductInputUsageDtos = CollectionUtils.emptyIfNull(entity.getOtherProductInputUsages())
                .stream().map(i -> inputUsageService.bindOtherProductInputUsageEntityToDTO(i)).toList();

        bindAbstractActionEntityToDTO(entity, builder);

        MineralFertilizersSpreadingActionDto result = builder.topiaId(action.getTopiaId())
                .mineralProductInputUsageDtos(mineralProductInputUsageDtos)
                .otherProductInputUsageDtos(otherProductInputUsageDtos)
                .build();

        return result;
    }

    @Override
    public List<AbstractActionDto> loadEffectiveActionsAndUsages(EffectiveIntervention intervention) {
        List<AbstractAction> actions = abstractActionDao.forEffectiveInterventionEquals(intervention).findAll();
        return transformeEntitiesToDtos(actions);
    }


    @Override
    public List<String> validActionDtosAndUsages(
            Collection<AbstractActionDto> actionDtos,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            List<SpeciesStadeDto> speciesStadesDtos,
            boolean isIntermediateIntervention) {

        domainInputStockUnitByIds = domainInputStockUnitByIds == null ? new HashMap<>() : domainInputStockUnitByIds;
        speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee == null ? new HashMap<>() : speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
        sectorByCodeEspceBotaniqueCodeQualifiant = sectorByCodeEspceBotaniqueCodeQualifiant == null ? new HashMap<>() : sectorByCodeEspceBotaniqueCodeQualifiant;

        List<String> errorMessages = new ArrayList<>();
        if (CollectionUtils.isEmpty(actionDtos)) {
            errorMessages.add("- Déclarer une action (une intervention doit contenir au moins une action)");
        } else {
            List<AgrosystInterventionType> usedActionTypes = new ArrayList<>();

            for (AbstractActionDto actionDto : actionDtos) {
                String manActionId = actionDto.getMainActionId();
                if (StringUtils.isBlank(manActionId)) {
                    errorMessages.add("- Renseigner le type de l'action");
                } else {
                    RefInterventionAgrosystTravailEDI mainAction = referentialService.getRefInterventionAgrosystTravailEDI(manActionId);
                    AgrosystInterventionType interventionType = mainAction.getIntervention_agrosyst();

                    validCommonAbstractActionDto(errorMessages, usedActionTypes, interventionType);

                    validateSpecializedActions(
                            domainInputStockUnitByIds,
                            speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                            sectorByCodeEspceBotaniqueCodeQualifiant,
                            speciesStadesDtos,
                            errorMessages,
                            actionDto,
                            mainAction,
                            interventionType
                    );
                }
            }
        }
        return errorMessages;
    }

    private void validateSpecializedActions(
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            List<SpeciesStadeDto> speciesStadesDtos,
            List<String> errorMessages,
            AbstractActionDto actionDto,
            RefInterventionAgrosystTravailEDI mainAction,
            AgrosystInterventionType interventionType) {

        switch (interventionType) {
            case RECOLTE ->
                    validHarvestingActionDtoAndUsages(domainInputStockUnitByIds, speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee, sectorByCodeEspceBotaniqueCodeQualifiant, speciesStadesDtos, errorMessages, (HarvestingActionDto) actionDto, mainAction);
            case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES ->
                    validPesticidesSpreadingActionDtoAndUsages(domainInputStockUnitByIds, errorMessages, (PesticidesSpreadingActionDto) actionDto, mainAction);
            case IRRIGATION ->
                    validIrrigationActionDtoAndUsages(domainInputStockUnitByIds, errorMessages, (IrrigationActionDto) actionDto, mainAction);
            case LUTTE_BIOLOGIQUE ->
                    validBiologicalControlActionDtoAndUsage(domainInputStockUnitByIds, errorMessages, (BiologicalControlActionDto) actionDto, mainAction);
            case SEMIS ->
                    validSeedingActionUsageDtoAndUsages(domainInputStockUnitByIds, speciesStadesDtos, errorMessages, (SeedingActionUsageDto) actionDto, mainAction);
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX ->
                    validMineralFertilizersSpreadingActionDtoAndUsages(domainInputStockUnitByIds, errorMessages, (MineralFertilizersSpreadingActionDto) actionDto, mainAction);
            case AUTRE ->
                    validOtherActionDtoAndUsages(domainInputStockUnitByIds, errorMessages, (OtherActionDto) actionDto, mainAction);
            case ENTRETIEN_TAILLE_VIGNE_ET_VERGER ->
                    validMaintenancePruningVinesActionDtoAndUsages(domainInputStockUnitByIds, errorMessages, (MaintenancePruningVinesActionDto) actionDto, mainAction);
            case EPANDAGES_ORGANIQUES ->
                    validOrganicFertilizersSpreadingActionDtoAndUsages(domainInputStockUnitByIds, errorMessages, (OrganicFertilizersSpreadingActionDto) actionDto, mainAction);
            case TRANSPORT -> validCarriageActionDto(errorMessages, (CarriageActionDto) actionDto);
            case TRAVAIL_DU_SOL -> validTillageActionDto(errorMessages, (TillageActionDto) actionDto);
            default ->
                    throw new UnsupportedOperationException("Unexpected intervention type: " + actionDto.getMainActionInterventionAgrosyst());
        }
    }


    protected Collection<AbstractActionDto> validActionDtosAndUsages0(
            String globalContext,
            Collection<AbstractActionDto> actionDtos,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            List<SpeciesStadeDto> speciesStadesDtos,
            boolean isIntermediateIntervention) {

        Preconditions.checkArgument(CollectionUtils.isNotEmpty(actionDtos), "- Déclarer une action (une intervention doit contenir au moins une action)");
        domainInputStockUnitByIds = domainInputStockUnitByIds == null ? new HashMap<>() : domainInputStockUnitByIds;
        speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee == null ? new HashMap<>() : speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
        sectorByCodeEspceBotaniqueCodeQualifiant = sectorByCodeEspceBotaniqueCodeQualifiant == null ? new HashMap<>() : sectorByCodeEspceBotaniqueCodeQualifiant;

        Collection<AbstractActionDto> validatedActionDtos = new ArrayList<>();
        List<AgrosystInterventionType> usedActionTypes = new ArrayList<>();

        for (AbstractActionDto actionDto : actionDtos) {
            String manActionId = actionDto.getMainActionId();
            Preconditions.checkNotNull(manActionId);

            RefInterventionAgrosystTravailEDI mainAction = referentialService.getRefInterventionAgrosystTravailEDI(manActionId);
            AgrosystInterventionType interventionType = mainAction.getIntervention_agrosyst();

            List<String> errorMessages = new ArrayList<>();

            validCommonAbstractActionDto(errorMessages, usedActionTypes, interventionType);

            validateSpecializedActions(
                    domainInputStockUnitByIds,
                    speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                    sectorByCodeEspceBotaniqueCodeQualifiant,
                    speciesStadesDtos,
                    errorMessages,
                    actionDto,
                    mainAction,
                    interventionType
            );

            Preconditions.checkArgument(CollectionUtils.isEmpty(errorMessages),  globalContext + String.join(", ", errorMessages));
            validatedActionDtos.add(actionDto);

        }
        return validatedActionDtos;
    }

    @Override
    public void searchMissingActionAndUsageInput(
            AbstractActionDto actionDto,
            Map<String, DomainInputDto> domainInputDtoByCode,
            String toDomainId) {

        AgrosystInterventionType agrosystInterventionType = actionDto.getMainActionInterventionAgrosyst();
        switch (agrosystInterventionType) {
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {

                final MineralFertilizersSpreadingActionDto mineralFertilizersSpreadingActionDto = (MineralFertilizersSpreadingActionDto) actionDto;

                inputUsageService.addExistingOrNewOtherProductInputDomainInput(
                        domainInputDtoByCode,
                        mineralFertilizersSpreadingActionDto.getOtherProductInputUsageDtos(),
                        toDomainId);

                inputUsageService.addExistingOrNewMineralProductInputUsageDomainInput(
                        domainInputDtoByCode,
                        mineralFertilizersSpreadingActionDto.getMineralProductInputUsageDtos(),
                        toDomainId);

            }
            case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES -> {

                final PesticidesSpreadingActionDto pesticidesSpreadingActionDto = (PesticidesSpreadingActionDto) actionDto;

                inputUsageService.addExistingOrNewOtherProductInputDomainInput(
                        domainInputDtoByCode,
                        pesticidesSpreadingActionDto.getOtherProductInputUsageDtos(),
                        toDomainId);

                inputUsageService.addExistingOrNewPhytoProductInputUsageDomainInput(
                        domainInputDtoByCode,
                        pesticidesSpreadingActionDto.getPhytoProductInputUsageDtos(),
                        toDomainId);

            }
            case AUTRE -> {
                final OtherActionDto otherActionDto = (OtherActionDto) actionDto;

                inputUsageService.addExistingOrNewPotInputUsageDomainInputDtos(
                        domainInputDtoByCode,
                        otherActionDto.getPotInputUsageDtos(),
                        toDomainId);
                inputUsageService.addExistingOrNewSubstrateInputUsageDomainInput(
                        domainInputDtoByCode,
                        otherActionDto.getSubstrateInputUsageDtos(),
                        toDomainId);
                inputUsageService.addExistingOrNewOtherProductInputDomainInput(
                        domainInputDtoByCode,
                        otherActionDto.getOtherProductInputUsageDtos(),
                        toDomainId);

            }
            case ENTRETIEN_TAILLE_VIGNE_ET_VERGER -> {

                final MaintenancePruningVinesActionDto maintenancePruningVinesActionDto = (MaintenancePruningVinesActionDto) actionDto;
                inputUsageService.addExistingOrNewOtherProductInputDomainInput(
                        domainInputDtoByCode,
                        maintenancePruningVinesActionDto.getOtherProductInputUsageDtos(),
                        toDomainId);

            }
            case EPANDAGES_ORGANIQUES -> {
                final OrganicFertilizersSpreadingActionDto organicFertilizersSpreadingActionDto = (OrganicFertilizersSpreadingActionDto) actionDto;

                inputUsageService.addExistingOrNewOtherProductInputDomainInput(
                        domainInputDtoByCode,
                        organicFertilizersSpreadingActionDto.getOtherProductInputUsageDtos(),
                        toDomainId);

                inputUsageService.addExistingOrNewOrganicProductInputUsageDomainInput(
                        domainInputDtoByCode,
                        organicFertilizersSpreadingActionDto.getOrganicProductInputUsageDtos(), toDomainId);

            }
            case IRRIGATION -> {
                final IrrigationActionDto irrigationActionDto = (IrrigationActionDto) actionDto;

                inputUsageService.addExistingOrNewOtherProductInputDomainInput(
                        domainInputDtoByCode,
                        irrigationActionDto.getOtherProductInputUsageDtos(),
                        toDomainId);

                inputUsageService.addExistingOrNewIrrigationInputUsageDomainInput(
                        domainInputDtoByCode,
                        irrigationActionDto.getIrrigationInputUsageDto(),
                        toDomainId);

            }
            case LUTTE_BIOLOGIQUE -> {
                final BiologicalControlActionDto biologicalControlActionDto = (BiologicalControlActionDto) actionDto;
                inputUsageService.addExistingOrNewOtherProductInputDomainInput(
                        domainInputDtoByCode,
                        biologicalControlActionDto.getOtherProductInputUsageDtos(),
                        toDomainId);

                inputUsageService.addExistingOrNewPhytoProductInputUsageDomainInput(
                        domainInputDtoByCode,
                        biologicalControlActionDto.getPhytoProductInputUsageDtos(),
                        toDomainId);

            }
            case RECOLTE -> {
                final HarvestingActionDto harvestingActionDto = (HarvestingActionDto) actionDto;
                inputUsageService.addExistingOrNewOtherProductInputDomainInput(
                        domainInputDtoByCode,
                        harvestingActionDto.getOtherProductInputUsageDtos(),
                        toDomainId);

            }
            case SEMIS -> {
                if (actionDto instanceof SeedingActionUsageDto seedingActionUsageDto) {

                    inputUsageService.addExistingOrNewOtherProductInputDomainInput(
                            domainInputDtoByCode,
                            seedingActionUsageDto.getOtherProductInputUsageDtos(),
                            toDomainId);

                    inputUsageService.addExistingOrNewSeedLotInputInputUsageDtos(
                            domainInputDtoByCode,
                            seedingActionUsageDto.getSeedLotInputUsageDtos(),
                            toDomainId);
                }

            }
            case TRANSPORT, TRAVAIL_DU_SOL -> {
                // Nothing to do
            }
            default -> throw new AgrosystTechnicalException("Unexpected value: " + agrosystInterventionType);

        }
    }


    @Override
    public AbstractActionDto cloneActionAndUsages(
            AbstractActionDto actionDto,
            Map<String, DomainInputDto> domainInputDtoByCode,
            String toDomainId) {

        AbstractActionDto clonedActionDto = null;
        Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds = Maps.uniqueIndex(refInterventionAgrosystTravailEdiDao.findAll(), TopiaEntity::getTopiaId);
        RefInterventionAgrosystTravailEDI mainAction = mainActionByIds.get(actionDto.getMainActionId());
        if (mainAction != null) {
            AgrosystInterventionType agrosystInterventionType = mainAction.getIntervention_agrosyst();
            switch (agrosystInterventionType) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {

                    final MineralFertilizersSpreadingActionDto mineralFertilizersSpreadingActionDto = (MineralFertilizersSpreadingActionDto) actionDto;

                    final Collection<OtherProductInputUsageDto> clonedOtherProductInputUsageDtos = inputUsageService.cloneOtherProductInputUsageDtos(
                            domainInputDtoByCode,
                            mineralFertilizersSpreadingActionDto.getOtherProductInputUsageDtos()
                    );

                    final Collection<MineralProductInputUsageDto> clonedPhytoProductInputUsageDtos = inputUsageService.cloneMineralProductInputUsageDtos(
                            domainInputDtoByCode,
                            mineralFertilizersSpreadingActionDto.getMineralProductInputUsageDtos()
                    );

                    clonedActionDto = mineralFertilizersSpreadingActionDto.toBuilder()
                            .topiaId(null)
                            .mineralProductInputUsageDtos(clonedPhytoProductInputUsageDtos)
                            .otherProductInputUsageDtos(clonedOtherProductInputUsageDtos)
                            .build();
                }
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES -> {
                    final PesticidesSpreadingActionDto pesticidesSpreadingActionDto = (PesticidesSpreadingActionDto) actionDto;

                    final Collection<OtherProductInputUsageDto> clonedOtherProductInputUsageDtos = inputUsageService.cloneOtherProductInputUsageDtos(
                            domainInputDtoByCode,
                            pesticidesSpreadingActionDto.getOtherProductInputUsageDtos()
                    );

                    final Collection<PhytoProductInputUsageDto> clonedPhytoProductInputUsageDtos = inputUsageService.clonePhytoProductInputUsageDtos(
                            domainInputDtoByCode,
                            pesticidesSpreadingActionDto.getPhytoProductInputUsageDtos()
                    );

                    clonedActionDto = pesticidesSpreadingActionDto.toBuilder()
                            .topiaId(null)
                            .phytoProductInputUsageDtos(clonedPhytoProductInputUsageDtos)
                            .otherProductInputUsageDtos(clonedOtherProductInputUsageDtos)
                            .build();
                }
                case AUTRE -> {
                    final OtherActionDto otherActionDto = (OtherActionDto) actionDto;

                    final Collection<PotInputUsageDto> clonedPotInputUsageDtos = inputUsageService.clonePotInputUsageDto(
                            domainInputDtoByCode,
                            otherActionDto.getPotInputUsageDtos());
                    final Collection<SubstrateInputUsageDto> clonedSubstrateInputUsageDtos = inputUsageService.cloneSubstrateInputUsageDto(
                            domainInputDtoByCode,
                            otherActionDto.getSubstrateInputUsageDtos());
                    final Collection<OtherProductInputUsageDto> clonedOtherProductInputUsageDtos = inputUsageService.cloneOtherProductInputUsageDtos(
                            domainInputDtoByCode,
                            otherActionDto.getOtherProductInputUsageDtos()
                    );

                    clonedActionDto = otherActionDto.toBuilder()
                            .topiaId(null)
                            .potInputUsageDtos(clonedPotInputUsageDtos)
                            .substrateInputUsageDtos(clonedSubstrateInputUsageDtos)
                            .otherProductInputUsageDtos(clonedOtherProductInputUsageDtos)
                            .build();
                }
                case ENTRETIEN_TAILLE_VIGNE_ET_VERGER -> {
                    final MaintenancePruningVinesActionDto maintenancePruningVinesActionDto = (MaintenancePruningVinesActionDto) actionDto;
                    final Collection<OtherProductInputUsageDto> clonedOtherProductInputUsageDtos = inputUsageService.cloneOtherProductInputUsageDtos(
                            domainInputDtoByCode,
                            maintenancePruningVinesActionDto.getOtherProductInputUsageDtos()
                    );

                    clonedActionDto = maintenancePruningVinesActionDto.toBuilder()
                            .topiaId(null)
                            .otherProductInputUsageDtos(clonedOtherProductInputUsageDtos)
                            .build();
                }
                case EPANDAGES_ORGANIQUES -> {
                    final OrganicFertilizersSpreadingActionDto organicFertilizersSpreadingActionDto = (OrganicFertilizersSpreadingActionDto) actionDto;

                    final Collection<OtherProductInputUsageDto> clonedOtherProductInputUsageDtos = inputUsageService.cloneOtherProductInputUsageDtos(
                            domainInputDtoByCode,
                            organicFertilizersSpreadingActionDto.getOtherProductInputUsageDtos()
                    );

                    final Collection<OrganicProductInputUsageDto> clonedOrganicProductInputUsageDtos = inputUsageService.cloneOrganicProductInputUsageDtos(
                            domainInputDtoByCode,
                            organicFertilizersSpreadingActionDto.getOrganicProductInputUsageDtos());

                    clonedActionDto = organicFertilizersSpreadingActionDto.toBuilder()
                            .topiaId(null)
                            .otherProductInputUsageDtos(clonedOtherProductInputUsageDtos)
                            .organicProductInputUsageDtos(clonedOrganicProductInputUsageDtos)
                            .build();
                }
                case IRRIGATION -> {
                    final IrrigationActionDto irrigationActionDto = (IrrigationActionDto) actionDto;

                    final Collection<OtherProductInputUsageDto> clonedOtherProductInputUsageDtos = inputUsageService.cloneOtherProductInputUsageDtos(
                            domainInputDtoByCode,
                            irrigationActionDto.getOtherProductInputUsageDtos()
                    );

                    IrrigationInputUsageDto clonedIrrigationInputUsageDto = inputUsageService.cloneIrrigationInputUsageDto(
                            domainInputDtoByCode,
                            irrigationActionDto.getIrrigationInputUsageDto()
                    );

                    clonedActionDto = irrigationActionDto.toBuilder()
                            .topiaId(null)
                            .otherProductInputUsageDtos(clonedOtherProductInputUsageDtos)
                            .irrigationInputUsageDto(clonedIrrigationInputUsageDto)
                            .build();
                }
                case LUTTE_BIOLOGIQUE -> {
                    final BiologicalControlActionDto biologicalControlActionDto = (BiologicalControlActionDto) actionDto;
                    final Collection<OtherProductInputUsageDto> clonedOtherProductInputUsageDtos = inputUsageService.cloneOtherProductInputUsageDtos(
                            domainInputDtoByCode,
                            biologicalControlActionDto.getOtherProductInputUsageDtos()
                    );

                    final Collection<PhytoProductInputUsageDto> clonedPhytoProductInputUsageDtos = inputUsageService.clonePhytoProductInputUsageDtos(
                            domainInputDtoByCode,
                            biologicalControlActionDto.getPhytoProductInputUsageDtos());

                    clonedActionDto = biologicalControlActionDto.toBuilder()
                            .topiaId(null)
                            .otherProductInputUsageDtos(clonedOtherProductInputUsageDtos)
                            .phytoProductInputUsageDtos(clonedPhytoProductInputUsageDtos)
                            .build();
                }
                case RECOLTE -> {
                    final HarvestingActionDto harvestingActionDto = (HarvestingActionDto) actionDto;
                    final Collection<OtherProductInputUsageDto> clonedOtherProductInputUsageDtos = inputUsageService.cloneOtherProductInputUsageDtos(
                            domainInputDtoByCode,
                            harvestingActionDto.getOtherProductInputUsageDtos()
                    );

                    List<HarvestingActionValorisationDto> clonedValorisationDtos = cloneHarvestingValorisationDtos(harvestingActionDto);

                    clonedActionDto = harvestingActionDto.toBuilder()
                            .topiaId(null)
                            .valorisationDtos(clonedValorisationDtos)
                            .otherProductInputUsageDtos(clonedOtherProductInputUsageDtos)
                            .build();

                }
                case SEMIS -> {
                    if (actionDto instanceof SeedingActionUsageDto seedingActionUsageDto) {

                        final Collection<OtherProductInputUsageDto> clonedOtherProductInputUsageDtos = inputUsageService.cloneOtherProductInputUsageDtos(
                                domainInputDtoByCode,
                                seedingActionUsageDto.getOtherProductInputUsageDtos()
                        );

                        final Collection<SeedLotInputUsageDto> clonedSeedLotInputUsageDtos = inputUsageService.cloneSeedLotInputUsageDtos(
                                domainInputDtoByCode,
                                seedingActionUsageDto.getSeedLotInputUsageDtos(),
                                toDomainId);

                        final Collection<SubstrateInputUsageDto> substrateInputUsageDtos = inputUsageService.cloneSubstrateInputUsageDto(
                                domainInputDtoByCode,
                                seedingActionUsageDto.getSubstrateInputUsageDtos()
                        );

                        clonedActionDto = seedingActionUsageDto.toBuilder()
                                .topiaId(null)
                                .otherProductInputUsageDtos(clonedOtherProductInputUsageDtos)
                                .seedLotInputUsageDtos(clonedSeedLotInputUsageDtos)
                                .substrateInputUsageDtos(substrateInputUsageDtos)
                                .build();
                    }

                }
                case TRANSPORT -> {
                    final CarriageActionDto carriageActionDto = (CarriageActionDto) actionDto;

                    clonedActionDto = carriageActionDto.toBuilder()
                            .topiaId(null)
                            .build();
                }
                case TRAVAIL_DU_SOL -> {
                    final TillageActionDto tillageActionDto = (TillageActionDto) actionDto;

                    clonedActionDto = tillageActionDto.toBuilder()
                            .topiaId(null)
                            .build();
                }
            }
        }
        return clonedActionDto;
    }

    protected List<HarvestingActionValorisationDto> cloneHarvestingValorisationDtos(HarvestingActionDto harvestingActionDto) {
        final List<HarvestingActionValorisationDto> valorisationDtos = harvestingActionDto.getValorisationDtos();
        List<HarvestingActionValorisationDto> clonedValorisationDtos = new ArrayList<>();
        valorisationDtos.forEach(valorisationDto -> {
            Collection<QualityCriteriaDto> clonedQualityCriteriaDtos;
            final Optional<Collection<QualityCriteriaDto>> optionalQualityCriteriaDtos = valorisationDto.getQualityCriteriaDtos();
            if (optionalQualityCriteriaDtos.isPresent()) {
                clonedQualityCriteriaDtos = new ArrayList<>();
                final Collection<QualityCriteriaDto> qualityCriteriaDtos = optionalQualityCriteriaDtos.get();
                qualityCriteriaDtos.forEach(qualityCriteriaDto -> {
                    final QualityCriteriaDto clonedQualityCriteriaDto = qualityCriteriaDto.toBuilder()
                            .topiaId(null)
                            .build();
                    clonedQualityCriteriaDtos.add(clonedQualityCriteriaDto);

                });
            } else {
                clonedQualityCriteriaDtos = null;
            }

            final HarvestingActionValorisationDto clonedValorisationDto = valorisationDto.toBuilder()
                    .topiaId(null)
                    .qualityCriteriaDtos(clonedQualityCriteriaDtos)
                    .build();
            clonedValorisationDtos.add(clonedValorisationDto);
        });
        return clonedValorisationDtos;
    }

    private void validCommonAbstractActionDto(List<String> errorMessages, List<AgrosystInterventionType> usedActionTypes, AgrosystInterventionType interventionType) {
        if (!TYPES_ALLOWING_3_ACTIONS.contains(interventionType) && usedActionTypes.contains(interventionType)) {
            errorMessages.add(String.format("- Une intervention ne peut contenir qu'une seule action de type '%s'", AgrosystI18nService.getEnumTraductionWithDefaultLocale(interventionType)));
        } else {
            usedActionTypes.add(interventionType);
        }
    }

    private void validOrganicFertilizersSpreadingActionDtoAndUsages(Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds, List<String> errorMessages, OrganicFertilizersSpreadingActionDto actionDto, RefInterventionAgrosystTravailEDI mainAction) {
        OrganicFertilizersSpreadingActionDto organicFertilizersSpreadingActionDto = actionDto;
        final Optional<Collection<OrganicProductInputUsageDto>> organicProductInputUsageDtos = organicFertilizersSpreadingActionDto.getOrganicProductInputUsageDtos();

        errorMessages.addAll(inputUsageService.validateOrganicProductInputUsage(
                domainInputStockUnitByIds,
                organicProductInputUsageDtos,
                mainAction));

        Optional<Collection<OtherProductInputUsageDto>> optionalOrganicFertilizersSpreadingActionOtherInputUsageDtos = organicFertilizersSpreadingActionDto.getOtherProductInputUsageDtos();
        errorMessages.addAll(inputUsageService.validateOtherInputUsages(
                domainInputStockUnitByIds,
                optionalOrganicFertilizersSpreadingActionOtherInputUsageDtos,
                mainAction));

    }

    private void validMaintenancePruningVinesActionDtoAndUsages(Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds, List<String> errorMessages, MaintenancePruningVinesActionDto actionDto, RefInterventionAgrosystTravailEDI mainAction) {
        MaintenancePruningVinesActionDto maintenancePruningVinesActionDto = actionDto;
        Optional<Collection<OtherProductInputUsageDto>> optionalMaintenancePruningVinesActionOtherInputUsageDtos = maintenancePruningVinesActionDto.getOtherProductInputUsageDtos();
        errorMessages.addAll(inputUsageService.validateOtherInputUsages(
                domainInputStockUnitByIds,
                optionalMaintenancePruningVinesActionOtherInputUsageDtos,
                mainAction));

    }

    private void validOtherActionDtoAndUsages(Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds, List<String> errorMessages, OtherActionDto actionDto, RefInterventionAgrosystTravailEDI mainAction) {
        OtherActionDto otherActionDto = actionDto;
        Optional<Collection<OtherProductInputUsageDto>> optionalOtherInputUsageDtos = otherActionDto.getOtherProductInputUsageDtos();
        errorMessages.addAll(inputUsageService.validateOtherInputUsages(
                domainInputStockUnitByIds,
                optionalOtherInputUsageDtos,
                mainAction));

        Optional<Collection<SubstrateInputUsageDto>> substrateInputUsageDtos = otherActionDto.getSubstrateInputUsageDtos();
        errorMessages.addAll(inputUsageService.validateSubstrateInputUsages(
                domainInputStockUnitByIds,
                substrateInputUsageDtos,
                mainAction));

        Optional<Collection<PotInputUsageDto>> potInputUsageDtos = otherActionDto.getPotInputUsageDtos();
        errorMessages.addAll(inputUsageService.validatePotInputUsages(
                domainInputStockUnitByIds,
                potInputUsageDtos,
                mainAction));

    }

    private void validMineralFertilizersSpreadingActionDtoAndUsages(Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds, List<String> errorMessages, MineralFertilizersSpreadingActionDto actionDto, RefInterventionAgrosystTravailEDI mainAction) {
        MineralFertilizersSpreadingActionDto mineralFertilizersSpreadingActionDto = actionDto;
        Optional<Collection<OtherProductInputUsageDto>> optionalMineralFertilizersInputUsageDtos = mineralFertilizersSpreadingActionDto.getOtherProductInputUsageDtos();

        errorMessages.addAll(inputUsageService.validateOtherInputUsages(
                domainInputStockUnitByIds,
                optionalMineralFertilizersInputUsageDtos,
                mainAction));

        Optional<Collection<MineralProductInputUsageDto>> optionalMineralProductInputUsageDtos = mineralFertilizersSpreadingActionDto.getMineralProductInputUsageDtos();

        errorMessages.addAll(inputUsageService.validateMineralProductInputUsage(
                domainInputStockUnitByIds,
                optionalMineralProductInputUsageDtos,
                mainAction));
    }

    protected void validSeedingActionUsageDtoAndUsages(
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            List<SpeciesStadeDto> speciesStadesDtos,
            List<String> errorMessages,
            SeedingActionUsageDto actionDto,
            RefInterventionAgrosystTravailEDI mainAction) {

        SeedingActionUsageDto seedingActionUsageDto = actionDto;

        final Double yealdTarget = seedingActionUsageDto.getYealdTarget();
        if (yealdTarget != null && yealdTarget < 0) {
            errorMessages.add("- L'objectif de rendement ne peut pas être négatif pour l'action de type 'Semis'");
        }

        Optional<Collection<SubstrateInputUsageDto>> substrateInputUsageDtos = seedingActionUsageDto.getSubstrateInputUsageDtos();
        errorMessages.addAll(inputUsageService.validateSubstrateInputUsages(
                domainInputStockUnitByIds,
                substrateInputUsageDtos,
                mainAction));

        Collection<SeedLotInputUsageDto> seedLotInputUsageDtos = CollectionUtils.emptyIfNull(seedingActionUsageDto.getSeedLotInputUsageDtos());

        if (seedLotInputUsageDtos.isEmpty()) {
            errorMessages.add("- Cette Action n'a pas de lot de semence de renseigné");
        } else {

            Set<String> itkSpeciesCodes = new HashSet<>(speciesStadesDtos != null ? speciesStadesDtos.stream().map(SpeciesStadeDto::getSpeciesCode)
                    .collect(Collectors.toSet()) : Collections.emptySet());

            for (SeedLotInputUsageDto seedLotInputUsageDto : seedLotInputUsageDtos) {
                errorMessages.addAll(inputUsageService.validateSeedLotInputUsages(
                        domainInputStockUnitByIds,
                        seedLotInputUsageDto,
                        mainAction,
                        itkSpeciesCodes
                ));
            }
            Optional<Collection<OtherProductInputUsageDto>> optionalSeedingOtherInputUsageDtos = seedingActionUsageDto.getOtherProductInputUsageDtos();
            errorMessages.addAll(inputUsageService.validateOtherInputUsages(
                    domainInputStockUnitByIds,
                    optionalSeedingOtherInputUsageDtos,
                    mainAction));

        }
    }

    private void validBiologicalControlActionDtoAndUsage(Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds, List<String> errorMessages, BiologicalControlActionDto actionDto, RefInterventionAgrosystTravailEDI mainAction) {
        BiologicalControlActionDto biologicalControlActionDto = actionDto;
        if (biologicalControlActionDto.getProportionOfTreatedSurface() <= 0) {
            errorMessages.add("- Renseigner la proportion de surface traitée (au regard de la surface d'intervention) pour l'action de type 'Traitements phytosanitaires : Produits sans AMM et macroorganismes'");
        }
        Optional<Collection<OtherProductInputUsageDto>> optionalBiologiqueOtherInputUsageDtos = biologicalControlActionDto.getOtherProductInputUsageDtos();
        errorMessages.addAll(inputUsageService.validateOtherInputUsages(
                domainInputStockUnitByIds,
                optionalBiologiqueOtherInputUsageDtos,
                mainAction));

        Optional<Collection<PhytoProductInputUsageDto>> optionalBiologiqueProductInputUsageDtos = biologicalControlActionDto.getPhytoProductInputUsageDtos();
        errorMessages.addAll(inputUsageService.validatePytoProductInputUsages(
                domainInputStockUnitByIds,
                optionalBiologiqueProductInputUsageDtos,
                mainAction));

        if (biologicalControlActionDto.getBoiledQuantity() < 0) {
            errorMessages.add("- Renseigner un volume positif de bouillie en hL/ha pour l'action de type 'Traitements phytosanitaires : Produits sans AMM et macroorganismes'");
        }
        final Double boiledQuantityPerTrip = biologicalControlActionDto.getBoiledQuantityPerTrip();
        if (boiledQuantityPerTrip != null && boiledQuantityPerTrip < 0) {
            errorMessages.add("- Renseigner un volume positif de bouillie chargée à chaque voyage en hL/ha pour l'action de type 'Traitements phytosanitaires : Produits sans AMM et macroorganismes'");
        }

        final Double tripFrequency = biologicalControlActionDto.getTripFrequency();
        if (tripFrequency != null && tripFrequency < 0) {
            errorMessages.add("- Renseigner un nombre de voyages par heure positif pour l'action de type 'Traitements phytosanitaires : Produits sans AMM et macroorganismes'");
        }
    }

    private void validIrrigationActionDtoAndUsages(Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds, List<String> errorMessages, IrrigationActionDto actionDto, RefInterventionAgrosystTravailEDI mainAction) {
        IrrigationActionDto irrigationActionDto = actionDto;
        double waterQuantityAverage = irrigationActionDto.getWaterQuantityAverage();
        if (waterQuantityAverage <= 0) {
            errorMessages.add("- Renseigner une quantité d'eau apportée supérieure à 0 pour l'action de type 'Irrigation'");
        }
        final Double waterQuantityMin = irrigationActionDto.getWaterQuantityMin();
        if (waterQuantityMin != null && waterQuantityMin < 0) {
            errorMessages.add("- Renseigner une quantité d'eau minimum supérieure à 0 pour l'action de type 'Irrigation'");
        }
        final Double waterQuantityMax = irrigationActionDto.getWaterQuantityMax();
        if (waterQuantityMax != null && waterQuantityMax < 0) {
            errorMessages.add("- Renseigner une quantité d'eau maximum supérieure à 0 pour l'action de type 'Irrigation'");
        }
        final Double waterQuantityMedian = irrigationActionDto.getWaterQuantityMedian();
        if (waterQuantityMedian != null && waterQuantityMedian < 0) {
            errorMessages.add("- Renseigner une quantité d'eau médiane supérieure à 0 pour l'action de type 'Irrigation'");
        }

        Optional<Collection<OtherProductInputUsageDto>> optionalIrrigationOtherProductInputUsageDtos = irrigationActionDto.getOtherProductInputUsageDtos();
        if (optionalIrrigationOtherProductInputUsageDtos.isPresent()) {
            errorMessages.addAll(inputUsageService.validateOtherInputUsages(
                    domainInputStockUnitByIds,
                    optionalIrrigationOtherProductInputUsageDtos,
                    mainAction));
        }
    }

    private void validPesticidesSpreadingActionDtoAndUsages(Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds, List<String> errorMessages, PesticidesSpreadingActionDto actionDto, RefInterventionAgrosystTravailEDI mainAction) {
        PesticidesSpreadingActionDto pesticidesSpreadingAction = actionDto;
        Double boiledQuantity = pesticidesSpreadingAction.getBoiledQuantity();
        final Double boiledQuantityPerTrip = pesticidesSpreadingAction.getBoiledQuantityPerTrip();
        if (boiledQuantity == null) {
            errorMessages.add("- Renseigner le volume de bouillie en hL/ha pour l'action de type 'Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM)' (il est possible de mettre '0' hL)");
        } else if (boiledQuantity < 0) {
            errorMessages.add("- Renseigner un volume positif de bouillie en hL/ha pour l'action de type 'Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM)' (il est possible de mettre '0' hL)");
        }
        if (boiledQuantityPerTrip != null && boiledQuantityPerTrip < 0) {
            errorMessages.add("- Renseigner un volume positif de bouillie chargée à chaque voyage en hL/ha pour l'action de type 'Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM)' (il est possible de mettre '0' hL)");
        }

        final Double tripFrequency = pesticidesSpreadingAction.getTripFrequency();
        if (tripFrequency != null && tripFrequency < 0) {
            errorMessages.add("- Renseigner un nombre de voyages par heure positif pour l'action de type 'Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM)'");
        }

        if (pesticidesSpreadingAction.getProportionOfTreatedSurface() <= 0) {
            errorMessages.add("- Renseigner la proportion de surface traitée (au regard de la surface d'intervention) pour l'action de type 'Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM)'");
        }
        Optional<Collection<OtherProductInputUsageDto>> optionalPesticidesProductInputUsageDtos = pesticidesSpreadingAction.getOtherProductInputUsageDtos();
        errorMessages.addAll(inputUsageService.validateOtherInputUsages(
                domainInputStockUnitByIds,
                optionalPesticidesProductInputUsageDtos,
                mainAction));

        Optional<Collection<PhytoProductInputUsageDto>> optionalPhytoProductInputUsageDtos = pesticidesSpreadingAction.getPhytoProductInputUsageDtos();
        errorMessages.addAll(inputUsageService.validatePytoProductInputUsages(
                domainInputStockUnitByIds,
                optionalPhytoProductInputUsageDtos,
                mainAction));

    }

    private void validHarvestingActionDtoAndUsages(
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            List<SpeciesStadeDto> speciesStadesDtos,
            List<String> errorMessages,
            HarvestingActionDto harvestingActionDto,
            RefInterventionAgrosystTravailEDI mainAction) {

        if (!isValidHarvestingActionWineValorisations(
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                speciesStadesDtos,
                harvestingActionDto)) {
            errorMessages.add("- Veuillez sélectionner une valorisation pour la vigne");
        }

        List<HarvestingActionValorisationDto> valorisationDtos = harvestingActionDto.getValorisationDtos();

        valorisationDtos.removeIf(Objects::isNull);

        if (CollectionUtils.isNotEmpty(valorisationDtos)) {
            List<Double> globalYealdAverages = new ArrayList<>();
            String lastSpecesCode = "";
            int destinationIndex = 0;

            Map<String, Pair<Double, List<HarvestingActionValorisationDto>>> yealdAverageByDestinationDto = new HashMap<>();

            Set<String> speciesCodes = CollectionUtils.emptyIfNull(speciesStadesDtos)
                    .stream()
                    .map(SpeciesStadeDto::getSpeciesCode)
                    .collect(Collectors.toSet());

            valorisationDtos.removeIf(valorisation -> !speciesCodes.contains(valorisation.getSpeciesCode()));

            // as HarvestingActionValorisationDto are immutable, new one are created for the validation needs, this map allow to keep correspondence between original ones and modified ones
            Map<HarvestingActionValorisationDto, HarvestingActionValorisationDto> tmpValorisationDtos = new HashMap<>();
            for (final HarvestingActionValorisationDto valorisationDto : valorisationDtos) {

                HarvestingActionValorisationDto tmpValorisationDto = valorisationDto;
                if (lastSpecesCode.contentEquals(valorisationDto.getSpeciesCode())) {
                    destinationIndex += 1;
                } else {
                    destinationIndex = 0;
                    lastSpecesCode = valorisationDto.getSpeciesCode();
                }
                final double yealdAverage = valorisationDto.getYealdAverage();
                if (yealdAverage < 0) {
                    errorMessages.add("- Le rendement ne peut pas être négatif pour l'action de type 'Récolte'");
                }

                if (globalYealdAverages.size() == destinationIndex) {
                    globalYealdAverages.add(yealdAverage);
                } else {
                    globalYealdAverages.set(destinationIndex, globalYealdAverages.get(destinationIndex) + yealdAverage);
                }
                int sales = valorisationDto.getSalesPercent();
                int selfConsumed = valorisationDto.getSelfConsumedPersent();
                int noValorisation = valorisationDto.getNoValorisationPercent();
                int sum = sales + selfConsumed + noValorisation;
                if (99 > sum || sum > 101) {
                    errorMessages.add("- Corriger la part des types de valorisation d'une destination pour l'action de type 'Récolte' (la somme doit être égale à 100)");
                }
                String destinationId = valorisationDto.getDestinationId();
                if (StringUtils.isBlank(destinationId)) {
                    errorMessages.add("- Déclarer à minima une destination pour l'action de type 'Récolte'");
                } else {
                    RefDestination destination = referentialService.getDestination(destinationId);
                    if (destination == null) {
                        errorMessages.add("- Déclarer à minima une destination pour l'action de type 'Récolte'");
                    } else {

                        HarvestingActionValorisationDto valorisationDto_ = removeValorisationDtoYealdAverageOnNoneValidDestinations(
                                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                                sectorByCodeEspceBotaniqueCodeQualifiant,
                                valorisationDto,
                                destination,
                                harvestingActionDto.getWineValorisations()
                        );

                        valorisationDto_ = validateQualityCriteria(
                                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                                sectorByCodeEspceBotaniqueCodeQualifiant,
                                valorisationDto_,
                                destination,
                                harvestingActionDto.getWineValorisations());

                        computeGlobalValorisationForDestination(valorisationDto_, yealdAverageByDestinationDto);

                        tmpValorisationDto = valorisationDto_;
                    }

                }
                tmpValorisationDtos.put(tmpValorisationDto, valorisationDto);

            }

            // remove valorisations with global Yeald average equal to 0
            List<HarvestingActionValorisationDto> valorisationToRemoves = getValorisationsDtoWhereGlobalYealdAverageIsZero(yealdAverageByDestinationDto);
            for (HarvestingActionValorisationDto valorisation : valorisationToRemoves) {
                if (LOGGER.isInfoEnabled()) {
                    LOGGER.info(String.format("- La valorisation avec comme id:'%s' portant sur l'espèce au code '%s' n'est pas valide et sera supprimée ",
                            valorisation.getTopiaId(), valorisation.getSpeciesCode()));
                }
            }

            valorisationToRemoves.forEach(valorisationToRemove -> valorisationDtos.remove(tmpValorisationDtos.get(valorisationToRemove)));

            for (Double globalYealdAverage : globalYealdAverages) {
                if (globalYealdAverage == 0.0) {
                    errorMessages.add("- Corriger le rendement de l'action de type 'Récolte', il ne peut être égal à 0");
                    break;
                }
            }

        }

        Optional<Collection<OtherProductInputUsageDto>> optionalOtherProductInputUsageDtos = harvestingActionDto.getOtherProductInputUsageDtos();
        errorMessages.addAll(inputUsageService.validateOtherInputUsages(
                domainInputStockUnitByIds,
                optionalOtherProductInputUsageDtos,
                mainAction));


        if (CollectionUtils.isEmpty(valorisationDtos)) {
            errorMessages.add("- L'action de type récolte n'est pas valorisée");
        }

    }

    private void validCarriageActionDto(List<String> errorMessages, CarriageActionDto actionDto) {
        final Double tripFrequency = actionDto.getTripFrequency();
        if (tripFrequency != null && tripFrequency < 0) {
            errorMessages.add("- Le nombre de voyages par heure ne peut pas être négatif pour l'action de type 'Transport'");
        }
    }

    public void validTillageActionDto(List<String> errorMessages, TillageActionDto actionDto) {
        final Double tillageDepth = actionDto.getTillageDepth();
        if (tillageDepth != null && tillageDepth < 0) {
            errorMessages.add("- La profondeur de travail du sol ne peut pas être négative pour l'action de type 'Travail du sol'");
        }
    }

    private boolean isValidHarvestingActionWineValorisations(
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            List<SpeciesStadeDto> speciesStadesDtos,
            HarvestingActionDto harvestingActionDto) {
        boolean isValid;

        if (CollectionUtils.isEmpty(speciesStadesDtos)) {
            return true;
        }

        boolean isWineSpeciesFound = ActionService.IS_WINE_SPECIES_FOUND_(
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                speciesStadesDtos.stream().map(SpeciesStadeDto::getSpeciesCode).toList());

        isValid = !isWineSpeciesFound || harvestingActionDto.getWineValorisations().isPresent();
        return isValid;
    }

    protected List<AbstractActionDto> transformeEntitiesToDtos(List<AbstractAction> actions) {
        List<AbstractActionDto> result = new ArrayList<>();

        i18nService.fillActionTranslations(actions);

        for (AbstractAction action : actions) {
            AgrosystInterventionType interventionAgrosyst = action.getMainAction().getIntervention_agrosyst();

            switch (interventionAgrosyst) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX ->
                        result.add(bindMineralFertilizersSpreadingActionEntityToDTO(action));
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES ->
                        result.add(bindPesticidesSpreadingActionEntityToDTO(action));
                case AUTRE -> result.add(bindOtherActionEntityToDTO(action));
                case ENTRETIEN_TAILLE_VIGNE_ET_VERGER ->
                        result.add(bindMaintenancePruningVinesActionEntityToDTO(action));
                case EPANDAGES_ORGANIQUES -> result.add(bindOrganicFertilizersSpreadingActionEntityToDTO(action));
                case IRRIGATION -> result.add(bindIrrigActionEntityToDTO(action));
                case LUTTE_BIOLOGIQUE -> result.add(bindBiologicalControlActionEntityToDTO(action));
                case RECOLTE -> result.add(bindHarvestingActionEntityToDTO(action));
                case SEMIS -> result.add(bindSeedingActionEntityToDTO(action));
                case TRANSPORT -> result.add(bindCarriageActionEntityToDTO(action));
                case TRAVAIL_DU_SOL -> result.add(bindTillageActionEntityToDTO(action));
            }
        }
        return result;
    }

    public void createOrUpdateMineralFertilizersSpreadingAction(
            AbstractActionDto actionDto,
            Map<String, AbstractAction> persistedActions,
            PracticedIntervention practicedIntervention,
            EffectiveIntervention effectiveIntervention,
            Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            ImmutableList<String> toolsCouplingCodes) {

        MineralFertilizersSpreadingActionDto dto = (MineralFertilizersSpreadingActionDto) actionDto;
        MineralFertilizersSpreadingAction action = actionDto.getTopiaId().isPresent() && !actionDto.getTopiaId().get().startsWith("NEW-ACTION") ?
                (MineralFertilizersSpreadingAction) persistedActions.remove(actionDto.getTopiaId().get()) : mineralFertilizersSpreadingActionDao.newInstance();

        if (!action.isPersisted()) {
            action.setPracticedIntervention(practicedIntervention);
            action.setEffectiveIntervention(effectiveIntervention);
            RefInterventionAgrosystTravailEDI mainAction = mainActionByIds.get(actionDto.getMainActionId());
            if (mainAction == null) {
                throw new AgrosystTechnicalException("The main action is required !");
            }
            action.setMainAction(mainAction);
        }

        bindDtoToActionCommonFields(
                toolsCouplingCodes,
                dto,
                action);

        action.setBurial(dto.isBurial());
        action.setLocalizedSpreading(dto.isLocalizedSpreading());

        final Optional<InputPersistanceResults<MineralProductInputUsage>> mineralIUsagesPersistanceResult = inputUsageService.persistOrDeleteMineralProductInputUsages(
                dto.getMineralProductInputUsageDtos(),
                action.getMineralProductInputUsages(),
                domainInputStockUnitByIds);
        final Optional<InputPersistanceResults<OtherProductInputUsage>> otherProductInputUsagesPersistanceResult = inputUsageService.persistOrDeleteOtherProductInputUsages(
                dto.getOtherProductInputUsageDtos(),
                action.getOtherProductInputUsages(),
                domainInputStockUnitByIds);

        if (action.isPersisted()) {
            mineralIUsagesPersistanceResult.ifPresent(
                    iupr -> {
                        removeGivenActionInputUsages(iupr, action::removeMineralProductInputUsages);
                        action.addAllMineralProductInputUsages(iupr.getInputUsagesCreated());
                    }
            );
            otherProductInputUsagesPersistanceResult.ifPresent(
                    iupr -> {
                        removeGivenActionInputUsages(iupr, action::removeOtherProductInputUsages);
                        action.addAllOtherProductInputUsages(iupr.getInputUsagesCreated());
                    }
            );
            mineralFertilizersSpreadingActionDao.update(action);
        } else {
            mineralIUsagesPersistanceResult.ifPresent(iupr -> action.addAllMineralProductInputUsages(iupr.getInputUsagesCreated()));
            otherProductInputUsagesPersistanceResult.ifPresent(iupr -> action.addAllOtherProductInputUsages(iupr.getInputUsagesCreated()));
            mineralFertilizersSpreadingActionDao.create(action);
        }
    }

    protected void createOrUpdatePesticidesSpreadingAction(
            AbstractActionDto actionDto,
            Map<String, AbstractAction> persistedActions,
            PracticedIntervention practicedIntervention,
            EffectiveIntervention effectiveIntervention,
            Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            ImmutableList<String> toolsCouplingCodes) {

        PesticidesSpreadingActionDto dto = (PesticidesSpreadingActionDto) actionDto;
        PesticidesSpreadingAction action = actionDto.getTopiaId().isPresent() && !actionDto.getTopiaId().get().startsWith("NEW-ACTION") ?
                (PesticidesSpreadingAction) persistedActions.remove(actionDto.getTopiaId().get()) : pesticidesSpreadingActionDao.newInstance();

        if (!action.isPersisted()) {
            action.setPracticedIntervention(practicedIntervention);
            action.setEffectiveIntervention(effectiveIntervention);
            RefInterventionAgrosystTravailEDI mainAction = mainActionByIds.get(actionDto.getMainActionId());
            if (mainAction == null) {
                throw new AgrosystTechnicalException("The main action is required !");
            }
            action.setMainAction(mainAction);
        }

        bindDtoToActionCommonFields(
                toolsCouplingCodes,
                dto,
                action);

        action.setBoiledQuantity(dto.getBoiledQuantity());
        action.setBoiledQuantityPerTrip(dto.getBoiledQuantityPerTrip());
        action.setTripFrequency(dto.getTripFrequency());
        action.setAntiDriftNozzle(dto.isAntiDriftNozzle());
        action.setProportionOfTreatedSurface(dto.getProportionOfTreatedSurface());


        final Optional<InputPersistanceResults<PesticideProductInputUsage>> pesticideProductInputUsagesPersistanceResult = inputUsageService.persistOrDeleteAbstractPhytoProductUsageInputUsages(dto.getPhytoProductInputUsageDtos(), action.getPesticideProductInputUsages(), domainInputStockUnitByIds);

        final Optional<InputPersistanceResults<OtherProductInputUsage>> otherProductInputUsagesPersistanceResult = inputUsageService.persistOrDeleteOtherProductInputUsages(dto.getOtherProductInputUsageDtos(), action.getOtherProductInputUsages(), domainInputStockUnitByIds);


        if (action.isPersisted()) {
            pesticideProductInputUsagesPersistanceResult.ifPresent(
                    iupr -> {
                        removeGivenActionInputUsages(iupr, action::removePesticideProductInputUsages);
                        action.addAllPesticideProductInputUsages(iupr.getInputUsagesCreated());
                    }
            );
            otherProductInputUsagesPersistanceResult.ifPresent(
                    iupr -> {
                        removeGivenActionInputUsages(iupr, action::removeOtherProductInputUsages);
                        action.addAllOtherProductInputUsages(iupr.getInputUsagesCreated());
                    }
            );
            pesticidesSpreadingActionDao.update(action);
        } else {
            pesticideProductInputUsagesPersistanceResult.ifPresent(iupr -> action.addAllPesticideProductInputUsages(iupr.getInputUsagesCreated()));
            otherProductInputUsagesPersistanceResult.ifPresent(iupr -> action.addAllOtherProductInputUsages(iupr.getInputUsagesCreated()));
            pesticidesSpreadingActionDao.create(action);
        }
    }

    protected void createOrUpdateOtherAction(
            AbstractActionDto actionDto,
            Map<String, AbstractAction> persistedActions,
            PracticedIntervention practicedIntervention,
            EffectiveIntervention effectiveIntervention,
            Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            ImmutableList<String> toolsCouplingCodes) {

        OtherActionDto dto = (OtherActionDto) actionDto;
        OtherAction action = actionDto.getTopiaId().isPresent() && !actionDto.getTopiaId().get().startsWith("NEW-ACTION") ?
                (OtherAction) persistedActions.remove(actionDto.getTopiaId().get()) : otherActionDao.newInstance();

        if (!action.isPersisted()) {
            action.setPracticedIntervention(practicedIntervention);
            action.setEffectiveIntervention(effectiveIntervention);
            RefInterventionAgrosystTravailEDI mainAction = mainActionByIds.get(actionDto.getMainActionId());
            if (mainAction == null) {
                throw new AgrosystTechnicalException("The main action is required !");
            }
            action.setMainAction(mainAction);
        }

        bindDtoToActionCommonFields(
                toolsCouplingCodes,
                dto,
                action);

        final Optional<InputPersistanceResults<SubstrateInputUsage>> substrateInputUsageInputPersistanceResults = inputUsageService.persistOrDeleteSubstrateInputUsages(
                dto.getSubstrateInputUsageDtos(),
                action.getSubstrateInputUsages(),
                domainInputStockUnitByIds);

        final Optional<InputPersistanceResults<PotInputUsage>> potInputUsageInputPersistanceResults = inputUsageService.persistOrDeletePotInputUsages(
                dto.getPotInputUsageDtos(),
                action.getPotInputUsages(),
                domainInputStockUnitByIds);

        final Optional<InputPersistanceResults<OtherProductInputUsage>> otherProductInputUsageInputPersistanceResults = inputUsageService.persistOrDeleteOtherProductInputUsages(
                dto.getOtherProductInputUsageDtos(),
                action.getOtherProductInputUsages(),
                domainInputStockUnitByIds);

        if (action.isPersisted()) {
            substrateInputUsageInputPersistanceResults.ifPresent(
                    iupr -> {
                        removeGivenActionInputUsages(iupr, action::removeSubstrateInputUsages);
                        action.addAllSubstrateInputUsages(iupr.getInputUsagesCreated());
                    }
            );

            potInputUsageInputPersistanceResults.ifPresent(
                    iupr -> {
                        removeGivenActionInputUsages(iupr, action::removePotInputUsages);
                        action.addAllPotInputUsages(iupr.getInputUsagesCreated());
                    }
            );
            otherProductInputUsageInputPersistanceResults.ifPresent(
                    iupr -> {
                        removeGivenActionInputUsages(iupr, action::removeOtherProductInputUsages);
                        action.addAllOtherProductInputUsages(iupr.getInputUsagesCreated());
                    }
            );
            otherActionDao.update(action);
        } else {
            substrateInputUsageInputPersistanceResults.ifPresent(
                    iupr -> action.addAllSubstrateInputUsages(iupr.getInputUsagesCreated()));

            potInputUsageInputPersistanceResults.ifPresent(
                    iupr -> action.addAllPotInputUsages(iupr.getInputUsagesCreated()));

            otherProductInputUsageInputPersistanceResults.ifPresent(
                    iupr -> action.addAllOtherProductInputUsages(iupr.getInputUsagesCreated()));

            otherActionDao.create(action);
        }
    }

    protected void createOrUpdateMaintenancePruningVinesAction(
            AbstractActionDto actionDto,
            Map<String, AbstractAction> persistedActions,
            PracticedIntervention practicedIntervention,
            EffectiveIntervention effectiveIntervention,
            Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            ImmutableList<String> toolsCouplingCodes) {

        MaintenancePruningVinesActionDto dto = (MaintenancePruningVinesActionDto) actionDto;
        MaintenancePruningVinesAction action = actionDto.getTopiaId().isPresent() && !actionDto.getTopiaId().get().startsWith("NEW-ACTION") ?
                (MaintenancePruningVinesAction) persistedActions.remove(actionDto.getTopiaId().get()) : maintenancePruningVinesActionDao.newInstance();

        if (!action.isPersisted()) {
            action.setPracticedIntervention(practicedIntervention);
            action.setEffectiveIntervention(effectiveIntervention);
            RefInterventionAgrosystTravailEDI mainAction = mainActionByIds.get(actionDto.getMainActionId());
            if (mainAction == null) {
                throw new AgrosystTechnicalException("The main action is required !");
            }
            action.setMainAction(mainAction);
        }

        bindDtoToActionCommonFields(
                toolsCouplingCodes,
                dto,
                action);

        action.setQuantityUnitEDI(dto.getQuantityUnitEDI());

        final Optional<InputPersistanceResults<OtherProductInputUsage>> inputUsagesPersistanceResult = inputUsageService.persistOrDeleteOtherProductInputUsages(
                dto.getOtherProductInputUsageDtos(),
                action.getOtherProductInputUsages(),
                domainInputStockUnitByIds);

        if (action.isPersisted()) {
            inputUsagesPersistanceResult.ifPresent(
                    iupr -> {
                        removeGivenActionInputUsages(iupr, action::removeOtherProductInputUsages);
                        action.addAllOtherProductInputUsages(iupr.getInputUsagesCreated());
                    }
            );
            maintenancePruningVinesActionDao.update(action);
        } else {
            inputUsagesPersistanceResult.ifPresent(iupr -> action.addAllOtherProductInputUsages(iupr.getInputUsagesCreated()));
            maintenancePruningVinesActionDao.create(action);
        }
    }

    protected void createOrUpdateOrganicFertilizersSpreadingAction(
            AbstractActionDto actionDto,
            Map<String, AbstractAction> persistedActions,
            PracticedIntervention practicedIntervention,
            EffectiveIntervention effectiveIntervention,
            Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            ImmutableList<String> toolsCouplingCodes) {

        OrganicFertilizersSpreadingActionDto dto = (OrganicFertilizersSpreadingActionDto) actionDto;
        OrganicFertilizersSpreadingAction action = actionDto.getTopiaId().isPresent() && !actionDto.getTopiaId().get().startsWith("NEW-ACTION") ?
                (OrganicFertilizersSpreadingAction) persistedActions.remove(actionDto.getTopiaId().get()) : organicFertilizersSpreadingActionDao.newInstance();

        if (!action.isPersisted()) {
            action.setPracticedIntervention(practicedIntervention);
            action.setEffectiveIntervention(effectiveIntervention);
            RefInterventionAgrosystTravailEDI mainAction = mainActionByIds.get(actionDto.getMainActionId());
            if (mainAction == null) {
                throw new AgrosystTechnicalException("The main action is required !");
            }
            action.setMainAction(mainAction);
        }

        bindDtoToActionCommonFields(
                toolsCouplingCodes,
                dto,
                action);

        action.setLandfilledWaste(dto.isLandfilledWaste());


        final Optional<InputPersistanceResults<OrganicProductInputUsage>> organicProductInputUsagesInputPersistanceResult = inputUsageService.persistOrDeleteOrganicProductInputUsages(
                dto.getOrganicProductInputUsageDtos(),
                action.getOrganicProductInputUsages(),
                domainInputStockUnitByIds);
        final Optional<InputPersistanceResults<OtherProductInputUsage>> otherProductInputUsagesPersistanceResult = inputUsageService.persistOrDeleteOtherProductInputUsages(
                dto.getOtherProductInputUsageDtos(),
                action.getOtherProductInputUsages(),
                domainInputStockUnitByIds);


        if (action.isPersisted()) {
            organicProductInputUsagesInputPersistanceResult.ifPresent(
                    iupr -> {
                        removeGivenActionInputUsages(iupr, action::removeOrganicProductInputUsages);
                        action.addAllOrganicProductInputUsages(iupr.getInputUsagesCreated());
                    }
            );
            otherProductInputUsagesPersistanceResult.ifPresent(
                    iupr -> {
                        removeGivenActionInputUsages(iupr, action::removeOtherProductInputUsages);
                        action.addAllOtherProductInputUsages(iupr.getInputUsagesCreated());
                    }
            );
            organicFertilizersSpreadingActionDao.update(action);
        } else {
            organicProductInputUsagesInputPersistanceResult.ifPresent(iupr -> action.addAllOrganicProductInputUsages(iupr.getInputUsagesCreated()));
            otherProductInputUsagesPersistanceResult.ifPresent(iupr -> action.addAllOtherProductInputUsages(iupr.getInputUsagesCreated()));
            organicFertilizersSpreadingActionDao.create(action);
        }
    }

    protected void createOrUpdateIrrigAction(
            AbstractActionDto actionDto,
            Map<String, AbstractAction> persistedActions,
            PracticedIntervention practicedIntervention,
            EffectiveIntervention effectiveIntervention,
            Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            ImmutableList<String> toolsCouplingCodes,
            Domain domain) {

        IrrigationActionDto dto = (IrrigationActionDto) actionDto;
        IrrigationAction action = actionDto.getTopiaId().isPresent() && !actionDto.getTopiaId().get().startsWith("NEW-ACTION") ?
                (IrrigationAction) persistedActions.remove(actionDto.getTopiaId().get()) : irrigationActionDao.newInstance();

        if (!action.isPersisted()) {
            action.setPracticedIntervention(practicedIntervention);
            action.setEffectiveIntervention(effectiveIntervention);
            RefInterventionAgrosystTravailEDI mainAction = mainActionByIds.get(actionDto.getMainActionId());
            if (mainAction == null) {
                throw new AgrosystTechnicalException("The main action is required !");
            }
            action.setMainAction(mainAction);
        }

        bindDtoToActionCommonFields(
                toolsCouplingCodes,
                dto,
                action);

        action.setWaterQuantityAverage(dto.getWaterQuantityAverage());
        action.setWaterQuantityMax(dto.getWaterQuantityMax());
        action.setWaterQuantityAverage(dto.getWaterQuantityAverage());
        action.setWaterQuantityMedian(dto.getWaterQuantityMedian());
        action.setAzoteQuantity(dto.getAzoteQuantity());

        final IrrigationInputUsage irrigationInputUsage = inputUsageService.persistOrDeleteIrrigationInputUsages(
                domainInputStockUnitByIds,
                action,
                domain);

        action.setIrrigationInputUsage(irrigationInputUsage);

        final Optional<InputPersistanceResults<OtherProductInputUsage>> otherProductInputUsagesPersistanceResult = inputUsageService.persistOrDeleteOtherProductInputUsages(
                dto.getOtherProductInputUsageDtos(),
                action.getOtherProductInputUsages(),
                domainInputStockUnitByIds);


        if (action.isPersisted()) {
            otherProductInputUsagesPersistanceResult.ifPresent(
                    iupr -> {
                        removeGivenActionInputUsages(iupr, action::removeOtherProductInputUsages);
                        action.addAllOtherProductInputUsages(iupr.getInputUsagesCreated());
                    }
            );
            irrigationActionDao.update(action);
        } else {
            otherProductInputUsagesPersistanceResult.ifPresent(
                    iupr -> action.addAllOtherProductInputUsages(iupr.getInputUsagesCreated())
            );
            irrigationActionDao.create(action);
        }
    }

    protected void createOrUpdateBiologicalControlAction(
            AbstractActionDto actionDto,
            Map<String, AbstractAction> persistedActions,
            PracticedIntervention practicedIntervention,
            EffectiveIntervention effectiveIntervention,
            Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            ImmutableList<String> toolsCouplingCodes) {

        BiologicalControlActionDto dto = (BiologicalControlActionDto) actionDto;
        BiologicalControlAction action = actionDto.getTopiaId().isPresent() && !actionDto.getTopiaId().get().startsWith("NEW-ACTION") ?
                (BiologicalControlAction) persistedActions.remove(actionDto.getTopiaId().get()) : biologicalControlActionDao.newInstance();

        if (!action.isPersisted()) {
            action.setPracticedIntervention(practicedIntervention);
            action.setEffectiveIntervention(effectiveIntervention);
            RefInterventionAgrosystTravailEDI mainAction = mainActionByIds.get(actionDto.getMainActionId());
            if (mainAction == null) {
                throw new AgrosystTechnicalException("The main action is required !");
            }
            action.setMainAction(mainAction);
        }

        bindDtoToActionCommonFields(
                toolsCouplingCodes,
                dto,
                action);

        action.setBoiledQuantity(dto.getBoiledQuantity());
        action.setBoiledQuantityPerTrip(dto.getBoiledQuantityPerTrip());
        action.setTripFrequency(dto.getTripFrequency());
        action.setProportionOfTreatedSurface(dto.getProportionOfTreatedSurface());

        final Optional<InputPersistanceResults<BiologicalProductInputUsage>> biologicalProductInputUsagesPersistanceResult = inputUsageService.persistOrDeleteBiologicalProductInputUsages(
                dto.getPhytoProductInputUsageDtos(),
                domainInputStockUnitByIds,
                action.getBiologicalProductInputUsages());
        final Optional<InputPersistanceResults<OtherProductInputUsage>> otherProductInputUsagesPersistanceResult = inputUsageService.persistOrDeleteOtherProductInputUsages(
                dto.getOtherProductInputUsageDtos(),
                action.getOtherProductInputUsages(),
                domainInputStockUnitByIds);

        if (action.isPersisted()) {
            biologicalProductInputUsagesPersistanceResult.ifPresent(
                    iupr -> {
                        removeGivenActionInputUsages(iupr, action::removeBiologicalProductInputUsages);
                        action.addAllBiologicalProductInputUsages(iupr.getInputUsagesCreated());
                    }
            );
            otherProductInputUsagesPersistanceResult.ifPresent(
                    iupr -> {
                        removeGivenActionInputUsages(iupr, action::removeOtherProductInputUsages);
                        action.addAllOtherProductInputUsages(iupr.getInputUsagesCreated());
                    }
            );
            biologicalControlActionDao.update(action);
        } else {
            biologicalProductInputUsagesPersistanceResult.ifPresent(iupr -> action.addAllBiologicalProductInputUsages(iupr.getInputUsagesCreated()));
            otherProductInputUsagesPersistanceResult.ifPresent(iupr -> action.addAllOtherProductInputUsages(iupr.getInputUsagesCreated()));
            biologicalControlActionDao.create(action);
        }
    }

    protected void createOrUpdatePracticedHarvestingAction(
            AbstractActionDto actionDto,
            Map<String, AbstractAction> persistedActions,
            PracticedIntervention practicedIntervention,
            Collection<CroppingPlanSpecies> croppingPlanSpecies,
            Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds,
            PracticedSystem practicedSystem,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode,
            ImmutableList<String> toolsCouplingCodes
    ) {

        Preconditions.checkNotNull(practicedIntervention, "Required PracticedIntervention not provide");
        Preconditions.checkNotNull(practicedSystem, "Required PracticedSystem not provide");

        HarvestingActionDto harvestingActionDto = (HarvestingActionDto) actionDto;
        if (harvestingActionDto.getCattleCode().isPresent()) {
            Domain d = practicedSystem.getGrowingSystem().getGrowingPlan().getDomain();
            checkCattlePrecondition(d, harvestingActionDto);
        }

        HarvestingAction actionEntity = bindHarvestingActionDtoToEntity(
                harvestingActionDto,
                persistedActions,
                practicedIntervention,
                null,
                croppingPlanSpecies,
                mainActionByIds,
                practicedSystem,
                null,
                domainInputStockUnitByIds,
                codeEspBotCodeQualiBySpeciesCode,
                toolsCouplingCodes
        );

        if (actionEntity.isPersisted()) {
            harvestingActionDao.update(actionEntity);
        } else {
            harvestingActionDao.create(actionEntity);
        }

    }

    /**
     * @param actionDto                 actionToPersist
     * @param persistedActions          persistedAction
     * @param practicedIntervention     practicedIntervention
     * @param effectiveIntervention     effectiveIntervention
     * @param cropIdentifier            Practiced: crop code, Effective crop topiaId
     * @param speciesIdentifiers        CroppingPanSpecies: Practiced: code, Effective Crop topiaId
     * @param mainActionByIds           RefInterventionAgrosystTravailEDI by Ids
     * @param toolsCouplingCodes        MainAction's tools coupling code
     * @param domainInputStockUnitByIds The related domain input stocks
     */
    protected void createOrUpdateSeedingActionUsage(
            AbstractActionDto actionDto,
            Map<String, AbstractAction> persistedActions,
            PracticedIntervention practicedIntervention,
            EffectiveIntervention effectiveIntervention,
            String cropIdentifier,
            Set<String> speciesIdentifiers,
            Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds,
            ImmutableList<String> toolsCouplingCodes,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {

        RefInterventionAgrosystTravailEDI mainAction = mainActionByIds.get(actionDto.getMainActionId());
        Preconditions.checkNotNull(mainAction, "The main action is required !");

        SeedingActionUsageDto dto = (SeedingActionUsageDto) actionDto;
        SeedingActionUsage action = actionDto.getTopiaId().isPresent() && !actionDto.getTopiaId().get().startsWith("NEW-ACTION") ?
                (SeedingActionUsage) persistedActions.remove(actionDto.getTopiaId().get()) : seedingActionUsageDao.newInstance();

        bindDtoToActionCommonFields(
                toolsCouplingCodes,
                dto,
                action);

        action.setDeepness(dto.getDeepness());
        action.setYealdTarget(dto.getYealdTarget());
        action.setYealdUnit(dto.getYealdUnit());

        if (!action.isPersisted()) {
            action.setPracticedIntervention(practicedIntervention);
            action.setEffectiveIntervention(effectiveIntervention);
            action.setMainAction(mainAction);
            action.setSeedType(dto.getSeedType());
            action.setSeedLotInputUsage(new ArrayList<>());
        }

        inputUsageService.persistOrDeleteSeedLotInputUsages(
                dto.getSeedLotInputUsageDtos(),
                action.getSeedLotInputUsage(),
                domainInputStockUnitByIds,
                cropIdentifier,
                speciesIdentifiers);

        Optional<InputPersistanceResults<OtherProductInputUsage>> optionalOtherInputUsageResults = inputUsageService.persistOrDeleteOtherProductInputUsages(dto.getOtherProductInputUsageDtos(), action.getOtherProductInputUsages(), domainInputStockUnitByIds);
        optionalOtherInputUsageResults.ifPresent(
                r -> {
                    r.getInputUsagesCreated().forEach(action::addOtherProductInputUsages);
                    r.getInputUsagesRemoved().forEach(action::removeOtherProductInputUsages);
                });

        final Optional<InputPersistanceResults<SubstrateInputUsage>> substrateInputUsageInputPersistanceResults = inputUsageService.persistOrDeleteSubstrateInputUsages(
                dto.getSubstrateInputUsageDtos(),
                action.getSubstrateInputUsage(),
                domainInputStockUnitByIds);

        substrateInputUsageInputPersistanceResults.ifPresent(
                r -> {
                    r.getInputUsagesCreated().forEach(action::addSubstrateInputUsage);
                    r.getInputUsagesRemoved().forEach(action::removeSubstrateInputUsage);
                });

        if (action.isPersisted()) {
            seedingActionUsageDao.update(action);
        } else {
            seedingActionUsageDao.create(action);
        }
    }

    protected void createOrUpdateCarriageAction(
            AbstractActionDto actionDto,
            Map<String, AbstractAction> persistedActions,
            PracticedIntervention practicedIntervention,
            EffectiveIntervention effectiveIntervention,
            Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds,
            ImmutableList<String> toolsCouplingCodes) {

        CarriageActionDto dto = (CarriageActionDto) actionDto;
        CarriageAction action = actionDto.getTopiaId().isPresent() && !actionDto.getTopiaId().get().startsWith("NEW-ACTION") ?
                (CarriageAction) persistedActions.remove(actionDto.getTopiaId().get()) : carriageActionDao.newInstance();

        if (!action.isPersisted()) {
            action.setPracticedIntervention(practicedIntervention);
            action.setEffectiveIntervention(effectiveIntervention);
            RefInterventionAgrosystTravailEDI mainAction = mainActionByIds.get(actionDto.getMainActionId());
            if (mainAction == null) {
                throw new AgrosystTechnicalException("The main action is required !");
            }
            action.setMainAction(mainAction);
        }

        bindDtoToActionCommonFields(
                toolsCouplingCodes,
                dto,
                action);

        action.setLoadCapacity(dto.getLoadCapacity());
        action.setTripFrequency(dto.getTripFrequency());
        action.setCapacityUnit(dto.getCapacityUnit());

        if (action.isPersisted()) {
            carriageActionDao.update(action);
        } else {
            carriageActionDao.create(action);
        }
    }

    protected void createOrUpdateTillageAction(
            AbstractActionDto actionDto,
            Map<String, AbstractAction> persistedActions,
            PracticedIntervention practicedIntervention,
            EffectiveIntervention effectiveIntervention,
            Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds,
            ImmutableList<String> toolsCouplingCodes) {

        TillageActionDto dto = (TillageActionDto) actionDto;
        TillageAction action = actionDto.getTopiaId().isPresent() && !actionDto.getTopiaId().get().startsWith("NEW-ACTION") ?
                (TillageAction) persistedActions.remove(actionDto.getTopiaId().get()) : tillageActionDao.newInstance();

        if (!action.isPersisted()) {
            action.setPracticedIntervention(practicedIntervention);
            action.setEffectiveIntervention(effectiveIntervention);
            RefInterventionAgrosystTravailEDI mainAction = mainActionByIds.get(actionDto.getMainActionId());
            if (mainAction == null) {
                throw new AgrosystTechnicalException("The main action is required !");
            }
            action.setMainAction(mainAction);
        }

        bindDtoToActionCommonFields(
                toolsCouplingCodes,
                dto,
                action);

        action.setTillageDepth(dto.getTillageDepth());
        action.setOtherSettingTool(dto.getOtherSettingTool());

        if (action.isPersisted()) {
            tillageActionDao.update(action);
        } else {
            tillageActionDao.create(action);
        }

    }

    private void bindDtoToActionCommonFields(
            ImmutableList<String> toolsCouplingCodes,
            AbstractActionDto dto,
            AbstractAction action) {

        Optional<String> toolsCouplingCode =
                toolsCouplingCodes.contains(dto.getToolsCouplingCode().orElse(null)) ?
                        dto.getToolsCouplingCode() : Optional.empty();
        action.setToolsCouplingCode(toolsCouplingCode.orElse(""));
        action.setComment(dto.getComment().orElse(""));
    }

    protected void createOrUpdateEffectiveHarvestingAction(
            AbstractActionDto actionDto,
            Map<String, AbstractAction> persistedActions,
            EffectiveIntervention intervention,
            Collection<CroppingPlanSpecies> croppingPlanSpecies,
            Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds,
            Zone zone,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode,
            ImmutableList<String> toolsCouplingCodes) {

        Preconditions.checkNotNull(intervention, "Required EffectiveIntervention not provide");
        Preconditions.checkNotNull(zone, "Required Zone not provide");

        HarvestingActionDto harvestingActionDto = (HarvestingActionDto) actionDto;
        if (harvestingActionDto.getCattleCode().isPresent()) {
            Domain d = zone.getPlot().getDomain();
            checkCattlePrecondition(d, harvestingActionDto);
        }

        HarvestingAction actionEntity = bindHarvestingActionDtoToEntity(
                harvestingActionDto,
                persistedActions,
                null,
                intervention,
                croppingPlanSpecies,
                mainActionByIds,
                null,
                zone,
                domainInputStockUnitByIds,
                codeEspBotCodeQualiBySpeciesCode,
                toolsCouplingCodes);

        if (actionEntity.isPersisted()) {
            harvestingActionDao.update(actionEntity);
        } else {
            harvestingActionDao.create(actionEntity);
        }
    }

    protected void checkCattlePrecondition(Domain d, HarvestingActionDto harvestingActionDto) {
        Collection<LivestockUnit> livestockUnits = livestockUnitDao.forDomainEquals(d).findAll();
        final Optional<String> optionalCattleCode = harvestingActionDto.getCattleCode();
        if (optionalCattleCode.isPresent()) {
            final String cattleCode = optionalCattleCode.get();
            Preconditions.checkArgument(livestockUnits.stream()
                            .map(livestockUnit -> livestockUnit.getCattles().stream()
                                    .filter(c -> c.getCode().contentEquals(cattleCode))).findAny().isPresent(),
                    String.format("Required Cattle with code '%s' not provide", cattleCode));
        }
    }

    protected HarvestingAction bindHarvestingActionDtoToEntity(
            HarvestingActionDto harvestingActionDto,
            Map<String, AbstractAction> persistedActions,
            PracticedIntervention practicedIntervention,
            EffectiveIntervention effectiveIntervention,
            Collection<CroppingPlanSpecies> croppingPlanSpecies,
            Map<String, RefInterventionAgrosystTravailEDI> mainActionByIds,
            PracticedSystem practicedSystem,
            Zone zone,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode,
            ImmutableList<String> toolsCouplingCodes) {

        MultiValuedMap<String, CroppingPlanSpecies> cpsByCodes = new HashSetValuedHashMap<>();
        croppingPlanSpecies.forEach(cps -> cpsByCodes.put(cps.getCode(), cps));

        HarvestingAction entity;

        if (harvestingActionDto.getTopiaId().isPresent() && !harvestingActionDto.getTopiaId().get().startsWith("NEW-ACTION")) {

            AbstractAction persistedEntity = persistedActions.remove(harvestingActionDto.getTopiaId().get());
            Preconditions.checkNotNull(persistedEntity, String.format("Expecting Harvesting action with id %s", harvestingActionDto.getTopiaId().get()));

            entity = (HarvestingAction) persistedEntity;

        } else {

            entity = harvestingActionDao.newInstance();

            RefInterventionAgrosystTravailEDI mainAction = mainActionByIds.get(harvestingActionDto.getMainActionId());
            Preconditions.checkNotNull(mainAction, "The main action is required !");
            entity.setMainAction(mainAction);
            entity.setPracticedIntervention(practicedIntervention);
            entity.setEffectiveIntervention(effectiveIntervention);

            entity = harvestingActionDao.create(entity);

        }

        Collection<String> speciesCodes = practicedIntervention != null ?
                practicedIntervention.getSpeciesStades().stream().map(PracticedSpeciesStade::getSpeciesCode).toList() :
                effectiveIntervention.getSpeciesStades().stream().map(ess -> ess.getCroppingPlanSpecies().getCode()).toList();

        boolean isWineSpecies = ActionService.IS_WINE_SPECIES_FOUND(codeEspBotCodeQualiBySpeciesCode, speciesCodes);

        bindDtoToActionCommonFields(
                toolsCouplingCodes,
                harvestingActionDto,
                entity);

        shallowBindVHarvestingAction(harvestingActionDto, entity, isWineSpecies);

        bindValorisations(
                cpsByCodes,
                harvestingActionDto,
                entity,
                practicedSystem,
                zone,
                codeEspBotCodeQualiBySpeciesCode
        );

        bindHarvestingActionOtherInputProductUsages(
                harvestingActionDto,
                domainInputStockUnitByIds,
                entity);

        return entity;

    }

    protected void bindHarvestingActionOtherInputProductUsages(
            HarvestingActionDto harvestingActionDto,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            HarvestingAction entity) {

        final Optional<InputPersistanceResults<OtherProductInputUsage>> otherProductInputUsagesPersistanceResult = inputUsageService.persistOrDeleteOtherProductInputUsages(
                harvestingActionDto.getOtherProductInputUsageDtos(),
                entity.getOtherProductInputUsages(),
                domainInputStockUnitByIds);

        if (entity.isPersisted()) {

            otherProductInputUsagesPersistanceResult.ifPresent(
                    iupr -> {
                        removeGivenActionInputUsages(iupr, entity::removeOtherProductInputUsages);
                        entity.addAllOtherProductInputUsages(iupr.getInputUsagesCreated());
                    }
            );
        } else {
            otherProductInputUsagesPersistanceResult.ifPresent(iupr -> entity.addAllOtherProductInputUsages(iupr.getInputUsagesCreated()));
        }
    }

    protected void bindValorisations(
            MultiValuedMap<String, CroppingPlanSpecies> cpsByCodes,
            HarvestingActionDto harvestingActionDto,
            HarvestingAction harvestingAction,
            PracticedSystem practicedSystem,
            Zone zone,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {

        Domain domain = practicedSystem != null ? practicedSystem.getGrowingSystem().getGrowingPlan().getDomain() : zone.getPlot().getDomain();

        Collection<HarvestingActionValorisation> valorisations = CollectionUtils.emptyIfNull(harvestingAction.getValorisations());
        Map<String, HarvestingPrice> harvestingPriceByValorisationIds = priceService.getPriceForValorisations(CollectionUtils.emptyIfNull(valorisations));

        Map<HarvestingActionValorisationTopiaDao.UniqueKeyValorisation, HarvestingActionValorisation> persistedValorisationsByKey = new HashMap<>();

        Set<HarvestingActionValorisationTopiaDao.UniqueKeyValorisation> uniqueKeyValorisations = new HashSet<>();
        valorisations.forEach(
            val -> {
                HarvestingActionValorisationTopiaDao.UniqueKeyValorisation key = new HarvestingActionValorisationTopiaDao.UniqueKeyValorisation(
                        harvestingAction,
                        val.getSpeciesCode(),
                        val.getDestination()
                );
                if (!uniqueKeyValorisations.contains(key)) {
                    uniqueKeyValorisations.add(key);
                    persistedValorisationsByKey.put(key, val);
                } else {
                    if (LOGGER.isErrorEnabled()) {
                        LOGGER.error(String.format("Valorisation en double remontée sur l'action %s %s", harvestingAction.getTopiaId(), key));
                    }
                }
            }
        );

        Collection<HarvestingActionValorisation> valorisationsToCreateOrUpdate = new ArrayList<>();

        List<HarvestingActionValorisationDto> valorisationDtos = harvestingActionDto.getValorisationDtos();

        Set<String> destinationIds = valorisationDtos.stream().map(HarvestingActionValorisationDto::getDestinationId).collect(Collectors.toSet());
        Map<String, RefDestination> destinationsByIds = Maps.uniqueIndex(referentialService.loadDestinationsForIds(destinationIds), TopiaEntity::getTopiaId);

        Map<String, RefQualityCriteria> refQualityCriteriaByIds = getRefQualityCriteriaByIds(valorisationDtos);
        Map<String, RefQualityCriteriaClass> refQualityCriteriaClassByIds = getRefQualityCriteriaClassByIds(valorisationDtos);

        Map<HarvestingActionValorisationDto, List<RefHarvestingPrice>> refHarvestingPricesForValorisationDtos = referentialService.getRefHarvestingPricesForValorisationDtos(
                valorisationDtos,
                codeEspBotCodeQualiBySpeciesCode);

        for (HarvestingActionValorisationDto valorisationDto : valorisationDtos) {

            RefDestination destination = destinationsByIds.get(valorisationDto.getDestinationId());
            if (destination == null)
                throw new AgrosystTechnicalException(String.format("Expected destination with id '%s' not found", valorisationDto.getDestinationId()));

            HarvestingActionValorisationTopiaDao.UniqueKeyValorisation valKey = new HarvestingActionValorisationTopiaDao.UniqueKeyValorisation(
                    harvestingAction,
                    valorisationDto.getSpeciesCode(),
                    destination
            );

            final Collection<CroppingPlanSpecies> allSpeciesForCode = cpsByCodes.get(valorisationDto.getSpeciesCode());
            if (CollectionUtils.isEmpty(allSpeciesForCode)) {
                continue;
            }

            HarvestingActionValorisation valorisationEntity;
            HarvestingPrice persistedPrice = null;
            if (persistedValorisationsByKey.get(valKey) != null) {

                valorisationEntity = persistedValorisationsByKey.remove(valKey);// remove to avoid pickup many times

            } else {
                if (uniqueKeyValorisations.contains(valKey)) {
                    if (LOGGER.isErrorEnabled()) {
                        String targetId = practicedSystem != null ? "du synthétisé :" + practicedSystem.getTopiaId() : "de la zone :" + zone.getTopiaId();
                        String interventionId = harvestingAction.getPracticedIntervention() != null ? harvestingAction.getPracticedIntervention().getTopiaId() : harvestingAction.getEffectiveIntervention().getTopiaId();
                        String interventionName = harvestingAction.getPracticedIntervention() != null ? harvestingAction.getPracticedIntervention().getName() : harvestingAction.getEffectiveIntervention().getName();
                        LOGGER.error(String.format("Valorisation en double remontée à la sauvegarde %s intervention %s %s",
                                targetId,
                                interventionName,
                                interventionId
                        ));
                    }
                    continue;
                } else {
                    uniqueKeyValorisations.add(valKey);
                    valorisationEntity = harvestingActionValorisationDao.newInstance();
                    valorisationsToCreateOrUpdate.add(valorisationEntity);
                }
            }

            String valorisationId = valorisationEntity.getTopiaId();
            if (StringUtils.isNotEmpty(valorisationId)) {
                persistedPrice = harvestingPriceByValorisationIds.remove(valorisationId);
            }

            CroppingPlanSpecies cps = allSpeciesForCode.iterator().next();

            shallowBindValorisation(valorisationDto, valorisationEntity, destination);

            bindQualityCriterias(refQualityCriteriaByIds, refQualityCriteriaClassByIds, valorisationDto, valorisationEntity);

            priceService.bindZoneOrPracticedHarvestingPrice(
                    valorisationDto,
                    valorisationEntity,
                    persistedPrice,
                    destination,
                    cps,
                    domain,
                    practicedSystem,
                    zone,
                    codeEspBotCodeQualiBySpeciesCode,
                    refHarvestingPricesForValorisationDtos);

        }

        priceService.removeHarvestingPrices(harvestingPriceByValorisationIds.values());

        // remove valorisations from action
        persistedValorisationsByKey.values().forEach(harvestingAction::removeValorisations);

        harvestingAction.addAllValorisations(valorisationsToCreateOrUpdate.stream().toList());
    }

    protected void shallowBindVHarvestingAction(HarvestingActionDto dto, HarvestingAction entity, boolean isWineSpecies) {
        // Pâturage
        if (PASTURE.equalsIgnoreCase(entity.getMainAction().getReference_code())) {
            dto.getCattleCode().ifPresent(entity::setCattleCode);
            dto.getPastureType().ifPresent(entity::setPastureType);
            entity.setPastureLoad(dto.getPastureLoad());
            entity.setPasturingAtNight(dto.isPasturingAtNight());
        } else
            // vendanges
            if (isWineSpecies) {
                Preconditions.checkArgument(
                        dto.getWineValorisations().isPresent() &&
                                !dto.getWineValorisations().get().isEmpty(),
                        "");
                dto.getWineValorisations().ifPresent(entity::setWineValorisations);
            }
    }

    protected Map<String, RefQualityCriteriaClass> getRefQualityCriteriaClassByIds(List<HarvestingActionValorisationDto> valorisationDtos) {

        Set<String> qualityCriteriaRefQualityCriteriaClassIds = valorisationDtos.stream().filter(valorisationDto -> valorisationDto.getQualityCriteriaDtos().isPresent())
                .map(valorisationDto -> valorisationDto.getQualityCriteriaDtos().get())
                .flatMap(qcs -> qcs.stream()
                        .filter(qcdto -> qcdto.getRefQualityCriteriaClassId().isPresent())
                        .map(qcdto -> qcdto.getRefQualityCriteriaClassId().get())).collect(Collectors.toSet());
        Collection<RefQualityCriteriaClass> refRefQualityCriteriaClass = CollectionUtils.isNotEmpty(qualityCriteriaRefQualityCriteriaClassIds) ? referentialService.loadRefQualityCriteriaClassForIds(qualityCriteriaRefQualityCriteriaClassIds) : new ArrayList<>();
        Map<String, RefQualityCriteriaClass> refQualityCriteriaClassByIds = Maps.uniqueIndex(refRefQualityCriteriaClass, TopiaEntity::getTopiaId);

        return refQualityCriteriaClassByIds;
    }

    protected Map<String, RefQualityCriteria> getRefQualityCriteriaByIds(List<HarvestingActionValorisationDto> valorisationDtos) {

        Set<String> qualityCriteriaRefIds = valorisationDtos.stream().filter(valorisationDto -> valorisationDto.getQualityCriteriaDtos().isPresent())
                .map(valorisationDto -> valorisationDto.getQualityCriteriaDtos().get())
                .flatMap(qcs -> qcs.stream().map(QualityCriteriaDto::getRefQualityCriteriaId)).collect(Collectors.toSet());
        Collection<RefQualityCriteria> refQualityCriterias = CollectionUtils.isNotEmpty(qualityCriteriaRefIds) ? referentialService.loadRefQualityCriteriaForIds(qualityCriteriaRefIds) : new ArrayList<>();
        Map<String, RefQualityCriteria> refQualityCriteriaByIds = Maps.uniqueIndex(refQualityCriterias, TopiaEntity::getTopiaId);

        return refQualityCriteriaByIds;
    }

    protected void shallowBindValorisation(HarvestingActionValorisationDto valorisationDto, HarvestingActionValorisation valorisationEntity, RefDestination destination) {
        valorisationEntity.setDestination(destination);
        valorisationEntity.setSpeciesCode(valorisationDto.getSpeciesCode());
        valorisationEntity.setSalesPercent(valorisationDto.getSalesPercent());
        valorisationEntity.setSelfConsumedPersent(valorisationDto.getSelfConsumedPersent());
        valorisationEntity.setNoValorisationPercent(valorisationDto.getNoValorisationPercent());
        valorisationEntity.setYealdMin(valorisationDto.getYealdMin());
        valorisationEntity.setYealdMax(valorisationDto.getYealdMax());
        valorisationEntity.setYealdAverage(valorisationDto.getYealdAverage());
        valorisationEntity.setYealdMedian(valorisationDto.getYealdMedian());
        valorisationEntity.setBeginMarketingPeriod(valorisationDto.getBeginMarketingPeriod());
        valorisationEntity.setBeginMarketingPeriodDecade(valorisationDto.getBeginMarketingPeriodDecade());
        valorisationEntity.setBeginMarketingPeriodCampaign(valorisationDto.getBeginMarketingPeriodCampaign());
        valorisationEntity.setEndingMarketingPeriod(valorisationDto.getEndingMarketingPeriod());
        valorisationEntity.setEndingMarketingPeriodDecade(valorisationDto.getEndingMarketingPeriodDecade());
        valorisationEntity.setEndingMarketingPeriodCampaign(valorisationDto.getEndingMarketingPeriodCampaign());
        valorisationEntity.setIsOrganicCrop(valorisationDto.isOrganicCrop());
        valorisationEntity.setYealdUnit(valorisationDto.getYealdUnit());
    }

    protected void bindQualityCriterias(
            Map<String, RefQualityCriteria> refQualityCriteriaByIds,
            Map<String, RefQualityCriteriaClass> refQualityCriteriaClassByIds,
            HarvestingActionValorisationDto valorisationDto,
            HarvestingActionValorisation valorisationEntity) {

        Map<String, QualityCriteria> qualityCriteriaByIds = CollectionUtils.emptyIfNull(valorisationEntity.getQualityCriteria())
                .stream().collect(Collectors.toMap((TopiaEntity::getTopiaId), Function.identity()));

        if (valorisationDto.getQualityCriteriaDtos().isPresent()) {
            Collection<QualityCriteriaDto> qualityCriteriaDtos = valorisationDto.getQualityCriteriaDtos().get();
            qualityCriteriaDtos.forEach(qualityCriteriaDto -> {
                QualityCriteria qCritEntity;
                if (qualityCriteriaDto.getTopiaId().isPresent() && valorisationEntity.isPersisted()) {
                    qCritEntity = qualityCriteriaByIds.remove(qualityCriteriaDto.getTopiaId().get());
                    Preconditions.checkNotNull(qCritEntity, String.format("Expected QualityCriteria with id '%s' not found", qualityCriteriaDto.getTopiaId().get()));
                } else {
                    qCritEntity = qualityCriteriaDao.newInstance();
                    valorisationEntity.addQualityCriteria(qCritEntity);
                }

                qCritEntity.setBinaryValue(qualityCriteriaDto.getBinaryValue());
                qCritEntity.setQuantitativeValue(qualityCriteriaDto.getQuantitativeValue());

                RefQualityCriteria refQualityCriteria = refQualityCriteriaByIds.get(qualityCriteriaDto.getRefQualityCriteriaId());
                Preconditions.checkNotNull(refQualityCriteria, String.format("Expected RefQualityCriteria with id '%s' not found", qualityCriteriaDto.getRefQualityCriteriaId()));
                qCritEntity.setRefQualityCriteria(refQualityCriteria);

                if (qualityCriteriaDto.getRefQualityCriteriaClassId().isPresent()) {
                    RefQualityCriteriaClass refQualityCriteriaClass = refQualityCriteriaClassByIds.get(qualityCriteriaDto.getRefQualityCriteriaClassId().get());
                    Preconditions.checkNotNull(refQualityCriteriaClass, String.format("Expected RefQualityCriteriaClass with id '%s' not found", qualityCriteriaDto.getRefQualityCriteriaClassId().get()));
                    qCritEntity.setRefQualityCriteriaClass(refQualityCriteriaClass);
                }
            });

        }
        qualityCriteriaByIds.values().forEach(valorisationEntity::removeQualityCriteria);
    }
}
