package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefConversionUnitesQSA;
import fr.inra.agrosyst.api.entities.referential.RefConversionUnitesQSAImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefConversionUnitesQSAModel extends AbstractAgrosystModel<RefConversionUnitesQSA> implements ExportModel<RefConversionUnitesQSA> {

    public RefConversionUnitesQSAModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("unite_sa", RefConversionUnitesQSA.PROPERTY_UNITE_SA);
        newMandatoryColumn("unite_saisie", RefConversionUnitesQSA.PROPERTY_UNITE_SAISIE, PHYTO_PRODUCT_UNIT_PARSER);
        newMandatoryColumn("qsa_kg_ha", RefConversionUnitesQSA.PROPERTY_QSA_KG_HA, DOUBLE_PARSER);
        newMandatoryColumn("remarque", RefConversionUnitesQSA.PROPERTY_REMARQUE);
        newMandatoryColumn("active", RefConversionUnitesQSA.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefConversionUnitesQSA, Object>> getColumnsForExport() {
        ModelBuilder<RefConversionUnitesQSA> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("unite_sa", RefConversionUnitesQSA.PROPERTY_UNITE_SA);
        modelBuilder.newColumnForExport("unite_saisie", RefConversionUnitesQSA.PROPERTY_UNITE_SAISIE, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("qsa_kg_ha", RefConversionUnitesQSA.PROPERTY_QSA_KG_HA, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("remarque", RefConversionUnitesQSA.PROPERTY_REMARQUE);
        modelBuilder.newColumnForExport("active", RefConversionUnitesQSA.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefConversionUnitesQSA newEmptyInstance() {
        RefConversionUnitesQSA ref = new RefConversionUnitesQSAImpl();
        ref.setActive(true);
        return ref;
    }
}
