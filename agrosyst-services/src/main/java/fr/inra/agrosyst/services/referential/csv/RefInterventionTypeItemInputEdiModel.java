package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefInterventionTypeItemInputEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionTypeItemInputEDIImpl;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDI;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefInterventionTypeItemInputEdiModel extends AbstractAgrosystModel<RefInterventionTypeItemInputEDI> implements ExportModel<RefInterventionTypeItemInputEDI> {

    public RefInterventionTypeItemInputEdiModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("intervention_type", RefInterventionTypeItemInputEDI.PROPERTY_INTERVENTION_TYPE, AGROSYST_INTERVENTION_TYPE_PARSER);
        newMandatoryColumn("input_edi_type_code", RefInterventionTypeItemInputEDI.PROPERTY_INPUT_EDI_TYPE_CODE);
        newMandatoryColumn("input_edi_label", RefInterventionTypeItemInputEDI.PROPERTY_INPUT_EDI_LABEL);
        newOptionalColumn(COLUMN_ACTIVE, RefInterventionTypeItemInputEDI.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public RefInterventionTypeItemInputEDI newEmptyInstance() {
        RefInterventionTypeItemInputEDI result = new RefInterventionTypeItemInputEDIImpl();
        result.setActive(true);
        return result;
    }

    @Override
    public Iterable<ExportableColumn<RefInterventionTypeItemInputEDI, Object>> getColumnsForExport() {
        ModelBuilder<RefOrientationEDI> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("intervention_type", RefInterventionTypeItemInputEDI.PROPERTY_INTERVENTION_TYPE, AGROSYST_INTERVENTION_TYPE_FORMATTER);
        modelBuilder.newColumnForExport("input_edi_type_code", RefInterventionTypeItemInputEDI.PROPERTY_INPUT_EDI_TYPE_CODE);
        modelBuilder.newColumnForExport("input_edi_label", RefInterventionTypeItemInputEDI.PROPERTY_INPUT_EDI_LABEL);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefOrientationEDI.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
}
