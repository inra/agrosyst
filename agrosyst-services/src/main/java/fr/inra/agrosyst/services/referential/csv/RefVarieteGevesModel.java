package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefVarieteGeves;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGevesImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefVarieteGevesModel extends AbstractAgrosystModel<RefVarieteGeves> implements ExportModel<RefVarieteGeves> {

    public RefVarieteGevesModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("Groupe", RefVarieteGeves.PROPERTY_GROUPE, INT_PARSER);
        newMandatoryColumn("Nom Groupe", RefVarieteGeves.PROPERTY_NOM__GROUPE);
        newMandatoryColumn("Code Section", RefVarieteGeves.PROPERTY_CODE__SECTION, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("Nom Section", RefVarieteGeves.PROPERTY_NOM__SECTION);
        newMandatoryColumn("N° Espèce Botanique", RefVarieteGeves.PROPERTY_NUM__ESPECE__BOTANIQUE, INT_PARSER);
        newMandatoryColumn("N° Espèce CTP", RefVarieteGeves.PROPERTY_NUM__ESPECE__CTP, INT_PARSER);
        newMandatoryColumn("N° Espèce DHS", RefVarieteGeves.PROPERTY_NUM__ESPECE__DHS, INT_PARSER);
        newMandatoryColumn("Nom Botanique", RefVarieteGeves.PROPERTY_NOM__BOTANIQUE);
        newMandatoryColumn("Nom Français", RefVarieteGeves.PROPERTY_NOM__FRANCAIS);
        newMandatoryColumn("Dénomination", RefVarieteGeves.PROPERTY_DENOMINATION);
        newMandatoryColumn("Référence Provisoire", RefVarieteGeves.PROPERTY_REFERENCE__PROVISOIRE);
        newMandatoryColumn("N° Dossier", RefVarieteGeves.PROPERTY_NUM__DOSSIER, INT_PARSER);
        newMandatoryColumn("Liste", RefVarieteGeves.PROPERTY_LISTE);
        newMandatoryColumn("Rubrique", RefVarieteGeves.PROPERTY_RUBRIQUE);
        newMandatoryColumn("Libellé Rubrique", RefVarieteGeves.PROPERTY_LIBELLE__RUBRIQUE);
        newMandatoryColumn("Type Variétal", RefVarieteGeves.PROPERTY_TYPE__VARIETAL);
        newMandatoryColumn("Libellé Type Variétal", RefVarieteGeves.PROPERTY_LIBELLE__TYPE__VARIETAL);
        newMandatoryColumn("Ploidie", RefVarieteGeves.PROPERTY_PLOIDIE);
        newMandatoryColumn("Libellé Ploidie", RefVarieteGeves.PROPERTY_LIBELLE__PLOIDIE);
        newMandatoryColumn("Zone de Précocité", RefVarieteGeves.PROPERTY_ZONE_DE__PRECOCITE);
        newMandatoryColumn("Libellé Zone", RefVarieteGeves.PROPERTY_LIBELLE__ZONE);
        newMandatoryColumn("1ère inscription", RefVarieteGeves.PROPERTY_PREMIERE_INSCRIPTION, INT_PARSER);
        newMandatoryColumn("Dernière Réinscription", RefVarieteGeves.PROPERTY_DERNIERE__REINSCRIPTION, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("Radiation", RefVarieteGeves.PROPERTY_RADIATION, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("Commercialisable jusqu'au", RefVarieteGeves.PROPERTY_COMMERCIALISABLE_JUSQU_AU, LOCAL_DATE_PARSER);
        newMandatoryColumn("Information Complémentaire", RefVarieteGeves.PROPERTY_INFORMATION__COMPLEMENTAIRE);
        newMandatoryColumn("N° Cultivar", RefVarieteGeves.PROPERTY_NUM__CULTIVAR, INT_PARSER);
        newMandatoryColumn("Code GNIS espèce", RefVarieteGeves.PROPERTY_CODE_GNIS_ESPECE);
        newMandatoryColumn("Code GNIS variété", RefVarieteGeves.PROPERTY_CODE_GNIS_VARIETE);
        newMandatoryColumn("Source", RefVarieteGeves.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefVarieteGeves.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefVarieteGeves, Object>> getColumnsForExport() {
        ModelBuilder<RefVarieteGeves> modelBuilder = new ModelBuilder<>();
        
        modelBuilder.newColumnForExport("Groupe", RefVarieteGeves.PROPERTY_GROUPE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Nom Groupe", RefVarieteGeves.PROPERTY_NOM__GROUPE);
        modelBuilder.newColumnForExport("Code Section", RefVarieteGeves.PROPERTY_CODE__SECTION, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Nom Section", RefVarieteGeves.PROPERTY_NOM__SECTION);
        modelBuilder.newColumnForExport("N° Espèce Botanique", RefVarieteGeves.PROPERTY_NUM__ESPECE__BOTANIQUE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("N° Espèce CTP", RefVarieteGeves.PROPERTY_NUM__ESPECE__CTP, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("N° Espèce DHS", RefVarieteGeves.PROPERTY_NUM__ESPECE__DHS, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Nom Botanique", RefVarieteGeves.PROPERTY_NOM__BOTANIQUE);
        modelBuilder.newColumnForExport("Nom Français", RefVarieteGeves.PROPERTY_NOM__FRANCAIS);
        modelBuilder.newColumnForExport("Dénomination", RefVarieteGeves.PROPERTY_DENOMINATION);
        modelBuilder.newColumnForExport("Référence Provisoire", RefVarieteGeves.PROPERTY_REFERENCE__PROVISOIRE);
        modelBuilder.newColumnForExport("N° Dossier", RefVarieteGeves.PROPERTY_NUM__DOSSIER, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Liste", RefVarieteGeves.PROPERTY_LISTE);
        modelBuilder.newColumnForExport("Rubrique", RefVarieteGeves.PROPERTY_RUBRIQUE);
        modelBuilder.newColumnForExport("Libellé Rubrique", RefVarieteGeves.PROPERTY_LIBELLE__RUBRIQUE);
        modelBuilder.newColumnForExport("Type Variétal", RefVarieteGeves.PROPERTY_TYPE__VARIETAL);
        modelBuilder.newColumnForExport("Libellé Type Variétal", RefVarieteGeves.PROPERTY_LIBELLE__TYPE__VARIETAL);
        modelBuilder.newColumnForExport("Ploidie", RefVarieteGeves.PROPERTY_PLOIDIE);
        modelBuilder.newColumnForExport("Libellé Ploidie", RefVarieteGeves.PROPERTY_LIBELLE__PLOIDIE);
        modelBuilder.newColumnForExport("Zone de Précocité", RefVarieteGeves.PROPERTY_ZONE_DE__PRECOCITE);
        modelBuilder.newColumnForExport("Libellé Zone", RefVarieteGeves.PROPERTY_LIBELLE__ZONE);
        modelBuilder.newColumnForExport("1ère inscription", RefVarieteGeves.PROPERTY_PREMIERE_INSCRIPTION, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Dernière Réinscription", RefVarieteGeves.PROPERTY_DERNIERE__REINSCRIPTION, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Radiation", RefVarieteGeves.PROPERTY_RADIATION, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Commercialisable jusqu'au", RefVarieteGeves.PROPERTY_COMMERCIALISABLE_JUSQU_AU, LOCAL_DATE_FORMATTER);
        modelBuilder.newColumnForExport("Information Complémentaire", RefVarieteGeves.PROPERTY_INFORMATION__COMPLEMENTAIRE);
        modelBuilder.newColumnForExport("N° Cultivar", RefVarieteGeves.PROPERTY_NUM__CULTIVAR, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Code GNIS espèce", RefVarieteGeves.PROPERTY_CODE_GNIS_ESPECE);
        modelBuilder.newColumnForExport("Code GNIS variété", RefVarieteGeves.PROPERTY_CODE_GNIS_VARIETE);
        modelBuilder.newColumnForExport("Source", RefVarieteGeves.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefVarieteGeves.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefVarieteGeves newEmptyInstance() {
        RefVarieteGeves result = new RefVarieteGevesImpl();
        result.setActive(true);
        return result;
    }

}
