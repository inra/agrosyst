package fr.inra.agrosyst.services.security;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.entities.AttachmentMetadata;
import fr.inra.agrosyst.api.entities.AttachmentMetadataTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.NetworkManager;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PlotTopiaDao;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRule;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRuleTopiaDao;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeTopiaDao;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlotTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.report.ReportRegional;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.entities.security.PermissionObjectType;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.entities.security.UserRole;
import fr.inra.agrosyst.api.services.security.AgrosystAccessDeniedException;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.services.common.CacheDiscriminator;
import lombok.Setter;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaIdFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Predicate;
import java.util.stream.StreamSupport;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Setter
public class BusinessAuthorizationServiceImpl extends AuthorizationServiceImpl implements BusinessAuthorizationService {

    private static final Log log = LogFactory.getLog(BusinessAuthorizationServiceImpl.class);

    protected static final Predicate<NetworkManager> IS_ACTIVE_NETWORK_MANAGER = input -> {
        // TODO AThimel 22/10/13 Manage case where toDate is in the future
        return input.isActive() && input.getToDate() == null;
    };

    protected PracticedSystemTopiaDao practicedSystemDao;
    protected ManagementModeTopiaDao managementModeDao;
    protected DecisionRuleTopiaDao decisionRuleDao;
    protected ZoneTopiaDao zoneDao;
    protected AttachmentMetadataTopiaDao attachmentMetadataDao;
    protected PerformanceTopiaDao performanceDao;
    protected PlotTopiaDao plotDao;
    protected PracticedPlotTopiaDao practicedPlotDao;
    protected ReportGrowingSystemTopiaDao reportGrowingSystemDao;

    protected boolean hasObjectPermissionAdmin(String userId, PermissionObjectType type, String object) {
        return hasPermissionActionLevel(userId, type, object, SecurityHelper.PERMISSION_ADMIN);
    }

    protected boolean hasObjectPermissionWritable(String userId, PermissionObjectType type, String object) {
        return hasPermissionActionLevel(userId, type, object, SecurityHelper.PERMISSION_WRITE);
    }

    protected boolean hasObjectPermissionWritable(String userId, String object) {
        return hasPermissionActionLevel(userId, object, SecurityHelper.PERMISSION_WRITE);
    }

    protected boolean hasObjectPermissionReadable(String userId, PermissionObjectType type, String object) {
        return hasPermissionActionLevel(userId, type, object, SecurityHelper.PERMISSION_READ_VALIDATED);
    }

    protected boolean hasObjectPermissionReadable(String userId, String object) {
        return hasPermissionActionLevel(userId, object, SecurityHelper.PERMISSION_READ_VALIDATED);
    }

    protected boolean hasPermissionActionLevel(String userId, String object, int expectedActionLevel) {
        int maxActionLevel = getHighestPermissionLevel(userId, null, object);
        return maxActionLevel >= expectedActionLevel;
    }

    protected boolean hasPermissionActionLevel(String userId, PermissionObjectType type, String object, int expectedActionLevel) {
        int maxActionLevel = getHighestPermissionLevel(userId, type, object);
        return maxActionLevel >= expectedActionLevel;
    }

    protected int getHighestPermissionLevel(String userId, PermissionObjectType type, String object) {
        return computedUserPermissionDao.getMaxAction(userId, object, type);
    }

    public boolean isDomainWritable(String domainId) {

        String userId = getUserIdAndComputePermissions();

        boolean result = isAdmin() ||
                hasObjectPermissionWritable(userId, PermissionObjectType.DOMAIN_ID, domainId);

        if (!result) {
            Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
            result = hasObjectPermissionWritable(userId, PermissionObjectType.DOMAIN_CODE, domain.getCode());
        }

        return result;
    }

    @Override
    public boolean areDomainPlotsEditable(String domainId) {
        boolean result;
        if (isDomainWritable(domainId)) {
            result = true;
        } else {
            Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
            // XXX AThimel 12/03/14 En fait il faudrait regarder sur tous les dispositifs à partir du code de dommaine
            Iterable<GrowingPlan> growingPlans = growingPlanDao.forDomainEquals(domain).findAllLazy();
            result = StreamSupport.stream(growingPlans.spliterator(), false).anyMatch(input -> isGrowingPlanAdministrable(input.getCode()));
        }
        return result;
    }

    @Override
    public boolean isDomainAdministrable(String domainCode) {

        String userId = getUserIdAndComputePermissions();

        return isAdmin() ||
                hasObjectPermissionAdmin(userId, PermissionObjectType.DOMAIN_CODE, domainCode);
    }

    protected boolean isDomainCodeWritable(String domainCode) {

        String userId = getUserIdAndComputePermissions();

        // XXX AThimel 18/02/14 Maybe iterate over domains for a given code and test each ID ?

        return isAdmin() ||
                hasObjectPermissionWritable(userId, PermissionObjectType.DOMAIN_CODE, domainCode);
    }

    public boolean isGrowingPlanWritable(String growingPlanId) {

        String userId = getUserIdAndComputePermissions();

        boolean result = isAdmin() ||
                hasObjectPermissionWritable(userId, PermissionObjectType.GROWING_PLAN_ID, growingPlanId);

        if (!result) {
            GrowingPlan growingPlan = growingPlanDao.forTopiaIdEquals(growingPlanId).findUnique();
            result = hasObjectPermissionWritable(userId, PermissionObjectType.GROWING_PLAN_CODE, growingPlan.getCode());
        }

        return result;
    }

    @Override
    public boolean isGrowingPlanAdministrable(String growingPlanCode) {

        String userId = getUserIdAndComputePermissions();

        return isAdmin() ||
                hasObjectPermissionAdmin(userId, PermissionObjectType.GROWING_PLAN_CODE, growingPlanCode);
    }

    public boolean isGrowingSystemWritable(String growingSystemId) {

        String userId = getUserIdAndComputePermissions();

        boolean result = isAdmin() ||
                hasObjectPermissionWritable(userId, PermissionObjectType.GROWING_SYSTEM_ID, growingSystemId);

        if (!result) {
            GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(growingSystemId).findUnique();
            result = hasObjectPermissionWritable(userId, PermissionObjectType.GROWING_SYSTEM_CODE, growingSystem.getCode());
        }

        return result;
    }

    @Override
    public boolean isGrowingSystemAdministrable(String growingSystemCode) {

        String userId = getUserIdAndComputePermissions();

        return isAdmin() ||
                hasObjectPermissionAdmin(userId, PermissionObjectType.GROWING_SYSTEM_CODE, growingSystemCode);
    }

    public boolean isPracticedSystemWritable(String practicedSystemId) {
        PracticedSystem practicedSystem = practicedSystemDao.forTopiaIdEquals(practicedSystemId).findUnique();
        String growingSystemId = practicedSystem.getGrowingSystem().getTopiaId(); // TODO AThimel 21/10/13 Optimize it
        return isGrowingSystemWritable(growingSystemId);
    }

    @Override
    public boolean isNetworkWritable(String networkId) {

        String userId = getUserIdAndComputePermissions();

        return isAdmin() ||
                hasObjectPermissionWritable(userId, PermissionObjectType.NETWORK_ID, networkId);
    }

    @Override
    public boolean isManagementModeWritable(String managementModeId) {
        ManagementMode managementMode = managementModeDao.forTopiaIdEquals(managementModeId).findUnique();
        String growingSystemId = managementMode.getGrowingSystem().getTopiaId(); // TODO AThimel 21/10/13 Optimize it
        return isGrowingSystemWritable(growingSystemId);
    }

    @Override
    public boolean isManagementModeReadable(String managementModeId) {
        ManagementMode managementMode = managementModeDao.forTopiaIdEquals(managementModeId).findUnique();
        String growingSystemId = managementMode.getGrowingSystem().getTopiaId(); // TODO AThimel 21/10/13 Optimize it
        return isGrowingSystemReadable(growingSystemId);
    }

    @Override
    // https://forge.codelutin.com/issues/4439
    public boolean isDecisionRuleWritable(String decisionRuleId) {
        DecisionRule decisionRule = decisionRuleDao.forTopiaIdEquals(decisionRuleId).findUnique();
        boolean result = isDomainCodeWritable(decisionRule.getDomainCode());
        if (!result) {
            List<Domain> domains = domainDao.forCodeEquals(decisionRule.getDomainCode()).findAll();
            List<GrowingPlan> growingPlans = growingPlanDao.forDomainIn(domains).findAll();
            for (GrowingPlan growingPlan : growingPlans) {
                String growingPlanId = growingPlan.getTopiaId();
                if (isGrowingPlanWritable(growingPlanId)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    @Override
    // https://forge.codelutin.com/issues/4439
    public boolean isDecisionRuleReadable(String decisionRuleId) {
        DecisionRule decisionRule = decisionRuleDao.forTopiaIdEquals(decisionRuleId).findUnique();
        boolean result = isDomainCodeReadable(decisionRule.getDomainCode());
        if (!result) {
            List<Domain> domains = domainDao.forCodeEquals(decisionRule.getDomainCode()).findAll();
            List<GrowingPlan> growingPlans = growingPlanDao.forDomainIn(domains).findAll();
            for (GrowingPlan growingPlan : growingPlans) {
                String growingPlanId = growingPlan.getTopiaId();
                if (isGrowingPlanReadable(growingPlanId)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    @Override
    public boolean isZoneWritable(String zoneId) {
        Zone zone = zoneDao.forTopiaIdEquals(zoneId).findUnique();

        boolean result;
        GrowingSystem growingSystem = zone.getPlot().getGrowingSystem();
        if (growingSystem == null) {
            String domainId = zone.getPlot().getDomain().getTopiaId();
            result = isDomainWritable(domainId);
        } else {
            result = isGrowingSystemWritable(growingSystem.getTopiaId());
        }
        return result;
    }

    @Override
    public void domainCreated(Domain domain) {

        String userId = getUserIdAndComputePermissions();

        AgrosystUser user = agrosystUserDao.forTopiaIdEquals(userId).findUnique();

        userRoleDao.create(
                UserRole.PROPERTY_AGROSYST_USER, user,
                UserRole.PROPERTY_TYPE, RoleType.DOMAIN_RESPONSIBLE,
                UserRole.PROPERTY_DOMAIN_CODE, domain.getCode()
        );

        dropComputedPermissions0(userId);

        clearSingleCacheAndSync(CacheDiscriminator.hasRole());
    }
    
    @Override
    public void edaplosDomainCreated(Domain domain) {
        
        String userId = getUserId();
        
        AgrosystUser user = agrosystUserDao.forTopiaIdEquals(userId).findUnique();
        
        userRoleDao.create(
                UserRole.PROPERTY_AGROSYST_USER, user,
                UserRole.PROPERTY_TYPE, RoleType.DOMAIN_RESPONSIBLE,
                UserRole.PROPERTY_DOMAIN_CODE, domain.getCode()
        );
        
    }

    @Override
    public void growingPlanCreated(GrowingPlan growingPlan) {
        String userId = getUserIdAndComputePermissions();

        AgrosystUser user = agrosystUserDao.forTopiaIdEquals(userId).findUnique();

        userRoleDao.create(
                UserRole.PROPERTY_AGROSYST_USER, user,
                UserRole.PROPERTY_TYPE, RoleType.GROWING_PLAN_RESPONSIBLE,
                UserRole.PROPERTY_GROWING_PLAN_CODE, growingPlan.getCode()
        );

        dropComputedPermissions0(userId);
        Domain domain = growingPlan.getDomain();
        domainIsDirty(domain);

        clearSingleCacheAndSync(CacheDiscriminator.hasRole());
    }

    @Override
    public void growingSystemCreated(GrowingSystem growingSystem) {
        GrowingPlan growingPlan = growingSystem.getGrowingPlan();
        growingPlanIsDirty(growingPlan);

        clearCacheAndSync(); // for network projections
    }

    @Override
    public void networkCreated(Network network) {
        String userId = getUserIdAndComputePermissions();

        resetNetworkManagers(network);

        dropComputedPermissions0(userId);

        clearSingleCacheAndSync(CacheDiscriminator.hasRole());
    }

    @Override
    public void networkUpdated(Network network) {
        resetNetworkManagers(network);
        // TODO AThimel 08/10/13 This is a bit aggressive, find a better way
        objectsAreDirty(PermissionObjectType.NETWORK_ID);
    }

    protected void resetNetworkManagers(Network network) {
        // TODO kmorin demander si on reset les writers
        List<UserRole> existingRoles = Lists.newArrayList(userRoleDao.forProperties(
                UserRole.PROPERTY_TYPE, RoleType.NETWORK_RESPONSIBLE,
                UserRole.PROPERTY_NETWORK_ID, network.getTopiaId()).findAll());
        userRoleDao.deleteAll(existingRoles);

        Iterable<NetworkManager> activeNetworkManagers = Iterables.filter(network.getManagers(), IS_ACTIVE_NETWORK_MANAGER::test);
        List<UserRole> newRoles = new ArrayList<>();
        for (NetworkManager manager : activeNetworkManagers) {

            UserRole newRole = userRoleDao.create(
                    UserRole.PROPERTY_AGROSYST_USER, manager.getAgrosystUser(),
                    UserRole.PROPERTY_TYPE, RoleType.NETWORK_RESPONSIBLE,
                    UserRole.PROPERTY_NETWORK_ID, network.getTopiaId()
            );

            Iterator<UserRole> existingRolesIt = existingRoles.iterator();
            boolean found = false;
            while (existingRolesIt.hasNext() && !found) {
                UserRole existingRole = existingRolesIt.next();
                if (existingRole.getAgrosystUser().equals(newRole.getAgrosystUser())) {
                    existingRolesIt.remove();
                    found = true;
                }
            }
            if (!found) {
                newRoles.add(newRole);
            }
        }

        for (UserRole existingRole : existingRoles) {
            trackerService.roleRemoved(existingRole);
        }
        for (UserRole newRole : newRoles) {
            trackerService.roleAdded(newRole);
        }
    }

    protected void growingPlanIsDirty(GrowingPlan growingPlan) {
        objectIsDirty(PermissionObjectType.GROWING_PLAN_ID, growingPlan.getTopiaId());
        objectIsDirty(PermissionObjectType.GROWING_PLAN_CODE, growingPlan.getCode());

        domainIsDirty(growingPlan.getDomain());
    }

    protected void domainIsDirty(Domain domain) {
        objectIsDirty(PermissionObjectType.DOMAIN_ID, domain.getTopiaId());
        objectIsDirty(PermissionObjectType.DOMAIN_CODE, domain.getCode());
    }

    @Override
    public void checkIsAdmin() throws AgrosystAccessDeniedException {
        if (!isAdmin()) {
            // TODO AThimel 07/10/13 Give some information to the created exception
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits d'admin");
        }
    }

    @Override
    public void checkDomainReadable(String domainId) throws AgrosystAccessDeniedException {
        boolean result = isDomainReadable(domainId);

        if (!result) {
            // TODO AThimel 07/10/13 Give some information to the created exception
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits " + domainId);
        }
    }

    protected boolean isDomainReadable(String domainId) {
        String userId = getUserIdAndComputePermissions();

        boolean result = isAdmin() ||
                hasObjectPermissionReadable(userId, PermissionObjectType.DOMAIN_ID, domainId);

        if (!result) {
            Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
            result = hasObjectPermissionReadable(userId, PermissionObjectType.DOMAIN_CODE, domain.getCode());
        }
        return result;
    }

    protected boolean isDomainCodeReadable(String domainCode) {
        String userId = getUserIdAndComputePermissions();

        return isAdmin() ||
                hasObjectPermissionReadable(userId, PermissionObjectType.DOMAIN_CODE, domainCode);
    }

    @Override
    public boolean shouldAnonymizeDomain(String domainId, boolean allowUnreadable) {
        boolean result;
        try {
            result = getShouldAnonymizeCanReadDomain(domainId, allowUnreadable).getLeft();
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le domaine " + domainId);
        }
        return result;
    }

    @Override
    public Pair<Boolean, Boolean> getShouldAnonymizeCanReadDomain(String domainId, boolean allowUnreadable) {
        // TODO AThimel 26/02/14 Introduce cache
        boolean canRead = true;
        boolean anonymized = false;

        if (!isAdmin()) {
            String userId = getUserIdAndComputePermissions();

            int highestPermissionLevel = getHighestPermissionLevel(userId, PermissionObjectType.DOMAIN_ID, domainId);
            if (highestPermissionLevel == 0) {
                Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
                highestPermissionLevel = getHighestPermissionLevel(userId, PermissionObjectType.DOMAIN_CODE, domain.getCode());
            }

            Preconditions.checkState(allowUnreadable || highestPermissionLevel > 0);
            anonymized = highestPermissionLevel < SecurityHelper.PERMISSION_READ_RAW;
            canRead = highestPermissionLevel >= SecurityHelper.PERMISSION_READ_VALIDATED;
        }
        return Pair.of(anonymized, canRead);
    }

    @Override
    public boolean shouldAnonymizeGrowingPlan(String growingPlanId, boolean allowUnreadable) {
        boolean result;
        try {
            result = getShouldAnonymizeCanReadGrowingPlan(growingPlanId, allowUnreadable).getLeft();
        } catch (IllegalStateException ise) {
            if (log.isWarnEnabled()) {
                log.warn("Erreur lors du contrôle d'anonymisation", ise);
            }
            throw new AgrosystAccessDeniedException(ise);
        }
        return result;
    }

    public  Pair<Boolean, Boolean> getShouldAnonymizeCanReadGrowingPlan(String growingPlanId, boolean allowUnreadable) {

        // TODO AThimel 26/02/14 Introduce cache
        boolean canRead = true;
        boolean result = false;

        if (!isAdmin()) {
            String userId = getUserIdAndComputePermissions();

            int highestPermissionLevel = getHighestPermissionLevel(userId, PermissionObjectType.GROWING_PLAN_ID, growingPlanId);
            if (highestPermissionLevel == 0) {
                GrowingPlan growingPlan = growingPlanDao.forTopiaIdEquals(growingPlanId).findUnique();
                highestPermissionLevel = getHighestPermissionLevel(userId, PermissionObjectType.GROWING_PLAN_CODE, growingPlan.getCode());
            }

            Preconditions.checkState(
                    allowUnreadable || highestPermissionLevel > 0,
                    String.format("Les dispositifs non lisibles ne sont pas autorisés et l'utilisateur %s n'a pas les droits minimum sur le dispositif %s", userId, growingPlanId)
            );
            result = highestPermissionLevel < SecurityHelper.PERMISSION_READ_RAW;
            canRead = highestPermissionLevel >= SecurityHelper.PERMISSION_READ_VALIDATED;
        }
        return Pair.of(result, canRead);
    }

    @Override
    public void checkGrowingPlanReadable(String growingPlanId) throws AgrosystAccessDeniedException {

        boolean result = isGrowingPlanReadable(growingPlanId);

        if (!result) {
            // TODO AThimel 07/10/13 Give some information to the created exception
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le dispositif " + growingPlanId);
        }
    }

    protected boolean isGrowingPlanReadable(String growingPlanId) {
        String userId = getUserIdAndComputePermissions();

        boolean result = isAdmin() ||
                hasObjectPermissionReadable(userId, PermissionObjectType.GROWING_PLAN_ID, growingPlanId);

        if (!result) {
            GrowingPlan growingPlan = growingPlanDao.forTopiaIdEquals(growingPlanId).findUnique();
            result = hasObjectPermissionReadable(userId, PermissionObjectType.GROWING_PLAN_CODE, growingPlan.getCode());
        }
        return result;
    }

    @Override
    public boolean isGrowingSystemReadable(String growingSystemId) {
        String userId = getUserIdAndComputePermissions();

        boolean result = isAdmin() ||
                hasObjectPermissionReadable(userId, PermissionObjectType.GROWING_SYSTEM_ID, growingSystemId);

        if (!result) {
            GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(growingSystemId).findUnique();
            result = hasObjectPermissionReadable(userId, PermissionObjectType.GROWING_SYSTEM_CODE, growingSystem.getCode());
        }

        return result;
    }

    @Override
    public void checkGrowingSystemReadable(String growingSystemId) throws AgrosystAccessDeniedException {
        if (!isGrowingSystemReadable(growingSystemId)) {
            // TODO AThimel 07/10/13 Give some information to the created exception
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le SDC " + growingSystemId);
        }
    }

    @Override
    public void checkPracticedSystemReadable(String practicedSystemId) throws AgrosystAccessDeniedException {
        PracticedSystem practicedSystem = practicedSystemDao.forTopiaIdEquals(practicedSystemId).findUnique();
        String growingSystemId = practicedSystem.getGrowingSystem().getTopiaId(); // TODO AThimel 21/10/13 Optimize it
        checkGrowingSystemReadable(growingSystemId);
    }

    @Override
    public void checkDecisionRuleReadable(String decisionRuleId) throws AgrosystAccessDeniedException {
        DecisionRule decisionRule = decisionRuleDao.forTopiaIdEquals(decisionRuleId).findUnique();
        
        if (!isDomainCodeReadable(decisionRule.getDomainCode())) {
            // TODO AThimel 07/10/13 Give some information to the created exception
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur la règle de décision " + decisionRuleId);
        }
    }

    @Override
    public void checkManagementModeReadable(String managementModeId) throws AgrosystAccessDeniedException {
        ManagementMode managementMode = managementModeDao.forTopiaIdEquals(managementModeId).findUnique();
        String growingSystemId = managementMode.getGrowingSystem().getTopiaId(); // TODO AThimel 21/10/13 Optimize it
        checkGrowingSystemReadable(growingSystemId);
    }

    @Override
    public void checkEffectiveCropCyclesReadable(String zoneId) throws AgrosystAccessDeniedException {
        boolean result = isZoneReadable(zoneId);
        if (!result) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur la zone " + zoneId);
        }
    }

    protected boolean isZoneReadable(String zoneId) {
        Zone zone = zoneDao.forTopiaIdEquals(zoneId).findUnique();
        boolean result;
        if (zone.getPlot().getGrowingSystem() == null) {
            String domainId = zone.getPlot().getDomain().getTopiaId();
            result = isDomainReadable(domainId);
        } else {
            String growingSystemId = zone.getPlot().getGrowingSystem().getTopiaId();
            result = isGrowingSystemReadable(growingSystemId);
        }
        return result;
    }

    @Override
    public void checkCreateOrUpdateDomain(String domainId) throws AgrosystAccessDeniedException {
        if (Strings.isNullOrEmpty(domainId)) {
            // TODO AThimel 18/10/13 create authorisation check
        } else if (!isDomainWritable(domainId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le domaine " + domainId);
        }
    }

    @Override
    public void checkCreateOrUpdateGrowingPlan(String growingPlanId) throws AgrosystAccessDeniedException {
        if (Strings.isNullOrEmpty(growingPlanId)) {
            // TODO AThimel 18/10/13 create authorisation check
        } else if (!isGrowingPlanWritable(growingPlanId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le dispositif " + growingPlanId);
        }
    }

    @Override
    public void checkCreateOrUpdateGrowingSystem(String growingSystemId) throws AgrosystAccessDeniedException {
        if (Strings.isNullOrEmpty(growingSystemId)) {
            // TODO AThimel 18/10/13 create authorisation check
        } else if (!isGrowingSystemWritable(growingSystemId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le SDC " + growingSystemId);
        }
    }

    @Override
    public void checkCreateOrUpdateNetwork(String networkId) throws AgrosystAccessDeniedException {
        if (Strings.isNullOrEmpty(networkId)) {
            // TODO AThimel 18/10/13 create authorisation check
        } else if (!isNetworkWritable(networkId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le réseau " + networkId);
        }
    }

    @Override
    public void checkCreateOrUpdatePracticedSystem(String practicedSystemId) throws AgrosystAccessDeniedException {
        if (Strings.isNullOrEmpty(practicedSystemId)) {
            // TODO AThimel 21/10/13 create authorisation check
        } else if (!isPracticedSystemWritable(practicedSystemId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le synthétisé " + practicedSystemId);
        }
    }

    @Override
    public void checkCreateOrUpdateDecisionRule(String decisionRuleId) throws AgrosystAccessDeniedException {
        if (Strings.isNullOrEmpty(decisionRuleId)) {
            // TODO AThimel 21/10/13 create authorisation check
        } else if (!isDecisionRuleWritable(decisionRuleId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur la règle de décision " + decisionRuleId);
        }
    }

    @Override
    public void checkCreateOrUpdateManagementMode(String managementModeId) throws AgrosystAccessDeniedException {
        if (Strings.isNullOrEmpty(managementModeId)) {
            // TODO AThimel 21/10/13 create authorisation check
        } else if (!isManagementModeWritable(managementModeId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le mode de gestion " + managementModeId);
    }
    }

    @Override
    public void checkCreateOrUpdateEffectiveCropCycles(String zoneId) throws AgrosystAccessDeniedException {
        if (Strings.isNullOrEmpty(zoneId)) {
            // TODO AThimel 21/10/13 create authorisation check
        } else if (!isZoneWritable(zoneId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur la zone " + zoneId);
        }
    }

    @Override
    public boolean isDomainValidable(String domainId) {

        // TODO AThimel 29/11/13 Check validable not only writable
        return isDomainWritable(domainId);
//        String userId = getUserId();
//
//        boolean result = isAdmin() ||
//                hasObjectPermissionWritable(userId, PermissionObjectType.DOMAIN_ID, domainId);
//
//        if (!result) {
//            Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
//            result = hasObjectPermissionWritable(userId, PermissionObjectType.DOMAIN_CODE, domain.getCode());
//        }
//
//        return result;
    }

    @Override
    public void checkValidateDomain(String domainId) throws AgrosystAccessDeniedException {
        if (!isDomainValidable(domainId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le domaine " + domainId);
        }
    }

    @Override
    public boolean isGrowingPlanValidable(String growingPlanId) {

        // TODO AThimel 29/11/13 Check validable not only writable
        return isGrowingPlanWritable(growingPlanId);
//        String userId = getUserId();
//
//        boolean result = isAdmin() ||
//                hasObjectPermissionWritable(userId, PermissionObjectType.DOMAIN_ID, domainId);
//
//        if (!result) {
//            Domain domain = domainDao.forTopiaIdEquals(domainId).finfUnique;
//            result = hasObjectPermissionWritable(userId, PermissionObjectType.DOMAIN_CODE, domain.getCode());
//        }
//
//        return result;
    }

    @Override
    public void checkValidateGrowingPlan(String growingPlanId) throws AgrosystAccessDeniedException {
        if (!isGrowingPlanWritable(growingPlanId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le dispositif " + growingPlanId);
        }
    }

    @Override
    public boolean isGrowingSystemValidable(String growingSystemId) {

        // TODO AThimel 29/11/13 Check validable not only writable
        return isGrowingSystemWritable(growingSystemId);
//        String userId = getUserId();
//
//        boolean result = isAdmin() ||
//                hasObjectPermissionWritable(userId, PermissionObjectType.DOMAIN_ID, domainId);
//
//        if (!result) {
//            Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique;
//            result = hasObjectPermissionWritable(userId, PermissionObjectType.DOMAIN_CODE, domain.getCode());
//        }
//
//        return result;
    }

    @Override
    public void checkValidateGrowingSystem(String growingSystemId) throws AgrosystAccessDeniedException {
        if (!isGrowingSystemWritable(growingSystemId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le SDC " + growingSystemId);
        }
    }

    @Override
    public boolean isPracticedSystemValidable(String practicedSystemId) {
        PracticedSystem practicedSystem = practicedSystemDao.forTopiaIdEquals(practicedSystemId).findUnique();
        String growingSystemId = practicedSystem.getGrowingSystem().getTopiaId();
        return isGrowingSystemWritable(growingSystemId);
    }

    @Override
    public void checkValidatePracticedSystem(String practicedSystemId) throws AgrosystAccessDeniedException {
        if (!isPracticedSystemValidable(practicedSystemId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le synthétisé " + practicedSystemId);
        }
    }

    @Override
    public boolean areAttachmentsAddableOrDeletable(String objectReferenceId) {
        return canCreateOrDeleteAttachment(objectReferenceId);
    }

    @Override
    public void checkAddAttachment(String objectReferenceId) {
        if (!canCreateOrDeleteAttachment(objectReferenceId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur la pièce jointe " + objectReferenceId);
        }
    }

    @Override
    public void checkDeleteAttachment(String attachmentMetadataId) {
        AttachmentMetadata attachmentMetadata = attachmentMetadataDao.forTopiaIdEquals(attachmentMetadataId).findUnique();
        String objectReferenceId = attachmentMetadata.getObjectReferenceId();
        if (!canCreateOrDeleteAttachment(objectReferenceId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur la pièce jointe " + attachmentMetadataId);
        }
    }

    protected boolean canCreateOrDeleteAttachment(String referenceId) {

        String userId = getUserIdAndComputePermissions();

        boolean result = isAdmin() ||
                hasObjectPermissionWritable(userId, referenceId);

        TopiaIdFactory topiaIdFactory = context.getPersistenceContext().getTopiaIdFactory();
        if (!result && topiaIdFactory.isTopiaId(referenceId) && ManagementMode.class.equals(topiaIdFactory.getClassName(referenceId))) {
            result = isManagementModeWritable(referenceId);
        }

        if (!result && topiaIdFactory.isTopiaId(referenceId) && Performance.class.equals(topiaIdFactory.getClassName(referenceId))) {
            result = isPerformanceWritable(referenceId);
        }

        if (!result && topiaIdFactory.isTopiaId(referenceId) && Zone.class.equals(topiaIdFactory.getClassName(referenceId))) {
            result = isZoneWritable(referenceId);
        }

        if (!result && topiaIdFactory.isTopiaId(referenceId) && PracticedSystem.class.equals(topiaIdFactory.getClassName(referenceId))) {
            result = isPracticedSystemWritable(referenceId);
        }

        if (!result && referenceId.endsWith("-measurement")) {
            referenceId = referenceId.substring(0, referenceId.lastIndexOf("-measurement"));
        }

        if (!result) {
            // User might not have permission on all campaigns (i.e code) but only a specific one (i.e topiaId)
            Iterable<DecisionRule> decisionRules = decisionRuleDao.forCodeEquals(referenceId).findAllLazy();
            Iterator<DecisionRule> iterator = decisionRules.iterator();
            while (!result && iterator.hasNext()) {
                DecisionRule decisionRule = iterator.next();
                result = isDecisionRuleWritable(decisionRule.getTopiaId());
            }
        }

        return result;
    }

    @Override
    public void checkReadAttachment(String attachmentMetadataId) {

        AttachmentMetadata attachmentMetadata = attachmentMetadataDao.forTopiaIdEquals(attachmentMetadataId).findUnique();
        String referenceId = attachmentMetadata.getObjectReferenceId();

        String userId = getUserIdAndComputePermissions();

        boolean result = isAdmin() ||
                hasObjectPermissionReadable(userId, referenceId);

        TopiaIdFactory topiaIdFactory = context.getPersistenceContext().getTopiaIdFactory();
        if (!result && topiaIdFactory.isTopiaId(referenceId) && ManagementMode.class.equals(topiaIdFactory.getClassName(referenceId))) {
            result = isManagementModeReadable(referenceId);
        }

        if (!result && topiaIdFactory.isTopiaId(referenceId) && Performance.class.equals(topiaIdFactory.getClassName(referenceId))) {
            result = isPerformanceReadable(referenceId);
        }

        if (!result && topiaIdFactory.isTopiaId(referenceId) &&
                PracticedSystem.class.equals(topiaIdFactory.getClassName(referenceId))) {
            result = isPracticedSystemWritable(referenceId);
        }

        if (!result) {
            if (referenceId.endsWith("-measurement")) {
                referenceId = referenceId.substring(0, referenceId.lastIndexOf("-measurement"));
            }
            if (topiaIdFactory.isTopiaId(referenceId) && Zone.class.equals(topiaIdFactory.getClassName(referenceId))) {
                result = isZoneReadable(referenceId);
            }
        }

        if (!result) {
            // User might not have permission on all campaigns (i.e code) but only a specific one (i.e topiaId)
            Iterable<GrowingSystem> growingSystems = growingSystemDao.forCodeEquals(referenceId).findAllLazy();
            Iterator<GrowingSystem> iterator = growingSystems.iterator();
            while (!result && iterator.hasNext()) {
                GrowingSystem growingSystem = iterator.next();
                result = hasObjectPermissionReadable(userId, growingSystem.getTopiaId());
            }
        }

        if (!result) {
            // User might not have permission on all campaigns (i.e code) but only a specific one (i.e topiaId)
            Iterable<DecisionRule> decisionRules = decisionRuleDao.forCodeEquals(referenceId).findAllLazy();
            Iterator<DecisionRule> iterator = decisionRules.iterator();
            while (!result && iterator.hasNext()) {
                DecisionRule decisionRule = iterator.next();
                result = isDecisionRuleReadable(decisionRule.getTopiaId());
            }
        }

        // TODO AThimel 13/01/14 Maybe other entities needs this kind of consideration ?

        if (!result) {
            // TODO AThimel 11/12/13 Give some information to the created exception
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur la pièce jointe" + attachmentMetadataId);
        }
    }

    @Override
    public boolean isPerformanceWritable(String performanceId) {

        boolean result = isAdmin();

        if (!result) {

            String userId = getUserIdAndComputePermissions();

            Callable<Boolean> loader = () -> {
                AgrosystUser user = agrosystUserDao.forTopiaIdEquals(userId).findUnique();

                long count = performanceDao.forProperties(
                        Performance.PROPERTY_TOPIA_ID, performanceId,
                        Performance.PROPERTY_AUTHOR, user).count();

                return count == 1L;
            };

            result = cacheService.get(CacheDiscriminator.hasRole(), userId + "_" + performanceId, loader);

        }

        return result;
    }



    @Override
    public void checkPerformanceReadable(String performanceId) throws AgrosystAccessDeniedException {
        if (!isPerformanceReadable(performanceId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le performance " + performanceId);
        }
    }

    protected boolean isPerformanceReadable(String performanceId) {
        // Pour les performances, il n'y a pas de différence entre les droits en lecture et écriture : seul le créateur peut les lire/écrire
        return isPerformanceWritable(performanceId);
    }

    @Override
    public void checkCreateOrUpdatePerformance(String performanceId) throws AgrosystAccessDeniedException {
        if (Strings.isNullOrEmpty(performanceId)) {
            // TODO AThimel 18/10/13 create authorisation check
        } else if (!isPerformanceWritable(performanceId)) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur la performance " + performanceId);
        }
    }

    @Override
    public boolean isPlotWritable(String plotId) {
        Plot plot = plotDao.forTopiaIdEquals(plotId).findUnique();
        String domainId = plot.getDomain().getTopiaId();
        return isDomainWritable(domainId);
    }

    @Override
    public void checkPlotReadable(String plotId) throws AgrosystAccessDeniedException {
        Plot plot = plotDao.forTopiaIdEquals(plotId).findUnique();
        String domainId = plot.getDomain().getTopiaId();
        checkDomainReadable(domainId);
    }

    @Override
    public void checkCreateOrUpdatePlot(String plotId) throws AgrosystAccessDeniedException {
        if (Strings.isNullOrEmpty(plotId)) {
            // TODO AThimel 18/10/13 create authorisation check
        } else {
            if (!isPlotWritable(plotId)) {
                throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur la parcelle" + plotId);
            }
        }
    }

    @Override
    public void checkPracticedPlotReadable(String practicedPlotId) throws AgrosystAccessDeniedException {
        PracticedPlot practicedPlot = practicedPlotDao.forTopiaIdEquals(practicedPlotId).findUnique();
        // TODO kmorin 20221121 check si practicedPlot AGS ou IpmWorks ?
        String practicedSystemId = IterableUtils.get(practicedPlot.getPracticedSystem(), 0).getTopiaId();
        checkPracticedSystemReadable(practicedSystemId);
    }

    @Override
    public boolean isPracticedPlotWritable(String practicedPlotId) {
        PracticedPlot practicedPlot = practicedPlotDao.forTopiaIdEquals(practicedPlotId).findUnique();
        // TODO kmorin 20221121 check si practicedPlot AGS ou IpmWorks ?
        String practicedSystemId = IterableUtils.get(practicedPlot.getPracticedSystem(), 0).getTopiaId();
        return isPracticedSystemWritable(practicedSystemId);
    }

    @Override
    public void checkCreateOrUpdatePracticedPlot(String practicedPlotId, String practicedSystemId) throws AgrosystAccessDeniedException {
        if (Strings.isNullOrEmpty(practicedPlotId)) {
            checkCreateOrUpdatePracticedSystem(practicedSystemId);
        } else {
            if (!isPracticedPlotWritable(practicedPlotId)) {
                throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur la parcelle " + practicedPlotId);
            }
        }
    }

    @Override
    public void checkReportRegionalReadable(String reportRegionalId) throws AgrosystAccessDeniedException {
        if (!isReportRegionalReadable(reportRegionalId)) {
            // TODO AThimel 07/10/13 Give some information to the created exception
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le raport régional " + reportRegionalId);
        }
    }

    @Override
    public void checkDeleteReportRegional(String reportRegionalId) {
        checkReportRegionalReadable(reportRegionalId);
        checkCreateOrUpdateReportRegional(reportRegionalId);
    }

    protected boolean isReportRegionalReadable(String reportRegionalId) {
        String userId = getUserIdAndComputePermissions();

        return isAdmin() ||
                hasObjectPermissionReadable(userId, PermissionObjectType.REPORT_REGIONAL_ID, reportRegionalId);
    }

    @Override
    public boolean isReportRegionalWritable(String reportRegionalId) {
        String userId = getUserIdAndComputePermissions();

        return isAdmin() ||
                hasObjectPermissionWritable(userId, PermissionObjectType.REPORT_REGIONAL_ID, reportRegionalId);
    }

    @Override
    public void checkCreateOrUpdateReportRegional(String reportRegionalId) throws AgrosystAccessDeniedException {
        if (Strings.isNullOrEmpty(reportRegionalId)) {
            // TODO AThimel 18/10/13 create authorisation check
        } else {
            if (!isReportRegionalWritable(reportRegionalId)) {
                throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le rapport régional " + reportRegionalId);
            }
        }
    }

    @Override
    public void reportRegionalCreated(ReportRegional reportRegional) {
        // for the first one, cannot make any dirty
        String userId = getUserIdAndComputePermissions();
        
        dropComputedPermissions0(userId);

        clearCacheAndSync(); // for network projections
    }

    @Override
    public boolean isReportGrowingSystemWritable(String reportGrowingSystemId) {
        ReportGrowingSystem reportGrowingSystem = reportGrowingSystemDao.forTopiaIdEquals(reportGrowingSystemId).findUnique();
        String growingSystemId = reportGrowingSystem.getGrowingSystem().getTopiaId();
        return isReportGrowingSystemWritable(reportGrowingSystem, growingSystemId);
    }

    @Override
    public void checkDeleteReportGrowingSystem(String reportGrowingSystemId) {
        ReportGrowingSystem reportGrowingSystem = reportGrowingSystemDao.forTopiaIdEquals(reportGrowingSystemId).findUnique();
        String growingSystemId = reportGrowingSystem.getGrowingSystem().getTopiaId();
        checkGrowingSystemReadable(growingSystemId);
        boolean isWritable = isReportGrowingSystemWritable(reportGrowingSystem, growingSystemId);
        if (!isWritable) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le rapport SDC " + reportGrowingSystemId);
        }
    }

    private boolean isReportGrowingSystemWritable(ReportGrowingSystem reportGrowingSystem, String growingSystemId) {
        ReportRegional reportRegional = reportGrowingSystem.getReportRegional();
        return isGrowingSystemWritable(growingSystemId) ||
                reportRegional != null && isReportRegionalWritable(reportRegional.getTopiaId());
    }

    @Override
    public void checkReportGrowingSystemReadable(String reportGrowingSystemId) throws AgrosystAccessDeniedException {
        String growingSystemId = reportGrowingSystemDao.findGrowingSystemIdForTopiaIdEquals(reportGrowingSystemId);
        checkGrowingSystemReadable(growingSystemId);
    }

    @Override
    public void checkCreateOrUpdateReportGrowingSystem(String reportGrowingSystemId) throws AgrosystAccessDeniedException {
        if (Strings.isNullOrEmpty(reportGrowingSystemId)) {
            // TODO AThimel 18/10/13 create authorisation check
        } else {
            if (!isReportGrowingSystemWritable(reportGrowingSystemId)) {
                throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le rapport SDC " + reportGrowingSystemId);
            }
        }
    }

}
