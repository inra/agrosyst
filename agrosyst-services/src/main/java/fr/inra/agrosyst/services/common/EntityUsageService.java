package fr.inra.agrosyst.services.common;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.services.common.UsageList;
import fr.inra.agrosyst.api.services.domain.CroppingPlanEntryDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlans;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Service has no interface because it should be used only internally in other services
 *
 * @author Arnaud Thimel (Code Lutin)
 */
@Setter
public class EntityUsageService extends AbstractAgrosystService {

    private static final Log LOGGER = LogFactory.getLog(EntityUsageService.class);
    
    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    protected DomainTopiaDao domainDao;
    protected AbstractInputUsageTopiaDao inputUsageTopiaDao;
    protected GrowingSystemTopiaDao growingSystemDao;
    protected HarvestingActionTopiaDao harvestingActionDao;
    protected PracticedCropCycleNodeTopiaDao practicedCropCycleNodeDao;
    protected PracticedPerennialCropCycleTopiaDao practicedPerennialCropCycleDao;
    protected PracticedSpeciesStadeTopiaDao practicedSpeciesStadeDao;
    protected PracticedSystemTopiaDao practicedSystemDao;
    protected ZoneTopiaDao zoneDao;
    
    /**
     * Cherche à vérifier pour chaque identifiant donné (à l'aide des fonctions fournies) s'il est utilisé
     *
     * @param allIds              les identifiants des éléments à trouver
     * @param usageCheckFunctions les fonctions à utiliser. L'ordre a son importance, c'est pourquoi on utilise une
     *                            LinkedHashMap
     * @return une Map dont la taille est la même que {@code allIds}, les identifiants étant les clés de la Map et la
     *         valeur indique si cet identifiant est utilisé ou non
     */
    protected ImmutableMap<String, Boolean> computeUsageMap(Set<String> allIds, LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions) {

        // Au fur et à mesure on va constituer un Set avec les éléments qui sont utilisés. Ce Set va permettre de les exclure des recherches suivantes
        Set<String> usedElements = Sets.newHashSet();

        for (Map.Entry<String, Function<Set<String>, Set<String>>> entry : usageCheckFunctions.entrySet()) {
            // On extrait d'abord les identifiants qui n'ont pa encore été trouvés
            Set<String> unknownEntityUsageIds = Sets.difference(allIds, usedElements).immutableCopy();

            // Si il en reste à chercher ...
            if (!unknownEntityUsageIds.isEmpty()) {
                long start = System.currentTimeMillis();

                // ... on utilise la fonction et on ajoute le résultat à la liste des éléments trouvés
                Set<String> inUseElements = entry.getValue().apply(unknownEntityUsageIds);
                usedElements.addAll(inUseElements);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(String.format("%s: %dms (%d in use over %d)",
                            entry.getKey(),
                            System.currentTimeMillis() - start,
                            inUseElements.size(),
                            unknownEntityUsageIds.size()));
                }
            }
        }

        // Pour chaque élément, on remplit la Map de retour en précisant si on l'a trouvé ou pas
        Map<String, Boolean> result = new HashMap<>();
        allIds.forEach(id -> result.put(id, usedElements.contains(id)));
        return ImmutableMap.copyOf(result);
    }

    public UsageList<ToolsCoupling> getToolsCouplingUsageList(List<ToolsCoupling> toolsCouplings, Integer campaign) {
        Map<String, Boolean> map;
        if (toolsCouplings != null && !toolsCouplings.isEmpty()){
            Set<String> ids = toolsCouplings.stream().map(TopiaEntity::getTopiaId).collect(Collectors.toSet());
            String stCampaign = "%" + campaign + "%";
            map = getToolsCouplingUsageMap(ids, stCampaign);
        } else {
            map = new HashMap<>();
        }

        UsageList<ToolsCoupling> result = UsageList.of(toolsCouplings, map);
        return result;
    }

    protected Map<String, Boolean> getToolsCouplingUsageMap(Set<String> toolsCouplingIds, String campaign) {

        LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();
        usageCheckFunctions.put("tcUsedForEffectiveInterventionResult", ids -> domainDao.getTCUsedForEffectiveInterventions(ids));
        usageCheckFunctions.put("forTCUsedForPracticedInterventionPhasesResult", ids -> domainDao.getTCUsedForPracticedInterventionPhases(ids, campaign));
        usageCheckFunctions.put("tcUsedForPracticedCropCycleNodesAndConnections", ids -> domainDao.getTCUsedForPracticedInterventionNodesAndConnections(ids, campaign));

        Map<String, Boolean> result = computeUsageMap(toolsCouplingIds, usageCheckFunctions);
        return result;
    }
    
    public Map<InputType, UsageList<DomainInputDto>> getDomainInputUsageList(Map<InputType, List<DomainInputDto>> domainInputDtosByInputType) {
        Map<InputType, UsageList<DomainInputDto>> globalResult = new HashMap<>();
        if (domainInputDtosByInputType != null && !domainInputDtosByInputType.isEmpty()){
            for (Map.Entry<InputType, List<DomainInputDto>> inputTypeToDomainInputs : domainInputDtosByInputType.entrySet()) {
                final InputType inputType = inputTypeToDomainInputs.getKey();
                final List<DomainInputDto> domainInputDtos = inputTypeToDomainInputs.getValue();
                switch (inputType) {
                    case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                        if (CollectionUtils.isNotEmpty(domainInputDtos)) {
                            
                            final Set<String> allDomainInputIds = domainInputDtos.stream()
                                    .filter(dto -> dto.getTopiaId().isPresent())
                                    .map(dto -> dto.getTopiaId().get())
                                    .collect(Collectors.toSet());
                            
                            LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();
    
                            usageCheckFunctions.put("forMineralProductInputUsage", ids -> inputUsageTopiaDao.getDomainMineralProductInputUsed(ids));
    
                            final ImmutableMap<String, Boolean> inputUsages = computeUsageMap(allDomainInputIds, usageCheckFunctions);
                            UsageList<DomainInputDto> currentResult = UsageList.of(domainInputDtos, inputUsages);
                            
                            globalResult.put(inputType, currentResult);
                        }
                    }
                    case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES -> {
                        if (CollectionUtils.isNotEmpty(domainInputDtos)) {
        
                            final Set<String> allDomainInputIds = domainInputDtos.stream()
                                    .filter(dto -> dto.getTopiaId().isPresent())
                                    .map(dto -> dto.getTopiaId().get())
                                    .collect(Collectors.toSet());
    
                            LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();
    
                            usageCheckFunctions.put("forPesticideProductInputUsage", ids -> inputUsageTopiaDao.getDomainPhytoProductInputUsedForPesticideProductInputUsage(ids));
    
                            final ImmutableMap<String, Boolean> inputUsages = computeUsageMap(allDomainInputIds, usageCheckFunctions);
                            UsageList<DomainInputDto> currentResult = UsageList.of(domainInputDtos, inputUsages);
                            
                            globalResult.put(inputType, currentResult);
                        }
                    }
                    case AUTRE -> {
                        if (CollectionUtils.isNotEmpty(domainInputDtos)) {
        
                            final Set<String> allDomainInputIds = domainInputDtos.stream()
                                    .filter(dto -> dto.getTopiaId().isPresent())
                                    .map(dto -> dto.getTopiaId().get())
                                    .collect(Collectors.toSet());
        
                            LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();
    
                            usageCheckFunctions.put("forOtherInputUsage", ids -> inputUsageTopiaDao.getDomainOtherInputUsed(ids));
        
                            final ImmutableMap<String, Boolean> inputUsages = computeUsageMap(allDomainInputIds, usageCheckFunctions);
                            UsageList<DomainInputDto> currentResult = UsageList.of(domainInputDtos, inputUsages);
        
                            globalResult.put(inputType, currentResult);
                        }
                        
                    }
                    case CARBURANT, MAIN_OEUVRE_MANUELLE, MAIN_OEUVRE_TRACTORISTE -> {
                        if (CollectionUtils.isNotEmpty(domainInputDtos)) {
                            final Set<String> allDomainInputIds = domainInputDtos.stream()
                                    .filter(dto -> dto.getTopiaId().isPresent())
                                    .map(dto -> dto.getTopiaId().get())
                                    .collect(Collectors.toSet());
                            
                            HashMap<String, Boolean> inputUsages = new HashMap<>();
                            allDomainInputIds.forEach(id -> inputUsages.put(id, false));
                            
                            UsageList<DomainInputDto> currentResult = UsageList.of(domainInputDtos, inputUsages);
                            globalResult.put(inputType, currentResult);
                        }
                    }
                    case EPANDAGES_ORGANIQUES -> {
                        if (CollectionUtils.isNotEmpty(domainInputDtos)) {
        
                            final Set<String> allDomainInputIds = domainInputDtos.stream()
                                    .filter(dto -> dto.getTopiaId().isPresent())
                                    .map(dto -> dto.getTopiaId().get())
                                    .collect(Collectors.toSet());
        
                            LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();
    
                            usageCheckFunctions.put("forOrganicProductInputUsage", ids -> inputUsageTopiaDao.getOrganicProductInputUsed(ids));
        
                            final ImmutableMap<String, Boolean> inputUsages = computeUsageMap(allDomainInputIds, usageCheckFunctions);
                            UsageList<DomainInputDto> currentResult = UsageList.of(domainInputDtos, inputUsages);
        
                            globalResult.put(inputType, currentResult);
                        }
                    }
                    case IRRIGATION -> {
                        if (CollectionUtils.isNotEmpty(domainInputDtos)) {
        
                            final Set<String> allDomainInputIds = domainInputDtos.stream()
                                    .filter(dto -> dto.getTopiaId().isPresent())
                                    .map(dto -> dto.getTopiaId().get())
                                    .collect(Collectors.toSet());
        
                            LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();
    
                            usageCheckFunctions.put("forIrrigationInputUsage", ids -> inputUsageTopiaDao.getDomainIrrigationInputUsed(ids));
        
                            final ImmutableMap<String, Boolean> inputUsages = computeUsageMap(allDomainInputIds, usageCheckFunctions);
                            UsageList<DomainInputDto> currentResult = UsageList.of(domainInputDtos, inputUsages);
        
                            globalResult.put(inputType, currentResult);
                        }
                    }
                    case LUTTE_BIOLOGIQUE -> {
                        if (CollectionUtils.isNotEmpty(domainInputDtos)) {
        
                            final Set<String> allDomainInputIds = domainInputDtos.stream()
                                    .filter(dto -> dto.getTopiaId().isPresent())
                                    .map(dto -> dto.getTopiaId().get())
                                    .collect(Collectors.toSet());
        
                            LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();
    
                            usageCheckFunctions.put("forBiologicalProductInputUsage", ids -> inputUsageTopiaDao.getDomainPhytoProductInputUsedForBiologicalProductInputUsage(ids));
        
                            final ImmutableMap<String, Boolean> inputUsages = computeUsageMap(allDomainInputIds, usageCheckFunctions);
                            UsageList<DomainInputDto> currentResult = UsageList.of(domainInputDtos, inputUsages);
        
                            globalResult.put(inputType, currentResult);
                        }
                    }
                    case POT -> {
                        if (CollectionUtils.isNotEmpty(domainInputDtos)) {
        
                            final Set<String> allDomainInputIds = domainInputDtos.stream()
                                    .filter(dto -> dto.getTopiaId().isPresent())
                                    .map(dto -> dto.getTopiaId().get())
                                    .collect(Collectors.toSet());
        
                            LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();
    
                            usageCheckFunctions.put("forPotInputUsage", ids -> inputUsageTopiaDao.getDomainPotInputUsed(ids));
        
                            final ImmutableMap<String, Boolean> inputUsages = computeUsageMap(allDomainInputIds, usageCheckFunctions);
                            UsageList<DomainInputDto> currentResult = UsageList.of(domainInputDtos, inputUsages);
        
                            globalResult.put(inputType, currentResult);
                        }
                    }
                    case PLAN_COMPAGNE, SEMIS -> {
                        // done later
                    }
                    case SUBSTRAT -> {
                        if (CollectionUtils.isNotEmpty(domainInputDtos)) {
        
                            final Set<String> allDomainInputIds = domainInputDtos.stream()
                                    .filter(dto -> dto.getTopiaId().isPresent())
                                    .map(dto -> dto.getTopiaId().get())
                                    .collect(Collectors.toSet());
        
                            LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();
    
                            usageCheckFunctions.put("forSubstrateInputUsage", ids -> inputUsageTopiaDao.getDomainSubstrateInputUsed(ids));
        
                            final ImmutableMap<String, Boolean> inputUsages = computeUsageMap(allDomainInputIds, usageCheckFunctions);
                            UsageList<DomainInputDto> currentResult = UsageList.of(domainInputDtos, inputUsages);
        
                            globalResult.put(inputType, currentResult);
                        }
                    }
                    case TRAITEMENT_SEMENCE -> {
                        if (CollectionUtils.isNotEmpty(domainInputDtos)) {
        
                            final Set<String> allDomainInputIds = domainInputDtos.stream()
                                    .filter(dto -> dto.getTopiaId().isPresent())
                                    .map(dto -> dto.getTopiaId().get())
                                    .collect(Collectors.toSet());
        
                            LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();
        
                            usageCheckFunctions.put("forSeedProductInputUsage", ids -> inputUsageTopiaDao.getDomainPhytoProductInputUsedForSeedProductInputUsage(ids));
        
                            final ImmutableMap<String, Boolean> inputUsages = computeUsageMap(allDomainInputIds, usageCheckFunctions);
                            UsageList<DomainInputDto> currentResult = UsageList.of(domainInputDtos, inputUsages);
        
                            globalResult.put(inputType, currentResult);
                        }
                    }
                }
            }

            List<DomainInputDto> seedingElement = ListUtils.union(ListUtils.emptyIfNull(domainInputDtosByInputType.get(InputType.SEMIS)),
                    ListUtils.emptyIfNull(domainInputDtosByInputType.get(InputType.PLAN_COMPAGNE)));
            if (CollectionUtils.isNotEmpty(seedingElement)) {
                final Set<String> allDomainInputIds = seedingElement.stream()
                        .filter(dto -> dto.getTopiaId().isPresent())
                        .map(dto -> dto.getTopiaId().get())
                        .collect(Collectors.toSet());

                LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();

                usageCheckFunctions.put("forDomainSeedLotInputUsage", ids -> inputUsageTopiaDao.getDomainSeedLotInputUsed(ids));
                usageCheckFunctions.put("forDomainSeedSpeciesInputUsage", ids -> inputUsageTopiaDao.getDomainSeedSpeciesInputUsed(ids));

                final ImmutableMap<String, Boolean> inputUsages = computeUsageMap(allDomainInputIds, usageCheckFunctions);
                UsageList<DomainInputDto> currentResult = UsageList.of(seedingElement, inputUsages);

                globalResult.put(InputType.SEMIS, currentResult);
            }

        } else {
            globalResult = new HashMap<>();
        }
    
        return globalResult;
    }
    
    public UsageList<Zone> getZonesUsageList(List<Zone> zones) {
        Map<String, Boolean> map;
        if (!zones.isEmpty()) {
            Set<String> ids = zones.stream().map(TopiaEntity::getTopiaId).collect(Collectors.toSet());
            map = getZonesUsageMap(ids);
        } else {
            map = new HashMap<>();
        }
        UsageList<Zone> result = UsageList.of(zones, map);
        return result;
    }

    public Map<String, Boolean> getZonesUsageMap(Set<String> zonesIds) {

        LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();
        usageCheckFunctions.put("getZonesUsedForPerformancesResult", ids -> zoneDao.getZonesUsedForPerformances(ids));
        usageCheckFunctions.put("getZonesUsedForMeasurementSessionsResult", ids -> zoneDao.getZonesUsedForMesurementSessions(ids));
        usageCheckFunctions.put("getZonesUsedForEffectivePerennialCropCyclesResult", ids -> zoneDao.getZonesUsedForEffectivePerennialCropCycles(ids));
        usageCheckFunctions.put("getZonesUsedForEffectiveSeasonalCropCyclesResult", ids -> zoneDao.getZonesUsedForEffectiveSeasonalCropCycles(ids));

        Map<String, Boolean> result = computeUsageMap(zonesIds, usageCheckFunctions);
        return result;

    }

    public UsageList<CroppingPlanEntryDto> getCroppingPlanEntryDtoAndUsage(List<CroppingPlanEntry> croppingPlanEntries, Integer campaign,
                                                                           ReferentialTranslationMap translationMap) {
        Map<String, Boolean> map;
        List<CroppingPlanEntryDto> croppingPlanEntryDtos;
        if (croppingPlanEntries != null && !croppingPlanEntries.isEmpty()) {
            Set<String> ids = croppingPlanEntries.stream().map(TopiaEntity::getTopiaId).collect(Collectors.toSet());
            String stCampaign = "%" + campaign + "%";
            map = getCroppingPlanEntryUsageMap(ids, stCampaign);
            croppingPlanEntryDtos = croppingPlanEntries.stream()
                    .map(cp -> CroppingPlans.getDtoForCroppingPlanEntry(cp, translationMap))
                    .collect(Collectors.toList());
        } else {
            map = new HashMap<>();
            croppingPlanEntryDtos = new ArrayList<>();
        }

        UsageList<CroppingPlanEntryDto> result = UsageList.of(croppingPlanEntryDtos, map);
        return result;
    }

    public Map<String, Boolean> getCroppingPlanEntryUsageMap(Set<String> croppingPlanEntriesIds, String campaign) {
        
        LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();
        usageCheckFunctions.put("forLotResult", ids -> croppingPlanEntryDao.getCPEUsedForLot(ids));
        usageCheckFunctions.put("forCPEUsedForEffectiveCropCycleNode", ids -> croppingPlanEntryDao.getCPEUsedForEffectiveCropCycleNode(ids));
        usageCheckFunctions.put("forEffectivePerennialCropCyclesResult", ids -> croppingPlanEntryDao.getCPEUsedForEffectivePerenialCropCycles(ids));
        usageCheckFunctions.put("forEffectiveCropCycleConnectionsIntermediateResult", ids -> croppingPlanEntryDao.getCPEUsedForEffectiveCropCycleConnectionsIntermediate(ids));
        usageCheckFunctions.put("forMeasurementSessionsResult", ids -> croppingPlanEntryDao.getCPEUsedForMeasurementSessions(ids));
        usageCheckFunctions.put("forStrategiesResult", ids -> croppingPlanEntryDao.getCPEUsedForStrategies(ids));
        usageCheckFunctions.put("forCropPestMaster", ids -> croppingPlanEntryDao.getCPEUsedForCropPestMaster(ids));
        usageCheckFunctions.put("forVerseMaster", ids -> croppingPlanEntryDao.getCPEUsedForVerseMaster(ids));
        usageCheckFunctions.put("forFoodMaster", ids -> croppingPlanEntryDao.getCPEUsedForFoodMaster(ids));
        usageCheckFunctions.put("forYieldLoss", ids -> croppingPlanEntryDao.getCPEUsedForYieldLoss(ids));
        usageCheckFunctions.put("forArboCropAdventiceMaster", ids -> croppingPlanEntryDao.getCPEUsedForArboCropAdventiceMaster(ids));
        usageCheckFunctions.put("forArboCropPestMaster", ids -> croppingPlanEntryDao.getCPEUsedForArboCropPestMaster(ids));
        usageCheckFunctions.put("forPracticedCropCycleConnectionsResult", ids -> croppingPlanEntryDao.getCPEUsedForPracticedCropCycleConnections(ids, campaign));
        usageCheckFunctions.put("forPracticedCropCycleNodesResult", ids -> croppingPlanEntryDao.getCPEUsedForPracticedCropCycleNodes(ids, campaign));
        usageCheckFunctions.put("forPracticedPerennialCropCyclesResult", ids -> croppingPlanEntryDao.getCPEUsedPracticedPerennialCropCycles(ids, campaign));

        Map<String, Boolean> result = computeUsageMap(croppingPlanEntriesIds, usageCheckFunctions);
        return result;
    }

    public Map<String, Boolean> getCroppingPlanSpeciesUsageMap(List<CroppingPlanSpecies> croppingPlanSpecies, Integer campaign, String domainCode) {

        Set<String> croppingPlanSpeciesIds = croppingPlanSpecies.stream().map(TopiaEntity::getTopiaId).collect(Collectors.toSet());
        String stCampaign = "%"+ campaign +"%";

        LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();
        usageCheckFunctions.put("forLotResults", ids -> croppingPlanSpeciesDao.getCPSpeciesUsedForLot(ids));
        usageCheckFunctions.put("forEffectiveCropCycleSpeciesResults", ids -> croppingPlanSpeciesDao.getCPSpeciesUsedForEffectiveCropCycleSpecies(ids));
        usageCheckFunctions.put("forEffectiveSpeciesStadesResults", ids -> croppingPlanSpeciesDao.getCPSpeciesUsedForEffectiveSpeciesStades(ids));
        usageCheckFunctions.put("forCropPestMaster", ids -> croppingPlanSpeciesDao.getCPSUsedForCropPestMaster(ids));
        usageCheckFunctions.put("forVerseMaster", ids -> croppingPlanSpeciesDao.getCPSUsedForVerseMaster(ids));
        usageCheckFunctions.put("forFoodMaster", ids -> croppingPlanSpeciesDao.getCPSUsedForFoodMaster(ids));
        usageCheckFunctions.put("forYieldLoss", ids -> croppingPlanSpeciesDao.getCPSUsedForYieldLoss(ids));
        usageCheckFunctions.put("forArboCropAdventiceMaster", ids -> croppingPlanSpeciesDao.getCPSUsedForArboCropAdventiceMaster(ids));
        usageCheckFunctions.put("forArboCropPestMaster", ids -> croppingPlanSpeciesDao.getCPSUsedForArboCropPestMaster(ids));
        usageCheckFunctions.put("forMeasuresResults", ids -> croppingPlanSpeciesDao.getCPSpeciesUsedForMeasures(ids));
        usageCheckFunctions.put("forPracticedPerennialCropCycleStadesResults", ids -> croppingPlanSpeciesDao.getCPSpeciesUsedForPracticedPerennialCropCycleStades(domainCode, stCampaign, ids));
        usageCheckFunctions.put("forPracticedCropCycleNodesAndConnectionsResults", ids -> getCPSpeciesUsedForPracticedCropCycleNodesAndConnections(ids, domainCode, stCampaign));
        usageCheckFunctions.put("forPracticedCropCycleSpeciesResults", ids -> croppingPlanSpeciesDao.getCPSpeciesUsedForPracticedCropCycleSpecies(domainCode, stCampaign, ids));

        Map<String, Boolean> result = computeUsageMap(croppingPlanSpeciesIds, usageCheckFunctions);
        return result;
    }

    protected Set<String> getCPSpeciesUsedForPracticedCropCycleNodesAndConnections(Set<String> croppingPlanSpeciesIds, String domainCode, String campaign) {

        Set<String> result = new HashSet<>();
        // Find all nodes related to a domain with same domainCode as the given one.
        //   and practicedSystem matching the given campaign
        List<PracticedCropCycleNode> practicedCropCycleNodes = croppingPlanSpeciesDao.getPracticedCropCycleNodeForCampaignAndDomainCode(domainCode, campaign);


        // intervention where crops are used in stades
        if(CollectionUtils.isNotEmpty(practicedCropCycleNodes)) {
            // find speciesStades where given species are used
            //    speciesStades: map of speciesStades and it's related Species TopiaId
            Map<PracticedSpeciesStade, String> speciesStades = croppingPlanSpeciesDao.getSpeciesPracticedSpeciesStades(croppingPlanSpeciesIds);
            if (speciesStades != null && !speciesStades.isEmpty()) {

                // find intervention related to the given nodes
                List<PracticedIntervention> practicedInterventions = croppingPlanSpeciesDao.practicedInterventionsForSources(practicedCropCycleNodes);
                final List<PracticedIntervention> targetedSpeciesInterventions = croppingPlanSpeciesDao.practicedInterventionsForTargets(practicedCropCycleNodes);
                practicedInterventions.addAll(targetedSpeciesInterventions);
                Set<PracticedIntervention> practicedInterventionSet = Sets.newHashSet(practicedInterventions);

                // find practiced stades related to the interventions
                Set<PracticedSpeciesStade> speciesStadesForGivenCampaign = Sets.newHashSet();
                for (PracticedIntervention practicedIntervention : practicedInterventionSet) {
                    if (practicedIntervention.getSpeciesStades() != null) {
                        speciesStadesForGivenCampaign.addAll(ListUtils.removeAll(practicedIntervention.getSpeciesStades(), speciesStades.values()));
                    }
                }

                // set 1 for species is used into speciesStade otherwise 0
                for (PracticedSpeciesStade practicedSpeciesStade : speciesStadesForGivenCampaign) {
                    final String speciesId = speciesStades.get(practicedSpeciesStade);
                    result.add(speciesId);
                }
            }
        }

        return result;
    }
    
    public Set<String> filterOnCattleCodeUsed(Set<String> cattleCodes, Integer campaign, String domainCode) {
        
        // be careful, there are many return !
        Set<String> result = new HashSet<>();
        
        // look for action that u sed the given cattle code
        List<HarvestingAction> harvestingActions = harvestingActionDao.forCattleCodeIn(cattleCodes).findAll();
        
        // filter on campaing
        List<EffectiveIntervention> effectiveInterventions = harvestingActions.stream()
                .map(AbstractAction::getEffectiveIntervention)
                .filter(Objects::nonNull)
                .filter(effectiveIntervention -> (effectiveIntervention.getStartInterventionDate().getYear() <= campaign && effectiveIntervention.getEndInterventionDate().getYear() >= campaign)).toList();
        
        Iterator<HarvestingAction> remainingActions = harvestingActions.iterator();
        
        while (remainingActions.hasNext()) {
            HarvestingAction harvestingAction = remainingActions.next();
            if (effectiveInterventions.contains(harvestingAction.getEffectiveIntervention())) {
                remainingActions.remove();
                result.add(harvestingAction.getCattleCode());
            }
        }
        
        // all action have been found, they all match effective intervention
        // no need to go any further
        if (harvestingActions.isEmpty()) {
            return result;
        }
        
        // look for usage on practiced systems
        
        // filter practiced system on growing systems that target the given domain code
        List<GrowingSystem> growingSystems = growingSystemDao.forProperties(
                GrowingSystem.PROPERTY_GROWING_PLAN + "." +
                GrowingPlan.PROPERTY_DOMAIN + "." +
                Domain.PROPERTY_CODE, domainCode).findAll();
        // there are no growing system so no practiced system as well
        if (CollectionUtils.isEmpty(growingSystems)) {
            return result;
        }
        
        // look for practiced system that target given growing systems and match given campaigns
        List<PracticedSystem> practicedSystems = practicedSystemDao.loadAllPracticedSystemsForGrowingSystemsAnsCampaign(new HashSet<>(growingSystems), campaign);
        
        final boolean practicedSystemsExists = CollectionUtils.isNotEmpty(practicedSystems);
        
        // no practiced systems found, no need to go any further
        if (!practicedSystemsExists) {
            return result;
        }
        
        // look for cattle usage in seasonnal paracticed crop cycle
        remainingActions = harvestingActions.iterator();
        while (remainingActions.hasNext()) {
            // look into seasonal practiced Systems
            
            HarvestingAction remainingAction = remainingActions.next();
            String cattleCode = remainingAction.getCattleCode();
            PracticedCropCycleConnection connection = remainingAction.getPracticedIntervention().getPracticedCropCycleConnection();
            if (connection != null) {
                PracticedCropCycle practicedCropCycle = connection.getTarget().getPracticedSeasonalCropCycle();
                if (practicedSystems.contains(practicedCropCycle.getPracticedSystem())) {
                    result.add(cattleCode);
                    remainingActions.remove();
                }
            }
            
        }
        
        // all action have been found
        // no need to go any further
        if (CollectionUtils.isEmpty(harvestingActions)) {
            return result;
        }
        
        // Look into perennial practiced systems
        List<PracticedPerennialCropCycle> practicedCropCycles = practicedPerennialCropCycleDao.forPracticedSystemIn(practicedSystems).findAll();
        
        remainingActions = harvestingActions.iterator();
        
        final boolean phaseCycleExists = CollectionUtils.isNotEmpty(practicedCropCycles);
        while (phaseCycleExists && remainingActions.hasNext()) {
            // look into seasonal practiced Systems
            
            HarvestingAction remainingAction = remainingActions.next();
            String cattleCode = remainingAction.getCattleCode();
            PracticedCropCyclePhase cropCyclePhase = remainingAction.getPracticedIntervention().getPracticedCropCyclePhase();
            
            if (cropCyclePhase != null) {
                for (PracticedPerennialCropCycle practicedCropCycle : practicedCropCycles) {
                    if (practicedCropCycle.getCropCyclePhases().contains(cropCyclePhase)) {
                        // phase found
                        result.add(cattleCode);
                        break;
                    }
                }
            }
            
        }
        
        return result;
    }

    public Map<String, Boolean> getDomainSeedLotInputUsageMap(Collection<DomainSeedLotInput> domainInputStockUnits) {

        Set<String> inputIds = domainInputStockUnits.stream()
                .map(AbstractDomainInputStockUnit::getTopiaId)
                .collect(Collectors.toSet());
        Function<Set<String>, Set<String>> setSetFunction = ids -> inputUsageTopiaDao.getDomainSeedLotInputUsed(ids);
        Map<String, Boolean> result = new HashMap<>(computeUsageMap(inputIds, setSetFunction, "forSeedLotInputUsage"));

        inputIds = domainInputStockUnits.stream()
                .map(DomainSeedLotInput::getDomainSeedSpeciesInput)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(TopiaEntity::getTopiaId)
                .collect(Collectors.toSet());
        setSetFunction = ids -> inputUsageTopiaDao.getDomainSeedSpeciesInputUsed(ids);
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forSeedSpeciesInputUsage"));

        inputIds = domainInputStockUnits.stream()
                .map(DomainSeedLotInput::getDomainSeedSpeciesInput)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(DomainSeedSpeciesInput::getSpeciesPhytoInputs)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(TopiaEntity::getTopiaId)
                .collect(Collectors.toSet());
        // Consider phyto as not used if it's quantity Average is not set
        setSetFunction = ids -> inputUsageTopiaDao.getDomainPhytoProductInputUsedForSeedProductInputUsage(ids);
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forSeedProductInputUsage"));

        return result;
    }

    public Set<String> getSeedingSpeciesAndProductsUsages(Collection<DomainSeedSpeciesInput> domainSeedSpeciesInput) {
        Set<String> seedingTreatmentInputIds = new HashSet<>();
        Set<String> speciesInputIds = new HashSet<>();
        if (domainSeedSpeciesInput != null) {
            domainSeedSpeciesInput.forEach(
                    dssi -> {
                        seedingTreatmentInputIds.addAll(dssi.getSpeciesPhytoInputsTopiaIds());
                        speciesInputIds.add(dssi.getTopiaId());
                    }
            );
        }

        Set<String> productInputUsage = inputUsageTopiaDao.getDomainPhytoProductInputUsedForSeedProductInputUsage(seedingTreatmentInputIds);
        Set<String> seedSpeciesInputUsage = inputUsageTopiaDao.getDomainSeedSpeciesInputUsed(speciesInputIds);
        Set<String> result = new HashSet<>(productInputUsage);
        result.addAll(seedSpeciesInputUsage);
        return result;
    }

    public Map<String, Boolean> getDomainInputUsageMap(Collection<AbstractDomainInputStockUnit> domainInputStockUnits) {

        //phytos
        Set<String> inputIds = domainInputStockUnits.stream().filter(adi -> InputType.LUTTE_BIOLOGIQUE.equals(adi.getInputType())).map(AbstractDomainInputStockUnit::getTopiaId).collect(Collectors.toSet());
        Function<Set<String>, Set<String>> setSetFunction = ids -> inputUsageTopiaDao.getDomainPhytoProductInputUsedForBiologicalProductInputUsage(ids);
        Map<String, Boolean> result = new HashMap<>(computeUsageMap(inputIds, setSetFunction, "forBiologicalProductInputUsage"));

        inputIds = domainInputStockUnits.stream().filter(adi -> InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES.equals(adi.getInputType())).map(AbstractDomainInputStockUnit::getTopiaId).collect(Collectors.toSet());
        setSetFunction = ids -> inputUsageTopiaDao.getDomainPhytoProductInputUsedForPesticideProductInputUsage(ids);
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forPesticideProductInputUsage"));

        inputIds = domainInputStockUnits.stream().filter(adi -> InputType.TRAITEMENT_SEMENCE.equals(adi.getInputType())).map(AbstractDomainInputStockUnit::getTopiaId).collect(Collectors.toSet());
        setSetFunction = ids -> inputUsageTopiaDao.getDomainPhytoProductInputUsedForSeedProductInputUsage(ids);
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forSeedProductInputUsage"));

        inputIds = domainInputStockUnits.stream().filter(adi -> InputType.IRRIGATION.equals(adi.getInputType())).map(AbstractDomainInputStockUnit::getTopiaId).collect(Collectors.toSet());
        setSetFunction = ids -> inputUsageTopiaDao.getDomainIrrigationInputUsed(ids);
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forIrrigationInputUsage"));

        inputIds = domainInputStockUnits.stream().filter(adi -> InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.equals(adi.getInputType())).map(AbstractDomainInputStockUnit::getTopiaId).collect(Collectors.toSet());
        setSetFunction = ids -> inputUsageTopiaDao.getDomainMineralProductInputUsed(ids);
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forMineralProductInputUsage"));

        inputIds = domainInputStockUnits.stream().filter(adi -> InputType.EPANDAGES_ORGANIQUES.equals(adi.getInputType())).map(AbstractDomainInputStockUnit::getTopiaId).collect(Collectors.toSet());
        setSetFunction = ids -> inputUsageTopiaDao.getOrganicProductInputUsed(ids);
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forOrganicProductInputUsage"));

        inputIds = domainInputStockUnits.stream().filter(adi -> InputType.AUTRE.equals(adi.getInputType())).map(AbstractDomainInputStockUnit::getTopiaId).collect(Collectors.toSet());
        setSetFunction = ids -> inputUsageTopiaDao.getDomainOtherInputUsed(ids);
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forOtherInputUsage"));

        inputIds = domainInputStockUnits.stream().filter(adi -> InputType.POT.equals(adi.getInputType())).map(AbstractDomainInputStockUnit::getTopiaId).collect(Collectors.toSet());
        setSetFunction = ids -> inputUsageTopiaDao.getDomainPotInputUsed(ids);
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forPotInputUsage"));

        inputIds = domainInputStockUnits.stream().filter(adi -> adi instanceof DomainSeedLotInput).map(AbstractDomainInputStockUnit::getTopiaId).collect(Collectors.toSet());
        setSetFunction = ids -> inputUsageTopiaDao.getDomainSeedLotInputUsed(ids);
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forSeedLotInputUsage"));

        inputIds = domainInputStockUnits.stream().filter(adi -> adi instanceof DomainSeedSpeciesInput).map(AbstractDomainInputStockUnit::getTopiaId).collect(Collectors.toSet());
        setSetFunction = ids -> inputUsageTopiaDao.getDomainSeedSpeciesInputUsed(ids);
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forSeedSpeciesInputUsage"));

        inputIds = domainInputStockUnits.stream().filter(adi -> InputType.SUBSTRAT.equals(adi.getInputType())).map(AbstractDomainInputStockUnit::getTopiaId).collect(Collectors.toSet());
        setSetFunction = ids -> inputUsageTopiaDao.getDomainSubstrateInputUsed(ids);
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forSubstrateInputUsage"));

        inputIds = domainInputStockUnits.stream().filter(adi -> InputType.CARBURANT.equals(adi.getInputType())).map(AbstractDomainInputStockUnit::getTopiaId).collect(Collectors.toSet());
        setSetFunction = this::getFuelInputUsed;
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forFuelInputUsage"));

        inputIds = domainInputStockUnits.stream().filter(adi -> InputType.MAIN_OEUVRE_MANUELLE.equals(adi.getInputType())).map(AbstractDomainInputStockUnit::getTopiaId).collect(Collectors.toSet());
        setSetFunction = this::getManualWorkforceInputUsed;
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forManualWorkforceInputUsage"));

        inputIds = domainInputStockUnits.stream().filter(adi -> InputType.MAIN_OEUVRE_TRACTORISTE.equals(adi.getInputType())).map(AbstractDomainInputStockUnit::getTopiaId).collect(Collectors.toSet());
        setSetFunction = this::getMechanizedWorkforceInputUsed;
        result.putAll(computeUsageMap(inputIds, setSetFunction, "forMechanizedWorkforceInputUsage"));

        return result;
    }

    public Set<String> getFuelInputUsed(Set<String> domainInputIds) {
        return new HashSet<>();
    }

    public Set<String> getManualWorkforceInputUsed(Set<String> domainInputIds) {
        return new HashSet<>();
    }

    public Set<String> getMechanizedWorkforceInputUsed(Set<String> domainInputIds) {
        return new HashSet<>();
    }

    protected ImmutableMap<String, Boolean> computeUsageMap(Set<String> unknownEntityUsageIds, Function<Set<String>, Set<String>> usageCheckFunction, String forInputUsageName) {
        Set<String> usedElements = Sets.newHashSet();

        // Si il en reste à chercher ...
        if (!unknownEntityUsageIds.isEmpty()) {
            long start = System.currentTimeMillis();

            // ... on utilise la fonction et on ajoute le résultat à la liste des éléments trouvés

            Set<String> inUseElements = usageCheckFunction.apply(unknownEntityUsageIds);
            usedElements.addAll(inUseElements);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format("%s: %dms (%d in use over %d)",
                        forInputUsageName,
                        System.currentTimeMillis() - start,
                        inUseElements.size(),
                        unknownEntityUsageIds.size()));
            }
        }

        // Pour chaque élément, on remplit la Map de retour en précisant si on l'a trouvé ou pas
        Map<String, Boolean> result = new HashMap<>();

        unknownEntityUsageIds.forEach(id -> result.put(id, usedElements.contains(id)));
        return ImmutableMap.copyOf(result);
    }
}
