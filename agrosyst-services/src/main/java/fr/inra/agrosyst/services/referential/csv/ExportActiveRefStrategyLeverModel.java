package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefStrategyLever;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Created by davidcosse on 12/10/16.
 */
public class ExportActiveRefStrategyLeverModel implements ExportModel<RefStrategyLever> {

    @Override
    public char getSeparator() {
        return AbstractAgrosystModel.CSV_SEPARATOR;
    }

    @Override
    public Iterable<ExportableColumn<RefStrategyLever, Object>> getColumnsForExport() {
        ModelBuilder<RefStrategyLever> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Code", RefStrategyLever.PROPERTY_CODE);
        modelBuilder.newColumnForExport("Filière", RefStrategyLever.PROPERTY_SECTOR, AbstractAgrosystModel.GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Rubrique", RefStrategyLever.PROPERTY_SECTION_TYPE, AbstractAgrosystModel.GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Type de gestion/levier", RefStrategyLever.PROPERTY_STRATEGY_TYPE, AbstractAgrosystModel.GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Levier", RefStrategyLever.PROPERTY_LEVER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

}
