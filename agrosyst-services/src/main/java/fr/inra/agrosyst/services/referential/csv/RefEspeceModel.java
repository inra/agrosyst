package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefEspece;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefEspeceModel extends AbstractAgrosystModel<RefEspeceDto> implements ExportModel<RefEspeceDto> {
    
    public RefEspeceModel() {
        super(CSV_SEPARATOR);
        
        newIgnoredColumn("Date modification");
        newIgnoredColumn("nouveau code espèce");
        newIgnoredColumn("topiaid");
        newMandatoryColumn("code catégorie de cultures", RefEspece.PROPERTY_CODE_CATEGORIE_DE_CULTURES);
        newMandatoryColumn("libellé catégorie de cultures", RefEspece.PROPERTY_LIBELLE_CATEGORIE_DE_CULTURES);
        newMandatoryColumn("commentaire", RefEspece.PROPERTY_COMMENTAIRE);
        newMandatoryColumn("code espèce botanique", RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE);
        newMandatoryColumn("libellé espèce botanique", RefEspece.PROPERTY_LIBELLE_ESPECE_BOTANIQUE);
        newMandatoryColumn("code qualifiant AEE", RefEspece.PROPERTY_CODE_QUALIFIANT__AEE);
        newMandatoryColumn("libellé qualifiant AEE", RefEspece.PROPERTY_LIBELLE_QUALIFIANT__AEE);
        newMandatoryColumn("code type saisonnier AEE", RefEspece.PROPERTY_CODE_TYPE_SAISONNIER__AEE);
        newMandatoryColumn("libellé type saisonnier AEE", RefEspece.PROPERTY_LIBELLE_TYPE_SAISONNIER__AEE);
        newMandatoryColumn("code destination AEE", RefEspece.PROPERTY_CODE_DESTINATION__AEE);
        newMandatoryColumn("libellé destination AEE", RefEspece.PROPERTY_LIBELLE_DESTINATION__AEE);
        newMandatoryColumn("code CIPAN AEE", RefEspece.PROPERTY_CODE__CIPAN__AEE);
        newMandatoryColumn("libellé CIPAN AEE", RefEspece.PROPERTY_LIBELLE__CIPAN__AEE);
        newMandatoryColumn("libellé destination BBCH", RefEspece.PROPERTY_LIBELLE_DESTINATION__BBCH);
        newMandatoryColumn("profil vegetatif BBCH", RefEspece.PROPERTY_PROFIL_VEGETATIF__BBCH);
        newMandatoryColumn("commentaires", RefEspece.PROPERTY_COMMENTAIRES);
        newMandatoryColumn("code espèce EPPO", RefEspece.PROPERTY_CODE_ESPECE__EPPO);
        newMandatoryColumn("Genre", RefEspece.PROPERTY_GENRE);
        newMandatoryColumn("Espèce", RefEspece.PROPERTY_ESPECE);
        newMandatoryColumn("code GNIS", RefEspece.PROPERTY_CODE__GNIS);
        newMandatoryColumn("Num groupe GNIS", RefEspece.PROPERTY_NUM_GROUPE__GNIS);
        newMandatoryColumn("nom GNIS", RefEspece.PROPERTY_NOM__GNIS);
        newMandatoryColumn("nom latin GNIS", RefEspece.PROPERTY_NOM_LATIN__GNIS);
        newMandatoryColumn("id_culture_acta", RefEspece.PROPERTY_ID_CULTURE_ACTA, INT_PARSER);
        newMandatoryColumn("nom_culture_ACTA", RefEspece.PROPERTY_NOM_CULTURE_ACTA);
        newMandatoryColumn("remarque_culture_ACTA", RefEspece.PROPERTY_REMARQUE_CULTURE_ACTA);
        newMandatoryColumn("code_culture_maa", RefEspeceDto.PROPERTY_CODE_CULTURE_MAA);
        newMandatoryColumn("culture_maa", RefEspeceDto.PROPERTY_CULTURE_MAA);
        newOptionalColumn("typocan_espece", RefEspeceDto.PROPERTY_TYPOCAN_ESPECE);
        newOptionalColumn("typocan_espece_maraich", RefEspeceDto.PROPERTY_TYPOCAN_ESPECE_MARAICH);
        newMandatoryColumn("Source", RefEspece.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefEspece.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }
    
    @Override
    public Iterable<ExportableColumn<RefEspeceDto, Object>> getColumnsForExport() {
        ModelBuilder<RefEspece> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("topiaid", RefEspece.PROPERTY_TOPIA_ID);
        modelBuilder.newColumnForExport("code catégorie de cultures", RefEspece.PROPERTY_CODE_CATEGORIE_DE_CULTURES);
        modelBuilder.newColumnForExport("libellé catégorie de cultures", RefEspece.PROPERTY_LIBELLE_CATEGORIE_DE_CULTURES);
        modelBuilder.newColumnForExport("commentaire", RefEspece.PROPERTY_COMMENTAIRE);
        modelBuilder.newColumnForExport("code espèce botanique", RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE);
        modelBuilder.newColumnForExport("libellé espèce botanique", RefEspece.PROPERTY_LIBELLE_ESPECE_BOTANIQUE);
        modelBuilder.newColumnForExport("code qualifiant AEE", RefEspece.PROPERTY_CODE_QUALIFIANT__AEE);
        modelBuilder.newColumnForExport("libellé qualifiant AEE", RefEspece.PROPERTY_LIBELLE_QUALIFIANT__AEE);
        modelBuilder.newColumnForExport("code type saisonnier AEE", RefEspece.PROPERTY_CODE_TYPE_SAISONNIER__AEE);
        modelBuilder.newColumnForExport("libellé type saisonnier AEE", RefEspece.PROPERTY_LIBELLE_TYPE_SAISONNIER__AEE);
        modelBuilder.newColumnForExport("code destination AEE", RefEspece.PROPERTY_CODE_DESTINATION__AEE);
        modelBuilder.newColumnForExport("libellé destination AEE", RefEspece.PROPERTY_LIBELLE_DESTINATION__AEE);
        modelBuilder.newColumnForExport("code CIPAN AEE", RefEspece.PROPERTY_CODE__CIPAN__AEE);
        modelBuilder.newColumnForExport("libellé CIPAN AEE", RefEspece.PROPERTY_LIBELLE__CIPAN__AEE);
        modelBuilder.newColumnForExport("libellé destination BBCH", RefEspece.PROPERTY_LIBELLE_DESTINATION__BBCH);
        modelBuilder.newColumnForExport("profil vegetatif BBCH", RefEspece.PROPERTY_PROFIL_VEGETATIF__BBCH);
        modelBuilder.newColumnForExport("commentaires", RefEspece.PROPERTY_COMMENTAIRES);
        modelBuilder.newColumnForExport("code espèce EPPO", RefEspece.PROPERTY_CODE_ESPECE__EPPO);
        modelBuilder.newColumnForExport("Genre", RefEspece.PROPERTY_GENRE);
        modelBuilder.newColumnForExport("Espèce", RefEspece.PROPERTY_ESPECE);
        modelBuilder.newColumnForExport("code GNIS", RefEspece.PROPERTY_CODE__GNIS);
        modelBuilder.newColumnForExport("Num groupe GNIS", RefEspece.PROPERTY_NUM_GROUPE__GNIS);
        modelBuilder.newColumnForExport("nom GNIS", RefEspece.PROPERTY_NOM__GNIS);
        modelBuilder.newColumnForExport("nom latin GNIS", RefEspece.PROPERTY_NOM_LATIN__GNIS);
        modelBuilder.newColumnForExport("Source", RefEspece.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport("id_culture_acta", RefEspece.PROPERTY_ID_CULTURE_ACTA, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("nom_culture_ACTA", RefEspece.PROPERTY_NOM_CULTURE_ACTA);
        modelBuilder.newColumnForExport("remarque_culture_ACTA", RefEspece.PROPERTY_REMARQUE_CULTURE_ACTA);
        modelBuilder.newColumnForExport("code_culture_maa", RefEspeceDto.PROPERTY_CODE_CULTURE_MAA);
        modelBuilder.newColumnForExport("culture_maa", RefEspeceDto.PROPERTY_CULTURE_MAA);
        modelBuilder.newColumnForExport("typocan_espece", RefEspeceDto.PROPERTY_TYPOCAN_ESPECE);
        modelBuilder.newColumnForExport("typocan_espece_maraich", RefEspeceDto.PROPERTY_TYPOCAN_ESPECE_MARAICH);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefEspece.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefEspeceDto newEmptyInstance() {
        RefEspeceDto result = new RefEspeceDto(null);
        result.setActive(true);
        return result;
    }
}
