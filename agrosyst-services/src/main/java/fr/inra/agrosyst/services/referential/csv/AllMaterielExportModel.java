package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.MaterielType;
import java.util.Map;

/**
 * All materiel export model.
 * 
 * Columns:
 * <ul>
 * <li>Type materiel 1
 * <li>type materiel 2
 * <li>Type materiel 3
 * <li>Type materiel 4
 * <li>idtypemateriel
 * <li>idsoustypemateriel
 * </ul>
 * 
 * @author Eric Chatellier
 */
public class AllMaterielExportModel extends AbstractTranslatableExportModel<AllMaterielExport> {

    protected final Map<MaterielType, String> translatedMaterielTypes;

    public AllMaterielExportModel(Map<String, String> translations, Map<MaterielType, String> translatedMaterielTypes) {
        super(translations);
        this.translatedMaterielTypes = translatedMaterielTypes;
        newColumnForExport(AllMaterielExport.PROPERTY_CATEGORY, translatedMaterielTypes::get);
        newColumnForExport(AllMaterielExport.PROPERTY_TYPE_MATERIEL1);
        newColumnForExport(AllMaterielExport.PROPERTY_TYPE_MATERIEL2);
        newColumnForExport(AllMaterielExport.PROPERTY_TYPE_MATERIEL3);
        newColumnForExport(AllMaterielExport.PROPERTY_TYPE_MATERIEL4);
        newColumnForExport(AllMaterielExport.PROPERTY_MANUAL_TOOL, getTranslated_O_N_Formatter());
    }

}
