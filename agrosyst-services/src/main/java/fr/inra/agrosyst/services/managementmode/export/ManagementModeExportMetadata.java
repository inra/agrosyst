package fr.inra.agrosyst.services.managementmode.export;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.managementmode.CategoryObjective;
import fr.inra.agrosyst.api.entities.managementmode.DamageType;
import fr.inra.agrosyst.api.entities.managementmode.ImplementationType;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeCategory;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.managementmode.StrategyType;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.export.ExportModel;
import lombok.Getter;
import lombok.Setter;

public class ManagementModeExportMetadata {

    @Getter
    public static class CommonBean {

        final ManagementModeCategory category;
        final String department;
        final String networkNames;
        final String growingSystemName;
        final String growingSystemTypeAgriculture;
        final String dephyNb;
        final String growingPlanName;
        final String domainName;
        final Integer campaign;

        /**
         * Constructeur avec tous les champs pour la première instanciation
         */
        public CommonBean(ManagementModeCategory category,
                          String department,
                          String networkNames,
                          String growingSystemName,
                          String growingSystemTypeAgriculture,
                          String dephyNb,
                          String growingPlanName,
                          String domainName,
                          Integer campaign) {
            this.category = category;
            this.department = department;
            this.networkNames = networkNames;
            this.growingSystemName = growingSystemName;
            this.growingSystemTypeAgriculture = growingSystemTypeAgriculture;
            this.dephyNb = dephyNb;
            this.growingPlanName = growingPlanName;
            this.domainName = domainName;
            this.campaign = campaign;
        }

        /**
         * Constructeur par recopie pour faciliter le travail des sous modèles
         */
        public CommonBean(CommonBean source) {
            this(
                    source.category,
                    source.department,
                    source.networkNames,
                    source.growingSystemName,
                    source.growingSystemTypeAgriculture,
                    source.dephyNb,
                    source.growingPlanName,
                    source.domainName,
                    source.campaign
            );
        }

    }

    public abstract static class CommonModel<T extends CommonBean> extends ExportModel<T> {
        public CommonModel() {
            newColumn("Modèle décisionnel", CommonBean::getCategory, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
            newColumn("Département", CommonBean::getDepartment);
            newColumn("Réseau(x)", CommonBean::getNetworkNames);
            newColumn("Système De Culture", CommonBean::getGrowingSystemName);
            newColumn("Type de conduite", CommonBean::getGrowingSystemTypeAgriculture);
            newColumn("N° DEPHY", CommonBean::getDephyNb);
            newColumn("Dispositif", CommonBean::getGrowingPlanName);
            newColumn("Domaine", CommonBean::getDomainName);
            newColumn("Campagne", CommonBean::getCampaign);
        }
    }

    @Getter
    @Setter
    public static class MainBean extends CommonBean {

        String mainChanges;
        String changeReason;
        String changeReasonFromPlanned;
        String mainChangesFromPlanned;

        public MainBean(CommonBean source) {
            super(source);
        }

    }

    public static class MailModel extends CommonModel<MainBean> {
        @Override
        public String getTitle() {
            return "Généralités";
        }

        public MailModel() {
            super();
            newColumn("Principaux changements", MainBean::getMainChanges);
            newColumn("Motifs de changement", MainBean::getChangeReason);
            newColumn("Principaux changements depuis le prévu", MainBean::getChangeReasonFromPlanned);
            newColumn("Motifs de changement depuis le prévu", MainBean::getMainChangesFromPlanned);
        }
    }

    @Getter
    @Setter
    public static class SectionBean extends CommonBean {

        SectionType sectionType;// colonne J "Type de rubrique"
        BioAgressorType bioAgressorType;//"Type de bio-agresseur"
        String groupeCible;//"Groupe cible"
        String bioAgressor;//"Bio-agresseur considéré"
        boolean sdcConcerneBioAgresseur;//SDC concerné par le bioagresseur
        String agronomicObjective;// "Objectifs agronomiques"
        CategoryObjective categoryObjective;//"Catégorie d'objectif"
        String expectedResult;// "Résultats attendus"

        DamageType damageType;// "Type de dommage"

        ImplementationType implementationType;// "Type de stratégie"
        StrategyType strategyType;// "Type de levier"
        String strategyLever;// "Levier"
        String explanation;// "Explication"
        String crops;// "Cultures"

        public SectionBean(CommonBean source) {
            super(source);
        }

    }

    public static class SectionModel extends CommonModel<SectionBean> {
        @Override
        public String getTitle() {
            return "Rubriques";
        }

        public SectionModel() {
            super();
            // section
            newColumn("Type de rubrique", SectionBean::getSectionType, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
            newColumn("Type de bio-agresseur", SectionBean::getBioAgressorType, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
            newColumn("Groupe cible", SectionBean::getGroupeCible);
            newColumn("Bio-agresseur considéré", SectionBean::getBioAgressor);
            newColumn("SDC non-concerné par le bioagresseur", SectionBean::isSdcConcerneBioAgresseur);
            newColumn("Objectifs agronomiques", SectionBean::getAgronomicObjective);
            newColumn("Catégorie d'objectif", SectionBean::getCategoryObjective, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
            newColumn("Résultats attendus", SectionBean::getExpectedResult);

            newColumn("Type de dommage", SectionBean::getDamageType, AgrosystI18nService::getEnumTraductionWithDefaultLocale);

            // strategies
            newColumn("Type de stratégie", SectionBean::getImplementationType, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
            newColumn("Type de levier", SectionBean::getStrategyType, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
            newColumn("Levier", SectionBean::getStrategyLever);
            newColumn("Explication", SectionBean::getExplanation);
            newColumn("Cultures", SectionBean::getCrops);
        }
    }
}
