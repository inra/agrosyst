package fr.inra.agrosyst.services;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.ServiceFactory;
import fr.inra.agrosyst.api.services.action.ActionService;
import fr.inra.agrosyst.api.services.async.AsyncService;
import fr.inra.agrosyst.api.services.common.AttachmentService;
import fr.inra.agrosyst.api.services.common.DephygraphConnectorService;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.common.RefInputPriceService;
import fr.inra.agrosyst.api.services.context.NavigationContextService;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.GroundService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.edaplos.EdaplosService;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.generic.GenericEntityService;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.history.MessageService;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.api.services.measurement.MeasurementService;
import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotService;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.api.services.referential.ExportService;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.report.ReportService;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.api.services.security.AuthenticationService;
import fr.inra.agrosyst.api.services.security.AuthorizationService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.security.TrackerService;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.services.action.ActionServiceImpl;
import fr.inra.agrosyst.services.async.AsyncServiceImpl;
import fr.inra.agrosyst.services.common.AttachmentServiceImpl;
import fr.inra.agrosyst.services.common.CacheAware;
import fr.inra.agrosyst.services.common.CacheService;
import fr.inra.agrosyst.services.common.DephygraphConnectorServiceImpl;
import fr.inra.agrosyst.services.common.InputPriceServiceImpl;
import fr.inra.agrosyst.services.common.PricesServiceImpl;
import fr.inra.agrosyst.services.common.RefInputPriceServiceImpl;
import fr.inra.agrosyst.services.context.NavigationContextServiceImpl;
import fr.inra.agrosyst.services.domain.DomainServiceImpl;
import fr.inra.agrosyst.services.domain.GroundServiceImpl;
import fr.inra.agrosyst.services.domain.inputStock.DomainInputStockUnitServiceImpl;
import fr.inra.agrosyst.services.edaplos.EdaplosServiceImpl;
import fr.inra.agrosyst.services.effective.EffectiveCropCycleServiceImpl;
import fr.inra.agrosyst.services.generic.GenericEntityServiceImpl;
import fr.inra.agrosyst.services.growingplan.GrowingPlanServiceImpl;
import fr.inra.agrosyst.services.growingsystem.GrowingSystemServiceImpl;
import fr.inra.agrosyst.services.history.MessageServiceImpl;
import fr.inra.agrosyst.services.managementmode.ManagementModeServiceImpl;
import fr.inra.agrosyst.services.measurement.MeasurementServiceImpl;
import fr.inra.agrosyst.services.network.NetworkServiceImpl;
import fr.inra.agrosyst.services.performance.PerformanceServiceImpl;
import fr.inra.agrosyst.services.plot.PlotServiceImpl;
import fr.inra.agrosyst.services.practiced.PracticedPlotServiceImpl;
import fr.inra.agrosyst.services.practiced.PracticedSystemServiceImpl;
import fr.inra.agrosyst.services.referential.ExportServiceImpl;
import fr.inra.agrosyst.services.referential.ImportServiceImpl;
import fr.inra.agrosyst.services.referential.ReferentialServiceImpl;
import fr.inra.agrosyst.services.report.ReportServiceImpl;
import fr.inra.agrosyst.services.security.AnonymizeServiceImpl;
import fr.inra.agrosyst.services.security.AuthenticationServiceImpl;
import fr.inra.agrosyst.services.security.AuthorizationServiceImpl;
import fr.inra.agrosyst.services.security.BusinessAuthorizationServiceImpl;
import fr.inra.agrosyst.services.security.TrackerServiceImpl;
import fr.inra.agrosyst.services.users.UserServiceImpl;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaDaoSupplier;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.BeanUtil;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class DefaultServiceFactory implements ServiceFactory {

    private static final Log LOGGER = LogFactory.getLog(DefaultServiceFactory.class);

    private static final Map<Class<? extends AgrosystService>, Class<? extends AgrosystService>> INTERFACE_TO_IMPL = new HashMap<>();

    static {
        INTERFACE_TO_IMPL.put(ActionService.class, ActionServiceImpl.class);
        INTERFACE_TO_IMPL.put(AnonymizeService.class, AnonymizeServiceImpl.class);
        INTERFACE_TO_IMPL.put(AsyncService.class, AsyncServiceImpl.class);
        INTERFACE_TO_IMPL.put(AttachmentService.class, AttachmentServiceImpl.class);
        INTERFACE_TO_IMPL.put(AuthenticationService.class, AuthenticationServiceImpl.class);
        INTERFACE_TO_IMPL.put(AuthorizationService.class, AuthorizationServiceImpl.class);
        INTERFACE_TO_IMPL.put(BusinessAuthorizationService.class, BusinessAuthorizationServiceImpl.class);
        INTERFACE_TO_IMPL.put(DephygraphConnectorService.class, DephygraphConnectorServiceImpl.class);
        INTERFACE_TO_IMPL.put(DomainService.class, DomainServiceImpl.class);
        INTERFACE_TO_IMPL.put(DomainInputStockUnitService.class, DomainInputStockUnitServiceImpl.class);
        INTERFACE_TO_IMPL.put(EdaplosService.class, EdaplosServiceImpl.class);
        INTERFACE_TO_IMPL.put(EffectiveCropCycleService.class, EffectiveCropCycleServiceImpl.class);
        INTERFACE_TO_IMPL.put(ExportService.class, ExportServiceImpl.class);
        INTERFACE_TO_IMPL.put(GenericEntityService.class, GenericEntityServiceImpl.class);
        INTERFACE_TO_IMPL.put(GroundService.class, GroundServiceImpl.class);
        INTERFACE_TO_IMPL.put(GrowingPlanService.class, GrowingPlanServiceImpl.class);
        INTERFACE_TO_IMPL.put(GrowingSystemService.class, GrowingSystemServiceImpl.class);
        INTERFACE_TO_IMPL.put(ImportService.class, ImportServiceImpl.class);
        INTERFACE_TO_IMPL.put(InputPriceService.class, InputPriceServiceImpl.class);
        INTERFACE_TO_IMPL.put(ManagementModeService.class, ManagementModeServiceImpl.class);
        INTERFACE_TO_IMPL.put(MeasurementService.class, MeasurementServiceImpl.class);
        INTERFACE_TO_IMPL.put(MessageService.class, MessageServiceImpl.class);
        INTERFACE_TO_IMPL.put(NavigationContextService.class, NavigationContextServiceImpl.class);
        INTERFACE_TO_IMPL.put(NetworkService.class, NetworkServiceImpl.class);
        INTERFACE_TO_IMPL.put(PerformanceService.class, PerformanceServiceImpl.class);// TODO replace with PerformanceServiceImpl
        INTERFACE_TO_IMPL.put(PlotService.class, PlotServiceImpl.class);
        INTERFACE_TO_IMPL.put(PracticedPlotService.class, PracticedPlotServiceImpl.class);
        INTERFACE_TO_IMPL.put(PracticedSystemService.class, PracticedSystemServiceImpl.class);
        INTERFACE_TO_IMPL.put(PricesService.class, PricesServiceImpl.class);
        INTERFACE_TO_IMPL.put(ReferentialService.class, ReferentialServiceImpl.class);
        INTERFACE_TO_IMPL.put(RefInputPriceService.class, RefInputPriceServiceImpl.class);
        INTERFACE_TO_IMPL.put(ReportService.class, ReportServiceImpl.class);
        INTERFACE_TO_IMPL.put(TrackerService.class, TrackerServiceImpl.class);
        INTERFACE_TO_IMPL.put(UserService.class, UserServiceImpl.class);
    }
    
    private static final MultiKeyMap<Class<? extends AgrosystService>, Class<? extends AgrosystService>> INTERFACE_TO_ABSTRACT_TO_IMPL = new MultiKeyMap<>();
    
    static {
        INTERFACE_TO_ABSTRACT_TO_IMPL.put(PerformanceService.class, PerformanceServiceImpl.class, PerformanceServiceImpl.class);
        INTERFACE_TO_ABSTRACT_TO_IMPL.put(PerformanceService.class, fr.inra.agrosyst.services.performance.PerformanceServiceImpl.class, fr.inra.agrosyst.services.performance.PerformanceServiceImpl.class);
    }
    protected static final String LEGACY_DAO_SUFFIX = "DAO";
    protected static final String DAO_SUFFIX = "TopiaDao";

    protected final Map<Class<? extends AgrosystService>, AgrosystService> servicesCache = Maps.newConcurrentMap(); // TODO AThimel 04/10/13 Is it possible to improve generics ?
    protected final Map<Class<? extends TopiaDao>, TopiaDao> daoCache = Maps.newConcurrentMap(); // TODO AThimel 04/10/13 Is it possible to improve generics ?

    protected ServiceContext serviceContext;

    public DefaultServiceFactory(ServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public ServiceContext getServiceContext() {
        return serviceContext;
    }

    @Override
    public <E extends AgrosystService> E newService(Class<E> clazz) {
        Preconditions.checkNotNull(clazz);

        long start = System.currentTimeMillis();
        int nbServicesBefore = servicesCache.size();
        int nbDaoBefore = daoCache.size();

        E service = findOrCreateService(clazz);

        if (LOGGER.isTraceEnabled()) {
            long duration = System.currentTimeMillis() - start;
            int nbServicesCreated = servicesCache.size() - nbServicesBefore;
            int nbDaoCreated = daoCache.size() - nbDaoBefore;
            String format = "Service '%s' created in %dms. %d new services and %d new dao has been instantiated";
            String message = String.format(format, clazz.getSimpleName(), duration, nbServicesCreated, nbDaoCreated);
            LOGGER.trace(message);
        }

        return service;
    }
    
    @Override
    public <E extends AgrosystService, F extends AgrosystService> F newExpectedService(Class<E> clazz, Class<F> expectedClazz) {
        Preconditions.checkNotNull(clazz);
        Preconditions.checkNotNull(expectedClazz);
        
        long start = System.currentTimeMillis();
        int nbServicesBefore = servicesCache.size();
        int nbDaoBefore = daoCache.size();
        
        F service = findOrCreateExpectedService(clazz, expectedClazz);
        
        if (LOGGER.isTraceEnabled()) {
            long duration = System.currentTimeMillis() - start;
            int nbServicesCreated = servicesCache.size() - nbServicesBefore;
            int nbDaoCreated = daoCache.size() - nbDaoBefore;
            String format = "Service '%s' created in %dms. %d new services and %d new dao has been instantiated";
            String message = String.format(format, clazz.getSimpleName(), duration, nbServicesCreated, nbDaoCreated);
            LOGGER.trace(message);
        }
        
        return service;
    }

    protected <E extends AgrosystService> E findOrCreateService(Class<E> clazz) {
        // Load from cache
        E service = (E) servicesCache.get(clazz);

        if (service == null) {
            // instantiate service using empty constructor
            try {
                // TODO AThimel 14/06/13 Remplacer la map par quelque chose de viable
                Class<? extends AgrosystService> implClazz = INTERFACE_TO_IMPL.get(clazz);
                if (implClazz == null) {
                    implClazz = clazz;
                }

                service = (E) implClazz.getConstructor().newInstance();
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException ie) {
                throw new AgrosystTechnicalException("Unable to instantiate service for class " + clazz.getName(), ie);
            }

            // Put instance in cache before init in case of some loop between the services
            servicesCache.put(clazz, service);

            // init instance
            injectProperties(service);

            // If the instance created in a service, set its serviceContext
            if (service instanceof AbstractAgrosystService) {
                ((AbstractAgrosystService) service).setContext(serviceContext);
            }
        }

        return service;
    }
    
    protected <E extends AgrosystService, F extends AgrosystService> F findOrCreateExpectedService(Class<E> clazz, Class<F> expectedClazz) {
        // Load from cache
        F service = (F) servicesCache.get(expectedClazz);
        
        if (service == null) {
            // instantiate service using empty constructor
            try {
                // TODO AThimel 14/06/13 Remplacer la map par quelque chose de viable
                Class<? extends AgrosystService> implClazz = INTERFACE_TO_ABSTRACT_TO_IMPL.get(clazz, expectedClazz);
                if (implClazz == null) {
                    implClazz = clazz;
                }
        
                service = (F) implClazz.getConstructor().newInstance();
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException ie) {
                throw new AgrosystTechnicalException("Unable to instantiate service for class " + clazz.getName(), ie);
            }
    
            // Put instance in cache before init in case of some loop between the services
            servicesCache.put(expectedClazz, service);
    
            // init instance
            injectProperties(service);
    
            // If the instance created in a service, set its serviceContext
            if (service instanceof AbstractAgrosystService) {
                ((AbstractAgrosystService) service).setContext(serviceContext);
            }
        }
        
        return service;
    }

    protected <E> void injectProperties(E instance) {
        // Check if some services has to be injected
        Set<PropertyDescriptor> descriptors =
                BeanUtil.getDescriptors(
                        instance.getClass(),
                        BeanUtil.IS_WRITE_DESCRIPTOR);

        for (PropertyDescriptor propertyDescriptor : descriptors) {

            Class<?> propertyType = propertyDescriptor.getPropertyType();
            Object toInject = null;

            if (AgrosystService.class.isAssignableFrom(propertyType)) {
                Class<? extends AgrosystService> serviceClass = (Class<? extends AgrosystService>) propertyType;
                toInject = findOrCreateService(serviceClass);
            } else if (AgrosystServiceConfig.class.isAssignableFrom(propertyType)) {
                toInject = serviceContext.getConfig();
            } else if (TopiaDao.class.isAssignableFrom(propertyType)) {
                Class<? extends TopiaDao> daoType = (Class<? extends TopiaDao>) propertyType;
                toInject = getDaoInstance(daoType);
            }

            if (toInject != null) {
                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace("injecting " + toInject + " in instance " + instance);
                }

                try {
                    propertyDescriptor.getWriteMethod().invoke(instance, toInject);
                } catch (IllegalAccessException | InvocationTargetException iae) {
                    String message = String.format("Unable to inject '%s' in instance '%s'",
                            toInject.getClass().getName(), instance.getClass().getName());
                    throw new AgrosystTechnicalException(message, iae);
                }
            }
        }
    }

    protected <D extends TopiaDao> D getDaoInstance(Class<D> daoClass) {
        D toInject = (D) daoCache.get(daoClass);
        if (toInject == null) {
            String daoName = daoClass.getName();
            if (daoName.endsWith(LEGACY_DAO_SUFFIX) || daoName.endsWith(DAO_SUFFIX)) { // TODO AThimel 10/10/13 Improve this code
                TopiaDaoSupplier daoSupplier = serviceContext.getDaoSupplier();
                try {
                    String className = daoName.substring(0, daoName.length() - DAO_SUFFIX.length());
                    if (daoName.endsWith(LEGACY_DAO_SUFFIX)) {
                        if (LOGGER.isWarnEnabled()) {
                            LOGGER.warn("Legacy DAO detected: " + daoName);
                        }
                        className = daoName.substring(0, daoName.length() - LEGACY_DAO_SUFFIX.length());
                    }
                    Class<? extends TopiaEntity> aClass = (Class<TopiaEntity>) Class.forName(className);
                    toInject = (D) daoSupplier.getDao(aClass);
                    daoCache.put(daoClass, toInject);
                } catch (ClassNotFoundException e) {
                    if (LOGGER.isErrorEnabled()) {
                        LOGGER.error("An exception occurred", e);
                    }
                }
            } else {
                if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn("Unable to guess entity name : " + daoClass);
                }
            }
        }

        if (toInject instanceof CacheAware) {
            ((CacheAware)toInject).setCacheServiceSupplier(new Supplier<>() {
                private CacheService cacheService;
    
                @Override
                public CacheService get() {
                    if (cacheService == null) {
                        cacheService = newService(CacheService.class);
                    }
                    return cacheService;
                }
            });
        }

        return toInject;
    }

    @Override
    public <I> I newInstance(Class<I> clazz) {
        I instance;
        try {
            instance = clazz.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException ie) {
            throw new AgrosystTechnicalException("Unable to instantiate object for class " + clazz.getName(), ie);
        }

        // init instance
        injectProperties(instance);

        return instance;
    }

}
