package fr.inra.agrosyst.services.cron;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystTopiaApplicationContext;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.services.ServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Classe abstraite pour les Job Quartz. Cette classe permet à chaque job d'accéder au contexte applicatif par le biais
 * de la commande {@link #newService(Class)}.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.61
 */
public abstract class AbstractAgrosystScheduledJob implements Job {

    public static final String CRON_JOB_DATA_APPLICATION_CONTEXT_KEY = "applicationContext";

    /** L'instance est récupéré via le {@link JobDataMap} contenu dans le {@link JobExecutionContext} */
    private AgrosystTopiaApplicationContext applicationContext;

    /** N'est instancié que si le job en a réellement besoin */
    private ServiceContext serviceContext;

    protected ServiceContext getServiceContext() {
        if (serviceContext == null) {
            serviceContext = applicationContext.newServiceContext();
        }
        return serviceContext;
    }

    protected <E extends AgrosystService> E newService(Class<E> clazz) {
        E result = getServiceContext().newService(clazz);
        return result;
    }

    public void execute(JobExecutionContext context)
            throws JobExecutionException {

        Log log = LogFactory.getLog(getClass());

        try {
            JobDataMap jobDataMap = context.getMergedJobDataMap();
            applicationContext = (AgrosystTopiaApplicationContext) jobDataMap.get(CRON_JOB_DATA_APPLICATION_CONTEXT_KEY);

            long start = System.currentTimeMillis();
            run();
            if (log.isTraceEnabled()) {
                long end = System.currentTimeMillis();
                log.trace(String.format("Le job %s a pris %dms", getClass().getSimpleName(), end - start));
            }
        } catch (Exception eee) {
            if (log.isWarnEnabled()) {
                log.warn("Error during job run", eee);
            }
            throw new JobExecutionException("Unable to run job " + getClass().getSimpleName(), eee);
        } finally {
            // Si une transaction a été ouverte, on la ferme
            if (serviceContext != null) {
                try {
                    serviceContext.close();
                } catch (Exception eee) {
                    log.error("Unable to close ServiceContext", eee);
                }
            }
        }
    }

    /**
     * La méthode à surcharger. Elle est appelée à chaque exécution du job.
     */
    protected abstract void run();

}
