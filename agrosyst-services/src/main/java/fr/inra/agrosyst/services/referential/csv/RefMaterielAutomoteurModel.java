package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteurImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

import java.util.List;

/**
 * Automoteur import model.
 * 
 * Columns:
 * <ul>
 * <li>Type materiel 1
 * <li>type materiel 2
 * <li>Type materiel 3
 * <li>Type materiel 4
 * <li>idtypemateriel
 * <li>idsoustypemateriel
 * <li>commentaire sur materiel
 * <li>Millésime
 * <li>Codetype
 * <li>Coderef
 * <li>prix neuf € unité
 * <li>prix moyen achat
 * <li>unité
 * <li>charges fixes €/an
 * <li>charges fixes €/unité de volume de travail annuel unité
 * <li>charges fixes €/unité de volume de travail annuel
 * <li>Réparations unité
 * <li>Réparations €/unité de travail annuel
 * <li>carburant cout unité
 * <li>carburant €/unité de travail
 * <li>lubrifiant cout unité
 * <li>lubrifiant €/unité de travail
 * <li>coût total unité
 * <li>coût total € / unité de travail annuel
 * <li>puissance ch ISO unité
 * <li>puissance ch ISO
 * <li>volume carter huile moteur unité
 * <li>volume carter huile moteur
 * <li>performance unité
 * <li>performance
 * <li>performance coût total unité
 * <li>performance coût total €
 * <li>donnees amortissement 1
 * <li>donnees amortissement 2
 * <li>données taux de charge moteur
 * <li>donnees transport 1 unite
 * <li>donnees transport 1
 * <li>donnees transport 2 unite
 * <li>donnees transport 2
 * <li>donnees transport 3 unite
 * <li>donnees transport 3
 * <li>codeEDI
 * <li>source
 * </ul>
 * 
 * @author Eric Chatellier
 */
public class RefMaterielAutomoteurModel extends AbstractAgrosystModel<RefMaterielAutomoteur> implements ExportModel<RefMaterielAutomoteur> {

    public RefMaterielAutomoteurModel() {
        super(CSV_SEPARATOR);
    }

    @Override
    public void pushCsvHeaderNames(List<String> headerNames) {
        super.pushCsvHeaderNames(headerNames);

        newMandatoryColumn("Type materiel 1", RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL1, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("type materiel 2", RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL2, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("Type materiel 3", RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL3, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("Type materiel 4", RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL4, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("idtypemateriel", RefMaterielAutomoteur.PROPERTY_IDTYPEMATERIEL);
        newMandatoryColumn("idsoustypemateriel", RefMaterielAutomoteur.PROPERTY_IDSOUSTYPEMATERIEL);
        newMandatoryColumn("commentaire sur materiel", RefMaterielAutomoteur.PROPERTY_COMMENTAIRE_SUR_MATERIEL);
        newMandatoryColumn("Millésime", RefMaterielAutomoteur.PROPERTY_MILLESIME, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("codetype", RefMaterielAutomoteur.PROPERTY_CODETYPE);
        newMandatoryColumn("Coderef", RefMaterielAutomoteur.PROPERTY_CODE_REF);
        newMandatoryColumn("prix neuf € unité", RefMaterielAutomoteur.PROPERTY_PRIX_NEUF_UNITE);
        newMandatoryColumn("prix moyen achat", RefMaterielAutomoteur.PROPERTY_PRIX_MOYEN_ACHAT, DOUBLE_PARSER);
        newMandatoryColumn("unité", RefMaterielAutomoteur.PROPERTY_UNITE);
        newMandatoryColumn("unité / an", RefMaterielAutomoteur.PROPERTY_UNITE_PAR_AN, DOUBLE_PARSER);
        newMandatoryColumn("charges fixes annuelle unité", RefMaterielAutomoteur.PROPERTY_CHARGES_FIXES_ANNUELLE_UNITE);
        newMandatoryColumn("charges fixes €/an", RefMaterielAutomoteur.PROPERTY_CHARGES_FIXES_PAR_AN, DOUBLE_PARSER);
        newMandatoryColumn("charges fixes €/unité de volume de travail annuel unité", RefMaterielAutomoteur.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL_UNITE);
        newMandatoryColumn("charges fixes €/unité de volume de travail annuel", RefMaterielAutomoteur.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL, DOUBLE_PARSER);
        newMandatoryColumn("Réparations unité", RefMaterielAutomoteur.PROPERTY_REPARATIONS_UNITE);
        newMandatoryColumn("Réparations €/unité de travail annuel", RefMaterielAutomoteur.PROPERTY_REPARATIONS_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_PARSER);
        newMandatoryColumn("carburant cout unité", RefMaterielAutomoteur.PROPERTY_CARBURANT_COUT_UNITE);
        newMandatoryColumn("carburant €/unité de travail", RefMaterielAutomoteur.PROPERTY_CARBURANT_PAR_UNITE_DE_TRAVAIL, DOUBLE_PARSER);
        newMandatoryColumn("lubrifiant cout unité", RefMaterielAutomoteur.PROPERTY_LUBRIFIANT_COUT_UNITE);
        newMandatoryColumn("lubrifiant €/unité de travail", RefMaterielAutomoteur.PROPERTY_LUBRIFIANT_PAR_UNITE_DE_TRAVAIL, DOUBLE_PARSER);
        newMandatoryColumn("coût total unité", RefMaterielAutomoteur.PROPERTY_COUT_TOTAL_UNITE);

        newMandatoryColumn("coût total AVEC CARBURANT € / unité de travail annuel", RefMaterielAutomoteur.PROPERTY_COUT_TOTAL_AVEC_CARBURANT_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_PARSER);
        newMandatoryColumn("coût total SANS CARBURANT € / unité de travail annuel", RefMaterielAutomoteur.PROPERTY_COUT_TOTAL_SANS_CARBURANT_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_PARSER);

        newMandatoryColumn("puissance ch ISO unité", RefMaterielAutomoteur.PROPERTY_PUISSANCE_CH_ISOUNITE);
        newMandatoryColumn("puissance ch ISO", RefMaterielAutomoteur.PROPERTY_PUISSANCE_CH_ISO, DOUBLE_PARSER);
        newMandatoryColumn("volume carter huile moteur unité", RefMaterielAutomoteur.PROPERTY_VOLUME_CARTER_HUILE_MOTEUR_UNITE);
        newMandatoryColumn("volume carter huile moteur", RefMaterielAutomoteur.PROPERTY_VOLUME_CARTER_HUILE_MOTEUR, DOUBLE_PARSER);
        newMandatoryColumn("performance unité", RefMaterielAutomoteur.PROPERTY_PERFORMANCE_UNITE, AGROSYST_MATERIEL_WORK_RATE_UNIT_PARSER);
        newMandatoryColumn("performance", RefMaterielAutomoteur.PROPERTY_PERFORMANCE, DOUBLE_PARSER);
        newMandatoryColumn("performance coût total unité", RefMaterielAutomoteur.PROPERTY_PERFORMANCE_COUT_TOTAL_UNITE);
        
        if (hasHeader("performance coût total €")) {
            newMandatoryColumn("performance coût total €", RefMaterielAutomoteur.PROPERTY_PERFORMANCE_COUT_TOTAL_AVEC_CARBURANT_PAR_H, DOUBLE_PARSER);
        } else {
            newMandatoryColumn("performance coût total AVEC CARBURANT €/h", RefMaterielAutomoteur.PROPERTY_PERFORMANCE_COUT_TOTAL_AVEC_CARBURANT_PAR_H, DOUBLE_PARSER);
            newMandatoryColumn("performance coût total SANS CARBURANT €/h", RefMaterielAutomoteur.PROPERTY_PERFORMANCE_COUT_TOTAL_SANS_CARBURANT_PAR_H, DOUBLE_PARSER);
        }

        newMandatoryColumn("donnees amortissement 1", RefMaterielAutomoteur.PROPERTY_DONNEES_AMORTISSEMENT1, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("donnees amortissement 2", RefMaterielAutomoteur.PROPERTY_DONNEES_AMORTISSEMENT2, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("données taux de charge moteur", RefMaterielAutomoteur.PROPERTY_DONNEES_TAUX_DE_CHARGE_MOTEUR, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("donnees transport 1 unite", RefMaterielAutomoteur.PROPERTY_DONNEES_TRANSPORT1_UNITE);
        newMandatoryColumn("donnees transport 1", RefMaterielAutomoteur.PROPERTY_DONNEES_TRANSPORT1);
        newMandatoryColumn("donnees transport 2 unite", RefMaterielAutomoteur.PROPERTY_DONNEES_TRANSPORT2_UNITE);
        newMandatoryColumn("donnees transport 2", RefMaterielAutomoteur.PROPERTY_DONNEES_TRANSPORT2);
        newMandatoryColumn("donnees transport 3 unite", RefMaterielAutomoteur.PROPERTY_DONNEES_TRANSPORT3_UNITE);
        newMandatoryColumn("donnees transport 3", RefMaterielAutomoteur.PROPERTY_DONNEES_TRANSPORT3);
        newMandatoryColumn("code materiel GES'TIM", RefMaterielAutomoteur.PROPERTY_CODE_MATERIEL__GESTIM);
        newMandatoryColumn("masse (kg)", RefMaterielAutomoteur.PROPERTY_MASSE, DOUBLE_PARSER);
        newMandatoryColumn("durée de vie théorique -obsolescence constructeur- (années)", RefMaterielAutomoteur.PROPERTY_DUREE_VIE_THEORIQUE, INT_PARSER);
        newMandatoryColumn("Code EDI", RefMaterielAutomoteur.PROPERTY_CODE_EDI);
        newMandatoryColumn("source", RefMaterielAutomoteur.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefMaterielAutomoteur.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefMaterielAutomoteur, Object>> getColumnsForExport() {
        ModelBuilder<RefMaterielAutomoteur> modelBuilder = new ModelBuilder<>();

        modelBuilder.newColumnForExport("Type materiel 1", RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL1);
        modelBuilder.newColumnForExport("type materiel 2", RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL2);
        modelBuilder.newColumnForExport("Type materiel 3", RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL3);
        modelBuilder.newColumnForExport("Type materiel 4", RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL4);
        modelBuilder.newColumnForExport("idtypemateriel", RefMaterielAutomoteur.PROPERTY_IDTYPEMATERIEL);
        modelBuilder.newColumnForExport("idsoustypemateriel", RefMaterielAutomoteur.PROPERTY_IDSOUSTYPEMATERIEL);
        modelBuilder.newColumnForExport("commentaire sur materiel", RefMaterielAutomoteur.PROPERTY_COMMENTAIRE_SUR_MATERIEL);
        modelBuilder.newColumnForExport("Millésime", RefMaterielAutomoteur.PROPERTY_MILLESIME, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("codetype", RefMaterielAutomoteur.PROPERTY_CODETYPE);
        modelBuilder.newColumnForExport("Coderef", RefMaterielAutomoteur.PROPERTY_CODE_REF);
        modelBuilder.newColumnForExport("prix neuf € unité", RefMaterielAutomoteur.PROPERTY_PRIX_NEUF_UNITE);
        modelBuilder.newColumnForExport("prix moyen achat", RefMaterielAutomoteur.PROPERTY_PRIX_MOYEN_ACHAT, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("unité", RefMaterielAutomoteur.PROPERTY_UNITE);
        modelBuilder.newColumnForExport("unité / an", RefMaterielAutomoteur.PROPERTY_UNITE_PAR_AN, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("charges fixes annuelle unité", RefMaterielAutomoteur.PROPERTY_CHARGES_FIXES_ANNUELLE_UNITE);
        modelBuilder.newColumnForExport("charges fixes €/an", RefMaterielAutomoteur.PROPERTY_CHARGES_FIXES_PAR_AN, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("charges fixes €/unité de volume de travail annuel unité", RefMaterielAutomoteur.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL_UNITE);
        modelBuilder.newColumnForExport("charges fixes €/unité de volume de travail annuel", RefMaterielAutomoteur.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Réparations unité", RefMaterielAutomoteur.PROPERTY_REPARATIONS_UNITE);
        modelBuilder.newColumnForExport("Réparations €/unité de travail annuel", RefMaterielAutomoteur.PROPERTY_REPARATIONS_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("carburant cout unité", RefMaterielAutomoteur.PROPERTY_CARBURANT_COUT_UNITE);
        modelBuilder.newColumnForExport("carburant €/unité de travail", RefMaterielAutomoteur.PROPERTY_CARBURANT_PAR_UNITE_DE_TRAVAIL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("lubrifiant cout unité", RefMaterielAutomoteur.PROPERTY_LUBRIFIANT_COUT_UNITE);
        modelBuilder.newColumnForExport("lubrifiant €/unité de travail", RefMaterielAutomoteur.PROPERTY_LUBRIFIANT_PAR_UNITE_DE_TRAVAIL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("coût total unité", RefMaterielAutomoteur.PROPERTY_COUT_TOTAL_UNITE);

        modelBuilder.newColumnForExport("coût total AVEC CARBURANT € / unité de travail annuel", RefMaterielAutomoteur.PROPERTY_COUT_TOTAL_AVEC_CARBURANT_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("coût total SANS CARBURANT € / unité de travail annuel", RefMaterielAutomoteur.PROPERTY_COUT_TOTAL_SANS_CARBURANT_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_FORMATTER);

        modelBuilder.newColumnForExport("puissance ch ISO unité", RefMaterielAutomoteur.PROPERTY_PUISSANCE_CH_ISOUNITE);
        modelBuilder.newColumnForExport("puissance ch ISO", RefMaterielAutomoteur.PROPERTY_PUISSANCE_CH_ISO, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("volume carter huile moteur unité", RefMaterielAutomoteur.PROPERTY_VOLUME_CARTER_HUILE_MOTEUR_UNITE);
        modelBuilder.newColumnForExport("volume carter huile moteur", RefMaterielAutomoteur.PROPERTY_VOLUME_CARTER_HUILE_MOTEUR, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("performance unité", RefMaterielAutomoteur.PROPERTY_PERFORMANCE_UNITE, AGROSYST_MATERIEL_WORK_RATE_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("performance", RefMaterielAutomoteur.PROPERTY_PERFORMANCE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("performance coût total unité", RefMaterielAutomoteur.PROPERTY_PERFORMANCE_COUT_TOTAL_UNITE);

        modelBuilder.newColumnForExport("performance coût total AVEC CARBURANT €/h", RefMaterielAutomoteur.PROPERTY_PERFORMANCE_COUT_TOTAL_AVEC_CARBURANT_PAR_H, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("performance coût total SANS CARBURANT €/h", RefMaterielAutomoteur.PROPERTY_PERFORMANCE_COUT_TOTAL_SANS_CARBURANT_PAR_H, DOUBLE_FORMATTER);

        modelBuilder.newColumnForExport("donnees amortissement 1", RefMaterielAutomoteur.PROPERTY_DONNEES_AMORTISSEMENT1, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("donnees amortissement 2", RefMaterielAutomoteur.PROPERTY_DONNEES_AMORTISSEMENT2, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("données taux de charge moteur", RefMaterielAutomoteur.PROPERTY_DONNEES_TAUX_DE_CHARGE_MOTEUR, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("donnees transport 1 unite", RefMaterielAutomoteur.PROPERTY_DONNEES_TRANSPORT1_UNITE);
        modelBuilder.newColumnForExport("donnees transport 1", RefMaterielAutomoteur.PROPERTY_DONNEES_TRANSPORT1);
        modelBuilder.newColumnForExport("donnees transport 2 unite", RefMaterielAutomoteur.PROPERTY_DONNEES_TRANSPORT2_UNITE);
        modelBuilder.newColumnForExport("donnees transport 2", RefMaterielAutomoteur.PROPERTY_DONNEES_TRANSPORT2);
        modelBuilder.newColumnForExport("donnees transport 3 unite", RefMaterielAutomoteur.PROPERTY_DONNEES_TRANSPORT3_UNITE);
        modelBuilder.newColumnForExport("donnees transport 3", RefMaterielAutomoteur.PROPERTY_DONNEES_TRANSPORT3);
        modelBuilder.newColumnForExport("code materiel GES'TIM", RefMaterielAutomoteur.PROPERTY_CODE_MATERIEL__GESTIM);
        modelBuilder.newColumnForExport("masse (kg)", RefMaterielAutomoteur.PROPERTY_MASSE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("durée de vie théorique -obsolescence constructeur- (années)", RefMaterielAutomoteur.PROPERTY_DUREE_VIE_THEORIQUE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Code EDI", RefMaterielAutomoteur.PROPERTY_CODE_EDI);
        modelBuilder.newColumnForExport("source", RefMaterielAutomoteur.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefMaterielAutomoteur.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefMaterielAutomoteur newEmptyInstance() {
        RefMaterielAutomoteur refMaterielAutomoteur = new RefMaterielAutomoteurImpl();
        refMaterielAutomoteur.setActive(true);
        return refMaterielAutomoteur;
    }

}
