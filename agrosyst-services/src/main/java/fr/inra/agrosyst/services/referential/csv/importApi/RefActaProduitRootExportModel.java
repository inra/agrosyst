package fr.inra.agrosyst.services.referential.csv.importApi;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCountryTopiaDao;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot;
import fr.inra.agrosyst.api.services.referential.ImportStatus;
import fr.inra.agrosyst.services.referential.csv.InternationalizationReferentialModel;
import fr.inra.agrosyst.services.referential.json.RefActaProduitRootExport;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by davidcosse on 05/01/16.
 */
public class RefActaProduitRootExportModel extends InternationalizationReferentialModel<RefActaProduitRootExport> implements ExportModel<RefActaProduitRootExport> {

    public RefActaProduitRootExportModel(RefCountryTopiaDao refCountryDao) {
        super(CSV_SEPARATOR);
        
        try(final Stream<RefCountry> refCountryStream = refCountryDao.streamAll()){
            refCountryByTrigram = refCountryStream.collect(
                    Collectors.toMap(refCountry -> refCountry.getTrigram().toLowerCase(), Function.identity()));
        }
        
        newMandatoryColumn("pays", RefActaProduitRoot.PROPERTY_REF_COUNTRY, TRIGRAM_PARSER);
        newMandatoryColumn("idProduit", RefActaProduitRoot.PROPERTY_ID_PRODUIT, EMPTY_TO_NULL);
        newMandatoryColumn("nomProduit", RefActaProduitRoot.PROPERTY_NOM_PRODUIT);
        newMandatoryColumn("amm", RefActaProduitRoot.PROPERTY_AMM, EMPTY_TO_NULL);
        newMandatoryColumn("ammUE", RefActaProduitRoot.PROPERTY_AMM_UE, EMPTY_TO_NULL);
        newMandatoryColumn("permis", RefActaProduitRoot.PROPERTY_PERMIS, EMPTY_TO_NULL);
        newMandatoryColumn(COLUMN_ACTIVE, RefActaProduitRoot.PROPERTY_ACTIVE, ACTIVE_PARSER);
        newMandatoryColumn("status", RefActaProduitRootExport.PROPERTY_STATUS, STATUS_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefActaProduitRootExport, Object>> getColumnsForExport() {
        ModelBuilder<RefActaProduitRoot> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("pays", RefActaProduitRoot.PROPERTY_REF_COUNTRY, TRIGRAM_FORMATTER);
        modelBuilder.newColumnForExport("idProduit", RefActaProduitRoot.PROPERTY_ID_PRODUIT);
        modelBuilder.newColumnForExport("nomProduit", RefActaProduitRoot.PROPERTY_NOM_PRODUIT);
        modelBuilder.newColumnForExport("amm", RefActaProduitRoot.PROPERTY_AMM);
        modelBuilder.newColumnForExport("ammUE", RefActaProduitRoot.PROPERTY_AMM_UE);
        modelBuilder.newColumnForExport("permis", RefActaProduitRoot.PROPERTY_PERMIS);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefActaProduitRoot.PROPERTY_ACTIVE, T_F_FORMATTER);
        modelBuilder.newColumnForExport("status", RefActaProduitRootExport.PROPERTY_STATUS, STATUS_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    protected static final ValueParser<ImportStatus> STATUS_PARSER = value -> {
        ImportStatus status = ImportStatus.valueOf(value);
        return status;
    };

    public static final ValueFormatter<ImportStatus> STATUS_FORMATTER = value -> {
        String status = value == null ? ImportStatus.REMOVED.name() : value.toString();
        return status;
    };

    @Override
    public RefActaProduitRootExport newEmptyInstance() {
        RefActaProduitRootExport refActaProduitRootExport = new RefActaProduitRootExport();
        refActaProduitRootExport.setActive(true);
        return refActaProduitRootExport;
    }
}
