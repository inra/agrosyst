package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefPotImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefPotModel extends AbstractAgrosystModel<RefPot> implements ExportModel<RefPot> {

    public RefPotModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("Caracteristique 1 (Volume)", RefPot.PROPERTY_CARACTERISTIC1);
        newMandatoryColumn("Volume (L)", RefPot.PROPERTY_VOLUME, DOUBLE_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefPot.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefPot, Object>> getColumnsForExport() {
        ModelBuilder<RefPot> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Caracteristique 1 (Volume)", RefPot.PROPERTY_CARACTERISTIC1);
        modelBuilder.newColumnForExport("Volume (L)", RefPot.PROPERTY_VOLUME, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefPot.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefPot newEmptyInstance() {
        RefPot entity = new RefPotImpl();
        entity.setActive(true);
        return entity;
    }
}
