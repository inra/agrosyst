package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import org.apache.commons.collections4.MultiValuedMap;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * #12313
 * On calcule la QSA totale (avec traitements de semences) pour toutes les substances actives retrouvées dans des AMM d'intérêt.
 * QSA CMR : le statut CMR des AMM est disponible dans le référentiel "Phrases de risque classes et mentions de danger par AMM" ; colonne I "CMR"
 */
public class IndicatorCMR extends QSAbyAMM_categories {
    public static final String COLUMN_NAME = "qsa_cmr";
    public static final String COLUMN_NAME_HTS = "qsa_cmr_hts";

    protected final String[] indicators = new String[]{
            "Indicator.label.qsa_cmr",        // QSA CMR
            "Indicator.label.qsa_cmr_hts",    // QSA CMR hors traitement de semence
    };

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME_HTS);
        return indicatorNameToColumnName;
    }

    @Override
    protected @NotNull List<RefCompositionSubstancesActivesParNumeroAMM> getQSA_Substance(
            Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> domainSubstancesByAmm,
            MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> allRefPhrasesRisqueEtClassesMentionDangerByAMM,
            String codeAmm) {

        boolean idConcerne = allRefPhrasesRisqueEtClassesMentionDangerByAMM.get(codeAmm)
                .stream()
                .anyMatch(RefPhrasesRisqueEtClassesMentionDangerParAMM::isCmr);

        if (idConcerne) {
            return domainSubstancesByAmm.get(codeAmm);
        }

        return new ArrayList<>();
    }

    @Override
    protected String[] getIndicators() {
        return indicators;
    }
}
