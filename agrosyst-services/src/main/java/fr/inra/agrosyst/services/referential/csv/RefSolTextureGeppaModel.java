package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppaImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Modele d'import du référentiel sol texture GEPPA:
 * 
 * <ul>
 * <li>abbreviation classes texturales GEPAA
 * <li>classes texturales GEPAA
 * <li>abbreviation INDIGO
 * <li>classe INDIGO
 * <li>Source
 * </ul>
 * 
 * @author Eric Chatellier
 */
public class RefSolTextureGeppaModel extends AbstractAgrosystModel<RefSolTextureGeppa> implements ExportModel<RefSolTextureGeppa> {

    public RefSolTextureGeppaModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("abbreviation classes texturales GEPAA", RefSolTextureGeppa.PROPERTY_ABBREVIATION_CLASSES_TEXTURALES__GEPAA);
        newMandatoryColumn("classes texturales GEPAA", RefSolTextureGeppa.PROPERTY_CLASSES_TEXTURALES__GEPAA);
        newMandatoryColumn("abbreviation INDIGO", RefSolTextureGeppa.PROPERTY_ABBREVIATION__INDIGO);
        newMandatoryColumn("classe INDIGO", RefSolTextureGeppa.PROPERTY_CLASSE__INDIGO);
        newMandatoryColumn("texture I-Phy", RefSolTextureGeppa.PROPERTY_TEXTURE_IPHY);
        newMandatoryColumn("Source", RefSolTextureGeppa.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefSolTextureGeppa.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefSolTextureGeppa, Object>> getColumnsForExport() {
        ModelBuilder<RefSolTextureGeppa> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("abbreviation classes texturales GEPAA", RefSolTextureGeppa.PROPERTY_ABBREVIATION_CLASSES_TEXTURALES__GEPAA);
        modelBuilder.newColumnForExport("classes texturales GEPAA", RefSolTextureGeppa.PROPERTY_CLASSES_TEXTURALES__GEPAA);
        modelBuilder.newColumnForExport("abbreviation INDIGO", RefSolTextureGeppa.PROPERTY_ABBREVIATION__INDIGO);
        modelBuilder.newColumnForExport("classe INDIGO", RefSolTextureGeppa.PROPERTY_CLASSE__INDIGO);
        modelBuilder.newColumnForExport("texture I-Phy", RefSolTextureGeppa.PROPERTY_TEXTURE_IPHY);
        modelBuilder.newColumnForExport("Source", RefSolTextureGeppa.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefSolTextureGeppa.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefSolTextureGeppa newEmptyInstance() {
        RefSolTextureGeppa refSolTextureGeppa = new RefSolTextureGeppaImpl();
        refSolTextureGeppa.setActive(true);
        return refSolTextureGeppa;
    }
}
