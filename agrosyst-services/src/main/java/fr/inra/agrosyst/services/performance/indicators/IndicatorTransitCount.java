package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.lang3.BooleanUtils;

import java.time.LocalDate;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Nombre de passages.

 * Présentation de l’indicateur
 * Le nombre de passages correspond au nombre d’interventions culturales, manuelles ou de traction
 * animale, faites sur un hectare. Il est exprimé par type d’intervention en passage ha-1. Toutes
 * les interventions sont concernées par cet indicateur.

 * Formule de calcul
 * Le nombre de passages est une donnée saisie par l’utilisateur. Par défaut de saisie, sa valeur
 * est 1 pour chaque intervention.
 *
 * @author Eric Chatellier
 */
public class IndicatorTransitCount extends AbstractIndicator {

    protected boolean detailedByMonth = true;
    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    protected String[] translatedMonth;
    
    public IndicatorTransitCount() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, "nombre_de_passages_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, "nombre_de_passages_detail_champs_non_renseig");
    }
    
    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        
        for (int i = 0; i <= NB_MONTH; i++) {
            String label = getIndicatorLabel(i);
            String dbColumnName;
            if (i < NB_MONTH) {// 12 months + sum
                dbColumnName = "nombre_de_passages_" + MONTHS_FOR_DB_COLUMNS[i];
            } else {
                dbColumnName = "nombre_de_passages";
            }
            indicatorNameToColumnName.put(label, dbColumnName);
        }
        return indicatorNameToColumnName;
    }
    
    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) &&
                displayed &&
                (detailedByMonth || i == NB_MONTH);// 12 months + sum;
    }
    
    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }
    
    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_SOCIO_TECHNIC);
    }
    
    @Override
    public String getIndicatorLabel(int i) {
        String result;
        if (0 <= i && i < NB_MONTH) {// 12 months + sum;
            result = l(locale, "Indicator.label.transitCountMonth", translatedMonth[i]);
        } else {
            result = l(locale, "Indicator.label.transitCount");
        }

        return result;
    }
    
    protected int getStartMonth(EffectiveIntervention intervention) {
        int startMonth = -1;

        LocalDate startDate = intervention.getStartInterventionDate();
        if (startDate == null) {
            return startMonth;
        }

        startMonth = startDate.getMonthValue() - 1;

        return startMonth;
    }
    
    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {
    
        EffectiveIntervention intervention = interventionContext.getIntervention();
        Double[] transitCounts;
        int transitCount = intervention.getTransitCount();
        transitCounts = newArray(13, 0.0);

        int month = getStartMonth(intervention);
        transitCounts[month] = (double) transitCount;
        transitCounts[NB_MONTH] = (double) transitCount; // sum

        return transitCounts;
    }
    
    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {
        // Can't be computed for practiced
        return null;
    }
    
    public void init(IndicatorFilter filter, String[] translatedMonth) {
        displayed = filter != null;
        detailedByMonth = displayed && BooleanUtils.isTrue(filter.getDetailedByMonth());
        this.translatedMonth = translatedMonth;
    }
}
