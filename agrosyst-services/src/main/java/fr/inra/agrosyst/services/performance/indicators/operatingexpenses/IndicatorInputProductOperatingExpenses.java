package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.OtherProductInputUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PotInputUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.PotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SubstrateInputUsage;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.UsagePerformanceResult;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import fr.inra.agrosyst.services.performance.indicators.IndicatorFieldsStatistics;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import static org.nuiton.i18n.I18n.l;

/**
 * @author David Cossé (cosse@codelutin.com)
 */
public abstract class IndicatorInputProductOperatingExpenses extends IndicatorFieldsStatistics {

    protected final boolean displayed;
    protected final boolean computeReal;
    protected final boolean computStandardised;

    protected boolean isDisplayed(int i) {
        return displayed && (i == 0 && computeReal) || (i == 1 && computStandardised);
    }

    public IndicatorInputProductOperatingExpenses(
            boolean displayed,
            boolean computeReal,
            boolean computStandardised,
            Locale locale) {
        super(locale);
        this.displayed = displayed;
        this.computeReal = computeReal;
        this.computStandardised = computStandardised;
    }

    protected Collection<RefInputUnitPriceUnitConverter> getInputUsageConversionRate(
            AbstractInputUsage inputUsage, Collection<RefInputUnitPriceUnitConverter> conversionRate0) {

        Collection<RefInputUnitPriceUnitConverter> conversionRate = CollectionUtils.emptyIfNull(conversionRate0);
        Collection<RefInputUnitPriceUnitConverter> converter;
        switch (inputUsage.getInputType()) {
            // #9313 Ajouter les doses des produits phyto
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                MineralProductInputUsage mineralProductInput = (MineralProductInputUsage) inputUsage;
                DomainMineralProductInput domainMineralProductInput = mineralProductInput.getDomainMineralProductInput();
                MineralProductUnit usageUnit = domainMineralProductInput.getUsageUnit();
                converter = conversionRate.stream().filter(cr -> usageUnit.equals(cr.getMineralProductUnit())).toList();
            }
            case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES -> {
                AbstractPhytoProductInputUsage phytoProductInput = (AbstractPhytoProductInputUsage) inputUsage;
                final DomainPhytoProductInput domainPhytoProductInput = phytoProductInput.getDomainPhytoProductInput();
                final PhytoProductUnit usageUnit = domainPhytoProductInput.getUsageUnit();
                converter = conversionRate.stream().filter(cr -> usageUnit.equals(cr.getPhytoProductUnit())).toList();
            }
            case AUTRE -> {
                OtherProductInputUsage otherProductInput = (OtherProductInputUsage) inputUsage;
                final OtherProductInputUnit usageUnit = otherProductInput.getDomainOtherInput().getUsageUnit();
                converter = conversionRate.stream().filter(cr -> usageUnit.equals(cr.getOtherProductInputUnit())).toList();
            }
            case LUTTE_BIOLOGIQUE -> {
                BiologicalProductInputUsage biologicalProductInputUsage = (BiologicalProductInputUsage) inputUsage;
                final PhytoProductUnit usageUnit = biologicalProductInputUsage.getDomainPhytoProductInput().getUsageUnit();
                converter = conversionRate.stream().filter(cr -> usageUnit.equals(cr.getPhytoProductUnit())).toList();
            }
            case SEMIS, PLAN_COMPAGNE, TRAITEMENT_SEMENCE -> {
                if (inputUsage instanceof SeedProductInputUsage seedProductInputUsage) {
                    DomainPhytoProductInput domainPhytoProductInput = seedProductInputUsage.getDomainPhytoProductInput();
                    final PhytoProductUnit usageUnit = domainPhytoProductInput.getUsageUnit();
                    converter = conversionRate.stream().filter(cr -> usageUnit.equals(cr.getPhytoProductUnit())).toList();
                } else {
                    converter = new ArrayList<>();
                }
            }
            case EPANDAGES_ORGANIQUES -> {
                OrganicProductInputUsage organicProductInput = (OrganicProductInputUsage) inputUsage;
                final OrganicProductUnit usageUnit = organicProductInput.getDomainOrganicProductInput().getUsageUnit();
                converter = conversionRate.stream().filter(cr -> usageUnit.equals(cr.getOrganicProductUnit())).toList();
            }
            case SUBSTRAT -> {
                SubstrateInputUsage substrateInput = (SubstrateInputUsage) inputUsage;
                final SubstrateInputUnit usageUnit = substrateInput.getDomainSubstrateInput().getUsageUnit();
                converter = conversionRate.stream().filter(cr -> usageUnit.equals(cr.getSubstrateInputUnit())).toList();
            }
            case POT -> {
                PotInputUsage potInput = (PotInputUsage) inputUsage;
                PotInputUnit usageUnit = potInput.getDomainPotInput().getUsageUnit();
                converter = conversionRate.stream().filter(cr -> usageUnit.equals(cr.getPotInputUnit())).toList();
            }
            case IRRIGATION -> converter = new ArrayList<>();// input usage is 'mm', there is no conversion
            default -> converter = new ArrayList<>();
        }
        return converter;
    }

    protected void getMissingInputUsageConversionRateMessage(AbstractInputUsage inputUsage, PriceUnit priceUnit, String interventionId) {

        String usageUnitName = "";
        switch (inputUsage.getInputType()) {
            // #9313 Ajouter les doses des produits phyto
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                MineralProductInputUsage mineralProductInput = (MineralProductInputUsage) inputUsage;
                MineralProductUnit usageUnit = mineralProductInput.getDomainMineralProductInput().getUsageUnit();
                usageUnitName = usageUnit.name();
            }
            case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES -> {
                AbstractPhytoProductInputUsage phytoProductInput = (AbstractPhytoProductInputUsage) inputUsage;
                final DomainPhytoProductInput domainPhytoProductInput = phytoProductInput.getDomainPhytoProductInput();
                final PhytoProductUnit usageUnit = domainPhytoProductInput.getUsageUnit();
                usageUnitName = usageUnit.name();
            }
            case AUTRE -> {
                OtherProductInputUsage otherProductInput = (OtherProductInputUsage) inputUsage;
                final OtherProductInputUnit usageUnit = otherProductInput.getDomainOtherInput().getUsageUnit();
                usageUnitName = usageUnit.name();
            }
            case LUTTE_BIOLOGIQUE -> {
                BiologicalProductInputUsage biologicalProductInputUsage = (BiologicalProductInputUsage) inputUsage;
                final PhytoProductUnit usageUnit = biologicalProductInputUsage.getDomainPhytoProductInput().getUsageUnit();
                usageUnitName = usageUnit.name();
            }
            case SEMIS, PLAN_COMPAGNE, TRAITEMENT_SEMENCE -> {
                if (inputUsage instanceof SeedProductInputUsage seedProductInputUsage) {
                    DomainPhytoProductInput domainPhytoProductInput = seedProductInputUsage.getDomainPhytoProductInput();
                    final PhytoProductUnit usageUnit = domainPhytoProductInput.getUsageUnit();
                    usageUnitName = usageUnit.name();
                }
            }
            case EPANDAGES_ORGANIQUES -> {
                OrganicProductInputUsage organicProductInput = (OrganicProductInputUsage) inputUsage;
                final OrganicProductUnit usageUnit = organicProductInput.getDomainOrganicProductInput().getUsageUnit();
                usageUnitName = usageUnit.name();
            }
            case SUBSTRAT -> {
                SubstrateInputUsage substrateInput = (SubstrateInputUsage) inputUsage;
                final SubstrateInputUnit usageUnit = substrateInput.getDomainSubstrateInput().getUsageUnit();
                usageUnitName = usageUnit.name();
            }
            case POT -> {
                PotInputUsage potInput = (PotInputUsage) inputUsage;
                PotInputUnit usageUnit = potInput.getDomainPotInput().getUsageUnit();
                usageUnitName = usageUnit.name();
            }
        }

        String priceUnitName = priceUnit != null ? priceUnit.name() : "?";
        addMissingFieldMessage(
                interventionId,
                messageBuilder.getMissingRefConversionRateForValuesMessage(usageUnitName, priceUnitName));
    }

    /**
     * return inputsCharges for price, inputsCharges for refprice
     */
    protected Pair<Double, Double> computeIndicatorForAction(
            WriterContext writerContext,
            AbstractAction action,
            Collection<? extends AbstractInputUsage> inputUsages,
            RefCampaignsInputPricesByDomainInput refCampaignsInputPricesByDomainInputAndCampaigns,
            double psci,
            Collection<RefInputUnitPriceUnitConverter> conversionRate,
            String interventionId,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels,
            Map<? extends AbstractInputUsage, UsagePerformanceResult> refVintageTargetIftDoseByUsages) {

        Pair<Double, Double> inputsCharges;

        double allRealCi = 0d;
        double allStandardizedPriceCi = 0d;

        for (AbstractInputUsage inputUsage : inputUsages) {

            Collection<RefInputUnitPriceUnitConverter> inputUsageConversionRate = getInputUsageConversionRate(inputUsage, conversionRate);

            Pair<Double, Double> inputResult = computeInputProductOperatingExpenses(
                    writerContext,
                    refCampaignsInputPricesByDomainInputAndCampaigns,
                    action,
                    inputUsage,
                    psci,
                    inputUsageConversionRate,
                    interventionId,
                    indicatorClass,
                    labels,
                    refVintageTargetIftDoseByUsages
            );

            allRealCi += inputResult.getLeft();
            allStandardizedPriceCi += inputResult.getRight();
        }

        inputsCharges = Pair.of(allRealCi, allStandardizedPriceCi);

        return inputsCharges;
    }

    public abstract Pair<Optional<InputPrice>, Optional<InputRefPrice>> getDomainInputPrice(
            AbstractInputUsage usage,
            RefCampaignsInputPricesByDomainInput refCampaignsInputPricesByDomainInputAndCampaigns);

    protected Pair<Double, Double> computeInputProductOperatingExpenses(
            WriterContext writerContext,
            RefCampaignsInputPricesByDomainInput refCampaignsInputPricesByDomainInputAndCampaigns,
            AbstractAction action,
            AbstractInputUsage inputUsage,
            double psci,
            Collection<RefInputUnitPriceUnitConverter> inputUsageConversionRates,
            String interventionId,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels,
            Map<? extends AbstractInputUsage, UsagePerformanceResult> refVintageTargetIftDoseByUsages) {

        final String usageTopiaId = inputUsage.getTopiaId();

        incrementAngGetTotalFieldCounterForTargetedId(usageTopiaId);// da
        incrementAngGetTotalFieldCounterForTargetedId(usageTopiaId);// product price

        double realCi = 0d;
        double standardizedPriceCi = 0d;

        addMissingDeprecatedInputQtyOrUnitIfRequired(inputUsage);

        Double da = inputUsage.getQtAvg();
        if (da == null) {
            // Pour les charges opérationnelles, on maintient le fonctionnement d'avant, si pas de dose, dose = 1
            if (InputType.LUTTE_BIOLOGIQUE == inputUsage.getInputType()) {
                da = 1.0d;
            } else {
                // #12637 Affectation de la dose de référence IFT à la cible millésimé aux données phyto manquantes pour les indicateurs QSA et Charges opérationnelles
                da = refVintageTargetIftDoseByUsages.get(inputUsage) != null ? refVintageTargetIftDoseByUsages.get(inputUsage).refDoseValue() : Indicator.DEFAULT_IFT;
            }
            addMissingFieldMessage(
                    usageTopiaId,
                    messageBuilder.getMissingDoseMessage(inputUsage.getInputType(), MissingMessageScope.INPUT));
        }

        Pair<Optional<InputPrice>, Optional<InputRefPrice>> priceForInput = getDomainInputPrice(inputUsage, refCampaignsInputPricesByDomainInputAndCampaigns);

        // #10063 Si aucun prix de référence n’est disponible pour le prix de l’intrant,
        // alors on prend la valeur « 0 » comme référence.
        final Optional<InputPrice> optionalUserPrice = priceForInput.getLeft();
        Optional<InputRefPrice> optionalRefPrice = priceForInput.getRight();
        if (optionalUserPrice.isEmpty() && optionalRefPrice.isEmpty()) {
            // aucun prix utilisateur ni prix de référence
            addMissingFieldMessage(
                    usageTopiaId,
                    messageBuilder.getMissingInputUsagePriceMessage(
                            inputUsage.getInputType(),
                            inputUsage,
                            action));

        } else {

            Double userPrice = null;
            PriceUnit userPriceUnit = null;
            if (optionalUserPrice.isPresent()) {
                final InputPrice userInputPrice = optionalUserPrice.get();
                userPrice = userInputPrice.getPrice();
                userPriceUnit = userInputPrice.getPriceUnit();
            }

            if (userPrice == null || userPriceUnit == null) {
                addMissingFieldMessage(
                        usageTopiaId,
                        messageBuilder.getMissingInputUsagePriceMessage(
                                inputUsage.getInputType(),
                                inputUsage,
                                action));
            }

            // seulement pour phyto sinon 1
            double treatedSurface = getPhytoInputTreatedSurfaceFactor(action);

            double conversionRate = 0;
            if (userPriceUnit != null) {
                final PriceUnit priceUnit0 = userPriceUnit;
                conversionRate = inputUsageConversionRates.stream()
                        .filter(cr -> priceUnit0.equals(cr.getPriceUnit()))
                        .findAny()
                        .map(RefInputUnitPriceUnitConverter::getConvertionRate)
                        .orElse(0d);

            }

            //Si aucun prix n’est saisi par l’utilisateur,
            // on prendra le prix de référence de l’intrant pour le calcul de l’indicateur.
            // Cf. bien se référer pour l’identification de ces prix de ref à la SPEC des charges
            //opérationnelles standardisées. Car l’identification des prix de référence est spécifique à chaque type
            //d’intrant.
            if (optionalRefPrice.isPresent()) {
                final InputRefPrice refInputPrice = optionalRefPrice.get();
                final PriceAndUnit averageRefPrice = refInputPrice.averageRefPrice();

                // L’utilisateur peut saisir un prix en €/ha pour les intrants. Dans ce cas-là, on considère que la dose
                // d’application de l’intrant vaut 1.
                standardizedPriceCi = treatedSurface * psci * IndicatorOperatingExpenses.computeCi(
                        da,
                        averageRefPrice.value(),
                        averageRefPrice.unit(),
                        1);// conversionRate already applied to refPrices
            }

            // #10063 Si aucun prix de référence n’est disponible pour le prix de l’intrant,
            // alors on prend la valeur « 0 » comme référence.

            if (conversionRate == 0) {
                getMissingInputUsageConversionRateMessage(inputUsage, userPriceUnit, interventionId);
            }

            if (userPrice != null) {

                // L’utilisateur peut saisir un prix en €/ha pour les intrants. Dans ce cas-là, on considère que la dose
                // d’application de l’intrant vaut 1.
                realCi = treatedSurface * psci * IndicatorOperatingExpenses.computeCi(da, userPrice, userPriceUnit, conversionRate);
            } else {
                realCi = standardizedPriceCi;
            }

        }

        writeInputUsage(action, inputUsage, writerContext, new Double[]{realCi, standardizedPriceCi}, indicatorClass, labels);

        return Pair.of(realCi, standardizedPriceCi);
    }

    protected double getPhytoInputTreatedSurfaceFactor(AbstractAction abstractAction) {
        double result = 1d;
        if (abstractAction instanceof PesticidesSpreadingAction pesticidesSpreadingAction) {
            result = pesticidesSpreadingAction.getProportionOfTreatedSurface() / 100;

        } else if (abstractAction instanceof BiologicalControlAction biologicalControlAction) {
            result = biologicalControlAction.getProportionOfTreatedSurface() / 100;
        }
        return result;
    }

    protected void writeInputUsage(
            AbstractAction action,
            AbstractInputUsage usage,
            WriterContext writerContext,
            Double[] inputResult,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        IndicatorWriter writer = writerContext.getWriter();

        final String inputTopiaId = usage.getTopiaId();

        String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(inputTopiaId, MissingMessageScope.INPUT);
        Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(inputTopiaId);

        Locale locale = writer.getLocale();
        for (int i = 0; i < inputResult.length; i++) {

            boolean isDisplayed = isDisplayed(i);

            if (isDisplayed) {
                // write input sheet
                writer.writeInputUsage(
                        writerContext.getIts(),
                        writerContext.getIrs(),
                        l(locale, Indicator.INDICATOR_CATEGORY_ECONOMIC),
                        l(locale, labels[i]),
                        usage,
                        action,
                        writerContext.getEffectiveIntervention(),
                        writerContext.getPracticedIntervention(),
                        writerContext.getCodeAmmBioControle(),
                        writerContext.getAnonymizeDomain(),
                        writerContext.getAnonymizeGrowingSystem(),
                        writerContext.getPlot(),
                        writerContext.getZone(),
                        writerContext.getPracticedSystem(),
                        writerContext.getCroppingPlanEntry(),
                        writerContext.getPracticedPhase(),
                        writerContext.getSolOccupationPercent(),
                        writerContext.getEffectivePhase(),
                        writerContext.getRank(),
                        writerContext.getPreviousPlanEntry(),
                        writerContext.getIntermediateCrop(),
                        indicatorClass,
                        inputResult[i],
                        reliabilityIndexForInputId,
                        reliabilityCommentForInputId,
                        null,
                        null,
                        null,
                        writerContext.getGroupesCiblesByCode());
            }
        }
    }
}
