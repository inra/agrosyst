package fr.inra.agrosyst.services.performance.indicators.ift;

/*
 * #%L
 * Agrosyst :: Services
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.PhytoProductTarget;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.Ift;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.RefCultureMAA;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefMAABiocontrole;
import fr.inra.agrosyst.api.entities.referential.RefMAABiocontroleTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCibleTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.services.performance.CropWithSpecies;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.performance.TraitementProduitWithCroppingPlanEntry;
import fr.inra.agrosyst.api.services.performance.TraitementProduitWithSpecies;
import fr.inra.agrosyst.api.services.performance.UsagePerformanceResult;
import fr.inra.agrosyst.api.services.performance.utils.PhytoProductUnitConverter;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import fr.inra.agrosyst.services.referential.csv.RefActaDosageSPCModel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jetbrains.annotations.NotNull;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * IFT à la cible non millésimé
 * d'après la spécification <a href="https://forge.codelutin.com/issues/12572">...</a>
 * <p>
 * En 2016 le MAA a proposé deux nouvelles méthodes de calcul de l’IFT, dites « à la cible » et « à la culture ».
 * <p>
 * Pour l’IFT « à la cible », la dose de référence pour le calcul de l’IFT n’est
 * plus définie par couple Produit|Culture, mais par trinôme Produit|Culture|Type de Cible.
 * La dose de référence pour un Type de Cible (ou Groupe de Cibles) est la plus
 * grande des doses homologuées sur les différents usages au sein de ce Type de Cible.
 * Si la cible du traitement n’est pas identifiée, la dose de référence retenue
 * est la plus petite des doses de références pour les différents Type de Cible
 * pour le couple Produit|Culture.
 * <p>
 * Pour chaque intrant de type ‘Phytosanitaire’
 * • Repérer les espèces cultivées de la culture concernée (le plus souvent il n’y en a qu’une, mais il peut y en avoir plusieurs)
 * • Sur la base de la table de correspondance Correspondance_CulturesAgrosyst_CulturesMAA, établir la liste des espèces MAA.
 * • Repérer les cibles de l’intrant (0, 1 ou plusieurs cibles)
 * • Sur la base de la table Correspondance_CiblesAgrosyst_GroupesCiblesMAA, établir la liste des GroupesCiblesMAA
 * • Dans le référentiel MAA_Doses_Ref_par_GroupeCibles, Repérer la Dose_Ref correspondant au quintuplet Campagne_Max|code_AMM|CultureMAA|Traitement_MAA|
 * où Campagne_Max est la valeur la plus élevée du champ ‘Campagne’ de la table MAA_Doses_Ref_par_GroupeCibles
 * ◦ S’il y a plusieurs valeurs parce que plusieurs cibles (et donc plusieurs GroupeCibleMAA),
 * retenir la dose de référence la plus élevée comme dose de référence (Dose_REF) pour ce traitement
 * ◦ S’il n’y a pas de cible déclarée (et donc pas de GroupeCiblesMAA),
 * retenir comme dose de référence la plus petite des doses de référence de la table MAA_Doses_Ref_par_GroupeCibles
 * pour le quadruplet Campagne_Max|code_AMM|CultureMAA| Traitement_MAA.
 * ◦ Si on ne trouve pas le quadruplet Campagne_Max|code_AMM|CultureMAA| Traitement_MAA dans MAA_Doses_Ref_par_GroupeCibles, alors
 * rechercher si on trouve le quadruplet code_AMM|CultureMAA|GroupesCiblesMAA| Traitement_MAA
 * et retenir comme Dose_Ref la plus grande des valeurs trouvées.
 * ◦ Si on ne trouve pas le quadruplet code_AMM|CultureMAA|GroupesCiblesMAA| Traitement_MAA dans MAA_Doses_Ref_par_GroupeCibles,
 * alors rechercher si on trouve le quadruplet Campagne_max|code_AMM|CultureMAA| Traitement_MAA  | et
 * retenir comme Dose_Ref la plus petite des valeurs trouvées.
 * ◦ Si on ne trouve pas le quadruplet Campagne_max|code_AMM|CultureMAA| Traitement_MAA dans MAA_Doses_Ref_par_GroupeCibles,
 * alors rechercher si on trouve le triplet code_AMM|CultureMAA| Traitement_MAA|
 * retenir comme Dose_Ref la plus petite des valeurs trouvées.
 * ◦ Si on ne trouve pas le triplet code_AMM|CultureMAA| Traitement_MAA dans MAA_Doses_Ref_par_GroupeCibles,
 * alors rechercher dans ACTA_Dosage_SPC_Complet selon le mécanisme présent actuellement,
 * à savoir id_produit*id_traitement*(id_culture+id_culture des groupes culture),
 * en retenant la plus petite valeur comme Dose_REF.
 * ◦ Si on ne trouve pas id_produit*id_traitement*(id_culture+id_culture des groupes culture) dans ACTA_Dosage_SPC_Complet, alors :
 * IFT_Cible_NonMillesimé = PSCI_PHYTO * 1
 * • Si le produit appliqué est identifié comme Biocontrôle = ‘O’ dans la table MAA_Biocontrôle pour la campagne Campagne_Max,
 * alors tous les IFT
 * - ift_Cible_NonMillesimé_total,
 * - ift_Cible_NonMillesimé_tot_hts,
 * - ift_Cible_NonMillesimé_h,
 * - ift_Cible_NonMillesimé_f,
 * - ift_Cible_NonMillesimé_i,
 * - ift_Cible_NonMillesimé_ts,
 * - ift_Cible_NonMillesimé_a,
 * - ift_Cible_NonMillesimé_hh
 * prennent la valeur 0,
 * et l’IFT_Cible calculé alimente
 * => IFT_Cible_Biocontrôle, (comme pour le calcul de l’IFT « à l’ancienne »).
 *
 * @author ymartel (martel@codelutin.com)
 */
@Setter
public class IndicatorRefMaxYearTargetIFT extends AbstractIndicator {

    private static final Log LOGGER = LogFactory.getLog(IndicatorRefMaxYearTargetIFT.class);
    public static final String REFERENCES_DOSAGE_UI = "Dose pour '(%s-%s):%s %s'";
    public static final String N_A = "N/A";

    protected RefMAABiocontroleTopiaDao refMAABiocontroleTopiaDao;
    protected RefMAADosesRefParGroupeCibleTopiaDao refMAADosesRefParGroupeCibleDao;
    /**
     * the corresponding indicator fields
     */
    protected static final String[] FIELDS = new String[]{
            "ift_a_la_cible_non_mil_ift_chimique_total",            // 0
            "ift_a_la_cible_non_mil_ift_chimique_tot_hts",          // 1
            "ift_a_la_cible_non_mil_ift_h",                         // 2
            "ift_a_la_cible_non_mil_ift_f",                         // 3
            "ift_a_la_cible_non_mil_ift_i",                         // 4
            "ift_a_la_cible_non_mil_ift_ts",                        // 5
            "ift_a_la_cible_non_mil_ift_a",                         // 6
            "ift_a_la_cible_non_mil_ift_hh",                        // 7
            "ift_a_la_cible_non_mil_ift_biocontrole"                // 8
    };

    protected static final String[] LABELS = new String[]{
            "Indicator.label.ift_a_la_cible_non_mil_ift_chimique_total",            // 0 IFT phytosanitaire chimique (ex total, refs #10576)
            "Indicator.label.ift_a_la_cible_non_mil_ift_chimique_tot_hts",          // 1 IFT phytosanitaire chimique total hors traitement de semence
            "Indicator.label.ift_a_la_cible_non_mil_ift_h",                         // 2 IFT herbicide
            "Indicator.label.ift_a_la_cible_non_mil_ift_f",                         // 3 IFT fongicide
            "Indicator.label.ift_a_la_cible_non_mil_ift_i",                         // 4 IFT insecticide
            "Indicator.label.ift_a_la_cible_non_mil_ift_ts",                        // 5 IFT traitement de semence
            "Indicator.label.ift_a_la_cible_non_mil_ift_a",                         // 6 IFT phytosanitaire autres
            "Indicator.label.ift_a_la_cible_non_mil_ift_hh",                        // 7 IFT phytosanitaire hors herbicide
            "Indicator.label.ift_a_la_cible_non_mil_ift_biocontrole"                // 8 IFT vert
    };
    protected Boolean[] iftsToDisplay = new Boolean[]{
            false,  //  0 IFT phytosanitaire chimique total
            false,  //  1 IFT phytosanitaire chimique total hors traitement de semence
            false,  //  2 IFT herbicide
            false,  //  3 IFT fongicide
            false,  //  4 IFT insecticide
            false,  //  5 IFT traitement de semence
            false,  //  6 IFT phytosanitaire autres
            false,  //  7 IFT phytosanitaire hors herbicide
            false   //  8 IFT vert
    };

    public static final Function<PhytoProductUnit, Boolean> IS_HL_PHYTO_UNIT = unitDoseProduct -> (
            unitDoseProduct == PhytoProductUnit.G_HL ||
                    unitDoseProduct == PhytoProductUnit.KG_HL || // k.ha
                    unitDoseProduct == PhytoProductUnit.L_HL);   // l.ha

    /**
     * Constructeur initialisant la map des champs optionnels
     */
    public IndicatorRefMaxYearTargetIFT() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, "ift_a_la_cible_non_mil_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, "ift_a_la_cible_non_mil_detail_champs_non_renseig");
        extraFields.put(OptionalExtraColumnEnumKey.DOSE_REFERENCE, "ift_a_la_cible_non_mil_dose_reference");
        extraFields.put(OptionalExtraColumnEnumKey.DOSE_REFERENCE_UNIT, "ift_a_la_cible_non_mil_dose_reference_unite");
    }

    protected record InterventionResult(
            Double[] values,
            Map<AbstractPhytoProductInputUsage, UsagePerformanceResult> inputResultByUsages
    ) {
    }

    protected record InputsResult(
            Double[] values,
            Map<AbstractPhytoProductInputUsage, UsagePerformanceResult> inputResultByUsages
    ) {
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_TARGET_IFT);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {

        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();

        for (int i = 0; i < LABELS.length; i++) {
            String indicatorName = getIndicatorLabel(i);
            String columnName = FIELDS[i];
            indicatorNameToColumnName.put(indicatorName, columnName);
        }
        return indicatorNameToColumnName;
    }

    @Override
    public boolean isDisplayed(ExportLevel atLevel, int i) {
        return iftsToDisplay[i];
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return true;// display at all level
    }

    /**
     * Formule de calcul:
     *
     * <pre>
     *
     *              DA (dose appliquée)
     * IFTi = sum ( -- * PSCi )
     *              DH (Dose homologuée)
     * </pre>
     */
    @Override
    public Double[] manageIntervention(
            WriterContext writerContext,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedCropExecutionContext cropContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            PracticedCropCyclePhase phase) {

        Double[] result = newArray(LABELS.length, 0.0);

        if (interventionContext.isFictive()) return result;

        InterventionComputeRefDoseContext interventionComputeRefDoseContext = new InterventionComputeRefDoseContext(
                interventionContext,
                cropContext.getRefEspeces());

        PracticedIntervention intervention = interventionContext.getIntervention();
        final double toolsPsci = getToolPSCi(intervention);

        InterventionResult seedingInputsResult = computeSeedingIFT(writerContext, interventionComputeRefDoseContext, toolsPsci);
        List<String> seedingRhytoRefDoseUis = seedingInputsResult.inputResultByUsages.values().stream().map(UsagePerformanceResult::refDoseUi).toList();
        interventionContext.addAllTargetedIftfDosageUiForPhytoInputs(seedingRhytoRefDoseUis);
        Map<AbstractPhytoProductInputUsage, UsagePerformanceResult> refDoseByUsages = filterOnMaaDosesRef(seedingInputsResult);

        InterventionResult phytoInputsIFT = computePracticedInterventionPhytoIFT(writerContext, interventionComputeRefDoseContext);
        List<String> phytoRefDoseUis = phytoInputsIFT.inputResultByUsages.values().stream().map(UsagePerformanceResult::refDoseUi).toList();
        interventionContext.addAllTargetedIftfDosageUiForPhytoInputs(phytoRefDoseUis);
        refDoseByUsages.putAll(filterOnMaaDosesRef(phytoInputsIFT));

        addAllTargetedIftRefDosageForPhytoUsages(interventionContext, refDoseByUsages);

        return sum(seedingInputsResult.values, phytoInputsIFT.values);
    }

    protected void addAllTargetedIftRefDosageForPhytoUsages(PerformanceInterventionContext interventionContext, Map<AbstractPhytoProductInputUsage, UsagePerformanceResult> refDoseByUsages) {
        // Affectation de la dose de référence IFT à la cible millésimé
    }

    private static Map<AbstractPhytoProductInputUsage, UsagePerformanceResult> filterOnMaaDosesRef(InterventionResult phytoInputsIFT) {
        Map<AbstractPhytoProductInputUsage, UsagePerformanceResult> refDoseByUsages = new HashMap<>();
        for (Map.Entry<AbstractPhytoProductInputUsage, UsagePerformanceResult> usageDoseRef : phytoInputsIFT.inputResultByUsages.entrySet()) {
            UsagePerformanceResult usageResult = usageDoseRef.getValue();
            if (!usageResult.isLegacyDose()) {
                refDoseByUsages.put(usageDoseRef.getKey(), usageResult);
            }
        }
        return refDoseByUsages;
    }

    /**
     * IFT-Cible = PSCI_PHYTO * (Dose appliquée/Dose_Ref)
     */
    protected InterventionResult computePracticedInterventionPhytoIFT(
            WriterContext writerContext,
            InterventionComputeRefDoseContext interventionComputeRefDoseContext
    ) {
        Double[] resultValues = newArray(LABELS.length, 0.0);

        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionComputeRefDoseContext.getOptionalPesticidesSpreadingAction();
        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionComputeRefDoseContext.getOptionalBiologicalControlAction();
        Map<AbstractPhytoProductInputUsage, UsagePerformanceResult> inputResultByUsages = Map.of();

        if (optionalPesticidesSpreadingAction.isPresent()) {
            final PesticidesSpreadingAction pesticidesSpreadingAction = optionalPesticidesSpreadingAction.get();
            Collection<? extends AbstractPhytoProductInputUsage> phytoProductInputs = pesticidesSpreadingAction.getPesticideProductInputUsages();

            double psci = getPhytoActionPSCi(interventionComputeRefDoseContext.getIntervention(), pesticidesSpreadingAction);

            InputsResult inputsResult = computeInputUsagesIFT(
                    interventionComputeRefDoseContext,
                    writerContext,
                    pesticidesSpreadingAction,
                    phytoProductInputs,
                    psci
            );

            resultValues = sum(resultValues, inputsResult.values);
            inputResultByUsages = inputsResult.inputResultByUsages;
        }

        if (optionalBiologicalControlAction.isPresent()) {
            final BiologicalControlAction biologicalControlAction = optionalBiologicalControlAction.get();
            Collection<? extends AbstractPhytoProductInputUsage> phytoProductInputs = biologicalControlAction.getBiologicalProductInputUsages();

            double psci = getPhytoActionPSCi(interventionComputeRefDoseContext.getIntervention(), biologicalControlAction);

            InputsResult inputsResult = computeInputUsagesIFT(
                    interventionComputeRefDoseContext,
                    writerContext,
                    biologicalControlAction,
                    phytoProductInputs,
                    psci);

            resultValues = sum(resultValues, inputsResult.values);
            inputResultByUsages = inputsResult.inputResultByUsages;
        }

        return new InterventionResult(resultValues, inputResultByUsages);
    }

    private InputsResult computeInputUsagesIFT(
            InterventionComputeRefDoseContext interventionComputeRefDoseContext,
            WriterContext writerContext,
            AbstractAction action,
            Collection<? extends AbstractPhytoProductInputUsage> phytoProductInputs,
            double psci) {

        Double[] result = newArray(LABELS.length, 0.0);
        Map<AbstractPhytoProductInputUsage, UsagePerformanceResult> inputResultByUsages = new HashMap<>();

        if (CollectionUtils.isNotEmpty(phytoProductInputs)) {

            for (AbstractPhytoProductInputUsage usage : phytoProductInputs) {

                Optional<UsagePerformanceResult> result_ = computeInputUsageIFT(
                        interventionComputeRefDoseContext,
                        writerContext,
                        action,
                        usage,
                        psci
                );

                if (result_.isPresent()) {
                    final UsagePerformanceResult usagePerformanceResult = result_.get();
                    result = sum(result, usagePerformanceResult.values());
                    inputResultByUsages.put(usage, usagePerformanceResult);
                }
            }
        }

        InputsResult inputsResult = new InputsResult(result, inputResultByUsages);
        return inputsResult;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {

        final EffectiveIntervention intervention = interventionContext.getIntervention();

        final double toolsPsci = getToolPSCi(intervention);

        final InterventionComputeRefDoseContext interventionComputeRefDoseContext = new InterventionComputeRefDoseContext(interventionContext);

        InterventionResult seedingInputsResult = computeSeedingIFT(writerContext, interventionComputeRefDoseContext, toolsPsci);
        List<String> seedingRhytoRefDoseUis = seedingInputsResult.inputResultByUsages.values().stream().map(UsagePerformanceResult::refDoseUi).toList();
        interventionContext.addAllTargetedIftfDosageUiForPhytoInputs(seedingRhytoRefDoseUis);
        Map<AbstractPhytoProductInputUsage, UsagePerformanceResult> refDoseByUsages = filterOnMaaDosesRef(seedingInputsResult);

        InterventionResult phytoInputsIFT = computeEffectiveInterventionPhytoIFT(writerContext, interventionComputeRefDoseContext);
        List<String> phytoRefDoseUis = phytoInputsIFT.inputResultByUsages.values().stream().map(UsagePerformanceResult::refDoseUi).toList();
        interventionContext.addAllTargetedIftfDosageUiForPhytoInputs(phytoRefDoseUis);
        Map<AbstractPhytoProductInputUsage, UsagePerformanceResult> phytoRefDoseByUsages = filterOnMaaDosesRef(phytoInputsIFT);
        refDoseByUsages.putAll(phytoRefDoseByUsages);

        addAllTargetedIftRefDosageForPhytoUsages(interventionContext, refDoseByUsages);

        return sum(seedingInputsResult.values, phytoInputsIFT.values);

    }

    protected InterventionResult computeEffectiveInterventionPhytoIFT(
            WriterContext writerContext,
            InterventionComputeRefDoseContext interventionComputeRefDoseContext) {

        Double[] resultValues = newArray(LABELS.length, 0.0);

        Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionComputeRefDoseContext.getOptionalPesticidesSpreadingAction();
        Map<AbstractPhytoProductInputUsage, UsagePerformanceResult> inputResultByUsages = Map.of();

        if (optionalPesticidesSpreadingAction.isPresent()) {
            final PesticidesSpreadingAction pesticidesSpreadingAction = optionalPesticidesSpreadingAction.get();
            Collection<? extends AbstractPhytoProductInputUsage> phytoProductInputs = pesticidesSpreadingAction.getPesticideProductInputUsages();

            double psci = getPhytoActionPSCi(interventionComputeRefDoseContext.getIntervention(), pesticidesSpreadingAction);

            InputsResult inputsResult = computeInputUsagesIFT(
                    interventionComputeRefDoseContext,
                    writerContext,
                    pesticidesSpreadingAction,
                    phytoProductInputs,
                    psci);

            resultValues = sum(resultValues, inputsResult.values);
            inputResultByUsages = inputsResult.inputResultByUsages;
        }

        Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionComputeRefDoseContext.getOptionalBiologicalControlAction();

        if (optionalBiologicalControlAction.isPresent()) {
            final BiologicalControlAction biologicalControlAction = optionalBiologicalControlAction.get();
            Collection<? extends AbstractPhytoProductInputUsage> phytoProductInputs = biologicalControlAction.getBiologicalProductInputUsages();

            double psci = getPhytoActionPSCi(interventionComputeRefDoseContext.getIntervention(), biologicalControlAction);

            InputsResult inputsResult = computeInputUsagesIFT(
                    interventionComputeRefDoseContext,
                    writerContext,
                    biologicalControlAction,
                    phytoProductInputs,
                    psci);

            resultValues = sum(resultValues, inputsResult.values);
            inputResultByUsages = inputsResult.inputResultByUsages;
        }

        return new InterventionResult(resultValues, inputResultByUsages);
    }

    protected Optional<UsagePerformanceResult> computeInputUsageIFT(
            InterventionComputeRefDoseContext interventionComputeRefDoseContext,
            WriterContext writerContext,
            AbstractAction phytoAction,
            AbstractPhytoProductInputUsage phytoProductInputUsage,
            double inputPsci) {

        final Collection<CroppingPlanSpecies> species = interventionComputeRefDoseContext.getCroppingPlanSpecies();
        final String usageTopiaId = phytoProductInputUsage.getTopiaId();

        // L’onglet «ACTA_traitements_produits» indique quels produits sont concernés par le calcul de l’IFT biocontrôle (colonne F).
        RefActaTraitementsProduit refActaTraitementsProduit = phytoProductInputUsage.getDomainPhytoProductInput().getRefInput();

        if (refActaTraitementsProduit == null) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Can't find phyto product for input " + phytoProductInputUsage.getDomainPhytoProductInput().getInputName());
            }

            addMissingFieldMessage(
                    usageTopiaId,
                    messageBuilder.getMissingInputProductMessage(phytoProductInputUsage.getInputType()));
            return Optional.empty();
        }

        // L’onglet « ACTA_traitements_prod_categ » indique quels traitements sont concernés par quelles catégories de l’IFT (colonnes G à O).
        RefActaTraitementsProduitsCateg traitementProduitCateg = interventionComputeRefDoseContext.traitementProduitCategByIdTraitement.get(refActaTraitementsProduit);

        String treatmentAmmCode = refActaTraitementsProduit.getCode_AMM();

        Map<AbstractInputUsage, UsagePerformanceResult> refVintageTargetIftDoseByUsages = getRefVintageTargetIftDoseByUsagesFromInterventionContext(interventionComputeRefDoseContext);

        final Optional<ReferenceDoseDTO> legacyActaDoseRef = interventionComputeRefDoseContext.getLegacyActaDoseRef(refActaTraitementsProduit);

        ComputeInputIftContext computeInputIftContext = new ComputeInputIftContext(
                interventionComputeRefDoseContext,
                phytoAction,
                phytoProductInputUsage,
                treatmentAmmCode,
                inputPsci,
                refActaTraitementsProduit.getCode_traitement_maa(),
                legacyActaDoseRef,
                refVintageTargetIftDoseByUsages);

        List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA = getRefMAADosesRefParGroupeCibles(computeInputIftContext, interventionComputeRefDoseContext);

        InputIftResult computedIft = computeIft(computeInputIftContext, doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);

        if (traitementProduitCateg == null) {

            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Aucune donnée dans le référentiel ACTA Traitements Produits (Catégories) pour le produit " +
                        refActaTraitementsProduit.getNom_produit() + " idProduit: '" +
                        refActaTraitementsProduit.getId_produit() + "' idTraitement: '" +
                        refActaTraitementsProduit.getId_traitement() + "'");
            }
            return Optional.of(new UsagePerformanceResult(
                    newArray(LABELS.length, 0.0),
                    computedIft.getReferencesDosageMessageUI(),
                    computedIft.refDoseId,
                    computedIft.refDoseValue,
                    computedIft.refDoseUnit,
                    computedIft.isLegacyDose));
        }

        // PhytoProduct
        incrementAngGetTotalFieldCounterForTargetedId(usageTopiaId);
        // avgDose
        incrementAngGetTotalFieldCounterForTargetedId(usageTopiaId);
        // PhytoProductUnit
        incrementAngGetTotalFieldCounterForTargetedId(usageTopiaId);
        // species
        incrementAngGetTotalFieldCounterForTargetedId(usageTopiaId);

        addMissingDeprecatedInputQtyOrUnitIfRequired(phytoProductInputUsage);

        if (phytoProductInputUsage.getQtAvg() == null) {
            addMissingFieldMessage(
                    usageTopiaId,
                    messageBuilder.getMissingDoseMessage(
                            phytoProductInputUsage.getInputType(),
                            MissingMessageScope.INPUT));
        }

        if (phytoProductInputUsage.getDomainPhytoProductInput().getUsageUnit() == null) {
            addMissingFieldMessage(
                    usageTopiaId,
                    messageBuilder.getMissingDoseUnitMessage(phytoProductInputUsage.getInputType()));
        }

        if (CollectionUtils.isEmpty(species)) {
            addMissingFieldMessage(usageTopiaId, messageBuilder.getMissingInterventionSpeciesMessage());
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("No species selected for intervention " + writerContext.getInterventionName());
            }
        }


        // Check if biocontrole
        //    • Si le produit appliqué est identifié comme Biocontrôle = ‘O’ dans la table MAA_Biocontrôle pour la campagne Campagne_Max,
        //      alors tous les IFT prennent la valeur 0, et l’IFT_Cible calculé alimente l’IFT_Cible_Biocontrôle,
        //      (comme pour le calcul de l’IFT « à l’ancienne »).
        long t0 = System.currentTimeMillis();
        RefMAABiocontrole refMAABiocontrole =
                refMAABiocontroleTopiaDao.forCode_ammEquals(treatmentAmmCode)
                        .addEquals(RefMAABiocontrole.PROPERTY_ACTIVE, true)
                        .setOrderByArguments(
                                RefMAABiocontrole.PROPERTY_CAMPAGNE + " DESC").
                        findFirstOrNull();// TODO peut-être remplacer par une requête native ? ou mettre un cache ?
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("RefMAABiocontrole " + (System.currentTimeMillis() - t0) + "ms");
        }

        boolean isBiocontrol = refMAABiocontrole != null && refMAABiocontrole.isBiocontrol();

        IndicatorWriter writer = writerContext.getWriter();

        String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(
                usageTopiaId,
                MissingMessageScope.INPUT);
        Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(usageTopiaId);

        // calcul de la valeur de l'ift
        Double[] inputResult = feedUpIftResults(isBiocontrol, traitementProduitCateg, computedIft.getIft());

        for (int i = 0; i < inputResult.length; i++) {

            boolean isDisplayed = isDisplayed(ExportLevel.INPUT, i);

            if (isDisplayed) {
                // write input sheet
                writer.writeInputUsage(
                        writerContext.getIts(),
                        writerContext.getIrs(),
                        getIndicatorCategory(),
                        getIndicatorLabel(i),
                        phytoProductInputUsage,
                        phytoAction,
                        writerContext.getEffectiveIntervention(),
                        writerContext.getPracticedIntervention(),
                        writerContext.getCodeAmmBioControle(),
                        writerContext.getAnonymizeDomain(),
                        writerContext.getAnonymizeGrowingSystem(),
                        writerContext.getPlot(),
                        writerContext.getZone(),
                        writerContext.getPracticedSystem(),
                        writerContext.getCroppingPlanEntry(),
                        writerContext.getPracticedPhase(),
                        writerContext.getSolOccupationPercent(),
                        writerContext.getEffectivePhase(),
                        writerContext.getRank(),
                        writerContext.getPreviousPlanEntry(),
                        writerContext.getIntermediateCrop(),
                        this.getClass(),
                        inputResult[i],
                        reliabilityIndexForInputId,
                        reliabilityCommentForInputId,
                        computedIft.getReferencesDosageMessageUI(),
                        computedIft.getRefDoseValue() != null ? computedIft.getRefDoseValue().toString() : null,
                        computedIft.getRefDoseUnit() != null ? RefActaDosageSPCModel.PHYTO_PRODUCT_UNIT_FORMATTER.format(computedIft.getRefDoseUnit()) : N_A,
                        writerContext.getGroupesCiblesByCode());
            }
        }

        return Optional.of(new UsagePerformanceResult(
                inputResult,
                computedIft.getReferencesDosageMessageUI(),
                computedIft.refDoseId,
                computedIft.refDoseValue,
                computedIft.refDoseUnit,
                computedIft.isLegacyDose));
    }

    /**
     * De référentiel en référentiel pour trouver la dose de ref MAA !
     */

    @Getter
    public static class ComputeInputIftContext {

        @Getter
        private final Domain domain;
        private final Collection<CroppingPlanSpecies> species;
        private final Collection<RefEspece> refEspeces;
        @Getter
        private final AbstractAction action;

        private final AbstractPhytoProductInputUsage phytoProductInputUsage;

        private final String ammCode;
        private final double psci;
        private final String maaTreatmentCode;
        @Getter
        private final Map<AbstractInputUsage, UsagePerformanceResult> refVintageTargetIftDoseByUsages;

        @Getter
        private final Optional<ReferenceDoseDTO> legacyActaDoseRef;

        public ComputeInputIftContext(
                InterventionComputeRefDoseContext interventionComputeRefDoseContext,
                AbstractAction action,
                AbstractPhytoProductInputUsage phytoProductInputUsage,
                String ammCode,
                double psci,
                String maaTreatmentCode,
                Optional<ReferenceDoseDTO> legacyActaDoseRef,
                Map<AbstractInputUsage, UsagePerformanceResult> refVintageTargetIftDoseByUsages) {

            this.species = interventionComputeRefDoseContext.croppingPlanSpecies;
            this.refEspeces = interventionComputeRefDoseContext.refEspeces;
            this.domain = interventionComputeRefDoseContext.domain;
            this.action = action;
            this.phytoProductInputUsage = phytoProductInputUsage;
            this.ammCode = ammCode;
            this.psci = psci;
            this.maaTreatmentCode = maaTreatmentCode;
            this.legacyActaDoseRef = legacyActaDoseRef;
            this.refVintageTargetIftDoseByUsages = refVintageTargetIftDoseByUsages;
        }

    }

    @Getter
    public static class InputIftResult {
        private final Double ift;
        private final String referencesDosageMessageUI;
        private final String refDoseId;
        private final Double refDoseValue;
        private final PhytoProductUnit refDoseUnit;
        private final boolean isLegacyDose;

        private InputIftResult(
                String referencesDosageUI, String refDoseId,
                Double refDoseValue,
                PhytoProductUnit refDoseUnit,
                double targetedIft, boolean isLegacyDose) {
            this.referencesDosageMessageUI = referencesDosageUI;
            this.refDoseId = refDoseId;
            this.refDoseValue = refDoseValue;
            this.refDoseUnit = refDoseUnit;
            this.ift = targetedIft;
            this.isLegacyDose = isLegacyDose;
        }

        public static InputIftResult createIftResult(
                String referencesDosageUI,
                String refDoseId,
                Double refDoseValue,
                PhytoProductUnit refDoseUnit,
                double targetedIft
        ) {
            return new InputIftResult(
                    referencesDosageUI,
                    refDoseId,
                    refDoseValue,
                    refDoseUnit,
                    targetedIft,
                    false);
        }

        public static InputIftResult createLegacyIftResult(
                String referencesDosageUI,
                Double refDoseValue,
                PhytoProductUnit refDoseUnit,
                double ift) {

            return new InputIftResult(
                    referencesDosageUI,
                    null, refDoseValue,
                    refDoseUnit,
                    ift,
                    true);
        }

        public static InputIftResult createLegacyIftResultWithoutRefDose(String referencesDosageUI, double ift) {
            return new InputIftResult(
                    referencesDosageUI,
                    null, null,
                    null,
                    ift,
                    true);
        }

        public static InputIftResult createRefMaxYearTargetIftWithoutResult(String referencesDosageUI, double ift) {
            return new InputIftResult(
                    referencesDosageUI,
                    null, null,
                    null,
                    ift,
                    false);
        }
    }

    protected Optional<RefMAADosesRefParGroupeCible> getVintageRefMAADosesRefParGroupeCible(
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
            AbstractPhytoProductInputUsage phytoProductInputUsage,
            int domainCampaign) {

        return Optional.empty();
    }

    protected InputIftResult computeIft(
            ComputeInputIftContext computeContext,
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA) {

        // Pour chaque intrant de type ‘Phytosanitaire’
        //    • Repérer les espèces cultivées de la culture concernée
        //    (le plus souvent il n’y en a qu’une, mais il peut y en avoir plusieurs)
        AbstractPhytoProductInputUsage phytoProductInputUsage = computeContext.getPhytoProductInputUsage();

        final InputIftResult result;
        double psci = computeContext.getPsci();

        // si la dose n'est pas renseignée, la dose appliquée est la dose de référence de l'ift à la cible millésimé (c'est à dire par rapport à la campagne du domaine ou la campagne la plus proche)
        UsagePerformanceResult indicatorRefDose = getIndicatorReferenceDoseDTO(phytoProductInputUsage, computeContext, doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);

        DomainPhytoProductInput productInput = phytoProductInputUsage.getDomainPhytoProductInput();

        Double appliedDose = phytoProductInputUsage.getQtAvg();
        PhytoProductUnit unitDoseProduct = productInput.getUsageUnit();

        //L'ancien fonctionnement est donc toujours de rigueur pour les sans AMM :
        //- Pour les indicateurs "recours aux moyens biologiques", la dose n'est pas importante (1 usage = 1)
        //- Pour les charges opérationnelles, on maintient le fonctionnement d'avant, si pas de dose, dose = 1

        // les IFT ne sont pas concernés par les intrants sans AMM
        Double indicatorCurentDoseRef = indicatorRefDose.refDoseValue();
        PhytoProductUnit indicatorCurentDoseRefUnit = indicatorRefDose.refDoseUnit();
        if (CollectionUtils.isEmpty(computeContext.getSpecies()) ||
                InputType.LUTTE_BIOLOGIQUE == phytoProductInputUsage.getDomainPhytoProductInput().getInputType()
        ) {
            result = InputIftResult.createIftResult(
                    indicatorRefDose.refDoseUi(),
                    indicatorRefDose.refDoseId(),
                    indicatorCurentDoseRef,
                    indicatorCurentDoseRefUnit,
                    DEFAULT_IFT * psci);
            return result;
        }
        // si pas de dose, la dose appliquée est la dose de référence de l'ift à la cible millésimé
        if (appliedDose == null && indicatorCurentDoseRef != null) {
            appliedDose = indicatorCurentDoseRef;
            unitDoseProduct = indicatorCurentDoseRefUnit;
        }

        double ift = 0.0;

        final RefActaTraitementsProduit phytoProduct = productInput.getRefInput();

        if (appliedDose != null && indicatorCurentDoseRef != null) {

            boolean isHlUnitDoseProduct = IS_HL_PHYTO_UNIT.apply(unitDoseProduct);
            boolean isHlUnitRefProduct = IS_HL_PHYTO_UNIT.apply(indicatorCurentDoseRefUnit);

            if (appliedDose != 0) {

                if (PhytoProductUnit.G_HL == unitDoseProduct) {
                    appliedDose = appliedDose * 0.001;
                }
                if (PhytoProductUnit.G_HL == indicatorCurentDoseRefUnit) {
                    indicatorCurentDoseRef = indicatorCurentDoseRef * 0.001;
                }

                double coefConversion = unitDoseProduct != indicatorCurentDoseRefUnit ? PhytoProductUnitConverter.getUnitConversionRatio(unitDoseProduct, indicatorCurentDoseRefUnit) : 1;

                if (isHlUnitDoseProduct && !isHlUnitRefProduct) {
                    final double boiledQuantityFactor = computeBoiledQuantityFactor(computeContext.action, unitDoseProduct);
                    // PSCI_PHYTO * (Dose appliquée/Dose_Ref) * Volume_Bouillie * tx conversion
                    ift = psci * (appliedDose / indicatorCurentDoseRef) * boiledQuantityFactor * coefConversion;
                } else if (isHlUnitDoseProduct) {
                    final double boiledQuantityFactor = computeBoiledQuantityFactor(computeContext.action, unitDoseProduct);
                    // PSCI_PHYTO * (Dose appliquée * Volume Bouillie)/(Dose_Ref * 10)
                    ift = psci * (appliedDose * boiledQuantityFactor) / (appliedDose * 10.0);
                } else {
                    // PSCI_PHYTO * (Dose appliquée/Dose_Ref) / tx conversion
                    ift = psci * (appliedDose / indicatorCurentDoseRef) * coefConversion;
                }
            }

            result = InputIftResult.createIftResult(
                    indicatorRefDose.refDoseUi(),
                    indicatorRefDose.refDoseId(),
                    indicatorCurentDoseRef,
                    unitDoseProduct,
                    ift
            );

        } else if (computeContext.getRefEspeces() == null) {

            String referencesDosageUI = REFERENCES_DOSAGE_UI;
            referencesDosageUI = String.format(
                    referencesDosageUI,
                    phytoProduct.getNom_produit(),
                    phytoProduct.getNom_traitement(),
                    N_A,
                    "");

            addMissingRefDosageMessageMessage(phytoProductInputUsage.getTopiaId());

            // utiliser la dose de l'IFT à la cible millésimé

            // ◦ aucune dose de référence ne peut être trouvé alors
            //   IFT_Cible_NonMillesimé = PSCI_PHYTO * 1
            ift = Optional.ofNullable(appliedDose).orElse(DEFAULT_IFT) * psci;

            result = InputIftResult.createRefMaxYearTargetIftWithoutResult(
                    referencesDosageUI,
                    ift);

        } else {

            if (computeContext.getLegacyActaDoseRef().isPresent() && appliedDose != null) {

                ReferenceDoseDTO referenceDoseDTO = computeContext.getLegacyActaDoseRef().get();

                double dh = Optional.ofNullable(referenceDoseDTO.getValue()).orElse(0.0d);
                unitDoseProduct = referenceDoseDTO.getUnit();
                if (unitDoseProduct == PhytoProductUnit.G_HL ||
                        unitDoseProduct == PhytoProductUnit.KG_HL ||
                        unitDoseProduct == PhytoProductUnit.L_HL) {
                    dh *= 10.0d;
                }

                String referencesDosageUI = REFERENCES_DOSAGE_UI;
                referencesDosageUI = String.format(
                        referencesDosageUI,
                        phytoProduct.getNom_produit(),
                        phytoProduct.getNom_traitement(),
                        referenceDoseDTO.getValue(),
                        RefActaDosageSPCModel.PHYTO_PRODUCT_UNIT_FORMATTER.format(unitDoseProduct));

                final double boiledQuantityFactor = computeBoiledQuantityFactor(computeContext.action, unitDoseProduct);
                appliedDose = appliedDose * boiledQuantityFactor;

                ift = dh == 0.0d ? 0.0d : psci * (appliedDose / dh);

                result = InputIftResult.createLegacyIftResult(
                        referencesDosageUI,
                        dh,
                        unitDoseProduct,
                        ift);

            } else {

                String referencesDosageUI = REFERENCES_DOSAGE_UI;
                referencesDosageUI = String.format(
                        referencesDosageUI,
                        phytoProduct.getNom_produit(),
                        phytoProduct.getNom_traitement(),
                        N_A,
                        "");
                addMissingRefDosageMessageMessage(phytoProductInputUsage.getTopiaId());
                // ◦ Si on ne trouve pas le triplet Code_AMM|EspèceEDI|traitementMAA dans ACTA_Dosage_SPC_Complet, alors :
                //                                            IFT_Cible_NonMillesimé = PSCI_PHYTO * 1
                ift = DEFAULT_IFT * psci;

                result = InputIftResult.createLegacyIftResultWithoutRefDose(
                        referencesDosageUI,
                        ift);
            }
        }

        return result;
    }

    protected UsagePerformanceResult getIndicatorReferenceDoseDTO(
            AbstractPhytoProductInputUsage usage,
            ComputeInputIftContext context,
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA) {

        // pour l'indicateur non-millésimé, c'est la dose la plus récente
        int refDoseCampaign = getStudiedCampaign(context, doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);

        Optional<RefMAADosesRefParGroupeCible> optionalDoseRef = getRefMAADosesRefParGroupeCible(doseRefsFor_CodeAMM_CultureMAA_TraitementMAA, usage, refDoseCampaign);

        return getUsagePerformanceResult(usage, doseRefsFor_CodeAMM_CultureMAA_TraitementMAA, optionalDoseRef, refDoseCampaign);
    }

    protected @NotNull UsagePerformanceResult getUsagePerformanceResult(
            AbstractPhytoProductInputUsage usage,
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
            Optional<RefMAADosesRefParGroupeCible> optionalDoseRef,
            int refDoseCampaign) {

        UsagePerformanceResult usagePerformanceResult;
        if (optionalDoseRef.isEmpty()) {
            optionalDoseRef = getMaaDoseRefForNoTargets(
                    refDoseCampaign,
                    doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);
        }

        RefActaTraitementsProduit refInput = usage.getDomainPhytoProductInput().getRefInput();
        String referencesDosageUI = REFERENCES_DOSAGE_UI;
        if (optionalDoseRef.isPresent() && optionalDoseRef.get().getDose_ref_maa() != null) {

            RefMAADosesRefParGroupeCible refMAADosesRefParGroupeCible = optionalDoseRef.get();

            referencesDosageUI = String.format(
                    referencesDosageUI,
                    refInput.getNom_produit(),
                    refInput.getNom_traitement(),
                    refMAADosesRefParGroupeCible.getDose_ref_maa(),
                    RefActaDosageSPCModel.PHYTO_PRODUCT_UNIT_FORMATTER.format(refMAADosesRefParGroupeCible.getUnit_dose_ref_maa()));

            usagePerformanceResult = new UsagePerformanceResult(
                    newArray(LABELS.length, 0.0),
                    referencesDosageUI,
                    refMAADosesRefParGroupeCible.getTopiaId(),
                    refMAADosesRefParGroupeCible.getDose_ref_maa(),
                    refMAADosesRefParGroupeCible.getUnit_dose_ref_maa(),
                    false);
        } else {

            referencesDosageUI = String.format(
                    referencesDosageUI,
                    refInput.getNom_produit(),
                    refInput.getNom_traitement(),
                    N_A,
                    usage.getDomainPhytoProductInput().getUsageUnit());
            usagePerformanceResult = new UsagePerformanceResult(
                    newArray(LABELS.length, 0.0),
                    referencesDosageUI,
                    referencesDosageUI,
                    null,
                    null,
                    false);
        }

        return usagePerformanceResult;
    }

    /**
     * Correspondant au quintuplet Campagne_Max|code_AMM|CultureMAA|GroupesCiblesMAA|traitementMAA,
     * où Campagne_Max est la valeur la plus élevée du champ ‘Campagne’ de la table MAA_Doses_Ref_par_GroupeCibles
     */
    protected Optional<RefMAADosesRefParGroupeCible> getRefMAADosesRefParGroupeCible(
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
            AbstractPhytoProductInputUsage phytoProductInputUsage,
            int refDoseCampaign) {

        Set<String> bioAgressorIdentifiantOrReference_id = null;
        Collection<String> groupesCibles = null;
        Collection<PhytoProductTarget> targets = phytoProductInputUsage.getTargets();
        if (CollectionUtils.isNotEmpty(targets)) {
            groupesCibles = targets.stream()
                    .map(PhytoProductTarget::getCodeGroupeCibleMaa)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());

            bioAgressorIdentifiantOrReference_id = getTargetIdsFromAdventicesAndNuisibleEdis(targets);
        }

        Function<Map<String, Boolean>, Collection<RefMAADosesRefParGroupeCible>> getMaaDoseRefFonction = groupesCiblesMaaCodesAndGeneric -> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA;

        return referentialService.computeMaaRefDose(bioAgressorIdentifiantOrReference_id, groupesCibles, getMaaDoseRefFonction);
    }

    protected Integer getStudiedCampaign(ComputeInputIftContext computeContext,
                                         List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA) {
        return getRefDoseMaxCampaign(doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);
    }

    protected Integer getRefDoseMaxCampaign(Collection<RefMAADosesRefParGroupeCible> doseRefs) {
        int maxCampaign = doseRefs.stream()
                .map(RefMAADosesRefParGroupeCible::getCampagne)
                .filter(Objects::nonNull)
                .max(Integer::compareTo)
                .orElse(-1);
        return maxCampaign;
    }

    protected List<RefMAADosesRefParGroupeCible> getRefMAADosesRefParGroupeCibles(ComputeInputIftContext computeContext, InterventionComputeRefDoseContext interventionComputeRefDoseContext) {

        final Set<String> codeCultureMaas = CollectionUtils.emptyIfNull(computeContext.getSpecies())
                .stream()
                .map(CroppingPlanSpecies::getSpecies)
                .map(RefEspece::getCulturesMaa)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(RefCultureMAA::getCode_culture_maa)
                .collect(Collectors.toSet());

        String ammCode = computeContext.getAmmCode();
        String maaTreatmentCode = computeContext.getMaaTreatmentCode();

        List<RefMAADosesRefParGroupeCible> codeAMM_CultureMAA_TraitementMAA_ToRefDoses;
        if (CollectionUtils.isNotEmpty(codeCultureMaas) && StringUtils.isNotBlank(ammCode) && StringUtils.isNotBlank(maaTreatmentCode)) {
            Optional<List<RefMAADosesRefParGroupeCible>> optionalDoseRefsForAmmCodeCodeCultureMaasMaaTreatmentCode =
                    interventionComputeRefDoseContext.getCodeAmmCultureMaaTraitementMaaAllDoseRefs(ammCode, codeCultureMaas, maaTreatmentCode);

            codeAMM_CultureMAA_TraitementMAA_ToRefDoses = optionalDoseRefsForAmmCodeCodeCultureMaasMaaTreatmentCode.orElseGet(() -> {
                List<RefMAADosesRefParGroupeCible> codeAMM_CultureMAA_TraitementMAA_ToRefDose =
                        refMAADosesRefParGroupeCibleDao.findByCodeAmmCodeCulturesAndTreatmentCode(
                                ammCode,
                                codeCultureMaas,
                                maaTreatmentCode
                        );
                interventionComputeRefDoseContext.addCodeAmmCultureMaaTraitementMaaAllDoseRefs(ammCode, codeCultureMaas, maaTreatmentCode, codeAMM_CultureMAA_TraitementMAA_ToRefDose);
                return codeAMM_CultureMAA_TraitementMAA_ToRefDose;
            });

        } else {
            codeAMM_CultureMAA_TraitementMAA_ToRefDoses = List.of();
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Code traitement MAA non retrouvé");
            }
        }

        return codeAMM_CultureMAA_TraitementMAA_ToRefDoses;
    }

    protected Optional<RefMAADosesRefParGroupeCible> getMaaDoseRefForNoTargets(int refDoseCampaign, List<RefMAADosesRefParGroupeCible> doseRefs) {
        // Si on ne trouve pas le quadruplet code_AMM|CultureMAA|GroupesCiblesMAA|traitementMAA dans MAA_Doses_Ref_par_GroupeCibles,
        // alors rechercher si on trouve le quadruplet Campagne|code_AMM|CultureMAA|traitementMAA,
        // et retenir comme Dose_Ref la plus petite des valeurs trouvées.
        Optional<RefMAADosesRefParGroupeCible> optionalDoseRef = doseRefs.stream()
                .filter(dose -> Objects.nonNull(dose.getDose_ref_maa()))
                .filter(dose -> refDoseCampaign == dose.getCampagne())
                .min(Comparator.comparing(RefMAADosesRefParGroupeCible::getDose_ref_maa));

        // ◦ Si on ne trouve pas le quadruplet Campagne|code_AMM|CultureMAA|traitementMAA dans MAA_Doses_Ref_par_GroupeCibles,
        // alors rechercher si on trouve le triplet code_AMM|CultureMAA|traitementMAA, et retenir comme Dose_Ref la plus petite des valeurs trouvées.
        if (optionalDoseRef.isEmpty()) {
            optionalDoseRef = doseRefs.stream()
                    .filter(dose -> Objects.nonNull(dose.getDose_ref_maa()))
                    .min(Comparator.comparing(RefMAADosesRefParGroupeCible::getDose_ref_maa));
        }
        return optionalDoseRef;
    }

    protected static Set<String> getTargetIdsFromAdventicesAndNuisibleEdis(Collection<PhytoProductTarget> targets) {
        Set<String> targetIds = new HashSet<>();
        for (PhytoProductTarget phytoProductTarget : targets) {
            RefBioAgressor target = phytoProductTarget.getTarget();
            if (target != null) {
                if (target.getReferenceParam() == BioAgressorType.ADVENTICE) {
                    targetIds.add(((RefAdventice) target).getIdentifiant());
                } else {
                    targetIds.add(String.valueOf(((RefNuisibleEDI) target).getReference_id()));
                }
            }
        }
        return targetIds;
    }

    protected Double[] feedUpIftResults(boolean isBiocontrole, RefActaTraitementsProduitsCateg traitementProduitCateg, double ift) {
        Double[] result = newArray(LABELS.length, 0.0);
        // Par exemple, pour le calcul de l'IFT Chimique, l'algorithme regardera dans la table
        // ACTA_traitements_produits si le produit est dans la liste NODU (Nombre de Doses Unités, colonne NODU O/N)
        // Si ce n'est pas le cas, il ira vérifier dans ACTA_traitement_prod_categ si le traitement
        // entre dans le calcul de l'IFT chimique total (colonne IFT_chimique_total O/N). Si toutes les conditions
        // sont respectées, alors le produit rentre dans le calcul de l'IFT.

        // affectation de l'IFT au catégories concernées
        if (traitementProduitCateg.isIft_chimique_total() && !isBiocontrole) {
            result[0] = ift; // "IFT Chimique"
        }
        if (traitementProduitCateg.isIft_chimique_tot_hts() && !isBiocontrole) {
            result[1] = ift; // "IFT tot hts"
        }
        if (traitementProduitCateg.isIft_h() && !isBiocontrole) {
            result[2] = ift; // "IFT h"
        }
        if (traitementProduitCateg.isIft_f() && !isBiocontrole) {
            result[3] = ift; // "IFT f"
        }
        if (traitementProduitCateg.isIft_i() && !isBiocontrole) {
            result[4] = ift; // "IFT i"
        }
        if (traitementProduitCateg.isIft_ts() && !isBiocontrole) {
            result[5] = ift; // "IFT ts"
        }
        if (traitementProduitCateg.isIft_a() && !isBiocontrole) {
            result[6] = ift; // "IFT a"
        }
        if (traitementProduitCateg.isIft_hh() && !isBiocontrole) {
            result[7] = ift; // "IFT hh (ts inclus)"
        }
        if (isBiocontrole) {
            result[8] = ift; // "IFT biocontrole"
        }

        return result;
    }

    /**
     * indicateur "Recours aux moyens biologiques"
     * Cet indicateur est calculé pour les actions de type 'Semis', lorsque la case 'Inoculation biologique des plants' est cochées dans l'action de semis
     * Les produits concernés sont ceux associés aux id_traitement pour lesquels le champ IFT moy bio = T dans le ref RefActaTraitementsProduitsCateg
     * Pour cet indicateurs, les doses de référence n'interviennent pas.
     * Recours aux moyens biologiques = 1 * PSCi (PSCi = fréquence spatiale * fréquence temporelle)
     * <p>
     * indicateur "IFT biocontrole"
     * Il est possible qu'un produit de lutte biologique soit aussi un produit de bio-contrôle (si nodu = T dans RefActaTraitementProduit).
     * Dans ce cas, on calcule aussi un IFT biocontrôle, dans lequel on fait intervenir les doses de référence et doses appliquées.]
     **/
    protected InterventionResult computeSeedingIFT(
            WriterContext writerContext,
            InterventionComputeRefDoseContext interventionComputeRefDoseContext,
            double toolsPsci) {

        Double[] result = newArray(LABELS.length, 0.0);

        Optional<SeedingActionUsage> optionalSeedingActionUsageAction = interventionComputeRefDoseContext.getSeedingActionUsage();

        final String interventionId = interventionComputeRefDoseContext.getIntervention().getTopiaId();

        Map<AbstractPhytoProductInputUsage, UsagePerformanceResult> inputResultByUsages = new HashMap<>();

        if (optionalSeedingActionUsageAction.isPresent()) {
            final SeedingActionUsage seedingActionUsage = optionalSeedingActionUsageAction.get();

            Collection<SeedLotInputUsage> seedLotInputUsages = seedingActionUsage.getSeedLotInputUsage();

            boolean isTreatment = seedLotInputUsages.stream()
                    .map(SeedLotInputUsage::getDomainSeedLotInput)
                    .map(DomainSeedLotInput::getDomainSeedSpeciesInput)
                    .flatMap(Collection::stream)
                    .anyMatch(DomainSeedSpeciesInput::isChemicalTreatment);

            Collection<SeedProductInputUsage> seedingProductInputUsages = new HashSet<>();
            seedLotInputUsages.forEach(
                    seedLotInputUsage -> {
                        // add species product used
                        final Collection<SeedSpeciesInputUsage> seedingSpecies = CollectionUtils.emptyIfNull(seedLotInputUsage.getSeedingSpecies());
                        seedingSpecies.stream()
                                .filter(ssiu -> CollectionUtils.isNotEmpty(ssiu.getSeedProductInputUsages()))
                                .forEach(
                                        ssiu -> seedingProductInputUsages.addAll(ssiu.getSeedProductInputUsages())
                                );
                    }
            );
            // isTreatment
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            // isBiologicalSeedInoculation
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);

            final Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement =
                    interventionComputeRefDoseContext.getTraitementProduitCategByIdTraitement();

            int ift_ts = 0;
            double ift_biocontrole = 0.0;

            // seedingActionSpecies
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);

            // isTreatment
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            // isBiologicalSeedInoculation
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);

            if (CollectionUtils.isEmpty(seedingProductInputUsages)) {

                if (isTreatment) {
                    ift_ts++;
                }
                // prise en compte de la part de la culture dans la surface du SDC sur l'IFT
                result[0] = ift_ts * toolsPsci; // "IFT Chimique"
                result[1] = 0.0d; // "IFT tot hts"
                result[2] = 0.0d; // "IFT h"
                result[3] = 0.0d; // "IFT f"
                result[4] = 0.0d; // "IFT i"
                result[5] = ift_ts * toolsPsci; // "IFT ts"
                result[6] = 0.0d; // "IFT a"
                result[7] = ift_ts * toolsPsci; // "IFT hh (ts inclus)"
                result[8] = ift_biocontrole; // "IFT biocontrole"
            }

            for (SeedProductInputUsage seedProductInputUsage : seedingProductInputUsages) {

                InputIftResult computedIft;

                final String productInputTopiaId = seedProductInputUsage.getTopiaId();
                // product
                incrementAngGetTotalFieldCounterForTargetedId(productInputTopiaId);
                final RefActaTraitementsProduit refActaTraitementsProduit = seedProductInputUsage.getDomainPhytoProductInput().getRefInput();

                if (refActaTraitementsProduit == null) {
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn("Can't find phyto product for input " + seedProductInputUsage.getDomainPhytoProductInput().getInputName());
                    }

                    addMissingFieldMessage(
                            productInputTopiaId,
                            messageBuilder.getMissingInputProductMessage(seedProductInputUsage.getInputType()));

                    return new InterventionResult(result, inputResultByUsages);
                }

                Map<AbstractInputUsage, UsagePerformanceResult> refDoseByUsages = getRefVintageTargetIftDoseByUsagesFromInterventionContext(interventionComputeRefDoseContext);

                // Il est necessaire de faire le calcul suivant même si le nodu est faux pour calculer la dose de référence dans les indicateurs HRI-1
                final String treatmentAmmCode = refActaTraitementsProduit.getCode_AMM();
                final double inputPSCi = getPhytoActionPSCi(interventionComputeRefDoseContext.getIntervention(), seedingActionUsage);
                Optional<ReferenceDoseDTO> legacyActaDoseRef = interventionComputeRefDoseContext.getLegacyActaDoseRef(refActaTraitementsProduit);
                ComputeInputIftContext computeInputIftContext = new ComputeInputIftContext(
                        interventionComputeRefDoseContext,
                        seedingActionUsage,
                        seedProductInputUsage,
                        treatmentAmmCode,
                        inputPSCi,
                        refActaTraitementsProduit.getCode_traitement_maa(),
                        legacyActaDoseRef,
                        refDoseByUsages);

                List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA = getRefMAADosesRefParGroupeCibles(computeInputIftContext, interventionComputeRefDoseContext);

                computedIft = computeIft(computeInputIftContext, doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);


                // L’onglet « ACTA_traitements_prod_categ » indique quels traitements sont concernés par quelles catégories de l’IFT (colonnes G à O).
                RefActaTraitementsProduitsCateg traitementProduitCateg = traitementProduitCategByIdTraitement.get(refActaTraitementsProduit);

                if (traitementProduitCateg == null) {
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn("Aucune donnée dans le référentiel ACTA Traitements Produits (Catégories) pour le produit " +
                                refActaTraitementsProduit.getNom_produit() + " idProduit: '" +
                                refActaTraitementsProduit.getId_produit() + "' idTraitement: '" +
                                refActaTraitementsProduit.getId_traitement() + "'");
                    }

                    Double[] inputResult = newArray(LABELS.length, 0.0);

                    UsagePerformanceResult usagePerformanceResult = new UsagePerformanceResult(
                            inputResult,
                            computedIft.getReferencesDosageMessageUI(),
                            computedIft.getRefDoseId(),
                            computedIft.getRefDoseValue(),
                            computedIft.getRefDoseUnit(),
                            computedIft.isLegacyDose());
                    inputResultByUsages.put(seedProductInputUsage, usagePerformanceResult);

                    continue;
                }

                //  ift_ts = isTreatment ? ift_ts + 1 : ift_ts;
                //  ift_bio = isBiologicalSeedInoculation ? ift_bio + 1 : ift_bio;
                boolean nodu = refActaTraitementsProduit.isNodu();//Nombre de Doses Unité

                // Case 'Traitement chimique des semences / plants' cochée et champ 'nodu = false'
                // dans le ref RefActaTraitementsProduit pour le couple produit*id_traitement = TS chimique
                // on calcule un ift_ts, qui est comptabilisé dans l'ift_chimique_total et décompté de l'ift_chimique_tot_hts avec
                // ift_ts = PSCi*1
                if (isTreatment && !nodu) {
                    ift_ts++;
                }

                // Il est possible qu'un produit de lutte biologique soit aussi un produit de bio-contrôle
                // (si nodu = T dans RefActaTraitementProduit).
                // Dans ce cas, on calcule aussi un IFT biocontrôle, dans lequel on fait intervenir les doses de référence et doses appliquées.

                if (nodu) {

                    // avgDose
                    incrementAngGetTotalFieldCounterForTargetedId(productInputTopiaId);
                    Double avgDose = seedProductInputUsage.getQtAvg();

                    addMissingDeprecatedInputQtyOrUnitIfRequired(seedProductInputUsage);

                    if (avgDose == null) {
                        // #10124 - si le champ 'dose appliquée' n'est pas renseignée pour un intrant de type 'produit phytosanitaire',
                        //          alors la valeur par défaut est la dose de référence pour ce produit phytosanitaire sur la culture concernée (ce qui donne
                        //          un IFT de 1, modulo PSCI);
                        //        - si Agrosyst ne trouve pas de dose de référence pour ce produit phytosanitaire sur la culture concernée,
                        //          alors l'IFT associée à l'intrant est 1*PSCI (comme c'est déjà le cas)
                        addMissingFieldMessage(
                                productInputTopiaId,
                                messageBuilder.getMissingDoseMessage(seedProductInputUsage.getInputType(), MissingMessageScope.INPUT));
                    }
                    // productUnit
                    incrementAngGetTotalFieldCounterForTargetedId(productInputTopiaId);
                    final PhytoProductUnit productUnit = seedProductInputUsage.getDomainPhytoProductInput().getUsageUnit();
                    if (productUnit == null) {
                        addMissingFieldMessage(
                                productInputTopiaId,
                                messageBuilder.getMissingDoseUnitMessage(seedProductInputUsage.getInputType()));
                    }

                    ift_biocontrole += computedIft.getIft();

                }

                Double[] inputResult = newArray(LABELS.length, 0.0);
                // prise en compte de la part de la culture dans la surface du SDC sur l'IFT
                inputResult[0] = ift_ts * toolsPsci; // "IFT Chimique"
                inputResult[1] = 0.0d; // "IFT tot hts"
                inputResult[2] = 0.0d; // "IFT h"
                inputResult[3] = 0.0d; // "IFT f"
                inputResult[4] = 0.0d; // "IFT i"
                inputResult[5] = ift_ts * toolsPsci; // "IFT ts"
                inputResult[6] = 0.0d; // "IFT a"
                inputResult[7] = ift_ts * toolsPsci; // "IFT hh (ts inclus)"
                inputResult[8] = ift_biocontrole; // "IFT biocontrole"

                UsagePerformanceResult usagePerformanceResult = new UsagePerformanceResult(
                        inputResult,
                        computedIft.getReferencesDosageMessageUI(),
                        computedIft.refDoseId,
                        computedIft.refDoseValue,
                        computedIft.refDoseUnit,
                        computedIft.isLegacyDose);
                inputResultByUsages.put(seedProductInputUsage, usagePerformanceResult);


                IndicatorWriter writer = writerContext.getWriter();

                String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(productInputTopiaId, MissingMessageScope.INPUT);
                Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(productInputTopiaId);

                for (int i = 0; i < inputResult.length; i++) {

                    boolean isDisplayed = isDisplayed(ExportLevel.INPUT, i);

                    if (isDisplayed) {

                        String refDoseUI = computedIft.getReferencesDosageMessageUI();
                        String refDoseValue = computedIft.getRefDoseValue() != null ? computedIft.getRefDoseValue().toString() : null;
                        String refDoseUnit = computedIft.getRefDoseUnit() != null ? RefActaDosageSPCModel.PHYTO_PRODUCT_UNIT_FORMATTER.format(computedIft.getRefDoseUnit()) : N_A;

                        // write input sheet
                        writer.writeInputUsage(
                                writerContext.getIts(),
                                writerContext.getIrs(),
                                getIndicatorCategory(),
                                getIndicatorLabel(i),
                                seedProductInputUsage,
                                seedingActionUsage,
                                writerContext.getEffectiveIntervention(),
                                writerContext.getPracticedIntervention(),
                                writerContext.getCodeAmmBioControle(),
                                writerContext.getAnonymizeDomain(),
                                writerContext.getAnonymizeGrowingSystem(),
                                writerContext.getPlot(),
                                writerContext.getZone(),
                                writerContext.getPracticedSystem(),
                                writerContext.getCroppingPlanEntry(),
                                writerContext.getPracticedPhase(),
                                writerContext.getSolOccupationPercent(),
                                writerContext.getEffectivePhase(),
                                writerContext.getRank(),
                                writerContext.getPreviousPlanEntry(),
                                writerContext.getIntermediateCrop(),
                                this.getClass(),
                                inputResult[i],
                                reliabilityIndexForInputId,
                                reliabilityCommentForInputId,
                                refDoseUI,
                                refDoseValue,
                                refDoseUnit,
                                writerContext.getGroupesCiblesByCode());
                    }
                }

                result = sum(result, inputResult);

            }

        }
        InterventionResult interventionResult = new InterventionResult(result, inputResultByUsages);
        return interventionResult;
    }

    protected Map<AbstractInputUsage, UsagePerformanceResult> getRefVintageTargetIftDoseByUsagesFromInterventionContext(InterventionComputeRefDoseContext interventionComputeRefDoseContext) {
        Map<AbstractInputUsage, UsagePerformanceResult> refDoseByUsages;
        if (interventionComputeRefDoseContext.getOptionalEffectiveInterventionContext().isPresent()) {
            refDoseByUsages = interventionComputeRefDoseContext.getOptionalEffectiveInterventionContext().get().getRefVintageTargetIftDoseByUsages();
        } else {
            refDoseByUsages = interventionComputeRefDoseContext.getOptionalPracticedInterventionContext().orElseThrow().getRefVintageTargetIftDoseByUsages();
        }
        return refDoseByUsages;
    }

    public void init(Collection<Ift> iftsToDisplay) {

        resetIftToDisplay();

        for (Ift ift : iftsToDisplay) {
            switch (ift) {
                case TOTAL -> this.iftsToDisplay[0] = true;
                case SUM_HTS -> this.iftsToDisplay[1] = true;
                case H -> this.iftsToDisplay[2] = true;
                case F -> this.iftsToDisplay[3] = true;
                case I -> this.iftsToDisplay[4] = true;
                case TS -> this.iftsToDisplay[5] = true;
                case A -> this.iftsToDisplay[6] = true;
                case HH -> this.iftsToDisplay[7] = true;
                case BIO_CONTROL -> this.iftsToDisplay[8] = true;
            }
        }
    }

    protected void resetIftToDisplay() {
        iftsToDisplay = new Boolean[]{
                false,  // 0 IFT phytosanitaire total
                false,  // 1 IFT phytosanitaire total hors traitement de semence
                false,  // 2 IFT herbicide
                false,  // 3 IFT fongicide
                false,  // 4 IFT insecticide
                false,  // 5 IFT traitement de semence
                false,  // 6 IFT phytosanitaire autres
                false,  // 7 IFT phytosanitaire hors herbicide
                false   // 8 IFT vert
        };
    }


    @Getter
    public static class InterventionComputeRefDoseContext {

        private static final String DELIMITER = ", ";

        final Domain domain;

        final Optional<PerformancePracticedInterventionExecutionContext> optionalPracticedInterventionContext;
        final Optional<PerformanceEffectiveInterventionExecutionContext> optionalEffectiveInterventionContext;

        final Optional<PracticedIntervention> optionalPracticedIntervention;
        final Optional<EffectiveIntervention> optionalEffectiveIntervention;

        @Getter
        final Set<RefEspece> refEspeces;
        final List<CroppingPlanSpecies> croppingPlanSpecies;
        @Getter
        final Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement;
        @Getter
        final Optional<SeedingActionUsage> seedingActionUsage;// ?

        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction;// use

        final Optional<BiologicalControlAction> optionalBiologicalControlAction;// use

        // key = ammCode, cultureIds, maaTreatmentCode
        protected final MultiKeyMap<String, List<RefMAADosesRefParGroupeCible>> doseRefs = new MultiKeyMap<>();

        public InterventionComputeRefDoseContext(
                PerformancePracticedInterventionExecutionContext practicedInterventionContext,
                Set<RefEspece> refEspeces) {
            this.domain = practicedInterventionContext.getCropWithSpecies().getCroppingPlanEntry().getDomain();
            this.optionalPracticedInterventionContext = Optional.of(practicedInterventionContext);
            this.optionalPracticedIntervention = Optional.of(practicedInterventionContext.getIntervention());
            this.refEspeces = refEspeces;
            this.croppingPlanSpecies = practicedInterventionContext.getInterventionSpecies();
            this.traitementProduitCategByIdTraitement = practicedInterventionContext.getTraitementProduitCategByIdTraitement();
            this.seedingActionUsage = practicedInterventionContext.getOptionalSeedingActionUsage();
            this.optionalPesticidesSpreadingAction = practicedInterventionContext.getOptionalPesticidesSpreadingAction();
            this.optionalBiologicalControlAction = practicedInterventionContext.getOptionalBiologicalControlAction();
            this.optionalEffectiveInterventionContext = Optional.empty();
            this.optionalEffectiveIntervention = Optional.empty();
        }

        public InterventionComputeRefDoseContext(
                PerformanceEffectiveInterventionExecutionContext effectiveInterventionContext) {
            this.domain = effectiveInterventionContext.getCroppingPlanEntry().getDomain();
            this.optionalEffectiveInterventionContext = Optional.of(effectiveInterventionContext);
            this.optionalEffectiveIntervention = Optional.of(effectiveInterventionContext.getIntervention());
            this.refEspeces = effectiveInterventionContext.getRefEspeces();
            this.croppingPlanSpecies = effectiveInterventionContext.getInterventionSpecies();
            this.traitementProduitCategByIdTraitement = effectiveInterventionContext.getTraitementProduitCategByIdTraitement();
            this.seedingActionUsage = effectiveInterventionContext.getOptionalSeedingActionUsage();
            this.optionalPesticidesSpreadingAction = effectiveInterventionContext.getOptionalPesticidesSpreadingAction();
            this.optionalBiologicalControlAction = effectiveInterventionContext.getOptionalBiologicalControlAction();
            this.optionalPracticedInterventionContext = Optional.empty();
            this.optionalPracticedIntervention = Optional.empty();
        }

        public Optional<List<RefMAADosesRefParGroupeCible>> getCodeAmmCultureMaaTraitementMaaAllDoseRefs(String ammCode, Set<String> codeCultureMaas, String maaTreatmentCode) {
            List<String> sortedCodeCultureMaas = codeCultureMaas.stream().sorted().toList();
            return Optional.ofNullable(doseRefs.get(ammCode, String.join(DELIMITER, sortedCodeCultureMaas), maaTreatmentCode));
        }

        public void addCodeAmmCultureMaaTraitementMaaAllDoseRefs(String ammCode, Set<String> codeCultureMaas, String maaTreatmentCode, List<RefMAADosesRefParGroupeCible> codeAMM_CultureMAA_TraitementMAA_ToRefDose) {
            List<String> sortedCodeCultureMaas = codeCultureMaas.stream().sorted().toList();
            doseRefs.put(ammCode, String.join(DELIMITER, sortedCodeCultureMaas), maaTreatmentCode, codeAMM_CultureMAA_TraitementMAA_ToRefDose);
        }

        public TopiaEntity getIntervention() {
            return optionalPracticedIntervention.isPresent() ? optionalPracticedIntervention.get() : optionalEffectiveIntervention.orElseThrow();
        }

        public Optional<ReferenceDoseDTO> getLegacyActaDoseRef(RefActaTraitementsProduit refActaTraitementsProduit) {

            Optional<ReferenceDoseDTO> actaDoseRef = Optional.empty();

            if (optionalPracticedInterventionContext.isPresent()) {

                final PerformancePracticedInterventionExecutionContext practicedInterventionExecutionContext = optionalPracticedInterventionContext.get();
                CropWithSpecies croppingPlanEntryWithSpecies = practicedInterventionExecutionContext.getCropWithSpecies();
                TraitementProduitWithSpecies traitementProduitWithSpecies = new TraitementProduitWithSpecies(
                        refActaTraitementsProduit,
                        croppingPlanEntryWithSpecies);

                Map<TraitementProduitWithSpecies, ReferenceDoseDTO> legacyActaDoseRefs =
                        practicedInterventionExecutionContext.getLegacyRefDosageForPhytoInputs();

                actaDoseRef = Optional.ofNullable(legacyActaDoseRefs.get(traitementProduitWithSpecies));
            } else if (optionalEffectiveInterventionContext.isPresent()) {
                final PerformanceEffectiveInterventionExecutionContext effectiveInterventionExecutionContext = optionalEffectiveInterventionContext.get();
                TraitementProduitWithCroppingPlanEntry traitementProduitWithCrop = new TraitementProduitWithCroppingPlanEntry(
                        refActaTraitementsProduit,
                        effectiveInterventionExecutionContext.getCroppingPlanEntry());

                Map<TraitementProduitWithCroppingPlanEntry, ReferenceDoseDTO> legacyActaDoseRefs =
                        effectiveInterventionExecutionContext.getLegacyRefDosageForPhytoInputs();
                actaDoseRef = Optional.ofNullable(legacyActaDoseRefs.get(traitementProduitWithCrop));
            }

            return actaDoseRef;
        }

        public List<CroppingPlanSpecies> getInterventionSpecies() {
            return croppingPlanSpecies;
        }

    }

    protected double getPhytoActionPSCi(TopiaEntity intervention, AbstractAction phytoAction) {
        if (intervention instanceof PracticedIntervention pi) {
            return super.getPhytoActionPSCi(pi, phytoAction);
        } else if (intervention instanceof EffectiveIntervention ei) {
            return super.getPhytoActionPSCi(ei, phytoAction);
        }
        return 0;
    }

    @Override
    protected List<String> getReferencesDosagesUserInfos(PerformanceInterventionContext interventionContext) {
        ArrayList<String> referencesDosagesUserInfos = new ArrayList<>(interventionContext.getTargetIftRefDoseForProductDisplay().stream().filter(Objects::nonNull).toList());
        return referencesDosagesUserInfos;
    }

}
