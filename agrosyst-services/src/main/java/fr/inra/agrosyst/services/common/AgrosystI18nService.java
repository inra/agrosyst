/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.services.common;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CompagneType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.MaterielType;
import fr.inra.agrosyst.api.entities.Stoniness;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.managementmode.CategoryObjective;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrgaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefOtherInputTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPotTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalisTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSubstrateTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgricultureTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntityTranslation;
import fr.inra.agrosyst.api.entities.referential.ReferentialI18nEntry;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.entities.referential.TradRefDivers;
import fr.inra.agrosyst.api.entities.referential.TradRefIntervention;
import fr.inra.agrosyst.api.entities.referential.TradRefSol;
import fr.inra.agrosyst.api.entities.referential.TradRefVivant;
import fr.inra.agrosyst.api.entities.report.GlobalMasterLevelQualifier;
import fr.inra.agrosyst.api.entities.report.MasterScale;
import fr.inra.agrosyst.api.entities.report.PressureScale;
import fr.inra.agrosyst.api.entities.report.YieldLossCause;
import fr.inra.agrosyst.api.services.domain.CroppingPlans;
import fr.inra.agrosyst.api.services.domain.Equipments;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Setter
public class AgrosystI18nService extends AbstractAgrosystService {

    private static final Log LOGGER = LogFactory.getLog(AgrosystI18nService.class);

    /**
     * Pour chaque énumération traduite, l'entité en base où il faut chercher les traductions.
     */
    private static final ImmutableMap<Class<? extends Enum<?>>, Class<? extends ReferentialI18nEntry>> TRANSLATABLE_ENUM_CLASS_TO_I18N_ENTITY_CLASSES;

    private RefEspeceTopiaDao refEspeceDao;

    private RefStadeEDITopiaDao refStadeEDIDao;

    private CroppingPlanEntryTopiaDao croppingPlanEntryDao;

    private RefFertiMinUNIFATopiaDao refFertiMinUNIFADao;

    private RefFertiOrgaTopiaDao refFertiOrgaDao;

    private RefPotTopiaDao refPotDao;

    private RefSubstrateTopiaDao refSubstrateDao;

    private RefOtherInputTopiaDao refOtherInputDao;

    private RefTypeAgricultureTopiaDao refTypeAgricultureDao;

    private RefSolArvalisTopiaDao refSolArvalisDao;

    private RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEdiDao;

    private RefMaterielTopiaDao refMaterielTopiaDao;

    /**
     * Pour chaque référentiel des traductions (vivant, sols, intrants...), la liste des clés qu'il doit contenir.
     */
    private static final ImmutableSetMultimap<? extends Class<? extends ReferentialI18nEntry>, String> ALL_ENUM_KEYS;

    static {
        TRANSLATABLE_ENUM_CLASS_TO_I18N_ENTITY_CLASSES =
                ImmutableMap.<Class<? extends Enum<?>>, Class<? extends ReferentialI18nEntry>>builder()
                .put(AgrosystInterventionType.class, TradRefIntervention.class)
                .put(BioAgressorType.class, TradRefVivant.class)
                .put(DomainType.class, TradRefDivers.class)
                .put(SeedType.class, TradRefVivant.class)
                .put(YealdUnit.class, TradRefDivers.class)
                .put(MaterielType.class, TradRefDivers.class)
                .put(Stoniness.class, TradRefSol.class)
                .put(CompagneType.class, TradRefVivant.class)
                .build();
        ALL_ENUM_KEYS = TRANSLATABLE_ENUM_CLASS_TO_I18N_ENTITY_CLASSES.entrySet().stream()
                .collect(ImmutableSetMultimap.flatteningToImmutableSetMultimap(
                        Map.Entry::getValue,
                        classClassEntry -> getAllEnumI18nKeys(classClassEntry.getKey()).stream()
                ));
    }

    private static Set<String> getAllEnumI18nKeys(Class<? extends Enum<?>> enumClass) {
        return Arrays.stream(enumClass.getEnumConstants())
                .map(enumConstant -> enumClass.getName() + "." + enumConstant.name())
                .collect(Collectors.toSet());
    }

    public ImmutableSetMultimap<? extends Class<? extends ReferentialI18nEntry>, String> getAllEnumKeys() {
        return ALL_ENUM_KEYS;
    }

    /**
     * Used by action to automatically generate enum map.
     */
    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface EnumContext {
        Class<? extends Enum> contextEnum();
        String context();
    }

    public Locale getLocale() {
        return getSecurityContext().getLocale();
    }

    public static Locale getDefaultLocale() {
        return I18n.getDefaultLocale();
    }

    /**
     * Transform enumeration values into map with i18n value for each enum value.
     *
     * i18n key is fqn.NAME
     *
     * @param <T> generic type
     * @param context use optionnal context (can be {code null})
     * @param values values to transform
     * @return map (enum value &gt; i18n text)
     */
    @SafeVarargs
    public final <T> Map<T, String> getEnumAsMap(String context, T... values) {
        Map<T, String> valuesMap = new LinkedHashMap<>();
        for (T value : values) {
            String trans = getEnumTraduction(context, value);
            valuesMap.put(value, trans);
        }
        return valuesMap;
    }

    @SafeVarargs
    public static <T> Map<T, String> getEnumAsMap(Locale locale, String context, T... values) {
        Map<T, String> valuesMap = new LinkedHashMap<>();
        for (T value : values) {
            String trans = getEnumTraduction(locale, context, value);
            valuesMap.put(value, trans);
        }
        return valuesMap;
    }

    @SafeVarargs
    public static <T> Map<T, String> getEnumAsMapWithDefaultLocale(String context, T... values) {
        Map<T, String> valuesMap = new LinkedHashMap<>();
        for (T value : values) {
            String trans = getEnumTraductionWithDefaultLocale(context, value);
            valuesMap.put(value, trans);
        }
        return valuesMap;
    }

    /**
     * Translate single enum value.
     *
     * @param context context
     * @param value value to translate
     * @param <T> generic type
     * @return value translation
     */
    public <T> String getEnumTraduction(String context, T value) {
        Locale locale = getSecurityContext().getLocale();
        return getEnumTraduction(locale, context, value);
    }

    public static <T> String getEnumTraduction(Locale locale, String context, T value) {
        String result = null;
        if (value != null) {
            String i18n = value.getClass().getName();
            if (context != null) {
                i18n += "#" + context;
            }
            i18n += "." + value;
            result = I18n.l(locale, i18n);
            if (i18n.equals(result)) {
                result = I18n.l(Locale.ENGLISH, i18n);
            }
        }
        return result;
    }

    public static <T> String getEnumTraduction(Locale locale, T value) {
        return getEnumTraduction(locale, null, value);
    }

    public static <T> String getEnumTraductionWithDefaultLocale(String context, T value) {
        String result = null;
        if (value != null) {
            String i18n = value.getClass().getName();
            if (context != null) {
                i18n += "#" + context;
            }
            i18n += "." + value;
            result = I18n.t(i18n);
        }
        return result;
    }

    public static <T> String getEnumTraductionWithDefaultLocale(T value) {
        return getEnumTraductionWithDefaultLocale(null, value);
    }

    public <T extends Enum<T>> Map<T, String> getEnumTranslationMap(Class<T> enumClass) {
        Preconditions.checkArgument(isTranslatedInDatabase(enumClass), "La traduction de " + enumClass + " n'est pas gérée est base");

        // look for user langage
        Language language = getSecurityContext().getLanguage();
        Map<T, String> result = getEnumTranslations(language, enumClass);

        List<T> missingTranslations = addUserLangageEnumTranslations(enumClass, result);

        addEnglishEnumTranslations(enumClass, result, missingTranslations);

        addFrenchEnumTranslations(enumClass, result, missingTranslations);

        addDefaultEnumName(result, missingTranslations);

        return result;
    }

    protected static <T extends Enum<T>> List<T> addUserLangageEnumTranslations(Class<T> enumClass, Map<T, String> result) {
        List<T> missingTranslations = new ArrayList<>();
        for (T enumConstant : enumClass.getEnumConstants()) {
            if (!result.containsKey(enumConstant)) {
                missingTranslations.add(enumConstant);
            }
        }
        return missingTranslations;
    }

    protected static <T extends Enum<T>> void addDefaultEnumName(Map<T, String> result, List<T> missingTranslations) {
        if (!missingTranslations.isEmpty()) {
            for (T missingTranslation : missingTranslations) {
                String translation = missingTranslation.name();
                result.put(missingTranslation, translation);
            }
        }
    }

    protected <T extends Enum<T>> void addFrenchEnumTranslations(Class<T> enumClass, Map<T, String> result, List<T> missingTranslations) {
        if (!missingTranslations.isEmpty()) {
            Map<T, String> missingResults = getEnumTranslations(Language.FRENCH, enumClass);
            addEnumTranslations(result, missingTranslations, missingResults);
        }
    }

    protected <T extends Enum<T>> void addEnglishEnumTranslations(Class<T> enumClass, Map<T, String> result, List<T> missingTranslations) {
        if (!missingTranslations.isEmpty()) {
            Map<T, String> missingResults = getEnumTranslations(Language.ENGLISH, enumClass);
            addEnumTranslations(result, missingTranslations, missingResults);
        }
    }

    protected static <T extends Enum<T>> void addEnumTranslations(Map<T, String> result, List<T> missingTranslations, Map<T, String> missingResults) {
        Iterator<T> missingTranslationIterator = missingTranslations.iterator();
        while (missingTranslationIterator.hasNext()) {
            T missingTranslation = missingTranslationIterator.next();
            String translation = missingResults.get(missingTranslation);
            if (translation != null) {
                result.put(missingTranslation, translation);
                missingTranslationIterator.remove();
            }
        }
    }

    protected <T extends Enum<T>> Map<T, String>  getEnumTranslations(Language language, Class<T> enumClass) {
        String enumClassName = enumClass.getName();
        String entityClassName = TRANSLATABLE_ENUM_CLASS_TO_I18N_ENTITY_CLASSES.get(enumClass).getName();
        String query = " FROM " + entityClassName +
                " WHERE lang = :lang and tradkey like :klassName " +
                " ORDER BY traduction ASC ";
        TopiaJpaSupport jpaSupport = getPersistenceContext().getJpaSupport();
        Map<String, Object> args = Map.of("lang", language.getTrigram(), "klassName", enumClassName + ".%");
        Map<T, String> result = new LinkedHashMap<>();
        jpaSupport.<ReferentialI18nEntry>stream(query, args).forEach(entry -> {
            String value = entry.getTradkey().replace(enumClassName + ".", "");
            T e = Enum.valueOf(enumClass, value);
            result.put(e, entry.getTraduction());
        });
        return result;
    }

    public boolean isTranslatedInDatabase(Class<? extends Enum> enumClass) {
        return TRANSLATABLE_ENUM_CLASS_TO_I18N_ENTITY_CLASSES.containsKey(enumClass);
    }

    public Map<String, String> getMessages() {
        Language language = getSecurityContext().getLanguage();
        var entries = I18n.getStore().getBundleEntries(language.getLocale());
        Properties properties = new Properties();
        Stream.of(entries).forEach((entry) -> {
            try {
                entry.load(properties, I18n.getStore().getResolver().getEncoding());
            } catch (IOException e) {
                LOGGER.error("Unable to load messages", e);
            }
        });
        return properties.entrySet().stream()
                .filter(entry -> !((String) entry.getKey()).startsWith("fr.inra.agrosyst") && !((String) entry.getKey()).startsWith("help"))
                .collect(Collectors.toMap(entry -> (String) entry.getKey(), entry -> (String) entry.getValue(), (e1, e2) -> e1, HashMap::new));
    }

    @EnumContext(contextEnum = CategoryObjective.class, context = "CYCLE_PLURIANNUEL_DE_CULTURE")
    public Map<CategoryObjective, String> enumCategoryObjectiveCycleCultures() {
        return getEnumAsMap(SectionType.CYCLE_PLURIANNUEL_DE_CULTURE.name(), CategoryObjective.values());
    }

    @EnumContext(contextEnum = CategoryObjective.class, context = "TRAVAIL_DU_SOL")
    public Map<CategoryObjective, String> enumCategoryObjectiveTravailSols() {
        return getEnumAsMap(SectionType.TRAVAIL_DU_SOL.name(), CategoryObjective.values());
    }

    @EnumContext(contextEnum = CategoryObjective.class, context = "ADVENTICES")
    public Map<CategoryObjective, String> enumCategoryObjectiveAdventives() {
        return getEnumAsMap(SectionType.ADVENTICES.name(), CategoryObjective.values());
    }

    @EnumContext(contextEnum = CategoryObjective.class, context = "MALADIES")
    public Map<CategoryObjective, String> enumCategoryObjectiveMaladies() {
        return getEnumAsMap(SectionType.MALADIES.name(), CategoryObjective.values());
    }

    @EnumContext(contextEnum = CategoryObjective.class, context = "RAVAGEURS")
    public Map<CategoryObjective, String> enumCategoryObjectiveRavageurs() {
        return getEnumAsMap(SectionType.RAVAGEURS.name(), CategoryObjective.values());
    }

    @EnumContext(contextEnum = CategoryObjective.class, context = "MAITRISE_DES_DOMMAGES_PHYSIQUES")
    public Map<CategoryObjective, String> enumCategoryObjectiveMaitriseDommages() {
        return getEnumAsMap(SectionType.MAITRISE_DES_DOMMAGES_PHYSIQUES.name(), CategoryObjective.values());
    }

    @EnumContext(contextEnum = CategoryObjective.class, context = "FERTILITE_SOL_CULTURES")
    public Map<CategoryObjective, String> enumCategoryObjectiveFerticiliteSols() {
        return getEnumAsMap(SectionType.FERTILITE_SOL_CULTURES.name(), CategoryObjective.values());
    }

    @EnumContext(contextEnum = CategoryObjective.class, context = "PRODUCTION")
    public Map<CategoryObjective, String> enumCategoryObjectiveProductions() {
        return getEnumAsMap(SectionType.PRODUCTION.name(), CategoryObjective.values());
    }

    @EnumContext(contextEnum = PressureScale.class, context = "Adventice")
    public Map<PressureScale, String> enumPressureScaleAdventice() {
        return getEnumAsMap("Adventice", PressureScale.values());
    }

    @EnumContext(contextEnum = PressureScale.class, context = "RegionPest")
    public Map<PressureScale, String> enumPressureScaleRegionPest() {
        return getEnumAsMap("RegionPest", PressureScale.values());
    }

    @EnumContext(contextEnum = PressureScale.class, context = "VitiAdventice")
    public Map<PressureScale, String> enumPressureScaleVitiAdventice() {
        return getEnumAsMap("VitiAdventice", PressureScale.values());
    }

    @EnumContext(contextEnum = MasterScale.class, context = "Adventice")
    public Map<MasterScale, String> enumMasterScaleAdventice() {
        return getEnumAsMap("Adventice", MasterScale.values());
    }

    @EnumContext(contextEnum = MasterScale.class, context = "Pest")
    public Map<MasterScale, String> enumMasterScaleArboPest() {
        return getEnumAsMap("Pest", MasterScale.values());
    }

    @EnumContext(contextEnum = MasterScale.class, context = "ArboAdventice")
    public Map<MasterScale, String> enumMasterScaleArboAdventice() {
        return getEnumAsMap("ArboAdventice", MasterScale.values());
    }

    @EnumContext(contextEnum = MasterScale.class, context = "ArboAdventiceExpe")
    public Map<MasterScale, String> enumMasterScaleArboAdventiceExpe() {
        return getEnumAsMap("ArboAdventiceExpe", MasterScale.NONE, MasterScale.LOW, MasterScale.HIGH);
    }

    @EnumContext(contextEnum = MasterScale.class, context = "Verse")
    public Map<MasterScale, String> enumMasterScaleVerse() {
        return getEnumAsMap("Verse", MasterScale.values());
    }

    @EnumContext(contextEnum = MasterScale.class, context = "VitiDisease")
    public Map<MasterScale, String> enumMasterScaleArboVitiDisease() {
        return getEnumAsMap("VitiDisease", MasterScale.values());
    }

    @EnumContext(contextEnum = MasterScale.class, context = "VitiPest")
    public Map<MasterScale, String> enumMasterScaleArboVitiPest() {
        return getEnumAsMap("VitiPest", MasterScale.values());
    }

    @EnumContext(contextEnum = YieldLossCause.class, context = "All")
    public Map<YieldLossCause, String> enumYieldLossCauseAll() {
        return getEnumAsMap(null,
                YieldLossCause.MALADIES,
                YieldLossCause.RAVAGEURS,
                YieldLossCause.ADVENTICES,
                YieldLossCause.VERSE,
                YieldLossCause.GEL,
                YieldLossCause.GRELE,
                YieldLossCause.STRESS_HYDRIQUE,
                YieldLossCause.ECHAUDAGE,
                YieldLossCause.STRESS_CLIMATIQUE,
                YieldLossCause.NUTRITION_MINERALE,
                YieldLossCause.TEMPETES_ET_CYCLONES
        );
    }

    @EnumContext(contextEnum = YieldLossCause.class, context = "ArboExpe")
    public Map<YieldLossCause, String> enumYieldLossCauseArboExpe() {
        return getEnumAsMap(null,
                YieldLossCause.MALADIES,
                YieldLossCause.RAVAGEURS,
                YieldLossCause.COULURE,
                YieldLossCause.GEL,
                YieldLossCause.GRELE,
                YieldLossCause.MORTALITE_MALADIE_BOIS,
                YieldLossCause.STRESS_HYDRIQUE,
                YieldLossCause.WIND,
                YieldLossCause.BUD_FERTILITY,
                YieldLossCause.ECHAUDAGE,
                YieldLossCause.DEFAUTS_EPIDERME,
                YieldLossCause.COUPS_CHALEUR
        );
    }

    @EnumContext(contextEnum = YieldLossCause.class, context = "ArboViti")
    public Map<YieldLossCause, String> enumYieldLossCauseArboViti() {
        return getEnumAsMap(null,
                YieldLossCause.MALADIES,
                YieldLossCause.RAVAGEURS,
                YieldLossCause.COULURE,
                YieldLossCause.GEL,
                YieldLossCause.GRELE,
                YieldLossCause.MORTALITE_MALADIE_BOIS,
                YieldLossCause.STRESS_HYDRIQUE,
                YieldLossCause.WIND,
                YieldLossCause.BUD_FERTILITY,
                YieldLossCause.ECHAUDAGE
        );
    }

    @EnumContext(contextEnum = GlobalMasterLevelQualifier.class, context = "ArboNotExpe")
    public Map<GlobalMasterLevelQualifier, String> enumGlobalMasterLevelQualifierarboNotExpe() {
        return getEnumAsMap("ArboNotExpe",
                GlobalMasterLevelQualifier.SATISFAIT,
                GlobalMasterLevelQualifier.PAS_SATISFAIT
        );
    }

    public ReferentialTranslationMap fillCroppingPlanEntryTranslationMaps(
            Collection<CroppingPlanEntry> croppingPlanEntries) {

        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(getSecurityContext().getLanguage());
        fillRefEspeceTranslations(getRefEspeceIds(croppingPlanEntries), translationMap);

        return translationMap;
    }

    public void fillTranslationMap(Collection<AbstractDomainInputStockUnit> domainInputs, ReferentialTranslationMap translationMap) {
        Set<String> refFertiMinUNIFAIds = domainInputs.stream()
                .filter(input -> input instanceof DomainMineralProductInput)
                .map(input -> ((DomainMineralProductInput) input).getRefInput().getTopiaId())
                .collect(Collectors.toSet());
        refFertiMinUNIFADao.fillTranslations(refFertiMinUNIFAIds, translationMap);

        Set<String> refFertiOrgaIds = domainInputs.stream()
                .filter(input -> input instanceof DomainOrganicProductInput)
                .map(input -> ((DomainOrganicProductInput) input).getRefInput().getTopiaId())
                .collect(Collectors.toSet());
        refFertiOrgaDao.fillTranslations(refFertiOrgaIds, translationMap);

        Set<String> refPotsIds = domainInputs.stream()
                .filter(input -> input instanceof DomainPotInput)
                .map(input -> ((DomainPotInput) input).getRefInput().getTopiaId())
                .collect(Collectors.toSet());
        refPotDao.fillTranslations(refPotsIds, translationMap);

        Set<String> refSubstrateIds = domainInputs.stream()
                .filter(input -> input instanceof DomainSubstrateInput)
                .map(input -> ((DomainSubstrateInput) input).getRefInput().getTopiaId())
                .collect(Collectors.toSet());
        refSubstrateDao.fillTranslations(refSubstrateIds, translationMap);

        Set<String> refOtherInputIds = domainInputs.stream()
                .filter(input -> input instanceof DomainOtherInput)
                .map(input -> ((DomainOtherInput) input).getRefInput().getTopiaId())
                .collect(Collectors.toSet());
        refOtherInputDao.fillTranslations(refOtherInputIds, translationMap);
    }

    public ReferentialTranslationMap fillCroppingPlanEntryTranslationMaps(
            Collection<CroppingPlanEntry> croppingPlanEntries,
            ReferentialTranslationMap translationMap) {

        fillRefEspeceTranslations(getRefEspeceIds(croppingPlanEntries), translationMap);

        return translationMap;
    }

    protected Set<String> getRefEspeceIds(Collection<CroppingPlanEntry> croppingPlanEntries) {
        Set<String> speciesIds = croppingPlanEntries.stream()
                .filter(CroppingPlanEntry::isCroppingPlanSpeciesNotEmpty)
                .map(CroppingPlanEntry::getCroppingPlanSpecies)
                .flatMap(Collection::stream)
                .map(CroppingPlanSpecies::getSpecies)
                .filter(Objects::nonNull)
                .map(TopiaEntity::getTopiaId)
                .collect(Collectors.toSet());
        return speciesIds;
    }

    protected List<I18nDaoHelper> getI18nDaoHelpers(Language language, String entityAlias) {
        return Lists.newArrayList(
                I18nDaoHelper.withSimpleI18nKey(RefEspece.PROPERTY_LIBELLE_ESPECE_BOTANIQUE, language, TradRefVivant.class, entityAlias),
                I18nDaoHelper.withSimpleI18nKey(RefEspece.PROPERTY_LIBELLE_QUALIFIANT__AEE, language, TradRefVivant.class, entityAlias),
                I18nDaoHelper.withSimpleI18nKey(RefEspece.PROPERTY_LIBELLE_TYPE_SAISONNIER__AEE, language, TradRefVivant.class, entityAlias),
                I18nDaoHelper.withSimpleI18nKey(RefEspece.PROPERTY_LIBELLE_DESTINATION__AEE, language, TradRefVivant.class, entityAlias)
        );
    }

    public void fillRefEspeceTranslations(Collection<String> refEspeceIds, ReferentialTranslationMap translationMap) {
        List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(translationMap.getLanguage(), "re");
        I18nDaoHelper.fillRefEntitiesTranslations(refEspeceDao, "re", i18nDaoHelpers, refEspeceIds, translationMap);
    }

    public void fillRefStadeEdiTranslations(Collection<String> refStadeEdiIds, ReferentialTranslationMap translationMap) {
        ArrayList<I18nDaoHelper> i18nDaoHelpers = Lists.newArrayList(
                I18nDaoHelper.withComplexI18nKey(RefStadeEDI.PROPERTY_COLONNE2, Language.fromTrigram(translationMap.getLanguage().getTrigram()), TradRefDivers.class, "st"));

        I18nDaoHelper.fillRefEntitiesTranslations(refStadeEDIDao, "st", i18nDaoHelpers, refStadeEdiIds, translationMap);
    }

    public void translateDomainSpecies(Collection<Domain> domains) {

        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryDao.findCroppingPlanEntriesForAllCampaignsDomains(domains);
        translateCroppingPlanEntry(croppingPlanEntries);
    }

    public void translateCroppingPlanEntry(List<CroppingPlanEntry> croppingPlanEntries) {

        ReferentialTranslationMap translationMap = fillCroppingPlanEntryTranslationMaps(croppingPlanEntries);

        croppingPlanEntries
                .forEach(croppingPlanEntry -> CroppingPlans.translateCroppingPlanEntry(croppingPlanEntry, translationMap));
    }

    public void translateGrowingSystems(Collection<GrowingSystem> growingSystems) {
        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(getSecurityContext().getLanguage());

        Set<String> typeAgricultureIds = growingSystems.stream()
                .map(GrowingSystem::getTypeAgriculture)
                .filter(Objects::nonNull)
                .map(TopiaEntity::getTopiaId)
                .collect(Collectors.toSet());

        refTypeAgricultureDao.fillTranslations(typeAgricultureIds, translationMap);

        growingSystems.forEach(growingSystem -> {
            RefTypeAgriculture typeAgriculture = growingSystem.getTypeAgriculture();
            if (typeAgriculture != null) {
                ReferentialEntityTranslation typeAgricultureTranslation = translationMap.getEntityTranslation(typeAgriculture.getTopiaId());
                typeAgriculture.setReference_label_Translated(
                        typeAgricultureTranslation.getPropertyTranslation(
                                RefTypeAgriculture.PROPERTY_REFERENCE_LABEL, typeAgriculture.getReference_label()
                        )
                );
            }
        });
    }

    public static String getTranslatedMonth(int monthIndex, Locale locale) {
        String month = new DateFormatSymbols(locale).getMonths()[monthIndex];
        return StringUtils.capitalize(month);
    }

    public void fillRefSolArvalisTranslations(Language language, Collection<RefSolArvalis> sols) {
        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(language);
        List<String> topiaIds = sols.stream().map(RefSolArvalis::getTopiaId).toList();
        fillRefSolArvalisTranslations(topiaIds, translationMap);
        sols.forEach(s -> {
            ReferentialEntityTranslation t = translationMap.getEntityTranslation(s.getTopiaId());
            s.setSol_nom_Translated(t.getPropertyTranslation(RefSolArvalis.PROPERTY_SOL_NOM, s.getSol_nom()));
            s.setSol_texture_Translated(t.getPropertyTranslation(RefSolArvalis.PROPERTY_SOL_TEXTURE, s.getSol_texture()));
            s.setSol_calcaire_Translated(t.getPropertyTranslation(RefSolArvalis.PROPERTY_SOL_CALCAIRE, s.getSol_calcaire()));
            s.setSol_profondeur_Translated(t.getPropertyTranslation(RefSolArvalis.PROPERTY_SOL_PROFONDEUR, s.getSol_profondeur()));
            s.setSol_hydromorphie_Translated(t.getPropertyTranslation(RefSolArvalis.PROPERTY_SOL_HYDROMORPHIE, s.getSol_hydromorphie()));
            s.setSol_pierrosite_Translated(t.getPropertyTranslation(RefSolArvalis.PROPERTY_SOL_PIERROSITE, s.getSol_pierrosite()));
        });
    }

    public <T extends TopiaEntity> void fillRefSolArvalisTranslations(
            Collection<String> topiaIdList,
            ReferentialTranslationMap translationMap) {

        List<I18nDaoHelper> i18nDaoHelpers = getI18nRefSolArvalisDaoHelpers(translationMap.getLanguage(), "sa");

        I18nDaoHelper.fillRefEntitiesTranslations(refSolArvalisDao, "sa", i18nDaoHelpers, topiaIdList, translationMap);
    }

    public void fillActionTranslations(List<AbstractAction> actions) {
        List<String> agrosystTravailEdiIds = actions.stream()
                .map(AbstractAction::getMainAction)
                .filter(Objects::nonNull)
                .map(TopiaEntity::getTopiaId)
                .toList();
        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(getSecurityContext().getLanguage());
        refInterventionAgrosystTravailEdiDao.fillTranslations(agrosystTravailEdiIds, translationMap);
        AgrosystI18nService.translateActions(actions, translationMap);
    }

    public void fillToolsCouplingTraductions(List<ToolsCoupling> toolsCouplings) {
        Set<String> refMaterielTopiaIds = Equipments.getRefMaterielTopiaIds(toolsCouplings);
        List<String> agrosystTravailEdiIds = toolsCouplings.stream()
                .map(ToolsCoupling::getMainsActions)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(TopiaEntity::getTopiaId)
                .toList();

        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(getSecurityContext().getLanguage());
        refMaterielTopiaDao.fillTranslations(refMaterielTopiaIds, translationMap);
        refInterventionAgrosystTravailEdiDao.fillTranslations(agrosystTravailEdiIds, translationMap);
        AgrosystI18nService.translateToolsCouplings(toolsCouplings, translationMap);
    }

    public static List<I18nDaoHelper> getI18nRefSolArvalisDaoHelpers(Language language, String entityAlias) {
        return Lists.newArrayList(
                I18nDaoHelper.withSimpleI18nKey(RefSolArvalis.PROPERTY_SOL_NOM, language, TradRefSol.class, entityAlias),
                I18nDaoHelper.withComplexI18nKey(RefSolArvalis.PROPERTY_SOL_TEXTURE, language, TradRefSol.class, entityAlias),
                I18nDaoHelper.withComplexI18nKey(RefSolArvalis.PROPERTY_SOL_CALCAIRE, language, TradRefSol.class, entityAlias),
                I18nDaoHelper.withComplexI18nKey(RefSolArvalis.PROPERTY_SOL_PROFONDEUR, language, TradRefSol.class, entityAlias),
                I18nDaoHelper.withComplexI18nKey(RefSolArvalis.PROPERTY_SOL_HYDROMORPHIE, language, TradRefSol.class, entityAlias),
                I18nDaoHelper.withComplexI18nKey(RefSolArvalis.PROPERTY_SOL_PIERROSITE, language, TradRefSol.class, entityAlias)
        );
    }

    public static void translateMateriel(RefMateriel materiel, ReferentialTranslationMap translationMap) {
        if (materiel == null) {
            return;
        }
        ReferentialEntityTranslation materielTranslation = translationMap.getEntityTranslation(materiel.getTopiaId());
        materiel.setTypeMateriel1_Translated(materielTranslation.getPropertyTranslation(RefMateriel.PROPERTY_TYPE_MATERIEL1, materiel.getTypeMateriel1()));
        materiel.setTypeMateriel2_Translated(materielTranslation.getPropertyTranslation(RefMateriel.PROPERTY_TYPE_MATERIEL2, materiel.getTypeMateriel2()));
        materiel.setTypeMateriel3_Translated(materielTranslation.getPropertyTranslation(RefMateriel.PROPERTY_TYPE_MATERIEL3, materiel.getTypeMateriel3()));
        materiel.setTypeMateriel4_Translated(materielTranslation.getPropertyTranslation(RefMateriel.PROPERTY_TYPE_MATERIEL4, materiel.getTypeMateriel4()));
        materiel.setUnite_Translated(materielTranslation.getPropertyTranslation(RefMateriel.PROPERTY_UNITE, materiel.getUnite()));
    }

    public static void translateEquipments(Collection<Equipment> equipments, ReferentialTranslationMap translationMap) {
        if (equipments == null) {
            return;
        }
        equipments.forEach(e -> translateMateriel(e.getRefMateriel(), translationMap));
    }

    public static void translateAgrosystActionEDI(RefInterventionAgrosystTravailEDI actionAgrosystEDI, ReferentialTranslationMap translationMap) {
        if (actionAgrosystEDI == null) {
            return;
        }
        ReferentialEntityTranslation materielTranslation = translationMap.getEntityTranslation(actionAgrosystEDI.getTopiaId());
        actionAgrosystEDI.setReference_label_Translated(
                materielTranslation.getPropertyTranslation(
                        RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_LABEL, actionAgrosystEDI.getReference_label()));
    }

    public static void translateMainActions(Collection<RefInterventionAgrosystTravailEDI> actionEDIs, ReferentialTranslationMap translationMap) {
        if (actionEDIs == null) {
            return;
        }
        actionEDIs.forEach(a -> translateAgrosystActionEDI(a, translationMap));
    }

    public static void translateToolsCouplings(Collection<ToolsCoupling> toolsCouplings, ReferentialTranslationMap translationMap) {
        toolsCouplings.forEach(tc -> translateEquipments(tc.getEquipments(), translationMap));
        toolsCouplings.forEach(tc -> translateMainActions(tc.getMainsActions(), translationMap));
    }

    public static void translateActions(Collection<AbstractAction> actions, ReferentialTranslationMap translationMap) {
        actions.forEach(a -> translateAgrosystActionEDI(a.getMainAction(), translationMap));
    }
}
