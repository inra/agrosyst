package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.performance.CropWithSpecies;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePlotExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.performance.PracticedCropPath;
import fr.inra.agrosyst.api.services.performance.Scenario;
import fr.inra.agrosyst.api.services.performance.ValorisationKey;
import fr.inra.agrosyst.api.services.performance.utils.PriceConverterKeysToRate;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingFieldMessage;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * Formule de calcul:
 *
 * <pre>
 * Produit brut standardisé avec l’autoconsommation:
 *
 * = PSCi * sum (Rendement(EVD) * Prix de vente pour le scénario(EVD) *  (% 𝑐𝑜𝑚𝑚𝑒𝑟𝑐𝑖𝑎𝑙𝑖𝑠é + % 𝑎𝑢𝑡𝑜𝑐𝑜𝑛𝑠𝑜𝑚𝑚é ) * TxUnitConverter))
 *
 * PSCi: (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i.
 * PSCi est calculé sur la base de données saisies par l’utilisateur.
 * Rendement(EVD): (diverses unités) : Rendement déclaré pour le couple EVD de l’action de type Récolte de l’intervention i.
 * Prix de vente pour le scénario(EVD): (diverses unités) : Prix de vente du couple EVD. Donnée saisie par l’utilisateur.
 *
 *
 * </pre>
 * <p>
 * Created by davidcosse on 28/09/16.
 */
public class IndicatorGrossIncomeForScenarios extends Indicator {

    private static final Log LOGGER = LogFactory.getLog(IndicatorGrossIncomeForScenarios.class);
    public static final String PRODUIT_BRUT_STANDARDISE =  "Produit brut standardisé";
    protected static final String WITHOUT_AUTO_CONSUME = " sans autoconsommation (€/ha)";
    public static final String WITH_AUTO_CONSUME = " avec autoconsommation (€/ha)";
    protected static final int ALL_PERIODS = -1;
    
    protected final Map<CroppingPlanEntry, Double> perennialCropCyclePercentByCrop = new HashMap<>();
    
    protected final Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> effectiveCropsYealdAverage = new HashMap<>();
    
    protected final Map<EffectiveItkCropCycleScaleKey, Map<Scenario, Double[]>> effectiveItkCropPrevCropPhaseValues = new HashMap<>();
    protected final Map<EffectiveItkCropCycleScaleKey, Integer> effectiveItkCropPrevCropPhaseTotalCounter = new HashMap<>();
    protected final Map<EffectiveItkCropCycleScaleKey, Integer> effectiveItkCropPrevCropPhaseFieldErrorCounter = new HashMap<>();
    protected final Map<EffectiveItkCropCycleScaleKey, Set<MissingFieldMessage>> effectiveItkCropPrevCropPhaseFieldsErrors = new HashMap<>();
    
    protected final Map<EffectiveCropCycleScaleKey, Map<Scenario, Double[]>> effectiveCroppingValues = new HashMap<>();
    protected final Map<EffectiveCropCycleScaleKey, Integer> effectiveCroppingReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<EffectiveCropCycleScaleKey, Integer> effectiveCroppingReliabilityTotalCounter = new HashMap<>();
    protected final Map<EffectiveCropCycleScaleKey, Set<MissingFieldMessage>> effectiveCroppingFieldsErrors = new HashMap<>();
    
    protected final Map<Zone, Map<Scenario, Double[]>> effectiveZoneValues = new HashMap<>();
    protected final Map<Zone, Integer> effectiveZoneReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<Zone, Integer> effectiveZoneReliabilityTotalCounter = new HashMap<>();
    protected final Map<Zone, Set<MissingFieldMessage>> effectiveZoneFieldsErrors = new HashMap<>();
    
    protected final Map<Plot, Map<Scenario, Double[]>> effectivePlotValues = new HashMap<>();
    protected final Map<Plot, Integer> effectivePlotReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<Plot, Integer> effectivePlotReliabilityTotalCounter = new HashMap<>();
    
    protected final Map<Optional<GrowingSystem>, Map<Scenario, Double[]>> effectiveGrowingSystemValues = new HashMap<>();
    protected final Map<Optional<GrowingSystem>, Integer> effectiveGrowingSystemReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<Optional<GrowingSystem>, Integer> effectiveGrowingSystemReliabilityTotalCounter = new HashMap<>();
    
    protected final Map<PracticedSystemScaleKey, Map<Scenario, Double[]>> practicedSystemsValues = new HashMap<>();
    protected final Map<PracticedSystemScaleKey, Set<MissingFieldMessage>> practicedSystemFieldsErrors = new HashMap<>();
    protected final Map<PracticedSystemScaleKey, Integer> reliabilityIndexPracticedSystemValuesTotalCounter = new HashMap<>();
    protected final Map<PracticedSystemScaleKey, Integer> reliabilityIndexPracticedSystemValuesErrorCounter = new HashMap<>();
    
    protected final Map<String, Scenario> scenariosByCode = new HashMap<>();
    protected final List<String> scenarioCodes = new ArrayList<>();
    
    protected boolean isWithAutoConsumed = false;
    protected boolean isWithoutAutoConsumed = false;
    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);
    
    public IndicatorGrossIncomeForScenarios() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, "produit_brut_std_scenarios_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, "produit_brut_std_scenarios_detail_champs_non_renseig");
    }
    
    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        for (Scenario scenario : scenariosByCode.values()) {
            String label = getPrintableScenarioLabel(scenario);
            final int scenarioCodeIndex = scenarioCodes.indexOf(scenario.code());
            if (scenarioCodeIndex != -1) {
                String scenarioIndexForDbColumn = StringUtils.leftPad(String.valueOf(scenarioCodeIndex + 1), 2, '0');
                indicatorNameToColumnName.put(getScenarioLabel(WITHOUT_AUTO_CONSUME, label),
                        String.format("produit_brut_std_scenario_%s_sans_autoconso", scenarioIndexForDbColumn));
                indicatorNameToColumnName.put(getScenarioLabel(WITH_AUTO_CONSUME, label),
                        String.format("produit_brut_std_scenario_%s_avec_autoconso", scenarioIndexForDbColumn));
            } else {
                LOGGER.error("Scénario non trouvé: '" + scenario.code() + "'");
            }
        }
        return indicatorNameToColumnName;
    }
    
    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return isWithoutAutoConsumed || isWithAutoConsumed;
    }
    
    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return isWithoutAutoConsumed || isWithAutoConsumed;
    }
    
    /**
     * return by refHarvestingPrices a pair of result with and result without auto consume
     */
    private Map<Scenario, Double[]> computeIndicator(
            PriceConverterKeysToRate priceConverterKeysToRate,
            MultiValuedMap<String, RefHarvestingPrice> refHarvestingPricesByValorisationKey,
            MultiValuedMap<ValorisationKey, HarvestingActionValorisation> interventionValorisationByValorisationKey,
            CroppingPlanEntry crop,
            Double psci,
            String interventionId) {
        
        boolean computeDone = false;

        boolean averageYealdsInsteadOfSum = crop.isCroppingPlanSpeciesNotEmpty()
                && crop.sizeCroppingPlanSpecies() > 1
                && !crop.isMixSpecies()
                && !crop.isMixVariety();

        Set<String> actionSpeciesCodes = interventionValorisationByValorisationKey.values().stream()
                .map(HarvestingActionValorisation::getSpeciesCode)
                .collect(Collectors.toSet());

        double totalAreaRatio;
        Map<String, Integer> speciesAreasByCode;

        if (averageYealdsInsteadOfSum && crop.isCroppingPlanSpeciesNotEmpty()) {
    
            speciesAreasByCode = crop.getCroppingPlanSpecies().stream()
                    .filter(species -> actionSpeciesCodes.contains(species.getCode()))
                    .collect(Collectors.toMap(CroppingPlanSpecies::getCode, croppingPlanSpecies -> {
                        Integer speciesArea = croppingPlanSpecies.getSpeciesArea();
                        incrementAngGetTotalFieldCounterForTargetedId(interventionId);
                        if (speciesArea == null) {
                            addMissingFieldMessage(interventionId, messageBuilder.getMissingSurfaceMessage(croppingPlanSpecies));
                            speciesArea = 100 / actionSpeciesCodes.size();
                        }
                        return speciesArea;
                    }));

            int areaSum = speciesAreasByCode.values().stream()
                    .mapToInt(a -> a)
                    .sum();
            totalAreaRatio = (double) 100 / areaSum;
    
        } else {
            totalAreaRatio = 1;
            speciesAreasByCode = new HashMap<>();
        }
    
        // group all results for this valorisation by code for average computing
        Map<Scenario, ValorisationScenarioPrices> valorisationScenarioPricesByScenarioCode = new HashMap<>();
    
        for (ValorisationKey valorisationKey : interventionValorisationByValorisationKey.keySet()) {
        
            // all refPrices that match codeEspeceBotanique, CodeQualifiantAEE, CodeDestination, organic
            String key0 = valorisationKey.getKeyLevel0();
            Collection<RefHarvestingPrice> refHarvestingPrices = refHarvestingPricesByValorisationKey.get(key0);
        
            // all refPrices that match codeEspeceBotanique, ALL , CodeDestination, organic
            if (CollectionUtils.isEmpty(refHarvestingPrices)) {
                String key1 = valorisationKey.getKeyLevel1();
                refHarvestingPrices = refHarvestingPricesByValorisationKey.get(key1);
            }
        
            // all refPrices that match ALL, ALL , CodeDestination, organic
            if (CollectionUtils.isEmpty(refHarvestingPrices)) {
                String key2 = valorisationKey.getKeyLevel2();
                refHarvestingPrices = refHarvestingPricesByValorisationKey.get(key2);
            }
        
            if (CollectionUtils.isEmpty(refHarvestingPrices)) {
                String key2 = valorisationKey.getKeyLevel2();
                refHarvestingPrices = refHarvestingPricesByValorisationKey.get(key2);
            }
        
            if (CollectionUtils.isNotEmpty(refHarvestingPrices)) {
            
                Collection<HarvestingActionValorisation> valorisations = interventionValorisationByValorisationKey.get(valorisationKey);
            
                for (HarvestingActionValorisation valorisation : valorisations) {
                
                    if (valorisation == null || valorisation.getYealdAverage() == 0) {
                        continue;
                    }
                
                    List<Pair<Integer, Integer>> periodsDecades = getAllValorisationDecadesPeriods(valorisation);
                
                    double yealdAverage = valorisation.getYealdAverage() / 100d;
                    if (averageYealdsInsteadOfSum && speciesAreasByCode.containsKey(valorisation.getSpeciesCode())) {
                        int speciesArea = speciesAreasByCode.get(valorisation.getSpeciesCode());
                        yealdAverage = yealdAverage * speciesArea * totalAreaRatio / 100;
                    }
                
                    double salingPercent = valorisation.getSalesPercent() / 100d;
                    double selfConsumePersent = valorisation.getSelfConsumedPersent() / 100d;

                    for (RefHarvestingPrice refHarvestingPrice : refHarvestingPrices) {
    
                        String scenarioCode = refHarvestingPrice.getCode_scenario();
                        Scenario scenario = scenariosByCode.get(scenarioCode);
                        if (scenario != null) {

                            Integer dp = refHarvestingPrice.getMarketingPeriodDecade();
                            if (dp == null) {
                                dp = valorisation.getBeginMarketingPeriod();
                            }

                            int mp = refHarvestingPrice.getMarketingPeriod();

                            // Rendement(EVD) * Prix de vente pour le scénario(EVD) *  (% 𝑐𝑜𝑚𝑚𝑒𝑟𝑐𝑖𝑎𝑙𝑖𝑠é + % 𝑎𝑢𝑡𝑜𝑐𝑜𝑛𝑠𝑜𝑚𝑚é ) * TxUnitConverter
                            if (mp == ALL_PERIODS || periodsDecades.contains(Pair.of(dp, mp))) { // only consider refPrice for correct period
        
                                // scenario is valid
                                Double price = refHarvestingPrice.getPrice();
                                if (price != null) {
                                    PriceUnit priceUnit = refHarvestingPrice.getPriceUnit();
            
                                    computeDone = true;
            
                                    final String code_destination_a = valorisation.getDestination().getCode_destination_A();

                                    Optional<Double> conversionRate = priceConverterKeysToRate.getPriceConverterRate(code_destination_a, priceUnit);
    
                                    if (conversionRate.isEmpty() && !PricesService.DEFAULT_PRICE_UNIT.equals(priceUnit)) {
                                        addMissingRefConversionRateMessageMessage(interventionId);
                                    }

                                    double unitRateConverteur = conversionRate.orElse(Indicator.DEFAULT_PRICE_CONVERTION_RATE);
            
                                    // sellingPercent
                                    incrementAngGetTotalFieldCounterForTargetedId(interventionId);
                                    // selfConsumePersent
                                    incrementAngGetTotalFieldCounterForTargetedId(interventionId);
                                    // unitRateConverteur
                                    incrementAngGetTotalFieldCounterForTargetedId(interventionId);

                                    double currentValue = yealdAverage * price * (salingPercent + selfConsumePersent) * unitRateConverteur;
                                    double currentValueWithoutAutoConsume = yealdAverage * price * salingPercent * unitRateConverteur;

                                    // add price to tha gross Income prices for code with and without auto consume
                                    ValorisationScenarioPrices pricesForCode =
                                            valorisationScenarioPricesByScenarioCode.computeIfAbsent(scenario, k -> new ValorisationScenarioPrices());
                                    pricesForCode.addWithAutoConsumePrice(currentValue);
                                    pricesForCode.addWithoutAutoConsumePrice(currentValueWithoutAutoConsume);
                                }
                            }
                        }
                    }

                }
            }
        }
    
        Map<Scenario, Double[]> allValorisationsScenarioPricesByScenarioCode = computeAveragePrices(valorisationScenarioPricesByScenarioCode);

        addPsci(psci, interventionId, allValorisationsScenarioPricesByScenarioCode);
        
        return computeDone ? allValorisationsScenarioPricesByScenarioCode : null;
    }

    private Map<Scenario, Double[]> computeAveragePrices(Map<Scenario, ValorisationScenarioPrices> allInterventionPricesByScenario) {
    
        Map<Scenario, Double[]> validScenarioPricesByScenario = new HashMap<>();
        
        for (Scenario scenario : allInterventionPricesByScenario.keySet()) {

            ValorisationScenarioPrices allResultsForThisCode = allInterventionPricesByScenario.get(scenario);
    
            double averageWithAutoConsume = allResultsForThisCode.getWithAutoConsumePrices().stream()
                    .mapToDouble(Double::doubleValue)
                    .sum();
            double averageWithoutAutoConsume = allResultsForThisCode.getWithoutAutoConsumePrices().stream()
                    .mapToDouble(Double::doubleValue)
                    .sum();

            validScenarioPricesByScenario.put(scenario, new Double[]{ averageWithAutoConsume, averageWithoutAutoConsume });
        }
        
        return validScenarioPricesByScenario;
    }

    protected void addPsci(Double psci, String interventionId, Map<Scenario, Double[]> validScenarioPricesByScenario) {
    
        // PSCi * sum (Rendement(EVD)
        //      * Prix de vente pour le scénario(EVD)
        //      *  (% 𝑐𝑜𝑚𝑚𝑒𝑟𝑐𝑖𝑎𝑙𝑖𝑠é + (% 𝑎𝑢𝑡𝑜𝑐𝑜𝑛𝑠𝑜𝑚𝑚é (optional)) )
        //      * TxUnitConverter))
    
        for (Scenario scenario : validScenarioPricesByScenario.keySet()) {
            // scenario price
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
        
            Double[] prices = mults(validScenarioPricesByScenario.get(scenario), psci * 100d);
            validScenarioPricesByScenario.put(scenario, prices);
        }
    }

    protected Map<Scenario, Double[]> addPerennialCropPart(Double solOccupationPercent, Double totalSolOccupationPercent, Map<Scenario, Double[]> result) {

        Map<Scenario, Double[]> r = new HashMap<>(result.size());
        // calcul de la part des culture pérennes dans la surface du SDC
        double part = solOccupationPercent == null ? 1 : solOccupationPercent / 100;
        double sumPart = totalSolOccupationPercent == null ? 1 : totalSolOccupationPercent / 100;

        if(sumPart == 0) {
            // resultat non valid on ignore le calcul des part
            sumPart = 1;
            part = 1;
        }

        for (Scenario scenario : result.keySet()) {
            Double[] valueWithAutoconsumeAndWithoutAutoConsume = result.get(scenario);
            Double[] calculatedValueWithAutoConsume = divs(mults(valueWithAutoconsumeAndWithoutAutoConsume, part), sumPart);
            r.put(scenario, calculatedValueWithAutoConsume);
        }
        return r;
    }

    protected List<Pair<Integer, Integer>> getAllValorisationDecadesPeriods(HarvestingActionValorisation valorisation) {
        int beginPeriod = valorisation.getBeginMarketingPeriod();
        int endingPeriod = valorisation.getEndingMarketingPeriod();

        boolean isSamePeriod = endingPeriod == beginPeriod;

        List<Pair<Integer, Integer>> decadesPeriods = new ArrayList<>();
        
        int beginDecade = valorisation.getBeginMarketingPeriodDecade();
        
        if (isSamePeriod) {
            int endingDecade = valorisation.getEndingMarketingPeriodDecade();
            int diff = endingDecade - beginDecade;

            for (int numDecade = 0; numDecade <= diff; numDecade++) {
                decadesPeriods.add(Pair.of(beginDecade + numDecade, beginPeriod));
            }

        } else {
            while (beginDecade <= 4) {
                decadesPeriods.add(Pair.of(beginDecade++, beginPeriod));
            }

            int currentPeriod = beginPeriod + 1;
            while (currentPeriod <= endingPeriod) {
                for (int decade = 1; decade <= 4; decade++) {
                    decadesPeriods.add(Pair.of(decade, currentPeriod));
                }
                currentPeriod++;
            }

            int endingDecade = valorisation.getEndingMarketingPeriodDecade();
            for (int decade = 1; decade <= endingDecade; decade++) {
                decadesPeriods.add(Pair.of(decade, endingDecade));
            }
        }
        return decadesPeriods;
    }
    
    protected MultiValuedMap<ValorisationKey, HarvestingActionValorisation> getValorisationForKey(
            String interventionId,
            MultiValuedMap<String, Pair<String, String>> codeEspeceBotaniqueCodeQualifantBySpeciesCode,
            HarvestingAction harvestingAction) {
        
        MultiValuedMap<ValorisationKey, HarvestingActionValorisation> result = new HashSetValuedHashMap<>();
        
        Collection<HarvestingActionValorisation> valorisations = CollectionUtils.emptyIfNull(harvestingAction.getValorisations())
                .stream().filter(Objects::nonNull).toList();
        if (CollectionUtils.isNotEmpty(valorisations)) {
    
            logNonValidValorisations(valorisations, interventionId);
            
            boolean isEdaplosDefaultValorisations = isEdaplosDefaultValorisations(valorisations);

            if (isEdaplosDefaultValorisations) {
                // YealdAverage
                addMissingFieldMessage(interventionId, messageBuilder.getMissingValorisationDestinationMessage());

            } else {
                result = getCodeEspeceBotaniqueCodeQualifiantAeeDestinationValorisations(interventionId, codeEspeceBotaniqueCodeQualifantBySpeciesCode, valorisations);
            }
        }
    
        return result;
    }
    
    private MultiValuedMap<ValorisationKey, HarvestingActionValorisation> getCodeEspeceBotaniqueCodeQualifiantAeeDestinationValorisations(
            String interventionId,
            MultiValuedMap<String, Pair<String, String>> codeEspeceBotaniqueCodeQualifantBySpeciesCode,
            Collection<HarvestingActionValorisation> valorisations) {
        
        MultiValuedMap<ValorisationKey, HarvestingActionValorisation> result = new HashSetValuedHashMap<>();
        
        for (HarvestingActionValorisation valorisation : valorisations) {
            
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            boolean isOrganic = valorisation.isIsOrganicCrop();
            String codeDestination = valorisation.getDestination().getCode_destination_A();
            Collection<Pair<String, String>> speciesCodeEspeceBotaniqueCodeQualifiantAEE =
                    codeEspeceBotaniqueCodeQualifantBySpeciesCode.get(valorisation.getSpeciesCode());
            
            if (speciesCodeEspeceBotaniqueCodeQualifiantAEE != null) { //XXX ymartel 20171116 : sometimes, nothing retrieved from a speciesCode
                for (Pair<String, String> codeEspeceBotaniqueCodeQualifiantAEE : speciesCodeEspeceBotaniqueCodeQualifiantAEE) {
                    String codeEspeceBotanique = codeEspeceBotaniqueCodeQualifiantAEE.getLeft();
                    String codeQualifantAee = codeEspeceBotaniqueCodeQualifiantAEE.getRight();
                    ValorisationKey scenarioKey = new ValorisationKey(codeEspeceBotanique, codeQualifantAee, codeDestination, isOrganic);
                    result.put(scenarioKey, valorisation);
                }
            }
            
        }
        return result;
    }
    
    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_ECONOMIC);
    }
    
    /**
     *
     * Not used
     * @return null
     */
    @Override
    public String getIndicatorLabel(int i) {
        return null;
    }

    @Override
    public void computePracticed(IndicatorWriter writer,
                                 PerformanceGlobalExecutionContext globalExecutionContext,
                                 PerformanceGrowingSystemExecutionContext growingSystemContext,
                                 PerformancePracticedDomainExecutionContext domainContext) {
        
        if (growingSystemContext.getAnonymizeGrowingSystem().isEmpty()) {
            return;
        }
    
        Optional<GrowingSystem> optionalGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();
    
        GrowingSystem growingSystem = optionalGrowingSystem.get();
    
        String its = growingSystemContext.getIts();
        String irs = growingSystemContext.getIrs();

        Domain anonymiseDomain = domainContext.getAnonymiseDomain();
    
        for (PerformancePracticedSystemExecutionContext practicedSystemExecutionContext : growingSystemContext.getPracticedSystemExecutionContexts()) {
            
            MultiValuedMap<String, RefHarvestingPrice> allRefHarvestingPricesByKey = growingSystemContext
                    .getHarvestingScenarioPricesByKey()
                    .get(practicedSystemExecutionContext.getPracticedSystem().getTopiaId());
    
            computePracticedSeasonal(
                    practicedSystemExecutionContext,
                    allRefHarvestingPricesByKey,
                    writer,
                    domainContext,
                    growingSystemContext);
    
            computePracticedPerennial(
                    practicedSystemExecutionContext,
                    allRefHarvestingPricesByKey,
                    writer,
                    domainContext,
                    growingSystemContext);
    
            writePracticedSystemSheet(
                    writer,
                    growingSystem,
                    its,
                    irs,
                    anonymiseDomain,
                    practicedSystemExecutionContext
            );
    
            perennialCropCyclePercentByCrop.clear();
        }
    
    
    }
    
    protected void writePracticedSystemSheet(IndicatorWriter writer,
                                             GrowingSystem growingSystem,
                                             String its,
                                             String irs,
                                             Domain anonymiseDomain,
                                             PerformancePracticedSystemExecutionContext practicedSystemExecutionContext) {
    
        PracticedSystem anonymizePracticedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();
        // mise à l'échelle vers Practiced System, on tient compte de la part des cultures perennes dans le sdc
        PracticedSystem practicedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();
        String campaigns = practicedSystem.getCampaigns();
    
        PracticedSystemScaleKey practicedSystemScaleKey = new PracticedSystemScaleKey(campaigns, growingSystem, practicedSystem);
    
        Map<Scenario, Double[]> practicedSystemValues = practicedSystemsValues.get(practicedSystemScaleKey);
    
        if (practicedSystemValues != null) {
    
            for (Scenario scenario : practicedSystemValues.keySet()) {
                if (anonymizePracticedSystem.getGrowingSystem().getCode().equals(growingSystem.getCode())) { // filter on practiced system related to the current growing system

                    Double[] values = practicedSystemValues.get(scenario);

                    Integer psREC = reliabilityIndexPracticedSystemValuesErrorCounter.get(practicedSystemScaleKey);
                    Integer psRTC = reliabilityIndexPracticedSystemValuesTotalCounter.get(practicedSystemScaleKey);

                    Integer reliability = computeReliabilityIndex(psREC, psRTC);

                    String label = getPrintableScenarioLabel(scenario);

                    String comments = getMessagesForScope(MissingMessageScope.PRACTICED_SYSTEM);

                    // CA Practice System scale
                    if (isWithAutoConsumed) {
                        String scenarioLabel = getScenarioLabel(WITH_AUTO_CONSUME, label);
                        writer.writePracticedSystem(
                                its,
                                irs,
                                campaigns,
                                getIndicatorCategory(),
                                scenarioLabel,
                                values[0],
                                reliability,
                                comments,
                                anonymiseDomain,
                                growingSystem,
                                anonymizePracticedSystem,
                                this.getClass()
                        );
                    }
                    if (isWithoutAutoConsumed) {
                        String scenarioLabel = getScenarioLabel(WITHOUT_AUTO_CONSUME, label);
                        writer.writePracticedSystem(
                                its,
                                irs,
                                campaigns,
                                getIndicatorCategory(),
                                scenarioLabel,
                                values[1],
                                reliability,
                                comments,
                                anonymiseDomain,
                                growingSystem,
                                anonymizePracticedSystem,
                                this.getClass()
                        );
                    }
                }
            }
        }
    }
    
    private void writePracticedSeasonalCroppingValues(
            IndicatorWriter writer,
            PerformancePracticedDomainExecutionContext domainContext,
            GrowingSystem growingSystem,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter,
            MultiKeyMap<Object, Double> cumulativeFrequenciesByPracticedCropCycleConnectionCode,
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            MultiKeyMap<Object, Map<Scenario, Double[]>> practicedCroppingValues,
            Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealds,
            String its,
            String irs,
            PracticedSystem anonymizePracticedSystem,
            List<Map.Entry<MultiKey<?>, Map<Scenario, Double[]>>> practicedSeasonalCroppingValues,
            MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors) {
    
        for (Map.Entry<MultiKey<?>, Map<Scenario, Double[]>> practicedCroppingKeyValue : practicedSeasonalCroppingValues) {
            MultiKey<?> key = practicedCroppingKeyValue.getKey();
            String croppingPlanEntryCode = (String) key.getKey(0);
            String previousPlanEntryCode = (String) key.getKey(1);
        
            Map<Scenario, Double[]> values = practicedCroppingValues.get(key);
        
            CroppingPlanEntry croppingPlanEntry = domainContext.getCropByCampaignAndCode()
                    .get(domainContext.getDomain().getCampaign(), croppingPlanEntryCode);
            
            if (croppingPlanEntry == null) {
                CropWithSpecies cropWithSpecies = practicedSystemExecutionContext.getCropByCodeWithSpecies().get(croppingPlanEntryCode);
                croppingPlanEntry = cropWithSpecies != null ? cropWithSpecies.getCroppingPlanEntry() : null;
            }
            
            if (croppingPlanEntry == null) continue;// nothing to do
        
            CroppingPlanEntry previousCropEntry;
            previousCropEntry = domainContext.getCropByCampaignAndCode()
                    .get(domainContext.getDomain().getCampaign(), previousPlanEntryCode);
            
            if (previousCropEntry == null) {
                CropWithSpecies cropWithSpecies = practicedSystemExecutionContext
                        .getCropByCodeWithSpecies()
                        .get(previousPlanEntryCode);
                previousCropEntry = cropWithSpecies != null ? cropWithSpecies.getCroppingPlanEntry() : null;
            }
            
            Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage = cropsYealds.get(croppingPlanEntry.getCode());
            
            writePracticedSeasonalCropSheet(
                    writer,
                    cumulativeFrequenciesByPracticedCropCycleConnectionCode,
                    growingSystem,
                    irs,
                    its,
                    anonymizePracticedSystem,
                    key,
                    values,
                    croppingPlanEntry,
                    previousCropEntry,
                    cropYealdAverage,
                    practicedCroppingReliabilityTotalCounter,
                    practicedCroppingReliabilityFieldErrorCounter,
                    practicedCroppingFieldsErrors
            );
        }
    }
    
    private void writePracticedPerennialCroppingValues(
            IndicatorWriter writer,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            MultiKeyMap<Object, Map<Scenario, Double[]>> practicedCroppingValues,
            Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealds,
            GrowingSystem growingSystem,
            String its,
            String irs,
            PracticedSystem anonymizePracticedSystem,
            List<Map.Entry<MultiKey<?>, Map<Scenario, Double[]>>> practicedPerennialCroppingValues,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter, MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter,
            MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors) {
        
        for (Map.Entry<MultiKey<?>, Map<Scenario, Double[]>> practicedCroppingKeyValue : practicedPerennialCroppingValues) {
            MultiKey<?> practicedCroppingKey = practicedCroppingKeyValue.getKey();
            String croppingPlanEntryCode = (String) practicedCroppingKey.getKey(0);
            PracticedCropCyclePhase phase = (PracticedCropCyclePhase) practicedCroppingKey.getKey(1);
            
            Map<Scenario, Double[]> values = practicedCroppingValues.get(practicedCroppingKey);
            
            CroppingPlanEntry croppingPlanEntry = domainContext.getCropByCampaignAndCode()
                    .get(domainContext.getDomain().getCampaign(), croppingPlanEntryCode);
            
            if (croppingPlanEntry == null) {
                CropWithSpecies cropWithSpecies = practicedSystemExecutionContext.getCropByCodeWithSpecies().get(croppingPlanEntryCode);
                croppingPlanEntry = cropWithSpecies != null ? cropWithSpecies.getCroppingPlanEntry() : null;
            }
            
            if (croppingPlanEntry == null) continue;// nothing to do
            
            Map<Pair<RefDestination, YealdUnit>, Double> cropYealds = cropsYealds.get(croppingPlanEntry.getCode());
            
            writePracticedPerennialCropSheet(
                    writer,
                    anonymizePracticedSystem,
                    croppingPlanEntry,
                    phase,
                    values,
                    growingSystem,
                    its,
                    irs,
                    practicedCroppingKey,
                    cropYealds,
                    practicedCroppingReliabilityFieldErrorCounter,
                    practicedCroppingReliabilityTotalCounter,
                    practicedCroppingFieldsErrors);
        }
    }
    
    protected String getPrintableScenarioLabel(Scenario scenario) {
        return " - Scénario:" + scenario.label();
    }
    
    @Override
    public void computePracticed(IndicatorWriter writer, Domain domain) {
        
        if (isDisplayed(null, 0)) {// 0 as global
            Map<Scenario, Double[]> weightedValueSum = new LinkedHashMap<>();
            Map<Scenario, Double[]> weightSum = new LinkedHashMap<>();
        
            Map<Scenario, Pair<Integer, Integer>> domainScenarioRelaibilityCount = new LinkedHashMap<>();
        
            Map<String, Pair<Integer, Integer>> domainCampaignRelaibilityCount = new LinkedHashMap<>();
        
            List<String> gstc = new ArrayList<>();
    
            for (PracticedSystemScaleKey key : practicedSystemsValues.keySet()) {
                // key = campaigns, growingSystem, practicedSystem;
                String campaigns = key.campaigns();
                GrowingSystem growingSystem = key.growingSystem();
        
                if (growingSystem.getTypeAgriculture() != null) gstc.add(growingSystem.getTypeAgriculture().getReference_label());
        
                Map<Scenario, Double[]> values = practicedSystemsValues.get(key);
        
                Integer psREC = reliabilityIndexPracticedSystemValuesErrorCounter.get(key);
    
                Integer psRTC = reliabilityIndexPracticedSystemValuesTotalCounter.get(key);
    
                Pair<Integer, Integer> dcrc = domainCampaignRelaibilityCount.computeIfAbsent(campaigns, k -> Pair.of(psREC, psRTC));
    
                Double usedAgriculturalArea = domain.getUsedAgriculturalArea();
                Double affectedAreaRate = growingSystem.getAffectedAreaRate();
    
                Integer ef = dcrc.getLeft();
                if (usedAgriculturalArea == null) {
                    ef++;
                }
                if (affectedAreaRate == null) {
                    ef++;
                }
                Integer tf = dcrc.getRight() + 2;
    
                domainCampaignRelaibilityCount.put(campaigns, Pair.of(ef, tf));
    
                final String domainTopiaId = domain.getTopiaId();
                if (usedAgriculturalArea == null) {
                    addMissingFieldMessage(domainTopiaId, messageBuilder.getDomainMissingAffectedAreaMessage());
                }
    
                if (affectedAreaRate == null) {
                    addMissingFieldMessage(domainTopiaId, messageBuilder.getDomainMissingAffectedAreaMessage());
                }
        
                for (Scenario scenario : values.keySet()) {
        
                    domainScenarioRelaibilityCount.put(scenario, Pair.of(ef, tf));
                    
                    // somme des valeurs pondérée
                    weightedValueSum.putIfAbsent(scenario, new Double[]{ 0d, 0d });

                    // somme des poids
                    weightSum.putIfAbsent(scenario, new Double[]{ 0d, 0d });
                }

                if (usedAgriculturalArea == null) {
                    usedAgriculturalArea = DEFAULT_USED_AGRICULTURAL_AREA;
                }
                if (affectedAreaRate == null) {
                    affectedAreaRate = DEFAULT_AFFECTED_AREA_RATE;
                }

                for (Scenario scenario : values.keySet()) {
        
                    Double[] valueWithAndWithoutAutoConsume = values.get(scenario);
                    
                    final Double[] weightSumValuesForScenario = weightSum.get(scenario);
        
                    Double[] weightedValueSumWithAndWithoutAutoConsume = mults(valueWithAndWithoutAutoConsume, usedAgriculturalArea * affectedAreaRate);
                    Double[] weightedSumWithAndWithoutAutoConsume = mults(weightSumValuesForScenario, usedAgriculturalArea * affectedAreaRate);
    
                    weightedValueSum.put(scenario, weightedValueSumWithAndWithoutAutoConsume);
                    weightSum.put(scenario, weightedSumWithAndWithoutAutoConsume);
                }
    
            }
    
            String domainCampaign = String.valueOf(domain.getCampaign());
            
            String comments = getMessagesForScope(MissingMessageScope.DOMAIN);
        
            for (Scenario scenario : weightedValueSum.keySet()) {

                Double[] values = weightedValueSum.get(scenario);
                Double[] weightValues = weightSum.get(scenario);
        
                Pair<Integer, Integer> reliabilityErrorAndTotal = domainScenarioRelaibilityCount.get(scenario);
                Integer practicedDomainREC = reliabilityErrorAndTotal.getLeft();
                Integer practicedDomainRTC = reliabilityErrorAndTotal.getRight();
        
                Integer reliability = computeReliabilityIndex(practicedDomainREC, practicedDomainRTC);
        
                String label = getPrintableScenarioLabel(scenario);
        
                if (isWithAutoConsumed) {
                    if (weightValues[0] != 0) {
                        // domain scale
                        double weightedAverage = values[0] / weightValues[0];
                        String scenarioLabel = getScenarioLabel(WITH_AUTO_CONSUME, label);
                        writer.writePracticedDomain(
                                domainCampaign,
                                getIndicatorCategory(),
                                scenarioLabel,
                                weightedAverage,
                                reliability,
                                comments,
                                domain,
                                String.join(", ", gstc)
                        );
                    } else {
                        if (LOGGER.isWarnEnabled()) {
                            LOGGER.warn("Can't compute growing system scale with 0 weigth");
                        }
                    }
                }
        
                if (isWithoutAutoConsumed) {
                    if (weightValues[1] != 0) {
                        // domain scale
                        double weightedAverage = values[1] / weightValues[1];
                        String scenarioLabel = getScenarioLabel(WITHOUT_AUTO_CONSUME, label);
                        writer.writePracticedDomain(
                                domainCampaign,
                                getIndicatorCategory(),
                                scenarioLabel,
                                weightedAverage,
                                reliability,
                                comments,
                                domain,
                                String.join(", ", gstc)
                        );
                    } else {
                        if (LOGGER.isWarnEnabled()) {
                            LOGGER.warn("Can't compute growing system scale with 0 weigth");
                        }
                    }
                }
            }
        }
    }
    
    @Override
    public void resetEffectiveCC() {
        // cleanup for next iteration
        effectiveCroppingReliabilityTotalCounter.clear();
        effectiveCroppingReliabilityFieldErrorCounter.clear();
        effectiveCroppingValues.clear();
        effectiveCroppingFieldsErrors.clear();
    }
    
    @Override
    public void resetEffectiveGrowingSystems() {
        // cleanup for next iteration
        effectiveGrowingSystemValues.clear();
        effectiveGrowingSystemReliabilityFieldErrorCounter.clear();
        effectiveGrowingSystemReliabilityTotalCounter.clear();
        targetedErrorFieldMessages.clear();
    }
    
    @Override
    public void resetEffectivePlots() {
        // cleanup for next iteration
        effectivePlotValues.clear();
        effectivePlotReliabilityFieldErrorCounter.clear();
        effectivePlotReliabilityTotalCounter.clear();
    }
    
    @Override
    public void resetEffectiveZones() {
        // cleanup for next iteration
        effectiveZoneValues.clear();
        effectiveZoneReliabilityFieldErrorCounter.clear();
        effectiveZoneReliabilityTotalCounter.clear();
        effectiveZoneFieldsErrors.clear();
    }

    @Override
    public void resetPracticed(Domain domain) {
        // cleanup for next iteration
        reliabilityIndexPracticedSystemValuesTotalCounter.clear();
        reliabilityIndexPracticedSystemValuesErrorCounter.clear();
        practicedSystemsValues.clear();
        targetedErrorFieldMessages.clear();
    }
    
    protected void computePracticedSeasonal(
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            MultiValuedMap<String, RefHarvestingPrice> refHarvestingPricesByValorisationKey,
            IndicatorWriter writer,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext) {
        
        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSystemExecutionContext.getSeasonalCropCycle();
    
        Optional<GrowingSystem> optionalGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();
    
        if (optionalGrowingSystem.isEmpty()) {
            return;
        }
        
        // seasonal
        if (practicedSeasonalCropCycle == null) {
            return;
        }
        
        if (CollectionUtils.isEmpty(practicedSeasonalCropCycle.getCropCycleNodes())) {
            return;
        }
        
        Set<PerformancePracticedCropExecutionContext> performancePracticedCropContextExecutionContexts_ =
                practicedSystemExecutionContext.getPerformancePracticedCropContextExecutionContexts().stream()
                        .filter(
                                performanceCropExecutionContext ->
                                        Objects.isNull(performanceCropExecutionContext.getPracticedPerennialCropCycle()))
                        .collect(Collectors.toSet());
        
        if (CollectionUtils.isEmpty(performancePracticedCropContextExecutionContexts_)) {
            return;// no crop in cycle
        }
        
        List<PerformancePracticedCropExecutionContext> performancePracticedCropContextExecutionContexts = performancePracticedCropContextExecutionContexts_
                .stream()
                .sorted(Comparator.comparingInt(PerformancePracticedCropExecutionContext::getRank)).toList();
        
        PracticedSystem practicedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();
        
        String campaigns = practicedSystem.getCampaigns();
    
        GrowingSystem growingSystem = optionalGrowingSystem.get();
        
        PracticedSystemScaleKey practicedSystemScaleKey = new PracticedSystemScaleKey(campaigns, growingSystem, practicedSystem);
    
        // sum value on cycle and divide by campaigns count to get
        Map<PracticedCropCycleConnection, Map<Scenario, Double[]>> cycleValues = new HashMap<>();
        
        AtomicBoolean seasonalInterventionFound = new AtomicBoolean(false);
        
        for (PerformancePracticedCropExecutionContext cropContext : performancePracticedCropContextExecutionContexts) {
            
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter = new MultiKeyMap<>();
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter = new MultiKeyMap<>();
            MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors = new MultiKeyMap<>();
            MultiKeyMap<Object, Double> cumulativeFrequenciesByPracticedCropCycleConnectionCode = new MultiKeyMap<>();
            Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealdAverage = new HashMap<>();
            
            MultiKeyMap<Object, Map<Scenario, Double[]>> practicedCroppingValues = new MultiKeyMap<>();
    
            CroppingPlanEntry crop = cropContext.getCropWithSpecies().getCroppingPlanEntry();
            CroppingPlanEntry previousCrop = cropContext.getSeasonalPreviousCrop();
    
            String cropCode = crop.getCode();
    
            int rank = cropContext.getRank();
            PracticedCropCycleConnection cropConnection = cropContext.getConnection();
            String previousPlanEntryCode = previousCrop != null ? previousCrop.getCode() : "";
            
            cropContext.getInterventionExecutionContexts().stream()
                .filter(interventionContext -> !interventionContext.isFictive())
                .filter(interventionContext -> interventionContext.getOptionalHarvestingAction().isPresent())
                .filter(interventionContext -> interventionContext.getIntervention().getPracticedCropCycleConnection() != null)
                .forEach(interventionContext -> {
                
                HarvestingAction harvestingAction = interventionContext.getOptionalHarvestingAction().get();
                
                PracticedIntervention intervention = interventionContext.getIntervention();
                
                if (!interventionContext.isFictive()) {
                    seasonalInterventionFound.set(true);
                }
                
                String interventionId = intervention.getTopiaId();
                
                CroppingPlanEntry interCrop = interventionContext.getCropWithSpecies().getCroppingPlanEntry();
                
                MultiValuedMap<String, Pair<String, String>> codeEspeceBotaniqueCodeQualifantBySpeciesCode =
                        getCodeEspeceBotaniqueCodeQualifantBySpeciesCode(interCrop);
                
                MultiValuedMap<ValorisationKey, HarvestingActionValorisation> interventionValorisationByValorisationKey = getValorisationForKey(
                        interventionId,
                        codeEspeceBotaniqueCodeQualifantBySpeciesCode,
                        harvestingAction
                );
                Map<Scenario, Double[]> interValues = computeIndicatorForPracticedIntervention0(
                        interventionContext.getPriceConverterKeysToRate(),
                        intervention,
                        refHarvestingPricesByValorisationKey,
                        interventionValorisationByValorisationKey,
                        interCrop
                );
                
                if (interValues == null) {
                    interValues = new HashMap<>();
                }
                
                // cet indicateur n'est pas écrit à l'échelle de l'intervention
                // trow up to crop the missing fields warning
                Set<MissingFieldMessage> missingFieldWarnings = practicedCroppingFieldsErrors.get(
                        cropCode,
                        previousPlanEntryCode,
                        rank,
                        cropConnection
                );
                if (missingFieldWarnings == null) {
                    missingFieldWarnings = new HashSet<>();
                    practicedCroppingFieldsErrors.put(
                            cropCode,
                            previousPlanEntryCode,
                            rank,
                            cropConnection,
                            missingFieldWarnings
                    );
                }
                Set<MissingFieldMessage> interventionWarnings = targetedErrorFieldMessages.getOrDefault(interventionId, new HashSet<>());
                Set<MissingFieldMessage> cropMissingFieldMessages = interventionWarnings.stream()
                        .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.CROP).isPresent() ||
                                missingFieldMessage.getMessageForScope(MissingMessageScope.INTERVENTION).isPresent())
                        .collect(Collectors.toSet());
                if (CollectionUtils.isNotEmpty(cropMissingFieldMessages)) {
                    missingFieldWarnings.addAll(cropMissingFieldMessages);
                }
                
                // somme pour CC le cycle (ensemble des cultures du cycle)
                Map<Scenario, Double[]> previous = practicedCroppingValues.get(
                        cropCode,
                        previousPlanEntryCode,
                        rank,
                        cropConnection);
                
                if (previous == null) {
                    practicedCroppingValues.put(cropCode, previousPlanEntryCode, rank, cropConnection, new HashMap<>(interValues));
                } else {
                    for (Scenario scenario : interValues.keySet()) {
                        previous.merge(scenario, interValues.get(scenario), GenericIndicator::sum);
                    }
                }
                
                // somme pour la culture seulement (toutes les interventions de la culture)
                PracticedCropCycleConnection connection = intervention.getPracticedCropCycleConnection();
                Map<Scenario, Double[]> previousValue = cycleValues.get(connection);
                
                if (previousValue == null) {
                    cycleValues.put(connection, new HashMap<>(interValues));
                    if (!interventionContext.isFictive() && intervention.isIntermediateCrop()) {
                        cropsYealdAverage.put(cropCode, cropContext.getIntermediateCropYealds());
                    } else {
                        cropsYealdAverage.put(cropCode, cropContext.getMainCropYealds());
                    }
                } else {
                    for (Scenario scenario : interValues.keySet()) {
                        previousValue.merge(scenario, interValues.get(scenario), GenericIndicator::sum);
                    }
                }
                
                // somme pour CC
                Integer prevReliabilityTotalCount = practicedCroppingReliabilityTotalCounter.get(
                        cropCode,
                        previousPlanEntryCode,
                        rank,
                        cropConnection
                );
                
                int interventionFieldsCounter = getTotalFieldCounterValueForTargetedId(interventionId);
                
                if (prevReliabilityTotalCount == null) {
                    
                    practicedCroppingReliabilityTotalCounter.put(
                            cropCode,
                            previousPlanEntryCode,
                            rank,
                            cropConnection,
                            interventionFieldsCounter
                    );
                } else {
                    practicedCroppingReliabilityTotalCounter.put(
                            cropCode,
                            previousPlanEntryCode,
                            rank,
                            cropConnection,
                            interventionFieldsCounter + prevReliabilityTotalCount
                    );
                }
                
                Integer prevReliabilityErrorCount = practicedCroppingReliabilityFieldErrorCounter.get(
                        cropCode,
                        previousPlanEntryCode,
                        rank,
                        cropConnection
                );
                
                int interventionMissingFieldCounter = getMissingFieldCounterValueForTargetedId(interventionId);
                if (prevReliabilityErrorCount == null) {
                    practicedCroppingReliabilityFieldErrorCounter.put(
                            cropCode,
                            previousPlanEntryCode,
                            rank,
                            cropConnection,
                            interventionMissingFieldCounter
                    );
                } else {
                    practicedCroppingReliabilityFieldErrorCounter.put(
                            cropCode,
                            previousPlanEntryCode,
                            rank,
                            cropConnection,
                            interventionMissingFieldCounter + prevReliabilityErrorCount
                    );
                }
                
                // practicedSystem scale
                reliabilityIndexPracticedSystemValuesTotalCounter.merge(practicedSystemScaleKey, getReliabilityIndexForTargetedId(interventionId), Integer::sum);
                
                reliabilityIndexPracticedSystemValuesErrorCounter.merge(practicedSystemScaleKey, interventionMissingFieldCounter, Integer::sum);
                
            }); // fin des interventions de la culture
    
            cumulativeFrequenciesByPracticedCropCycleConnectionCode.put(
                    crop,
                    previousCrop,
                    rank,
                    cropContext.getCummulativeFrequencyForCrop());
            
            String its = growingSystemContext.getIts();
            String irs = growingSystemContext.getIrs();
    
            PracticedSystem anonymizePracticedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();
    
            // mise à l'echelle Intervention >> Culture (CC)
            List<Map.Entry<MultiKey<?>, Map<Scenario, Double[]>>> practicedSeasonalCroppingValues = practicedCroppingValues.entrySet().stream()
                    .filter(entry -> {
                        final Object previousPlanEntryCodeOrPhase = entry.getKey().getKey(1);
                        return !(previousPlanEntryCodeOrPhase instanceof PracticedCropCyclePhase);
                    })
                    .sorted(
                            Comparator.comparingInt(entry -> {
                                final Integer _rank = (Integer) entry.getKey().getKey(2);
                                return _rank;
                            })
                    )
                    .collect(Collectors.toList());
    
            writePracticedSeasonalCroppingValues(
                    writer,
                    domainContext,
                    growingSystem,
                    practicedCroppingReliabilityFieldErrorCounter,
                    practicedCroppingReliabilityTotalCounter,
                    cumulativeFrequenciesByPracticedCropCycleConnectionCode,
                    practicedSystemExecutionContext,
                    practicedCroppingValues,
                    cropsYealdAverage,
                    its,
                    irs,
                    anonymizePracticedSystem,
                    practicedSeasonalCroppingValues,
                    practicedCroppingFieldsErrors);
            
            
        } // fin du cylce
        
        // compute at practiced system scale
        if (seasonalInterventionFound.get()) {
            
            Set<PracticedCropPath> practicedCropPaths = practicedSystemExecutionContext.getPracticedCropPaths();
            Map<PracticedCropPath, Map<Scenario, Double[]>> scenarioValuesByRoad = computeValuesByRoad(cycleValues, practicedCropPaths);
            
            if (!scenarioValuesByRoad.isEmpty()) {
                
                Map<Scenario, Double[]> weightedAverageValueSum = computeWeightedAverageValueSumByScenario(scenarioValuesByRoad);
                
                double weightedAverageValuePathSize = computeWeightedAveragePathSizeByScenario(scenarioValuesByRoad);
                Map<Scenario, Double[]> finalResult = new HashMap<>();
                weightedAverageValueSum.forEach((k, v) -> finalResult.put(k, divs(v, weightedAverageValuePathSize)));
    
                practicedSystemsValues.merge(practicedSystemScaleKey, finalResult, this::sumInto);
            }
        }
    }

    protected Map<Scenario, Double[]> computeWeightedAverageValueSumByScenario(Map<PracticedCropPath, Map<Scenario, Double[]>> scenarioValuesByRoad) {
        Map<Scenario, Double[]> result = new HashMap<>();
        // on multiplie la somme des valeurs par chemin par le poids du chemin
        for (PracticedCropPath path : scenarioValuesByRoad.keySet()) {
            Map<Scenario, Double[]> practicedCropValuesByScenario = scenarioValuesByRoad.get(path);
            if (practicedCropValuesByScenario != null) {
                for (Scenario scenario : practicedCropValuesByScenario.keySet()) {
                    Double[] scenarioValue = practicedCropValuesByScenario.get(scenario);
                    if (scenarioValue != null) {
                        Double[] currentPathValue = mults(scenarioValue, path.getFinalFrequency());
                        result.merge(scenario, currentPathValue, GenericIndicator::sum);
                    }
                }
            }
        }
        return result;
    }

    protected double computeWeightedAveragePathSizeByScenario(Map<PracticedCropPath, Map<Scenario, Double[]>> scenarioValuesByRoad) {
        // Lors du calcul de la longueur de chaque branche (en nombre de campagnes),
        // il faut prendre en compte les cultures qui sont déclarées comme étant "même campagne que la culture précédente"
        return scenarioValuesByRoad.keySet().stream()
                .mapToDouble(path -> path.getFinalFrequency() *
                        Math.max(path.getConnections().stream()
                                .filter(connection -> !connection.isNotUsedForThisCampaign())
                                .map(PracticedCropCycleConnection::getTarget)
                                .filter(practicedCropCycleNode -> !practicedCropCycleNode.isSameCampaignAsPreviousNode())
                                .count(), 1))
                .sum();
    }

    protected Map<PracticedCropPath, Map<Scenario, Double[]>> computeValuesByRoad(Map<PracticedCropCycleConnection, Map<Scenario, Double[]>> cycleValues,
                                                                                  Set<PracticedCropPath> practicedCropPaths) {
    
        Map<PracticedCropPath, Map<Scenario, Double[]>> scenarioValueByRoad = new HashMap<>();
    
        if (CollectionUtils.isNotEmpty(practicedCropPaths)) {

            // moyenne pour CA
            for (PracticedCropCycleConnection connection : cycleValues.keySet()) {
                Map<Scenario, Double[]> scenarioValues = cycleValues.get(connection);

                List<PracticedCropPath> pathsWithConnection = practicedCropPaths.stream()
                        .filter(path -> path.getConnections().contains(connection)).toList();

                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace(this.getClass().getSimpleName());
                    LOGGER.trace(this.getClass().getSimpleName() + " Freq: " + connection.getCroppingPlanEntryFrequency());
                    LOGGER.trace(this.getClass().getSimpleName() + " Source: X:" + connection.getSource().getRank() + " Y:" + connection.getSource().getY());
                    LOGGER.trace(this.getClass().getSimpleName() + " Source Code: " + (connection.getSource() != null ? connection.getSource().getCroppingPlanEntryCode() : ""));
                    LOGGER.trace(this.getClass().getSimpleName() + " Target: X:" + connection.getTarget().getRank() + " Y:" + connection.getTarget().getY());
                    LOGGER.trace(this.getClass().getSimpleName() + " Target Code: " + connection.getTarget().getCroppingPlanEntryCode());

                    for (PracticedCropPath practicedCropPath : pathsWithConnection) {
                        String path = practicedCropPath.getConnections().stream()
                                .map(PracticedCropCycleConnection::getTarget)
                                .sorted(Comparator.comparing(PracticedCropCycleNode::getRank))
                                .map(PracticedCropCycleNode::getCroppingPlanEntryCode)
                                .collect(Collectors.joining(" -> "));
                        LOGGER.trace("Path: " + path + " Path frequency:" + practicedCropPath.getFinalFrequency());

                        String freqs = practicedCropPath.getConnections().stream()
                                .map(PracticedCropCycleConnection::getCroppingPlanEntryFrequency)
                                .map(String::valueOf)
                                .collect(Collectors.joining(IndicatorWriter.SEPARATOR));
                        LOGGER.trace("freqs: " + freqs);
                    }
                }

                for (Scenario scenario : scenarioValues.keySet()) {
                    // application de la correction de fréquence cumulées
                    Double[] values = scenarioValues.get(scenario);

                    // on fait la somme des valeurs par chemin
                    for (PracticedCropPath practicedCropPath : practicedCropPaths) {
                        Map<Scenario, Double[]> valuesForRoad = scenarioValueByRoad.computeIfAbsent(practicedCropPath, k -> new HashMap<>());
                        if (pathsWithConnection.contains(practicedCropPath)) {
                            valuesForRoad.merge(scenario, values, GenericIndicator::sum);
                        }
                    }
                }
            }
        }
        return scenarioValueByRoad;
    }
    
    protected MultiValuedMap<String, Pair<String, String>> getCodeEspeceBotaniqueCodeQualifantBySpeciesCode(CroppingPlanEntry interCrop) {
        List<CroppingPlanSpecies> species = ListUtils.emptyIfNull(interCrop.getCroppingPlanSpecies());
        MultiValuedMap<String, Pair<String, String>> codeEspeceBotaniqueCodeQualifantBySpeciesCode = new HashSetValuedHashMap<>();
        for (CroppingPlanSpecies croppingPlanSpecies : species) {
            RefEspece espece = croppingPlanSpecies.getSpecies();
            codeEspeceBotaniqueCodeQualifantBySpeciesCode.put(croppingPlanSpecies.getCode(), Pair.of(espece.getCode_espece_botanique(), espece.getCode_qualifiant_AEE()));
        }
        return codeEspeceBotaniqueCodeQualifantBySpeciesCode;
    }
    
    protected void computePracticedPerennial(
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            MultiValuedMap<String, RefHarvestingPrice> allRefHarvestingPricesByKey,
            IndicatorWriter writer,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext) {
        
        List<PracticedPerennialCropCycle> practicedPerennialCropCycles = practicedSystemExecutionContext.getPracticedPerennialCropCycles();
    
        final Optional<GrowingSystem> optionalGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();
        if (optionalGrowingSystem.isEmpty()) {
            return;
        }
        
        // perennial
        if (CollectionUtils.isEmpty(practicedPerennialCropCycles)) {
            return;
        }
        
        Set<PerformancePracticedCropExecutionContext> cropContexts =
                practicedSystemExecutionContext.getPerformancePracticedCropContextExecutionContexts().stream()
                        .filter(
                                performanceCropExecutionContext ->
                                        Objects.nonNull(performanceCropExecutionContext.getPracticedPerennialCropCycle()))
                        .collect(Collectors.toSet());
        
        if (CollectionUtils.isEmpty(cropContexts)) {
            return;// no crop in cycle
        }
        
        PracticedSystem practicedSystem = practicedSystemExecutionContext.getPracticedSystem();
        
        String campaigns = practicedSystem.getCampaigns();
    
        GrowingSystem growingSystem = optionalGrowingSystem.get();
        
        PracticedSystemScaleKey practicedSystemScaleKey = new PracticedSystemScaleKey(campaigns, growingSystem, practicedSystem);
    
        // surface totale d'occupation des cultures pérennes sur le SDC, devrait-être égal à 100.
        Double totalSolOccupationPercent = practicedPerennialCropCycles.stream()
                .mapToDouble(PracticedPerennialCropCycle::getSolOccupationPercent)
                .sum();
        if (totalSolOccupationPercent == 0d) {
            totalSolOccupationPercent = null;
        }
        
        MultiKeyMap<Object, Map<Scenario, Double[]>> practicedSystemCycleValues = new MultiKeyMap<>();
        
        cropContexts.stream()
            .filter(cropContext -> cropContext.getPracticedPerennialCropCycle() != null)
            .forEach(cropContext -> {
                
                PracticedPerennialCropCycle cycle = cropContext.getPracticedPerennialCropCycle();

                Set<PerformancePracticedInterventionExecutionContext> interventionExecutionContexts = cropContext.getInterventionExecutionContexts();
                
                final CropWithSpecies cropWithSpecies = cropContext.getCropWithSpecies();
                
                if (cropWithSpecies == null) return;
                
                final MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter = new MultiKeyMap<>();
                final MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter = new MultiKeyMap<>();
                final MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors = new MultiKeyMap<>();
                final Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealdAverage = new HashMap<>();
                final MultiKeyMap<Object, Map<Scenario, Double[]>> practicedCroppingValues = new MultiKeyMap<>();
                
                CroppingPlanEntry croppingPlanEntry = cropWithSpecies.getCroppingPlanEntry();
                String cropCode = croppingPlanEntry.getCode();
                
                perennialCropCyclePercentByCrop.put(croppingPlanEntry, cycle.getSolOccupationPercent());
                
                interventionExecutionContexts.stream()
                    .filter(interventionContext -> !interventionContext.isFictive())
                    .filter(interventionContext -> interventionContext.getOptionalHarvestingAction().isPresent())
                    .forEach(interventionContext -> {
                        
                        HarvestingAction harvestingAction = interventionContext.getOptionalHarvestingAction().get();
                        
                        PracticedIntervention intervention = interventionContext.getIntervention();
                        String interventionId = intervention.getTopiaId();
                        
                        PracticedCropCyclePhase phase = intervention.getPracticedCropCyclePhase();
                        
                        MultiValuedMap<String, Pair<String, String>> codeEspeceBotaniqueCodeQualifiantBySpeciesCode =
                                getCodeEspeceBotaniqueCodeQualifantBySpeciesCode(croppingPlanEntry);
                        
                        MultiValuedMap<ValorisationKey, HarvestingActionValorisation> interventionValorisationByScenarioKey =
                                getValorisationForKey(interventionId, codeEspeceBotaniqueCodeQualifiantBySpeciesCode, harvestingAction);
                        
                        Map<Scenario, Double[]> interValues = computeIndicatorForPracticedIntervention0(
                                interventionContext.getPriceConverterKeysToRate(), intervention,
                                allRefHarvestingPricesByKey,
                                interventionValorisationByScenarioKey,
                                croppingPlanEntry
                        );
                        
                        if (interValues == null) {
                            interValues = new HashMap<>();
                        }
                        
                        // trow up to crop the missing fields warning
                        Set<MissingFieldMessage> missingFieldWarnings = practicedCroppingFieldsErrors.get(
                                practicedSystem,
                                cropCode,
                                phase
                        );
                        
                        // trow up to crop the missing fields warning
                        if (missingFieldWarnings == null) {
                            missingFieldWarnings = new HashSet<>();
                        }
                        Set<MissingFieldMessage> interventionWarnings = targetedErrorFieldMessages.getOrDefault(interventionId, new HashSet<>());
                        Set<MissingFieldMessage> cropMissingFieldMessages = interventionWarnings.stream()
                                .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.CROP).isPresent() ||
                                        missingFieldMessage.getMessageForScope(MissingMessageScope.INTERVENTION).isPresent())
                                .collect(Collectors.toSet());
                        missingFieldWarnings.addAll(cropMissingFieldMessages);
                        
                        practicedCroppingFieldsErrors.put(
                                practicedSystem,
                                cropCode,
                                phase,
                                missingFieldWarnings
                        );
                        
                        // somme pour CC
                        Map<Scenario, Double[]> previous = practicedCroppingValues.get(cropCode, phase);
                        if (previous == null) {
                            practicedCroppingValues.put(cropCode, phase, new HashMap<>(interValues));
                            if (!interventionContext.isFictive() && intervention.isIntermediateCrop()) {
                                cropsYealdAverage.put(cropCode, cropContext.getIntermediateCropYealds());
                            } else {
                                cropsYealdAverage.put(cropCode, cropContext.getMainCropYealds());
                            }
                        } else {
                            sumInto(interValues, previous);
                        }
                        
                        Map<Scenario, Double[]> pspreviousValue = practicedSystemCycleValues.get(practicedSystem, cycle);
                        if (pspreviousValue == null) {
                            practicedSystemCycleValues.put(practicedSystem, cycle, new HashMap<>(interValues));
                        } else {
                            sumInto(interValues, pspreviousValue);
                        }
                        
                        // somme pour CC
                        Integer prevReliabilityTotalCount = practicedCroppingReliabilityTotalCounter.get(practicedSystem, cropCode, phase);
                        if (prevReliabilityTotalCount == null) {
                            practicedCroppingReliabilityTotalCounter.put(
                                    practicedSystem,
                                    cropCode,
                                    phase,
                                    getTotalFieldCounterValueForTargetedId(interventionId)
                            );
                        } else {
                            practicedCroppingReliabilityTotalCounter.put(
                                    practicedSystem,
                                    cropCode,
                                    phase,
                                    getTotalFieldCounterValueForTargetedId(interventionId) + prevReliabilityTotalCount
                            );
                        }
                        
                        Integer prevReliabilityErrorCount = practicedCroppingReliabilityFieldErrorCounter.get(
                                practicedSystem,
                                cropCode,
                                phase
                        );
                        
                        if (prevReliabilityErrorCount == null) {
                            practicedCroppingReliabilityFieldErrorCounter.put(
                                    practicedSystem,
                                    cropCode,
                                    phase,
                                    getMissingFieldCounterValueForTargetedId(interventionId)
                            );
                        } else {
                            practicedCroppingReliabilityFieldErrorCounter.put(
                                    practicedSystem,
                                    cropCode,
                                    phase,
                                    getMissingFieldCounterValueForTargetedId(interventionId) + prevReliabilityErrorCount
                            );
                        }
                        
                        reliabilityIndexPracticedSystemValuesTotalCounter.merge(practicedSystemScaleKey, getTotalFieldCounterValueForTargetedId(interventionId), Integer::sum);
                        
                        reliabilityIndexPracticedSystemValuesErrorCounter.merge(practicedSystemScaleKey, getMissingFieldCounterValueForTargetedId(interventionId), Integer::sum);
                        
                        Set<MissingFieldMessage> messagesAtPracticedScope = practicedSystemFieldsErrors.computeIfAbsent(practicedSystemScaleKey, k -> new HashSet<>());
                        
                        Set<MissingFieldMessage> practicedMissingFieldMessages = interventionWarnings.stream()
                                .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.PRACTICED_SYSTEM).isPresent())
                                .collect(Collectors.toSet());
                        if (CollectionUtils.isNotEmpty(practicedMissingFieldMessages)) {
                            messagesAtPracticedScope.addAll(practicedMissingFieldMessages);
                        }
                    }
                );
                
                PracticedSystem anonymizePracticedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();
                
                String its = growingSystemContext.getIts();
                String irs = growingSystemContext.getIrs();
                
                List<Map.Entry<MultiKey<?>, Map<Scenario, Double[]>>> practicedPerennialCroppingValues = practicedCroppingValues.entrySet().stream()
                        .filter(entry -> {
                            final Object previousPlanEntryCodeOrPhase = entry.getKey().getKey(1);
                            return previousPlanEntryCodeOrPhase instanceof PracticedCropCyclePhase;
                        })
                        .collect(Collectors.toList());

                writePracticedPerennialCroppingValues(
                        writer,
                        domainContext,
                        practicedSystemExecutionContext,
                        practicedCroppingValues,
                        cropsYealdAverage,
                        growingSystem,
                        its,
                        irs,
                        anonymizePracticedSystem,
                        practicedPerennialCroppingValues,
                        practicedCroppingReliabilityTotalCounter,
                        practicedCroppingReliabilityFieldErrorCounter,
                        practicedCroppingFieldsErrors);
                
                perennialCropCyclePercentByCrop.clear();
                
            });
        
        // mise à l'échelle vers Practiced System, on tient compte de la part des cultures perennes dans le sdc
        for (MultiKey<?> key : practicedSystemCycleValues.keySet()) {
            PracticedSystem ps = (PracticedSystem) key.getKey(0);
            PracticedPerennialCropCycle cycle = (PracticedPerennialCropCycle) key.getKey(1);
            Map<Scenario, Double[]> psValues = practicedSystemCycleValues.get(ps, cycle);
            Map<Scenario, Double[]> psPartValues = addPerennialCropPart(
                    cycle.getSolOccupationPercent(),
                    totalSolOccupationPercent,
                    psValues
            );
            
            practicedSystemsValues.merge(practicedSystemScaleKey, psPartValues, this::sumInto);
        }
        
    }

    protected Map<Scenario, Double[]> computeIndicatorForPracticedIntervention0(
            PriceConverterKeysToRate priceConverterKeysToRate,
            PracticedIntervention intervention,
            MultiValuedMap<String, RefHarvestingPrice> refHarvestingPricesByValorisationKey,
            MultiValuedMap<ValorisationKey, HarvestingActionValorisation> interventionValorisationByValorisationKey,
            CroppingPlanEntry crop) {

        Map<Scenario, Double[]> allValorisationsScenarioPricesByScenarioCode = null;

        if (interventionValorisationByValorisationKey != null && refHarvestingPricesByValorisationKey != null) {

            Double psci = getToolPSCi(intervention);

            allValorisationsScenarioPricesByScenarioCode = computeIndicator(
                    priceConverterKeysToRate,
                    refHarvestingPricesByValorisationKey,
                    interventionValorisationByValorisationKey,
                    crop,
                    psci,
                    intervention.getTopiaId()
            );
        }
        return addMissingScenarioPrices(allValorisationsScenarioPricesByScenarioCode);
    }
    
    protected Map<Scenario, Double[]> addMissingScenarioPrices(Map<Scenario, Double[]> allValorisationsScenarioPricesByScenarioCode) {
        Map<Scenario, Double[]> result = new HashMap<>();
        if (allValorisationsScenarioPricesByScenarioCode != null) {
            result.putAll(allValorisationsScenarioPricesByScenarioCode);
        }
        Collection<Scenario> requestedScenarios = scenariosByCode.values();
        for (Scenario requestedScenario : requestedScenarios) {
            result.putIfAbsent(requestedScenario, new Double[]{0d, 0d});
        }
        return result;
    }
    
    protected void writePracticedPerennialCropSheet(IndicatorWriter writer,
                                                    PracticedSystem practicedSystem,
                                                    CroppingPlanEntry croppingPlanEntry,
                                                    PracticedCropCyclePhase phase,
                                                    Map<Scenario, Double[]> values,
                                                    GrowingSystem growingSystem,
                                                    String its,
                                                    String irs,
                                                    MultiKey<?> practicedCroppingKey,
                                                    Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                                    MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter,
                                                    MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter,
                                                    MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors) {
        
        Integer reliabilityErrorForCrop = practicedCroppingReliabilityFieldErrorCounter.get(practicedCroppingKey);
        Integer totalFieldCounter = practicedCroppingReliabilityTotalCounter.get(practicedCroppingKey);
        
        Integer reliabilityIndexForCrop = computeReliabilityIndex(reliabilityErrorForCrop, totalFieldCounter);
        
        Set<MissingFieldMessage> missingFields = practicedCroppingFieldsErrors.get(practicedCroppingKey);
        String reliabilityComments =
                CollectionUtils.isEmpty(missingFields) ?
                        RELIABILITY_INDEX_NO_COMMENT :
                        missingFields.stream()
                                .map(MissingFieldMessage::getMessage)
                                .collect(Collectors.joining(", "));
        // perennial
        final Domain domain = growingSystem.getGrowingPlan().getDomain();
        
        for (Scenario scenario : values.keySet()) {
            
            String label = getPrintableScenarioLabel(scenario);
            
            Double[] valueWithAndWithoutAutoConsume = values.get(scenario);
            
            if (isWithAutoConsumed) {
                String scenarioLabel = getScenarioLabel(WITH_AUTO_CONSUME, label);
                writer.writePracticedPerennialCop(
                        its,
                        irs,
                        practicedSystem.getCampaigns(),
                        getIndicatorCategory(),
                        scenarioLabel,
                        valueWithAndWithoutAutoConsume[0],
                        cropYealdAverage,
                        reliabilityIndexForCrop,
                        reliabilityComments,
                        domain,
                        growingSystem,
                        practicedSystem,
                        croppingPlanEntry,
                        phase,
                        perennialCropCyclePercentByCrop.get(croppingPlanEntry),
                        this.getClass()
                );
            }
            if (isWithoutAutoConsumed) {
                // write crop sheet
                String scenarioLabel = getScenarioLabel(WITHOUT_AUTO_CONSUME, label);
                writer.writePracticedPerennialCop(
                        its,
                        irs,
                        practicedSystem.getCampaigns(),
                        getIndicatorCategory(),
                        scenarioLabel,
                        valueWithAndWithoutAutoConsume[1],
                        cropYealdAverage,
                        reliabilityIndexForCrop,
                        reliabilityComments,
                        domain,
                        growingSystem,
                        practicedSystem,
                        croppingPlanEntry,
                        phase,
                        perennialCropCyclePercentByCrop.get(croppingPlanEntry),
                        this.getClass()
                );
            }
        }
    }
    
    protected void writePracticedSeasonalCropSheet(IndicatorWriter writer,
                                                   MultiKeyMap<Object, Double> cumulativeFrequenciesByPracticedCropCycleConnectionCode,
                                                   GrowingSystem growingSystem,
                                                   String irs,
                                                   String its,
                                                   PracticedSystem practicedSystem,
                                                   MultiKey<?> practicedCroppingKey,
                                                   Map<Scenario, Double[]> values,
                                                   CroppingPlanEntry croppingPlanEntry,
                                                   CroppingPlanEntry previousCropEntry,
                                                   Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                                   MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter,
                                                   MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter,
                                                   MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors) {
    
        // seasonal
        int rank = (Integer) practicedCroppingKey.getKey(2);
        PracticedCropCycleConnection cropConnection = (PracticedCropCycleConnection) practicedCroppingKey.getKey(3);
    
        Double cumulativeFrequency = cumulativeFrequenciesByPracticedCropCycleConnectionCode.get(croppingPlanEntry, previousCropEntry, rank);
    
        Integer reliabilityErrorForCrop = practicedCroppingReliabilityFieldErrorCounter.get(practicedCroppingKey);
        Integer totalFieldCounter = practicedCroppingReliabilityTotalCounter.get(practicedCroppingKey);
    
        Integer reliabilityIndexForCrop = computeReliabilityIndex(reliabilityErrorForCrop, totalFieldCounter);
    
        final Domain domain = growingSystem.getGrowingPlan().getDomain();
    
        for (Scenario scenario : values.keySet()) {
            String label = getPrintableScenarioLabel(scenario);
        
            Double[] valueWithAndWithoutAutoConsume = values.get(scenario);
        
            Set<MissingFieldMessage> missingFields = practicedCroppingFieldsErrors.get(practicedCroppingKey);
            String reliabilityComments = CollectionUtils.isEmpty(missingFields) ? RELIABILITY_INDEX_NO_COMMENT :
                    missingFields.stream().map(MissingFieldMessage::getMessage).collect(Collectors.joining(", "));
        
            if (isWithAutoConsumed) {
                // write crop sheet
                final String scenarioLabel = getScenarioLabel(WITH_AUTO_CONSUME, label);
                writer.writePracticedSeasonalCrop(
                        its,
                        irs,
                        practicedSystem.getCampaigns(),
                        cumulativeFrequency,
                        getIndicatorCategory(),
                        scenarioLabel,
                        valueWithAndWithoutAutoConsume[0],
                        cropYealdAverage,
                        reliabilityIndexForCrop,
                        reliabilityComments,
                        rank,
                        domain,
                        growingSystem,
                        practicedSystem,
                        croppingPlanEntry,
                        previousCropEntry,
                        cropConnection.getTopiaId(),
                        this.getClass(),
                        cropConnection);
            }
            if (isWithoutAutoConsumed) {
                // write crop sheet
                writer.writePracticedSeasonalCrop(
                        its,
                        irs,
                        practicedSystem.getCampaigns(),
                        cumulativeFrequency,
                        getIndicatorCategory(),
                        getScenarioLabel(WITHOUT_AUTO_CONSUME, label),
                        valueWithAndWithoutAutoConsume[1],
                        cropYealdAverage,
                        reliabilityIndexForCrop,
                        reliabilityComments,
                        rank,
                        domain,
                        growingSystem,
                        practicedSystem,
                        croppingPlanEntry,
                        previousCropEntry,
                        cropConnection.getTopiaId(),
                        this.getClass(),
                        cropConnection);
            }
        }
    }
    
    @Override
    public void computeEffective(IndicatorWriter writer,
                                 PerformanceGlobalExecutionContext globalExecutionContext,
                                 PerformanceEffectiveDomainExecutionContext domainContext,
                                 PerformanceZoneExecutionContext zoneContext) {
        
        MultiValuedMap<String, RefHarvestingPrice> refHarvestingPricesByValorisationKey = domainContext.getRefScenarioHarvestingPricesByValorisationKey();
        
        computeEffectivePerennial(zoneContext, refHarvestingPricesByValorisationKey);
    
        computeEffectiveSeasonal(zoneContext, refHarvestingPricesByValorisationKey);
    
        // la map effectiveZoneValues est valuée plusieurs fois avec plusieurs zones par plusieurs
        // appel pour la mise à l'echelle de la methode suivante
        // computeEffective(IndicatorWriter writer, Domain domain, GrowingSystem growingSystem, plot plot)
        // on ne genere une ligne dans le fichier de sortie que pour la zone courante (s'il y a des valeurs)
        // et non pour toutes les zones
        writeEffectiveZone(writer, domainContext, zoneContext);
    
    }
    
    private void writeEffectiveZone(IndicatorWriter writer, PerformanceEffectiveDomainExecutionContext domainContext, PerformanceZoneExecutionContext zoneContext) {
        
        Zone anonymizeZone = zoneContext.getAnonymizeZone();
        Map<Scenario, Double[]> values = effectiveZoneValues.get(anonymizeZone);
        if (values != null) {
        
            Plot anonymizePlot = zoneContext.getAnonymizePlot();
            Optional<GrowingSystem> optionalAnoGrowingSystem = zoneContext.getAnonymizeGrowingSystem();
        
            String its = zoneContext.getIts();
            String irs = zoneContext.getIrs();
        
            String speciesName = zoneContext.getZoneSpeciesNames();
            String varietyNames = zoneContext.getZoneVarietiesNames();
        
            Map<Pair<RefDestination, YealdUnit>, Double> zoneAverageYeald = zoneContext.getZoneAverageYeald();
        
            String comments;
            Set<MissingFieldMessage> fieldMessages = effectiveZoneFieldsErrors.get(anonymizeZone);
            if (CollectionUtils.isNotEmpty(fieldMessages)) {
                comments = fieldMessages.stream()
                        .map(MissingFieldMessage::getMessage)
                        .collect(Collectors.joining(","));
            } else {
                comments = RELIABILITY_INDEX_NO_COMMENT;
            }
        
            Integer rmfzc = effectiveZoneReliabilityFieldErrorCounter.get(anonymizeZone);
            Integer rtzc = effectiveZoneReliabilityTotalCounter.get(anonymizeZone);
            Integer reliabilityIndex = computeReliabilityIndex(rmfzc, rtzc);
        
            for (Scenario scenario : values.keySet()) {
                String label = getPrintableScenarioLabel(scenario);
            
                Double[] resultWithAndWithoutAutoConsumme = values.get(scenario);
            
                if (isWithAutoConsumed) {
                    // write zone sheet
                    Double resultWithAutoConsumme = resultWithAndWithoutAutoConsumme[0];
                    final String scenarioLabel = getScenarioLabel(WITH_AUTO_CONSUME, label);
                
                    writer.writeEffectiveZone(
                            its,
                            irs,
                            getIndicatorCategory(),
                            scenarioLabel,
                            resultWithAutoConsumme,
                            zoneAverageYeald,
                            reliabilityIndex,
                            comments,
                            domainContext.getAnonymiseDomain(),
                            domainContext.getCroppingPlanSpecies(),
                            optionalAnoGrowingSystem,
                            anonymizePlot,
                            anonymizeZone,
                            speciesName,
                            varietyNames,
                            this.getClass()
                    );
                }
                if (isWithoutAutoConsumed) {
                    // write zone sheet
                    Double resultWithoutAutoConsumme = resultWithAndWithoutAutoConsumme[1];
                    final String scenarioLabel = getScenarioLabel(WITHOUT_AUTO_CONSUME, label);
                
                    writer.writeEffectiveZone(
                            its,
                            irs,
                            getIndicatorCategory(),
                            scenarioLabel,
                            resultWithoutAutoConsumme,
                            zoneAverageYeald,
                            reliabilityIndex,
                            comments,
                            domainContext.getAnonymiseDomain(),
                            domainContext.getCroppingPlanSpecies(),
                            optionalAnoGrowingSystem,
                            anonymizePlot,
                            anonymizeZone,
                            speciesName,
                            varietyNames,
                            this.getClass()
                    );
                }
    
                for (Map.Entry<EffectiveItkCropCycleScaleKey, Map<Scenario, Double[]>> effectiveItkCropPrevCropPhaseKeyEntry : effectiveItkCropPrevCropPhaseValues.entrySet()) {
        
                    EffectiveItkCropCycleScaleKey key = effectiveItkCropPrevCropPhaseKeyEntry.getKey();
                    Map<Scenario, Double[]> itkValues = effectiveItkCropPrevCropPhaseKeyEntry.getValue();
                    resultWithAndWithoutAutoConsumme = itkValues.get(scenario);
                    Integer effectiveItkCropPrevCropPhaseTotalCount = effectiveItkCropPrevCropPhaseTotalCounter.get(key);
                    Integer effectiveItkCropPrevCropPhaseFieldErrorCount = effectiveItkCropPrevCropPhaseFieldErrorCounter.get(key);
                    Integer effectiveItkCropPrevCropPhaseReliabilityIndex = computeReliabilityIndex(effectiveItkCropPrevCropPhaseFieldErrorCount, effectiveItkCropPrevCropPhaseTotalCount);
        
                    String effectiveItkCropPrevCropPhaseComments;
                    Set<MissingFieldMessage> effectiveItkCropPrevCropPhaseFieldMessages = effectiveItkCropPrevCropPhaseFieldsErrors.get(key);
                    if (CollectionUtils.isNotEmpty(effectiveItkCropPrevCropPhaseFieldMessages)) {
                        Set<String> errors = effectiveItkCropPrevCropPhaseFieldMessages.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet());
                        effectiveItkCropPrevCropPhaseComments = String.join(IndicatorWriter.SEPARATOR, errors);
                    } else {
                        effectiveItkCropPrevCropPhaseComments = RELIABILITY_INDEX_NO_COMMENT;
                    }
    
                    if (isWithAutoConsumed) {
                        // write zone sheet
                        Double resultWithAutoConsumme = resultWithAndWithoutAutoConsumme[0];
                        final String scenarioLabel = getScenarioLabel(WITH_AUTO_CONSUME, label);
    
                        // write zone sheet
                        writer.writeEffectiveItk(
                                its,
                                irs,
                                getIndicatorCategory(),
                                scenarioLabel,
                                resultWithAutoConsumme,
                                effectiveItkCropPrevCropPhaseReliabilityIndex,
                                effectiveItkCropPrevCropPhaseComments,
                                domainContext.getAnonymiseDomain(),
                                optionalAnoGrowingSystem,
                                anonymizePlot,
                                anonymizeZone,
                                this.getClass(),
                                key);
                        
                    }
    
                    if (isWithoutAutoConsumed) {
                        // write zone sheet
                        Double resultWithoutAutoConsumme = resultWithAndWithoutAutoConsumme[1];
                        final String scenarioLabel = getScenarioLabel(WITHOUT_AUTO_CONSUME, label);
    
                        // write zone sheet
                        writer.writeEffectiveItk(
                                its,
                                irs,
                                getIndicatorCategory(),
                                scenarioLabel,
                                resultWithoutAutoConsumme,
                                effectiveItkCropPrevCropPhaseReliabilityIndex,
                                effectiveItkCropPrevCropPhaseComments,
                                domainContext.getAnonymiseDomain(),
                                optionalAnoGrowingSystem,
                                anonymizePlot,
                                anonymizeZone,
                                this.getClass(),
                                key);
                        
                    }
        
                }
            }
        }
        
        effectiveItkCropPrevCropPhaseValues.clear();
        effectiveItkCropPrevCropPhaseTotalCounter.clear();
        effectiveItkCropPrevCropPhaseFieldErrorCounter.clear();
        effectiveItkCropPrevCropPhaseFieldsErrors.clear();
    }
    
    private void computeEffectivePerennial(
            PerformanceZoneExecutionContext zoneContext,
            MultiValuedMap<String, RefHarvestingPrice> refHarvestingPricesByValorisationKey) {
    
        List<EffectivePerennialCropCycle> perennialCycles = zoneContext.getPerennialCropCycles();
        
        if (!CollectionUtils.isEmpty(perennialCycles)) {
            Set<PerformanceEffectiveCropExecutionContext> performanceCropContextExecutionContexts = zoneContext.getPerformanceCropContextExecutionContexts()
                    .stream()
                    .filter(
                            performanceCropExecutionContext ->
                                    !Objects.isNull(performanceCropExecutionContext.getPerennialCropCycle()
                                    ))
                    .collect(Collectors.toSet());
            
            
            for (PerformanceEffectiveCropExecutionContext cropContext : performanceCropContextExecutionContexts) {
                Set<PerformanceEffectiveInterventionExecutionContext> interventionContexts = cropContext.getInterventionExecutionContexts();
                
                computeEffectivePerennial(refHarvestingPricesByValorisationKey, zoneContext, cropContext, interventionContexts);
            }
        }
    }
    
    private void computeEffectiveSeasonal(
            PerformanceZoneExecutionContext zoneContext,
            MultiValuedMap<String, RefHarvestingPrice> refHarvestingPricesByValorisationKey) {
        
        EffectiveSeasonalCropCycle seasonalCropCycle = zoneContext.getSeasonalCropCycle();
        
        if (seasonalCropCycle != null) {
            
            Set<PerformanceEffectiveCropExecutionContext> cropContexts_ = zoneContext.getPerformanceCropContextExecutionContexts()
                    .stream()
                    .filter(
                            performanceCropExecutionContext ->
                                    Objects.isNull(performanceCropExecutionContext.getPerennialCropCycle()
                                    ))
                    .collect(Collectors.toSet());
            
            List<PerformanceEffectiveCropExecutionContext> cropContexts = cropContexts_
                    .stream()
                    .sorted(Comparator.comparingInt(PerformanceEffectiveCropExecutionContext::getRank)).toList();
            
            for (PerformanceEffectiveCropExecutionContext cropContext : cropContexts) {
                
                Set<PerformanceEffectiveInterventionExecutionContext> interventionContexts = cropContext.getInterventionExecutionContexts();
                
                computEffectiveSeasonal(refHarvestingPricesByValorisationKey, zoneContext, cropContext, interventionContexts);
                
            }
        }
    }
    
    private void computEffectiveSeasonal(MultiValuedMap<String, RefHarvestingPrice> refHarvestingPricesByValorisationKey,
                                         PerformanceZoneExecutionContext zoneContext,
                                         PerformanceEffectiveCropExecutionContext cropContext,
                                         Set<PerformanceEffectiveInterventionExecutionContext> interventionContexts) {
        
        Zone anonymizeZone = zoneContext.getAnonymizeZone();
        interventionContexts.stream()
        .filter(interventionContext -> interventionContext.getIntervention().getEffectiveCropCycleNode() != null)
        .filter(interventionContext -> interventionContext.getOptionalHarvestingAction().isPresent())
        .forEach(interventionContext -> {

            HarvestingAction harvestingAction = interventionContext.getOptionalHarvestingAction().get();
            EffectiveIntervention intervention = interventionContext.getIntervention();

            EffectiveCropCycleNode node = intervention.getEffectiveCropCycleNode();
            String interventionId = intervention.getTopiaId();
            // main or intermediate one
            CroppingPlanEntry interventionCrop = interventionContext.getCroppingPlanEntry();
            CroppingPlanEntry previousCrop = cropContext.getSeasonalPreviousCrop();
            
            MultiValuedMap<String, Pair<String, String>> codeEspeceBotaniqueCodeQualifantBySpeciesCode =
                    getCodeEspeceBotaniqueCodeQualifantBySpeciesCode(interventionCrop);

            MultiValuedMap<ValorisationKey, HarvestingActionValorisation> interventionValorisationByValorisationKey = getValorisationForKey(
                    interventionId,
                    codeEspeceBotaniqueCodeQualifantBySpeciesCode,
                    harvestingAction
            );

            // get value for intervention
            Map<Scenario, Double[]> interValues = computeIndicatorForEffectiveIntervention(
                    interventionContext.getPriceConverterKeysToRate(),
                    intervention,
                    refHarvestingPricesByValorisationKey,
                    interventionValorisationByValorisationKey,
                    interventionCrop
            );
            
            // cet indicateur n'est pas écrit à l'échelle de l'intervention (n'a pas de sens)
            Set<MissingFieldMessage> interventionWarnings = targetedErrorFieldMessages.getOrDefault(interventionId, new HashSet<>());
            Set<MissingFieldMessage> cropMissingFieldMessages = interventionWarnings.stream()
                    .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.CROP).isPresent() ||
                            missingFieldMessage.getMessageForScope(MissingMessageScope.INTERVENTION).isPresent())
                    .collect(Collectors.toSet());

            EffectiveCropCycleScaleKey effectiveSeasonalCropKey =
                    new EffectiveCropCycleScaleKey(
                            interventionCrop,
                            Optional.ofNullable(previousCrop),
                            Optional.of(node.getRank()),
                            Optional.empty());

            aggregateToCropScale(
                    cropContext,
                    intervention,
                    interventionCrop,
                    interValues,
                    cropMissingFieldMessages,
                    effectiveSeasonalCropKey);

            // somme pour CA
            Map<Scenario, Double[]> previousValue = effectiveZoneValues.computeIfAbsent(
                    anonymizeZone,
                    k -> new HashMap<>());
            sumInto(interValues, previousValue);

            effectiveZoneReliabilityTotalCounter.merge(
                    anonymizeZone,
                    getTotalFieldCounterValueForTargetedId(interventionId),
                    Integer::sum);
            effectiveZoneReliabilityFieldErrorCounter.merge(
                    anonymizeZone,
                    getMissingFieldCounterValueForTargetedId(interventionId),
                    Integer::sum);

            Set<MissingFieldMessage> zoneMissingFieldMessages = interventionWarnings.stream()
                    .filter(missingFieldMessage -> missingFieldMessage
                            .getMessageForScope(MissingMessageScope.ZONE)
                            .isPresent())
                    .collect(Collectors.toSet());

            Set<MissingFieldMessage> missingFieldsForZone = effectiveZoneFieldsErrors
                    .computeIfAbsent(anonymizeZone, k -> new HashSet<>());

            missingFieldsForZone.addAll(zoneMissingFieldMessages);

            EffectiveItkCropCycleScaleKey effectiveItkCropPrevCropPhaseKey =
                    new EffectiveItkCropCycleScaleKey(
                            zoneContext.getGrowingSystem(),
                            zoneContext.getAnonymizePlot(),
                            anonymizeZone,
                            interventionCrop,
                            Optional.ofNullable(previousCrop),
                            Optional.of(node.getRank()),
                            Optional.empty(),
                            Optional.ofNullable(intervention.getEffectiveCropCycleNode())
                    );

            effectiveItkCropPrevCropPhaseValues.merge(
                    effectiveItkCropPrevCropPhaseKey,
                    interValues,
                    this::sumABtoB);

            effectiveItkCropPrevCropPhaseTotalCounter.merge(
                    effectiveItkCropPrevCropPhaseKey,
                    getTotalFieldCounterValueForTargetedId(interventionId),
                    Integer::sum);

            effectiveItkCropPrevCropPhaseFieldErrorCounter.merge(
                    effectiveItkCropPrevCropPhaseKey,
                    getMissingFieldCounterValueForTargetedId(interventionId),
                    Integer::sum);

            // on ajoute ceux de la zone ?
            Set<MissingFieldMessage> missingFieldsForItkCropPrevCropPhase = effectiveItkCropPrevCropPhaseFieldsErrors.computeIfAbsent(effectiveItkCropPrevCropPhaseKey, k -> new HashSet<>());
            missingFieldsForItkCropPrevCropPhase.addAll(zoneMissingFieldMessages);
        });
    }
    
    protected void computeEffectivePerennial(
            MultiValuedMap<String, RefHarvestingPrice> refHarvestingPricesByValorisationKey,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveCropExecutionContext cropContext,
            Set<PerformanceEffectiveInterventionExecutionContext> interventionContexts) {
    
        Zone anonymizeZone = zoneContext.getAnonymizeZone();
        // perennial
        interventionContexts.stream()
        .filter(interventionContext -> interventionContext.getIntervention().getEffectiveCropCyclePhase() != null)
        .filter(interventionContext -> interventionContext.getOptionalHarvestingAction().isPresent())
        .forEach(interventionContext -> {
            HarvestingAction harvestingAction = interventionContext.getOptionalHarvestingAction().get();
            EffectiveIntervention intervention = interventionContext.getIntervention();
            
            String interventionId = intervention.getTopiaId();
            CroppingPlanEntry interventionCrop = interventionContext.getCroppingPlanEntry();
            
            Map<Scenario, Double[]> interValues;
            
            MultiValuedMap<String, Pair<String, String>> codeEspeceBotaniqueCodeQualifantBySpeciesCode =
                    getCodeEspeceBotaniqueCodeQualifantBySpeciesCode(interventionCrop);

            MultiValuedMap<ValorisationKey, HarvestingActionValorisation> interventionValorisationByValorisationKey =
                    getValorisationForKey(
                            interventionId,
                            codeEspeceBotaniqueCodeQualifantBySpeciesCode,
                            harvestingAction
                    );

            // get value for intervention
            interValues = computeIndicatorForEffectiveIntervention(
                    interventionContext.getPriceConverterKeysToRate(), intervention,
                    refHarvestingPricesByValorisationKey,
                    interventionValorisationByValorisationKey,
                    interventionCrop
            );
            
            Set<MissingFieldMessage> interventionWarnings = targetedErrorFieldMessages.getOrDefault(interventionId, new HashSet<>());
            Set<MissingFieldMessage> cropMissingFieldMessages = interventionWarnings.stream()
                    .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.CROP).isPresent() ||
                            missingFieldMessage.getMessageForScope(MissingMessageScope.INTERVENTION).isPresent())
                    .collect(Collectors.toSet());
            
            EffectiveCropCyclePhase phase = intervention.getEffectiveCropCyclePhase();
            
            // cet indicateur n'est pas écrit à l'échelle de l'intervention (n'a pas de sens)
            EffectiveCropCycleScaleKey effectivePerennialCropKey =
                    new EffectiveCropCycleScaleKey(
                            interventionCrop,
                            Optional.empty(),
                            Optional.empty(),
                            Optional.of(phase));
    
            aggregateToCropScale(
                    cropContext,
                    intervention,
                    interventionCrop,
                    interValues,
                    cropMissingFieldMessages,
                    effectivePerennialCropKey);
    
            // somme pour CA
            effectiveZoneValues.merge(anonymizeZone, interValues, this::sumInto);
            
            effectiveZoneReliabilityTotalCounter.merge(
                    anonymizeZone,
                    getTotalFieldCounterValueForTargetedId(interventionId),
                    Integer::sum);
            effectiveZoneReliabilityFieldErrorCounter.merge(
                    anonymizeZone,
                    getMissingFieldCounterValueForTargetedId(interventionId),
                    Integer::sum);
    
            Set<MissingFieldMessage> zoneMissingFieldMessages = interventionWarnings.stream()
                    .filter(missingFieldMessage -> missingFieldMessage
                            .getMessageForScope(MissingMessageScope.ZONE)
                            .isPresent())
                    .collect(Collectors.toSet());
            
            Set<MissingFieldMessage> missingFieldsForZone = effectiveZoneFieldsErrors
                    .computeIfAbsent(anonymizeZone, k -> new HashSet<>());
    
            missingFieldsForZone.addAll(zoneMissingFieldMessages);
    
            EffectiveItkCropCycleScaleKey effectiveItkCropCycleScaleKey =
                    new EffectiveItkCropCycleScaleKey(
                            zoneContext.getGrowingSystem(),
                            zoneContext.getAnonymizePlot(),
                            anonymizeZone,
                            interventionCrop,
                            Optional.empty(),
                            Optional.empty(),
                            Optional.of(phase),
                            Optional.ofNullable(intervention.getEffectiveCropCycleNode())
                    );
    
            effectiveItkCropPrevCropPhaseValues.merge(
                    effectiveItkCropCycleScaleKey,
                    interValues,
                    this::sumABtoB);
    
            effectiveItkCropPrevCropPhaseTotalCounter.merge(
                    effectiveItkCropCycleScaleKey,
                    getTotalFieldCounterValueForTargetedId(interventionId),
                    Integer::sum);
    
            effectiveItkCropPrevCropPhaseFieldErrorCounter.merge(
                    effectiveItkCropCycleScaleKey,
                    getMissingFieldCounterValueForTargetedId(interventionId),
                    Integer::sum);
    
            // on ajoute ceux de la zone ?
            Set<MissingFieldMessage> missingFieldsForItkCropPrevCropPhase = effectiveItkCropPrevCropPhaseFieldsErrors.computeIfAbsent(effectiveItkCropCycleScaleKey, k -> new HashSet<>());
            missingFieldsForItkCropPrevCropPhase.addAll(zoneMissingFieldMessages);
        });
    }
    
    private void aggregateToCropScale(
            PerformanceEffectiveCropExecutionContext cropContext,
            EffectiveIntervention intervention,
            CroppingPlanEntry crop,
            Map<Scenario, Double[]> interValues,
            Set<MissingFieldMessage> cropMissingFieldMessages,
            EffectiveCropCycleScaleKey effectiveCropKey) {

        String interventionId = intervention.getTopiaId();
        Set<MissingFieldMessage> missingFieldsForCrop = effectiveCroppingFieldsErrors.computeIfAbsent(effectiveCropKey, k -> new HashSet<>());
        // trow up to crop the missing fields warning
        if (CollectionUtils.isNotEmpty(cropMissingFieldMessages)) {
            missingFieldsForCrop.addAll(cropMissingFieldMessages);
        }

        if (intervention.isIntermediateCrop()) {
            effectiveCropsYealdAverage.computeIfAbsent(crop.getCode(), k -> cropContext.getIntermediateCropYealds());
        } else {
            effectiveCropsYealdAverage.computeIfAbsent(crop.getCode(), k -> cropContext.getMainCropYealds());
        }

        // somme pour CC (pour toutes les cultures du cycle)
        effectiveCroppingValues.merge(effectiveCropKey, interValues, this::sumInto);
        
        // somme pour CC
        effectiveCroppingReliabilityTotalCounter.merge(effectiveCropKey, getTotalFieldCounterValueForTargetedId(interventionId), Integer::sum);
        effectiveCroppingReliabilityFieldErrorCounter.merge(effectiveCropKey, getMissingFieldCounterValueForTargetedId(interventionId), Integer::sum);
        
    }
    
    private String getScenarioLabel(String withAutoConsume, String printableScenarioLabel) {
        return IndicatorGrossIncomeForScenarios.PRODUIT_BRUT_STANDARDISE + withAutoConsume + printableScenarioLabel;
    }
    
    protected Map<Scenario, Double[]> sumInto(Map<Scenario, Double[]> interValues, Map<Scenario, Double[]> previousValue) {
        if (previousValue == null) {
            return interValues;
        }
        for (Scenario scenario : interValues.keySet()) {
            Double[] value = interValues.get(scenario);
            previousValue.merge(scenario, value, GenericIndicator::sum);
        }
        return previousValue;
    }
    
    protected Map<Scenario, Double[]> sumABtoB(Map<Scenario, Double[]> interValues, Map<Scenario, Double[]> previousValue) {
        for (Scenario scenario : interValues.keySet()) {
            Double[] value = interValues.get(scenario);
            previousValue.merge(scenario, value, GenericIndicator::sum);
        }
        return previousValue;
    }

    protected Map<Scenario, Double[]> computeIndicatorForEffectiveIntervention(
            PriceConverterKeysToRate priceConverterKeysToRate,
            EffectiveIntervention intervention,
            MultiValuedMap<String, RefHarvestingPrice> refHarvestingPricesByValorisationKey,
            MultiValuedMap<ValorisationKey, HarvestingActionValorisation> interventionValorisationByValorisationKey,
            CroppingPlanEntry crop) {

        Map<Scenario, Double[]> allValorisationsScenarioPricesByScenarioCode = null;

        if (refHarvestingPricesByValorisationKey != null && interventionValorisationByValorisationKey != null) {

            Double psci = getToolPSCi(intervention);

            allValorisationsScenarioPricesByScenarioCode = computeIndicator(
                    priceConverterKeysToRate,
                    refHarvestingPricesByValorisationKey,
                    interventionValorisationByValorisationKey,
                    crop,
                    psci,
                    intervention.getTopiaId()
            );
        }
    
        return addMissingScenarioPrices(allValorisationsScenarioPricesByScenarioCode);
    }
    
    @Override
    public void computeEffective(IndicatorWriter writer,
                                 PerformanceEffectiveDomainExecutionContext domainContext,
                                 Optional<GrowingSystem> optionalGrowingSystem,
                                 PerformancePlotExecutionContext plotContext) {
        
        Map<Scenario, Double[]> weightedValueSum = new HashMap<>();
        Map<Scenario, Double> weightSum = new HashMap<>();
        
        Integer reliabilityPlotTotal = 0;
        Integer reliabilityPlotMissingFileld = 0;
        Set<MissingFieldMessage> plotMissingFieldMessages = new HashSet<>();
        
        Set<String> zoneTopiaIds = new HashSet<>();
        // moyenne pondérée sur la surface
        // I_Parcelle = somme(surface_zone_i * I_zone_i) / somme(surface_zone_i) où zone_i est la ième zone de la parcelle
        for (Zone zone : effectiveZoneValues.keySet()) {
            zoneTopiaIds.add(zone.getTopiaId());
            reliabilityPlotTotal += effectiveZoneReliabilityTotalCounter.get(zone);
            reliabilityPlotMissingFileld += effectiveZoneReliabilityFieldErrorCounter.get(zone);

            if (effectiveZoneFieldsErrors.get(zone) != null) {
                Set<MissingFieldMessage> zoneMissingFieldMessages = effectiveZoneFieldsErrors.get(zone).stream()
                        .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.PLOT).isPresent())
                        .collect(Collectors.toSet());
                plotMissingFieldMessages.addAll(zoneMissingFieldMessages);
            }

            Map<Scenario, Double[]> values = effectiveZoneValues.get(zone);
    
            for (Scenario scenario : values.keySet()) {
                Double[] resultWithAndWithoutAutoConsume = values.get(scenario);
                Double[] currentWeightResult = mults(resultWithAndWithoutAutoConsume, zone.getArea());
                weightedValueSum.merge(scenario, currentWeightResult, GenericIndicator::sum);
                weightSum.merge(scenario, zone.getArea(), Double::sum);
            }
        }
        
        if (!weightSum.isEmpty()) {
            String zoneIds = String.join(", ", zoneTopiaIds);
            Plot anonymizePlot = plotContext.getAnonymizePlot();
            Map<Pair<RefDestination, YealdUnit>, Double> plotYealdAverages = plotContext.getPlotYealdAverages();
            String its = plotContext.getIts();
            String irs = plotContext.getIrs();
    
            Integer plotReliability = computeReliabilityIndex(reliabilityPlotMissingFileld, reliabilityPlotTotal);
    
            effectivePlotReliabilityFieldErrorCounter.put(anonymizePlot, reliabilityPlotMissingFileld);
    
            effectivePlotReliabilityTotalCounter.put(anonymizePlot, reliabilityPlotTotal);
    
            Map<Scenario, Double[]> plotValues = new HashMap<>();
    
            String comments;
            if (CollectionUtils.isNotEmpty(plotMissingFieldMessages)) {
                comments = plotMissingFieldMessages.stream()
                        .map(MissingFieldMessage::getMessage)
                        .collect(Collectors.joining(","));
        
            } else {
                comments = RELIABILITY_INDEX_NO_COMMENT;
            }
            
            for (Map.Entry<Scenario, Double[]> weightValueSumForScenario : weightedValueSum.entrySet()) {
                Scenario scenario = weightValueSumForScenario.getKey();
                String label = getPrintableScenarioLabel(scenario);
                
                Double[] resultWithAndWithoutAutoConsumme = weightValueSumForScenario.getValue();
                
                Double valueWithAutoConsume = resultWithAndWithoutAutoConsumme[0];
                Double valueWithoutAutoConsume = resultWithAndWithoutAutoConsumme[1];
                
                Double plotValueWithAutoConsume = weightSum.get(scenario) == 0 ?
                        0 : valueWithAutoConsume / weightSum.get(scenario);
                
                if (isWithAutoConsumed) {
                    // write plot sheet
                    final String scenarioLabel = getScenarioLabel(WITH_AUTO_CONSUME, label);
    
                    writer.writeEffectivePlot(
                            its,
                            irs,
                            getIndicatorCategory(),
                            scenarioLabel,
                            plotValueWithAutoConsume,
                            plotReliability,
                            comments,
                            domainContext.getAnonymiseDomain(), domainContext.getCroppingPlanSpecies(), optionalGrowingSystem,
                            anonymizePlot,
                            plotYealdAverages,
                            this.getClass(),
                            zoneIds);
                    
                }
                Double plotValueWithoutAutoConsume = weightSum.get(scenario) == 0 ?
                        0 : valueWithoutAutoConsume / weightSum.get(scenario);
                
                if (isWithoutAutoConsumed) {
                    // write plot sheet
                    final String scenarioLabel = getScenarioLabel(WITHOUT_AUTO_CONSUME, label);
    
                    writer.writeEffectivePlot(
                            its,
                            irs,
                            getIndicatorCategory(),
                            scenarioLabel,
                            plotValueWithoutAutoConsume,
                            plotReliability,
                            comments,
                            domainContext.getAnonymiseDomain(), domainContext.getCroppingPlanSpecies(), optionalGrowingSystem,
                            anonymizePlot,
                            plotYealdAverages,
                            this.getClass(),
                            zoneIds);
                }
                plotValues.put(scenario, new Double[] { plotValueWithAutoConsume, plotValueWithoutAutoConsume });
            }
            
            effectivePlotValues.put(anonymizePlot, plotValues);
            
        }
    }
    
    @Override
    public void computeEffectiveCC(
            IndicatorWriter writer,
            Domain domain,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            Plot plot) {
    
        Optional<GrowingSystem> optionnalAnoGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();
        String its = growingSystemContext.getIts();
        String irs = growingSystemContext.getIrs();
        
        // toutes les interventions culturales d’une campagne culturale (CC)
        for (EffectiveCropCycleScaleKey effectiveCroppingKey : effectiveCroppingValues.keySet()) {
            Map<Scenario, Double[]> values = effectiveCroppingValues.get(effectiveCroppingKey);
            CroppingPlanEntry crop = effectiveCroppingKey.crop();
            Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverages = effectiveCropsYealdAverage.get(crop.getCode());
    
            CroppingPlanEntry previousCrop = effectiveCroppingKey.previousCrop().orElse(null);
            Integer rank = effectiveCroppingKey.rang().orElse(null);
    
            EffectiveCropCyclePhase effectiveCropCyclePhase = effectiveCroppingKey.phase().orElse(null);
    
            Integer riec = effectiveCroppingReliabilityFieldErrorCounter.get(effectiveCroppingKey);
            Integer ritc = effectiveCroppingReliabilityTotalCounter.get(effectiveCroppingKey);
    
            Integer reliability = computeReliabilityIndex(riec, ritc);
    
            String comments = RELIABILITY_INDEX_NO_COMMENT;
            if (isWithAutoConsumed || isWithoutAutoConsumed) {
                Set<MissingFieldMessage> messages = effectiveCroppingFieldsErrors.get(effectiveCroppingKey);
        
                if (CollectionUtils.isNotEmpty(messages)) {
                    comments = messages.stream()
                            .map(MissingFieldMessage::getMessage)
                            .collect(Collectors.joining(","));
                }
            }
            
            for (Scenario scenario : values.keySet()) {
                String label = getPrintableScenarioLabel(scenario);
    
                Double[] valueWithAndWithoutAutoConsume = values.get(scenario);
    
                final String scenarioLabel = getScenarioLabel(WITH_AUTO_CONSUME, label);
                final String scenarioLabelWithoutAutoConsume = getScenarioLabel(WITHOUT_AUTO_CONSUME, label);
    
                if (effectiveCroppingKey.rang().isPresent()) {

                    if (isWithAutoConsumed) {
                        // write crop sheet
                        Double valueWithAutoConsume = valueWithAndWithoutAutoConsume[0];
                        writer.writeEffectiveSeasonalCrop(
                                its,
                                irs,
                                domain.getCampaign(),
                                getIndicatorCategory(),
                                scenarioLabel,
                                valueWithAutoConsume,
                                cropYealdAverages,
                                reliability,
                                comments,
                                domain,
                                optionnalAnoGrowingSystem,
                                plot,
                                crop,
                                rank,
                                previousCrop,
                                this.getClass()
                        );
                    }
                    
                    if (isWithoutAutoConsumed) {
                        // write crop sheet
                        Double valueWithoutAutoConsume = valueWithAndWithoutAutoConsume[1];
                        writer.writeEffectiveSeasonalCrop(
                                its,
                                irs,
                                domain.getCampaign(),
                                getIndicatorCategory(),
                                scenarioLabelWithoutAutoConsume,
                                valueWithoutAutoConsume,
                                cropYealdAverages,
                                reliability,
                                comments,
                                domain,
                                optionnalAnoGrowingSystem,
                                plot,
                                crop,
                                rank,
                                previousCrop,
                                this.getClass()
                        );
                    }
                } else if (effectiveCroppingKey.phase().isPresent()) {
                    
                    if (isWithAutoConsumed) {
                        // write crop sheet
                        Double valueWithAutoConsume = valueWithAndWithoutAutoConsume[0];
                        writer.writeEffectivePerennialCrop(
                                its,
                                irs,
                                domain.getCampaign(),
                                getIndicatorCategory(),
                                scenarioLabel,
                                valueWithAutoConsume,
                                cropYealdAverages,
                                reliability,
                                comments,
                                domain,
                                optionnalAnoGrowingSystem,
                                plot,
                                crop,
                                effectiveCropCyclePhase,
                                this.getClass()
                        );
                    }
    
                    if (isWithoutAutoConsumed) {
                        // write crop sheet
                        Double valueWithoutAutoConsume = valueWithAndWithoutAutoConsume[1];
                        writer.writeEffectivePerennialCrop(
                                its,
                                irs,
                                domain.getCampaign(),
                                getIndicatorCategory(),
                                scenarioLabelWithoutAutoConsume,
                                valueWithoutAutoConsume,
                                cropYealdAverages,
                                reliability,
                                comments,
                                domain,
                                optionnalAnoGrowingSystem,
                                plot,
                                crop,
                                effectiveCropCyclePhase,
                                this.getClass()
                        );
                    }
                }
            }
        }
    }
    
    @Override
    public void computeEffective(IndicatorWriter writer,
                                 PerformanceEffectiveDomainExecutionContext domainContext,
                                 PerformanceGrowingSystemExecutionContext growingSystemContext) {
        // Parcelle >> SdC
        // plot: moyenne pondérée sur la surface
        Map<Scenario, Double[]> weightedValueSum = new HashMap<>();
        Map<Scenario, Double> weightSum = new HashMap<>();
        
        int reliabilityMissingFileldCounter = 0;
        int reliabilityTotalCounter = 0;
        
        Domain anonymiseDomain = domainContext.getAnonymiseDomain();
        
        for (Plot plot : effectivePlotValues.keySet()) {
            Map<Scenario, Double[]> values = effectivePlotValues.get(plot);
            
            reliabilityMissingFileldCounter += effectivePlotReliabilityFieldErrorCounter.getOrDefault(plot, 0);
            reliabilityTotalCounter += effectivePlotReliabilityTotalCounter.get(plot);
            
            // plot Area
            reliabilityTotalCounter = reliabilityTotalCounter + 1;

            double plotArea = plot.getArea();

            for (Scenario scenario : values.keySet()) {
                Double[] valuei = mults(values.get(scenario), plotArea);
                weightedValueSum.merge(scenario, valuei, GenericIndicator::sum);
    
                weightSum.merge(scenario, plotArea, Double::sum);
            }
        }
    
        Optional<GrowingSystem> optionnalAnoGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();
        
        if (!weightSum.isEmpty() && optionnalAnoGrowingSystem.isPresent()) {
        
            Collection<CroppingPlanSpecies> domainAgrosystSpecies = domainContext.getCroppingPlanSpecies();
        
            Integer reliability = computeReliabilityIndex(reliabilityMissingFileldCounter, reliabilityTotalCounter);
        
            String its = growingSystemContext.getIts();
            String irs = growingSystemContext.getIrs();
        
            effectiveGrowingSystemReliabilityFieldErrorCounter.put(optionnalAnoGrowingSystem, reliabilityMissingFileldCounter);
            effectiveGrowingSystemReliabilityTotalCounter.put(optionnalAnoGrowingSystem, reliabilityTotalCounter);
        
            Map<Scenario, Double[]> growingSystemValues = new HashMap<>();
        
            for (Scenario scenario : weightedValueSum.keySet()) {
                if (weightSum.get(scenario) != 0) {
                    String label = getPrintableScenarioLabel(scenario);
                    Double[] weightedValues = weightedValueSum.get(scenario);

                    Double plotValue = weightedValues[0] / weightSum.get(scenario);
                
                    String comments = getMessagesForScope(MissingMessageScope.GROWING_SYSTEM);
    
                    if (isWithAutoConsumed) {
                        // write SDC sheet
                        String scenarioLabel = getScenarioLabel(WITH_AUTO_CONSUME, label);
                        writer.writeEffectiveGrowingSystem(
                                its,
                                irs,
                                getIndicatorCategory(),
                                scenarioLabel,
                                plotValue,
                                reliability,
                                comments,
                                anonymiseDomain,
                                optionnalAnoGrowingSystem,
                                this.getClass(),
                                domainAgrosystSpecies);
                    }
                    Double plotValueWithoutAutoConsume = weightedValues[1] / weightSum.get(scenario);
                    if (isWithoutAutoConsumed) {
                        // write SDC sheet
                        String scenarioLabel = getScenarioLabel(WITHOUT_AUTO_CONSUME, label);
                        writer.writeEffectiveGrowingSystem(
                                its,
                                irs,
                                getIndicatorCategory(),
                                scenarioLabel,
                                plotValueWithoutAutoConsume,
                                reliability,
                                comments,
                                anonymiseDomain,
                                optionnalAnoGrowingSystem,
                                this.getClass(),
                                domainAgrosystSpecies
                        );
                    }
                    growingSystemValues.put(scenario, new Double[] { plotValue, plotValueWithoutAutoConsume });

                } else {
                    growingSystemValues.put(scenario, new Double[]{0d, 0d});
                }
            }
            effectiveGrowingSystemValues.put(optionnalAnoGrowingSystem, growingSystemValues);
        }
    }

    @Override
    public void computeEffective(IndicatorWriter writer, Domain domain) {
        Map<Scenario, Double[]> weightedValueSum = new HashMap<>();
        Map<Scenario, Double> weightSum = new HashMap<>();

        Integer domaineRmfc = 0;
        Integer domaineRtfc = 0;
    
        List<String> gst = new ArrayList<>();
        
        // moyenne pondérée sur la surface
        for (Optional<GrowingSystem> optionnalGrowingSystem : effectiveGrowingSystemValues.keySet()) {
        
            if (optionnalGrowingSystem.isPresent()) {

                GrowingSystem growingSystem = optionnalGrowingSystem.get();

                Map<Scenario, Double[]> values = effectiveGrowingSystemValues.get(optionnalGrowingSystem);

                if (growingSystem.getTypeAgriculture() != null) {
                    gst.add(growingSystem.getTypeAgriculture().getReference_label());
                }

                domaineRmfc += effectiveGrowingSystemReliabilityFieldErrorCounter.get(optionnalGrowingSystem);
                domaineRtfc += effectiveGrowingSystemReliabilityTotalCounter.get(optionnalGrowingSystem);

                // AffectedAreaRate
                domaineRtfc += 1;

                incrementAngGetTotalFieldCounterForTargetedId(growingSystem.getTopiaId());
                Double affectedAreaRate = growingSystem.getAffectedAreaRate();
                if (affectedAreaRate == null) {
                    addMissingFieldMessage(domain.getTopiaId(), messageBuilder.getDomainMissingAffectedAreaMessage());
                    domaineRmfc += 1;
                    affectedAreaRate = DEFAULT_AFFECTED_AREA_RATE;
                }

                for (Scenario scenario : values.keySet()) {
                    Double[] valuei = values.get(scenario);
                    Double[] weightedValueSumi = mults(valuei, affectedAreaRate);
                    weightedValueSum.put(scenario, weightedValueSumi);

                    weightSum.merge(scenario, affectedAreaRate, Double::sum);
                }
            }
        }

        if (!weightSum.isEmpty()) {

            Integer domaineReliability = computeReliabilityIndex(domaineRmfc, domaineRtfc);
            
            for (Scenario scenario : weightedValueSum.keySet()) {
                String label = getPrintableScenarioLabel(scenario);
                if (weightSum.get(scenario) != 0) {
                    Double[] domainValue = divs(weightedValueSum.get(scenario), weightSum.get(scenario));
    
                    String comments = getMessagesForScope(MissingMessageScope.DOMAIN);
                    
                    if (isWithAutoConsumed) {
                        // write domain sheet
                        final String scenarioLabel = getScenarioLabel(WITH_AUTO_CONSUME, label);
                        writer.writeEffectiveDomain(
                                getIndicatorCategory(),
                                scenarioLabel,
                                domainValue[0],
                                domaineReliability,
                                comments,
                                domain,
                                String.join(", ", gst)
                        );
                    }
                    if (isWithoutAutoConsumed) {
                        // write domain sheet
                        final String scenarioLabel = getScenarioLabel(WITHOUT_AUTO_CONSUME, label);
                        writer.writeEffectiveDomain(
                                getIndicatorCategory(),
                                scenarioLabel,
                                domainValue[1],
                                domaineReliability,
                                comments,
                                domain,
                                String.join(", ", gst)
                        );
                    }
    
                } else {
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn("Can't compute growing system scale with 0 weigth");
                    }
                }
            }
        }
    }
    
    public void init(IndicatorFilter grossIncomeIndicatorFilter, Collection<Scenario> scenarios) {
        
        if (grossIncomeIndicatorFilter != null && scenarios != null) {
            for (Scenario scenario : scenarios) {
                scenariosByCode.put(scenario.code(), scenario);
                scenarioCodes.add(scenario.code());
            }
            isWithAutoConsumed = grossIncomeIndicatorFilter.getWithAutoConsumed() != null
                    && grossIncomeIndicatorFilter.getWithAutoConsumed()
                    && CollectionUtils.isNotEmpty(scenarios);
            isWithoutAutoConsumed = grossIncomeIndicatorFilter.getWithoutAutoConsumed() != null
                    && grossIncomeIndicatorFilter.getWithoutAutoConsumed()
                    && CollectionUtils.isNotEmpty(scenarios);
        }
    }

    @Getter
    private static class ValorisationScenarioPrices {
        private final List<Double> withAutoConsumePrices = new ArrayList<>();
        private final List<Double> withoutAutoConsumePrices = new ArrayList<>();

        public void addWithAutoConsumePrice(double price) {
            withAutoConsumePrices.add(price);
        }
        public void addWithoutAutoConsumePrice(double price) {
            withoutAutoConsumePrices.add(price);
        }
    }
}
