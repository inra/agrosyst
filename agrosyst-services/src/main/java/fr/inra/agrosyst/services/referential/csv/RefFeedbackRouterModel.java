package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter;
import fr.inra.agrosyst.api.entities.referential.RefFeedbackRouterImpl;
import fr.inra.agrosyst.api.exceptions.AgrosystImportException;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueSetter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class RefFeedbackRouterModel extends AbstractAgrosystModel<RefFeedbackRouter> implements ExportModel<RefFeedbackRouter> {
    
    public RefFeedbackRouterModel(Set<String> userEmails) {
        super(CSV_SEPARATOR);
        newMandatoryColumn("Filiere", RefFeedbackRouter.PROPERTY_SECTOR, SECTOR_PARSER);
        newMandatoryColumn("Dispositif", RefFeedbackRouter.PROPERTY_TYPE_DEPHY, TYPE_DEPHY_PARSER);
        newMandatoryColumn("Categories", RefFeedbackRouter.PROPERTY_FEEDBACK_CATEGORY, FEED_BACK_TYPE_PARSER);
        newOptionalColumn("Actif", RefFeedbackRouter.PROPERTY_ACTIVE, ACTIVE_PARSER);
    
        newMandatoryColumn("Mails", parseEmailAddresses(userEmails, RefFeedbackRouter::setMails));
        newMandatoryColumn("Cci", parseEmailAddresses(userEmails, RefFeedbackRouter::setCci));
    }

    private ValueSetter<RefFeedbackRouter, String> parseEmailAddresses(Set<String> userEmails, BiConsumer<RefFeedbackRouter, String> setRefFeedBackValue) {
        return (refFeedbackRouter, value) -> {
            List<String> allRowEmails = new ArrayList<>();
            if (StringUtils.isNotBlank(value)) {
                String[] emails = value.split("[,;]");
                for (String email : emails) {
                    if (StringUtils.isNotBlank(email)) {
                        String pEmail = PARSE_EMAIL_ADDRESS.parse(email);
                        if (userEmails.contains(pEmail)) {
                            allRowEmails.add(pEmail);
                        } else {
                            throw new AgrosystImportException(String.format("L'email %s (%s) n'existe pas dans agrosyst.", value, pEmail));
                        }
                    }
                }
            }
            setRefFeedBackValue.accept(refFeedbackRouter, StringUtils.join(allRowEmails, ", "));
        };
    }

    @Override
    public Iterable<ExportableColumn<RefFeedbackRouter, Object>> getColumnsForExport() {
        ModelBuilder<RefFeedbackRouter> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Filiere", RefFeedbackRouter.PROPERTY_SECTOR, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Dispositif", RefFeedbackRouter.PROPERTY_TYPE_DEPHY, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Categories", RefFeedbackRouter.PROPERTY_FEEDBACK_CATEGORY, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Mails", RefFeedbackRouter.PROPERTY_MAILS);
        modelBuilder.newColumnForExport("Actif", RefFeedbackRouter.PROPERTY_ACTIVE, T_F_FORMATTER);
        modelBuilder.newColumnForExport("Cci", RefFeedbackRouter.PROPERTY_CCI);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefFeedbackRouter newEmptyInstance() {
        RefFeedbackRouter entity = new RefFeedbackRouterImpl();
        entity.setActive(true);
        return entity;
    }
}
