package fr.inra.agrosyst.services.performance;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceState;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.services.ServiceContext;
import fr.inra.agrosyst.services.async.BusinessTasksManager;
import fr.inra.agrosyst.services.async.NoResultAbstractTaskRunner;
import fr.inra.agrosyst.services.async.ScheduledTask;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;

public class PerformanceDbTaskRunner extends NoResultAbstractTaskRunner<PerformanceDbTask> {

    private static final Log LOGGER = LogFactory.getLog(PerformanceDbTaskRunner.class);

    @Override
    protected void executeTaskNoResult(PerformanceDbTask task, ServiceContext serviceContext) {
        /* Performance to generate file. */
        String performanceId = task.getPerformanceId();

        long ts = System.currentTimeMillis();
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info(String.format("%s: Starting performance task '%s' for performance %s", task.getUserEmail(), serviceContext, performanceId));
        }
        try {
            PerformanceServiceImpl performanceService = (PerformanceServiceImpl) serviceContext.newService(PerformanceService.class);

            Collection<IndicatorFilter> indicatorFilters = performanceService.getAllIndicatorFilters();

            final List<GenericIndicator> indicators = performanceService.getAllIndicators(task.getScenarioCodes());

            // start compute all indicators
            performanceService.writePerformanceToDb(task, indicatorFilters, indicators);

            BusinessTasksManager taskManager = serviceContext.getTaskManager();
            saveSuccessPerformanceStatus(performanceId, performanceService, taskManager);
    
        } catch (Exception ex) {
            try {
                PerformanceService performanceService = serviceContext.newService(PerformanceService.class);
                saveFailurePerformanceStatus(performanceId, performanceService);
            } catch (Exception e) {
                LOGGER.error(String.format("Failed to change status for performance '%s'", performanceId), e);
            }
            throw new AgrosystTechnicalException(String.format("Failed to persist the performance '%s'", performanceId), ex);
        } finally {
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info(String.format(task.getUserEmail() + ": Performance thread finished for %s in %d s", performanceId, (System.currentTimeMillis() - ts) / 1000));
            }
        }
    }

    private void saveFailurePerformanceStatus(String performanceId, PerformanceService performanceService) {
        performanceService.saveState(performanceId, PerformanceState.FAILED);
    }

    private void saveSuccessPerformanceStatus(String performanceId, PerformanceService performanceService, BusinessTasksManager businessTasksManager) {
        final long nbCurrentDbPerformanceRemainingTask = businessTasksManager != null ? businessTasksManager.getRunningAndPendingTasks()
                .stream()
                .map(ScheduledTask::getTask)
                .filter(task -> task instanceof PerformanceTask && ((PerformanceTask)task).getPerformanceId().equals(performanceId))
                .count() : -1;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("It remains %d task to perform", nbCurrentDbPerformanceRemainingTask));
        }
        // nbCurrentDbPerformanceRemainingTask == 1 because The curent Task is not ended yet.
        if (nbCurrentDbPerformanceRemainingTask == 1 && performanceService != null) {
            Performance p = performanceService.getPerformance(performanceId);
            // pour ne pas écraser les écritures d'échec
            if (PerformanceState.GENERATING.equals(p.getComputeStatus())) {
                performanceService.saveState(performanceId, PerformanceState.SUCCESS);
            }
        }
    }
    
    protected void taskFailed(PerformanceDbTask task, ServiceContext serviceContext, Exception eee) {
        if (LOGGER.isErrorEnabled()) {
            PerformanceService performanceService = serviceContext.newExpectedService(PerformanceService.class, PerformanceService.class);
            String message = String.format(
                    "Task failed with %s %s",
                    eee.getCause(),
                    eee.getMessage());
            LOGGER.error(message);
            performanceService.saveState(task.getPerformanceId(), PerformanceState.FAILED);
        }
    }
    
}
