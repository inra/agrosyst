package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCategImpl;
import fr.inra.agrosyst.services.common.CommonService;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

/**
 * id_traitement, code_traitement, nom_traitement, action, type_intrant, type_produit, ift_chimique,
 * ift_chimique_hts, ift_h, ift_f, ift_i, ift_ts, ift_a, ift_hh, ift_moy_bio, Source.
 * 
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class RefActaTraitementsProduitsCategModel extends AbstractAgrosystModel<RefActaTraitementsProduitsCateg> implements ExportModel<RefActaTraitementsProduitsCateg> {

    protected static final String GROWTH_SUBSTANCES="Substances de croissance";
    protected static final String ASSOCIATIONS="Associations";
    protected static final String OTHER="Autres";
    protected static final String CHEMICAL_MEDIATORS="Médiateurs chimiques";
    protected static final String ADJUVANTS="Adjuvants";
    protected static final String FERTILIZING_MATERIALS_AND_GROWING_MEDIA="Matières Fertilisantes et Supports de Culture";
    protected static final String FUNGI_PREPARATIONS="Préparations fongiques";
    protected static final String VIRUS_PREPARATIONS="Préparations virales";
    protected static final String NATURAL_CONTROL_SYSTEM_STIMULATOR="Stimulateurs des défenses naturelles";
    protected static final String FUNGICIDAL="Fongicides";
    protected static final String MASS_TRAPPING="Piégeage de masse";
    protected static final String SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION="Inoculation biologique de semences ou plants";
    protected static final String VARIOUS="Divers";
    protected static final String INSECTICIDAL="Insecticides";
    protected static final String MOLLUSCICIDAL="Molluscicides";
    protected static final String NEMATICIDAL="Nématicides";
    protected static final String MACRO_ORGANISMS="Macro-organismes";
    protected static final String HERBICIDAL="Herbicides";
    protected static final String BIOLOGICAL_FUNGICIDAL_SOILTREATMENT_MEANS="Moyens biologiques - Fongicides - Traitement du sol";
    protected static final String BACTERIAL_PREPARATIONS="Préparations bactériennes";

    protected static final ValueParser<ProductType> PRODUCT_TYPE_PARSER = value -> {
        ProductType result = null;
        if (!Strings.isNullOrEmpty(value)) {
            String strValue = StringUtils.deleteWhitespace(StringUtils.stripAccents(value));
            if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(GROWTH_SUBSTANCES)))) {
                result = ProductType.GROWTH_SUBSTANCES;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(ASSOCIATIONS)))) {
                result = ProductType.ASSOCIATIONS;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(OTHER)))) {
                result = ProductType.OTHER;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(CHEMICAL_MEDIATORS)))) {
                result = ProductType.CHEMICAL_MEDIATORS;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(ADJUVANTS)))) {
                result = ProductType.ADJUVANTS;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(FERTILIZING_MATERIALS_AND_GROWING_MEDIA)))) {
                result = ProductType.FERTILIZING_MATERIALS_AND_GROWING_MEDIA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(FUNGI_PREPARATIONS)))) {
                result = ProductType.FUNGI_PREPARATIONS;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(VIRUS_PREPARATIONS)))) {
                result = ProductType.VIRUS_PREPARATIONS;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(NATURAL_CONTROL_SYSTEM_STIMULATOR)))) {
                result = ProductType.NATURAL_CONTROL_SYSTEM_STIMULATOR;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(FUNGICIDAL)))) {
                result = ProductType.FUNGICIDAL;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(MASS_TRAPPING)))) {
                result = ProductType.MASS_TRAPPING;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION)))) {
                result = ProductType.SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(VARIOUS)))) {
                result = ProductType.VARIOUS;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(INSECTICIDAL)))) {
                result = ProductType.INSECTICIDAL;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(MOLLUSCICIDAL)))) {
                result = ProductType.MOLLUSCICIDAL;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(NEMATICIDAL)))) {
                result = ProductType.NEMATICIDAL;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(MACRO_ORGANISMS)))) {
                result = ProductType.MACRO_ORGANISMS;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(HERBICIDAL)))) {
                result = ProductType.HERBICIDAL;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(BIOLOGICAL_FUNGICIDAL_SOILTREATMENT_MEANS)))) {
                result = ProductType.BIOLOGICAL_FUNGICIDAL_SOILTREATMENT_MEANS;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(BACTERIAL_PREPARATIONS)))) {
                result = ProductType.BACTERIAL_PREPARATIONS;
            } else {
                result = (ProductType) getGenericEnumParser(ProductType.class, value);
            }
        }
        return result;
    };

    protected static final ValueFormatter<ProductType> PRODUCT_TYPE_FORMATTER = value -> {
        String result = null;
        if (value != null) {
            result = switch (value) {
                case GROWTH_SUBSTANCES -> GROWTH_SUBSTANCES;
                case ASSOCIATIONS -> ASSOCIATIONS;
                case OTHER -> OTHER;
                case CHEMICAL_MEDIATORS -> CHEMICAL_MEDIATORS;
                case ADJUVANTS -> ADJUVANTS;
                case FERTILIZING_MATERIALS_AND_GROWING_MEDIA -> FERTILIZING_MATERIALS_AND_GROWING_MEDIA;
                case FUNGI_PREPARATIONS -> FUNGI_PREPARATIONS;
                case VIRUS_PREPARATIONS -> VIRUS_PREPARATIONS;
                case NATURAL_CONTROL_SYSTEM_STIMULATOR -> NATURAL_CONTROL_SYSTEM_STIMULATOR;
                case FUNGICIDAL -> FUNGICIDAL;
                case SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION -> SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION;
                case MASS_TRAPPING -> MASS_TRAPPING;
                case VARIOUS -> VARIOUS;
                case INSECTICIDAL -> INSECTICIDAL;
                case MOLLUSCICIDAL -> MOLLUSCICIDAL;
                case NEMATICIDAL -> NEMATICIDAL;
                case MACRO_ORGANISMS -> MACRO_ORGANISMS;
                case HERBICIDAL -> HERBICIDAL;
                case BIOLOGICAL_FUNGICIDAL_SOILTREATMENT_MEANS -> BIOLOGICAL_FUNGICIDAL_SOILTREATMENT_MEANS;
                case BACTERIAL_PREPARATIONS -> BACTERIAL_PREPARATIONS;
                default -> value.name().toLowerCase();
            };
        }
        return result;
    };

    public RefActaTraitementsProduitsCategModel() {
        super(CSV_SEPARATOR);
        //id_produit;nom_produit;id_traitement;code_traitement;nom_traitement;Source
        newMandatoryColumn("id_traitement", RefActaTraitementsProduitsCateg.PROPERTY_ID_TRAITEMENT, INT_PARSER);
        newMandatoryColumn("code_traitement", RefActaTraitementsProduitsCateg.PROPERTY_CODE_TRAITEMENT);
        newMandatoryColumn("nom_traitement", RefActaTraitementsProduitsCateg.PROPERTY_NOM_TRAITEMENT);
        newMandatoryColumn("action", RefActaTraitementsProduitsCateg.PROPERTY_ACTION, AGROSYST_INTERVENTION_TYPE_PARSER);
        newIgnoredColumn("type_intrant");
        newMandatoryColumn("type_produit", RefActaTraitementsProduitsCateg.PROPERTY_TYPE_PRODUIT, PRODUCT_TYPE_PARSER);
        newMandatoryColumn("ift_chimique_total", RefActaTraitementsProduitsCateg.PROPERTY_IFT_CHIMIQUE_TOTAL, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("ift_chimique_tot_hts", RefActaTraitementsProduitsCateg.PROPERTY_IFT_CHIMIQUE_TOT_HTS, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("ift_h", RefActaTraitementsProduitsCateg.PROPERTY_IFT_H, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("ift_f", RefActaTraitementsProduitsCateg.PROPERTY_IFT_F, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("ift_i", RefActaTraitementsProduitsCateg.PROPERTY_IFT_I, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("ift_ts", RefActaTraitementsProduitsCateg.PROPERTY_IFT_TS, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("ift_a", RefActaTraitementsProduitsCateg.PROPERTY_IFT_A, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("ift_hh", RefActaTraitementsProduitsCateg.PROPERTY_IFT_HH, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("ift_moy_bio", RefActaTraitementsProduitsCateg.PROPERTY_IFT_MOY_BIO, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("Source", RefActaTraitementsProduitsCateg.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefActaTraitementsProduitsCateg.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefActaTraitementsProduitsCateg, Object>> getColumnsForExport() {
        ModelBuilder<RefActaTraitementsProduitsCateg> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("id_traitement", RefActaTraitementsProduitsCateg.PROPERTY_ID_TRAITEMENT, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("code_traitement", RefActaTraitementsProduitsCateg.PROPERTY_CODE_TRAITEMENT);
        modelBuilder.newColumnForExport("nom_traitement", RefActaTraitementsProduitsCateg.PROPERTY_NOM_TRAITEMENT);
        modelBuilder.newColumnForExport("action", RefActaTraitementsProduitsCateg.PROPERTY_ACTION, AGROSYST_INTERVENTION_TYPE_FORMATTER);
        modelBuilder.newColumnForExport("type_produit", RefActaTraitementsProduitsCateg.PROPERTY_TYPE_PRODUIT, PRODUCT_TYPE_FORMATTER);
        modelBuilder.newColumnForExport("ift_chimique_total", RefActaTraitementsProduitsCateg.PROPERTY_IFT_CHIMIQUE_TOTAL, O_N_FORMATTER);
        modelBuilder.newColumnForExport("ift_chimique_tot_hts", RefActaTraitementsProduitsCateg.PROPERTY_IFT_CHIMIQUE_TOT_HTS, O_N_FORMATTER);
        modelBuilder.newColumnForExport("ift_h", RefActaTraitementsProduitsCateg.PROPERTY_IFT_H, O_N_FORMATTER);
        modelBuilder.newColumnForExport("ift_f", RefActaTraitementsProduitsCateg.PROPERTY_IFT_F, O_N_FORMATTER);
        modelBuilder.newColumnForExport("ift_i", RefActaTraitementsProduitsCateg.PROPERTY_IFT_I, O_N_FORMATTER);
        modelBuilder.newColumnForExport("ift_ts", RefActaTraitementsProduitsCateg.PROPERTY_IFT_TS, O_N_FORMATTER);
        modelBuilder.newColumnForExport("ift_a", RefActaTraitementsProduitsCateg.PROPERTY_IFT_A, O_N_FORMATTER);
        modelBuilder.newColumnForExport("ift_hh", RefActaTraitementsProduitsCateg.PROPERTY_IFT_HH, O_N_FORMATTER);
        modelBuilder.newColumnForExport("ift_moy_bio", RefActaTraitementsProduitsCateg.PROPERTY_IFT_MOY_BIO, O_N_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefActaTraitementsProduitsCateg.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefActaTraitementsProduitsCateg.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefActaTraitementsProduitsCateg newEmptyInstance() {
        RefActaTraitementsProduitsCateg refActaTraitementsProduitsCateg = new RefActaTraitementsProduitsCategImpl();
        refActaTraitementsProduitsCateg.setActive(true);
        return refActaTraitementsProduitsCateg;
    }
}
