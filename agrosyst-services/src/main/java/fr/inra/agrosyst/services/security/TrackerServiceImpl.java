package fr.inra.agrosyst.services.security;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.entities.security.AgrosystUserTopiaDao;
import fr.inra.agrosyst.api.entities.security.TrackedEvent;
import fr.inra.agrosyst.api.entities.security.TrackedEventTopiaDao;
import fr.inra.agrosyst.api.entities.security.TrackedEventType;
import fr.inra.agrosyst.api.entities.security.UserRole;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.security.TrackedEventFilter;
import fr.inra.agrosyst.api.services.security.TrackerService;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import lombok.Setter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.List;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
@Setter
public class TrackerServiceImpl extends AbstractAgrosystService implements TrackerService {
    
    protected BusinessAuthorizationService businessAuthorizationService;

    protected AgrosystUserTopiaDao usersDao;
    protected TrackedEventTopiaDao eventsDao;

    @Override
    public PaginationResult<TrackedEvent> list(TrackedEventFilter filter) {
        businessAuthorizationService.checkIsAdmin();
    
        PaginationResult<TrackedEvent> result = eventsDao.getFilteredTrackedEvents(filter);
        return result;
    }

    protected AgrosystUser getCurrentUser() {
        AgrosystUser result = null;
        try {
            String userId = getSecurityContext().getUserId();
            result = usersDao.forTopiaIdEquals(userId).findUnique();
        } catch (Exception eee) {
            // Aïe
        }
        return result;
    }

    protected void create(TrackedEventType type, List<String> args) {
        eventsDao.create(
                TrackedEvent.PROPERTY_DATE, context.getCurrentTime(),
                TrackedEvent.PROPERTY_AUTHOR, getCurrentUser(),
                TrackedEvent.PROPERTY_TYPE, type,
                TrackedEvent.PROPERTY_ARGS, context.getGson().toJson(args)
        );
    }

    public void userCreated(AgrosystUser user) {
        List<String> args = Lists.newArrayList(user.getEmail());
        create(TrackedEventType.USER_CREATION, args);
    }

    public void userModified(AgrosystUser user, boolean passwordChange) {
        List<String> args = Lists.newArrayList(user.getEmail(), String.valueOf(passwordChange));
        AgrosystUser currentUser = getCurrentUser();
        TrackedEventType type = user.equals(currentUser) ? TrackedEventType.USER_MOD_SELF : TrackedEventType.USER_MOD;
        create(type, args);
    }

    public void userActivation(AgrosystUser user, boolean active) {
        List<String> args = Lists.newArrayList(user.getEmail(), String.valueOf(active));
        create(TrackedEventType.USER_ACTIVATION, args);
    }

    protected List<String> roleToArgs(UserRole role) {
        List<String> result = Lists.newArrayList(role.getType().name());
        switch (role.getType()) {
            case DOMAIN_RESPONSIBLE:
                Domain domain = context.getDaoSupplier().getDomainDao().forCodeEquals(role.getDomainCode()).findAny();
                result.add(domain.getName());
                result.add("Toutes campagnes");
                break;
            case GROWING_PLAN_RESPONSIBLE:
                GrowingPlan growingPlan = context.getDaoSupplier().getGrowingPlanDao().forCodeEquals(role.getGrowingPlanCode()).findAny();
                result.add(growingPlan.getName());
                result.add("Toutes campagnes");
                break;
            case GS_DATA_PROCESSOR:
                if (Strings.isNullOrEmpty(role.getGrowingSystemCode())) {
                    GrowingSystem growingSystem = context.getDaoSupplier().getGrowingSystemDao().forTopiaIdEquals(role.getGrowingSystemId()).findUnique();
                    result.add(growingSystem.getName());
                    result.add(String.valueOf(growingSystem.getGrowingPlan().getDomain().getCampaign()));
                } else {
                    GrowingSystem growingSystem = context.getDaoSupplier().getGrowingSystemDao().forCodeEquals(role.getGrowingSystemCode()).findAny();
                    result.add(growingSystem.getName());
                    result.add("Toutes campagnes");
                }
                break;
            case NETWORK_RESPONSIBLE:
            case NETWORK_SUPERVISOR:
                Network network = context.getDaoSupplier().getNetworkDao().forTopiaIdEquals(role.getNetworkId()).findUnique();
                result.add(network.getName());
                break;
        }
        result.add(role.getAgrosystUser().getEmail());
        return result;
    }

    public void roleAdded(UserRole role) {
        List<String> args = roleToArgs(role);
        create(TrackedEventType.USER_ROLE_ADD, args);
    }

    public void roleRemoved(UserRole role) {
        List<String> args = roleToArgs(role);
        create(TrackedEventType.USER_ROLE_REMOVE, args);
    }

}
