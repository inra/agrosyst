package fr.inra.agrosyst.services.edaplos;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.csvreader.CsvWriter;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.edaplos.EdaplosParsingResult;
import fr.inra.agrosyst.api.services.edaplos.EdaplosResultLog;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Export parsing result as CSV.
 */
public class EdaplosCSVExporter {

    public InputStream export(EdaplosParsingResult edaplosParsingResult) {

        ByteArrayInputStream input;
        try(ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            CsvWriter csvWriter = new CsvWriter(out, ';', StandardCharsets.UTF_8);

            // header
            csvWriter.write("Level");
            csvWriter.write("Message");
            csvWriter.write("Contexte");
            csvWriter.endRecord();

            // add filename
            csvWriter.write(EdaplosResultLog.EdaplosResultLevel.INFO.name());
            csvWriter.write("Fichier : " + edaplosParsingResult.getFilename());
            csvWriter.endRecord();

            // add report
            for (EdaplosResultLog m : edaplosParsingResult.getMessages()) {
                csvWriter.write(m.getLevel().name());
                csvWriter.write(m.getLabel());
                csvWriter.write(m.getPath());
                csvWriter.endRecord();
            }
            csvWriter.close();

            input = new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new AgrosystTechnicalException("Can't export as csv", e);
        }

        return input;
    }
}
