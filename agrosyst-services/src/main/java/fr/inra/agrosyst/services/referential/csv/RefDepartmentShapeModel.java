package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Splitter;
import fr.inra.agrosyst.api.entities.referential.RefDepartmentShape;
import fr.inra.agrosyst.api.entities.referential.RefDepartmentShapeImpl;
import org.nuiton.csv.ValueSetter;

import java.util.Iterator;

/**
 * @author David Cossé
 */
public class RefDepartmentShapeModel extends AbstractAgrosystModel<RefDepartmentShape> {

    public static final ValueSetter<RefDepartmentShape, String> SHAPE_LAT_LON_SETTER = (shape, geomXY) -> {
        Iterable<String> split = Splitter.on(",").trimResults().omitEmptyStrings().split(geomXY);
        Iterator<String> coords = split.iterator();
        shape.setLatitude(DOUBLE_PARSER.parse(coords.next()));
        shape.setLongitude(DOUBLE_PARSER.parse(coords.next()));
    };

    public RefDepartmentShapeModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("geom_x_y", SHAPE_LAT_LON_SETTER);
        newMandatoryColumn("code_dept", RefDepartmentShape.PROPERTY_DEPARTMENT);
        newMandatoryColumn("geom", RefDepartmentShape.PROPERTY_SHAPE);
        newIgnoredColumn("id_geofla");
        newIgnoredColumn("nom_dept");
        newIgnoredColumn("code_chf");
        newIgnoredColumn("nom_chf");
        newIgnoredColumn("x_chf_lieu");
        newIgnoredColumn("y_chf_lieu");
        newIgnoredColumn("x_centroid");
        newIgnoredColumn("y_centroid");
        newIgnoredColumn("code_reg");
        newIgnoredColumn("nom_region");
        newOptionalColumn(COLUMN_ACTIVE, RefDepartmentShape.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public RefDepartmentShape newEmptyInstance() {
        RefDepartmentShape refDepartmentShape = new RefDepartmentShapeImpl();
        refDepartmentShape.setActive(true);
        return refDepartmentShape;
    }
}
