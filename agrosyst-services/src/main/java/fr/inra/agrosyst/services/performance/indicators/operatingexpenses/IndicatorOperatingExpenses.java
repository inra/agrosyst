package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.OtherProductInputUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PotInputUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.MissingFieldMessage;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static org.nuiton.i18n.I18n.l;

/**
 * Les charges opérationnelles réelles sont exprimées en €/ha.
 * Elles correspondent aux dépenses liées à l’achat des intrants (semences et plants,
 * produits fertilisants minéraux  et  organiques, traitement de semences et plants,
 * irrigation, produits phytosanitaires, produits de lutte biologique, autres intrants, substrats, pots).
 * <p>
 * Le calcul se fait sur la base des prix saisis par l’utilisateur.
 * <p>
 * Les interventions concernées par cet indicateur sont toutes les interventions
 * contenant un intrant et/ou une action de type « Semis » et/ou une action de
 * type « Irrigation ».
 * <p>
 * Formule de calcul:
 *
 * <pre>
 * CI réel_i:
 *
 * = PSCi * ( sum(Q_ev * PA_ev) +
 *    sum(Q_j * PA_j) +
 *    sum(Q_e * PA_e) +
 *    sum(Q_a * PA_a)) +
 *    PSCi_phyto * sum(Q_k * PA_k) +
 *    PSCi_luttebio * sum(Q_h * PA_h)
 *
 * Avec :
 * - CI réel_i : charges intrants de l’intervention i
 * - PSCi (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils
 *   ou l’opération manuelle de l’intervention i.
 *   PSCi est calculé sur la base des données saisies par l’utilisateur.
 *
 *  - Semis :
 *   - Q_ev (diverses unités) : quantité semée du couple EV, EV appartenant à la liste des couples EV semés
 *     dans l’action semis de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *   - PA_ev (diverses unités) : prix d’achat du couple EV pour ce type de semence (de ferme, certifiées ...),
 *     EV appartenant à la liste des couples EV semés au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *      - ATTENTION :  dès lors que la case « traitement inclus ... » est cochée, ne plus réaliser l’étape décrite dans
 *        le paragraphe ci-dessous pour ce qui est du traitement de semence (par contre, réaliser l’opération pour les
 *        intrants de fertilisation). Car il ne faut pas compter cet intrant 2 fois.
 *
 * - Fertilisation minérale et organique  – Traitements de semences
 *   - Q_j (diverses unités) : quantité de l’intrant j, j appartenant à la liste
 *     des intrants de type « Traitements de semences »,
 *     « Engrais/amendement (organo) minéral »
 *     ou « Engrais/amendement organique » appliqués au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *   - PA_j (diverses unités) : prix d’achat de l’intrant j, j appartenant à la liste
 *     des intrants de type « Traitements de semences »,
 *     « Engrais/amendement (organo) minéral »
 *     ou « Engrais/amendement organique » appliqués au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *
 * - Irrigation
 *   - Q_e (diverses unités) : quantité d’eau déclarée dans l’action de type Irrigation
 *     au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *   - PA_e (€/m³) : prix d’achat de l’eau.
 *
 *     Si unité = €/ha, alors charge de l'intervention = PSCi x Prix en €/ha
 *
 * - Autres intrants
 *   - Q_a (diverses unités) : quantité de l'intrant autre. Donnée saisie par l’utilisateur.
 *   - PA_a (unité unique) : pour le moment, l’utilisateur ne peut saisir un prix que dans une seule unité : Euros/ha.
 *     Mais il faut prévoir à termes que l’utilisateur puisse saisir d’autres unités
 *     et donc adopter le même fonctionnement que pour tous les autres intrants. Donnée saisie par l’utilisateur.
 *
 * - Phytosanitaire
 *   - PSCi_phyto (sans unité) : proportion de surface concernée par
 *     l’action de type application de produit phytosanitaire.
 *   - Q_k (diverses unités) : quantité de l’intrant k, k appartenant à la liste des intrants de type
 *     « Phytosanitaire » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *   - PA_k (diverses unités) : prix d’achat de l ’intrant k, k appartenant à la liste des intrants de type
 *     « Phytosanitaire » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *
 * - Lutte biologique
 *   - PSCi_luttebio (sans unité) : proportion de surface concernée par l’action de type lutte biologique.
 *   - Q_h (diverses unités) : quantité de l’intrant h, h appartenant à la liste des intrants de type
 *     « Lutte biologique » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *   - PA_h (diverses unités) : prix d’achat de l’intrant h,
 *     h appartenant à la liste des intrants de type « Lutte biologique » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *
 * </pre>
 *
 * @author Yannick Martel (martel@codelutin.com)
 */
public class IndicatorOperatingExpenses extends AbstractIndicator {

    protected static final Log LOGGER = LogFactory.getLog(IndicatorOperatingExpenses.class);

    public static final String CHARGES_OPERATIONNELLES_REELLES_TX_COMP = "charges_operationnelles_tot_reelles_taux_de_completion";
    public static final String CHARGES_OPERATIONNELLES_STD_MIL_TX_COMP = "charges_operationnelles_tot_std_mil_taux_de_completion";
    public static final String CHARGES_OPERATIONNELLES_REELLES_DETAIL = "charges_operationnelles_tot_reelles_detail_champs_non_renseig";
    public static final String CHARGES_OPERATIONNELLES_STD_MIL_DETAIL = "charges_operationnelles_tot_std_mil_detail_champs_non_renseig";

    protected static final Double DEFAULT_REAL_VALUE = 0d;
    protected static final Double DEFAULT_STANDARDIZED_VALUE = 0d;
    protected static final Pair<Double, Double> DEFAULT_REAL_AND_STANDARDIZED_VALUES = Pair.of(DEFAULT_REAL_VALUE, DEFAULT_STANDARDIZED_VALUE);
    protected boolean computeReal = true;
    protected boolean computStandardised = true;

    protected static final String[] LABELS = {
            "Indicator.label.operatingExpensesReal",
            "Indicator.label.operatingExpensesStandard"
    };

    public IndicatorOperatingExpenses() {
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_OPERATIONNELLES_REELLES_TX_COMP,
                CHARGES_OPERATIONNELLES_REELLES_TX_COMP
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_OPERATIONNELLES_REELLES_DETAIL,
                CHARGES_OPERATIONNELLES_REELLES_DETAIL
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_OPERATIONNELLES_STD_MIL_TX_COMP,
                CHARGES_OPERATIONNELLES_STD_MIL_TX_COMP
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_OPERATIONNELLES_STD_MIL_DETAIL,
                CHARGES_OPERATIONNELLES_STD_MIL_DETAIL
        );
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = new HashMap<>();

        indicatorNameToColumnName.put(getIndicatorLabel(0), "charges_operationnelles_tot_reelles");
        indicatorNameToColumnName.put(getIndicatorLabel(1), "charges_operationnelles_tot_std_mil");

        indicatorNameToColumnName.put(CHARGES_OPERATIONNELLES_REELLES_TX_COMP, CHARGES_OPERATIONNELLES_REELLES_TX_COMP);
        indicatorNameToColumnName.put(CHARGES_OPERATIONNELLES_STD_MIL_TX_COMP, CHARGES_OPERATIONNELLES_STD_MIL_TX_COMP);
        indicatorNameToColumnName.put(CHARGES_OPERATIONNELLES_REELLES_DETAIL, CHARGES_OPERATIONNELLES_REELLES_DETAIL);
        indicatorNameToColumnName.put(CHARGES_OPERATIONNELLES_STD_MIL_DETAIL, CHARGES_OPERATIONNELLES_STD_MIL_DETAIL);

        return indicatorNameToColumnName;
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return displayed && (i == 0 && computeReal || i == 1 && computStandardised);
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return true;// display at all level
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, Indicator.INDICATOR_CATEGORY_ECONOMIC);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }

    Pair<Double, Double> computeSeedingForCropAndProduct(PerformanceGlobalExecutionContext globalExecutionContext,
                                                         WriterContext writerContext,
                                                         PerformancePracticedSystemExecutionContext practicedSystemContext,
                                                         PerformancePracticedInterventionExecutionContext interventionContext) {
        IndicatorSeedingOperatingExpenses indicatorSeedingOperatingExpenses = new IndicatorSeedingOperatingExpenses(
                displayed,
                computeReal,
                computStandardised,
                locale
        );
        var result = indicatorSeedingOperatingExpenses.computeOperatingExpenses(
                writerContext,
                practicedSystemContext,
                interventionContext,
                globalExecutionContext.getConvertersBySeedingProductUnit(),
                globalExecutionContext.getConvertersByPhytoProductUnit()
        );
        reportFieldsStats(indicatorSeedingOperatingExpenses.totalFieldCounter, indicatorSeedingOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Pair<Double, Double> computeSeedingForCropAndProduct(PerformanceGlobalExecutionContext globalExecutionContext,
                                                         WriterContext writerContext,
                                                         PerformanceEffectiveDomainExecutionContext domainContext,
                                                         PerformanceZoneExecutionContext zoneContext,
                                                         PerformanceEffectiveInterventionExecutionContext interventionContext) {
        IndicatorSeedingOperatingExpenses indicatorSeedingOperatingExpenses = new IndicatorSeedingOperatingExpenses(
                displayed,
                computeReal,
                computStandardised,
                locale
        );
        var result = indicatorSeedingOperatingExpenses.computeOperatingExpenses(
                writerContext,
                domainContext,
                zoneContext,
                interventionContext,
                globalExecutionContext.getConvertersBySeedingProductUnit(),
                globalExecutionContext.getConvertersByPhytoProductUnit()
        );
        reportFieldsStats(indicatorSeedingOperatingExpenses.totalFieldCounter, indicatorSeedingOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Pair<Double, Double> computeSeedingForCrop(PerformanceGlobalExecutionContext globalExecutionContext,
                                               WriterContext writerContext,
                                               PerformancePracticedSystemExecutionContext practicedSystemContext,
                                               PerformancePracticedInterventionExecutionContext interventionContext,
                                               Set<SeedingActionUsage> actionCropPricesIncludedTreatment) {
        IndicatorSeedingOperatingExpenses indicatorSeedingOperatingExpenses = new IndicatorSeedingOperatingExpenses(
                displayed,
                computeReal,
                computStandardised,
                locale
        );
        var result = indicatorSeedingOperatingExpenses.computeOperatingExpensesForCrop(
                writerContext,
                practicedSystemContext,
                interventionContext,
                globalExecutionContext.getConvertersBySeedingProductUnit(),
                IndicatorDecomposedOperatingExpenses.class,
                IndicatorDecomposedOperatingExpenses.SEMIS_LABELS,
                actionCropPricesIncludedTreatment
        );
        reportFieldsStats(indicatorSeedingOperatingExpenses.totalFieldCounter, indicatorSeedingOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Pair<Double, Double> computeSeedingForCrop(PerformanceGlobalExecutionContext globalExecutionContext,
                                               WriterContext writerContext,
                                               PerformanceEffectiveDomainExecutionContext domainContext,
                                               PerformanceEffectiveInterventionExecutionContext interventionContext,
                                               Set<SeedingActionUsage> actionCropPricesIncludedTreatment) {
        IndicatorSeedingOperatingExpenses indicatorSeedingOperatingExpenses = new IndicatorSeedingOperatingExpenses(
                displayed,
                computeReal,
                computStandardised,
                locale
        );
        var result = indicatorSeedingOperatingExpenses.computeOperatingExpensesForCrop(
                writerContext,
                domainContext,
                interventionContext,
                globalExecutionContext.getConvertersBySeedingProductUnit(),
                IndicatorDecomposedOperatingExpenses.class,
                IndicatorDecomposedOperatingExpenses.SEMIS_LABELS,
                actionCropPricesIncludedTreatment
        );
        reportFieldsStats(indicatorSeedingOperatingExpenses.totalFieldCounter, indicatorSeedingOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Pair<Double, Double> computeSeedingForProduct(PerformanceGlobalExecutionContext globalExecutionContext,
                                                  WriterContext writerContext,
                                                  PerformancePracticedSystemExecutionContext practicedSystemContext,
                                                  PerformancePracticedInterventionExecutionContext interventionContext,
                                                  Set<SeedingActionUsage> actionCropPricesIncludedTreatment) {
        IndicatorSeedingOperatingExpenses indicatorSeedingOperatingExpenses = new IndicatorSeedingOperatingExpenses(
                displayed,
                computeReal,
                computStandardised,
                locale
        );
        var result = indicatorSeedingOperatingExpenses.computeOperatingExpensesForProduct(
                writerContext,
                practicedSystemContext,
                interventionContext,
                globalExecutionContext.getConvertersByPhytoProductUnit(),
                IndicatorDecomposedOperatingExpenses.class,
                IndicatorDecomposedOperatingExpenses.TRAITEMENTS_DE_SEMENCES_LABELS,
                actionCropPricesIncludedTreatment
        );
        reportFieldsStats(indicatorSeedingOperatingExpenses.totalFieldCounter, indicatorSeedingOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Pair<Double, Double> computeSeedingForProduct(PerformanceGlobalExecutionContext globalExecutionContext,
                                                  WriterContext writerContext,
                                                  PerformanceEffectiveDomainExecutionContext domainContext,
                                                  PerformanceEffectiveInterventionExecutionContext interventionContext,
                                                  Set<SeedingActionUsage> actionCropPricesIncludedTreatment) {
        IndicatorSeedingOperatingExpenses indicatorSeedingOperatingExpenses = new IndicatorSeedingOperatingExpenses(
                displayed,
                computeReal,
                computStandardised,
                locale
        );
        var result = indicatorSeedingOperatingExpenses.computeOperatingExpensesForProduct(
                writerContext,
                domainContext,
                interventionContext,
                globalExecutionContext.getConvertersByPhytoProductUnit(),
                IndicatorDecomposedOperatingExpenses.class,
                IndicatorDecomposedOperatingExpenses.TRAITEMENTS_DE_SEMENCES_LABELS,
                actionCropPricesIncludedTreatment
        );
        reportFieldsStats(indicatorSeedingOperatingExpenses.totalFieldCounter, indicatorSeedingOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computeMineral(PerformanceGlobalExecutionContext globalExecutionContext,
                            WriterContext writerContext,
                            PerformancePracticedSystemExecutionContext practicedSystemContext,
                            PerformancePracticedInterventionExecutionContext interventionContext,
                            Class<? extends GenericIndicator> indicatorClass,
                            String[] labels) {
        Map<MineralProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByMineralProductUnit = globalExecutionContext.getConvertersByMineralProductUnit();
        Collection<RefInputUnitPriceUnitConverter> mineralProductConverters = MapUtils.emptyIfNull(convertersByMineralProductUnit).values().stream().flatMap(Collection::stream).toList();
        IndicatorMineralProductOperatingExpenses indicatorMineralProductOperatingExpenses = new IndicatorMineralProductOperatingExpenses(
                displayed,
                computeReal,
                computStandardised,
                locale
        );
        var result = indicatorMineralProductOperatingExpenses.computeOperatingExpenses(
                writerContext,
                practicedSystemContext,
                interventionContext,
                mineralProductConverters,
                indicatorClass,
                labels
        );
        reportFieldsStats(indicatorMineralProductOperatingExpenses.totalFieldCounter, indicatorMineralProductOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computeMineral(PerformanceGlobalExecutionContext globalExecutionContext,
                            WriterContext writerContext,
                            PerformanceEffectiveDomainExecutionContext domainContext,
                            PerformanceZoneExecutionContext zoneContext,
                            PerformanceEffectiveInterventionExecutionContext interventionContext,
                            Class<? extends GenericIndicator> indicatorClass,
                            String[] labels) {
        Map<MineralProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByMineralProductUnit = globalExecutionContext.getConvertersByMineralProductUnit();
        Collection<RefInputUnitPriceUnitConverter> mineralConverters = MapUtils.emptyIfNull(convertersByMineralProductUnit).values().stream().flatMap(Collection::stream).toList();
        IndicatorMineralProductOperatingExpenses indicatorMineralProductOperatingExpenses =
                new IndicatorMineralProductOperatingExpenses(displayed, computeReal, computStandardised, locale);
        var result = indicatorMineralProductOperatingExpenses.computeOperatingExpenses(
                writerContext,
                domainContext,
                zoneContext.getZone(),
                interventionContext,
                mineralConverters,
                indicatorClass,
                labels
        );
        reportFieldsStats(indicatorMineralProductOperatingExpenses.totalFieldCounter, indicatorMineralProductOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computeOrganic(PerformanceGlobalExecutionContext globalExecutionContext,
                            WriterContext writerContext,
                            PerformancePracticedSystemExecutionContext practicedSystemContext,
                            PerformancePracticedInterventionExecutionContext interventionContext,
                            Class<? extends GenericIndicator> indicatorClass,
                            String[] labels) {
        Map<OrganicProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByOrganicProductUnit = globalExecutionContext.getConvertersByOrganicProductUnit();
        Collection<RefInputUnitPriceUnitConverter> organicProductConverters = MapUtils.emptyIfNull(convertersByOrganicProductUnit).values().stream().flatMap(Collection::stream).toList();
        IndicatorOrganicProductOperatingExpenses indicatorOrganicProductOperatingExpenses =
                new IndicatorOrganicProductOperatingExpenses(
                        displayed,
                        computeReal,
                        computStandardised,
                        locale
                );
        var result = indicatorOrganicProductOperatingExpenses.computeOperatingExpenses(
                writerContext,
                practicedSystemContext,
                interventionContext,
                organicProductConverters,
                indicatorClass,
                labels
        );
        reportFieldsStats(indicatorOrganicProductOperatingExpenses.totalFieldCounter, indicatorOrganicProductOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computeOrganic(PerformanceGlobalExecutionContext globalExecutionContext,
                            WriterContext writerContext,
                            PerformanceEffectiveDomainExecutionContext domainContext,
                            PerformanceZoneExecutionContext zoneContext,
                            PerformanceEffectiveInterventionExecutionContext interventionContext,
                            Class<? extends GenericIndicator> indicatorClass,
                            String[] labels) {
        Map<OrganicProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByOrganicProductUnit = globalExecutionContext.getConvertersByOrganicProductUnit();
        Collection<RefInputUnitPriceUnitConverter> organicConverters = MapUtils.emptyIfNull(convertersByOrganicProductUnit).values().stream()
                .flatMap(Collection::stream).toList();
        IndicatorOrganicProductOperatingExpenses indicatorOrganicProductOperatingExpenses =
                new IndicatorOrganicProductOperatingExpenses(
                        displayed,
                        computeReal,
                        computStandardised,
                        locale
                );
        var result = indicatorOrganicProductOperatingExpenses.computeOperatingExpenses(
                writerContext,
                domainContext,
                zoneContext.getZone(),
                interventionContext,
                organicConverters,
                indicatorClass,
                labels
        );
        reportFieldsStats(indicatorOrganicProductOperatingExpenses.totalFieldCounter, indicatorOrganicProductOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computeIrrig(WriterContext writerContext,
                          PerformancePracticedSystemExecutionContext practicedSystemContext,
                          PerformancePracticedInterventionExecutionContext interventionContext,
                          Class<? extends GenericIndicator> indicatorClass,
                          String[] labels) {
        IndicatorIrrigationOperatingExpenses indicatorIrrigationOperatingExpenses =
                new IndicatorIrrigationOperatingExpenses(
                        displayed,
                        computeReal,
                        computStandardised,
                        locale);
        var result = indicatorIrrigationOperatingExpenses.computeOperatingExpenses(
                writerContext,
                practicedSystemContext,
                interventionContext,
                indicatorClass,
                labels
        );
        reportFieldsStats(indicatorIrrigationOperatingExpenses.totalFieldCounter, indicatorIrrigationOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computeIrrig(WriterContext writerContext,
                          PerformanceEffectiveDomainExecutionContext domainContext,
                          PerformanceZoneExecutionContext zoneContext,
                          PerformanceEffectiveInterventionExecutionContext interventionContext,
                          Class<? extends GenericIndicator> indicatorClass,
                          String[] labels) {
        IndicatorIrrigationOperatingExpenses indicatorIrrigationOperatingExpenses =
                new IndicatorIrrigationOperatingExpenses(
                        displayed,
                        computeReal,
                        computStandardised,
                        locale);
        var result = indicatorIrrigationOperatingExpenses.computeOperatingExpenses(
                writerContext,
                domainContext,
                zoneContext.getZone(),
                interventionContext,
                indicatorClass,
                labels
        );
        reportFieldsStats(indicatorIrrigationOperatingExpenses.totalFieldCounter, indicatorIrrigationOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computeAllPhyto(PerformanceGlobalExecutionContext globalExecutionContext,
                             WriterContext writerContext,
                             PerformancePracticedSystemExecutionContext practicedSystemContext,
                             PerformancePracticedInterventionExecutionContext interventionContext) {
        Map<PhytoProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByPhytoProductUnit = globalExecutionContext.getConvertersByPhytoProductUnit();
        Collection<RefInputUnitPriceUnitConverter> phytoProductConverters = convertersByPhytoProductUnit != null ?
                convertersByPhytoProductUnit.values().stream().flatMap(Collection::stream).toList() : new ArrayList<>();
        IndicatorPhytoProductOperatingExpenses indicatorPhytoProductOperatingExpenses = new IndicatorPhytoProductOperatingExpenses(
                displayed,
                computeReal,
                computStandardised,
                locale);
        var result = indicatorPhytoProductOperatingExpenses.computeOperatingExpenses(
                writerContext,
                practicedSystemContext,
                interventionContext,
                phytoProductConverters,
                IndicatorOperatingExpenses.class,
                LABELS
        );
        reportFieldsStats(indicatorPhytoProductOperatingExpenses.totalFieldCounter, indicatorPhytoProductOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computeAllPhyto(PerformanceGlobalExecutionContext globalExecutionContext,
                             WriterContext writerContext,
                             PerformanceEffectiveDomainExecutionContext domainContext,
                             PerformanceZoneExecutionContext zoneContext,
                             PerformanceEffectiveInterventionExecutionContext interventionContext) {
        Map<PhytoProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByPhytoProductUnit = globalExecutionContext.getConvertersByPhytoProductUnit();
        Collection<RefInputUnitPriceUnitConverter> phytoProductConverters = convertersByPhytoProductUnit != null ?
                convertersByPhytoProductUnit.values().stream().flatMap(Collection::stream).toList() : new ArrayList<>();
        IndicatorPhytoProductOperatingExpenses indicatorPhytoProductOperatingExpenses = new IndicatorPhytoProductOperatingExpenses(
                displayed,
                computeReal,
                computStandardised,
                locale);
        var result = indicatorPhytoProductOperatingExpenses.computeOperatingExpenses(
                writerContext,
                domainContext,
                zoneContext,
                interventionContext,
                phytoProductConverters,
                IndicatorOperatingExpenses.class,
                LABELS
        );
        reportFieldsStats(indicatorPhytoProductOperatingExpenses.totalFieldCounter, indicatorPhytoProductOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computePesticidePhyto(PerformanceGlobalExecutionContext globalExecutionContext,
                                   WriterContext writerContext,
                                   PerformancePracticedSystemExecutionContext practicedSystemContext,
                                   PerformancePracticedInterventionExecutionContext interventionContext) {
        Map<PhytoProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByPhytoProductUnit = globalExecutionContext.getConvertersByPhytoProductUnit();
        Collection<RefInputUnitPriceUnitConverter> phytoProductConverters = MapUtils.emptyIfNull(convertersByPhytoProductUnit).values().stream()
                .flatMap(Collection::stream).toList();
        IndicatorPhytoProductOperatingExpenses indicatorPhytoProductOperatingExpenses = new IndicatorPhytoProductOperatingExpenses(
                displayed,
                computeReal,
                computStandardised,
                locale);
        var result = indicatorPhytoProductOperatingExpenses.computeOperatingExpensesForPesticide(
                writerContext,
                practicedSystemContext,
                interventionContext,
                phytoProductConverters,
                IndicatorDecomposedOperatingExpenses.class,
                IndicatorDecomposedOperatingExpenses.PHYTO_AVEC_AMM_LABELS
        );
        reportFieldsStats(indicatorPhytoProductOperatingExpenses.totalFieldCounter, indicatorPhytoProductOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computePesticidePhyto(PerformanceGlobalExecutionContext globalExecutionContext,
                                   WriterContext writerContext,
                                   PerformanceEffectiveDomainExecutionContext domainContext,
                                   PerformanceZoneExecutionContext zoneContext,
                                   PerformanceEffectiveInterventionExecutionContext interventionContext) {
        Map<PhytoProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByPhytoProductUnit = globalExecutionContext.getConvertersByPhytoProductUnit();
        Collection<RefInputUnitPriceUnitConverter> phytoProductConverters = MapUtils.emptyIfNull(convertersByPhytoProductUnit).values().stream()
                .flatMap(Collection::stream).toList();
        IndicatorPhytoProductOperatingExpenses indicatorPhytoProductOperatingExpenses = new IndicatorPhytoProductOperatingExpenses(
                displayed,
                computeReal,
                computStandardised,
                locale);
        var result = indicatorPhytoProductOperatingExpenses.computeOperatingExpensesForPesticide(
                writerContext,
                domainContext,
                zoneContext,
                interventionContext,
                phytoProductConverters,
                IndicatorDecomposedOperatingExpenses.class,
                IndicatorDecomposedOperatingExpenses.PHYTO_AVEC_AMM_LABELS
        );
        reportFieldsStats(indicatorPhytoProductOperatingExpenses.totalFieldCounter, indicatorPhytoProductOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computeBiologicalPhyto(PerformanceGlobalExecutionContext globalExecutionContext,
                                    WriterContext writerContext,
                                    PerformancePracticedSystemExecutionContext practicedSystemContext,
                                    PerformancePracticedInterventionExecutionContext interventionContext) {
        Map<PhytoProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByPhytoProductUnit = globalExecutionContext.getConvertersByPhytoProductUnit();
        Collection<RefInputUnitPriceUnitConverter> phytoProductConverters = MapUtils.emptyIfNull(convertersByPhytoProductUnit).values().stream()
                .flatMap(Collection::stream).toList();
        IndicatorPhytoProductOperatingExpenses indicatorPhytoProductOperatingExpenses = new IndicatorPhytoProductOperatingExpenses(
                displayed,
                computeReal,
                computStandardised,
                locale);
        var result = indicatorPhytoProductOperatingExpenses.computeOperatingExpensesForBiological(
                writerContext,
                practicedSystemContext,
                interventionContext,
                phytoProductConverters,
                IndicatorDecomposedOperatingExpenses.class,
                IndicatorDecomposedOperatingExpenses.PHYTO_SANS_AMM_LABELS
        );
        reportFieldsStats(indicatorPhytoProductOperatingExpenses.totalFieldCounter, indicatorPhytoProductOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computeBiologicalPhyto(PerformanceGlobalExecutionContext globalExecutionContext,
                                    WriterContext writerContext,
                                    PerformanceEffectiveDomainExecutionContext domainContext,
                                    PerformanceZoneExecutionContext zoneContext,
                                    PerformanceEffectiveInterventionExecutionContext interventionContext) {
        Map<PhytoProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByPhytoProductUnit = globalExecutionContext.getConvertersByPhytoProductUnit();
        Collection<RefInputUnitPriceUnitConverter> phytoProductConverters = MapUtils.emptyIfNull(convertersByPhytoProductUnit).values().stream()
                .flatMap(Collection::stream).toList();
        IndicatorPhytoProductOperatingExpenses indicatorPhytoProductOperatingExpenses = new IndicatorPhytoProductOperatingExpenses(
                displayed,
                computeReal,
                computStandardised,
                locale);
        var result = indicatorPhytoProductOperatingExpenses.computeOperatingExpensesForBiological(
                writerContext,
                domainContext,
                zoneContext,
                interventionContext,
                phytoProductConverters,
                IndicatorDecomposedOperatingExpenses.class,
                IndicatorDecomposedOperatingExpenses.PHYTO_SANS_AMM_LABELS
        );
        reportFieldsStats(indicatorPhytoProductOperatingExpenses.totalFieldCounter, indicatorPhytoProductOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computeSubstrate(PerformanceGlobalExecutionContext globalExecutionContext,
                              WriterContext writerContext,
                              PerformancePracticedSystemExecutionContext practicedSystemContext,
                              PerformancePracticedInterventionExecutionContext interventionContext,
                              Class<? extends GenericIndicator> indicatorClass,
                              String[] labels) {

        Map<SubstrateInputUnit, List<RefInputUnitPriceUnitConverter>> convertersBySubstrateUnit = globalExecutionContext.getConvertersBySubstrateUnit();
        Collection<RefInputUnitPriceUnitConverter> substrateProductConverters = MapUtils.emptyIfNull(convertersBySubstrateUnit).values().stream()
                .flatMap(Collection::stream).toList();

        IndicatorSubstrateInputOperatingExpenses indicatorSubstrateInputOperatingExpenses =
                new IndicatorSubstrateInputOperatingExpenses(
                        displayed,
                        computeReal,
                        computStandardised,
                        locale
                );
        var result = indicatorSubstrateInputOperatingExpenses.computeOperatingExpenses(
                writerContext,
                practicedSystemContext,
                interventionContext,
                substrateProductConverters,
                indicatorClass,
                labels
        );
        reportFieldsStats(indicatorSubstrateInputOperatingExpenses.totalFieldCounter, indicatorSubstrateInputOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computeSubstrate(PerformanceGlobalExecutionContext globalExecutionContext,
                              WriterContext writerContext,
                              PerformanceEffectiveDomainExecutionContext domainContext,
                              PerformanceZoneExecutionContext zoneContext,
                              PerformanceEffectiveInterventionExecutionContext interventionContext,
                              Class<? extends GenericIndicator> indicatorClass,
                              String[] labels) {
        Map<SubstrateInputUnit, List<RefInputUnitPriceUnitConverter>> convertersBySubstrateUnit = globalExecutionContext.getConvertersBySubstrateUnit();
        Collection<RefInputUnitPriceUnitConverter> substrateProductConverters = MapUtils.emptyIfNull(convertersBySubstrateUnit).values().stream()
                .flatMap(Collection::stream).toList();
        IndicatorSubstrateInputOperatingExpenses indicatorSubstrateInputOperatingExpenses =
                new IndicatorSubstrateInputOperatingExpenses(
                        displayed,
                        computeReal,
                        computStandardised,
                        locale
                );
        var result = indicatorSubstrateInputOperatingExpenses.computeOperatingExpenses(
                writerContext,
                domainContext,
                zoneContext.getZone(),
                interventionContext,
                substrateProductConverters,
                indicatorClass,
                labels
        );
        reportFieldsStats(indicatorSubstrateInputOperatingExpenses.totalFieldCounter, indicatorSubstrateInputOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computePot(PerformanceGlobalExecutionContext globalExecutionContext,
                        WriterContext writerContext,
                        PerformancePracticedSystemExecutionContext practicedSystemContext,
                        PerformancePracticedInterventionExecutionContext interventionContext,
                        Class<? extends GenericIndicator> indicatorClass,
                        String[] labels) {
        Map<PotInputUnit, List<RefInputUnitPriceUnitConverter>> convertersByPotInputUnit = globalExecutionContext.getConvertersByPotUnit();
        Collection<RefInputUnitPriceUnitConverter> converters = MapUtils.emptyIfNull(convertersByPotInputUnit).values().stream()
                .flatMap(Collection::stream).toList();
        IndicatorPotInputOperatingExpenses indicatorPotInputOperatingExpenses =
                new IndicatorPotInputOperatingExpenses(
                        displayed,
                        computeReal,
                        computStandardised,
                        locale
                );
        var result = indicatorPotInputOperatingExpenses.computeOperatingExpenses(
                writerContext,
                practicedSystemContext,
                interventionContext,
                converters,
                indicatorClass,
                labels
        );
        reportFieldsStats(indicatorPotInputOperatingExpenses.totalFieldCounter, indicatorPotInputOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computePot(PerformanceGlobalExecutionContext globalExecutionContext,
                        WriterContext writerContext,
                        PerformanceEffectiveDomainExecutionContext domainContext,
                        PerformanceZoneExecutionContext zoneContext,
                        PerformanceEffectiveInterventionExecutionContext interventionContext,
                        Class<? extends GenericIndicator> indicatorClass,
                        String[] labels) {
        Map<PotInputUnit, List<RefInputUnitPriceUnitConverter>> convertersByPotInputUnit = globalExecutionContext.getConvertersByPotUnit();
        Collection<RefInputUnitPriceUnitConverter> converters = MapUtils.emptyIfNull(convertersByPotInputUnit).values().stream()
                .flatMap(Collection::stream).toList();
        IndicatorPotInputOperatingExpenses indicatorPotInputOperatingExpenses =
                new IndicatorPotInputOperatingExpenses(
                        displayed,
                        computeReal,
                        computStandardised,
                        locale
                );
        var result = indicatorPotInputOperatingExpenses.computeOperatingExpenses(
                writerContext,
                domainContext,
                zoneContext.getZone(),
                interventionContext,
                converters,
                indicatorClass,
                labels
        );
        reportFieldsStats(indicatorPotInputOperatingExpenses.totalFieldCounter, indicatorPotInputOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computeOther(PerformanceGlobalExecutionContext globalExecutionContext,
                          WriterContext writerContext,
                          PerformancePracticedSystemExecutionContext practicedSystemContext,
                          PerformancePracticedInterventionExecutionContext interventionContext,
                          Class<? extends GenericIndicator> indicatorClass,
                          String[] labels) {
        Map<OtherProductInputUnit, List<RefInputUnitPriceUnitConverter>> convertersByOtherProductUnit = globalExecutionContext.getConvertersByOtherProductUnit();
        Collection<RefInputUnitPriceUnitConverter> otherProductConverters = MapUtils.emptyIfNull(convertersByOtherProductUnit).values().stream()
                .flatMap(Collection::stream).toList();
        IndicatorOtherProductInputOperatingExpenses indicatorOtherProductInputOperatingExpenses =
                new IndicatorOtherProductInputOperatingExpenses(
                        displayed,
                        computeReal,
                        computStandardised,
                        locale
                );
        var result = indicatorOtherProductInputOperatingExpenses.computeOperatingExpenses(
                writerContext,
                practicedSystemContext,
                interventionContext,
                otherProductConverters,
                indicatorClass,
                labels
        );
        reportFieldsStats(indicatorOtherProductInputOperatingExpenses.totalFieldCounter, indicatorOtherProductInputOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    Double[] computeOther(PerformanceGlobalExecutionContext globalExecutionContext,
                          WriterContext writerContext,
                          PerformanceEffectiveDomainExecutionContext domainContext,
                          PerformanceZoneExecutionContext zoneContext,
                          PerformanceEffectiveInterventionExecutionContext interventionContext,
                          Class<? extends GenericIndicator> indicatorClass,
                          String[] labels) {
        Map<OtherProductInputUnit, List<RefInputUnitPriceUnitConverter>> convertersByOtherProductUnit = globalExecutionContext.getConvertersByOtherProductUnit();
        Collection<RefInputUnitPriceUnitConverter> otherProductConverters = MapUtils.emptyIfNull(convertersByOtherProductUnit).values().stream()
                .flatMap(Collection::stream).toList();
        IndicatorOtherProductInputOperatingExpenses indicatorOtherProductInputOperatingExpenses =
                new IndicatorOtherProductInputOperatingExpenses(
                        displayed,
                        computeReal,
                        computStandardised,
                        locale
                );
        var result = indicatorOtherProductInputOperatingExpenses.computeOperatingExpenses(
                writerContext,
                domainContext,
                zoneContext.getZone(),
                interventionContext,
                otherProductConverters,
                indicatorClass,
                labels
        );
        reportFieldsStats(indicatorOtherProductInputOperatingExpenses.totalFieldCounter, indicatorOtherProductInputOperatingExpenses.targetedErrorFieldMessages);
        return result;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {

        if (interventionContext.isFictive()) return newArray(LABELS.length, 0d);

        // phyto
        Double[] realAndStandardised = computeAllPhyto(globalExecutionContext, writerContext, practicedSystemContext, interventionContext);

        // irrig
        realAndStandardised = sum(realAndStandardised, computeIrrig(writerContext, practicedSystemContext, interventionContext, IndicatorOperatingExpenses.class, LABELS));

        // seeding
        var seedingResults = computeSeedingForCropAndProduct(globalExecutionContext, writerContext, practicedSystemContext, interventionContext);
        realAndStandardised = sum(realAndStandardised, new Double[]{seedingResults.getLeft(), seedingResults.getRight()});

        // mineral
        realAndStandardised = sum(realAndStandardised, computeMineral(globalExecutionContext, writerContext, practicedSystemContext, interventionContext, IndicatorOperatingExpenses.class, LABELS));

        // organic
        realAndStandardised = sum(realAndStandardised, computeOrganic(globalExecutionContext, writerContext, practicedSystemContext, interventionContext, IndicatorOperatingExpenses.class, LABELS));

        // other
        realAndStandardised = sum(realAndStandardised, computeOther(globalExecutionContext, writerContext, practicedSystemContext, interventionContext, IndicatorOperatingExpenses.class, LABELS));

        // substrate
        realAndStandardised = sum(realAndStandardised, computeSubstrate(globalExecutionContext, writerContext, practicedSystemContext, interventionContext, IndicatorOperatingExpenses.class, LABELS));

        // pot
        realAndStandardised = sum(realAndStandardised, computePot(globalExecutionContext, writerContext, practicedSystemContext, interventionContext, IndicatorOperatingExpenses.class, LABELS));

        interventionContext.addExpenses(realAndStandardised[0]);
        interventionContext.addStandardisedExpenses(realAndStandardised[1]);

        return realAndStandardised;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {

        // phyto
        Double[] realAndStandardised = computeAllPhyto(globalExecutionContext, writerContext, domainContext, zoneContext, interventionContext);

        // irrig
        realAndStandardised = sum(realAndStandardised, computeIrrig(writerContext, domainContext, zoneContext, interventionContext, IndicatorOperatingExpenses.class, LABELS));

        // seeding
        var seedingResults = computeSeedingForCropAndProduct(globalExecutionContext, writerContext, domainContext, zoneContext, interventionContext);
        realAndStandardised = sum(realAndStandardised, new Double[]{seedingResults.getLeft(), seedingResults.getRight()});

        // mineral
        realAndStandardised = sum(realAndStandardised, computeMineral(globalExecutionContext, writerContext, domainContext, zoneContext, interventionContext, IndicatorOperatingExpenses.class, LABELS));

        // organic
        realAndStandardised = sum(realAndStandardised, computeOrganic(globalExecutionContext, writerContext, domainContext, zoneContext, interventionContext, IndicatorOperatingExpenses.class, LABELS));

        // other
        realAndStandardised = sum(realAndStandardised, computeOther(globalExecutionContext, writerContext, domainContext, zoneContext, interventionContext, IndicatorOperatingExpenses.class, LABELS));

        // substrate
        realAndStandardised = sum(realAndStandardised, computeSubstrate(globalExecutionContext, writerContext, domainContext, zoneContext, interventionContext, IndicatorOperatingExpenses.class, LABELS));

        // pot
        realAndStandardised = sum(realAndStandardised, computePot(globalExecutionContext, writerContext, domainContext, zoneContext, interventionContext, IndicatorOperatingExpenses.class, LABELS));

        interventionContext.addExpenses(realAndStandardised[0]);
        interventionContext.addStandardisedExpenses(realAndStandardised[1]);

        return realAndStandardised;
    }

    protected void reportFieldsStats(
            Map<String, AtomicInteger> totalFieldCounter,
            Map<String, Set<MissingFieldMessage>> comments) {
        for (Map.Entry<String, AtomicInteger> fieldsCounter : totalFieldCounter.entrySet()) {
            final String interventionId = fieldsCounter.getKey();
            AtomicInteger counter = fieldsCounter.getValue();

            for (int i = 0; i < counter.get(); i++) {
                incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            }
        }
        for (Map.Entry<String, Set<MissingFieldMessage>> missingInterventionFields : comments.entrySet()) {
            final String interventionId = missingInterventionFields.getKey();
            Set<MissingFieldMessage> messages = comments.get(interventionId);
            for (MissingFieldMessage message : messages) {
                addMissingFieldMessage(interventionId, message);
            }
        }
    }

    protected static double computeCi(Double da, Double price, PriceUnit pu, double conversionRate) {
        double inputsCharges;
        //L’utilisateur peut saisir un prix en €/ha pour les intrants. Dans ce cas-là, on considère que la dose
        //d’application de l’intrant vaut 1.
        if (PricesService.DEFAULT_PRICE_UNIT == pu) {
            price = price == null ? DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft() : price;
            inputsCharges = price; // = 1d * price
        } else {
            price = price == null ? DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft() : price;
            inputsCharges = da != null ? da * price * conversionRate : 0.0d;
        }
        return inputsCharges;
    }

    public void init(IndicatorFilter operatingExpensesIndicatorFilter) {
        displayed = operatingExpensesIndicatorFilter != null;
        computeReal = displayed && operatingExpensesIndicatorFilter.getComputeReal();
        computStandardised = displayed && operatingExpensesIndicatorFilter.getComputeStandardized();
    }
}
