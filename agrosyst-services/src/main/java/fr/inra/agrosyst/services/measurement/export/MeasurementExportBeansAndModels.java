package fr.inra.agrosyst.services.measurement.export;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.VariableType;
import fr.inra.agrosyst.api.entities.measure.HorizonType;
import fr.inra.agrosyst.api.entities.measure.MeasureType;
import fr.inra.agrosyst.api.entities.measure.MeasurementType;
import fr.inra.agrosyst.api.entities.measure.NitrogenMolecule;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActiveTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMesure;
import fr.inra.agrosyst.api.entities.referential.RefMesureTopiaDao;
import fr.inra.agrosyst.services.common.export.ExportModel;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

public class MeasurementExportBeansAndModels {

    @Getter
    public static class CommonBean {

        final String zoneName;
        final String plotName;
        final String growingSystemName;
        final String growingPlanName;
        final String domainName;
        final int campaign;
        final LocalDate startDate;
        final LocalDate endDate;

        /**
         * Constructeur avec tous les champs pour la première instanciation
         */
        public CommonBean(String zoneName,
                          String plotName,
                          String growingSystemName,
                          String growingPlanName,
                          String domainName,
                          int campaign,
                          LocalDate startDate,
                          LocalDate endDate) {
            this.zoneName = zoneName;
            this.plotName = plotName;
            this.growingSystemName = growingSystemName;
            this.growingPlanName = growingPlanName;
            this.domainName = domainName;
            this.campaign = campaign;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        /**
         * Constructeur par recopie pour faciliter le travail des sous modèles
         */
        public CommonBean(CommonBean source) {
            this(
                    source.zoneName,
                    source.plotName,
                    source.growingSystemName,
                    source.growingPlanName,
                    source.domainName,
                    source.campaign,
                    source.startDate,
                    source.endDate
            );
        }
    }

    public abstract static class CommonModel<T extends CommonBean> extends ExportModel<T> {
        public CommonModel() {
            newColumn( "Zone", CommonBean::getZoneName);
            newColumn( "Parcelle", CommonBean::getPlotName);
            newColumn( "Système De Culture", CommonBean::getGrowingSystemName);
            newColumn( "Dispositif", CommonBean::getGrowingPlanName);
            newColumn( "Domaine", CommonBean::getDomainName);
            newColumn( "Campagne", CommonBean::getCampaign);
            newColumn( "Date de début", CommonBean::getStartDate);
            newColumn( "Date de fin", CommonBean::getEndDate);
        }
    }

    @Getter
    @Setter
    public static class MeasurementBean extends CommonBean {

        Integer repetitionNumber;
        MeasurementType measurementType;
        String variable;
        String value;

        public MeasurementBean(CommonBean source) {
            super(source);
        }
    }

    public static class MeasurementModel extends CommonModel<MeasurementBean> {
        @Override
        public String getTitle() {
            return "Synthèse";
        }

        public MeasurementModel() {
            super();
            newColumn("Numéro de répétition", MeasurementBean::getRepetitionNumber);
            newColumn("Type d’observation", MeasurementType.class, MeasurementBean::getMeasurementType);
            newColumn("Variable", MeasurementBean::getVariable);
            newColumn("Valeur", MeasurementBean::getValue);
        }
    }

    @Getter
    @Setter
    public static class StadeBean extends CommonBean {

        MeasurementType measurementType;
        String libelle_espece_botanique;
        String libelle_qualifiant__aee;
        String libelle_type_saisonnier__aee;
        String libelle_destination__aee;
        String measuring_protocol;
        Integer repetition_number;
        Integer crop_number_observed;
        String crop_stage_min;
        String crop_stage_average;
        String crop_stage_medium;
        String crop_stage_max;
        String comment;

        public StadeBean(CommonBean source) {
            super(source);
        }
    }

    public static class StadeModel extends CommonModel<StadeBean> {
        @Override
        public String getTitle() {
            return "Stade de culture";
        }

        public StadeModel() {
            super();
            newColumn("Type d’observation", MeasurementType.class, StadeBean::getMeasurementType);
            newColumn("Espèce", StadeBean::getLibelle_espece_botanique);
            newColumn("Qualifiant", StadeBean::getLibelle_qualifiant__aee);
            newColumn("Type saisonnier", StadeBean::getLibelle_type_saisonnier__aee);
            newColumn("Destination", StadeBean::getLibelle_destination__aee);
            newColumn("Protocole de mesure", StadeBean::getMeasuring_protocol);
            newColumn("Numéro de répétition", StadeBean::getRepetition_number);
            newColumn("Nombre de plantes observées", StadeBean::getCrop_number_observed);
            newColumn("Stade mini", StadeBean::getCrop_stage_min);
            newColumn("Stade moyen", StadeBean::getCrop_stage_average);
            newColumn("Stade median", StadeBean::getCrop_stage_medium);
            newColumn("Stade maxi", StadeBean::getCrop_stage_max);
            newColumn("Commentaire", StadeBean::getComment);
        }
    }

    @Getter
    @Setter
    public static class AdventiceBean extends CommonBean {

        MeasurementType measurementType;
        String measuring_protocol;
        Integer repetition_number;
        String comment;
        String measured_adventice;
        String adventice_stage;
        Double adventice_min;
        Double adventice_average;
        Double adventice_max;
        Double adventice_median;

        public AdventiceBean(CommonBean source) {
            super(source);
        }
    }

    public static class AdventiceModel extends CommonModel<AdventiceBean> {
        @Override
        public String getTitle() {
            return "Adventices";
        }

        public AdventiceModel() {
            super();
            newColumn("Type d’observation", MeasurementType.class, AdventiceBean::getMeasurementType);
            newColumn("Protocole de mesure", AdventiceBean::getMeasuring_protocol);
            newColumn("Numéro de répétition", AdventiceBean::getRepetition_number);
            newColumn("Adventice mesurée", AdventiceBean::getMeasured_adventice);
            newColumn("Stade adventice", AdventiceBean::getAdventice_stage);
            newColumn("Nombre mini/m²", AdventiceBean::getAdventice_min);
            newColumn("Nombre moyen/m²", AdventiceBean::getAdventice_average);
            newColumn("Nombre maxi/m²", AdventiceBean::getAdventice_max);
            newColumn("Nombre median/m²", AdventiceBean::getAdventice_median);
            newColumn("Commentaire", AdventiceBean::getComment);
        }
    }

    @Getter
    @Setter
    public static class NuisibleMaladiesAuxiliairesBean extends CommonBean {

        MeasurementType measurementType;
        String measuring_protocol;
        Integer repetition_number;
        String comment;
        String protocol;
        Integer crop_number_observed;
        String pest;
        String crop_organism_stage;
        String crop_organ_support;
        String crop_notation_type;
        String crop_qualititive_value;
        Double quantitative_value;
        String unit_edi;
        String crop_unit_qualifier;
        String unit_other;
        String other_qualifier;
        public NuisibleMaladiesAuxiliairesBean(CommonBean source) {
            super(source);
        }
    }

    public static class NuisibleMaladiesAuxiliairesModel extends CommonModel<NuisibleMaladiesAuxiliairesBean> {
        @Override
        public String getTitle() {
            return "Nuisibles-Maladies-Auxiliaires";
        }

        public NuisibleMaladiesAuxiliairesModel() {
            super();
            newColumn("Type d’observation", MeasurementType.class, NuisibleMaladiesAuxiliairesBean::getMeasurementType);
            newColumn("Protocole VgObs", NuisibleMaladiesAuxiliairesBean::getProtocol);
            newColumn("Autre protocole", NuisibleMaladiesAuxiliairesBean::getMeasuring_protocol);
            newColumn("Numéro de répétition", NuisibleMaladiesAuxiliairesBean::getRepetition_number);
            newColumn("Nombre de plantes observées", NuisibleMaladiesAuxiliairesBean::getCrop_number_observed);
            newColumn("Nuisible", NuisibleMaladiesAuxiliairesBean::getPest);
            newColumn("Stade organisme observé", NuisibleMaladiesAuxiliairesBean::getCrop_organism_stage);
            newColumn("Support organe observé", NuisibleMaladiesAuxiliairesBean::getCrop_organ_support);
            newColumn("Type de notation", NuisibleMaladiesAuxiliairesBean::getCrop_notation_type);
            newColumn("Valeur qualitative", NuisibleMaladiesAuxiliairesBean::getCrop_qualititive_value);
            newColumn("Valeur quantitative", NuisibleMaladiesAuxiliairesBean::getQuantitative_value);
            newColumn("Unite EDI", NuisibleMaladiesAuxiliairesBean::getUnit_edi);
            newColumn("Qualifiant EDI", NuisibleMaladiesAuxiliairesBean::getCrop_unit_qualifier);
            newColumn("Unite autre", NuisibleMaladiesAuxiliairesBean::getUnit_other);
            newColumn("Qualifiant autre", NuisibleMaladiesAuxiliairesBean::getOther_qualifier);
            newColumn("Commentaire", NuisibleMaladiesAuxiliairesBean::getComment);
        }
    }

    @Getter
    @Setter
    public static class MeasureBean extends CommonBean {

        MeasurementType measurementType;
        String measuringProtocol;
        Integer repetitionNumber;
        String comment;
        String variable_mesuree;
        MeasureType measure_type;
        String measure_value;
        String measure_unit;

        String cropping_plan_species;
        String effective_or_area_taken;
        VariableType type_variable_mesuree;
        HorizonType horizonType;
        String sampling;
        String activeSubstance;
        NitrogenMolecule nitrogenMolecule;

        public MeasureBean(CommonBean source) {
            super(source);
        }
    }

    public static class PlanteModel extends CommonModel<MeasureBean> {
        @Override
        public String getTitle() {
            return "Plantes";
        }

        public PlanteModel(RefMesureTopiaDao refMesureTopiaDao) {
            super();
            newColumn("Catégorie de mesure", MeasurementType.class, MeasureBean::getMeasurementType);
            newColumn("Espèce", MeasureBean::getCropping_plan_species);
            newColumn("Protocole de mesure", MeasureBean::getMeasuringProtocol);
            newColumn("Numéro de répétition", MeasureBean::getRepetitionNumber);
            newColumn("Effectif ou surface prélevée", MeasureBean::getEffective_or_area_taken);
            newColumn("Type de variable mesurée", MeasureBean::getType_variable_mesuree);
            Iterable<String> variablesMesureesValues = refMesureTopiaDao.findPropertyDistinctValues(RefMesure.PROPERTY_VARIABLE_MESUREE);
            newColumn("Variable mesurée", variablesMesureesValues, MeasureBean::getVariable_mesuree);
            newColumn("Type de mesure", MeasureType.class, MeasureBean::getMeasure_type);
            newColumn("Valeur de mesure", MeasureBean::getMeasure_value);
            newColumn("Unité de mesure", MeasureBean::getMeasure_unit);
            newColumn("Commentaire", MeasureBean::getComment);
        }
    }

    public static class SolModel extends CommonModel<MeasureBean> {
        @Override
        public String getTitle() {
            return "Sol";
        }

        public SolModel(RefMesureTopiaDao refMesureTopiaDao) {
            super();
            newColumn("Catégorie de mesure", MeasurementType.class, MeasureBean::getMeasurementType);
            newColumn("Protocole de mesure", MeasureBean::getMeasuringProtocol);
            newColumn("Horizon mesuré", MeasureBean::getHorizonType);
            newColumn("Numéro de répétition", MeasureBean::getRepetitionNumber);
            newColumn("Échantillonnage", MeasureBean::getSampling);
            newColumn("Type de variable mesurée", MeasureBean::getType_variable_mesuree);
            Iterable<String> variablesMesureesValues = refMesureTopiaDao.findPropertyDistinctValues(RefMesure.PROPERTY_VARIABLE_MESUREE);
            newColumn("Variable mesurée", variablesMesureesValues, MeasureBean::getVariable_mesuree);
            newColumn("Type de mesure", MeasureType.class, MeasureBean::getMeasure_type);
            newColumn("Valeur de mesure", MeasureBean::getMeasure_value);
            newColumn("Unité de mesure", MeasureBean::getMeasure_unit);
            newColumn("Commentaire", MeasureBean::getComment);
        }
    }

    public static class SoluteModel extends CommonModel<MeasureBean> {
        @Override
        public String getTitle() {
            return "Transfert de solutés";
        }

        public SoluteModel(RefMesureTopiaDao refMesureTopiaDao, RefActaSubstanceActiveTopiaDao refActaSubstanceActiveTopiaDao) {
            super();
            newColumn("Catégorie de mesure", MeasurementType.class, MeasureBean::getMeasurementType);
            newColumn("Protocole de mesure", MeasureBean::getMeasuringProtocol);
            newColumn("Numéro de répétition", MeasureBean::getRepetitionNumber);
            newColumn("Échantillonnage", MeasureBean::getSampling);
            newColumn("Horizon mesuré", MeasureBean::getHorizonType);
            Iterable<String> variablesMesureesValues = refMesureTopiaDao.findPropertyDistinctValues(RefMesure.PROPERTY_VARIABLE_MESUREE);
            newColumn("Variable mesurée", variablesMesureesValues, MeasureBean::getVariable_mesuree);
            Iterable<String> activeSubstanceValues = refActaSubstanceActiveTopiaDao.findPropertyDistinctValues(RefActaSubstanceActive.PROPERTY_NOM_PRODUIT);
            newColumn("Molécule phytosanitaire mesurée", activeSubstanceValues, MeasureBean::getActiveSubstance);
            newColumn("Molécule azotée mesurée", NitrogenMolecule.class, MeasureBean::getNitrogenMolecule);
            newColumn("Type de mesure", MeasureType.class, MeasureBean::getMeasure_type);
            newColumn("Valeur de mesure", MeasureBean::getMeasure_value);
            newColumn("Unité de mesure", MeasureBean::getMeasure_unit);
            newColumn("Commentaire", MeasureBean::getComment);
        }
    }

    public static class GesModel extends CommonModel<MeasureBean> {
        @Override
        public String getTitle() {
            return "Gaz à effet de serre";
        }

        public GesModel(RefMesureTopiaDao refMesureTopiaDao) {
            super();
            newColumn("Catégorie de mesure", MeasurementType.class, MeasureBean::getMeasurementType);
            newColumn("Protocole de mesure", MeasureBean::getMeasuringProtocol);
            newColumn("Numéro de répétition", MeasureBean::getRepetitionNumber);
            newColumn("Échantillonnage", MeasureBean::getSampling);
            Iterable<String> variablesMesureesValues = refMesureTopiaDao.findPropertyDistinctValues(RefMesure.PROPERTY_VARIABLE_MESUREE);
            newColumn("Variable mesurée", variablesMesureesValues, MeasureBean::getVariable_mesuree);
            newColumn("Type de mesure", MeasureType.class, MeasureBean::getMeasure_type);
            newColumn("Valeur de mesure", MeasureBean::getMeasure_value);
            newColumn("Unité de mesure", MeasureBean::getMeasure_unit);
            newColumn("Commentaire", MeasureBean::getComment);
        }
    }

    public static class MeteoModel extends CommonModel<MeasureBean> {
        @Override
        public String getTitle() {
            return "Météo";
        }

        public MeteoModel(RefMesureTopiaDao refMesureTopiaDao) {
            super();
            newColumn("Catégorie de mesure", MeasurementType.class, MeasureBean::getMeasurementType);
            newColumn("Protocole de mesure", MeasureBean::getMeasuringProtocol);
            newColumn("Numéro de répétition", MeasureBean::getRepetitionNumber);
            newColumn("Échantillonnage", MeasureBean::getSampling);
            Iterable<String> variablesMesureesValues = refMesureTopiaDao.findPropertyDistinctValues(RefMesure.PROPERTY_VARIABLE_MESUREE);
            newColumn("Variable mesurée", variablesMesureesValues, MeasureBean::getVariable_mesuree);
            newColumn("Type de mesure", MeasureType.class, MeasureBean::getMeasure_type);
            newColumn("Valeur de mesure", MeasureBean::getMeasure_value);
            newColumn("Unité de mesure", MeasureBean::getMeasure_unit);
            newColumn("Commentaire", MeasureBean::getComment);
        }
    }

}
