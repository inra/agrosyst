package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import org.apache.commons.collections4.MultiValuedMap;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

import static org.nuiton.i18n.I18n.l;

/**
 * Classe calculant l'indicateur HRI-1.
 * <p>
 * Cet indicateur est globalement le même que l'indicateur de quantité de substance active totale, mais
 * il faut ajouter au calcul la prise en compte du coefficient de pondération (que l'on trouve dans le référentiel
 * RefSubstancesActivesCommissionEuropeenne).
 * <p>
 * Donc cette classe surcharge simplement le calcul du coefficient utilisé lors du calcul, tout le reste
 * est identique.
 */
public class IndicatorHRI1 extends IndicatorTotalActiveSubstanceAmount {

    public static final String COLUMN_NAME = "hri1_tot";
    public static final String COLUMN_NAME_HTS = "hri1_hts";


    protected final String[] indicators = new String[]{
            "Indicator.label.HRI1",        // HRI-1
            "Indicator.label.HRI1_hts",    // HRI-1 hors traitement de semence
    };

    /**
     * Si on ne trouve pas de coefficient, on applique un coef de 8 par défaut. Ce coefficient peut être amené à changer.
     */
    public static final double DEFAULT_COEF_PONDERATION = 8.0d;

    public IndicatorHRI1() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, COLUMN_NAME + "_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, COLUMN_NAME + "_detail_champs_non_renseig");
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME_HTS);
        return indicatorNameToColumnName;
    }

    /**
     * Pour cet indicateur, on garde le coefficient pour passer de l'unité du référentiel de composition de substances
     * actives vers kg/ha, et on ajoute le coefficient de pondération du référentiel RefSubstancesActivesCommissionEuropeenne.
     */
    @Override
    protected double getCoef(@NotNull RefCompositionSubstancesActivesParNumeroAMM activeSubstance, PerformanceInterventionContext interventionContext) {

        MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode = interventionContext.getAllSubstancesActivesCommissionEuropeenneByAmmCode();
        Double coef = allSubstancesActivesCommissionEuropeenneByAmmCode.values()
                .stream().filter(ref -> ref.getId_sa().contentEquals(activeSubstance.getId_sa()))
                .map(RefSubstancesActivesCommissionEuropeenne::getCoef_ponderation)
                .findFirst()
                .orElse(DEFAULT_COEF_PONDERATION);

        return super.getCoef(activeSubstance, interventionContext) * coef;
    }

    protected double merge(Double previousValue, Double newValue) {
        double result = 0;
        if (previousValue != null) {
            result += previousValue;
        }
        if (newValue != null) {
            result += newValue;
        }
        return result;
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, indicators[i]);
    }

}
