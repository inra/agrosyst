package fr.inra.agrosyst.services.edaplos.annotations;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.utils.DataValidator;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.ConstraintValidator;
import org.apache.commons.lang3.StringUtils;


public class ValidSiretValidator implements ConstraintValidator<ValidSiret, String> {

    @Override
    public void initialize(ValidSiret validSiret) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        boolean valid = true;

        if (StringUtils.isNotBlank(s)) {
            valid = DataValidator.isEdaplosSiretValid(s);
        }

        return valid;
    }
}
