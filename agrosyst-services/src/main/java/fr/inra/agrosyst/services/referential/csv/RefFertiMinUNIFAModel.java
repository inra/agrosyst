package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFAImpl;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRoot;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * @author David Cossé
 */
public class RefFertiMinUNIFAModel extends AbstractAgrosystModel<RefFertiMinUNIFA> implements ExportModel<RefFertiMinUNIFA> {

    public RefFertiMinUNIFAModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("CATEG", RefFertiMinUNIFA.PROPERTY_CATEG, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("Type_produit", RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT);
        newMandatoryColumn("CODEPROD", RefFertiMinUNIFA.PROPERTY_CODEPROD);
        newMandatoryColumn("FORME", RefFertiMinUNIFA.PROPERTY_FORME);
        newMandatoryColumn("N (% poids)", RefFertiMinUNIFA.PROPERTY_N, DOUBLE_PARSER);
        newMandatoryColumn("P2O5 (% poids)", RefFertiMinUNIFA.PROPERTY_P2_O5, DOUBLE_PARSER);
        newMandatoryColumn("K2O (% poids)", RefFertiMinUNIFA.PROPERTY_K2_O, DOUBLE_PARSER);
        newMandatoryColumn("Bore (% poids)", RefFertiMinUNIFA.PROPERTY_BORE, DOUBLE_PARSER);
        newMandatoryColumn("Calcium (% poids)", RefFertiMinUNIFA.PROPERTY_CALCIUM, DOUBLE_PARSER);
        newMandatoryColumn("Fer (% poids)", RefFertiMinUNIFA.PROPERTY_FER, DOUBLE_PARSER);
        newMandatoryColumn("Manganèse (% poids)", RefFertiMinUNIFA.PROPERTY_MANGANESE, DOUBLE_PARSER);
        newMandatoryColumn("Molybdène (% poids)", RefFertiMinUNIFA.PROPERTY_MOLYBDENE, DOUBLE_PARSER);
        newMandatoryColumn("MgO (% poids)", RefFertiMinUNIFA.PROPERTY_MG_O, DOUBLE_PARSER);
        newMandatoryColumn("Oxyde de Sodium (% poids)", RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, DOUBLE_PARSER);
        newMandatoryColumn("SO3 (% poids)", RefFertiMinUNIFA.PROPERTY_S_O3, DOUBLE_PARSER);
        newMandatoryColumn("Cuivre (% poids)", RefFertiMinUNIFA.PROPERTY_CUIVRE, DOUBLE_PARSER);
        newMandatoryColumn("Zinc (% poids)", RefFertiMinUNIFA.PROPERTY_ZINC, DOUBLE_PARSER);
        newMandatoryColumn("source", RefFertiMinUNIFA.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefActaDosageSaRoot.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefFertiMinUNIFA, Object>> getColumnsForExport() {
        ModelBuilder<RefFertiMinUNIFA> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("CATEG", RefFertiMinUNIFA.PROPERTY_CATEG, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Type_produit", RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT);
        modelBuilder.newColumnForExport("CODEPROD", RefFertiMinUNIFA.PROPERTY_CODEPROD);
        modelBuilder.newColumnForExport("FORME", RefFertiMinUNIFA.PROPERTY_FORME);
        modelBuilder.newColumnForExport("N (% poids)", RefFertiMinUNIFA.PROPERTY_N, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("P2O5 (% poids)", RefFertiMinUNIFA.PROPERTY_P2_O5, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("K2O (% poids)", RefFertiMinUNIFA.PROPERTY_K2_O, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Bore (% poids)", RefFertiMinUNIFA.PROPERTY_BORE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Calcium (% poids)", RefFertiMinUNIFA.PROPERTY_CALCIUM, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Fer (% poids)", RefFertiMinUNIFA.PROPERTY_FER, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Manganèse (% poids)", RefFertiMinUNIFA.PROPERTY_MANGANESE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Molybdène (% poids)", RefFertiMinUNIFA.PROPERTY_MOLYBDENE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("MgO (% poids)", RefFertiMinUNIFA.PROPERTY_MG_O, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Oxyde de Sodium (% poids)", RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("SO3 (% poids)", RefFertiMinUNIFA.PROPERTY_S_O3, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Cuivre (% poids)", RefFertiMinUNIFA.PROPERTY_CUIVRE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Zinc (% poids)", RefFertiMinUNIFA.PROPERTY_ZINC, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("source", RefFertiMinUNIFA.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefFertiMinUNIFA.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefFertiMinUNIFA newEmptyInstance() {
        RefFertiMinUNIFA refFertiMinUNIFA = new RefFertiMinUNIFAImpl();
        refFertiMinUNIFA.setActive(true);
        return refFertiMinUNIFA;
    }
}
