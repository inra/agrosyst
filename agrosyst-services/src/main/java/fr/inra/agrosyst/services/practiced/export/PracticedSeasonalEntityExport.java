package fr.inra.agrosyst.services.practiced.export;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

public class PracticedSeasonalEntityExport {

    protected String label;

    protected boolean endCycle;

    protected boolean sameCampaignAsPreviousNode;

    protected Double initNodeFrequency;

    protected int x;

    protected final List<String> previousCrops = new ArrayList<>();

    protected Double croppingPlanEntryFrequency;

    protected String intermediateCropName;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean getEndCycle() {
        return endCycle;
    }

    public void setEndCycle(boolean endCycle) {
        this.endCycle = endCycle;
    }

    public boolean getSameCampaignAsPreviousNode() {
        return sameCampaignAsPreviousNode;
    }

    public void setSameCampaignAsPreviousNode(boolean sameCampaignAsPreviousNode) {
        this.sameCampaignAsPreviousNode = sameCampaignAsPreviousNode;
    }

    public Double getInitNodeFrequency() {
        return initNodeFrequency;
    }

    public void setInitNodeFrequency(Double initNodeFrequency) {
        this.initNodeFrequency = initNodeFrequency;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public String getPreviousCrops() {
        return String.join(" ; ", previousCrops);
    }

    public void addPreviousCrop(String previousCrop) {
        this.previousCrops.add(previousCrop);
    }

    public Double getCroppingPlanEntryFrequency() {
        return croppingPlanEntryFrequency;
    }

    public void setCroppingPlanEntryFrequency(Double croppingPlanEntryFrequency) {
        this.croppingPlanEntryFrequency = croppingPlanEntryFrequency;
    }

    public String getIntermediateCropName() {
        return intermediateCropName;
    }

    public void setIntermediateCropName(String intermediateCropName) {
        this.intermediateCropName = intermediateCropName;
    }
}
