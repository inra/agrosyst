package fr.inra.agrosyst.services.performance.indicators;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.OrganicProduct;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.performance.TraitementProduitWithCroppingPlanEntry;
import fr.inra.agrosyst.api.services.performance.TraitementProduitWithSpecies;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import fr.inra.agrosyst.services.referential.csv.RefActaDosageSPCModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static org.nuiton.i18n.I18n.l;

/**
 * Classe pour calculer les 4 indicateurs suivants :
 *
 * - recours à des moyens biologiques. Cet indicateur est calculé pour les actions de type 'Semis', lorsque la case 'Inoculation biologique des plants' est cochées dans l'action de semis
 * - recours aux macro-organismes. Cet indicateur est calculé pour les produits phyto utilisés qui sont de type MACRO_ORGANISMS.
 * - recours aux produits biotiques sans AMM. Cet indicateur est calculé pour les produits phyto utilisés qui sont de type BASIC_SUBSTANCES.
 * - recours aux produits abiotiques sans AMM. Cet indicateur est calculé pour les produits phyto utilisés qui sont de type FERTILIZING_MATERIALS_AND_GROWING_MEDIA ou PLANT_EXTRACT.
 *
 * Ces 4 indicateurs sont calculés selon la formule suivante :
 *
 *   nombre d'utilisations * PSCi
 *
 * Donc par exemple, si on trouve 2 produits de type BASIC_SUBSTANCE et qu'on a un PSCi = 0.7, alors l'indicateur
 * "Recours aux produits biotiques sans AMM" sera égal à :
 *
 * 2 * 0.7 = 1.4
 */
public class IndicatorOrganicProducts extends AbstractIndicator {
    private static final Log LOGGER = LogFactory.getLog(IndicatorOrganicProducts.class);

    public static final String[] COLUMN_NAMES = new String[] {
            "recours_moyens_biologiques",
            "recours_macroorganismes",
            "recours_produits_biotiques_sansamm",
            "recours_produits_abiotiques_sansamm"
    };

    protected static final String[] FIELDS = new String[]{
            "recours_aux_moyens_biologiques",
            "recours_macro_organismes",
            "recours_produits_biotiques",
            "recours_produits_abiotiques"
    };

    protected static final String[] INDICATORS = new String[] {
            "Indicator.label.recours_aux_moyens_biologiques", // anciennement IFT moyens biologiques refs #7686
            "Indicator.label.recours_macro_organismes",
            "Indicator.label.recours_produits_biotiques",
            "Indicator.label.recours_produits_abiotiques"
    };

    protected Boolean[] recoursToDisplay = new Boolean[] {
            true,  // Recours aux moyens biologiques (anciennement IFT moyens biologiques)
            true,  // Recours aux macro-organismes
            true,  // Recours aux produits biotiques sans AMM
            true,  // Recours aux produits abiotiques sans AMM
    };

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        for (int i = 0; i < COLUMN_NAMES.length; i++) {
            indicatorNameToColumnName.put(getIndicatorLabel(i), COLUMN_NAMES[i]);
        }
        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {
        Double[] result = newArray(INDICATORS.length, 0.0);
        if (interventionContext.isFictive()) {
            return result;
        }

        final PracticedIntervention intervention = interventionContext.getIntervention();
        final String interventionId = intervention.getTopiaId();
        final double psci = getToolPSCi(interventionContext.getIntervention());
        Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement =
                interventionContext.getTraitementProduitCategByIdTraitement();

        Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();


        // Calcul pour les actions de semis
        final Double[] values = computeSeedingInputsIFT(
                writerContext,
                traitementProduitCategByIdTraitement,
                optionalSeedingActionUsage,
                psci,
                interventionId);
        result = sum(result, values);

        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionContext.getOptionalBiologicalControlAction();
        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction();

        if (optionalBiologicalControlAction.isPresent()) {
            BiologicalControlAction treatmentAction = optionalBiologicalControlAction.get();
            Collection<BiologicalProductInputUsage> biologicalProductInputUsages = treatmentAction.getBiologicalProductInputUsages();

            if (CollectionUtils.isNotEmpty(biologicalProductInputUsages)) {
                result = sum(result,
                        computeTreatmentIftAndWriteInputPracticed(
                                writerContext,
                                interventionContext,
                                traitementProduitCategByIdTraitement,
                                treatmentAction,
                                biologicalProductInputUsages));
            }
        }

        if (optionalPesticidesSpreadingAction.isPresent()) {
            PesticidesSpreadingAction treatmentAction = optionalPesticidesSpreadingAction.get();
            Collection<PesticideProductInputUsage> pesticideProductInputs = treatmentAction.getPesticideProductInputUsages();

            if (CollectionUtils.isNotEmpty(pesticideProductInputs)) {
                result = sum(result,
                        computeTreatmentIftAndWriteInputPracticed(
                                writerContext,
                                interventionContext,
                                traitementProduitCategByIdTraitement,
                                treatmentAction,
                                pesticideProductInputs));
            }
        }

        return result;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {
        Double[] result = newArray(INDICATORS.length, 0.0);
        final EffectiveIntervention intervention = interventionContext.getIntervention();
        final String interventionId = intervention.getTopiaId();
        final double psci = getToolPSCi(interventionContext.getIntervention());
        Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement =
                interventionContext.getTraitementProduitCategByIdTraitement();

        Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();

        final Double[] values = computeSeedingInputsIFT(
                writerContext,
                traitementProduitCategByIdTraitement,
                optionalSeedingActionUsage,
                psci,
                interventionId);
        result = sum(result, values);

        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionContext.getOptionalBiologicalControlAction();
        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction();

        if (optionalBiologicalControlAction.isPresent()) {
            BiologicalControlAction treatmentAction = optionalBiologicalControlAction.get();
            Collection<BiologicalProductInputUsage> biologicalProductInputUsages = treatmentAction.getBiologicalProductInputUsages();

            if (CollectionUtils.isNotEmpty(biologicalProductInputUsages)) {
                result = sum(result,
                        computeTreatmentIftAndWriteInputEffective(
                                writerContext,
                                interventionContext,
                                traitementProduitCategByIdTraitement,
                                treatmentAction,
                                biologicalProductInputUsages));
            }
        }

        if (optionalPesticidesSpreadingAction.isPresent()) {
            PesticidesSpreadingAction treatmentAction = optionalPesticidesSpreadingAction.get();
            Collection<PesticideProductInputUsage> pesticideProductInputs = treatmentAction.getPesticideProductInputUsages();

            if (CollectionUtils.isNotEmpty(pesticideProductInputs)) {
                result = sum(result,
                        computeTreatmentIftAndWriteInputEffective(
                                writerContext,
                                interventionContext,
                                traitementProduitCategByIdTraitement,
                                treatmentAction,
                                pesticideProductInputs));
            }
        }

        return result;
    }

    private Double[] computeSeedingInputsIFT(
            WriterContext writerContext,
            Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement,
            Optional<SeedingActionUsage> optionalSeedingActionUsage,
            double toolsPsci,
            String interventionId) {
        Double[] result = newArray(INDICATORS.length, 0.0);

        if (optionalSeedingActionUsage.isPresent()) {
            SeedingActionUsage seedingActionUsage = optionalSeedingActionUsage.get();
            Collection<SeedLotInputUsage> seedLotInputUsages = seedingActionUsage.getSeedLotInputUsage();

            boolean isOrganicProduct = seedLotInputUsages.stream()
                    .map(SeedLotInputUsage::getDomainSeedLotInput)
                    .map(DomainSeedLotInput::getDomainSeedSpeciesInput)
                    .flatMap(Collection::stream)
                    .filter(Objects::nonNull)
                    .anyMatch(DomainSeedSpeciesInput::isBiologicalSeedInoculation);

            final List<DomainPhytoProductInput> domainPhytoProducts = seedLotInputUsages.stream()
                    .map(SeedLotInputUsage::getDomainSeedLotInput)
                    .flatMap(domainSeedLotInput -> domainSeedLotInput.getDomainSeedSpeciesInput().stream())
                    .filter(d -> d != null && d.getSpeciesPhytoInputs() != null)
                    .flatMap(domainSeedSpeciesInput -> domainSeedSpeciesInput.getSpeciesPhytoInputs().stream())
                    .toList();
            boolean usesMacroOrganisms = domainPhytoProducts.stream()
                    .anyMatch(domainPhytoProductInput -> domainPhytoProductInput.getProductType() == ProductType.MACRO_ORGANISMS);
            boolean usesBioticProducts = domainPhytoProducts.stream()
                    .anyMatch(domainPhytoProductInput -> domainPhytoProductInput.getProductType() == ProductType.BASIC_SUBSTANCES);
            boolean usesAbioticProducts = domainPhytoProducts.stream()
                    .anyMatch(domainPhytoProductInput ->
                            domainPhytoProductInput.getProductType() == ProductType.FERTILIZING_MATERIALS_AND_GROWING_MEDIA ||
                                    domainPhytoProductInput.getProductType() == ProductType.PLANT_EXTRACT
                    );

            incrementAngGetTotalFieldCounterForTargetedId(interventionId); // isOrganicProduct
            incrementAngGetTotalFieldCounterForTargetedId(interventionId); // usesMacroOrganisms
            incrementAngGetTotalFieldCounterForTargetedId(interventionId); // usesBioticProducts
            incrementAngGetTotalFieldCounterForTargetedId(interventionId); // usesAbioticProducts

            Collection<SeedProductInputUsage> seedingProductInputUsages = new HashSet<>();
            seedLotInputUsages.forEach(
                    seedLotInputUsage -> {
                        final Collection<SeedSpeciesInputUsage> seedingSpecies = CollectionUtils.emptyIfNull(seedLotInputUsage.getSeedingSpecies());
                        seedingSpecies.forEach(
                                seedSpeciesInputUsage -> seedingProductInputUsages.addAll(seedSpeciesInputUsage.getSeedProductInputUsages())
                        );
                    }
            );

            if (CollectionUtils.isEmpty(seedingProductInputUsages)) {
                int organicProductsUsages = isOrganicProduct ? 1 : 0;
                int macroOrganismsUsages = usesMacroOrganisms ? 1 : 0;
                int bioticProductsUsages = usesBioticProducts ? 1 : 0;
                int abioticProductsUsages = usesAbioticProducts ? 1 : 0;

                result = sum(result, computeOrganicProductIndicators(toolsPsci, organicProductsUsages, macroOrganismsUsages, bioticProductsUsages, abioticProductsUsages));
            }

            for (SeedProductInputUsage phytoProductInputUsage : seedingProductInputUsages) {
                int organicProductsUsages = 0;
                int macroOrganismsUsages = 0;
                int bioticProductsUsages = 0;
                int abioticProductsUsages = 0;

                final String inputTopiaId = phytoProductInputUsage.getTopiaId();

                DomainPhytoProductInput domainPhytoProductInput = phytoProductInputUsage.getDomainPhytoProductInput();
                RefActaTraitementsProduit refActaTraitementsProduit = domainPhytoProductInput.getRefInput();
                if (refActaTraitementsProduit == null) {
                    LOGGER.warn("Can't find phyto product for input " + domainPhytoProductInput.getInputName());
                    return result;
                }
                RefActaTraitementsProduitsCateg traitementProduitCateg = traitementProduitCategByIdTraitement.get(refActaTraitementsProduit);
                if (traitementProduitCateg == null) {
                    LOGGER.warn("Aucune donnée dans le référentiel ACTA Traitements Produits (Catégories) pour le produit " +
                            refActaTraitementsProduit.getNom_produit() + " idProduit: '" +
                            refActaTraitementsProduit.getId_produit() + "' idTraitement: '" +
                            refActaTraitementsProduit.getId_traitement() + "'");
                    continue;
                }

                if (isOrganicProduct && traitementProduitCateg.isIft_moy_bio()) {
                    organicProductsUsages += 1;
                }
                final ProductType productType = traitementProduitCateg.getType_produit();
                if (usesMacroOrganisms && productType == ProductType.MACRO_ORGANISMS) {
                    macroOrganismsUsages += 1;
                }

                if (usesBioticProducts && productType == ProductType.BASIC_SUBSTANCES) {
                    bioticProductsUsages += 1;
                }

                if (usesAbioticProducts && (productType == ProductType.FERTILIZING_MATERIALS_AND_GROWING_MEDIA || productType == ProductType.PLANT_EXTRACT)) {
                    abioticProductsUsages += 1;
                }

                final Double[] values = computeOrganicProductIndicators(toolsPsci, organicProductsUsages, macroOrganismsUsages, bioticProductsUsages, abioticProductsUsages);
                IndicatorWriter writer = writerContext.getWriter();

                String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(inputTopiaId, MissingMessageScope.INPUT);
                Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(inputTopiaId);

                for (int i = 0; i < values.length; i++) {

                    boolean isDisplayed = isDisplayed(ExportLevel.INPUT, i);

                    if (isDisplayed) {
                        // write intervention sheet
                        writer.writeInputUsage(
                                writerContext.getIts(),
                                writerContext.getIrs(),
                                getIndicatorCategory(),
                                getIndicatorLabel(i),
                                phytoProductInputUsage,
                                seedingActionUsage,
                                writerContext.getEffectiveIntervention(),
                                writerContext.getPracticedIntervention(),
                                writerContext.getCodeAmmBioControle(),
                                writerContext.getAnonymizeDomain(),
                                writerContext.getAnonymizeGrowingSystem(),
                                writerContext.getPlot(),
                                writerContext.getZone(),
                                writerContext.getPracticedSystem(),
                                writerContext.getCroppingPlanEntry(),
                                writerContext.getPracticedPhase(),
                                writerContext.getSolOccupationPercent(),
                                writerContext.getEffectivePhase(),
                                writerContext.getRank(),
                                writerContext.getPreviousPlanEntry(),
                                writerContext.getIntermediateCrop(),
                                this.getClass(),
                                values[i],
                                reliabilityIndexForInputId,
                                reliabilityCommentForInputId,
                                null,
                                null,
                                null,
                                writerContext.getGroupesCiblesByCode());
                    }
                }
                result = sum(result, values);
            }
        }

        return result;
    }

    private Double[] computeOrganicProductIndicators(double psci, int organicProductsUsages, int macroOrganismsUsages, int bioticProductsUsages, int abioticProductsUsages) {
        Double[] result = newArray(INDICATORS.length, 0.0);
        result[0] += organicProductsUsages * psci;
        result[1] += macroOrganismsUsages  * psci;
        result[2] += bioticProductsUsages  * psci;
        result[3] += abioticProductsUsages * psci;
        return result;
    }

    private Double[] computeTreatmentIftAndWriteInputEffective(
            WriterContext writerContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement,
            AbstractAction treatmentAction,
            Collection<? extends AbstractPhytoProductInputUsage> phytoProductInputUsages) {

        Double[] result = newArray(INDICATORS.length, 0.0);

        EffectiveIntervention intervention = interventionContext.getIntervention();
        String interventionId = intervention.getTopiaId();
        final boolean isSpecies = CollectionUtils.isNotEmpty(interventionContext.getInterventionSpecies());
        final double inputPSCi = getPesticideOrBiologicalControlActionPSCi(intervention, treatmentAction);

        return computeAndWriteIftTreatment(writerContext, interventionContext, traitementProduitCategByIdTraitement, treatmentAction, phytoProductInputUsages, isSpecies, interventionId, result, inputPSCi);
    }

    private Double[] computeTreatmentIftAndWriteInputPracticed(
            WriterContext writerContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement,
            AbstractAction treatmentAction,
            Collection<? extends AbstractPhytoProductInputUsage> phytoProductInputUsages) {

        Double[] result = newArray(INDICATORS.length, 0.0);

        PracticedIntervention intervention = interventionContext.getIntervention();
        String interventionId = intervention.getTopiaId();
        final boolean isSpecies = CollectionUtils.isNotEmpty(interventionContext.getInterventionSpecies());

        final double inputPSCi = getPesticideOrBiologicalControlActionPSCi(intervention, treatmentAction);

        return computeAndWriteIftTreatment(writerContext, interventionContext, traitementProduitCategByIdTraitement, treatmentAction, phytoProductInputUsages, isSpecies, interventionId, result, inputPSCi);
    }

    private Double[] computeAndWriteIftTreatment(WriterContext writerContext, PerformanceInterventionContext interventionContext, Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement, AbstractAction treatmentAction, Collection<? extends AbstractPhytoProductInputUsage> phytoProductInputUsages, boolean isSpecies, String interventionId, Double[] result, double inputPSCi) {
        for (AbstractPhytoProductInputUsage phytoProductInputUsage : phytoProductInputUsages) {

            final String inputTopiaId = phytoProductInputUsage.getTopiaId();

            // espèces
            incrementAngGetTotalFieldCounterForTargetedId(inputTopiaId);
            if (!isSpecies) {
                addMissingFieldMessage(
                        interventionId,
                        messageBuilder.getMissingInterventionSpeciesMessage());
            }

            // L’onglet «ACTA_traitements_produits» indique quels produits sont concernés par le calcul de l’IFT biocontrôle (colonne F).
            final RefActaTraitementsProduit phytoProduct = phytoProductInputUsage.getDomainPhytoProductInput().getRefInput();

            String refDoseValue = null;
            String refDoseUnit = "N/A";
            String refDoseUi = "";

            if (isSpecies && phytoProduct != null) {

                ReferenceDoseDTO referenceDose = getProductDoseHomologue(
                        phytoProduct,
                        interventionContext);

                refDoseUi = getReferencesDosageUI(phytoProduct, referenceDose);
                interventionContext.addLegacyReferencesDosagesUserInfos(refDoseUi);

                if (referenceDose != null) {
                    refDoseValue = referenceDose.getValue() != null ? referenceDose.getValue().toString() : null;
                    refDoseUnit = RefActaDosageSPCModel.PHYTO_PRODUCT_UNIT_FORMATTER.format(referenceDose.getUnit());
                }

            }

            Double[] inputResult = computeValues(
                    traitementProduitCategByIdTraitement,
                    phytoProductInputUsage,
                    inputPSCi
            );

            IndicatorWriter writer = writerContext.getWriter();

            String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(inputTopiaId, MissingMessageScope.INPUT);
            Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(inputTopiaId);

            for (int i = 0; i < inputResult.length; i++) {

                boolean isDisplayed = isDisplayed(ExportLevel.INPUT, i);

                if (isDisplayed) {
                    // write input sheet
                    writer.writeInputUsage(
                            writerContext.getIts(),
                            writerContext.getIrs(),
                            getIndicatorCategory(),
                            getIndicatorLabel(i),
                            phytoProductInputUsage,
                            treatmentAction,
                            writerContext.getEffectiveIntervention(),
                            writerContext.getPracticedIntervention(),
                            writerContext.getCodeAmmBioControle(),
                            writerContext.getAnonymizeDomain(),
                            writerContext.getAnonymizeGrowingSystem(),
                            writerContext.getPlot(),
                            writerContext.getZone(),
                            writerContext.getPracticedSystem(),
                            writerContext.getCroppingPlanEntry(),
                            writerContext.getPracticedPhase(),
                            writerContext.getSolOccupationPercent(),
                            writerContext.getEffectivePhase(),
                            writerContext.getRank(),
                            writerContext.getPreviousPlanEntry(),
                            writerContext.getIntermediateCrop(),
                            this.getClass(),
                            inputResult[i],
                            reliabilityIndexForInputId,
                            reliabilityCommentForInputId,
                            refDoseUi,
                            refDoseValue,
                            refDoseUnit,
                            writerContext.getGroupesCiblesByCode());
                }
            }

            result = sum(result, inputResult);
        }
        return result;
    }

    private Double[] computeValues(
            Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement,
            AbstractPhytoProductInputUsage phytoProductInput,
            double inputPSCi) {

        Double[] result = newArray(INDICATORS.length, 0.0);
        final String inputTopiaId = phytoProductInput.getTopiaId();

        final DomainPhytoProductInput domainPhytoProductInput = phytoProductInput.getDomainPhytoProductInput();
        RefActaTraitementsProduit phytoProduct = domainPhytoProductInput.getRefInput();
        if (phytoProduct == null) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Can't find phyto product for input " + domainPhytoProductInput.getInputName());
            }

            addMissingFieldMessage(
                    inputTopiaId,
                    messageBuilder.getMissingInputProductMessage(phytoProductInput.getInputType()));
            return result;
        }

        RefActaTraitementsProduitsCateg traitementProduitCateg = traitementProduitCategByIdTraitement.get(phytoProduct);

        if (traitementProduitCateg == null) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Aucune donnée dans le référentiel ACTA Traitements Produits (Catégories) pour le produit " +
                        phytoProduct.getNom_produit() + " idProduit: '" +
                        phytoProduct.getId_produit() + "' idTraitement: '" +
                        phytoProduct.getId_traitement()+ "'");

            }
            return result;
        }

        // Le calcul est le même quelque soit l'indicateur : nb utilisations * psci
        // dans cette méthode on gère 1 intrant à la fois donc c'est toujours 1 * psci,
        // la somme est gérée par l'appelant. On se contente donc d'affecter le résultat
        // au bon endroit dans le tableau.
        final double value = 1 * inputPSCi;

//        addMissingDeprecatedInputQtyOrUnitIfRequired(phytoProductInput);

        // affectation de l'IFT au catégories concernées
        final ProductType productType = traitementProduitCateg.getType_produit();
        if (traitementProduitCateg.isIft_moy_bio()) {
            result[0] = value; // recours aux moyens biologiques
        }
        if (productType == ProductType.MACRO_ORGANISMS) {
            result[1] = value; // recour macro-organismes
        }
        if (productType == ProductType.BASIC_SUBSTANCES) {
            result[2] = value; // recours produits biotiques
        }
        if (productType == ProductType.FERTILIZING_MATERIALS_AND_GROWING_MEDIA || productType == ProductType.PLANT_EXTRACT) {
            result[3] = value; // recours produits abiotiques
        }

        return result;

    }

    private ReferenceDoseDTO getProductDoseHomologue(
            RefActaTraitementsProduit phytoProduct,
            PerformanceInterventionContext interventionContext) {

        ReferenceDoseDTO referenceDose = null;

        if (interventionContext instanceof PerformanceEffectiveInterventionExecutionContext effectiveInterventionConcext) {
            Map<TraitementProduitWithCroppingPlanEntry, ReferenceDoseDTO> refDosageSPCForPhytoInputs =
                    effectiveInterventionConcext.getLegacyRefDosageForPhytoInputs();

            TraitementProduitWithCroppingPlanEntry traitementProduitWithCroppingPlanEntry =
                    new TraitementProduitWithCroppingPlanEntry(phytoProduct, effectiveInterventionConcext.getCroppingPlanEntry());
            referenceDose = refDosageSPCForPhytoInputs.get(traitementProduitWithCroppingPlanEntry);
        } else if (interventionContext instanceof PerformancePracticedInterventionExecutionContext practicedInterventionContext) {
            Map<TraitementProduitWithSpecies, ReferenceDoseDTO> refDosageSPCForPhytoInputs =
                    practicedInterventionContext.getLegacyRefDosageForPhytoInputs();

            TraitementProduitWithSpecies traitementProduitWithSpecies =
                    new TraitementProduitWithSpecies(phytoProduct, practicedInterventionContext.getCropWithSpecies());

            referenceDose = refDosageSPCForPhytoInputs.get(traitementProduitWithSpecies);
        }

        return referenceDose;
    }

    protected String getReferencesDosageUI(
            RefActaTraitementsProduit phytoProduct,
            ReferenceDoseDTO referenceDose) {

        String referencesDosageUI = "Dose pour '(%s-%s):%s %s'";
        if (referenceDose != null &&
                referenceDose.getValue() != null) {

            PhytoProductUnit unit = referenceDose.getUnit();
            referencesDosageUI = String.format(
                    referencesDosageUI,
                    phytoProduct.getNom_produit(),
                    phytoProduct.getNom_traitement(),
                    referenceDose.getValue().toString(),
                    RefActaDosageSPCModel.PHYTO_PRODUCT_UNIT_FORMATTER.format(unit));

        } else {
            referencesDosageUI = String.format(referencesDosageUI, phytoProduct.getNom_produit(), phytoProduct.getNom_traitement(), "#NA", "");
        }

        return referencesDosageUI;
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return true;
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, "Indicator.category.organicProducts");
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, INDICATORS[i]);
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return recoursToDisplay[i];
    }

    public void init(Collection<OrganicProduct> products) {

        resetRecoursToDisplay();

        for (OrganicProduct product : products) {
            switch (product) {
                case MOYENS_BIOLOGIQUES -> this.recoursToDisplay[0] = true;
                case MACRO_ORGANISMES -> this.recoursToDisplay[1] = true;
                case BIOTIQUES_SANS_AMM -> this.recoursToDisplay[2] = true;
                case ABIOTIQUES_SANS_AMM -> this.recoursToDisplay[3] = true;
            }
        }
    }

    protected void resetRecoursToDisplay() {
        recoursToDisplay = new Boolean[]{
                false,  // Recours aux moyens biologiques (anciennement IFT moyens biologiques)
                false,  // Recours aux macro-organismes
                false,  // Recours aux produits biotiques sans AMM
                false,  // Recours aux produits abiotiques sans AMM
        };
    }
}
