package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Getter
@Setter
@RequiredArgsConstructor
public class RefEspeceDto implements Serializable {
    
    @Serial
    private static final long serialVersionUID = 774371293805167063L;

    protected final String topiaId;

    public static String PROPERTY_CODE_ESPECE_BOTANIQUE = "code_espece_botanique";
    
    public static String PROPERTY_LIBELLE_ESPECE_BOTANIQUE = "libelle_espece_botanique";
    
    public static String PROPERTY_CODE_QUALIFIANT__AEE = "code_qualifiant_AEE";
    
    public static String PROPERTY_LIBELLE_QUALIFIANT__AEE = "libelle_qualifiant_AEE";
    
    public static String PROPERTY_CODE_TYPE_SAISONNIER__AEE = "code_type_saisonnier_AEE";
    
    public static String PROPERTY_LIBELLE_TYPE_SAISONNIER__AEE = "libelle_type_saisonnier_AEE";
    
    public static String PROPERTY_CODE_DESTINATION__AEE = "code_destination_AEE";
    
    public static String PROPERTY_LIBELLE_DESTINATION__AEE = "libelle_destination_AEE";
    
    public static String PROPERTY_CODE_CATEGORIE_DE_CULTURES = "code_categorie_de_cultures";
    
    public static String PROPERTY_LIBELLE_CATEGORIE_DE_CULTURES = "libelle_categorie_de_cultures";
    
    public static String PROPERTY_ID_CULTURE_ACTA = "id_culture_acta";
    
    public static String PROPERTY_NOM_CULTURE_ACTA = "nom_culture_acta";
    
    public static String PROPERTY_CODE_CULTURE_MAA = "code_culture_maa";
    
    public static String PROPERTY_CULTURE_MAA = "culture_maa";
    
    public static String PROPERTY_COMMENTAIRE = "commentaire";
    
    public static String PROPERTY_CODE__CIPAN__AEE = "code_CIPAN_AEE";
    
    public static String PROPERTY_LIBELLE__CIPAN__AEE = "libelle_CIPAN_AEE";
    
    public static String PROPERTY_LIBELLE_DESTINATION__BBCH = "libelle_destination_BBCH";
    
    public static String PROPERTY_PROFIL_VEGETATIF__BBCH = "profil_vegetatif_BBCH";
    
    public static String PROPERTY_COMMENTAIRES = "commentaires";
    
    public static String PROPERTY_CODE_ESPECE__EPPO = "code_espece_EPPO";
    
    public static String PROPERTY_GENRE = "genre";
    
    public static String PROPERTY_ESPECE = "espece";
    
    public static String PROPERTY_CODE__GNIS = "code_GNIS";
    
    public static String PROPERTY_NUM_GROUPE__GNIS = "num_groupe_GNIS";
    
    public static String PROPERTY_NOM__GNIS = "nom_GNIS";
    
    public static String PROPERTY_NOM_LATIN__GNIS = "nom_latin_GNIS";
    
    public static String PROPERTY_REMARQUE_CULTURE_ACTA = "remarque_culture_acta";
    
    public static String PROPERTY_SOURCE = "source";

    public static String PROPERTY_TYPOCAN_ESPECE = "typocan_espece";

    public static String PROPERTY_TYPOCAN_ESPECE_MARAICH = "typocan_espece_maraich";
    
    public static String PROPERTY_ACTIVE = "active";
    
    
    private String code_culture_maa;
    private String culture_maa;
    
    // extends from RefEspeceImpl to be visible into the referential list
    /**
     * Nom de l'attribut en BD : code_categorie_de_cultures
     */
    public String code_categorie_de_cultures;
    
    /**
     * Nom de l'attribut en BD : libelle_categorie_de_cultures
     */
    public String libelle_categorie_de_cultures;
    
    /**
     * Nom de l'attribut en BD : commentaire
     */
    public String commentaire;
    
    /**
     * Nom de l'attribut en BD : code_espece_botanique
     */
    public String code_espece_botanique;
    
    /**
     * Nom de l'attribut en BD : libelle_espece_botanique
     */
    public String libelle_espece_botanique;
    
    /**
     * Nom de l'attribut en BD : code_qualifiant_AEE
     */
    public String code_qualifiant_AEE;
    
    /**
     * Nom de l'attribut en BD : libelle_qualifiant_AEE
     */
    public String libelle_qualifiant_AEE;
    
    /**
     * Nom de l'attribut en BD : code_type_saisonnier_AEE
     */
    public String code_type_saisonnier_AEE;
    
    /**
     * Nom de l'attribut en BD : libelle_type_saisonnier_AEE
     */
    public String libelle_type_saisonnier_AEE;
    
    /**
     * Nom de l'attribut en BD : code_destination_AEE
     */
    public String code_destination_AEE;
    
    /**
     * Nom de l'attribut en BD : libelle_destination_AEE
     */
    public String libelle_destination_AEE;
    
    /**
     * Nom de l'attribut en BD : code_CIPAN_AEE
     */
    public String code_CIPAN_AEE;
    
    /**
     * Nom de l'attribut en BD : libelle_CIPAN_AEE
     */
    public String libelle_CIPAN_AEE;
    
    /**
     * Nom de l'attribut en BD : libelle_destination_BBCH
     */
    public String libelle_destination_BBCH;
    
    /**
     * Nom de l'attribut en BD : profil_vegetatif_BBCH
     */
    public String profil_vegetatif_BBCH;
    
    /**
     * Nom de l'attribut en BD : commentaires
     */
    public String commentaires;
    
    /**
     * Nom de l'attribut en BD : code_espece_EPPO
     */
    public String code_espece_EPPO;
    
    /**
     * Nom de l'attribut en BD : genre
     */
    public String genre;
    
    /**
     * Nom de l'attribut en BD : espece
     */
    public String espece;
    
    /**
     * Nom de l'attribut en BD : code_GNIS
     */
    public String code_GNIS;
    
    /**
     * Nom de l'attribut en BD : num_groupe_GNIS
     */
    public String num_groupe_GNIS;
    
    /**
     * Nom de l'attribut en BD : nom_GNIS
     */
    public String nom_GNIS;
    
    /**
     * Nom de l'attribut en BD : nom_latin_GNIS
     */
    public String nom_latin_GNIS;
    
    /**
     * Nom de l'attribut en BD : source
     */
    public String source;
    
    /**
     * Nom de l'attribut en BD : id_culture_acta
     */
    public int id_culture_acta;
    
    /**
     * Nom de l'attribut en BD : nom_culture_acta
     */
    public String nom_culture_acta;
    
    /**
     * Nom de l'attribut en BD : remarque_culture_acta
     */
    public String remarque_culture_acta;

    /** Typologie espèce de la CAN */
    public String typocan_espece;

    /** Typologie espèce de la CAN plus précise pour les espèces maraîchaires */
    public String typocan_espece_maraich;
    
    /**
     * Nom de l'attribut en BD : active
     */
    public boolean active;

}
