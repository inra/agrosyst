package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefMAABiocontrole;
import fr.inra.agrosyst.api.entities.referential.RefMAABiocontroleImpl;
import fr.inra.agrosyst.services.common.CommonService;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class RefMAABiocontroleModel extends AbstractAgrosystModel<RefMAABiocontrole> implements ExportModel<RefMAABiocontrole> {

    public RefMAABiocontroleModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("code_amm", RefMAABiocontrole.PROPERTY_CODE_AMM, STRING_MANDATORY_PARSER);
        newMandatoryColumn("biocontrol", RefMAABiocontrole.PROPERTY_BIOCONTROL, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("campagne", RefMAABiocontrole.PROPERTY_CAMPAGNE, INT_MANDATORY_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefMAABiocontrole.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefMAABiocontrole, Object>> getColumnsForExport() {
        ModelBuilder<RefMAABiocontrole> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("code_amm", RefMAABiocontrole.PROPERTY_CODE_AMM);
        modelBuilder.newColumnForExport("campagne", RefMAABiocontrole.PROPERTY_CAMPAGNE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("biocontrol", RefMAABiocontrole.PROPERTY_BIOCONTROL, T_F_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefMAABiocontrole.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefMAABiocontrole newEmptyInstance() {
        RefMAABiocontrole refMAABiocontrole = new RefMAABiocontroleImpl();
        refMAABiocontrole.setActive(true);
        return refMAABiocontrole;
    }
}
