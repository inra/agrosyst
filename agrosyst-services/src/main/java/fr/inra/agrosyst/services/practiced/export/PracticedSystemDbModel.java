package fr.inra.agrosyst.services.practiced.export;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.ModalityDephy;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.services.performance.DataFormatter;
import fr.inra.agrosyst.services.performance.dbPersistence.Column;
import fr.inra.agrosyst.services.performance.dbPersistence.TableRowModel;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import org.apache.commons.lang3.StringUtils;

import java.sql.Date;
import java.util.Collection;
import java.util.Map;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class PracticedSystemDbModel {
    
    public static class PracticedSystemCommonRowModel extends TableRowModel {
        
        protected PracticedSystemCommonRowModel(String id,
                                                String tableName,
                                                Date performanceDate,
                                                String performanceId,
                                                Domain domain,
                                                GrowingSystem growingSystem,
                                                PracticedSystem practicedSystem,
                                                String its,
                                                String irs) {
            
            super(id, tableName);
            
            addColumn(Column.createCommonTableColumn("performance_id", performanceId, String.class));
            addColumn(Column.createCommonTableColumn("date_calcul", performanceDate, Date.class));
            addColumn(Column.createCommonTableColumn("nom_domaine_exploitation", domain.getName(), String.class));
            addColumn(Column.createCommonTableColumn("id_domaine", domain.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("domaine_campagne", domain.getCampaign(), Integer.class));
            addColumn(Column.createCommonTableColumn("domaine_type", domain.getType(), DomainType.class));
            addColumn(Column.createCommonTableColumn("departement", domain.getLocation().getDepartement(), String.class));
            addColumn(Column.createCommonTableColumn("id_dispositif", growingSystem.getGrowingPlan().getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("type_dispositif", growingSystem.getGrowingPlan().getType(), TypeDEPHY.class));
            addColumn(Column.createCommonTableColumn("nom_reseau_it", its, String.class));
            addColumn(Column.createCommonTableColumn("nom_reseau_ir", irs, String.class));
            addColumn(Column.createCommonTableColumn("nom_sdc", growingSystem.getName(), String.class));
            addColumn(Column.createCommonTableColumn("id_sdc", growingSystem.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("num_dephy", StringUtils.defaultString(growingSystem.getDephyNumber()), String.class));
            addColumn(Column.createCommonTableColumn("sdc_filiere", growingSystem.getSector(), Sector.class));
            addColumn(Column.createCommonTableColumn("sdc_valide", growingSystem.isValidated(), Boolean.class));
            addColumn(Column.createCommonTableColumn("type_conduite_sdc", growingSystem.getTypeAgriculture() == null ? "" : growingSystem.getTypeAgriculture().getReference_label(), String.class));
            addColumn(Column.createCommonTableColumn("nom_systeme_synthetise", practicedSystem.getName(), String.class));
            addColumn(Column.createCommonTableColumn("id_systeme_synthetise", practicedSystem.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("synthetise_valide", practicedSystem.isValidated(), Boolean.class));
            addColumn(Column.createCommonTableColumn("synthetise_campagnes", practicedSystem.getCampaigns(), String.class));
            addColumn(Column.createCommonTableColumn("approche_de_calcul", "synthétisé", String.class));
            
        }
    }
    
    public static class InterventionRowModel extends PracticedSystemCommonRowModel implements DataFormatter {
        
        public InterventionRowModel(String performanceId,
                                    Date performanceDate,
                                    PracticedIntervention intervention,
                                    Domain domain,
                                    GrowingSystem growingSystem,
                                    String its,
                                    String irs,
                                    PracticedSystem practicedSystem,
                                    CroppingPlanEntry crop,
                                    Integer rank,
                                    CroppingPlanEntry previousCrop,
                                    PracticedCropCyclePhase phase,
                                    Double perennialCropCyclePercent,
                                    Collection<AbstractAction> actions,
                                    Collection<String> codeAmmBioControle,
                                    Map<String, String> groupesCiblesParCode,
                                    String interventionYealdAverages) {
            
            super(intervention.getTopiaId(),
                    "synthetise_echelle_intervention",
                    performanceDate,
                    performanceId,
                    domain,
                    growingSystem,
                    practicedSystem,
                    its,
                    irs);
            
            String actionNames = getActionsToString(actions);
            Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(actions, domain);
            String inputNames = getInputUsagesToString(inputUsages, actions, crop);
            String groupeCible = getGroupesCiblesToString(inputUsages, groupesCiblesParCode);
            String target = getInputUsageTargetsToString(inputUsages);
            Boolean biocontrole = isOneInputUsageBiocontrole(inputUsages, codeAmmBioControle);
            
            String speciesNames = getCropSpeciesName(crop);
            String varietyNames = getCropVarietyName(crop);
            String interventionSpeciesNames = getInterventionSpeciesName(crop, intervention);
            String interventionVarietyNames = getInterventionVarietyName(crop, intervention);
            
            addColumn(Column.createCommonTableColumn("culture", crop.getName(), String.class));
            addColumn(Column.createCommonTableColumn("culture_id", crop.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("culture_code", crop.getCode(), String.class));
            addColumn(Column.createCommonTableColumn("rang", rank, Integer.class));
            addColumn(Column.createCommonTableColumn("especes", speciesNames, String.class));
            addColumn(Column.createCommonTableColumn("varietes", varietyNames, String.class));
            addColumn(Column.createCommonTableColumn("especes_concernees_par_intervention", interventionSpeciesNames, String.class));
            addColumn(Column.createCommonTableColumn("varietes_concernees_par_intervention", interventionVarietyNames, String.class));
            addColumn(Column.createCommonTableColumn("culture_precedente", previousCrop != null ? previousCrop.getName() : null, String.class));
            addColumn(Column.createCommonTableColumn("culture_precedente_id", previousCrop != null ? previousCrop.getTopiaId() : null, String.class));
            addColumn(Column.createCommonTableColumn("culture_precedente_code", previousCrop != null ? previousCrop.getCode() : null, String.class));
            addColumn(Column.createCommonTableColumn("phase", phase != null ? phase.getType() : null, CropCyclePhaseType.class));
            addColumn(Column.createCommonTableColumn("phase_id", phase != null ? phase.getTopiaId() : null, String.class));
            addColumn(Column.createCommonTableColumn("pourcentage_culture_perennes", perennialCropCyclePercent, Double.class));
            addColumn(Column.createCommonTableColumn("intervention", intervention.getName(), String.class));
            addColumn(Column.createCommonTableColumn("intervention_id", intervention.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("debut_intervention", intervention.getStartingPeriodDate(), String.class));
            addColumn(Column.createCommonTableColumn("fin_intervention", intervention.getEndingPeriodDate(), String.class));
            addColumn(Column.createCommonTableColumn("actions", actionNames, String.class));
            addColumn(Column.createCommonTableColumn("intrants", inputNames, String.class));
            addColumn(Column.createCommonTableColumn("nb_intrants", getNbInputUsages(inputUsages), Integer.class));
            addColumn(Column.createCommonTableColumn("groupe_cible", groupeCible, String.class));
            addColumn(Column.createCommonTableColumn("cibles_traitement", target, String.class));
            addColumn(Column.createCommonTableColumn("biocontrole", biocontrole, Boolean.class));
            addColumn(Column.createCommonTableColumn("rendement_total", StringUtils.defaultString(interventionYealdAverages), String.class));
            
        }
    }
    
    public static class CropRowModel extends PracticedSystemCommonRowModel implements DataFormatter {
        
        public CropRowModel(String performanceId,
                            Date performanceDate,
                            String rowId,
                            Domain domain,
                            GrowingSystem growingSystem,
                            String its,
                            String irs,
                            PracticedSystem practicedSystem,
                            CroppingPlanEntry crop,
                            Integer rank,
                            CroppingPlanEntry previousCrop,
                            PracticedCropCyclePhase phase,
                            Double perennialCropCyclePercent,
                            String printableYealdAverage,
                            String culturePrecedentId,
                            PracticedCropCycleConnection practicedCropCycleConnection) {
            
            super(rowId,
                    "synthetise_echelle_culture", performanceDate,
                    performanceId,
                    domain,
                    growingSystem,
                    practicedSystem,
                    its,
                    irs);
            
            String speciesNames = getCropSpeciesName(crop);
            String varietyNames = getCropVarietyName(crop);
            
            addColumn(Column.createCommonTableColumn("culture", crop.getName(), String.class));
            addColumn(Column.createCommonTableColumn("culture_id", crop.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("culture_code", crop.getCode(), String.class));
            addColumn(Column.createCommonTableColumn("rang", rank, Integer.class));
            addColumn(Column.createCommonTableColumn("especes", speciesNames, String.class));
            addColumn(Column.createCommonTableColumn("varietes", varietyNames, String.class));
            addColumn(Column.createCommonTableColumn("culture_precedente", previousCrop != null ? previousCrop.getName() : null, String.class));
            addColumn(Column.createCommonTableColumn("culture_precedente_id", previousCrop != null ? previousCrop.getTopiaId() : null, String.class));
            addColumn(Column.createCommonTableColumn("culture_precedente_code", previousCrop != null ? previousCrop.getCode() : null, String.class));
            addColumn(Column.createCommonTableColumn("phase", phase != null ? phase.getType() : null, CropCyclePhaseType.class));
            addColumn(Column.createCommonTableColumn("phase_id", phase != null ? phase.getTopiaId() : null, String.class));
            addColumn(Column.createCommonTableColumn("pourcentage_culture_perennes", perennialCropCyclePercent, Double.class));
            addColumn(Column.createCommonTableColumn("rendement", printableYealdAverage, String.class));
            addColumn(Column.createCommonTableColumn("culture_precedent_rang_id", culturePrecedentId, String.class));
            addColumn(Column.createCommonTableColumn("connexion_synthetise_id", practicedCropCycleConnection != null ? practicedCropCycleConnection.getTopiaId() : "", String.class));
        }
    }
    
    public static class PracticedSystemRowModel extends PracticedSystemCommonRowModel {
    
        public PracticedSystemRowModel(String performanceId,
                                       Date performanceDate,
                                       PracticedSystem practicedSystem,
                                       Domain domain,
                                       GrowingSystem growingSystem,
                                       String its,
                                       String irs) {
        
            super(practicedSystem.getTopiaId(),
                    "synthetise_echelle_synthetise",
                    performanceDate,
                    performanceId,
                    domain,
                    growingSystem,
                    practicedSystem,
                    its,
                    irs);
        
            addColumn(Column.createCommonTableColumn("modalite_suivi_dephy", growingSystem.getModality(), ModalityDephy.class));
        }
        
    }
}
