package fr.inra.agrosyst.services.growingsystem.export;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.DoseType;
import fr.inra.agrosyst.api.entities.EstimatingIftRules;
import fr.inra.agrosyst.api.entities.GrowingSystemCharacteristicType;
import fr.inra.agrosyst.api.entities.IftSeedsType;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.managementmode.CategoryStrategy;
import fr.inra.agrosyst.services.common.export.ExportModel;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

/**
 * @author David Cossé
 */
public class GrowingSystemExportModels {

    @Getter
    public static class CommonBean {

        final String department;
        final String growingSystemName;
        final Sector sector;
        final LocalDate startingDate;
        final String growingPlanName;
        final String domainName;
        final Integer campaign;

        /**
         * Constructeur avec tous les champs pour la première instanciation
         */
        public CommonBean(String department, String growingSystemName, Sector sector, LocalDate startingDate,
                          String growingPlanName, String domainName, Integer campaign) {
            this.department = department;
            this.growingSystemName = growingSystemName;
            this.sector = sector;
            this.startingDate = startingDate;
            this.growingPlanName = growingPlanName;
            this.domainName = domainName;
            this.campaign = campaign;
        }

        /**
         * Constructeur par recopie pour faciliter le travail des sous modèles
         */
        public CommonBean(CommonBean source) {
            this(
                    source.department,
                    source.growingSystemName,
                    source.sector,
                    source.startingDate,
                    source.growingPlanName,
                    source.domainName,
                    source.campaign);
        }
    }

    public abstract static class CommonModel<T extends CommonBean> extends ExportModel<T> {
        public CommonModel() {
            newColumn("Département", CommonBean::getDepartment);
            newColumn("Système De Culture", CommonBean::getGrowingSystemName);
            newColumn("Filière", Sector.class, CommonBean::getSector);
            newColumn("Date de début d'utilisation", CommonBean::getStartingDate);
            newColumn("Dispositif", CommonBean::getGrowingPlanName);
            newColumn("Domaine", CommonBean::getDomainName);
            newColumn("Campagne", CommonBean::getCampaign);
        }
    }

    @Getter
    @Setter
    public static class MainBean extends CommonBean {

        String dephyNumber;
        String networkNames;
        String description;
        Double affectedAreaRate;
        Integer affectedWorkForceRate;
        Integer domainsToolsUsageRate;
        LocalDate endingDate;
        String endActivityComment;
        String overallObjectives;

        public MainBean(CommonBean source) {
            super(source);
        }
    }

    public static class MainModel extends CommonModel<MainBean> {
        @Override
        public String getTitle() {
            return "Généralités";
        }

        public MainModel() {
            newColumn("Numéro DEPHY", MainBean::getDephyNumber);
            newColumn("Réseau(x) de rattachement", MainBean::getNetworkNames);

            // Description du SdC
            newColumn("Description", MainBean::getDescription);
            newColumn("Date de fin d'utilisation", MainBean::getEndingDate);
            newColumn("Motif de fin d'utilisation", MainBean::getEndActivityComment);
            newColumn("Objectifs généraux assignés au système de culture", MainBean::getOverallObjectives);

            // Caractéristiques du SdC
            newColumn("Pourcentage de surface du domaine affectée", MainBean::getAffectedAreaRate);
            newColumn("Pourcentage de la main d’oeuvre affectée", MainBean::getAffectedWorkForceRate);
            newColumn("Taux d'utilisation des outils et installations du domaine", MainBean::getDomainsToolsUsageRate);
        }
    }

    @Getter
    @Setter
    public static class MarketingBean extends CommonBean {

        boolean selectedForGS;
        String marketingDestination;
        Double part;

        public MarketingBean(CommonBean source) {
            super(source);
        }
    }

    public static class MarketingModel extends CommonModel<MarketingBean> {
        @Override
        public String getTitle() {
            return "Mode de commercialisation";
        }

        public MarketingModel() {
            newColumn("Sélectionnée", MarketingBean::isSelectedForGS);
            newColumn("Destination", MarketingBean::getMarketingDestination);
            newColumn("Pourcentage", MarketingBean::getPart);
        }
    }

    @Getter
    @Setter
    public static class CaracteristicsBean extends CommonBean {

        String typeAgriculture;
        Double averageIFT;
        Double biocontrolIFT;
        EstimatingIftRules estimatingIftRules;
        IftSeedsType iftSeedsType;
        DoseType doseType;
        CategoryStrategy categoryStrategy;
        GrowingSystemCharacteristicType typeTraitSdc;
        String nomTrait;
        String characteristicComment;

        public CaracteristicsBean(CommonBean source) {
            super(source);
        }
    }

    public static class CaracteristicsModel extends CommonModel<CaracteristicsBean> {
        @Override
        public String getTitle() {
            return "Principaux traits";
        }

        public CaracteristicsModel() {
            newColumn("Type de conduite", CaracteristicsBean::getTypeAgriculture);
            newColumn("Catégorie de stratégie globale", CategoryStrategy.class, CaracteristicsBean::getCategoryStrategy);
            newColumn("IFT moyen (hors biocontrôle)", CaracteristicsBean::getAverageIFT);
            newColumn("IFT biocontrôle", CaracteristicsBean::getBiocontrolIFT);
            newColumn("Modalités d’estimation de l’IFT", EstimatingIftRules.class, CaracteristicsBean::getEstimatingIftRules);
            newColumn("Type d’IFT", IftSeedsType.class, CaracteristicsBean::getIftSeedsType);
            newColumn("Type de doses de référence", DoseType.class, CaracteristicsBean::getDoseType);
            newColumn("Type de caractéristiques", GrowingSystemCharacteristicType.class, CaracteristicsBean::getTypeTraitSdc);
            newColumn("Caractéristiques", CaracteristicsBean::getNomTrait);
            newColumn("Commentaires", CaracteristicsBean::getCharacteristicComment);
        }
    }

    @Getter
    @Setter
    public static class PlotBean extends CommonBean {

        String plotName;
        int pacIlotNumber;
        String plotOutputReason;

        public PlotBean(CommonBean source) {
            super(source);
        }
    }

    public static class PlotModel extends CommonModel<PlotBean> {
        @Override
        public String getTitle() {
            return "Parcelles";
        }

        public PlotModel() {
            newColumn("Nom de la parcelle", PlotBean::getPlotName);
            newColumn("Numéro d'ilôt PAC", PlotBean::getPacIlotNumber);
            newColumn("Motifs de sortie des parcelles", PlotBean::getPlotOutputReason);
        }
    }

}
