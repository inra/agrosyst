package fr.inra.agrosyst.services.common;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.Downloadable;
import fr.inra.agrosyst.api.entities.DownloadableTopiaDao;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.referential.FeedbackCategory;
import fr.inra.agrosyst.api.entities.referential.RefFeedbackRouterTopiaDao;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.referential.ImportResult;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.api.services.users.FeedbackContext;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.hibernate.Session;

import javax.activation.DataSource;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * TODO AThimel 26/01/2021 Il faudrait revoir les méthode {@link #sendEmail} et {@link #sendEmail0} pour qu'elles
 *      acceptent un objet type "mail" plutôt que des champs 1 par 1
 *
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class EmailService extends AbstractAgrosystService implements AgrosystService {

    private static final Log LOGGER = LogFactory.getLog(EmailService.class);

    protected DownloadableTopiaDao downloadableDao;

    protected RefFeedbackRouterTopiaDao refFeedbackRouterDao;

    public void setDownloadableDao(DownloadableTopiaDao downloadableDao) {
        this.downloadableDao = downloadableDao;
    }

    public void setRefFeedbackRouterDao(RefFeedbackRouterTopiaDao refFeedbackRouterDao) {
        this.refFeedbackRouterDao = refFeedbackRouterDao;
    }
    
    protected HtmlEmail newEmptyEmail() {
        HtmlEmail newEmptyEmail = new HtmlEmail();
        newEmptyEmail.setHostName(getConfig().getSmtpHost());
        newEmptyEmail.setSmtpPort(getConfig().getSmtpPort());
        newEmptyEmail.setCharset(StandardCharsets.UTF_8.name());
        try {
            newEmptyEmail.setFrom(getConfig().getEmailFrom());
        } catch (EmailException e) {
            throw new AgrosystTechnicalException("Unable to initialize email template", e);
        }
        return newEmptyEmail;
    }

    @Deprecated
    protected void sendEmail(String subject,
                             List<String> to,
                             List<String> ccs,
                             List<String> bccs,
                             String plainBody,
                             String htmlBody,
                             byte[] screenshotData,
                             InputStream importResult,
                             FeedbackContext.FeedbackAttachment attachment) {
        List<AttachmentInfos> attachments = new LinkedList<>();

        if (screenshotData != null) {
            attachments.add(new AttachmentInfos("screenshot.jpg", screenshotData, "image/jpeg"));
        }
        if (attachment != null) {
            attachments.add(new AttachmentInfos(attachment.getName(), attachment.getData(), attachment.getType()));
        }
        if (importResult != null) {
            try {
                attachments.add(new AttachmentInfos("rapport.csv", IOUtils.toByteArray(importResult), "text/csv"));
            } catch (IOException ioe) {
                throw new AgrosystTechnicalException("Unable to convert importResult to byte[]", ioe);
            }
        }

        sendEmail0(subject, to, ccs, bccs, plainBody, htmlBody, attachments);
    }

    @Deprecated
    protected void sendEmail0(String subject,
                              List<String> to,
                              List<String> ccs,
                              List<String> bccs,
                              String plainBody,
                              String htmlBody,
                              List<AttachmentInfos> attachments) {

        Preconditions.checkArgument(!Strings.isNullOrEmpty(subject));
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(to) && to.stream().anyMatch(Objects::nonNull));
        Preconditions.checkArgument(!Strings.isNullOrEmpty(plainBody) || !Strings.isNullOrEmpty(htmlBody));

        List<String> to1 = to.stream()
                .filter(Objects::nonNull)
                .toList();

        HtmlEmail email = newEmptyEmail();

        // subject
        email.setSubject(subject);

        try {
            // to
            List<InternetAddress> toInternetAddress = new ArrayList<>();
            for (String to_ : to1) {
                InternetAddress internetAddressTo = new InternetAddress(to_);
                toInternetAddress.add(internetAddressTo);
            }

            email.setTo(toInternetAddress);

            // cc
            if (CollectionUtils.isNotEmpty(ccs)) {
                List<InternetAddress> ccList = new ArrayList<>();
                for (String cc : ccs) {
                    InternetAddress internetAddress = new InternetAddress(cc);
                    ccList.add(internetAddress);
                }
                email.setCc(ccList);
            }

            // bcc
            if (CollectionUtils.isNotEmpty(bccs)) {
                List<InternetAddress> bccList = new ArrayList<>();
                for (String cc : bccs) {
                    InternetAddress internetAddress = new InternetAddress(cc);
                    bccList.add(internetAddress);
                }
                email.setBcc(bccList);
            }

            // body
            if (plainBody != null) {
                email.setTextMsg(plainBody);
            }
            if (htmlBody != null) {
                email.setHtmlMsg(htmlBody);
            }

            // attachments
            if (CollectionUtils.isNotEmpty(attachments)) {
                for (AttachmentInfos attachment : attachments) {
                    DataSource ds = new ByteArrayDataSource(attachment.data, attachment.type);
                    email.attach(ds, attachment.name, null, EmailAttachment.ATTACHMENT);
                }
            }

            if (LOGGER.isInfoEnabled()) {
                String format = "Will send e-mail to '%s' with subject '%s'";
                LOGGER.info(String.format(format, email.getToAddresses(), email.getSubject()));
            }

            if (getConfig().isEmailEnabled()) {
                email.send();
            } else if (LOGGER.isWarnEnabled()){
                String message = String.format("Email will not be send because it is explicitly disabled: to=%s ; subject=%s", email.getToAddresses(), email.getSubject());
                LOGGER.warn(message);
            }

        } catch (AddressException ae) {
            throw new AgrosystTechnicalException("Configured address is not usable: " + to, ae);
        } catch (EmailException ee) {
            throw new AgrosystTechnicalException("Unable to send email", ee);
        }
    }

    public void sendPasswordReminder(AgrosystUser user, String token, String next) {
        String applicationBaseUrl = getConfig().getApplicationBaseUrl();
        String reminderUrl = String.format("%s/auth/retrieve-password-input.action?userId=%s&token=%s&next=%s", applicationBaseUrl, user.getTopiaId(), token, next);

        String body = String.format("Bonjour %s,\n", user.getFirstName()) + '\n' +
                "Une demande de regénération du mot de passe a été faite sur Agrosyst.\n\n" +
                "Pour regénérer votre mot de passe, il vous suffit de vous rendre à l'adresse suivante :\n" +
                reminderUrl +
                "\n\n" +
                "S'il s'agit d'une erreur retournez simplement à la page d'accueil.\n" +
                applicationBaseUrl+
                "\n\n" +
                "Cordialement,\n" +
                "L'équipe Agrosyst.\n" +
                '\n' +
                applicationBaseUrl;
        sendEmail("Agrosyst - Mot de passe oublié", Collections.singletonList(user.getEmail()), null,
                null, body, null, null, null, null);
    }
    
    /**
     * Send email with user feedback.
     * @return context
     */
    public FeedbackContext sendFeedback(FeedbackContext feedbackContext) {
        
        List<String> toMails = new ArrayList<>();
        List<String> ccMails = new ArrayList<>();

        if (isForItOnly(feedbackContext)) {
            toMails.add(feedbackContext.getItEmail());
        } else {
            final List<String> destinataires = refFeedbackRouterDao.findDestinataires(
                    feedbackContext.getCategory(),
                    feedbackContext.getDephyType(),
                    feedbackContext.getSectorTypes());

            // Solution de repli si on ne trouve pas de destinataire :
            // l'email de l'IT passe en destinataire, et on l'enlève des emails en cc
            if (CollectionUtils.isEmpty(destinataires)) {
                toMails.add(feedbackContext.getItEmail());
            } else {
                toMails.addAll(destinataires);
                addCcItEmail(ccMails, feedbackContext);
            }
        }

        List<String> cciMails = new ArrayList<>(refFeedbackRouterDao.findCci(feedbackContext.getCategory(), feedbackContext.getDephyType()));

        if (CollectionUtils.isNotEmpty(toMails)) {
            sendFeedbackMessage(feedbackContext, toMails, ccMails, cciMails);
            sendFeedbackReceipt(feedbackContext);

        } else {
            feedbackContext.setFeedbackStatus(FeedbackContext.FeedbackStatus.NOT_SEND);
        }
        return feedbackContext;
    }
    
    protected void sendFeedbackMessage(FeedbackContext feedbackContext, List<String> toMails, List<String> ccMails, List<String> cciMails) {
        feedbackContext.setFeedbackRecipients(toMails, ccMails);
        
        final String feedbackMessageDate = DateTimeFormatter.ofPattern("dd MMMM yyyy à HH:mm").format(context.getCurrentTime());
        feedbackContext.setMessageDateTime(feedbackMessageDate);
        
        String body = "Le " + feedbackMessageDate +
                ",\n\n" +
                "Description\n" +
                "-----------\n\n" +
                feedbackContext.getFeedback() +
                "\n\n" +
                "Emplacement\n" +
                "-----------\n" +
                "Titre de la page : " + feedbackContext.getLocationTitle() + "\n" +
                "URL              : " + feedbackContext.getLocation() + "\n" +
                "URL demandée     : " + feedbackContext.getRequested() + "\n" +
                "URL précédente   : " + feedbackContext.getReferer() + "\n" +
                "\n" +
                "Environnement\n" +
                "-------------\n" +
                feedbackContext.getEnv() +
                "Version services : " +
                getConfig().getApplicationVersion();
        
        String user = String.format("%s %s <%s>", feedbackContext.getAgrosystUser().getFirstName(), feedbackContext.getAgrosystUser().getLastName(), feedbackContext.getAgrosystUser().getEmail());
        
        try {
            
            sendEmail("Feedback Agrosyst - " + feedbackContext.getCategory() + " - " + user, toMails, ccMails,
                    cciMails, body, null, feedbackContext.getScreenshotData(), null, feedbackContext.getAttachment());
            feedbackContext.setFeedbackStatus(FeedbackContext.FeedbackStatus.SUCCESS);
            
        } catch (AgrosystTechnicalException me) {
            
            feedbackContext.setFeedbackStatus(FeedbackContext.FeedbackStatus.FAILED);
            
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(me.getMessage());

                final Collection<String> allRecipients = feedbackContext.getFeedbackRecipients();
                String allRecipientsEmails = StringUtils.join(allRecipients, ", ");

                LOGGER.error(
                        String.format("Échec d'envoie du feedback %s %s \n %s",
                                allRecipients.size() == 1 ? "au destinataire" : "aux destinataires",
                                allRecipientsEmails ,
                                body));
            }
        }
    }
    
    private void sendFeedbackReceipt(FeedbackContext feedbackContext) {
        
        Collection<String> eMails = feedbackContext.getFeedbackRecipients();
        String recipientEmailsPart = eMails.size() == 1 ? "à l'adresse suivante : %s " : "aux adresses suivantes : %s \n";
        String allRecipientsEmails = StringUtils.join(feedbackContext.getFeedbackRecipients(), ", ");
        recipientEmailsPart = String.format(recipientEmailsPart, allRecipientsEmails);
    
        final String sendingStatusPart = switch (feedbackContext.getFeedbackStatus()) {
            case SUCCESS -> ", le message suivant a été transmis ";
            case FAILED -> ", le message suivant n'a pu être transmis ";
            case NOT_SEND -> ", le message suivant n'a pas été transmis, aucun destinataire trouvé correspondant à la catégorie d'annomalie choisie ";
            default -> throw new IllegalStateException("Unexpected value: " + feedbackContext.getFeedbackStatus());
        };
        // Récépissé de la demande
        String body = "Le " + feedbackContext.getMessageDateTime() +
                sendingStatusPart +
                recipientEmailsPart +
                "\n\n" +
                "Description\n" +
                "-----------\n\n" +
                feedbackContext.getFeedback() +
                "\n\n" +
                "Emplacement\n" +
                "-----------\n" +
                "Titre de la page : " + feedbackContext.getLocationTitle() + "\n" +
                "URL              : " + feedbackContext.getLocation() + "\n" +
                "URL demandée     : " + feedbackContext.getRequested() + "\n" +
                "URL précédente   : " + feedbackContext.getReferer() + "\n" +
                "\n" +
                "Environnement\n" +
                "-------------\n" +
                feedbackContext.getEnv() +
                "Version services : " +
                getConfig().getApplicationVersion();
    
        String user = String.format("%s %s <%s>", feedbackContext.getAgrosystUser().getFirstName(), feedbackContext.getAgrosystUser().getLastName(), feedbackContext.getAgrosystUser().getEmail());
    
        try {
        
            sendEmail("Récépissé de feedback Agrosyst - " + feedbackContext.getCategory() + " - " + user,
                    Collections.singletonList(feedbackContext.getAgrosystUser().getEmail()), null, null, body,
                    null, feedbackContext.getScreenshotData(), null,
                    feedbackContext.getAttachment());
        
        } catch (AgrosystTechnicalException me) {
        
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(me.getMessage());
            
                LOGGER.error(
                        String.format("Échec d'envoie du récépissé de feedback à l'utilisateur %s \n %s",
                                user,
                                body));
            }
        }
    }
    
    protected boolean isForItOnly(FeedbackContext feedbackContext) {
        return FeedbackCategory.EDAPLOS.equals(feedbackContext.getCategory()) &&
                TypeDEPHY.DEPHY_FERME.equals(feedbackContext.getDephyType()) &&
                !feedbackContext.isNotNetworkIrChecked();
    }
    
    protected void addCcItEmail(List<String> emails, FeedbackContext feedbackContext) {
        if (TypeDEPHY.DEPHY_FERME.equals(feedbackContext.getDephyType()) &&
                !feedbackContext.isNotNetworkIrChecked() &&
                StringUtils.isNotBlank(feedbackContext.getItEmail())) {
            emails.add(feedbackContext.getItEmail());
        }
    }
    
    /**
     * Envoi un mail de notification de création de compte.
     *
     * @param user updated user
     */
    public void sendCreatedAccountNotification(AgrosystUser user, String token) {
        String applicationBaseUrl = getConfig().getApplicationBaseUrl();
        String supportEmail = getConfig().getSupportEmail();
        String reminderUrl = String.format("%s/auth/retrieve-password-input.action?userId=%s&token=%s", applicationBaseUrl, user.getTopiaId(), token);

        String body = "Bonjour,\n" + '\n' +
                "Quelqu'un - sans doute vous ou un administrateur Agrosyst - a demandé d'allouer ou de renouveler votre mot de passe associé à votre compte sur le site " + applicationBaseUrl + "\n\n" +
                "Vous pouvez ignorer cette requête ou bien cliquer sur le lien suivant pour choisir votre mot de passe :\n" +
                reminderUrl +
                "\n\n" +
                "Si vous avez des questions par rapport à ce message vous pouvez contacter : " + supportEmail +
                "\n\n" +
                "Cordialement,\n" +
                "L'équipe Agrosyst.\n";
        sendEmail("Agrosyst - Mise à jour de votre profil", Collections.singletonList(user.getEmail()), null,
                null, body, null, null, null, null);
    }

    public void sendImportReferentialReport(String userEmail, Class<?> report, ImportResult result, InputStream errorAttachement) {
        String supportEmail = getConfig().getSupportEmail();
        int created = result.getCreated();
        int updated = result.getUpdated();
        int ignore = result.getIgnored();
        List<String> errors = result.getErrors();

        String summary = "Rapport d'import pour " + report + "\n";
        summary += String.format("%d entitée créés, %d mises à jour, %d lignes ignorées, %d en erreur.\n", created, updated, ignore, errors.size());
        summary += "Veuillez consulter le fichier en pièce jointe pour plus de détails";

        try {
            sendEmail("Agrosyst - Import report (" + report.getSimpleName() + ")", Collections.singletonList(userEmail), Collections.singletonList(supportEmail), null,
                    summary, null, null, errorAttachement, null);
        } catch (AgrosystTechnicalException me) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(me.getMessage());
                LOGGER.error(String.format("Failed to send mail to user email %s \n %s", userEmail , summary));
            }
        }
    }

    /**
     * Envoi un mail de notification d'import eDaplos avec le rapport en piece jointe.
     *
     * @param user updated user
     */
    public void sendEDaplosReport(AuthenticatedUser user, String filename, InputStream eDaplosReport) {
        String supportEmail = getConfig().getSupportEmail();

        StringBuilder body = new StringBuilder("Bonjour,\n");
        body.append('\n');
        body.append("Votre fichier eDaplos (");
        body.append(filename);
        body.append(") à été intégré avec succès.\n");
        body.append("Le rapport des éléménts restants à prendre en compte est attaché en pièce jointe.\n");
        body.append("\n\n");
        body.append("Cordialement,\n");
        body.append("L'équipe Agrosyst.\n");

        try {
            sendEmail("Agrosyst - Import eDaplos (" + filename + ")", Collections.singletonList(user.getEmail()), Collections.singletonList(supportEmail), null,
                    body.toString(), null, null, eDaplosReport, null);
        } catch (AgrosystTechnicalException me) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(me.getMessage());
                LOGGER.error(String.format("Failed to send mail to user email %s \n %s", user.getEmail() , body));
            }
        }
    }

    public void sendEDaplosExceptionFeedback(AuthenticatedUser user, Exception e, String context, String fileName) {
        String feedbackEmail = getConfig().getFeedbackEmail();

        StringBuilder body = new StringBuilder("Le ");
        body.append(DateTimeFormatter.ofPattern("dd MMMM yyyy à HH:mm").format(this.context.getCurrentTime()));
        body.append(",\n\n");

        body.append("Emplacement\n");
        body.append("-----------\n");
        body.append("Action de la page        : ").append(context).append("\n");
        body.append("Nom du ficher            : ").append(fileName).append("\n");
        body.append("URL de l'application     : ").append(getConfig().getApplicationBaseUrl()).append("\n");
        body.append("Version de l'application : ").append(getConfig().getApplicationVersion()).append("\n");
        body.append("Email utilisateur        : ").append(user.getEmail());
        body.append(",\n\n");

        body.append("Exception survenue à l'import d'un fichier eDaplos\n");

        body.append("-----------\n\n");

        body.append(e.getClass().getName()).append("\n");

        if (e.getMessage() != null) {
            body.append(e.getMessage()).append("\n");
        }

        StackTraceElement[] stes = e.getStackTrace();
        for (StackTraceElement ste : stes) {
            body.append(ste.toString()).append("\n");
        }

        body.append("\n\n");

        try {
            sendEmail("Feedback Agrosyst - " + user.getEmail(), Collections.singletonList(feedbackEmail), null,
                    null, body.toString(), null, null, null, null);
        } catch (AgrosystTechnicalException me) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(me.getMessage());
                LOGGER.error(String.format("Failed to send mail to user email %s \n %s", user.getEmail() , body));
            }
        }
    }

    /**
     * Envoi un mail automatiquement en cas d'erreur de génération des fichiers de performances.
     */
    public void notifyPerformanceError(String userEmail, String performanceId, Throwable t) {
        String supportEmail = getConfig().getSupportEmail();

        StringBuilder body = new StringBuilder("Bonjour,\n");
        body.append('\n');
        body.append("L'utilisateur ");
        body.append(userEmail);
        body.append(" a tenté de génerer un fichier pour la performance '");
        body.append(performanceId);
        body.append("' mais l'erreur suivante s'est produite:\n");
        body.append(ExceptionUtils.getStackTrace(t));
        body.append("\n\n");
        body.append("Cordialement,\n");
        body.append("L'équipe Agrosyst.\n");

        try {
            sendEmail("Agrosyst - Erreur de performance", Collections.singletonList(supportEmail), null, null,
                    body.toString(), null, null, null, null);
        } catch (AgrosystTechnicalException me) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(me.getMessage());
                LOGGER.error(String.format("Failed to send mail to user email %s \n %s", userEmail , body));
            }
        }
    }

    public void notifyPerformanceSuccess(String userEmail, String performanceName, long executionTime) {
        if (StringUtils.isNotEmpty(userEmail)) {
            StringBuilder body = new StringBuilder("Bonjour,\n");
            body.append('\n');
            body.append("Le rapport de performance '").append(performanceName).append(String.format("' s'est correctement terminé, sa génération a pris %ds.", executionTime));
            body.append("\n\n");
            body.append("Cordialement,\n");
            body.append("L'équipe Agrosyst.\n");

            try {
                sendEmail("Agrosyst - Rapport de performance généré", Collections.singletonList(userEmail), null, null,
                        body.toString(), null, null, null, null);
            } catch (AgrosystTechnicalException me) {
                if (LOGGER.isErrorEnabled()) {
                    String message = String.format("Failed to send mail to user email %s \n %s", userEmail, body);
                    LOGGER.error(message, me);
                }
            }
        }
    }
    
    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    public void notifyExportSuccessful(UUID taskId, String userEmail, String description, ExportResult result) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(userEmail));
        Preconditions.checkArgument(StringUtils.isNotEmpty(description));
        Preconditions.checkArgument(result != null);

        int retentionDays = getConfig().getDownloadableRetentionDays();
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime expiresOn = now.plusDays(retentionDays);

        final String attachmentName = result.getFileName();
        final String attachmentType = result.content().type().toString();

        // copy file into blob
        Session hibernateSession = getPersistenceContext().getHibernateSupport().getHibernateSession();
        Blob blob = hibernateSession.getLobHelper().createBlob(result.content().bytes());

        final Downloadable downloadable = downloadableDao.create(
                Downloadable.PROPERTY_FILE_NAME, attachmentName,
                Downloadable.PROPERTY_EXPIRES_ON, expiresOn,
                Downloadable.PROPERTY_MIME_TYPE, attachmentType,
                Downloadable.PROPERTY_TASK_ID, taskId,
                Downloadable.PROPERTY_CONTENT, blob
        );

        // Fichier écrit en base, on a besoin de commiter
        getTransaction().commit();

        if (LOGGER.isInfoEnabled()) {
            String message = String.format(
                    "On stocke en base le résultat du la tâche t=%s. Il expirera le %s. -> %s",
                    taskId,
                    expiresOn,
                    downloadable.getTopiaId());
            LOGGER.info(message);
        }
        // Il faut commiter pour s'assurer que le fichier est conservé
        getPersistenceContext().commit();

        String applicationBaseUrl = getConfig().getApplicationBaseUrl();
        String id = getPersistenceContext().getTopiaIdFactory().getRandomPart(downloadable.getTopiaId());
        String downloadUrl = String.format("%s/commons/download.action?id=%s", applicationBaseUrl, id);

        StringBuilder body = new StringBuilder("Bonjour,\n");
        body.append('\n');
        body.append("L'").append(description).append(" que vous avez demandé sur Agrosyst est terminé.\n");
        body.append("Le fichier est disponible au téléchargement pendant ").append(retentionDays).append(" jours à l'adresse : ");
        body.append(downloadUrl);
        body.append("\n\n");
        body.append("Cordialement,\n");
        body.append("L'équipe Agrosyst.\n");

        try {
            final String subject = String.format("Agrosyst - %s", StringUtils.capitalize(description));
            sendEmail0(
                    subject,
                    Collections.singletonList(userEmail),
                    null,
                    null,
                    body.toString(),
                    null,
                    Collections.emptyList());
        } catch (AgrosystTechnicalException me) {
            if (LOGGER.isErrorEnabled()) {
                String message = String.format("Failed to send mail to user email %s \n %s", userEmail, body);
                LOGGER.error(message, me);
            }
        }
    }

    public void notifyExportFailure(String userEmail, String description, UUID exportId) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(userEmail));
        Preconditions.checkArgument(StringUtils.isNotEmpty(description));

        StringBuilder body = new StringBuilder("Bonjour,\n");
        body.append('\n');
        body.append("L'").append(description).append(" que vous avez demandé sur Agrosyst n'a pas pu aboutir, merci de réessayer ultérieurement.\n");
        body.append("Si le problème persiste, merci de nous communiquer l'identifiant de traitement: ").append(exportId).append(".");
        body.append("\n\n");
        body.append("Cordialement,\n");
        body.append("L'équipe Agrosyst.\n");

        try {
            final String subject = String.format("Agrosyst - %s - ÉCHEC", StringUtils.capitalize(description));
            sendEmail0(
                    subject,
                    Collections.singletonList(userEmail),
                    null,
                    null,
                    body.toString(),
                    null,
                    Collections.emptyList());
        } catch (AgrosystTechnicalException me) {
            if (LOGGER.isErrorEnabled()) {
                String message = String.format("Failed to send mail to user email %s \n %s", userEmail, body);
                LOGGER.error(message, me);
            }
        }
    }

    private record AttachmentInfos(String name, byte[] data, String type) {}

}
