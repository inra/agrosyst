package fr.inra.agrosyst.services.practiced.export;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.OrchardFrutalForm;
import fr.inra.agrosyst.api.entities.PollinatorSpreadMode;
import fr.inra.agrosyst.api.entities.VineFrutalForm;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemSource;
import fr.inra.agrosyst.services.common.export.ExportModel;
import fr.inra.agrosyst.services.export.ExportMetadata;
import lombok.Getter;
import lombok.Setter;

public class PracticedSystemMetadata extends ExportMetadata {

    @Getter
    public static class PracticedBean extends Bean {

        public PracticedBean(String practicedSystemName,
                             String campaigns,
                             String departement,
                             String networkNames,
                             String growingSystemName,
                             String typeAgricultureLabel,
                             String dephyNumber,
                             String growingPlanName,
                             String domainName,
                             int campaign) {
            super(departement,
                    networkNames,
                    growingSystemName,
                    typeAgricultureLabel,
                    dephyNumber,
                    growingPlanName,
                    domainName,
                    campaign);
            this.practicedSystemName = practicedSystemName;
            this.campaigns = campaigns;
        }

        public PracticedBean(PracticedBean baseBean) {
            this(baseBean.getPracticedSystemName(),
                    baseBean.getCampaigns(),
                    baseBean.getDepartment(),
                    baseBean.getNetworkNames(),
                    baseBean.getGrowingSystemName(),
                    baseBean.getGrowingSystemTypeAgriculture(),
                    baseBean.getDephyNb(),
                    baseBean.getGrowingPlanName(),
                    baseBean.getDomainName(),
                    baseBean.getCampaign());
        }

        public static <T extends Bean> void columns(ExportModel<T> m) {
            m.newColumn("Nom du système synthétisé", Bean::getPracticedSystemName);
            m.newColumn("Série de campagnes agricoles", Bean::getCampaigns);
            baseColumnsWithoutCampaign(m);
        }
    }

    @Getter
    @Setter
    public static class MainBean extends Bean {
        protected PracticedSystemSource source;

        public MainBean(PracticedBean baseBean) {
            super(baseBean);
        }
    }

    public static class MainModel extends ExportModel<MainBean> {
        @Override
        public String getTitle() {
            return "Généralités";
        }

        public MainModel() {
            PracticedBean.columns(this);

            newColumn("Source", PracticedSystemSource.class, MainBean::getSource);
            newColumn("Commentaire", MainBean::getComment);
        }
    }

    @Getter
    @Setter
    public static class SeasonalCropCycleBean extends PracticedBean {

        String label;
        boolean endCycle;
        boolean sameCampaignAsPreviousNode;
        Double initNodeFrequency;
        String previousCrops;
        Double croppingPlanEntryFrequency;
        String intermediateCropName;
        int x;

        public SeasonalCropCycleBean(PracticedBean baseBean) {
            super(baseBean);
        }
    }

    public static class SeasonalCropCycleBeanModel extends ExportModel<SeasonalCropCycleBean> {
        @Override
        public String getTitle() {
            return "Cycles de cultures assolées";
        }

        public SeasonalCropCycleBeanModel() {
            PracticedBean.columns(this);

            // nodes
            newColumn("Culture", SeasonalCropCycleBean::getLabel);
            newColumn("Fin de cycle", SeasonalCropCycleBean::isEndCycle);
            newColumn("Même campagne agricole", SeasonalCropCycleBean::isSameCampaignAsPreviousNode);
            newColumn("Fréquence initiale de la culture", SeasonalCropCycleBean::getInitNodeFrequency);// for first node

            // connections
            //   label same field name as node
            newColumn("Culture précédente", SeasonalCropCycleBean::getPreviousCrops);
            newColumn("Fréquence", SeasonalCropCycleBean::getCroppingPlanEntryFrequency);
            newColumn("Culture intermédiaire", SeasonalCropCycleBean::getIntermediateCropName);// need to call service

            // nodes
            newColumn("Rang", SeasonalCropCycleBean::getX);
        }
    }

    @Getter
    @Setter
    public static class PerennialCropCycleBean extends PracticedBean {

        String label;
        CropCyclePhaseType phaseType;
        Integer phaseDuration;
        Integer plantingYear;
        Integer plantingInterFurrow;
        Integer plantingSpacing;
        Double plantingDensity;
        OrchardFrutalForm orchardFrutalForm;
        VineFrutalForm vineFrutalForm;
        String orientation;
        Double plantingDeathRate;
        Integer plantingDeathRateMeasureYear;
        WeedType weedType;
        boolean pollinator;
        Double pollinatorPercent;
        PollinatorSpreadMode pollinatorSpreadMode;
        String otherCharacteristics;
        Double solOccupationPercent;

        public PerennialCropCycleBean(PracticedBean baseBean) {
            super(baseBean);
        }
    }

    public static class PerennialCropCycleBeanModel extends ExportModel<PerennialCropCycleBean> {
        @Override
        public String getTitle() {
            return "Cycles de cultures perennes";
        }

        public PerennialCropCycleBeanModel() {
            PracticedBean.columns(this);

            newColumn("Culture", PerennialCropCycleBean::getLabel);
            newColumn("% dans le SdC", PerennialCropCycleBean::getSolOccupationPercent);
            newColumn("Phase", CropCyclePhaseType.class, PerennialCropCycleBean::getPhaseType);
            newColumn("Durée de la phase", PerennialCropCycleBean::getPhaseDuration);
            newColumn("Année de plantation", PerennialCropCycleBean::getPlantingYear);
            newColumn("Inter-rang de plantation", PerennialCropCycleBean::getPlantingInterFurrow);
            newColumn("Espacement de plantation sur le rang", PerennialCropCycleBean::getPlantingSpacing);
            newColumn("Densité de plantation", PerennialCropCycleBean::getPlantingDensity);
            newColumn("Forme fruitière vergers", OrchardFrutalForm.class, PerennialCropCycleBean::getOrchardFrutalForm);
            newColumn("Forme fruitière vigne", VineFrutalForm.class, PerennialCropCycleBean::getVineFrutalForm);
            newColumn("Orientation des rangs", PerennialCropCycleBean::getOrientation);
            newColumn("Taux de mortalité dans la plantation", PerennialCropCycleBean::getPlantingDeathRate);
            newColumn("Année de mesure de ce taux de mortalité", PerennialCropCycleBean::getPlantingDeathRateMeasureYear);
            newColumn("Type d'enherbement", WeedType.class, PerennialCropCycleBean::getWeedType);
            newColumn("Pollinisateurs", PerennialCropCycleBean::isPollinator);
            newColumn("% de pollinisateur", PerennialCropCycleBean::getPollinatorPercent);
            newColumn("Mode de répartition des pollinisateurs", PollinatorSpreadMode.class, PerennialCropCycleBean::getPollinatorSpreadMode);
            newColumn("Autres caractéristiques du couvert végétal", PerennialCropCycleBean::getOtherCharacteristics);
        }
    }

    public static class PerennialCropCycleSpeciesBeanModel extends ExportModel<PerennialCropCycleSpeciesBean> {
        @Override
        public String getTitle() {
                return "Espèces des phases de production des cultures pérennes";
        }

        public PerennialCropCycleSpeciesBeanModel() {
            PracticedBean.columns(this);
            PerennialCropCycleSpeciesBean.columnsWithProfilVegetatifBBCH(this);
        }
    }



    @Getter
    @Setter
    public static class ItkBean extends Bean {
        double temporalFrequency;

        public ItkBean(Bean baseBean) {
            super(baseBean);
        }

        public ItkBean(ItkBean itkBean) {
            super(itkBean);
            this.temporalFrequency = itkBean.temporalFrequency;
        }
    }

    public static class ITKModel extends ExportModel<ItkBean> {

        @Override
        public String getTitle() {
            return "Interventions";
        }
        public ITKModel() {
            PracticedBean.columns(this);
            ExportMetadata.interventionColumns(this);
            newColumn("Fréquence temporelle", ItkBean::getTemporalFrequency);
            ExportMetadata.interventionDetailsColumns(this);
            ExportMetadata.actionColumns(this);
        }

    }

    public static class ItkActionApplicationProduitsMinerauxModel extends ExportModel<ItkActionApplicationProduitsMinerauxBean> {

        @Override
        public String getTitle() {
                return "Application de produits minéraux";
        }

        public ItkActionApplicationProduitsMinerauxModel() {
            PracticedBean.columns(this);
            ItkActionApplicationProduitsMinerauxBean.columns(this);
        }
    }

    public static class ItkActionApplicationProduitsPhytoModel extends ExportModel<ItkApplicationProduitsPhytoBean> {

        @Override
        public String getTitle() {
            return "Application de phyto avec AMM";
        }

        public ItkActionApplicationProduitsPhytoModel() {
            PracticedBean.columns(this);
            ItkApplicationProduitsPhytoBean.columns(this);
        }
    }

    public static class ItkActionAutreModel extends ExportModel<ItkActionAutreBean> {

        @Override
        public String getTitle() {
            return "Autre";
        }

        public ItkActionAutreModel() {
            PracticedBean.columns(this);
            ItkActionAutreBean.columns(this);
        }
    }

    public static class ItkActionEntretienTailleVigneModel extends ExportModel<ItkActionEntretienTailleVigneBean> {

        @Override
        public String getTitle() {
            return "Entretien-Taille de vigne et verger";
        }

        public ItkActionEntretienTailleVigneModel() {
            PracticedBean.columns(this);
            ItkActionEntretienTailleVigneBean.columns(this);
        }
    }

    public static class ItkActionEpandageOrganiqueModel extends ExportModel<ItkActionEpandageOrganiqueBean> {

        @Override
        public String getTitle() {
            return "Épandage organique";
        }

        public ItkActionEpandageOrganiqueModel() {
            PracticedBean.columns(this);
            ItkActionEpandageOrganiqueBean.columns(this);
        }
    }

    public static class ItkActionIrrigationModel extends ExportModel<ItkActionIrrigationBean> {

        @Override
        public String getTitle() {
            return "Irrigation";
        }

        public ItkActionIrrigationModel() {
            PracticedBean.columns(this);
            ItkActionIrrigationBean.columns(this);
        }
    }

    public static class ItkActionLutteBiologiqueModel extends ExportModel<ItkActionLutteBiologiqueBean> {

        @Override
        public String getTitle() {
            return "Application de phyto sans AMM";
        }

        public ItkActionLutteBiologiqueModel() {
            PracticedBean.columns(this);
            ItkActionLutteBiologiqueBean.columns(this);
        }
    }

    public static class ItkActionRecolteModel extends ExportModel<ItkActionRecolteBean> {

        @Override
        public String getTitle() {
            return "Récolte";
        }

        public ItkActionRecolteModel() {
            PracticedBean.columns(this);
            ItkActionRecolteBean.columns(this);
        }
    }

    public static class ItkActionSemiModel extends ExportModel<ItkActionSemiBean> {

        @Override
        public String getTitle() {
            return "Semis";
        }

        public ItkActionSemiModel() {
            PracticedBean.columns(this);
            ItkActionSemiBean.columns(this);
        }
    }

    public static class ItkActionTransportModel extends ExportModel<ItkActionTransportBean> {

        @Override
        public String getTitle() {
            return "Transport";
        }

        public ItkActionTransportModel() {
            PracticedBean.columns(this);
            ItkActionTransportBean.columns(this);
        }
    }

    public static class ItkActionTravailSolModel extends ExportModel<ItkActionTravailSolBean> {

        @Override
        public String getTitle() {
            return "Travail du sol";
        }

        public ItkActionTravailSolModel() {
            PracticedBean.columns(this);
            interventionColumns(this);
            ItkActionTravailSolBean.columns(this);
        }
    }
}

