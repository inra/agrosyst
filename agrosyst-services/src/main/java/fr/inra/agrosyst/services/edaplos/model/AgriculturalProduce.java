package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.edaplos.EdaplosConstants;
import org.apache.commons.lang3.EnumUtils;

import java.util.ArrayList;
import java.util.List;

public class AgriculturalProduce implements AgroEdiObject {

    public static final String PRODUIT_PRINCIPAL_RECOLTE = "ZJH";
    public static final String SOUS_PRODUIT_RECOLTE = "ZJI";
    
    /** Type de produit récolté - http://agroedieurope.fr/fr/referentiels/15.html */
    protected String typeCode;

    /** Code du produit récolté */
    protected String subordinateTypeCode;

    /** Nom du produit récolté. */
    protected String name;

    /** Quantité récoltée. */
    protected String valueMeasure;

    protected String valueMeasureUnitCode;

    /** Destination du produit récolté - http://agroedieurope.fr/fr/referentiels/17.html. */
    protected String useCode;

    protected String calculatedYieldMeasure;

    /** Rendement estimé de la récolte. */
    protected String estimatedYieldMeasure;

    protected String estimatedYieldMeasureUnitCode;

    /** Prix de vente. */
    protected String unitPrice;

    /** Caractéristiques techniques du produit récolté. */
    protected List<TechnicalCharacteristic> applicableTechnicalCharacteristics = new ArrayList<>();

    /** lot du produit. */
    protected String specifiedProduceBatch;

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getSubordinateTypeCode() {
        return subordinateTypeCode;
    }

    public void setSubordinateTypeCode(String subordinateTypeCode) {
        this.subordinateTypeCode = subordinateTypeCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValueMeasure() {
        return valueMeasure;
    }

    public EdaplosConstants.UnitCode getValueMeasureUnitCode() {
        return EnumUtils.getEnum(EdaplosConstants.UnitCode.class, valueMeasureUnitCode);
    }

    public void setValueMeasureUnitCode(String valueMeasureUnitCode) {
        this.valueMeasureUnitCode = valueMeasureUnitCode;
    }

    public EdaplosConstants.UnitCode getEstimatedYieldMeasureUnitCode() {
        return EnumUtils.getEnum(EdaplosConstants.UnitCode.class, estimatedYieldMeasureUnitCode);
    }

    public void setEstimatedYieldMeasureUnitCode(String estimatedYieldMeasureUnitCode) {
        this.estimatedYieldMeasureUnitCode = estimatedYieldMeasureUnitCode;
    }

    public void setValueMeasure(String valueMeasure) {
        this.valueMeasure = valueMeasure;
    }

    public String getUseCode() {
        return useCode;
    }

    public void setUseCode(String useCode) {
        this.useCode = useCode;
    }

    public String getCalculatedYieldMeasure() {
        return calculatedYieldMeasure;
    }

    public void setCalculatedYieldMeasure(String calculatedYieldMeasure) {
        this.calculatedYieldMeasure = calculatedYieldMeasure;
    }

    public String getEstimatedYieldMeasure() {
        return estimatedYieldMeasure;
    }

    public void setEstimatedYieldMeasure(String estimatedYieldMeasure) {
        this.estimatedYieldMeasure = estimatedYieldMeasure;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public List<TechnicalCharacteristic> getApplicableTechnicalCharacteristics() {
        return applicableTechnicalCharacteristics;
    }

    public void setApplicableTechnicalCharacteristics(List<TechnicalCharacteristic> applicableTechnicalCharacteristics) {
        this.applicableTechnicalCharacteristics = applicableTechnicalCharacteristics;
    }

    public void addApplicableTechnicalCharacteristic(TechnicalCharacteristic applicableTechnicalCharacteristic) {
        this.applicableTechnicalCharacteristics.add(applicableTechnicalCharacteristic);
    }

    public String getSpecifiedProduceBatch() {
        return specifiedProduceBatch;
    }

    public void setSpecifiedProduceBatch(String specifiedProduceBatch) {
        this.specifiedProduceBatch = specifiedProduceBatch;
    }

    @Override
    public String getLocalizedIdentifier() {
        return "récolte '" + typeCode + "'";
    }

}
