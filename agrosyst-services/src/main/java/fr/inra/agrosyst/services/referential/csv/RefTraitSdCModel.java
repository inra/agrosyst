package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.GrowingSystemCharacteristicType;
import fr.inra.agrosyst.api.entities.referential.RefTraitSdC;
import fr.inra.agrosyst.api.entities.referential.RefTraitSdCImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

/**
 * @author Estelle Ancelet
 */
public class RefTraitSdCModel extends AbstractAgrosystModel<RefTraitSdC> implements ExportModel<RefTraitSdC> {

    protected static final ValueParser<GrowingSystemCharacteristicType> GROWING_SYSTEM_CHARACTERISTIC_TYPE_PARSER = value -> {
        GrowingSystemCharacteristicType result = null;
        if (!Strings.isNullOrEmpty(value)) {
            result = GrowingSystemCharacteristicType.valueOf(value);
        }
        return result;
    };
    
    protected static final ValueFormatter<GrowingSystemCharacteristicType> GROWING_SYSTEM_CHARACTERISTIC_TYPE_FORMATTER = Enum::name;

    public RefTraitSdCModel() {
        super(CSV_SEPARATOR);
        //id_trait;type_trait_sdc;nom_trait
        newMandatoryColumn("id_trait", RefTraitSdC.PROPERTY_ID_TRAIT, INT_PARSER);
        newMandatoryColumn("type_trait_sdc", RefTraitSdC.PROPERTY_TYPE_TRAIT_SDC, GROWING_SYSTEM_CHARACTERISTIC_TYPE_PARSER); 
        newMandatoryColumn("nom_trait", RefTraitSdC.PROPERTY_NOM_TRAIT);
        newOptionalColumn(COLUMN_ACTIVE, RefTraitSdC.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefTraitSdC, Object>> getColumnsForExport() {
        ModelBuilder<RefTraitSdC> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("id_trait", RefTraitSdC.PROPERTY_ID_TRAIT, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("type_trait_sdc", RefTraitSdC.PROPERTY_TYPE_TRAIT_SDC, GROWING_SYSTEM_CHARACTERISTIC_TYPE_FORMATTER);
        modelBuilder.newColumnForExport("nom_trait", RefTraitSdC.PROPERTY_NOM_TRAIT);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefTraitSdC.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefTraitSdC newEmptyInstance() {
        RefTraitSdC refTraitSdC = new RefTraitSdCImpl();
        refTraitSdC.setActive(true);
        return refTraitSdC;
    }
}
