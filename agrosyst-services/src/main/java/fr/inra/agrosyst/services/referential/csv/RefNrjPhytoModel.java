package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefNrjPhyto;
import fr.inra.agrosyst.api.entities.referential.RefNrjPhytoImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Intitulé;NRJ (MJ/kg);Source
 * 
 * @author Eric Chatellier
 */
public class RefNrjPhytoModel extends AbstractAgrosystModel<RefNrjPhyto> implements ExportModel<RefNrjPhyto> {

    public RefNrjPhytoModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("Intitulé", RefNrjPhyto.PROPERTY_INTITULE);
        newMandatoryColumn("NRJ (MJ/kg)", RefNrjPhyto.PROPERTY_NRJ, DOUBLE_PARSER);
        newMandatoryColumn("Source", RefNrjPhyto.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefNrjPhyto.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefNrjPhyto, Object>> getColumnsForExport() {
        ModelBuilder<RefNrjPhyto> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Intitulé", RefNrjPhyto.PROPERTY_INTITULE);
        modelBuilder.newColumnForExport("NRJ (MJ/kg)", RefNrjPhyto.PROPERTY_NRJ, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefNrjPhyto.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefNrjPhyto.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefNrjPhyto newEmptyInstance() {
        RefNrjPhyto refNrjPhyto = new RefNrjPhytoImpl();
        refNrjPhyto.setActive(true);
        return refNrjPhyto;
    }
}
