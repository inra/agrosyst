package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefGesPhyto;
import fr.inra.agrosyst.api.entities.referential.RefGesPhytoImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Intitulé;GES (kq eq-CO2/kg);Source
 * 
 * @author Eric Chatellier
 */
public class RefGesPhytoModel extends AbstractAgrosystModel<RefGesPhyto> implements ExportModel<RefGesPhyto> {

    public RefGesPhytoModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("Intitulé", RefGesPhyto.PROPERTY_INTITULE);
        newMandatoryColumn("GES (kq eq-CO2/kg)", RefGesPhyto.PROPERTY_GES, DOUBLE_PARSER);
        newMandatoryColumn("Source", RefGesPhyto.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefGesPhyto.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefGesPhyto, Object>> getColumnsForExport() {
        ModelBuilder<RefGesPhyto> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Intitulé", RefGesPhyto.PROPERTY_INTITULE);
        modelBuilder.newColumnForExport("GES (kq eq-CO2/kg)", RefGesPhyto.PROPERTY_GES, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefGesPhyto.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefGesPhyto.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefGesPhyto newEmptyInstance() {
        RefGesPhyto refGesPhyto = new RefGesPhytoImpl();
        refGesPhyto.setActive(true);
        return refGesPhyto;
    }
}
