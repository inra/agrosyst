package fr.inra.agrosyst.services.performance;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Getter
@Setter
public class MissingFieldMessage {
    
    protected List<MissingMessageScope> scopes;
    protected String message;
    
    public MissingFieldMessage(String message, MissingMessageScope... scopes) {
        this.scopes = Arrays.asList(scopes);
        this.message = message;
    }
    
    public Optional<String> getMessageForScope(MissingMessageScope targetedScope) {
        return scopes.contains(targetedScope) ? Optional.of(message) : Optional.empty();
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MissingFieldMessage that = (MissingFieldMessage) o;
        return message.equals(that.message);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(message);
    }
}
