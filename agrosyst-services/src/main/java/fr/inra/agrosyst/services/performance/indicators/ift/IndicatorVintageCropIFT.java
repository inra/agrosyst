package fr.inra.agrosyst.services.performance.indicators.ift;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.nuiton.i18n.I18n.l;

/**
 * IFT à la culture millésimé
 * Pour chaque intrant de type ‘Phytosanitaire’
 * • L’IFT à la culture millésimé ne peut théoriquement pas être calculé pour les campagnes antérieures à 2015. Donc si l’intervention est rattachée à un Domaine d’une campagne (strictement) antérieure à 2015, l’IFT à la cible millésimé récupèrera une dose de référence de 2015 ou plus tard.
 * Pour les campagnes 2015 et suivante, procéder de la façon suivante :
 * • Repérer les espèces cultivées de la culture concernée (le plus souvent il n’y en a qu’une, mais il peut y en avoir plusieurs)
 * • Sur la base de la table de correspondance Correspondance_CulturesAgrosyst_CulturesMAA, établir la liste des espèces MAA.
 * • Dans le référentiel MAA_Doses_Ref_par_GroupeCibles, Repérer les Dose_Ref correspondant au quadruplet Campagne|code_AMM|CultureMAA|Traitement_MAA.
 * Retenir la dose de référence la plus petite comme dose de référence (Dose_REF) pour ce traitement
 * Campagne correspond à la campagne rattachée au domaine
 * Si on ne trouve pas le quadruplet Campagne|code_AMM|CultureMAA| Traitement_MAA. dans MAA_Doses_Ref_par_GroupeCibles, alors :
 * reproduire cette dernière étape sur les campagnes précédentes, puis suivantes
 * •
 * ◦ Si on ne trouve pas le quadruplet Campagne|code_AMM|CultureMAA| Traitement_MAA dans MAA_Doses_Ref_par_GroupeCibles, alors :
 * IFT_Culture_Millesimé = PSCI_PHYTO * 1
 * <p>
 * • Si le produit appliqué est identifié comme Biocontrôle = ‘O’ dans la table MAA_Biocontrôle pour la campagne du domaine lié,
 * alors tous les IFT ift_Culture_Millesimé_total, ift_Culture_Millesimé_tot_hts, ift_Culture_Millesimé_h, ift_Culture_Millesimé_f, ift_Culture_Millesimé_i, ift_Culture_Millesimé_ts, ift_Culture_Millesimé_a, ift_Culture_Millesimé_hh,
 * ci-dessus prennent la valeur 0, et l’IFT_Culture calculé alimente l’IFT_Culture_Biocontrôle, (comme pour le calcul de l’IFT « à l’ancienne »).
 */
public class IndicatorVintageCropIFT extends IndicatorRefMaxYearCropIFT {

    protected static final String[] FIELDS = new String[]{
            "ift_a_la_culture_mil_ift_chimique_total",            // 0
            "ift_a_la_culture_mil_ift_chimique_tot_hts",          // 1
            "ift_a_la_culture_mil_ift_h",                         // 2
            "ift_a_la_culture_mil_ift_f",                         // 3
            "ift_a_la_culture_mil_ift_i",                         // 4
            "ift_a_la_culture_mil_ift_ts",                        // 5
            "ift_a_la_culture_mil_ift_a",                         // 6
            "ift_a_la_culture_mil_ift_hh",                        // 7
            "ift_a_la_culture_mil_ift_biocontrole",               // 8
    };

    protected static final String[] LABELS = new String[]{
            "Indicator.label.ift_a_la_culture_mil_ift_chimique_total",            // 0 IFT phytosanitaire chimique (ex total, refs #10576)
            "Indicator.label.ift_a_la_culture_mil_ift_chimique_tot_hts",          // 1 IFT phytosanitaire chimique total hors traitement de semence
            "Indicator.label.ift_a_la_culture_mil_ift_h",                         // 2 IFT herbicide
            "Indicator.label.ift_a_la_culture_mil_ift_f",                         // 3 IFT fongicide
            "Indicator.label.ift_a_la_culture_mil_ift_i",                         // 4 IFT insecticide
            "Indicator.label.ift_a_la_culture_mil_ift_ts",                        // 5 IFT traitement de semence
            "Indicator.label.ift_a_la_culture_mil_ift_a",                         // 6 IFT phytosanitaire autres
            "Indicator.label.ift_a_la_culture_mil_ift_hh",                        // 7 IFT phytosanitaire hors herbicide
            "Indicator.label.ift_a_la_culture_mil_ift_biocontrole",               // 8 IFT vert
    };

    /**
     * Constructeur initialisant la map des champs optionnels
     */
    public IndicatorVintageCropIFT() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, "ift_a_la_culture_mil_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, "ift_a_la_culture_mil_detail_champs_non_renseig");
        extraFields.put(OptionalExtraColumnEnumKey.DOSE_REFERENCE, "ift_a_la_culture_mil_dose_reference");
        extraFields.put(OptionalExtraColumnEnumKey.DOSE_REFERENCE_UNIT, "ift_a_la_culture_mil_dose_reference_unite");
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_VINTAGE_CROP_IFT);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {

        HashMap<String, String> indicatorNameToColumnName = new HashMap<>();
        for (Map.Entry<OptionalExtraColumnEnumKey, String> extraField : extraFields.entrySet()) {
            indicatorNameToColumnName.put(indicatorName + "_" + extraField.getKey(), extraField.getValue());
        }

        for (int i = 0; i < LABELS.length; i++) {
            String indicatorName = getIndicatorLabel(i);
            String columnName = FIELDS[i];
            indicatorNameToColumnName.put(indicatorName, columnName);
        }
        return indicatorNameToColumnName;
    }

    @Override
    protected Integer getStudiedCampaign(ComputeInputIftContext computeContext, List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA) {
        return computeContext.getDomain().getCampaign();
    }

    @Override
    protected Optional<RefMAADosesRefParGroupeCible> getRefMAADosesRefParGroupeCible(
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
            AbstractPhytoProductInputUsage phytoProductInputUsage,
            int domainCampaign) {

        Optional<RefMAADosesRefParGroupeCible> refMAADosesRefParGroupeCible = getRefMAADosesRefForTraitment(
                doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
                domainCampaign);

        // Si on ne trouve pas le quadruplet
        // Campagne|code_AMM|CultureMAA|Traitement_MAA.
        // alors: reproduire cette dernière étape sur les campagnes précédentes, puis suivantes
        int failDownMinCampaign = domainCampaign - 1;
        while (refMAADosesRefParGroupeCible.isEmpty() && failDownMinCampaign >= IndicatorVintageTargetIFT.MINIMAL_CAMPAIGN) {
            int finalFailDownMinCampaign = failDownMinCampaign;
            refMAADosesRefParGroupeCible = getRefMAADosesRefForTraitment(
                    doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
                    finalFailDownMinCampaign);
            failDownMinCampaign--;
        }

        if (refMAADosesRefParGroupeCible.isEmpty()) {
            final Integer MAX_CAMPAIGN = getRefDoseMaxCampaign(doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);
            int failDownMaxCampaign = domainCampaign + 1;
            while (refMAADosesRefParGroupeCible.isEmpty() && failDownMaxCampaign <= MAX_CAMPAIGN) {
                int finalFailDownMaxCampaign = failDownMaxCampaign;
                refMAADosesRefParGroupeCible = getRefMAADosesRefForTraitment(
                        doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
                        finalFailDownMaxCampaign);
                failDownMaxCampaign++;
            }
        }
        return refMAADosesRefParGroupeCible;
    }

    protected Optional<RefMAADosesRefParGroupeCible> getRefMAADosesRefForTraitment(
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
            Integer domainCampaign) {

        if (CollectionUtils.isEmpty(doseRefsFor_CodeAMM_CultureMAA_TraitementMAA)) return Optional.empty();

        Comparator<RefMAADosesRefParGroupeCible> minComparator =
                Comparator.comparing(d -> Optional.ofNullable(d.getDose_ref_maa()).orElse(Double.MAX_VALUE));


        // Si on ne trouve pas le quadruplet code_AMM|CultureMAA|GroupesCiblesMAA| Traitement_MAA dans MAA_Doses_Ref_par_GroupeCibles,
        // alors rechercher si on trouve le quadruplet
        // ==> Campagne|code_AMM|CultureMAA| Traitement_MAA,
        // retenir comme Dose_Ref la plus petite des valeurs trouvées.
        Optional<RefMAADosesRefParGroupeCible> optionalDoseRef = doseRefsFor_CodeAMM_CultureMAA_TraitementMAA.stream()
                .filter(dose -> domainCampaign.equals(dose.getCampagne()))
                .min(minComparator);

        return optionalDoseRef;
    }

}
