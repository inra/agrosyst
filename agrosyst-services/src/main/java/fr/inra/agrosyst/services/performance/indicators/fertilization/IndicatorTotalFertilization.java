package fr.inra.agrosyst.services.performance.indicators.fertilization;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.performance.TotalFertilization;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.nuiton.i18n.I18n.l;

/**
 * Calcul de la quantité totale (organique et minérale) de substances fertilisantes.
 *
 * Pour l'instant, ce n'est développé que pour : N, P, K.
 *
 * Il s'agit simplement de la somme des quantités organiques et minérales pour une substance donnée.
 * On stocke dans le contexte de l'intervention les quantités organiques et les quantités minérales
 * par intrant dans {@link  IndicatorOrganicFertilization} et {@link  IndicatorMineralFertilization}
 * ce qui permet ici de faire le calcul. On doit aussi noter la quantité totale au niveau de l'intrant,
 * ce qui sera toujours égal à la quantité organique ou à la quantité minérale, selon si l'intrant est
 * de nature organique ou minérale. L'agrégation se fait à proprement parler aux échelles supérieures.
 */
public class IndicatorTotalFertilization extends AbstractIndicatorFertilization {

    private static final List<String> FIELDS = List.of(
            "ferti_n_tot",
            "ferti_p2o5_tot",
            "ferti_k2o_tot"
    );

    private boolean[] substancesToDisplay = new boolean[] {
            true, // N
            true, // P2O5
            true  // K2O
    };

    public IndicatorTotalFertilization() {
        // labels
        super(new String[] {
                "Indicator.label.totalFertilization.N",
                "Indicator.label.totalFertilization.P2O5",
                "Indicator.label.totalFertilization.K2O"
        });
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();

        for (int i = 0; i < this.labels.length; i++) {
            String indicatorName = getIndicatorLabel(i);
            String columnName = FIELDS.get(i);
            indicatorNameToColumnName.put(indicatorName, columnName);
        }
        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext, PerformanceGlobalExecutionContext globalExecutionContext, PerformancePracticedDomainExecutionContext domainContext, PerformanceGrowingSystemExecutionContext growingSystemContext, PerformancePracticedSystemExecutionContext practicedSystemContext, PerformancePracticedCropExecutionContext cropContext, PerformancePracticedInterventionExecutionContext interventionContext, PracticedCropCyclePhase phase) {
        if (interventionContext.isFictive()) {
            return null;
        }

        Optional<Double[]> optionalResult = this.computeTotalFertilizationAmounts(writerContext, interventionContext);
        return optionalResult.orElse(null);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext, PerformanceGlobalExecutionContext globalExecutionContext, PerformanceEffectiveDomainExecutionContext domainContext, PerformanceZoneExecutionContext zoneContext, PerformanceEffectiveCropExecutionContext cropContext, PerformanceEffectiveInterventionExecutionContext interventionContext) {
        Optional<Double[]> optionalResult = this.computeTotalFertilizationAmounts(writerContext, interventionContext);
        return optionalResult.orElse(null);
    }

    private Optional<Double[]> computeTotalFertilizationAmounts(WriterContext writerContext, PerformanceInterventionContext interventionContext) {
        // Dans les calculs suivants, on ne conserve que les 3 premiers éléments des tableaux, qui correspondent
        // respectivement aux quantités de N, P et K.

        AtomicBoolean computeDoneFlag = new AtomicBoolean(false);

        Double[] results = newArray(labels.length, 0.0);

        final Map<Pair<AbstractAction, AbstractInputUsage>, Double[]> mineralFertilizationAmountsByInput = interventionContext.getMineralFertilizationAmountsByInput();

        if (mineralFertilizationAmountsByInput != null) {
            for (Pair<AbstractAction, AbstractInputUsage> actionAndInputUsage : mineralFertilizationAmountsByInput.keySet()) {
                final Double[] amounts = mineralFertilizationAmountsByInput.get(actionAndInputUsage);

                final AbstractInputUsage inputUsage = actionAndInputUsage.getRight();
                final AbstractAction action = actionAndInputUsage.getLeft();
                this.writeInputUsageForSubstance(writerContext, inputUsage, action, amounts[0], 0); // N
                this.writeInputUsageForSubstance(writerContext, inputUsage, action, amounts[1], 1); // P2O5
                this.writeInputUsageForSubstance(writerContext, inputUsage, action, amounts[2], 2); // K2O

                results = sum(results, amounts);
                computeDoneFlag.set(true);
            }
        }

        final Map<Pair<AbstractAction, AbstractInputUsage>, Double[]> organicFertilizationAmountsByInput = interventionContext.getOrganicFertilizationAmountsByInput();
        if (organicFertilizationAmountsByInput != null) {
            for (Pair<AbstractAction, AbstractInputUsage> actionAndInputUsage : organicFertilizationAmountsByInput.keySet()) {
                final Double[] amounts = organicFertilizationAmountsByInput.get(actionAndInputUsage);

                final AbstractInputUsage inputUsage = actionAndInputUsage.getRight();
                final AbstractAction action = actionAndInputUsage.getLeft();
                this.writeInputUsageForSubstance(writerContext, inputUsage, action, amounts[0], 0); // N
                this.writeInputUsageForSubstance(writerContext, inputUsage, action, amounts[1], 1); // P2O5
                this.writeInputUsageForSubstance(writerContext, inputUsage, action, amounts[2], 2); // K2O

                results = sum(results, amounts);
                computeDoneFlag.set(true);
            }
        }

        return computeDoneFlag.get() ? Optional.of(results) : Optional.empty();
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, "Indicator.category.totalFertilization");
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return isRelevant(atLevel) && this.substancesToDisplay[i];
    }

    public void init(Collection<TotalFertilization> totalFertilizations) {
        this.substancesToDisplay = new boolean[] {
                false, // N
                false, // P2O5
                false  // K2O
        };

        for (TotalFertilization totalFertilization : totalFertilizations) {
            switch (totalFertilization) {
                case N -> substancesToDisplay[0] = true;
                case P2O5 -> substancesToDisplay[1] = true;
                case K2O -> substancesToDisplay[2] = true;
                default -> {}
            }
        }
    }
}
