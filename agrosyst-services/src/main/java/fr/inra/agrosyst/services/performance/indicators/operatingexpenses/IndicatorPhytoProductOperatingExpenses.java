package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.UsagePerformanceResult;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

/**
 * Les charges opérationnelles réelles sont exprimées en €/ha.
 * Elles correspondent aux dépenses liées à l’achat des intrants (semences et plants,
 * produits fertilisants minéraux  et  organiques, traitement de semences et plants,
 * irrigation, produits phytosanitaires, produits de lutte biologique, autres intrants, substrats, pots).
 * <p>
 * Le calcul se fait sur la base des prix saisis par l’utilisateur.
 * <p>
 * Les interventions concernées par cet indicateur sont toutes les interventions
 * contenant un intrant et/ou une action de type « Semis » et/ou une action de
 * type « Irrigation ».
 * <p>
 *
 * <p>
 * Cette classe est une composante de {@link IndicatorOperatingExpenses} chargée de calculer les charges relatives au semis.
 * </p>
 * Rappel de la formule globale de calcul:
 *
 * <pre>
 * CI réel_i:
 *
 * = PSCi * ( sum(Q_ev * PA_ev) + sum(Q_j * PA_j) + sum(Q_e * PA_e) + sum(Q_a * PA_a)) + PSCi_phyto * sum(Q_k * PA_k) + PSCi_luttebio * sum(Q_h * PA_h)
 *
 * Avec :
 * - CI réel_i : charges intrants de l’intervention i
 * - PSCi (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i.
 *   PSCi est calculé sur la base des données saisies par l’utilisateur.
 *
 * - Phytosanitaire
 *   - PSCi_phyto (sans unité) : proportion de surface concernée par l’action de type application de produit phytosanitaire.
 *     PSCi_phyto (PSCi * Proportion de surface traitée)
 *   - Q_k (diverses unités) : quantité de l’intrant k, k appartenant à la liste des intrants de type  « Phytosanitaire »
 *     appliqués au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *   - PA_k (diverses unités) : prix d’achat de l ’intrant k, k appartenant à la liste des intrants de type « Phytosanitaire »
 *     appliqués au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *
 *
 * </pre>
 *
 * @author David Cossé (cosse@codelutin.com)
 */
public class IndicatorPhytoProductOperatingExpenses extends IndicatorInputProductOperatingExpenses {

    protected static final Log LOGGER = LogFactory.getLog(IndicatorPhytoProductOperatingExpenses.class);

    public IndicatorPhytoProductOperatingExpenses(
            boolean displayed,
            boolean computeReal,
            boolean computStandardised,
            Locale locale) {
        super(displayed, computeReal, computStandardised, locale);
    }

    @Override
    public Pair<Optional<InputPrice>, Optional<InputRefPrice>> getDomainInputPrice(AbstractInputUsage usage, RefCampaignsInputPricesByDomainInput refCampaignsInputPricesByDomainInputAndCampaigns) {
        final DomainPhytoProductInput input = ((AbstractPhytoProductInputUsage) usage).getDomainPhytoProductInput();
        final InputPrice inputPrice = input.getInputPrice();
        Optional<InputRefPrice> inputRePrice = refCampaignsInputPricesByDomainInputAndCampaigns.phytoRefPriceForInput().get(input);
        inputRePrice = inputRePrice == null ? Optional.empty() : inputRePrice;
        return Pair.of(Optional.ofNullable(inputPrice), inputRePrice);
    }

    // commons
    private Pair<Double, Double> computeOperatingExpenses(
            WriterContext writerContext,
            Optional<BiologicalControlAction> optionalBiologicalControlAction,
            Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction,
            RefCampaignsInputPricesByDomainInput inputRefPricesByDomainInput,
            String interventionId,
            double toolsPsci,
            Collection<RefInputUnitPriceUnitConverter> converters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels,
            Map<? extends AbstractInputUsage, UsagePerformanceResult> refVintageTargetIftDoseByUsages) {

        Pair<Double, Double> inputsCharges = IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;

        if (inputRefPricesByDomainInput == null) {
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            addMissingFieldMessage(
                    interventionId,
                    messageBuilder.getMissingInputUsagePriceMessage(
                            InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                            null,
                            null));
            return inputsCharges;
        }

        incrementAngGetTotalFieldCounterForTargetedId(interventionId);//treatedSurface (1 for all inputs)

        if (optionalBiologicalControlAction.isPresent()) {
            final BiologicalControlAction biologicalControlAction = optionalBiologicalControlAction.get();
            final Collection<BiologicalProductInputUsage> biologicalProductInputs = biologicalControlAction.getBiologicalProductInputUsages();

            Pair<Double, Double> actionCi = computeIndicatorForAction(
                    writerContext,
                    biologicalControlAction,
                    biologicalProductInputs,
                    inputRefPricesByDomainInput,
                    toolsPsci,
                    converters,
                    interventionId,
                    indicatorClass,
                    labels,
                    refVintageTargetIftDoseByUsages);
            Double realCi = inputsCharges.getLeft() + actionCi.getLeft();
            Double standardizedCi = inputsCharges.getRight() + actionCi.getRight();
            inputsCharges = Pair.of(realCi, standardizedCi);
        }

        if (optionalPesticidesSpreadingAction.isPresent()) {

            final PesticidesSpreadingAction pesticidesSpreadingAction = optionalPesticidesSpreadingAction.get();
            final Collection<PesticideProductInputUsage> pesticideProductInputs = pesticidesSpreadingAction.getPesticideProductInputUsages();

            Pair<Double, Double> actionCi = computeIndicatorForAction(
                    writerContext,
                    pesticidesSpreadingAction,
                    pesticideProductInputs,
                    inputRefPricesByDomainInput,
                    toolsPsci,
                    converters,
                    interventionId,
                    indicatorClass,
                    labels,
                    refVintageTargetIftDoseByUsages);
            Double realCi = inputsCharges.getLeft() + actionCi.getLeft();
            Double standardizedCi = inputsCharges.getRight() + actionCi.getRight();
            inputsCharges = Pair.of(realCi, standardizedCi);
        }

        inputsCharges = Pair.of(inputsCharges.getLeft(), inputsCharges.getRight());

        return inputsCharges;
    }

    /**
     * input must not be seeding product input
     */
    protected double getPhytoInputTreatedSurfaceFactor(AbstractAction abstractAction) {
        double result = 1d;
        if (abstractAction instanceof PesticidesSpreadingAction pesticidesSpreadingAction) {
            result = pesticidesSpreadingAction.getProportionOfTreatedSurface() / 100;

        } else if (abstractAction instanceof BiologicalControlAction biologicalControlAction) {
            result = biologicalControlAction.getProportionOfTreatedSurface() / 100;
        }
        return result;
    }

    // practiced
    public Double[] computeOperatingExpenses(
            WriterContext writerContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> converters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionContext.getOptionalBiologicalControlAction();
        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction();
        final Collection<BiologicalProductInputUsage> biologicalProductInputUsages = optionalBiologicalControlAction.isPresent() ? CollectionUtils.emptyIfNull(optionalBiologicalControlAction.get().getBiologicalProductInputUsages()) : new ArrayList<>();
        final Collection<PesticideProductInputUsage> pesticideProductInputUsages = optionalPesticidesSpreadingAction.isPresent() ? CollectionUtils.emptyIfNull(optionalPesticidesSpreadingAction.get().getPesticideProductInputUsages()) : new ArrayList<>();

        Collection<AbstractPhytoProductInputUsage> productInputUsages = new ArrayList<>(biologicalProductInputUsages);
        productInputUsages.addAll(pesticideProductInputUsages);

        return computeOperatingExpenses(
                writerContext,
                practicedSystemContext,
                interventionContext,
                converters,
                productInputUsages,
                indicatorClass,
                labels);
    }

    // effective
    public Double[] computeOperatingExpenses(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> converters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionContext.getOptionalBiologicalControlAction();
        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction();
        final Collection<BiologicalProductInputUsage> biologicalProductInputUsages = optionalBiologicalControlAction.isPresent() ? CollectionUtils.emptyIfNull(optionalBiologicalControlAction.get().getBiologicalProductInputUsages()) : new ArrayList<>();
        final Collection<PesticideProductInputUsage> pesticideProductInputUsages = optionalPesticidesSpreadingAction.isPresent() ? CollectionUtils.emptyIfNull(optionalPesticidesSpreadingAction.get().getPesticideProductInputUsages()) : new ArrayList<>();

        Collection<AbstractPhytoProductInputUsage> productInputUsages = new ArrayList<>(biologicalProductInputUsages);
        productInputUsages.addAll(pesticideProductInputUsages);

        return computeOperatingExpenses(
                writerContext,
                domainContext,
                interventionContext,
                zoneContext.getZone(),
                converters,
                productInputUsages,
                indicatorClass,
                labels);
    }

    public Double[] computeOperatingExpensesForPesticide(
            WriterContext writerContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> converters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction();
        final Collection<PesticideProductInputUsage> pesticideProductInputUsages = optionalPesticidesSpreadingAction.isPresent() ? CollectionUtils.emptyIfNull(optionalPesticidesSpreadingAction.get().getPesticideProductInputUsages()) : new ArrayList<>();

        return computeOperatingExpenses(
                writerContext,
                practicedSystemContext,
                interventionContext,
                converters,
                new ArrayList<>(pesticideProductInputUsages),
                indicatorClass,
                labels);
    }

    public Double[] computeOperatingExpensesForPesticide(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> converters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction();
        final Collection<PesticideProductInputUsage> pesticideProductInputUsages = optionalPesticidesSpreadingAction.isPresent() ? CollectionUtils.emptyIfNull(optionalPesticidesSpreadingAction.get().getPesticideProductInputUsages()) : new ArrayList<>();

        return computeOperatingExpenses(
                writerContext,
                domainContext,
                interventionContext,
                zoneContext.getZone(),
                converters,
                new ArrayList<>(pesticideProductInputUsages),
                indicatorClass,
                labels);
    }

    public Double[] computeOperatingExpensesForBiological(
            WriterContext writerContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> converters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionContext.getOptionalBiologicalControlAction();
        final Collection<BiologicalProductInputUsage> biologicalProductInputUsages = optionalBiologicalControlAction.isPresent() ? CollectionUtils.emptyIfNull(optionalBiologicalControlAction.get().getBiologicalProductInputUsages()) : new ArrayList<>();

        return computeOperatingExpenses(
                writerContext,
                practicedSystemContext,
                interventionContext,
                converters,
                new ArrayList<>(biologicalProductInputUsages),
                indicatorClass,
                labels);
    }

    public Double[] computeOperatingExpensesForBiological(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> converters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionContext.getOptionalBiologicalControlAction();
        final Collection<BiologicalProductInputUsage> biologicalProductInputUsages = optionalBiologicalControlAction.isPresent() ? CollectionUtils.emptyIfNull(optionalBiologicalControlAction.get().getBiologicalProductInputUsages()) : new ArrayList<>();

        return computeOperatingExpenses(
                writerContext,
                domainContext,
                interventionContext,
                zoneContext.getZone(),
                converters,
                new ArrayList<>(biologicalProductInputUsages),
                indicatorClass,
                labels);
    }

    // practiced
    private Double[] computeOperatingExpenses(
            WriterContext writerContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> converters,
            Collection<AbstractPhytoProductInputUsage> productInputUsages,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        long start = System.currentTimeMillis();

        // do not consider Seeding ones
        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionContext.getOptionalBiologicalControlAction();
        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction();
        if (optionalBiologicalControlAction.isEmpty() && optionalPesticidesSpreadingAction.isEmpty()) {
            return new Double[]{
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};
        }

        if (CollectionUtils.isEmpty(productInputUsages)) {
            // no phyto product cost
            return new Double[]{
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};
        }

        PracticedIntervention intervention = interventionContext.getIntervention();
        final String interventionId = intervention.getTopiaId();

        double toolsPsci = getToolPSCi(intervention);

        final RefCampaignsInputPricesByDomainInput inputRefPricesByDomainInput = practicedSystemContext.getPracticedSystemInputRefPricesByDomainInput();

        Pair<Double, Double> inputsCharges = computeOperatingExpenses(
                writerContext,
                optionalBiologicalControlAction,
                optionalPesticidesSpreadingAction,
                inputRefPricesByDomainInput,
                interventionId,
                toolsPsci,
                converters,
                indicatorClass,
                labels,
                interventionContext.getRefVintageTargetIftDoseByUsages()
        );

        Double[] result = {inputsCharges.getLeft(), inputsCharges.getRight()};

        if (LOGGER.isDebugEnabled()) {
            PracticedSystem practicedSystem = practicedSystemContext.getAnonymizePracticedSystem();
            PracticedIntervention practicedIntervention = interventionContext.getIntervention();
            LOGGER.debug("computePhytoProductInputIndicator intervention:" + practicedIntervention.getName() +
                    " (" + practicedIntervention.getTopiaId() + ")" +
                    ", practicedSystem:" + practicedSystem.getName() +
                    "(" + practicedSystem.getCampaigns() + " ) calculé en :"
                    + (System.currentTimeMillis() - start) + "ms");
        }

        return result;
    }

    // effective
    private Double[] computeOperatingExpenses(WriterContext writerContext,
                                              PerformanceEffectiveDomainExecutionContext domainExecutionContext,
                                              PerformanceEffectiveInterventionExecutionContext interventionContext,
                                              Zone zone,
                                              Collection<RefInputUnitPriceUnitConverter> converters,
                                              Collection<AbstractPhytoProductInputUsage> productInputUsages,
                                              Class<? extends GenericIndicator> indicatorClass,
                                              String[] labels) {
        long start = System.currentTimeMillis();

        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionContext.getOptionalBiologicalControlAction();
        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction();
        if (optionalBiologicalControlAction.isEmpty() && optionalPesticidesSpreadingAction.isEmpty()) {
            return new Double[]{
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};
        }

        if (CollectionUtils.isEmpty(productInputUsages)) {
            // no phyto product cost
            return new Double[]{
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};
        }

        EffectiveIntervention intervention = interventionContext.getIntervention();
        final String interventionId = intervention.getTopiaId();

        double toolsPsci = getToolPSCi(intervention);

        final RefCampaignsInputPricesByDomainInput inputRefPricesByDomainInput = domainExecutionContext.getRefInputPricesForCampaignsByInput();

        Pair<Double, Double> inputsCharges = computeOperatingExpenses(
                writerContext,
                optionalBiologicalControlAction,
                optionalPesticidesSpreadingAction,
                inputRefPricesByDomainInput,
                interventionId,
                toolsPsci,
                converters,
                indicatorClass,
                labels,
                interventionContext.getRefVintageTargetIftDoseByUsages());

        Double[] result = {inputsCharges.getLeft(), inputsCharges.getRight()};

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(
                    "computePhytoProductInputIndicator intervention:" +
                            intervention.getName() + " (" + intervention.getTopiaId() + ")" +
                            ", zone:" + zone.getTopiaId() +
                            " calculé en :" + (System.currentTimeMillis() - start));
        }

        return result;
    }

}
