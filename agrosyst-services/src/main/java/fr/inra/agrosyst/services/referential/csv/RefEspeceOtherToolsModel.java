package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefEspeceOtherTools;
import fr.inra.agrosyst.api.entities.referential.RefEspeceOtherToolsImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefEspeceOtherToolsModel extends AbstractAgrosystModel<RefEspeceOtherTools> implements ExportModel<RefEspeceOtherTools> {

    public RefEspeceOtherToolsModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("code espèce botanique autres outils", RefEspeceOtherTools.PROPERTY_CODE_ESPECE_BOTANIQUE_OTHER);
        newMandatoryColumn("libellé espèce botanique autres outils", RefEspeceOtherTools.PROPERTY_LIBELLE_ESPECE_BOTANIQUE_OTHER);
        newMandatoryColumn("code qualifiant AEE autres outils", RefEspeceOtherTools.PROPERTY_CODE_QUALIFIANT_AEEOTHER);
        newMandatoryColumn("libellé qualifiant AEE autres outils", RefEspeceOtherTools.PROPERTY_LIBELLE_QUALIFIANT_AEEOTHER);
        newMandatoryColumn("code type saisonnier AEE autres outils", RefEspeceOtherTools.PROPERTY_CODE_TYPE_SAISONNIER_AEEOTHER);
        newMandatoryColumn("libellé type saisonnier AEE autres outils", RefEspeceOtherTools.PROPERTY_LIBELLE_TYPE_SAISONNIER_AEEOTHER);

        newMandatoryColumn("code espèce botanique", RefEspeceOtherTools.PROPERTY_CODE_ESPECE_BOTANIQUE);
        newMandatoryColumn("libellé espèce botanique", RefEspeceOtherTools.PROPERTY_LIBELLE_ESPECE_BOTANIQUE);
        newMandatoryColumn("code qualifiant AEE", RefEspeceOtherTools.PROPERTY_CODE_QUALIFIANT_AEE);
        newMandatoryColumn("libellé qualifiant AEE", RefEspeceOtherTools.PROPERTY_LIBELLE_QUALIFIANT_AEE);
        newMandatoryColumn("code type saisonnier AEE", RefEspeceOtherTools.PROPERTY_CODE_TYPE_SAISONNIER_AEE);
        newMandatoryColumn("libellé type saisonnier AEE", RefEspeceOtherTools.PROPERTY_LIBELLE_TYPE_SAISONNIER_AEE);

        newOptionalColumn(COLUMN_ACTIVE, RefEspeceOtherTools.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefEspeceOtherTools, Object>> getColumnsForExport() {
        ModelBuilder<RefEspeceOtherTools> modelBuilder = new ModelBuilder<>();

        modelBuilder.newColumnForExport("code espèce botanique autres outils", RefEspeceOtherTools.PROPERTY_CODE_ESPECE_BOTANIQUE_OTHER);
        modelBuilder.newColumnForExport("libellé espèce botanique autres outils", RefEspeceOtherTools.PROPERTY_LIBELLE_ESPECE_BOTANIQUE_OTHER);
        modelBuilder.newColumnForExport("code qualifiant AEE autres outils", RefEspeceOtherTools.PROPERTY_CODE_QUALIFIANT_AEEOTHER);
        modelBuilder.newColumnForExport("libellé qualifiant AEE autres outils", RefEspeceOtherTools.PROPERTY_LIBELLE_QUALIFIANT_AEEOTHER);
        modelBuilder.newColumnForExport("code type saisonnier AEE autres outils", RefEspeceOtherTools.PROPERTY_CODE_TYPE_SAISONNIER_AEEOTHER);
        modelBuilder.newColumnForExport("libellé type saisonnier AEE autres outils", RefEspeceOtherTools.PROPERTY_LIBELLE_TYPE_SAISONNIER_AEEOTHER);

        modelBuilder.newColumnForExport("code espèce botanique", RefEspeceOtherTools.PROPERTY_CODE_ESPECE_BOTANIQUE);
        modelBuilder.newColumnForExport("libellé espèce botanique", RefEspeceOtherTools.PROPERTY_LIBELLE_ESPECE_BOTANIQUE);
        modelBuilder.newColumnForExport("code qualifiant AEE", RefEspeceOtherTools.PROPERTY_CODE_QUALIFIANT_AEE);
        modelBuilder.newColumnForExport("libellé qualifiant AEE", RefEspeceOtherTools.PROPERTY_LIBELLE_QUALIFIANT_AEE);
        modelBuilder.newColumnForExport("code type saisonnier AEE", RefEspeceOtherTools.PROPERTY_CODE_TYPE_SAISONNIER_AEE);
        modelBuilder.newColumnForExport("libellé type saisonnier AEE", RefEspeceOtherTools.PROPERTY_LIBELLE_TYPE_SAISONNIER_AEE);

        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefEspeceOtherTools.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefEspeceOtherTools newEmptyInstance() {
        RefEspeceOtherTools result = new RefEspeceOtherToolsImpl();
        result.setActive(true);
        return result;
    }
}
