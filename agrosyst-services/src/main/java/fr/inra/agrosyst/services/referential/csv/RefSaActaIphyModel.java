package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefSaActaIphy;
import fr.inra.agrosyst.api.entities.referential.RefSaActaIphyImpl;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRoot;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Référentiel SA_ACTA_IPHY.
 * 
 * nom_SA_ACTA;nom_SA_IPHY
 */
public class RefSaActaIphyModel extends AbstractAgrosystModel<RefSaActaIphy> implements ExportModel<RefSaActaIphy> {

    public RefSaActaIphyModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("nom_SA_ACTA", RefSaActaIphy.PROPERTY_NOM_SA_ACTA);
        newMandatoryColumn("nom_SA_IPHY", RefSaActaIphy.PROPERTY_NOM_SA_IPHY);
        newIgnoredColumn("Commentaires (NMJ_14/01/2014)");
        newOptionalColumn(COLUMN_ACTIVE, RefActaDosageSaRoot.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefSaActaIphy, Object>> getColumnsForExport() {
        ModelBuilder<RefSaActaIphy> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("nom_SA_ACTA", RefSaActaIphy.PROPERTY_NOM_SA_ACTA);
        modelBuilder.newColumnForExport("nom_SA_IPHY", RefSaActaIphy.PROPERTY_NOM_SA_IPHY);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefSaActaIphy.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefSaActaIphy newEmptyInstance() {
        RefSaActaIphy refSaActaIphy = new RefSaActaIphyImpl();
        refSaActaIphy.setActive(true);
        return refSaActaIphy;
    }
}
