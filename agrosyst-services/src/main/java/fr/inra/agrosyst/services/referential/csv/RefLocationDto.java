package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import lombok.Getter;
import lombok.Setter;

/**
 * DTO used to create the RefLocation referential.
 *
 * @author cosse
 */
@Getter
@Setter
public class RefLocationDto {

    protected String codeInsee;

    protected int region;

    protected String departement;

    protected String codeCommune;

    protected String articleCommune;

    protected String commune;

    protected String petiteRegionAgricoleCode;

    protected String petiteRegionAgricoleNom;

    protected String codePostal;

    protected double latitude;

    protected double longitude;

    protected String pays;

    protected Integer altitude_moy;

    protected String aire_attr;

    protected String arrondissement_code;

    protected String bassin_vie;

    protected String intercommunalite_code;

    protected String unite_urbaine;

    protected String zone_emploi;

    protected String bassin_viticole;

    protected String ancienne_region;

    protected boolean active;

    @Override
    public String toString() {
        String result = MoreObjects.toStringHelper(this)
                .add("codeInsee", codeInsee)
                .add("commune", commune)
                .add("codePostal", codePostal)
                .add("petiteRegionAgricoleCode", petiteRegionAgricoleCode)
                .add("petiteRegionAgricoleNom", petiteRegionAgricoleNom)
                .add("latitude", latitude)
                .add("longitude", longitude)
                .toString();
        return result;
    }
}
