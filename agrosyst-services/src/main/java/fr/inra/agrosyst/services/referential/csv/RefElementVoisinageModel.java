package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinageImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Element de voisinage.
 * 
 * id_ref_infrastructure serial NOT NULL,;  iae_nom text,;  iae_surface_equivalente real,;
 * iae_remarque text,;  p_dt_crea timestamp without time zone DEFAULT now(),;
 * p_dt_maj timestamp without time zone,;  iae_no_ordre integer,;  unite_surface character varying,;Source
 * 
 * @author Eric Chatellier
 */
public class RefElementVoisinageModel extends AbstractAgrosystModel<RefElementVoisinage> implements ExportModel<RefElementVoisinage> {

    // parse les dates de la forme 2010-02-22 11:31:41.145
    protected final DateTimeFormatter DF = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    protected final ValueParser<LocalDateTime> P_DATE_PARSER = value -> {
        LocalDateTime result = null;
        if (!Strings.isNullOrEmpty(value)) {
            result = LocalDateTime.parse(value, DF);
        }
        return result;
    };
    
    protected final ValueFormatter<LocalDateTime> P_DATE_FORMATTER = value -> {
        String result;
        if (value != null) {
            result = DF.format(value);
        } else {
            result = "";
        }
        return result;
    };

    public RefElementVoisinageModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("id_ref_infrastructure serial NOT NULL,", RefElementVoisinage.PROPERTY_ID_REF_INFRASTRUCTURE, INT_PARSER);
        newMandatoryColumn("iae_nom text,", RefElementVoisinage.PROPERTY_IAE_NOM);
        newMandatoryColumn("iae_surface_equivalente real,", RefElementVoisinage.PROPERTY_IAE_SURFACE_EQUIVALENTE, DOUBLE_PARSER);
        newMandatoryColumn("iae_remarque text,", RefElementVoisinage.PROPERTY_IAE_REMARQUE);
        newMandatoryColumn("p_dt_crea timestamp without time zone DEFAULT now(),", RefElementVoisinage.PROPERTY_P_DT_CREA, P_DATE_PARSER);
        newMandatoryColumn("p_dt_maj timestamp without time zone,", RefElementVoisinage.PROPERTY_P_DT_MAJ, P_DATE_PARSER);
        newMandatoryColumn("iae_no_ordre integer,", RefElementVoisinage.PROPERTY_IAE_NO_ORDRE, INT_PARSER);
        newMandatoryColumn("unite_surface character varying,", RefElementVoisinage.PROPERTY_UNITE_SURFACE);
        newMandatoryColumn("Source", RefElementVoisinage.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefElementVoisinage.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefElementVoisinage, Object>> getColumnsForExport() {
        ModelBuilder<RefElementVoisinage> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("id_ref_infrastructure serial NOT NULL,", RefElementVoisinage.PROPERTY_ID_REF_INFRASTRUCTURE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("iae_nom text,", RefElementVoisinage.PROPERTY_IAE_NOM);
        modelBuilder.newColumnForExport("iae_surface_equivalente real,", RefElementVoisinage.PROPERTY_IAE_SURFACE_EQUIVALENTE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("iae_remarque text,", RefElementVoisinage.PROPERTY_IAE_REMARQUE);
        modelBuilder.newColumnForExport("p_dt_crea timestamp without time zone DEFAULT now(),", RefElementVoisinage.PROPERTY_P_DT_CREA, P_DATE_FORMATTER);
        modelBuilder.newColumnForExport("p_dt_maj timestamp without time zone,", RefElementVoisinage.PROPERTY_P_DT_MAJ, P_DATE_FORMATTER);
        modelBuilder.newColumnForExport("iae_no_ordre integer,", RefElementVoisinage.PROPERTY_IAE_NO_ORDRE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("unite_surface character varying,", RefElementVoisinage.PROPERTY_UNITE_SURFACE);
        modelBuilder.newColumnForExport("Source", RefElementVoisinage.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefElementVoisinage.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefElementVoisinage newEmptyInstance() {
        RefElementVoisinage refElementVoisinage = new RefElementVoisinageImpl();
        refElementVoisinage.setActive(true);
        return refElementVoisinage;
    }
}
