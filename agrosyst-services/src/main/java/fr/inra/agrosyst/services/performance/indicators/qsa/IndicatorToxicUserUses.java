package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2025 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import org.apache.commons.collections4.MultiValuedMap;

import java.util.List;
import java.util.Map;

public class IndicatorToxicUserUses extends IndicatorHazardousProductsUses {

    public static final String COLUMN_NAME = "recours_produits_toxiques_utilisateurs";
    public static final String COLUMN_NAME_HTS = "recours_produits_toxiques_utilisateurs_hts";

    protected final String[] indicatorLabels = new String[] {
            "Indicator.label.toxic_user_products",
            "Indicator.label.toxic_user_products_hts"
    };

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME_HTS);
        return indicatorNameToColumnName;
    }

    @Override
    protected boolean isHazardousInput(Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> domainSubstancesByAmm, MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> allRefPhrasesRisqueEtClassesMentionDangerByAMM, String codeAmm) {
        return allRefPhrasesRisqueEtClassesMentionDangerByAMM.get(codeAmm)
                .stream()
                .anyMatch(RefPhrasesRisqueEtClassesMentionDangerParAMM::isToxique_utilisateur);
    }

    @Override
    protected String[] getIndicatorsLabels() {
        return this.indicatorLabels;
    }

}
