package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefNrjSemence;
import fr.inra.agrosyst.api.entities.referential.RefNrjSemenceImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * code culture;libelle culture;NRJ (MJ/kg);source
 * 
 * @author Eric Chatellier
 */
public class RefNrjSemenceModel extends AbstractAgrosystModel<RefNrjSemence> implements ExportModel<RefNrjSemence> {

    public RefNrjSemenceModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("code culture", RefNrjSemence.PROPERTY_CODE_CULTURE);
        newMandatoryColumn("libelle culture", RefNrjSemence.PROPERTY_LIBELLE_CULTURE);
        newMandatoryColumn("NRJ (MJ/kg)", RefNrjSemence.PROPERTY_NRJ, DOUBLE_PARSER);
        newMandatoryColumn("source", RefNrjSemence.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefNrjSemence.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefNrjSemence, Object>> getColumnsForExport() {
        ModelBuilder<RefNrjSemence> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Intitulé", RefNrjSemence.PROPERTY_CODE_CULTURE);
        modelBuilder.newColumnForExport("libelle culture", RefNrjSemence.PROPERTY_LIBELLE_CULTURE);
        modelBuilder.newColumnForExport("NRJ (MJ/kg)", RefNrjSemence.PROPERTY_NRJ, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("source", RefNrjSemence.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefNrjSemence.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefNrjSemence newEmptyInstance() {
        RefNrjSemence refNrjSemence = new RefNrjSemenceImpl();
        refNrjSemence.setActive(true);
        return refNrjSemence;
    }
}
