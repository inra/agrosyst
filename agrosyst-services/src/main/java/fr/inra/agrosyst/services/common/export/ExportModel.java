package fr.inra.agrosyst.services.common.export;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Modèle d'export. Chaque modèle correspond à un onglet dans un tableur. Il y a un titre d'onglet ({@link #getTitle()}
 * et surtout une liste de colonnes. La liste est matérialisée par une Map dont la clé est le nom de la colonne et la
 * valeur est une fonction permettant de récupérer la valeur pour cette colonne.
 *
 * @param <E> est le type que ce modèle transforme en version tabulaire.
 */
public abstract class ExportModel<E>  {

    public abstract String getTitle();

    /**
     * Liste des colonnes et pour chacune la fonction pour récupérer la valeur
     *
     * Note: l'ordre d'insertion a de l'importance, donc une interface {@link Map} ne suffit pas
     */
    protected final LinkedHashMap<String, Function<E, ?>> columns = new LinkedHashMap<>();

    /**
     * Liste des colonnes qui ont une liste exhaustive de valeurs
     */
    protected final Multimap<String, String> dropDowns = ArrayListMultimap.create();

    /**
     * Liste des colonnes et pour chacune la fonction pour injecter la valeur
     */
    protected final Map<String, BiConsumer<E, ?>> importColumns = new LinkedHashMap<>();

    public ImmutableMap<String, Function<E, ?>> getColumns() {
        return ImmutableMap.copyOf(columns);
    }

    public ImmutableMultimap<String, String> getDropDowns() {
        return ImmutableMultimap.copyOf(dropDowns);
    }

    /**
     * Déclare une nouvelle colonne.
     *
     * @param columnName  nom de la colonne (typiquement le header)
     * @param valueGetter fonction à utiliser sur la source {@code E} pour récupérer la valeur
     */
    public <F> void newColumn(String columnName,
                              Function<E, F> valueGetter) {
        Function<E, ?> previousValueGetter = columns.put(columnName, valueGetter);
        if (previousValueGetter != null) {
            LogFactory.getLog(this.getClass()).warn("A previous valueGetter has been replaced for column " + columnName);
        }
    }

    /**
     * Déclare une nouvelle colonne en passant par un délégateur.
     *
     * @param columnName  nom de la colonne (typiquement le header)
     * @param delegator   fonction à utiliser pour récupérer l'instance sur laquelle déléguer les appels
     * @param valueGetter fonction à utiliser sur la source déléguée {@code D} pour récupérer la valeur
     */
    public <F, D> void newColumn(String columnName,
                                 Function<E, D> delegator,
                                 Function<D, F> valueGetter) {
        final Function<E, F> valueGetterThroughDelegation = input -> {
            D delegation = delegator.apply(input);
            F result = Optional.ofNullable(delegation)
                    .map(valueGetter)
                    .orElse(null);
            return result;
        };
        newColumn(columnName, valueGetterThroughDelegation);
    }

    /**
     * Déclare une nouvelle colonne.
     *
     * @param columnName  nom de la colonne (typiquement le header)
     * @param enumClass   class de l'enum dont on va inclure les valeurs en liste exhaustive
     * @param valueGetter fonction à utiliser sur la source {@code E} pour récupérer la valeur
     */
    public <F extends Enum<F>> void newColumn(String columnName,
                                              Class<F> enumClass,
                                              Function<E, F> valueGetter) {
        Iterable<String> dropDownValues = ExportUtils.allStringOf(enumClass);
        newColumn(columnName, dropDownValues, valueGetter);
    }

    /**
     * Déclare une nouvelle colonne.
     *
     * @param columnName     nom de la colonne (typiquement le header)
     * @param dropDownValues liste exhaustive des valeurs attendues pour cette colonne
     * @param valueGetter    fonction à utiliser sur la source {@code E} pour récupérer la valeur
     */
    public <F> void newColumn(String columnName,
                              Iterable<String> dropDownValues,
                              Function<E, F> valueGetter) {
        newColumn(columnName, valueGetter);
        dropDowns.putAll(columnName, dropDownValues);
    }

    /**
     * Déclare une nouvelle colonne en passant par un délégateur.
     *
     * @param columnName  nom de la colonne (typiquement le header)
     * @param dropDownValues liste exhaustive des valeurs attendues pour cette colonne
     * @param delegator   fonction à utiliser pour récupérer l'instance sur laquelle déléguer les appels
     * @param valueGetter fonction à utiliser sur la source déléguée {@code D} pour récupérer la valeur
     */
    public <F, D> void newColumn(String columnName,
                                 Iterable<String> dropDownValues,
                                 Function<E, D> delegator,
                                 Function<D, F> valueGetter) {
        newColumn(columnName, delegator, valueGetter);
        dropDowns.putAll(columnName, dropDownValues);
    }

    /**
     * Déclare une nouvelle colonne qui peut également être utilisée à l'import.
     *
     * @param columnName  nom de la colonne (typiquement le header)
     * @param valueGetter fonction à utiliser sur la source {@code E} pour récupérer la valeur
     * @param valueSetter fonction à utiliser pour injecter la valeur lue à l'import dans {code E}
     */
    public <F> void newImportColumn(String columnName,
                                    Function<E, F> valueGetter,
                                    BiConsumer<E, String> valueSetter) {
        newColumn(columnName, valueGetter);
        importColumns.put(columnName, valueSetter);
    }

    /**
     * Déclare une nouvelle colonne qui peut également être utilisée à l'import. Si la valeur attendue à l'import n'est
     * pas une String, on doit fournir un {@code parser}.
     *
     * @param columnName  nom de la colonne (typiquement le header)
     * @param valueGetter fonction à utiliser sur la source {@code E} pour récupérer la valeur
     * @param parser      fonction permettant de transformer le champ en String vers le type attendue par le setter
     * @param valueSetter fonction à utiliser pour injecter la valeur lue à l'import dans {code E}
     */
    public <F, G> void newImportColumn(String columnName,
                                       Function<E, F> valueGetter,
                                       Function<String, G> parser,
                                       BiConsumer<E, G> valueSetter) {
        BiConsumer<E, String> stringSetter = (e, s) -> {
            if (StringUtils.isNotEmpty(s)) {
                G value = parser.apply(s);
                if (value != null) {
                    valueSetter.accept(e, value);
                }
            }
        };
        newImportColumn(columnName, valueGetter, stringSetter);
    }

    /**
     * Déclare une nouvelle colonne qui peut également être utilisée à l'import.
     *
     * @param columnName  nom de la colonne (typiquement le header)
     * @param valueGetter fonction à utiliser sur la source {@code E} pour récupérer la valeur
     * @param valueSetter fonction à utiliser pour injecter la valeur lue à l'import dans {code E}
     */
    public <F> void newImportDoubleColumn(String columnName,
                                          Function<E, F> valueGetter,
                                          BiConsumer<E, Double> valueSetter) {
        newImportColumn(columnName, valueGetter, ExportModel::doubleParser, valueSetter);
    }

    public static Double doubleParser(String input) {
        // TODO AThimel 10/03/2021 : Gérer les différents formats (notamment avec les points et les virgules)
        return Double.valueOf(input);
    }

}
