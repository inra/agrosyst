package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class EuropeanProductDto implements Serializable {

    public static final String PROPERTY_AMM_UE = "amm_ue";
    public static final String PROPERTY_NOM_PRODUIT = "nom_produit";
    public static final String PROPERTY_SA_1 = "sa_1";
    public static final String PROPERTY_DOSE_1 = "dose_1";
    public static final String PROPERTY_UNITE_DOSE_1 = "unite_dose_1";
    public static final String PROPERTY_SA_2 = "sa_2";
    public static final String PROPERTY_DOSE_2 = "dose_2";
    public static final String PROPERTY_UNITE_DOSE_2 = "unite_dose_2";
    public static final String PROPERTY_SA_3 = "sa_3";
    public static final String PROPERTY_DOSE_3 = "dose_3";
    public static final String PROPERTY_UNITE_DOSE_3 = "unite_dose_3";
    public static final String PROPERTY_SA_4 = "sa_4";
    public static final String PROPERTY_DOSE_4 = "dose_4";
    public static final String PROPERTY_UNITE_DOSE_4 = "unite_dose_4";
    public static final String PROPERTY_SA_5 = "sa_5";
    public static final String PROPERTY_DOSE_5 = "dose_5";
    public static final String PROPERTY_UNITE_DOSE_5 = "unite_dose_5";
    public static final String PROPERTY_AMM_NOM_PRODUIT = "amm_nom_produit";
    public static final String PROPERTY_AMM_SA = "amm_sa";
    public static final String PROPERTY_AMM_PROBABLE = "amm_probable";
    public static final String PROPERTY_REF_DATA = "ref_data";

    private String amm_ue;
    private String nom_produit;
    private String sa_1;
    private Double dose_1;
    private String unite_dose_1;
    private String sa_2;
    private Double dose_2;
    private String unite_dose_2;
    private String sa_3;
    private Double dose_3;
    private String unite_dose_3;
    private String sa_4;
    private Double dose_4;
    private String unite_dose_4;
    private String sa_5;
    private Double dose_5;
    private String unite_dose_5;
    private String amm_nom_produit;
    private String amm_sa;
    private String amm_probable;
    private String ref_data;
}
