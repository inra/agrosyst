package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public class SolsArvalisRegionsModel extends AbstractAgrosystModel<RegionDto> {

    public SolsArvalisRegionsModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("sol_region", RegionDto.PROPERTY_NAME);
        newMandatoryColumn("Région", RegionDto.PROPERTY_CODE, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("Source", RegionDto.PROPERTY_SOURCE);
    }

    @Override
    public RegionDto newEmptyInstance() {
        return new RegionDto();
    }

}
