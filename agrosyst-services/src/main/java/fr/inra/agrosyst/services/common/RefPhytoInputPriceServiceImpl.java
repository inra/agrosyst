package fr.inra.agrosyst.services.common;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.referential.RefInputPriceImpl;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhyto;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class RefPhytoInputPriceServiceImpl {

    Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> getRefInputPriceForScenarios(
            Collection<String> scenarioCodes,
            Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters,
            Collection<DomainPhytoProductInput> phytoInputs,
            Map<String, Map<String, List<RefPrixPhyto>>> refPhytoScenarioPricesByObjectIdAndByScenarioCodes) {
    
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> refPrixPhytoByObjectIds = new HashMap<>();
        for (DomainPhytoProductInput input : phytoInputs) {
            Map<String, List<RefPrixPhyto>> refPhytoScenarioPricesByScenarioCodes =
                    refPhytoScenarioPricesByObjectIdAndByScenarioCodes.get(InputPriceService.GET_INPUT_OBJECT_ID.apply(input));
            
            if (refPhytoScenarioPricesByScenarioCodes == null ||
                    refPhytoScenarioPricesByScenarioCodes.isEmpty()) {
                scenarioCodes.forEach(scenarioCode -> {
                    Map<String, Optional<InputRefPrice>> rePriceMap = refPrixPhytoByObjectIds.computeIfAbsent(input, k -> new HashMap<>());
                    rePriceMap.put(scenarioCode, Optional.empty());
                });
                continue;
            }
            
            Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> convertersByPriceUnit = RefInputPriceServiceImpl.getConvertersByPriceUnits(refInputUnitPriceUnitConverters);
            
            for (Map.Entry<String, List<RefPrixPhyto>> refPhytoScenarioPricesByScenarioCode : refPhytoScenarioPricesByScenarioCodes.entrySet()) {
                String scenarioCode = refPhytoScenarioPricesByScenarioCode.getKey();
                List<RefPrixPhyto> refScenarioProductPrices = refPhytoScenarioPricesByScenarioCode.getValue();
                
                if (CollectionUtils.isEmpty(refScenarioProductPrices)) {
                    Map<String, Optional<InputRefPrice>> rePriceMap = refPrixPhytoByObjectIds.computeIfAbsent(input, k -> new HashMap<>());
                    rePriceMap.put(scenarioCode, Optional.empty());
                } else {
    
                    Optional<PriceAndUnit> refPrice = RefInputPriceServiceImpl.getAverageRefPrice(refScenarioProductPrices, refInputUnitPriceUnitConverters, input);
    
                    Map<String, Optional<InputRefPrice>> rePriceMap = refPrixPhytoByObjectIds.computeIfAbsent(input, k -> new HashMap<>());
                    
                    if (refPrice.isEmpty()) {
                        rePriceMap.put(scenarioCode, Optional.empty());
                    }
                    
                    Optional<Double> conversionRate = getPhytoConversionRate(input, convertersByPriceUnit, refPrice);
                    if (conversionRate.isPresent() && refPrice.isPresent()) {
                        PriceAndUnit scenarioPriceAndUnit = refPrice.get();
                        final double priceValue = scenarioPriceAndUnit.value();
                        double inputsCharges = PricesService.DEFAULT_PRICE_UNIT == scenarioPriceAndUnit.unit() ? priceValue : conversionRate.get() * priceValue;
                        InputRefPrice inputRefPrice = RefInputPriceServiceImpl.getScenarioInputRePrice(input, scenarioCode, new PriceAndUnit(inputsCharges, scenarioPriceAndUnit.unit()));
    
                        rePriceMap.put(scenarioCode, Optional.of(inputRefPrice));
                    } else {
                        rePriceMap.put(scenarioCode, Optional.empty());
                    }
                    
                }
            }
        }
        return refPrixPhytoByObjectIds;
    }
    
    protected Pair<PhytoProductUnit, Double> getConversionRateForInput(
            DomainPhytoProductInput input,
            List<RefInputUnitPriceUnitConverter> doseUnitToPriceUnitConverters) {
        
        PhytoProductUnit productUnit = input.getUsageUnit();

        Double conversionRate = doseUnitToPriceUnitConverters.stream()
                .filter(obj -> obj.getPhytoProductUnit() != null && obj.getPhytoProductUnit().equals(productUnit))
                .toList()
                .stream()
                .map(RefInputUnitPriceUnitConverter::getConvertionRate).findAny().orElse(null);

        Pair<PhytoProductUnit, Double> conversionRateForDoseUnit = Pair.of(productUnit, conversionRate);

        return conversionRateForDoseUnit;
    }

    // TODO factoriser as RefOrganicInputPriceServiceImpl.getDoseUnitToPriceUnitConversionRate() RefInputPriceServiceImpl.getDoseUnitToPriceUnitConversionRate()
    protected Pair<PhytoProductUnit, Double> getDoseUnitToPriceUnitConversionRate(
            Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> doseUnitToPriceUnitConverterByPriceUnit,
            PriceUnit priceUnit,
            DomainPhytoProductInput input) {
        
        Pair<PhytoProductUnit, Double> conversionRateForDoseUnit = null;
        
        if (doseUnitToPriceUnitConverterByPriceUnit != null) {
            
            List<RefInputUnitPriceUnitConverter> doseUnitToPriceUnitConverters = doseUnitToPriceUnitConverterByPriceUnit.get(priceUnit);
            
            if (CollectionUtils.isNotEmpty(doseUnitToPriceUnitConverters)) {
                
                conversionRateForDoseUnit = getConversionRateForInput(input, doseUnitToPriceUnitConverters);
            }
            
            if (conversionRateForDoseUnit == null || conversionRateForDoseUnit.getValue() == null) {
                // si pas trouver, essayer avec une unité cohérente
                Double conversionRate = null;
                Iterator<Map.Entry<PriceUnit, List<RefInputUnitPriceUnitConverter>>> iter = doseUnitToPriceUnitConverterByPriceUnit.entrySet().iterator();
                while (iter.hasNext() && conversionRate == null) {
                    Map.Entry<PriceUnit, List<RefInputUnitPriceUnitConverter>> priceUnitListEntry = iter.next();
                    PriceUnit to = priceUnitListEntry.getKey();
                    doseUnitToPriceUnitConverters = doseUnitToPriceUnitConverterByPriceUnit.get(to);
                    Double priceUnitConversionRate = RefInputPriceImpl.GET_PRICE_UNIT_TO_OTHER_PRICE_UNIT_CONVERSION_RATE.apply(Pair.of(priceUnit, to));
                    if (priceUnitConversionRate != null && priceUnitConversionRate != 0 && CollectionUtils.isNotEmpty(doseUnitToPriceUnitConverters)) {
                        conversionRateForDoseUnit = getConversionRateForInput(input, doseUnitToPriceUnitConverters);
                        final Double optionalValue = conversionRateForDoseUnit.getRight();
                        conversionRate = optionalValue != null ? optionalValue * priceUnitConversionRate : null;
                        conversionRateForDoseUnit = Pair.of(conversionRateForDoseUnit.getLeft(), conversionRate);
                    }
                }
            }
        }
        
        return conversionRateForDoseUnit;
    }
    
    
    protected Optional<Double> getPhytoConversionRate(DomainPhytoProductInput input,
                                                      Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> convertersByPriceUnit,
                                                      Optional<PriceAndUnit> optionalPriceAndUnit) {
        if (optionalPriceAndUnit.isPresent()) {
            PriceAndUnit priceAndUnit = optionalPriceAndUnit.get();
            Double conversionRate = null;
            Pair<PhytoProductUnit, Double> conversionRateForDoseUnit = getDoseUnitToPriceUnitConversionRate(
                    convertersByPriceUnit,
                    priceAndUnit.unit(),
                    input);
    
            if (conversionRateForDoseUnit != null &&
                    conversionRateForDoseUnit.getRight() != null) {
                conversionRate = conversionRateForDoseUnit.getRight();
            }
            return Optional.ofNullable(conversionRate);
        }
        
        return Optional.empty();
    }
    
    public Map<? extends AbstractDomainInputStockUnit, ? extends Map<Integer, Optional<InputRefPrice>>> getRefInputPriceForCampaigns(
            Collection<Integer> campaigns,
            Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters,
            Collection<DomainPhytoProductInput> phytoInputs,
            Map<String, Map<Integer, List<RefPrixPhyto>>> refPhytoPricesByObjectIdAndByCampaigns) {
    
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refInputPriceByCampaignForInput = new HashMap<>();
        
        for (DomainPhytoProductInput input : phytoInputs) {
            Map<Integer, List<RefPrixPhyto>> refPhytoPricesByCampaigns =
                    refPhytoPricesByObjectIdAndByCampaigns.get(InputPriceService.GET_INPUT_OBJECT_ID.apply(input));
        
            if (refPhytoPricesByCampaigns == null || refPhytoPricesByCampaigns.isEmpty()) {
                campaigns.forEach(campaign -> {
                    Map<Integer, Optional<InputRefPrice>> rePriceMap = refInputPriceByCampaignForInput.computeIfAbsent(input, k -> new HashMap<>());
                    rePriceMap.put(campaign, Optional.empty());
                });
                continue;
            }

            InputPrice inputPrice = input.getInputPrice();
            Optional<PriceUnit> optionalInputPriceUnit = inputPrice != null ? Optional.ofNullable(inputPrice.getPriceUnit()) : Optional.empty();

            Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> convertersByPriceUnit = RefInputPriceServiceImpl.getConvertersByPriceUnits(refInputUnitPriceUnitConverters);
        
            for (Map.Entry<Integer, List<RefPrixPhyto>> refPhytoPricesByCampaign : refPhytoPricesByCampaigns.entrySet()) {
                Integer campaign = refPhytoPricesByCampaign.getKey();
                List<RefPrixPhyto> refProductPrices = refPhytoPricesByCampaign.getValue();
            
                if (CollectionUtils.isEmpty(refProductPrices)) {
                    Map<Integer, Optional<InputRefPrice>> rePriceMap = refInputPriceByCampaignForInput.computeIfAbsent(input, k -> new HashMap<>());
                    rePriceMap.put(campaign, Optional.empty());
                } else {

                    Collection<PriceAndUnit> priceAndUnits = RefInputPriceServiceImpl.getAverageRefPriceAndUnit(convertersByPriceUnit, refProductPrices, optionalInputPriceUnit);
                    //best
                    tryFindRefInputPricesFromNoneDefaultPriceUnit(input, refInputPriceByCampaignForInput, priceAndUnits, convertersByPriceUnit, campaign);
                    // intermediate
                    ifNotFoundTryFindRefInputPricesFromDefaultPriceUnit(input, refInputPriceByCampaignForInput, priceAndUnits, convertersByPriceUnit, campaign);
                    // fallback
                    ifNotFoundTryFindRefInputsDefaultPriceUnit(input, refInputPriceByCampaignForInput, priceAndUnits, campaign);

                }
            }
        }
        return refInputPriceByCampaignForInput;
    }

    private void tryFindRefInputPricesFromNoneDefaultPriceUnit(
            DomainPhytoProductInput input,
            Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refInputPriceByCampaignForInput,
            Collection<PriceAndUnit> priceAndUnits,
            Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> convertersByPriceUnit,
            Integer campaign) {

        List<PriceAndUnit> priceAndUnitWithoutDefaultUnit = CollectionUtils.emptyIfNull(priceAndUnits).stream().filter(pau -> pau.unit() != PricesService.DEFAULT_PRICE_UNIT).toList();
        tryFindConvertibleRefPrices(input, refInputPriceByCampaignForInput, convertersByPriceUnit, campaign, priceAndUnitWithoutDefaultUnit);
    }

    private void ifNotFoundTryFindRefInputPricesFromDefaultPriceUnit(
            DomainPhytoProductInput input,
            Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refInputPriceByCampaignForInput,
            Collection<PriceAndUnit> priceAndUnits,
            Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> convertersByPriceUnit,
            Integer campaign) {

        if (refInputPriceByCampaignForInput.get(input)==null || (refInputPriceByCampaignForInput.get(input).get(campaign) == null)) {
            List<PriceAndUnit> priceAndUnitWitDefaultUnit = CollectionUtils.emptyIfNull(priceAndUnits).stream().filter(pau -> pau.unit() == PricesService.DEFAULT_PRICE_UNIT).toList();
            tryFindConvertibleRefPrices(input, refInputPriceByCampaignForInput, convertersByPriceUnit, campaign, priceAndUnitWitDefaultUnit);
        }
    }

    private void tryFindConvertibleRefPrices(DomainPhytoProductInput input, Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refInputPriceByCampaignForInput, Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> convertersByPriceUnit, Integer campaign, List<PriceAndUnit> priceAndUnitWitDefaultUnit) {
        for (PriceAndUnit priceAndUnit : priceAndUnitWitDefaultUnit) {
            Optional<Double> conversionRate = getPhytoConversionRate(input, convertersByPriceUnit, Optional.ofNullable(priceAndUnit));
            Map<Integer, Optional<InputRefPrice>> optionalPriceForCampaign = refInputPriceByCampaignForInput.get(input);

            if (!conversionRate.orElse(0d).equals(0d) &&
                    (optionalPriceForCampaign == null || optionalPriceForCampaign.get(campaign) == null)) {

                if (optionalPriceForCampaign == null) {
                    optionalPriceForCampaign = new HashMap<>();
                    refInputPriceByCampaignForInput.put(input, optionalPriceForCampaign);
                }

                PriceAndUnit refPriceValue = getConvertRefPriceAndUnit(priceAndUnit, conversionRate);
                InputRefPrice inputRefPrice = RefInputPriceServiceImpl.getCampaignInputRePrice(input, campaign, refPriceValue, false);
                optionalPriceForCampaign.put(campaign, Optional.of(inputRefPrice));
                break;
            }
        }
    }

    private static PriceAndUnit getConvertRefPriceAndUnit(PriceAndUnit priceAndUnit, Optional<Double> conversionRate) {
        final double priceValue;
        PriceUnit unit;
        if (priceAndUnit == null) {
            priceValue = 0;
            unit = PricesService.DEFAULT_PRICE_UNIT;
        } else {
            priceValue = priceAndUnit.value();
            unit = priceAndUnit.unit();
        }
        // computeCi without da
        double inputsCharges = PricesService.DEFAULT_PRICE_UNIT == unit ? priceValue : conversionRate.get() * priceValue;
        PriceAndUnit refPriceValue = new PriceAndUnit(inputsCharges, unit);
        return refPriceValue;
    }

    private static void ifNotFoundTryFindRefInputsDefaultPriceUnit(
            DomainPhytoProductInput input,
            Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refInputPriceByCampaignForInput,
            Collection<PriceAndUnit> priceAndUnits,
            Integer campaign) {

        if (refInputPriceByCampaignForInput.get(input) == null || (refInputPriceByCampaignForInput.get(input).get(campaign) == null)) {
            Optional<PriceAndUnit> anyDefaultRefInputPrice = CollectionUtils.emptyIfNull(priceAndUnits)
                    .stream()
                    .filter(rpu -> PricesService.DEFAULT_PRICE_UNIT.equals(rpu.unit()) && rpu.value() > 0)
                    .findAny();
            if (anyDefaultRefInputPrice.isPresent()) {
                double defaultRefPriceValue = anyDefaultRefInputPrice.get().value();
                PriceAndUnit refPriceValue = new PriceAndUnit(defaultRefPriceValue, PricesService.DEFAULT_PRICE_UNIT);
                InputRefPrice inputRefPrice = RefInputPriceServiceImpl.getCampaignInputRePrice(input, campaign, refPriceValue, true);
                Map<Integer, Optional<InputRefPrice>> rePriceMap = refInputPriceByCampaignForInput.computeIfAbsent(input, k -> new HashMap<>());
                rePriceMap.put(campaign, Optional.of(inputRefPrice));
            }
        }
    }

}
