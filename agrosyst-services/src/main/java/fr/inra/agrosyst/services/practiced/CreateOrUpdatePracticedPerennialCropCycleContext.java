package fr.inra.agrosyst.services.practiced;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCyclePhaseDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleSpeciesDto;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by davidcosse on 26/04/16.
 */
@Getter
@Setter
public class CreateOrUpdatePracticedPerennialCropCycleContext {
    
    protected final Map<String, AbstractDomainInputStockUnit> domainInputStockByIds;
    protected final PracticedSystem practicedSystem;
    protected final PracticedPerennialCropCycle practicedPerennialCropCycle;
    protected final List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos;
    protected final List<PracticedCropCycleSpeciesDto> cropCyclePerennialSpeciesDto;
    protected final String domainCode;
    
    // for Valorisation validation
    protected final Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
    protected final Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant;
    private final Map<String, CroppingPlanEntry> croppingPlanEntryMap;
    private final Map<String, CroppingPlanSpecies> croppingPlanSpeciesMap;

    protected Map<PracticedCropCyclePhaseDto, PracticedCropCyclePhase> phasesDtoPhase;
    
    protected Map<String, HarvestingActionValorisation> savedActionValorisationsByOriginalIds = new HashMap<>();

    CreateOrUpdatePracticedPerennialCropCycleContext(
            String domainCode,
            PracticedSystem practicedSystem,
            PracticedPerennialCropCycle practicedPerennialCropCycle,
            List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos,
            List<PracticedCropCycleSpeciesDto> cropCyclePerennialSpeciesDto,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockByIds,
            Map<String, CroppingPlanEntry> croppingPlanEntryMap,
            Map<String, CroppingPlanSpecies> croppingPlanSpeciesMap) {

        this.domainCode = domainCode;
        this.practicedSystem = practicedSystem;
        this.practicedPerennialCropCycle = practicedPerennialCropCycle;
        this.cropCyclePhaseDtos = cropCyclePhaseDtos;
        this.cropCyclePerennialSpeciesDto = cropCyclePerennialSpeciesDto;
        this.speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
        this.sectorByCodeEspceBotaniqueCodeQualifiant = sectorByCodeEspceBotaniqueCodeQualifiant;
        this.domainInputStockByIds = domainInputStockByIds;
        this.croppingPlanEntryMap = croppingPlanEntryMap;
        this.croppingPlanSpeciesMap = croppingPlanSpeciesMap;
    }

}
