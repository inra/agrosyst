package fr.inra.agrosyst.services.performance.dbPersistence;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.performance.PerformanceTopiaDao;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class DbPerformanceDomainsContext implements Serializable {
    
    @Serial
    private static final long serialVersionUID = 1115281091295778236L;
    
    final Map<String, List<String>> growingSystemIdsByDomainIds;
    final Map<String, List<String>> zoneIdsByDomainIds;
    
    final List<DbPerformanceDomainContext> dbPerformanceDomainsContext = new ArrayList<>();
    
    public DbPerformanceDomainsContext(PerformanceTopiaDao performanceDao, List<String> filter) {
        growingSystemIdsByDomainIds = performanceDao.findAllPracticedSystemGrowingSystemIdsByDomainIds();
        zoneIdsByDomainIds = performanceDao.findAllEffectiveZoneIdsByDomainIds();
        
        Set<String> allDomainIds = new HashSet<>();
        if (CollectionUtils.isNotEmpty(filter)) {
            allDomainIds.addAll(filter);
        } else {
            allDomainIds.addAll(growingSystemIdsByDomainIds.keySet());
            allDomainIds.addAll(zoneIdsByDomainIds.keySet());
        }

        for (String allDomainId : allDomainIds) {
            List<String> domainGrowingSystemIds = ListUtils.emptyIfNull(growingSystemIdsByDomainIds.get(allDomainId));
            List<String> domainZoneIds = ListUtils.emptyIfNull(zoneIdsByDomainIds.get(allDomainId));
            
            dbPerformanceDomainsContext.add(
                    new DbPerformanceDomainContext(allDomainId, domainGrowingSystemIds, domainZoneIds)
            );
            
        }
    }
    
    public List<DbPerformanceDomainContext> getDbPerformanceDomainsContext() {
        return dbPerformanceDomainsContext;
    }
    
}
