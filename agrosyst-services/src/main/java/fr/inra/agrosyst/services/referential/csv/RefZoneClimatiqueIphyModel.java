package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefPhytoSubstanceActiveIphy;
import fr.inra.agrosyst.api.entities.referential.RefZoneClimatiqueIphy;
import fr.inra.agrosyst.api.entities.referential.RefZoneClimatiqueIphyImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Référentiel Acta substance active.
 * 
 * departement;zone_climatique
 */
public class RefZoneClimatiqueIphyModel extends AbstractAgrosystModel<RefZoneClimatiqueIphy> implements ExportModel<RefZoneClimatiqueIphy> {

    public RefZoneClimatiqueIphyModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("departement", RefZoneClimatiqueIphy.PROPERTY_DEPARTEMENT);
        newMandatoryColumn("zone_climatique", RefZoneClimatiqueIphy.PROPERTY_ZONE_CLIMATIQUE, INT_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefZoneClimatiqueIphy.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefZoneClimatiqueIphy, Object>> getColumnsForExport() {
        ModelBuilder<RefPhytoSubstanceActiveIphy> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("departement", RefZoneClimatiqueIphy.PROPERTY_DEPARTEMENT);
        modelBuilder.newColumnForExport("zone_climatique", RefZoneClimatiqueIphy.PROPERTY_ZONE_CLIMATIQUE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefZoneClimatiqueIphy.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefZoneClimatiqueIphy newEmptyInstance() {
        RefZoneClimatiqueIphy refZoneClimatiqueIphy = new RefZoneClimatiqueIphyImpl();
        refZoneClimatiqueIphy.setActive(true);
        return refZoneClimatiqueIphy;
    }
}
