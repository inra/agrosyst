package fr.inra.agrosyst.services.common;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AgrosystTopiaDaoSupplier;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.NetworkTopiaDao;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.entities.report.ReportRegional;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class ProjectionHelper {

    protected static final String PROPERTY_GROWING_PLAN_ACTIVE = GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_ACTIVE;
    protected static final String PROPERTY_GROWING_PLAN_DOMAIN_ACTIVE = GrowingSystemTopiaDao.PROPERTY_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_ACTIVE;
    protected static final String PROPERTY_GROWING_PLAN_CODE = GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_CODE;
    protected static final String PROPERTY_GROWING_PLAN_DOMAIN_CODE = GrowingSystemTopiaDao.PROPERTY_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_CODE;

    protected final AgrosystTopiaDaoSupplier topiaDaoSupplier;
    protected final TopiaJpaSupport topiaJpaSupport;
    protected final CacheService cacheService;

    public static void getNetworkSubQuery(StringBuilder query, Map<String, Object> args, Set<String> networkIds) {
        Collection<String> ids = DaoUtils.getRealIds(networkIds, Network.class);
        query   .append(" EXISTS ( ")
                .append("   FROM ").append(GrowingSystem.class.getName()).append(" gs0 ")
                .append("   INNER JOIN gs0.").append(GrowingSystem.PROPERTY_NETWORKS).append(" nw ")
                .append("   WHERE gs = gs0 ")
                .append("   AND nw.topiaId IN (:networkIds) ")
                .append(" )");
        args.put("networkIds", ids);
    }

    public static void getReportNetworkSubQuery(StringBuilder query, Map<String, Object> args, Set<String> networkIds) {
        Collection<String> ids = DaoUtils.getRealIds(networkIds, Network.class);
        query   .append(" EXISTS ( ")
                .append("   FROM ").append(ReportRegional.class.getName()).append(" rr0 ")
                .append("   INNER JOIN rr0.").append(ReportRegional.PROPERTY_NETWORKS).append(" nw ")
                .append("   WHERE R = rr0 ")
                .append("   AND nw.topiaId IN (:networkIds) ")
                .append(" )");
        args.put("networkIds", ids);
    }

    public ProjectionHelper(AgrosystTopiaDaoSupplier topiaDaoSupplier, TopiaJpaSupport topiaJpaSupport, CacheService cacheService) {
        this.topiaDaoSupplier = topiaDaoSupplier;
        this.topiaJpaSupport = topiaJpaSupport;
        this.cacheService = cacheService;
    }

    protected Set<String> networksProjection(Set<String> networkIds, String projection) {
        return networksProjection(networkIds, projection, null, null);
    }

    /**
     * Charge les réseaux et sous-réseaux des réseaux en paramètre. Cette méthode utilise un cache pour éviter de
     * refaire trop souvent ce calcul.
     */
    protected Set<String> loadSubNetworkIds(Set<String> networkIds) {

        Callable<LinkedHashSet<String>> loader = () -> {
            NetworkTopiaDao networkDAO = topiaDaoSupplier.getDao(Network.class, NetworkTopiaDao.class);
            Set<Network> networks = networkDAO.loadNetworksWithDescendantHierarchy(networkIds);
            LinkedHashSet<String> networkWithHierarchyIds = new LinkedHashSet<>();
            networks.stream().map(Network::getTopiaId).forEach(networkWithHierarchyIds::add);
            return networkWithHierarchyIds;
        };

        String key = networkIds.stream()
                .sorted()
                .collect(Collectors.joining("|"));

        Set<String> result = cacheService.get(CacheDiscriminator.networksHierarchy(), key, loader);
        return result;
    }

    protected Set<String> networksProjection(final Set<String> networkIds,
                                             final String projection,
                                             final String filterProperty,
                                             final Object filterValue) {

        /* XXX AThimel 07/10/2020
        Si problème de performance, il est possible de passer en SQL directement pour charger la liste des SdC.

        Exemple : select growingsystem from growingsystem_networks where networks = '...';

        On peut ensuite concaténer les identifiants de SdC dans un Set<String> et l'utiliser pour les requêtes.
        En gros, plutôt que faire 1 seule requête pour charger les codes de SdC selon les réseaux, faire 1 première
        requête pour charger les identifiants de SdC, puis une seconde pour charger les code

        À noter que le gros avantage que cela peut avoir c'est que comme à l'usage on fait souvent 2 requêtes
        successives pour charger les données (Exemple avec les identifiants de domaine puis les codes de domaines) le
        second appel pourrait se contenter de faire la 2ème requête
         */

        Callable<LinkedHashSet<String>> loader = () -> {
            StringBuilder query = new StringBuilder(String.format(" SELECT DISTINCT gs.%s FROM %s gs ", projection, GrowingSystem.class.getName()));
            query.append(" WHERE ( ");
            Map<String, Object> args = Maps.newLinkedHashMap();

            Set<String> networkWithHierarchyIds = loadSubNetworkIds(networkIds);
            getNetworkSubQuery(query, args, networkWithHierarchyIds);

            query.append(" ) ");

            query.append(DaoUtils.andAttributeEquals("gs", filterProperty, args, filterValue));

            List<String> projectedList = topiaJpaSupport.findAll(query.toString(), args);
            LinkedHashSet<String> result = Sets.newLinkedHashSet(projectedList);

            return result;
        };

        Collection<String> ids = DaoUtils.getRealIds(networkIds, Network.class);

        String key = String.format("%s_%s_%s_%s", ids, projection, filterProperty, filterValue);
        Set<String> result = cacheService.get(CacheDiscriminator.networksProjection(), key, loader);
        return result;
    }

    protected Set<String> networksReportProjection(final Set<String> networkIds, final String projection) {
        Callable<LinkedHashSet<String>> loader = () -> {
            StringBuilder query = new StringBuilder(String.format(" SELECT DISTINCT R.%s FROM %s R ", projection, ReportRegional.class.getName()));
            query.append(" WHERE ( ");
            Map<String, Object> args = Maps.newLinkedHashMap();

            Set<String> networkWithHierarchyIds = loadSubNetworkIds(networkIds);
            getReportNetworkSubQuery(query, args, networkWithHierarchyIds);

            query.append(" ) ");

            List<String> projectedList = topiaJpaSupport.findAll(query.toString(), args);
            LinkedHashSet<String> result = Sets.newLinkedHashSet(projectedList);

            return result;
        };

        String key = String.format("%s_%s", networkIds, projection);
        Set<String> result = cacheService.get(CacheDiscriminator.networksReportProjection(), key, loader);
        return result;
    }

    public Set<String> networksToGrowingSystems(Set<String> networkIds) {
        if (networkIds == null || networkIds.isEmpty()) {
            return Sets.newHashSet();
        }
        Set<String> result = networksProjection(networkIds, GrowingSystem.PROPERTY_TOPIA_ID);
        return result;
    }

    public Set<String> networksToGrowingSystemsCode(Set<String> networkIds) {
        Set<String> result = networksProjection(networkIds, GrowingSystem.PROPERTY_CODE);
        return result;
    }

    public Set<String> networksToReportRegional(Set<String> networkIds) {
        Set<String> result = networksReportProjection(networkIds, ReportRegional.PROPERTY_TOPIA_ID);
        return result;
    }

    public Set<String> networksToGrowingPlans(Set<String> networkIds) {
        Set<String> result = networksProjection(networkIds, GrowingSystemTopiaDao.PROPERTY_GROWING_PLAN_ID);
        return result;
    }

    public Set<String> networksToGrowingPlans(Set<String> networkIds, boolean active) {
        Set<String> result = networksProjection(networkIds, GrowingSystemTopiaDao.PROPERTY_GROWING_PLAN_ID, PROPERTY_GROWING_PLAN_ACTIVE, active);
        return result;
    }

    public Set<String> networksToGrowingPlansCode(Set<String> networkIds) {
        Set<String> result = networksProjection(networkIds, PROPERTY_GROWING_PLAN_CODE);
        return result;
    }

    public Set<String> networksToDomains(Set<String> networkIds) {
        Set<String> result = networksProjection(networkIds, GrowingSystemTopiaDao.PROPERTY_GROWING_PLAN_DOMAIN_ID);
        return result;
    }

    public Set<String> networksToDomains(Set<String> networkIds, boolean active) {
        Set<String> result = networksProjection(networkIds, GrowingSystemTopiaDao.PROPERTY_GROWING_PLAN_DOMAIN_ID, PROPERTY_GROWING_PLAN_DOMAIN_ACTIVE, active);
        return result;
    }

    public Set<String> networksToDomainsCode(Set<String> networkIds, boolean active) {
        Set<String> result = networksProjection(networkIds, PROPERTY_GROWING_PLAN_DOMAIN_CODE, PROPERTY_GROWING_PLAN_DOMAIN_ACTIVE, active);
        return result;
    }

    public Set<String> networksToDomainsCode(Set<String> networkIds) {
        Set<String> result = networksProjection(networkIds, PROPERTY_GROWING_PLAN_DOMAIN_CODE);
        return result;
    }

    public LinkedHashSet<String> domainsToGrowingSystemsCode(Set<String> domainCodes) {
        Preconditions.checkArgument(domainCodes != null && !domainCodes.isEmpty());

        String domainCodeProperty = GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CODE;

        StringBuilder query = new StringBuilder(String.format("SELECT distinct gs.%s FROM %s gs", GrowingSystem.PROPERTY_CODE, GrowingSystem.class.getName()));
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = Maps.newLinkedHashMap();

        // domains
        query.append(DaoUtils.andAttributeIn("gs", domainCodeProperty, args, domainCodes));

        List<String> growingSystemCodes = topiaJpaSupport.findAll(query.toString(), args);
        LinkedHashSet<String> result = Sets.newLinkedHashSet(growingSystemCodes);

        return result;
    }

    public LinkedHashSet<String> growingPlansToGrowingSystemsCode(Set<String> growingPlanCodes) {
        Preconditions.checkArgument(growingPlanCodes != null && !growingPlanCodes.isEmpty());

        String growingPlanCodeProperty = GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_CODE;

        StringBuilder query = new StringBuilder(String.format("SELECT distinct gs.%s FROM %s gs", GrowingSystem.PROPERTY_CODE, GrowingSystem.class.getName()));
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = Maps.newLinkedHashMap();

        // domains
        query.append(DaoUtils.andAttributeIn("gs", growingPlanCodeProperty, args, growingPlanCodes));

        List<String> growingSystemCodes = topiaJpaSupport.findAll(query.toString(), args);
        LinkedHashSet<String> result = Sets.newLinkedHashSet(growingSystemCodes);

        return result;
    }

    public Set<String> domainsToDomainCodes(final Set<String> domainIds, final Boolean active) {

        Callable<LinkedHashSet<String>> loader = () -> {
            StringBuilder query = new StringBuilder("SELECT DISTINCT d." + Domain.PROPERTY_CODE + " FROM " + Domain.class.getName() + " d WHERE 1 = 1");

            Map<String, Object> args = Maps.newLinkedHashMap();

            // domain TopiaIds
            if (domainIds != null) {
                query.append(DaoUtils.andAttributeIn("d", Domain.PROPERTY_TOPIA_ID, args, DaoUtils.getRealIds(domainIds, Domain.class)));
            }

            // domain Active
            if (active != null) {
                query.append(DaoUtils.andAttributeEquals("d", Domain.PROPERTY_ACTIVE, args, active));
            }

            List<String> domainCodes = topiaJpaSupport.findAll(query.toString(), args);
            LinkedHashSet<String> result = Sets.newLinkedHashSet(domainCodes);
            return result;
        };

        Set<String> result;
        if (domainIds == null) {
            try {
                result = loader.call();
            } catch (Exception ex) {
                throw new AgrosystTechnicalException("Unable to load domain's code",ex);
            }
        } else {
            String key = String.format("%s_%s_%s_%s", domainIds, Domain.PROPERTY_CODE, Domain.PROPERTY_TOPIA_ID, active);
            result = cacheService.get(CacheDiscriminator.domainsToDomainCodesProjection(), key, loader);
        }

        return result;
    }

    public Set<String> growingPlansToDomainCodes(final Set<String> growingPlanTopiaIds, final boolean active) {
        Preconditions.checkArgument(growingPlanTopiaIds != null && !growingPlanTopiaIds.isEmpty());

        final String growingPlanToDomainCodeProperty = GrowingPlan.PROPERTY_DOMAIN + "." +  Domain.PROPERTY_CODE;
        Callable<LinkedHashSet<String>> loader = () -> {

            StringBuilder query = new StringBuilder(String.format("SELECT distinct gp.%s FROM %s gp", growingPlanToDomainCodeProperty, GrowingPlan.class.getName()));
            query.append(" WHERE 1 = 1");
            Map<String, Object> args = Maps.newLinkedHashMap();

            // domains
            query.append(DaoUtils.andAttributeIn("gp", GrowingPlan.PROPERTY_TOPIA_ID, args, DaoUtils.getRealIds(growingPlanTopiaIds, GrowingPlan.class)));

            query.append(DaoUtils.andAttributeEquals("gp", GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_ACTIVE, args, active));

            List<String> domainCodes = topiaJpaSupport.findAll(query.toString(), args);
            LinkedHashSet<String> result = Sets.newLinkedHashSet(domainCodes);

            return result;
        };

        String key = String.format("%s_%s_%s_%s", growingPlanTopiaIds, growingPlanToDomainCodeProperty, Domain.PROPERTY_TOPIA_ID, active);
        Set<String> result = cacheService.get(CacheDiscriminator.growingPlansToDomainCodes(), key, loader);
        return result;

    }

    public Set<String> growingSystemsToDomainCodes(final Set<String> growingSystemsIds, final boolean active) {
        Preconditions.checkArgument(growingSystemsIds != null && !growingSystemsIds.isEmpty());

        final String growingSystemToDomainCodeProperty = GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." +  Domain.PROPERTY_CODE;


        Callable<LinkedHashSet<String>> loader = () -> {
            StringBuilder query = new StringBuilder(String.format("SELECT distinct gs.%s FROM %s gs", growingSystemToDomainCodeProperty, GrowingSystem.class.getName()));
            query.append(" WHERE 1 = 1");
            Map<String, Object> args = Maps.newLinkedHashMap();

            // domains
            query.append(DaoUtils.andAttributeIn("gs", GrowingSystem.PROPERTY_TOPIA_ID, args, DaoUtils.getRealIds(growingSystemsIds, GrowingSystem.class)));

            query.append(DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_ACTIVE, args, active));

            List<String> domainCodes = topiaJpaSupport.findAll(query.toString(), args);
            LinkedHashSet<String> result = Sets.newLinkedHashSet(domainCodes);

            return result;
        };

        String key = String.format("%s_%s_%s_%s", growingSystemsIds, growingSystemToDomainCodeProperty, Domain.PROPERTY_TOPIA_ID, active);
        Set<String> result = cacheService.get(CacheDiscriminator.networksProjection(), key, loader);
        return result;
    }

    public Set<String> campaignsToDomainCodes(final Set<Integer> campaigns, final boolean active) {
        Preconditions.checkArgument(campaigns != null && !campaigns.isEmpty());

        Callable<LinkedHashSet<String>> loader = () -> {
            StringBuilder query = new StringBuilder(String.format("SELECT distinct d.%s FROM %s d", Domain.PROPERTY_CODE, Domain.class.getName()));
            query.append(" WHERE 1 = 1");
            Map<String, Object> args = Maps.newLinkedHashMap();

            // domains
            query.append(DaoUtils.andAttributeIn("d", Domain.PROPERTY_CAMPAIGN, args, campaigns));

            query.append(DaoUtils.andAttributeEquals("d", Domain.PROPERTY_ACTIVE, args, active));

            List<String> domainCodes = topiaJpaSupport.findAll(query.toString(), args);
            LinkedHashSet<String> result = Sets.newLinkedHashSet(domainCodes);

            return result;
        };

        String key = String.format("%s_%s_%s_%s", campaigns, Domain.PROPERTY_CODE, Domain.PROPERTY_TOPIA_ID, active);
        Set<String> result = cacheService.get(CacheDiscriminator.networksProjection(), key, loader);
        return result;
    }

    public LinkedHashSet<String> domainsToReportRegional(Set<String> domainCodes) {
        String reportRegionalIdProperty = ReportGrowingSystem.PROPERTY_REPORT_REGIONAL + "." + ReportRegional.PROPERTY_TOPIA_ID;
        StringBuilder query = new StringBuilder(String.format("SELECT distinct R.%s FROM %s R", reportRegionalIdProperty, ReportGrowingSystem.class.getName()));
        query.append(" WHERE R." + ReportGrowingSystem.PROPERTY_REPORT_REGIONAL + " IS NOT NULL");
        Map<String, Object> args = Maps.newLinkedHashMap();

        // domains
        String domainCodeProperty = ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CODE;
        query.append(DaoUtils.andAttributeIn("R", domainCodeProperty, args, domainCodes));

        List<String> reportRegionalIds = topiaJpaSupport.findAll(query.toString(), args);
        LinkedHashSet<String> result = Sets.newLinkedHashSet(reportRegionalIds);

        return result;
    }

    public LinkedHashSet<String> growingPlanToReportRegional(Set<String> growingPlanCodes) {
        String reportRegionalIdProperty = ReportGrowingSystem.PROPERTY_REPORT_REGIONAL + "." + ReportRegional.PROPERTY_TOPIA_ID;
        StringBuilder query = new StringBuilder(String.format("SELECT distinct R.%s FROM %s R", reportRegionalIdProperty, ReportGrowingSystem.class.getName()));
        query.append(" WHERE R." + ReportGrowingSystem.PROPERTY_REPORT_REGIONAL + " IS NOT NULL");
        Map<String, Object> args = Maps.newLinkedHashMap();

        // growingPlan
        String growingPlanCodeProperty = ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_CODE;
        query.append(DaoUtils.andAttributeIn("R", growingPlanCodeProperty, args, growingPlanCodes));

        List<String> reportRegionalIds = topiaJpaSupport.findAll(query.toString(), args);
        LinkedHashSet<String> result = Sets.newLinkedHashSet(reportRegionalIds);

        return result;
    }

    public Set<String> growingSystemsToReportRegional(String property, String growingSystemIdentifier) {
        Preconditions.checkArgument(growingSystemIdentifier != null && !growingSystemIdentifier.isEmpty());

        Callable<LinkedHashSet<String>> loader = () -> {
            StringBuilder query = new StringBuilder(String.format("SELECT distinct RGS.%s.%s FROM %s RGS",
                    ReportGrowingSystem.PROPERTY_REPORT_REGIONAL, ReportRegional.PROPERTY_TOPIA_ID, ReportGrowingSystem.class.getName()));
            query.append(" WHERE 1 = 1");
            Map<String, Object> args = Maps.newLinkedHashMap();

            // growingSystem
            query.append(DaoUtils.andAttributeEquals("RGS", property, args, growingSystemIdentifier));
            query.append(String.format(" AND RGS.%s IS NOT NULL", ReportGrowingSystem.PROPERTY_REPORT_REGIONAL));

            List<String> reportRegionalIds = topiaJpaSupport.findAll(query.toString(), args);
            LinkedHashSet<String> result = Sets.newLinkedHashSet(reportRegionalIds);

            return result;
        };

        String key = String.format("%s_%s", property, growingSystemIdentifier);
        Set<String> result = cacheService.get(CacheDiscriminator.growingSystemsToReportRegional(), key, loader);
        return result;
    }
}
