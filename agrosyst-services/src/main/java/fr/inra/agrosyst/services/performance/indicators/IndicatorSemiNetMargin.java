package fr.inra.agrosyst.services.performance.indicators;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class IndicatorSemiNetMargin extends AbstractIndicator {

    private static final String COLUMN_NAME = "marge_semi_nette";
    private static final String MARGE_SEMI_NETTE_REELLE_TX_COMP = COLUMN_NAME + "_reelle_taux_de_completion";
    private static final String MARGE_SEMI_NETTE_STD_MIL_TX_COMP = COLUMN_NAME + "_std_mil_taux_de_completion";
    private static final String MARGE_SEMI_NETTE_REELLE_DETAIL = COLUMN_NAME + "_reelle_detail_champs_non_renseig";
    private static final String MARGE_SEMI_NETTE_STD_MIL_DETAIL = COLUMN_NAME + "_std_mil_detail_champs_non_renseig";

    private static final String[] LABELS = new String[]{
            "Indicator.label.semiNetMarginRealWithoutAC",
            "Indicator.label.semiNetMarginRealWithAC",
            "Indicator.label.semiNetMarginStandardisedWithoutAC",
            "Indicator.label.semiNetMarginStandardisedWithAC",
    };

    private boolean isWithAutoConsumed = true;
    private boolean isWithoutAutoConsumed = true;
    private boolean computeReal = true;
    private boolean computeStandardised = true;

    private final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    public IndicatorSemiNetMargin() {
        extraFields.put(
                OptionalExtraColumnEnumKey.MARGE_SEMI_NETTE_REELLE_TX_COMP,
                MARGE_SEMI_NETTE_REELLE_TX_COMP
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.MARGE_SEMI_NETTE_REELLE_DETAIL,
                MARGE_SEMI_NETTE_REELLE_DETAIL
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.MARGE_SEMI_NETTE_STD_MIL_TX_COMP,
                MARGE_SEMI_NETTE_STD_MIL_TX_COMP
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.MARGE_SEMI_NETTE_STD_MIL_DETAIL,
                MARGE_SEMI_NETTE_STD_MIL_DETAIL
        );
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        // pour inclure les colonnes extra
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();

        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME + "_reelle_sans_autoconso");
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME + "_reelle_avec_autoconso");
        indicatorNameToColumnName.put(getIndicatorLabel(2), COLUMN_NAME + "_std_mil_sans_autoconso");
        indicatorNameToColumnName.put(getIndicatorLabel(3), COLUMN_NAME + "_std_mil_avec_autoconso");

        indicatorNameToColumnName.put(MARGE_SEMI_NETTE_REELLE_TX_COMP, MARGE_SEMI_NETTE_REELLE_TX_COMP);
        indicatorNameToColumnName.put(MARGE_SEMI_NETTE_STD_MIL_TX_COMP, MARGE_SEMI_NETTE_STD_MIL_TX_COMP);
        indicatorNameToColumnName.put(MARGE_SEMI_NETTE_REELLE_DETAIL, MARGE_SEMI_NETTE_REELLE_DETAIL);
        indicatorNameToColumnName.put(MARGE_SEMI_NETTE_STD_MIL_DETAIL, MARGE_SEMI_NETTE_STD_MIL_DETAIL);

        return indicatorNameToColumnName;
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) &&
                displayed && (
                        i == 0 && isWithoutAutoConsumed && computeReal ||
                                i == 1 && isWithAutoConsumed && computeReal ||
                                i == 2 && isWithoutAutoConsumed && computeStandardised ||
                                i == 3 && isWithAutoConsumed && computeStandardised
        );
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {

        if (interventionContext.isFictive()) {
            interventionContext.setSemiNetMarginWithAutoconsume(Pair.of(0.0d, 0.0d));
            interventionContext.setSemiNetMarginWithoutAutoconsume(Pair.of(0.0d, 0.0d));
            return newArray(LABELS.length, 0.0d);
        }

        double realGrossMarginWithAutoconsume = interventionContext.getRealGrossMarginWithAutoconsume();
        double realGrossMarginWithoutAutoconsume = interventionContext.getRealGrossMarginWithoutAutoconsume();
        double standardisedGrossMarginWithAutoconsume = interventionContext.getStandardisedGrossMarginWithAutoconsume();
        double standardisedGrossMarginWithoutAutoconsume = interventionContext.getStandardisedGrossMarginWithoutAutoconsume();

        double realEquipmentsExpenses = interventionContext.getRealEquipmentsExpenses();
        double standardisedEquipmentsExpenses = interventionContext.getStandardisedEquipmentsExpenses();

        double realSemiNetMarginWithAutoConsume = realGrossMarginWithAutoconsume - realEquipmentsExpenses;
        double realSemiNetMarginWithoutAutoConsume = realGrossMarginWithoutAutoconsume - realEquipmentsExpenses;
        double standardisedSemiNetMarginWithAutoConsume = standardisedGrossMarginWithAutoconsume - standardisedEquipmentsExpenses;
        double standardisedSemiNetMarginWithoutAutoConsume = standardisedGrossMarginWithoutAutoconsume - standardisedEquipmentsExpenses;

        interventionContext.setSemiNetMarginWithAutoconsume(Pair.of(realSemiNetMarginWithAutoConsume, standardisedSemiNetMarginWithAutoConsume));
        interventionContext.setSemiNetMarginWithoutAutoconsume(Pair.of(realSemiNetMarginWithoutAutoConsume, standardisedSemiNetMarginWithoutAutoConsume));

        return newResult(realSemiNetMarginWithoutAutoConsume, realSemiNetMarginWithAutoConsume, standardisedSemiNetMarginWithoutAutoConsume, standardisedSemiNetMarginWithAutoConsume);
    }

    /**
     * Présentation de l’indicateur
     * <p>
     * La marge semi-nette réelle est exprimée en €/ha. Elle est calculée aux échelles : CP + Parcelle + SD + Domaine
     * <p>
     * Elle correspond à la différence entre la marge brute réelle d’une culture et les charges de mécanisation réelles de cette culture.
     * <p>
     * Formule de calcul
     * <p>
     * Marge Semi-Nette réelle sans l’autoconsommation (MSN réel sans autoconso) 𝑀𝑆𝑁 𝑟é𝑒𝑙 𝑠𝑎𝑛𝑠 𝑎𝑢𝑡𝑜𝑐𝑜𝑛𝑠𝑜 𝐶𝑃 = 𝑀𝐵 𝑟é𝑒𝑙 𝑠𝑎𝑛𝑠 𝑎𝑢𝑡𝑜𝑐𝑜𝑛𝑠𝑜 𝐶𝑃− 𝐶𝑀 𝑟é𝑒𝑙 𝐶𝑃
     * <p>
     * Marge Semi-Nette réelle avec l’autoconsommation (MSN réel avec autoconso) 𝑀𝑆𝑁 𝑟é𝑒𝑙 𝑎𝑣𝑒𝑐 𝑎𝑢𝑡𝑜𝑐𝑜𝑛𝑠𝑜 𝐶𝑃 = 𝑀𝐵 𝑟é𝑒𝑙 𝑎𝑣𝑒𝑐 𝑎𝑢𝑡𝑜𝑐𝑜𝑛𝑠𝑜 𝐶𝑃− 𝐶𝑀 𝑟é𝑒𝑙 𝐶𝑃
     */
    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {

        double realGrossMarginWithAutoconsume = interventionContext.getRealGrossMarginWithAutoconsume();
        double realGrossMarginWithoutAutoconsume = interventionContext.getRealGrossMarginWithoutAutoconsume();
        double standardisedGrossMarginWithAutoconsume = interventionContext.getStandardisedGrossMarginWithAutoconsume();
        double standardisedGrossMarginWithoutAutoconsume = interventionContext.getStandardisedGrossMarginWithoutAutoconsume();

        double realEquipmentsExpenses = interventionContext.getRealEquipmentsExpenses();
        double standardisedEquipmentsExpenses = interventionContext.getStandardisedEquipmentsExpenses();

        double realSemiNetMarginWithAutoConsume = realGrossMarginWithAutoconsume - realEquipmentsExpenses;
        double realSemiNetMarginWithoutAutoConsume = realGrossMarginWithoutAutoconsume - realEquipmentsExpenses;
        double standardisedSemiNetMarginWithAutoConsume = standardisedGrossMarginWithAutoconsume - standardisedEquipmentsExpenses;
        double standardisedSemiNetMarginWithoutAutoConsume = standardisedGrossMarginWithoutAutoconsume - standardisedEquipmentsExpenses;

        interventionContext.setSemiNetMarginWithAutoconsume(Pair.of(realSemiNetMarginWithAutoConsume, standardisedSemiNetMarginWithAutoConsume));
        interventionContext.setSemiNetMarginWithoutAutoconsume(Pair.of(realSemiNetMarginWithoutAutoConsume, standardisedSemiNetMarginWithoutAutoConsume));

        return newResult(realSemiNetMarginWithoutAutoConsume, realSemiNetMarginWithAutoConsume, standardisedSemiNetMarginWithoutAutoConsume, standardisedSemiNetMarginWithAutoConsume);
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_ECONOMIC);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
        if (displayed) {
            isWithAutoConsumed = filter.getWithAutoConsumed() != null && filter.getWithAutoConsumed();
            isWithoutAutoConsumed = filter.getWithoutAutoConsumed() != null && filter.getWithoutAutoConsumed();
            computeReal = filter.getComputeReal() != null && filter.getComputeReal();
            computeStandardised = filter.getComputeStandardized() != null && filter.getComputeStandardized();
        }
    }
}
