package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.performance.ActiveSubstance;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.l;

/**
 * Calcul des quantités de substances actives (QSA) utilisées.
 * <p>
 * Se base sur les informations du référentiel RefCompositionSubstancesActivesParNumeroAMM.
 * <p>
 * Pour l'instant, on calcule les quantités des substances actives suivantes :
 * <p>
 * - QSA Glyphosate (ID_SA 811)
 * - QSA S-métolachlore (ID_SA 1137)
 * - QSA Prosulfocarbe (ID_SA 711)
 * - QSA Chlortoluron (ID_SA 545)
 * - QSA Diflufenican (ID_SA 635)
 * - QSA Lambda-Cyhalothrine (ID_SA 259)
 * - QSA Boscalid (ID_SA 472)
 * - QSA Fluopyram (ID_SA 723)
 * <p>
 * en attendant de permettre aux utilisateurs de sélectionner n'importe quelle substance active au niveau de l'écran
 * des exports de performances
 * <p>
 * La méthode de calcul est simple :
 * <p>
 * QSA (x) = Σ[ Q(PPP)*C(x) ]
 * <p>
 * Avec :
 * <p>
 * - QSA(x) = quantité de substance active d'intérêt en kg/ha
 * - Q(PPP) = quantité d'intrant phytosanitaire appliquée, donnée saisie par l'utilisateur
 * - C(x)   = concentration en substance active d'intérêt, information issue du référentiel
 * <p>
 * Dans le référentiel RefCompositionSubstancesActivesParNumeroAMM, les quantités de substances actives peuvent
 * être indiquées dans différentes unités, donc il faut bien faire attention à traduire ces unités vers kg/ha.
 * Pour cela, utiliser RefConversionUnitesQSA pour retrouver le coefficient à appliquer selon l'unité de départ pour
 * passer en kg/ha. Il y a des cas pour lesquels la conversion ne peut pas se faire (valeur N/A dans le référentiel),
 * dans ce cas on indique une valeur nulle.
 * <p>
 * Cet indicateur nécessaire au calcul de:
 * - IndicatorTotalActiveSubstanceAmount
 * - IndicatorHRI1
 *
 * @see fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM
 * @see fr.inra.agrosyst.api.entities.referential.RefConversionUnitesQSA
 */
public class IndicatorActiveSubstanceAmount extends AbstractIndicatorQSA {

    public static final String[] COLUMN_NAMES = new String[]{
            "qsa_glyphosate",
            "qsa_glyphosate_hts",
            "qsa_smetolachlore",
            "qsa_smetolachlore_hts",
            "qsa_prosulfocarbe",
            "qsa_prosulfocarbe_hts",
            "qsa_chlortoluron",
            "qsa_chlortoluron_hts",
            "qsa_diflufenican",
            "qsa_diflufenican_hts",
            "qsa_lambda_cyhalothrine",
            "qsa_lambda_cyhalothrine_hts",
            "qsa_boscalid",
            "qsa_boscalid_hts",
            "qsa_fluopyram",
            "qsa_fluopyram_hts",
            "qsa_bixafen",
            "qsa_bixafen_hts",
            "qsa_dicamba",
            "qsa_dicamba_hts",
            "qsa_mancozeb",
            "qsa_mancozeb_hts",
            "qsa_phosmet",
            "qsa_phosmet_hts",
            "qsa_tebuconazole",
            "qsa_tebuconazole_hts",
            "qsa_dimethenamidp",
            "qsa_dimethenamidp_hts",
            "qsa_pendimethalin",
            "qsa_pendimethalin_hts",
            "qsa_flufenacet",
            "qsa_flufenacet_hts",
            "qsa_aclonifen",
            "qsa_aclonifen_hts",
            "qsa_isoxaben",
            "qsa_isoxaben_hts",
            "qsa_beflutamid",
            "qsa_beflutamid_hts",
            "qsa_isoproturon",
            "qsa_isoproturon_hts",
            "qsa_clothianidine",
            "qsa_clothianidine_hts",
            "qsa_imidaclopride",
            "qsa_imidaclopride_hts",
            "qsa_thiamethoxam",
            "qsa_thiamethoxam_hts",
            "qsa_acetamipride",
            "qsa_acetamipride_hts",
            "qsa_thiaclopride",
            "qsa_thiaclopride_hts",
            "qsa_triallate",
            "qsa_triallate_hts",
            "qsa_metsulfuron",
            "qsa_metsulfuron_hts",
            "qsa_florasulam",
            "qsa_florasulam_hts",
            "qsa_picolinafen",
            "qsa_picolinafen_hts",
            "qsa_propoxycarbazone",
            "qsa_propoxycarbazone_hts",
            "qsa_tribenuron",
            "qsa_tribenuron_hts",
            "qsa_sulfosulfuron",
            "qsa_sulfosulfuron_hts"
    };

    private static final String[] LABELS = new String[]{
            "Glyphosate",
            "Glyphosate hors traitement de semence",
            "S-métolachlore",
            "S-métolachlore hors traitement de semence",
            "Prosulfocarbe",
            "Prosulfocarbe hors traitement de semence",
            "Chlortoluron",
            "Chlortoluron hors traitement de semence",
            "Diflufenican",
            "Diflufenican hors traitement de semence",
            "Lambda-Cyhalothrine",
            "Lambda-Cyhalothrine hors traitement de semence",
            "Boscalid",
            "Boscalid hors traitement de semence",
            "Fluopyram",
            "Fluopyram hors traitement de semence",
            "Bixafen",
            "Bixafen hors traitement de semence",
            "Dicamba",
            "Dicamba hors traitement de semence",
            "Mancozeb",
            "Mancozeb hors traitement de semence",
            "Phosmet",
            "Phosmet hors traitement de semence",
            "Tebuconazole",
            "Tebuconazole hors traitement de semence",
            "Dimethenamid-P",
            "Dimethenamid-P hors traitement de semence",
            "Pendimethalin",
            "Pendimethalin hors traitement de semence",
            "Flufenacet",
            "Flufenacet hors traitement de semence",
            "Aclonifen",
            "Aclonifen hors traitement de semence",
            "Isoxaben",
            "Isoxaben hors traitement de semence",
            "Beflutamid",
            "Beflutamid hors traitement de semence",
            "Isoproturon",
            "Isoproturon hors traitement de semence",
            "Clothianidine",
            "Clothianidine hors traitement de semence",
            "Imidaclopride",
            "Imidaclopride hors traitement de semence",
            "Thiamethoxam",
            "Thiamethoxam hors traitement de semence",
            "Acétamipride",
            "Acétamipride hors traitement de semence",
            "Thiaclopride",
            "Thiaclopride hors traitement de semence",
            "Triallate",
            "Triallate hors traitement de semence",
            "Metsulfuron",
            "Metsulfuron hors traitement de semence",
            "Florasulam",
            "Florasulam hors traitement de semence",
            "Picolinafen",
            "Picolinafen hors traitement de semence",
            "Propoxycarbazone",
            "Propoxycarbazone hors traitement de semence",
            "Tribenuron",
            "Tribenuron hors traitement de semence",
            "Sulfosulfuron",
            "Sulfosulfuron hors traitement de semence"
    };

    public static final List<String> IDS_ACTIVE_SUBSTANCES = List.of(
            "811",  // Glyphosate
            "1137", // S-métolachlore
            "711",  // Prosulfocarbe
            "545",  // Chlortoluron
            "635",  // Diflufenican
            "259",  // Lambda-Cyhalothrine
            "472",  // Boscalid
            "723",  // Fluopyram
            "675",  // Bixafen
            "613",  // Dicamba
            "277",  // Mancozeb
            "850",  // Phosmet
            "779",  // Tebuconazole
            "641",  // Dimethenamid-P
            "961",  // Pendimethalin
            "1313", // Flufenacet
            "1054", // Aclonifen
            "248",  // Isoxaben
            "440",  // Beflutamid
            "246",  // Isoproturon
            "867",  // Clothianidine
            "688",  // Imidaclopride
            "796",  // Thiamethoxam
            "1050", // Acétamipride
            "841",  // Thiaclopride
            "154",  // Triallate
            "880",  // Metsulfuron
            "28",   // Florasulam
            "291",  // Picolinafen
            "708",  // Propoxycarbazone
            "160",  // Tribenuron
            "1412"  // Sulfosulfuron
    );

    protected Boolean[] substancesToDisplay = new Boolean[] {
            true,  // Glyphosate
            true,  // Glyphosate HTS
            true,  // S-métolachlore
            true,  // S-métolachlore HTS
            true,  // Prosulfocarbe
            true,  // Prosulfocarbe HTS
            true,  // Chlortoluron
            true,  // Chlortoluron HTS
            true,  // Diflufenican
            true,  // Diflufenican HTS
            true,  // Lambda-Cyhalothrine
            true,  // Lambda-Cyhalothrine HTS
            true,  // Boscalid
            true,  // Boscalid HTS
            true,  // Fluopyram
            true,  // Fluopyram HTS
            true,  // Bixafen
            true,  // Bixafen HTS
            true,  // Dicamba
            true,  // Dicamba HTS
            true,  // Mancozeb
            true,  // Mancozeb HTS
            true,  // Phosmet
            true,  // Phosmet HTS
            true,  // Tebuconazole
            true,  // Tebuconazole HTS
            true,  // Dimethenamid-P
            true,  // Dimethenamid-P HTS
            true,  // Pendimethalin
            true,  // Pendimethalin HTS
            true,  // Flufenacet
            true,  // Flufenacet HTS
            true,  // Aclonifen
            true,  // Aclonifen HTS
            true,  // Isoxaben
            true,  // Isoxaben HTS
            true,  // Beflutamid
            true,  // Beflutamid HTS
            true,  // Isoproturon
            true,  // Isoproturon HTS
            true,  // Clothianidine
            true,  // Clothianidine HTS
            true,  // Imidaclopride
            true,  // Imidaclopride HTS
            true,  // Thiamethoxam
            true,  // Thiamethoxam HTS
            true,  // Acétamipride
            true,  // Acétamipride HTS
            true,  // Thiaclopride
            true,  // Thiaclopride HTS
            true,  // Triallate
            true,  // Triallate HTS
            true,  // Metsulfuron
            true,  // Metsulfuron HTS
            true,  // Florasulam
            true,  // Florasulam HTS
            true,  // Picolinafen
            true,  // Picolinafen HTS
            true,  // Propoxycarbazone
            true,  // Propoxycarbazone HTS
            true,  // Tribenuron
            true,  // Tribenuron HTS
            true,  // Sulfosulfuron
            true   // Sulfosulfuron HTS
    };

    private final static List<Integer> NEONICOTINOID_SUBSTANCES_INDEXES = List.of(
            20, 21, 22, 23, 24
    );
    private final static List<Integer> SOIL_APPLIED_HERBICIDES_INDEXES = List.of(
            1, 2, 3, 4, 13, 14, 15, 16, 17, 18, 19, 25, 26, 27, 28, 29, 30, 31
    );


    protected boolean displayed = true;
    protected boolean isWithSeedingTreatment = true;
    protected boolean isWithoutSeedingTreatment = true;


    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        for (int i = 0; i < COLUMN_NAMES.length; i++) {
            indicatorNameToColumnName.put(getIndicatorLabel(i), COLUMN_NAMES[i]);
        }
        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(
            WriterContext writerContext,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedCropExecutionContext cropContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            PracticedCropCyclePhase phase) {

        if (interventionContext.isFictive()) {
            return newArray(LABELS.length, 0.0d);
        }

        // just to increment field counter
        double toolPSCi = getToolPSCi(interventionContext.getIntervention());

        return this.computeActiveSubstancesAmount(
                writerContext,
                toolPSCi,
                interventionContext,
                domainContext.getAllDomainSubstancesByAmm());
    }

    @Override
    public Double[] manageIntervention(
            WriterContext writerContext,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveCropExecutionContext cropContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {

        // just to increment field counter
        double toolPSCi = getToolPSCi(interventionContext.getIntervention());

        return this.computeActiveSubstancesAmount(
                writerContext,
                toolPSCi,
                interventionContext,
                domainContext.getAllDomainSubstancesByAmm());
    }

    private Double[] computeActiveSubstancesAmount(
            WriterContext writerContext,
            double psci,
            PerformanceInterventionContext interventionContext,
            Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> compositionForAMMsWithActiveSubstances) {

        IndicatorWriter writer = writerContext.getWriter();

        final Map<String, Double> qsaByIdSaWithSeedingTreatment = new LinkedHashMap<>();
        IDS_ACTIVE_SUBSTANCES.forEach(idSa -> qsaByIdSaWithSeedingTreatment.put(idSa, 0.0));

        interventionContext.getOptionalPesticidesSpreadingAction().ifPresent(pesticidesSpreadingAction0 -> {
            final double treatedSurface = pesticidesSpreadingAction0.getProportionOfTreatedSurface() / 100;
            final double phytoPsci = psci * treatedSurface;

            Collection<? extends AbstractPhytoProductInputUsage> pesticideProductInputUsages = pesticidesSpreadingAction0.getPesticideProductInputUsages();

            if (pesticideProductInputUsages != null) {
                pesticideProductInputUsages.stream()
                        .filter(u -> u.getDomainPhytoProductInput() != null)
                        .filter(u -> u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                        .forEach(
                                usage -> {
                                    ReferenceDoseDTO referenceDose = getReferenceDoseDTO(usage, interventionContext);

                                    final double inputAmount = this.computeAmountUsedInKgHa(pesticidesSpreadingAction0, usage, phytoPsci, referenceDose);
                                    computeInput(
                                            writerContext,
                                            compositionForAMMsWithActiveSubstances,
                                            pesticidesSpreadingAction0,
                                            usage,
                                            writer,
                                            inputAmount,
                                            qsaByIdSaWithSeedingTreatment,
                                            interventionContext, false);
                                }
                        );
            }
        });

        interventionContext.getOptionalBiologicalControlAction().ifPresent(biologicalControlAction0 -> {
            // Application du psci phyto pour les actions d'application de produit phytosanitaire
            final double treatedSurface = biologicalControlAction0.getProportionOfTreatedSurface() / 100;
            final double phytoPsci = psci * treatedSurface;

            // Parmi les produits sans AMM et macro-organismes, certains ont un AMM (qui peut être un vrai AMM
            // ou un numéro de dossier quelconque) que l'on peut retrouver dans refactatraitementsproduit,
            // donc pour ces produits on peut trouver des quantités de substances actives
            Collection<? extends AbstractPhytoProductInputUsage> biologicalProductInputUsages = biologicalControlAction0.getBiologicalProductInputUsages();
            if (biologicalProductInputUsages != null) {
                biologicalProductInputUsages.stream()
                        .filter(u -> u.getDomainPhytoProductInput() != null)
                        .filter(u -> u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                        .forEach(
                                usage -> {
                                    ReferenceDoseDTO referenceDose = getReferenceDoseDTO(usage, interventionContext);

                                    final double inputAmount = this.computeAmountUsedInKgHa(biologicalControlAction0, usage, phytoPsci, referenceDose);
                                    computeInput(
                                            writerContext,
                                            compositionForAMMsWithActiveSubstances,
                                            biologicalControlAction0,
                                            usage,
                                            writer,
                                            inputAmount,
                                            qsaByIdSaWithSeedingTreatment,
                                            interventionContext, false);
                                }
                        );
            }
        });

        final Map<String, Double> qsaByIdSaWithoutSeedingTreatment = new LinkedHashMap<>(qsaByIdSaWithSeedingTreatment);

        interventionContext.getOptionalSeedingActionUsage().ifPresent(action -> action.getSeedLotInputUsage()
                .stream()
                .filter(u -> u != null && u.getSeedingSpecies() != null)
                .flatMap(u -> u.getSeedingSpecies().stream())
                .filter(u -> u.getSeedProductInputUsages() != null)
                .flatMap(u -> u.getSeedProductInputUsages().stream())
                .filter(u -> u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                .forEach(
                        usage -> {
                            ReferenceDoseDTO referenceDose = getReferenceDoseDTO(usage, interventionContext);

                            double amount = this.computeAmountUsedInKgHa(action, usage, psci, referenceDose);
                            computeInput(
                                    writerContext,
                                    compositionForAMMsWithActiveSubstances,
                                    action,
                                    usage,
                                    writer,
                                    amount,
                                    qsaByIdSaWithSeedingTreatment,
                                    interventionContext,
                                    true);
                        }
                ));

        // Constitue le tableau des valeurs de l'indicateur
        // en colonne i : la valeur de l'indicateur avec traitement de semence, et en colonne i+1 la valeur
        // de l'indicateur hors traitement de semence (HTS).
        // Dans le tableau de retour il y a 2 fois plus d'éléments que le nombre de substances actives
        // traitées car à chaque fois on doit calculer la version avec et hors traitement de semence.
        final Double[] indicatorValues = new Double[LABELS.length];
        for (int i = 0; i < IDS_ACTIVE_SUBSTANCES.size(); i++) {
            final String idSa = IDS_ACTIVE_SUBSTANCES.get(i);

            indicatorValues[i * 2] = qsaByIdSaWithSeedingTreatment.getOrDefault(idSa, 0.0);
            indicatorValues[i * 2 + 1] = qsaByIdSaWithoutSeedingTreatment.getOrDefault(idSa, 0.0);
        }

        return indicatorValues;
    }

    private void computeInput(
            WriterContext writerContext,
            Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> compositionForAMMsWithActiveSubstances,
            AbstractAction action,
            AbstractPhytoProductInputUsage usage,
            IndicatorWriter writer,
            double amount,
            Map<String, Double> qsaByIdSa, PerformanceInterventionContext interventionContext,
            boolean withSeedingTreatment) {

        final String codeAmm = usage.getDomainPhytoProductInput().getRefInput().getCode_AMM();

        compositionForAMMsWithActiveSubstances.getOrDefault(codeAmm, List.of())
                .stream()
                .filter(csa -> IndicatorActiveSubstanceAmount.IDS_ACTIVE_SUBSTANCES.contains(csa.getId_sa()))
                .forEach(activeSubstance -> {

                    final double coefToKgHa = this.conversionRatesByUnit.getOrDefault(activeSubstance.getUnite_sa(), 0.0);

                    final int idSaIndex = IDS_ACTIVE_SUBSTANCES.indexOf(activeSubstance.getId_sa());

                    final int withSeedingTreatmentIndex = idSaIndex * 2;
                    final int withoutSeedingTreatmentIndex = idSaIndex * 2 + 1;

                    final boolean isDisplayedWithSeedingTreatment = isDisplayed(ExportLevel.INPUT, withSeedingTreatmentIndex);
                    final boolean isDisplayedWithoutSeedingTreatment = isDisplayed(ExportLevel.INPUT, withoutSeedingTreatmentIndex);

                    final double currentSubstanceValue = activeSubstance.getDose_sa() * amount * coefToKgHa;
                    final double valueWithSeedingTreatment = currentSubstanceValue;
                    final double valueWithoutSeedingTreatment = withSeedingTreatment ? 0.0 : currentSubstanceValue;

                    if (NEONICOTINOID_SUBSTANCES_INDEXES.contains(idSaIndex)) {
                        interventionContext
                                .getNeonicotinoidAmounts()
                                .merge(Pair.of(action, usage), currentSubstanceValue, Double::sum);
                    }
                    if (SOIL_APPLIED_HERBICIDES_INDEXES.contains(idSaIndex)) {
                        interventionContext
                                .getSoilAppliedHerbicideAmounts()
                                .merge(Pair.of(action, usage), currentSubstanceValue, Double::sum);
                    }

                    if (isDisplayedWithSeedingTreatment) {
                        writeInputValue(writerContext, action, usage, writer, withSeedingTreatmentIndex, valueWithSeedingTreatment);
                    }
                    if (isDisplayedWithoutSeedingTreatment) {
                        writeInputValue(writerContext, action, usage, writer, withoutSeedingTreatmentIndex, valueWithoutSeedingTreatment);
                    }

                    qsaByIdSa.merge(activeSubstance.getId_sa(),
                            valueWithSeedingTreatment,
                            Double::sum
                    );
                });
    }

    private void writeInputValue(WriterContext writerContext, AbstractAction action, AbstractPhytoProductInputUsage usage, IndicatorWriter writer, int indicatorIndex, double value) {
        // dose
        String usageTopiaId = usage.getTopiaId();
        incrementAngGetTotalFieldCounterForTargetedId(usageTopiaId);

        if (usage.getQtAvg() == null) {
            addMissingFieldMessage(
                    usageTopiaId,
                    messageBuilder.getMissingDoseMessage(
                            usage.getInputType(),
                            MissingMessageScope.INPUT));
        }

        Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(usageTopiaId);
        String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(
                usageTopiaId,
                MissingMessageScope.INPUT);

        // write input sheet
        writer.writeInputUsage(
                writerContext.getIts(),
                writerContext.getIrs(),
                getIndicatorCategory(),
                getIndicatorLabel(indicatorIndex),
                usage,
                action,
                writerContext.getEffectiveIntervention(),
                writerContext.getPracticedIntervention(),
                writerContext.getCodeAmmBioControle(),
                writerContext.getAnonymizeDomain(),
                writerContext.getAnonymizeGrowingSystem(),
                writerContext.getPlot(),
                writerContext.getZone(),
                writerContext.getPracticedSystem(),
                writerContext.getCroppingPlanEntry(),
                writerContext.getPracticedPhase(),
                writerContext.getSolOccupationPercent(),
                writerContext.getEffectivePhase(),
                writerContext.getRank(),
                writerContext.getPreviousPlanEntry(),
                writerContext.getIntermediateCrop(),
                this.getClass(),
                value,
                reliabilityIndexForInputId,
                reliabilityCommentForInputId,
                "",
                "",
                "",
                writerContext.getGroupesCiblesByCode());
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return true;
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, "Indicator.category.specificActiveSubstances");
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, "Indicator.label.activeSubstances").formatted(LABELS[i]);
    }

    @Override
    public boolean isDisplayed(ExportLevel atLevel, int i) {
        return displayed && substancesToDisplay[i];
    }

    public void init(
            IndicatorFilter filter,
            Map<String, Double> coeffsConversionVersKgHa) {

        displayed = filter != null;
        if (displayed) {
            this.isWithSeedingTreatment = filter.getWithSeedingTreatment();
            this.isWithoutSeedingTreatment = filter.getWithoutSeedingTreatment();
        }

        resetSubstanceToDisplay();

        if (filter != null && filter.getActiveSubstances() != null) {
            for (ActiveSubstance substance : filter.getActiveSubstances()) {
                switch (substance) {
                    case GLYPHOSATE -> this.displaySubstanceWithBaseIndex(0);
                    case S_METOLACHLORE -> this.displaySubstanceWithBaseIndex(2);
                    case PROSULFOCARBE -> this.displaySubstanceWithBaseIndex(4);
                    case CHLORTOLURON -> this.displaySubstanceWithBaseIndex(6);
                    case DIFLUFENICAN -> this.displaySubstanceWithBaseIndex(8);
                    case LAMBDA_CYHALOTHRINE -> this.displaySubstanceWithBaseIndex(10);
                    case BOSCALID -> this.displaySubstanceWithBaseIndex(12);
                    case FLUOPYRAM -> this.displaySubstanceWithBaseIndex(14);
                    case BIXAFEN -> this.displaySubstanceWithBaseIndex(16);
                    case DICAMBA -> this.displaySubstanceWithBaseIndex(18);
                    case MANCOZEB -> this.displaySubstanceWithBaseIndex(20);
                    case PHOSMET -> this.displaySubstanceWithBaseIndex(22);
                    case TEBUCONAZOLE -> this.displaySubstanceWithBaseIndex(24);
                    case DIMETHENAMID_P -> this.displaySubstanceWithBaseIndex(26);
                    case PENDIMETHALIN -> this.displaySubstanceWithBaseIndex(28);
                    case FLUFENACET -> this.displaySubstanceWithBaseIndex(30);
                    case ACLONIFEN -> this.displaySubstanceWithBaseIndex(32);
                    case ISOXABEN -> this.displaySubstanceWithBaseIndex(34);
                    case BEFLUTAMID -> this.displaySubstanceWithBaseIndex(36);
                    case ISOPROTURON -> this.displaySubstanceWithBaseIndex(38);
                    case CLOTHIANIDINE -> this.displaySubstanceWithBaseIndex(40);
                    case IMIDACLOPRIDE -> this.displaySubstanceWithBaseIndex(42);
                    case THIAMETHOXAM -> this.displaySubstanceWithBaseIndex(44);
                    case ACETAMIPRIDE -> this.displaySubstanceWithBaseIndex(46);
                    case THIACLOPRIDE -> this.displaySubstanceWithBaseIndex(48);
                    case TRIALLATE -> this.displaySubstanceWithBaseIndex(50);
                    case METSULFURON -> this.displaySubstanceWithBaseIndex(52);
                    case FLORASULAM -> this.displaySubstanceWithBaseIndex(54);
                    case PICOLINAFEN -> this.displaySubstanceWithBaseIndex(56);
                    case PROPOXYCARBAZONE -> this.displaySubstanceWithBaseIndex(58);
                    case TRIBENURON -> this.displaySubstanceWithBaseIndex(60);
                    case SULFOSULFURON -> this.displaySubstanceWithBaseIndex(62);
                }
            }
        }

        conversionRatesByUnit = coeffsConversionVersKgHa;
    }

    private void displaySubstanceWithBaseIndex(int index) {
        if (isWithSeedingTreatment) {
            this.substancesToDisplay[index] = true;
        }
        if (isWithoutSeedingTreatment) {
            this.substancesToDisplay[index + 1] = true;
        }
    }


    protected void resetSubstanceToDisplay() {
        substancesToDisplay = new Boolean[] {
                false,  // Glyphosate
                false,  // Glyphosate HTS
                false,  // S-métolachlore
                false,  // S-métolachlore HTS
                false,  // Prosulfocarbe
                false,  // Prosulfocarbe HTS
                false,  // Chlortoluron
                false,  // Chlortoluron HTS
                false,  // Diflufenican
                false,  // Diflufenican HTS
                false,  // Lambda-Cyhalothrine
                false,  // Lambda-Cyhalothrine HTS
                false,  // Boscalid
                false,  // Boscalid HTS
                false,  // Fluopyram
                false,  // Fluopyram HTS
                false,  // Bixafen
                false,  // Bixafen HTS
                false,  // Dicamba
                false,  // Dicamba HTS
                false,  // Mancozeb
                false,  // Mancozeb HTS
                false,  // Phosmet
                false,  // Phosmet HTS
                false,  // Tebuconazole
                false,  // Tebuconazole HTS
                false,  // Dimethenamid-P
                false,  // Dimethenamid-P HTS
                false,  // Pendimethalin
                false,  // Pendimethalin HTS
                false,  // Flufenacet
                false,  // Flufenacet HTS
                false,  // Aclonifen
                false,  // Aclonifen HTS
                false,  // Isoxaben
                false,  // Isoxaben HTS
                false,  // Beflutamid
                false,  // Beflutamid HTS
                false,  // Isoproturon
                false,  // Isoproturon HTS
                false,  // Clothianidine
                false,  // Clothianidine HTS
                false,  // Imidaclopride
                false,  // Imidaclopride HTS
                false,  // Thiamethoxam
                false,  // Thiamethoxam HTS
                false,  // Acétamipride
                false,  // Acétamipride HTS
                false,  // Thiaclopride
                false,  // Thiaclopride HTS
                false,  // Triallate
                false,  // Triallate HTS
                false,  // Metsulfuron
                false,  // Metsulfuron HTS
                false,  // Florasulam
                false,  // Florasulam HTS
                false,  // Picolinafen
                false,  // Picolinafen HTS
                false,  // Propoxycarbazone
                false,  // Propoxycarbazone HTS
                false,  // Tribenuron
                false,  // Tribenuron HTS
                false,  // Sulfosulfuron
                false   // Sulfosulfuron HTS
        };
    }
}
