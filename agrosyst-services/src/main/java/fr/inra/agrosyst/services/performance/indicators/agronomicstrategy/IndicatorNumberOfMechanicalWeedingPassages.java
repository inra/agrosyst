package fr.inra.agrosyst.services.performance.indicators.agronomicstrategy;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.TypeTravailSol;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * Nombre de passages de désherbage mécanique.
 * <p>
 * Présentation de l’indicateur
 * <p>
 * Le nombre de passage de Désherbage mécanique est calculé à toutes les échelles hormis l'intrant et concerne les
 * interventions mobilisant une combinaison d'outil avec un outil de type de TCS dans la colonne Type_travail_sol du
 * référentiel de travail du sol et la valeur O dans la colonne désherbage mécanique.
 * <p>
 * A l'échelle de l'intervention le nombre de passage de désherbage mécanique correspond au PSCI.
 * <p>
 * Pour les interventions mobilisant une combinaison d'outil avec un outil dont la valeur de la colonne Type_travail_sol
 * du référentiel travail du sol est TCS et la valeur de la colonne désherbage_mécanique est N alors le nombre de passage de désherbage mécanique n'est pas calculé.
 * <p>
 * Pour les interventions mobilisant une combinaison d'outil avec un outil dont la valeur de la colonne Type_travail_sol
 * du référentiel travail du sol est TCS et la valeur de la colonne déhserbage_mécanique est O alors il faut comparer la date du passage de l'outil à la date du semis.
 * <p>
 * Nb_passage_désherbage_mécanique = PSCI
 * <p>
 * Si le matériel utilisé n'est pas de type TCS alors l'indicateur prend la valeur 0.
 * <p>
 * De la culture à la parcelle, l'indicateur se calcule en faisant l'addition des valeurs obtenues à l'échelles inférieure.
 * <p>
 * A l'échelle du SdC, il s'agit de faire une agrégation classique en faisant la moyenne des valeurs de l'indicateur à l'échelle de l'intervention de type désherbage mécanique pour le SdC.
 * Formule:
 * <p>
 * Nb_passage_désherbage_mécanique = (Σ (indicateur_echelle_intervention_désherbage_mécanique))/Nb_intervention_déhserbage_mécanique
 * <p>
 * Avec Nb_intervention_désherbage_mécanique = le nombre d'intervention de type désherbage mécanique dans le SdC.
 *
 * @author Geoffroy Gley (gley@codelutin.com)
 */
public class IndicatorNumberOfMechanicalWeedingPassages extends AbstractIndicator {

    private static final String COLUMN_NAME = "nombre_de_passages_desherbage_mecanique";

    private static final String[] LABELS = new String[] {
            "Indicator.label.nbOfMechanicalWeedingPassages"
    };

    public IndicatorNumberOfMechanicalWeedingPassages() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, COLUMN_NAME + "_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, COLUMN_NAME + "_detail_champs_non_renseig");
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        return indicatorNameToColumnName;
    }

    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN
    );

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {

        final Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> correspondanceByRefMateriel = interventionContext.getCorrespondanceByRefMateriel();

        if (correspondanceByRefMateriel == null || interventionContext.isFictive() ||
                interventionContext.getToolsCoupling() == null ||
                interventionContext.getToolsCoupling().getEquipments().stream().filter(Objects::nonNull).noneMatch(e -> correspondanceByRefMateriel.containsKey(e.getRefMateriel()))) {
            return newArray(1, 0.0d);
        }

        // pour ne pas avoir 0, car il n'y a pas de données utilisateur sur cet indicateur
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId());

        final ToolsCoupling toolsCoupling = interventionContext.getToolsCoupling();

        final Set<RefCorrespondanceMaterielOutilsTS> refCorrespondanceMaterielOutilsTS = toolsCoupling != null ? findRefCorrespondanceMaterielOutilsTS(toolsCoupling, correspondanceByRefMateriel) : Collections.emptySet();
        // Sans semis, on ne peut pas considérer que ce soit un désherbage mécanique.
        boolean isSeedingIntervention = cropContext.getInterventionExecutionContexts().stream()
                .map(PerformancePracticedInterventionExecutionContext::getIntervention)
                .filter(intervention -> !intervention.isIntermediateCrop())
                .anyMatch(intervention -> intervention.getType() == AgrosystInterventionType.SEMIS);

        if (isSeedingIntervention && refCorrespondanceMaterielOutilsTS.stream().anyMatch(RefCorrespondanceMaterielOutilsTS::isDesherbage_mecanique)) {
            final Optional<Double> averageDayOfMostRecentSeedingIntervention = cropContext.getInterventionExecutionContexts().stream()
                    .map(PerformancePracticedInterventionExecutionContext::getIntervention)
                    .filter(intervention -> intervention.getType() == AgrosystInterventionType.SEMIS)
                    .filter(intervention -> !intervention.isIntermediateCrop())
                    .map(this::computeAverageDayOfYear)
                    .max(Comparator.naturalOrder());

            final Double currentInterventionAverageDay = computeAverageDayOfYear(interventionContext.getIntervention());
            if (averageDayOfMostRecentSeedingIntervention.isEmpty() || currentInterventionAverageDay > averageDayOfMostRecentSeedingIntervention.get()) {
                return newArray(1, this.getToolPSCi(interventionContext.getIntervention()));
            }
        }

        return newArray(1, 0.0d);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {

        final Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> correspondanceByRefMateriel = interventionContext.getCorrespondanceByRefMateriel();

        if (correspondanceByRefMateriel == null ||
                interventionContext.getToolsCoupling() == null ||
                interventionContext.getToolsCoupling().getEquipments().stream().filter(Objects::nonNull).noneMatch(e -> correspondanceByRefMateriel.containsKey(e.getRefMateriel()))) {
            return newArray(LABELS.length, 0.0d);
        }

        // pour ne pas avoir 0, car il n'y a pas de données utilisateur sur cet indicateur
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId());

        final ToolsCoupling toolsCoupling = interventionContext.getToolsCoupling();

        final Set<RefCorrespondanceMaterielOutilsTS> refCorrespondanceMaterielOutilsTS = toolsCoupling != null ? findRefCorrespondanceMaterielOutilsTS(toolsCoupling, correspondanceByRefMateriel) : Collections.emptySet();

        // Sans semis, on ne peut pas considérer que ce soit un désherbage mécanique.
        boolean isSeedingIntervention = cropContext.getInterventionExecutionContexts().stream()
                .map(PerformanceEffectiveInterventionExecutionContext::getIntervention)
                .filter(intervention -> !intervention.isIntermediateCrop())
                .anyMatch(intervention -> intervention.getType() == AgrosystInterventionType.SEMIS);

        if (isSeedingIntervention && refCorrespondanceMaterielOutilsTS.stream().anyMatch(RefCorrespondanceMaterielOutilsTS::isDesherbage_mecanique)) {

            final Optional<Date> averageDayOfMostRecentSeedingIntervention = cropContext.getInterventionExecutionContexts().stream()
                    .map(PerformanceEffectiveInterventionExecutionContext::getIntervention)
                    .filter(intervention -> !intervention.isIntermediateCrop())
                    .filter(intervention -> intervention.getType() == AgrosystInterventionType.SEMIS)
                    .map(this::computeAverageDay)
                    .max(Comparator.naturalOrder());

            final Date currentInterventionAverageDay = computeAverageDay(interventionContext.getIntervention());
            if (averageDayOfMostRecentSeedingIntervention.isEmpty() || currentInterventionAverageDay.compareTo(averageDayOfMostRecentSeedingIntervention.get()) > 0) {
                return newArray(1, this.getToolPSCi(interventionContext.getIntervention()));
            }


        }

        return newArray(1, 0.0d);
    }

    private Set<RefCorrespondanceMaterielOutilsTS> findRefCorrespondanceMaterielOutilsTS(ToolsCoupling toolsCoupling, Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> correspondanceByRefMateriel) {
        return toolsCoupling.getEquipments().stream()
                .map(Equipment::getRefMateriel)
                .filter(correspondanceByRefMateriel::containsKey)
                .map(correspondanceByRefMateriel::get)
                .filter(refCorrespondance -> refCorrespondance.getTypeTravailSol() == TypeTravailSol.TCS)
                .collect(Collectors.toSet());
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorCategory() {
        return INDICATOR_CATEGORY_AGRONOMIC_STRATEGY;
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) && displayed;
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
    }
}
