package fr.inra.agrosyst.services.performance.indicators.fertilization;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;

import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

public abstract class AbstractIndicatorFertilization extends AbstractIndicator {

    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.INPUT,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    protected static final Map<MineralProductUnit, Double> COEF_CONVERT_MINERAL_PRODUCT_USAGE_TO_KG_HA = Map.of(
            MineralProductUnit.KG_HA, 1.0,
            MineralProductUnit.L_HA, 1.0,
            MineralProductUnit.T_HA, 1000.0
    );

    protected static final Map<OrganicProductUnit, Double> COEF_CONVERT_ORGANIC_PRODUCT_USAGE_TO_KG_HA = Map.of(
            OrganicProductUnit.KG_HA, 1.0,
            OrganicProductUnit.M_CUB_HA, 1000.0,
            OrganicProductUnit.T_HA, 1000.0
    );


    protected final String[] labels;

    protected AbstractIndicatorFertilization(String[] labels) {
        this.labels = labels;
    }


    protected void writeInputUsageForSubstance(WriterContext writerContext, AbstractInputUsage inputUsage, AbstractAction abstractAction, double substanceAmount, int rank) {
        if (isDisplayed(ExportLevel.INPUT, rank)) {
            String usageTopiaId;
            Integer reliabilityIndexForInputId = null;
            String reliabilityCommentForInputId = null;
            if (inputUsage != null) {
                usageTopiaId = inputUsage.getTopiaId();
                reliabilityIndexForInputId = getReliabilityIndexForTargetedId(usageTopiaId);
                reliabilityCommentForInputId = getReliabilityCommentForTargetedId(
                        usageTopiaId,
                        MissingMessageScope.INPUT);
            }

            writerContext.getWriter()
                    .writeInputUsage(
                            writerContext.getIts(),
                            writerContext.getIrs(),
                            getIndicatorCategory(),
                            getIndicatorLabel(rank),
                            inputUsage,
                            abstractAction,
                            writerContext.getEffectiveIntervention(),
                            writerContext.getPracticedIntervention(),
                            writerContext.getCodeAmmBioControle(),
                            writerContext.getAnonymizeDomain(),
                            writerContext.getAnonymizeGrowingSystem(),
                            writerContext.getPlot(),
                            writerContext.getZone(),
                            writerContext.getPracticedSystem(),
                            writerContext.getCroppingPlanEntry(),
                            writerContext.getPracticedPhase(),
                            writerContext.getSolOccupationPercent(),
                            writerContext.getEffectivePhase(),
                            writerContext.getRank(),
                            writerContext.getPreviousPlanEntry(),
                            writerContext.getIntermediateCrop(),
                            this.getClass(),
                            substanceAmount,
                            reliabilityIndexForInputId,
                            reliabilityCommentForInputId,
                            "",
                            "",
                            "",
                            writerContext.getGroupesCiblesByCode()
                    );
        }
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, this.labels[i]);
    }

}
