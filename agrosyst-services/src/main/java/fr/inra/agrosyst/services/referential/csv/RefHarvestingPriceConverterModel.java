package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceConverter;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceConverterImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefHarvestingPriceConverterModel extends AbstractDestinationAndPriceModel<RefHarvestingPriceConverter> implements ExportModel<RefHarvestingPriceConverter> {

    private static final Log LOGGER = LogFactory.getLog(RefHarvestingPriceConverterModel.class);

    public RefHarvestingPriceConverterModel() {
        super(CSV_SEPARATOR);
    }

    public RefHarvestingPriceConverterModel(RefDestinationTopiaDao refDestinationsDao, RefEspeceTopiaDao refEspeceDao) {
        super(CSV_SEPARATOR);
        super.refDestinationsDao = refDestinationsDao;
        super.refEspeceDao = refEspeceDao;
        getRefDestinationByCodes();
        getUpperCodeEspeceBotaniqueToCodeEspeceBotanique();

        newMandatoryColumn("code_destination_A", RefHarvestingPriceConverter.PROPERTY_CODE_DESTINATION__A, CODE_DESTINATION_A_PARSER);
        newMandatoryColumn("Unité_rendement", RefHarvestingPriceConverter.PROPERTY_YEALD_UNIT, YEALD_UNIT_PARSER);
        newMandatoryColumn("Unité_prix", RefHarvestingPriceConverter.PROPERTY_PRICE_UNIT, PRICE_UNIT_PARSER);
        newMandatoryColumn("convertionRate", RefHarvestingPriceConverter.PROPERTY_CONVERTION_RATE, DOUBLE_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefHarvestingPriceConverter.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefHarvestingPriceConverter, Object>> getColumnsForExport() {
        ModelBuilder<RefDestination> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("code_destination_A", RefHarvestingPriceConverter.PROPERTY_CODE_DESTINATION__A);
        modelBuilder.newColumnForExport("Unité_rendement", RefHarvestingPriceConverter.PROPERTY_YEALD_UNIT, YEALD_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("Unité_prix", RefHarvestingPriceConverter.PROPERTY_PRICE_UNIT, PRICE_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("convertionRate", RefHarvestingPriceConverter.PROPERTY_CONVERTION_RATE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefHarvestingPriceConverter.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefHarvestingPriceConverter newEmptyInstance() {
        RefHarvestingPriceConverter refHarvestingPriceConverter = new RefHarvestingPriceConverterImpl();
        refHarvestingPriceConverter.setActive(true);
        return refHarvestingPriceConverter;
    }
}
