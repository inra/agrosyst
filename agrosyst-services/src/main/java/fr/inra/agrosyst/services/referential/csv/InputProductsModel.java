package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import org.nuiton.csv.ValueGetter;

import java.util.Map;

public class InputProductsModel extends AbstractTranslatableExportModel<RefActaTraitementsProduit> {

    public InputProductsModel(Map<String, String> translations, Map<Integer, String> actionTypeByTraitementId) {
        super(translations);
        //countryTrigram;type_action;nom_traitement;nom_produit;code_amm;bio-controle
        newColumnForExport(translateColumnName("countryTrigram"), (ValueGetter<RefActaTraitementsProduit, String>) r -> r.getRefCountry().getTrigram());
        newColumnForExport(translateColumnName("actionType"), (ValueGetter<RefActaTraitementsProduit, String>) r -> actionTypeByTraitementId.get(r.getId_traitement()));
        newColumnForExport(RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT);
        newColumnForExport(RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT);
        newColumnForExport(RefActaTraitementsProduit.PROPERTY_CODE__AMM);
        newColumnForExport(RefActaTraitementsProduit.PROPERTY_NODU, getTranslated_O_N_Formatter());
        newColumnForExport(RefActaTraitementsProduit.PROPERTY_ETAT_USAGE);
        newColumnForExport(RefActaTraitementsProduit.PROPERTY_DATE_RETRAIT_PRODUIT, AbstractAgrosystModel.LOCAL_DATE_FORMATTER);
        newColumnForExport(RefActaTraitementsProduit.PROPERTY_DATE_AUTORISATION_PRODUIT, AbstractAgrosystModel.LOCAL_DATE_FORMATTER);
    }

}
