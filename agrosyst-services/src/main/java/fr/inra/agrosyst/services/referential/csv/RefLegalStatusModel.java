package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefLegalStatus;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatusImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

public class RefLegalStatusModel extends AbstractAgrosystModel<RefLegalStatus> implements ExportModel<RefLegalStatus> {

    /**
     * String to integer converter.
     */
    protected static final ValueParser<Boolean> INT_TO_BOOLEAN_PARSER = value -> {
        boolean result = "1".equals(value);
        return result;
    };

    public RefLegalStatusModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("code INSEE", RefLegalStatus.PROPERTY_CODE__INSEE, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("libelle INSEE", RefLegalStatus.PROPERTY_LIBELLE__INSEE);
        newMandatoryColumn("Société", RefLegalStatus.PROPERTY_SOCIETE, INT_TO_BOOLEAN_PARSER);
        newMandatoryColumn("Source", RefLegalStatus.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefLegalStatus.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefLegalStatus, Object>> getColumnsForExport() {
        ModelBuilder<RefLegalStatus> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("code INSEE", RefLegalStatus.PROPERTY_CODE__INSEE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("libelle INSEE", RefLegalStatus.PROPERTY_LIBELLE__INSEE);
        modelBuilder.newColumnForExport("Société", RefLegalStatus.PROPERTY_SOCIETE, (ValueFormatter<Boolean>) value -> {
            if (value) {
                return "1";
            } else {
                return "0";
            }
        });
        modelBuilder.newColumnForExport("Source", RefLegalStatus.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefLegalStatus.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefLegalStatus newEmptyInstance() {
        RefLegalStatus refLegalStatus = new RefLegalStatusImpl();
        refLegalStatus.setActive(true);
        return refLegalStatus;
    }

}
