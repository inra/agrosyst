package fr.inra.agrosyst.services.referential.csv.importApi;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSpcRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSpcRootImpl;
import fr.inra.agrosyst.services.referential.csv.AbstractAgrosystModel;
import fr.inra.agrosyst.services.referential.json.RefActaDosageSpcRootExport;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Created by davidcosse on 05/01/16.
 */
public class RefActaDosageSpcRootExportModel extends AbstractAgrosystModel<RefActaDosageSpcRoot> implements ExportModel<RefActaDosageSpcRootExport> {

    public RefActaDosageSpcRootExportModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("idProduit", RefActaDosageSpcRoot.PROPERTY_ID_PRODUIT);
        newMandatoryColumn("nomProduit", RefActaDosageSpcRoot.PROPERTY_NOM_PRODUIT);
        newMandatoryColumn("idTraitement", RefActaDosageSpcRoot.PROPERTY_ID_TRAITEMENT, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("codeTraitement", RefActaDosageSpcRoot.PROPERTY_CODE_TRAITEMENT);
        newMandatoryColumn("id_culture", RefActaDosageSpcRoot.PROPERTY_ID_CULTURE, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("nom_culture", RefActaDosageSpcRoot.PROPERTY_NOM_CULTURE);
        newMandatoryColumn("remarque_culture", RefActaDosageSpcRoot.PROPERTY_REMARQUE_CULTURE);
        newMandatoryColumn("dosage_spc_valeur", RefActaDosageSpcRoot.PROPERTY_DOSAGE_SPC_VALEUR);
        newMandatoryColumn("dosage_spc_unite", RefActaDosageSpcRoot.PROPERTY_DOSAGE_SPC_UNITE);
        newMandatoryColumn("dosage_spc_commentaire", RefActaDosageSpcRoot.PROPERTY_DOSAGE_SPC_COMMENTAIRE);
        newOptionalColumn(COLUMN_ACTIVE, RefActaDosageSpcRoot.PROPERTY_ACTIVE, ACTIVE_PARSER);
        newIgnoredColumn("status");
    }

    @Override
    public RefActaDosageSpcRoot newEmptyInstance() {
        RefActaDosageSpcRoot refActaDosageSpcRoot = new RefActaDosageSpcRootImpl();
        refActaDosageSpcRoot.setActive(true);
        return refActaDosageSpcRoot;
    }

    @Override
    public Iterable<ExportableColumn<RefActaDosageSpcRootExport, Object>> getColumnsForExport() {
        ModelBuilder<RefActaDosageSpcRoot> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("idProduit", RefActaDosageSpcRoot.PROPERTY_ID_PRODUIT);
        modelBuilder.newColumnForExport("nomProduit", RefActaDosageSpcRoot.PROPERTY_NOM_PRODUIT);
        modelBuilder.newColumnForExport("idTraitement", RefActaDosageSpcRoot.PROPERTY_ID_TRAITEMENT, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("codeTraitement", RefActaDosageSpcRoot.PROPERTY_CODE_TRAITEMENT);
        modelBuilder.newColumnForExport("id_culture", RefActaDosageSpcRoot.PROPERTY_ID_CULTURE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("nom_culture", RefActaDosageSpcRoot.PROPERTY_NOM_CULTURE);
        modelBuilder.newColumnForExport("remarque_culture", RefActaDosageSpcRoot.PROPERTY_REMARQUE_CULTURE);
        modelBuilder.newColumnForExport("dosage_spc_valeur", RefActaDosageSpcRoot.PROPERTY_DOSAGE_SPC_VALEUR);
        modelBuilder.newColumnForExport("dosage_spc_unite", RefActaDosageSpcRoot.PROPERTY_DOSAGE_SPC_UNITE);
        modelBuilder.newColumnForExport("dosage_spc_commentaire", RefActaDosageSpcRoot.PROPERTY_DOSAGE_SPC_COMMENTAIRE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefActaDosageSpcRoot.PROPERTY_ACTIVE, T_F_FORMATTER);
        modelBuilder.newColumnForExport("status", RefActaDosageSpcRootExport.PROPERTY_STATUS, RefActaDosageSaRootExportModel.STATUS_FORMATTER);

        return (Iterable) modelBuilder.getColumnsForExport();
    }
}
