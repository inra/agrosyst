package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.IrrigationInputUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefInputPriceImpl;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static org.nuiton.i18n.I18n.l;

/**
 * Les charges opérationnelles réelles sont exprimées en €/ha.
 * Elles correspondent aux dépenses liées à l’achat des intrants (semences et plants,
 * produits fertilisants minéraux  et  organiques, traitement de semences et plants,
 * irrigation, produits phytosanitaires, produits de lutte biologique, autres intrants, substrats, pots).
 * <p>
 * Le calcul se fait sur la base des prix saisis par l’utilisateur.
 * <p>
 * Les interventions concernées par cet indicateur sont toutes les interventions
 * contenant un intrant et/ou une action de type « Semis » et/ou une action de
 * type « Irrigation ».
 * <p>
 *
 * <p>
 * Cette classe est une composante de {@link IndicatorOperatingExpenses} chargée de calculer les charges relatives au semis.
 * </p>
 * Rappel de la formule globale de calcul:
 *
 * <pre>
 * CI réel_i:
 *
 * = PSCi * ( sum(Q_ev * PA_ev) + sum(Q_j * PA_j) + sum(Q_e * PA_e) + sum(Q_a * PA_a)) + PSCi_phyto * sum(Q_k * PA_k) + PSCi_luttebio * sum(Q_h * PA_h)
 *
 * Avec :
 * - CI réel_i : charges intrants de l’intervention i
 * - PSCi (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i.
 *   PSCi est calculé sur la base des données saisies par l’utilisateur.
 *
 * - Irrigation
 *   - Q_e (diverses unités) : quantité d’eau déclarée dans l’action de type Irrigation au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *   - PA_e (€/m³) : prix d’achat de l’eau.
 *
 *     Si unité = €/ha, alors charge de l'intervention = PSCi x Prix en €/ha
 *
 * </pre>
 *
 * @author David Cossé (cosse@codelutin.com)
 */
public class IndicatorIrrigationOperatingExpenses extends IndicatorInputProductOperatingExpenses {

    protected static final Log LOGGER = LogFactory.getLog(IndicatorIrrigationOperatingExpenses.class);

    protected static final int M3_CONVERSION = 10;

    public IndicatorIrrigationOperatingExpenses(boolean displayed, boolean computeReal, boolean computStandardised, Locale locale) {
        super(displayed, computeReal, computStandardised, locale);
    }

    @Override
    public Pair<Optional<InputPrice>, Optional<InputRefPrice>> getDomainInputPrice(
            AbstractInputUsage usage,
            RefCampaignsInputPricesByDomainInput refCampaignsInputPricesByDomainInputAndCampaigns) {

        final DomainIrrigationInput input = ((IrrigationInputUsage) usage).getDomainIrrigationInput();
        final InputPrice inputPrice = input.getInputPrice();
        Optional<InputRefPrice> inputRePrice = refCampaignsInputPricesByDomainInputAndCampaigns.irrigRefPriceForInput().get(input);
        inputRePrice = inputRePrice == null ? Optional.empty() : inputRePrice;
        return Pair.of(Optional.ofNullable(inputPrice), inputRePrice);
    }

    // commons
    protected Pair<Double, Double> computeIrrigIndicator(
            WriterContext writerContext,
            Optional<InputRefPrice> optionalRefPrixIrrig,
            IrrigationAction action,
            IrrigationInputUsage irrigationInputUsage,
            double psci,
            String interventionId,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        incrementAngGetTotalFieldCounterForTargetedId(action.getTopiaId());//WaterQuantityAverage, can not be null

        final DomainIrrigationInput domainIrrigationInput = irrigationInputUsage.getDomainIrrigationInput();

        InputPrice price = domainIrrigationInput.getInputPrice();

        if (price == null && optionalRefPrixIrrig.isEmpty()) {

            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            addMissingFieldMessage(interventionId, messageBuilder.getMissingIrrigationPriceMessage());
            return IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;
        }

        // exprimé en mm, soit 1mm = 1l/m2 = 10000l/ha = 10m3/ha
        double da = action.getWaterQuantityAverage();

        double priceValue;
        final PriceUnit priceUnit;

        final double standardizedPrice;
        final PriceUnit standardizedPriceUnit;

        if (optionalRefPrixIrrig.isPresent()) {
            final PriceAndUnit refPriceAndUnit = optionalRefPrixIrrig.get().averageRefPrice();
            standardizedPrice = refPriceAndUnit.value();
            standardizedPriceUnit = refPriceAndUnit.unit();
        } else {
            standardizedPrice = IndicatorOperatingExpenses.DEFAULT_STANDARDIZED_VALUE;
            standardizedPriceUnit = PricesService.DEFAULT_PRICE_UNIT;
        }

        if (price != null) {
            incrementAngGetTotalFieldCounterForTargetedId(action.getTopiaId());// price
            priceValue = price.getPrice() != null ? price.getPrice() : standardizedPrice;
            priceUnit = price.getPriceUnit() != null ? price.getPriceUnit() : standardizedPriceUnit;
        } else {
            priceValue = standardizedPrice;
            priceUnit = standardizedPriceUnit;
        }

        final double realOperationalExpense;
        final double standardizedOperationalExpense;

        if (PricesService.DEFAULT_PRICE_UNIT.equals(priceUnit)) {
            // #10240: si unité = €/ha, alors charge de l'intervention = PSCi x Prix en €/ha
            realOperationalExpense = priceValue * psci;
        } else {
            Double conversionRate = RefInputPriceImpl.GET_PRICE_UNIT_TO_OTHER_PRICE_UNIT_CONVERSION_RATE.apply(Pair.of(priceUnit, PriceUnit.EURO_M3));
            if (conversionRate != null) {
                priceValue = conversionRate * priceValue;
            }
            // PA e (€.m -3 ) : prix d’achat de l’eau.
            realOperationalExpense = priceValue * da * M3_CONVERSION * psci;
        }

        if (PricesService.DEFAULT_PRICE_UNIT.equals(standardizedPriceUnit)) {
            // #10240: si unité = €/ha, alors charge de l'intervention = PSCi x Prix en €/ha
            standardizedOperationalExpense = standardizedPrice * psci;
        } else {
            // PA e (€.m -3 ) : prix d’achat de l’eau.
            // 1 mm = 1L / m² = 10 m3 / ha
            standardizedOperationalExpense = standardizedPrice * da * M3_CONVERSION * psci;
        }

        final Double[] inputResult = {realOperationalExpense, standardizedOperationalExpense};

        writeInputUsage(action, writerContext, inputResult, indicatorClass, labels);

        return Pair.of(realOperationalExpense, standardizedOperationalExpense);
    }

    private void writeInputUsage(
            IrrigationAction action,
            WriterContext writerContext,
            Double[] inputResult,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        IndicatorWriter writer = writerContext.getWriter();

        String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(action.getTopiaId(), MissingMessageScope.INPUT);
        Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(action.getTopiaId());

        Locale locale = writer.getLocale();
        for (int i = 0; i < inputResult.length; i++) {

            boolean isDisplayed = isDisplayed(i);

            if (isDisplayed) {
                // write input sheet
                writer.writeInputUsage(
                        writerContext.getIts(),
                        writerContext.getIrs(),
                        l(locale, Indicator.INDICATOR_CATEGORY_ECONOMIC),
                        l(locale, labels[i]),
                        action.getIrrigationInputUsage(),
                        action,
                        writerContext.getEffectiveIntervention(),
                        writerContext.getPracticedIntervention(),
                        writerContext.getCodeAmmBioControle(),
                        writerContext.getAnonymizeDomain(),
                        writerContext.getAnonymizeGrowingSystem(),
                        writerContext.getPlot(),
                        writerContext.getZone(),
                        writerContext.getPracticedSystem(),
                        writerContext.getCroppingPlanEntry(),
                        writerContext.getPracticedPhase(),
                        writerContext.getSolOccupationPercent(),
                        writerContext.getEffectivePhase(),
                        writerContext.getRank(),
                        writerContext.getPreviousPlanEntry(),
                        writerContext.getIntermediateCrop(),
                        indicatorClass,
                        inputResult[i],
                        reliabilityIndexForInputId,
                        reliabilityCommentForInputId,
                        null,
                        null,
                        null,
                        writerContext.getGroupesCiblesByCode());
            }
        }
    }

    // practiced
    protected Double[] computeOperatingExpenses(
            WriterContext writerContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        long start = System.currentTimeMillis();

        PracticedIntervention intervention = interventionContext.getIntervention();

        Optional<IrrigationAction> optionalIrrigationAction = interventionContext.getOptionalIrrigationAction();

        if (optionalIrrigationAction.isEmpty()) {
            // no expense
            Double[] result = {
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};
            return result;
        }

        double psci = getToolPSCi(intervention);

        final IrrigationAction irrigationAction = optionalIrrigationAction.get();

        final IrrigationInputUsage irrigationInputUsage = irrigationAction.getIrrigationInputUsage();
        Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> optionalRefPricesByIrrigDomainInput = practicedSystemContext.getPracticedSystemInputRefPricesByDomainInput().irrigRefPriceForInput();
        Optional<InputRefPrice> refPrixIrrig;
        if (optionalRefPricesByIrrigDomainInput != null) {
            Optional<InputRefPrice> inputRePrice = optionalRefPricesByIrrigDomainInput.get(irrigationInputUsage.getDomainIrrigationInput());
            refPrixIrrig = Objects.requireNonNullElseGet(inputRePrice, Optional::empty);
        } else {
            refPrixIrrig = Optional.empty();
        }

        Pair<Double, Double> inputsCharges = computeIrrigIndicator(
                writerContext,
                refPrixIrrig,
                irrigationAction,
                irrigationInputUsage,
                psci,
                intervention.getTopiaId(),
                indicatorClass,
                labels);

        Double[] result = {inputsCharges.getLeft(), inputsCharges.getRight()};

        if (LOGGER.isDebugEnabled()) {
            PracticedSystem practicedSystem = practicedSystemContext.getPracticedSystem();
            LOGGER.debug(
                    "computeIrrigationIndicator intervention:" + intervention.getName() + " (" + intervention.getTopiaId() + ")" +
                            ", practicedSystem:" + practicedSystem.getName() + "(" + practicedSystem.getCampaigns() + " )" +
                            " calculé en :" + (System.currentTimeMillis() - start) + "ms");
        }

        return result;
    }

    // effective
    protected Double[] computeOperatingExpenses(
            WriterContext writerContex,
            PerformanceEffectiveDomainExecutionContext domainContext,
            Zone zone,
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        long start = System.currentTimeMillis();

        Optional<IrrigationAction> optionalIrrigationAction = interventionContext.getOptionalIrrigationAction();

        if (optionalIrrigationAction.isEmpty()) {
            // not expenses for this intervention
            Double[] result = {
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};
            return result;
        }
        IrrigationAction irrigationAction = optionalIrrigationAction.get();

        EffectiveIntervention intervention = interventionContext.getIntervention();
        String interventionId = intervention.getTopiaId();

        double psci = getToolPSCi(intervention);

        final IrrigationInputUsage irrigationInputUsage = irrigationAction.getIrrigationInputUsage();
        Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> optionalRefPricesByIrrigDomainInput = domainContext.getRefInputPricesForCampaignsByInput().irrigRefPriceForInput();

        Optional<InputRefPrice> refPrixIrrig;
        if (optionalRefPricesByIrrigDomainInput != null) {
            Optional<InputRefPrice> inputRePrice = optionalRefPricesByIrrigDomainInput.get(irrigationInputUsage.getDomainIrrigationInput());
            refPrixIrrig = Objects.requireNonNullElseGet(inputRePrice, Optional::empty);
        } else {
            refPrixIrrig = Optional.empty();
        }

        Pair<Double, Double> inputsCharges = computeIrrigIndicator(
                writerContex,
                refPrixIrrig,
                irrigationAction,
                irrigationInputUsage,
                psci,
                interventionId,
                indicatorClass,
                labels);

        Double[] result = {inputsCharges.getLeft(), inputsCharges.getRight()};

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(
                    "computeIrrigationIndicator intervention:" + intervention.getName() + " (" + interventionId + ")" +
                            ", zone:" + zone.getTopiaId() +
                            " calculé en :" + (System.currentTimeMillis() - start));
        }

        return result;

    }
}
