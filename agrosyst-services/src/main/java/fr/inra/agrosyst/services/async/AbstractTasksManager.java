package fr.inra.agrosyst.services.async;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multiset;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.async.Task;
import fr.inra.agrosyst.api.services.security.AuthenticationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.ServiceContext;
import fr.inra.agrosyst.services.common.export.AgrosystDateUtils;
import fr.inra.agrosyst.services.performance.PerformanceTask;
import org.apache.commons.logging.Log;
import org.nuiton.topia.persistence.TopiaTransaction;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.function.Supplier;

public abstract class AbstractTasksManager {
    
    protected final Multiset<Class<? extends Task>> counter = HashMultiset.create();

    protected abstract Log getLog();

    protected abstract ConcurrentLinkedQueue<ScheduledTask> getRunningTasks();
    
    protected abstract BlockingQueue<ScheduledTaskRunnable> getQueuedRunnables();
    
    protected abstract BlockingQueue<ScheduledTaskRunnable> getImmediateRunnables();

    protected abstract ExecutorService getImmediateExecutor();

    protected abstract ExecutorService getQueueExecutor();
    
    protected ThreadPoolExecutor buildExecutor(int size, String threadNameFormat, BlockingQueue<? extends Runnable> queue) {
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat(threadNameFormat).build();
        ThreadPoolExecutor result = new ThreadPoolExecutor(
                size,
                size,
                0L,
                TimeUnit.MILLISECONDS,
                (BlockingQueue<Runnable>) queue,
                threadFactory);
        return result;
    }

    public <T extends Task> void schedule(T task) {
        if (getLog().isInfoEnabled()) {
            String message = String.format(
                    "Prise en charge d'un job : %s[%s] (%d en cours ; %d en attente)",
                    task.getClass().getSimpleName(),
                    task.getTaskId(),
                    countActiveTasks(),
                    countPendingTasks()
            );
            getLog().info(message);
        }
        ScheduledTask<T> scheduled = new ScheduledTask<>(task, LocalDateTime.now());
        counter.add(task.getClass());
        if (task.mustBeQueued()) {
            ScheduledTaskRunnable<T> runner = new ScheduledTaskRunnable<>(scheduled);
            getQueueExecutor().execute(runner);
        } else {
            ScheduledTaskRunnable<T> runner = new ScheduledTaskRunnable<>(scheduled);
            getImmediateExecutor().execute(runner);
        }
    }

    protected void notifyTaskStarts(ScheduledTask<?> scheduledTask) {
        LocalDateTime startedAt = LocalDateTime.now();
        scheduledTask.setStartedAt(startedAt);
        scheduledTask.setRunningInThread(Thread.currentThread());
        if (getLog().isInfoEnabled()) {
            Task task = scheduledTask.getTask();
            String wait = AgrosystDateUtils.formatDuration(scheduledTask.getScheduledAt(), startedAt);
            int pendingTasks = countPendingTasks();
            UUID taskId = task.getTaskId();
            String taskClassName = task.getClass().getSimpleName();
            String message = String.format("[t=%s] Début du job %s après une attente de %s. %d tâches en attente.", taskId, taskClassName, wait, pendingTasks);
            getLog().info(message);
        }
        getRunningTasks().add(scheduledTask);
    }

    protected void notifyTaskEnds(ScheduledTask<?> scheduledTask) {
        LocalDateTime finishedAt = LocalDateTime.now();
        scheduledTask.setFinishedAt(finishedAt);
        if (getLog().isInfoEnabled()) {
            Task task = scheduledTask.getTask();
            if (scheduledTask.getStartedAt().isPresent()) {
                LocalDateTime startedAt = scheduledTask.getStartedAt().get();
                String duration = AgrosystDateUtils.formatDuration(startedAt, finishedAt);
                String wait = AgrosystDateUtils.formatDuration(scheduledTask.getScheduledAt(), startedAt);
                UUID taskId = task.getTaskId();
                String taskSimpleName = task.getClass().getSimpleName();
                String message = String.format("[t=%s] Fin du job %s au bout de %s (+attente=%s)", taskId, taskSimpleName, duration, wait);
                getLog().info(message);
            }
        }
        getRunningTasks().remove(scheduledTask);
    }

    public ImmutableList<ScheduledTask> getActiveTasks() {
        return ImmutableList.copyOf(getRunningTasks());
    }

    protected int countActiveTasks() {
        int result = Iterables.size(getRunningTasks());
        return result;
    }

    public ImmutableList<ScheduledTask> getPendingTasks() {
        Iterable<ScheduledTaskRunnable> runnables = Iterables.concat(getQueuedRunnables(), getImmediateRunnables());
        Iterable<ScheduledTask> tasks = Iterables.transform(runnables, ScheduledTaskRunnable::getScheduled);
        return ImmutableList.copyOf(tasks);
    }

    protected int countPendingTasks() {
        int queuedTasks = Iterables.size(getQueuedRunnables());
        int immediateTasks = Iterables.size(getImmediateRunnables());
        int result = queuedTasks + immediateTasks;
        return result;
    }

    public ImmutableList<ScheduledTask> getRunningAndPendingTasks() {
        ImmutableList.Builder<ScheduledTask> builder = ImmutableList.builder();
        builder.addAll(getActiveTasks());
        builder.addAll(getPendingTasks());
        return builder.build();
    }

    protected void stopRunningTask(ScheduledTask<?> scheduledTask) {
        if (getLog().isWarnEnabled()) {
            getLog().warn("/!\\ About to cancel running task, consequences are unknown /!\\ : " + scheduledTask);
        }
        Optional<Thread> threadOptional = scheduledTask.getRunningInThread();
        if (threadOptional.isPresent()) {
            Task task = scheduledTask.getTask();

            TaskRunner<Task> taskRunner;
            try {
                taskRunner = TaskRunnerRegistry.getInstance().getRunner(task);
                taskRunner.cancelTask(task);
            } catch (Exception e) {
                getLog().warn("Failed to stop task " + task.getTaskId() + " " + task.getDescription() + " from:" + task.getUserEmail(), e);
            }

            Thread thread = threadOptional.get();
            thread.interrupt();
            try {
                // #join() dans le simple but de rendre la main une fois que le Thread est bien arrêté
                thread.join();
            } catch (InterruptedException ie) {
                getLog().warn("Thread " + thread.getName() + " interrupted", ie);
            }
        }
    }

    public void cancelTask(String taskId) {
        Preconditions.checkArgument(taskId != null);
        if (getLog().isInfoEnabled()) {
            getLog().info(String.format("Annulation du job %s", taskId));
        }

        UUID taskUuid = UUID.fromString(taskId);

        // On annule la tâche si elle est programmée
        cancelPendingTask(taskUuid);
    
        // On l'arrête si elle est en cours
        cancelRunningClass(taskUuid);
    }
    
    protected void cancelRunningClass(UUID taskId) {
        getRunningTasks().stream()
                .filter(task -> taskId.equals(task.getTask().getTaskId()))
                .forEach(this::stopRunningTask);
    }
    
    public void cancelPendingTask(UUID taskId) {
        Predicate<ScheduledTaskRunnable> predicate = str -> taskId.equals(str.getScheduled().getTask().getTaskId());
        if (getQueuedRunnables().removeIf(predicate) || getImmediateRunnables().removeIf(predicate)) {
            if (getLog().isInfoEnabled()) {
                getLog().info(String.format("Job %s annulé avant son démarrage", taskId));
            }
        }
    }
    
    public void cancelAllDbTasks(){
        List<UUID> pendingTasks = getPendingTasks().stream()
                .map(ScheduledTask::getTask)
                .filter(task -> task instanceof PerformanceTask && ((PerformanceTask) task).isDbPerformanceTask())
                .map(Task::getTaskId)
                .toList();
        for (UUID pendingTaskId : pendingTasks) {
            cancelDbTask(pendingTaskId);
        }
    }

    protected void cancelDbTask(UUID taskId) {
        Preconditions.checkArgument(taskId != null);
        if (getLog().isInfoEnabled()) {
            getLog().info(String.format("Annulation du job %s", taskId));
        }
        
        // On annule la tâche si elle est programmée
        cancelPendingTask(taskId);
    }

    /**
     * Runnable responsable du traitement d'une {@link Task}. Le but de cette classe est d'identifier/créer/récupérer la
     * bonne instance de {@link TaskRunner} et lui confier l'éxécution de la tâche.
     */
    protected class ScheduledTaskRunnable<G extends Task> implements Runnable {

        private final ScheduledTask<G> scheduled;

        private ScheduledTaskRunnable(ScheduledTask<G> scheduled) {
            this.scheduled = scheduled;
        }

        public ScheduledTask<G> getScheduled() {
            return scheduled;
        }

        @Override
        public void run() {
            notifyTaskStarts(scheduled);
            ServiceContext serviceContext = null;
            try {
                G task = scheduled.getTask();
                TaskRunner<G> taskRunner = TaskRunnerRegistry.getInstance().getRunner(task);
                serviceContext = createAsyncServiceContext(task.getUserId());
                taskRunner.runTask(task, serviceContext);
            } catch (Exception eee) {
                if (getLog().isErrorEnabled()) {
                    getLog().error("Unable to run task", eee);
                }
            } finally {
                notifyTaskEnds(scheduled);
                closeAsyncServiceContext(serviceContext);
            }
        }

    }

    /**
     * Créé un ServiceContext dédié à une tâche. Ce service contexte donnera l'illusion aux services que l'utilisateur
     * avec l'identifiant {@code userId} est connecté.
     *
     * @param userId l'utilisateur au nom duquel le ServiceContext doit être créé
     * @return un nouveau ServiceContext pour l'utilisateur donné.
     */
    protected ServiceContext createAsyncServiceContext(String userId) {
        try (ServiceContext serviceContext = newServiceContextSupplier().get()) {
            AuthenticationService authenticationService = serviceContext.newService(AuthenticationService.class);

            String sid = "async:" + UUID.randomUUID();
            AuthenticatedUser userDto = authenticationService.getAuthenticatedUserFromUserId(userId, sid);

            // On a créé une transaction potentiellement, donc on commit si jamais il y a eu écriture en base
            TopiaTransaction transaction = serviceContext.getTransaction(false);
    
            if (getLog().isDebugEnabled()) {
                String isCommitOrNot = transaction == null ? "will not be" : "will be";
                getLog().debug(String.format("For user %s transaction %s commit", userId, isCommitOrNot));
            }
    
            if (transaction != null) {
                transaction.commit();
            }
    
            ServiceContext result = serviceContext.newServiceContext(userDto);
            return result;
        } catch (Exception eee) {
            throw new AgrosystTechnicalException("Unable to close ServiceContext used to create a token", eee);
        }
    }

    /**
     * Permet de mettre fin à un ServiceContext. Si besoin la méthode fera un logout pour libérer d'éventuelles ressources
     *
     * @param serviceContext l'instance de ServiceContext à fermer
     */
    protected void closeAsyncServiceContext(ServiceContext serviceContext) {
        if (serviceContext != null) {
            // On ferme le ServiceContext
            try {
                serviceContext.close();
            } catch (Exception eee) {
                getLog().error("Unable to close ServiceContext", eee);
            }
        }
    }

    protected abstract Supplier<ServiceContext> newServiceContextSupplier();

    public int count(Class<? extends Task> taskClass) {
        return counter.count(taskClass);
    }

    public ImmutableMap<String, Integer> getCounterSnapshot() {
        ImmutableMap.Builder<String, Integer> builder = ImmutableMap.builder();
        for (Multiset.Entry<Class<? extends Task>> entry : counter.entrySet()) {
            builder.put(entry.getElement().getName(), entry.getCount());
        }
        return builder.build();
    }
}
