package fr.inra.agrosyst.services.async;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.async.Task;
import fr.inra.agrosyst.services.ServiceContext;

import java.time.Duration;

/**
 * Extension de {@link AbstractTaskRunner} mais qui ne renvoie aucun résultat
 */
public abstract class NoResultAbstractTaskRunner<T extends Task> extends AbstractTaskRunner<T, Void> {

    @Override
    protected Void executeTask(T task, ServiceContext serviceContext) {
        executeTaskNoResult(task, serviceContext);
        return null;
    }

    protected abstract void executeTaskNoResult(T task, ServiceContext serviceContext);

    protected void taskSucceeded(T task, ServiceContext serviceContext, Void result, Duration duration) {
        // C'est plutôt normal de ne pas utiliser le résultat dans le cas d'un NoResultAbstractTaskRunner
    }

}
