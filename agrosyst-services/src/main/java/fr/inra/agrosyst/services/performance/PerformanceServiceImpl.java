package fr.inra.agrosyst.services.performance;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.BasicPlot;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.performance.ActiveSubstance;
import fr.inra.agrosyst.api.entities.performance.DecomposedOperatingExpenses;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.Ift;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilterImpl;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilterTopiaDao;
import fr.inra.agrosyst.api.entities.performance.MineralFertilization;
import fr.inra.agrosyst.api.entities.performance.OrganicFertilization;
import fr.inra.agrosyst.api.entities.performance.OrganicProduct;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceState;
import fr.inra.agrosyst.api.entities.performance.TotalFertilization;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceConverter;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.HarvestingValorisationPriceSummary;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.performance.CropWithSpecies;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePlotExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.RefScenariosInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.utils.CurrentPerformanceDto;
import fr.inra.agrosyst.api.services.performance.utils.PriceConverterKeysToRate;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.async.BusinessTasksManager;
import fr.inra.agrosyst.services.async.ScheduledTask;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.performance.dbPersistence.DbCommonReferentiels;
import fr.inra.agrosyst.services.performance.dbPersistence.DbPerformanceDomainContext;
import fr.inra.agrosyst.services.performance.dbPersistence.DbPerformanceDomainsContext;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import fr.inra.agrosyst.services.performance.indicators.IndicatorDirectMargin;
import fr.inra.agrosyst.services.performance.indicators.IndicatorEquipmentsExpenses;
import fr.inra.agrosyst.services.performance.indicators.IndicatorFieldsStatistics;
import fr.inra.agrosyst.services.performance.indicators.IndicatorFuelConsumption;
import fr.inra.agrosyst.services.performance.indicators.IndicatorGrossIncome;
import fr.inra.agrosyst.services.performance.indicators.IndicatorGrossIncomeForScenarios;
import fr.inra.agrosyst.services.performance.indicators.IndicatorGrossMargin;
import fr.inra.agrosyst.services.performance.indicators.IndicatorIrrigation;
import fr.inra.agrosyst.services.performance.indicators.IndicatorManualWorkTime;
import fr.inra.agrosyst.services.performance.indicators.IndicatorManualWorkforceExpenses;
import fr.inra.agrosyst.services.performance.indicators.IndicatorMechanizedWorkTime;
import fr.inra.agrosyst.services.performance.indicators.IndicatorMechanizedWorkforceExpenses;
import fr.inra.agrosyst.services.performance.indicators.IndicatorOrganicProducts;
import fr.inra.agrosyst.services.performance.indicators.IndicatorSemiNetMargin;
import fr.inra.agrosyst.services.performance.indicators.IndicatorStandardisedGrossIncome;
import fr.inra.agrosyst.services.performance.indicators.IndicatorSurfaceUTH;
import fr.inra.agrosyst.services.performance.indicators.IndicatorToolUsageTime;
import fr.inra.agrosyst.services.performance.indicators.IndicatorTotalWorkTime;
import fr.inra.agrosyst.services.performance.indicators.IndicatorTotalWorkforceExpenses;
import fr.inra.agrosyst.services.performance.indicators.IndicatorTransitCount;
import fr.inra.agrosyst.services.performance.indicators.IndicatorUTH;
import fr.inra.agrosyst.services.performance.indicators.agronomicstrategy.IndicatorNumberOfMechanicalWeedingPassages;
import fr.inra.agrosyst.services.performance.indicators.agronomicstrategy.IndicatorNumberOfPloughingPassages;
import fr.inra.agrosyst.services.performance.indicators.agronomicstrategy.IndicatorNumberOfTCSPassages;
import fr.inra.agrosyst.services.performance.indicators.agronomicstrategy.IndicatorTillageType;
import fr.inra.agrosyst.services.performance.indicators.agronomicstrategy.IndicatorUsageOfMechanicalWeeding;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorMineralFertilization;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorOrganicFertilization;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorTotalFertilization;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFT;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorRefMaxYearCropIFT;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorRefMaxYearTargetIFT;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorVintageCropIFT;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorVintageTargetIFT;
import fr.inra.agrosyst.services.performance.indicators.operatingexpenses.IndicatorDecomposedOperatingExpenses;
import fr.inra.agrosyst.services.performance.indicators.operatingexpenses.IndicatorOperatingExpenses;
import fr.inra.agrosyst.services.performance.indicators.operatingexpenses.IndicatorScenariosOperatingExpenses;
import fr.inra.agrosyst.services.performance.indicators.qsa.AbstractIndicatorQSA;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorActiveSubstanceAmount;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCMR;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCMRUses;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCopperFertilisationProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCopperPhytoProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCopperTotalProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorEnvironmentalRisk;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorEnvironmentalRiskUses;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1_G1;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1_G2;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1_G3;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1_G4;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorLowRiskSubstances;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorNeonicotinoidsAmount;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSoilAppliedHerbicidesAmount;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSubstancesCandidateToSubstitution;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSulfurFertilisationProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSulfurPhytoProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSulfurTotalProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorTotalActiveSubstanceAmount;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorToxicUser;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorToxicUserUses;
import fr.inra.agrosyst.services.performance.performancehelper.PerformanceEffectiveExecutionContextBuilder;
import fr.inra.agrosyst.services.performance.performancehelper.PerformancePracticedExecutionContextBuilder;
import fr.inra.agrosyst.services.security.SecurityContext;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
@Setter
public class PerformanceServiceImpl extends CommonPerformanceService {

    private static final Log LOGGER = LogFactory.getLog(PerformanceServiceImpl.class);

    protected InputPriceService inputPriceService;

    protected IndicatorFilterTopiaDao indicatorFilterDao;

    protected PerformanceEffectiveExecutionContextBuilder getEffectiveExecutionContextBuilder(
            Set<String> scenarioCodes,
            RefSolTextureGeppa defaultRefSolTextureGeppa,
            RefSolProfondeurIndigo defaultSolDepth,
            RefLocation defaultLocation,
            PriceConverterKeysToRate priceConverterKeysToRate,
            Domain domain,
            Domain anonymizeDomain,
            Set<Plot> computedPlots,
            Collection<String> codeAmmBioControls,
            Map<String, String> groupesCiblesParCode,
            Collection<IndicatorFilter> indicatorFilters) {

        PerformanceEffectiveExecutionContextBuilder performanceEffectiveExecutionBuilder =
                new PerformanceEffectiveExecutionContextBuilder(
                        anonymizeService,
                        domainInputStockUnitService,
                        domainService,
                        this,
                        inputPriceService,
                        pricesService, refInputPriceService,
                        referentialService,
                        codeAmmBioControls,
                        Pair.of(domain, anonymizeDomain),
                        defaultRefSolTextureGeppa,
                        true,
                        priceConverterKeysToRate,
                        scenarioCodes,
                        defaultLocation,
                        defaultSolDepth,
                        groupesCiblesParCode,
                        computedPlots,
                        indicatorFilters);
        return performanceEffectiveExecutionBuilder;
    }

    protected void writeEffectivePrices(
            String performanceId,
            IndicatorWriter writer,
            PerformanceEffectiveDomainExecutionContext effectiveDomainContext,
            Collection<HarvestingActionValorisation> allDomainValorisations,
            List<Equipment> equipments) {

        if (writer instanceof SqlWriter) return; // NOTHING TO DO

        RefCampaignsInputPricesByDomainInput effectiveDomainInputPriceRefPrices = effectiveDomainContext.getRefInputPricesForCampaignsByInput();
        List<HarvestingPrice> harvestingPrices = effectiveDomainContext.getHarvestingPrices();

        Domain anonymiseDomain = effectiveDomainContext.getAnonymiseDomain();

        Map<String, HarvestingValorisationPriceSummary>
                refHarvestingPricesSummaries = getEffectiveRefHarvestingValorisationPriceSummaryByValorisationIds(
                allDomainValorisations,
                performanceId);

        final MultiValuedMap<String, RefHarvestingPrice> refScenarioHarvestingPricesByValorisationKey = effectiveDomainContext.getRefScenarioHarvestingPricesByValorisationKey();
        final Collection<RefHarvestingPrice> refHarvestingPrices = refScenarioHarvestingPricesByValorisationKey.values();
        final MultiKeyMap<String, List<RefHarvestingPrice>> refHarvestingPricesByCodeEspeceAndCodeDestination = getRefHarvestingPricesByCodeEspeceAndCodeDestination(refHarvestingPrices);

        writer.writePrices(
                effectiveDomainInputPriceRefPrices,
                harvestingPrices,
                refHarvestingPricesSummaries,
                equipments,
                anonymiseDomain,
                null,
                effectiveDomainContext.getRefScenariosInputPricesByDomainInput(),
                refHarvestingPricesByCodeEspeceAndCodeDestination,
                effectiveDomainContext.getCroppingPlanSpecies());

    }

    @Override
    protected void createOrUpdateIndicatorFilters(Performance performance,
                                                  Collection<IndicatorFilter> indicatorFilters,
                                                  boolean computeOnReal,
                                                  boolean computeOnStandardised,
                                                  Collection<String> scenarioCodes) {

        Map<String, IndicatorFilter> persistedIndicatorFiltersById = new HashMap<>(
                CollectionUtils.emptyIfNull(performance.getIndicatorFilter()).stream()
                        .collect(Collectors.toMap(Entities.GET_TOPIA_ID, Function.identity()))
        );

        Binder<IndicatorFilter, IndicatorFilter> binder = BinderFactory.newBinder(IndicatorFilter.class);
        List<IndicatorFilter> indicatorFilterToCreates = new ArrayList<>();
        for (IndicatorFilter indicatorFilter : indicatorFilters) {
            indicatorFilter.setComputeReal(computeOnReal);
            indicatorFilter.setComputeStandardized(computeOnStandardised);

            // do not add indicator that used scenario Codes if no scenario are selected
            if (CollectionUtils.isEmpty(scenarioCodes) && (indicatorFilter.getClazz().equals(IndicatorGrossIncomeForScenarios.class.getSimpleName()) ||
                    indicatorFilter.getClazz().equals(IndicatorScenariosOperatingExpenses.class.getSimpleName()))) {
                continue;// to remove it
            }

            if ((indicatorFilter.getClazz().contains(IndicatorLegacyIFT.class.getSimpleName()) &&
                    CollectionUtils.isEmpty(indicatorFilter.getIfts())) ||
                    (indicatorFilter.getClazz().contains(IndicatorRefMaxYearTargetIFT.class.getSimpleName()) &&
                            CollectionUtils.isEmpty(indicatorFilter.getIfts())) ||
                    (indicatorFilter.getClazz().contains(IndicatorVintageTargetIFT.class.getSimpleName()) &&
                            CollectionUtils.isEmpty(indicatorFilter.getIfts())) ||
                    (indicatorFilter.getClazz().contains(IndicatorRefMaxYearCropIFT.class.getSimpleName()) &&
                            CollectionUtils.isEmpty(indicatorFilter.getIfts())) ||
                    (indicatorFilter.getClazz().contains(IndicatorVintageCropIFT.class.getSimpleName()) &&
                            CollectionUtils.isEmpty(indicatorFilter.getIfts())) ||
                    (indicatorFilter.getClazz().contains(IndicatorDecomposedOperatingExpenses.class.getSimpleName()) &&
                            CollectionUtils.isEmpty(indicatorFilter.getDoeIndicators())) ||
                    (indicatorFilter.getClazz().contains(IndicatorGrossIncome.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithAutoConsumed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithoutAutoConsumed())) ||
                    (indicatorFilter.getClazz().contains(IndicatorSemiNetMargin.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithAutoConsumed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithoutAutoConsumed())) ||
                    (indicatorFilter.getClazz().contains(IndicatorGrossMargin.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithAutoConsumed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithoutAutoConsumed())) ||
                    (indicatorFilter.getClazz().contains(IndicatorToolUsageTime.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getNoDetailed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getDetailedByMonth())) ||
                    (indicatorFilter.getClazz().contains(IndicatorManualWorkTime.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getNoDetailed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getDetailedByMonth())) ||
                    (indicatorFilter.getClazz().contains(IndicatorMechanizedWorkTime.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getNoDetailed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getDetailedByMonth())) ||
                    (indicatorFilter.getClazz().contains(IndicatorTotalWorkTime.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getNoDetailed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getDetailedByMonth())) ||
                    (indicatorFilter.getClazz().contains(IndicatorTransitCount.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getNoDetailed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getDetailedByMonth()))
            ) {
                continue;// no data to compute, remove it
            }

            if (indicatorFilter.isPersisted()) {
                IndicatorFilter filterToUpdate = persistedIndicatorFiltersById.remove(indicatorFilter.getTopiaId());
                if (filterToUpdate != null) {
                    binder.copy(indicatorFilter, filterToUpdate);
                }
            } else {
                indicatorFilterToCreates.add(indicatorFilter);
            }
        }

        if (performance.isPersisted()) {
            // remove no more present filters
            persistedIndicatorFiltersById.values().forEach(performance::removeIndicatorFilter);
        }

        if (!indicatorFilterToCreates.isEmpty()) {
            indicatorFilterDao.createAll(indicatorFilterToCreates).forEach(performance::addIndicatorFilter);
        }
    }

    @Override
    protected Collection<IndicatorFilter> createOrUpdateIndicatorFilters(
            Collection<IndicatorFilter> indicatorFilters,
            Collection<String> scenarioCodes) {

        Collection<IndicatorFilter> result = new ArrayList<>();

        for (IndicatorFilter indicatorFilter : indicatorFilters) {

            // do not add indicator that used scenario Codes if no scenario are selected
            if (CollectionUtils.isEmpty(scenarioCodes) && (indicatorFilter.getClazz().equals(IndicatorGrossIncomeForScenarios.class.getSimpleName()) ||
                    indicatorFilter.getClazz().equals(IndicatorScenariosOperatingExpenses.class.getSimpleName()))) {
                continue;// to remove it
            }

            if ((indicatorFilter.getClazz().contains(IndicatorLegacyIFT.class.getSimpleName()) &&
                    CollectionUtils.isEmpty(indicatorFilter.getIfts())) ||
                    (indicatorFilter.getClazz().contains(IndicatorRefMaxYearTargetIFT.class.getSimpleName()) &&
                            CollectionUtils.isEmpty(indicatorFilter.getIfts())) ||
                    (indicatorFilter.getClazz().contains(IndicatorVintageTargetIFT.class.getSimpleName()) &&
                            CollectionUtils.isEmpty(indicatorFilter.getIfts())) ||
                    (indicatorFilter.getClazz().contains(IndicatorRefMaxYearCropIFT.class.getSimpleName()) &&
                            CollectionUtils.isEmpty(indicatorFilter.getIfts())) ||
                    (indicatorFilter.getClazz().contains(IndicatorVintageCropIFT.class.getSimpleName()) &&
                            CollectionUtils.isEmpty(indicatorFilter.getIfts())) ||
                    (indicatorFilter.getClazz().contains(IndicatorDecomposedOperatingExpenses.class.getSimpleName()) &&
                            CollectionUtils.isEmpty(indicatorFilter.getDoeIndicators())) ||
                    (indicatorFilter.getClazz().contains(IndicatorGrossIncome.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithAutoConsumed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithoutAutoConsumed())) ||
                    (indicatorFilter.getClazz().contains(IndicatorStandardisedGrossIncome.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithAutoConsumed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithoutAutoConsumed())) ||
                    (indicatorFilter.getClazz().contains(IndicatorSemiNetMargin.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithAutoConsumed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithoutAutoConsumed())) ||
                    (indicatorFilter.getClazz().contains(IndicatorDirectMargin.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithAutoConsumed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithoutAutoConsumed())) ||
                    (indicatorFilter.getClazz().contains(IndicatorGrossMargin.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithAutoConsumed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getWithoutAutoConsumed())) ||
                    (indicatorFilter.getClazz().contains(IndicatorToolUsageTime.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getNoDetailed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getDetailedByMonth())) ||
                    (indicatorFilter.getClazz().contains(IndicatorManualWorkTime.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getNoDetailed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getDetailedByMonth())) ||
                    (indicatorFilter.getClazz().contains(IndicatorMechanizedWorkTime.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getNoDetailed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getDetailedByMonth())) ||
                    (indicatorFilter.getClazz().contains(IndicatorTotalWorkTime.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getNoDetailed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getDetailedByMonth())) ||
                    (indicatorFilter.getClazz().contains(IndicatorTransitCount.class.getSimpleName()) &&
                            BooleanUtils.isFalse(indicatorFilter.getNoDetailed()) &&
                            BooleanUtils.isFalse(indicatorFilter.getDetailedByMonth()))
            ) {
                continue;// no data to compute, remove it
            }

            indicatorFilter = indicatorFilterDao.create(indicatorFilter);
            result.add(indicatorFilter);
        }
        
        return result;
    }


    @Override
    protected boolean isScenarioCodeValid(Collection<IndicatorFilter> indicatorFilters, Collection<String> scenarioCodes) {
        boolean isScenarioCodeValid = CollectionUtils.isNotEmpty(scenarioCodes) && indicatorFilters.stream().anyMatch(
                indicatorFilter -> (IndicatorGrossIncome.class.getSimpleName().equals(indicatorFilter.getClazz()) ||
                        IndicatorGrossIncomeForScenarios.class.getSimpleName().equals(indicatorFilter.getClazz()) ||
                        IndicatorOperatingExpenses.class.getSimpleName().equals(indicatorFilter.getClazz()) ||
                        IndicatorScenariosOperatingExpenses.class.getSimpleName().equals(indicatorFilter.getClazz()))
        );
        return isScenarioCodeValid;
    }

    protected String[] getTranslatedMonths(Locale locale) {
        String[] result = new String[IndicatorFieldsStatistics.NB_MONTH];
        for (int month = 0; month < IndicatorFieldsStatistics.NB_MONTH; month++) {
            result[month] = AgrosystI18nService.getTranslatedMonth(month, locale);
        }
        return result;
    }

    /**
     * Cette méthode construit une liste d'indicateurs à calculer.
     * Les indicateurs ont des dépendances entre eux (certains nécessitent le résultat d'autres pour être calculés).
     * Cette méthode déclare (et ajoute à la liste) volontairement les indicateurs dépendant d'autre en premier et inverse ensuite la liste des indicateurs avant de la retourner.
     * Cela permet ainsi d'optimiser la liste des indicateurs à calculer en créant un pseudo arbre de dépendance.
     *
     * @param indicatorFiltersByClazz une map de filter par indicateur
     * @param scenarioCodes la liste des scénarios
     * @param exportToFile pour ajouter certains indicateurs qui ne sont calculés que pour les exports individuels
     * @return une liste ordonnée d'indicateurs à calculer
     */
    protected List<GenericIndicator> getIndicators(
            Map<String, IndicatorFilter> indicatorFiltersByClazz,
            Collection<String> scenarioCodes,
            boolean exportToFile) {

        // build indicator list to compute
        List<GenericIndicator> indicators = new ArrayList<>();

        String[]  translatedMonth = getTranslatedMonths(getSecurityContext().getLanguage().getLocale());

        final boolean computeCopperTotal = indicatorFiltersByClazz.get(IndicatorCopperTotalProduct.class.getSimpleName()) != null;

        final boolean computeSulfurFerti = indicatorFiltersByClazz.get(IndicatorSulfurFertilisationProduct.class.getSimpleName()) != null;

        final boolean computeCopperFerti = indicatorFiltersByClazz.get(IndicatorCopperFertilisationProduct.class.getSimpleName()) != null || computeCopperTotal;

        final boolean computeTotalFertilization = this.addIndicatorTotalFertilization(indicatorFiltersByClazz, indicators);

        // required IndicatorSemiNetMargin & IndicatorTotalWorkforceExpenses
        final boolean computeDirectMargin = addDirectMarginIndicator(indicatorFiltersByClazz, indicators);

        // required IndicatorGrossMargin & IndicatorEquipmentsExpenses to be computed
        final boolean computeSemiNetMargin = addSemiNetMarginIndicator(indicatorFiltersByClazz, computeDirectMargin, indicators);

        addQSAIndicators(indicatorFiltersByClazz, indicators);

        addSurfaceUTHIndicator(indicatorFiltersByClazz, exportToFile, indicators);

        // required IndicatorGrossIncome, IndicatorOperatingExpenses & IndicatorFuelConsumption to be computed
        final boolean computeGrossMargin = addGrossMarginIndicator(indicatorFiltersByClazz, computeSemiNetMargin, indicators);

        // required IndicatorFuelConsumption to be computed
        final boolean computeEquipmentsExpenses = addEquipmentsExpensesIndicator(indicatorFiltersByClazz, computeSemiNetMargin, indicators);

        addStandardisedGrossIncomeIndicator(indicatorFiltersByClazz, computeGrossMargin, indicators);

        addGrossIncomeIndicator(indicatorFiltersByClazz, computeGrossMargin, indicators);

        // require IndicatorRefMaxYearTargetIFT for RefDosageForPhytoUsage
        addOperatingExpensesIndicator(indicatorFiltersByClazz, computeGrossMargin, indicators);

        // Same as previous but decomposed by input type
        addIndicatorDecomposedOperatingExpenses(indicatorFiltersByClazz, indicators);

        // required ToolsCooplingWorkTime to be computed
        final boolean computeFuelConsumption = addFuelConsumptionIndicator(indicatorFiltersByClazz, computeEquipmentsExpenses, indicators);

        // required IndicatorManualWorkforceExpenses & IndicatorMechanizedWorkforceExpenses to be computed
        final boolean computeTotalWorkforceExpenses = addTotalWorkforceExpensesIndicator(indicatorFiltersByClazz, computeDirectMargin, indicators);

        // required IndicatorMechanizedWorkTime to be computed
        final boolean computeMechanizedWorkforceExpenses = addMechanizedWorkforceExpensesIndicator(indicatorFiltersByClazz, computeTotalWorkforceExpenses, indicators);

        // Requires IndicatorTotalWorkTime to be computed
        final boolean computeUTH = addUTHIndicator(indicatorFiltersByClazz, indicators);

        // required IndicatorManualWorkTime & IndicatorMechanizedWorkTime to be computed
        final boolean computeTotalWorkTime = addTotalWorkTimeIndicator(indicatorFiltersByClazz, computeUTH, translatedMonth, indicators);

        // required by IndicatorMechanizedExpenses & IndicatorTotalWorkTime
        final boolean computeMechanizedWorkTime = addMechanizedWorkTimeIndicator(indicatorFiltersByClazz, computeMechanizedWorkforceExpenses, computeTotalWorkTime, translatedMonth, indicators);

        // required IndicatorManualWorkTime to be computed
        final boolean computeManualWorkforceExpenses = addManualWorkforceExpensesIndicator(indicatorFiltersByClazz, computeTotalWorkforceExpenses, indicators);

        addManualWorkTimeIndicator(indicatorFiltersByClazz, computeManualWorkforceExpenses, computeTotalWorkTime, translatedMonth, indicators);

        addTransitCountIndicator(indicatorFiltersByClazz, translatedMonth, indicators);

        addToolUsageTimeIndicator(indicatorFiltersByClazz, computeFuelConsumption, computeMechanizedWorkTime, translatedMonth, indicators);

        addIrrigationIndicator(indicatorFiltersByClazz, indicators);

        addOrganicProductsIndicator(indicatorFiltersByClazz, indicators);

        addAgronomicStrategyIndicators(indicatorFiltersByClazz, indicators, exportToFile);

        addTillageTypeIndicator(indicatorFiltersByClazz, indicators);

        addMineralFertilizationIndicator(indicatorFiltersByClazz, indicators, computeCopperFerti, computeSulfurFerti, computeTotalFertilization);

        addOrganicFertilizationIndicator(indicatorFiltersByClazz, indicators, computeSulfurFerti, computeTotalFertilization);

        // IFT chimique, IFT tot hts, IFT h, IFT f, IFT i, IFT ts, IFT a, IFT hh (ts inclus), IFT biocontrole,
        // recours à des macro-organismes, recours à des produits biotiques, recours à des produits abiotiques
        // doit être calculé en premier pour avoir le calcul des doses de référence
        addIFT_Indicators(indicatorFiltersByClazz, indicators);

        indicators.forEach(indicator -> indicator.setLocale(getSecurityContext().getLocale()));

        Collections.reverse(indicators);

        return indicators;
    }

    private void addTillageTypeIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators) {
        IndicatorFilter indicatorTillageTypeFilter = indicatorFiltersByClazz.get(IndicatorTillageType.class.getSimpleName());
        if (indicatorTillageTypeFilter != null) {
            IndicatorTillageType indicatorTillageType = newInstance(IndicatorTillageType.class);
            indicatorTillageType.init(indicatorTillageTypeFilter);
            indicators.add(indicatorTillageType);
        }
    }

    private void addOrganicProductsIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators) {
        final IndicatorFilter organicProductsFilter = indicatorFiltersByClazz.get(IndicatorOrganicProducts.class.getSimpleName());
        final boolean computeOrganicProducts = organicProductsFilter != null;
        if (computeOrganicProducts) {
            final IndicatorOrganicProducts organicProductsIndicator = newInstance(IndicatorOrganicProducts.class);
            organicProductsIndicator.init(organicProductsFilter.getOrganicProducts());
            indicators.add(organicProductsIndicator);
        }
    }

    private void addIrrigationIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators) {
        final IndicatorFilter irrigationFilter = indicatorFiltersByClazz.get(IndicatorIrrigation.class.getSimpleName());
        final boolean computeIrrigation = irrigationFilter != null;
        if (computeIrrigation) {
            IndicatorIrrigation indicatorIrrigation = newInstance(IndicatorIrrigation.class);
            indicatorIrrigation.init(irrigationFilter);
            indicators.add(indicatorIrrigation);
        }
    }

    private void addToolUsageTimeIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean computeFuelConsumption, boolean computeMechanizedWorkTime, String[] translatedMonth, List<GenericIndicator> indicators) {
        final IndicatorFilter toolUsageTimeFilter = indicatorFiltersByClazz.get(IndicatorToolUsageTime.class.getSimpleName());
        final boolean computeToolUsageTime = toolUsageTimeFilter != null || computeFuelConsumption || computeMechanizedWorkTime;  // required by IndicatorFuelConsumption & IndicatorMechanizedWorkTime
        if (computeToolUsageTime) {
            final IndicatorToolUsageTime toolUsageTimeIndicator = newInstance(IndicatorToolUsageTime.class);
            toolUsageTimeIndicator.init(toolUsageTimeFilter, translatedMonth);
            indicators.add(toolUsageTimeIndicator);// Temps d'utilisation du matériel.
        }
    }

    private void addTransitCountIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, String[] translatedMonth, List<GenericIndicator> indicators) {
        final IndicatorFilter transitCountFilter = indicatorFiltersByClazz.get(IndicatorTransitCount.class.getSimpleName());
        final boolean computeTransitCount = transitCountFilter != null;
        if (computeTransitCount) {
            final IndicatorTransitCount transitCountIndicator = newInstance(IndicatorTransitCount.class);
            transitCountIndicator.init(transitCountFilter, translatedMonth);
            indicators.add(transitCountIndicator);// Le nombre de passages correspond au nombre d’interventions culturales,
            // manuelles ou de traction animale, faites sur un hectare.
        }
    }

    private void addManualWorkTimeIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean computeManualWorkforceExpenses, boolean computeTotalWorkTime, String[] translatedMonth, List<GenericIndicator> indicators) {
        final IndicatorFilter manualWorkTimeFilter = indicatorFiltersByClazz.get(IndicatorManualWorkTime.class.getSimpleName());
        final boolean computeManualWorkTime = manualWorkTimeFilter != null || computeManualWorkforceExpenses || computeTotalWorkTime; // required by IndicatorManualWorkforceExpenses & IndicatorTotalWorkTime
        if (computeManualWorkTime) {
            final IndicatorManualWorkTime manualWorkTimeIndicator = newInstance(IndicatorManualWorkTime.class);
            manualWorkTimeIndicator.init(manualWorkTimeFilter, translatedMonth);
            indicators.add(manualWorkTimeIndicator); // Temps de travail manuel.
        }
    }

    private boolean addManualWorkforceExpensesIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean computeTotalWorkforceExpenses, List<GenericIndicator> indicators) {
        final IndicatorFilter manualWorkforceExpensesFilter = indicatorFiltersByClazz.get(IndicatorManualWorkforceExpenses.class.getSimpleName());
        final boolean computeManualWorkforceExpenses = manualWorkforceExpensesFilter != null || computeTotalWorkforceExpenses; // required by IndicatorTotalWorkforceExpenses
        if (computeManualWorkforceExpenses) {
            final IndicatorManualWorkforceExpenses manualWorkforceExpensesIndicator = newInstance(IndicatorManualWorkforceExpenses.class);
            manualWorkforceExpensesIndicator.init(manualWorkforceExpensesFilter);
            indicators.add(manualWorkforceExpensesIndicator); // Charges de main d'œuvre manuelle.
        }
        return computeManualWorkforceExpenses;
    }

    private boolean addMechanizedWorkTimeIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean computeMechanizedWorkforceExpenses, boolean computeTotalWorkTime, String[] translatedMonth, List<GenericIndicator> indicators) {
        final IndicatorFilter mechanizedWorkTimeFilter = indicatorFiltersByClazz.get(IndicatorMechanizedWorkTime.class.getSimpleName());
        final boolean computeMechanizedWorkTime = mechanizedWorkTimeFilter != null || computeMechanizedWorkforceExpenses || computeTotalWorkTime; // required by IndicatorMechanizedExpenses & IndicatorTotalWorkTime
        if (computeMechanizedWorkTime) {
            final IndicatorMechanizedWorkTime mechanizedWorkTimeIndicator = newInstance(IndicatorMechanizedWorkTime.class);
            mechanizedWorkTimeIndicator.init(mechanizedWorkTimeFilter, translatedMonth);
            indicators.add(mechanizedWorkTimeIndicator); // Temps de travail mécanisé.
        }
        return computeMechanizedWorkTime;
    }

    private boolean addTotalWorkTimeIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean computeUTH, String[] translatedMonth, List<GenericIndicator> indicators) {
        final IndicatorFilter totalWorkTimeFilter = indicatorFiltersByClazz.get(IndicatorTotalWorkTime.class.getSimpleName());
        final boolean computeTotalWorkTime = totalWorkTimeFilter != null || computeUTH;
        if (computeTotalWorkTime) {
            final IndicatorTotalWorkTime totalWorkTimeIndicator = newInstance(IndicatorTotalWorkTime.class);
            totalWorkTimeIndicator.init(totalWorkTimeFilter, translatedMonth);
            indicators.add(totalWorkTimeIndicator); // Temps de travail total.
        }
        return computeTotalWorkTime;
    }

    private boolean addUTHIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators) {
        final IndicatorFilter uthFilter = indicatorFiltersByClazz.get(IndicatorUTH.class.getSimpleName());
        final boolean computeUTH = uthFilter != null;
        if (computeUTH) {
            IndicatorUTH indicatorUTH = newInstance(IndicatorUTH.class);
            indicatorUTH.init(uthFilter);
            indicators.add(indicatorUTH);
        }
        return computeUTH;
    }

    private boolean addMechanizedWorkforceExpensesIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean computeTotalWorkforceExpenses, List<GenericIndicator> indicators) {
        final IndicatorFilter mechanizedWorkforceExpensesFilter = indicatorFiltersByClazz.get(IndicatorMechanizedWorkforceExpenses.class.getSimpleName());
        final boolean computeMechanizedWorkforceExpenses = mechanizedWorkforceExpensesFilter != null || computeTotalWorkforceExpenses; // required by IndicatorTotalWorkforceExpenses
        if (computeMechanizedWorkforceExpenses) {
            final IndicatorMechanizedWorkforceExpenses mechanizedWorkforceExpensesIndicator = newInstance(IndicatorMechanizedWorkforceExpenses.class);
            mechanizedWorkforceExpensesIndicator.init(mechanizedWorkforceExpensesFilter);
            indicators.add(mechanizedWorkforceExpensesIndicator); // Charges de main d'œuvre mécanisé.
        }
        return computeMechanizedWorkforceExpenses;
    }

    private boolean addTotalWorkforceExpensesIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean computeDirectMargin, List<GenericIndicator> indicators) {
        final IndicatorFilter totalWorkforceExpensesFilter = indicatorFiltersByClazz.get(IndicatorTotalWorkforceExpenses.class.getSimpleName());
        final boolean computeTotalWorkforceExpenses = totalWorkforceExpensesFilter != null || computeDirectMargin; // required by IndicatorDirectMargin
        if (computeTotalWorkforceExpenses) {
            final IndicatorTotalWorkforceExpenses totalWorkforceExpensesIndicator = newInstance(IndicatorTotalWorkforceExpenses.class);
            totalWorkforceExpensesIndicator.init(totalWorkforceExpensesFilter);
            indicators.add(totalWorkforceExpensesIndicator); // Charges de main d'œuvre totales.
        }
        return computeTotalWorkforceExpenses;
    }

    private boolean addFuelConsumptionIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean computeEquipmentsExpenses, List<GenericIndicator> indicators) {
        final IndicatorFilter fuelConsumptionFilter = indicatorFiltersByClazz.get(IndicatorFuelConsumption.class.getSimpleName());
        final boolean computeFuelConsumption = fuelConsumptionFilter != null || computeEquipmentsExpenses; // required by IndicatorEquipmentsExpenses
        if (computeFuelConsumption) {
            final IndicatorFuelConsumption fuelConsumptionIndicator = newInstance(IndicatorFuelConsumption.class);
            fuelConsumptionIndicator.init(fuelConsumptionFilter);
            indicators.add(fuelConsumptionIndicator); // Consommation de carburant d’une combinaison d’outils, d’un automoteur ou d’un tracteur
        }
        return computeFuelConsumption;
    }

    private void addOperatingExpensesIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean computeGrossMargin, List<GenericIndicator> indicators) {
        final IndicatorFilter operatingExpensesFilter = indicatorFiltersByClazz.get(IndicatorOperatingExpenses.class.getSimpleName());
        final boolean computeOperatingExpenses = operatingExpensesFilter != null || computeGrossMargin; // required by IndicatorGrossMargin
        if (computeOperatingExpenses) {
            final IndicatorOperatingExpenses indicatorOperatingExpenses = newInstance(IndicatorOperatingExpenses.class);
            indicatorOperatingExpenses.init(operatingExpensesFilter);
            indicators.add(indicatorOperatingExpenses); // Les charges opérationnelles réelles/standardisées (millésimé)
        }
    }

    private void addGrossIncomeIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean computeGrossMargin, List<GenericIndicator> indicators) {
        final IndicatorFilter grossIncomeFilter = indicatorFiltersByClazz.get(IndicatorGrossIncome.class.getSimpleName());
        final boolean computeGrossIncome = grossIncomeFilter != null || computeGrossMargin; // required by IndicatorGrossMargin
        if (computeGrossIncome) {
            final IndicatorGrossIncome indicatorGrossIncome = newInstance(IndicatorGrossIncome.class);
            indicatorGrossIncome.init(grossIncomeFilter);
            indicators.add(indicatorGrossIncome); // Produit brut réel avec/sans autoconsommation
        }
    }

    private void addStandardisedGrossIncomeIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean computeGrossMargin, List<GenericIndicator> indicators) {
        final IndicatorFilter standardisedGrossIncomeFilter = indicatorFiltersByClazz.get(IndicatorStandardisedGrossIncome.class.getSimpleName());
        final boolean computeStandardizedGrossIncome = standardisedGrossIncomeFilter != null || computeGrossMargin; // required by IndicatorGrossMargin
        if (computeStandardizedGrossIncome) {
            final IndicatorStandardisedGrossIncome indicatorStandardisedGrossIncome = newInstance(IndicatorStandardisedGrossIncome.class);
            indicatorStandardisedGrossIncome.init(standardisedGrossIncomeFilter);
            indicators.add(indicatorStandardisedGrossIncome); // Produit brut standardisé (millésimé) avec/sans l’autoconsommation
        }
    }

    private boolean addEquipmentsExpensesIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean computeSemiNetMargin, List<GenericIndicator> indicators) {
        final IndicatorFilter equipmentsExpensesFilter = indicatorFiltersByClazz.get(IndicatorEquipmentsExpenses.class.getSimpleName());
        final boolean computeEquipmentsExpenses = equipmentsExpensesFilter != null || computeSemiNetMargin; // required by IndicatorSemiNetMargin
        if (computeEquipmentsExpenses) {
            final IndicatorEquipmentsExpenses equipmentsExpensesIndicator = newInstance(IndicatorEquipmentsExpenses.class);
            equipmentsExpensesIndicator.init(equipmentsExpensesFilter);
            indicators.add(equipmentsExpensesIndicator); // Charges de mécanisation réelles et standardisées
        }
        return computeEquipmentsExpenses;
    }

    private boolean addGrossMarginIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean computeSemiNetMargin, List<GenericIndicator> indicators) {
        // required IndicatorGrossIncome, IndicatorOperatingExpenses & IndicatorFuelConsumption to be computed
        final IndicatorFilter grossMarginFilter = indicatorFiltersByClazz.get(IndicatorGrossMargin.class.getSimpleName());
        final boolean computeGrossMargin = grossMarginFilter != null || computeSemiNetMargin; // required by IndicatorSemiNetMargin
        if (computeGrossMargin) {
            final IndicatorGrossMargin indicatorGrossMargin = newInstance(IndicatorGrossMargin.class);
            indicatorGrossMargin.init(grossMarginFilter);
            indicators.add(indicatorGrossMargin); // Marge brut
        }
        return computeGrossMargin;
    }

    private void addSurfaceUTHIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean exportToFile, List<GenericIndicator> indicators) {
        final IndicatorFilter surfaceUTHIndicatorFilter = indicatorFiltersByClazz.get(IndicatorSurfaceUTH.class.getSimpleName());
        IndicatorSurfaceUTH indicatorSurfaceUTH = newInstance(IndicatorSurfaceUTH.class);
        if (surfaceUTHIndicatorFilter != null) {
            indicatorSurfaceUTH.init(surfaceUTHIndicatorFilter, exportToFile);
            indicators.add(indicatorSurfaceUTH);// Surface par UTH (ha)
        }
    }

    private boolean addSemiNetMarginIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, boolean computeDirectMargin, List<GenericIndicator> indicators) {
        // required IndicatorGrossMargin & IndicatorEquipmentsExpenses to be computed
        final IndicatorFilter semiNetMarginFilter = indicatorFiltersByClazz.get(IndicatorSemiNetMargin.class.getSimpleName());
        final boolean computeSemiNetMargin = semiNetMarginFilter != null || computeDirectMargin; // required by IndicatorDirectMargin
        if (computeSemiNetMargin) {
            IndicatorSemiNetMargin indicatorSemiNetMargin = newInstance(IndicatorSemiNetMargin.class);
            indicatorSemiNetMargin.init(semiNetMarginFilter);
            indicators.add(indicatorSemiNetMargin); // Marge semi-nette réelle
        }
        return computeSemiNetMargin;
    }

    private boolean addDirectMarginIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators) {
        // required IndicatorSemiNetMargin & IndicatorTotalWorkforceExpenses
        final IndicatorFilter directMarginFilter = indicatorFiltersByClazz.get(IndicatorDirectMargin.class.getSimpleName());
        final boolean computeDirectMargin = directMarginFilter != null && (directMarginFilter.getComputeReal() || directMarginFilter.getComputeStandardized());
        if (computeDirectMargin) {
            IndicatorDirectMargin indicatorDirectMargin = newInstance(IndicatorDirectMargin.class);
            indicatorDirectMargin.init(directMarginFilter);
            indicators.add(indicatorDirectMargin); // Marge directe réelle
        }
        return computeDirectMargin;
    }

    private void addAgronomicStrategyIndicators(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators, boolean exportToFile) {
        IndicatorFilter numberOfPloughingPassagesFilter = indicatorFiltersByClazz.get(IndicatorNumberOfPloughingPassages.class.getSimpleName());
        if (numberOfPloughingPassagesFilter != null) {
            IndicatorNumberOfPloughingPassages indicatorNumberOfPloughingPassages = newInstance(IndicatorNumberOfPloughingPassages.class);
            indicatorNumberOfPloughingPassages.init(numberOfPloughingPassagesFilter);
            indicators.add(indicatorNumberOfPloughingPassages);
        }

        IndicatorFilter numberOfTCSPassagesFilter = indicatorFiltersByClazz.get(IndicatorNumberOfTCSPassages.class.getSimpleName());
        if (numberOfTCSPassagesFilter != null) {
            IndicatorNumberOfTCSPassages indicatorNumberOfTCSPassages = newInstance(IndicatorNumberOfTCSPassages.class);
            indicatorNumberOfTCSPassages.init(numberOfTCSPassagesFilter);
            indicators.add(indicatorNumberOfTCSPassages);
        }

        IndicatorFilter numberOfMechanicalWeedingPassagesFilter = indicatorFiltersByClazz.get(IndicatorNumberOfMechanicalWeedingPassages.class.getSimpleName());
        if (numberOfMechanicalWeedingPassagesFilter != null) {
            IndicatorNumberOfMechanicalWeedingPassages indicatorNumberOfMechanicalWeedingPassages = newInstance(IndicatorNumberOfMechanicalWeedingPassages.class);
            indicatorNumberOfMechanicalWeedingPassages.init(numberOfMechanicalWeedingPassagesFilter);
            indicators.add(indicatorNumberOfMechanicalWeedingPassages);
        }

        IndicatorFilter usageOfMechanicalWeedingPassagesFilter = indicatorFiltersByClazz.get(IndicatorUsageOfMechanicalWeeding.class.getSimpleName());
        if (usageOfMechanicalWeedingPassagesFilter != null) {
            IndicatorUsageOfMechanicalWeeding indicatorUsageOfMechanicalWeeding = newInstance(IndicatorUsageOfMechanicalWeeding.class);
            indicatorUsageOfMechanicalWeeding.init(usageOfMechanicalWeedingPassagesFilter, exportToFile);
            indicators.add(indicatorUsageOfMechanicalWeeding);
        }
    }

    private void addIFT_Indicators(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators) {

        addIndicatorLegacyIFT(indicatorFiltersByClazz, indicators);

        addIndicatorRefMaxYearTargetIFT(indicatorFiltersByClazz, indicators);

        addIndicatorRefMaxYearCropIFT(indicatorFiltersByClazz, indicators);

        addIndicatorIndicatorVintageCropIFT(indicatorFiltersByClazz, indicators);

        addIndicatorIndicatorVintageTargetIFT(indicatorFiltersByClazz, indicators);

    }

    /**
     * Pour le moment, les indicateurs QSA ne sont calculés que pour les exports individuels, pas pour les exports de masse
     */
    private void addQSAIndicators(
            Map<String, IndicatorFilter> indicatorFiltersByClazz,
            List<GenericIndicator> indicators) {

        final Map<String, Double> coeffsConversionVersKgHa = this.referentialService.getCoeffsConversionVersKgHa();

        IndicatorFilter indicatorNeonicotinoidsAmountFilter = indicatorFiltersByClazz.get(IndicatorNeonicotinoidsAmount.class.getSimpleName());
        final boolean computeNeonicotinoidsAmount = indicatorNeonicotinoidsAmountFilter != null;
        if (computeNeonicotinoidsAmount) {
            IndicatorNeonicotinoidsAmount indicatorNeonicotinoidsAmount = newInstance(IndicatorNeonicotinoidsAmount.class);
            indicatorNeonicotinoidsAmount.init(indicatorNeonicotinoidsAmountFilter);
            indicators.add(indicatorNeonicotinoidsAmount);
        }

        IndicatorFilter indicatorSoilAppliedHerbicidesAmountFilter = indicatorFiltersByClazz.get(IndicatorSoilAppliedHerbicidesAmount.class.getSimpleName());
        final boolean computeSoilAppliedHerbicidesAmount = indicatorSoilAppliedHerbicidesAmountFilter != null;
        if (computeSoilAppliedHerbicidesAmount) {
            IndicatorSoilAppliedHerbicidesAmount indicatorSoilAppliedHerbicidesAmount = newInstance(IndicatorSoilAppliedHerbicidesAmount.class);
            indicatorSoilAppliedHerbicidesAmount.init(indicatorSoilAppliedHerbicidesAmountFilter);
            indicators.add(indicatorSoilAppliedHerbicidesAmount);
        }

        IndicatorFilter indicatorActiveSubstanceAmountFilter = indicatorFiltersByClazz.get(IndicatorActiveSubstanceAmount.class.getSimpleName());
        if (indicatorActiveSubstanceAmountFilter != null || computeNeonicotinoidsAmount || computeSoilAppliedHerbicidesAmount) {
            IndicatorActiveSubstanceAmount indicatorActiveSubstanceAmount = newInstance(IndicatorActiveSubstanceAmount.class);
            indicatorActiveSubstanceAmount.init(indicatorActiveSubstanceAmountFilter, coeffsConversionVersKgHa);
            indicators.add(indicatorActiveSubstanceAmount);
        }

        IndicatorFilter indicatorTotalActiveSubstanceAmountFilter = indicatorFiltersByClazz.get(IndicatorTotalActiveSubstanceAmount.class.getSimpleName());
        if (indicatorTotalActiveSubstanceAmountFilter != null) {
            IndicatorTotalActiveSubstanceAmount indicatorTotalActiveSubstanceAmount = newInstance(IndicatorTotalActiveSubstanceAmount.class);
            indicatorTotalActiveSubstanceAmount.init(indicatorTotalActiveSubstanceAmountFilter, coeffsConversionVersKgHa);
            indicators.add(indicatorTotalActiveSubstanceAmount);
        }

        IndicatorFilter indicatorHRI1Filter = indicatorFiltersByClazz.get(IndicatorHRI1.class.getSimpleName());
        if (indicatorHRI1Filter != null) {
            IndicatorHRI1 indicatorHRI1 = newInstance(IndicatorHRI1.class);
            indicatorHRI1.init(indicatorHRI1Filter, coeffsConversionVersKgHa);
            indicators.add(indicatorHRI1);
        }

        IndicatorFilter indicatorHRI1_g1_Filter = indicatorFiltersByClazz.get(IndicatorHRI1_G1.class.getSimpleName());
        if (indicatorHRI1_g1_Filter != null) {
            IndicatorHRI1_G1 indicator = newInstance(IndicatorHRI1_G1.class);
            indicator.init(indicatorHRI1_g1_Filter, coeffsConversionVersKgHa);
            indicators.add(indicator);
        }

        IndicatorFilter indicatorHRI1_g2_Filter = indicatorFiltersByClazz.get(IndicatorHRI1_G2.class.getSimpleName());
        if (indicatorHRI1_g2_Filter != null) {
            IndicatorHRI1_G2 indicator = newInstance(IndicatorHRI1_G2.class);
            indicator.init(indicatorHRI1_g2_Filter, coeffsConversionVersKgHa);
            indicators.add(indicator);
        }

        IndicatorFilter indicatorHRI1_g3_Filter = indicatorFiltersByClazz.get(IndicatorHRI1_G3.class.getSimpleName());
        if (indicatorHRI1_g3_Filter != null) {
            IndicatorHRI1_G3 indicator = newInstance(IndicatorHRI1_G3.class);
            indicator.init(indicatorHRI1_g3_Filter, coeffsConversionVersKgHa);
            indicators.add(indicator);
        }

        IndicatorFilter indicatorHRI1_g4_Filter = indicatorFiltersByClazz.get(IndicatorHRI1_G1.class.getSimpleName());
        if (indicatorHRI1_g4_Filter != null) {
            IndicatorHRI1_G4 indicator = newInstance(IndicatorHRI1_G4.class);
            indicator.init(indicatorHRI1_g4_Filter, coeffsConversionVersKgHa);
            indicators.add(indicator);
        }

        IndicatorFilter indicatorSubstancesCandidateToSubstittionFilter = indicatorFiltersByClazz.get(IndicatorSubstancesCandidateToSubstitution.class.getSimpleName());
        if (indicatorSubstancesCandidateToSubstittionFilter != null) {
            IndicatorSubstancesCandidateToSubstitution indicatorSubstancesCandidateToSubstittion = newInstance(IndicatorSubstancesCandidateToSubstitution.class);
            indicatorSubstancesCandidateToSubstittion.init(indicatorSubstancesCandidateToSubstittionFilter, coeffsConversionVersKgHa);
            indicators.add(indicatorSubstancesCandidateToSubstittion);
        }

        IndicatorFilter indicatorLowRiskSubstancesFilter = indicatorFiltersByClazz.get(IndicatorLowRiskSubstances.class.getSimpleName());
        if (indicatorLowRiskSubstancesFilter != null) {
            IndicatorLowRiskSubstances indicatorLowRiskSubstances = newInstance(IndicatorLowRiskSubstances.class);
            indicatorLowRiskSubstances.init(indicatorLowRiskSubstancesFilter, coeffsConversionVersKgHa);
            indicators.add(indicatorLowRiskSubstances);
        }

        IndicatorFilter indicatorCMRFilter = indicatorFiltersByClazz.get(IndicatorCMR.class.getSimpleName());
        if (indicatorCMRFilter != null) {
            IndicatorCMR indicatorCMR = newInstance(IndicatorCMR.class);
            indicatorCMR.init((indicatorCMRFilter), coeffsConversionVersKgHa);
            indicators.add(indicatorCMR);
        }

        IndicatorFilter indicatorEnvironementalRiskFilter = indicatorFiltersByClazz.get(IndicatorEnvironmentalRisk.class.getSimpleName());
        if (indicatorEnvironementalRiskFilter != null) {
            IndicatorEnvironmentalRisk indicatorEnvironmentalRisk = newInstance(IndicatorEnvironmentalRisk.class);
            indicatorEnvironmentalRisk.init(indicatorEnvironementalRiskFilter, coeffsConversionVersKgHa);
            indicators.add(indicatorEnvironmentalRisk);
        }

        IndicatorFilter indicatorCMRUsesFilter = indicatorFiltersByClazz.get(IndicatorCMRUses.class.getSimpleName());
        if (indicatorCMRUsesFilter != null) {
            IndicatorCMRUses indicatorCMRUses = newInstance(IndicatorCMRUses.class);
            indicatorCMRUses.init(indicatorCMRUsesFilter);
            indicators.add(indicatorCMRUses);
        }

        IndicatorFilter indicatorToxicUserUsesFilter = indicatorFiltersByClazz.get(IndicatorToxicUserUses.class.getSimpleName());
        if (indicatorToxicUserUsesFilter != null) {
            IndicatorToxicUserUses indicatorToxicUserUses = newInstance(IndicatorToxicUserUses.class);
            indicatorToxicUserUses.init(indicatorToxicUserUsesFilter);
            indicators.add(indicatorToxicUserUses);
        }

        IndicatorFilter indicatorEnvironmentalRiskUsesFilter = indicatorFiltersByClazz.get(IndicatorEnvironmentalRiskUses.class.getSimpleName());
        if (indicatorEnvironmentalRiskUsesFilter != null) {
            IndicatorEnvironmentalRiskUses indicatorToxicUserUses = newInstance(IndicatorEnvironmentalRiskUses.class);
            indicatorToxicUserUses.init(indicatorEnvironmentalRiskUsesFilter);
            indicators.add(indicatorToxicUserUses);
        }

        IndicatorFilter indicatorIndicatorToxicUserFilter = indicatorFiltersByClazz.get(IndicatorToxicUser.class.getSimpleName());
        if (indicatorIndicatorToxicUserFilter != null) {
            IndicatorToxicUser indicatorToxicUser = newInstance(IndicatorToxicUser.class);
            indicatorToxicUser.init(indicatorIndicatorToxicUserFilter, coeffsConversionVersKgHa);
            indicators.add(indicatorToxicUser);
        }

        IndicatorFilter copperTotalProductFilter = indicatorFiltersByClazz.get(IndicatorCopperTotalProduct.class.getSimpleName());
        final boolean computeCopperTotal = copperTotalProductFilter != null;
        if (computeCopperTotal) {
            IndicatorCopperTotalProduct indicatorCopperTotalProduct = newInstance(IndicatorCopperTotalProduct.class);
            indicatorCopperTotalProduct.init(copperTotalProductFilter);
            indicators.add(indicatorCopperTotalProduct);
        }

        IndicatorFilter sulfurTotalProductFilter = indicatorFiltersByClazz.get(IndicatorSulfurTotalProduct.class.getSimpleName());
        final boolean computeSulfurTotal = sulfurTotalProductFilter != null;
        if (computeSulfurTotal) {
            IndicatorSulfurTotalProduct indicatorSulfurTotalProduct = newInstance(IndicatorSulfurTotalProduct.class);
            indicatorSulfurTotalProduct.init(sulfurTotalProductFilter);
            indicators.add(indicatorSulfurTotalProduct);
        }

        IndicatorFilter copperFertilisationProductFilter = indicatorFiltersByClazz.get(IndicatorCopperFertilisationProduct.class.getSimpleName());
        boolean computeCopperFerti = copperFertilisationProductFilter != null || computeCopperTotal;
        if (computeCopperFerti) {
            IndicatorCopperFertilisationProduct indicatorCopperFertilisationProduct = newInstance(IndicatorCopperFertilisationProduct.class);
            indicatorCopperFertilisationProduct.init(copperFertilisationProductFilter);
            indicators.add(indicatorCopperFertilisationProduct);
        }

        IndicatorFilter sulfurFertilisationProductFilter = indicatorFiltersByClazz.get(IndicatorSulfurFertilisationProduct.class.getSimpleName());
        boolean computeSulfurFerti = sulfurFertilisationProductFilter != null;
        if (computeSulfurFerti) {
            IndicatorSulfurFertilisationProduct indicatorSulfurFertilisationProduct = newInstance(IndicatorSulfurFertilisationProduct.class);
            indicatorSulfurFertilisationProduct.init(sulfurFertilisationProductFilter);
            indicators.add(indicatorSulfurFertilisationProduct);
        }

        IndicatorFilter indicatorCopperPhytoProductFilter = indicatorFiltersByClazz.get(IndicatorCopperPhytoProduct.class.getSimpleName());
        if (indicatorCopperPhytoProductFilter != null || computeCopperTotal) {
            IndicatorCopperPhytoProduct indicatorCopperPhytoProduct = newInstance(IndicatorCopperPhytoProduct.class);
            indicatorCopperPhytoProduct.init(indicatorCopperPhytoProductFilter, coeffsConversionVersKgHa);
            indicators.add(indicatorCopperPhytoProduct);
        }

        IndicatorFilter indicatorSulfurPhytoProductFilter = indicatorFiltersByClazz.get(IndicatorSulfurPhytoProduct.class.getSimpleName());
        if (indicatorSulfurPhytoProductFilter != null || computeSulfurTotal) {
            IndicatorSulfurPhytoProduct indicatorSulfurPhytoProduct = newInstance(IndicatorSulfurPhytoProduct.class);
            indicatorSulfurPhytoProduct.init(indicatorSulfurPhytoProductFilter, coeffsConversionVersKgHa);
            indicators.add(indicatorSulfurPhytoProduct);
        }
    }

    /**
     * @return true s'il faut calculer l'indicateur de fertilisation totale
     */
    protected boolean addIndicatorTotalFertilization(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators) {
        final IndicatorFilter totalFertilizationFilter = indicatorFiltersByClazz.get(IndicatorTotalFertilization.class.getSimpleName());

        if (totalFertilizationFilter != null && CollectionUtils.isNotEmpty(totalFertilizationFilter.getTotalFertilizations())) {
            IndicatorTotalFertilization indicatorTotalFertilization = newInstance(IndicatorTotalFertilization.class);
            indicatorTotalFertilization.init(totalFertilizationFilter.getTotalFertilizations());
            indicators.add(indicatorTotalFertilization);
        }

        return totalFertilizationFilter != null;
    }

    protected void addMineralFertilizationIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators, boolean computeQSACopperFerti, boolean computeQSASulfurFerti, boolean computeTotalFertilization) {
        final IndicatorFilter mineralFertilizationFilter = indicatorFiltersByClazz.get(IndicatorMineralFertilization.class.getSimpleName());
        if (mineralFertilizationFilter != null && CollectionUtils.isNotEmpty(mineralFertilizationFilter.getMineralFertilizations()) || computeQSACopperFerti || computeQSASulfurFerti || computeTotalFertilization) {
            var mineralFertilizations = mineralFertilizationFilter != null && mineralFertilizationFilter.getMineralFertilizations() != null ? Sets.newHashSet(mineralFertilizationFilter.getMineralFertilizations()) : new HashSet<MineralFertilization>();
            if (computeQSACopperFerti) mineralFertilizations.add(MineralFertilization.CU);
            if (computeQSASulfurFerti) mineralFertilizations.add(MineralFertilization.SO3);
            IndicatorMineralFertilization indicatorMineralFertilization = newInstance(IndicatorMineralFertilization.class);
            indicatorMineralFertilization.init(mineralFertilizationFilter, mineralFertilizations);
            indicators.add(indicatorMineralFertilization);
        }
    }

    protected void addOrganicFertilizationIndicator(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators, boolean computeQSASulfurFerti, boolean computeTotalFertilization) {
        final IndicatorFilter organicFertilizationFilter = indicatorFiltersByClazz.get(IndicatorOrganicFertilization.class.getSimpleName());
        if (organicFertilizationFilter != null && CollectionUtils.isNotEmpty(organicFertilizationFilter.getOrganicFertilizations()) || computeQSASulfurFerti || computeTotalFertilization) {
            var organicFertilisations = organicFertilizationFilter != null && organicFertilizationFilter.getOrganicFertilizations() != null ? Sets.newHashSet(organicFertilizationFilter.getOrganicFertilizations()) : new HashSet<OrganicFertilization>();
            if (computeQSASulfurFerti) organicFertilisations.add(OrganicFertilization.S);
            IndicatorOrganicFertilization indicatorOrganicFertilization = newInstance(IndicatorOrganicFertilization.class);
            indicatorOrganicFertilization.init(organicFertilizationFilter, organicFertilisations);
            indicators.add(indicatorOrganicFertilization);
        }
    }

    protected void addIndicatorLegacyIFT(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators) {
        final IndicatorFilter iftFilter = indicatorFiltersByClazz.get("IndicatorLegacyIFT");
        final Collection<Ift> computedIfts = iftFilter != null && iftFilter.getIfts() != null ? iftFilter.getIfts() : new ArrayList<>();
        final IndicatorLegacyIFT indicatorIFT = newInstance(IndicatorLegacyIFT.class);
        indicatorIFT.init(computedIfts);
        indicators.add(indicatorIFT);
    }

    protected void addIndicatorRefMaxYearTargetIFT(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators) {
        // Cet indicateur est toujours coché au niveau de l'UI
        final IndicatorFilter iftFilter = indicatorFiltersByClazz.get("IndicatorRefMaxYearTargetIFT");
        if (iftFilter != null && CollectionUtils.isNotEmpty(iftFilter.getIfts())) {
            final IndicatorRefMaxYearTargetIFT indicatorIFT = newInstance(IndicatorRefMaxYearTargetIFT.class);
            indicatorIFT.init(iftFilter.getIfts());
            indicators.add(indicatorIFT);
        } else if (indicatorFiltersByClazz.get(IndicatorOperatingExpenses.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorActiveSubstanceAmount.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorSubstancesCandidateToSubstitution.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorTotalActiveSubstanceAmount.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorCMR.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorEnvironmentalRisk.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorToxicUser.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorHRI1.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorHRI1_G1.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorHRI1_G2.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorHRI1_G3.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorHRI1_G4.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorLowRiskSubstances.class.getSimpleName()) != null
        ) {
            final IndicatorRefMaxYearTargetIFT indicatorIFT = newInstance(IndicatorRefMaxYearTargetIFT.class);
            indicators.add(indicatorIFT);
        }
    }

    protected void addIndicatorIndicatorVintageTargetIFT(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators) {
        final IndicatorFilter iftFilter = indicatorFiltersByClazz.get("IndicatorVintageTargetIFT");
        if (iftFilter != null && CollectionUtils.isNotEmpty(iftFilter.getIfts())) {
            final IndicatorVintageTargetIFT indicatorIFT = newInstance(IndicatorVintageTargetIFT.class);
            indicatorIFT.init(iftFilter.getIfts());
            indicators.add(indicatorIFT);
        } else if (indicatorFiltersByClazz.get(IndicatorActiveSubstanceAmount.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorTotalActiveSubstanceAmount.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorHRI1.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorHRI1_G1.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorHRI1_G2.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorHRI1_G3.class.getSimpleName()) != null ||
                indicatorFiltersByClazz.get(IndicatorHRI1_G4.class.getSimpleName()) != null) {
            final IndicatorVintageTargetIFT indicatorIFT = newInstance(IndicatorVintageTargetIFT.class);
            indicators.add(indicatorIFT);
        }
    }

    protected void addIndicatorRefMaxYearCropIFT(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators) {
        final IndicatorFilter iftFilter = indicatorFiltersByClazz.get("IndicatorRefMaxYearCropIFT");
        if (iftFilter != null && CollectionUtils.isNotEmpty(iftFilter.getIfts())) {
            final IndicatorRefMaxYearCropIFT indicatorIFT = newInstance(IndicatorRefMaxYearCropIFT.class);
            indicatorIFT.init(iftFilter.getIfts());
            indicators.add(indicatorIFT);
        }
    }

    protected void addIndicatorIndicatorVintageCropIFT(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators) {
        final IndicatorFilter iftFilter = indicatorFiltersByClazz.get("IndicatorVintageCropIFT");
        if (iftFilter != null && CollectionUtils.isNotEmpty(iftFilter.getIfts())) {
            final IndicatorVintageCropIFT indicatorIFT = newInstance(IndicatorVintageCropIFT.class);
            indicatorIFT.init(iftFilter.getIfts());
            indicators.add(indicatorIFT);
        }
    }

    protected void addIndicatorDecomposedOperatingExpenses(Map<String, IndicatorFilter> indicatorFiltersByClazz, List<GenericIndicator> indicators) {
        final IndicatorFilter doeFilter = indicatorFiltersByClazz.get(IndicatorDecomposedOperatingExpenses.class.getSimpleName());
        if (doeFilter != null && CollectionUtils.isNotEmpty(doeFilter.getDoeIndicators())) {
            final IndicatorDecomposedOperatingExpenses indicatorDOE = newInstance(IndicatorDecomposedOperatingExpenses.class);
            indicatorDOE.init(doeFilter);
            indicators.add(indicatorDOE);
        }
    }

    protected PerformancePracticedExecutionContextBuilder getPerformancePracticedExecutionContextBuilder(
            boolean executeAsAdmin,
            PriceConverterKeysToRate priceConverterKeysToRate,
            RefSolTextureGeppa defaultRefSolTextureGeppa,
            Set<String> scenarioCodes,
            RefLocation defaultLocation,
            RefSolProfondeurIndigo defaultSolDepth,
            Collection<IndicatorFilter> indicatorFilters) {

        PerformancePracticedExecutionContextBuilder performanceExecutionBuilder =
                new PerformancePracticedExecutionContextBuilder(
                        anonymizeService,
                        domainInputStockUnitService,
                        domainService,
                        inputPriceService,
                        this,
                        referentialService,
                        refInputPriceService,
                        pricesService,
                        indicatorFilters,
                        executeAsAdmin,
                        priceConverterKeysToRate,
                        defaultRefSolTextureGeppa,
                        scenarioCodes,
                        defaultLocation,
                        defaultSolDepth
                );
        return performanceExecutionBuilder;
    }

    @Override
    public Performance createPerformanceToDb(List<String> filteredDomainIds) {

        authorizationService.checkIsAdmin();

        BusinessTasksManager businessTasksManager = getBusinessTaskManager();

        boolean isGlobalPerformanceRunning = businessTasksManager.getRunningAndPendingTasks()
                .stream()
                .map(ScheduledTask::getTask)
                .anyMatch(task -> task instanceof PerformanceTask &&
                        ((PerformanceTask) task).isDbPerformanceTask());
        if (isGlobalPerformanceRunning) {
            return null;
        }

        final AuthenticatedUser userDto = getAuthenticatedUser();
        AgrosystUser user = userDao.forTopiaIdEquals(userDto.getTopiaId()).findUnique();

        final List<String> fullDomainIds = filteredDomainIds != null ? new ArrayList<>(DaoUtils.getRealIds(filteredDomainIds, Domain.class)) : null;
        final DbPerformanceDomainsContext dbPerformanceDomainContext = new DbPerformanceDomainsContext(performanceDao, fullDomainIds);

        final Set<String> scenarioCodes = getAllScenarioCodes();

        final RefSolTextureGeppa defaultRefSolTextureGeppa = refSolTextureGeppaDao.forNaturalId(DEFAULT_TEXTURAL_CLASSE).findUnique();
        final RefSolProfondeurIndigo defaultSolDepth = refSolProfondeurIndigosDao.forNaturalId(DEFAULT_DEEPEST_INDIGO).findUnique();
        final RefLocation defaultLocation = refLocationDao.forCodeInseeEquals(PARIS_DEFAULT_INSEE_CODE).findUnique();
        final List<RefHarvestingPriceConverter> allHarvestingPriceConverters = refHarvestingPriceConverterDao.findAll();
        final PriceConverterKeysToRate priceConverterKeysToRate = new PriceConverterKeysToRate(allHarvestingPriceConverters);

        final DbCommonReferentiels dbCommonReferentiels = new DbCommonReferentiels(
                defaultRefSolTextureGeppa,
                defaultSolDepth,
                defaultLocation,
                priceConverterKeysToRate);

        final OffsetDateTime performanceDate = OffsetDateTime.now();
        Performance performance = performanceDao.newInstance();
        performance.setName("Export global vers base");
        performance.setComputeStatus(PerformanceState.GENERATING);
        performance.setAuthor(user);
        performance.setUpdateDate(performanceDate);
        performance.setNbTasks(dbPerformanceDomainContext.getDbPerformanceDomainsContext().size());
        performance.setExportType(ExportType.DB);

        performance = performanceDao.create(performance);

        if (LOGGER.isDebugEnabled()) {
            String message = "For performance status is now GENERATING";
            LOGGER.debug(String.format(message, performance.getTopiaId()));
        }

        getTransaction().commit();

        String performanceId = performance.getTopiaId();
        for (DbPerformanceDomainContext performanceDomainContext : dbPerformanceDomainContext.getDbPerformanceDomainsContext()) {

            PerformanceDbTask task = new PerformanceDbTask(
                    performanceId,
                    userDto.getTopiaId(),
                    userDto.getEmail(),
                    scenarioCodes,
                    dbCommonReferentiels,
                    performanceDomainContext,
                    performanceDate);

            businessTasksManager.schedule(task);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("ADD TASK  " + task.getDescription());
            }

        }
        if (LOGGER.isInfoEnabled()) {
            LOGGER.debug(String.format("Exécution de la Performance en cours, %d tâches programmées", performance.getNbTasks()));
        }

        return performance;
    }

    @Override
    public List<IndicatorFilter> getAllIndicatorFilters() {
        List<IndicatorFilter> indicatorFilters = new ArrayList<>();
        String pkg = Indicator.class.getPackage().getName();
        Reflections reflections = new Reflections(pkg);
        List<String> classNames = reflections.getSubTypesOf(Indicator.class).stream()
                .filter(cl -> !Modifier.isAbstract(cl.getModifiers()))
                .map(Class::getSimpleName).sorted(String::compareTo).toList();

        for (String className : classNames) {
            IndicatorFilter indicatorFilter = new IndicatorFilterImpl();
            indicatorFilter.setClazz(className);
            indicatorFilter.setComputeReal(true);
            indicatorFilter.setComputeStandardized(true);
            indicatorFilter.setNoDetailed(true);
            indicatorFilter.setDetailedByMonth(true);
            indicatorFilter.setWithAutoConsumed(true);
            indicatorFilter.setWithoutAutoConsumed(true);
            indicatorFilter.setWithSeedingTreatment(true);
            indicatorFilter.setWithoutSeedingTreatment(true);

            if (className.equals(IndicatorLegacyIFT.class.getSimpleName()) ||
                    className.equals(IndicatorRefMaxYearTargetIFT.class.getSimpleName()) ||
                    className.equals(IndicatorVintageTargetIFT.class.getSimpleName()) ||
                    className.equals(IndicatorRefMaxYearCropIFT.class.getSimpleName()) ||
                    className.equals(IndicatorVintageCropIFT.class.getSimpleName())
            ) {
                indicatorFilter.addAllIfts(Arrays.asList(Ift.values()));
            }

            if (className.equals(IndicatorDecomposedOperatingExpenses.class.getSimpleName())) {
                indicatorFilter.addAllDoeIndicators(Set.of(DecomposedOperatingExpenses.values()));
            }

            if (className.equals(IndicatorOrganicProducts.class.getSimpleName())) {
                indicatorFilter.addAllOrganicProducts(Set.of(OrganicProduct.values()));
            }

            if (className.equals(IndicatorMineralFertilization.class.getSimpleName())) {
                indicatorFilter.addAllMineralFertilizations(Set.of(MineralFertilization.values()));
            }

            if (className.equals(IndicatorOrganicFertilization.class.getSimpleName())) {
                indicatorFilter.addAllOrganicFertilizations(Set.of(OrganicFertilization.values()));
            }

            if (className.equals(IndicatorTotalFertilization.class.getSimpleName())) {
                indicatorFilter.addAllTotalFertilizations(Set.of(TotalFertilization.values()));
            }

            if (className.equals(IndicatorActiveSubstanceAmount.class.getSimpleName())) {
                indicatorFilter.addAllActiveSubstances(Set.of(ActiveSubstance.values()));
            }

            indicatorFilters.add(indicatorFilter);
        }
        return indicatorFilters;
    }

    @Override
    public List<String> getQSAIndicatorClassNames() {
        String pkg = AbstractIndicatorQSA.class.getPackage().getName();
        Reflections reflections = new Reflections(pkg);
        List<String> classNames = reflections.getSubTypesOf(AbstractIndicatorQSA.class).stream()
                .filter(cl -> !Modifier.isAbstract(cl.getModifiers()))
                .map(Class::getSimpleName).sorted(String::compareTo).toList();
        return classNames;
    }

    protected Map<String, HarvestingValorisationPriceSummary> getEffectiveRefHarvestingValorisationPriceSummaryByValorisationIds(
            Collection<HarvestingActionValorisation> allPracticedSystemValorisations,
            String performanceId) {

        Map<String, HarvestingValorisationPriceSummary> refHarvestingPrices = new HashMap<>();

        if (CollectionUtils.isNotEmpty(allPracticedSystemValorisations)) {

            Set<String> speciesCodes = allPracticedSystemValorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
            Map<String, Set<Pair<String, String>>>  codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);

            try {
                refHarvestingPrices =
                        pricesService.getEffectiveAverageReferencePricesForValorisations(
                                allPracticedSystemValorisations,
                                null,
                                codeEspBotCodeQualiBySpeciesCode
                        );

            } catch (Exception e) {
                saveState(performanceId, PerformanceState.FAILED);
                // peut être remonté en cas de valorisation aux campagnes non valides
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("ÉCHEC DU CALCUL DES PRIX DE RÉCOLTE'", e);
                }
            }
        }

        return refHarvestingPrices;
    }
    protected Map<String, HarvestingValorisationPriceSummary> getPracticedRefHarvestingValorisationPriceSummaryByValorisationIds(
            PracticedSystem practicedSystem,
            Collection<HarvestingActionValorisation> allPracticedSystemValorisations,
            String performanceId) {

        Map<String, HarvestingValorisationPriceSummary> refHarvestingPrices = new HashMap<>();

        if (CollectionUtils.isNotEmpty(allPracticedSystemValorisations)) {

            Set<String> speciesCodes = allPracticedSystemValorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);

            try {
                refHarvestingPrices =
                        pricesService.getPracticedAverageReferencePricesForValorisations(
                                allPracticedSystemValorisations,
                                practicedSystem.getCampaigns(),
                                codeEspBotCodeQualiBySpeciesCode
                        );

            } catch (Exception e) {
                saveState(performanceId, PerformanceState.FAILED);
                // peut être remonté en cas de valorisation aux campagnes non valides
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("ÉCHEC DU CALCUL DES PRIX DE RÉCOLTE'", e);
                }
            }
        }

        return refHarvestingPrices;
    }

    protected void writePracticedPrice(
            IndicatorWriter writer,
            String performanceId,
            Domain domain,
            Collection<PerformancePracticedSystemExecutionContext> practicedSystemContexts,
            RefScenariosInputPricesByDomainInput refScenariosInputPricesByDomainInput,
            MultiKeyMap<String, List<RefHarvestingPrice>>  refHarvestingPricesByCodeEspeceAndCodeDestination) {

        if (writer instanceof SqlWriter) return; // NOTHING TO DO

        for (PerformancePracticedSystemExecutionContext practicedSystemContext : practicedSystemContexts) {
            RefCampaignsInputPricesByDomainInput practicedSystemDomainInputPriceRefPrices = practicedSystemContext.getPracticedSystemInputRefPricesByDomainInput();
            List<HarvestingPrice> harvestingPrices = practicedSystemContext.getHarvestingPrices();
            PracticedSystem practicedSystem = practicedSystemContext.getPracticedSystem();

            Collection<HarvestingActionValorisation> allPracticedSystemValorisations =
                    getPracticedSystemHarvestingActionValorisations(practicedSystemContext);

            Map<String, HarvestingValorisationPriceSummary>
                    refHarvestingPrices = getPracticedRefHarvestingValorisationPriceSummaryByValorisationIds(
                    practicedSystem,
                    allPracticedSystemValorisations,
                    performanceId);

            List<Equipment> equipments = practicedSystemService.getEquipmentsForInterventionPracticedSystem(practicedSystem, domain);

            List<CroppingPlanSpecies> croppingPlanSpecies = null;

            if (practicedSystemContext.getCropByCodeWithSpecies() != null) {
                croppingPlanSpecies = practicedSystemContext.getCropByCodeWithSpecies().values().stream()
                        .map(CropWithSpecies::getCroppingPlanEntry)
                        .flatMap(cpe -> cpe.getCroppingPlanSpecies() != null ? cpe.getCroppingPlanSpecies().stream() : Stream.empty())
                        .toList();
            }

            writer.writePrices(
                    practicedSystemDomainInputPriceRefPrices,
                    harvestingPrices,
                    refHarvestingPrices,
                    equipments,
                    domain,
                    practicedSystem,
                    refScenariosInputPricesByDomainInput,
                    refHarvestingPricesByCodeEspeceAndCodeDestination,
                    croppingPlanSpecies);
        }
    }

    protected void writeEffectivePerformanceToDb(String performanceId,
                                                 OffsetDateTime performanceDate, Set<String> scenarioCodes,
                                                 Collection<IndicatorFilter> indicatorFilters,
                                                 List<GenericIndicator> indicators,
                                                 DbCommonReferentiels dbCommonReferentiels,
                                                 DbPerformanceDomainContext dbPerformanceDomainContext) {

        final IndicatorWriter writer = new SqlWriter(performanceDao, performanceId, performanceDate);

        GenericIndicator[] allIndicator = new GenericIndicator[indicators.size()];
        writer.addComputedIndicators(indicators.toArray(allIndicator));

        final RefSolTextureGeppa defaultRefSolTextureGeppa = dbCommonReferentiels.defaultRefSolTextureGeppa();
        final RefSolProfondeurIndigo defaultSolDepth = dbCommonReferentiels.defaultSolDepth();
        final RefLocation defaultLocation = dbCommonReferentiels.defaultLocation();
        final PriceConverterKeysToRate priceConverterKeysToRate = dbCommonReferentiels.priceConverterKeysToRate();

        // pour un domaine, on récupére l'ensemble des systèmes de cultures
        // visibles par un utilisateur (sécuritycontext) et pour le domaine courant
        final Domain domain = domainDao.forTopiaIdEquals(dbPerformanceDomainContext.domainId()).findUnique();
        List<Equipment> equipments = domainService.getEquipmentsForDomain(domain);

        // get all domain plots
        List<Zone> zones = zoneDao.forTopiaIdIn(dbPerformanceDomainContext.zoneIds()).findAll();

        if (zones.isEmpty()) {
            return;
        }

        long start = System.currentTimeMillis();
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Pour domaine: " + domain.getName() + " (" + domain.getCampaign() + ") " + " code : '" + domain.getCode() + "', " +
                    "DEBUT du calcul des indicateurs ");
        }

        Set<Plot> computedPlots = zones.stream().map(Zone::getPlot).collect(Collectors.toSet());

        Collection<String> codeAmmBioControle = referentialService.getCodeAmmBioControle(domain.getCampaign());
        Map<String, String> groupesCiblesParCode = referentialService.getGroupesCiblesParCode();

        PerformanceEffectiveExecutionContextBuilder performanceEffectiveExecutionBuilder = getEffectiveExecutionContextBuilder(
                scenarioCodes,
                defaultRefSolTextureGeppa,
                defaultSolDepth,
                defaultLocation,
                priceConverterKeysToRate,
                domain,
                domain,
                computedPlots,
                codeAmmBioControle,
                groupesCiblesParCode,
                indicatorFilters);


        PerformanceGlobalExecutionContext effectiveGlobalExecutionContext = performanceEffectiveExecutionBuilder.getPerformanceGlobalExecutionContext();

        performanceEffectiveExecutionBuilder.buildPerformanceExecutionContext(zones);
        PerformanceEffectiveDomainExecutionContext effectiveDomainContext = performanceEffectiveExecutionBuilder.getDomainExecutionContext();
        Map<BasicPlot, Set<PerformanceZoneExecutionContext>> plotZoneContexts = effectiveDomainContext.getPlotZoneContext();

        Collection<HarvestingActionValorisation> allDomainValorisations = getDomainHarvestingActionValorisations(effectiveDomainContext);

        writeEffectivePrices(performanceId, writer, effectiveDomainContext, allDomainValorisations, equipments);

        Collection<PerformanceGrowingSystemExecutionContext> growingSystemsContexts = effectiveDomainContext.getGrowingSystemContextByGrowingSystems().values();

        long elapsedD = System.currentTimeMillis() - start;
        if (elapsedD > getConfig().getPerformanceSlowContextThreshold()) {
            long gsCount = growingSystemsContexts.stream().map(PerformanceGrowingSystemExecutionContext::getGrowingSystem).filter(Optional::isPresent).count();
            long zoneCount = plotZoneContexts.size();
            LOGGER.warn(" FIN de construction des contextes pour le domaine: " + domain.getName() + " (" + domain.getCampaign() + ")" +
                    " nb Système de culture " + gsCount +
                    " nb zone " + zoneCount +
                    " Réalisé en :" + elapsedD + "ms");
        }

        for (PerformanceGrowingSystemExecutionContext growingSystemContext : growingSystemsContexts) {
            Pair<Optional<GrowingSystem>, Optional<GrowingSystem>> growingSystemAndAnonymizeGrowingSystem =
                    growingSystemContext.getGrowingSystemAndAnonymizeGrowingSystem();

            Optional<GrowingSystem> optionalGrowingSystem = growingSystemAndAnonymizeGrowingSystem.getLeft();

            Set<PerformancePlotExecutionContext> plotContexts = growingSystemContext.getPlotContexts();
            for (PerformancePlotExecutionContext plotContext : plotContexts) {

                Plot anonymizePlot = plotContext.getPlot();
                Set<PerformanceZoneExecutionContext> zoneContexts = plotZoneContexts.get(anonymizePlot);

                Map<Pair<RefDestination, YealdUnit>, List<Double>> plotYealds = new HashMap<>();

                for (PerformanceZoneExecutionContext zoneContext : zoneContexts) {
                    Zone zone = zoneContext.getZone();

                    Map<Pair<RefDestination, YealdUnit>, Double> zoneYealAverage = computeYealdAverageByDestinationAndUnit(zoneContext.getZoneYealds());
                    zoneContext.setZoneAverageYeald(zoneYealAverage);
                    addZoneYealdAverage(plotYealds, zoneYealAverage);

                    for (GenericIndicator gIndicator : indicators) {
                        Indicator indicator = (Indicator) gIndicator;
                        long startI = System.currentTimeMillis();
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("Pour domaine: " + domain.getName() + " (" + domain.getCampaign() + "), zone :" + zone.getName() +
                                    " calcul de l'indicateur " + indicator.getClass().getSimpleName() + " pour le réalisé ");
                        }

                        // its, irs, Espèces, Variétés, Type de conduite du SdC, Rendement (Agrégation par destination)
                        try {
                            // write intervention sheet, zone sheet
                            indicator.computeEffective(
                                    writer,
                                    effectiveGlobalExecutionContext,
                                    effectiveDomainContext,
                                    zoneContext);

                        } catch (Exception e) {
                            if (LOGGER.isErrorEnabled()) {
                                String message = String.format(
                                        """
                                                Exception sur le calcul des performance du réalisé, domain %s ('%s'), zoneId '%s', indicateur %s, avec l'erreur suivante:
                                                %s
                                                %s""",
                                        domain.getName(),
                                        domain.getTopiaId(),
                                        zone.getTopiaId(),
                                        indicator.getClass().getSimpleName(),
                                        e.getMessage(),
                                        e.getCause());
                                LOGGER.error(message, e);
                            }
                            saveState(performanceId, PerformanceState.FAILED);
                        }

                        long elapsed = System.currentTimeMillis() - startI;
                        if (elapsed > getConfig().getPerformanceSlowIndicatorThreshold()) {
                            LOGGER.warn(" FIN du calcul de l'indicateur :" + indicator.getClass().getSimpleName() + ", pour domaine: " + domain.getName() + " (" + domain.getCampaign() + "), zone :" + zone.getName() +
                                    " Réalisé en :" + elapsed + "ms");
                        }

                        if (LOGGER.isDebugEnabled()) {
                            if (elapsed <= getConfig().getPerformanceSlowIndicatorThreshold()) {
                                LOGGER.debug("-> Pour domaine: " + domain.getName() + " (" + domain.getCampaign() + "), zone :" + zone.getName() +
                                        " FIN du calcul de l'indicateur " + indicator.getClass().getSimpleName() +
                                        " Réalisé en :" + elapsed + "ms");
                            }
                        }
                    }

                    afterAllIndicatorsAreWritten(writer, performanceId, domain);
                }

                Map<Pair<RefDestination, YealdUnit>, Double> plotYealdAverages = computeYealdAverageByDestinationAndUnit(plotYealds);
                plotContext.setPlotYealdAverages(plotYealdAverages);

                // compute CC ici
                for (GenericIndicator gindicator : indicators) {
                    Indicator indicator = (Indicator) gindicator;
                    // write crop sheet
                    indicator.computeEffectiveCC(
                            writer,
                            domain,
                            growingSystemContext,
                            anonymizePlot);

                    indicator.resetEffectiveCC();
                }
                afterAllIndicatorsAreWritten(writer, performanceId, domain);

                for (GenericIndicator gindicator : indicators) {
                    Indicator indicator = (Indicator) gindicator;
                    // write plot sheet
                    indicator.computeEffective(
                            writer,
                            effectiveDomainContext,
                            optionalGrowingSystem,
                            plotContext);
                }
                afterAllIndicatorsAreWritten(writer, performanceId, domain);

                // reset zones
                for (GenericIndicator gindicator : indicators) {
                    Indicator indicator = (Indicator) gindicator;
                    indicator.resetEffectiveZones();
                }
            }

            // et si on a généré pour toutes les parcelles du GS, on a en plus réalisé une mise à l'echelle
            // (n'a de sens que pour toutes les données)
            for (GenericIndicator gindicator : indicators) {
                Indicator indicator = (Indicator) gindicator;
                // write SDC sheet
                indicator.computeEffective(
                        writer,
                        effectiveDomainContext,
                        growingSystemContext);
            }
            afterAllIndicatorsAreWritten(writer, performanceId, domain);

            // reset
            for (GenericIndicator gindicator : indicators) {
                Indicator indicator = (Indicator) gindicator;
                indicator.resetEffectivePlots();
            }
        }

        growingSystemsContexts.clear();

        // et si on a généré tous ceux du domaine, on réalisé en plus une mise à l'echelle
        // (n'a de sens que pour toutes les données)
        for (GenericIndicator gindicator : indicators) {
            Indicator indicator = (Indicator) gindicator;
            // write domain sheet
            indicator.computeEffective(writer, domain);
        }
        afterAllIndicatorsAreWritten(writer, performanceId, domain);

        // reset
        for (GenericIndicator gindicator : indicators) {
            Indicator indicator = (Indicator) gindicator;
            indicator.resetEffectiveGrowingSystems();
        }

        getTransaction().commit();

        if (LOGGER.isInfoEnabled()) {
            long fin = System.currentTimeMillis();
            LOGGER.info("Pour domaine: " + domain.getName() + " (" + domain.getCampaign() + "), " +
                    "FIN du calcul des indicateurs " +
                    "exécuté en: " + ((fin - start) / 1000) + "ms," + " ID : '" + domain.getTopiaId() + "'");
        }
    }

    private Collection<HarvestingActionValorisation> getDomainHarvestingActionValorisations(PerformanceEffectiveDomainExecutionContext domainContext) {
        Collection<HarvestingActionValorisation> allDomainValorisations = new ArrayList<>();

        domainContext.getPlotZoneContext().values().stream()
                .flatMap(Collection::stream)
                .flatMap(pzc -> pzc.getPerformanceCropContextExecutionContexts().stream())
                .flatMap(pcc -> pcc.getInterventionExecutionContexts().stream())
                .forEach(interventionContext -> allDomainValorisations.addAll(
                        addInterventionValorisationsToDomainValorisations(
                                interventionContext.getOptionalHarvestingAction(),
                                interventionContext.getInterventionId(),
                                domainContext.getDomain().getTopiaId(),
                                null
                        )
                ));

        return allDomainValorisations;
    }

    protected void writePerformanceToDb(PerformanceDbTask task, Collection<IndicatorFilter> indicatorFilters, List<GenericIndicator> indicators) {

        final Set<String> scenarioCodes = task.getScenarioCodes();
        final DbCommonReferentiels dbCommonReferentiels = task.getDbCommonReferentiels();
        final DbPerformanceDomainContext dbPerformanceDomainContext = task.getDbPerformanceDomainContext();

        Exception e = null;
        try {
            writePracticedPerformanceToDb(
                    task.getPerformanceId(),
                    task.getPerformanceDate(),
                    scenarioCodes,
                    indicatorFilters,
                    indicators,
                    dbCommonReferentiels,
                    dbPerformanceDomainContext);
        } catch (Exception ex) {
            e = ex;
            LOGGER.error("Failed to execute Practiced Performance with ID: '" + task.getPerformanceId() + "'", ex);
        }
        try {
            writeEffectivePerformanceToDb(
                    task.getPerformanceId(),
                    task.getPerformanceDate(),
                    scenarioCodes,
                    indicatorFilters,
                    indicators,
                    dbCommonReferentiels,
                    dbPerformanceDomainContext);
        } catch (Exception ex) {
            e = ex;
            LOGGER.error("Failed to execute Effective Performance with ID: '" + task.getPerformanceId() + "'", ex);
        }
        if (e != null) {
            throw new AgrosystTechnicalException("Failed to execute Performance", e);
        }
    }

    protected void writePracticedPerformanceToDb(String performanceId,
                                                 OffsetDateTime performanceDate,
                                                 Set<String> scenarioCodes,
                                                 Collection<IndicatorFilter> indicatorFilters,
                                                 List<GenericIndicator> indicators,
                                                 DbCommonReferentiels dbCommonReferentiels,
                                                 DbPerformanceDomainContext dbPerformanceDomainContext) {

        final IndicatorWriter writer = new SqlWriter(performanceDao, performanceId, performanceDate);

        final GenericIndicator[] allIndicator = new GenericIndicator[indicators.size()];
        writer.addComputedIndicators(indicators.toArray(allIndicator));

        final RefSolTextureGeppa defaultRefSolTextureGeppa = dbCommonReferentiels.defaultRefSolTextureGeppa();
        final RefSolProfondeurIndigo defaultSolDepth = dbCommonReferentiels.defaultSolDepth();
        final RefLocation defaultLocation = dbCommonReferentiels.defaultLocation();
        final PriceConverterKeysToRate priceConverterKeysToRate = dbCommonReferentiels.priceConverterKeysToRate();

        PerformancePracticedExecutionContextBuilder performanceExecutionBuilder =
                getPerformancePracticedExecutionContextBuilder(
                        true, priceConverterKeysToRate, defaultRefSolTextureGeppa, scenarioCodes, defaultLocation, defaultSolDepth, indicatorFilters);

        PerformanceGlobalExecutionContext globalExecutionContext = performanceExecutionBuilder.getPerformanceGlobalExecutionContext();

        // pour un domaine, on récupére l'ensemble des systèmes de cultures
        // visibles par un utilisateur (sécuritycontext) et pour le domaine courant
        final Domain domain = domainDao.forTopiaIdEquals(dbPerformanceDomainContext.domainId()).findUnique();
        final List<GrowingSystem> allGrowingSystems = growingSystemDao.forTopiaIdIn(dbPerformanceDomainContext.growingSystemIds()).findAll();

        long start = System.currentTimeMillis();
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Pour domaine: " + domain.getName() + " (" + domain.getCampaign() + ") " + " code : '" + domain.getCode() +
                    "', DEBUT du calcul des indicateurs ");
        }

        PerformancePracticedDomainExecutionContext domainContext = performanceExecutionBuilder.buildPerformanceDomainExecutionContext(
                Pair.of(domain, domain),
                allGrowingSystems);

        Set<PerformanceGrowingSystemExecutionContext> performanceGrowingSystemExecutionContexts = domainContext.getPerformanceGrowingSystemExecutionContexts();

        long elapsedD = System.currentTimeMillis() - start;
        if (elapsedD > getConfig().getPerformanceSlowContextThreshold()) {
            long gsCount = allGrowingSystems.size();
            long practicedSystemCount = performanceGrowingSystemExecutionContexts.size();
            LOGGER.warn(" FIN de construction des contextes pour le domaine: " + domain.getName() + " (" + domain.getCampaign() + ")" +
                    " nb Système de culture " + gsCount +
                    " nb synthétisé " + practicedSystemCount +
                    " Réalisé en :" + elapsedD + "ms");
        }

        for (PerformanceGrowingSystemExecutionContext performanceGrowingSystemExecutionContext : performanceGrowingSystemExecutionContexts) {

            Optional<GrowingSystem> optionalGrowingSystem = performanceGrowingSystemExecutionContext.getGrowingSystem();
            String growingSystemName =
                    optionalGrowingSystem.isPresent() ?
                            optionalGrowingSystem.get().getName()
                            : " aucun ";

            for (GenericIndicator gindicator : indicators) {
                Indicator indicator = (Indicator) gindicator;
                long startI = System.currentTimeMillis();
                if (LOGGER.isDebugEnabled()) {

                    LOGGER.debug("Pour domaine: " + domain.getName() + " (" + domain.getCampaign() + ")," +
                            " dispositif :" + growingSystemName +
                            " DEBUT calcul de l'indicateur " + indicator.getClass().getSimpleName());
                }

                try {
                    // Interventions scales, Crop scales = (Crop Cycles (CC)), PraticedSystem scales
                    indicator.computePracticed(
                            writer,
                            globalExecutionContext,
                            performanceGrowingSystemExecutionContext,
                            domainContext);

                } catch (Exception e) {
                    if (LOGGER.isErrorEnabled()) {
                        String growingSystemId =
                                optionalGrowingSystem.isPresent() ?
                                        optionalGrowingSystem.get().getTopiaId()
                                        : " aucun ";

                        String message = String.format(
                                """
                                        Exception sur le calcul des performance du synthétisé, domain %s ('%s'), growingSystemId '%s', indicateur %s, avec l'erreur suivante:
                                        %s
                                        %s""",
                                domain.getName(),
                                domain.getTopiaId(),
                                growingSystemId,
                                indicator.getClass().getSimpleName(),
                                e.getMessage(),
                                e.getCause());
                        LOGGER.error(message, e);
                    }
                    saveState(performanceId, PerformanceState.FAILED);
                }

                long elapsed = System.currentTimeMillis() - startI;
                if (elapsed > getConfig().getPerformanceSlowIndicatorThreshold()) {
                    LOGGER.warn(" FIN du calcul de l'indicateur :" + indicator.getClass().getSimpleName() + ", pour domaine: " + domain.getName() + " (" + domain.getCampaign() + ")," +
                            " dispositif :" + growingSystemName +
                            " Réalisé en :" + ((System.currentTimeMillis() - startI)) + "ms");
                }

                if (LOGGER.isDebugEnabled()) {
                    if (elapsed <= getConfig().getPerformanceSlowIndicatorThreshold()) {
                        LOGGER.debug("-> Pour domaine: " + domain.getName() + " (" + domain.getCampaign() + ")," +
                                " dispositif :" + growingSystemName +
                                " FIN du calcul de l'indicateur " + indicator.getClass().getSimpleName() +
                                " Réalisé en :" + ((System.currentTimeMillis() - startI)) + "ms");
                    }
                }
            }
            afterAllIndicatorsAreWritten(writer, performanceId, domain);

        }

        // reset
        for (GenericIndicator gindicator : indicators) {
            Indicator indicator = (Indicator) gindicator;
            indicator.resetPracticed(domain);
        }

        getTransaction().commit();

        if (LOGGER.isInfoEnabled()) {
            long fin = System.currentTimeMillis();
            LOGGER.info("Pour domaine: " + domain.getName() + " (" + domain.getCampaign() + "), " +
                    "FIN du calcul des indicateurs exécuté en: " + ((fin - start) / 1000) + "ms," + " ID : '" + domain.getTopiaId() + "'");
        }
    }

    @Override
    protected void computeEffectivePerformance(CurrentPerformanceDto performance,
                                               IndicatorWriter writer,
                                               Collection<Domain> domains,
                                               Set<String> scenarioCodes,
                                               RefSolTextureGeppa defaultRefSolTextureGeppa,
                                               RefSolProfondeurIndigo defaultSolDepth,
                                               RefLocation defaultLocation,
                                               PriceConverterKeysToRate priceConverterKeysToRate,
                                               SecurityContext securityContext,
                                               Map<Domain, Domain> domainToAnonymizeDomaines,
                                               Collection<IndicatorFilter> indicatorFilters,
                                               GenericIndicator[] indicators) {
        // pour un domaine, on récupére l'ensemble des systèmes de cultures
        // visibles par un utilisateur (sécuritycontext) et pour le domaine courant
        final List<GrowingSystem> allGrowingSystems = getGrowingSystemsForDomainIds(domains, securityContext);

        // on détermine ensuite si l'utilisateur a demandé spécifiquement la génération
        // des données pour certains GS en particulier
        final Collection<GrowingSystem> askedGrowingSystems = CollectionUtils.retainAll(
                allGrowingSystems,
                CollectionUtils.emptyIfNull(performance.getGrowingSystems()));

        // on génére soit les GS demandés, soit tous ceux du domaine
        final Collection<GrowingSystem> allComputedGrowingSystems = CollectionUtils.isNotEmpty(askedGrowingSystems) ?
                askedGrowingSystems :
                allGrowingSystems;

        if (allComputedGrowingSystems.isEmpty()) {
            return;
        }

        i18nService.translateGrowingSystems(allComputedGrowingSystems);

        int domainIndex = 0;
        int domainSize = domains.size();

        HashMap<Domain, List<GrowingSystem>> growingSystemsByDomain = getDomainGrowingSystems(allComputedGrowingSystems);

        boolean isFilteredPlots = CollectionUtils.isNotEmpty(performance.getPlots());
        boolean isFilteredZones = CollectionUtils.isNotEmpty(performance.getZones());

        Iterator<Map.Entry<Domain, Domain>> domainToAnonymizeDomaineIterator = domainToAnonymizeDomaines.entrySet().iterator();

        while (domainToAnonymizeDomaineIterator.hasNext()) {
            long start = System.currentTimeMillis();

            Map.Entry<Domain, Domain> domainToAnonymizeDomaine = domainToAnonymizeDomaineIterator.next();
            domainToAnonymizeDomaineIterator.remove();

            Domain domain = domainToAnonymizeDomaine.getKey();
            Domain anonymiseDomain = domainToAnonymizeDomaine.getValue();


            if (LOGGER.isInfoEnabled()) {
                domainIndex++;
                LOGGER.info(String.format("Generate performance of domain %d of %d, %s (%d) id:'%s' " +
                                "calcul start",
                        domainIndex,
                        domainSize,
                        domain.getName(),
                        domain.getCampaign(),
                        domain.getTopiaId()));
            }

            final List<GrowingSystem> domainGrowingSystems = growingSystemsByDomain.remove(domain);

            if (CollectionUtils.isEmpty(domainGrowingSystems)) {
                continue;// nothing to do
            }

            // get plots
            Set<String> domainGrowingSystemIds = domainGrowingSystems.stream()
                    .filter(askedGrowingSystems::contains)
                    .map(GrowingSystem::getTopiaId)
                    .collect(Collectors.toSet());

            final List<Plot> allPlots = plotTopiaDao.findAllActiveGrantedByDomainId(securityContext, domain.getTopiaId(), domainGrowingSystemIds);

            // on détermine ensuite si l'utilisateur a demandé spécifiquement la génération
            // des données pour certaines parcelles en particulier
            // on génère soit les parcelles demandées, soit toutes celles du GS
            Collection<Plot> computedPlots;
            if (isFilteredPlots) {
                computedPlots = CollectionUtils.retainAll(allPlots, performance.getPlots());
            } else {
                computedPlots = allPlots;
            }

            if (computedPlots.isEmpty()) {
                continue;// nothing to do
            }

            // on récupere les zones de la parcelle (sans sécurité)
            List<Zone> allZones = zoneDao.forPlotIn(computedPlots)
                    .addEquals(Zone.PROPERTY_ACTIVE, true).findAll();

            // on génére soit les zones demandées, soit toutes celles du GS
            Collection<Zone> computedZones;
            if (isFilteredZones) {
                // on détermine ensuite si l'utilisateur a demande specifiquement la génération
                // des données pour certaines zones en particulier
                computedZones = CollectionUtils.retainAll(allZones, performance.getZones());
            } else {
                computedZones = allZones;
            }

            if (computedZones.isEmpty()) {
                continue; // Nothing to do
            }

            Pair<Domain, Domain> domainAndAnonymizeDomain = Pair.of(domain, anonymiseDomain);

            Collection<String> codeAmmBioControle = referentialService.getCodeAmmBioControle(domain.getCampaign());
            Map<String, String> groupesCiblesParCode = referentialService.getGroupesCiblesParCode();

            PerformanceEffectiveExecutionContextBuilder performanceExecutionBuilder =
                    new PerformanceEffectiveExecutionContextBuilder(
                            anonymizeService,
                            domainInputStockUnitService,
                            domainService,
                            this,
                            inputPriceService,
                            pricesService,
                            refInputPriceService,
                            referentialService,
                            codeAmmBioControle,
                            domainAndAnonymizeDomain,
                            defaultRefSolTextureGeppa,
                            false,
                            priceConverterKeysToRate,
                            scenarioCodes,
                            defaultLocation,
                            defaultSolDepth,
                            groupesCiblesParCode,
                            computedPlots,
                            indicatorFilters
                    );

            PerformanceGlobalExecutionContext globalExecutionContext = performanceExecutionBuilder.getPerformanceGlobalExecutionContext();

            performanceExecutionBuilder.buildPerformanceExecutionContext(computedZones);
            PerformanceEffectiveDomainExecutionContext domainContext = performanceExecutionBuilder.getDomainExecutionContext();
            Map<BasicPlot, Set<PerformanceZoneExecutionContext>> plotZoneContexts = domainContext.getPlotZoneContext();

            Collection<PerformanceGrowingSystemExecutionContext> growingSystemsContexts = domainContext.getGrowingSystemContextByGrowingSystems().values();

            Collection<HarvestingActionValorisation> allDomainValorisations = getDomainHarvestingActionValorisations(domainContext);
            List<Equipment> equipments = domainService.getEquipments(domain.getTopiaId());
            writeEffectivePrices(performance.getTopiaId(), writer, domainContext, allDomainValorisations, equipments);

            String performanceId = performance.getTopiaId();

            long elapsedD = System.currentTimeMillis() - start;
            if (elapsedD > getConfig().getPerformanceSlowContextThreshold()) {
                long gsCount = growingSystemsContexts.stream().map(PerformanceGrowingSystemExecutionContext::getGrowingSystem).filter(Optional::isPresent).count();
                long zoneCount = plotZoneContexts.size();
                LOGGER.warn(" FIN de construction des contextes pour le domaine: " + domain.getName() + " (" + domain.getCampaign() + ")" +
                        " nb Système de culture " + gsCount +
                        " nb zone " + zoneCount +
                        " Réalisé en :" + elapsedD + "ms");
            }

            for (PerformanceGrowingSystemExecutionContext growingSystemContext : growingSystemsContexts) {

                Pair<Optional<GrowingSystem>, Optional<GrowingSystem>> growingSystemAndAnonymizeGrowingSystem =
                        growingSystemContext.getGrowingSystemAndAnonymizeGrowingSystem();

                Optional<GrowingSystem> optionalAnoGrowingSystem = growingSystemAndAnonymizeGrowingSystem.getRight();

                Set<PerformancePlotExecutionContext> plotContexts = growingSystemContext.getPlotContexts();
                for (PerformancePlotExecutionContext plotContext : plotContexts) {

                    Plot anonymizePlot = plotContext.getAnonymizePlot();
                    Set<PerformanceZoneExecutionContext> zoneContexts = plotZoneContexts.get(anonymizePlot);

                    Map<Pair<RefDestination, YealdUnit>, List<Double>> plotYealds = new HashMap<>();

                    for (PerformanceZoneExecutionContext zoneContext : zoneContexts) {
                        Zone anonZone = zoneContext.getAnonymizeZone();

                        Map<Pair<RefDestination, YealdUnit>, Double> zoneYealAverage = computeYealdAverageByDestinationAndUnit(zoneContext.getZoneYealds());
                        zoneContext.setZoneAverageYeald(zoneYealAverage);
                        addZoneYealdAverage(plotYealds, zoneYealAverage);

                        for (GenericIndicator gindicator : indicators) {
                            try {
                                Indicator indicator = (Indicator) gindicator;
                                long startI = System.currentTimeMillis();
                                if (LOGGER.isDebugEnabled()) {
                                    LOGGER.debug("Pour domaine: " + domain.getName() + " (" + domain.getCampaign() + "), " +
                                            "zone :" + anonZone.getName() +
                                            " calcul de l'indicateur " + indicator.getClass().getSimpleName() + " pour le réalisé ");
                                }

                                // its, irs, Espèces, Variétés, Type de conduite du SdC, Rendement (Agrégation par destination)
                                // write intervention sheet, zone sheet
                                indicator.computeEffective(
                                        writer,
                                        globalExecutionContext,
                                        domainContext,
                                        zoneContext);

                                long elapsed = System.currentTimeMillis() - startI;
                                if (elapsed > getConfig().getPerformanceSlowIndicatorThreshold()) {
                                    LOGGER.warn(" FIN du calcul de l'indicateur :" + indicator.getClass().getSimpleName() + ", pour domaine: " + domain.getName() + " (" + domain.getCampaign() + "), zone :" + anonZone.getName() +
                                            " Réalisé en :" + (System.currentTimeMillis() - startI) + "ms");
                                }

                                if (LOGGER.isDebugEnabled()) {
                                    if (elapsed <= getConfig().getPerformanceSlowIndicatorThreshold()) {
                                        LOGGER.debug("-> Pour domaine: " + domain.getName() + " (" + domain.getCampaign() + "), zone :" + anonZone.getName() +
                                                " FIN du calcul de l'indicateur " + indicator.getClass().getSimpleName() +
                                                " Réalisé en :" + (System.currentTimeMillis() - startI) + "ms");
                                    }
                                }
                                afterAllIndicatorsAreWritten(writer, performanceId, domain);
                            } catch (Exception e) {
                                String message = String.format(
                                        """
                                                Exception sur le calcul des performance du réalisé, domain %s ('%s'), zoneId '%s', indicateur %s, avec l'erreur suivante:
                                                %s
                                                %s""",
                                        domain.getName(),
                                        domain.getTopiaId(),
                                        anonZone.getTopiaId(),
                                        gindicator.getClass().getSimpleName(),
                                        e.getMessage(),
                                        e.getCause());
                                throw new AgrosystTechnicalException(message, e);
                            }
                        }

                    }

                    Map<Pair<RefDestination, YealdUnit>, Double> plotYealdAverages = computeYealdAverageByDestinationAndUnit(plotYealds);
                    plotContext.setPlotYealdAverages(plotYealdAverages);

                    // compute CC ici
                    for (GenericIndicator gindicator : indicators) {
                        Indicator indicator = (Indicator) gindicator;
                        // write crop sheet
                        indicator.computeEffectiveCC(
                                writer,
                                anonymiseDomain,
                                growingSystemContext,
                                anonymizePlot);

                        indicator.resetEffectiveCC();
                    }
                    afterAllIndicatorsAreWritten(writer, performanceId, domain);

                    // et si on a généré toutes celle de la parcelle, on réalise en plus une mise à l'échelle
                    // (n'a de sens que pour toutes les données)
                    if (!isFilteredZones) {
                        for (GenericIndicator gindicator : indicators) {
                            Indicator indicator = (Indicator) gindicator;
                            // write plot sheet
                            indicator.computeEffective(
                                    writer,
                                    domainContext,
                                    optionalAnoGrowingSystem,
                                    plotContext);
                        }
                        afterAllIndicatorsAreWritten(writer, performanceId, domain);
                    }

                    // reset zones
                    for (GenericIndicator gindicator : indicators) {
                        Indicator indicator = (Indicator) gindicator;
                        indicator.resetEffectiveZones();
                    }
                }

                // et si on a généré pour toutes les parcelles du GS, on réalise en plus une mise à l'échelle
                // (n'a de sens que pour toutes les données)
                if (!isFilteredPlots) {
                    for (GenericIndicator gindicator : indicators) {
                        Indicator indicator = (Indicator) gindicator;
                        // write SDC sheet
                        indicator.computeEffective(
                                writer,
                                domainContext,
                                growingSystemContext);
                    }
                    afterAllIndicatorsAreWritten(writer, performanceId, domain);
                }

                // reset
                for (GenericIndicator gindicator : indicators) {
                    Indicator indicator = (Indicator) gindicator;
                    indicator.resetEffectivePlots();
                }
            }

            growingSystemsContexts.clear();

            // et si on a généré tous ceux du domaine, on réalisé en plus une mise à l'echelle
            // (n'a de sens que pour toutes les données)
            if (CollectionUtils.isEmpty(askedGrowingSystems)) {
                for (GenericIndicator gindicator : indicators) {
                    Indicator indicator = (Indicator) gindicator;
                    // write domain sheet
                    indicator.computeEffective(writer, anonymiseDomain);
                }
                afterAllIndicatorsAreWritten(writer, performanceId, domain);
            }

            // reset
            for (GenericIndicator gindicator : indicators) {
                Indicator indicator = (Indicator) gindicator;
                indicator.resetEffectiveGrowingSystems();
            }

            if (LOGGER.isInfoEnabled()) {
                long fin = System.currentTimeMillis();
                LOGGER.info(String.format("Generate performance of domain %d of %d, %s (%d) id:'%s' calcul end in: %ds",
                        domainIndex,
                        domainSize,
                        domain.getName(),
                        domain.getCampaign(),
                        domain.getTopiaId(),
                        (fin - start) / 1000));
            }
        }
    }

    @Override
    protected void computePracticedPerformance(CurrentPerformanceDto performance,
                                               IndicatorWriter writer,
                                               Collection<Domain> domains,
                                               Set<String> scenarioCodes,
                                               RefSolTextureGeppa defaultRefSolTextureGeppa,
                                               RefSolProfondeurIndigo defaultSolDepth,
                                               RefLocation defaultLocation,
                                               PriceConverterKeysToRate priceConverterKeysToRate,
                                               SecurityContext securityContext,
                                               Map<Domain, Domain> domainToAnonymizeDomaines,
                                               Collection<IndicatorFilter> indicatorFilters,
                                               GenericIndicator[] indicators) {

        PerformancePracticedExecutionContextBuilder performanceExecutionBuilder = getPerformancePracticedExecutionContextBuilder(false, priceConverterKeysToRate, defaultRefSolTextureGeppa, scenarioCodes, defaultLocation, defaultSolDepth, indicatorFilters);

        PerformanceGlobalExecutionContext globalExecutionContext = performanceExecutionBuilder.getPerformanceGlobalExecutionContext();

        final String performanceId = performance.getTopiaId();

        // pour un domaine, on récupère l'ensemble des systèmes de cultures
        // visibles par un utilisateur (sécuritycontext) et pour le domaine courant
        final List<GrowingSystem> allGrowingSystems = getGrowingSystemsForDomainIds(domains, securityContext);

        // on détermine ensuite si l'utilisateur a demandé spécifiquement la génération
        // des données pour certains GS en particulier
        Collection<GrowingSystem> askedGrowingSystems = CollectionUtils.retainAll(
                allGrowingSystems,
                CollectionUtils.emptyIfNull(performance.getGrowingSystems()));

        // on génère soit les GS demandés, soit tous ceux du domaine
        Collection<GrowingSystem> allComputedGrowingSystems =
                CollectionUtils.isNotEmpty(askedGrowingSystems) ?
                        askedGrowingSystems :
                        allGrowingSystems;

        if (allComputedGrowingSystems.isEmpty()) {
            return;
        }

        i18nService.translateGrowingSystems(allComputedGrowingSystems);

        HashMap<Domain, List<GrowingSystem>> growingSystemsByDomain = getDomainGrowingSystems(allComputedGrowingSystems);

        int domainIndex = 0;
        int domainSize = domains.size();

        for (Map.Entry<Domain, Domain> domainToAnonymizeDomaine : domainToAnonymizeDomaines.entrySet()) {

            Domain domain = domainToAnonymizeDomaine.getKey();
            Domain anonymiseDomain = domainToAnonymizeDomaine.getValue();

            final List<GrowingSystem> domainGrowingSystems = growingSystemsByDomain.remove(domain);

            if (CollectionUtils.isEmpty(domainGrowingSystems)) {
                continue;// nothing to do
            }

            long start = System.currentTimeMillis();
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info("Pour domaine: " + domain.getName() + " (" + domain.getCampaign() + ") " +
                        " code : '" + domain.getCode() + "', DEBUT du calcul des indicateurs ");
            }

            PerformancePracticedDomainExecutionContext domainContext = performanceExecutionBuilder.buildPerformanceDomainExecutionContext(
                    Pair.of(domain, anonymiseDomain),
                    domainGrowingSystems);

            if (LOGGER.isInfoEnabled()) {
                domainIndex++;
                LOGGER.info(String.format("Generate performance of domain %d of %d", domainIndex, domainSize));
            }

            Set<PerformanceGrowingSystemExecutionContext> performanceGrowingSystemExecutionContexts =
                    domainContext.getPerformanceGrowingSystemExecutionContexts()
                            .stream()
                            .filter(
                                    c -> c.getAnonymizeGrowingSystem().isPresent())
                            .collect(Collectors.toSet());

            List<PerformancePracticedSystemExecutionContext> practicedSystemContexts =
                    performanceGrowingSystemExecutionContexts.stream()
                            .flatMap(pgsec -> pgsec.getPracticedSystemExecutionContexts().stream()).toList();

            Collection<RefHarvestingPrice> valuesScenariosHarvestingPrices = domainContext.getRefScenarioHarvestingPricesByValorisationKey().values();
            MultiKeyMap<String, List<RefHarvestingPrice>>  refHarvestingPricesByCodeEspeceAndCodeDestination = getRefHarvestingPricesByCodeEspeceAndCodeDestination(valuesScenariosHarvestingPrices);

            writePracticedPrice(writer, performanceId, domain, practicedSystemContexts, domainContext.getRefScenariosInputPricesByDomainInput(), refHarvestingPricesByCodeEspeceAndCodeDestination);

            long elapsedD = System.currentTimeMillis() - start;
            if (elapsedD > getConfig().getPerformanceSlowContextThreshold()) {
                long gsCount = allGrowingSystems.size();
                long practicedSystemCount = performanceGrowingSystemExecutionContexts.size();
                LOGGER.warn(" FIN de construction des contextes pour le domaine: " + domain.getName() + " (" + domain.getCampaign() + ")" +
                        " nb Système de culture " + gsCount +
                        " nb synthétisé " + practicedSystemCount +
                        " Réalisé en :" + elapsedD + "ms");
            }

            for (PerformanceGrowingSystemExecutionContext performanceGrowingSystemExecutionContext : performanceGrowingSystemExecutionContexts) {

                final Optional<GrowingSystem> anonymizeGrowingSystem = performanceGrowingSystemExecutionContext.getAnonymizeGrowingSystem();
                String anonymizeGrowingSystemName =
                        anonymizeGrowingSystem.isPresent() ?
                                anonymizeGrowingSystem.get().getName()
                                : "";

                for (GenericIndicator gindicator : indicators) {
                    try {


                        Indicator indicator = (Indicator) gindicator;
                        long startI = System.currentTimeMillis();
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("Pour domaine: " + domain.getName() + " (" + domain.getCampaign() + "), dispositif :" + anonymizeGrowingSystemName
                                    + " DEBUT calcul de l'indicateur " + indicator.getClass().getSimpleName());
                        }
                        // Interventions scales, Crop scales = (Crop Cycles (CC)), PracticedSystem scales
                        indicator.computePracticed(
                                writer,
                                globalExecutionContext,
                                performanceGrowingSystemExecutionContext,
                                domainContext);

                        long elapsed = System.currentTimeMillis() - startI;
                        if (elapsed > getConfig().getPerformanceSlowIndicatorThreshold()) {
                            LOGGER.warn(" FIN du calcul de l'indicateur :" + indicator.getClass().getSimpleName() + ", pour domaine: " + domain.getName() + " (" + domain.getCampaign() + ")," +
                                    " dispositif :" + anonymizeGrowingSystemName +
                                    " Réalisé en :" + ((System.currentTimeMillis() - startI)) + "ms");
                        }
                        if (LOGGER.isDebugEnabled()) {
                            if (elapsed <= getConfig().getPerformanceSlowIndicatorThreshold()) {
                                LOGGER.debug("-> Pour domaine: " + domain.getName() + " (" + domain.getCampaign() + ")," +
                                        " dispositif :" + anonymizeGrowingSystemName +
                                        " FIN du calcul de l'indicateur " + indicator.getClass().getSimpleName() +
                                        " Réalisé en :" + ((System.currentTimeMillis() - startI)) + "ms");
                            }
                        }
                    } catch (Exception e) {
                        String growingSystemId =
                                anonymizeGrowingSystem.isPresent() ?
                                        anonymizeGrowingSystem.get().getTopiaId()
                                        : " aucun ";

                        String message = String.format(
                                """
                                        Exception sur le calcul des performance du synthétisé, domain %s ('%s'), growingSystemId '%s', indicateur %s, avec l'erreur suivante:
                                        %s
                                        %s""",
                                domain.getName(),
                                domain.getTopiaId(),
                                growingSystemId,
                                gindicator.getClass().getSimpleName(),
                                e.getMessage(),
                                e.getCause());
                        throw new AgrosystTechnicalException(message, e);
                    }
                }
            }
            afterAllIndicatorsAreWritten(writer, performanceId, domain);

            // et si on a généré tous ceux du domaine, on réalisé en plus une mise à l'echelle
            // (n'a de sens que pour toutes les données)
            if (CollectionUtils.isEmpty(askedGrowingSystems)) {
                for (GenericIndicator gindicator : indicators) {
                    Indicator indicator = (Indicator) gindicator;
                    // domain scale
                    indicator.computePracticed(writer, anonymiseDomain);
                }
            }
            afterAllIndicatorsAreWritten(writer, performanceId, domain);

            // reset
            for (GenericIndicator gindicator : indicators) {
                Indicator indicator = (Indicator) gindicator;
                indicator.resetPracticed(anonymiseDomain);
            }

            if (LOGGER.isInfoEnabled()) {
                long fin = System.currentTimeMillis();
                LOGGER.info("Pour domaine: " + domain.getName() + " (" + domain.getCampaign() + "), " +
                        "FIN du calcul des indicateurs " +
                        "exécuté en: " + ((fin - start) / 1000) + "s, ID : '" + domain.getTopiaId() + "'");
            }
        }
    }

    @Override
    public PriceAndUnit loadHarvestingPriceWithUnitRefPriceForValorisation(Optional<String> optionalPracticedSystemCampaigns,
                                                                           Optional<PriceUnit> priceUnit,
                                                                           String interventionId,
                                                                           HarvestingActionValorisation valorisation,
                                                                           Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {
        PriceAndUnit refPrice = null;
        try {
            refPrice = pricesService.getAverageRefHarvestingPriceForValorisation(
                    valorisation,
                    optionalPracticedSystemCampaigns,
                    priceUnit,
                    codeEspBotCodeQualiBySpeciesCode
            );

            if (refPrice == null && LOGGER.isWarnEnabled()) {
                LOGGER.warn(
                        String.format("ÉCHEC DU CALCUL DU PRIX DE RÉFÉRENCE POUR LA VALORISATION '%s' ET L'INTERVENTION %s",
                                valorisation.getTopiaId(),
                                interventionId));

            }

        } catch (Exception e) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn(
                        String.format("ÉCHEC DU CALCUL DU PRIX DE RÉFÉRENCE POUR LA VALORISATION '%s' ET L'INTERVENTION %s",
                                valorisation.getTopiaId(),
                                interventionId)
                );
                LOGGER.warn(e);
            }
        }

        return refPrice;
    }

    @Override
    public void cancelPerformance(String performanceId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Performance with id %s is about to be cancel", performanceId));
        }
        try {
            this.getPersistenceContext().close();
        } catch (IllegalStateException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Persistence context is closed. Nothing to do");
            }
        }

    }

    protected MultiKeyMap<String, List<RefHarvestingPrice>> getRefHarvestingPricesByCodeEspeceAndCodeDestination(Collection<RefHarvestingPrice> valuesScenariosHarvestingPrices) {
        Map<String, List<RefHarvestingPrice>> refHarvestingPriceByCodeEspece = new HashMap<>();
        MultiKeyMap<String, List<RefHarvestingPrice>> refHarvestingPricesByCodeEspeceAndCodeDestination = new MultiKeyMap<>();

        valuesScenariosHarvestingPrices.forEach(refHarvestingPrice -> {
            final String codeEspece = refHarvestingPrice.getCode_espece_botanique();
            if (!refHarvestingPriceByCodeEspece.containsKey(codeEspece)) {
                refHarvestingPriceByCodeEspece.put(codeEspece, new ArrayList<>());
            }

            refHarvestingPriceByCodeEspece.get(codeEspece).add(refHarvestingPrice);
        });

        for (String codeEspece : refHarvestingPriceByCodeEspece.keySet()) {
            for (RefHarvestingPrice refHarvestingPrice : refHarvestingPriceByCodeEspece.get(codeEspece)) {
                final String codeDestination = refHarvestingPrice.getCode_destination_A();

                List<RefHarvestingPrice> refHarvestingPricesByCodeDestination = refHarvestingPricesByCodeEspeceAndCodeDestination.get(codeEspece, codeDestination);
                if (refHarvestingPricesByCodeDestination == null) {
                    refHarvestingPricesByCodeDestination = new ArrayList<>();
                    refHarvestingPricesByCodeEspeceAndCodeDestination.put(codeEspece, codeDestination, refHarvestingPricesByCodeDestination);
                }

                refHarvestingPricesByCodeDestination.add(refHarvestingPrice);
            }
        }

        return refHarvestingPricesByCodeEspeceAndCodeDestination;
    }

    protected Collection<HarvestingActionValorisation> getPracticedSystemHarvestingActionValorisations(
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext) {

        Collection<HarvestingActionValorisation> allPracticedSystemValorisations = new ArrayList<>();

        practicedSystemExecutionContext.getPerformancePracticedCropContextExecutionContexts().stream()
                .flatMap(ppcc -> ppcc.getInterventionExecutionContexts().stream())
                .forEach(interventionContext -> allPracticedSystemValorisations.addAll(
                        addInterventionValorisationsToDomainValorisations(
                                interventionContext.getOptionalHarvestingAction(),
                                interventionContext.getInterventionId(),
                                null,
                                practicedSystemExecutionContext.getPracticedSystem().getTopiaId()
                        )
                ));

        return allPracticedSystemValorisations;
    }

}
