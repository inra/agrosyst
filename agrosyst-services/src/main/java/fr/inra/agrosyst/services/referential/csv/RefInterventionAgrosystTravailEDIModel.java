package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDIImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * @author David Cossé
 */
public class RefInterventionAgrosystTravailEDIModel  extends AbstractAgrosystModel<RefInterventionAgrosystTravailEDI> implements ExportModel<RefInterventionAgrosystTravailEDI> {

    public RefInterventionAgrosystTravailEDIModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Intervention agrosyst", RefInterventionAgrosystTravailEDI.PROPERTY_INTERVENTION_AGROSYST, AGROSYST_INTERVENTION_TYPE_PARSER);
        newMandatoryColumn("reference_code", RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_CODE);
        newMandatoryColumn("reference_label", RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_LABEL);
        newMandatoryColumn("source", RefInterventionAgrosystTravailEDI.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefInterventionAgrosystTravailEDI.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefInterventionAgrosystTravailEDI, Object>> getColumnsForExport() {

        ModelBuilder<RefInterventionAgrosystTravailEDI> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Intervention agrosyst", RefInterventionAgrosystTravailEDI.PROPERTY_INTERVENTION_AGROSYST, AGROSYST_INTERVENTION_TYPE_FORMATTER);
        modelBuilder.newColumnForExport("reference_code", RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_CODE);
        modelBuilder.newColumnForExport("reference_label", RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_LABEL);
        modelBuilder.newColumnForExport("source", RefInterventionAgrosystTravailEDI.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefInterventionAgrosystTravailEDI.PROPERTY_ACTIVE, T_F_FORMATTER);
        Iterable result = modelBuilder.getColumnsForExport();
        return result;
    }

    @Override
    public RefInterventionAgrosystTravailEDI newEmptyInstance() {
        RefInterventionAgrosystTravailEDI refInterventionAgrosystTravailEDI = new RefInterventionAgrosystTravailEDIImpl();
        refInterventionAgrosystTravailEDI.setActive(true);
        return refInterventionAgrosystTravailEDI;
    }

}
