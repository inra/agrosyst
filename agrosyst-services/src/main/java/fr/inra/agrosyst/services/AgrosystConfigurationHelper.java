package fr.inra.agrosyst.services;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2015 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.inra.agrosyst.api.PostgreSQL10CustomDialect;
import fr.inra.agrosyst.api.entities.AgrosystTopiaApplicationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.HibernateAvailableSettings;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;

import java.util.Properties;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class AgrosystConfigurationHelper {

    private static final Log LOGGER = LogFactory.getLog(AgrosystConfigurationHelper.class);

    public static AgrosystTopiaApplicationContext newApplicationContext(AgrosystServiceConfig config, TopiaConfiguration topiaConfiguration) {
        return new AgrosystTopiaApplicationContext(config, topiaConfiguration);
    }

    public static TopiaConfiguration getPersistanceConfiguration(Properties properties){
        BeanTopiaConfiguration topiaConfiguration = null;

        try {
            TopiaConfigurationBuilder topiaConfigurationBuilder = new TopiaConfigurationBuilder();

            topiaConfiguration = topiaConfigurationBuilder.readProperties(properties);

            if (LOGGER.isInfoEnabled()) {
                LOGGER.info(topiaConfiguration);
            }

            topiaConfiguration.setHibernateExtraConfiguration(newHibernateExtraConfigurationForPostgresql());

        } catch (Exception e) {
            LOGGER.error(e);
        }

        return topiaConfiguration;
    }

    static ImmutableMap<String, String> newHibernateExtraConfigurationForPostgresql() {
        return ImmutableMap.of(HibernateAvailableSettings.DIALECT, PostgreSQL10CustomDialect.class.getName());
    }


}
