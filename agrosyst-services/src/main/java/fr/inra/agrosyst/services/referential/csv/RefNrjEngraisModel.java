package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefNrjEngrais;
import fr.inra.agrosyst.api.entities.referential.RefNrjEngraisImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Type d'engrais, NRJ (MJ/kg N), NRJ (MJ/kg P205), NRJ (MJ/kg K2O), Source
 * 
 * @author Eric Chatellier
 */
public class RefNrjEngraisModel extends AbstractAgrosystModel<RefNrjEngrais> implements ExportModel<RefNrjEngrais> {

    public RefNrjEngraisModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("Type d'engrais", RefNrjEngrais.PROPERTY_TYPE_ENGRAIS);
        newMandatoryColumn("NRJ (MJ/kg N)", RefNrjEngrais.PROPERTY_NRJ_N, DOUBLE_PARSER);
        newMandatoryColumn("NRJ (MJ/kg P205)", RefNrjEngrais.PROPERTY_NRJ_P2O5, DOUBLE_PARSER);
        newMandatoryColumn("NRJ (MJ/kg K2O)", RefNrjEngrais.PROPERTY_NRJ_K2O, DOUBLE_PARSER);
        newMandatoryColumn("Source", RefNrjEngrais.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefNrjEngrais.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefNrjEngrais, Object>> getColumnsForExport() {
        ModelBuilder<RefNrjEngrais> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Type d'engrais", RefNrjEngrais.PROPERTY_TYPE_ENGRAIS);
        modelBuilder.newColumnForExport("NRJ (MJ/kg N)", RefNrjEngrais.PROPERTY_NRJ_N, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("NRJ (MJ/kg P205)", RefNrjEngrais.PROPERTY_NRJ_P2O5, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("NRJ (MJ/kg K2O)", RefNrjEngrais.PROPERTY_NRJ_K2O, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefNrjEngrais.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefNrjEngrais.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefNrjEngrais newEmptyInstance() {
        RefNrjEngrais refNrjEngrais = new RefNrjEngraisImpl();
        refNrjEngrais.setActive(true);
        return refNrjEngrais;
    }
}
