package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.PeriodeSemis;
import fr.inra.agrosyst.api.entities.referential.RefCouvSolAnnuelle;
import fr.inra.agrosyst.api.entities.referential.RefCouvSolAnnuelleImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

/**
 * Vitesse couv Période Semis   Début Inter Fin Inter   Couv
 * 
 * @author Eric Chatellier
 */
public class RefCouvSolAnnuelleModel extends AbstractAgrosystModel<RefCouvSolAnnuelle> implements ExportModel<RefCouvSolAnnuelle> {

    protected static final ValueParser<PeriodeSemis> PERIODE_SEMIS_PARSER = value -> {
        PeriodeSemis result = PeriodeSemis.valueOf(value.replace('-', '_'));
        return result;
    };

    protected static final ValueFormatter<PeriodeSemis> PERIODE_SEMIS_FORMATTER = Enum::name;

    public RefCouvSolAnnuelleModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Vitesse couv", RefCouvSolAnnuelle.PROPERTY_VITESSE_COUV, VITESSE_COUV_PARSER);
        newMandatoryColumn("Période Semis", RefCouvSolAnnuelle.PROPERTY_PERIODE_SEMIS, PERIODE_SEMIS_PARSER);
        newMandatoryColumn("Début Inter", RefCouvSolAnnuelle.PROPERTY_DEBUT_INTER);
        newMandatoryColumn("Fin Inter", RefCouvSolAnnuelle.PROPERTY_FIN_INTER);
        newMandatoryColumn("Couv", RefCouvSolAnnuelle.PROPERTY_COUV, DOUBLE_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefCouvSolAnnuelle.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefCouvSolAnnuelle, Object>> getColumnsForExport() {
        ModelBuilder<RefCouvSolAnnuelle> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Vitesse couv", RefCouvSolAnnuelle.PROPERTY_VITESSE_COUV, VITESSE_COUV_FORMATTER);
        modelBuilder.newColumnForExport("Période Semis", RefCouvSolAnnuelle.PROPERTY_PERIODE_SEMIS, PERIODE_SEMIS_FORMATTER);
        modelBuilder.newColumnForExport("Début Inter", RefCouvSolAnnuelle.PROPERTY_DEBUT_INTER);
        modelBuilder.newColumnForExport("Fin Inter", RefCouvSolAnnuelle.PROPERTY_FIN_INTER);
        modelBuilder.newColumnForExport("Couv", RefCouvSolAnnuelle.PROPERTY_COUV, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefCouvSolAnnuelle.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefCouvSolAnnuelle newEmptyInstance() {
        RefCouvSolAnnuelle result = new RefCouvSolAnnuelleImpl();
        result.setActive(true);
        return result;
    }
}
