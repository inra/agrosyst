package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverterImpl;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspece;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueParser;

public class RefInputUnitPriceUnitConverterModel extends AbstractDestinationAndPriceModel<RefInputUnitPriceUnitConverter> implements ExportModel<RefInputUnitPriceUnitConverter> {

    public RefInputUnitPriceUnitConverterModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("PhytoProductUnit", RefInputUnitPriceUnitConverter.PROPERTY_PHYTO_PRODUCT_UNIT, PHYTO_PRODUCT_UNIT_PARSER);
        newMandatoryColumn("CapacityUnit", RefInputUnitPriceUnitConverter.PROPERTY_CAPACITY_UNIT, CAPACITY_UNIT_PARSER);
        newMandatoryColumn("MineralProductUnit", RefInputUnitPriceUnitConverter.PROPERTY_MINERAL_PRODUCT_UNIT, MINERAL_PRODUCT_UNIT_PARSER);
        newMandatoryColumn("OrganicProductUnit", RefInputUnitPriceUnitConverter.PROPERTY_ORGANIC_PRODUCT_UNIT, ORGANIC_PRODUCT_UNIT_PARSER);
        newMandatoryColumn("SeedPlantUnit", RefInputUnitPriceUnitConverter.PROPERTY_SEED_PLANT_UNIT, SEED_PLANT_UNIT_VALUE_UNIT_PARSER);
        newMandatoryColumn("PotInputUnit", RefInputUnitPriceUnitConverter.PROPERTY_POT_INPUT_UNIT, POT_INPUT_UNIT_PARSER);
        newMandatoryColumn("SubstrateInputUnit", RefInputUnitPriceUnitConverter.PROPERTY_SUBSTRATE_INPUT_UNIT, SUBSTRATE_INPUT_UNIT_PARSER);
        newMandatoryColumn("PriceUnit", RefInputUnitPriceUnitConverter.PROPERTY_PRICE_UNIT, PRICE_UNIT_PARSER);
        newMandatoryColumn("Tx De Conversion", RefInputUnitPriceUnitConverter.PROPERTY_CONVERTION_RATE, DOUBLE_PARSER_DEFAULT_1);
        newOptionalColumn(COLUMN_ACTIVE, RefInputUnitPriceUnitConverter.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefInputUnitPriceUnitConverter, Object>> getColumnsForExport() {
        ModelBuilder<RefPrixEspece> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("PhytoProductUnit", RefInputUnitPriceUnitConverter.PROPERTY_PHYTO_PRODUCT_UNIT, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("CapacityUnit", RefInputUnitPriceUnitConverter.PROPERTY_CAPACITY_UNIT, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("MineralProductUnit", RefInputUnitPriceUnitConverter.PROPERTY_MINERAL_PRODUCT_UNIT, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("OrganicProductUnit", RefInputUnitPriceUnitConverter.PROPERTY_ORGANIC_PRODUCT_UNIT, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("SeedPlantUnit", RefInputUnitPriceUnitConverter.PROPERTY_SEED_PLANT_UNIT, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("SubstrateInputUnit", RefInputUnitPriceUnitConverter.PROPERTY_SUBSTRATE_INPUT_UNIT, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("PotInputUnit", RefInputUnitPriceUnitConverter.PROPERTY_POT_INPUT_UNIT, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("PriceUnit", RefInputUnitPriceUnitConverter.PROPERTY_PRICE_UNIT, PRICE_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("Tx De Conversion", RefInputUnitPriceUnitConverter.PROPERTY_CONVERTION_RATE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefInputUnitPriceUnitConverter.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    /**
     * String to double converter.
     * Can handle , or . as decimal separator and ' ' as thousand separator
     */
    protected static final ValueParser<Double> DOUBLE_PARSER_DEFAULT_1 = value -> {
        double result = 1d;
        if (!value.isEmpty()) {
            // " " est un espace insécable, pas un " "
            result = Double.parseDouble(value.replace(',', '.').replace(" ", ""));
        }
        return result;
    };
    
    @Override
    public RefInputUnitPriceUnitConverter newEmptyInstance() {
        RefInputUnitPriceUnitConverter result = new RefInputUnitPriceUnitConverterImpl();
        result.setActive(true);
        return result;
    }
}
