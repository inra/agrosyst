package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefActaGroupeCultures;
import fr.inra.agrosyst.api.entities.referential.RefActaGroupeCulturesImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Référentiel ActaGroupeCultures.
 * 
 * id_culture;nom_culture;id_groupe_culture;nom_groupe_culture
 */
public class RefActaGroupeCulturesModel extends AbstractAgrosystModel<RefActaGroupeCultures> implements ExportModel<RefActaGroupeCultures> {

    public RefActaGroupeCulturesModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("id_culture", RefActaGroupeCultures.PROPERTY_ID_CULTURE, INT_PARSER);
        newMandatoryColumn("nom_culture", RefActaGroupeCultures.PROPERTY_NOM_CUTURE);
        newMandatoryColumn("id_groupe_culture", RefActaGroupeCultures.PROPERTY_ID_GROUPE_CULTURE, INT_PARSER);
        newMandatoryColumn("nom_groupe_culture", RefActaGroupeCultures.PROPERTY_NOM_GROUPE_CULTURE);
        newMandatoryColumn("Commentaires", RefActaGroupeCultures.PROPERTY_COMMENTAIRES);
        newOptionalColumn(COLUMN_ACTIVE, RefActaGroupeCultures.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefActaGroupeCultures, Object>> getColumnsForExport() {
        ModelBuilder<RefActaGroupeCultures> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("id_culture", RefActaGroupeCultures.PROPERTY_ID_CULTURE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("nom_culture", RefActaGroupeCultures.PROPERTY_NOM_CUTURE);
        modelBuilder.newColumnForExport("id_groupe_culture", RefActaGroupeCultures.PROPERTY_ID_GROUPE_CULTURE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("nom_groupe_culture", RefActaGroupeCultures.PROPERTY_NOM_GROUPE_CULTURE);
        modelBuilder.newColumnForExport("Commentaires", RefActaGroupeCultures.PROPERTY_COMMENTAIRES);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefActaGroupeCultures.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefActaGroupeCultures newEmptyInstance() {
        RefActaGroupeCultures refActaGroupeCultures = new RefActaGroupeCulturesImpl();
        refActaGroupeCultures.setActive(true);
        return refActaGroupeCultures;
    }
}
