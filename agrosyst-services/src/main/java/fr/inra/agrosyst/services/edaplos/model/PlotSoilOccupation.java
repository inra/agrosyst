package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jakarta.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * {@code
 * <ram:Identification>ID</ram:Identification>
 * <ram:TypeCode listAgencyName="FR, AGRO EDI EUROPE" listAgencyID="312">ABA01</ram:TypeCode> <!--Code Précédent principal – année n-1-->
 * <ram:SequenceNumeric>0</ram:SequenceNumeric>
 * <ram:StartDateTime>2007-10-13T09:30:47Z</ram:StartDateTime>
 *
 * <ram:SownAgriculturalCrop>
 * <ram:BotanicalSpeciesCode listAgencyName="FR, AGRO EDI EUROPE" listAgencyID="312">ZCS</ram:BotanicalSpeciesCode>
 * <ram:SupplementaryBotanicalSpeciesCode listAgencyName="FR, AGRO EDI EUROPE" listAgencyID="312"></ram:SupplementaryBotanicalSpeciesCode>
 * <ram:Description>Maïs Fourrage</ram:Description>
 * </ram:SownAgriculturalCrop>
 *
 * <ram:UsedAgriculturalArea>
 * <ram:TypeCode>A17</ram:TypeCode>
 * <ram:Description>Surface de la parcelle culturale</ram:Description>
 * <ram:ActualMeasure unitCode="HAR">14.79</ram:ActualMeasure>
 * </ram:UsedAgriculturalArea>
 *
 * <ram:PreviousSoilOccupationCropResidue>
 * <ram:TypeCode listAgencyName="FR, AGRO EDI EUROPE" listAgencyID="312">ZLJ</ram:TypeCode>
 * </ram:PreviousSoilOccupationCropResidue>
 *
 * <ram:ApplicableTechnicalCharacteristic>
 * <ram:TypeCode listAgencyName="FR, AGRO EDI EUROPE" listAgencyID="312">AAA36</ram:TypeCode> <!--Reliquat Sortie Hiver-->
 *
 * <ram:ValueMeasure unitCode="NAR">52</ram:ValueMeasure>
 * </ram:ApplicableTechnicalCharacteristic>
 * }
 * </pre>
 */
public class PlotSoilOccupation implements AgroEdiObject {

    /** Culture principale – année n. */
    public static final String TYPE_CULTURE_PRINCIPALE = "ABA00";
    /** Culture dérobée. */
    public static final String TYPE_CULTURE_DEROBEE = "ABA09";
    /** Culture intermédiaire précédente (CIPAN). */
    public static final String TYPE_CULTURE_INTERMEDIAIRE_PRECEDENTE = "ABA03";
    /** Culture intermédiaire précédente (CIPAN Repousses). */
    public static final String TYPE_CULTURE_INTERMEDIAIRE_PRECEDENTE_REPOUSSEE = "ABA04";

    protected String identification;

    protected String typeCode;

    protected String sequenceNumeric;

    protected String startDateTime;

    protected String plantationYear;

    @Valid
    protected List<AgriculturalCrop> sownAgriculturalCrops = new ArrayList<>();

    @Valid
    protected SoilOccupationCropResidue previousSoilOccupationCropResidue;

    @Valid
    protected AgriculturalArea usedAgriculturalArea;

    @Valid
    protected TechnicalCharacteristic applicableTechnicalCharacteristic;

    @Valid
    protected List<AgriculturalCropProductionCycle> specifiedAgriculturalCropProductionCycles = new ArrayList<>();

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getSequenceNumeric() {
        return sequenceNumeric;
    }

    public void setSequenceNumeric(String sequenceNumeric) {
        this.sequenceNumeric = sequenceNumeric;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public List<AgriculturalCrop> getSownAgriculturalCrops() {
        return sownAgriculturalCrops;
    }

    public void setSownAgriculturalCrops(List<AgriculturalCrop> sownAgriculturalCrops) {
        this.sownAgriculturalCrops = sownAgriculturalCrops;
    }
    
    public void addSownAgriculturalCrop(AgriculturalCrop sownAgriculturalCrop) {
        sownAgriculturalCrops.add(sownAgriculturalCrop);
    }

    public List<AgriculturalCropProductionCycle> getSpecifiedAgriculturalCropProductionCycles() {
        return specifiedAgriculturalCropProductionCycles;
    }

    public void setSpecifiedAgriculturalCropProductionCycles(List<AgriculturalCropProductionCycle> specifiedAgriculturalCropProductionCycles) {
        this.specifiedAgriculturalCropProductionCycles = specifiedAgriculturalCropProductionCycles;
    }

    public void addSpecifiedAgriculturalCropProductionCycle(AgriculturalCropProductionCycle specifiedAgriculturalCropProductionCycle) {
        specifiedAgriculturalCropProductionCycles.add(specifiedAgriculturalCropProductionCycle);
    }

    public SoilOccupationCropResidue getPreviousSoilOccupationCropResidue() {
        return previousSoilOccupationCropResidue;
    }

    public void setPreviousSoilOccupationCropResidue(SoilOccupationCropResidue previousSoilOccupationCropResidue) {
        this.previousSoilOccupationCropResidue = previousSoilOccupationCropResidue;
    }

    public AgriculturalArea getUsedAgriculturalArea() {
        return usedAgriculturalArea;
    }

    public void setUsedAgriculturalArea(AgriculturalArea usedAgriculturalArea) {
        this.usedAgriculturalArea = usedAgriculturalArea;
    }

    public TechnicalCharacteristic getApplicableTechnicalCharacteristic() {
        return applicableTechnicalCharacteristic;
    }

    public void setApplicableTechnicalCharacteristic(TechnicalCharacteristic applicableTechnicalCharacteristic) {
        this.applicableTechnicalCharacteristic = applicableTechnicalCharacteristic;
    }

    public void addApecifiedAgriculturalCropProductionCycle(AgriculturalCropProductionCycle specifiedAgriculturalCropProductionCycle) {
        specifiedAgriculturalCropProductionCycles.add(specifiedAgriculturalCropProductionCycle);
    }

    public String getPlantationYear() {
        return plantationYear;
    }

    public void setPlantationYear(String plantationYear) {
        this.plantationYear = plantationYear;
    }

    @Override
    public String getLocalizedIdentifier() {
        return "parcelle '" + identification + "'";
    }
}
