package fr.inra.agrosyst.services.referential.csv.iphy;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesoCaseGroundWater;
import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesoCaseGroundWaterImpl;
import fr.inra.agrosyst.services.referential.csv.AbstractAgrosystModel;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Climate;ApplicationPeriod;Surface Texture;SubSoil_Texture;CaseNumber.
 * 
 * @author Eric Chatellier
 */
public class RefRcesoCaseGroundWaterModel extends AbstractAgrosystModel<RefRcesoCaseGroundWater> implements ExportModel<RefRcesoCaseGroundWater> {

    public RefRcesoCaseGroundWaterModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("Climate", RefRcesoCaseGroundWater.PROPERTY_CLIMATE, INT_PARSER);
        newMandatoryColumn("ApplicationPeriod", RefRcesoCaseGroundWater.PROPERTY_APPLICATION_PERIOD);
        newMandatoryColumn("Surface Texture", RefRcesoCaseGroundWater.PROPERTY_SURFACE_TEXTURE);
        newMandatoryColumn("SubSoil_Texture", RefRcesoCaseGroundWater.PROPERTY_SUBSOIL_TEXTURE);
        newMandatoryColumn("CaseNumber", RefRcesoCaseGroundWater.PROPERTY_CASE_NUMBER, INT_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefRcesoCaseGroundWater.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public RefRcesoCaseGroundWater newEmptyInstance() {
        RefRcesoCaseGroundWater result = new RefRcesoCaseGroundWaterImpl();
        result.setActive(true);
        return result;
    }

    @Override
    public Iterable<ExportableColumn<RefRcesoCaseGroundWater, Object>> getColumnsForExport() {
        ModelBuilder<RefRcesoCaseGroundWater> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Climate", RefRcesoCaseGroundWater.PROPERTY_CLIMATE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("ApplicationPeriod", RefRcesoCaseGroundWater.PROPERTY_APPLICATION_PERIOD);
        modelBuilder.newColumnForExport("Surface Texture", RefRcesoCaseGroundWater.PROPERTY_SURFACE_TEXTURE);
        modelBuilder.newColumnForExport("SubSoil_Texture", RefRcesoCaseGroundWater.PROPERTY_SUBSOIL_TEXTURE);
        modelBuilder.newColumnForExport("CaseNumber", RefRcesoCaseGroundWater.PROPERTY_CASE_NUMBER, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefRcesoCaseGroundWater.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
}
