/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.services.report;

import java.io.Serial;
import java.io.Serializable;

/**
 * Cet object est l'equivalent de la mécanique AngularJS dans le front.
 * Il contient un "PestMaster" (de type multiples) et les cultures/especes sur lesuels il porte.
 * Il contient également une meta information concernant le nombre de colonne sur lequels doit s'étendre
 * la culture concernées (1, 2, 3) ou (0) si la culture ne doit pas être affiché pour ce PestMaster.
 */
public class WithCropObject implements Serializable {
    
    @Serial
    private static final long serialVersionUID = 5175016383247449113L;
    
    protected Object crop; // parent XPestmaster (with crops and species)
    protected Object object; // current PestMaster object
    protected int span;

    public Object getCrop() {
        return crop;
    }

    public void setCrop(Object crop) {
        this.crop = crop;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public int getSpan() {
        return span;
    }

    public void setSpan(int span) {
        this.span = span;
    }
}
