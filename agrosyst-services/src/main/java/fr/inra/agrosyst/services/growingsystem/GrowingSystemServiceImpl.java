package fr.inra.agrosyst.services.growingsystem;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.MarketingDestinationObjective;
import fr.inra.agrosyst.api.entities.MarketingDestinationObjectiveTopiaDao;
import fr.inra.agrosyst.api.entities.ModalityDephy;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.NetworkTopiaDao;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMarketingDestination;
import fr.inra.agrosyst.api.entities.referential.RefTraitSdC;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgricultureTopiaDao;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.action.ActionService;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.ExtendContext;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemDto;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.api.services.practiced.OperationStatus;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.security.AgrosystAccessDeniedException;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.export.EntityExporter;
import fr.inra.agrosyst.services.common.export.ExportModelAndRows;
import fr.inra.agrosyst.services.growingsystem.export.GrowingSystemExportModels.CaracteristicsBean;
import fr.inra.agrosyst.services.growingsystem.export.GrowingSystemExportModels.CaracteristicsModel;
import fr.inra.agrosyst.services.growingsystem.export.GrowingSystemExportModels.CommonBean;
import fr.inra.agrosyst.services.growingsystem.export.GrowingSystemExportModels.MainBean;
import fr.inra.agrosyst.services.growingsystem.export.GrowingSystemExportModels.MainModel;
import fr.inra.agrosyst.services.growingsystem.export.GrowingSystemExportModels.MarketingBean;
import fr.inra.agrosyst.services.growingsystem.export.GrowingSystemExportModels.MarketingModel;
import fr.inra.agrosyst.services.growingsystem.export.GrowingSystemExportModels.PlotBean;
import fr.inra.agrosyst.services.growingsystem.export.GrowingSystemExportModels.PlotModel;
import fr.inra.agrosyst.services.growingsystem.export.GrowingSystemExportTask;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.nuiton.util.pagination.PaginationResult;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Growing system service impl.
 *
 * @author Arnaud Thimel : thimel@codelutin.com
 * @author Eric Chatellier
 */
@Setter
public class GrowingSystemServiceImpl extends AbstractAgrosystService implements GrowingSystemService {
    
    private static final Log LOGGER = LogFactory.getLog(GrowingSystemServiceImpl.class);
    
    protected ActionService actionService;
    protected AnonymizeService anonymizeService;
    protected BusinessAuthorizationService authorizationService;
    protected PlotService plotService;
    protected ReferentialService referentialService;
    protected DomainService domainService;
    protected GrowingPlanService growingPlanService;
    protected NetworkService networkService;
    
    protected GrowingSystemTopiaDao growingSystemDao;
    protected ManagementModeTopiaDao managementModeDao;
    protected MarketingDestinationObjectiveTopiaDao marketingDestinationObjectiveDao;
    protected NetworkTopiaDao networkDao;
    protected PracticedSystemTopiaDao practicedSystemDao;
    protected RefTypeAgricultureTopiaDao refTypeAgricultureTopiaDao;

    protected static final Function<MarketingDestinationObjective, RefMarketingDestination> OBJECTIVE_TO_REF_MARKETING_DESTINATION = input -> {
        RefMarketingDestination result = input.getRefMarketingDestination();
        return result;
    };

    protected static boolean isAgricultureBio(RefTypeAgriculture refTypeAgriculture) {
        if (refTypeAgriculture != null) {
            return refTypeAgriculture.getReference_id() == ReferentialService.ORGANIC_REF_TYPE_CONDUITE_REFERENCE_ID;
        } else {
            return false;
        }
    }

    protected static boolean isMissingTypeAgriculture(RefTypeAgriculture refTypeAgriculture) {
        if (refTypeAgriculture != null) {
            return refTypeAgriculture.getReference_id() == ReferentialService.MISSING_REF_TYPE_CONDUITE_REFERENCE_ID;
        } else {
            return true;
        }
    }

    @Override
    public PaginationResult<GrowingSystem> getFilteredGrowingSystems(GrowingSystemFilter filter) {
        PaginationResult<GrowingSystem> result = growingSystemDao.getFilteredGrowingSystems(filter, getSecurityContext());
        return result;
    }

    @Override
    public PaginationResult<GrowingSystemDto> getFilteredGrowingSystemsDto(GrowingSystemFilter filter) {
        PaginationResult<GrowingSystem> growingSystems = getFilteredGrowingSystems(filter);
        PaginationResult<GrowingSystemDto> result = growingSystems.transform(anonymizeService.getGrowingSystemToDtoFunction());
        for (GrowingSystemDto gs : result.getElements()) {
            boolean canValidate = authorizationService.isGrowingSystemValidable(gs.getTopiaId());
            gs.setUserCanValidate(canValidate);
        }
        return result;
    }

    @Override
    public Set<String> getFilteredGrowingSystemIds(GrowingSystemFilter filter) {
        return growingSystemDao.getFilteredGrowingSystemIds(filter, getSecurityContext());
    }

    @Override
    public List<GrowingSystemDto> getGrowingSystems(Collection<String> growingSystemIds) {
        List<GrowingSystem> growingSystems = growingSystemDao.forTopiaIdIn(growingSystemIds).findAll();
        List<GrowingSystemDto> result = growingSystems.stream().map(anonymizeService.getGrowingSystemToDtoFunction()).collect(Collectors.toList());
        return result;
    }

    @Override
    public GrowingSystem getGrowingSystem(String growingSystemId) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(growingSystemId));
        GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(growingSystemId).findUnique();
        GrowingSystem result = anonymizeService.checkForGrowingSystemAnonymization(growingSystem);
        return result;
    }

    @Override
    public GrowingSystem newGrowingSystem() {
        GrowingSystem result = growingSystemDao.newInstance();
        return result;
    }

    @Override
    public GrowingSystem createOrUpdateGrowingSystem(GrowingSystem growingSystem,
                                                     List<String> growingSystemNetworkIds,
                                                     Collection<String> selectedPlotsIds,
                                                     String typeAgricultureTopiaId,
                                                     List<String> growingSystemCharacteristicIds,
                                                     Map<Sector, List<MarketingDestinationObjective>> marketingDestinationsBySector) {
        String growingSystemId = growingSystem.getTopiaId();
        authorizationService.checkCreateOrUpdateGrowingSystem(growingSystemId);

        GrowingSystem result = createOrUpdateGrowingSystemWithoutCommit(
                growingSystem,
                growingSystemNetworkIds,
                selectedPlotsIds,
                typeAgricultureTopiaId,
                growingSystemCharacteristicIds,
                marketingDestinationsBySector);
    
        addUserAuthorization(growingSystemId, result);
    
        getTransaction().commit();
    
        return result;
    }

    @Override
    public GrowingSystem createDefaultIpmworksGrowingSystem(
            String domainId,
            String growingSystemName,
            Sector growingSystemSector,
            String ipmWorksId,
            String typeAgricultureId,
            Map<Sector, List<MarketingDestinationObjective>> marketingDestinationsBySector
    ) {

        GrowingSystem growingSystem = createDefaultIpmworksGrowingSystemWithoutCommit(
                domainId,
                growingSystemName,
                growingSystemSector,
                ipmWorksId,
                typeAgricultureId,
                marketingDestinationsBySector
        );
        // growingSystemId = null to force adding rules
        addUserAuthorization(null, growingSystem);

        getTransaction().commit();

        return growingSystem;
    }

    protected void checkActivated(GrowingSystem growingSystem) {
        if(growingSystem.isPersisted()) {
            Preconditions.checkArgument(
                    growingSystem.isActive() &&
                            growingSystem.getGrowingPlan().isActive() &&
                            growingSystem.getGrowingPlan().getDomain().isActive(),
                    String.format("The growing system with id '%s' or the growing plan or domain related to it are unactivated"
                            , growingSystem.getTopiaId()));
        }
    }
    
    protected void addUserAuthorization(String growingSystemId, GrowingSystem result) {
        if (StringUtils.isBlank(growingSystemId)) {
            authorizationService.growingSystemCreated(result);
        }
    }

    protected GrowingSystem createDefaultIpmworksGrowingSystemWithoutCommit(
            String domainId,
            String growingSystemName,
            Sector growingSystemSector,
            String ipmWorksId,
            String typeAgricultureId,
            Map<Sector, List<MarketingDestinationObjective>> marketingDestinationsBySector
    ) {

        Domain domain = domainService.getDomain(domainId);
        GrowingPlan growingPlan = growingPlanService.getDefaultGrowingPlanForDomain(domain);

        GrowingSystem growingSystem = newGrowingSystem();
        growingSystem.setGrowingPlan(growingPlan);
        growingSystem.setName(growingSystemName);
        growingSystem.setSector(growingSystemSector);
        growingSystem.setDephyNumber(ipmWorksId);
        growingSystem.setStartingDate(LocalDate.of(domain.getCampaign(), 1, 1));
        growingSystem.setActive(true);

        List<Network> growingSystemNetworks = networkService.getNetworksForUserManager(getAuthenticatedUser().getTopiaId());

        checkIpmworksCreateOrUpdatePreconditions(growingSystem);

        if (CollectionUtils.isNotEmpty(growingSystemNetworks)) {
            growingSystem.setNetworks(growingSystemNetworks);
        } else {
            growingSystem.clearNetworks();
        }
        clearCacheAndSync();

        final String growingSystemId = growingSystem.getTopiaId();

        RefTypeAgriculture typeAgriculture = referentialService.getRefTypeAgricultureWithId(typeAgricultureId);
        growingSystem.setTypeAgriculture(typeAgriculture);

        GrowingSystem persistedGrowingSystem;
        growingSystem.setUpdateDate(context.getCurrentTime());
        if (StringUtils.isBlank(growingSystemId)) {
            growingSystem.setActive(true);
            // create a random growingSystem code, used to link growingSystems each other
            setGrowingSystemCode(growingSystem);
            persistedGrowingSystem = growingSystemDao.create(growingSystem);
        } else {
            persistedGrowingSystem = growingSystemDao.update(growingSystem);
        }

        if (isAgricultureBio(typeAgriculture)) {
            boolean isOrganic = isAgricultureBio(typeAgriculture);
            // update all valorisation
            Collection<HarvestingAction> harvestingActionForGrowingSystem = actionService.getHarvestingActionForGrowingSystem(growingSystem);

            harvestingActionForGrowingSystem.stream()
                    .map(HarvestingAction::getValorisations)
                    .flatMap(Collection::stream)
                    .forEach(valorisation -> valorisation.setIsOrganicCrop(isOrganic));

            actionService.updateAllValorisations(harvestingActionForGrowingSystem);

        }

        // save selected plots
        plotService.updatePlotsGrowingSystemRelationship(growingSystem, null);

        manageMarketingDestinationObjectives(growingSystem, marketingDestinationsBySector, persistedGrowingSystem);

        return persistedGrowingSystem;
    }

    protected GrowingSystem createOrUpdateGrowingSystemWithoutCommit(
            GrowingSystem growingSystem,
            List<String> growingSystemNetworkIds,
            Collection<String> selectedPlotsIds,
            String typeAgricultureId,
            List<String> growingSystemCharacteristicIds,
            Map<Sector, List<MarketingDestinationObjective>> marketingDestinationsBySector) {
        
        checkCreateOrUpdatePreconditions(growingSystem);
        
        if (growingSystemNetworkIds != null) {
            List<Network> networks = networkDao.forTopiaIdIn(growingSystemNetworkIds).findAll();
            Preconditions.checkArgument(networks.size() == growingSystemNetworkIds.size(), "Réseau non retrouvé !");
            growingSystem.setNetworks(networks);
        } else {
            growingSystem.clearNetworks();
        }
        clearCacheAndSync();
        
        if (growingSystem.getGrowingPlan() != null && TypeDEPHY.DEPHY_FERME.equals(growingSystem.getGrowingPlan().getType())) {
            if (ModalityDephy.ALLEGE.equals(growingSystem.getModality())) {
                growingSystem.setDephyNumber(null);
            }
        } else {
            growingSystem.setModality(null);
            growingSystem.setDephyNumber(null);
        }
    
        /*
         * GrowingSystemCharacteristic
         * Simple scenario, so the GrowingSystemCharacteristics list is erased and re-completed each time (for more complexity see plots and zones)
         */
        if (growingSystemCharacteristicIds != null) {
            List<RefTraitSdC> growingSystemCharacteristics = referentialService.getTraitSdcForIdsIn(growingSystemCharacteristicIds);
            Preconditions.checkArgument(growingSystemCharacteristics.size() == growingSystemCharacteristicIds.size(), "Caractéristiques non trouvées !");
            growingSystem.setCharacteristics(growingSystemCharacteristics);
        }
    
        final String growingSystemId = growingSystem.getTopiaId();
    
        RefTypeAgriculture prevRefTypeAgriculture = null;
        if (StringUtils.isNotBlank(growingSystemId)) {
            prevRefTypeAgriculture = growingSystemDao.findGrowingSystemRefTypeAgriculture(growingSystemId);
        }

        RefTypeAgriculture typeAgriculture = referentialService.getRefTypeAgricultureWithId(typeAgricultureId);
        growingSystem.setTypeAgriculture(typeAgriculture);

        GrowingSystem persistedGrowingSystem;
        growingSystem.setUpdateDate(context.getCurrentTime());
        if (StringUtils.isBlank(growingSystemId)) {
            growingSystem.setActive(true);
            // create a random growingSystem code, used to link growingSystems each other
            setGrowingSystemCode(growingSystem);
            persistedGrowingSystem = growingSystemDao.create(growingSystem);
        } else {
            persistedGrowingSystem = growingSystemDao.update(growingSystem);
        }

        if (prevRefTypeAgriculture != typeAgriculture &&
                (isAgricultureBio(prevRefTypeAgriculture) || isAgricultureBio(typeAgriculture))) {
            boolean isOrganic = isAgricultureBio(typeAgriculture);
            // update all valorisation
            Collection<HarvestingAction> harvestingActionForGrowingSystem = actionService.getHarvestingActionForGrowingSystem(growingSystem);

            harvestingActionForGrowingSystem.stream()
                    .map(HarvestingAction::getValorisations)
                    .flatMap(Collection::stream)
                    .forEach(valorisation -> valorisation.setIsOrganicCrop(isOrganic));

            actionService.updateAllValorisations(harvestingActionForGrowingSystem);
        }
    
        // save selected plots
        plotService.updatePlotsGrowingSystemRelationship(growingSystem, selectedPlotsIds);
    
        manageMarketingDestinationObjectives(growingSystem, marketingDestinationsBySector, persistedGrowingSystem);
    
        return persistedGrowingSystem;
    }

    private void checkCreateOrUpdatePreconditions(GrowingSystem growingSystem) {

        checkActivated(growingSystem);

        final String growingSystemName = growingSystem.isPersisted() ? growingSystem.getTopiaId() : "NEW";

        Preconditions.checkNotNull(
                growingSystem.getGrowingPlan(),
                String.format("For growing system '%s' no growing plan selected !", growingSystemName));

        Preconditions.checkNotNull(growingSystem.getSector(),
                String.format("For growing system '%s' no sector selected !", growingSystemName));

        Preconditions.checkNotNull(growingSystem.getStartingDate(),
                String.format("For growing system '%s' no starting date defined !", growingSystemName));

        if (growingSystem.getGrowingPlan() != null && TypeDEPHY.DEPHY_FERME.equals(growingSystem.getGrowingPlan().getType())) {
    
            Preconditions.checkNotNull(growingSystem.getModality(),
                    String.format("For growing system '%s' with Growing Plan DEPHY type = 'FERME' no DEPHY modality defined !",
                            growingSystemName));
    
            if (ModalityDephy.DETAILLE.equals(growingSystem.getModality())) {
                Preconditions.checkArgument(Boolean.TRUE.equals(StringUtils.isNotBlank(growingSystem.getDephyNumber())),
                        String.format("For growing system '%s' with Growing Plan DEPHY type = 'FERME' no DEPHY number defined !",
                                growingSystemName));
            }
    
        }

        if (Sector.HORTICULTURE == growingSystem.getSector() || Sector.MARAICHAGE == growingSystem.getSector()) {
            Preconditions.checkNotNull(growingSystem.getProduction(),
                    String.format("For growing system %s with sector %s, no Production defined!", growingSystemName, growingSystem.getSector()));
        }
    
    }
    private void checkIpmworksCreateOrUpdatePreconditions(GrowingSystem growingSystem) {

        checkActivated(growingSystem);

        final String growingSystemName = growingSystem.isPersisted() ? growingSystem.getTopiaId() : "NEW";

        Preconditions.checkNotNull(
                growingSystem.getGrowingPlan(),
                String.format("For growing system '%s' no growing plan selected !", growingSystemName));

        Preconditions.checkNotNull(growingSystem.getSector(),
                String.format("For growing system '%s' no sector selected !", growingSystemName));

        Preconditions.checkNotNull(growingSystem.getStartingDate(),
                String.format("For growing system '%s' no starting date defined !", growingSystemName));

        if (Sector.HORTICULTURE == growingSystem.getSector() || Sector.MARAICHAGE == growingSystem.getSector()) {
            Preconditions.checkNotNull(growingSystem.getProduction(),
                    String.format("For growing system %s with sector %s, no Production defined!", growingSystemName, growingSystem.getSector()));
        }

    }
    
    private void manageMarketingDestinationObjectives(
            GrowingSystem growingSystem,
            Map<Sector, List<MarketingDestinationObjective>> marketingDestinationsBySector,
            GrowingSystem persistedGrowingSystem) {
        
        Sector sector = growingSystem.getSector();
        List<MarketingDestinationObjective> objectives = marketingDestinationsBySector.get(sector);
        
        List<MarketingDestinationObjective> objectives_ = marketingDestinationObjectiveDao.forGrowingSystemEquals(growingSystem).findAll();
        Map<String, MarketingDestinationObjective> objectives_ByIds = Maps.newHashMap(Maps.uniqueIndex(objectives_, Entities.GET_TOPIA_ID::apply));
        Binder<MarketingDestinationObjective, MarketingDestinationObjective> binder = BinderFactory.newBinder(MarketingDestinationObjective.class);
        List<MarketingDestinationObjective> objectivesToUpdate = new ArrayList<>();
        
        bindMarketingDestinationObjectives(persistedGrowingSystem, objectives, objectives_ByIds, binder, objectivesToUpdate);
        
        marketingDestinationObjectiveDao.updateAll(objectivesToUpdate);
        marketingDestinationObjectiveDao.deleteAll(objectives_ByIds.values());
    }
    
    private void bindMarketingDestinationObjectives(
            GrowingSystem persistedGrowingSystem,
            List<MarketingDestinationObjective> objectives,
            Map<String, MarketingDestinationObjective> objectives_ByIds,
            Binder<MarketingDestinationObjective, MarketingDestinationObjective> binder,
            List<MarketingDestinationObjective> objectivesToUpdate) {
        
        if (objectives != null) {
            for (MarketingDestinationObjective objectiveToSave : objectives) {
                MarketingDestinationObjective target = objectives_ByIds.remove(objectiveToSave.getTopiaId());
                
                if (target != null) {
                    bindMarketingDestinationObjective(binder, objectiveToSave, target);
                } else {
                    target = objectiveToSave;
                }
                target.setGrowingSystem(persistedGrowingSystem);
                
                objectivesToUpdate.add(target);
                
            }
        }
    }
    
    private void bindMarketingDestinationObjective(
            Binder<MarketingDestinationObjective, MarketingDestinationObjective> binder,
            MarketingDestinationObjective objectiveToSave,
            MarketingDestinationObjective target) {
        binder.copyExcluding(objectiveToSave, target,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                MarketingDestinationObjective.PROPERTY_GROWING_SYSTEM
        );
    }
    
    private void setGrowingSystemCode(GrowingSystem growingSystem) {
        if (StringUtils.isBlank(growingSystem.getCode())) {
            growingSystem.setCode(UUID.randomUUID().toString());
        }
    }

    @Override
    public List<GrowingSystem> findAllByGrowingPlan(GrowingPlan growingPlan) {
        List<GrowingSystem> result = growingSystemDao.forGrowingPlanEquals(growingPlan).findAll();
        return result;
    }

    @Override
    public GrowingSystem duplicateGrowingSystem(ExtendContext extendContext, GrowingPlan clonedGrowingPlan, GrowingSystem growingSystem) {

        // perform clone
        List<MarketingDestinationObjective> objectives = marketingDestinationObjectiveDao.forGrowingSystemEquals(growingSystem).findAll();
        Binder<GrowingSystem, GrowingSystem> binder = BinderFactory.newBinder(GrowingSystem.class);
        GrowingSystem clonedGrowingSystem = growingSystemDao.newInstance();
        binder.copyExcluding(growingSystem, clonedGrowingSystem,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                GrowingSystem.PROPERTY_GROWING_PLAN,
                GrowingSystem.PROPERTY_NETWORKS,
                GrowingSystem.PROPERTY_UPDATE_DATE,
                GrowingSystem.PROPERTY_VALIDATION_DATE);
        clonedGrowingSystem.setGrowingPlan(clonedGrowingPlan);
        clonedGrowingSystem.setValidated(false);

        // break code in duplication case
        if (extendContext.isDuplicateOnly()) {
            clonedGrowingSystem.setCode(UUID.randomUUID().toString());
        }

        // force collection copy instead of collection reference copy
        if (growingSystem.getNetworks() != null) {
            clonedGrowingSystem.setNetworks(Lists.newArrayList(growingSystem.getNetworks()));
        }

        // same for growingSystemCharacteristics
        if (growingSystem.getCharacteristics() != null) {
            clonedGrowingSystem.setCharacteristics(Lists.newArrayList(growingSystem.getCharacteristics()));
        }
    
        // persist clone
        clonedGrowingSystem.setUpdateDate(context.getCurrentTime());
        GrowingSystem clonedGrowingSystemResult = growingSystemDao.create(clonedGrowingSystem);
        extendContext.addGrowingSystem(growingSystem, clonedGrowingSystemResult);
    
        Binder<MarketingDestinationObjective, MarketingDestinationObjective> marketingDestinationObjectiveBinder =
                BinderFactory.newBinder(MarketingDestinationObjective.class);
        List<MarketingDestinationObjective> copiedMarketingDestinationObjectives = new ArrayList<>();
        for (MarketingDestinationObjective objective : objectives) {
            MarketingDestinationObjective target = marketingDestinationObjectiveDao.newInstance();
            bindMarketingDestinationObjective(marketingDestinationObjectiveBinder, objective, target);
            target.setGrowingSystem(clonedGrowingSystemResult);
            copiedMarketingDestinationObjectives.add(target);
        }
        marketingDestinationObjectiveDao.createAll(copiedMarketingDestinationObjectives);
    
        return clonedGrowingSystemResult;
    }

    @Override
    public void unactivateGrowingSystems(List<String> growingSystemsIds, boolean activate) {
        if (CollectionUtils.isNotEmpty(growingSystemsIds)) {

            for (String growingSystemId : growingSystemsIds) {
                GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(growingSystemId).findUnique();
                growingSystem.setActive(activate);
                if (growingSystem.getEndingDate() == null) {
                    growingSystem.setEndingDate(context.getCurrentDate());
                }
                growingSystem.setUpdateDate(context.getCurrentTime());
                growingSystemDao.update(growingSystem);
            }
            getTransaction().commit();
        }
    }

    @Override
    public List<GrowingSystem> findAllActiveByDomainForPlotEdit(Domain domain) {
        List<GrowingSystem> result = growingSystemDao.findAllActiveWritableByDomain(domain, getSecurityContext());
        return result;
    }

    @Override
    public LinkedHashMap<Integer, String> getRelatedGrowingSystemIdsByCampaigns(String growingSystemCode) {
        LinkedHashMap<Integer, String> result = growingSystemDao.findAllRelatedGrowingSystemsIdByCampaigns(growingSystemCode);
        return result;
    }
    
    @Override
    public GrowingSystem getLastGrowingSystemForCampaigns(String growingSystemCode, Set<Integer> practicedSystemCampaigns) {
        GrowingSystem result = growingSystemDao.findLastGrowingSystemForCampaigns(growingSystemCode, practicedSystemCampaigns);
        return result;
    }
    
    @Override
    public long getGrowingSystemsCount(Boolean active) {
        if (active == null) {
            return growingSystemDao.count();
        }
        return growingSystemDao.forActiveEquals(active).count();
    }

    @Override
    public List<GrowingSystem> findAllByNetwork(Network network) {
        List<GrowingSystem> result = growingSystemDao.forNetworksContains(network).findAll();
        return result;
    }

    @Override
    public List<OperationStatus> validate(List<String> growingSystemIds, boolean validate) {
    
        List<OperationStatus> result = new ArrayList<>();
    
        List<GrowingSystem> growingSystemsToChangeValidationStatus =
                validate ? getAllUnValidatedForGivenIds(growingSystemIds)
                        : getAllValidatedForGivenIds(growingSystemIds);
    
        for (GrowingSystem growingSystem : growingSystemsToChangeValidationStatus) {
        
            OperationStatus status = new OperationStatus();
            result.add(status);
        
            String growingSystemName = growingSystem.getName();
            int campaign = growingSystem.getGrowingPlan().getDomain().getCampaign();
        
            try {
                authorizationService.checkValidateGrowingSystem(growingSystem.getTopiaId());
        
                // AThimel 09/12/13 Perform DB update is more powerful
                growingSystemDao.updateGrowingSystemValidateStatus(growingSystem.getTopiaId(), context.getCurrentTime(), validate);
    
                status.success(String.format(
                        "Système de culture '%s' %s pour la campagne '%d-%d'",
                        growingSystemName,
                        validate ? "validé" : "dévalidé",
                        campaign - 1,
                        campaign));
                
            } catch (AgrosystAccessDeniedException deniedException) {
                status.failed(String.format("Pour le système de culture '%s' campagne '%d-%d', l'erreur suivante est survenue: %s",
                        growingSystemName,
                        campaign - 1,
                        campaign,
                        "Droits insuffisants pour modifier l'état de validation du système de culture"));
            }
        }
        
        getTransaction().commit();
    
        // AThimel 09/12/13 The next line makes sure that cache state is synchronized with database state
        getPersistenceContext().getHibernateSupport().getHibernateSession().clear();

        return result;
    }

    @Override
    public List<RefTypeAgriculture> findAllTypeAgricultures() {
        List<RefTypeAgriculture> result = referentialService.getAllTypeAgricultures();
        return result;
    }

    @Override
    public List<PracticedSystem> getPracticedSystems(String growingSystemCode) {
        List<PracticedSystem> result = practicedSystemDao.forProperties(
                PracticedSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_CODE, growingSystemCode)
                .setOrderByArguments(
                        PracticedSystem.PROPERTY_CAMPAIGNS,
                        PracticedSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_NAME)
                .findAll();
        return result;
    }

    @Override
    public List<ManagementMode> getManagementModes(String growingSystemId) {
        List<ManagementMode> result = managementModeDao.forProperties(
                ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID, growingSystemId)
                .setOrderByArguments(
                        ManagementMode.PROPERTY_VERSION_NUMBER, ManagementMode.PROPERTY_CATEGORY)
                .findAll();
        return result;
    }

    @Override
    public List<Integer> getGrowingSystemCampaigns(String growingSystemCode) {
        List<Integer> result = growingSystemDao.findCampaigns(growingSystemCode);
        return result;
    }

    @Override
    public List<GrowingSystem> getAvailableGsForDuplication(String practicedSystemId, NavigationContext navigationContext) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(practicedSystemId));
    
        PracticedSystem practicedSystem = practicedSystemDao.forTopiaIdEquals(practicedSystemId).findUnique();
        // Dans un premier temps on filtre sur les systèmes de cultures sur lesquels on a le droit en écriture
        GrowingSystemFilter growingSystemFilter = new GrowingSystemFilter();
        growingSystemFilter.setAllPageSize();
        growingSystemFilter.setActive(true);
        // refs #9926 il est privilégié de remonter l'erreur à l'utilisateur
        //growingSystemFilter.setActiveParents(true);
        growingSystemFilter.setNavigationContext(navigationContext);
    
        // dans un second temps on ne garde que les sdc pointant vers un domaine du même code que celui du mode de gestion
        //  et sur lesquels il n'y a pas de mode de gestion.
        List<GrowingSystem> result = growingSystemDao.findAllAvailableFoPracticedSystemDuplication(growingSystemFilter, practicedSystem.getGrowingSystem());
    
    
        return result;
    }

    @Override
    public List<GrowingSystem> getGrowingSystemWithNameAndCampaign(String growingSystemName, Integer campaign) {
        Preconditions.checkNotNull(growingSystemName);
        List<GrowingSystem> result;
        if (campaign != null) {
            String campaignProperty = GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN;
            result = growingSystemDao.forProperties(GrowingSystem.PROPERTY_NAME, growingSystemName, campaignProperty , campaign).findAll();
        } else {
            result = growingSystemDao.forProperties(GrowingSystem.PROPERTY_NAME, growingSystemName).findAll();
        }
        return result;
    }

    @Override
    public ExportResult exportGrowingSystemsAsXls(Collection<String> growingSystemIds) {

        // get all possible bean infos
        List<MainBean> mainBeans = new LinkedList<>();
        List<MarketingBean> marketingBeans = new LinkedList<>();
        List<CaracteristicsBean> caracteristicsBeans = new LinkedList<>();
        List<PlotBean> plotBeans = new LinkedList<>();

        try {
            if (CollectionUtils.isNotEmpty(growingSystemIds)) {
                long start = System.currentTimeMillis();
                Iterable<List<String>> chunks = Iterables.partition(growingSystemIds, 20);
                for (List<String> chunk : chunks) {

                    growingSystemDao.clear();

                    List<GrowingSystem> growingSystems = growingSystemDao.forTopiaIdIn(chunk).findAll();
                    for (GrowingSystem rawGrowingSystem : growingSystems) {
                        // anonymize growing system if necessary
                        final GrowingSystem growingSystem = anonymizeService.checkForGrowingSystemAnonymization(rawGrowingSystem);

                        GrowingPlan growingPlan = growingSystem.getGrowingPlan();
                        Domain domain = growingPlan.getDomain();
                        CommonBean baseBean = new CommonBean(
                                domain.getLocation().getDepartement(),
                                growingSystem.getName(),
                                growingSystem.getSector(),
                                growingSystem.getStartingDate(),
                                growingPlan.getName(),
                                domain.getName(),
                                domain.getCampaign()
                        );

                        // main tab
                        MainBean mainBean = toMainBean(growingSystem, baseBean);
                        mainBeans.add(mainBean);

                        // Plots tab
                        List<MarketingDestinationObjective> objectives = marketingDestinationObjectiveDao.forGrowingSystemEquals(growingSystem).findAll();
                        CollectionUtils.emptyIfNull(objectives)
                                .stream()
                                .map(objective -> toMarketingBean(objective, baseBean))
                                .forEach(marketingBeans::add);

                        // characteristics tab
                        Optional<String> typeAgriculture = Optional.ofNullable(growingSystem.getTypeAgriculture())
                                .map(RefTypeAgriculture::getReference_label);
                        Supplier<CaracteristicsBean> caracteristicsSupplier = () -> {
                            CaracteristicsBean caracteristicsBean = new CaracteristicsBean(baseBean);
                            typeAgriculture.ifPresent(caracteristicsBean::setTypeAgriculture);
                            caracteristicsBean.setCategoryStrategy(growingSystem.getCategoryStrategy());
                            caracteristicsBean.setAverageIFT(growingSystem.getAverageIFT());
                            caracteristicsBean.setBiocontrolIFT(growingSystem.getBiocontrolIFT());
                            caracteristicsBean.setEstimatingIftRules(growingSystem.getEstimatingIftRules());
                            caracteristicsBean.setIftSeedsType(growingSystem.getIftSeedsType());
                            caracteristicsBean.setDoseType(growingSystem.getDoseType());
                            return caracteristicsBean;
                        };
                        Collection<RefTraitSdC> characteristics = growingSystem.getCharacteristics();
                        if (CollectionUtils.isNotEmpty(characteristics)) {
                            for (RefTraitSdC characteristic : characteristics) {
                                CaracteristicsBean caracteristicsBean = caracteristicsSupplier.get();

                                caracteristicsBean.setTypeTraitSdc(characteristic.getType_trait_sdc());
                                caracteristicsBean.setNomTrait(characteristic.getNom_trait());

                                String characteristicComment = null;
                                if (StringUtils.isNotBlank(growingSystem.getCycleManagementComment())) {
                                    characteristicComment = growingSystem.getCycleManagementComment();
                                } else if (StringUtils.isNotBlank(growingSystem.getCultureManagementComment())) {
                                    characteristicComment = growingSystem.getCultureManagementComment();
                                } else if (StringUtils.isNotBlank(growingSystem.getGroundWorkComment())) {
                                    characteristicComment = growingSystem.getGroundWorkComment();
                                } else if (StringUtils.isNotBlank(growingSystem.getParcelsManagementComment())) {
                                    characteristicComment = growingSystem.getParcelsManagementComment();
                                }
                                caracteristicsBean.setCharacteristicComment(characteristicComment);
                                caracteristicsBeans.add(caracteristicsBean);
                            }
                        } else {
                            // the fields are part of characteristics tabs but ore not related characteristics entities.
                            // they can be setted and so must be exported even if there are no characteristics entities.
                            caracteristicsBeans.add(caracteristicsSupplier.get());
                        }

                        // Plots tab
                        List<Plot> plots = plotService.getAllGrowingSystemPlot(growingSystem);
                        CollectionUtils.emptyIfNull(plots)
                                .stream()
                                .map(plot -> toPlotBean(growingSystem, baseBean, plot))
                                .forEach(plotBeans::add);
                    }
                }
                if (LOGGER.isInfoEnabled()) {
                    long duration = System.currentTimeMillis() - start;
                    int count = growingSystemIds.size();
                    LOGGER.warn(String.format("%d SdC exportés en %dms (%dms/sdc)", count, duration, duration / count));
                }
            }
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't copy properties", ex);
        }

        ExportModelAndRows<MainBean> mainTab = new ExportModelAndRows<>(new MainModel(), mainBeans);
        ExportModelAndRows<MarketingBean> marketingTab = new ExportModelAndRows<>(new MarketingModel(), marketingBeans);
        ExportModelAndRows<CaracteristicsBean> caracteristicsTab = new ExportModelAndRows<>(new CaracteristicsModel(), caracteristicsBeans);
        ExportModelAndRows<PlotBean> plotTab = new ExportModelAndRows<>(new PlotModel(), plotBeans);

        // technical export
        EntityExporter exporter = new EntityExporter();
        ExportResult result = exporter.exportAsXlsx(
                "Systeme-de-culture-export",
                mainTab,
                marketingTab,
                caracteristicsTab,
                plotTab
        );

        return result;

    }

    @Override
    public void exportGrowingSystemAsXlsAsync(Collection<String> growingSystemIds) {
        AuthenticatedUser user = getAuthenticatedUser();

        GrowingSystemExportTask exportTask = new GrowingSystemExportTask(user.getTopiaId(), user.getEmail(), growingSystemIds);
        getBusinessTaskManager().schedule(exportTask);
    }

    protected PlotBean toPlotBean(GrowingSystem growingSystem, CommonBean baseBean, Plot plot) {
        PlotBean plotBean = new PlotBean(baseBean);
        plotBean.setPlotName(plot.getName());
        plotBean.setPacIlotNumber(plot.getPacIlotNumber());
        plotBean.setPlotOutputReason(growingSystem.getPlotOutputReason());
        return plotBean;
    }

    protected MarketingBean toMarketingBean(MarketingDestinationObjective objective, CommonBean baseBean) {
        MarketingBean result = new MarketingBean(baseBean);
        Optional.ofNullable(objective.getRefMarketingDestination())
                .map(RefMarketingDestination::getMarketingDestination)
                .ifPresent(result::setMarketingDestination);
        result.setSelectedForGS(objective.isSelectedForGS());
        result.setPart(objective.getPart());
        return result;
    }

    protected MainBean toMainBean(GrowingSystem growingSystem, CommonBean baseBean) {
        MainBean mainBean = new MainBean(baseBean);
        mainBean.setDephyNumber(growingSystem.getDephyNumber());
        String networkNames = growingSystem.getNetworks()
                .stream()
                .map(Network::getName)
                .collect(Collectors.joining(", "));
        mainBean.setNetworkNames(networkNames);
        mainBean.setDescription(growingSystem.getDescription());
        mainBean.setAffectedAreaRate(growingSystem.getAffectedAreaRate());
        mainBean.setAffectedWorkForceRate(growingSystem.getAffectedWorkForceRate());
        mainBean.setDomainsToolsUsageRate(growingSystem.getDomainsToolsUsageRate());
        mainBean.setEndingDate(growingSystem.getEndingDate());
        mainBean.setEndActivityComment(growingSystem.getEndActivityComment());
        mainBean.setOverallObjectives(growingSystem.getOverallObjectives());
        return mainBean;
    }

    @Override
    public Sector getGrowingSystemSector(String growingSystemId) {
        Sector sector = growingSystemDao.findGrowingSystemSector(growingSystemId);
        return sector;
    }

    @Override
    public Boolean isOrganicGrowingSystem(String growingSystemId) {
        RefTypeAgriculture refTypeAgriculture = growingSystemDao.findGrowingSystemRefTypeAgriculture(growingSystemId);
        boolean result = isAgricultureBio(refTypeAgriculture);
        return result;
    }

    @Override
    public boolean isMissingTypeAgriculture(GrowingSystem growingSystem) {
        boolean result = isMissingTypeAgriculture(growingSystem.getTypeAgriculture());
        return result;
    }

    @Override
    public String getMissingTypeAgricultureTopiaId() {
        String topiaId = refTypeAgricultureTopiaDao
                .forReference_idEquals(ReferentialService.MISSING_REF_TYPE_CONDUITE_REFERENCE_ID)
                .findUnique()
                .getTopiaId();
        return topiaId;
    }

    @Override
    public Map<Sector, List<MarketingDestinationObjective>> loadMarketingDestinationObjective(GrowingSystem growingSystem) {
        Sector sector = growingSystem.getSector();
        Set<Sector> sectorSet = new HashSet<>(Collections.singletonList(sector));
        EnumMap<Sector, List<RefMarketingDestination>> refMarketingDestinationsBySectors =
                referentialService.getRefMarketingDestinationsBySectorForSectors(sectorSet);
    
        List<RefMarketingDestination> refMarketingDestinationForSector = refMarketingDestinationsBySectors.get(sector);
    
        List<MarketingDestinationObjective> marketingDestinationObjectives =
                marketingDestinationObjectiveDao.forGrowingSystemEquals(growingSystem)
                        .setOrderByArguments(
                                MarketingDestinationObjective.PROPERTY_REF_MARKETING_DESTINATION +
                                        "." + RefMarketingDestination.PROPERTY_MARKETING_DESTINATION)
                        .findAll();
    
        if (refMarketingDestinationForSector != null &&
                (refMarketingDestinationForSector.size() != marketingDestinationObjectives.size())) {
        
            Map<RefMarketingDestination, MarketingDestinationObjective> obbjectiveForMD = Maps.uniqueIndex(
                    marketingDestinationObjectives, OBJECTIVE_TO_REF_MARKETING_DESTINATION::apply);
        
            for (RefMarketingDestination refMarketingDestination : refMarketingDestinationForSector) {
                MarketingDestinationObjective objective = obbjectiveForMD.get(refMarketingDestination);
                if (objective == null) {
                    // add missing objective
                    objective = marketingDestinationObjectiveDao.newInstance();
                    objective.setGrowingSystem(growingSystem);
                    objective.setRefMarketingDestination(refMarketingDestination);
                    marketingDestinationObjectives.add(objective);
                }
            }
            marketingDestinationObjectiveDao.updateAll(marketingDestinationObjectives);
        }
        EnumMap<Sector, List<MarketingDestinationObjective>> result = new EnumMap<>(Sector.class);
        result.put(sector, marketingDestinationObjectives);
        return result;
    }

    @Override
    public Map<Sector, List<MarketingDestinationObjective>> loadMarketingDestinationObjectivesForSectors(Set<Sector> sectors) {
        EnumMap<Sector, List<MarketingDestinationObjective>> result = new EnumMap<>(Sector.class);
        EnumMap<Sector, List<RefMarketingDestination>> refMarketingDestinationsBySectors =
                referentialService.getRefMarketingDestinationsBySectorForSectors(sectors);
        referentialService.getRefMarketingDestinationsBySectorForSectors(sectors);
        for (Map.Entry<Sector, List<RefMarketingDestination>> sectorToRefMarketingDestinations : refMarketingDestinationsBySectors.entrySet()) {
            Sector sector = sectorToRefMarketingDestinations.getKey();
            List<RefMarketingDestination> marketingDestinations = sectorToRefMarketingDestinations.getValue();
            List<MarketingDestinationObjective> objectivesForSector = new ArrayList<>();
            result.put(sector, objectivesForSector);
            for (RefMarketingDestination marketingDestination : marketingDestinations) {
                MarketingDestinationObjective marketingDestinationObjective = marketingDestinationObjectiveDao.newInstance();
                marketingDestinationObjective.setRefMarketingDestination(marketingDestination);
                objectivesForSector.add(marketingDestinationObjective);
            }
        }
        return result;
    }

    @Override
    public List<Integer> getGrowingSystemCampaignsFromId(String growingSystemId, NavigationContext navigationContext) {
        List<Integer>  result = growingSystemDao.findCampaignsFromGrowingSystemId(growingSystemId);
        return result;
    }

    @Override
    public List<CroppingPlanEntry> getCropForGrowingSystemId(String growingSystemId) {
        List<CroppingPlanEntry> result = growingSystemDao.findCropForGrowingSystemId(growingSystemId);
        return result;
    }
    
    @Override
    public List<GrowingSystem> getAllUnValidatedForGivenIds(List<String> growingSystemIds) {
        return growingSystemDao.forTopiaIdIn(growingSystemIds).addEquals(PracticedSystem.PROPERTY_VALIDATED, false).findAll();
    }
    
    @Override
    public List<GrowingSystem> getAllValidatedForGivenIds(List<String> growingSystemIds) {
        return growingSystemDao.forTopiaIdIn(growingSystemIds).addEquals(PracticedSystem.PROPERTY_VALIDATED, true).findAll();
    }

    @Override
    public GrowingSystem updateGrowingSystem(GrowingSystem growingSystem) {
        return growingSystemDao.update(growingSystem);
    }

}
