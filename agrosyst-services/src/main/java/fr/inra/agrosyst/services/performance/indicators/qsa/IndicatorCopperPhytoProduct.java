package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.MultiValuedMap;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

/**
 * #12314
 * à toutes les échelles, on calcule un indicateur sur le même principe que QSA totale, en ne prenant en compte que les SA pour lesquelles la colonne "cuivre" prend la valeur "1"
 */
public class IndicatorCopperPhytoProduct extends IndicatorSubstancesCandidateToSubstitution {

    private static final String COLUMN_NAME = "qsa_cuivre_phyto";
    private static final String COLUMN_NAME_HTS = "qsa_cuivre_phyto_hts";

    private final String[] indicators = new String[]{
            "Indicator.label.copperPhytoProduct",        // QSA Cuivre phyto
            "Indicator.label.copperPhytoProduct_hts",    // QSA Cuivre phyto hors traitement de semence
    };

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME_HTS);
        return indicatorNameToColumnName;
    }

    @Override
    protected @NotNull List<RefCompositionSubstancesActivesParNumeroAMM> getQSA_Substance(Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> domainSubstancesByAmm, MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allCopperByAmmCode, String codeAmm) {

        List<String> idSaCopper = allCopperByAmmCode.get(codeAmm)
                .stream()
                .filter(RefSubstancesActivesCommissionEuropeenne::isCuivre)
                .toList()
                .stream()
                .map(RefSubstancesActivesCommissionEuropeenne::getId_sa)
                .toList();

        List<RefCompositionSubstancesActivesParNumeroAMM> dose = domainSubstancesByAmm.getOrDefault(codeAmm, List.of())
                .stream()
                .filter(ref -> idSaCopper.contains(ref.getId_sa())).
                toList();
        return dose;
    }

    @Override
    protected void pushValuesToContext(PerformanceInterventionContext interventionContext, Double tsaWithTS, Double tsaWithoutTS) {
        interventionContext.setPhytoCuQuantity(tsaWithTS);
        interventionContext.setPhytoCuQuantityHts(tsaWithoutTS);
    }

    @Override
    protected void pushValuesToContext(WriterContext writerContext, PerformanceInterventionContext interventionContext, boolean isSeedingTreatment, AbstractAction action, AbstractPhytoProductInputUsage usage, Double quantity) {
        interventionContext.addUsagePhytoCuQuantity(action, usage, quantity);
        if (!isSeedingTreatment) {
            interventionContext.addUsagePhytoCuQuantityHts(action, usage, quantity);
        }
    }

    @Override
    protected String[] getIndicators() {
        return indicators;
    }
}
