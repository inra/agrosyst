package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Temps de travail Manuel.
 * <p>
 * Le temps de travail manuel est exprimé en h/ha. Il est calculé pour les interventions manuelles (i.e. qui ne mobilisent pas de combinaison d’outils).
 * <p>
 * Il est calculé suivant la formule suivante :
 * 𝑇𝑒𝑚𝑝𝑠 𝑑𝑒 𝑡𝑟𝑎𝑣𝑎𝑖𝑙 𝑚𝑎𝑛𝑢𝑒𝑙 𝑖 = 𝑃𝑆𝐶𝑖×𝐷é𝑏𝑖𝑡 𝑑𝑒 𝑐ℎ𝑎𝑛𝑡𝑖𝑒𝑟 𝑖 × 𝑁𝑜𝑚𝑏𝑟𝑒 𝑑𝑒 𝑝𝑒𝑟𝑠𝑜𝑛𝑛𝑒𝑠 𝑖𝑛𝑡𝑒𝑟𝑣𝑒𝑛𝑎𝑛𝑡 𝑖
 * <p>
 * Avec :
 * 𝑇𝑒𝑚𝑝𝑠 𝑑𝑒 𝑡𝑟𝑎𝑣𝑎𝑖𝑙 𝑚𝑎𝑛𝑢𝑒𝑙 𝑖 (h/ha) : Temps de travail manuel de l’intervention i.
 * 𝑁𝑜𝑚𝑏𝑟𝑒 𝑑𝑒 𝑝𝑒𝑟𝑠𝑜𝑛𝑛𝑒𝑠 𝑖𝑛𝑡𝑒𝑟𝑣𝑒𝑛𝑎𝑛𝑡 𝑖 (h/ha) : Nombre de personnes mobilisées pour l’intervention i. Donnée saisie par l’utilisateur.
 * 𝑃𝑆𝐶𝑖 (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i. PSCi est calculé sur la base de données saisies par l’utilisateur.
 * 𝐷é𝑏𝑖𝑡 𝑑𝑒 𝑐ℎ𝑎𝑛𝑡𝑖𝑒𝑟 𝑖 (h/ha) : Débit de chantier de l’intervention i. Donnée saisie par l’utilisateur.
 *
 * @author Yannick Martel (martel@codelutin.com)
 */
public class IndicatorManualWorkTime extends AbstractIndicator {

    private static final Log LOGGER = LogFactory.getLog(IndicatorManualWorkTime.class);
    public static final String COLUMN_NAME = "temps_travail_manuel";
    // see #10060 for the new default values
    public static final double DEFAULT_WORK_RATE_VALUE = 0d;
    public static final double DEFAULT_INVOLVED_PEOPLE_NUMBER_VALUE = 1d;

    private boolean detailedByMonth = true;
    protected String[] translatedMonth;

    private final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    public IndicatorManualWorkTime() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, COLUMN_NAME + "_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, COLUMN_NAME + "_detail_champs_non_renseig");
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_SOCIO_TECHNIC);
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) &&
                displayed &&
                (detailedByMonth || i == NB_MONTH);// 12 months + sum
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorLabel(int i) {
        String result;
        if (0 <= i && i < NB_MONTH) {// 12 months + sum
            result = l(locale, "Indicator.label.manualWorkTimeMonth", translatedMonth[i]);
        } else {
            result = l(locale, "Indicator.label.manualWorkTime");
        }

        return result;
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();

        for (int i = 0; i <= NB_MONTH; i++) {
            String label = getIndicatorLabel(i);
            String dbColumnName;
            if (i < NB_MONTH) { // 12 months + sum
                dbColumnName = COLUMN_NAME + "_" + MONTHS_FOR_DB_COLUMNS[i];
            } else {
                dbColumnName = COLUMN_NAME;
            }
            indicatorNameToColumnName.put(label, dbColumnName);
        }
        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {

        if (interventionContext.isFictive()) return newArray(NB_MONTH + 1, 0.0); // 12 months + sum

        double totalWorkTime;

        PracticedIntervention intervention = interventionContext.getIntervention();
        ToolsCoupling toolsCoupling = interventionContext.getToolsCoupling();
        // not iso with other work rate request but we do not need tools coupling so we don't want to load them if not needed
        toolsCoupling = intervention.getWorkRate() != null ? toolsCoupling : null;

        if (toolsCoupling == null || toolsCoupling.isManualIntervention()) {

            Pair<Double, MaterielWorkRateUnit> workRate = getPracticedInterventionWorkRate(
                    domainContext,
                    intervention,
                    null,
                    DEFAULT_WORK_RATE_VALUE,
                    MaterielWorkRateUnit.H_HA
            );

            // No tools coupling = manual work, or toolsCoupling as manual intervention
            double toolPSCi = getToolPSCi(intervention);

            // InvolvedPeopleNumber (default value is tools coupling work force)
            Double involvedPeopleNumber = getPracticedInterventionInvolvedPeopleNumber(intervention, DEFAULT_INVOLVED_PEOPLE_NUMBER_VALUE);

            double workRateValue = getWorkRateValue_H_HA_OrDefault(
                    workRate,
                    null,
                    null,
                    null,
                    DEFAULT_WORK_RATE_VALUE
            );

            totalWorkTime = toolPSCi * workRateValue * involvedPeopleNumber;

        } else {
            totalWorkTime = 0.0d;
        }

        interventionContext.setManualWorkTime(totalWorkTime);

        return newResult(getMonthsRatio(intervention, totalWorkTime));

    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {

        Double[] workTimes = null;

        EffectiveIntervention intervention = interventionContext.getIntervention();
        // workRate
        incrementAngGetTotalFieldCounterForTargetedId(intervention.getTopiaId());

        Pair<Double, MaterielWorkRateUnit> workRate = getEffectiveInterventionWorkRate(
                domainContext,
                interventionContext,
                DEFAULT_WORK_RATE_VALUE,
                MaterielWorkRateUnit.H_HA
        );

        ToolsCoupling toolsCoupling = interventionContext.getToolsCoupling();

        if (toolsCoupling == null || toolsCoupling.isManualIntervention()) {
            // No tools coupling, manual work !
            double toolPSCi = getToolPSCi(intervention);

            Double involvedPeople = getEffectiveInterventionInvolvedPeopleCount(intervention, DEFAULT_INVOLVED_PEOPLE_NUMBER_VALUE);

            double workRateValue = getWorkRateValue_H_HA_OrDefault(
                    workRate,
                    null,
                    null,
                    null,
                    DEFAULT_WORK_RATE_VALUE
            );

            double totalWorkTime = toolPSCi * workRateValue * involvedPeople;

            workTimes = newResult(getMonthsRatio(intervention, totalWorkTime));

            interventionContext.setManualWorkTime(totalWorkTime);
        }

        return workTimes;
    }

    public void init(IndicatorFilter filter, String[] translatedMonth) {
        displayed = filter != null;
        detailedByMonth = displayed && BooleanUtils.isTrue(filter.getDetailedByMonth());
        this.translatedMonth = translatedMonth;
    }

}
