package fr.inra.agrosyst.services.edaplos;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingEntryType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryImpl;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInputImpl;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInputImpl;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInputImpl;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInputImpl;
import fr.inra.agrosyst.api.entities.DomainSeedLotInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInputImpl;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.EquipmentImpl;
import fr.inra.agrosyst.api.entities.EquipmentTopiaDao;
import fr.inra.agrosyst.api.entities.Ground;
import fr.inra.agrosyst.api.entities.GroundTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.HarvestingPriceImpl;
import fr.inra.agrosyst.api.entities.HarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputPriceImpl;
import fr.inra.agrosyst.api.entities.InputPriceTopiaDao;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PlotImpl;
import fr.inra.agrosyst.api.entities.PlotTopiaDao;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.SeedPrice;
import fr.inra.agrosyst.api.entities.SeedPriceTopiaDao;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.ToolsCouplingTopiaDao;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneImpl;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.ZoneType;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.BiologicalControlActionImpl;
import fr.inra.agrosyst.api.entities.action.BiologicalControlActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsageImpl;
import fr.inra.agrosyst.api.entities.action.CarriageAction;
import fr.inra.agrosyst.api.entities.action.CarriageActionImpl;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionImpl;
import fr.inra.agrosyst.api.entities.action.HarvestingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisationImpl;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisationTopiaDao;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.IrrigationActionImpl;
import fr.inra.agrosyst.api.entities.action.IrrigationActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.IrrigationInputUsage;
import fr.inra.agrosyst.api.entities.action.MaintenancePruningVinesAction;
import fr.inra.agrosyst.api.entities.action.MaintenancePruningVinesActionImpl;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingActionImpl;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsageImpl;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingActionImpl;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsageImpl;
import fr.inra.agrosyst.api.entities.action.OtherAction;
import fr.inra.agrosyst.api.entities.action.OtherActionImpl;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsageImpl;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingActionImpl;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.PhytoProductTarget;
import fr.inra.agrosyst.api.entities.action.PhytoProductTargetTopiaDao;
import fr.inra.agrosyst.api.entities.action.QualityCriteria;
import fr.inra.agrosyst.api.entities.action.QualityCriteriaImpl;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsageImpl;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsageImpl;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsageImpl;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsageImpl;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.TillageAction;
import fr.inra.agrosyst.api.entities.action.TillageActionImpl;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnection;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnectionImpl;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNodeImpl;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhaseImpl;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleImpl;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleImpl;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCategTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefAdventiceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSolTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEdaplosTypeTraitement;
import fr.inra.agrosyst.api.entities.referential.RefEdaplosTypeTraitementTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceOtherTools;
import fr.inra.agrosyst.api.entities.referential.RefEspeceOtherToolsTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspeceToVariete;
import fr.inra.agrosyst.api.entities.referential.RefEspeceToVarieteTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFAImpl;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrgaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionTypeItemInputEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionTypeItemInputEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefOtherInputTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefPotTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalisTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSector;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSectorTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefSubstrateTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGeves;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGevesTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrapeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.TypeCulture;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRootTopiaDao;
import fr.inra.agrosyst.api.services.action.ActionService;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.edaplos.EdaplosParsingResult;
import fr.inra.agrosyst.api.services.edaplos.EdaplosService;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.services.action.ActionServiceImpl;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.edaplos.EdaplosConstants.UnitCode;
import fr.inra.agrosyst.services.edaplos.model.AgriculturalArea;
import fr.inra.agrosyst.services.edaplos.model.AgriculturalCrop;
import fr.inra.agrosyst.services.edaplos.model.AgriculturalCropProductionCycle;
import fr.inra.agrosyst.services.edaplos.model.AgriculturalPlot;
import fr.inra.agrosyst.services.edaplos.model.AgriculturalProcessCropInput;
import fr.inra.agrosyst.services.edaplos.model.AgriculturalProcessCropStage;
import fr.inra.agrosyst.services.edaplos.model.AgriculturalProcessReason;
import fr.inra.agrosyst.services.edaplos.model.AgriculturalProcessTargetObject;
import fr.inra.agrosyst.services.edaplos.model.AgriculturalProcessWorkItem;
import fr.inra.agrosyst.services.edaplos.model.AgriculturalProduce;
import fr.inra.agrosyst.services.edaplos.model.CropDataSheetDocument;
import fr.inra.agrosyst.services.edaplos.model.CropDataSheetMessage;
import fr.inra.agrosyst.services.edaplos.model.CropDataSheetMessages;
import fr.inra.agrosyst.services.edaplos.model.CropDataSheetParty;
import fr.inra.agrosyst.services.edaplos.model.CropInputChemical;
import fr.inra.agrosyst.services.edaplos.model.CropSpeciesVariety;
import fr.inra.agrosyst.services.edaplos.model.DelimitedPeriod;
import fr.inra.agrosyst.services.edaplos.model.PartyContact;
import fr.inra.agrosyst.services.edaplos.model.PlotAgriculturalProcess;
import fr.inra.agrosyst.services.edaplos.model.PlotSoilOccupation;
import fr.inra.agrosyst.services.edaplos.model.ProductionSoftware;
import fr.inra.agrosyst.services.edaplos.model.SpecifiedAgriculturalDevice;
import fr.inra.agrosyst.services.edaplos.model.SpecifiedGeographicalCoordinate;
import fr.inra.agrosyst.services.edaplos.model.TechnicalCharacteristic;
import fr.inra.agrosyst.services.edaplos.model.UnstructuredAddress;
import fr.inra.agrosyst.services.input.InputUsageServiceImpl;
import fr.inra.agrosyst.services.referential.ReferentialServiceImpl;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math3.util.Precision;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;

import java.lang.reflect.InvocationTargetException;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import static fr.inra.agrosyst.api.entities.AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES;
import static fr.inra.agrosyst.api.entities.AgrosystInterventionType.SEMIS;
import static fr.inra.agrosyst.services.action.ActionServiceImpl.TYPES_ALLOWING_3_ACTIONS;

/**
 * Parser dom d'un document eDaplos.
 *
 * @author Eric Chatellier, eancelet
 * TODO : assurer la filiation des domaines et parcelles (avec ce qui existe déjà en base et ce qui est importé lors du même import)
 * TODO : assurer la filiation des cultures ?
 * TODO : remonter un message d'erreur si la parcelle trouvée a plus d'une zone
 * TODO : persister le eDaplosIssuerId si la parcelle est retrouvée avec le nom + l'air
 * TODO : ajouter un responsable domaine / responsable dispositif ?
 * TODO : modifier le comportement du découpage de parcelle : ne pas copier le eDaplosIssuerId
 * TODO : ajouter id du domaine + campagne dans tous les messages d'erreur (sera utile lors de l'import de plusieurs fichiers en même temps)
 * TODO : vérifier les droits sur les dispositifs
 * TODO eancelet 2016-02-01 : enlever les "continue;" et se débrouiller autrement
 * TODO eancelet 2016-05-03 : pour déterminer la nature du cycle (pérenne ou assolé), tester toutes les espèces d'une culture et remonter une erreur s'il y a incohérence
 * TODO eancelet 2016-05-27 : test prenant en entrée un xml et non un zip
 * TODO eancelet 2016-05-31 : vérifier avec les partenaires s'il n'y a pas confusion entre AgriculturalPlot.Identification (id)
 *                              et AgriculturalPlot.IssuerInternalReference et AgriculturalPlot.Description (nom)
 * TODO eancelet 2016-06-02 : tester les droits des rôles d'import
 * TODO eancelet 2016-06-02 : pouvoir faire en sorte que les noeuds et connexions puissent être créées
 *                              dans n'importe quel ordre (la validation doit être à la fin)
 * TODO eancelet 2016-06-02 : compléter les caractéristiques de la zone ?
 * TODO eancelet 2016-06-06 : voir si le status d'update pour une parcelle est pertinent car finallement rien n'est mis à jour si la parcelle est retrouvée
 * TODO eancelet 2016-06-06 : faire des tests avec des noeuds qui changent, des inter qui s'ajoutent, des noeuds qui s'ajoutent etc.
 * TODO eancelet 2016-06-06 : ajouter en base des contraintes uniques sur les edaplosissuerID ?
 */
@Setter
public class EdaplosPersister {

    protected static final Log LOGGER = LogFactory.getLog(EdaplosPersister.class);
    protected static final String DEFAULT_SPECIES_BOTANICAL_CODE = "ZBO";
    protected static final int DEFAUlT_TRANSIT_COUNT = 1;
    protected static final String ORGANIC_TYPE_CODE = "W16";
    protected static final String PHYTO_TYPE_CODE = "SEX";
    protected static final String PARIS_DEFAULT_POST_CODE = "75008";
    protected static final double SEEDING_ACTION_SPECIES_DEFAULT_QUANTITY = 999.0;
    protected static final String GENERIC_AGRESSOR_ID = "000000AEE021";
    protected static final String SIRET_A_COMPLETER = "siret_a_compléter_";
    protected double DEFAULT_BOILED_QUANTITY = 9999d;
    protected double DEFAULT_PROPORTION_OF_TREATED_SURFACE = 100d;

    protected static String defaultMineralProductName = "Produit mineral";
    protected static String defaultOrganicProductName = "Engrais organique";

    protected RefActaTraitementsProduit defaultRefActaTraitementsProduit;
    protected RefOtherInput defaultRefOtherInput;
    protected RefFertiOrga defaultRefFertiOrga;
    protected RefSubstrate defaultRefSubstrate;
    protected RefPot defaultRefPot;

    protected String defaultEquipmentCode;
    protected String defaultIrrigationCode;
    protected String defaultTractorCode;
    protected String defaultAutomoteurCode;

    protected Random randomGenerator = new Random();
    protected EdaplosContext globalContext;

    protected AbstractActionTopiaDao abstractActionDao;
    protected BiologicalControlActionTopiaDao biologicalControlActionDao;
    protected DomainSeedLotInputTopiaDao domainSeedLotInputDao;

    protected AgrosystI18nService i18nService;
    protected BusinessAuthorizationService authorizationService;
    protected DomainService domainService;
    protected DomainInputStockUnitService domainInputStockUnitService;
    protected EdaplosService edaplosService;
    protected InputPriceService inputPriceService;
    protected InputUsageServiceImpl inputUsageService;
    protected PlotService plotService;
    protected PricesService pricesService;
    protected ReferentialService referentialService;

    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    protected DomainTopiaDao domainTopiaDao;

    protected EffectiveCropCycleConnectionTopiaDao effectiveCropCycleConnectionTopiaDao;
    protected EffectiveCropCycleNodeTopiaDao effectiveCropCycleNodeTopiaDao;
    protected EffectiveCropCyclePhaseTopiaDao effectiveCropCyclePhaseTopiaDao;
    protected EffectiveCropCycleService effectiveCropCycleService;
    protected EffectiveInterventionTopiaDao effectiveInterventionDao;
    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleDao;
    protected EffectiveSeasonalCropCycleTopiaDao effectiveSeasonalCropCycleDao;
    protected EffectiveSpeciesStadeTopiaDao effectiveSpeciesStadeDao;
    protected EquipmentTopiaDao equipmentDao;
    protected GroundTopiaDao groundDao;
    protected HarvestingActionTopiaDao harvestingActionDao;
    protected HarvestingActionValorisationTopiaDao harvestingActionValorisationDao;
    protected IrrigationActionTopiaDao irrigationActionDao;
    protected InputPriceTopiaDao inputPriceDao;
    protected MineralFertilizersSpreadingActionTopiaDao mineralFertilizersSpreadingActionDao;
    protected OrganicFertilizersSpreadingActionTopiaDao organicFertilizersSpreadingActionsDao;
    protected PesticidesSpreadingActionTopiaDao pesticidesSpreadingActionDao;
    protected PhytoProductTargetTopiaDao phytoProductTargetDao;
    protected PlotTopiaDao plotDao;
    protected HarvestingPriceTopiaDao harvestingPriceDao;
    protected RefActaProduitRootTopiaDao refActaProduitRootDao;
    protected RefActaTraitementsProduitsCategTopiaDao refActaTraitementsProduitsCategDao;
    protected RefActaTraitementsProduitTopiaDao refActaTraitementsProduitsDao;
    protected RefAdventiceTopiaDao refAdventiceDao;
    protected RefCultureEdiGroupeCouvSolTopiaDao refCultureEdiGroupeCouvSolDao;
    protected RefDestinationTopiaDao refDestinationDao;
    protected RefEdaplosTypeTraitementTopiaDao refEdaplosTypeTraitementDao;

    protected RefEspeceOtherToolsTopiaDao refEspeceOtherToolsDao;
    protected RefEspeceTopiaDao refEspeceDao;
    protected RefEspeceToVarieteTopiaDao refEspeceToVarieteDao;
    protected RefFertiMinUNIFATopiaDao refFertiMinUNIFADao;
    protected RefFertiOrgaTopiaDao refFertiOrgaDao;
    protected RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEDIDao;
    protected RefInterventionTypeItemInputEDITopiaDao refInterventionTypeItemInputEDIDao;
    protected RefLocationTopiaDao locationDao;
    protected RefMaterielTopiaDao refMaterielDao;
    protected RefNuisibleEDITopiaDao refNuisibleEDIDao;
    protected RefOtherInputTopiaDao refOtherInputDao;
    protected RefPotTopiaDao refPotDao;
    protected RefQualityCriteriaTopiaDao refQualityCriteriaDao;
    protected RefSolArvalisTopiaDao refSolArvalisDao;
    protected RefSpeciesToSectorTopiaDao refSpeciesToSectorDao;
    protected RefStadeEDITopiaDao refStadeEDIDao;
    protected RefSubstrateTopiaDao refSubstrateDao;
    protected RefVarieteGevesTopiaDao refVarieteGevesDao;
    protected RefVarietePlantGrapeTopiaDao refVarietePlantGrapeDao;
    protected SeedingActionUsageTopiaDao seedingActionUsageDao;
    protected SeedPriceTopiaDao seedPriceDao;
    protected ToolsCouplingTopiaDao toolsCouplingDao;
    protected ZoneTopiaDao zoneDao;

    Function<String, String> GET_NORMALIZED_SIRET = StringUtils::trimToEmpty;

    protected void importDefaultReferential() {
        defaultRefActaTraitementsProduit = refActaTraitementsProduitsDao.forTopiaIdEquals(ReferentialService.DEFAULT_REF_ACTA_TRAITEMENTS_PRODUIT_ID).findUniqueOrNull();
        defaultRefOtherInput = refOtherInputDao.forTopiaIdEquals(ReferentialService.DEFAULT_REF_OTHER_INPUT_ID).findUniqueOrNull();
        defaultRefFertiOrga = refFertiOrgaDao.forTopiaIdEquals(ReferentialService.DEFAULT_REF_FERTI_ORGA_ID).findAnyOrNull();
        defaultRefSubstrate = refSubstrateDao.forTopiaIdEquals(ReferentialService.DEFAULT_REF_SUBSTRAT_ID).findAnyOrNull();
        defaultRefPot = refPotDao.forTopiaIdEquals(ReferentialService.DEFAULT_REF_POT_ID).findAnyOrNull();
    }

    protected String formatDomain(Domain domain) {
        return domain.getName() + " - " + domain.getCampaign() + " (" + domain.getSiret() + ")";
    }

    protected String formatDomain(EdaplosContext context) {
        return formatDomain(context.getDomain());
    }

    protected String formatPlot(Plot plot) {
        return plot.getName() + " (" + plot.getArea() + " ha)";
    }

    protected String formatPlot(EdaplosContext context) {
        return formatPlot(context.getPlot());
    }

    protected String formatIntervention(EffectiveIntervention intervention) {
        String result = intervention.getName();
        if (intervention.getStartInterventionDate() != null) {
            result += " (" + EdaplosUtils.formatInterventionDate(intervention.getStartInterventionDate()) + ")";
        }
        return result;
    }

    protected String formatIntervention(EdaplosContext context) {
        return formatIntervention(context.getEffectiveIntervention());
    }

    protected String formatSolOccupation(EdaplosContext context) {
        return context.getSoilOccupation();
    }

    protected String formatCrop(EdaplosContext context) {
        return context.getCroppingPlanEntry().getName();
    }

    protected String formatInput(AgriculturalProcessCropInput input) {
        String result;
        if (StringUtils.isNotBlank(input.getDescription())) {
            result = input.getDescription() + " (" + input.getIdentification() + ")";
        } else if (StringUtils.isNotBlank(input.getIdentification())) {
            result = input.getIdentification();
        } else {
            result = input.getTypeCode();
        }
        return result;
    }

    /**
     * Process document
     * TODO eancelet 2016-05-02 : à compléter
     * An eDaplos document is build like this :
     *  Root Metadata (containing domain + campaign ?)
     *      Domains
     *          Plots
     *            Occupation du sol (qui correspondent à xxxx dans Agrosyst)
     *               ...
     * Les erreurs sont remontées de la balise la plus imbriquée à la racine (exemple : ajout d'une
     * parcelle à un domaine, le status de la parcelle change celui du domaine).
     */
    public void persist(EdaplosParsingResult r, String defaultEquipmentCode, String defaultIrrigationCode, String defaultTractorCode, String defaultAutomoteurCode, CropDataSheetMessages cropDataSheetMessages) {

        Map<SeedType, String> seedTypeTranslations = i18nService.getEnumTranslationMap(SeedType.class);

        globalContext = new EdaplosContext(seedTypeTranslations, r);

        EdaplosContext globalContext = this.globalContext;

        domainSecurityValidation(cropDataSheetMessages, globalContext);

        this.defaultEquipmentCode = defaultEquipmentCode;
        this.defaultIrrigationCode = defaultIrrigationCode;
        this.defaultTractorCode = defaultTractorCode;
        this.defaultAutomoteurCode = defaultAutomoteurCode;

        Set<String> issuerIds = effectiveInterventionDao.findAllIssuerIds();
        globalContext.setIssuerIds(issuerIds);

        for (CropDataSheetMessage cropDataSheetMessage : cropDataSheetMessages.getCropDataSheetMessages()) {

            List<CropDataSheetDocument> cropDataSheetDocuments = cropDataSheetMessage.getCropDataSheetDocuments();
            if (CollectionUtils.isEmpty(cropDataSheetDocuments)) {
                break;
            }

            for (CropDataSheetDocument metadataElement : cropDataSheetDocuments) {

                String documentId = metadataElement.getIdentification();

                // get message issuer Info (= domain)
                CropDataSheetParty issuerCropDataSheetParty = metadataElement.getIssuerCropDataSheetParty();
                String siret = null;
                String domainName = null;
                String mainContact = null;
                RefLocation location = null;
                if (issuerCropDataSheetParty != null) {
                    siret = issuerCropDataSheetParty.getIdentification();
                    if (StringUtils.isBlank(siret)) {
                        // fake siret is given so errors are displayed
                        int suffix = globalContext.getNextSuffix();
                        siret = SIRET_A_COMPLETER + suffix;
                        // break;
                    }

                    domainName = issuerCropDataSheetParty.getName();
                    if (StringUtils.isBlank(domainName)) {
                        globalContext.formatInfo("Dans le document '%s', le nom de l'émetteur n'est pas fourni, le siret '%s' du domaine sera utilisé.",
                                documentId, siret);
                        domainName = siret;
                    }

                    // localisation
                    UnstructuredAddress address = issuerCropDataSheetParty.getSpecifiedUnstructuredAddress();
                    if (address == null) {
                        globalContext.formatWarning("Aucune information relative à la localisation du domaine '%s (%s)'. Par défaut, la localisation sera Paris (75008).",
                                domainName, siret);
                    } else {
                        String cityID = address.getCityID();
                        String cityName = address.getCityName();
                        String postCode = address.getPostcodeCode();

                        location = locationDao.forCodeInseeEquals(cityID).findUniqueOrNull();
                        if (location == null) {
                            // first try to find the exact same city otherwise try to find one from postcode
                            location = edaplosService.searchCommune(postCode, cityName);
                        }

                        if (location == null) {
                            List<RefLocation> locations = locationDao.forCodePostalEquals(postCode).findAll();
                            if (!locations.isEmpty()) {
                                location = locations.getFirst();
                            }
                            if (locations.size() > 1) {
                                globalContext.formatWarning("Le code postal '%s' correspond à plusieurs communes. '%s' a été sélectionné par défaut.",
                                        postCode, location.getCommune());
                            }
                        }

                        if (location == null) {
                            globalContext.formatWarning("Le nom de la commune %s, son code postal %s et son code INSEE %s ne " +
                                            "permettent pas de retrouver son emplacement. Par défaut la localisation sera Paris (75008)",
                                    cityName, postCode, cityID);
                        }
                    }

                    // Default location : Paris (75008)
                    if (location == null) {
                        location = locationDao.forCodePostalEquals(PARIS_DEFAULT_POST_CODE).findAnyOrNull();
                    }

                    PartyContact contact = issuerCropDataSheetParty.getDefinedPartyContact();
                    if (contact == null) {
                        mainContact = "À compléter";
                        globalContext.formatWarning("Aucune information trouvée concernant l'interlocuteur du domaine '%s (%s)'. Son nom sera '%s'",
                                domainName, siret, mainContact);
                    } else {
                        mainContact = contact.getPersonName(); // eancelet 2015-12-01 : optional field in Agrosyst
                        if (StringUtils.isBlank(mainContact)) {
                            mainContact = "À compléter";
                            globalContext.formatWarning("Les informations concernant l'interlocuteur du domaine '%s (%s)' ne peuvent être retrouvées. Le nom de l'interlocuteur sera '%s'",
                                    domainName, siret, mainContact);
                        }
                    }
                }

                // get plots info to get campaigns. There can be several plots per message
                List<AgriculturalPlot> plotElements = cropDataSheetMessage.getAgriculturalPlots();

                // if no plots in the message, then error
                if (CollectionUtils.isEmpty(plotElements)) {
                    globalContext.formatError("Aucune culture n'est déclarée sur le domaine '%s (%s)'. Aucune information ne sera ajoutée",
                            domainName, siret);
                } else {
                    // check campaign format and store plots by campaign in a Map
                    // if the campaign format is not the good one then CAMPAIGN_ERSATZ is used to go on parsing
                    Map<Integer, List<AgriculturalPlot>> mapCampaignPlots = new HashMap<>();
                    for (AgriculturalPlot plotElement : plotElements) {
                        Integer campaign = getPlotCampaign(globalContext, siret, domainName, plotElement);
                        List<AgriculturalPlot> plotList = mapCampaignPlots.computeIfAbsent(campaign, k -> new ArrayList<>());
                        plotList.add(plotElement);
                    }

                    if (MapUtils.isEmpty(mapCampaignPlots)) { // si aucune campagne détectée
                        globalContext.formatError("Aucune campagne n'est définie pour le domaine '%s', de siret '%s'",
                                domainName, siret);
                    } else {
                        // parsing plots
                        // First check that there is not twice the same edaplosIssuerId
                        List<String> issuerIdsToImport = mapCampaignPlots.values().stream()
                                .flatMap(Collection::stream)
                                .flatMap(plotElement -> plotElement.getAppliedPlotSoilOccupations().stream())
                                .filter(Objects::nonNull)
                                .flatMap(soilOccupation -> soilOccupation.getSpecifiedAgriculturalCropProductionCycles().stream())
                                .filter(Objects::nonNull)
                                .flatMap(prodCycle -> prodCycle.getApplicablePlotAgriculturalProcesss().stream())
                                .filter(Objects::nonNull)
                                .map(PlotAgriculturalProcess::getIdentification)
                                .filter(Objects::nonNull)
                                .collect(Collectors.toList());

                        HashSet<String> uniqueIssuers = new HashSet<>(issuerIdsToImport);
                        if (uniqueIssuers.size() != issuerIdsToImport.size()) {
                            for (String id : uniqueIssuers) {
                                issuerIdsToImport.remove(id);
                            }
                            String doublons = issuerIdsToImport.stream().sorted().map(Object::toString).collect(Collectors.joining(", "));
                            globalContext.formatError("Les identifiants eDaplos suivants et correspondant à des interventions ne sont pas uniques dans le fichier d'import : %s. Cela n'est pas autorisé dans le format eDaplos, merci de contacter le support de votre outil d'export eDaplos.",
                                    doublons);
                        } else {
                            // Then check domains as we now know the domain campaigns
                            for (Map.Entry<Integer, List<AgriculturalPlot>> campaignPlots : mapCampaignPlots.entrySet()) {
                                Integer studiedCampaign = campaignPlots.getKey();
                                globalContext.addStudiedCampaign(studiedCampaign);

                                Map<InputType, List<AbstractDomainInputStockUnit>> domainInputsByTypes;

                                Domain domain = domainTopiaDao.forSiretEquals(GET_NORMALIZED_SIRET.apply(siret))
                                        .addEquals(Domain.PROPERTY_CAMPAIGN, studiedCampaign)
                                        .findAnyOrNull();

                                if (domain != null) {
                                    domainInputsByTypes = domainInputStockUnitService.loadDomainInputStock(domain);
                                    globalContext.formatInfo("Le domaine '%s (%s)' figure déjà dans Agrosyst pour la campagne '%d' sous le nom '%s'. Les nouvelles données seront ajoutées à celles existantes.",
                                            domainName, siret, studiedCampaign, domain.getName());
                                } else {
                                    globalContext.formatError("Aucun domaine avec ce numéro SIRET: %s n'a été trouvé pour la campagne %d", siret, studiedCampaign);
                                    // create to be able to continue but import wil failed
                                    // domain is to be created from scratch
                                    domain = domainService.newDomain();
                                    domain.setSiret(StringUtils.trimToNull(GET_NORMALIZED_SIRET.apply(siret)));
                                    domain.setMainContact(mainContact);
                                    domain.setCampaign(studiedCampaign);
                                    domain.setName(domainName);
                                    domain.setLocation(location);
                                    domain.setUpdateDate(LocalDateTime.now());
                                    // eancelet 2014-12-03 : all created domains are of type "Exploitation Agricole"
                                    DomainType domainType = DomainType.EXPLOITATION_AGRICOLE;
                                    domain.setType(domainType);
                                    domain.setCode(UUID.randomUUID().toString());
                                    domain = domainTopiaDao.create(domain);

                                    domainInputsByTypes = new HashMap<>();

                                }

                                EdaplosContext domainContext = globalContext.ofDomain(domain, domainInputsByTypes);

                                processPlotImport(domainContext, campaignPlots.getValue());
                            }
                        }
                    }
                }

                saveImportLog(this.globalContext.result, metadataElement);
            }
        }
    }

    protected void domainSecurityValidation(CropDataSheetMessages cropDataSheetMessages, EdaplosContext context) {
        for (CropDataSheetMessage cropDataSheetMessage : cropDataSheetMessages.getCropDataSheetMessages()) {
            List<CropDataSheetDocument> cropDataSheetDocuments = cropDataSheetMessage.getCropDataSheetDocuments();
            if (CollectionUtils.isEmpty(cropDataSheetDocuments)) {
                break;
            }
            for (CropDataSheetDocument metadataElement : cropDataSheetDocuments) {

                CropDataSheetParty issuerCropDataSheetParty = metadataElement.getIssuerCropDataSheetParty();

                String siret = SIRET_A_COMPLETER;
                if (issuerCropDataSheetParty != null) {
                    siret = issuerCropDataSheetParty.getIdentification();
                }

                List<AgriculturalPlot> plotElements = cropDataSheetMessage.getAgriculturalPlots();
                Map<Integer, List<AgriculturalPlot>> mapCampaignPlots = new HashMap<>();
                for (AgriculturalPlot plotElement : plotElements) {
                    Integer campaign = getPlotCampaign(plotElement);
                    List<AgriculturalPlot> plotList = mapCampaignPlots.computeIfAbsent(campaign, k -> new ArrayList<>());
                    plotList.add(plotElement);
                }
                for (Map.Entry<Integer, List<AgriculturalPlot>> campaignPlots : mapCampaignPlots.entrySet()) {
                    Integer studiedCampaign = campaignPlots.getKey();
                    Domain domain = null;

                    if (!siret.contentEquals(SIRET_A_COMPLETER)) {
                        domain = domainTopiaDao.forSiretEquals(GET_NORMALIZED_SIRET.apply(siret)).addEquals(Domain.PROPERTY_CAMPAIGN, studiedCampaign).findAnyOrNull();
                    }

                    if (domain == null) {
                        domain = domainTopiaDao.forSiretEquals(GET_NORMALIZED_SIRET.apply(siret)).findAnyOrNull();
                    }

                    if (domain != null) {
                        // security
                        if (!authorizationService.isDomainWritable(domain.getTopiaId())) {
                            context.formatError("Vous n'avez pas les droits d'écriture sur le domaine '%s' pour la campagne '%d'",
                                    formatDomain(domain), studiedCampaign);
                        }
                    }
                }

            }

        }
    }

    protected Integer getPlotCampaign(EdaplosContext context, String siret, String domainName, AgriculturalPlot plotElement) {
        int campaign = 9999;
        String harvestedYear = plotElement.getHarvestedYear();
        if (StringUtils.isNotBlank(harvestedYear)) {
            try {
                if (!CommonService.getInstance().areCampaignsValids(harvestedYear)) {
                    context.formatError("Format non valide pour la campagne du domaine '%s (%s)'. Il sera remplacé par '%d' pour la suite de l'analyse du fichier",
                            domainName, siret, campaign);
                } else {
                    campaign = Integer.parseInt(harvestedYear);
                }
            } catch (NumberFormatException e) {
                context.formatError("Le format de la campagne '%s' pour le domaine '%s (%s)' n'est pas valide. Il sera remplacé par '%d' pour la suite de l'analyse du fichier",
                        harvestedYear, domainName, siret, campaign);
            }
        } else {
            context.formatError("Aucune campagne déclarée pour la parcelle '%s', domaine '%s (%s)'",
                    plotElement.getIdentification(), domainName, siret);
        }
        return campaign;
    }

    protected Integer getPlotCampaign(AgriculturalPlot plotElement) {
        int campaign = 9999;
        String harvestedYear = plotElement.getHarvestedYear();
        if (StringUtils.isNotBlank(harvestedYear)) {
            try {
                if (CommonService.getInstance().areCampaignsValids(harvestedYear)) {
                    campaign = Integer.parseInt(harvestedYear);
                }
            } catch (NumberFormatException e) {
                LOGGER.warn("Campagne non valide:" + harvestedYear);
            }
        }
        return campaign;
    }

    /**
     * Add log entry in table.
     */
    protected void saveImportLog(EdaplosParsingResult result, CropDataSheetDocument cropDataSheetDocument) {
        result.setDocumentIdentification(cropDataSheetDocument.getIdentification());
        result.setDocumentTypeCode(cropDataSheetDocument.getTypeCode());

        CropDataSheetParty issuerCropDataSheetParty = cropDataSheetDocument.getIssuerCropDataSheetParty();
        if (issuerCropDataSheetParty != null) {
            result.setIssuerIdentification(issuerCropDataSheetParty.getIdentification());
            result.setIssuerName(issuerCropDataSheetParty.getName());
        }

        ProductionSoftware usedProductionSoftware = cropDataSheetDocument.getUsedProductionSoftware();
        if (usedProductionSoftware != null) {
            result.setSoftwareName(usedProductionSoftware.getName());
            result.setSoftwareVersion(usedProductionSoftware.getVersion());
        }
    }

    /**
     * Analyse les parcelles
     * Required fields for plot are:
     * - name
     * - domain
     */
    protected void processPlotImport(EdaplosContext domainContext, List<AgriculturalPlot> plotList) {

        Domain domain = domainContext.getDomain();

        for (AgriculturalPlot plotElement : plotList) {
            // Test plot identifier
            String issuerPlotId = plotElement.getIdentification();
            String plotName = plotElement.getDescription();

            // Test plot area
            List<AgriculturalArea> includedAgriculturalAreas = plotElement.getIncludedAgriculturalAreas();
            for (AgriculturalArea includedAgriculturalArea : includedAgriculturalAreas) {
                double plotArea = 0;
                Double latitude = null;
                Double longitude = null;
                // impossible if areatype is wrong or area is null
                if (includedAgriculturalArea == null
                        || (!AgriculturalArea.TYPE_SURFACE_PARCELLE_CULTURALE.equals(includedAgriculturalArea.getTypeCode())
                        && !AgriculturalArea.TYPE_SURFACE_PARCELLE_PERENNE.equals(includedAgriculturalArea.getTypeCode()))
                        || StringUtils.isBlank(includedAgriculturalArea.getActualMeasure())) {
                    domainContext.formatError("La surface de la parcelle '%s (%s)' du domaine '%s' n'est pas valide (doit être un nombre non nul). Impossible de créer la parcelle et de complèter l'itinéraire technique correspondant.",
                            plotName, issuerPlotId, formatDomain(domain));
                } else {
                    try {
                        plotArea = Double.parseDouble(includedAgriculturalArea.getActualMeasure());
                    } catch (NumberFormatException e) {
                        domainContext.formatError("La surface de la parcelle '%s (%s)' du domaine '%s' n'est pas valide (doit être un nombre, pas '%s'). Impossible de créer la parcelle et de complèter l'itinéraire technique correspondant.",
                                plotName, issuerPlotId, formatDomain(domain), includedAgriculturalArea.getActualMeasure());
                    }
                    List<SpecifiedGeographicalCoordinate> specifiedGeographicalCoordinates = includedAgriculturalArea.getSpecifiedGeographicalCoordinates();
                    if (CollectionUtils.isNotEmpty(specifiedGeographicalCoordinates)) {
                        SpecifiedGeographicalCoordinate specifiedGeographicalCoordinate = specifiedGeographicalCoordinates.stream()
                                .filter(c -> SpecifiedGeographicalCoordinate.GEOGRAPHIC_SYSTEM_84.equalsIgnoreCase(c.getSystemID()))
                                .findFirst()
                                .orElse(null);
                        if (specifiedGeographicalCoordinate == null) {
                            domainContext.formatError("Les coordonnées géographiques sont manquantes ou invalides sur la parcelle '%s' (%s), domaine '%s'.",
                                    plotName, issuerPlotId, formatDomain(domain));

                        } else {
                            String latitudeMeasure = specifiedGeographicalCoordinate.getLatitudeMeasure();
                            if (StringUtils.isNotBlank(latitudeMeasure)) {
                                try {
                                    latitude = Double.parseDouble(latitudeMeasure);
                                } catch (NumberFormatException nfe) {
                                    domainContext.formatInfo("Latitude non renseignée/renseignée au mauvais format pour la parcelle '%s (%s)",
                                            plotName, issuerPlotId);
                                }
                            } else {
                                domainContext.formatInfo("Latitude non renseignée/renseignée au mauvais format pour la parcelle '%s (%s)'",
                                        plotName, issuerPlotId);
                            }
                            String longitudeMeasure = specifiedGeographicalCoordinate.getLongitudeMeasure();
                            if (StringUtils.isNotBlank(longitudeMeasure)) {
                                try {
                                    longitude = Double.parseDouble(longitudeMeasure);
                                } catch (NumberFormatException nfe) {
                                    domainContext.formatInfo("Longitude non renseignée/renseignée au mauvais format pour la parcelle '%s (%s)'",
                                            plotName, issuerPlotId);
                                }
                            } else {
                                domainContext.formatInfo("Longitude non renseignée/renseignée au mauvais format pour la parcelle '%s (%s)'",
                                        plotName, issuerPlotId);
                            }
                        }
                    }

                }

                // messages with area
                if (StringUtils.isBlank(issuerPlotId)) {
                    domainContext.formatError("La parcelle de surface '%f' du domaine '%s' n'a pas d'identifiant",
                            plotArea, formatDomain(domain));
                    issuerPlotId = "id_parcelle_a_completer_" + randomGenerator.nextInt(10000);
                }

                // Test plot name
                if (StringUtils.isBlank(plotName)) {
                    domainContext.formatInfo("La parcelle de surface '%f' du domaine '%s' n'a pas de nom.",
                            plotArea, formatDomain(domain));
                    plotName = issuerPlotId;
                }

                // Look for existing plots associated to the domain
                int studiedCampaign = domain.getCampaign();
                String domainName = domain.getName();

                Plot plot = plotDao.forDomainEquals(domain)
                        .addEquals(Plot.PROPERTY_E_DAPLOS_ISSUER_ID, issuerPlotId)
                        .addEquals(Plot.PROPERTY_ACTIVE, true)
                        .findUniqueOrNull();
                Zone zone;

                if (plot == null) {
                    PlotFilter filter = new PlotFilter(domain, plotArea);
                    filter.setActive(true);
                    List<Plot> domainPlots = plotDao.findAllForFilter(filter);

                    String finalPlotName = plotName;
                    domainPlots = domainPlots.stream().filter(plot0 -> isSameDespiteWhitespacesOrAccents(plot0.getName(), finalPlotName)).collect(Collectors.toList());

                    boolean sameAreaPlotFound = !domainPlots.isEmpty();
                    if (!sameAreaPlotFound) {
                        filter.removeAreaFilter();
                        domainPlots = plotDao.findAllForFilter(filter);
                        domainPlots = domainPlots.stream().filter(plot0 -> isSameDespiteWhitespacesOrAccents(plot0.getName(), finalPlotName)).collect(Collectors.toList());
                    }
                    if (domainPlots.size() == 1) {
                        plot = domainPlots.getFirst();
                        if (!sameAreaPlotFound) {
                            domainContext.formatWarning("La parcelle '%s (%s)' existe déjà dans Agrosyst dans le domaine '%s' pour la campagne '%s' mais pour une surface de '%f'ha, la surface va être mise à jour à '%f'ha. Les nouvelles données seront ajoutées à celles existantes.",
                                    plotName, issuerPlotId, formatDomain(domain), studiedCampaign, plot.getArea(), plotArea);
                            plot.setArea(plotArea);
                            //l'identifiant edaplos n'est ajouté que lors de la création d'une parcelle
                            plot = plotDao.update(plot);

                        } else {
                            domainContext.formatInfo("La parcelle '%s (%s)' existe déjà dans Agrosyst dans le domaine '%s' pour la campagne '%s'. Les nouvelles données seront ajoutées à celles existantes.",
                                    plotName, issuerPlotId, formatDomain(domain), studiedCampaign);
                        }
                        zone = zoneDao.forPlotEquals(plot).addEquals(Zone.PROPERTY_TYPE, ZoneType.PRINCIPALE).findAnyOrNull();

                    } else if (domainPlots.size() > 1) {
                        domainContext.formatError("Trop de parcelles avec comme nom '%s' et comme surface '%f' dans le domaine '%s' ! Les informations ne seront pas ajoutées",
                                plotName, plotArea, formatDomain(domain));
                        continue;

                    } else {
                        domainContext.formatInfo("La parcelle '%s (%s ha)' sera créée sur le domaine '%s' pour la campagne '%s'",
                                plotName, plotArea, domainName, studiedCampaign);

                        // create the plot
                        plot = new PlotImpl();
                        plot.setDomain(domain);
                        plot.setName(plotName);
                        if (!PARIS_DEFAULT_POST_CODE.contentEquals(domain.getLocation().getCodePostal())) {
                            plot.setLocation(domain.getLocation());
                        }
                        plot.seteDaplosIssuerId(issuerPlotId);
                        plot.setArea(plotArea);
                        plot.setActive(true);
                        plot.setLatitude(latitude);
                        plot.setLongitude(longitude);
                        plot.setCode(UUID.randomUUID().toString());
                        plot = plotDao.create(plot);

                        // create zone
                        zone = new ZoneImpl();
                        zone.setPlot(plot);
                        zone.setArea(plotArea);
                        zone.setName("Zone principale");
                        zone.setCode(UUID.randomUUID().toString());
                        zone.setLatitude(latitude);
                        zone.setLongitude(longitude);
                        zone.setType(ZoneType.PRINCIPALE);
                        zone.setActive(true);
                        zone = zoneDao.create(zone);
                    }

                } else {

                    GrowingSystem gs = plot.getGrowingSystem();
                    if (gs != null) {
                        // in case plot are liked with growing system we check user permission to write on it and it's growing plan
                        // for domain the validation was previously done.
                        if (!authorizationService.isGrowingSystemValidable(gs.getTopiaId())) {
                            domainContext.formatError("Vous n'avez pas les droits d'écriture sur le système de culture '%s' de la parcelle '%s' du domaine '%s' pour la campagne '%d'",
                                    gs.getName(), formatPlot(plot), formatDomain(domain), studiedCampaign);
                        }

                        GrowingPlan gp = gs.getGrowingPlan();
                        if (!authorizationService.isGrowingPlanWritable(gp.getTopiaId())) {
                            domainContext.formatError("Vous n'avez pas les droits d'écriture sur le dispositif '%s' de la parcelle '%s' du domaine '%s' pour la campagne '%d'",
                                    gp.getName(), formatPlot(plot), formatDomain(domain), studiedCampaign);
                        }
                    }

                    domainContext.formatInfo("La parcelle '%s' existe déjà dans Agrosyst dans le domaine '%s' pour la campagne '%s'. Les nouvelles données seront ajoutées à celles existantes.",
                            formatPlot(plot), formatDomain(domain), studiedCampaign);

                    // no need to check permission on zone as it is same as growing system
                    zone = zoneDao.forPlotEquals(plot).addEquals(Zone.PROPERTY_TYPE, ZoneType.PRINCIPALE).findAnyOrNull();

                }

                EdaplosContext plotZoneContext = domainContext.ofPlotAndZone(plot, zone);

                // check whether there is one and only one zone
                // eDaplos import works only if plot as only one zone
                long count = zoneDao.forPlotEquals(plot).count();
                if (count != 1) {
                    plotZoneContext.formatError("La parcelle Agrosyst '%s' correspondante à la parcelle eDaplos '%s (%s)' ne doit être composée que d'une seule zone.",
                            formatPlot(plot), plotName, issuerPlotId);
                }

                // Gestion des caractéristiques de la parcelle
                List<TechnicalCharacteristic> groundElements = plotElement.getApplicableTechnicalCharacteristics();
                if (CollectionUtils.isEmpty(groundElements)) {
                    plotZoneContext.formatInfo("Aucun type de sol n'a été renseigné pour la parcelle '%s' du domaine '%s'. Il sera à compléter dans Agrosyst.", formatPlot(plot), formatDomain(domain));
                } else {
                    processPlotTechnicalCharacteristic(plotZoneContext, domain, plot, groundElements);
                }

                // get plotSoilOccupations
                List<PlotSoilOccupation> soilOccupationElements = plotElement.getAppliedPlotSoilOccupations();
                if (CollectionUtils.isEmpty(soilOccupationElements)) {
                    plotZoneContext.formatWarning("La parcelle '%s' du domaine '%s' ne contient aucune information sur les cultures, les ITK et le matériel. Veuillez vérifier les données exportées.",
                            formatPlot(plot), formatDomain(domain));
                } else { // else plotSoilOccupations analysis
                    processPlotSoilOccupation(plotZoneContext, soilOccupationElements);
                }

                processSeasonalCycleValidation(plotZoneContext, plot, zone);
            }
        }
    }

    /**
     * Analyse les données du {@code EdaplosPlotDto#seasonalCycleDto} pour vérifier :
     * <ul>
     * <li>la cohérence de l'ordre des rangs</li>
     * </ul>
     */
    protected void processSeasonalCycleValidation(EdaplosContext plotZoneContext, Plot plot, Zone zone) {
        EffectiveSeasonalCropCycle seasonalCycle = effectiveSeasonalCropCycleDao
                .forZoneEquals(zone)
                .setOrderByArguments(EffectiveSeasonalCropCycle.PROPERTY_TOPIA_ID)
                .findFirstOrNull();
        if (seasonalCycle != null && seasonalCycle.getNodes() != null) {
            Collection<EffectiveCropCycleNode> nodes = seasonalCycle.getNodes();
            EffectiveCropCycleNode[] orderedNodes = new EffectiveCropCycleNode[nodes.size()];
            boolean hasOutBound = false;
            for (EffectiveCropCycleNode node : nodes) {
                try {
                    orderedNodes[node.getRank()] = node;
                } catch (IndexOutOfBoundsException e) {
                    LOGGER.error("Parcelle: " + formatPlot(plot) + " domaine:" + formatDomain(plot.getDomain()) + ", Rang non valide:" + node.getRank() + " pour un cycle de " + nodes.size() + " noeuds, EdaplosIssuerId: '" + node.getEdaplosIssuerId() + "', culture: " + node.getCroppingPlanEntry().getName() + ", id culture: " + node.getCroppingPlanEntry().getTopiaId());
                    hasOutBound = true;
                }
            }

            consoleLogErrors(plot, nodes, orderedNodes);

            if (hasOutBound || ArrayUtils.contains(orderedNodes, null)) {
                // S'il y a un null, c'est qu'il y a un raté sur le numerotage (au moins deux nodes avec le meme rank)
                plotZoneContext.formatError("Les rangs des occupations du sol sur la parcelle '%s' du domaine '%s' ne sont pas correctement ordonnés (ils devraient s'étaler de 1 à %d sans doublon).",
                        formatPlot(plot), formatDomain(plot.getDomain()), orderedNodes.length);
            }
        }
    }

    protected void consoleLogErrors(Plot plot, Collection<EffectiveCropCycleNode> nodes, EffectiveCropCycleNode[] orderedNodes) {
        if (ArrayUtils.contains(orderedNodes, null)) {
            List<String> nodeDetails = new ArrayList<>();
            nodeDetails.add("\n");
            for (EffectiveCropCycleNode node : nodes) {
                StringBuilder nodeDetail = new StringBuilder("Culture:" + node.getCroppingPlanEntry().getName());
                nodeDetail.append(" IdCulture:").append(node.getCroppingPlanEntry().getTopiaId());
                nodeDetail.append(" Type de culture").append(node.getCroppingPlanEntry().getType());
                nodeDetail.append(" EdaplosIssuerId:").append(node.getEdaplosIssuerId());
                nodeDetail.append(" Rang:").append(node.getRank());
                List<CroppingPlanSpecies> species = ListUtils.emptyIfNull(node.getCroppingPlanEntry().getCroppingPlanSpecies());
                for (CroppingPlanSpecies croppingPlanSpecies : species) {
                    RefEspece refEspece = croppingPlanSpecies.getSpecies();
                    nodeDetail.append(" Espèce: ").append(refEspece.getCode_espece_botanique()).append("-").append(refEspece.getCode_qualifiant_AEE()).append("-").append(refEspece.getCode_type_saisonnier_AEE());
                }
                nodeDetails.add(nodeDetail.toString());
            }
            LOGGER.error("Parcelle: " + formatPlot(plot) + " domaine:" + formatDomain(plot.getDomain()) + ", Le cycle contient des noeuds nulls, nombre de noeuds attendus:" + nodes.size());
            LOGGER.error(StringUtils.join(nodeDetails, '\n'));
        }
    }

    /**
     * Analyse des occupations du sol
     * L'équivalent de l'occupation du sol dans Agrosyst est :
     * * pour les espèces assolées : effectivecropcyclenode pour les cultures principales et effectivecropcycleconnection pour les intermédiaires
     * * pour les espèces pérennes : effectiveperennialcropcycle
     */
    protected void processPlotSoilOccupation(EdaplosContext plotZoneContext, List<PlotSoilOccupation> soilOccupationList) {
        Domain domain = plotZoneContext.getDomain();
        Plot plot = plotZoneContext.getPlot();
        Zone zone = plotZoneContext.getZone();
        String issuerPlotId = plot.geteDaplosIssuerId();

        for (PlotSoilOccupation soilOccupation : soilOccupationList) {
            // Premise : only one effectiveseasonalcropcycle per zone 
            // (so per plot, as there is only one zone per plot using edaplos), 
            // so it is easy to get the effectiveseasonalcropcycle from the plot

            String soilOccupationIssuerId = soilOccupation.getIdentification();
            if (StringUtils.isBlank(soilOccupationIssuerId)) {
                plotZoneContext.formatError("La parcelle '%s' du domaine '%s' possède une culture sans identifiant. Aucune donnée ne pourra être ajoutée",
                        formatPlot(plot), formatDomain(domain));
                // set default value to go on parsing the message
                soilOccupationIssuerId = "id_occupation_du_sol_à_compléter_" + randomGenerator.nextInt(10000);
            }

            String soilOccupationType = soilOccupation.getTypeCode();
            CroppingEntryType croppingEntryType;
            if (StringUtils.isBlank(soilOccupationType)) {
                plotZoneContext.formatError("L'occupation du sol '%s' sur la parcelle '%s' du domaine '%s', ne possède pas de type. Cette information est obligatoire. À compléter avant import",
                        soilOccupationIssuerId, formatPlot(plot), formatDomain(domain));
                // set default value to go on parsing the message
                croppingEntryType = CroppingEntryType.MAIN;
            } else {
                // set croppingPlanEntryType
                switch (soilOccupationType) {
                    case PlotSoilOccupation.TYPE_CULTURE_PRINCIPALE -> croppingEntryType = CroppingEntryType.MAIN;
                    case PlotSoilOccupation.TYPE_CULTURE_DEROBEE -> croppingEntryType = CroppingEntryType.CATCH;
                    case PlotSoilOccupation.TYPE_CULTURE_INTERMEDIAIRE_PRECEDENTE, PlotSoilOccupation.TYPE_CULTURE_INTERMEDIAIRE_PRECEDENTE_REPOUSSEE ->
                            croppingEntryType = CroppingEntryType.INTERMEDIATE;
                    default -> {
                        plotZoneContext.formatInfo("L'occupation du sol '%s' sur la parcelle '%s (%s)'  est de type '%s'. Ce type d'occupation du sol n'est pas géré par Agrosyst, il ne sera pas importé.",
                                soilOccupationIssuerId, plot.getName(), issuerPlotId, soilOccupationType);
                        // no need to go on parsing within this soil occupation
                        continue;
                    }
                }
            }

            // only one culture per soiloccupation (made of one or more species)
            CroppingPlanEntry tmpCroppingPlanEntry = new CroppingPlanEntryImpl();
            tmpCroppingPlanEntry.setType(croppingEntryType);

            List<AgriculturalCrop> speciesOnSoilOccupation = soilOccupation.getSownAgriculturalCrops();

            // get soil occupation rank
            String soilOccupationSequence = soilOccupation.getSequenceNumeric();
            // the PlotSoilOccupation.SequenceNumeric has to be an integer or ' ' or null
            if (!StringUtils.isNumericSpace(soilOccupationSequence)) {
                plotZoneContext.formatError("Format du rang non valide (entier obligatoire) pour l'occupation du sol '%s', parcelle '%s', domaine '%s'",
                        soilOccupationIssuerId, formatPlot(plotZoneContext), formatDomain(plotZoneContext));
            }
            int soilOccupationRank = randomGenerator.nextInt(100) + 20; // default value
            if (!StringUtils.isBlank(soilOccupationSequence)) {
                try {
                    soilOccupationRank = Integer.parseInt(soilOccupationSequence) - 1;
                } // numbering start from 1 in eDaplos while 0 in Agrosyst
                catch (NumberFormatException e) {
                    //eDomainDto.setEdaplosParsingStatus(EdaplosParsingStatus.FAIL);
                    plotZoneContext.formatError("Format du rang non valide (entier obligatoire) pour l'occupation du sol '%s', parcelle '%s', domaine '%s'",
                            soilOccupationIssuerId, formatPlot(plotZoneContext), formatDomain(plotZoneContext));
                }
            } else {
                //eDomainDto.setEdaplosParsingStatus(EdaplosParsingStatus.FAIL);
                plotZoneContext.formatError("Format du rang non valide (entier obligatoire) pour l'occupation du sol '%s', parcelle '%s', domaine '%s'",
                        soilOccupationIssuerId, formatPlot(plotZoneContext), formatDomain(plotZoneContext));
                soilOccupationRank = 0;
            }
            if (soilOccupationRank < 0) {
                plotZoneContext.formatError("Format du rang non valide (entier obligatoire) pour l'occupation du sol '%s', parcelle '%s', domaine '%s'",
                        soilOccupationIssuerId, formatPlot(plotZoneContext), formatDomain(plotZoneContext));
            }

            List<CroppingPlanSpecies> species;
            // ticket #8650 : "Si la balise <AgriculturalCrop> est manquante, remonter une erreur et mettre une espèce par défaut (Fenouil)."
            // ticket #9228 : "Rendre possible l'import d'une occupation du sol sans cultures"
            if (CollectionUtils.isEmpty(speciesOnSoilOccupation)) {
                plotZoneContext.formatInfo("Aucune culture n'est déclarée à l'emplacement de rang %s sur la parcelle '%s', domaine '%s', campagne %d",
                        soilOccupationRank + 1, formatPlot(plot), formatDomain(domain), domain.getCampaign());

                // Avec le #10411, on constate que suite au #9228, le comportement n'a pas été revu : si l'occupation de sol sans culture est possible, il n'est pas pour autant nécessaire de créer une culture par défaut.
                continue;

            } else {
                // test species format and populate
                species = createSpeciesToCroppingPlanEntry(plotZoneContext, speciesOnSoilOccupation, soilOccupationIssuerId);
                tmpCroppingPlanEntry.setName(plotZoneContext.getCroppingName());

                if (CollectionUtils.isEmpty(species)) {
                    plotZoneContext.formatError("L'occupation du sol ayant pour identifiant : %s, ne possède aucune espèce valide permettant de créer une culture",
                            soilOccupationIssuerId);
                    continue;
                } else {
                    tmpCroppingPlanEntry.addAllCroppingPlanSpecies(species);
                }
            }

            // test if croppingPlanEntry already exists on domain
            List<CroppingPlanEntry> domainCroppingPlanEntries = croppingPlanEntryDao.forDomainEquals(plotZoneContext.getDomain()).findAll();
            if (CollectionUtils.isNotEmpty(domainCroppingPlanEntries)) {
                tmpCroppingPlanEntry = getRightCroppingPlanEntry(plotZoneContext, tmpCroppingPlanEntry, domainCroppingPlanEntries);
            }

            if (tmpCroppingPlanEntry.isPersisted()) {
                tmpCroppingPlanEntry = croppingPlanEntryDao.update(tmpCroppingPlanEntry);
            } else {
                tmpCroppingPlanEntry.setCode(UUID.randomUUID().toString());
                tmpCroppingPlanEntry.setDomain(domain);
                tmpCroppingPlanEntry = croppingPlanEntryDao.create(tmpCroppingPlanEntry);
            }

            EdaplosContext cropContext = plotZoneContext.ofCroppingPlanEntry(tmpCroppingPlanEntry);

            // Determine whether the culture is seasonal or perennial in order to know which type of cropcycle to create 
            // (seasonalcropcycle or perennialcrop cycle ?) 
            // Premise : we take only the first species            
            RefEspece speciesToGetCycle = CollectionUtils.emptyIfNull(species).iterator().next().getSpecies();
            TypeCulture soilOccType = getCycleTypeFromSpecies(speciesToGetCycle);
            if (soilOccType == null) {
                cropContext.formatError("L'occupation du sol ayant pour identifiant : %s, possède une espèce à laquelle il n'est pas possible d'attribuer un type (assolée ou pérenne). " +
                                "Contactez l'équipe Agrosyst avec les caractéristiques de cette culture : Espèce botanique '%s', Qualifiant '%s', Type saisonnier '%s'"
                        , soilOccupationIssuerId, speciesToGetCycle.getCode_espece_botanique(), speciesToGetCycle.getCode_qualifiant_AEE(), speciesToGetCycle.getCode_type_saisonnier_AEE());
                // put default value
                soilOccType = TypeCulture.ANNUELLE;
            }

            // add soil occupations to plot
            if (soilOccType.equals(TypeCulture.PERENNE)) { // perennial species
                if (!tmpCroppingPlanEntry.getType().equals(CroppingEntryType.MAIN)) {
                    cropContext.formatError("Une culture pérenne %s est associée à une culture dérobée ou intermédiaire sur la parcelle '%s', domaine '%s'. Ceci empêche l'import de se dérouler convenablement.",
                            tmpCroppingPlanEntry.getName(), formatPlot(plot), formatDomain(domain));
                }

                List<EffectivePerennialCropCycle> effPerennialCycles = effectivePerennialCropCycleDao.forZoneEquals(zone).findAll();
                List<EffectivePerennialCropCycle> matchingPerennialCycles = effPerennialCycles.stream()
                        .filter(effectivePerennialCropCycle -> Objects.equals(effectivePerennialCropCycle.getPhase().getEdaplosIssuerId(), issuerPlotId)).toList();
                EffectivePerennialCropCycle perennialCycle;

                // TODO eancelet 2016-05-03 : rechercher par topiaid de la culture ?        
                if (matchingPerennialCycles.size() > 1) {
                    cropContext.formatError("Sur la parcelle '%s', l'occupation du sol '%s' est associée à plusieurs itinéraires techniques d'espèces pérennes dans Agrosyst. Contacter l'équipe Agrosyst pour plus d'informations",
                            formatPlot(cropContext), formatSolOccupation(cropContext));
                    // to go on parsing, default behavior
                    perennialCycle = createNewEffPerennialCropCycle(issuerPlotId, zone, tmpCroppingPlanEntry);
                    //effPerennialCycleDtos.add(perennialCycleDto);
                } else if (matchingPerennialCycles.isEmpty()) {
                    perennialCycle = createNewEffPerennialCropCycle(issuerPlotId, zone, tmpCroppingPlanEntry);
                    //effPerennialCycleDtos.add(perennialCycleDto);
                    // TODO eancelet 2016-06-07 : add Phase (one phase per perennialCycle)
                } else {
                    perennialCycle = matchingPerennialCycles.getFirst();
                }

                if (NumberUtils.isCreatable(soilOccupation.getPlantationYear())) {
                    perennialCycle.setPlantingYear(Integer.parseInt(soilOccupation.getPlantationYear()));
                }

                EffectiveCropCyclePhase phase = perennialCycle.getPhase();
                List<AgriculturalCropProductionCycle> prodCycles = soilOccupation.getSpecifiedAgriculturalCropProductionCycles();
                processEffectiveCropPhaseInterventions(cropContext.ofEffectivePhase(phase), prodCycles);
            } else { // seasonal species
                EffectiveSeasonalCropCycle effSeasonalCropCycle = effectiveSeasonalCropCycleDao
                        .forZoneEquals(zone)
                        .setOrderByArguments(EffectiveIntervention.PROPERTY_TOPIA_ID)
                        .findFirstOrNull();
                if (effSeasonalCropCycle == null) {
                    effSeasonalCropCycle = new EffectiveSeasonalCropCycleImpl();
                    effSeasonalCropCycle.setZone(zone);
                    effSeasonalCropCycle = effectiveSeasonalCropCycleDao.create(effSeasonalCropCycle);
                }

                Set<EffectiveCropCycleConnection> connections = new HashSet<>();
                if (effSeasonalCropCycle.getNodes() != null) {
                    connections.addAll(effectiveCropCycleConnectionTopiaDao.forSourceIn(effSeasonalCropCycle.getNodes()).findAll());
                    connections.addAll(effectiveCropCycleConnectionTopiaDao.forTargetIn(effSeasonalCropCycle.getNodes()).findAll());
                }

                // Main and catch croppingPlanEntries 
                if (tmpCroppingPlanEntry.getType().equals(CroppingEntryType.CATCH) || tmpCroppingPlanEntry.getType().equals(CroppingEntryType.MAIN)) {

                    // various tests
                    if (CollectionUtils.isEmpty(effSeasonalCropCycle.getNodes()) && !CollectionUtils.isEmpty(connections)) {
                        cropContext.formatError("Aucune culture principale n'a été déclarée pour la parcelle '%s', domaine '%s' alors que plusieurs cultures intermédiaires y ont été déclarées.",
                                formatPlot(plot), formatDomain(domain));
                    }
                    if (CollectionUtils.isEmpty(effSeasonalCropCycle.getNodes()) && soilOccupationRank > 0) {
                        cropContext.formatError("Le rang de l'occupation du sol '%s' doit être '1' (il est de '%s')",
                                soilOccupationIssuerId, soilOccupationRank + 1);
                    }

                    // get or construct nodes
                    Collection<EffectiveCropCycleNode> effNodes = effSeasonalCropCycle.getNodes();
                    if (effNodes == null) {
                        effNodes = new ArrayList<>();
                        effSeasonalCropCycle.setNodes(effNodes);
                    }
                    EffectiveCropCycleNode effNode = null;

                    // test if node already exists (with edaplosIssuerId then with rank)
                    int matchingNodes = 0;
                    for (EffectiveCropCycleNode existingNode : effNodes) {
                        if (StringUtils.isNotBlank(existingNode.getEdaplosIssuerId()) && existingNode.getEdaplosIssuerId().equals(soilOccupationIssuerId)) {
                            if (existingNode.getRank() == soilOccupationRank) {
                                effNode = existingNode;
                                matchingNodes++;
                                // ("UPDATE");
                                // eancelet 2015-03-05 : pour le moment on ne fait rien de plus, après il faudra rajouter les interventions

                                List<AgriculturalCropProductionCycle> prodCycles = soilOccupation.getSpecifiedAgriculturalCropProductionCycles();
                                processEffectiveCropCycleInterventions(cropContext.ofEffectiveNode(effNode), prodCycles, false);
                            } else {
                                cropContext.formatError("L'occupation du sol (%s - rang %s) est retrouvé dans la BDD Agrosyst avec le rang %s (domaine : %s - %s)",
                                        soilOccupationIssuerId, soilOccupationRank, existingNode.getRank(),
                                        cropContext.getDomain().getName(), domain.getCampaign());
                                // TODO eancelet 2016-06-21 : add default behaviour 
                            }

                            // Cas ou l'existant n'a pas d'eDaplossIssuerId mais le même rang : on considère que ce sont des infos complementaires au nœud
                        } else if (StringUtils.isBlank(existingNode.getEdaplosIssuerId())
                                && !existingNode.getTopiaId().startsWith(EffectiveCropCycleService.NEW_NODE_PREFIX) // be sure it is not new one
                                && existingNode.getRank() == soilOccupationRank) {
                            effNode = existingNode;
                            matchingNodes++;
                            // in case the match was made with the rank only, the persistedNode has no eDaplosIssuerId
                            if (StringUtils.isNotBlank(effNode.getEdaplosIssuerId())) {
                                effNode.setEdaplosIssuerId(soilOccupationIssuerId);
                            }
                            List<AgriculturalCropProductionCycle> prodCycles = soilOccupation.getSpecifiedAgriculturalCropProductionCycles();
                            processEffectiveCropCycleInterventions(cropContext.ofEffectiveNode(effNode), prodCycles, false);
                        } if (effNode != null) {
                            if (CroppingEntryType.INTERMEDIATE == effNode.getCroppingPlanEntry().getType()) {
                                // this node was created to allow importing interventions on intermediate crop
                                // while actual node was not existing.
                                // this node is the real targeted node, we update previous temporary values by correct ones.
                                effNode.setCroppingPlanEntry(tmpCroppingPlanEntry);
                                effNode.setEdaplosIssuerId(soilOccupationIssuerId);
                                effectiveCropCycleNodeTopiaDao.update(effNode);
                            }
                            if (!tmpCroppingPlanEntry.equals(effNode.getCroppingPlanEntry())) {
                                cropContext.formatError("La culture du fichier d'import déclarée au rang '%s' sur la parcelle '%s' du domaine '%s' est différente de celle dans Agrosyst (issue d'un import précédent ou déclaré dans Agrosyst)",
                                        soilOccupationRank, formatPlot(plot), formatDomain(domain));
                            }
                        }
                    }

                    if (matchingNodes > 1) {
                        cropContext.formatError("L'occupation du sol (%s - rang %s - culture %s) possède trop de noeuds correspondants dans l'import ou dans la BDD Agrosyst (domaine : %s - %s). Contactez l'équipe Agrosyst",
                                soilOccupationIssuerId, soilOccupationRank, tmpCroppingPlanEntry.getName(), domain.getName(),
                                domain.getCampaign());
                        // TODO eancelet 2016-06-21 : add default behaviour 
                    } else if (matchingNodes == 0) { // the node needs to be created
                        // ("CREATE")
                        effNode = createNode(soilOccupationIssuerId, tmpCroppingPlanEntry, soilOccupationRank);
                        // 2015-03-12 eancelet : à priori pas besoin de contrôler si le numéro de rang est déjà utilisé car déjà testé juste au-dessus
                        effNodes.add(effNode);
                        List<AgriculturalCropProductionCycle> prodCycles = soilOccupation.getSpecifiedAgriculturalCropProductionCycles();
                        processEffectiveCropCycleInterventions(cropContext.ofEffectiveNode(effNode), prodCycles, false);
                    }

                } else if (tmpCroppingPlanEntry.getType().equals(CroppingEntryType.INTERMEDIATE)) { // Intermediate croppingPlanEntries
                    processEffectiveIntermediateCrop(tmpCroppingPlanEntry, cropContext, effSeasonalCropCycle, soilOccupationRank, connections, soilOccupation, soilOccupationIssuerId);
                }
            }

        }
    }

    protected void processEffectiveIntermediateCrop(
            CroppingPlanEntry intermediateCrop,
            EdaplosContext cropContext,
            EffectiveSeasonalCropCycle effSeasonalCropCycle,
            int soilOccupationRank,
            Set<EffectiveCropCycleConnection> connections,
            PlotSoilOccupation soilOccupation,
            String soilOccupationIssuerId) {

        Plot plot = cropContext.getPlot();
        Domain domain = cropContext.getDomain();
        CroppingPlanEntry croppingPlanEntry = cropContext.getCroppingPlanEntry();

        Collection<EffectiveCropCycleNode> nodes = effSeasonalCropCycle.getNodes();
        if (nodes == null) {
            nodes = new ArrayList<>();
            effSeasonalCropCycle.setNodes(nodes);
            // cas où les cultures intermédiaires sont avant dans le message par rapport aux cultures principales
            // crée une node dont la culture devra être remplacée une fois trouvée
            // case where intermediate crops are first and node have not been created yet
            // we create a temporary node to allow importing interventions.
            // valid Crop and EdaplosIssuerId will be set when parsing future nodes.
            EffectiveCropCycleNode effNode = createNode(null, croppingPlanEntry, soilOccupationRank);
            nodes.add(effNode);
            effectiveSeasonalCropCycleDao.update(effSeasonalCropCycle);
        }
        if (CollectionUtils.isEmpty(nodes)) {
            cropContext.formatError("Impossible d'importer la culture intermédiaire sur la parcelle '%s', domaine '%s' : aucune culture principale n'existe.",
                    formatPlot(plot), formatDomain(cropContext));
        }

        EffectiveCropCycleConnection conn = null;
        // search if connection already exists
        int matchingConn = 0;
        for (EffectiveCropCycleConnection connection : connections) {
            // search with edaplosIssuerId
            if (connection.getEdaplosIssuerId() != null && connection.getEdaplosIssuerId().equals(soilOccupationIssuerId)) {
                for (EffectiveCropCycleNode node : nodes) {
                    if (connection.getTarget().equals(node) && node.getRank() != soilOccupationRank) {
                        cropContext.formatError("L'occupation du sol (%s - rang %s) est retrouvé dans la BDD Agrosyst avec le rang %s (domaine : %s - %s)",
                                soilOccupationIssuerId, soilOccupationRank, node.getRank(),
                                domain.getName(), domain.getCampaign());
                    }
                }
                conn = connection;
                matchingConn++;
            } else { // search with rank (a connection has the same rank as its target)
                for (EffectiveCropCycleNode node : nodes) {
                    if (connection.getTarget().equals(node) && node.getRank() == soilOccupationRank) { // connection found
                        conn = connection;
                        conn.setEdaplosIssuerId(soilOccupationIssuerId);
                        matchingConn++;
                    }
                }
            }
            if (conn != null) {
                if (croppingPlanEntry.equals(conn.getIntermediateCroppingPlanEntry())) {
                    cropContext.formatError("L'occupation du sol (%s - rang %s) possède une culture différente '%s' de l'occupation du sol équivalente issue du même import ou de la BDD Agrosyst (domaine : %s - %s)",
                            soilOccupationIssuerId, soilOccupationRank, croppingPlanEntry.getName(), domain.getName(),
                            domain.getCampaign());
                }
            }
        }
        if (matchingConn > 1) {
            cropContext.formatError("L'occupation du sol (%s - rang %s - culture %s) possède trop de connexions correspondantes dans l'import ou dans la BDD Agrosyst (domaine : %s - %s). Contactez l'équipe Agrosyst",
                    soilOccupationIssuerId, soilOccupationRank, croppingPlanEntry.getName(), domain.getName(),
                    domain.getCampaign());
            // by default, the connection is then the last connection we loop through
        } else if (matchingConn == 0) {// connection needs to be created
            // get target and source
            EffectiveCropCycleNode connectionTarget = null;
            EffectiveCropCycleNode connectionTargetNode = null;
            EffectiveCropCycleNode connectionSource = null;
            // set connection's source and target
            for (EffectiveCropCycleNode existingNode : nodes) {
                if (existingNode.getRank() == soilOccupationRank) {
                    connectionTarget = existingNode;
                    connectionTargetNode = existingNode;
                }
                if (soilOccupationRank > 0 && existingNode.getRank() == (soilOccupationRank - 1)) {
                    connectionSource = existingNode;
                }
                if (connectionTarget != null && connectionSource != null) {
                    break;
                }
            }

            if (connectionTarget == null) {
                //effectiveIntermediateCropElementInformation.geteDomainDto().setEdaplosParsingStatus(EdaplosParsingStatus.FAIL);
                cropContext.formatError("Impossible d'importer la culture intermédiaire sur la parcelle '%s', domaine '%s' : aucune culture principale n'existe.",
                        formatPlot(plot), formatDomain(domain));

                // Create fake node if there is no target node
                connectionTargetNode = new EffectiveCropCycleNodeImpl();
                //connectionTargetNode.setLabel("Element fictif pour ratacher la culture intermédiaire " + effectiveIntermediateCropElementInformation.getSoilOccupationIssuerId());
                connectionTargetNode.setRank(soilOccupationRank);
                //connectionTargetNode.setInterventions(new ArrayList<>());
                connectionTargetNode.setCroppingPlanEntry(croppingPlanEntry);
                connectionTargetNode = effectiveCropCycleNodeTopiaDao.create(connectionTargetNode);
            }

            if (soilOccupationRank > 0 && connectionSource == null) {
                cropContext.formatError("Impossible d'importer la culture intermédiaire sur la parcelle '%s', domaine '%s' : aucune culture précédente présente.",
                        formatPlot(plot), formatDomain(domain));
            }

            conn = new EffectiveCropCycleConnectionImpl();
            conn.setEdaplosIssuerId(soilOccupationIssuerId);
            conn.setSource(connectionSource);
            conn.setTarget(connectionTargetNode);
            conn.setIntermediateCroppingPlanEntry(intermediateCrop);
            conn = effectiveCropCycleConnectionTopiaDao.create(conn);
        }

        if (conn != null) {
            List<AgriculturalCropProductionCycle> productionCycles = soilOccupation.getSpecifiedAgriculturalCropProductionCycles();
            processEffectiveCropCycleInterventions(cropContext.ofEffectiveNode(conn.getTarget()), productionCycles, true);
        }
    }


    protected void processEffectiveCropCycleInterventions(EdaplosContext nodeContext,
                                                          List<AgriculturalCropProductionCycle> prodCycleList, boolean isIntermediateCrop) {
        Plot plot = nodeContext.getPlot();
        EffectiveCropCycleNode effectiveNode = nodeContext.getEffectiveNode();
        String soilOccId = effectiveNode.getEdaplosIssuerId();
        String ePlotId = plot.geteDaplosIssuerId();

        // if there is no production cycle in the soil occupation then it is not normal
        if (prodCycleList == null || prodCycleList.isEmpty()) {
            nodeContext.formatInfo("L'occupation du sol ayant pour identifiant : %s (parcelle : %s), n'a pas de cycle de production (il en faut un)",
                    soilOccId, ePlotId);
        } else {

            // so far, there must be only one production cycle in the soil occupation
            if (prodCycleList.size() > 1) {
                nodeContext.formatError("L'occupation du sol ayant pour identifiant : %s (parcelle : %s), a plus d'un cycle de production (il en faut un seul)",
                        soilOccId, ePlotId);
            }
            for (AgriculturalCropProductionCycle prodCycle : prodCycleList) {
                List<PlotAgriculturalProcess> agriculturalProcess = prodCycle.getApplicablePlotAgriculturalProcesss();
                if (agriculturalProcess == null || agriculturalProcess.isEmpty()) {
                    nodeContext.formatInfo("L'occupation du sol ayant pour identifiant : %s (parcelle : %s), possède un cycle de production sans intervention",
                            soilOccId, ePlotId);
                } else {

                    processCropCycleNodeInterventions(nodeContext, agriculturalProcess, isIntermediateCrop);
                }
            }
        }
    }

    protected void processCropCycleNodeInterventions(EdaplosContext nodeContext,
                                                     List<PlotAgriculturalProcess> agriculturalProcesses,
                                                     boolean isIntermediateCrop) {

        for (PlotAgriculturalProcess agriculturalProcess : agriculturalProcesses) {
            String eInterId = agriculturalProcess.getIdentification();
            if (StringUtils.isEmpty(eInterId)) {
                nodeContext.formatError("Sur la parcelle '%s', occupation du sol '%s', une intervention ne possède pas d'identifiant. Import Impossible",
                        formatPlot(nodeContext), formatSolOccupation(nodeContext));
                eInterId = "id_inter_a_completer_" + randomGenerator.nextInt(10000);
            }

            EffectiveIntervention existingIntervention = effectiveInterventionDao.forEdaplosIssuerIdEquals(eInterId).findUniqueOrNull();
            if (existingIntervention != null) {
                // les interventions sont dupliquées, ont les ignores seulement (sans message)
                continue;
            }

            EffectiveIntervention effectiveIntervention = processCropCycleNewIntervention(nodeContext, agriculturalProcess, eInterId);
            if (effectiveIntervention != null) {
                effectiveIntervention.setIntermediateCrop(isIntermediateCrop);
                effectiveInterventionDao.update(effectiveIntervention);
            }
        }
    }

    /**
     * Get the croppingPlanEntry from domain if already exist
     * (match made with croppingPlanEntry type and botanical code species, suplementary botanical code species,
     * sowing period code)
     *
     * @param temporaryCrop   : croppingPlanEntry we try to import
     * @param domainEntryList : list of domain's croppingPlanEntries
     */

    protected CroppingPlanEntry getRightCroppingPlanEntry(EdaplosContext context, CroppingPlanEntry temporaryCrop, List<CroppingPlanEntry> domainEntryList) {

        // get edaplos Species list without duplicates
        Collection<CroppingPlanSpecies> temporarySpecies = Lists.newArrayList(temporaryCrop.getCroppingPlanSpecies());
        Map<String, CroppingPlanSpecies> eSpeciesWithoutDuplicate = new HashMap<>();
        for (CroppingPlanSpecies eSoilOccupationSpecies : temporarySpecies) {
            RefEspece eRefEspece = eSoilOccupationSpecies.getSpecies();
            // TODO eancelet 2016-05-04 : gérer un peu mieux la clef avec les variétés
            String varietyKey = (eSoilOccupationSpecies.getVariety() != null) ? eSoilOccupationSpecies.getVariety().getLabel() : null;
            String eKey = eRefEspece.getCode_espece_botanique() + eRefEspece.getCode_qualifiant_AEE() + eRefEspece.getCode_type_saisonnier_AEE() + varietyKey + temporaryCrop.getType();
            eSpeciesWithoutDuplicate.put(eKey, eSoilOccupationSpecies); // if key already stored (in case of duplicate species) then element is replaced
        }

        for (CroppingPlanEntry domainCroppingPlanEntry : domainEntryList) {
            // compare crop type
            if (!temporaryCrop.getType().equals(domainCroppingPlanEntry.getType())) {
                continue;
            }
            // compare species
            Collection<CroppingPlanSpecies> domainCroppingPlanSpecies = Lists.newArrayList(domainCroppingPlanEntry.getCroppingPlanSpecies());
            // get domainSpecies list without duplicates
            Map<String, CroppingPlanSpecies> persistedSpeciesWithoutDuplicate = new HashMap<>();
            for (CroppingPlanSpecies persistedSpecies : domainCroppingPlanSpecies) {
                RefEspece persistedRefEspece = persistedSpecies.getSpecies();
                String varietyKey = (persistedSpecies.getVariety() != null) ? persistedSpecies.getVariety().getLabel() : null;
                // TODO eancelet 2016-05-04 : gérer un peu mieux la clef avec les variétés
                String eKey = persistedRefEspece.getCode_espece_botanique() + persistedRefEspece.getCode_qualifiant_AEE() + persistedRefEspece.getCode_type_saisonnier_AEE() + varietyKey + domainCroppingPlanEntry.getType();
                persistedSpeciesWithoutDuplicate.put(eKey, persistedSpecies);
            }

            if (persistedSpeciesWithoutDuplicate.size() != eSpeciesWithoutDuplicate.size()) {
                continue;
            }

            Map<CroppingPlanSpecies, CroppingPlanSpecies> tmpSpeciesToPersistedSpecies = new HashMap<>();

            for (Map.Entry<String, CroppingPlanSpecies> persistedSpeciesElement : persistedSpeciesWithoutDuplicate.entrySet()) {
                for (Map.Entry<String, CroppingPlanSpecies> eSoilOccupationSpeciesElement : eSpeciesWithoutDuplicate.entrySet()) {
                    if (persistedSpeciesElement.getKey().equals(eSoilOccupationSpeciesElement.getKey())) {

                        tmpSpeciesToPersistedSpecies.put(eSoilOccupationSpeciesElement.getValue(), persistedSpeciesElement.getValue());
                    }
                }
            }
            if (eSpeciesWithoutDuplicate.size() == tmpSpeciesToPersistedSpecies.size() && temporaryCrop.getTopiaId() == null) { // we found a matching croppingPlanEntry in domain
                temporaryCrop = domainCroppingPlanEntry;
                Map<String, CroppingPlanSpecies> newCodeGnisToSpecies = new HashMap<>();
                Map<String, CroppingPlanSpecies> codeGnisToSpecies = context.getCodeGnisToSpecies();
                for (Map.Entry<String, CroppingPlanSpecies> codeGnisToCroppingPlanSpecies : codeGnisToSpecies.entrySet()) {
                    final CroppingPlanSpecies tmpSpecies = codeGnisToCroppingPlanSpecies.getValue();
                    final CroppingPlanSpecies value = ObjectUtils.firstNonNull(tmpSpeciesToPersistedSpecies.get(tmpSpecies), tmpSpecies);
                    newCodeGnisToSpecies.put(codeGnisToCroppingPlanSpecies.getKey(), value);
                }
                codeGnisToSpecies.clear();
                codeGnisToSpecies.putAll(newCodeGnisToSpecies);
                // break; // eancelet 2015-03-04 : if uncommented, lazyloading problem in fr.inra.agrosyst.services.domain.DomainServiceImpl.validPreconditions() : Preconditions.checkNotNull(speciesDto.getSpeciesId());  
            }
        }
        return temporaryCrop;
    }


    /**
     * Construct the croppingPlanSpecies list and add it to the CroppingPlanEntry
     * eSoilOccupation and eEntry are modified
     *
     * @return the species
     */
    protected List<CroppingPlanSpecies> createSpeciesToCroppingPlanEntry(
            EdaplosContext cropContext,
            List<AgriculturalCrop> speciesXmlList,
            String soilOccupationId) {

        List<CroppingPlanSpecies> species = new ArrayList<>(speciesXmlList.size());
        List<String> speciesNames = new ArrayList<>(speciesXmlList.size());
        for (AgriculturalCrop speciesElement : speciesXmlList) {
            // 1. On récupère le code de l'espèce dans la balise SownAgriculturalCrop.BotanicalSpeciesCode
            String speciesBotanicalCode = StringUtils.normalizeSpace(speciesElement.getBotanicalSpeciesCode());
            String speciesSupplementaryBotanicalCode = Strings.nullToEmpty(StringUtils.normalizeSpace(speciesElement.getSupplementaryBotanicalSpeciesCode()));
            String speciesSowingPeriodCode = Strings.nullToEmpty(StringUtils.normalizeSpace(speciesElement.getSowingPeriodCode()));
            String speciesName = speciesElement.getDescription();

            if (StringUtils.isBlank(speciesBotanicalCode)) {
                cropContext.formatError("L'occupation du sol '%s', ne possède aucun identifiant d'espèce botanique", soilOccupationId);
                speciesBotanicalCode = DEFAULT_SPECIES_BOTANICAL_CODE;//ZBO
            }

            // if the species is not found in referential, then error
            RefEspece refEspece = refEspeceDao.forProperties(
                    RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, speciesBotanicalCode,
                    RefEspece.PROPERTY_CODE_QUALIFIANT__AEE, speciesSupplementaryBotanicalCode,
                    RefEspece.PROPERTY_CODE_TYPE_SAISONNIER__AEE, speciesSowingPeriodCode).findAnyOrNull();
            if (refEspece == null) {
                RefEspeceOtherTools refEspeceOtherTool = refEspeceOtherToolsDao.forProperties(
                        RefEspeceOtherTools.PROPERTY_CODE_ESPECE_BOTANIQUE_OTHER, speciesBotanicalCode,
                        RefEspeceOtherTools.PROPERTY_CODE_QUALIFIANT_AEEOTHER, speciesSupplementaryBotanicalCode,
                        RefEspeceOtherTools.PROPERTY_CODE_TYPE_SAISONNIER_AEEOTHER, speciesSowingPeriodCode).findAnyOrNull();
                if (refEspeceOtherTool != null) {
                    refEspece = refEspeceDao.forProperties(
                            RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, refEspeceOtherTool.getCodeEspeceBotanique(),
                            RefEspece.PROPERTY_CODE_QUALIFIANT__AEE, refEspeceOtherTool.getCodeQualifiantAEE(),
                            RefEspece.PROPERTY_CODE_TYPE_SAISONNIER__AEE, refEspeceOtherTool.getCodeTypeSaisonnierAEE()).findAnyOrNull();

                    if (refEspece != null) {

                        String eDaplosSpeciesNameToPrint = Strings.isNullOrEmpty(speciesName) ? (Strings.isNullOrEmpty(refEspeceOtherTool.getLibelleEspeceBotaniqueOther()) ? "" : " - " + refEspeceOtherTool.getLibelleEspeceBotaniqueOther()) : " - " + speciesName;

                        // edaplos other qualifier
                        List<String> speciesSupplementaryBotanical = new ArrayList<>();
                        if (StringUtils.isNotBlank(speciesSupplementaryBotanicalCode))
                            speciesSupplementaryBotanical.add(speciesSupplementaryBotanicalCode);
                        if (StringUtils.isNotBlank(refEspeceOtherTool.getLibelleQualifiantAEEOther()))
                            speciesSupplementaryBotanical.add(refEspeceOtherTool.getLibelleQualifiantAEEOther());
                        final String speciesSupplementaryBotanicalToPrint = String.join(" - ", speciesSupplementaryBotanical);

                        // edaplos other seasonal type
                        List<String> otherSeasonalType = new ArrayList<>();
                        if (StringUtils.isNotBlank(speciesSowingPeriodCode))
                            otherSeasonalType.add(speciesSowingPeriodCode);
                        if (StringUtils.isNotBlank(refEspeceOtherTool.getLibelleTypeSaisonnierAEEOther()))
                            otherSeasonalType.add(refEspeceOtherTool.getLibelleTypeSaisonnierAEEOther());
                        final String otherSeasonalTypeToPrint = String.join(" - ", otherSeasonalType);

                        String agrosystLibelle_espece_botanique = Strings.nullToEmpty(refEspeceOtherTool.getLibelleEspeceBotaniqueOther());

                        // agrosyst qualifier
                        List<String> qualifiers = new ArrayList<>();
                        if (StringUtils.isNotBlank(refEspece.getCode_qualifiant_AEE()))
                            qualifiers.add(refEspece.getCode_qualifiant_AEE());
                        if (StringUtils.isNotBlank(refEspeceOtherTool.getLibelleQualifiantAEE()))
                            qualifiers.add(refEspeceOtherTool.getLibelleQualifiantAEE());
                        final String qualifiersToPrint = String.join(" - ", qualifiers);

                        // agrosyst seasonal type
                        List<String> seasonalType = new ArrayList<>();
                        if (StringUtils.isNotBlank(refEspece.getCode_type_saisonnier_AEE()))
                            seasonalType.add(refEspece.getCode_type_saisonnier_AEE());
                        if (StringUtils.isNotBlank(refEspece.getLibelle_type_saisonnier_AEE()))
                            seasonalType.add(refEspece.getLibelle_type_saisonnier_AEE());
                        final String seasonalTypeToPrint = String.join(" - ", seasonalType);

                        cropContext.formatWarning("L'occupation du sol ayant pour identifiant : %s, possède une espèce qui n'a pas été retrouvée dans le référentiel des espèces Agrosyst mais une correspondance a été établie avec une espèce présente dans Agrosyst. " +
                                        "Caractéristiques de la culture à importer: Espèce botanique '%s%s', Qualifiant '%s', Type saisonnier '%s'. " +
                                        "Nouvelles caractéristiques de la culture après import dans Agrosyst : Espèce botanique '%s-%s', Qualifiant '%s', Type saisonnier '%s'.",
                                // identifiant
                                soilOccupationId,
                                //Espèce botanique '%s%s'                                     , Qualifiant '%s'                                                           , Type saisonnier '%s'."
                                speciesBotanicalCode, eDaplosSpeciesNameToPrint, speciesSupplementaryBotanicalToPrint, otherSeasonalTypeToPrint,
                                //Espèce botanique '%s-%s'                                    , Qualifiant '%s'                                                           , Type saisonnier '%s'
                                refEspece.getCode_espece_botanique(), agrosystLibelle_espece_botanique, qualifiersToPrint, seasonalTypeToPrint);
                    }
                }
            }
            if (refEspece == null) {
                cropContext.formatError("L'occupation du sol ayant pour identifiant : %s, possède une espèce qui n'a pas été retrouvée dans le référentiel des espèces Agrosyst. " +
                                "Voici les caractéristiques de cette culture : Espèce botanique '%s', Qualifiant '%s', Type saisonnier '%s'. Contacter l'équipe Agrosyst pour faire ajouter cette espèce.",
                        soilOccupationId, speciesBotanicalCode, speciesSupplementaryBotanicalCode, speciesSowingPeriodCode);
                // we set a default species to go on parsing
                refEspece = refEspeceDao.forCode_espece_botaniqueEquals(DEFAULT_SPECIES_BOTANICAL_CODE).findAny();
            }

            if (StringUtils.isBlank(speciesName)) {
                speciesName = refEspece.getLibelle_espece_botanique_Translated();
            }
            speciesNames.add(speciesName);

            // Process varieties
            List<CropSpeciesVariety> varieties = speciesElement.getSownCropSpeciesVarietys();

            if (CollectionUtils.isNotEmpty(varieties)) {
                for (CropSpeciesVariety variety : varieties) {

                    final String description = variety.getDescription();
                    // Le code GNIS transmis par les partenaires pourra avoir la forme 512 A041 ou 512A041.
                    String varietyCode = StringUtils.remove(variety.getTypeCode(), ' ');

                    RefVariete variete = null;

                    // 1/ On récupère le code de l'espèce dans la balise SownAgriculturalCrop.BotanicalSpeciesCode
                    // 2/ Dans RefEspecesToVarietes, on cherche les codes espèce de plantgrape ou geves correspondant.
                    List<RefEspeceToVariete> especeToVarietes = refEspeceToVarieteDao.forCode_espece_ediEquals(speciesBotanicalCode).findAll();

                    if (!especeToVarietes.isEmpty()) {

                        Iterator<RefEspeceToVariete> especeToVarieteIterator = especeToVarietes.iterator();

                        // dans le cas ou il existe plusieurs liens especeToVarietes alors on essaie jusqu'à en trouver potentiellement un qui fonctionne
                        while (variete == null && especeToVarieteIterator.hasNext()) {

                            RefEspeceToVariete especeToVariete = especeToVarieteIterator.next();

                            // 3/ Dans RefVarieteGeves ou RefVarietePlantGrape, on récupère les codes gnis des variétés correspondant au code espèce retrouvé en 2
                            if (StringUtils.isNotBlank(varietyCode)) {
                                if (ReferentialServiceImpl.VARIETE_GEVES.equalsIgnoreCase(especeToVariete.getReferentiel_source())) {
                                    List<RefVarieteGeves> varieteGeves = refVarieteGevesDao.findAllForCodeGnisVarieteEqualsIgnoreSpace(varietyCode);// unique key = Num_Dossier

                                    if (varieteGeves.size() > 1 && StringUtils.isNotBlank(description)) {
                                        // try find with matching description
                                        variete = varieteGeves.stream()
                                                .filter(refVarieteGeves -> isSameDespiteWhitespacesOrAccents(refVarieteGeves.getDenomination(), description))
                                                .findFirst()
                                                .orElse(null);
                                    }

                                    if (variete == null && !varieteGeves.isEmpty()) {
                                        variete = varieteGeves.getFirst();
                                    }

                                } else {
                                    variete = refVarietePlantGrapeDao.findAnyOrNullForCodeGnisEqualsIgnoreSpace(varietyCode);// unique key = CodeVar
                                }

                                if (variete == null) {
                                    variete = getVarieteFromVarietyCode(varietyCode);
                                }

                            }

                            if (variete == null && StringUtils.isNotBlank(description) && especeToVariete != null) {
                                // Chercher une variété en fonction de son libellé (balise Description) combiné au num_Espece_Botanique
                                // (code_espece_autre_referentiel de RefEspecesToVarietes)
                                String codeEspeceAutre = especeToVariete.getCode_espece_autre_referentiel();
                                if (StringUtils.isNotBlank(codeEspeceAutre) && NumberUtils.isCreatable(codeEspeceAutre)) {
                                    int numEspeceBotanique = Integer.parseInt(codeEspeceAutre);
                                    List<RefVarieteGeves> refVarieteGeves = refVarieteGevesDao.findForNumEspeceBotaniqueAndDescriptionEquals(numEspeceBotanique, varietyCode);// varietyCode can be null
                                    variete = refVarieteGeves.stream()
                                            .filter(varieteGeves -> isSameDespiteWhitespacesOrAccents(varieteGeves.getDenomination(), description))
                                            .findFirst()
                                            .orElse(null);
                                }
                            }
                        }
                    } else if (StringUtils.isNotBlank(varietyCode)) {

                        variete = refVarietePlantGrapeDao.findAnyOrNullForCodeGnisEqualsIgnoreSpace(varietyCode);// unique key = CodeVar

                        if (variete == null) {
                            variete = getVarieteFromVarietyCode(varietyCode);
                        }
                    }

                    CroppingPlanSpecies croppingPlanSpecies = croppingPlanSpeciesDao.newInstance();
                    croppingPlanSpecies.setCode(UUID.randomUUID().toString());
                    croppingPlanSpecies.setSpecies(refEspece);

                    if (variete == null) {
                        String warningDescription = description;
                        if (StringUtils.isNotBlank(description)) {
                            croppingPlanSpecies.setEdaplosUnknownVariety(description);
                        } else {
                            warningDescription = "?";
                        }
                        cropContext.formatWarning("L'occupation du sol ayant pour identifiant : '%s', possède une variété '%s', '%s'" +
                                        " dont le nom n'est pas présent dans le référentiel variété d'Agrosyst. Vous devrez compléter la variété de l'espèce %s",
                                soilOccupationId, varietyCode, warningDescription, speciesName);
                    } else {
                        croppingPlanSpecies.setVariety(variete);
                    }

                    croppingPlanSpecies.setValidated(true);

                    species.add(croppingPlanSpecies);

                    cropContext.addSpeciesCodeToSpeciesBotanicalCode(speciesBotanicalCode, croppingPlanSpecies);

                    if (variete != null && StringUtils.isNotBlank(varietyCode)) {
                        cropContext.addGnisCodeToVariety(varietyCode, croppingPlanSpecies);
                    }
                }
            } else {
                CroppingPlanSpecies croppingPlanSpecies = croppingPlanSpeciesDao.newInstance();
                croppingPlanSpecies.setCode(UUID.randomUUID().toString());
                croppingPlanSpecies.setSpecies(refEspece);
                croppingPlanSpecies.setValidated(true);
                species.add(croppingPlanSpecies);

                cropContext.addSpeciesCodeToSpeciesBotanicalCode(speciesBotanicalCode, croppingPlanSpecies);
            }
        }
        String croppingName = String.join(" - ", speciesNames);
        cropContext.setCroppingName(croppingName);

        return species;
    }

    protected RefVariete getVarieteFromVarietyCode(String varietyCode) {
        try {
            if (NumberUtils.isParsable(varietyCode)) {
                int numDossier = Integer.parseInt(varietyCode);
                return refVarieteGevesDao.forNum_DossierEquals(numDossier).findUniqueOrNull();
            }// unique key = Num_Dossier
        } catch (NumberFormatException e) {
            // The variety code is just not an int
            return null;
        }
        return null;
    }


    /**
     * Return cycle type (perennial or seasonal (annuel, biannuel)) from one species
     */
    protected TypeCulture getCycleTypeFromSpecies(RefEspece eSpecies) {
        RefCultureEdiGroupeCouvSol refCultureEdiGroupeCouvSol = refCultureEdiGroupeCouvSolDao.forProperties(
                        RefCultureEdiGroupeCouvSol.PROPERTY_CODE_ESPECE_BOTANIQUE, eSpecies.getCode_espece_botanique(),
                        RefCultureEdiGroupeCouvSol.PROPERTY_CODE_QUALIFIANT_AEE, eSpecies.getCode_qualifiant_AEE(),
                        RefCultureEdiGroupeCouvSol.PROPERTY_CODE_TYPE_SAISONNIER_AEE, eSpecies.getCode_type_saisonnier_AEE())
                .findAnyOrNull();
        if (refCultureEdiGroupeCouvSol == null) {
            return null;
        } else {
            return refCultureEdiGroupeCouvSol.getTypeCulture();
        }
    }

    protected EffectiveCropCycleNode createNode(String soilOccupationIssuerId, CroppingPlanEntry croppingPlanEntry, int rank) {
        EffectiveCropCycleNode node = new EffectiveCropCycleNodeImpl();
        node.setEdaplosIssuerId(soilOccupationIssuerId);
        node.setCroppingPlanEntry(croppingPlanEntry);
        node.setRank(rank);
        node = effectiveCropCycleNodeTopiaDao.create(node);
        return node;
    }

    protected EffectivePerennialCropCycle createNewEffPerennialCropCycle(String eDaplosIssuerId, Zone zone, CroppingPlanEntry croppingPlanEntry) {
        EffectivePerennialCropCycle perennialCycle = new EffectivePerennialCropCycleImpl();
        perennialCycle.setCroppingPlanEntry(croppingPlanEntry);
        EffectiveCropCyclePhase phase = new EffectiveCropCyclePhaseImpl();
        phase.setType(CropCyclePhaseType.PLEINE_PRODUCTION);
        phase.setEdaplosIssuerId(eDaplosIssuerId);
        phase = effectiveCropCyclePhaseTopiaDao.create(phase);
        perennialCycle.setPhase(phase);
        perennialCycle.setWeedType(WeedType.PARTIEL);
        perennialCycle.setZone(zone);
        return effectivePerennialCropCycleDao.create(perennialCycle);
    }

    protected void processEffectiveCropPhaseInterventions(
            EdaplosContext phaseContext,
            List<AgriculturalCropProductionCycle> prodCycleList) {

        // if there is no production cycle in the soil occupation then it is not normal
        if (prodCycleList == null || prodCycleList.isEmpty()) {
            //TODO ymartel 20161010 : specific message for phase instead of cycle ?
            phaseContext.formatInfo("L'occupation du sol ayant pour identifiant : '%s', parcelle : '%s', n'a pas de cycle de production (il en faut un)",
                    formatSolOccupation(phaseContext), formatPlot(phaseContext));

        } else {
            // so far, there must be only one production cycle in the soil occupation
            if (prodCycleList.size() > 1) {
                phaseContext.formatError("L'occupation du sol ayant pour identifiant : '%s', parcelle : '%s', a plus d'un cycle de production (il en faut un seul)",
                        formatSolOccupation(phaseContext), formatPlot(phaseContext));
            }

            for (AgriculturalCropProductionCycle prodCycle : prodCycleList) {
                List<PlotAgriculturalProcess> agriculturalProcesses = prodCycle.getApplicablePlotAgriculturalProcesss();
                if (agriculturalProcesses == null || agriculturalProcesses.isEmpty()) {
                    phaseContext.formatInfo("L'occupation du sol ayant pour identifiant : '%s', parcelle : '%s', possède un cycle de production sans intervention",
                            formatSolOccupation(phaseContext), formatPlot(phaseContext));
                } else {
                    processCropCyclePhaseInterventions(phaseContext, agriculturalProcesses);
                }
            }

        }
    }

    protected void processCropCyclePhaseInterventions(EdaplosContext phaseContext, List<PlotAgriculturalProcess> agriculturalProcesses) {

        for (PlotAgriculturalProcess agriculturalProcess : agriculturalProcesses) {
            String eInterId = agriculturalProcess.getIdentification();
            if (StringUtils.isEmpty(eInterId)) {
                phaseContext.formatError("Une intervention portant sur l'occupation du sol '%s', parcelle '%s', domaine '%s' ne possède pas d'identifiant. Import Impossible",
                        formatSolOccupation(phaseContext), formatPlot(phaseContext), formatDomain(phaseContext));
                eInterId = "id_inter_a_completer_" + randomGenerator.nextInt(10000);
            }

            processCropCycleNewIntervention(phaseContext, agriculturalProcess, eInterId);
        }
    }

    protected EffectiveIntervention processCropCycleNewIntervention(EdaplosContext phaseOrNodeContext,
                                                                    PlotAgriculturalProcess agriculturalProcess, String eInterId) {

        if (phaseOrNodeContext.getIssuerIds().contains(eInterId)) {
            // l' intervention existe, ont l' ignore seulement (sans message)
            return null;
        }

        // context stuff
        Domain domain = phaseOrNodeContext.getDomain();
        CroppingPlanEntry croppingPlanEntry = phaseOrNodeContext.getCroppingPlanEntry();
        phaseOrNodeContext.setPlotAgriculturalProcess(agriculturalProcess);

        EffectiveIntervention effectiveIntervention = effectiveInterventionDao.newInstance();
        effectiveIntervention.setEdaplosIssuerId(eInterId);

        // handle comment
        addInterventionComment(agriculturalProcess, effectiveIntervention);

        addInterventionName(phaseOrNodeContext, agriculturalProcess, effectiveIntervention);

        // not complete species stades
        addSpeciesToIntervention(effectiveIntervention, croppingPlanEntry);

        // handle dates
        addInterventionDate(phaseOrNodeContext, agriculturalProcess, domain, effectiveIntervention);

        String interSubordinateType = agriculturalProcess.getSubordinateTypeCode();
        if (StringUtils.isNotEmpty(interSubordinateType)) {
            if (!interSubordinateType.equals(PlotAgriculturalProcess.TYPE_EVENEMENT_REALISE)) {
                // if subordinate type of intervention is not 'done', then no need to parse it
                phaseOrNodeContext.formatInfo("Sur la parcelle '%s', occupation du sol '%s', l'intervention '%s' a un qualifiant autre que 'réalisé' '%s'. "
                                + "Cette information n'étant pas présente dans Agrosyst, l'intervention ne sera pas importée.",
                        formatPlot(phaseOrNodeContext), formatSolOccupation(phaseOrNodeContext), formatIntervention(effectiveIntervention), interSubordinateType);
                return null;
            }
        }

        // Handle spatial frequency
        addInterventionSpacialFrequency(phaseOrNodeContext, agriculturalProcess, effectiveIntervention);

        // trip number
        parseTriNumberAndSetTransitCount(phaseOrNodeContext, agriculturalProcess, effectiveIntervention);

        // débit de chantier
        addInterventionWorkRate(phaseOrNodeContext, agriculturalProcess, effectiveIntervention);

        // Manage equipments
        List<SpecifiedAgriculturalDevice> usedAgriculturalDevices = agriculturalProcess.getUsedSpecifiedAgriculturalDevices();
        final EdaplosContext interventionContext = phaseOrNodeContext.ofEffectiveIntervention(effectiveIntervention);
        Map<String, Equipment> equipments = processInterventionEquipments(interventionContext, usedAgriculturalDevices);
        Map<String, SpecifiedAgriculturalDevice> notFoundAgriculturalDevices = getNotFoundMaterials(phaseOrNodeContext, usedAgriculturalDevices, equipments);

        List<RefInterventionAgrosystTravailEDI> mainActions = new ArrayList<>();

        // Listing of interventions type for the current intervention
        Map<AgrosystInterventionType, AbstractAction> interventionActions = new HashMap<>();

        AgrosystInterventionType firstNoneOtherAgrosystInterventionType = null;
        firstNoneOtherAgrosystInterventionType = importActions(
                phaseOrNodeContext,
                effectiveIntervention,
                interventionContext,
                agriculturalProcess.getPerformedAgriculturalProcessWorkItems(),
                mainActions,
                interventionActions,
                firstNoneOtherAgrosystInterventionType);

        // set intervention type according to the action type if exists (take first one if many) otherwise set default value
        AgrosystInterventionType agrosystInterventionType = firstNoneOtherAgrosystInterventionType == null ? AgrosystInterventionType.AUTRE : firstNoneOtherAgrosystInterventionType;
        effectiveIntervention.setType(agrosystInterventionType);
        effectiveIntervention.setEffectiveCropCyclePhase(phaseOrNodeContext.getEffectivePhase());
        effectiveIntervention.setEffectiveCropCycleNode(phaseOrNodeContext.getEffectiveNode());
        effectiveIntervention = effectiveInterventionDao.create(effectiveIntervention);
        phaseOrNodeContext.getIssuerIds().add(eInterId);

        Map<RefInterventionAgrosystTravailEDI, List<AbstractAction>> mainActionsToActions = getmRefMainActionsToActions(effectiveIntervention, interventionActions);

        // input
        firstNoneOtherAgrosystInterventionType = importInputAndUsages(
                agriculturalProcess,
                interventionContext,
                interventionActions,
                firstNoneOtherAgrosystInterventionType);

        firstNoneOtherAgrosystInterventionType = importHarvestingAction(
                agriculturalProcess,
                interventionContext,
                interventionActions,
                firstNoneOtherAgrosystInterventionType);

        Collection<AbstractAction> abstractActions = valideInterventionActionPresence(phaseOrNodeContext, effectiveIntervention, interventionActions);
        
        /*
          Par default, les interventions ont le type "AUTRE", mais suite à la création des actions, on peut corriger
          ce type pour être cohérent.
         */
        if (AgrosystInterventionType.AUTRE == effectiveIntervention.getType() && firstNoneOtherAgrosystInterventionType != null) {
            effectiveIntervention.setType(firstNoneOtherAgrosystInterventionType);
        }

        addInterventionToolsCoupling(
                effectiveIntervention,
                interventionContext,
                equipments,
                notFoundAgriculturalDevices,
                mainActions,
                mainActionsToActions);

        // mise à jour des actions après les avoir modifiées en actions principales.
        abstractActionDao.createAll(abstractActions);

        importInterventionSpeciesStades(
                phaseOrNodeContext,
                agriculturalProcess,
                croppingPlanEntry,
                effectiveIntervention.getSpeciesStades());

        effectiveIntervention = effectiveInterventionDao.update(effectiveIntervention);

        // on actualise la collection de d'intrants
        phaseOrNodeContext.setDomainInputsByTypes(interventionContext.domainInputsByTypes);

        return effectiveIntervention;
    }

    protected static void addInterventionName(EdaplosContext phaseOrNodeContext, PlotAgriculturalProcess agriculturalProcess, EffectiveIntervention effectiveIntervention) {
        String plotTypeCode = agriculturalProcess.getTypeCode();
        if (StringUtils.isNotEmpty(plotTypeCode)) {
            String eInterTypeName = PlotAgriculturalProcess.INTERVENTION_NAME_PER_TYPE_CODE.get(plotTypeCode);
            effectiveIntervention.setName(eInterTypeName);
        }

        if (StringUtils.isBlank(effectiveIntervention.getName())) {
            effectiveIntervention.setName("Intervention");
            phaseOrNodeContext.formatInfo("Le type d'intervention '%s' n'est pas connu par Agrosyst. L'intervention importée aura pour nom par défaut 'Intervention'. Vous pourrez le corriger après import.",
                    plotTypeCode);
        }

        effectiveIntervention.setName(StringUtils.isEmpty((effectiveIntervention.getName())) ? effectiveIntervention.getType().name() : effectiveIntervention.getName());
    }

    protected static void addInterventionComment(
            PlotAgriculturalProcess agriculturalProcess,
            EffectiveIntervention effectiveIntervention) {

        String description = agriculturalProcess.getDescription();
        effectiveIntervention.setComment(description);

        List<AgriculturalProcessReason> specifiedReasons = agriculturalProcess.getSpecifiedAgriculturalProcessReasons();
        if (CollectionUtils.isNotEmpty(specifiedReasons)) {
            for (AgriculturalProcessReason specifiedReason : specifiedReasons) {
                String interReasonType = specifiedReason.getType();
                String interReasonComment = specifiedReason.getDescription();
                if (StringUtils.isNotEmpty(interReasonType)) {
                    effectiveIntervention.setComment(effectiveIntervention.getComment() + interReasonType + ". ");
                }
                if (StringUtils.isNotEmpty(interReasonComment)) {
                    effectiveIntervention.setComment(effectiveIntervention.getComment() + interReasonComment + ". ");
                }
                // TODO eancelet 2016-06-20 : modifier interReasonType du code à l'intitulé
            }

        }
    }

    protected void addInterventionDate(
            EdaplosContext phaseOrNodeContext,
            PlotAgriculturalProcess agriculturalProcess,
            Domain domain,
            EffectiveIntervention effectiveIntervention) {

        DelimitedPeriod eDelimitedPeriod;
        LocalDate startingDate;
        LocalDate endingDate;
        List<DelimitedPeriod> occurrenceDelimitedPeriods = agriculturalProcess.getOccurrenceDelimitedPeriods();
        int year = domain.getCampaign();
        if (CollectionUtils.isEmpty(occurrenceDelimitedPeriods)) {
            phaseOrNodeContext.formatInfo("Aucune période d'intervention n'a été renseignée pour l'intervention '%s' sur occupation du sol '%s', parcelle '%s',  domaine '%s'. Elle sera mise par défaut au 01/01.",
                    formatIntervention(effectiveIntervention), formatSolOccupation(phaseOrNodeContext), formatPlot(phaseOrNodeContext), formatDomain(phaseOrNodeContext));
            // Par défaut : 01/01 de l'année courante
            startingDate = LocalDate.of(year, 1, 1);
            endingDate = LocalDate.of(year, 1, 1);

        } else {
            if (occurrenceDelimitedPeriods.size() > 1) {
                phaseOrNodeContext.formatInfo("Plusieurs périodes d'intervention ont été déclarées pour  l'intervention '%s' sur occupation du sol '%s', parcelle '%s',  domaine '%s'. Seule la première a été retenue.",
                        formatIntervention(effectiveIntervention), formatSolOccupation(phaseOrNodeContext), formatPlot(phaseOrNodeContext), formatDomain(phaseOrNodeContext));
            }
            eDelimitedPeriod = occurrenceDelimitedPeriods.getFirst();

            String startingDateString = eDelimitedPeriod.getStartDateTime();
            String endingDateString = eDelimitedPeriod.getEndDateTime();
            if (StringUtils.isEmpty(startingDateString)) {
                phaseOrNodeContext.formatInfo("Aucune date de début d'intervention n'a été renseignée pour l'intervention '%s' sur occupation du sol '%s', parcelle '%s',  domaine '%s'. Le 01/01 sera utilisé par défaut.",
                        formatIntervention(effectiveIntervention), formatSolOccupation(phaseOrNodeContext), formatPlot(phaseOrNodeContext), formatDomain(phaseOrNodeContext));
                startingDate = LocalDate.of(year, 1, 1);
            } else {
                startingDate = EdaplosUtils.parseInterventionDate(startingDateString);
                if (startingDate == null) {
                    phaseOrNodeContext.formatInfo("La date de début d'intervention '%s' n'est pas au bon format:(" + EdaplosUtils.ALLOWED_FORMATS + ") pour l'intervention '%s' sur occupation du sol '%s', parcelle '%s',  domaine '%s'. Le 01/01 sera utilisé par défaut.",
                            startingDateString, formatIntervention(effectiveIntervention), formatSolOccupation(phaseOrNodeContext), formatPlot(phaseOrNodeContext), formatDomain(phaseOrNodeContext));
                    startingDate = LocalDate.of(year, 1, 1);
                }
            }
            if (StringUtils.isEmpty(endingDateString)) {
                phaseOrNodeContext.formatInfo("Aucune date de fin d'intervention n'a été renseignée pour l'intervention '%s' sur occupation du sol '%s', parcelle '%s',  domaine '%s'. La date de début sera utilisée par défaut.",
                        formatIntervention(effectiveIntervention), formatSolOccupation(phaseOrNodeContext), formatPlot(phaseOrNodeContext), formatDomain(phaseOrNodeContext));
                endingDate = startingDate;
            } else {
                endingDate = EdaplosUtils.parseInterventionDate(endingDateString);
                if (endingDate == null) {
                    phaseOrNodeContext.formatInfo("La date de fin d'intervention '%s' n'est pas au bon format:(" + EdaplosUtils.ALLOWED_FORMATS + ") pour l'intervention '%s' sur occupation du sol '%s', parcelle '%s',  domaine '%s'. Le 01/01 sera utilisé par défaut.",
                            endingDateString, formatIntervention(effectiveIntervention), formatSolOccupation(phaseOrNodeContext), formatPlot(phaseOrNodeContext), formatDomain(phaseOrNodeContext));
                    endingDate = LocalDate.of(year, 1, 1);
                }
            }
        }

        effectiveIntervention.setStartInterventionDate(startingDate);
        effectiveIntervention.setEndInterventionDate(endingDate);
    }

    protected void addInterventionWorkRate(EdaplosContext phaseOrNodeContext, PlotAgriculturalProcess agriculturalProcess, EffectiveIntervention effectiveIntervention) {
        TechnicalCharacteristic debitChantierCharacteristique = agriculturalProcess.getApplicableTechnicalCharacteristics().stream()
                .filter(c -> TechnicalCharacteristic.TYPE_CODE_DEBIT_CHANTIER.equals(c.getTypeCode()))
                .findFirst().orElse(null);
        if (debitChantierCharacteristique != null) {
            if (NumberUtils.isCreatable(debitChantierCharacteristique.getValueMeasure())) {
                effectiveIntervention.setWorkRate(Double.parseDouble(debitChantierCharacteristique.getValueMeasure()));
                effectiveIntervention.setWorkRateUnit(MaterielWorkRateUnit.H_HA);
            } else {
                phaseOrNodeContext.formatWarning("Le débit de chantier incorrect pour l'intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s'. Valeur ignorée.",
                        formatIntervention(effectiveIntervention), formatSolOccupation(phaseOrNodeContext), formatPlot(phaseOrNodeContext), formatDomain(phaseOrNodeContext));

            }
        }
    }

    protected void addInterventionSpacialFrequency(EdaplosContext phaseOrNodeContext, PlotAgriculturalProcess agriculturalProcess, EffectiveIntervention effectiveIntervention) {
        AgriculturalArea surfaceTravaillee = agriculturalProcess.getOccurrenceAgriculturalAreas().stream()
                .filter(area -> AgriculturalArea.TYPE_SURFACE_TRAVAILLEE.equals(area.getTypeCode()))
                .findFirst().orElse(null);
        double spatialFrequency = 1;
        if (surfaceTravaillee != null) {
            if (StringUtils.isBlank(surfaceTravaillee.getSpatialFrequency())) {
                phaseOrNodeContext.formatError("La surface concernée par l'intervention '%s' sur occupation du sol '%s', parcelle '%s', domaine '%s' est incorrecte. Import impossible.",
                        formatIntervention(effectiveIntervention), formatSolOccupation(phaseOrNodeContext), formatPlot(phaseOrNodeContext), formatDomain(phaseOrNodeContext));
            } else {
                try {
                    spatialFrequency = Double.parseDouble(surfaceTravaillee.getSpatialFrequency());
                    if (spatialFrequency < 0 || spatialFrequency > 100) {
                        phaseOrNodeContext.formatInfo("La surface concernée par l'intervention '%s' portant sur occupation du sol '%s', parcelle '%s', domaine '%s' doit être comprise entre 0 et 100.",
                                formatIntervention(effectiveIntervention), formatSolOccupation(phaseOrNodeContext), formatPlot(phaseOrNodeContext), formatDomain(phaseOrNodeContext));
                        spatialFrequency = 1;
                    } else {
                        spatialFrequency = spatialFrequency / 100d;
                    }
                } catch (NumberFormatException ex) {
                    phaseOrNodeContext.formatError("La surface concernée par l'intervention '%s' sur occupation du sol '%s', parcelle '%s', domaine '%s' est incorrecte. Import impossible.",
                            formatIntervention(effectiveIntervention), formatSolOccupation(phaseOrNodeContext), formatPlot(phaseOrNodeContext), formatDomain(phaseOrNodeContext));
                }
            }
        }
        effectiveIntervention.setSpatialFrequency(spatialFrequency);
    }

    protected void addSpeciesToIntervention(
            EffectiveIntervention effectiveIntervention,
            CroppingPlanEntry croppingPlanEntry) {

        List<CroppingPlanSpecies> croppingPlanSpecies = croppingPlanEntry.getCroppingPlanSpecies();
        List<EffectiveSpeciesStade> effectiveSpeciesStades = croppingPlanSpecies.stream()
                .map(species -> {
                    EffectiveSpeciesStade effectiveSpeciesStade = effectiveSpeciesStadeDao.newInstance();
                    effectiveSpeciesStade.setCroppingPlanSpecies(species);
                    return effectiveSpeciesStade;
                })
                .collect(Collectors.toList());
        effectiveIntervention.addAllSpeciesStades(effectiveSpeciesStades);
    }

    protected void importInterventionSpeciesStades(
            EdaplosContext phaseOrNodeContext,
            PlotAgriculturalProcess agriculturalProcess,
            CroppingPlanEntry croppingPlanEntry,
            Collection<EffectiveSpeciesStade> effectiveSpeciesStades) {

        List<AgriculturalProcessCropStage> specifiedAgriculturalProcessCropStages = agriculturalProcess.getSpecifiedAgriculturalProcessCropStages();
        if (CollectionUtils.isNotEmpty(specifiedAgriculturalProcessCropStages)) {
            AgriculturalProcessCropStage specifiedAgriculturalProcessCropStage = specifiedAgriculturalProcessCropStages.getFirst();
            String stadeTypeCode = specifiedAgriculturalProcessCropStage.getTypeCode();
            String startCropStage = specifiedAgriculturalProcessCropStage.getStartCropStage();
            String endCropStage = specifiedAgriculturalProcessCropStage.getEndCropStage();

            RefStadeEDI minStadeEdi = null;
            RefStadeEDI maxStadeEdi = null;
            if (StringUtils.isNotBlank(startCropStage)) {
                minStadeEdi = refStadeEDIDao.forAeeEquals(startCropStage).findUniqueOrNull();
            }
            if (minStadeEdi == null && StringUtils.isNotBlank(stadeTypeCode)) {
                minStadeEdi = refStadeEDIDao.forAeeEquals(stadeTypeCode).findUniqueOrNull();
            }
            if (StringUtils.isNotBlank(endCropStage)) {
                maxStadeEdi = refStadeEDIDao.forAeeEquals(endCropStage).findUniqueOrNull();
            }
            if (maxStadeEdi == null) {
                maxStadeEdi = minStadeEdi;
            }

            if (minStadeEdi == null) {
                phaseOrNodeContext.formatInfo("Impossible de trouver les stades de cultures '%s', et '%s' de la culture '%s'. " +
                                "Ils ne seront pas importés dans Agrosyst. Vous pouvez les ajouter manuellement après import.",
                        stadeTypeCode, startCropStage, croppingPlanEntry.getName());
            } else {
                for (EffectiveSpeciesStade effectiveSpeciesStade : effectiveSpeciesStades) {
                    CroppingPlanSpecies stadeSpecies = effectiveSpeciesStade.getCroppingPlanSpecies();
                    String profilVegetatifBbch = stadeSpecies.getSpecies().getProfil_vegetatif_BBCH();
                    if (profilVegetatifBbch.equals(String.valueOf(minStadeEdi.getProfil_vegetatif()))) {
                        effectiveSpeciesStade.setMinStade(minStadeEdi);
                    }
                    if (profilVegetatifBbch.equals(String.valueOf(maxStadeEdi.getProfil_vegetatif()))) {
                        effectiveSpeciesStade.setMaxStade(maxStadeEdi);
                    }
                }
            }
        }
    }

    protected void addInterventionToolsCoupling(EffectiveIntervention effectiveIntervention, EdaplosContext interventionContext, Map<String, Equipment> equipments, Map<String, SpecifiedAgriculturalDevice> notFoundAgriculturalDevices, List<RefInterventionAgrosystTravailEDI> mainActions, Map<RefInterventionAgrosystTravailEDI, List<AbstractAction>> mainActionsToActions) {
        // Management ToolsCoupling : One to regroup all equipments (need to extract tractor from equipments if there is one)
        // Make after actions management cause we need to attache main actions
        if (MapUtils.isNotEmpty(equipments)) {
            ToolsCoupling toolsCoupling = processInterventionToolsCoupling(interventionContext, equipments.values(), notFoundAgriculturalDevices);
            if (toolsCoupling != null) {
                // Si la CO n'a pas de mainAction, ajout de la liste deja parsée sur les workItems de l'intervention.
                if (toolsCoupling.getMainsActions() == null || toolsCoupling.getMainsActions().isEmpty()) {
                    if (!mainActions.isEmpty()) {
                        toolsCoupling.addAllMainsActions(mainActions);
                    } else {
                        // Si l'intervention n'a pas de mainAction (car pas de travail Edi), on ajoute celui par défaut (ZTC, "Autre type de travail").
                        RefInterventionAgrosystTravailEDI defaultTravailEDI = getDefaultTravailEDI();
                        toolsCoupling.addMainsActions(defaultTravailEDI);
                    }

                    // Si la CO a déjà des mainAction (car créée avant le parsage de cette intervention),
                    // ajout des mainActions qui existent sur l'intervention mais pas sur la CO (= nouvelles mainActions).
                } else {

                    // sur une CO il ne peut y avoir qu'une mainAction (RefInterventionAgrosystTravailEDI) par type d'intervention (colonne intervention_agrosyst de RefInterventionAgrosystTravailEDI).
                    for (RefInterventionAgrosystTravailEDI mainAction : mainActions) {
                        boolean canAddMainAction = true;
                        for (RefInterventionAgrosystTravailEDI toolsCouplingMainAction : toolsCoupling.getMainsActions()) {
                            final AgrosystInterventionType toolsCouplingInterventionType = toolsCouplingMainAction.getIntervention_agrosyst();
                            if (mainAction.getIntervention_agrosyst() == toolsCouplingInterventionType
                                    && !mainAction.equals(toolsCouplingMainAction)
                                    // Les mainactions de type "ENTRETIEN_TAILLE_VIGNE_ET_VERGER" et "AUTRE" ne suivent pas la règle ci-dessus.
                                    // Dans ce cas là, on peut ajouter autant de mainsactions de type "ENTRETIEN_TAILLE_VIGNE_ET_VERGER"
                                    // que l'on veut sans en mettre en double cependant
                                    && !TYPES_ALLOWING_3_ACTIONS.contains(toolsCouplingInterventionType)
                            ) {
                                canAddMainAction = false;
                                break;
                            }
                        }
                        if (canAddMainAction) {
                            toolsCoupling.addMainsActions(mainAction);
                        }
                        // Premiere version du #8852 disait également :
                        // De ce fait, si par exemple, dans l'import eDaplos on a une intervention avec du matériel
                        // et une action de semis direct (agrosyst_intervention = SEMIS) et une intervention
                        // sur le même domaine avec exactement le même matériel mais une action de semis de précision (agrosyst_intervention = SEMIS)
                        // et bien il faudra créer une 2ème CO avec les même équipements (les équipements ne sont pas à recréer, ils serviront aux 2 CO)
                        // que l'on lie aux actions de cette 2ème intervention.
                    }
                }

                effectiveIntervention.addToolCouplings(toolsCoupling);

                // Définie les actions comme étant des actions principales à partir du moment ou elles partagent l'action principale: RefInterventionAgrosystTravailEDI avec une combinaison d'outils.
                // l'association est alors faite en affectant à l'action le code de la combinaison d'outil.
                for (RefInterventionAgrosystTravailEDI mainsAction : toolsCoupling.getMainsActions()) {
                    List<AbstractAction> actionsForMainAction = mainActionsToActions.get(mainsAction);
                    if (CollectionUtils.isNotEmpty(actionsForMainAction)) {
                        for (AbstractAction abstractAction : actionsForMainAction) {
                            abstractAction.setToolsCouplingCode(toolsCoupling.getCode());
                        }
                    }
                }
            }
        }
    }

    protected AgrosystInterventionType importActions(EdaplosContext phaseOrNodeContext,
                                                     EffectiveIntervention effectiveIntervention,
                                                     EdaplosContext interventionContext,
                                                     List<AgriculturalProcessWorkItem> processWorkItems,
                                                     List<RefInterventionAgrosystTravailEDI> mainActions,
                                                     Map<AgrosystInterventionType, AbstractAction> interventionActions,
                                                     AgrosystInterventionType firstNoneOtherAgrosystInterventionType) {
        if (processWorkItems != null && !processWorkItems.isEmpty()) {
            for (AgriculturalProcessWorkItem workItem : processWorkItems) {
                AbstractAction action = null;
                String eActionType = workItem.getTypeCode();
                if (StringUtils.isEmpty(eActionType)) {
                    phaseOrNodeContext.formatError("Sur la parcelle '%s', occupation du sol '%s', l'intervention '%s' a un travail sans typeCode. Import impossible.",
                            formatPlot(phaseOrNodeContext), formatSolOccupation(phaseOrNodeContext), formatIntervention(effectiveIntervention));
                } else {
                    RefInterventionAgrosystTravailEDI travailEdi = refInterventionAgrosystTravailEDIDao.forReference_codeEquals(eActionType).findUniqueOrNull();
                    if (travailEdi == null) {
                        phaseOrNodeContext.formatError("L'intervention '%s' portant sur l'occupation du sol '%s', parcelle '%s', domaine '%s' a un typecode non reconnu. Import impossible",
                                formatIntervention(effectiveIntervention), formatSolOccupation(phaseOrNodeContext), formatPlot(phaseOrNodeContext), formatDomain(phaseOrNodeContext));

                        // Deux actions d'un meme type non permis, sauf si Entretien vigne-verger, Travail du sol, et Autre
                    } else if (interventionActions.containsKey(travailEdi.getIntervention_agrosyst()) &&
                            !TYPES_ALLOWING_3_ACTIONS.contains(travailEdi.getIntervention_agrosyst())) {
                        phaseOrNodeContext.formatError("L'intervention '%s' portant sur occupation du sol '%s' parcelle '%s', domaine '%s' a plusieurs actions de même type '%s' ce qui n'est pas autorisé dans Agrosyst. Import impossible",
                                formatIntervention(effectiveIntervention), formatSolOccupation(phaseOrNodeContext), formatPlot(phaseOrNodeContext), formatDomain(phaseOrNodeContext), travailEdi.getIntervention_agrosyst());
                    } else {
                        mainActions.add(travailEdi);
                        AgrosystInterventionType actionType = travailEdi.getIntervention_agrosyst();

                        if (AgrosystInterventionType.AUTRE != actionType) {
                            firstNoneOtherAgrosystInterventionType = firstNoneOtherAgrosystInterventionType == null ? actionType : firstNoneOtherAgrosystInterventionType;
                        }
                        try {
                            if (actionType.equals(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX)) {
                                action = createNewMineralFertilizersSpreadingActionWithDefaultValues(effectiveIntervention, travailEdi, workItem.getDescription());
                            } else if (actionType.equals(APPLICATION_DE_PRODUITS_PHYTOSANITAIRES)) {
                                action = createNewPhytoActionWithDefaultValues(effectiveIntervention, travailEdi, workItem.getDescription(), interventionContext);
                            } else if (actionType.equals(AgrosystInterventionType.AUTRE)) {
                                action = createNewOtherActionActionWithDefaultValues(effectiveIntervention, travailEdi, workItem.getDescription());
                            } else if (actionType.equals(AgrosystInterventionType.ENTRETIEN_TAILLE_VIGNE_ET_VERGER)) {
                                action = createNewMaintenancePruningVinesActionWithDefaultValues(effectiveIntervention, travailEdi, workItem.getDescription());
                            } else if (actionType.equals(AgrosystInterventionType.EPANDAGES_ORGANIQUES)) {
                                action = createNewOrganicFertilizersSpreadingActionWithDefaultValues(effectiveIntervention, travailEdi, workItem.getDescription());
                            } else if (actionType.equals(AgrosystInterventionType.IRRIGATION)) {
                                action = createNewIrrigationActionWithDefaultValues(effectiveIntervention, interventionContext, travailEdi, workItem.getDescription());
                            } else if (actionType.equals(AgrosystInterventionType.LUTTE_BIOLOGIQUE)) {
                                action = createNewBiologicalControlActionWithDefaultValues(effectiveIntervention, travailEdi, workItem.getDescription(), interventionContext);
                                double treatedSurface = getPhytoTreatedSurface(effectiveIntervention, interventionContext);
                                ((BiologicalControlAction) action).setProportionOfTreatedSurface(treatedSurface);
                            } else if (actionType.equals(AgrosystInterventionType.RECOLTE)) {
                                action = createNewHarvestingActionWithDefaultValues(effectiveIntervention, travailEdi, workItem.getDescription());
                            } else if (actionType.equals(SEMIS)) {
                                action = createNewSeedingActionUsageWithDefaultValues(interventionContext, "SEX", travailEdi, workItem.getDescription());
                            } else if (actionType.equals(AgrosystInterventionType.TRANSPORT)) {
                                action = createNewCarriageActionWithDefaultValues(effectiveIntervention, travailEdi, workItem.getDescription());
                            } else if (actionType.equals(AgrosystInterventionType.TRAVAIL_DU_SOL)) {
                                action = createNewTillageActionWithDefaultValues(effectiveIntervention, travailEdi, workItem.getDescription());
                            }

                            if (action != null) {
                                // Keep the phyto actions for later, when processing input
                                interventionActions.put(actionType, action);
                            } else {
                                phaseOrNodeContext.formatError("Sur la parcelle '%s', occupation du sol '%s', l'intervention '%s' a un travail dont le type n'est pas géré par l'import '%s'. Import impossible, ceci doit être géré par l'équipe Agosyst.",
                                        formatPlot(phaseOrNodeContext), formatSolOccupation(phaseOrNodeContext), formatIntervention(effectiveIntervention), actionType.toString());
                            }
                        } catch (Exception e) {
                            phaseOrNodeContext.formatError("Sur la parcelle '%s', occupation du sol '%s', l'intervention '%s' a un travail dont le type n'est pas compatible avec l'action principale par l'import '%s'. Import impossible, ceci doit être géré par l'équipe Agosyst.",
                                    formatPlot(phaseOrNodeContext), formatSolOccupation(phaseOrNodeContext), formatIntervention(effectiveIntervention), actionType.toString());
                        }
                    }
                }
            }
        }
        return firstNoneOtherAgrosystInterventionType;
    }

    protected static Map<RefInterventionAgrosystTravailEDI, List<AbstractAction>> getmRefMainActionsToActions(EffectiveIntervention effectiveIntervention, Map<AgrosystInterventionType, AbstractAction> interventionActions) {
        Map<RefInterventionAgrosystTravailEDI, List<AbstractAction>> mainActionsToActions = new HashMap<>();

        for (Map.Entry<AgrosystInterventionType, AbstractAction> actionsByTypes : interventionActions.entrySet()) {
            AbstractAction aa = actionsByTypes.getValue();
            aa.setEffectiveIntervention(effectiveIntervention);
            //AbstractAction action = abstractActionDao.create(aa);
            // replace action by the saved one
            actionsByTypes.setValue(aa);
            List<AbstractAction> actionsForMainAction = mainActionsToActions.computeIfAbsent(aa.getMainAction(), k -> Lists.newArrayList());
            actionsForMainAction.add(aa);
        }
        return mainActionsToActions;
    }

    protected Collection<AbstractAction> valideInterventionActionPresence(EdaplosContext phaseOrNodeContext, EffectiveIntervention effectiveIntervention, Map<AgrosystInterventionType, AbstractAction> interventionActions) {
        // Make this check here, because some actions can be created with inputs if not present
        Collection<AbstractAction> abstractActions = interventionActions.values();
        if (abstractActions.isEmpty()) {
            phaseOrNodeContext.formatError("Sur la parcelle '%s', occupation du sol '%s', l'intervention '%s' n'a aucun travail associé. Import impossible.",
                    formatPlot(phaseOrNodeContext), formatSolOccupation(phaseOrNodeContext), formatIntervention(effectiveIntervention));
        }
        return abstractActions;
    }

    protected AgrosystInterventionType importHarvestingAction(
            PlotAgriculturalProcess agriculturalProcess,
            EdaplosContext interventionContext,
            Map<AgrosystInterventionType, AbstractAction> interventionActions,
            AgrosystInterventionType firstNoneOtherAgrosystInterventionType) {

        // harvesting
        List<AgriculturalProduce> harvestedAgriculturalProduces = agriculturalProcess.getHarvestedAgriculturalProduces();
        if (CollectionUtils.isEmpty(harvestedAgriculturalProduces)) {
            if (interventionActions.containsKey(AgrosystInterventionType.RECOLTE)) {
                processAgriculturalProduce(interventionContext, null, interventionActions);
                if (firstNoneOtherAgrosystInterventionType == null)
                    firstNoneOtherAgrosystInterventionType = AgrosystInterventionType.RECOLTE;
            }
        } else {
            for (AgriculturalProduce harvestedAgriculturalProduce : harvestedAgriculturalProduces) {
                processAgriculturalProduce(interventionContext, harvestedAgriculturalProduce, interventionActions);
                if (firstNoneOtherAgrosystInterventionType == null)
                    firstNoneOtherAgrosystInterventionType = AgrosystInterventionType.RECOLTE;
            }
        }
        return firstNoneOtherAgrosystInterventionType;
    }

    protected AgrosystInterventionType importInputAndUsages(
            PlotAgriculturalProcess agriculturalProcess,
            EdaplosContext interventionContext,
            Map<AgrosystInterventionType, AbstractAction> interventionActions,
            AgrosystInterventionType firstNoneOtherAgrosystInterventionType) {

        List<AgriculturalProcessCropInput> cropInputs = new ArrayList<>(ListUtils.emptyIfNull(agriculturalProcess.getUsedAgriculturalProcessCropInputs()));
        LinkedHashMap<String, AgrosystInterventionType> typeCodeToAgrosystInterventionType = new LinkedHashMap<>();
        cropInputs.stream().filter(cpi -> cpi.getTypeCode() != null)
                .forEach(ci -> {
                    String typeCode = ci.getTypeCode();
                    RefInterventionTypeItemInputEDI refInterventionTypeItemInputEDI = refInterventionTypeItemInputEDIDao.forInputEdiTypeCodeEquals(typeCode).findAnyOrNull();
                    AgrosystInterventionType interventionType = refInterventionTypeItemInputEDI != null ? refInterventionTypeItemInputEDI.getInterventionType() : AgrosystInterventionType.AUTRE;
                    typeCodeToAgrosystInterventionType.put(typeCode, interventionType);
                });
        for (Map.Entry<String, AgrosystInterventionType> codeToAgrosystInterventionType : typeCodeToAgrosystInterventionType.entrySet()) {
            if (AgrosystInterventionType.AUTRE != codeToAgrosystInterventionType.getValue()) {
                firstNoneOtherAgrosystInterventionType = codeToAgrosystInterventionType.getValue();
                break;
            }
        }

        processSeedInputs(
                interventionContext,
                interventionActions,
                cropInputs);

        cropInputs.forEach(
                cropInput ->
                        processCropInput(interventionContext, cropInput, interventionActions));

        if (interventionContext.isIncorrectSeedingUnit) {
            interventionContext.isIncorrectSeedingUnit = false;
            List<UnitCode> wrongUnitCodes = interventionContext.getIncorrectUnits();
            for (UnitCode wrongUnitCode : wrongUnitCodes) {
                interventionContext.formatError("L'unité %s utilisée pour déclarer la densité de semis/plantation sur l'intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' n'est pas autorisée par Agrosyst. Merci de remplacer celle ci par une unité universelle dans votre logiciel pour pouvoir importer.",
                        wrongUnitCode, formatIntervention(interventionContext), formatSolOccupation(interventionContext), formatPlot(interventionContext), formatDomain(interventionContext));
            }
        }

        return firstNoneOtherAgrosystInterventionType;
    }

    protected void processSeedInputs(
            EdaplosContext interventionContext,
            Map<AgrosystInterventionType, AbstractAction> interventionActions,
            List<AgriculturalProcessCropInput> cropInputs) {

        List<AgriculturalProcessCropInput> seedsOrPlans = cropInputs.stream().filter(
                cropInput -> (AgriculturalProcessCropInput.SEMENCE.equals(cropInput.getTypeCode()) || AgriculturalProcessCropInput.PLANTS.equals(cropInput.getTypeCode()))
        ).toList();

        List<AgriculturalProcessCropInput> seedingProductInputs = cropInputs.stream().filter(
                cropInput -> AgriculturalProcessCropInput.TRAITEMENT_SEMENCE.equals(cropInput.getTypeCode())
        ).toList();

        if (seedsOrPlans.isEmpty() && seedingProductInputs.isEmpty()) return;

        InterventionPhytoProductContext interventionPhytoProductContext = getInterventionPhytoProductContext(interventionContext, seedingProductInputs);

        List<AgriculturalProcessCropInput> sortedSeedsOrPlans = getSortedSeedsOrPlans(interventionContext, seedsOrPlans);

        Set<SeedPlantUnit> seedingActionUsageUnits = getInterventionSpeciesCropByUsageUnits(interventionContext, sortedSeedsOrPlans);

        Collection<DomainSeedLotInput> domainSeedLotInputs = getOrCreateDomainSeedLotInput(
                interventionContext,
                interventionPhytoProductContext.seedingActionUsageChemicalORBiologicalTraitment(),
                seedingActionUsageUnits);

        SeedingActionUsage seedingActionUsage = createSeedingActionUsage(interventionContext, interventionActions, domainSeedLotInputs);

        // add species Quantity

        // manage species quantity
        // refs #10495: si pas de variété présentes, on trouve plusieurs balises UsedAgriculturalProcessCropInput successives, la première valeur est appliquée à toutes les espèces du mélange
        // refs #11382: si des variétés sont présentes, récupérer chaque quantité semée et l'affecter à la variété correspondante en se basant sur la balise Identification de la déclaration de quantité semée :
        //
        //Exemple :
        //
        //déclaration de la variété sur la parcelle :
        //
        //<ram:SownCropSpeciesVariety>
        //<ram:TypeCode>512B370</ram:TypeCode>
        //<ram:Description><![CDATA[RENAN]]></ram:Description>
        //</ram:SownCropSpeciesVariety>
        //
        //déclaration de la quantité semée :
        //
        //<ram:UsedAgriculturalProcessCropInput>
        //<ram:Identification>512B370</ram:Identification>
        //<ram:TypeCode listAgencyName="FR, AGRO EDI EUROPE" listAgencyID="312">ZJF</ram:TypeCode>
        //<ram:Description><![CDATA[RENAN]]></ram:Description>
        //<ram:SubordinateTypeCode>ZQA</ram:SubordinateTypeCode>
        //<ram:AppliedSurfaceUnitValueMeasure unitCode="KGM">85.0000</ram:AppliedSurfaceUnitValueMeasure>
        //</ram:UsedAgriculturalProcessCropInput>

        addSeedSpeciesUsageQuantity(
                interventionContext,
                sortedSeedsOrPlans,
                domainSeedLotInputs,
                seedingActionUsage);

        // add products quantities
        addSeedProductQuantity(
                interventionContext,
                seedingProductInputs,
                interventionPhytoProductContext,
                seedingActionUsage);

        Collection<SeedLotInputUsage> seedLotInputUsages = seedingActionUsage.getSeedLotInputUsage();
        for (SeedLotInputUsage seedLotInputUsage : seedLotInputUsages) {
            DomainSeedLotInput seedLotInput = persistDomainSeedLot(seedLotInputUsage.getDomainSeedLotInput());
            seedLotInputUsage.setDomainSeedLotInput(seedLotInput);
        }

        cropInputs.removeAll(seedsOrPlans);
        cropInputs.removeAll(seedingProductInputs);
    }

    protected void addSeedProductQuantity(EdaplosContext interventionContext, List<AgriculturalProcessCropInput> seedingProductInputs, InterventionPhytoProductContext interventionPhytoProductContext, SeedingActionUsage seedingActionUsage) {
        if (CollectionUtils.isNotEmpty(seedingProductInputs)) {
            Collection<SeedLotInputUsage> seedLotInputUsages = CollectionUtils.emptyIfNull(seedingActionUsage.getSeedLotInputUsage());
            for (SeedLotInputUsage seedLotInputUsage : seedLotInputUsages) {
                processSeedCropInput(
                        interventionContext,
                        seedLotInputUsage.getDomainSeedLotInput(),
                        interventionPhytoProductContext,
                        seedLotInputUsage
                );
            }
        }
    }

    protected void addSeedSpeciesUsageQuantity(EdaplosContext interventionContext, List<AgriculturalProcessCropInput> sortedSeedsOrPlans, Collection<DomainSeedLotInput> domainSeedLotInputs, SeedingActionUsage seedingActionUsage) {
        Map<SeedPlantUnit, List<DomainSeedLotInput>> domainSeedLotInputsByUsageUnit = new HashMap<>();
        domainSeedLotInputs.forEach(dsli -> {
            List<DomainSeedLotInput> domainSeedLotInputList = domainSeedLotInputsByUsageUnit.computeIfAbsent(dsli.getUsageUnit(), k -> new ArrayList<>());
            domainSeedLotInputList.add(dsli);
        });

        for (AgriculturalProcessCropInput seedsOrPlan : sortedSeedsOrPlans) {
            RefInterventionTypeItemInputEDI refInterventionTypeItemInputEDI = refInterventionTypeItemInputEDIDao.forInputEdiTypeCodeEquals(seedsOrPlan.getTypeCode()).findAnyOrNull();
            if (refInterventionTypeItemInputEDI == null) {
                interventionContext.formatError("L'intrant déclaré sur la culture '%s', intervention '%s', parcelle '%s', domaine '%s', a un 'Typecode': '%s' qui n'est pas géré par Agrosyst. L'intrant ne sera pas importé. Merci de contacter l'équipe Agrosyst.",
                        formatCrop(interventionContext), formatIntervention(interventionContext.getEffectiveIntervention()), formatPlot(interventionContext), formatDomain(interventionContext), seedsOrPlan.getTypeCode());
                continue;
            }

            String edaplosValueReported = seedsOrPlan.getAppliedSurfaceUnitValueMeasure();

            if (NumberUtils.isCreatable(edaplosValueReported)) {
                double quantity = Double.parseDouble(edaplosValueReported);
                UnitCode unitCode = seedsOrPlan.getAppliedSurfaceUnitValueMeasureUnitCode();

                Collection<SeedLotInputUsage> currentSeedLotInputUsage = getSeedLotInputUsagesForCurrentSeed(
                        seedingActionUsage,
                        domainSeedLotInputsByUsageUnit,
                        unitCode);

                for (SeedLotInputUsage seedLotInputUsage : currentSeedLotInputUsage) {
                    addSeedingActionSpeciesInputs(interventionContext, seedLotInputUsage, quantity, unitCode, seedsOrPlan);
                }

            } else {
                edaplosValueReported = seedsOrPlan.getValueMeasure();
                if (NumberUtils.isCreatable(edaplosValueReported)) {
                    double divider = interventionContext.getEffectiveIntervention().getSpatialFrequency() * interventionContext.getPlot().getArea();
                    double quantity = divider != 0 ? Double.parseDouble(edaplosValueReported) / divider : 0;
                    UnitCode unitCode = seedsOrPlan.getValueMeasureUnitCode();

                    Collection<SeedLotInputUsage> currentSeedLotInputUsage = getSeedLotInputUsagesForCurrentSeed(seedingActionUsage, domainSeedLotInputsByUsageUnit, unitCode);
                    for (SeedLotInputUsage seedLotInputUsage : currentSeedLotInputUsage) {
                        addSeedingActionSpeciesInputs(interventionContext, seedLotInputUsage, quantity, unitCode, seedsOrPlan);
                    }
                } else {
                    interventionContext.formatError("La quantité déclarée d'intrant %s sur l'intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' est incorrecte.",
                            formatInput(seedsOrPlan), formatIntervention(interventionContext), formatSolOccupation(interventionContext), formatPlot(interventionContext), formatDomain(interventionContext));
                }
            }
        }
    }

    protected SeedingActionUsage createSeedingActionUsage(EdaplosContext interventionContext, Map<AgrosystInterventionType, AbstractAction> interventionActions, Collection<DomainSeedLotInput> domainSeedLotInputs) {
        SeedingActionUsage seedingActionUsage = getOrCreateSeedingActionUsage(interventionContext, interventionActions);

        for (DomainSeedLotInput domainSeedLotInput : domainSeedLotInputs) {

            SeedLotInputUsage seedLotInputUsage = new SeedLotInputUsageImpl();
            seedLotInputUsage.setDomainSeedLotInput(domainSeedLotInput);
            seedLotInputUsage.setInputType(InputType.SEMIS);

            Collection<DomainSeedSpeciesInput> domainSeedSpeciesInput = CollectionUtils.emptyIfNull(domainSeedLotInput.getDomainSeedSpeciesInput());
            for (DomainSeedSpeciesInput seedSpeciesInput : domainSeedSpeciesInput) {
                SeedSpeciesInputUsage seedSpeciesInputUsage = new SeedSpeciesInputUsageImpl();
                seedSpeciesInputUsage.setDomainSeedSpeciesInput(seedSpeciesInput);
                seedSpeciesInputUsage.setInputType(InputType.SEMIS);
                seedSpeciesInputUsage.setQtAvg(SEEDING_ACTION_SPECIES_DEFAULT_QUANTITY);// temporary
                seedLotInputUsage.addSeedingSpecies(seedSpeciesInputUsage);
            }
            seedingActionUsage.addSeedLotInputUsage(seedLotInputUsage);
        }
        return seedingActionUsage;
    }

    protected Collection<SeedLotInputUsage> getSeedLotInputUsagesForCurrentSeed(
            SeedingActionUsage seedingActionUsage,
            Map<SeedPlantUnit, List<DomainSeedLotInput>> domainSeedLotInputsByUsageUnit,
            UnitCode unitCode) {

        SeedPlantUnit seedPlantUnit = getSeedPlantUnitFromUnitCode(unitCode);
        List<DomainSeedLotInput> curentDomainSeedLotInputs = ListUtils.emptyIfNull(domainSeedLotInputsByUsageUnit.get(seedPlantUnit));
        Collection<SeedLotInputUsage> seedLotInputUsage = CollectionUtils.emptyIfNull(seedingActionUsage.getSeedLotInputUsage());
        Collection<SeedLotInputUsage> currentSeedLotInputUsage = seedLotInputUsage.stream().filter(sliu -> curentDomainSeedLotInputs.contains(sliu.getDomainSeedLotInput())).toList();

        return currentSeedLotInputUsage;
    }

    protected Set<SeedPlantUnit> getInterventionSpeciesCropByUsageUnits(EdaplosContext interventionContext, List<AgriculturalProcessCropInput> sortedSeedsOrPlans) {
        Set<SeedPlantUnit> seedingActionSpeciesByUsageUnit = new HashSet<>();
        for (AgriculturalProcessCropInput seedsOrPlan : sortedSeedsOrPlans) {
            String edaplosValueReported = seedsOrPlan.getAppliedSurfaceUnitValueMeasure();

            UnitCode unitCode = null;
            if (NumberUtils.isCreatable(edaplosValueReported)) {
                unitCode = seedsOrPlan.getAppliedSurfaceUnitValueMeasureUnitCode();

            } else {
                edaplosValueReported = seedsOrPlan.getValueMeasure();
                if (NumberUtils.isCreatable(edaplosValueReported)) {
                    unitCode = seedsOrPlan.getValueMeasureUnitCode();
                }
            }

            SeedPlantUnit seedPlantUnit = getSeedPlantUnitFromUnitCode(unitCode);
            if (seedPlantUnit == null) {
                interventionContext.addIncorrectUnitCode(unitCode);
            }

            seedPlantUnit = seedPlantUnit != null ? seedPlantUnit : SeedPlantUnit.KG_PAR_HA;

            seedingActionSpeciesByUsageUnit.add(seedPlantUnit);

        }
        return seedingActionSpeciesByUsageUnit;
    }

    protected static List<AgriculturalProcessCropInput> getSortedSeedsOrPlans(EdaplosContext interventionContext, List<AgriculturalProcessCropInput> seedsOrPlans) {
        List<AgriculturalProcessCropInput> sortedSeedsOrPlans = seedsOrPlans.stream()
                .filter(sop -> sop.getIdentification() != null && interventionContext.getCodeGnisToSpecies().containsKey(sop.getIdentification().toLowerCase(Locale.ROOT).strip()))
                .collect(Collectors.toList());

        List<AgriculturalProcessCropInput> remainingSeedsOrPlans = ListUtils.removeAll(seedsOrPlans, sortedSeedsOrPlans);
        List<AgriculturalProcessCropInput> withIdSeedsOrPlans = remainingSeedsOrPlans.stream()
                .filter(sop -> sop.getIdentification() != null)
                .collect(Collectors.toList());
        sortedSeedsOrPlans.addAll(withIdSeedsOrPlans);

        remainingSeedsOrPlans = ListUtils.removeAll(remainingSeedsOrPlans, withIdSeedsOrPlans);
        sortedSeedsOrPlans.addAll(remainingSeedsOrPlans);
        return sortedSeedsOrPlans;
    }

    protected Map<String, SpecifiedAgriculturalDevice> getNotFoundMaterials(
            EdaplosContext context,
            List<SpecifiedAgriculturalDevice> usedAgriculturalDevices,
            Map<String, Equipment> equipments0) {

        Map<String, SpecifiedAgriculturalDevice> unUsedAgriculturalDevicesByIds = new HashMap<>();
        // si des equipements inconnu existent
        if (equipments0.size() < usedAgriculturalDevices.size()) {
            Set<String> importedEquipmentIds = equipments0.keySet();
            List<SpecifiedAgriculturalDevice> unUsedAgriculturalDevices = usedAgriculturalDevices
                    .stream()
                    .filter(specifiedAgriculturalDevice -> !importedEquipmentIds.contains(
                            specifiedAgriculturalDevice.getIdentification() + "_" + specifiedAgriculturalDevice.getDescription())).toList();

            for (SpecifiedAgriculturalDevice unUsedAgriculturalDevice : unUsedAgriculturalDevices) {
                unUsedAgriculturalDevicesByIds.put(unUsedAgriculturalDevice.getIdentification(), unUsedAgriculturalDevice);
                try {
                    context.formatWarning(
                            "Certains matériels ne sont pas reconnus dans Agrosyst. Code du matériel : '%s'. Descriptif du matériel : '%s'. Veuillez contacter l'équipe Agrosyst pour les ajouter."
                            , unUsedAgriculturalDevice.getIdentification()
                            , unUsedAgriculturalDevice.getDescription()
                    );
                } catch (Exception e) {
                    LOGGER.error(e);
                    LOGGER.error(unUsedAgriculturalDevice.getIdentification());
                    LOGGER.error(unUsedAgriculturalDevice.getDescription());
                }
            }
        }
        return unUsedAgriculturalDevicesByIds;
    }

    protected void parseTriNumberAndSetTransitCount(EdaplosContext context, PlotAgriculturalProcess agriculturalProcess, EffectiveIntervention effectiveIntervention) {

        Integer transitCount = getTransitCountFromPlotAgriculturalProcess(agriculturalProcess);

        if (transitCount != null && transitCount <= 0) {
            context.formatWarning("Le nombre de passage renseigné pour l'intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' n'est pas cohérent (cela doit être un nombre entier >=1). Prendra la valeur 1 par défaut.",
                    formatIntervention(effectiveIntervention), formatSolOccupation(context), formatPlot(context), formatDomain(context));
            transitCount = DEFAUlT_TRANSIT_COUNT;
        }

        transitCount = transitCount == null ? DEFAUlT_TRANSIT_COUNT : transitCount;

        effectiveIntervention.setTransitCount(transitCount);
    }

    protected Integer getTransitCountFromPlotAgriculturalProcess(PlotAgriculturalProcess agriculturalProcess) {

        String tripNumber = agriculturalProcess.getTripNumber();

        if (tripNumber == null) {
            return null;
        }

        tripNumber = tripNumber.replace(',', '.');

        return getTransitCountFromTripNumber(tripNumber);
    }

    protected int getTransitCountFromTripNumber(String tripNumber) {
        int transitCount = 0;

        if (NumberUtils.isCreatable(tripNumber)) {
            Number parseNumber = NumberUtils.createNumber(tripNumber);
            if ((parseNumber.floatValue() % 1) == 0) {
                transitCount = parseNumber.intValue();
            }
        }
        return transitCount;
    }

    protected MineralFertilizersSpreadingAction createNewMineralFertilizersSpreadingActionWithDefaultValues(EffectiveIntervention effectiveIntervention, RefInterventionAgrosystTravailEDI travailEdi, String description) {
        MineralFertilizersSpreadingAction action = new MineralFertilizersSpreadingActionImpl();
        travailEdi = travailEdi == null ? refInterventionAgrosystTravailEDIDao.forReference_codeEquals("SEM").findUniqueOrNull() : travailEdi;
        addCommonActionFields(action, travailEdi, effectiveIntervention, description);
        return action;
    }

    protected HarvestingAction createNewHarvestingActionWithDefaultValues(EffectiveIntervention effectiveIntervention, RefInterventionAgrosystTravailEDI travailEdi, String description) {
        HarvestingAction action = new HarvestingActionImpl();
        travailEdi = travailEdi == null ? refInterventionAgrosystTravailEDIDao.forReference_codeEquals("SER").findUniqueOrNull() : travailEdi;
        addCommonActionFields(action, travailEdi, effectiveIntervention, description);
        return action;
    }

    protected TillageAction createNewTillageActionWithDefaultValues(EffectiveIntervention effectiveIntervention, RefInterventionAgrosystTravailEDI travailEdi, String description) {
        TillageAction action = new TillageActionImpl();
        addCommonActionFields(action, travailEdi, effectiveIntervention, description);
        return action;
    }

    protected CarriageAction createNewCarriageActionWithDefaultValues(EffectiveIntervention effectiveIntervention, RefInterventionAgrosystTravailEDI travailEdi, String description) {
        CarriageAction action = new CarriageActionImpl();
        addCommonActionFields(action, travailEdi, effectiveIntervention, description);
        return action;
    }

    protected OrganicFertilizersSpreadingAction createNewOrganicFertilizersSpreadingActionWithDefaultValues(EffectiveIntervention effectiveIntervention, RefInterventionAgrosystTravailEDI travailEdi, String description) {
        OrganicFertilizersSpreadingAction action = new OrganicFertilizersSpreadingActionImpl();
        addCommonActionFields(action, travailEdi, effectiveIntervention, description);
        return action;
    }

    protected OtherAction createNewOtherActionActionWithDefaultValues(EffectiveIntervention effectiveIntervention, RefInterventionAgrosystTravailEDI travailEdi, String description) {
        OtherAction action = new OtherActionImpl();
        addCommonActionFields(action, travailEdi, effectiveIntervention, description);
        return action;
    }

    protected double getPhytoTreatedSurface(EffectiveIntervention effectiveIntervention, EdaplosContext context) {

        Double treatedSurface = null;
        PlotAgriculturalProcess agriculturalProcess = context.getPlotAgriculturalProcess();
        String treatedSurfaceString = "-";
        if (agriculturalProcess != null) {
            treatedSurfaceString = agriculturalProcess.getUsedSpecifiedAgriculturalDevices().stream()
                    .map(SpecifiedAgriculturalDevice::getTreatedAreaPercentage)
                    .filter(Objects::nonNull)
                    .findFirst().orElse(null);

            if (NumberUtils.isCreatable(treatedSurfaceString)) {
                treatedSurface = Double.parseDouble(treatedSurfaceString);
                if (treatedSurface < 0 || treatedSurface > 100) {
                    treatedSurface = null;
                }
            }
        }

        if (treatedSurface == null) {
            treatedSurface = 100d;
            context.formatInfo("Sur la parcelle '%s', occupation du sol '%s', l'intervention '%s' n'a aucun équipement comportant une surface traitée valide '%s'. Le pourcentage de surface traitée de l'action phyto est fixé à 100.",
                    formatPlot(context), formatSolOccupation(context), formatIntervention(effectiveIntervention), treatedSurfaceString);
        }

        return treatedSurface;
    }

    protected RefInterventionAgrosystTravailEDI getDefaultTravailEDI() {
        return refInterventionAgrosystTravailEDIDao.forReference_codeEquals("ZTC").findUniqueOrNull();
    }

    /**
     * Manage a "UsedAgriculturalDevice" tag to transform information into a
     * {@link Equipment}.
     *
     * @return a map with CodeRef and an {@link Equipment} corresponding, or null if intervention cannot be parsed (in case of wrong subordinate type)
     */
    protected Map<String, Equipment> processInterventionEquipments(EdaplosContext context,
                                                                   List<SpecifiedAgriculturalDevice> agriculturalDevices) {
        Domain domain = context.getDomain();

        Map<String, Equipment> equipments = new HashMap<>();

        Set<Pair<String, String>> requestingEquipmentNameForCodeProd = context.getRequestingEquipmentNameForCodeProd();

        List<Equipment> domainEquipments = equipmentDao.forDomainEquals(domain).findAll();

        if (CollectionUtils.isNotEmpty(agriculturalDevices)) {

            for (SpecifiedAgriculturalDevice agriculturalDevice : agriculturalDevices) {
                String toolIdentification = agriculturalDevice.getIdentification();
                String toolDescription = agriculturalDevice.getDescription();


                // no identifier
                if (validIdentifier(context, toolIdentification)) continue;// case not valid, warning through up

                checkDefaultMaterialUsage(context, domain, toolIdentification);

                List<RefMateriel> materials = loadRefMaterielsForCodeRef(context, toolIdentification);

                if (materials == null) continue;// case not valid, warning through up

                // already existing in current domain
                List<Equipment> loadedEquipments = domainEquipments.stream().filter(equipment -> materials.contains(equipment.getRefMateriel())).collect(Collectors.toList());

                // description
                loadedEquipments = filterDomainEquipmentOnDescription(context, requestingEquipmentNameForCodeProd, toolIdentification, toolDescription, loadedEquipments);

                Equipment equipment = CollectionUtils.isEmpty(loadedEquipments) ? createNewDomainEquipment(domain, agriculturalDevice, toolDescription, materials) : loadedEquipments.getFirst();

                equipments.put(toolIdentification + "_" + toolDescription, equipment);

            }
        }

        equipments = persistInterventionEquipments(equipments);

        return equipments;
    }

    protected boolean validIdentifier(EdaplosContext context, String toolIdentification) {
        if (StringUtils.isBlank(toolIdentification)) {
            context.formatError("Sur la parcelle '%s', occupation du sol '%s', l'intervention '%s' a un outil sans identifiant. Import impossible.",
                    formatPlot(context), formatSolOccupation(context), formatIntervention(context));
            return true;
        }
        return false;
    }

    protected void checkDefaultMaterialUsage(EdaplosContext context, Domain domain, String toolIdentification) {
        // default material is used and must be replaced into the domain
        if (toolIdentification.equals(defaultTractorCode) && !globalContext.isTractorWithDefaultCode) {
            context.formatWarning("Sur la parcelle '%s' du domaine '%s' au moins un tracteur par défaut est utilisé pour l'import eDaplos mais devra être remplacé dans l'interface.",
                    formatPlot(context), formatDomain(domain));
            globalContext.setTractorWithDefaultCode(true);
        }

        if (toolIdentification.equals(defaultIrrigationCode) && !globalContext.isIrrigationMaterialWithDefaultCode()) {
            context.formatWarning("Sur la parcelle '%s' du domaine '%s' au moins un matériel d'irrigation par défaut est utilisé pour l'import eDaplos mais devra être remplacé dans l'interface.",
                    formatPlot(context), formatDomain(domain));
            globalContext.setIrrigationMaterialWithDefaultCode(true);
        }

        if (toolIdentification.equals(defaultEquipmentCode) && !globalContext.isEquipmentWithDefaultCode) {
            context.formatWarning("Sur la parcelle '%s' du domaine '%s' au moins un équipement par défaut est utilisé pour l'import eDaplos mais devra être remplacé dans l'interface.",
                    formatPlot(context), formatDomain(domain));
            globalContext.setEquipmentWithDefaultCode(true);
        }

        if (toolIdentification.equals(defaultAutomoteurCode) && !globalContext.isAutomoteurDefaultCode) {
            context.formatWarning("Sur la parcelle '%s' du domaine '%s' au moins un automoteur par défaut est utilisé pour l'import eDaplos mais devra être remplacé dans l'interface.",
                    formatPlot(context), formatDomain(domain));
            globalContext.setAutomoteurDefaultCode(true);
        }
    }

    protected List<RefMateriel> loadRefMaterielsForCodeRef(EdaplosContext context, String toolIdentification) {
        List<RefMateriel> materials = refMaterielDao.forProperties(RefMaterielOutil.PROPERTY_CODE_REF, toolIdentification).findAll();
        if (materials.isEmpty()) {
            context.formatWarning("L'outil '%s' mobilisé dans l'intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' n'existe pas dans Agrosyst et ne sera pas importé.",
                    toolIdentification, formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
            return null;
        }
        return materials;
    }

    protected List<Equipment> filterDomainEquipmentOnDescription(EdaplosContext context, Set<Pair<String, String>> requestingEquipmentNameForCodeProd, String toolIdentification, String toolDescription, List<Equipment> loadedEquipments) {
        if (StringUtils.isBlank(toolDescription)) {
            context.formatWarning("L'outil '%s' mobilisé dans l'intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' n'a pas de nom.",
                    toolIdentification, formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
        } else {

            loadedEquipments = tryGetEquipmentWithSameName(requestingEquipmentNameForCodeProd, toolIdentification, toolDescription, loadedEquipments);

        }
        return loadedEquipments;
    }

    protected Equipment createNewDomainEquipment(Domain domain, SpecifiedAgriculturalDevice agriculturalDevice, String toolDescription, List<RefMateriel> materiels) {
        Equipment equipment;// eta
        CropDataSheetParty toolOwner = agriculturalDevice.getOwnerCropDataSheetParty();//il a été décidé en février 2016 que toutes les balises xxxParty seraient harmonisées et contiendraient un SIRET
        boolean etaTool = false;
        if (toolOwner != null) {
            String domainSiret = StringUtils.isNotBlank(domain.getSiret()) ? domain.getSiret() : "";
            String toolOwnerSiret = toolOwner.getIdentification();
            if (StringUtils.isNotBlank(toolOwnerSiret) && !StringUtils.equalsIgnoreCase(toolOwnerSiret, domainSiret)) {
                etaTool = true;// indique que le matériel est loué (case CUMA/ETA cochée)
            }
        }
        RefMateriel materiel = materiels.getFirst();// take one

        equipment = new EquipmentImpl();
        equipment.setCode(UUID.randomUUID().toString());
        equipment.setRefMateriel(materiel);
        equipment.setMaterielETA(etaTool);
        equipment.setValidated(true);
        equipment.setDomain(domain);

        setEquipmentName(toolDescription, equipment, materiel);

        return equipment;

    }

    protected void setEquipmentName(String toolDescription, Equipment equipment, RefMateriel materiel) {
        equipment.setName(toolDescription);
        if (StringUtils.isBlank(equipment.getName())) {
            try {
                //XXX ymartel 20161018 : use this cause we can retrieve on all subtype of RefMateriel the attribute :
                // fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur#idtypemateriel
                // fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation#idtypemateriel
                // fr.inra.agrosyst.api.entities.referential.RefMaterielOutil#idtypemateriel
                // fr.inra.agrosyst.api.entities.referential.RefMaterielTraction#idtypemateriel
                String idtypemateriel = (String) materiel.getClass().getMethod("getIdtypemateriel").invoke(materiel);
                if (StringUtils.isBlank(idtypemateriel)) {
                    equipment.setName(materiel.getTypeMateriel1());
                } else {
                    equipment.setName(idtypemateriel);
                }
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                // Seriously, who think this can happen ? :D
                LOGGER.error("An access on 'getIdtypemateriel' has failed on a RefMateriel SubClass", e);
                equipment.setName(materiel.getTypeMateriel1());
            }
        }
    }

    protected List<Equipment> tryGetEquipmentWithSameName(Set<Pair<String, String>> requestingEquipmentNameForCodeProd, String toolIdentification, String toolDescription, List<Equipment> loadedEquipments) {

        // if name is given we try to filter on it to avoid using a unique equipment as it can be several same equipments
        final String equipmentName = CommonService.NORMALISE.apply(toolDescription);
        final List<Equipment> loadedEquipmentsTmp = loadedEquipments.stream().filter(equipment -> CommonService.NORMALISE.apply(equipment.getName()).equals(equipmentName)).collect(Collectors.toList());

        if (loadedEquipmentsTmp.isEmpty()) {
            // equipment with the same name could not be find, inform user
            requestingEquipmentNameForCodeProd.add(Pair.of(toolIdentification, toolDescription));

        }  // else: equipement with same code and label is found

        loadedEquipments = loadedEquipmentsTmp;
        return loadedEquipments;

    }

    /**
     * La sauvegarde des euipements est faites dans un second temps car certains equipement (petit materiel)
     * peuvent empêcher la sauvegarde de tous les equipements de l'intervention.
     */
    protected Map<String, Equipment> persistInterventionEquipments(Map<String, Equipment> equipments) {

        Map<String, Equipment> result = new HashMap<>();

        // persist or not
        for (Map.Entry<String, Equipment> entry : equipments.entrySet()) {
            String id = entry.getKey();
            Equipment equipment = entry.getValue();
            equipment = equipmentDao.update(equipment);
            result.put(id, equipment);
        }

        return result;
    }

    /**
     * From a list of {@link Equipment}, reconstitute a {@link ToolsCoupling}.
     * Has from import we have no distinction between all equipments, we have to
     * extract from the list the tractor if there is one.
     * <p>
     * Some rules should be checked, according to <a href="https://forge.codelutin.com/issues/6678#note-4">...</a>
     * """
     * Au niveau des combinaisons de matériel possibles ce serait plutôt :
     * - uniquement un et un seul (?) matériel d'irrigation sélectionné ou
     * - plusieurs matériels d'irrigation sélectionnés
     * - un ou plusieurs matériels d'irrigation sélectionné plus un tracteur
     * - un ou plusieurs matériels d'irrigation sélectionné plus un automoteur
     * """
     */
    protected ToolsCoupling processInterventionToolsCoupling(EdaplosContext context,
                                                             Collection<Equipment> equipments, Map<String, SpecifiedAgriculturalDevice> notFoundAgriculturalDevices) {

        EffectiveIntervention intervention = context.getEffectiveIntervention();

        if (!notFoundAgriculturalDevices.isEmpty()) {
            context.formatWarning("L'intervention '%s' avec comme edaplosIssuerId '%s' réalisée sur occupation du sol '%s',  la parcelle '%s', domaine '%s' comporte du matériel non reconnu. Aucune combinaison d'outils sera associée à cette intervention.",
                    formatIntervention(context), intervention.getEdaplosIssuerId(), formatSolOccupation(context), formatPlot(context), formatDomain(context));
            return null;
        }

        Domain domain = context.getDomain();

        // Check that the list of equipment is already registered as a tools coupling
        // la comparaison s'effectue sur les refMateriels car les combinaisons d'outils peuvent avoir été prolongés
        // et ne sont pas les mêmes instances
        List<ToolsCoupling> existingToolsCouplings = toolsCouplingDao.forDomainEquals(domain).findAll();
        if (CollectionUtils.isNotEmpty(existingToolsCouplings)) {
            // equipement referencing same referential materiel by name as it can have several similar equipments on a domain
            Set<Pair<String, RefMateriel>> refMaterielsByName = equipments.stream().map(equipment -> Pair.of(CommonService.NORMALISE.apply(equipment.getName()), equipment.getRefMateriel())).collect(Collectors.toSet());
            for (ToolsCoupling existingToolsCoupling : existingToolsCouplings) {
                Set<Pair<String, RefMateriel>> existingMaterielsByName = new HashSet<>();
                // existing tools coupling
                existingToolsCoupling.getEquipments().forEach(e -> existingMaterielsByName.add(Pair.of(CommonService.NORMALISE.apply(e.getName()), e.getRefMateriel())));
                final Equipment tractor = existingToolsCoupling.getTractor();
                if (tractor != null) {
                    existingMaterielsByName.add(Pair.of(CommonService.NORMALISE.apply(tractor.getName()), tractor.getRefMateriel()));
                }
                if (existingMaterielsByName.equals(refMaterielsByName)) {
                    return existingToolsCoupling;
                }
            }
        }

        // So, let's verify the ToolsCoupling
        ToolsCoupling toolsCoupling = null;

        boolean irrigationEquipmentsPresent = false;
        boolean nonIrrigationEquipmentsPresent = false;

        Equipment tractor = null;
        Iterator<Equipment> equipmentIterator = equipments.iterator();

        List<String> names = new ArrayList<>();
        // loop over equipments to find the intervention tractor or automotor and to remove it from the list
        // as only tools must remain into the list
        // throw up error if more that one tractor or automotor is found
        while (equipmentIterator.hasNext()) {
            Equipment equipment = equipmentIterator.next();

            names.add(equipment.getName());

            if (equipment.getRefMateriel() instanceof RefMaterielTraction || equipment.getRefMateriel() instanceof RefMaterielAutomoteur) {
                // We need to extract the tractor, and we must have only one (or no) tractor !
                if (tractor == null) {
                    tractor = equipment;
                    equipmentIterator.remove();
                } else {
                    // Warning : un seul tracteur autorisé
                    context.formatWarning("Combinaison d'outils invalide pour l'intervention '%s' avec comme edaplosIssuerId '%s' réalisée sur occupation du sol '%s',  la parcelle '%s', domaine '%s' a plus d'un tracteur/automoteur. Les matériels seront importés au niveau du domaine mais ils ne seront pas rattachés à l'intervention. Vous devrez donc constituer la combinaison d'outils et la rattacher à l'intervention après import.",
                            formatIntervention(context), intervention.getEdaplosIssuerId(), formatSolOccupation(context), formatPlot(context), formatDomain(context));
                    return null;
                }
            } else {

                boolean isIrrigationEquipment = equipment.getRefMateriel() != null && equipment.getRefMateriel() instanceof RefMaterielIrrigation;
                if (isIrrigationEquipment) {
                    irrigationEquipmentsPresent = true;
                } else {
                    nonIrrigationEquipmentsPresent = true;
                }
            }
        }

        boolean hasError = false;

        if (tractor == null && nonIrrigationEquipmentsPresent) {
            context.formatWarning("Combinaison d'outils invalide : l'intervention '%s' portant sur occupation du sol '%s', parcelle '%s', domaine '%s' mobilise un outil sans tracteur/automoteur. Les matériels seront importés au niveau du domaine mais ils ne seront pas rattachés à l'intervention. Vous devrez donc constituer la combinaison d'outils et la rattacher à l'intervention après import.",
                    formatIntervention(intervention), formatSolOccupation(context), formatPlot(context), formatDomain(context));
            hasError = true;
        } else if (tractor != null && !(tractor.getRefMateriel() instanceof RefMaterielAutomoteur) && CollectionUtils.isEmpty(equipments)) {
            // if tractor is not auto-motor's type, it must be associated to 1+n equipment
            context.formatWarning("Combinaison d'outils invalide pour l'intervention '%s' portant sur occupation du sol '%s', parcelle '%s', domaine '%s' : aucun outil/équipement associé au tracteur. Les matériels seront importés au niveau du domaine mais ils ne seront pas rattachés à l'intervention. Vous devrez donc constituer la combinaison d'outils et la rattacher à l'intervention après import.",
                    formatIntervention(intervention), formatSolOccupation(context), formatPlot(context), formatDomain(context));
            hasError = true;

        }

        if (CollectionUtils.isNotEmpty(equipments) && (irrigationEquipmentsPresent && nonIrrigationEquipmentsPresent)) {
            context.formatWarning("Combinaison d'outils invalide pour l'intervention '%s' portant sur occupation du sol '%s', parcelle '%s', domaine '%s': du matériel d'irrigation ne peut être associé avec du matériel autre qu'un tracteur ou un automoteur. Les matériels seront importés au niveau du domaine mais ils ne seront pas rattachés à l'intervention. Vous devrez donc constituer la combinaison d'outils et la rattacher à l'intervention après import.",
                    formatIntervention(intervention), formatSolOccupation(context), formatPlot(context), formatDomain(context));
            hasError = true;
        }

        if (!hasError) {
            toolsCoupling = toolsCouplingDao.newInstance();
            toolsCoupling.setCode(UUID.randomUUID().toString());
            // Le nom du TC correspond à l'aggregation des noms d'équipements
            toolsCoupling.setToolsCouplingName(StringUtils.join(names, ' '));
            toolsCoupling.setTractor(tractor);
            toolsCoupling.setEquipments(equipments);
            toolsCoupling.setValidated(true);
            toolsCoupling.setDomain(domain);
            toolsCoupling = toolsCouplingDao.create(toolsCoupling);
        }

        return toolsCoupling;
    }

    /**
     * create inputs and actions if there are not for input
     */
    protected void processCropInput(
            EdaplosContext interventionContext,
            AgriculturalProcessCropInput cropInput,
            Map<AgrosystInterventionType, AbstractAction> actionsByInterventionTypes) {

        EffectiveIntervention effectiveIntervention = interventionContext.getEffectiveIntervention();

        String typeCode = cropInput.getTypeCode();

        if (StringUtils.isBlank(typeCode)) {
            interventionContext.formatError("L'intrant déclaré sur la culture '%s', intervention '%s', parcelle '%s', domaine '%s' n'a pas de 'Typecode' renseigné. Import impossible.",
                    formatCrop(interventionContext), formatIntervention(interventionContext), formatPlot(interventionContext), formatDomain(interventionContext));
            return;
        }

        RefInterventionTypeItemInputEDI refInterventionTypeItemInputEDI = refInterventionTypeItemInputEDIDao.forInputEdiTypeCodeEquals(typeCode).findAnyOrNull();
        if (refInterventionTypeItemInputEDI == null) {
            interventionContext.formatError("L'intrant déclaré sur la culture '%s', intervention '%s', parcelle '%s', domaine '%s', a un 'Typecode': '%s' qui n'est pas géré par Agrosyst. L'intrant ne sera pas importé. Merci de contacter l'équipe Agrosyst.",
                    formatCrop(interventionContext), formatIntervention(effectiveIntervention), formatPlot(interventionContext), formatDomain(interventionContext), typeCode);
            return;
        }

        AgrosystInterventionType agrosystInterventionType = refInterventionTypeItemInputEDI.getInterventionType();

        switch (agrosystInterventionType) {
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX ->
                    processMineralProductInput(interventionContext, cropInput, actionsByInterventionTypes);
            case EPANDAGES_ORGANIQUES ->
                    processOrganicProductInput(interventionContext, cropInput, actionsByInterventionTypes);
            case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES ->
                    processPesticideProductInput(interventionContext, cropInput, actionsByInterventionTypes);
            case LUTTE_BIOLOGIQUE ->
                    processBiologicalProductInput(interventionContext, cropInput, actionsByInterventionTypes);
            case IRRIGATION -> processIrrigationInput(interventionContext, cropInput, actionsByInterventionTypes);
            case SEMIS -> {
                // done
            }
            case RECOLTE -> {
                // nothing to do, will be done with call to method importHarvestingAction
            }
            case AUTRE -> processOtherProductInput(actionsByInterventionTypes, effectiveIntervention);
            default ->
                    interventionContext.formatError("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', pour un intrant, le TypeCode '%s' n'est pas géré par Agrosyst. L'intrant ne sera pas importé.",
                            formatPlot(interventionContext), formatSolOccupation(interventionContext), formatIntervention(interventionContext), typeCode);
        }

    }

    protected void processOtherProductInput(Map<AgrosystInterventionType, AbstractAction> actionsByInterventionTypes, EffectiveIntervention effectiveIntervention) {
        // les intrants autre ne sont pas a importer
        // On crée tous de même une action s'il n'en n'existe pas
        OtherAction otherAction = (OtherAction) actionsByInterventionTypes.get(AgrosystInterventionType.AUTRE);
        if (otherAction == null) {
            RefInterventionAgrosystTravailEDI travailEdi = getDefaultTravailEDI();
            otherAction = createNewOtherActionActionWithDefaultValues(effectiveIntervention, travailEdi, null);
            actionsByInterventionTypes.put(AgrosystInterventionType.AUTRE, otherAction);
        }
    }

    protected void processSeedCropInput(
            EdaplosContext interventionContext,
            DomainSeedLotInput domainSeedLotInput,
            InterventionPhytoProductContext interventionPhytoProductContext,
            SeedLotInputUsage seedLotInputUsage) {

        EffectiveIntervention effectiveIntervention = interventionContext.getEffectiveIntervention();

        List<PhytoProductInputContext> validPhytoProductInputContext = interventionPhytoProductContext.phytoProductInputContexts().stream()
                .filter(phytoProductInputContext -> phytoProductInputContext.refInput != null)
                .toList();

        Collection<SeedSpeciesInputUsage> seedingSpeciesUsages = CollectionUtils.emptyIfNull(seedLotInputUsage.getSeedingSpecies());

        for (PhytoProductInputContext phytoProductInputContext : validPhytoProductInputContext) {
            RefActaTraitementsProduit refInput = phytoProductInputContext.refInput;
            AgriculturalProcessCropInput productInput = phytoProductInputContext.productInput;

            ProductType typeProduit = null;
            RefActaTraitementsProduitsCateg refActaTraitementsProduitsCateg =
                    refActaTraitementsProduitsCategDao.forId_traitementEquals(phytoProductInputContext.refInput().getId_traitement()).findAnyOrNull();
            if (refActaTraitementsProduitsCateg != null) {
                typeProduit = refActaTraitementsProduitsCateg.getType_produit();
            }

            Collection<DomainSeedSpeciesInput> domainSeedSpeciesInput = CollectionUtils.emptyIfNull(domainSeedLotInput.getDomainSeedSpeciesInput());
            for (DomainSeedSpeciesInput seedSpeciesInput : domainSeedSpeciesInput) {

                Map<String, DomainPhytoProductInput> speciesSeedPhytoInputByKey = new HashMap<>();
                if (seedSpeciesInput.isPersisted()) {
                    Collection<DomainPhytoProductInput> speciesPhytoInputs = CollectionUtils.emptyIfNull(seedSpeciesInput.getSpeciesPhytoInputs());
                    for (DomainPhytoProductInput speciesPhytoInput : speciesPhytoInputs) {
                        speciesSeedPhytoInputByKey.put(speciesPhytoInput.getInputKey(), speciesPhytoInput);
                    }
                }
                String phytoInputKey = DomainInputStockUnitService.getPhytoInputKey(refInput, phytoProductInputContext.usageUnit());
                DomainPhytoProductInput seedProductInput = speciesSeedPhytoInputByKey.get(phytoInputKey);

                if (seedProductInput == null && refInput != defaultRefActaTraitementsProduit) {

                    seedProductInput = getNewDomainPhytoProductInput(
                            interventionContext,
                            productInput,
                            phytoProductInputContext,
                            InputType.TRAITEMENT_SEMENCE,
                            typeProduit,
                            refInput);
                    seedSpeciesInput.addSpeciesPhytoInputs(seedProductInput);
                }
            }
        }

        Double treatedSurface = getPhytoTreatedSurface(effectiveIntervention, interventionContext);

        CollectionUtils.emptyIfNull(domainSeedLotInput.getDomainSeedSpeciesInput())
                .forEach(
                        dssi -> {
                            Optional<SeedSpeciesInputUsage> optionalSeedSpeciesInputUsage = seedingSpeciesUsages.stream()
                                    .filter(seedSpeciesInputUsage ->
                                            seedSpeciesInputUsage.getDomainSeedSpeciesInput().equals(dssi)).findAny();
                            SeedSpeciesInputUsage seedSpeciesInputUsage;
                            if (optionalSeedSpeciesInputUsage.isEmpty()) {
                                seedSpeciesInputUsage = new SeedSpeciesInputUsageImpl();
                                seedSpeciesInputUsage.setDomainSeedSpeciesInput(dssi);
                                seedSpeciesInputUsage.setInputType(InputType.SEMIS);
                                seedSpeciesInputUsage.setQtAvg(SEEDING_ACTION_SPECIES_DEFAULT_QUANTITY);// temporary

                                seedLotInputUsage.addSeedingSpecies(seedSpeciesInputUsage);
                            } else {
                                seedSpeciesInputUsage = optionalSeedSpeciesInputUsage.get();
                            }

                            if (CollectionUtils.isNotEmpty(dssi.getSpeciesPhytoInputs())) {
                                for (DomainPhytoProductInput domainInput : dssi.getSpeciesPhytoInputs()) {
                                    Optional<PhytoProductInputContext> optionalPhytoProductInputContext = validPhytoProductInputContext.stream().filter(ppic -> ppic.refInput.equals(domainInput.getRefInput())).findAny();
                                    if (optionalPhytoProductInputContext.isEmpty()) {
                                        continue;
                                    }

                                    PhytoProductInputContext phytoProductInputContext = optionalPhytoProductInputContext.get();

                                    AgriculturalProcessCropInput productInput = phytoProductInputContext.productInput;

                                    RefInterventionTypeItemInputEDI refInterventionTypeItemInputEDI = refInterventionTypeItemInputEDIDao.forInputEdiTypeCodeEquals(productInput.getTypeCode()).findAnyOrNull();
                                    if (refInterventionTypeItemInputEDI == null) {
                                        interventionContext.formatError("L'intrant déclaré sur la culture '%s', intervention '%s', parcelle '%s', domaine '%s', a un 'Typecode': '%s' qui n'est pas géré par Agrosyst. L'intrant ne sera pas importé. Merci de contacter l'équipe Agrosyst.",
                                                formatCrop(interventionContext), formatIntervention(effectiveIntervention), formatPlot(interventionContext), formatDomain(interventionContext), productInput.getTypeCode());
                                        continue;
                                    }

                                    SeedProductInputUsage seedProductInputUsage = newSeedProductInputUsage(
                                            interventionContext,
                                            phytoProductInputContext, domainInput,
                                            treatedSurface
                                    );

                                    // cibles
                                    addPhytoProductInputTargets(
                                            interventionContext,
                                            seedProductInputUsage,
                                            phytoProductInputContext);

                                    seedSpeciesInputUsage.addSeedProductInputUsages(seedProductInputUsage);
                                }
                            }

                        }
                );

    }

    protected void addPhytoProductInputTargets(
            EdaplosContext interventionContext,
            AbstractPhytoProductInputUsage usage,
            PhytoProductInputContext phytoProductInputContext) {

        AgriculturalProcessCropInput productInput = phytoProductInputContext.productInput;
        List<AgriculturalProcessTargetObject> specifiedAgriculturalProcessTargetObjects = phytoProductInputContext.specifiedAgriculturalProcessTargetObjects;
        EffectiveIntervention effectiveIntervention = interventionContext.getEffectiveIntervention();
        for (AgriculturalProcessTargetObject c : specifiedAgriculturalProcessTargetObjects) {
            String subordinateTypeCode = c.getSubordinateTypeCode();
            if (StringUtils.isBlank(subordinateTypeCode)) {
                interventionContext.formatError("La cible de l'intrant %s, intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' est obligatoire.",
                        productInput.getIdentification(), formatIntervention(effectiveIntervention), formatSolOccupation(interventionContext), formatPlot(interventionContext), formatDomain(interventionContext));
            } else {
                RefNuisibleEDI nuisible = refNuisibleEDIDao.forReference_codeEquals(StringUtils.trim(subordinateTypeCode)).findUniqueOrNull();
                if (nuisible != null) {
                    PhytoProductTarget target = phytoProductTargetDao.create(PhytoProductTarget.PROPERTY_TARGET, nuisible);
                    usage.addTargets(target);
                } else {
                    // Since #10508, if not found, try to get it from RefAdventice :
                    // - si le code n'est pas retrouvé dans le référentiel RefNuisibleEDI alors il faut récupérer les 5 derniers caractères du code de la cible eDaplos et regarder si ce code à 5 caractères est présent dans la colonne "identifiant" du référentiel RefAdventice
                    // - si le code est présent alors on intègre la cible en tant qu'adventice
                    // - si le code n'est présent ni dans l'un ni dans l'autre alors on change également le fonctionnement actuel : on met un message de warning à l'utilisateur et on intègre quand même la cible en tant que nuisible générique : reference_code "000000AEE021", reference_label "Désherbage", reference_param "GENERIQUE"

                    RefAdventice refAdventice = refAdventiceDao.forIdentifiantEquals(subordinateTypeCode).findUniqueOrNull();

                    if (refAdventice == null) {
                        // If not found with full code, try with last 5 chars
                        final int subordinateTypeCodeLength = subordinateTypeCode.length();
                        String eDaplosTarget = subordinateTypeCode.substring(subordinateTypeCodeLength - 5, subordinateTypeCodeLength);
                        refAdventice = refAdventiceDao.forIdentifiantEquals(eDaplosTarget).findUniqueOrNull();
                    }

                    if (refAdventice != null) {
                        PhytoProductTarget target = phytoProductTargetDao.create(PhytoProductTarget.PROPERTY_TARGET, refAdventice);
                        usage.addTargets(target);

                    } else {
                        RefAdventice genericAgressor = refAdventiceDao.forIdentifiantEquals(GENERIC_AGRESSOR_ID).findUnique();
                        PhytoProductTarget target = phytoProductTargetDao.create(PhytoProductTarget.PROPERTY_TARGET, genericAgressor);
                        usage.addTargets(target);
                        interventionContext.formatWarning("La cible de l'intrant %s, code '%s' intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' n'est pas reconnue. Veuillez contacter l'équipe Agrosyst."
                                        + " Une cible par défaut a été utilisée (code '%s')",
                                formatInput(productInput), StringUtils.trim(subordinateTypeCode), formatIntervention(effectiveIntervention), formatSolOccupation(interventionContext), formatPlot(interventionContext), formatDomain(interventionContext), GENERIC_AGRESSOR_ID);
                    }
                }
            }
        }
    }

    protected SeedingActionUsage getOrCreateSeedingActionUsage(EdaplosContext interventionContext, Map<AgrosystInterventionType, AbstractAction> actionsByInterventionTypes) {
        SeedingActionUsage seedingActionUsage = (SeedingActionUsage) actionsByInterventionTypes.get(SEMIS);

        if (seedingActionUsage == null) {
            seedingActionUsage = createNewSeedingActionUsageWithDefaultValues(interventionContext, "SET", null, null);
            actionsByInterventionTypes.put(SEMIS, seedingActionUsage);
        }
        return seedingActionUsage;
    }

    protected InterventionPhytoProductContext getInterventionPhytoProductContext(
            EdaplosContext interventionContext,
            List<AgriculturalProcessCropInput> seedingProductInputs) {

        // #13318 Dans les lots de semence, créer par défaut en "Semences certifiées" + Traitement chimique "Oui" + Inoculation biologique "Non"
        // Cela couvrirait 95% des cas.
        boolean chemicalTreatment = CollectionUtils.isEmpty(seedingProductInputs);
        boolean biologicalSeedInoculation = false;
        Collection<PhytoProductInputContext> phytoProductInputContexts = new ArrayList<>();
        for (AgriculturalProcessCropInput cropInputProduct : seedingProductInputs) {

            RefInterventionTypeItemInputEDI refInterventionTypeItemInputEDI = refInterventionTypeItemInputEDIDao.forInputEdiTypeCodeEquals(cropInputProduct.getTypeCode()).findAnyOrNull();
            if (refInterventionTypeItemInputEDI == null) {
                interventionContext.formatError("L'intrant déclaré sur la culture '%s', intervention '%s', parcelle '%s', domaine '%s', a un 'Typecode': '%s' qui n'est pas géré par Agrosyst. L'intrant ne sera pas importé. Merci de contacter l'équipe Agrosyst.",
                        formatCrop(interventionContext), formatIntervention(interventionContext.getEffectiveIntervention()), formatPlot(interventionContext), formatDomain(interventionContext), cropInputProduct.getTypeCode());
                continue;
            }

            PhytoProductInputContext phytoProductInputContext = buildPhytoProductInputContext(
                    interventionContext,
                    cropInputProduct,
                    InputType.TRAITEMENT_SEMENCE);

            RefActaTraitementsProduit refInput = phytoProductInputContext.refInput();
            if (refInput == null || refInput == defaultRefActaTraitementsProduit) {
                //Si aucun AMM n’est fourni ou si la valeur n’est pas retrouvée dans notre
                //référentiel (pour les les 3 traitements en question) on coche simplement
                //la case « Traitement chimique des semences / plants » + INFO à
                //l’utilisateur.
                chemicalTreatment = true;
            } else {
                ChemicalORBiologicalTraitment chemicalOrBiologicalTraitment = getChemicalOBiologicalTreatment(phytoProductInputContext.refInput);
                if (chemicalOrBiologicalTraitment.biologicalSeedInoculation) {
                    biologicalSeedInoculation = true;
                }
                if (chemicalOrBiologicalTraitment.chemicalTreatment) {
                    chemicalTreatment = true;
                }
            }

            phytoProductInputContexts.add(phytoProductInputContext);
        }
        ChemicalORBiologicalTraitment seedingActionUsageChemicalORBiologicalTraitment = new ChemicalORBiologicalTraitment(biologicalSeedInoculation, chemicalTreatment);
        InterventionPhytoProductContext interventionPhytoProductContext = new InterventionPhytoProductContext(phytoProductInputContexts, seedingActionUsageChemicalORBiologicalTraitment);
        return interventionPhytoProductContext;
    }

    protected record InterventionPhytoProductContext(
            Collection<PhytoProductInputContext> phytoProductInputContexts,
            ChemicalORBiologicalTraitment seedingActionUsageChemicalORBiologicalTraitment
    ) {
    }


    protected void processMineralProductInput(EdaplosContext context,
                                              AgriculturalProcessCropInput cropInput,
                                              Map<AgrosystInterventionType, AbstractAction> actionsByInterventionTypes) {

        // context
        Plot plot = context.getPlot();
        InputType inputType = InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX;

        Collection<AbstractDomainInputStockUnit> persistedMineralDomainInputs = context.getDomainInputsByTypes().computeIfAbsent(inputType, k -> new ArrayList<>());

        Map<Pair<RefFertiMinUNIFA, MineralProductUnit>, DomainMineralProductInput> domainMineralProductInputByRefInputs = new HashMap<>();
        persistedMineralDomainInputs.forEach(dmi -> {
            DomainMineralProductInput dmi_ = (DomainMineralProductInput) dmi;
            RefFertiMinUNIFA refInput = dmi_.getRefInput();
            MineralProductUnit usageUnit = dmi_.getUsageUnit();
            domainMineralProductInputByRefInputs.put(Pair.of(refInput, usageUnit), dmi_);
        });

        EffectiveIntervention intervention = context.getEffectiveIntervention();

        // input
        String inputName = StringUtils.firstNonBlank(cropInput.getDescription(), defaultMineralProductName);
        // ferti min unifa
        RefFertiMinUNIFA refInput = findOrCreateMineralProduct(context, cropInput);

        MineralProductUnit usageUnit = null;

        String appliedSurfaceUnitValueMeasure = cropInput.getAppliedSurfaceUnitValueMeasure();
        String valueMeasure = cropInput.getValueMeasure();

        if (NumberUtils.isCreatable(appliedSurfaceUnitValueMeasure) || NumberUtils.isCreatable(valueMeasure)) {
            UnitCode code = NumberUtils.isCreatable(appliedSurfaceUnitValueMeasure) ?
                    cropInput.getAppliedSurfaceUnitValueMeasureUnitCode() : cropInput.getValueMeasureUnitCode();
            if (code == UnitCode.KGM) {
                usageUnit = MineralProductUnit.KG_HA;
            } else if (code == UnitCode.LTR) {
                usageUnit = MineralProductUnit.L_HA;
            }
        }

        if (usageUnit == null) {
            context.formatWarning("L'unité d'usage pour la quantité épandue (réelle ou par unité de surface) doit être renseignée pour l'intrant de l'intervention '%s' portant sur occupation du sol '%s', parcelle '%s', domaine '%s'.",
                    formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));

        }
        usageUnit = usageUnit != null ? usageUnit : MineralProductUnit.KG_HA;

        DomainMineralProductInput persistedDomainInput = domainMineralProductInputByRefInputs.get(Pair.of(refInput, usageUnit));


        if (persistedDomainInput == null) {
            boolean isPhytoEffect = false;
            DomainMineralProductInput domainInput = new DomainMineralProductInputImpl();
            domainInput.setInputType(inputType);
            domainInput.setDomain(plot.getDomain());
            domainInput.setCode(UUID.randomUUID().toString());
            domainInput.setRefInput(refInput);
            domainInput.setUsageUnit(usageUnit);
            domainInput.setInputName(inputName);
            domainInput.setInputKey(DomainInputStockUnitService.getMineralInputKey(refInput, isPhytoEffect, usageUnit));
            domainInput.setPhytoEffect(isPhytoEffect);
            domainInput = domainInputStockUnitService.createOrUpdateDomainInput(domainInput);

            persistedMineralDomainInputs.add(domainInput);

            persistedDomainInput = domainInput;
        }

        // price
        addMineralInputPrice(
                context,
                cropInput,
                refInput,
                usageUnit,
                persistedDomainInput);

        // end price

        // usage
        MineralProductInputUsage inputUsage = new MineralProductInputUsageImpl();
        inputUsage.setInputType(inputType);
        inputUsage.setDomainMineralProductInput(persistedDomainInput);
        // Quantité par unité de surface
        if (NumberUtils.isCreatable(appliedSurfaceUnitValueMeasure)) {
            double avtQuantity = Double.parseDouble(appliedSurfaceUnitValueMeasure);
            inputUsage.setQtAvg(avtQuantity);
        } else {

            // Quantité épandue réellement
            if (NumberUtils.isCreatable(valueMeasure)) {
                double divider = intervention.getSpatialFrequency() * plot.getArea();
                double avtQuantity = divider != 0 ? Double.parseDouble(valueMeasure) / divider : 0;
                avtQuantity = Precision.round(avtQuantity, 4, RoundingMode.HALF_UP.ordinal());
                inputUsage.setQtAvg(avtQuantity);
            } else {
                context.formatError("La quantité épandue (réelle ou par unité de surface) doit être renseignée pour l'intrant de l'intervention '%s' portant sur occupation du sol '%s', parcelle '%s', domaine '%s'.",
                        formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
            }
        }

        // action
        MineralFertilizersSpreadingAction mineralAction = (MineralFertilizersSpreadingAction) actionsByInterventionTypes.get(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);
        if (mineralAction == null) {
            mineralAction = new MineralFertilizersSpreadingActionImpl();
            mineralAction.setEffectiveIntervention(intervention);
            RefInterventionAgrosystTravailEDI mainAction = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("SEM").findUniqueOrNull();
            mineralAction.setMainAction(mainAction);
            mineralAction.addMineralProductInputUsages(inputUsage);
            actionsByInterventionTypes.put(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX, mineralAction);

            context.formatInfo("Pour l'intrant '%s', aucune action identifiable n' a été associée à  l'intervention'%s' sur occupation du sol '%s',  parcelle '%s', domaine '%s'. Une par défaut a été créée.",
                    formatInput(cropInput), formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));

            mineralFertilizersSpreadingActionDao.create(mineralAction);
        } else {
            mineralAction.addMineralProductInputUsages(inputUsage);
            mineralFertilizersSpreadingActionDao.update(mineralAction);

            context.formatInfo("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', pour l'intrant '%s', action mise à jour.",
                    formatPlot(context), formatSolOccupation(context), formatIntervention(context), formatInput(cropInput));
        }

    }

    protected RefFertiMinUNIFA findOrCreateMineralProduct(EdaplosContext context, AgriculturalProcessCropInput cropInput) {
        RefFertiMinUNIFA result;

        Double chemicalB = getChemical(cropInput, CropInputChemical.B);
        Double chemicalCa = getChemical(cropInput, CropInputChemical.CA);
        Double chemicalCu = getChemical(cropInput, CropInputChemical.CU);
        Double chemicalFe = getChemical(cropInput, CropInputChemical.FE);
        Double chemicalK2O = getChemical(cropInput, CropInputChemical.K2O);
        Double chemicalMn = getChemical(cropInput, CropInputChemical.MN);
        Double chemicalMg = getChemical(cropInput, CropInputChemical.MG);
        Double chemicalMo = getChemical(cropInput, CropInputChemical.MO);
        Double chemicalN = getChemical(cropInput, CropInputChemical.N_TOTAL);
        Double chemicalNA2O = getChemical(cropInput, CropInputChemical.NA2O);
        Double chemicalP2O5 = getChemical(cropInput, CropInputChemical.P2O5);
        Double chemicalS03 = getChemical(cropInput, CropInputChemical.SO3);
        Double chemicalZn = getChemical(cropInput, CropInputChemical.ZN);

        chemicalB = chemicalB == null ? 0d : chemicalB;
        chemicalCa = chemicalCa == null ? 0d : chemicalCa;
        chemicalCu = chemicalCu == null ? 0d : chemicalCu;
        chemicalFe = chemicalFe == null ? 0d : chemicalFe;
        chemicalK2O = chemicalK2O == null ? 0d : chemicalK2O;
        chemicalMn = chemicalMn == null ? 0d : chemicalMn;
        chemicalMg = chemicalMg == null ? 0d : chemicalMg;
        chemicalMo = chemicalMo == null ? 0d : chemicalMo;
        chemicalN = chemicalN == null ? 0d : chemicalN;
        chemicalNA2O = chemicalNA2O == null ? 0d : chemicalNA2O;
        chemicalP2O5 = chemicalP2O5 == null ? 0d : chemicalP2O5;
        chemicalS03 = chemicalS03 == null ? 0d : chemicalS03;
        chemicalZn = chemicalZn == null ? 0d : chemicalZn;


        // si toutes les données sont vides : on force tout à 0.999 et on indique un Warning (#10520)
        if ((chemicalB == 0) && (chemicalCa == 0) && (chemicalCu == 0) &&
                (chemicalFe == 0) && (chemicalK2O == 0) && (chemicalMn == 0) &&
                (chemicalMg == 0) && (chemicalMo == 0) && (chemicalN == 0) &&
                (chemicalNA2O == 0) && (chemicalP2O5 == 0) && (chemicalS03 == 0) &&
                (chemicalZn == 0)) {

            // Since #10520 : just return a warning with a default value for Nitrogen
            context.formatWarning("La composition chimique de l'intrant '%s', intervention '%s', " +
                            "occupation du sol '%s', parcelle '%s', domaine '%s' est manquante. L'intrant " +
                            "sera importé avec une teneur par défaut de 0,999%% pour tous les éléments chimiques. Veuillez corriger la composition chimique après import.",
                    formatInput(cropInput), formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));

            chemicalB = 0.999;
            chemicalCa = 0.999;
            chemicalCu = 0.999;
            chemicalFe = 0.999;
            chemicalK2O = 0.999;
            chemicalMn = 0.999;
            chemicalMg = 0.999;
            chemicalMo = 0.999;
            chemicalN = 0.999;
            chemicalNA2O = 0.999;
            chemicalP2O5 = 0.999;
            chemicalS03 = 0.999;
            chemicalZn = 0.999;
        }
        result = findExistingRefFertiMinUNIFA(cropInput, chemicalB, chemicalCa, chemicalCu, chemicalFe, chemicalK2O, chemicalMn, chemicalMg, chemicalMo, chemicalN, chemicalNA2O, chemicalP2O5, chemicalS03, chemicalZn);

        if (result == null) {
            result = createFertiMin(cropInput, chemicalB, chemicalCa, chemicalCu, chemicalFe, chemicalK2O, chemicalMn, chemicalMg, chemicalMo, chemicalN, chemicalNA2O, chemicalP2O5, chemicalS03, chemicalZn);
        }

        return result;
    }

    protected RefFertiMinUNIFA createFertiMin(AgriculturalProcessCropInput cropInput, Double chemicalB, Double chemicalCa, Double chemicalCu, Double chemicalFe, Double chemicalK2O, Double chemicalMn, Double chemicalMg, Double chemicalMo, Double chemicalN, Double chemicalNA2O, Double chemicalP2O5, Double chemicalS03, Double chemicalZn) {

        String qualifiant = cropInput.getSubordinateTypeCode();
        int categ = 315;
        String typeProduit = "AUTRES";
        String forme = "Granulé";
        if (StringUtils.isNotBlank(qualifiant)) {
            if (CropInputChemical.FERTI_MIN_UNIFA_CHAUX.contains(qualifiant)) {
                categ = 99998;
                typeProduit = "CHAUX";
                forme = "Solide";
            } else if (CropInputChemical.FERTI_MIN_UNIFA_SIDERURGIQUE.contains(qualifiant)) {
                categ = 251;
                typeProduit = "AMENDEMENTS-ENGRAIS SIDERURGIQUES BASIQUES";
            }
        }

        RefFertiMinUNIFA refFertiMinUNIFA = new RefFertiMinUNIFAImpl();
        refFertiMinUNIFA.setCateg(categ);
        refFertiMinUNIFA.setType_produit(typeProduit);
        refFertiMinUNIFA.setForme(forme);
        refFertiMinUNIFA.setBore(chemicalB);
        refFertiMinUNIFA.setCalcium(chemicalCa);
        refFertiMinUNIFA.setCuivre(chemicalCu);
        refFertiMinUNIFA.setFer(chemicalFe);
        refFertiMinUNIFA.setK2O(chemicalK2O);
        refFertiMinUNIFA.setManganese(chemicalMn);
        refFertiMinUNIFA.setMgO(chemicalMg);
        refFertiMinUNIFA.setMolybdene(chemicalMo);
        refFertiMinUNIFA.setN(chemicalN);
        refFertiMinUNIFA.setOxyde_de_sodium(chemicalNA2O);
        refFertiMinUNIFA.setP2O5(chemicalP2O5);
        refFertiMinUNIFA.setsO3(chemicalS03);
        refFertiMinUNIFA.setZinc(chemicalZn);
        refFertiMinUNIFA.setSource(UserService.AGROSYST_USER);
        refFertiMinUNIFA.setActive(true);
        refFertiMinUNIFADao.create(refFertiMinUNIFA);

        return refFertiMinUNIFA;
    }

    protected RefFertiMinUNIFA findExistingRefFertiMinUNIFA(AgriculturalProcessCropInput cropInput, Double chemicalB, Double chemicalCa, Double chemicalCu, Double chemicalFe, Double chemicalK2O, Double chemicalMn, Double chemicalMg, Double chemicalMo, Double chemicalN, Double chemicalNA2O, Double chemicalP2O5, Double chemicalS03, Double chemicalZn) {
        RefFertiMinUNIFA result;
        TopiaQueryBuilderAddCriteriaOrRunQueryStep<RefFertiMinUNIFA> refFertiMinUNIFAQueryStep = null;
        String qualifiant = cropInput.getSubordinateTypeCode();
        if (StringUtils.isNotBlank(qualifiant)) {
            if (CropInputChemical.FERTI_MIN_UNIFA_CHAUX.contains(qualifiant)) {
                refFertiMinUNIFAQueryStep = refFertiMinUNIFADao.forCategEquals(99998);
            } else if (CropInputChemical.FERTI_MIN_UNIFA_SIDERURGIQUE.contains(qualifiant)) {
                refFertiMinUNIFAQueryStep = refFertiMinUNIFADao.forCategEquals(251);
            }
        }
        if (refFertiMinUNIFAQueryStep == null) {
            refFertiMinUNIFAQueryStep = refFertiMinUNIFADao.forCategEquals(315); // AUTRES
        }

        result = refFertiMinUNIFAQueryStep.addEquals(RefFertiMinUNIFA.PROPERTY_BORE, chemicalB)
                .addEquals(RefFertiMinUNIFA.PROPERTY_CALCIUM, chemicalCa)
                .addEquals(RefFertiMinUNIFA.PROPERTY_CUIVRE, chemicalCu)
                .addEquals(RefFertiMinUNIFA.PROPERTY_FER, chemicalFe)
                .addEquals(RefFertiMinUNIFA.PROPERTY_K2_O, chemicalK2O)
                .addEquals(RefFertiMinUNIFA.PROPERTY_MANGANESE, chemicalMn)
                .addEquals(RefFertiMinUNIFA.PROPERTY_MG_O, chemicalMg)
                .addEquals(RefFertiMinUNIFA.PROPERTY_MOLYBDENE, chemicalMo)
                .addEquals(RefFertiMinUNIFA.PROPERTY_N, chemicalN)
                .addEquals(RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, chemicalNA2O)
                .addEquals(RefFertiMinUNIFA.PROPERTY_P2_O5, chemicalP2O5)
                .addEquals(RefFertiMinUNIFA.PROPERTY_S_O3, chemicalS03)
                .addEquals(RefFertiMinUNIFA.PROPERTY_ZINC, chemicalZn)
                .findAnyOrNull();
        return result;
    }

    protected Double getChemical(AgriculturalProcessCropInput cropInput, String typeCode) {
        Double chemical = ListUtils.emptyIfNull(cropInput.getComponentCropInputChemicals()).stream()
                .filter(ccic -> ccic.getTypeCode() != null)
                .filter(ccic -> typeCode.equals(ccic.getTypeCode()))
                .map(CropInputChemical::getPresencePercent)
                .filter(Objects::nonNull)
                .filter(NumberUtils::isCreatable)
                .map(Double::parseDouble)
                .findFirst().orElse(null);

        if (chemical == null && typeCode.equals(CropInputChemical.N_TOTAL)) {
            Double chemicalNa = getChemical(cropInput, CropInputChemical.N_AMMONIACAL);
            Double chemicalNn = getChemical(cropInput, CropInputChemical.N_NITRIQUE);
            Double chemicalNu = getChemical(cropInput, CropInputChemical.N_UREIQUE);
            if (chemicalNa != null || chemicalNn != null || chemicalNu != null) {
                chemical = 0d;
                if (chemicalNa != null) {
                    chemical += chemicalNa;
                }
                if (chemicalNn != null) {
                    chemical += chemicalNn;
                }
                if (chemicalNu != null) {
                    chemical += chemicalNu;
                }
            }
        }
        return chemical;
    }

    protected void processOrganicProductInput(EdaplosContext context,
                                              AgriculturalProcessCropInput cropInput,
                                              Map<AgrosystInterventionType, AbstractAction> actionsByInterventionTypes) {
        // context
        Plot plot = context.getPlot();

        InputType inputType = InputType.EPANDAGES_ORGANIQUES;

        EffectiveIntervention intervention = context.getEffectiveIntervention();

        Collection<AbstractDomainInputStockUnit> persistedOrganicDomainInputs = context.getDomainInputsByTypes().computeIfAbsent(inputType, k -> new ArrayList<>());

        Map<String, DomainOrganicProductInput> domainOrganicProductInputByKeys = new HashMap<>();
        persistedOrganicDomainInputs.forEach(adi -> {
            DomainOrganicProductInput input = (DomainOrganicProductInput) adi;
            RefFertiOrga refInput = input.getRefInput();
            String organicProductInputKey = DomainInputStockUnitService.getOrganicProductInputKey(
                    refInput.getIdtypeeffluent(),
                    input.getUsageUnit(),
                    input.getN(),
                    input.getP2O5(),
                    input.getK2O(),
                    input.getCaO(),
                    input.getMgO(),
                    input.getS(),
                    input.isOrganic()
            );
            domainOrganicProductInputByKeys.put(organicProductInputKey, input);
        });

        // input
        // qualifiant
        RefFertiOrga refInput = null;
        String qualifiant = cropInput.getSubordinateTypeCode();
        if (StringUtils.isNotBlank(qualifiant)) {
            refInput = refFertiOrgaDao.forIdtypeeffluentEquals(qualifiant).findUniqueOrNull();
        }

        if (refInput == null) {
            refInput = defaultRefFertiOrga;
        }

        if (refInput == defaultRefFertiOrga) {
            context.formatWarning("Le qualifiant EDI '%s' de l'intrant '%s', intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' est inconnu du référentiel Utilisation du fertilisant organique par défault idtypeeffluent: '" + refInput.getIdtypeeffluent() + "'.",
                    qualifiant, formatInput(cropInput), formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
        }

        // Unité d'usage
        OrganicProductUnit usageUnit = null;

        String appliedSurfaceUnitValueMeasure = cropInput.getAppliedSurfaceUnitValueMeasure();
        String valueMeasure = cropInput.getValueMeasure();

        if (NumberUtils.isCreatable(appliedSurfaceUnitValueMeasure) || NumberUtils.isCreatable(valueMeasure)) {
            UnitCode code = NumberUtils.isCreatable(appliedSurfaceUnitValueMeasure) ?
                    cropInput.getAppliedSurfaceUnitValueMeasureUnitCode() : cropInput.getValueMeasureUnitCode();
            if (code == UnitCode.TNE) {
                usageUnit = OrganicProductUnit.T_HA;
            } else if (code == UnitCode.ZHL) {
                usageUnit = OrganicProductUnit.M_CUB_HA;
            }
        }

        if (usageUnit == null) {
            context.formatWarning("L'unité d'usage pour la quantité épandue (réelle ou par unité de surface) doit être renseignée pour l'intrant de l'intervention '%s' portant sur occupation du sol '%s', parcelle '%s', domaine '%s'.",
                    formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));

        }
        usageUnit = usageUnit != null ? usageUnit : OrganicProductUnit.KG_HA;

        // composition chimique
        Double chemicalP = getChemical(cropInput, CropInputChemical.P2O5);
        Double chemicalK = getChemical(cropInput, CropInputChemical.K2O);
        Double chemicalN = getChemical(cropInput, CropInputChemical.N_TOTAL);

        if (chemicalP == null) {
            chemicalP = refInput.getTeneur_Ferti_Orga_P();
        }
        if (chemicalN == null) {
            chemicalN = refInput.getTeneur_Ferti_Orga_N_total();
        }
        if (chemicalK == null) {
            chemicalK = refInput.getTeneur_Ferti_Orga_K();
        }
        if (chemicalP == null) {
            chemicalP = 999d;
        }
        if (chemicalN == null) {
            chemicalN = 999d;
        }
        if (chemicalK == null) {
            chemicalK = 999d;
        }

        String inputKey = DomainInputStockUnitService.getOrganicProductInputKey(
                refInput.getIdtypeeffluent(),
                usageUnit,
                chemicalN,
                chemicalP,
                chemicalK,
                null,
                null,
                null,
                false
        );
        DomainOrganicProductInput persistedDomainInput = domainOrganicProductInputByKeys.get(inputKey);

        // input
        String inputName = StringUtils.firstNonBlank(cropInput.getDescription(), defaultOrganicProductName);

        if (persistedDomainInput == null) {
            DomainOrganicProductInput domainInput = new DomainOrganicProductInputImpl();
            domainInput.setInputType(inputType);
            domainInput.setDomain(plot.getDomain());
            domainInput.setCode(UUID.randomUUID().toString());
            domainInput.setRefInput(refInput);
            domainInput.setUsageUnit(usageUnit);
            domainInput.setInputName(inputName);
            domainInput.setInputKey(inputKey);
            domainInput.setP2O5(chemicalP);
            domainInput.setK2O(chemicalK);
            domainInput.setN(chemicalN);

            domainInput = domainInputStockUnitService.createOrUpdateDomainInput(domainInput);

            persistedOrganicDomainInputs.add(domainInput);

            persistedDomainInput = domainInput;

        }

        addOrganicInputPrice(
                context,
                cropInput,
                refInput,
                usageUnit,
                persistedDomainInput);

        // usage
        OrganicProductInputUsage inputUsage = new OrganicProductInputUsageImpl();
        inputUsage.setInputType(inputType);
        inputUsage.setDomainOrganicProductInput(persistedDomainInput);

        // Quantité réellement épandue
        if (NumberUtils.isCreatable(cropInput.getValueMeasure())) {
            inputUsage.setQtAvg(Double.parseDouble(cropInput.getValueMeasure()));
        }

        // Quantité par unité de surface
        if (NumberUtils.isCreatable(appliedSurfaceUnitValueMeasure)) {
            double avtQuantity = Double.parseDouble(appliedSurfaceUnitValueMeasure);
            inputUsage.setQtAvg(avtQuantity);
        } else {

            // Quantité épandue réellement
            if (NumberUtils.isCreatable(valueMeasure)) {
                double divider = intervention.getSpatialFrequency() * plot.getArea();
                double avtQuantity = divider != 0 ? Double.parseDouble(valueMeasure) / divider : 0;
                avtQuantity = Precision.round(avtQuantity, 4, RoundingMode.HALF_UP.ordinal());
                inputUsage.setQtAvg(avtQuantity);
            } else {
                context.formatError("La quantité épandue (réelle ou par unité de surface) doit être renseignée pour l'intrant de l'intervention '%s' portant sur occupation du sol '%s', parcelle '%s', domaine '%s'.",
                        formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
            }
        }

        // action
        OrganicFertilizersSpreadingAction organicAction = (OrganicFertilizersSpreadingAction) actionsByInterventionTypes.get(AgrosystInterventionType.EPANDAGES_ORGANIQUES);
        if (organicAction == null) {
            organicAction = new OrganicFertilizersSpreadingActionImpl();
            RefInterventionAgrosystTravailEDI mainAction = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("SEK").findUniqueOrNull();
            organicAction.setMainAction(mainAction);
            organicAction.setLandfilledWaste(false);
            organicAction.setEffectiveIntervention(intervention);
            organicAction.addOrganicProductInputUsages(inputUsage);
            organicAction = organicFertilizersSpreadingActionsDao.create(organicAction);
            actionsByInterventionTypes.put(AgrosystInterventionType.EPANDAGES_ORGANIQUES, organicAction);
            context.formatInfo("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', pour l'intrant '%s', aucune action correspondante n'a pu être trouvée. Une par défaut a été créée.",
                    formatPlot(context), formatSolOccupation(context), formatIntervention(context), formatInput(cropInput));
        } else {
            organicAction.addOrganicProductInputUsages(inputUsage);
            organicFertilizersSpreadingActionsDao.update(organicAction);
            context.formatInfo("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', pour l'intrant '%s', action mise à jour.",
                    formatPlot(context), formatSolOccupation(context), formatIntervention(context), formatInput(cropInput));
        }

    }

    protected void addOrganicInputPrice(EdaplosContext context, AgriculturalProcessCropInput cropInput, RefFertiOrga refInput, OrganicProductUnit usageUnit, DomainOrganicProductInput persistedDomainInput) {
        InputPrice inputPrice = persistedDomainInput.getInputPrice();

        String costUnit = cropInput.getCostUnit();
        if (StringUtils.isNotBlank(costUnit)) {

            if (NumberUtils.isCreatable(costUnit)) {

                if (inputPrice == null) {

                    inputPrice = new InputPriceImpl();
                    inputPrice.setDomain(context.getDomain());
                    inputPrice.setCategory(InputPriceCategory.ORGANIQUES_INPUT);
                    inputPrice.setSourceUnit(usageUnit.name());

                    String objectid = InputPriceService.GET_REF_FERTI_ORGA_OBJECT_ID.apply(refInput);
                    inputPrice.setObjectId(objectid);

                    String displayName = InputPriceService.GET_REF_FERTI_ORGA_DISPLAY_NAME.apply(refInput);
                    inputPrice.setDisplayName(displayName);

                    inputPrice.setPrice(Double.parseDouble(costUnit));

                    if (OrganicProductUnit.T_HA == usageUnit) {
                        inputPrice.setPriceUnit(PriceUnit.EURO_T);
                    } else if (OrganicProductUnit.M_CUB_HA == usageUnit) {
                        inputPrice.setPriceUnit(PriceUnit.EURO_M3);
                    } else {
                        inputPrice.setPriceUnit(PriceUnit.EURO_T);
                    }
                    inputPrice = createOrUpdateInputPrice(inputPrice);

                } else if (inputPrice.getPrice() == null) {

                    inputPrice.setPrice(Double.parseDouble(costUnit));

                    if (OrganicProductUnit.T_HA == usageUnit) {
                        inputPrice.setPriceUnit(PriceUnit.EURO_T);
                    } else if (OrganicProductUnit.M_CUB_HA == usageUnit) {
                        inputPrice.setPriceUnit(PriceUnit.EURO_M3);
                    } else {
                        inputPrice.setPriceUnit(PriceUnit.EURO_T);
                    }
                    inputPrice = createOrUpdateInputPrice(inputPrice);
                }

            } else {
                context.formatError("Le prix de l'intrant '%s', intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' n'est pas valide.",
                        formatInput(cropInput), formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
            }
        }
        if (inputPrice != null) {
            persistedDomainInput.setInputPrice(inputPrice);
            domainInputStockUnitService.createOrUpdateDomainInput(persistedDomainInput);
        }
    }

    //Phyto
    protected void processPesticideProductInput(
            EdaplosContext interventionContext,
            AgriculturalProcessCropInput cropInput,
            Map<AgrosystInterventionType, AbstractAction> actionsByInterventionTypes) {

        InputType inputType = InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES;

        PhytoProductInputContext phytoProductInputContext = buildPhytoProductInputContext(
                interventionContext,
                cropInput,
                inputType);

        if (phytoProductInputContext == null || phytoProductInputContext.refInput == defaultRefActaTraitementsProduit)
            return;

        DomainPhytoProductInput domainInput = getOrCreateDomainPhytoProductInput(
                interventionContext,
                phytoProductInputContext,
                inputType,
                InputPriceCategory.PHYTO_TRAITMENT_INPUT);

        // action
        double treatedSurface = getPhytoTreatedSurface(interventionContext.getEffectiveIntervention(), interventionContext);

        PesticideProductInputUsage usage = newPesticideProductInputUsage(
                interventionContext,
                phytoProductInputContext,
                domainInput,
                treatedSurface);

        PesticidesSpreadingAction phytoAction = (PesticidesSpreadingAction) actionsByInterventionTypes.get(APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        if (phytoAction == null) {
            Double boiledQuantity = findAndGetBoiledQuantity(interventionContext, cropInput, phytoProductInputContext.refInput());

            phytoAction = createNewPhytoActionWithDefaultValues(interventionContext.effectiveIntervention, null, null, interventionContext);
            phytoAction.setProportionOfTreatedSurface(treatedSurface);
            phytoAction.setBoiledQuantity(boiledQuantity);
            phytoAction.addPesticideProductInputUsages(usage);
            phytoAction = pesticidesSpreadingActionDao.create(phytoAction);
            actionsByInterventionTypes.put(APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, phytoAction);

            interventionContext.formatInfo("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', pour l'intrant '%s', aucune action correspondante n'a pu être trouvée. Une par défaut a été créée.",
                    formatPlot(interventionContext), formatSolOccupation(interventionContext), formatIntervention(interventionContext), formatInput(cropInput));
        } else {
            if (DEFAULT_BOILED_QUANTITY == phytoAction.getBoiledQuantity()) {
                Double boiledQuantity = findAndGetBoiledQuantity(interventionContext, cropInput, phytoProductInputContext.refInput());
                phytoAction.setBoiledQuantity(boiledQuantity);
            }
            if (DEFAULT_PROPORTION_OF_TREATED_SURFACE == phytoAction.getProportionOfTreatedSurface()) {
                phytoAction.setProportionOfTreatedSurface(treatedSurface);
            }
            phytoAction.addPesticideProductInputUsages(usage);
            pesticidesSpreadingActionDao.update(phytoAction);
        }

    }

    protected DomainIrrigationInput getOrCreateDomainIrrigationInput(
            EdaplosContext context) {

        Collection<AbstractDomainInputStockUnit> persistedIrrigationDomainInputs = context.getDomainInputsByTypes().computeIfAbsent(InputType.IRRIGATION, k -> new ArrayList<>());

        DomainIrrigationInput domainIrrigationInput;
        if (CollectionUtils.isEmpty(persistedIrrigationDomainInputs)) {
            domainIrrigationInput = domainInputStockUnitService.newDomainIrrigationInput(context.getDomain());
            context.getDomainInputsByTypes().put(InputType.IRRIGATION, Collections.singletonList(domainIrrigationInput));
        } else {
            domainIrrigationInput = (DomainIrrigationInput) persistedIrrigationDomainInputs.iterator().next();
        }

        return domainIrrigationInput;
    }

    protected PesticidesSpreadingAction createNewPhytoActionWithDefaultValues(
            EffectiveIntervention intervention,
            RefInterventionAgrosystTravailEDI travailEdi,
            String description,
            EdaplosContext interventionContext) {

        PesticidesSpreadingAction phytoAction;

        phytoAction = new PesticidesSpreadingActionImpl();
        phytoAction.setAntiDriftNozzle(false);

        phytoAction.setBoiledQuantity(DEFAULT_BOILED_QUANTITY);
        travailEdi = travailEdi == null ? refInterventionAgrosystTravailEDIDao.forReference_codeEquals(PHYTO_TYPE_CODE).findUniqueOrNull() : travailEdi;
        addCommonActionFields(phytoAction, travailEdi, intervention, description);
        double treatedSurface = getPhytoTreatedSurface(intervention, interventionContext);
        phytoAction.setProportionOfTreatedSurface(treatedSurface);

        return phytoAction;
    }

    protected Double findAndGetBoiledQuantity(EdaplosContext context, AgriculturalProcessCropInput cropInput, RefActaTraitementsProduit refActaTraitementsProduit) {
        TechnicalCharacteristic characteristicBoiled = cropInput.getApplicableTechnicalCharacteristics().stream()
                .filter(c -> TechnicalCharacteristic.TYPE_CODE_VOLUME_BOUILLIE.equals(c.getTypeCode()))
                .findAny().orElse(null);

        Double boiledQuantity = null;
        if (characteristicBoiled != null) {
            String valueBoiled = characteristicBoiled.getValueMeasure();
            if (!NumberUtils.isCreatable(valueBoiled)) {
                context.formatError("Le volume de bouillie de l'intrant '%s (%s)' n'est pas correct pour l'intervention '%s', occupation du sol '%s', parcelle '%s' du domaine '%s'.",
                        refActaTraitementsProduit.getNom_produit(), cropInput.getIdentification(), formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
            } else if (characteristicBoiled.getValueMeasureUnitCode() != UnitCode.LTR) {
                context.formatError("L'unité du volume de bouillie de l'intrant '%s (%s)' n'est pas correcte pour l'intervention '%s', occupation du sol '%s', parcelle '%s' du domaine '%s'.",
                        refActaTraitementsProduit.getNom_produit(), cropInput.getIdentification(), formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
            } else {
                boiledQuantity = Double.parseDouble(valueBoiled) / 100;
            }
        } else {
            context.formatInfo("Aucun volume de bouillie renseigné pour l'intrant '%s (%s)', portant sur occupation du sol '%s' pour l'intervention '%s', parcelle '%s', domaine '%s'. Volume ajouté par défaut : 1,5 hL",
                    refActaTraitementsProduit.getNom_produit(), cropInput.getIdentification(), formatSolOccupation(context), formatIntervention(context), formatPlot(context), formatDomain(context));
            boiledQuantity = 1.5;
        }

        return boiledQuantity;
    }

    protected void processBiologicalProductInput(EdaplosContext context,
                                                 AgriculturalProcessCropInput cropInput,
                                                 Map<AgrosystInterventionType, AbstractAction> actionsByInterventionTypes) {

        InputType inputType = InputType.LUTTE_BIOLOGIQUE;

        PhytoProductInputContext phytoProductInputContext = buildPhytoProductInputContext(
                context,
                cropInput,
                inputType);

        if (phytoProductInputContext == null) return;

        DomainPhytoProductInput domainInput = getOrCreateDomainPhytoProductInput(
                context,
                phytoProductInputContext,
                inputType,
                InputPriceCategory.BIOLOGICAL_CONTROL_INPUT);

        // action
        double treatedSurface = getPhytoTreatedSurface(context.getEffectiveIntervention(), context);

        BiologicalProductInputUsage usage = newBiologicalProductInputUsage(
                context,
                phytoProductInputContext, domainInput,
                treatedSurface
        );

        BiologicalControlAction biologicalControlAction = (BiologicalControlAction) actionsByInterventionTypes.get(AgrosystInterventionType.LUTTE_BIOLOGIQUE);
        if (biologicalControlAction == null) {
            Double boiledQuantity = findAndGetBoiledQuantity(context, cropInput, phytoProductInputContext.refInput());

            biologicalControlAction = createNewBiologicalControlActionWithDefaultValues(context.effectiveIntervention, null, null, context);
            biologicalControlAction.setProportionOfTreatedSurface(treatedSurface);
            biologicalControlAction.setBoiledQuantity(boiledQuantity);
            biologicalControlAction.addBiologicalProductInputUsages(usage);
            biologicalControlAction = biologicalControlActionDao.create(biologicalControlAction);
            actionsByInterventionTypes.put(AgrosystInterventionType.LUTTE_BIOLOGIQUE, biologicalControlAction);

            context.formatInfo("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', pour l'intrant '%s', aucune action correspondante n'a pu être trouvée. Une par défaut a été créée.",
                    formatPlot(context), formatSolOccupation(context), formatIntervention(context), formatInput(cropInput));
        } else {
            if (DEFAULT_BOILED_QUANTITY == biologicalControlAction.getBoiledQuantity()) {
                Double boiledQuantity = findAndGetBoiledQuantity(context, cropInput, phytoProductInputContext.refInput());
                biologicalControlAction.setBoiledQuantity(boiledQuantity);
            }
            if (DEFAULT_PROPORTION_OF_TREATED_SURFACE == biologicalControlAction.getProportionOfTreatedSurface()) {
                biologicalControlAction.setProportionOfTreatedSurface(treatedSurface);
            }
            biologicalControlAction.addBiologicalProductInputUsages(usage);
            biologicalControlActionDao.update(biologicalControlAction);
        }

    }

    protected DomainPhytoProductInput getOrCreateDomainPhytoProductInput(
            EdaplosContext context,
            PhytoProductInputContext phytoProductInputContext,
            InputType inputType,
            InputPriceCategory phytoTreatmentInputCategory) {

        Collection<AbstractDomainInputStockUnit> persistedPhytoDomainInputs = context.getDomainInputsByTypes().computeIfAbsent(inputType, k -> new ArrayList<>());
        Map<String, DomainPhytoProductInput> domainPhytoProductInputByKeys = getDomainPhytoProductInputByKeys(persistedPhytoDomainInputs);

        AgriculturalProcessCropInput cropInput = phytoProductInputContext.productInput;
        DomainPhytoProductInput persistedDomainInput = domainPhytoProductInputByKeys.get(phytoProductInputContext.inputKey);

        DomainPhytoProductInput productInput;
        if (persistedDomainInput == null) {
            ProductType typeProduit = null;
            RefActaTraitementsProduit refInput = phytoProductInputContext.refInput();
            RefActaTraitementsProduitsCateg refActaTraitementsProduitsCateg =
                    refActaTraitementsProduitsCategDao.forId_traitementEquals(refInput.getId_traitement()).findAnyOrNull();
            if (refActaTraitementsProduitsCateg != null) {
                typeProduit = refActaTraitementsProduitsCateg.getType_produit();

            }

            productInput = getNewDomainPhytoProductInput(
                    context,
                    cropInput,
                    phytoProductInputContext,
                    inputType,
                    typeProduit,
                    refInput);

            productInput = domainInputStockUnitService.createOrUpdateDomainInput(productInput);

            persistedPhytoDomainInputs.add(productInput);

        } else {
            productInput = persistedDomainInput;
        }

        // price
        addPhytoInputPrice(
                context,
                phytoProductInputContext,
                productInput,
                phytoTreatmentInputCategory);

        return productInput;
    }

    protected static Map<String, DomainPhytoProductInput> getDomainPhytoProductInputByKeys(Collection<AbstractDomainInputStockUnit> persistedPhytoDomainInputs) {
        Map<String, DomainPhytoProductInput> domainPhytoProductInputByKeys = new HashMap<>();
        persistedPhytoDomainInputs.forEach(adi -> {
            DomainPhytoProductInput input = (DomainPhytoProductInput) adi;
            input.getInputKey();
            String inputKey = DomainInputStockUnitService.getPhytoInputKey(
                    input.getRefInput(),
                    input.getUsageUnit()
            );
            domainPhytoProductInputByKeys.put(inputKey, input);
        });
        return domainPhytoProductInputByKeys;
    }

    protected DomainPhytoProductInput getNewDomainPhytoProductInput(
            EdaplosContext context,
            AgriculturalProcessCropInput cropInput,
            PhytoProductInputContext phytoProductInputContext,
            InputType inputType,
            ProductType typeProduit,
            RefActaTraitementsProduit refInput) {

        DomainPhytoProductInput productInput;
        String ammProductName = cropInput.getDescription();

        String productName;
        if (StringUtils.isNotBlank(ammProductName) && !isSameDespiteWhitespacesOrAccents(refInput.getNom_produit(), ammProductName)) {
            productName = ammProductName;
        } else {
            productName = refInput.getNom_produit();
        }

        String inputName = StringUtils.firstNonBlank(productName, "?");

        String phytoInputKey = DomainInputStockUnitService.getPhytoInputKey(refInput, phytoProductInputContext.usageUnit());

        productInput = new DomainPhytoProductInputImpl();
        productInput.setRefInput(refInput);
        productInput.setProductType(typeProduit);
        productInput.setInputType(inputType);
        productInput.setDomain(context.getDomain());
        productInput.setCode(UUID.randomUUID().toString());
        productInput.setUsageUnit(phytoProductInputContext.usageUnit());
        productInput.setInputName(inputName);
        productInput.setInputKey(phytoInputKey);
        return productInput;
    }

    protected PhytoProductInputContext buildPhytoProductInputContext(
            EdaplosContext interventionContext,
            AgriculturalProcessCropInput cropInput,
            InputType inputType) {

        RefActaTraitementsProduit refInput = retrievePhytoProductTreatment(
                interventionContext,
                cropInput.getIdentification(),
                cropInput.getDescription(),
                cropInput.getTypeCode(),
                InputType.TRAITEMENT_SEMENCE.equals(inputType));

        if (refInput == null || refInput == defaultRefActaTraitementsProduit) {
            refInput = defaultRefActaTraitementsProduit;
            String qualifiant = cropInput.getSubordinateTypeCode();
            interventionContext.formatWarning("Le qualifiant EDI '%s' de l'intrant '%s', intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' est inconnu du référentiel Agrosyst. Import impossible de celui ci.",
                    qualifiant, formatInput(cropInput), formatIntervention(interventionContext), formatSolOccupation(interventionContext), formatPlot(interventionContext), formatDomain(interventionContext));
        }

        CroppingPlanEntry croppingPlanEntry = interventionContext.getCroppingPlanEntry();
        Set<String> speciesId = CollectionUtils.emptyIfNull(croppingPlanEntry.getCroppingPlanSpecies()).stream().map(s -> s.getSpecies().getTopiaId()).collect(Collectors.toSet());

        PhytoProductUnit usageUnit = null;
        String appliedSurfaceUnitValueMeasure = cropInput.getAppliedSurfaceUnitValueMeasure();
        String valueMeasure = cropInput.getValueMeasure();

        Double treatedSurface = getPhytoTreatedSurface(interventionContext.effectiveIntervention, interventionContext);
        Double avtQuantity = getInputAverageQuantity(interventionContext, appliedSurfaceUnitValueMeasure, valueMeasure, treatedSurface);

        ReferenceDoseDTO referenceDose = refInput != null ? referentialService.computeReferenceDoseForIFT(refInput.getTopiaId(), speciesId) : null;
        boolean currentCropInputBudUsageUnit = false;


        if (NumberUtils.isCreatable(appliedSurfaceUnitValueMeasure) || NumberUtils.isCreatable(valueMeasure)) {

            if (referenceDose != null) {

                UnitCode unitCode = NumberUtils.isCreatable(appliedSurfaceUnitValueMeasure) ? cropInput.getAppliedSurfaceUnitValueMeasureUnitCode() : cropInput.getValueMeasureUnitCode();
                Double refAvtQuantity = convertEdiToAgrosystPhytoUnit(unitCode, referenceDose.getUnit(), avtQuantity);

                if (refAvtQuantity != null) {
                    usageUnit = referenceDose.getUnit();
                }

            } else {

                UnitCode code = NumberUtils.isCreatable(appliedSurfaceUnitValueMeasure) ? cropInput.getAppliedSurfaceUnitValueMeasureUnitCode() : cropInput.getValueMeasureUnitCode();
                if (code == UnitCode.KGM) {
                    usageUnit = PhytoProductUnit.KG_HA;
                } else if (code == UnitCode.TNE) {
                    usageUnit = PhytoProductUnit.T_HA;
                } else if (code == UnitCode.LTR) {
                    usageUnit = PhytoProductUnit.L_HA;
                } else if (code == UnitCode.W95) {
                    usageUnit = PhytoProductUnit.G_L;
                } else if (code == UnitCode.PCD) {
                    usageUnit = PhytoProductUnit.PERCENT;
                }
            }
        }

        if (usageUnit == null) {
            avtQuantity = null;
            currentCropInputBudUsageUnit = true;
            interventionContext.formatWarning("L'unité d'usage pour la quantité épandue (réelle ou par unité de surface) doit être renseignée pour l'intrant de l'intervention '%s' portant sur occupation du sol '%s', parcelle '%s', domaine '%s'.",
                    formatIntervention(interventionContext), formatSolOccupation(interventionContext), formatPlot(interventionContext), formatDomain(interventionContext));

        }
        usageUnit = usageUnit != null ? usageUnit : PhytoProductUnit.KG_HA;

        String inputKey = refInput != null ? DomainInputStockUnitService.getPhytoInputKey(
                refInput,
                usageUnit
        ) : "INVALID";

        PhytoProductInputContext phytoProductInputContext = new PhytoProductInputContext(
                cropInput,
                refInput,
                referenceDose,
                usageUnit,
                inputKey,
                avtQuantity,
                cropInput.getSpecifiedAgriculturalProcessTargetObjects(),
                currentCropInputBudUsageUnit);

        return phytoProductInputContext;
    }

    protected static Double getInputAverageQuantity(EdaplosContext context, String appliedSurfaceUnitValueMeasure, String valueMeasure, Double treatedSurface) {
        Double avtQuantity = null;
        if (NumberUtils.isCreatable(appliedSurfaceUnitValueMeasure)) {
            avtQuantity = Double.parseDouble(appliedSurfaceUnitValueMeasure);
        } else if (NumberUtils.isCreatable(valueMeasure)) {
            double divider = context.getEffectiveIntervention().getSpatialFrequency() * context.getPlot().getArea() * treatedSurface / 100;
            avtQuantity = divider != 0 ? Double.parseDouble(valueMeasure) / divider : 0;
            avtQuantity = Precision.round(avtQuantity, 4, RoundingMode.HALF_UP.ordinal());
        }
        return avtQuantity;
    }

    protected record PhytoProductInputContext(

            AgriculturalProcessCropInput productInput,

            RefActaTraitementsProduit refInput,
            ReferenceDoseDTO referenceDose,
            PhytoProductUnit usageUnit,

            //Map<String, DomainPhytoProductInput> domainPhytoProductInputByKeys,
            String inputKey,

            Double avtQuantity,

            List<AgriculturalProcessTargetObject> specifiedAgriculturalProcessTargetObjects,
            boolean currentCropInputBudUsageUnit) {
    }

    protected void addPhytoInputPrice(
            EdaplosContext context,
            PhytoProductInputContext phytoProductInputContext,
            DomainPhytoProductInput persistedDomainInput,
            InputPriceCategory inputPriceCategory) {

        AgriculturalProcessCropInput productInput = phytoProductInputContext.productInput();
        RefActaTraitementsProduit refInput = phytoProductInputContext.refInput();
        ReferenceDoseDTO referenceDose = phytoProductInputContext.referenceDose();
        PhytoProductUnit usageUnit = phytoProductInputContext.usageUnit();

        InputPrice inputPrice = persistedDomainInput.getInputPrice();
        String costUnit = productInput.getCostUnit();
        if (StringUtils.isNotBlank(costUnit)) {

            if ((referenceDose != null && referenceDose.getUnit() != usageUnit || phytoProductInputContext.currentCropInputBudUsageUnit)) {
                context.formatInfo("Attention, le prix de l'intrant associé à l'intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' ne peut-être importé (soit l'unité de la dose associée ne correspond pas à l'unité de la dose de référence, soit l'intrant associé ne peut pas être importé). Il devra être complété manuellement après import.",
                        formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
                costUnit = null; // to not set price value
            }

            if (inputPrice == null) {

                inputPrice = new InputPriceImpl();
                inputPrice.setDomain(context.getDomain());
                inputPrice.setCategory(inputPriceCategory);
                inputPrice.setSourceUnit(usageUnit.name());

                String objectid = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(refInput);
                inputPrice.setObjectId(objectid);

                String displayName = StringUtils.firstNonBlank(InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUIT_DISPLAY_NAME.apply(refInput), productInput.getDescription(), "?");
                inputPrice.setDisplayName(displayName);

                setPriceValue(inputPrice, costUnit, context, productInput);

                if (PhytoProductUnit.KG_HA == usageUnit) {
                    inputPrice.setPriceUnit(PriceUnit.EURO_KG);
                } else {
                    inputPrice.setPriceUnit(PriceUnit.EURO_L);
                }
                inputPrice = createOrUpdateInputPrice(inputPrice);

            } else if (inputPrice.getPrice() == null) {

                setPriceValue(inputPrice, costUnit, context, productInput);

                if (PhytoProductUnit.KG_HA == usageUnit) {
                    inputPrice.setPriceUnit(PriceUnit.EURO_KG);
                } else {
                    inputPrice.setPriceUnit(PriceUnit.EURO_L);
                }
                inputPrice = createOrUpdateInputPrice(inputPrice);
            }

        }
        if (inputPrice != null) {
            persistedDomainInput.setInputPrice(inputPrice);
            domainInputStockUnitService.createOrUpdateDomainInput(persistedDomainInput);
        }
    }

    protected void setPriceValue(InputPrice price, String cost, EdaplosContext context, AgriculturalProcessCropInput cropInput) {
        if (StringUtils.isNotBlank(cost)) {
            if (NumberUtils.isCreatable(cost)) {
                price.setPrice(Double.parseDouble(cost));
            } else {
                context.formatError("Le prix de l'intrant '%s', intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' n'est pas valide.",
                        formatInput(cropInput), formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
            }
        }
    }

    protected void addMineralInputPrice(EdaplosContext context, AgriculturalProcessCropInput cropInput, RefFertiMinUNIFA refInput, MineralProductUnit usageUnit, DomainMineralProductInput persistedDomainInput) {
        InputPrice inputPrice = persistedDomainInput.getInputPrice();
        String costUnit = cropInput.getCostUnit();
        if (StringUtils.isNotBlank(costUnit)) {

            if (NumberUtils.isCreatable(costUnit)) {

                if (inputPrice == null) {

                    inputPrice = new InputPriceImpl();
                    inputPrice.setDomain(context.getDomain());
                    inputPrice.setCategory(InputPriceCategory.MINERAL_INPUT);
                    inputPrice.setSourceUnit(usageUnit.name());

                    String objectid = InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(refInput);
                    inputPrice.setObjectId(objectid);

                    String displayName = inputPriceService.getRefFertiMinUnifaDisplayName(Language.FRENCH, refInput);
                    inputPrice.setDisplayName(displayName);

                    inputPrice.setPrice(Double.parseDouble(costUnit));

                    if (MineralProductUnit.KG_HA == usageUnit) {
                        inputPrice.setPriceUnit(PriceUnit.EURO_KG);
                    } else {
                        inputPrice.setPriceUnit(PriceUnit.EURO_L);
                    }
                    inputPrice = createOrUpdateInputPrice(inputPrice);

                } else if (inputPrice.getPrice() == null) {
                    inputPrice.setPrice(Double.parseDouble(costUnit));

                    if (MineralProductUnit.KG_HA == usageUnit) {
                        inputPrice.setPriceUnit(PriceUnit.EURO_KG);
                    } else {
                        inputPrice.setPriceUnit(PriceUnit.EURO_L);
                    }
                    inputPrice = createOrUpdateInputPrice(inputPrice);
                }

            } else {
                context.formatError("Le prix de l'intrant '%s', intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' n'est pas valide.",
                        formatInput(cropInput), formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
            }
        }
        if (inputPrice != null) {
            persistedDomainInput.setInputPrice(inputPrice);
            domainInputStockUnitService.createOrUpdateDomainInput(persistedDomainInput);
        }
    }

    protected BiologicalControlAction createNewBiologicalControlActionWithDefaultValues(
            EffectiveIntervention intervention,
            RefInterventionAgrosystTravailEDI travailEdi,
            String description,
            EdaplosContext interventionContext) {

        BiologicalControlAction phytoAction = new BiologicalControlActionImpl();
        double treatedSurface = getPhytoTreatedSurface(intervention, interventionContext);
        phytoAction.setProportionOfTreatedSurface(treatedSurface);
        travailEdi = travailEdi == null ? refInterventionAgrosystTravailEDIDao.forReference_codeEquals(ORGANIC_TYPE_CODE).findUniqueOrNull() : travailEdi;
        addCommonActionFields(phytoAction, travailEdi, intervention, description);
        return phytoAction;
    }

    protected void processIrrigationInput(EdaplosContext context, AgriculturalProcessCropInput cropInput,
                                          Map<AgrosystInterventionType, AbstractAction> actionsByType) {
        // context
        EffectiveIntervention intervention = context.getEffectiveIntervention();

        // action
        IrrigationAction irrigationAction = (IrrigationAction) actionsByType.get(AgrosystInterventionType.IRRIGATION);
        if (irrigationAction == null) {

            irrigationAction = createNewIrrigationActionWithDefaultValues(intervention, context, null, null);
            irrigationAction = irrigationActionDao.create(irrigationAction);
            actionsByType.put(AgrosystInterventionType.IRRIGATION, irrigationAction);

            DomainIrrigationInput domainInput = getOrCreateDomainIrrigationInput(context);
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds = new HashMap<>();
            domainInputStockUnitByIds.put(domainInput.getTopiaId(), domainInput);
            inputUsageService.persistOrDeleteIrrigationInputUsages(domainInputStockUnitByIds, irrigationAction, context.getDomain());

            context.formatInfo("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', pour l'intrant '%s', aucune action correspondante n'a pu être trouvée. Une par défaut a été créée.",
                    formatPlot(context), formatSolOccupation(context), formatIntervention(context), formatInput(cropInput));
        }

        // input
        irrigationAction.setComment(cropInput.getDescription());

        // quantité
        if (cropInput.getAppliedSurfaceUnitValueMeasureUnitCode() == UnitCode.MMT) {
            if (NumberUtils.isCreatable(cropInput.getAppliedSurfaceUnitValueMeasure())) {
                double waterQuantityAverage = Double.parseDouble(cropInput.getAppliedSurfaceUnitValueMeasure());
                IrrigationInputUsage irrigationInputUsage = irrigationAction.getIrrigationInputUsage();
                Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds = new HashMap<>();
                domainInputStockUnitByIds.put(irrigationInputUsage.getDomainIrrigationInput().getTopiaId(), irrigationInputUsage.getDomainIrrigationInput());
                inputUsageService.persistOrDeleteIrrigationInputUsages(domainInputStockUnitByIds, irrigationAction, context.getDomain());
                irrigationInputUsage.setQtAvg(waterQuantityAverage);

                irrigationAction.setWaterQuantityAverage(waterQuantityAverage);
            } else {
                context.formatInfo("La quantité par unité de surface déclarée pour l'intrant '%s', occupation du sol '%s', intervention '%s', parcelle '%s', domaine '%s' est incorrecte.",
                        cropInput.getIdentification(), formatSolOccupation(context), formatIntervention(context), formatPlot(context), formatDomain(context));
            }
        } else {
            context.formatWarning("L'unité '%s' pour l'intrant irrigation n'est pas celle attendue '%s' pour l'intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s'. Pensez-à corriger la quantité dans Agrosyst.",
                    cropInput.getAppliedSurfaceUnitValueMeasureUnitCode(), UnitCode.MMT, formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
        }

    }

    protected MaintenancePruningVinesAction createNewMaintenancePruningVinesActionWithDefaultValues(
            EffectiveIntervention intervention, RefInterventionAgrosystTravailEDI travaiEdi, String comment) {
        MaintenancePruningVinesAction action = new MaintenancePruningVinesActionImpl();
        addCommonActionFields(action, travaiEdi, intervention, comment);
        return action;
    }

    protected void addCommonActionFields(AbstractAction action, RefInterventionAgrosystTravailEDI travailEdi, EffectiveIntervention intervention, String comment) {
        action.setEffectiveIntervention(intervention);
        action.setMainAction(travailEdi);
        if (StringUtils.isNotBlank(comment)) {
            action.setComment(comment);
        }
    }

    protected IrrigationAction createNewIrrigationActionWithDefaultValues(
            EffectiveIntervention intervention,
            EdaplosContext context,
            RefInterventionAgrosystTravailEDI travailEdi,
            String description) {

        IrrigationAction irrigationAction = new IrrigationActionImpl();
        irrigationAction.setWaterQuantityAverage(9999);
        travailEdi = travailEdi == null ? refInterventionAgrosystTravailEDIDao.forReference_codeEquals("SEO").findUniqueOrNull() : travailEdi;
        addCommonActionFields(irrigationAction, travailEdi, intervention, description);

        DomainIrrigationInput domainInput = getOrCreateDomainIrrigationInput(context);
        Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds = new HashMap<>();
        domainInputStockUnitByIds.put(domainInput.getTopiaId(), domainInput);
        IrrigationInputUsage irrigationInputUsage = inputUsageService.persistOrDeleteIrrigationInputUsages(domainInputStockUnitByIds, irrigationAction, context.getDomain());

        irrigationAction.setIrrigationInputUsage(irrigationInputUsage);

        return irrigationAction;
    }

    protected List<DomainSeedLotInput> getOrCreateDomainSeedLotInput(
            EdaplosContext interventionContext,
            ChemicalORBiologicalTraitment chemicalOrBiologicalTraitment,
            Set<SeedPlantUnit> seedingActionSpeciesByUsageUnit) {

        List<DomainSeedLotInput> domainSeedLotInputs = new ArrayList<>();

        Domain domain = interventionContext.getDomain();
        CroppingPlanEntry croppingPlanEntry = interventionContext.getCroppingPlanEntry();
        EffectiveIntervention intervention = interventionContext.getEffectiveIntervention();
        Map<InputType, List<AbstractDomainInputStockUnit>> domainInputsByTypes = interventionContext.getDomainInputsByTypes();

        Collection<EffectiveSpeciesStade> speciesStades = intervention.getSpeciesStades();
        List<CroppingPlanSpecies> interventionSpecies = speciesStades.stream().map(EffectiveSpeciesStade::getCroppingPlanSpecies).toList();

        for (SeedPlantUnit seedPlantUnit : seedingActionSpeciesByUsageUnit) {
            Optional<DomainSeedLotInput> optionalPersistedSeedLot = getOptionalPersistedDomainSeedLotInput(
                    croppingPlanEntry,
                    interventionSpecies,
                    chemicalOrBiologicalTraitment,
                    domainInputsByTypes,
                    seedPlantUnit);

            DomainSeedLotInput domainSeedLotInput = optionalPersistedSeedLot.orElseGet(() -> createDomainSeedLotInput(
                    interventionContext,
                    domain,
                    croppingPlanEntry,
                    interventionSpecies,
                    chemicalOrBiologicalTraitment,
                    seedPlantUnit
            ));

            domainSeedLotInputs.add(domainSeedLotInput);
        }
        if (domainSeedLotInputs.isEmpty()) {
            SeedPlantUnit seedPlantUnit = SeedPlantUnit.KG_PAR_HA;
            Optional<DomainSeedLotInput> optionalPersistedSeedLot = getOptionalPersistedDomainSeedLotInput(
                    croppingPlanEntry,
                    interventionSpecies,
                    chemicalOrBiologicalTraitment,
                    domainInputsByTypes,
                    seedPlantUnit);

            DomainSeedLotInput domainSeedLotInput = optionalPersistedSeedLot.orElseGet(() -> createDomainSeedLotInput(
                    interventionContext,
                    domain,
                    croppingPlanEntry,
                    interventionSpecies,
                    chemicalOrBiologicalTraitment,
                    seedPlantUnit
            ));

            domainSeedLotInputs.add(domainSeedLotInput);
        }

        return domainSeedLotInputs;
    }

    protected DomainSeedLotInput createDomainSeedLotInput(
            EdaplosContext interventionContext,
            Domain domain,
            CroppingPlanEntry croppingPlanEntry,
            List<CroppingPlanSpecies> interventionSpecies,
            ChemicalORBiologicalTraitment chemicalOrBiologicalTraitment,
            SeedPlantUnit seedPlantUnit) {
        Set<String> subordinateTypeCodes = interventionContext.getPlotAgriculturalProcess().getUsedAgriculturalProcessCropInputs().stream()
                .filter(c -> AgriculturalProcessCropInput.PLANTS.equals(c.getTypeCode()) || AgriculturalProcessCropInput.SEMENCE.equals(c.getTypeCode()))
                .map(AgriculturalProcessCropInput::getSubordinateTypeCode)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());

        SeedType seedType = AgriculturalProcessCropInput.FROM_CODE_SEMENCE_TO_SEED_TYPE.apply(subordinateTypeCodes);

        Map<SeedType, String> seedTypeTranslations = interventionContext.getSeedTypeTranslations();
        final DomainSeedLotInput domainSeedLotInput = new DomainSeedLotInputImpl();

        // #13318 Dans les lots de semence, créer par défaut en "Semences certifiées" + Traitement chimique "Oui" + Inoculation biologique "Non"
        // Cela couvrirait 95% des cas.
        boolean biologicalSeedInoculation = chemicalOrBiologicalTraitment != null && chemicalOrBiologicalTraitment.biologicalSeedInoculation();
        boolean chemicalTreatment = chemicalOrBiologicalTraitment == null || chemicalOrBiologicalTraitment.chemicalTreatment();

        String lotName = domainInputStockUnitService.getDomainSeedLotName(
                croppingPlanEntry,
                seedType,
                interventionContext.getSeedTypeTranslations(),
                biologicalSeedInoculation,
                chemicalTreatment,
                Locale.FRANCE
        );

        String lotInputKey = DomainInputStockUnitService.getLotCropSeedInputKey(
                croppingPlanEntry,
                false,
                seedPlantUnit);

        domainSeedLotInput.setInputType(InputType.SEMIS);
        domainSeedLotInput.setDomain(domain);
        domainSeedLotInput.setCode(UUID.randomUUID().toString());
        domainSeedLotInput.setInputKey(lotInputKey);
        domainSeedLotInput.setCropSeed(croppingPlanEntry);
        domainSeedLotInput.setOrganic(false);
        domainSeedLotInput.setInputName(lotName);
        domainSeedLotInput.setUsageUnit(seedPlantUnit);

        CollectionUtils.emptyIfNull(interventionSpecies).forEach(cps -> {
            String speciesInputName = domainInputStockUnitService.getDomainSeedSpeciesInputName(
                    cps,
                    seedType,
                    seedTypeTranslations,
                    biologicalSeedInoculation,
                    chemicalTreatment,
                    Locale.FRANCE);

            final RefVariete variety = cps.getVariety();
            final RefEspece species = cps.getSpecies();
            final String vLabel = variety != null ? variety.getLabel() : "";

            String speciesInputKey = DomainInputStockUnitService.getLotSpeciesInputKey(
                    species.getCode_espece_botanique(),
                    species.getCode_qualifiant_AEE(),
                    biologicalSeedInoculation,
                    chemicalTreatment,
                    false,
                    vLabel,
                    seedPlantUnit,
                    seedType);

            final DomainSeedSpeciesInput domainSeedSpeciesInput = new DomainSeedSpeciesInputImpl();
            domainSeedSpeciesInput.setDomain(domain);
            domainSeedSpeciesInput.setCode(UUID.randomUUID().toString());
            domainSeedSpeciesInput.setInputKey(speciesInputKey);
            domainSeedSpeciesInput.setSpeciesSeed(cps);
            domainSeedSpeciesInput.setOrganic(false);
            domainSeedSpeciesInput.setInputType(InputType.SEMIS);
            domainSeedSpeciesInput.setSeedType(seedType);
            domainSeedSpeciesInput.setInputName(speciesInputName);
            domainSeedSpeciesInput.setChemicalTreatment(chemicalTreatment);
            domainSeedSpeciesInput.setBiologicalSeedInoculation(biologicalSeedInoculation);

            domainSeedLotInput.addDomainSeedSpeciesInput(domainSeedSpeciesInput);
        });

        Collection<AbstractDomainInputStockUnit> persistedDomainSeedSpeciesInputs = interventionContext.getDomainInputsByTypes().computeIfAbsent(InputType.SEMIS, k -> new ArrayList<>());
        persistedDomainSeedSpeciesInputs.add(domainSeedLotInput);

        return domainSeedLotInput;
    }

    protected static Optional<DomainSeedLotInput> getOptionalPersistedDomainSeedLotInput(
            CroppingPlanEntry croppingPlanEntry,
            List<CroppingPlanSpecies> interventionSpecies,
            ChemicalORBiologicalTraitment chemicalOrBiologicalTraitment,
            Map<InputType, List<AbstractDomainInputStockUnit>> domainInputsByTypes,
            final SeedPlantUnit seedPlantUnit) {

        Collection<AbstractDomainInputStockUnit> seedingDomainInputStockUnits = domainInputsByTypes.computeIfAbsent(InputType.SEMIS, k -> new ArrayList<>());
        Collection<AbstractDomainInputStockUnit> seedingPlanCompagneDomainInputStockUnits = domainInputsByTypes.computeIfAbsent(InputType.PLAN_COMPAGNE, k -> new ArrayList<>());

        List<AbstractDomainInputStockUnit> persistedSeedLotDomainInputs = new ArrayList<>(seedingDomainInputStockUnits);
        persistedSeedLotDomainInputs.addAll(seedingPlanCompagneDomainInputStockUnits);

        // #13318 Dans les lots de semence, créer par défaut en "Semences certifiées" + Traitement chimique "Oui" + Inoculation biologique "Non"
        // Cela couvrirait 95% des cas.
        persistedSeedLotDomainInputs = persistedSeedLotDomainInputs.stream().filter(psli -> psli instanceof DomainSeedLotInput)
                .filter(
                        dsli -> {
                            DomainSeedLotInput domainSeedLotInput = (DomainSeedLotInput) dsli;
                            boolean res = domainSeedLotInput.getCropSeed().equals(croppingPlanEntry);
                            res &= seedPlantUnit == domainSeedLotInput.getUsageUnit();
                            res &= !domainSeedLotInput.isOrganic();
                            res &= (CollectionUtils.emptyIfNull(interventionSpecies).containsAll(
                                    CollectionUtils.emptyIfNull(domainSeedLotInput.getDomainSeedSpeciesInput())
                                            .stream()
                                            .map(DomainSeedSpeciesInput::getSpeciesSeed).toList()));

                            if (chemicalOrBiologicalTraitment == null || chemicalOrBiologicalTraitment.biologicalSeedInoculation() || chemicalOrBiologicalTraitment.chemicalTreatment()) {
                                res &= !CollectionUtils.emptyIfNull(domainSeedLotInput.getDomainSeedSpeciesInput())
                                        .stream()
                                        .filter(domainSeedSpeciesInput ->
                                                domainSeedSpeciesInput.isBiologicalSeedInoculation() || domainSeedSpeciesInput.isChemicalTreatment()
                                        ).toList().isEmpty();
                            }
                            return res;
                        })
                .toList();

        if (chemicalOrBiologicalTraitment != null) {
            if (chemicalOrBiologicalTraitment.biologicalSeedInoculation()) {
                persistedSeedLotDomainInputs = persistedSeedLotDomainInputs.stream()
                        .filter(domainSeedLotInput ->
                                CollectionUtils.emptyIfNull(((DomainSeedLotInput) domainSeedLotInput).getDomainSeedSpeciesInput())
                                        .stream()
                                        .anyMatch(DomainSeedSpeciesInput::isBiologicalSeedInoculation))
                        .toList();
            }

            if (chemicalOrBiologicalTraitment.chemicalTreatment()) {
                persistedSeedLotDomainInputs = persistedSeedLotDomainInputs.stream()
                        .filter(domainSeedLotInput ->
                                CollectionUtils.emptyIfNull(((DomainSeedLotInput) domainSeedLotInput).getDomainSeedSpeciesInput())
                                        .stream()
                                        .anyMatch(DomainSeedSpeciesInput::isChemicalTreatment))
                        .toList();
            }
        }

        Optional<AbstractDomainInputStockUnit> optionalPersistedSeedLot = persistedSeedLotDomainInputs.stream()
                .filter(dsl -> ((DomainSeedLotInput) dsl).getSeedPrice() != null &&
                        ((DomainSeedLotInput) dsl).getSeedPrice().getPrice() != null)
                .findAny();

        if (optionalPersistedSeedLot.isEmpty()) {
            optionalPersistedSeedLot = persistedSeedLotDomainInputs.stream().findAny();
        }

        DomainSeedLotInput persitedDomainSeedLotInput = null;
        if (optionalPersistedSeedLot.isPresent()) {
            persitedDomainSeedLotInput = (DomainSeedLotInput) optionalPersistedSeedLot.get();
        }
        return Optional.ofNullable(persitedDomainSeedLotInput);
    }

    protected ChemicalORBiologicalTraitment getChemicalOBiologicalTreatment(RefActaTraitementsProduit refInput) {
        final boolean biologicalSeedInoculation, chemicalTreatment;

        RefActaTraitementsProduitsCateg refActaTraitementsProduitsCateg = refActaTraitementsProduitsCategDao.forId_traitementEquals(refInput.getId_traitement()).findUnique();
        if (refActaTraitementsProduitsCateg.getType_produit().equals(ProductType.FUNGICIDAL)
                || refActaTraitementsProduitsCateg.getType_produit().equals(ProductType.HERBICIDAL)
                || refActaTraitementsProduitsCateg.getType_produit().equals(ProductType.INSECTICIDAL)
                || refActaTraitementsProduitsCateg.getType_produit().equals(ProductType.ASSOCIATIONS)) {
            chemicalTreatment = true;
            biologicalSeedInoculation = false;
        } else {
            chemicalTreatment = false;
            biologicalSeedInoculation = refActaTraitementsProduitsCateg.getType_produit().equals(ProductType.SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION);
        }
        ChemicalORBiologicalTraitment chemicalORBiologicalTraitment = new ChemicalORBiologicalTraitment(biologicalSeedInoculation, chemicalTreatment);
        return chemicalORBiologicalTraitment;
    }

    protected record ChemicalORBiologicalTraitment(boolean biologicalSeedInoculation, boolean chemicalTreatment) {
    }


    protected DomainSeedLotInput persistDomainSeedLot(
            DomainSeedLotInput domainSeedLotInput) {

        DomainSeedLotInput seedLotInput;
        if (domainSeedLotInput.isPersisted()) {
            seedLotInput = domainSeedLotInputDao.update(domainSeedLotInput);
        } else {
            seedLotInput = domainSeedLotInputDao.create(domainSeedLotInput);
        }

        return seedLotInput;
    }

    protected void addSeedingActionSpeciesInputs(
            EdaplosContext context,
            SeedLotInputUsage seedLotInputUsage,
            double quantity,
            UnitCode unitCode,
            AgriculturalProcessCropInput seedsOrPlan) {

        // si des variété sont présente on utilise tous les intrants  #11382
        // sinon on prend la première #10495
        AgriculturalProcessCropInput input = seedsOrPlan;
        Optional<String> inputId = Optional.ofNullable(input.getIdentification())
                .map(identification -> identification.toLowerCase(Locale.ROOT).strip());

        Optional<SeedSpeciesInputUsage> optionalSpeciesInputUsage = inputId.map(inputGnisCode -> context.getCodeGnisToSpecies().get(inputGnisCode))
                .flatMap(cps -> CollectionUtils.emptyIfNull(seedLotInputUsage.getSeedingSpecies()).stream()
                        .filter(ssu -> ssu.getDomainSeedSpeciesInput().getSpeciesSeed().equals(cps))
                        .max(Comparator.comparing(ssu -> SEEDING_ACTION_SPECIES_DEFAULT_QUANTITY == ssu.getQtAvg())));

        if (optionalSpeciesInputUsage.isEmpty()) {
            optionalSpeciesInputUsage = inputId.flatMap(speciesBotanicalCode ->
                    CollectionUtils.emptyIfNull(seedLotInputUsage.getSeedingSpecies()).stream()
                            .filter(ssu -> speciesBotanicalCode.equals(context.getSpeciesCodeToSpeciesBotanicalCode().get(ssu.getDomainSeedSpeciesInput().getSpeciesSeed().getCode()))
                                    && SEEDING_ACTION_SPECIES_DEFAULT_QUANTITY == ssu.getQtAvg())
                            .findFirst());

        }

        if (optionalSpeciesInputUsage.isEmpty()) {
            optionalSpeciesInputUsage = CollectionUtils.emptyIfNull(seedLotInputUsage.getSeedingSpecies()).stream()
                    .filter(sas -> SEEDING_ACTION_SPECIES_DEFAULT_QUANTITY == sas.getQtAvg())
                    .findFirst();
        }

        optionalSpeciesInputUsage.ifPresent(sas -> {
            if (SEEDING_ACTION_SPECIES_DEFAULT_QUANTITY == sas.getQtAvg()) {
                setSeedingActionSpeciesUnit(context, seedLotInputUsage.getDomainSeedLotInput(), unitCode);
                setSeedingActionSpeciesQuantity(sas, quantity, unitCode);
            }
        });


    }

    protected void setSeedingActionSpeciesUnit(
            EdaplosContext context,
            DomainSeedLotInput domainSeedLotInput,
            UnitCode unitCode) {

        if (UnitCode.NAR == unitCode) {
            domainSeedLotInput.setUsageUnit(SeedPlantUnit.UNITE_PAR_HA);
        } else if (UnitCode.TNE == unitCode) {
            domainSeedLotInput.setUsageUnit(SeedPlantUnit.KG_PAR_HA);
        } else if (UnitCode.KGM == unitCode) {
            domainSeedLotInput.setUsageUnit(SeedPlantUnit.KG_PAR_HA);
        } else if (UnitCode.GRA == unitCode) {
            domainSeedLotInput.setUsageUnit(SeedPlantUnit.GRAINES_PAR_HA);
        } else if (UnitCode.PLT == unitCode) {
            domainSeedLotInput.setUsageUnit(SeedPlantUnit.PLANTS_PAR_HA);
        } else {
            context.isIncorrectSeedingUnit = true;
            context.addIncorrectUnitCode(unitCode);
        }
    }

    protected SeedPlantUnit getSeedPlantUnitFromUnitCode(
            UnitCode unitCode) {
        SeedPlantUnit result = null;
        if (UnitCode.NAR == unitCode) {
            result = SeedPlantUnit.UNITE_PAR_HA;
        } else if (UnitCode.TNE == unitCode) {
            result = SeedPlantUnit.KG_PAR_HA;
        } else if (UnitCode.KGM == unitCode) {
            result = SeedPlantUnit.KG_PAR_HA;
        } else if (UnitCode.GRA == unitCode) {
            result = SeedPlantUnit.GRAINES_PAR_HA;
        } else if (UnitCode.PLT == unitCode) {
            result = SeedPlantUnit.PLANTS_PAR_HA;
        }
        return result;
    }


    protected void setSeedingActionSpeciesQuantity(
            SeedSpeciesInputUsage sas,
            double quantity,
            UnitCode unitCode) {

        if (UnitCode.TNE == unitCode) {
            sas.setQtAvg(quantity * 1000);
        } else {
            sas.setQtAvg(quantity);
        }
    }

    protected SeedingActionUsage createNewSeedingActionUsageWithDefaultValues(
            EdaplosContext interventionContext,
            String travailEdiRefCode,
            RefInterventionAgrosystTravailEDI travailEdi,
            String description) {

        EffectiveIntervention intervention = interventionContext.getEffectiveIntervention();
        SeedingActionUsage seedingActionUsage = new SeedingActionUsageImpl();

        travailEdi = travailEdi == null ? refInterventionAgrosystTravailEDIDao.forReference_codeEquals(travailEdiRefCode).findUniqueOrNull() : travailEdi;
        addCommonActionFields(seedingActionUsage, travailEdi, intervention, description);

        Set<String> subordinateTypeCodes = interventionContext.getPlotAgriculturalProcess().getUsedAgriculturalProcessCropInputs().stream()
                .filter(c -> AgriculturalProcessCropInput.PLANTS.equals(c.getTypeCode()) || AgriculturalProcessCropInput.SEMENCE.equals(c.getTypeCode()))
                .map(AgriculturalProcessCropInput::getSubordinateTypeCode)
                .collect(Collectors.toSet());

        SeedType seedType = AgriculturalProcessCropInput.FROM_CODE_SEMENCE_TO_SEED_TYPE.apply(subordinateTypeCodes);
        seedingActionUsage.setSeedType(seedType);

        return seedingActionUsage;
    }

    //Phyto
    protected RefActaTraitementsProduit retrievePhytoProductTreatment(
            EdaplosContext context,
            String ammCode,
            String productName,
            String typeCode,
            boolean isSeedingInput) {

        if (StringUtils.isBlank(ammCode)) {
            if (isSeedingInput) {
                context.formatInfo("L'intrant déclaré sur la culture '%s', intervention '%s', parcelle '%s', domaine '%s' n'a pas de code d'identification (AMM) renseigné.",
                        formatCrop(context), formatIntervention(context), formatPlot(context), formatDomain(context));
            } else {
                context.formatWarning("L'intrant déclaré sur la culture '%s', intervention '%s', parcelle '%s', domaine '%s' n'a pas de code d'identification (AMM) renseigné. Vous devrez ajouter manuellement ce produit dans Agrosyst après l'import, ainsi que les prix éventuels s'y rattachant.",
                        formatCrop(context), formatIntervention(context), formatPlot(context), formatDomain(context));
            }
            return null;
        }

        // A partir de l’AMM donné par eDaplos, on va retrouver dans refActaProduitsRoot, 0 à n id_produit.
        // si l'AMM n'est pas trouvé, on tente avec le numéro de produit
        List<RefActaProduitRoot> refActaProduitRoots = refActaProduitRootDao.forAmmEquals(ammCode)
                .addEquals(RefActaProduitRoot.PROPERTY_ACTIVE, true) // cf #10409 : search only on active
                .findAll();
        if (CollectionUtils.isEmpty(refActaProduitRoots)) {
            refActaProduitRoots = refActaProduitRootDao.forPermisEquals(ammCode)
                    .addEquals(RefActaProduitRoot.PROPERTY_ACTIVE, true)
                    .findAll();
        }
        List<String> idProduits = refActaProduitRoots.stream()
                .filter(refActaProduitRoot -> isSameDespiteWhitespacesOrAccents(refActaProduitRoot.getNomProduit(), productName))
                .map(RefActaProduitRoot::getIdProduit)
                .map(String::valueOf)
                .collect(Collectors.toList());

        boolean foundWithGivenName = !idProduits.isEmpty();
        // #10402 : si le nom du traitement remonté du référentiel n'est pas le même que celui saisie dans eDaplos, on prendra le premier produit qui correspond au code AMM
        if (!foundWithGivenName) {
            idProduits = refActaProduitRoots.stream()
                    .map(RefActaProduitRoot::getIdProduit)
                    .map(String::valueOf)
                    .collect(Collectors.toList());
        }

        // À partir du type d’intrant AgroEDI on va retrouver 0 à n traitements Agrosyst (voir correspondance dans refEdaplosTypeTraitement).
        List<RefEdaplosTypeTraitement> edaplosTypeTraitements = refEdaplosTypeTraitementDao.forReferenceCodeEquals(typeCode)
                .addEquals(RefEdaplosTypeTraitement.PROPERTY_ACTIVE, true) // cf #10409 : search only on active
                .findAll();
        List<String> codeTraitements = edaplosTypeTraitements.stream()
                .map(RefEdaplosTypeTraitement::getCodeTraitement).toList();

        // Avec les O/n traitements Agrosyst et O/n id_produit on va trouver 0 à n lignes correspondantes dans
        // refactatraitementsproduits. L’info que l’on cherche à stoker est un refactatraitementsproduits.topiaid dans abstractinput.phytoproduct.
        List<RefActaTraitementsProduit> refActaTraitementsProduits = refActaTraitementsProduitsDao.forId_produitIn(idProduits)
                .addEquals(RefActaTraitementsProduit.PROPERTY_ACTIVE, true) // cf #10409 : search only on active
                .findAll();

        RefActaTraitementsProduit refActaTraitementsProduit = null;

        boolean foundWithCodeTreatment = false;
        if (!refActaTraitementsProduits.isEmpty()) {
            Optional<RefActaTraitementsProduit> traitementsProduit = refActaTraitementsProduits.stream().filter(t -> codeTraitements.contains(t.getCode_traitement())).findFirst();
            if (traitementsProduit.isPresent()) {
                refActaTraitementsProduit = traitementsProduit.get();
                foundWithCodeTreatment = true;
            } else {
                refActaTraitementsProduit = refActaTraitementsProduits.getFirst();
            }
        }

        if (refActaTraitementsProduit == null) {

            context.formatWarning("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', pour un intrant '%s', " +
                            "impossible de trouver le produit correspondant au code AMM '%s' ! Vous devrez rajouter manuellement ce produit dans Agrosyst après l'import, ainsi que les prix éventuels s'y rattachant.",
                    formatPlot(context), formatSolOccupation(context), formatIntervention(context), typeCode, ammCode);

        } else if (!foundWithGivenName) {
            // #10402 : si le nom du traitement remonté du référentiel n'est pas le même que celui saisie dans eDaplos, il faut remonté l'information à l'utilisateur en Warning, en précisant le produit utilisé
            context.formatWarning("L'intrant déclaré sur la culture '%s', intervention '%s', parcelle '%s', domaine '%s' "
                            + "n'a pas pu être retrouvé avec le nom fourni '%s'. Utilisation du produit '%s' - traitement '%s'.",
                    formatCrop(context), formatIntervention(context), formatPlot(context),
                    formatDomain(context), productName, refActaTraitementsProduit.getNom_produit(),
                    refActaTraitementsProduit.getNom_traitement());

        } else if (!foundWithCodeTreatment) {
            context.formatInfo("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', pour l'intrant dont le type eDaplos est '%s', " +
                            "import du produit '%s' avec le traitement '%s' issu d'agrosyst au lieu de celui fourni.",
                    formatPlot(context), formatSolOccupation(context), formatIntervention(context), typeCode, refActaTraitementsProduit.getNom_produit(),
                    refActaTraitementsProduit.getNom_traitement());
        }

        return refActaTraitementsProduit;
    }

    protected PesticideProductInputUsage newPesticideProductInputUsage(
            EdaplosContext interventionContext,
            PhytoProductInputContext phytoProductInputContext,
            DomainPhytoProductInput domainInput,
            double proportionOfTreatedSurface) {

        PesticideProductInputUsage usage = new PesticideProductInputUsageImpl();
        addPhytoProductInputUsageParameters(
                interventionContext,
                phytoProductInputContext,
                domainInput,
                proportionOfTreatedSurface,
                InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                usage);
        return usage;
    }

    protected SeedProductInputUsage newSeedProductInputUsage(
            EdaplosContext context,
            PhytoProductInputContext phytoProductInputContext,
            DomainPhytoProductInput domainInput,
            Double proportionOfTreatedSurface) {

        SeedProductInputUsage usage = new SeedProductInputUsageImpl();
        addPhytoProductInputUsageParameters(
                context,
                phytoProductInputContext,
                domainInput,
                proportionOfTreatedSurface,
                InputType.TRAITEMENT_SEMENCE,
                usage);
        usage.setQtAvg(phytoProductInputContext.avtQuantity);

        return usage;
    }

    protected BiologicalProductInputUsage newBiologicalProductInputUsage(
            EdaplosContext context,
            PhytoProductInputContext phytoProductInputContext,
            DomainPhytoProductInput phytoProductInput,
            Double proportionOfTreatedSurface) {

        BiologicalProductInputUsage usage = new BiologicalProductInputUsageImpl();
        addPhytoProductInputUsageParameters(
                context,
                phytoProductInputContext,
                phytoProductInput,
                proportionOfTreatedSurface,
                InputType.LUTTE_BIOLOGIQUE,
                usage);
        return usage;
    }

    protected void addPhytoProductInputUsageParameters(
            EdaplosContext context,
            PhytoProductInputContext phytoProductInputContext,
            DomainPhytoProductInput phytoProductInput,
            Double proportionOfTreatedSurface,
            InputType inputType,
            AbstractPhytoProductInputUsage usage) {

        AgriculturalProcessCropInput cropInput = phytoProductInputContext.productInput;
        ReferenceDoseDTO referenceDose = phytoProductInputContext.referenceDose;

        usage.setInputType(inputType);
        usage.setDomainPhytoProductInput(phytoProductInput);

        Plot plot = context.getPlot();
        EffectiveIntervention intervention = context.getEffectiveIntervention();

        // Quantité par unité de surface
        String appliedSurfaceUnitValueMeasure = cropInput.getAppliedSurfaceUnitValueMeasure();
        String valueMeasure = cropInput.getValueMeasure();

        Double avtQuantity = null;

        if (NumberUtils.isCreatable(appliedSurfaceUnitValueMeasure)) {
            avtQuantity = Double.parseDouble(appliedSurfaceUnitValueMeasure);
        } else if (NumberUtils.isCreatable(valueMeasure) && proportionOfTreatedSurface != null) {
            double divider = intervention.getSpatialFrequency() * plot.getArea() * proportionOfTreatedSurface / 100;
            avtQuantity = divider != 0 ? Double.parseDouble(valueMeasure) / divider : 0;
            avtQuantity = Precision.round(avtQuantity, 4, RoundingMode.HALF_UP.ordinal());
        }

        if (referenceDose != null && avtQuantity != null) {
            // Si une dose est trouvée et que son unité correspond avec celle importée depuis le fichier edaplos alors elle est retenue
            // ou
            // Si une dose est trouvée et que son unité correspond avec celle importée depuis le fichier edaplos après conversion selon les règles du fichier xls
            // Intrants_correspondances_EDI_Agrosyst_ebauche1.xlsx elors elle est retenue et l'import se fait en convertissant la dose et l'unité du fichier eDaplos
            UnitCode unitCode = NumberUtils.isCreatable(appliedSurfaceUnitValueMeasure) ? cropInput.getAppliedSurfaceUnitValueMeasureUnitCode() : cropInput.getValueMeasureUnitCode();
            Double refAvtQuantity = convertEdiToAgrosystPhytoUnit(unitCode, referenceDose.getUnit(), avtQuantity);
            if (refAvtQuantity != null) {
                avtQuantity = refAvtQuantity;
            }
        }

        if (avtQuantity == null) {
            context.formatError("La quantité épandue (réelle ou par unité de surface) doit être renseignée pour l'intrant de l'intervention '%s' portant sur occupation du sol '%s', parcelle '%s', domaine '%s'.",
                    formatIntervention(context), formatSolOccupation(context), formatPlot(plot), formatDomain(context));
        }

        usage.setQtAvg(avtQuantity);

        // cibles
        addPhytoProductInputTargets(
                context,
                usage,
                phytoProductInputContext);

    }

    /*
     * Unité quantité par unité de surface EDI	Unité Agrosyst	Conversion nécessaire (o/n)	Facteur de conversion
     * l/ha	                                    L/ha	        n
     * l/ha	                                    ml/ha	        o	1000
     * l/ha	                                    ml/m2	        o	0,1
     * l/ha	                                    L/m²	        o	0,0001
     * kg/ha	                                t/ha	        o	0,001
     * kg/ha	                                kg/ha	        n
     * kg/ha	                                g/ha	        o	1000
     * kg/ha	                                g/m2	        o	0,1
     * kg/ha	                                kg/m2	        o	0,0001
     * kg/ha	                                kg/ha	        n
     * q/ha	                                    kg/ha	        o	100
     * t/ha	                                    kg/ha	        o	1000
     * kg/ha	                                q/ha	        o	0,01
     * q/ha	                                    q/ha	        n
     * t/ha	                                    q/ha	        o	10
     * kg/ha	                                t/ha	        o	0,001
     * q/ha	                                    t/ha	        o	0,1
     * t/ha	                                    t/ha	        n
     */

    /**
     * Applied conversion from EDI unit to Agrosyst phytoProductUnit
     * Deprecated: change to use referential Intrants_correspondances_EDI_Agrosyst_ebauche1 file
     * also the conversion can applied to other input and harvesting action
     *
     * @return converted value
     */
    protected Double convertEdiToAgrosystPhytoUnit(UnitCode ediUnit, PhytoProductUnit agrosytUnit, double avtQuantity) {
        Double result = null;

        if (ediUnit != null && agrosytUnit != null) {

            switch (ediUnit) {
                case LTR -> { // l/ha
                    switch (agrosytUnit) {
                        case L_HA -> result = avtQuantity;
                        case ML_HA -> result = avtQuantity * 1000;
                        case ML_M2 -> result = avtQuantity * 0.1;
                        case L_M2 -> result = avtQuantity * 0.0001;
                    }
                }
                case KGM -> // kg/ha
                    //case q/ha ?
                        result = switch (agrosytUnit) {
                            case T_HA -> avtQuantity * 0.001;
                            case KG_HA -> avtQuantity;
                            case G_HA -> avtQuantity * 1000;
                            case G_M2 -> avtQuantity * 0.1;
                            case KG_M2 -> avtQuantity * 0.0001;
                            default -> result;
                        };

            /*case XXX: // q/ha
                switch (agrosytUnit) {
                    //case q/ha ?
                    case KG_HA: result = avtQuantity * 100; break;
                    case T_HA: result = avtQuantity * 0,1; break;
                }
                break;*/
                case TNE -> // t/ha
                        result = switch (agrosytUnit) {
                            //case q/ha ?
                            case KG_HA -> avtQuantity * 1000;
                            case T_HA -> avtQuantity;
                            default -> result;
                        };
            }
        }

        return result;
    }

    /**
     * Deal AgriculturalPlot#ApplicableTechnicalCharacteristic attribute : as
     * this could be several types of information, we regroup all of them into
     * an unique object, but we need to identify thanks to "TypeCode" element
     * what we have.
     * For the moment, here are the known characteristics :
     * <ul>
     * <li>ABG02 : Ground characteritic to détermine real "calcaire" typeCode (obtained in "SubordinateTypeCode")</li>
     * <li>ABG03 : Ground characteritic to détermine real "textures" typeCode (obtained in "SubordinateTypeCode")</li>
     * <li>ABG04 : Ground characteritic to détermine real "pierrosité" typeCode (obtained in "SubordinateTypeCode")</li>
     * <li>ZD2 : Ground characteritic to détermine real "profondeur" typeCode (obtained in "SubordinateTypeCode")</li>
     * <li>ZD3 : Ground characteritic to détermine real "hydromorphie" typeCode (obtained in "SubordinateTypeCode")</li>
     * </ul>
     */
    protected void processPlotTechnicalCharacteristic(EdaplosContext context, Domain domain, Plot plot,
                                                      List<TechnicalCharacteristic> technicalCharacteristics) {
        String limestoneTypeCode = null;
        String structureTypeCode = null;
        String stoninessTypeCode = null;
        String depthTypeCode = null;
        String waterLoggingTypeCode = null;
        String idTypeSolArvalis = null;
        StringBuilder comment = new StringBuilder();

        for (TechnicalCharacteristic technicalCharacteristic : technicalCharacteristics) {
            String typeCode = technicalCharacteristic.getTypeCode();
            String subordinateTypeCode = technicalCharacteristic.getSubordinateTypeCode();
            String valueMeasure = technicalCharacteristic.getValueMeasure();
            String description = technicalCharacteristic.getDescription();

            if (StringUtils.isNotBlank(description)) {
                comment.append(description);
            }

            if (typeCode == null) {
                continue;
            }

            switch (typeCode) {
                case TechnicalCharacteristic.TYPE_CODE_LIMESTONE -> {
                    if (StringUtils.isNotBlank(valueMeasure)) {
                        idTypeSolArvalis = valueMeasure;
                    } else {
                        if (StringUtils.isNotBlank(limestoneTypeCode)) {
                            context.formatWarning("Plusieurs valeurs de teneur en calcaire ont été renseignées pour la parcelle '%s', domaine '%s'. Seule la première valeur sera utilisée.",
                                    formatPlot(plot), formatDomain(domain));
                        } else {
                            limestoneTypeCode = subordinateTypeCode;
                        }
                    }
                }
                case TechnicalCharacteristic.TYPE_CODE_STRUCTURE -> {
                    if (StringUtils.isNotBlank(valueMeasure)) {
                        idTypeSolArvalis = valueMeasure;
                    } else {
                        if (StringUtils.isNotBlank(structureTypeCode)) {
                            context.formatWarning("Plusieurs valeurs de structure du sol ont été renseignées pour la parcelle '%s', domaine '%s'. Seule la première valeur sera utilisée.",
                                    formatPlot(plot), formatDomain(domain));
                        } else {
                            structureTypeCode = subordinateTypeCode;
                        }
                    }
                }
                case TechnicalCharacteristic.TYPE_CODE_STONINESS -> {
                    if (StringUtils.isNotBlank(valueMeasure)) {
                        idTypeSolArvalis = valueMeasure;
                    } else {
                        if (StringUtils.isNotBlank(stoninessTypeCode)) {
                            context.formatWarning("Plusieurs valeurs de pierrosité ont été renseignées pour la parcelle '%s', domaine '%s'. Seule la première valeur sera utilisée.",
                                    formatPlot(plot), formatDomain(domain));
                        } else {
                            stoninessTypeCode = subordinateTypeCode;
                        }
                    }
                }
                case TechnicalCharacteristic.TYPE_CODE_DEPTH -> {
                    if (StringUtils.isNotBlank(valueMeasure)) {
                        idTypeSolArvalis = valueMeasure;
                    } else {
                        if (StringUtils.isNotBlank(depthTypeCode)) {
                            context.formatWarning("Plusieurs valeurs de profondeur ont été renseignées pour la parcelle '%s', domaine '%s'. Seule la première valeur sera utilisée.",
                                    formatPlot(plot), formatDomain(domain));
                        } else {
                            depthTypeCode = subordinateTypeCode;
                        }
                    }
                }
                case TechnicalCharacteristic.TYPE_CODE_WATER_LOGGING -> {
                    if (StringUtils.isNotBlank(valueMeasure)) {
                        idTypeSolArvalis = valueMeasure;
                    } else {
                        if (StringUtils.isNotBlank(waterLoggingTypeCode)) {
                            context.formatWarning("Plusieurs valeurs d'humidité du sol ont été renseignées pour la parcelle '%s', domaine '%s'. Seule la première valeur sera utilisée.",
                                    formatPlot(plot), formatDomain(domain));
                        } else {
                            waterLoggingTypeCode = subordinateTypeCode;
                        }
                    }
                }
                case TechnicalCharacteristic.TYPE_CODE_TYPE_SOL_ARVALIS -> {
                    // find refSolArvalis from subordinateTypeCode
                    if (StringUtils.isNotBlank(idTypeSolArvalis)) {
                        context.formatWarning("Plusieurs id de type sol Arvalis ont été renseignées pour la parcelle '%s', domaine '%s'. Seule la première valeur sera utilisée.",
                                formatPlot(plot), formatDomain(domain));
                    } else {
                        idTypeSolArvalis = subordinateTypeCode;
                    }
                }
            }

        }

        // Traitement du sol
        if (StringUtils.isAnyBlank(limestoneTypeCode, structureTypeCode, stoninessTypeCode, depthTypeCode, waterLoggingTypeCode) && StringUtils.isBlank(idTypeSolArvalis)) {
            context.formatWarning("La description du type de sol de la parcelle '%s' du domaine '%s' est incomplète. Le type de sol ne sera donc pas importé.", formatPlot(plot), formatDomain(domain));
        } else {

            RefSolArvalis refSolArvalis;

            if (StringUtils.isNotBlank(idTypeSolArvalis)) {
                refSolArvalis = refSolArvalisDao.forId_type_solEquals(idTypeSolArvalis).findAnyOrNull();

            } else {
                refSolArvalis = refSolArvalisDao.forProperties(
                        RefSolArvalis.PROPERTY_SOL_CALCAIRE_TYPE_CODE, limestoneTypeCode,
                        RefSolArvalis.PROPERTY_SOL_TEXTURE_TYPE_CODE, structureTypeCode,
                        RefSolArvalis.PROPERTY_SOL_PIERROSITE_TYPE_CODE, stoninessTypeCode,
                        RefSolArvalis.PROPERTY_SOL_PROFONDEUR_TYPE_CODE, depthTypeCode,
                        RefSolArvalis.PROPERTY_SOL_HYDROMORPHIE_TYPE_CODE, waterLoggingTypeCode,
                        RefSolArvalis.PROPERTY_SOL_REGION_CODE, domain.getLocation().getRegion()
                ).findAnyOrNull();
            }

            // refSolArvalis is required for ground
            if (refSolArvalis != null) {
                // check ground already existing on domain
                Ground ground = groundDao.forDomainEquals(domain).addEquals(Ground.PROPERTY_REF_SOL_ARVALIS, refSolArvalis).findAnyOrNull();

                //it's a new one !
                if (ground == null) {
                    ground = groundDao.newInstance();
                    ground.setDomain(domain);
                    ground.setName(refSolArvalis.getSol_nom());
                    ground.setComment(comment.toString());
                    ground.setRefSolArvalis(refSolArvalis);
                    ground.setValidated(true);
                    ground = groundDao.create(ground);
                }

                plot.setGround(ground);
            } else {
                context.formatWarning("Le type de sol de la parcelle '%s' du domaine '%s' n'est pas présent dans le référentiel Agrosyst. Il ne sera pas importé. Veuillez contacter l'équipe Agrosyst.", formatPlot(plot), formatDomain(domain));
            }
        }
    }

    protected void processAgriculturalProduce(
            EdaplosContext context,
            AgriculturalProduce agriculturalProduce,
            Map<AgrosystInterventionType, AbstractAction> interventionActions) {

        // context
        Domain domain = context.getDomain();
        Plot plot = context.getPlot();
        EffectiveIntervention intervention = context.getEffectiveIntervention();
        CroppingPlanEntry croppingPlanEntry = context.getCroppingPlanEntry();
        String identification = agriculturalProduce != null ? agriculturalProduce.getTypeCode() : null;

        // action
        HarvestingAction harvestingAction = (HarvestingAction) interventionActions.get(AgrosystInterventionType.RECOLTE);
        if (harvestingAction == null) {

            harvestingAction = createNewHarvestingActionWithDefaultValues(intervention, null, null);
            interventionActions.put(AgrosystInterventionType.RECOLTE, harvestingAction);

            context.formatInfo("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', pour l'intrant '%s', aucune action correspondante n'a pu être trouvée. Une par défaut a été créée.",
                    formatPlot(context), formatSolOccupation(context), formatIntervention(context), identification);
            harvestingAction = harvestingActionDao.create(harvestingAction);
        }

        // produce
        addHarvestingActionValorisations(context, agriculturalProduce, domain, plot, intervention, croppingPlanEntry, harvestingAction);
    }

    protected void addHarvestingActionValorisations(EdaplosContext context, AgriculturalProduce agriculturalProduce, Domain domain, Plot plot, EffectiveIntervention intervention, CroppingPlanEntry croppingPlanEntry, HarvestingAction harvestingAction) {
        List<CroppingPlanSpecies> croppingPlanSpecies = ListUtils.emptyIfNull(croppingPlanEntry.getCroppingPlanSpecies());

        Double cropYealdAverage = null;
        // yield
        YealdUnit yealdUnit = null;

        boolean isActionValid = true;

        if (agriculturalProduce != null) {
            if (NumberUtils.isCreatable(agriculturalProduce.getEstimatedYieldMeasure())) {
                cropYealdAverage = Double.parseDouble(agriculturalProduce.getEstimatedYieldMeasure());


            } else if (NumberUtils.isCreatable(agriculturalProduce.getValueMeasure())) {
                double avgYield = Double.parseDouble(agriculturalProduce.getValueMeasure()) /
                        (intervention.getSpatialFrequency() * plot.getArea());
                avgYield = Precision.round(avgYield, 4, RoundingMode.HALF_UP.ordinal());
                cropYealdAverage = avgYield;
            }

            // unit
            UnitCode unitCode = ObjectUtils.firstNonNull(agriculturalProduce.getEstimatedYieldMeasureUnitCode(), agriculturalProduce.getValueMeasureUnitCode());
            if (unitCode != null) {
                if (unitCode == UnitCode.KGM) {
                    yealdUnit = YealdUnit.KG_M2;
                } else if (unitCode == UnitCode.TNE) {
                    yealdUnit = YealdUnit.TONNE_HA;
                } else {
                    isActionValid = false;
                    yealdUnit = YealdUnit.TONNE_HA;// to go throw other validation steps
                    context.formatError("L'unité de mesure %s renseignée dans l'intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' est invalide ou non prise en compte.",
                            unitCode, formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
                }
            }

        } else {
            yealdUnit = YealdUnit.TONNE_HA;// to go throw other validation steps
            cropYealdAverage = 9999d;// to go throw other validation steps
            // S'il n'y a pas de rendement de déclaré pour une action de récolte, alors on affiche le message suivant
            context.formatWarning("Aucun rendement déclaré dans l'intervention de récolte '%s', occupation du sol '%s', parcelle '%s', domaine '%s'. La destination '" + ActionService.DEFAULT_DESTINATION_NAME + "' est mise par défaut avec un rendement de 9999t/ha. À corriger.",
                    formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
        }

        if (yealdUnit == null && cropYealdAverage == null) {
            isActionValid = false;
            cropYealdAverage = 9999d;// to go throw other validation steps
            yealdUnit = YealdUnit.TONNE_HA;// to go throw other validation steps
            context.formatError("L'unité de mesure et la quantité récoltée ne sont pas renseignées dans l'intervention de récolte '%s', occupation du sol '%s', parcelle '%s', domaine '%s'.",
                    formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
        }

        if (yealdUnit == null) {
            isActionValid = false;
            yealdUnit = YealdUnit.TONNE_HA;// to go throw other validation steps
            context.formatError("L'unité de mesure n'est pas renseignée dans l'intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s'.",
                    formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
        }

        if (cropYealdAverage == null) {
            isActionValid = false;
            cropYealdAverage = 9999d;// to go throw other validation steps
            context.formatError("La quantité récoltée n'est pas renseignée dans l'intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s'.",
                    formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
        }

        double speciesYealdlAverage = croppingPlanSpecies.isEmpty() ? 0 : cropYealdAverage / croppingPlanSpecies.size();

        Collection<HarvestingActionValorisation> valorisations = CollectionUtils.emptyIfNull(harvestingAction.getValorisations());
        Set<HarvestingActionValorisationTopiaDao.UniqueKeyValorisation> uniqueKeyValorisations = new HashSet<>();
        valorisations.forEach(
            val -> {
                HarvestingActionValorisationTopiaDao.UniqueKeyValorisation key = new HarvestingActionValorisationTopiaDao.UniqueKeyValorisation(
                        harvestingAction,
                        val.getSpeciesCode(),
                        val.getDestination()
                );
                if (!uniqueKeyValorisations.contains(key)) {
                    uniqueKeyValorisations.add(key);
                } else {
                    context.formatError("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', doublon non authorisé détecté (action, espèce, destination) %s",
                            formatPlot(context), formatSolOccupation(context), formatIntervention(context), key);
                    if (LOGGER.isErrorEnabled()) {
                        LOGGER.error(String.format("Valorisation en double remontée sur l'action %s %s", harvestingAction.getTopiaId(), key));
                    }
                }
            }
        );

        for (CroppingPlanSpecies croppingPlanSpecy : croppingPlanSpecies) {

            boolean isValorisationValid = isActionValid;

            HarvestingActionValorisation valorisation = new HarvestingActionValorisationImpl();
            valorisation.setSpeciesCode(croppingPlanSpecy.getCode());

            valorisation.setYealdAverage(speciesYealdlAverage);

            RefEspece species = croppingPlanSpecy.getSpecies();

            // destination
            RefSpeciesToSector speciesToSector = refSpeciesToSectorDao.findByCodeEspeceBotaniqueCodeQualifant(
                    species.getCode_espece_botanique(), species.getCode_qualifiant_AEE());

            RefDestination destination = null;

            if (speciesToSector == null) {
                isValorisationValid = false;
                context.formatError("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', impossible de déterminer le secteur de l'espèce.",
                        formatPlot(context), formatSolOccupation(context), formatIntervention(context));

            } else {

                Map<Pair<String, String>, List<Sector>> sectorsByCodeEspeceBotaniqueCodeQualifiant = new HashMap<>();
                sectorsByCodeEspeceBotaniqueCodeQualifiant.put(Pair.of(speciesToSector.getCode_espece_botanique(), speciesToSector.getCode_qualifiant_AEE()), List.of(speciesToSector.getSector()));
                List<RefDestination> destinations = refDestinationDao.getDestinations(
                        Language.FRENCH,
                        sectorsByCodeEspeceBotaniqueCodeQualifiant,
                        harvestingAction.getWineValorisations()
                );
                YealdUnit finalYealdUnit = yealdUnit;
                List<RefDestination> refDestinations = destinations.stream().filter(d -> ActionService.DEFAULT_DESTINATION_NAME.contentEquals(d.getDestination()))
                        .filter(d -> d.getYealdUnit() == finalYealdUnit).toList();

                if (CollectionUtils.isEmpty(refDestinations)) {
                    isValorisationValid = false;
                    context.formatError("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', impossible de déterminer la destination du secteur '%s'",
                            formatPlot(context), formatSolOccupation(context), formatIntervention(context), speciesToSector.getSector());
                } else {
                    destination = refDestinations.getFirst();
                }
            }

            valorisation.setYealdUnit(yealdUnit);
            valorisation.setDestination(destination);
            valorisation.setSalesPercent(100);
            valorisation.setSelfConsumedPersent(0);
            valorisation.setNoValorisationPercent(0);

            final LocalDate startInterventionDate = intervention.getStartInterventionDate();
            valorisation.setBeginMarketingPeriodCampaign(startInterventionDate.getYear());
            valorisation.setBeginMarketingPeriod(startInterventionDate.getMonthValue() - 1); // to be as Calendar JANUARY = 0
            valorisation.setBeginMarketingPeriodDecade(ActionServiceImpl.getEffectiveDecade(startInterventionDate));

            final LocalDate endInterventionDate = intervention.getEndInterventionDate();
            valorisation.setEndingMarketingPeriodCampaign(endInterventionDate.getYear());
            valorisation.setEndingMarketingPeriod(endInterventionDate.getMonthValue() - 1); // to be as Calendar JANUARY = 0
            valorisation.setEndingMarketingPeriodDecade(ActionServiceImpl.getEffectiveDecade(endInterventionDate));

            // s'il y a un rendement de déclarer initialement, on le transfert bien et on affiche le message suivant
            if (cropYealdAverage != 9999) {
                context.setMissingDestinationWarning();
            }

            // caracteristiques techniques
            if (agriculturalProduce != null) {
                List<TechnicalCharacteristic> applicableTechnicalCharacteristics = agriculturalProduce.getApplicableTechnicalCharacteristics();
                applicableTechnicalCharacteristics.stream()
                        // "AAA13" -> import des récoltes -> Caractéristiques techniques du produit récolté
                        .filter(tc -> TechnicalCharacteristic.TYPE_CODE_PRODUIT_RECOLTE.equals(tc.getTypeCode()))
                        .forEach(tc -> {
                            // ZJ9 -> import des récoltes -> critère de qualité taux de Matières Sèches Spec Spec_Récolte_EDI_to_Agrosyst_v1-4
                            if (TechnicalCharacteristic.SUB_TYPE_CODE_TAUX_MS.equals(tc.getSubordinateTypeCode())) {
                                if (speciesToSector != null && Sector.GRANDES_CULTURES == speciesToSector.getSector()) {
                                    if (NumberUtils.isCreatable(tc.getValueMeasure())) {

                                        double tauxHumidite = 100 - Double.parseDouble(tc.getValueMeasure());
                                        QualityCriteria qualityCriteria = new QualityCriteriaImpl();
                                        qualityCriteria.setBinaryValue(false);
                                        qualityCriteria.setQuantitativeValue(tauxHumidite);

                                        // referentiel
                                        RefQualityCriteria humidityCrit = refQualityCriteriaDao.forCodeEquals(TechnicalCharacteristic.AGROSYST_QUALITY_CRITERIA_HUMIDITY).findUnique();
                                        qualityCriteria.setRefQualityCriteria(humidityCrit);

                                        valorisation.addQualityCriteria(qualityCriteria);
                                    } else {
                                        context.formatInfo("Le taux de matière sèche renseigné pour l'intervention '%s', occupation du sol '%s', parcelle '%s', domaine '%s' est incorrect.",
                                                formatIntervention(context), formatSolOccupation(context), formatPlot(context), formatDomain(context));
                                    }
                                }
                            } else {
                                context.formatInfo("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', le SubordinateTypeCode '%s' n'est pas géré par Agrosyst. Contacter l'équipe Agrosyst pour plus d'informations.",
                                        formatPlot(context), formatSolOccupation(context), formatIntervention(context), tc.getSubordinateTypeCode());
                            }
                        });
            }

            HarvestingActionValorisationTopiaDao.UniqueKeyValorisation valKey = new HarvestingActionValorisationTopiaDao.UniqueKeyValorisation(
                    harvestingAction,
                    valorisation.getSpeciesCode(),
                    destination
            );

            if (uniqueKeyValorisations.contains(valKey)) {
                context.formatError("Sur la parcelle '%s', occupation du sol '%s', intervention '%s', doublon non authorisé détecté (action, espèce, destination) %s",
                        formatPlot(context), formatSolOccupation(context), formatIntervention(context), valKey);
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error(String.format("Valorisation en double remontée sur l'action %s %s", harvestingAction.getTopiaId(), valKey));
                }
                isValorisationValid = false;
            } else {
                uniqueKeyValorisations.add(valKey);
            }

            if (isValorisationValid) {

                // create before price
                HarvestingActionValorisation persistedValorisation = harvestingActionValorisationDao.create(valorisation);

                // prix
                createHarvestingPrice(context, agriculturalProduce, domain, croppingPlanSpecy, persistedValorisation);

                harvestingAction.addValorisations(persistedValorisation);

            }

        }
    }

    protected void createHarvestingPrice(
            EdaplosContext context,
            AgriculturalProduce agriculturalProduce,
            Domain domain,
            CroppingPlanSpecies croppingPlanSpecy,
            HarvestingActionValorisation persistedValorisation) {

        if (agriculturalProduce != null && persistedValorisation != null) {
            String displayName = PricesService.GET_HARVESTING_PRICE_DISPLAY_NAME.apply(croppingPlanSpecy);
            final String objectId = PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(croppingPlanSpecy, persistedValorisation.getDestination());

            HarvestingPrice price = new HarvestingPriceImpl();
            price.setObjectId(objectId);
            price.setZone(context.getZone());
            price.setDomain(domain);
            price.setHarvestingActionValorisation(persistedValorisation);
            price.setDisplayName(displayName);

            if (StringUtils.isNotBlank(agriculturalProduce.getUnitPrice())) {
                if (NumberUtils.isCreatable(agriculturalProduce.getUnitPrice())) {
                    double unitPrice = Double.parseDouble(agriculturalProduce.getUnitPrice());
                    if (unitPrice != 0.0d) {
                        price.setPrice(unitPrice);
                    }
                } else {
                    context.formatError("Le prix de la récolte %s, occupation du sol '%s', parcelle '%s', domaine '%s' n'est pas valide.",
                            agriculturalProduce.getTypeCode(), formatSolOccupation(context), formatPlot(context), formatDomain(context));
                }
            }
            if (YealdUnit.KG_M2 == persistedValorisation.getYealdUnit()) {
                price.setPriceUnit(PriceUnit.EURO_KG);
            } else {
                price.setPriceUnit(PriceUnit.EURO_T);
            }
            boolean exists = harvestingPriceDao.forObjectIdEquals(price.getObjectId())
                    .addEquals(HarvestingPrice.PROPERTY_SOURCE_UNIT, price.getSourceUnit())
                    .addEquals(HarvestingPrice.PROPERTY_DOMAIN, price.getDomain())
                    .addEquals(HarvestingPrice.PROPERTY_ZONE, price.getZone())
                    .addEquals(HarvestingPrice.PROPERTY_PRACTICED_SYSTEM, null)
                    .addEquals(HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, price.getHarvestingActionValorisation())
                    .exists();

            if (!exists) {
                harvestingPriceDao.create(price);
            }
        }
    }

    protected InputPrice createOrUpdateInputPrice(InputPrice price) {

        if (price == null) return null;

        InputPrice persistedInputPrice;

        if (price.isPersisted()) {
            if (price instanceof SeedPrice seedPrice) {
                persistedInputPrice = seedPriceDao.update(seedPrice);
            } else {
                persistedInputPrice = inputPriceDao.create(price);
            }
        } else {
            if (price instanceof SeedPrice seedPrice) {
                persistedInputPrice = seedPriceDao.create(seedPrice);
            } else {
                persistedInputPrice = inputPriceDao.create(price);
            }
        }

        return persistedInputPrice;

    }

    protected boolean isSameDespiteWhitespacesOrAccents(String origin, String input) {
        if (StringUtils.equalsIgnoreCase(origin, input)) {
            return true;
        }

        return StringUtils.equalsIgnoreCase(
                StringUtils.deleteWhitespace(StringUtils.stripAccents(origin)),
                StringUtils.deleteWhitespace(StringUtils.stripAccents(input))
        );
    }

}
