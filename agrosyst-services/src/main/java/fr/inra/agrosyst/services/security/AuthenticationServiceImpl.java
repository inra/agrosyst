package fr.inra.agrosyst.services.security;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.entities.security.AgrosystUserTopiaDao;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.entities.security.UserRoleTopiaDao;
import fr.inra.agrosyst.api.services.security.AuthenticationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.api.services.users.ImmutableAuthenticatedUser;
import fr.inra.agrosyst.api.services.users.Users;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.text.RandomStringGenerator;
import org.nuiton.util.StringUtil;

import java.util.Optional;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Setter
public class AuthenticationServiceImpl extends AbstractAgrosystService implements AuthenticationService {

    private static final Log LOGGER = LogFactory.getLog(AuthenticationServiceImpl.class);

    protected AgrosystUserTopiaDao agrosystUserDao;
    protected UserRoleTopiaDao userRoleDao;

    /* Pour générer le UserDto#sid on utilise un nombre aléatoire composé des caractères 0-9a-f */
    private static final char[][] SID_RANGE = {{'0','9'},{'a','f'}};
    private static final RandomStringGenerator SID_GENERATOR = new RandomStringGenerator.Builder()
            .withinRange(SID_RANGE)
            .build();

    @Override
    public AuthenticatedUser login(String email, String password) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(email), "email cannot be empty");

        if (LOGGER.isInfoEnabled()) {
            String message = String.format("Check user authentication with email: '%s'", email);
            LOGGER.info(message);
        }

        AgrosystUser user = agrosystUserDao.forProperties(
                AgrosystUser.PROPERTY_EMAIL, email.toLowerCase(),
                AgrosystUser.PROPERTY_ACTIVE, true).findAnyOrNull();

        AuthenticatedUser result = null;

        boolean passwordVerified = verifyPassword(password, user);
        if (passwordVerified) {

            String sid = SID_GENERATOR.generate(8);

            if (LOGGER.isInfoEnabled()) {
                LOGGER.info(String.format("Newly session for user '%s' is created under [sid=%s].", user.getEmail(), sid));
            }

            result = toAuthenticatedUser(sid, user);
        } else {
            if (LOGGER.isWarnEnabled()) {
                String message = String.format("Could not authenticate user with email: '%s' and the given password", email);
                LOGGER.warn(message);
            }
        }

        return result;
    }

    private boolean verifyPassword(String password, AgrosystUser user) {
        if (user == null) {
            return false;
        }
        boolean result = BCrypt.verifyer()
                .verify(StringUtil.encodeSHA1(StringUtils.defaultString(password)).toCharArray(), user.getPassword())
                .verified;
        return result;
    }

    @Override
    public AuthenticatedUser reloadAuthenticatedUser() {
        AuthenticatedUser authenticatedUser = getSecurityContext().getAuthenticatedUser();
        String userId = authenticatedUser.getTopiaId();
        String sid = authenticatedUser.getSid();
        AuthenticatedUser result = getAuthenticatedUserFromUserId(userId, sid);
        return result;
    }

    @Override
    public AuthenticatedUser getAuthenticatedUserFromUserId(String userId, String sid) {
        AuthenticatedUser result = null;
        if (userId != null) {
            AgrosystUser user = agrosystUserDao.forTopiaIdEquals(userId).findUnique();
            result = toAuthenticatedUser(sid, user);
        }
        return result;
    }

    private AuthenticatedUser toAuthenticatedUser(String sid, AgrosystUser user) {
        String userId = user.getTopiaId();
        Preconditions.checkState(user.isActive(), "L'utilisateur " + userId + " n'est pas actif");
        ImmutableSet<RoleType> roleTypes = userRoleDao.findUserRoleTypes(userId);
        AuthenticatedUser result = ImmutableAuthenticatedUser.builder()
                .topiaId(userId)
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .roles(roleTypes)
                .sid(sid)
                .isAcceptedCharter(Users.charterVersionToDtoBoolean(user.getCharterVersion()))
                .itEmail(Optional.ofNullable(user.getItEmail()))
                .language(Language.fromTrigram(user.getUserLang()))
                .banner(Optional.ofNullable(user.getBanner()))
                .build();
        return result;
    }

}
