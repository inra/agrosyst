package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.lang3.BooleanUtils;

import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Temps de travail mécanisé.
 * <p>
 * Le temps de travail mécanisé possède plusieurs unités selon le matériel utilisé. Il est calculé à toutes les échelles hormis l'intrant et concerne les interventions mobilisant une combinaison d'outil.
 * <p>
 * Il est calculé suivant la formule suivante :
 * 𝑇𝑒𝑚𝑝𝑠 𝑑𝑒 𝑡𝑟𝑎𝑣𝑎𝑖𝑙 𝑚écanisé 𝑖 = Temps d'utilisation du matériel × 𝑁𝑜𝑚𝑏𝑟𝑒 𝑑𝑒 𝑝𝑒𝑟𝑠𝑜𝑛𝑛𝑒𝑠 𝑖𝑛𝑡𝑒𝑟𝑣𝑒𝑛𝑎𝑛𝑡 𝑖
 * <p>
 * Avec :
 * 𝑇𝑒𝑚𝑝𝑠 𝑑𝑒 𝑡𝑟𝑎𝑣𝑎𝑖𝑙 𝑚écanisé 𝑖 (h/ha|voy/h|bal/h|t/h) : Temps de travail mécanisé de l’intervention i.
 * Temps d'utilisation du matériel (h/ha|voy/h|bal/h|t/h) : Temps d'utilisation du matériel. Indicateur calculé par Agrosyst.
 * 𝑁𝑜𝑚𝑏𝑟𝑒 𝑑𝑒 𝑝𝑒𝑟𝑠𝑜𝑛𝑛𝑒𝑠 𝑖𝑛𝑡𝑒𝑟𝑣𝑒𝑛𝑎𝑛𝑡 𝑖 : Nombre de personnes mobilisées pour l’intervention i. Donnée saisie par l’utilisateur.
 *
 * @author Geoffroy Gley (gley@codelutin.com)
 */
public class IndicatorMechanizedWorkTime extends AbstractIndicator {

    public static final String COLUMN_NAME = "temps_travail_mecanise";
    public static final double DEFAULT_INVOLVED_PEOPLE_NUMBER_VALUE = 1.0d;

    private boolean detailedByMonth = true;
    protected String[] translatedMonth;

    private final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    public IndicatorMechanizedWorkTime() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, COLUMN_NAME + "_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, COLUMN_NAME + "_detail_champs_non_renseig");
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_ECONOMIC);
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) && displayed && (detailedByMonth || i == NB_MONTH);
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorLabel(int i) {
        String result;
        if (0 <= i && i < NB_MONTH) { // 12 months + sum
            result = l(locale, "Indicator.label.mechanizedWorkTimeMonth", translatedMonth[i]);
        } else {
            result = l(locale, "Indicator.label.mechanizedWorkTime");
        }
        return result;
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();

        for (int i = 0; i <= NB_MONTH; i++) {
            String label = getIndicatorLabel(i);
            String dbColumnName;
            if (i < NB_MONTH) {// 12 months + sum
                dbColumnName = COLUMN_NAME + "_" + MONTHS_FOR_DB_COLUMNS[i];
            } else {
                dbColumnName = COLUMN_NAME;
            }
            indicatorNameToColumnName.put(label, dbColumnName);
        }

        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {
        if (interventionContext.isFictive()) return newArray(NB_MONTH + 1, 0.0); // 12 months + sum
        PracticedIntervention intervention = interventionContext.getIntervention();
        Double toolsCouplingWorkingTime = interventionContext.getToolsCouplingWorkingTime();
        Double involvedPeople = getPracticedInterventionInvolvedPeopleNumber(intervention, DEFAULT_INVOLVED_PEOPLE_NUMBER_VALUE);
        double mechanizedWorkTime = computeMechanizedWorkTime(toolsCouplingWorkingTime, involvedPeople, intervention.getTopiaId());
        interventionContext.setMechanizedWorkTime(mechanizedWorkTime);
        return newResult(getMonthsRatio(intervention, mechanizedWorkTime));
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {
        EffectiveIntervention intervention = interventionContext.getIntervention();
        Double toolsCouplingWorkingTime = interventionContext.getToolsCouplingWorkingTime();
        Double involvedPeople = getEffectiveInterventionInvolvedPeopleCount(intervention, DEFAULT_INVOLVED_PEOPLE_NUMBER_VALUE);
        double mechanizedWorkTime = computeMechanizedWorkTime(toolsCouplingWorkingTime, involvedPeople, intervention.getTopiaId());
        interventionContext.setMechanizedWorkTime(mechanizedWorkTime);
        return newResult(getMonthsRatio(intervention, mechanizedWorkTime));
    }

    private Double computeMechanizedWorkTime(Double toolsCouplingWorkingTime, Double involvedPeople, String interventionId) {
        double mechanizedWorkTime = 0.0d;
        if (toolsCouplingWorkingTime != null && involvedPeople != null) {
            mechanizedWorkTime = toolsCouplingWorkingTime * involvedPeople;
        } else {
            addMissingIndicatorMessage(interventionId, messageBuilder.getMissingToolsCouplingWorkingTimeMessage());
        }
        return mechanizedWorkTime;
    }

    public void init(IndicatorFilter filter, String[] translatedMonth) {
        displayed = filter != null;
        detailedByMonth = displayed && BooleanUtils.isTrue(filter.getDetailedByMonth());
        this.translatedMonth = translatedMonth;
    }
}
