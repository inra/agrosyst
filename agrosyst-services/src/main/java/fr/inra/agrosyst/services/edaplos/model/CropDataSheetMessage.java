package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;

import java.util.ArrayList;
import java.util.List;

public class CropDataSheetMessage implements AgroEdiObject {

    /** Nom du fichier de provenance. */
    protected transient String filename;

    @Valid
    @NotEmpty(message = "Le document ne contient pas les informations nécessaires comme le TypeCode et l'Identification")
    protected final List<CropDataSheetDocument> cropDataSheetDocuments = new ArrayList<>();

    @Valid
    protected final List<AgriculturalPlot> agriculturalPlots = new ArrayList<>();

    public List<CropDataSheetDocument> getCropDataSheetDocuments() {
        return cropDataSheetDocuments;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void addCropDataSheetDocument(CropDataSheetDocument cropDataSheetDocument) {
        cropDataSheetDocuments.add(cropDataSheetDocument);
    }

    public void addAgriculturalPlot(AgriculturalPlot agriculturalPlot) {
        agriculturalPlots.add(agriculturalPlot);
    }

    public List<AgriculturalPlot> getAgriculturalPlots() {
        return agriculturalPlots;
    }

    @Override
    public String getLocalizedIdentifier() {
        return filename != null ? "fichier '" + filename + "'" : "";
    }
}
