package fr.inra.agrosyst.services.performance;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2022 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PlotTopiaDao;
import fr.inra.agrosyst.api.entities.PotInputUnit;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnection;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceFile;
import fr.inra.agrosyst.api.entities.performance.PerformanceFileTopiaDao;
import fr.inra.agrosyst.api.entities.performance.PerformanceState;
import fr.inra.agrosyst.api.entities.performance.PerformanceTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlotTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActiveTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCategTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement;
import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissementTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceConverter;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceConverterTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInputPriceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverterTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigoTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppaTopiaDao;
import fr.inra.agrosyst.api.entities.security.AgrosystUserTopiaDao;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.async.Task;
import fr.inra.agrosyst.api.services.common.BinaryStream;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.common.RefInputPriceService;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.PlotDto;
import fr.inra.agrosyst.api.services.domain.ZoneDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.performance.EquipmentUsageRange;
import fr.inra.agrosyst.api.services.performance.PerformanceDto;
import fr.inra.agrosyst.api.services.performance.PerformanceFilter;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.performance.PerformanceStatistics;
import fr.inra.agrosyst.api.services.performance.PracticedCropPath;
import fr.inra.agrosyst.api.services.performance.Scenario;
import fr.inra.agrosyst.api.services.performance.utils.CurrentPerformanceDto;
import fr.inra.agrosyst.api.services.performance.utils.PriceConverterKeysToRate;
import fr.inra.agrosyst.api.services.plot.PlotFilter;
import fr.inra.agrosyst.api.services.plot.ZoneFilter;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.security.AgrosystAccessDeniedException;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.async.BusinessTasksManager;
import fr.inra.agrosyst.services.async.ScheduledTask;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.security.SecurityContext;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.poifs.filesystem.FileMagic;
import org.hibernate.Session;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.internal.AbstractTopiaDao;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
@Setter
public abstract class CommonPerformanceService extends AbstractAgrosystService implements PerformanceService {
    
    private static final Log LOGGER = LogFactory.getLog(CommonPerformanceService.class);

    private static final String KEY_SEPARATOR = "_";
    public static final String PARIS_DEFAULT_INSEE_CODE = "75056";
    public static final String DEFAULT_DEEPEST_INDIGO = "Profond";
    public static final String DEFAULT_TEXTURAL_CLASSE = "LAS";

    public static String getValorisationKey(String codeEspeceBotanique, String codeQualifiantAee, String codeDestinationA, boolean isOrganic) {
        return codeEspeceBotanique + KEY_SEPARATOR + codeQualifiantAee + KEY_SEPARATOR + codeDestinationA + KEY_SEPARATOR + isOrganic;
    }

    protected AnonymizeService anonymizeService;
    protected BusinessAuthorizationService authorizationService;
    protected DomainInputStockUnitService domainInputStockUnitService;
    protected DomainService domainService;
    protected PracticedSystemService practicedSystemService;
    protected ReferentialService referentialService;
    protected RefInputPriceService refInputPriceService;
    protected AgrosystI18nService i18nService;
    protected PricesService pricesService;
//
    protected AbstractActionTopiaDao actionDao;
    protected AgrosystUserTopiaDao userDao;
    protected EffectiveCropCycleConnectionTopiaDao effectiveCropCycleConnectionDao;
    protected DomainTopiaDao domainDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDao;
    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCyclDao;
    protected EffectiveSeasonalCropCycleTopiaDao effectiveSeasonalCropCycleDao;
    protected GrowingSystemTopiaDao growingSystemDao;
    protected PlotTopiaDao plotTopiaDao;
    protected PracticedCropCycleConnectionTopiaDao practicedCropCycleConnectionDao;
    protected PracticedInterventionTopiaDao practicedInterventionDao;
    protected PracticedPerennialCropCycleTopiaDao practicedPerennialCropCycleDao;
    protected PracticedPlotTopiaDao practicedPlotTopiaDao;
    protected PracticedSeasonalCropCycleTopiaDao practicedSeasonalCropCycleDao;
    protected PracticedSystemTopiaDao practicedSystemDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;

    protected RefActaTraitementsProduitsCategTopiaDao refActaTraitementsProduitsCategDao;
    protected RefActaSubstanceActiveTopiaDao actaSubstanceActiveDao;
    protected RefAgsAmortissementTopiaDao refAgsAmortissementDao;
    protected RefLocationTopiaDao refLocationDao;
    protected RefSolProfondeurIndigoTopiaDao refSolProfondeurIndigosDao;
    protected RefSolTextureGeppaTopiaDao refSolTextureGeppaDao;
    protected ZoneTopiaDao zoneDao;
//
//    // performance dao
    protected PerformanceTopiaDao performanceDao;
    protected PerformanceFileTopiaDao performanceFileTopiaDao;
//
//    // prices
    protected RefHarvestingPriceTopiaDao refHarvestingPriceDao;
    protected RefHarvestingPriceConverterTopiaDao refHarvestingPriceConverterDao;
    protected RefInputUnitPriceUnitConverterTopiaDao refInputUnitPriceUnitConverterDao;
    
    protected RefInputPriceTopiaDao refInputPriceDao;
    
    public static final String IT_PREFIX = "IT_";
    
    /**
     * Pour un ensemble de connexion, reconstitue le graph pour calculer les % cumulés de chaque
     * connexion depuis l'origine du graph.
     *
     */
    public Map<PracticedCropCycleConnection, Double> computeCumulativeFrequencies(List<PracticedCropCycleConnection> cropCycleConnections) {
        List<PracticedCropCycleConnection> localConnections = new ArrayList<>(cropCycleConnections);
        
        // get precedence of each node
        MultiValuedMap<PracticedCropCycleNode, PracticedCropCycleConnection> precedenceMap = new HashSetValuedHashMap<>();
        for (PracticedCropCycleConnection conn : localConnections) {
            precedenceMap.put(conn.getTarget(), conn);
        }
        
        // on calcul les frequences cumulées en aditionnant les frequences des précédences
        // et on multiplie le tout par la frequence de la connexion courante excepté dans le cas ou la source est sur un rand supérieur à la target
        Map<PracticedCropCycleConnection, Double> result = new HashMap<>();
        for (PracticedCropCycleConnection conn : localConnections) {
            double frequency = getCumulativeFrequencies(precedenceMap, conn);
            result.put(conn, frequency);
        }
        return result;
    }
    
    /**
     * Retourne le pourcentage cumulé d'une connection récursivement jusqu'à ce qu'une connexion
     * n'ait plus de précédence.
     *
     * @param precedenceMap precedence map
     * @param conn          connection
     * @return cumulative frequency
     */
    protected static double getCumulativeFrequencies(MultiValuedMap<PracticedCropCycleNode, PracticedCropCycleConnection> precedenceMap,
                                                     PracticedCropCycleConnection conn) {
        double result;
        Collection<PracticedCropCycleConnection> precedences = precedenceMap.get(conn.getSource());
        if (CollectionUtils.isEmpty(precedences)) {
            result = 1d;
        } else {
            result = 0d;
            for (PracticedCropCycleConnection precedence : precedences) {
                // manage loop connection on end cycle node
                final PracticedCropCycleNode source = precedence.getSource();
                final PracticedCropCycleNode target = precedence.getTarget();
            
                final int precedenceSourceRank = source.getRank();
                final int precedenceTargetRank = target.getRank();
            
                if (source.isEndCycle() && (precedenceSourceRank > precedenceTargetRank)) {
                    // previous node = first node
                    // on retourne la valeur de la fréquence initiale
                    result = target.getInitNodeFrequency() != null ?
                            target.getInitNodeFrequency() / 100.d :
                            0d;
                
                    break; // we are on the 1st connection, the other precedence will result on same result
                } else if (precedenceSourceRank < precedenceTargetRank) {
                    // on fait la somme des fréquences cumulées
                    result += getCumulativeFrequencies(precedenceMap, precedence);
                }
            }
        }
        return (result * conn.getCroppingPlanEntryFrequency()) / 100.0d;
    }
    
    /**
     * Parmis les rangs définit sur le graph, détermine combien de campagnes existe réelement
     * dans les cas où des campagnes portent sur plusieurs rang.
     *
     * @param nodes cycle nodes
     * @return campaigns count
     */
    public long getCampaignsCount(Collection<PracticedCropCycleNode> nodes) {
    
        long nbRank = nodes.stream().filter(n -> !n.isSameCampaignAsPreviousNode()).
                mapToInt(PracticedCropCycleNode::getRank).
                distinct().
                count();
    
        // cas particulier où tous les noeuds serait sur la même campagne
        if (nbRank == 0) {
            nbRank = 1;
        }
    
        return nbRank;
    }
    
    public Set<PracticedCropPath> retrievePracticedPaths(Collection<PracticedCropCycleConnection> connections) {
        
        final Set<PracticedCropPath> paths = new HashSet<>();
        
        if (!connections.isEmpty()) {
            final Map<Integer, List<PracticedCropCycleConnection>> connectionsPerTargetRank = connections.stream().
                    collect(
                            Collectors.groupingBy(c -> c.getTarget().getRank()));
    
            int minRank = Collections.min(connectionsPerTargetRank.keySet());
            int maxRank = Collections.max(connectionsPerTargetRank.keySet());
    
            final List<PracticedCropCycleConnection> startingConnections = connectionsPerTargetRank.get(minRank);
    
            // look for starting connection. Many connections targeting one node can come from other ending cycle node
            final Map<PracticedCropCycleNode, List<PracticedCropCycleConnection>> startingConnectionsByNode = new HashMap<>();
            for (PracticedCropCycleConnection practicedCropCycleConnection : startingConnections) {
                PracticedCropCycleNode target = practicedCropCycleConnection.getTarget();
                List<PracticedCropCycleConnection> connexionsForNode = startingConnectionsByNode.computeIfAbsent(target, k -> new ArrayList<>());
                connexionsForNode.add(practicedCropCycleConnection);
            }
            
            // if there are several starting node, there are also several starting path
            // we build the path for all starting nodes
            for (Map.Entry<PracticedCropCycleNode, List<PracticedCropCycleConnection>> startingConnectionsForNode : startingConnectionsByNode.entrySet()) {
                final List<PracticedCropCycleConnection> startingConnections0 = startingConnectionsForNode.getValue();
                
                // we look for the all return connections: connection that goes from the end cycle node to a first cycle node.
                final List<PracticedCropCycleConnection> returnConnections = new ArrayList<>();
                for (PracticedCropCycleConnection practicedCropCycleConnection : startingConnections0) {
                    if(practicedCropCycleConnection.getTarget().getRank() < practicedCropCycleConnection.getSource().getRank()) {
                        returnConnections.add(practicedCropCycleConnection);
                    }
                }
                
                // we start building the path
                for (PracticedCropCycleConnection startingConnection : startingConnections0) {
                    if (connectionsPerTargetRank.size() == 1 || startingConnection.getTarget().isEndCycle()) {
                        // the path has just one node
                        PracticedCropPath finalPath = new PracticedCropPath();
                        finalPath.addConnection(startingConnection);
                        paths.add(finalPath);
                    } else {
                        final List<PracticedCropPath> currentPath = buildPathsForGivenRank(
                                connectionsPerTargetRank,
                                minRank + 1,
                                startingConnection.getTarget(),
                                returnConnections,
                                maxRank);
                        if (currentPath != null) {
                            paths.addAll(currentPath);
                        }
                    }
                }
            }
        }
        return paths;
    }
    
    protected List<PracticedCropPath> buildPathsForGivenRank(final Map<Integer, List<PracticedCropCycleConnection>> connectionsPerTargetRank,
                                                             final int currentRank,
                                                             final PracticedCropCycleNode sourceNode,
                                                             final List<PracticedCropCycleConnection> returnConnections,
                                                             int maxRank) {
    
        List<PracticedCropCycleConnection> practicedCropCycleConnections = connectionsPerTargetRank.get(currentRank);
    
        final int nextRank = currentRank + 1;

        if (practicedCropCycleConnections == null) {
            // Pas de connection sur ce rang : peut être jachère
            if (nextRank <= maxRank) {
                return buildPathsForGivenRank(
                        connectionsPerTargetRank,
                        nextRank,
                        sourceNode,
                        returnConnections,
                        maxRank);
            } else {
                // il n'est pas possible d'aller plus loin
                return null;
            }
        }

        List<PracticedCropPath> paths = new ArrayList<>();
        
        boolean connexionFoundForGivenRankAndSourceNode = false;
        for (PracticedCropCycleConnection practicedCropCycleConnection : practicedCropCycleConnections) {
            
            final PracticedCropCycleNode source = practicedCropCycleConnection.getSource();
            if (sourceNode.equals(source)) {
                // S'il s'agit d'une connection vers le noeud final, on termine le chemin
                final PracticedCropCycleNode target = practicedCropCycleConnection.getTarget();
                if (target.isEndCycle() || currentRank == maxRank) {
                    PracticedCropPath finalPath = new PracticedCropPath();
                    finalPath.addConnection(practicedCropCycleConnection);
                    paths.add(finalPath);
                    // we have return connection we had them to the path
                    if (returnConnections != null) {
                        for (PracticedCropCycleConnection cropCycleConnection : returnConnections) {
                            if (cropCycleConnection.getSource().equals(target)) {
                                finalPath.addConnection(cropCycleConnection);
                            }
                        }
                    }
    
                } else {
                    // Sinon, on va chercher l'ensemble des chemins depuis la connection courante (ie: rang suivant) auxquelles on ajoute la connection courante
                    List<PracticedCropPath> nextPaths = buildPathsForGivenRank(
                            connectionsPerTargetRank,
                            nextRank,
                            target,
                            returnConnections, maxRank);
                    if (nextPaths != null) {
                        nextPaths.forEach(path -> path.addConnection(practicedCropCycleConnection));
                        paths.addAll(nextPaths);
                    }
                }
                connexionFoundForGivenRankAndSourceNode = true;
            }
        }
        if (!connexionFoundForGivenRankAndSourceNode) {
            // pas de connexion trouvée sur ce rang en lien avec la culture source
            // on cherche sur le rang suivant
            return buildPathsForGivenRank(
                    connectionsPerTargetRank,
                    nextRank,
                    sourceNode,
                    returnConnections, maxRank);
        }
        
        return paths;
        
    }
    
    public Set<Network> getIRs(GrowingSystem gs) {
    
        Set<Network> irs = gs != null && CollectionUtils.isNotEmpty(gs.getNetworks()) ? new HashSet<>(gs.getNetworks()) : new HashSet<>();
    
        Set<Network> its = irs.stream().
                filter(network -> network.getName()
                        .trim().
                                toUpperCase().
                                startsWith(CommonPerformanceService.IT_PREFIX))
                .collect(Collectors.toSet());
    
        irs.removeAll(its);
    
        return irs;
    }
    
    public Set<Network> getIts(GrowingSystem gs) {
    
        Set<Network> irs = gs != null &&
                CollectionUtils.isNotEmpty(gs.getNetworks()) ?
                new HashSet<>(gs.getNetworks()) :
                new HashSet<>();
    
        Set<Network> its = irs.stream().
                filter(network ->
                        network.
                                getName().
                                trim().
                                toUpperCase().
                                startsWith(CommonPerformanceService.IT_PREFIX))
                .collect(Collectors.toSet());
    
        irs.removeAll(its);
    
        its.addAll(getIts(irs));
    
        return its;
    }
    
    public Set<Network> getIts(Collection<Network> irs) {
        Set<Network> its = new HashSet<>();
        if (irs != null) {
            Set<Network> subParents = irs.stream().
                    map(Network::getParents).
                    filter(Objects::nonNull).
                    flatMap(Collection::stream).
                    collect(Collectors.toSet());
    
            while (!subParents.isEmpty()) {
                its.addAll(subParents.stream().
                        filter(network ->
                                network.getName().
                                        trim().
                                        toUpperCase().
                                        startsWith(IT_PREFIX)).toList());
        
                subParents.removeAll(its);
                Set<Network> nextSubParents = new HashSet<>();
                for (Network network : subParents) {
                    if (network.getParents() != null) {
                        nextSubParents.addAll(network.getParents());
                    }
                }
                subParents = nextSubParents;
            }
        }
        return its;
    }

    @Override
    public PaginationResult<PerformanceDto> getFilteredPerformances(PerformanceFilter performanceFilter, int subItemsLimit) {
        PaginationResult<Performance> performances = performanceDao.getFilteredPerformances(performanceFilter, getSecurityContext());
        PaginationResult<PerformanceDto> result = performances.transform(anonymizeService.getPerformanceToDtoFunction(subItemsLimit));
        return result;
    }

    @Override
    public Performance newPerformance() {
        return performanceDao.newInstance();
    }

    @Override
    public Performance getPerformance(String performanceTopiaId) {
        Performance result;
        if (StringUtils.isBlank(performanceTopiaId)) {
            result = newPerformance();
        } else {
            authorizationService.checkPerformanceReadable(performanceTopiaId);
            result = performanceDao.forTopiaIdEquals(performanceTopiaId).findUnique();
        }
        return result;
    }

    @Override
    public void saveState(String performanceId, PerformanceState state) {
        if (StringUtils.isNotBlank(performanceId)) {
            try {
    
                if (LOGGER.isDebugEnabled()) {
                    String message = "For performance %s status is now %s";
                    LOGGER.debug(String.format(message, performanceId, state));
                }

                performanceDao.changePerformanceStatus(performanceId, state);
                getTransaction().commit();

            } catch (Exception e) {
                LOGGER.error("Failed to change status of performance to " + state, e);
            }
        }
    }

    @Override
    public PaginationResult<PlotDto> getPlotsDto(PlotFilter filter) {
        int pageNumber = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;

        PaginationParameter page = PaginationParameter.of(pageNumber, count);
        Collection<String> selectedIds = null;
        Collection<String> domainIds = null;
        Collection<String> growingSystemIds = null;
        Boolean active = null;

        if (filter != null) {
            selectedIds = filter.getSelectedIds();
            domainIds = filter.getDomainIds();
            growingSystemIds = filter.getGrowingSystemIds();
            active = filter.getActive();
        }

        SecurityContext securityContext = getSecurityContext();
        PaginationResult<Plot> plotsPage = plotTopiaDao.findWithDomainAndSdcFilter(
                securityContext,
                page,
                selectedIds,
                domainIds,
                growingSystemIds,
                active
        );

        Function<Plot, PlotDto> anonymizePlotFunction = anonymizeService.getPlotToDtoFunction();
        return plotsPage.transform(anonymizePlotFunction);
    }

    @Override
    public Set<String> getPlotIds(PlotFilter filter) {
        AbstractTopiaDao.InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<Plot> query = plotTopiaDao.newQueryBuilder();
        if (filter != null) {
            if (CollectionUtils.isNotEmpty(filter.getDomainIds())) {

                query.addTopiaIdIn(Plot.PROPERTY_DOMAIN, DaoUtils.getRealIds(filter.getDomainIds(), Domain.class));
            }
            if (CollectionUtils.isNotEmpty(filter.getGrowingSystemIds())) {
                query.addTopiaIdIn(Plot.PROPERTY_GROWING_SYSTEM, DaoUtils.getRealIds(filter.getGrowingSystemIds(), GrowingSystem.class));
            }
        }
        List<String> allIds = query.findAllIds();
        return new HashSet<>(allIds);
    }

    @Override
    public PaginationResult<ZoneDto> getZonesDto(ZoneFilter filter) {
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
    
    
        AbstractTopiaDao.InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<Zone> query = zoneDao.newQueryBuilder();
        if (filter != null) {
            if (CollectionUtils.isNotEmpty(filter.getSelectedIds())) {
                query.addTopiaIdIn(Zone.PROPERTY_TOPIA_ID, DaoUtils.getRealIds(filter.getSelectedIds(), Zone.class));
            }
            if (CollectionUtils.isNotEmpty(filter.getPlotIds())) {
                query.addTopiaIdIn(Zone.PROPERTY_PLOT, DaoUtils.getRealIds(filter.getPlotIds(), Plot.class));
            }
            if (filter.getActive() != null) {
                query.addEquals(Zone.PROPERTY_ACTIVE, filter.getActive());
            }
        }
        long totalCount = query.count();
        List<Zone> zones = query.setOrderByArguments(Zone.PROPERTY_NAME)
                .find(startIndex, endIndex);

        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        PaginationResult<Zone> result = PaginationResult.of(zones, totalCount, pager);
        return result.transform(anonymizeService.getZoneToDtoFunction(true, false));
    }

    @Override
    public Set<String> getZoneIds(ZoneFilter filter) {
        AbstractTopiaDao.InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<Zone> query = zoneDao.newQueryBuilder();
        if (filter != null && CollectionUtils.isNotEmpty(filter.getPlotIds())) {
            query.addTopiaIdIn(Zone.PROPERTY_PLOT, DaoUtils.getRealIds(filter.getPlotIds(), Plot.class));
        }
        return new HashSet<>(query.findAllIds());
    }

    @Override
    public Performance createperformance(
            String name,
            List<String> domainIds,
            List<String> growingSystemIds,
            List<String> plotIds,
            List<String> zoneIds,
            List<IndicatorFilter> indicatorFilters,
            Set<String> scenarioCodes,
            boolean exportToDb,
            boolean isPracticed) {

        Performance performanceCreated = createOrUpdatePerformance(
                null,
                name,
                domainIds,
                growingSystemIds,
                plotIds,
                zoneIds,
                indicatorFilters,
                Collections.emptySet(),
                exportToDb,
                isPracticed);

        return performanceCreated;
    }

    @Override
    public Performance updatePerformance(
            String performanceId,
            String name,
            List<String> domainIds,
            List<String> growingSystemIds,
            List<String> plotIds,
            List<String> zoneIds,
            List<IndicatorFilter> indicatorFilters,
            Set<String> scenarioCodes,
            boolean exportToDb) {

        Performance performanceCreated = createOrUpdatePerformance(
                performanceId,
                name,
                domainIds,
                growingSystemIds,
                plotIds,
                zoneIds,
                indicatorFilters,
                Collections.emptySet(),
                exportToDb,
                null);
        
        return performanceCreated;
    }

    public Performance createOrUpdatePerformance(String performanceId,
                                                 String name,
                                                 List<String> domainIds,
                                                 List<String> growingSystemIds,
                                                 List<String> plotIds,
                                                 List<String> zoneIds,
                                                 Collection<IndicatorFilter> indicatorFilters,
                                                 Set<String> scenarioCodes,
                                                 boolean exportToDb,
                                                 Boolean isPracticed) {

        if (exportToDb) {
            return createPerformanceToDb(domainIds);
        }

        CurrentPerformanceDto performanceDto = new CurrentPerformanceDto();

        performanceDto.setTopiaId(performanceId);

        performanceDto.setName(name);

        BusinessTasksManager businessTasksManager = getBusinessTaskManager();

        Collection<Domain> domains;
        if (CollectionUtils.isNotEmpty(domainIds)) {
            domains = domainDao.forTopiaIdIn(DaoUtils.getRealIds(domainIds, Domain.class)).findAll();
        } else {
            domains = null;
        }
        performanceDto.setDomains(domains);

        Collection<GrowingSystem> growingSystems;
        if (CollectionUtils.isNotEmpty(growingSystemIds)) {
            growingSystems = growingSystemDao.forTopiaIdIn(DaoUtils.getRealIds(growingSystemIds, GrowingSystem.class)).findAll();
        } else {
            growingSystems = null;
        }
        performanceDto.setGrowingSystems(growingSystems);

        Collection<Plot> plots;
        if (CollectionUtils.isNotEmpty(plotIds)) {
            plots = plotTopiaDao.forTopiaIdIn(DaoUtils.getRealIds(plotIds, Plot.class)).findAll();
        } else {
            plots = null;
        }
        performanceDto.setPlots(plots);

        Collection<Zone> zones;
        if (CollectionUtils.isNotEmpty(zoneIds)) {
            zones = zoneDao.forTopiaIdIn(DaoUtils.getRealIds(zoneIds, Zone.class)).findAll();
        } else {
            zones = null;
        }
        performanceDto.setZones(zones);
    
        performanceDto.setUpdateDate(context.getOffsetTime());
        performanceDto.setComputeStatus(PerformanceState.GENERATING);
        performanceDto.setExportType(ExportType.FILE);
        
        boolean isScenarioCodeValid = isScenarioCodeValid(indicatorFilters, scenarioCodes);
        if (!isScenarioCodeValid) {
            scenarioCodes = null;
        }
    
        performanceDto.setScenarioCodes(scenarioCodes);

        Binder<CurrentPerformanceDto, Performance> performanceBinder = BinderFactory.newBinder(CurrentPerformanceDto.class, Performance.class);

        Performance persistedPerformance;
        if (performanceDto.isPersisted()) {

            cancelPendingPerformances(performanceDto.getTopiaId());

            persistedPerformance = performanceDao.forTopiaIdEquals(performanceDto.getTopiaId()).findUnique();

            // remove
            persistedPerformance.clearIndicatorFilter();

            Collection<IndicatorFilter> persistedIndicatorFilters = createOrUpdateIndicatorFilters(
                    indicatorFilters,
                    scenarioCodes);
            // add
            persistedIndicatorFilters
                    .forEach(persistedPerformance::addIndicatorFilter);

            performanceBinder.copyExcluding(
                    performanceDto, persistedPerformance,
                    Performance.PROPERTY_TOPIA_ID,
                    Performance.PROPERTY_TOPIA_CREATE_DATE,
                    Performance.PROPERTY_TOPIA_VERSION,
                    Performance.PROPERTY_INDICATOR_FILTER,
                    Performance.PROPERTY_PRACTICED,
                    Performance.PROPERTY_AUTHOR
            );

            persistedPerformance = performanceDao.update(persistedPerformance);

        } else {

            performanceDto.setPracticed(isPracticed);
            String userId = getSecurityContext().getUserId();
            performanceDto.setAuthor(userDao.forTopiaIdEquals(userId).findUnique());

            Collection<IndicatorFilter> persistedIndicatorFilters = createOrUpdateIndicatorFilters(
                    indicatorFilters,
                    scenarioCodes);

            persistedPerformance = performanceDao.newInstance();
            performanceBinder.copy(performanceDto, persistedPerformance);
            persistedPerformance.addAllIndicatorFilter(persistedIndicatorFilters);

            persistedPerformance = performanceDao.create(persistedPerformance);
        }

        performanceId = persistedPerformance.getTopiaId();

        try {
            authorizationService.checkCreateOrUpdatePerformance(performanceId);
        } catch (AgrosystAccessDeniedException e) {
            getTransaction().rollback();
            throw e;
        }

        if (LOGGER.isDebugEnabled()) {
            String message = "For performance status is now GENERATING";
            LOGGER.debug(String.format(message, performanceDto.getTopiaId()));
        }

        cancelPendingPerformances(performanceId);

        getTransaction().commit();

        PerformanceFileTask task = new PerformanceFileTask(getAuthenticatedUser(), performanceId);
        businessTasksManager.schedule(task);

        return persistedPerformance;
    }

    protected void cancelPendingPerformances(String performanceId) {
        if (StringUtils.isNoneBlank(performanceId)) {
            BusinessTasksManager businessTasksManager = getBusinessTaskManager();
            ImmutableList<ScheduledTask> runningAndPendingTasks = businessTasksManager.getRunningAndPendingTasks();
            List<Task> currentOrPendingSamePerformaceTask = runningAndPendingTasks.stream()
                    .map(ScheduledTask::getTask)
                    .filter(t -> t instanceof PerformanceFileTask)
                    .filter(t -> ((PerformanceFileTask) t).getPerformanceId().equals(performanceId))
                    .toList();

            if (CollectionUtils.isNotEmpty(currentOrPendingSamePerformaceTask)) {
                currentOrPendingSamePerformaceTask.forEach(
                    cpt -> {
                        if (LOGGER.isInfoEnabled()) {
                            LOGGER.info(String.format("Performance %s en cours d'exécution mais la tâche suivant %s avec comme Id %s lancée par l'utilisateur %s et encours, on essaie de l'interrompre.",
                                    performanceId,
                                    cpt.getDescription(),
                                    cpt.getTaskId().toString(),
                                    cpt.getUserEmail()));
                        }
                        try {
                            businessTasksManager.cancelPendingTask(cpt.getTaskId());
                        } catch (Exception e) {
                            // do nothing
                        }
                    }
                );
            }

        }
    }

    @Override
    public abstract Performance createPerformanceToDb(List<String> filteredDomainIds);
    
    protected Set<String> getAllScenarioCodes() {
        PaginationResult<Scenario> allScenario = refInputPriceDao.findAllScenariosByScenarioCodeForName(null);
        return allScenario.getElements().stream().map(Scenario::code).collect(Collectors.toSet());
    }
    
    public List<GenericIndicator> getAllIndicators(Collection<String> scenarioCodes) {
    
        Collection<IndicatorFilter> indicatorFilters = getAllIndicatorFilters();
    
        Map<String, IndicatorFilter> indicatorFiltersByClazz = indicatorFilters.stream()
                .collect(Collectors.toMap(IndicatorFilter::getClazz, Function.identity()));
    
        // build indicator list to compute
        return getIndicators(indicatorFiltersByClazz, scenarioCodes, false);
    }
    
    protected abstract List<GenericIndicator> getIndicators(Map<String, IndicatorFilter> indicatorFiltersByClazz, Collection<String> scenarioCodes, boolean exportToFile);
    
    protected abstract void createOrUpdateIndicatorFilters(Performance performance,
                                                           Collection<IndicatorFilter> indicatorFilters,
                                                           boolean computeOnReal,
                                                           boolean computeOnStandardised,
                                                           Collection<String> scenarioCodes);

    protected abstract Collection<IndicatorFilter> createOrUpdateIndicatorFilters(
            Collection<IndicatorFilter> indicatorFilters,
            Collection<String> scenarioCodes);
    
    protected abstract boolean isScenarioCodeValid(Collection<IndicatorFilter> indicatorFilters, Collection<String> scenarioCodes);
    
    @Override
    public PerformanceState getPerformanceStatus(String performanceId) {
        PerformanceState result = null;
        if (StringUtils.isNotBlank(performanceId)) {
            Performance performance = performanceDao.forTopiaIdEquals(performanceId).findUnique();
            result = performance.getComputeStatus();
        }
        return result;
    }

    protected File generatePerformanceFileToTmpFile(CurrentPerformanceDto performance) throws IOException {
        // write on file system instead of memory
        File tempWriterFile = File.createTempFile("agrosyst-perf-", ".xlsx");
        tempWriterFile.deleteOnExit();

        try (FileOutputStream outTempStream = new FileOutputStream(tempWriterFile)) {
            // build writer
            Language language = getSecurityContext().getLanguage();
            IndicatorWriter writer = new PoiIndicatorWriter(language, performance, outTempStream, i18nService);

            // compute and output to writer
            writer.init();
            // compute the indicators
            convertToWriter(performance, writer);

            writer.finish();
        }

        return tempWriterFile;
    }

    protected void writePerformanceFileToDatabase(CurrentPerformanceDto performanceDto, File generatedFile) throws IOException {

        try (InputStream inputStream = new BufferedInputStream(new FileInputStream(generatedFile))) {

            Performance performance = performanceDao.forTopiaIdEquals(performanceDto.getTopiaId()).findUnique();
            PerformanceFile performanceFile = performanceFileTopiaDao.forPerformanceEquals(performance)
                    .findUniqueOrNull();

            if (performanceFile == null) {
                performanceFile = performanceFileTopiaDao.newInstance();
                performanceFile.setPerformance(performance);
            }
            performanceFile.setPerformance(performance);

            // store binary stream into database
            Session hibernateSession = getPersistenceContext().getHibernateSupport().getHibernateSession();
            Blob blob = hibernateSession.getLobHelper().createBlob(inputStream.readAllBytes());
            performanceFile.setContent(blob);
    
            if (LOGGER.isDebugEnabled()) {
                String message = performanceFile.isPersisted() ? "Performance %s will be updated " : "Performance %s will be crated";
                LOGGER.debug(String.format(message, performanceDto.getTopiaId()));
            }
    
            // persist performance file
            if (performanceFile.isPersisted()) {
                performanceFileTopiaDao.update(performanceFile);
            } else {
                performanceFileTopiaDao.create(performanceFile);
            }

            if (LOGGER.isDebugEnabled()) {
                String message = "For performance status is now SUCCESS";
                LOGGER.debug(String.format(message, performanceDto.getTopiaId()));
            }
    
            performanceDao.changePerformanceStatus(performanceDto.getTopiaId(), PerformanceState.SUCCESS);

            getTransaction().commit();
        }
    }

    @Override
    public String generatePerformanceFile(String performanceId) throws Exception {
    
        // get performance
        Performance performance_ = performanceDao.forTopiaIdEquals(performanceId).findUniqueOrNull();
        Binder<Performance, CurrentPerformanceDto> performanceBinder = BinderFactory.newBinder(Performance.class, CurrentPerformanceDto.class);
        CurrentPerformanceDto performanceDto = new CurrentPerformanceDto();
        performanceBinder.copy(performance_, performanceDto);
    
        // write on file system instead of memory
        if (ExportType.FILE.equals(performanceDto.getExportType())) {
            File tempWriterFile;
            try {
                tempWriterFile = generatePerformanceFileToTmpFile(performanceDto);
            } catch (Exception eee) {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("Unexpected error during performance generation", eee);
                }
                performanceFailureEndingProcess(performanceDto);
                throw eee;
            }
        
            try {
                // Écrit le fichier généré en base
                writePerformanceFileToDatabase(performanceDto, tempWriterFile);
            } catch(Exception eee) {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("Unexpected error while saving file to database, rollback", eee);
                }
                getTransaction().rollback();
                performanceFailureEndingProcess(performanceDto);
                throw eee;
            } finally {
                // Le fichier est écrit en base, on le supprime
                FileUtils.deleteQuietly(tempWriterFile);
            }
        }
    
        return performanceDto.getName();
    
    }

    protected void performanceFailureEndingProcess(CurrentPerformanceDto performanceDto) {
        // update status to failed
        String performanceName = performanceDto.getName();
        try {
            if (LOGGER.isDebugEnabled()) {
                String message = "For performance %s status is now FAILED";
                LOGGER.debug(String.format(message, performanceDto.getTopiaId()));
            }
            performanceDao.changePerformanceStatus(performanceDto.getTopiaId(), PerformanceState.FAILED);
            // commit for status
            getTransaction().commit();
        } catch (Exception e) {
            saveState(performanceDto.getTopiaId(), PerformanceState.FAILED);
            String message = String.format("Failed to save performance : '%s' ; id=%s", performanceName, performanceDto.getTopiaId());
            LOGGER.error(message);
        }

    }
    
    @Override
    public Map<RefMateriel, RefAgsAmortissement> getDeprecationRateByEquipments(List<ToolsCoupling> toolsCouplings) {
        
        Map<RefMateriel, RefAgsAmortissement> deprecationRateByEquipments = new HashMap<>();
        
        List<ToolsCoupling> toolsCouplings0 = toolsCouplings.stream().
                filter(toolsCoupling -> Objects.nonNull(toolsCoupling.getTractor())).toList();
        
        Set<RefMateriel> tractors = toolsCouplings0.stream().
                map(ToolsCoupling::getTractor).
                map(Equipment::getRefMateriel).
                filter(refMateriel -> (refMateriel instanceof RefMaterielTraction)).
                collect(Collectors.toSet());
        
        if (!tractors.isEmpty()) {
            deprecationRateByEquipments.putAll(refAgsAmortissementDao.findRefAgsAmortissementForTractors(tractors));
        }
        
        Set<RefMateriel> automotors = toolsCouplings0.stream().
                map(ToolsCoupling::getTractor).
                map(Equipment::getRefMateriel).
                filter(refMateriel -> (refMateriel instanceof RefMaterielAutomoteur)).
                collect(Collectors.toSet());
        
        if (!automotors.isEmpty()) {
            deprecationRateByEquipments.putAll(refAgsAmortissementDao.findRefAgsAmortissementForAutomotors(automotors));
        }
        
        Set<RefMateriel> equipments = new HashSet<>();
        for (ToolsCoupling toolsCoupling : toolsCouplings0) {
            // all equipments that are not irrigation equipment.
            // there are no amortissement for irrigation equipment
            equipments.addAll(
                    toolsCoupling.getEquipments().stream().
                            map(Equipment::getRefMateriel).
                            filter(refMateriel -> !(refMateriel instanceof RefMaterielIrrigation)).
                            collect(Collectors.toSet()));
        }
        if (!equipments.isEmpty()) {
            deprecationRateByEquipments.putAll(refAgsAmortissementDao.findRefAgsAmortissementForTools(equipments));
        }
        
        return deprecationRateByEquipments;
    }

    @Override
    public BinaryStream downloadPerformances(String performanceId) {

        Performance performance = performanceDao.forTopiaIdEquals(performanceId).findUnique();
        PerformanceFile performanceFile = performanceFileTopiaDao.forPerformanceEquals(performance).findUniqueOrNull();

        // write file
        BinaryStream file;
        try {
            InputStream inputStream = performanceFile.getContent().getBinaryStream();
            file = new BinaryStream(inputStream);

            // test stream content type
            Preconditions.checkArgument(inputStream.markSupported());
            if (FileMagic.valueOf(inputStream) == FileMagic.OOXML) {
                file.setType(BinaryStream.FileType.XLSX);
            } else {
                file.setType(BinaryStream.FileType.XLS);
            }

            // le code plus haut ne fonctionne pas tout seul :(
            // pour l'instant, on recupere deux fois le flux pour faire la même chose
            // et ne pas corrompre le flux final
            file.setStream(performanceFile.getContent().getBinaryStream());
        } catch (SQLException | IOException ex) {
            throw new AgrosystTechnicalException("Can't get performance file stream", ex);
        }

        return file;
    }

    /**
     * Compute performances indicators with all available indicators.
     *
     * @param performance performance
     * @param writer      writer
     */
    protected void convertToWriter(CurrentPerformanceDto performance, IndicatorWriter writer) {
    
        Collection<IndicatorFilter> indicatorFilters = performance.getIndicatorFilter();
        Map<String, IndicatorFilter> indicatorFiltersByClazz = indicatorFilters == null ?
                new HashMap<>() :
                indicatorFilters.stream().
                        collect(Collectors.toMap(IndicatorFilter::getClazz, Function.identity()));
    
        final Collection<String> scenarioCodes = performance.getScenarioCodes();
    
        List<GenericIndicator> indicators = getIndicators(indicatorFiltersByClazz, scenarioCodes, true);
    
        GenericIndicator[] allIndicator = new GenericIndicator[indicators.size()];
    
        convertToWriter(performance, writer, indicatorFilters, indicators.toArray(allIndicator));
    }

    @Deprecated
    protected void convertToWriter(Performance performance, IndicatorWriter writer) {
        Binder<Performance, CurrentPerformanceDto> performanceBinder = BinderFactory.newBinder(Performance.class, CurrentPerformanceDto.class);
        CurrentPerformanceDto performanceDto = new CurrentPerformanceDto();
        performanceBinder.copy(performance, performanceDto);
        convertToWriter(performanceDto, writer);
    }

    @Deprecated
    public void convertToWriter(Performance performance, IndicatorWriter writer, Collection<IndicatorFilter> indicatorFilters, GenericIndicator... indicators) {
        Binder<Performance, CurrentPerformanceDto> performanceBinder = BinderFactory.newBinder(Performance.class, CurrentPerformanceDto.class);
        CurrentPerformanceDto performanceDto = new CurrentPerformanceDto();
        performanceBinder.copy(performance, performanceDto);
        convertToWriter(performanceDto, writer, indicatorFilters, indicators);
    }
    /**
     * Compute performances indicators with given indicators.
     *
     * @param performanceDto performance
     * @param writer      writer
     * @param indicators  indicators
     */
    public void convertToWriter(CurrentPerformanceDto performanceDto, IndicatorWriter writer, Collection<IndicatorFilter> indicatorFilters, GenericIndicator... indicators) {
    
        Collection<Domain> domains = performanceDto.getDomains();
        if (CollectionUtils.isEmpty(domains)) {
            return;
        }
    
        try {
            writer.addComputedIndicators(indicators);
        
            final Collection<String> scenarioCodes0 = performanceDto.getScenarioCodes();
            final Set<String> scenarioCodes = scenarioCodes0 != null ? new HashSet<>(scenarioCodes0) : Collections.emptySet();
        
            final RefSolTextureGeppa defaultRefSolTextureGeppa = refSolTextureGeppaDao.forNaturalId(DEFAULT_TEXTURAL_CLASSE).findUnique();
            final RefSolProfondeurIndigo defaultSolDepth = refSolProfondeurIndigosDao.forNaturalId(DEFAULT_DEEPEST_INDIGO).findUnique();
            final RefLocation defaultLocation = refLocationDao.forCodeInseeEquals(PARIS_DEFAULT_INSEE_CODE).findUnique();
            final List<RefHarvestingPriceConverter> allHarvestingPriceConverters = refHarvestingPriceConverterDao.findAll();
            final PriceConverterKeysToRate priceConverterKeysToRate = new PriceConverterKeysToRate(allHarvestingPriceConverters);
        
            // execute performance computing with performance author
            SecurityContext securityContext = getSecurityContextAsUser(performanceDto.getAuthor().getTopiaId());
        
            // get domainAndAnonymizeDomain again for security and anonymization
            Map<Domain, Domain> domainToAnonymizeDomaines = new HashMap<>();
            Map<String, Domain> domainByIds = domains.stream().collect(Collectors.toMap(Entities.GET_TOPIA_ID, Function.identity()));
            Collection<Domain> anonymizeDomains = anonymizeService.checkForDomainsAnonymization(domains);
            for (Domain anonymizeDomain : anonymizeDomains) {
                domainToAnonymizeDomaines.put(domainByIds.get(anonymizeDomain.getTopiaId()), anonymizeDomain);
            }

            i18nService.translateDomainSpecies(domains);

            if (performanceDto.isPracticed()) {
    
                computePracticedPerformance(
                        performanceDto,
                        writer,
                        domains,
                        scenarioCodes,
                        defaultRefSolTextureGeppa,
                        defaultSolDepth,
                        defaultLocation,
                        priceConverterKeysToRate,
                        securityContext,
                        domainToAnonymizeDomaines,
                        indicatorFilters,
                        indicators);
            
            } else {
    
                computeEffectivePerformance(
                        performanceDto,
                        writer,
                        domains,
                        scenarioCodes,
                        defaultRefSolTextureGeppa,
                        defaultSolDepth,
                        defaultLocation,
                        priceConverterKeysToRate,
                        securityContext,
                        domainToAnonymizeDomaines,
                        indicatorFilters,
                        indicators);
            
            }
        } catch (Exception e) {
            saveState(performanceDto.getTopiaId(), PerformanceState.FAILED);
            throw new AgrosystTechnicalException("Échec de génération de la performance " + performanceDto.getTopiaId(), e);
        }
    }
    
    protected abstract void computeEffectivePerformance(CurrentPerformanceDto performance,
                                                        IndicatorWriter writer,
                                                        Collection<Domain> domains,
                                                        Set<String> scenarioCodes,
                                                        RefSolTextureGeppa defaultRefSolTextureGeppa,
                                                        RefSolProfondeurIndigo defaultSolDepth,
                                                        RefLocation defaultLocation,
                                                        PriceConverterKeysToRate priceConverterKeysToRate,
                                                        SecurityContext securityContext,
                                                        Map<Domain, Domain> domainToAnonymizeDomaines,
                                                        Collection<IndicatorFilter> indicatorFilters,
                                                        GenericIndicator[] indicators);
    
    protected abstract void computePracticedPerformance(CurrentPerformanceDto performance,
                                                        IndicatorWriter writer,
                                                        Collection<Domain> domains,
                                                        Set<String> scenarioCodes,
                                                        RefSolTextureGeppa defaultRefSolTextureGeppa,
                                                        RefSolProfondeurIndigo defaultSolDepth,
                                                        RefLocation defaultLocation,
                                                        PriceConverterKeysToRate priceConverterKeysToRate,
                                                        SecurityContext securityContext,
                                                        Map<Domain, Domain> domainToAnonymizeDomaines,
                                                        Collection<IndicatorFilter> indicatorFilters,
                                                        GenericIndicator[] indicators);
    
    public void afterAllIndicatorsAreWritten(IndicatorWriter writer, String performanceId, Domain domain) {
        try {
            writer.afterAllIndicatorsAreWritten();
        } catch (Exception e) {
            saveState(performanceId, PerformanceState.FAILED);
            if (LOGGER.isErrorEnabled()) {
                
                String message = String.format(
                        """
                        Exception sur le calcul des performance, domain %s ('%s'), avec l'erreur suivante:
                        %s
                        %s""",
                        domain.getName(),
                        domain.getTopiaId(),
                        e.getMessage(),
                        e.getCause());
                LOGGER.error(message, e);
            }
        }
    }


    
    protected HashMap<Domain, List<GrowingSystem>> getDomainGrowingSystems(Collection<GrowingSystem> allComputedGrowingSystems) {
        HashMap<Domain, List<GrowingSystem>> growingSystemsByDomain = new HashMap<>();
        for (GrowingSystem growingSystem : allComputedGrowingSystems) {
            Domain gsDomain = growingSystem.getGrowingPlan().getDomain();
            List<GrowingSystem> growingSystems = growingSystemsByDomain.computeIfAbsent(gsDomain, k -> new ArrayList<>());
            growingSystems.add(growingSystem);
        }
        return growingSystemsByDomain;
    }
    
    protected List<GrowingSystem> getGrowingSystemsForDomainIds(Collection<Domain> domains, SecurityContext securityContext) {
    
        // filtre commun pour tous les domaines
        final GrowingSystemFilter growingSystemFilter = getPerformanceGrowingSystemFilter();
        
        final Set<String> domainIds = domains.stream().map(TopiaEntity::getTopiaId).collect(Collectors.toSet());
        final NavigationContext navigationContext = new NavigationContext(null, null, domainIds, null, null);
        growingSystemFilter.setNavigationContext(navigationContext);
    
        return growingSystemDao.getFilteredGrowingSystems(growingSystemFilter, securityContext).getElements();
    }
    
    protected GrowingSystemFilter getPerformanceGrowingSystemFilter() {
        GrowingSystemFilter growingSystemFilter = new GrowingSystemFilter();
        growingSystemFilter.setActive(true);
        growingSystemFilter.setAllPageSize();
        return growingSystemFilter;
    }
    
    protected Collection<HarvestingActionValorisation> addInterventionValorisationsToDomainValorisations(Optional<HarvestingAction> optionalHarvestingAction,
                                                                                                 String interventionId,
                                                                                                 String domainId,
                                                                                                 String practicedSystemId) {
        Collection<HarvestingActionValorisation> result = new ArrayList<>();
    
        if (optionalHarvestingAction.isPresent()) {
            Collection<HarvestingActionValorisation> valorisations = CollectionUtils.emptyIfNull(optionalHarvestingAction.get()
                    .getValorisations()).stream().filter(Objects::nonNull).toList();
    
            final CommonService commonService = CommonService.getInstance();
    
            for (HarvestingActionValorisation valorisation : valorisations) {
        
                final int beginMarketingPeriodCampaign = valorisation.getBeginMarketingPeriodCampaign();
                final int endingMarketingPeriodCampaign = valorisation.getEndingMarketingPeriodCampaign();
        
                final boolean isBeginCampaignValid = commonService.isCampaignValid(beginMarketingPeriodCampaign);
                final boolean isEndingCampaignValid = commonService.isCampaignValid(endingMarketingPeriodCampaign);
            
                if (isBeginCampaignValid && isEndingCampaignValid) {
                
                    result.add(valorisation);
                
                } else {
                    if (LOGGER.isWarnEnabled()) {
                        if (domainId != null) {
                            LOGGER.warn(String.format("Pour le domaine '%s' et l'intervention '%s', " +
                                            "la valorisation '%s' a des campagnes marketing non valides: %d -> %d. " +
                                            "Cette valorisation ne sera pas prise en compte dans le calcul de la performance.",
                                    domainId,
                                    interventionId,
                                    valorisation.getTopiaId(),
                                    beginMarketingPeriodCampaign,
                                    endingMarketingPeriodCampaign));
                        } else if (practicedSystemId != null) {
                            LOGGER.warn(String.format("Pour le système synthétisé '%s' et l'intervention '%s', " +
                                            "la valorisation '%s' a des campagnes marketing non valides: %d -> %d. " +
                                            "Cette valorisation ne sera pas prise en compte dans le calcul de la performance.",
                                    practicedSystemId,
                                    interventionId,
                                    valorisation.getTopiaId(),
                                    beginMarketingPeriodCampaign,
                                    endingMarketingPeriodCampaign));
                        }
                    }
                }
            }
        
        }
        return result;
    }
    
    protected void addZoneYealdAverage(Map<Pair<RefDestination, YealdUnit>, List<Double>> plotYealds,
                                       Map<Pair<RefDestination, YealdUnit>, Double> zoneYealAverage) {
        
        addYeald(plotYealds, zoneYealAverage);
    }
    
    public void addYeald(Map<Pair<RefDestination, YealdUnit>, List<Double>> collectionToAddTo,
                         Map<Pair<RefDestination, YealdUnit>, Double> yealdsToAdd) {
        
        for (Map.Entry<Pair<RefDestination, YealdUnit>, Double> yealdAverageForDestinationAndUnit : yealdsToAdd.entrySet()) {
            Pair<RefDestination, YealdUnit> destinationAndUnit = yealdAverageForDestinationAndUnit.getKey();
            List<Double> yealds = collectionToAddTo.computeIfAbsent(destinationAndUnit, k -> new ArrayList<>());
            yealds.add(yealdAverageForDestinationAndUnit.getValue());
        }
    }
    
    @Override
    public Map<PracticedSystem, PracticedSeasonalCropCycle> getPracticedSeasonalCropCyclesByPracticedSystems(List<PracticedSystem> practicedSystems) {
        Map<PracticedSystem, PracticedSeasonalCropCycle> practicedSeasonalCropCyclesByPracticedSystems = new HashMap<>();

        if (!practicedSystems.isEmpty()) {
            Iterable<PracticedSeasonalCropCycle> practicedSeasonalCropCycles = practicedSeasonalCropCycleDao.
                    forPracticedSystemIn(practicedSystems).
                    findAllLazy();
    
            for (PracticedSeasonalCropCycle practicedSeasonalCropCycle : practicedSeasonalCropCycles) {
                PracticedSystem ps = practicedSeasonalCropCycle.getPracticedSystem();
                practicedSeasonalCropCyclesByPracticedSystems.put(ps, practicedSeasonalCropCycle);
            }
        }
        return practicedSeasonalCropCyclesByPracticedSystems;
    }

    public Map<Zone, EffectiveSeasonalCropCycle> getEffectiveSeasonalCropCyclesByZones(Collection<Zone> zones) {
        Map<Zone, EffectiveSeasonalCropCycle> effectiveSeasonalCropCyclesByPracticedSystems = new HashMap<>();

        if (!zones.isEmpty()) {
            Iterable<EffectiveSeasonalCropCycle> effectiveSeasonalCropCycles = effectiveSeasonalCropCycleDao.forZoneIn(zones).findAllLazy();
            for (EffectiveSeasonalCropCycle effectiveSeasonalCropCycle : effectiveSeasonalCropCycles) {
                Zone zone = effectiveSeasonalCropCycle.getZone();
                effectiveSeasonalCropCyclesByPracticedSystems.put(zone, effectiveSeasonalCropCycle);
            }
        }
        return effectiveSeasonalCropCyclesByPracticedSystems;
    }

    @Override
    public Map<PracticedSystem, List<PracticedPerennialCropCycle>> getPracticedPerennialCropCyclesByPracticedSystems(List<PracticedSystem> practicedSystems) {
        Map<PracticedSystem, List<PracticedPerennialCropCycle>> practicedPerennialCropCyclesByPracticedSystems = new HashMap<>();

        if (!practicedSystems.isEmpty()) {
            List<PracticedPerennialCropCycle> practicedPerennialCropCycles = practicedPerennialCropCycleDao.forPracticedSystemIn(practicedSystems).findAll();
    
            for (PracticedPerennialCropCycle practicedPerennialCropCycle : practicedPerennialCropCycles) {
        
                PracticedSystem ps = practicedPerennialCropCycle.getPracticedSystem();
                List<PracticedPerennialCropCycle> practicedPerennialCropCyclesForPracticedSystem = practicedPerennialCropCyclesByPracticedSystems.
                        computeIfAbsent(ps, k -> new ArrayList<>());
        
                practicedPerennialCropCyclesForPracticedSystem.add(practicedPerennialCropCycle);
            }
        }
        return practicedPerennialCropCyclesByPracticedSystems;
    }

    public Map<Zone, List<EffectivePerennialCropCycle>> getEffectivePerennialCropCyclesByZones(Collection<Zone> zones) {
        Map<Zone, List<EffectivePerennialCropCycle>> effectivePerennialCropCyclesByZones = new HashMap<>();

        if (!zones.isEmpty()) {
            List<EffectivePerennialCropCycle> effectivePerennialCropCycles = effectivePerennialCropCyclDao.forZoneIn(zones).findAll();
    
            for (EffectivePerennialCropCycle effectivePerennialCropCycle : effectivePerennialCropCycles) {
        
                Zone zone = effectivePerennialCropCycle.getZone();
                List<EffectivePerennialCropCycle> effectivePerennialCropCyclesForzone = effectivePerennialCropCyclesByZones.
                        computeIfAbsent(
                                zone,
                                k -> new ArrayList<>());
        
                effectivePerennialCropCyclesForzone.add(effectivePerennialCropCycle);
            }
        }
        return effectivePerennialCropCyclesByZones;
    }


    @Override
    public List<PracticedSystem> getPracticedSystemsForGrowingSystems(List<GrowingSystem> growingSystems) {
        return practicedSystemDao.forGrowingSystemIn(growingSystems)
                .addEquals(PracticedSystem.PROPERTY_ACTIVE, true)
                .findAll();
    }

    @Override
    public void deletePerformance(Collection<String> performanceTopiaIds) {
        List<Performance> performances = performanceDao.forTopiaIdIn(performanceTopiaIds).findAll();
        List<Performance> performanceToFiles = performances.stream().
                filter(p -> ExportType.FILE.equals(p.getExportType())).
                collect(Collectors.toList());
        List<PerformanceFile> performanceFiles = performanceFileTopiaDao.forPerformanceIn(performanceToFiles).findAll();
        performanceFileTopiaDao.deleteAll(performanceFiles);
        performanceDao.deleteAll(performanceToFiles);
    
        List<Performance> performancesDb = performances.stream().
                filter(p -> ExportType.DB.equals(p.getExportType())).
                filter(p -> !PerformanceState.GENERATING.equals(p.getComputeStatus())).
                collect(Collectors.toList());
        if (!performancesDb.isEmpty()) {
            authorizationService.checkIsAdmin();
            for (Performance performance : performancesDb) {
                performanceDao.deleteDbPerformanceData(performance.getTopiaId());
            }
            performanceDao.deleteAll(performancesDb);
        }
        getTransaction().commit();
    }

    public List<PracticedCropCycleConnection> getPracticedConnectionsForNodes(Collection<PracticedCropCycleNode> nodes) {
        return practicedCropCycleConnectionDao.forTargetIn(nodes).setOrderByArguments(
                PracticedCropCycleConnection.PROPERTY_TARGET + "." + PracticedCropCycleNode.PROPERTY_RANK).findAll();
    }
    
    public List<PracticedIntervention> getPracticedInterventionForConnections(List<PracticedCropCycleConnection> cropCycleConnections) {
        
        return practicedInterventionDao.forPracticedCropCycleConnectionIn(cropCycleConnections)
                .setOrderByArguments(PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION + "." +
                        PracticedCropCycleConnection.PROPERTY_TARGET + "." + PracticedCropCycleNode.PROPERTY_RANK).findAll();
    }
    
    public List<PracticedIntervention> getPracticedInterventionForPracticedPerennialCropCycles(
            List<PracticedPerennialCropCycle> practicedPerennialCropCycles) {
        
        List<PracticedCropCyclePhase> phases = new ArrayList<>();
        for (PracticedPerennialCropCycle practicedPerennialCropCycle : practicedPerennialCropCycles) {
            phases.addAll(practicedPerennialCropCycle.getCropCyclePhases());
        }
        
        return practicedInterventionDao.forPracticedCropCyclePhaseIn(phases).findAll();
    }
    
    public List<AbstractAction> getPracticedActionsForCropInterventions(List<PracticedIntervention> cropInterventions) {
        return actionDao.forPracticedInterventionIn(cropInterventions).findAll();
    }

    public List<AbstractAction> getEffectiveActionsForCropInterventions(List<EffectiveIntervention> cropInterventions) {
        return actionDao.forEffectiveInterventionIn(cropInterventions).findAll();
    }
    
    public Map<RefActaTraitementsProduit, ReferenceDoseDTO> getLegacyDoseForActaProducts(Set<RefActaTraitementsProduit> actaTraitementProducts,
                                                                                         Set<RefEspece> refEspeces) {
        Map<RefActaTraitementsProduit, ReferenceDoseDTO> result = new HashMap<>();
        for (RefActaTraitementsProduit actaProduct : actaTraitementProducts) {
            ReferenceDoseDTO referenceDose = referentialService.computeReferenceDoseForGivenProductAndActaSpecies(actaProduct, refEspeces);
            result.put(actaProduct, referenceDose);
        }
        return result;
    }
    
    public Map<PracticedSystem, PracticedPlot> getPracticedPlotsByPracticedSystems(Collection<PracticedSystem> practicedSystems) {
        Map<PracticedSystem, PracticedPlot> result = new HashMap<>();
        List<PracticedPlot> practicedPlots = practicedSystems.stream()
                .map(practicedSystem -> practicedPlotTopiaDao.forPracticedSystemContains(practicedSystem).findAll())
                .flatMap(Collection::stream)
                .toList();
        for (PracticedPlot practicedPlot : practicedPlots) {
            for (PracticedSystem practicedSystem : practicedPlot.getPracticedSystem()) {
                result.put(practicedSystem, practicedPlot);
            }
        }
        return result;
    }
    
    public Map<RefActaTraitementsProduit, List<RefActaSubstanceActive>> getAtciveSubstancesByProductsForPhytoProductById(
            Map<String, RefActaTraitementsProduit> productByIds) {
        
        Map<RefActaTraitementsProduit, List<RefActaSubstanceActive>> result = new HashMap<>();
        
        if (!productByIds.isEmpty()) {
            List<RefActaSubstanceActive> substanceActives = actaSubstanceActiveDao.
                    forId_produitIn(productByIds.keySet()).
                    addEquals(RefActaSubstanceActive.PROPERTY_ACTIVE, true)
                    .findAll();
            
            for (RefActaSubstanceActive substanceActive : substanceActives) {
                RefActaTraitementsProduit product = productByIds.get(substanceActive.getId_produit());
                List<RefActaSubstanceActive> activeSubstances = result.computeIfAbsent(product, k -> new ArrayList<>());
                activeSubstances.add(substanceActive);
            }
        }

        return result;
    }

    public Map<String, MultiValuedMap<String, RefHarvestingPrice>> getHarvestingScenarioPricesByKeyForGrowingSystemByPracticedSystemIds(
            GrowingSystem growingSystem,
            Set<String> scenarioCodes) {
        
        Map<String, MultiValuedMap<String, RefHarvestingPrice>> scenarioPricesByKeysByPracticedSystemIds;
        
        if (CollectionUtils.isNotEmpty(scenarioCodes)) {
            Map<String, List<RefHarvestingPrice>> refHarvestingPrices = refHarvestingPriceDao.getAllScenarioPricesForGrowingSystemByPracticedSystemIds(
                    growingSystem,
                    scenarioCodes
            );
            
            scenarioPricesByKeysByPracticedSystemIds = getRefHarvestingPricesByValorisationKey(refHarvestingPrices);
        } else {
            scenarioPricesByKeysByPracticedSystemIds = new HashMap<>();
        }
        
        return scenarioPricesByKeysByPracticedSystemIds;
    }
    
    public MultiValuedMap<String, RefHarvestingPrice> getRefHarvestingPricesByValorisationKeyForDomain(Domain domain, Set<String> scenarioCodes) {
        
        MultiValuedMap<String, RefHarvestingPrice> refHarvestingPricesByValorisationKey;
        
        List<RefHarvestingPrice> refHarvestingPrices = CollectionUtils.isNotEmpty(scenarioCodes) ?
                refHarvestingPriceDao.getAllRefPricesForDomainAndScenarios(domain, scenarioCodes) : new ArrayList<>();
        
        refHarvestingPricesByValorisationKey = getRefHarvestingPricesByValorisationKeysForDomain(domain, refHarvestingPrices);
        
        return refHarvestingPricesByValorisationKey;
    }
    
    protected MultiValuedMap<String, RefHarvestingPrice> getRefHarvestingPricesByValorisationKeysForDomain(Domain domain,
                                                                                                           List<RefHarvestingPrice> refHarvestingPrices) {
        Map<String, List<RefHarvestingPrice>> refHarvestingPricesByDomains = new HashMap<>();
        refHarvestingPricesByDomains.put(domain.getTopiaId(), refHarvestingPrices);
        Map<String, MultiValuedMap<String, RefHarvestingPrice>> refHarvestingPricesByValorisationKeysByDomains =
                getRefHarvestingPricesByValorisationKey(refHarvestingPricesByDomains);
        
        return refHarvestingPricesByValorisationKeysByDomains.get(domain.getTopiaId());
    }
    
    protected Map<String, MultiValuedMap<String, RefHarvestingPrice>> getRefHarvestingPricesByValorisationKey(
            Map<String, List<RefHarvestingPrice>> refHarvestingPricesByPracticedSystemIdsOrDomainIds) {
        
        Map<String, MultiValuedMap<String, RefHarvestingPrice>> allRefHarvestingPricesByKeysByPracticedSystemIdsOrDomainIds = new HashMap<>();
        
        for (Map.Entry<String, List<RefHarvestingPrice>> refHarvestingPricesByPracticedSystemIdOrDomainId :
                refHarvestingPricesByPracticedSystemIdsOrDomainIds.entrySet()) {
            
            String practicedSystemOrDomainId = refHarvestingPricesByPracticedSystemIdOrDomainId.getKey();
            
            List<RefHarvestingPrice> refHarvestingPrices = refHarvestingPricesByPracticedSystemIdOrDomainId.getValue();
            MultiValuedMap<String, RefHarvestingPrice> allRefHarvestingPricesByKey = new HashSetValuedHashMap<>();
            allRefHarvestingPricesByKeysByPracticedSystemIdsOrDomainIds.put(practicedSystemOrDomainId, allRefHarvestingPricesByKey);
        
            for (RefHarvestingPrice refHarvestingPrice : refHarvestingPrices) {
                String codeEspeceBotanique = refHarvestingPrice.getCode_espece_botanique();
                String codeQualifiantAee = refHarvestingPrice.getCode_qualifiant_AEE();
                String codeDestinationA = refHarvestingPrice.getCode_destination_A();
                boolean isOrganic = refHarvestingPrice.isOrganic();
                String valorisationKey = getValorisationKey(codeEspeceBotanique, codeQualifiantAee, codeDestinationA, isOrganic);
                allRefHarvestingPricesByKey.put(valorisationKey, refHarvestingPrice);
            }
        }

        return allRefHarvestingPricesByKeysByPracticedSystemIdsOrDomainIds;
    }

    public List<RefInputUnitPriceUnitConverter> getPriceUnitConvertersForPhytoProductUnits(Set<PhytoProductUnit> phytoProductUnits) {

        return refInputUnitPriceUnitConverterDao.forPhytoProductUnitIn(phytoProductUnits)
                .addEquals(RefInputUnitPriceUnitConverter.PROPERTY_ACTIVE, true).findAll();
    }

    public List<RefInputUnitPriceUnitConverter> getPriceUnitConvertersForMineralProductUnit(Set<MineralProductUnit> productUnits) {

        return refInputUnitPriceUnitConverterDao.forMineralProductUnitIn(productUnits)
                .addEquals(RefInputUnitPriceUnitConverter.PROPERTY_ACTIVE, true).findAll();
    }

    public List<RefInputUnitPriceUnitConverter> getPriceUnitConvertersForOrganicProductUnit(Set<OrganicProductUnit> productUnits) {

        return refInputUnitPriceUnitConverterDao.forOrganicProductUnitIn(productUnits)
                .addEquals(RefInputUnitPriceUnitConverter.PROPERTY_ACTIVE, true).findAll();
    }

    public List<RefInputUnitPriceUnitConverter> getPriceUnitConvertersForSeedPlantUnit(Set<SeedPlantUnit> seedPlantUnits) {

        return refInputUnitPriceUnitConverterDao.forSeedPlantUnitIn(seedPlantUnits)
                .addEquals(RefInputUnitPriceUnitConverter.PROPERTY_ACTIVE, true).findAll();
    }

    public List<RefInputUnitPriceUnitConverter> getPriceUnitConvertersForSubstrateUnit(Set<SubstrateInputUnit> substrateInputUnits) {
    
        return refInputUnitPriceUnitConverterDao.forSubstrateInputUnitIn(substrateInputUnits)
                .addEquals(RefInputUnitPriceUnitConverter.PROPERTY_ACTIVE, true).findAll();
    }
    
    public List<RefInputUnitPriceUnitConverter> getPriceUnitConvertersForPotUnit(Set<PotInputUnit> potInputUnits) {
        
        return refInputUnitPriceUnitConverterDao.forPotInputUnitIn(potInputUnits)
                .addEquals(RefInputUnitPriceUnitConverter.PROPERTY_ACTIVE, true).findAll();
    }
    
    public Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> getRefActaTraitementsProduitsCategFor(
            Set<RefActaTraitementsProduit> refActaTraitementsProduits) {
        
        Map<Integer, List<RefActaTraitementsProduit>> traitementByIdTraitements = new HashMap<>();
        for (RefActaTraitementsProduit refActaTraitementsProduit : refActaTraitementsProduits) {
            Integer id_traitement = refActaTraitementsProduit.getId_traitement();
            List<RefActaTraitementsProduit> refActaTraitements = traitementByIdTraitements.computeIfAbsent(id_traitement, k -> new ArrayList<>());
            refActaTraitements.add(refActaTraitementsProduit);
        }
        List<RefActaTraitementsProduitsCateg> traitementProduitCategs = refActaTraitementsProduitsCategDao.
                forId_traitementIn(traitementByIdTraitements.keySet()).
                findAll();
        
        Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> result = new HashMap<>();
        for (RefActaTraitementsProduitsCateg traitementProduitCateg : traitementProduitCategs) {
            
            final List<RefActaTraitementsProduit> refActaTraitementsProduits1 = traitementByIdTraitements.get(traitementProduitCateg.getId_traitement());
            for (RefActaTraitementsProduit refActaTraitementsProduit : refActaTraitementsProduits1) {
                result.put(refActaTraitementsProduit, traitementProduitCateg);
            }
        }
        return result;
    }

    public List<EffectiveIntervention> getEffectiveInterventionsForEffectivePerennialCropCycles(List<EffectivePerennialCropCycle> perennialCropCycles) {

        List<EffectiveCropCyclePhase> phases = perennialCropCycles.stream().map(EffectivePerennialCropCycle::getPhase).collect(Collectors.toList());

        return effectiveInterventionDao.forEffectiveCropCyclePhaseIn(phases).findAll();
    }


    public List<EffectiveIntervention> getEffectiveInterventionForNodes(Collection<EffectiveCropCycleNode> nodes) {
    
        return effectiveInterventionDao.forEffectiveCropCycleNodeIn(nodes)
                .setOrderByArguments(EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_NODE + "." +
                        EffectiveCropCycleNode.PROPERTY_RANK).findAll();
    }
    
    public List<EffectiveCropCycleConnection> getEffectiveConnectionsForNodes(Set<EffectiveCropCycleNode> nodeWithIntermediateInterventions) {
        return effectiveCropCycleConnectionDao.forTargetIn(nodeWithIntermediateInterventions).findAll();
    }

    public Map<Pair<RefDestination, YealdUnit>, Double> computeYealdAverageByDestinationAndUnit(
            Map<Pair<RefDestination, YealdUnit>, List<Double>> allResultForDestinationsAndUnits) {
        
        Map<Pair<RefDestination, YealdUnit>, Double> sumAverageResultForDestinationsAndUnits = new HashMap<>();
        for (Pair<RefDestination, YealdUnit> destinationAndUnit : allResultForDestinationsAndUnits.keySet()) {
            List<Double> values = allResultForDestinationsAndUnits.get(destinationAndUnit);
            Double averageValue = values.stream()
                    .filter(Objects::nonNull)
                    .mapToDouble(Double::doubleValue)
                    .sum();
            sumAverageResultForDestinationsAndUnits.put(destinationAndUnit, averageValue);
        }
        
        return sumAverageResultForDestinationsAndUnits;
    }
    public Map<Pair<RefDestination, YealdUnit>, Double> extractYealdAverage(
            Optional<HarvestingAction> optionalHarvestingAction,
            CroppingPlanEntry croppingPlanEntry) {
        
        Map<Pair<RefDestination, YealdUnit>, Double> yealdAveragesByDestinations = new HashMap<>();
        
        if (optionalHarvestingAction.isPresent()) {
    
            boolean averageYealdsInsteadOfSum = croppingPlanEntry.isCroppingPlanSpeciesNotEmpty()
                    && croppingPlanEntry.sizeCroppingPlanSpecies() > 1
                    && !croppingPlanEntry.isMixSpecies()
                    && !croppingPlanEntry.isMixVariety();
            
            Collection<HarvestingActionValorisation> valorisations = optionalHarvestingAction.get().getValorisations();

            Set<String> actionSpeciesCodes = valorisations.stream()
                    .filter(Objects::nonNull)
                    .map(HarvestingActionValorisation::getSpeciesCode)
                    .collect(Collectors.toSet());

            double totalAreaRatio;
            Map<String, Integer> speciesAreasByCode;

            if (averageYealdsInsteadOfSum && CollectionUtils.isNotEmpty(croppingPlanEntry.getCroppingPlanSpecies())) {

                speciesAreasByCode = croppingPlanEntry.getCroppingPlanSpecies().stream()
                        .filter(species -> actionSpeciesCodes.contains(species.getCode()))
                        .collect(Collectors.toMap(CroppingPlanSpecies::getCode, croppingPlanSpecies -> {
                            Integer speciesArea = croppingPlanSpecies.getSpeciesArea();
                            if (speciesArea == null) {
                                speciesArea = 100 / actionSpeciesCodes.size();
                            }
                            return speciesArea;
                        }));
                // totalAreaRatio : uniquement sur les espèces concernées de l'intervention
                //   souvent dans le cas du VITI les utilisateurs ont déclaré une culture composée de plusieurs espèces
                //   cultivée sur des parcelles différentes,
                //   dans ce cas il faut considérer chaque espèce comme une culture propre.
                // pour résumer:
                // il ne faut pas calculer le rendement par rapport à la part de l'espèce dans la culture
                // mais par rapport à la part de l'espèce dans les espèces concernées de l'intervention
                // le calcul dans ce cas est la somme des moyennes des rendements pondérées par la surface de l'espèce
                int areaSum = speciesAreasByCode.values().stream()
                        .mapToInt(a -> a)
                        .sum();
                totalAreaRatio = areaSum == 0 ? 0 : (double) 100 / areaSum;

            } else {
                totalAreaRatio = 1;
                speciesAreasByCode = new HashMap<>();
            }

            Map<Pair<RefDestination, YealdUnit>, List<Double>> yealdAveragesByUnits = new HashMap<>();
            for (HarvestingActionValorisation valorisation : valorisations) {
                if (valorisation != null) {
                    final Pair<RefDestination, YealdUnit> yealdAverageForDestination = Pair.of(valorisation.getDestination(), valorisation.getYealdUnit());
                    List<Double> yealdAverages = yealdAveragesByUnits.computeIfAbsent(
                            yealdAverageForDestination,
                            k -> new ArrayList<>()
                    );
                    double yealdAverage = valorisation.getYealdAverage();
                    if (averageYealdsInsteadOfSum && speciesAreasByCode.containsKey(valorisation.getSpeciesCode())) {
                        Integer speciesArea = speciesAreasByCode.get(valorisation.getSpeciesCode());
                        yealdAverage = yealdAverage * speciesArea * totalAreaRatio / 100;
                    }
                    yealdAverages.add(yealdAverage);
                }
            }
            for (Map.Entry<Pair<RefDestination, YealdUnit>, List<Double>> yealdAveragesForDestinationUnit : yealdAveragesByUnits.entrySet()) {
                double averageForUnit = yealdAveragesForDestinationUnit.getValue().stream()
                        .mapToDouble(Double::doubleValue)
                        .sum();
                yealdAveragesByDestinations.put(yealdAveragesForDestinationUnit.getKey(), averageForUnit);
            }
        }
        return yealdAveragesByDestinations;
    }
    
    public Collection<EquipmentUsageRange> getEquipmentUsageRanges(Iterable<ToolsCoupling> toolsCouplings) {
        
        Set<RefMateriel> allMateriels = new HashSet<>();
        for (ToolsCoupling toolsCoupling : toolsCouplings) {
            if (!toolsCoupling.isManualIntervention()) {
                final Equipment tractor = toolsCoupling.getTractor();
                if (tractor != null) {
                    allMateriels.add(tractor.getRefMateriel());
                }
                Collection<Equipment> equipments = toolsCoupling.getEquipments();
                if (CollectionUtils.isNotEmpty(equipments)) {
                    allMateriels.addAll(equipments.stream().map(Equipment::getRefMateriel).toList());
                }
            }
        }
        
        return referentialService.getEquipmentlUsageRangeForEquipments(allMateriels);
    }
    
    public abstract List<IndicatorFilter> getAllIndicatorFilters();
    
    @Override
    public List<PerformanceStatistics> getDbPerformancesStatistics(String performanceId) {
    
        authorizationService.checkIsAdmin();
    
        BusinessTasksManager businessTasksManager = getBusinessTaskManager();
    
        // if there is a running performance we can compute task done throw the businessTasksManager
        final ImmutableList<ScheduledTask> runningAndPendingTasks = businessTasksManager.getRunningAndPendingTasks();
    
        // if performanceId is given the compute statistic only for this performance otherwise comput for all
        final List<PerformanceStatistics> dbEffectivePerformancesStatistics = performanceDao.loadDbPerformancesStatistics(performanceId);
    
        Map<String, PerformanceStatistics> result0 = new HashMap<>();
        for (PerformanceStatistics dbPerformancesStatistic : dbEffectivePerformancesStatistics) {
            final String aPerformanceId = dbPerformancesStatistic.getPerformanceId();
            long nbTaskToDo = runningAndPendingTasks.stream()
                    .map(ScheduledTask::getTask)
                    .filter(task -> task instanceof PerformanceTask && ((PerformanceTask) task).getPerformanceId().equals(aPerformanceId))
                    .count();
            dbPerformancesStatistic.setNbTaskPerformed(dbPerformancesStatistic.getNbTotalTask() - nbTaskToDo);
            result0.put(dbPerformancesStatistic.getPerformanceId(), dbPerformancesStatistic);
        }
    
        return new ArrayList<>(result0.values());
    }
}
