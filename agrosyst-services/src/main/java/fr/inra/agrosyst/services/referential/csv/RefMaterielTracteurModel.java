package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTractionImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

import java.util.List;

/**
 * Tracteur import model.
 * 
 * Columns:
 * <ul>
 * <li>Type materiel 1
 * <li>type materiel 2
 * <li>Type materiel 3
 * <li>Type materiel 4
 * <li>idtypemateriel
 * <li>idsoustypemateriel
 * <li>commentaire sur materiel
 * <li>Millésime
 * <li>codetype
 * <li>Coderef
 * <li>prix neuf € unité
 * <li>prix moyen achat
 * <li>unité
 * <li>unité / an
 * <li>charges fixes annuelle unité
 * <li>charges fixes €/an
 * <li>charges fixes €/unité de volume de travail annuel unité
 * <li>charges fixes €/unité de volume de travail annuel
 * <li>Réparations unité
 * <li>Réparations €/unité de travail annuel
 * <li>coût total unité
 * <li>coût total AVEC carburant € / unité de travail annuel
 * <li>carburant cout unité
 * <li>carburant €/unité de travail
 * <li>lubrifiant cout unité
 * <li>lubrifiant €/unité de travail
 * <li>pneus cout unité
 * <li>pneus €/unité de travail
 * <li>puissance ch ISO unité
 * <li>puissance ch ISO
 * <li>volume carter huile BV unité
 * <li>volume carter huile BV
 * <li>volume carter huile moteur unité
 * <li>volume carter huile moteur
 * <li>pneus AV taille
 * <li>pneus AV prix €
 * <li>pneus AV prix €
 * <li>pneus AV durée vie unité
 * <li>pneus AV durée vie
 * <li>pneus AR taille
 * <li>pneus AR prix unité
 * <li>pneus AR prix
 * <li>pneus AR durée vie unité
 * <li>pneus AR durée vie
 * <li>donnees amortissement 1
 * <li>donnees amortissement 2
 * <li>données taux de charge moteur
 * </ul>
 * 
 * @author Eric Chatellier
 */
public class RefMaterielTracteurModel extends AbstractAgrosystModel<RefMaterielTraction> implements ExportModel<RefMaterielTraction> {

    public RefMaterielTracteurModel() {
        super(CSV_SEPARATOR);
    }

    @Override
    public void pushCsvHeaderNames(List<String> headerNames) {
        super.pushCsvHeaderNames(headerNames);

        newMandatoryColumn("Type materiel 1", RefMaterielTraction.PROPERTY_TYPE_MATERIEL1, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("Type materiel 2", RefMaterielTraction.PROPERTY_TYPE_MATERIEL2, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("Type materiel 3", RefMaterielTraction.PROPERTY_TYPE_MATERIEL3, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("Type materiel 4", RefMaterielTraction.PROPERTY_TYPE_MATERIEL4, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("idtypemateriel", RefMaterielTraction.PROPERTY_IDTYPEMATERIEL);
        newMandatoryColumn("idsoustypemateriel", RefMaterielTraction.PROPERTY_IDSOUSTYPEMATERIEL);
        newMandatoryColumn("commentaire sur materiel", RefMaterielTraction.PROPERTY_COMMENTAIRE_SUR_MATERIEL);
        newMandatoryColumn("Millésime", RefMaterielTraction.PROPERTY_MILLESIME, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("codetype", RefMaterielTraction.PROPERTY_CODETYPE);
        newMandatoryColumn("Coderef", RefMaterielTraction.PROPERTY_CODE_REF);
        newMandatoryColumn("prix neuf € unité", RefMaterielTraction.PROPERTY_PRIX_NEUF_UNITE);
        newMandatoryColumn("prix moyen achat", RefMaterielTraction.PROPERTY_PRIX_MOYEN_ACHAT, DOUBLE_PARSER);
        newMandatoryColumn("unité", RefMaterielTraction.PROPERTY_UNITE);
        newMandatoryColumn("unité / an", RefMaterielTraction.PROPERTY_UNITE_PAR_AN, DOUBLE_PARSER);
        newMandatoryColumn("charges fixes annuelle unité", RefMaterielTraction.PROPERTY_CHARGES_FIXES_ANNUELLE_UNITE);
        newMandatoryColumn("charges fixes €/an", RefMaterielTraction.PROPERTY_CHARGES_FIXES_PAR_AN, DOUBLE_PARSER);
        newMandatoryColumn("charges fixes €/unité de volume de travail annuel unité", RefMaterielTraction.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL_UNITE);
        newMandatoryColumn("charges fixes €/unité de volume de travail annuel", RefMaterielTraction.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL, DOUBLE_PARSER);
        newMandatoryColumn("Réparations unité", RefMaterielTraction.PROPERTY_REPARATIONS_UNITE);
        newMandatoryColumn("Réparations €/unité de travail annuel", RefMaterielTraction.PROPERTY_REPARATIONS_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_PARSER);
        newMandatoryColumn("coût total unité", RefMaterielTraction.PROPERTY_COUT_TOTAL_UNITE);
        newMandatoryColumn("coût total AVEC CARBURANT € / unité de travail annuel", RefMaterielTraction.PROPERTY_COUT_TOTAL_AVEC_CARBURANT_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_PARSER);
        newMandatoryColumn("coût total SANS CARBURANT € / unité de travail annuel", RefMaterielTraction.PROPERTY_COUT_TOTAL_SANS_CARBURANT_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_PARSER);newMandatoryColumn("carburant cout unité", RefMaterielTraction.PROPERTY_CARBURANT_COUT_UNITE);
        newMandatoryColumn("carburant €/unité de travail", RefMaterielTraction.PROPERTY_CARBURANT_PAR_UNITE_DE_TRAVAIL, DOUBLE_PARSER);
        newMandatoryColumn("lubrifiant cout unité", RefMaterielTraction.PROPERTY_LUBRIFIANT_COUT_UNITE);
        newMandatoryColumn("lubrifiant €/unité de travail", RefMaterielTraction.PROPERTY_LUBRIFIANT_PAR_UNITE_DE_TRAVAIL, DOUBLE_PARSER);
        newMandatoryColumn("pneus cout unité", RefMaterielTraction.PROPERTY_PNEUS_COUT_UNITE);
        newMandatoryColumn("pneus €/unité de travail", RefMaterielTraction.PROPERTY_PNEUS_PAR_UNITE_DE_TRAVAIL, DOUBLE_PARSER);
        newMandatoryColumn("puissance ch ISO unité", RefMaterielTraction.PROPERTY_PUISSANCE_CH_ISO_UNITE);
        newMandatoryColumn("puissance ch ISO", RefMaterielTraction.PROPERTY_PUISSANCE_CH_ISO, DOUBLE_PARSER);
        newMandatoryColumn("volume carter huile BV unité", RefMaterielTraction.PROPERTY_VOLUME_CARTER_HUILE_BVUNITE);
        newMandatoryColumn("volume carter huile BV", RefMaterielTraction.PROPERTY_VOLUME_CARTER_HUILE_BV, DOUBLE_PARSER);
        newMandatoryColumn("volume carter huile moteur unité", RefMaterielTraction.PROPERTY_VOLUME_CARTER_HUILE_MOTEUR_UNITE);
        newMandatoryColumn("volume carter huile moteur", RefMaterielTraction.PROPERTY_VOLUME_CARTER_HUILE_MOTEUR, DOUBLE_PARSER);
        newMandatoryColumn("pneus AV taille", RefMaterielTraction.PROPERTY_PNEUS_AVTAILLE);
        newMandatoryColumn("pneus AV prix € unité", RefMaterielTraction.PROPERTY_PNEUS_AVPRIX_UNITE);
        newMandatoryColumn("pneus AV prix €", RefMaterielTraction.PROPERTY_PNEUS_AVPRIX, DOUBLE_PARSER);
        newMandatoryColumn("pneus AV durée vie unité", RefMaterielTraction.PROPERTY_PNEUS_AVDUREE_VIE_UNITE);
        newMandatoryColumn("pneus AV durée vie", RefMaterielTraction.PROPERTY_PNEUS_AVDUREE_VIE, DOUBLE_PARSER);
        newMandatoryColumn("pneus AR taille", RefMaterielTraction.PROPERTY_PNEUS_ARTAILLE);
        newMandatoryColumn("pneus AR prix unité", RefMaterielTraction.PROPERTY_PNEUS_ARPRIX_UNITE);
        newMandatoryColumn("pneus AR prix", RefMaterielTraction.PROPERTY_PNEUS_ARPRIX, DOUBLE_PARSER);
        newMandatoryColumn("pneus AR durée vie unité", RefMaterielTraction.PROPERTY_PNEUS_ARDUREE_VIE_UNITE);
        newMandatoryColumn("pneus AR durée vie", RefMaterielTraction.PROPERTY_PNEUS_ARDUREE_VIE, DOUBLE_PARSER);
        newMandatoryColumn("donnees amortissement 1", RefMaterielTraction.PROPERTY_DONNEES_AMORTISSEMENT1, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("donnees amortissement 2", RefMaterielTraction.PROPERTY_DONNEES_AMORTISSEMENT2, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("données taux de charge moteur", RefMaterielTraction.PROPERTY_DONNEES_TAUX_DE_CHARGE_MOTEUR, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("code materiel GES'TIM", RefMaterielTraction.PROPERTY_CODE_MATERIEL__GESTIM);
        newMandatoryColumn("masse (kg)", RefMaterielTraction.PROPERTY_MASSE, DOUBLE_PARSER);
        newMandatoryColumn("durée de vie théorique -obsolescence constructeur- (années)", RefMaterielTraction.PROPERTY_DUREE_VIE_THEORIQUE, INT_PARSER);
        newMandatoryColumn("Code EDI", RefMaterielTraction.PROPERTY_CODE_EDI);
        newMandatoryColumn("source", RefMaterielTraction.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefMaterielTraction.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefMaterielTraction, Object>> getColumnsForExport() {
        ModelBuilder<RefMaterielTraction> modelBuilder = new ModelBuilder<>();
        
        modelBuilder.newColumnForExport("Type materiel 1", RefMaterielTraction.PROPERTY_TYPE_MATERIEL1);
        modelBuilder.newColumnForExport("Type materiel 2", RefMaterielTraction.PROPERTY_TYPE_MATERIEL2);
        modelBuilder.newColumnForExport("Type materiel 3", RefMaterielTraction.PROPERTY_TYPE_MATERIEL3);
        modelBuilder.newColumnForExport("Type materiel 4", RefMaterielTraction.PROPERTY_TYPE_MATERIEL4);
        modelBuilder.newColumnForExport("idtypemateriel", RefMaterielTraction.PROPERTY_IDTYPEMATERIEL);
        modelBuilder.newColumnForExport("idsoustypemateriel", RefMaterielTraction.PROPERTY_IDSOUSTYPEMATERIEL);
        modelBuilder.newColumnForExport("commentaire sur materiel", RefMaterielTraction.PROPERTY_COMMENTAIRE_SUR_MATERIEL);
        modelBuilder.newColumnForExport("Millésime", RefMaterielTraction.PROPERTY_MILLESIME, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("codetype", RefMaterielTraction.PROPERTY_CODETYPE);
        modelBuilder.newColumnForExport("Coderef", RefMaterielTraction.PROPERTY_CODE_REF);
        modelBuilder.newColumnForExport("prix neuf € unité", RefMaterielTraction.PROPERTY_PRIX_NEUF_UNITE);
        modelBuilder.newColumnForExport("prix moyen achat", RefMaterielTraction.PROPERTY_PRIX_MOYEN_ACHAT, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("unité", RefMaterielTraction.PROPERTY_UNITE);
        modelBuilder.newColumnForExport("unité / an", RefMaterielTraction.PROPERTY_UNITE_PAR_AN, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("charges fixes annuelle unité", RefMaterielTraction.PROPERTY_CHARGES_FIXES_ANNUELLE_UNITE);
        modelBuilder.newColumnForExport("charges fixes €/an", RefMaterielTraction.PROPERTY_CHARGES_FIXES_PAR_AN, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("charges fixes €/unité de volume de travail annuel unité", RefMaterielTraction.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL_UNITE);
        modelBuilder.newColumnForExport("charges fixes €/unité de volume de travail annuel", RefMaterielTraction.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Réparations unité", RefMaterielTraction.PROPERTY_REPARATIONS_UNITE);
        modelBuilder.newColumnForExport("Réparations €/unité de travail annuel", RefMaterielTraction.PROPERTY_REPARATIONS_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("coût total unité", RefMaterielTraction.PROPERTY_COUT_TOTAL_UNITE);
        modelBuilder.newColumnForExport("coût total AVEC CARBURANT € / unité de travail annuel", RefMaterielTraction.PROPERTY_COUT_TOTAL_AVEC_CARBURANT_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("coût total SANS CARBURANT € / unité de travail annuel", RefMaterielTraction.PROPERTY_COUT_TOTAL_SANS_CARBURANT_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("carburant cout unité", RefMaterielTraction.PROPERTY_CARBURANT_COUT_UNITE);
        modelBuilder.newColumnForExport("carburant €/unité de travail", RefMaterielTraction.PROPERTY_CARBURANT_PAR_UNITE_DE_TRAVAIL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("lubrifiant cout unité", RefMaterielTraction.PROPERTY_LUBRIFIANT_COUT_UNITE);
        modelBuilder.newColumnForExport("lubrifiant €/unité de travail", RefMaterielTraction.PROPERTY_LUBRIFIANT_PAR_UNITE_DE_TRAVAIL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("pneus cout unité", RefMaterielTraction.PROPERTY_PNEUS_COUT_UNITE);
        modelBuilder.newColumnForExport("pneus €/unité de travail", RefMaterielTraction.PROPERTY_PNEUS_PAR_UNITE_DE_TRAVAIL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("puissance ch ISO unité", RefMaterielTraction.PROPERTY_PUISSANCE_CH_ISO_UNITE);
        modelBuilder.newColumnForExport("puissance ch ISO", RefMaterielTraction.PROPERTY_PUISSANCE_CH_ISO, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("volume carter huile BV unité", RefMaterielTraction.PROPERTY_VOLUME_CARTER_HUILE_BVUNITE);
        modelBuilder.newColumnForExport("volume carter huile BV", RefMaterielTraction.PROPERTY_VOLUME_CARTER_HUILE_BV, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("volume carter huile moteur unité", RefMaterielTraction.PROPERTY_VOLUME_CARTER_HUILE_MOTEUR_UNITE);
        modelBuilder.newColumnForExport("volume carter huile moteur", RefMaterielTraction.PROPERTY_VOLUME_CARTER_HUILE_MOTEUR, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("pneus AV taille", RefMaterielTraction.PROPERTY_PNEUS_AVTAILLE);
        modelBuilder.newColumnForExport("pneus AV prix € unité", RefMaterielTraction.PROPERTY_PNEUS_AVPRIX_UNITE);
        modelBuilder.newColumnForExport("pneus AV prix €", RefMaterielTraction.PROPERTY_PNEUS_AVPRIX, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("pneus AV durée vie unité", RefMaterielTraction.PROPERTY_PNEUS_AVDUREE_VIE_UNITE);
        modelBuilder.newColumnForExport("pneus AV durée vie", RefMaterielTraction.PROPERTY_PNEUS_AVDUREE_VIE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("pneus AR taille", RefMaterielTraction.PROPERTY_PNEUS_ARTAILLE);
        modelBuilder.newColumnForExport("pneus AR prix unité", RefMaterielTraction.PROPERTY_PNEUS_ARPRIX_UNITE);
        modelBuilder.newColumnForExport("pneus AR prix", RefMaterielTraction.PROPERTY_PNEUS_ARPRIX, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("pneus AR durée vie unité", RefMaterielTraction.PROPERTY_PNEUS_ARDUREE_VIE_UNITE);
        modelBuilder.newColumnForExport("pneus AR durée vie", RefMaterielTraction.PROPERTY_PNEUS_ARDUREE_VIE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("donnees amortissement 1", RefMaterielTraction.PROPERTY_DONNEES_AMORTISSEMENT1, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("donnees amortissement 2", RefMaterielTraction.PROPERTY_DONNEES_AMORTISSEMENT2, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("données taux de charge moteur", RefMaterielTraction.PROPERTY_DONNEES_TAUX_DE_CHARGE_MOTEUR, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("code materiel GES'TIM", RefMaterielTraction.PROPERTY_CODE_MATERIEL__GESTIM);
        modelBuilder.newColumnForExport("masse (kg)", RefMaterielTraction.PROPERTY_MASSE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("durée de vie théorique -obsolescence constructeur- (années)", RefMaterielTraction.PROPERTY_DUREE_VIE_THEORIQUE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Code EDI", RefMaterielTraction.PROPERTY_CODE_EDI);
        modelBuilder.newColumnForExport("source", RefMaterielTraction.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefMaterielTraction.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefMaterielTraction newEmptyInstance() {
        RefMaterielTraction refMaterielTraction = new RefMaterielTractionImpl();
        refMaterielTraction.setActive(true);
        return refMaterielTraction;
    }

}
