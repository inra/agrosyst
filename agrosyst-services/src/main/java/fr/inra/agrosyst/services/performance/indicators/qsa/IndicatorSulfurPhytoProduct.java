package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.MultiValuedMap;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

/**
 * #12314
 * à toutes les échelles, on calcule un indicateur sur le même principe que QSA totale, en ne prenant en compte que les SA pour lesquelles la colonne "soufre" prend la valeur "1"
 */
public class IndicatorSulfurPhytoProduct extends IndicatorSubstancesCandidateToSubstitution {

    private static final String COLUMN_NAME = "qsa_soufre_phyto";
    private static final String COLUMN_NAME_HTS = "qsa_soufre_phyto_hts";


    protected final String[] indicators = new String[]{
            "Indicator.label.sulfurPhytoProduct",        // QSA Soufre phyto
            "Indicator.label.sulfurPhytoProduct_hts",    // QSA Soufre phyto hors traitement de semence
    };

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME_HTS);
        return indicatorNameToColumnName;
    }

    @Override
    protected @NotNull List<RefCompositionSubstancesActivesParNumeroAMM> getQSA_Substance(Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> domainSubstancesByAmm, MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSulfurByAmmCode, String codeAmm) {
        List<String> idSaSulfure = allSulfurByAmmCode.get(codeAmm)
                .stream()
                .filter(RefSubstancesActivesCommissionEuropeenne::isSoufre)
                .toList()
                .stream()
                .map(RefSubstancesActivesCommissionEuropeenne::getId_sa)
                .toList();

        List<RefCompositionSubstancesActivesParNumeroAMM> dose = domainSubstancesByAmm.getOrDefault(codeAmm, List.of())
                .stream()
                .filter(ref -> idSaSulfure.contains(ref.getId_sa())).
                toList();
        return dose;
    }

    @Override
    protected void pushValuesToContext(WriterContext writerContext, PerformanceInterventionContext interventionContext, boolean isSeedingTreatment, AbstractAction action, AbstractPhytoProductInputUsage usage, Double quantity) {

        interventionContext.addUsagePhytoSQuantity(action, usage, quantity);
        if (!isSeedingTreatment) {
            interventionContext.addUsagePhytoSQuantityHts(action, usage, quantity);
        }

    }


    protected void pushValuesToContext(PerformanceInterventionContext interventionContext, Double tsaWithTS, Double tsaWithoutTS) {
        interventionContext.setPhytoSQuantity(tsaWithTS);
        interventionContext.setPhytoSQuantityHts(tsaWithoutTS);
    }

    @Override
    protected String[] getIndicators() {
        return indicators;
    }
}
