package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMMImpl;
import fr.inra.agrosyst.api.entities.referential.TypeInfo;
import fr.inra.agrosyst.api.entities.referential.TypeProduit;
import fr.inra.agrosyst.services.common.CommonService;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

public class RefPhrasesRisqueEtClassesMentionDangerParAMMModel extends AbstractAgrosystModel<RefPhrasesRisqueEtClassesMentionDangerParAMM> implements ExportModel<RefPhrasesRisqueEtClassesMentionDangerParAMM> {

    private static final ValueParser<TypeProduit> TYPE_PRODUIT_PARSER = value -> {
        if (value.equals("PRODUIT-MIXTE")) {
            return TypeProduit.PRODUIT_MIXTE;
        }
        return (TypeProduit) getGenericEnumParser(TypeProduit.class, value);
    };

    private static final ValueParser<TypeInfo> TYPE_INFO_PARSER = value -> (TypeInfo) getGenericEnumParser(TypeInfo.class, value);

    private static final ValueFormatter<TypeProduit> TYPE_PRODUIT_FORMATTER = value -> {
        if (value == TypeProduit.PRODUIT_MIXTE) {
            return "PRODUIT-MIXTE";
        }
        return GENERIC_ENUM_FORMATTER.format(value);
    };

    public RefPhrasesRisqueEtClassesMentionDangerParAMMModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("code_AMM", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_CODE__AMM);
        newMandatoryColumn("Type_produit", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_TYPE_PRODUIT, TYPE_PRODUIT_PARSER);
        newMandatoryColumn("Type_info", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_TYPE_INFO, TYPE_INFO_PARSER);
        newMandatoryColumn("Code_info", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_CODE_INFO);
        newMandatoryColumn("Libelle_info", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_LIBELLE_INFO);
        newMandatoryColumn("Libelle_court_info", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_LIBELLE_COURT_INFO);
        newMandatoryColumn("Danger_environnement", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_DANGER_ENVIRONNEMENT, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("Toxique_utilisateur", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_TOXIQUE_UTILISATEUR, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("CMR", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_CMR, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("Source", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_SOURCE);
        newMandatoryColumn("Active", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefPhrasesRisqueEtClassesMentionDangerParAMM, Object>> getColumnsForExport() {
        ModelBuilder<RefPhrasesRisqueEtClassesMentionDangerParAMM> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("code_AMM", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_CODE__AMM);
        modelBuilder.newColumnForExport("Type_produit", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_TYPE_PRODUIT, TYPE_PRODUIT_FORMATTER);
        modelBuilder.newColumnForExport("Type_info", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_TYPE_INFO, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Code_info", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_CODE_INFO);
        modelBuilder.newColumnForExport("Libelle_info", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_LIBELLE_INFO);
        modelBuilder.newColumnForExport("Libelle_court_info", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_LIBELLE_COURT_INFO);
        modelBuilder.newColumnForExport("Danger_environnement", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_DANGER_ENVIRONNEMENT, O_N_FORMATTER);
        modelBuilder.newColumnForExport("Toxique_utilisateur", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_TOXIQUE_UTILISATEUR, O_N_FORMATTER);
        modelBuilder.newColumnForExport("CMR", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_CMR, O_N_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport("Active", RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefPhrasesRisqueEtClassesMentionDangerParAMM newEmptyInstance() {
        RefPhrasesRisqueEtClassesMentionDangerParAMM referential = new RefPhrasesRisqueEtClassesMentionDangerParAMMImpl();
        referential.setActive(true);
        return referential;
    }
}
