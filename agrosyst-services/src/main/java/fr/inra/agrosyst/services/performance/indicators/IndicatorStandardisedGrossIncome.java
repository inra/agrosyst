package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2022 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.performance.CropWithSpecies;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePlotExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.performance.PracticedCropPath;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingFieldMessage;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * Formule de calcul:
 *
 * <pre>
 * Produit brut standardisé avec/sans l’autoconsommation selon prix de référence de la campagne associée au domaine sélectionné :
 * dans le cas ou une culture est déclarée comme étant un melange d'espèces:
 *
 * = PSCi * sum (Rendement(EVD) * Prix de vente pour le scénario(EVD) *  (% 𝑐𝑜𝑚𝑚𝑒𝑟𝑐𝑖𝑎𝑙𝑖𝑠é + % 𝑎𝑢𝑡𝑜𝑐𝑜𝑛𝑠𝑜𝑚𝑚é ) * TxUnitConverter))
 *
 * dans le cas contraire:
 * PB_culture sans autoconsommation = PSCi * Σ ( Xn*Prt_n*Pn*%Comm_n)/ Σ(Prt_n)
 * PB_culture avec autoconsommation = PSCi * Σ ( Xn*Prt_n*Pn)/ Σ(Prt_n)
 *
 * avec n = nombre de couples espèce*variété ; Xn : rendement du couple espèce*variété ; Prt_n : part relative du couple espèce*variété ;
 * Pn : prix du couple espèce*variété ;
 * %Comm_n = % commercialisé du couple espèce*variété.
 *
 * PSCi: (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i.
 * PSCi est calculé sur la base de données saisies par l’utilisateur.
 * Rendement(EVD): (diverses unités) : Rendement déclaré pour le couple EVD de l’action de type Récolte de l’intervention i.
 * Prix de vente pour le scénario(EVD): (diverses unités) : Prix de vente du couple EVD. Donnée saisie par l’utilisateur.
 *
 * le scénario pour les prix millésimé correspond à la campagne associée au domaine sélectionné
 *
 * </pre>
 * <p>
 * Created by davidcosse on 28/09/16.
 */
public class IndicatorStandardisedGrossIncome extends Indicator {

    public static final int WITHOUT_AUTO_CONSUME_INDEX = 0;
    public static final int WITH_AUTO_CONSUME_INDEX = 1;
    @Setter
    protected PricesService pricesService;

    @Setter
    protected PerformanceService performanceService;

    @Setter
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;

    private static final Log LOGGER = LogFactory.getLog(IndicatorStandardisedGrossIncome.class);

    protected final Map<Optional<GrowingSystem>, Double[]> effectiveStandardizedGrossIncomeGrowingSystemWithoutWithValues = new HashMap<>();

    private boolean isDisplayed;
    protected boolean isWithAutoConsumed = true;
    protected boolean isWithoutAutoConsumed = true;

    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    protected final Map<Plot, Double[]> effectivePlotWithoutWithAutoConsumeValuesValues = new HashMap<>();

    protected final Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> effectiveCropsYealdAverage = new HashMap<>();

    protected final Map<EffectiveItkCropCycleScaleKey, Double[]> effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseValues = new HashMap<>();
    protected final Map<EffectiveItkCropCycleScaleKey, Integer> effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseTotalCounter = new HashMap<>();
    protected final Map<EffectiveItkCropCycleScaleKey, Integer> effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseFieldErrorCounter = new HashMap<>();
    protected final Map<EffectiveItkCropCycleScaleKey, Set<MissingFieldMessage>> effectiveItkCropPrevCropPhaseFieldsErrors = new HashMap<>();

    protected final Map<EffectiveCropCycleScaleKey, Double[]> effectiveCroppingWithoutWithAutoConsumeValues = new HashMap<>();
    protected final Map<EffectiveCropCycleScaleKey, Integer> effectiveCroppingReliabilityTotalCounter = new HashMap<>();
    protected final Map<EffectiveCropCycleScaleKey, Integer> effectiveCroppingReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<EffectiveCropCycleScaleKey, Set<MissingFieldMessage>> effectiveCroppingFieldsErrors = new HashMap<>();

    protected final Map<Zone, Double[]> effectiveWithoutWithAutoconsumeZoneValues = new HashMap<>();
    protected final Map<Zone, Integer> effectiveZoneReliabilityTotalCounter = new HashMap<>();
    protected final Map<Zone, Set<MissingFieldMessage>> effectiveZoneFieldsErrors = new HashMap<>();
    protected final Map<Zone, Integer> effectiveZoneReliabilityFieldErrorCounter = new HashMap<>();

    protected final Map<Plot, Integer> effectivePlotReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<Plot, Integer> effectivePlotReliabilityTotalCounter = new HashMap<>();

    protected final Map<Optional<GrowingSystem>, Integer> effectiveGrowingSystemReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<Optional<GrowingSystem>, Integer> effectiveGrowingSystemReliabilityTotalCounter = new HashMap<>();

    protected final Map<PracticedSystemScaleKey, Double[]> practicedSystemsWithoutWithAutoConsumeValues = new HashMap<>();
    protected final Map<PracticedSystemScaleKey, Set<MissingFieldMessage>> practicedSystemFieldsErrors = new HashMap<>();
    protected final Map<PracticedSystemScaleKey, Integer> reliabilityIndexPracticedSystemValuesErrorCounter = new HashMap<>();
    protected final Map<PracticedSystemScaleKey, Integer> reliabilityIndexPracticedSystemValuesTotalCounter = new HashMap<>();

    public IndicatorStandardisedGrossIncome() {
        // non utilisé pour cet indicateur voir SqlWriter.addExtraValues()
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, "produit_brut_std_mil_taux_de_completion");
        // non utilisé pour cet indicateur SqlWriter.addExtraValues()
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, "produit_brut_std_mil_detail_champs_non_renseig");
    }

    private static final String[] LABELS = new String[]{
            "Indicator.label.grossIncomeStandardisedWithoutAutoConsume",
            "Indicator.label.grossIncomeStandardisedWithAutoConsume",
    };

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        // pour inclure les colonnes extra
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();

        indicatorNameToColumnName.put(getIndicatorLabel(WITHOUT_AUTO_CONSUME_INDEX), "produit_brut_std_mil_sans_autoconso");// Produit brut standardisé sans autoconsommation, millésimé (€/ha)
        indicatorNameToColumnName.put(getIndicatorLabel(WITH_AUTO_CONSUME_INDEX), "produit_brut_std_mil_avec_autoconso");   // Produit brut standardisé avec autoconsommation, millésimé (€/ha)

        return indicatorNameToColumnName;
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) &&
                isDisplayed && (i == WITHOUT_AUTO_CONSUME_INDEX && isWithoutAutoConsumed || i == WITH_AUTO_CONSUME_INDEX && isWithAutoConsumed);
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_ECONOMIC);
    }

    /**
     * Not used
     *
     * @return null
     */
    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }

    /**
     * return by refHarvestingPrices a pair of result with and result without auto consume
     */
    private Double[] computeInterWithoutWithValuesIndicator(Collection<HarvestingActionValorisation> valorisations,
                                                            Double psci,
                                                            PerformanceInterventionContext interventionContext,
                                                            CroppingPlanEntry crop,
                                                            Optional<String> optionalPracticedSystemCampaigns) {

        String interventionId = interventionContext.getInterventionId();

        boolean computeDone = false;

        Double[] interWithoutWithValues = null;

        Double[] interventionWithoutAndWithAutoConsuRes = newResult(0d, 0d);

        boolean isEdaplosDefaultValorisations = isEdaplosDefaultValorisations(valorisations);

        logNonValidValorisations(valorisations, interventionId);

        if (isEdaplosDefaultValorisations) {
            // yealdAverage
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            addMissingFieldMessage(interventionId, messageBuilder.getMissingValorisationDestinationMessage());
            return interventionWithoutAndWithAutoConsuRes;
        }

        boolean averageYealdsInsteadOfSum = crop.isCroppingPlanSpeciesNotEmpty()
                && crop.sizeCroppingPlanSpecies() > 1
                && !crop.isMixSpecies()
                && !crop.isMixVariety();

        Set<String> actionSpeciesCodes = valorisations.stream()
                .filter(Objects::nonNull)
                .map(HarvestingActionValorisation::getSpeciesCode)
                .collect(Collectors.toSet());

        double totalAreaRatio;
        Map<String, Integer> speciesAreasByCode;

        if (averageYealdsInsteadOfSum && CollectionUtils.isNotEmpty(crop.getCroppingPlanSpecies())) {

            speciesAreasByCode = crop.getCroppingPlanSpecies().stream()
                    .filter(species -> actionSpeciesCodes.contains(species.getCode()))
                    .collect(Collectors.toMap(CroppingPlanSpecies::getCode, croppingPlanSpecies -> {
                        Integer speciesArea = croppingPlanSpecies.getSpeciesArea();
                        incrementAngGetTotalFieldCounterForTargetedId(interventionId);
                        if (speciesArea == null) {
                            addMissingFieldMessage(interventionId, messageBuilder.getMissingSurfaceMessage(croppingPlanSpecies));
                            speciesArea = 100 / actionSpeciesCodes.size();
                        }
                        return speciesArea;
                    }));

            int areaSum = speciesAreasByCode.values().stream()
                    .mapToInt(a -> a)
                    .sum();
            totalAreaRatio = areaSum == 0 ? 0 : (double) 100 / areaSum;

        } else {
            totalAreaRatio = 1;
            speciesAreasByCode = new HashMap<>();
        }

        //PB_culture sans autoconsommation = PSCi * Σ ( Xn*Prt_n*Pn*%Comm_n)/ Σ(Prt_n)
        //PB_culture avec autoconsommation = PSCi * Σ ( Xn*Prt_n*Pn)/ Σ(Prt_n)
        //        ◦ avec n = nombre de couples espèce*variété ; Xn : rendement du couple espèce*variété ;
        //         Prt_n : part relative du couple espèce*variété ;
        //Pn : prix du couple espèce*variété ;
        //%Comm_n = % commercialisé du couple espèce*variété.

        Set<String> speciesCodes = valorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);

        Map<RefDestination, PriceUnit> priceUnitForDestination = new HashMap<>();
        for (HarvestingActionValorisation valorisation : valorisations) {

            if (valorisation == null || valorisation.getYealdAverage() == 0) {
                continue;
            }

            Pair<Boolean, PriceAndUnit> valorisationRefPrice = interventionContext.getValorisationRefPrice(valorisation);
            PriceAndUnit refPrice = valorisationRefPrice.getRight();

            // salesPercent
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            double salesPercent = valorisation.getSalesPercent() / 100.0d;

            //selfConsumePersent
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            double selfConsumePercent = valorisation.getSelfConsumedPersent() / 100.0d;

            // yealdAverage
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            double yealdAverage = valorisation.getYealdAverage();

            if (averageYealdsInsteadOfSum && speciesAreasByCode.containsKey(valorisation.getSpeciesCode())) {
                int speciesArea = ObjectUtils.firstNonNull(speciesAreasByCode.get(valorisation.getSpeciesCode()), 0);
                yealdAverage = yealdAverage * speciesArea * totalAreaRatio / 100;
            }

            // regroupement des prix par campagne
            Map<Integer, ValorisationPrices> standardisedPricesByCampaign = new HashMap<>();

            if (refPrice == null) {
                if (!valorisationRefPrice.getLeft()) {// le prix de référence n'a pas été encore cherché
                    PriceUnit priceUnit = priceUnitForDestination.get(valorisation.getDestination());
                    refPrice = performanceService.loadHarvestingPriceWithUnitRefPriceForValorisation(
                            optionalPracticedSystemCampaigns,
                            Optional.ofNullable(priceUnit),
                            interventionId,
                            valorisation,
                            codeEspBotCodeQualiBySpeciesCode);

                    if (refPrice == null) {
                        refPrice = performanceService.loadHarvestingPriceWithUnitRefPriceForValorisation(
                                optionalPracticedSystemCampaigns,
                                Optional.empty(),
                                interventionId,
                                valorisation,
                                codeEspBotCodeQualiBySpeciesCode);
                    }

                    interventionContext.addValorisationRefPrice(valorisation, refPrice);
                } else {
                    refPrice =  valorisationRefPrice.getRight();
                }
            }

            if (refPrice != null) {

                computeDone = true;

                PriceUnit priceUnit = refPrice.unit();
                priceUnitForDestination.putIfAbsent(valorisation.getDestination(), priceUnit);

                String code_destination_a = valorisation.getDestination().getCode_destination_A();
                Optional<Double> conversionRate = interventionContext.getPriceConverterKeysToRate().getPriceConverterRate(code_destination_a, priceUnit);

                if (conversionRate.isEmpty() && !PricesService.DEFAULT_PRICE_UNIT.equals(priceUnit)) {
                    addMissingRefConversionRateMessageMessage(interventionId);
                }

                double unitRateConverterValue = conversionRate.orElse(DEFAULT_PRICE_CONVERTION_RATE);

                // salesPercent
                incrementAngGetTotalFieldCounterForTargetedId(interventionId);
                // selfConsumePersent
                incrementAngGetTotalFieldCounterForTargetedId(interventionId);
                // unitRateConverteur
                incrementAngGetTotalFieldCounterForTargetedId(interventionId);

                double currentValueWithAutoConsume = 0.0d;
                double currentValueWithoutAutoConsume = 0.0d;

                double price = refPrice.value();

                // #10856 multiplier par 1 si €/ha et pas par le rendement moyen
                // Lorsqu'un utilisateur saisit un prix en €/ha pour une récolte,
                // la quantité récoltée déclarée ne doit pas être prise en compte,
                // on doit considérer, comme pour les intrants, une quantité de 1.
                if (PricesService.DEFAULT_PRICE_UNIT.equals(priceUnit)) {
                    double amount = price * unitRateConverterValue;
                    currentValueWithoutAutoConsume += amount;
                    currentValueWithAutoConsume += amount;
                } else {
                    currentValueWithAutoConsume += yealdAverage * price * (salesPercent + selfConsumePercent) * unitRateConverterValue;
                    currentValueWithoutAutoConsume += yealdAverage * price * salesPercent * unitRateConverterValue;
                }

                ValorisationPrices valorisationPrices =
                        standardisedPricesByCampaign.computeIfAbsent(valorisation.getBeginMarketingPeriodCampaign(),
                                k -> new ValorisationPrices());

                valorisationPrices.addWithAutoConsumePrice(currentValueWithAutoConsume);
                valorisationPrices.addWithoutAutoConsumePrice(currentValueWithoutAutoConsume);
            }

            List<Double> cropSumWithAutoConsumeValues = new ArrayList<>();
            List<Double> cropSumWithoutAutoConsumeValues = new ArrayList<>();

            for (Map.Entry<Integer, ValorisationPrices> standardisedPricesForCampaign : standardisedPricesByCampaign.entrySet()) {

                ValorisationPrices values = standardisedPricesForCampaign.getValue();

                Double sumWithAutoConsumeValues = null;
                Double sumWithoutAutoConsumeValues = null;

                List<Double> withAutoConsumeValues = values.getWithAutoConsumePrices();
                if (CollectionUtils.isNotEmpty(withAutoConsumeValues)) {
                    sumWithAutoConsumeValues = withAutoConsumeValues.stream()
                            .mapToDouble(Double::doubleValue)
                            .average()
                            .orElse(0);
                }

                List<Double> withoutAutoConsumeValues = values.getWithoutAutoConsumePrices();
                if (CollectionUtils.isNotEmpty(withoutAutoConsumeValues)) {
                    sumWithoutAutoConsumeValues = withoutAutoConsumeValues.stream()
                            .mapToDouble(Double::doubleValue)
                            .average()
                            .orElse(0);
                }

                if (sumWithAutoConsumeValues != null) {
                    cropSumWithAutoConsumeValues.add(sumWithAutoConsumeValues);
                }
                if (sumWithoutAutoConsumeValues != null) {
                    cropSumWithoutAutoConsumeValues.add(sumWithoutAutoConsumeValues);
                }

            }

            double cropAverageWithAutoConsumeValues = cropSumWithAutoConsumeValues.stream()
                    .mapToDouble(Double::doubleValue)
                    .average()
                    .orElse(0);

            double cropAverageWithoutAutoConsumeValues = cropSumWithoutAutoConsumeValues.stream()
                    .mapToDouble(Double::doubleValue)
                    .average()
                    .orElse(0);

            Double[] withoutAndWithAutoConsuRes = newResult(cropAverageWithoutAutoConsumeValues, cropAverageWithAutoConsumeValues);

            interventionWithoutAndWithAutoConsuRes = sum(interventionWithoutAndWithAutoConsuRes, withoutAndWithAutoConsuRes);
        }

        if (computeDone) {
            interWithoutWithValues = mults(interventionWithoutAndWithAutoConsuRes, psci);
        }

        return interWithoutWithValues;
    }

    @Override
    public void computePracticed(IndicatorWriter writer,
                                 PerformanceGlobalExecutionContext globalExecutionContext,
                                 PerformanceGrowingSystemExecutionContext growingSystemContext,
                                 PerformancePracticedDomainExecutionContext domainContext) {

        if (growingSystemContext.getAnonymizeGrowingSystem().isEmpty()) {
            return;
        }

        GrowingSystem anonymizeGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem().get();



        Set<PerformancePracticedSystemExecutionContext> practicedSystemExecutionContexts = growingSystemContext.getPracticedSystemExecutionContexts();

        for (PerformancePracticedSystemExecutionContext practicedSystemExecutionContext : practicedSystemExecutionContexts) {

            computePracticedSeasonal(
                    practicedSystemExecutionContext,
                    anonymizeGrowingSystem,
                    writer,
                    domainContext,
                    growingSystemContext);

            computePracticedPerennial(
                    practicedSystemExecutionContext,
                    writer,
                    domainContext,
                    anonymizeGrowingSystem,
                    growingSystemContext);

        }

        writePracticedSystemSheet(
                writer,
                domainContext,
                anonymizeGrowingSystem,
                growingSystemContext.getIts(),
                growingSystemContext.getIrs()
        );

    }

    protected void writePracticedSystemSheet(IndicatorWriter writer,
                                             PerformancePracticedDomainExecutionContext domainContext,
                                             GrowingSystem anonymizeGrowingSystem,
                                             String its,
                                             String irs) {

        Domain anonymizeDomain = domainContext.getAnonymiseDomain();

        // mise à l'échelle vers Practiced System, on tient compte de la part des cultures perennes dans le sdc
        for (Map.Entry<PracticedSystemScaleKey, Double[]> entry : practicedSystemsWithoutWithAutoConsumeValues.entrySet()) {

            PracticedSystemScaleKey key = entry.getKey();
            String campaigns = key.campaigns();
            GrowingSystem loopGrowingSystem = key.growingSystem();
            PracticedSystem anonymizePracticedSystem = key.practicedSystem();

            if (!anonymizeGrowingSystem.getCode().contentEquals(loopGrowingSystem.getCode())) {
                continue;// filter on practiced system related to the current growing system
            }

            Double[] withoutWithValues = entry.getValue();

            Integer psREC = reliabilityIndexPracticedSystemValuesErrorCounter.get(key);

            Integer psRTC = reliabilityIndexPracticedSystemValuesTotalCounter.get(key);

            Set<MissingFieldMessage> fieldsErrorMessages = practicedSystemFieldsErrors.get(key);

            Integer reliability = computeReliabilityIndex(psREC, psRTC);

            String messages;
            if (CollectionUtils.isEmpty(fieldsErrorMessages)) {
                messages = RELIABILITY_INDEX_NO_COMMENT;
            } else {
                messages = StringUtils.join(fieldsErrorMessages, ", ");
            }

            // CA Practice System scale
            if (isDisplayed && isWithoutAutoConsumed) {
                writer.writePracticedSystem(
                        its,
                        irs,
                        campaigns,
                        getIndicatorCategory(),
                        getIndicatorLabel(WITHOUT_AUTO_CONSUME_INDEX),
                        withoutWithValues[WITHOUT_AUTO_CONSUME_INDEX],
                        reliability,
                        messages,
                        anonymizeDomain,
                        loopGrowingSystem,
                        anonymizePracticedSystem,
                        this.getClass()
                );
            }

            if (isDisplayed && isWithAutoConsumed) {
                writer.writePracticedSystem(
                        its,
                        irs,
                        campaigns,
                        getIndicatorCategory(),
                        getIndicatorLabel(WITH_AUTO_CONSUME_INDEX),
                        withoutWithValues[WITH_AUTO_CONSUME_INDEX],
                        reliability,
                        messages,
                        anonymizeDomain,
                        loopGrowingSystem,
                        anonymizePracticedSystem,
                        this.getClass()
                );
            }
        }
    }

    @Override
    public void computePracticed(IndicatorWriter writer, Domain domain) {
        Map<String, Pair<Double, Double>> weightedWithoutWithValueSum = new LinkedHashMap<>();
        Map<String, Pair<Double, Double>> weightWithoutWithSum = new LinkedHashMap<>();

        Map<String, Pair<Integer, Integer>> domainCampaignRelaibilityCount = new LinkedHashMap<>();

        List<String> gstc = new ArrayList<>();

        for (Map.Entry<PracticedSystemScaleKey, Double[]> entry : practicedSystemsWithoutWithAutoConsumeValues.entrySet()) {
            Double[] withoutWithValues = entry.getValue();

            // key = campaigns, growingSystem, practicedSystem;
            final PracticedSystemScaleKey key = entry.getKey();
            String campaigns = key.campaigns();
            GrowingSystem growingSystem = key.growingSystem();

            if (growingSystem.getTypeAgriculture() != null) {
                gstc.add(growingSystem.getTypeAgriculture().getReference_label());
            }

            Integer psREC = reliabilityIndexPracticedSystemValuesErrorCounter.get(key);
            Integer psRTC = reliabilityIndexPracticedSystemValuesTotalCounter.get(key);

            Pair<Integer, Integer> dcrc = domainCampaignRelaibilityCount.computeIfAbsent(campaigns, k -> Pair.of(psREC, psRTC));

            Double usedAgriculturalArea = domain.getUsedAgriculturalArea();
            Double affectedAreaRate = growingSystem.getAffectedAreaRate();

            Integer ef = dcrc.getLeft();
            if (usedAgriculturalArea == null) {
                ef++;
            }
            if (affectedAreaRate == null) {
                ef++;
            }
            Integer tf = dcrc.getRight() + 2;

            domainCampaignRelaibilityCount.put(campaigns, Pair.of(ef, tf));

            final String domainTopiaId = domain.getTopiaId();
            if (usedAgriculturalArea == null) {
                addMissingFieldMessage(domainTopiaId, messageBuilder.getDomainMissingAffectedAreaMessage());
            }

            if (affectedAreaRate == null) {
                addMissingFieldMessage(domainTopiaId, messageBuilder.getDomainMissingAffectedAreaMessage());
            }

            // somme des valeurs pondérée
            if (!weightedWithoutWithValueSum.containsKey(campaigns)) {
                weightedWithoutWithValueSum.put(campaigns, Pair.of(0d, 0d));
            }

            // somme des poids
            if (!weightWithoutWithSum.containsKey(campaigns)) {
                weightWithoutWithSum.put(campaigns, Pair.of(0d, 0d));
            }

            usedAgriculturalArea = usedAgriculturalArea == null ? DEFAULT_USED_AGRICULTURAL_AREA : usedAgriculturalArea;
            affectedAreaRate = affectedAreaRate == null ? DEFAULT_AFFECTED_AREA_RATE : affectedAreaRate;

            Double weightedValueSumWithoutAutoConsume = weightedWithoutWithValueSum.get(campaigns).getRight()
                    + withoutWithValues[WITHOUT_AUTO_CONSUME_INDEX] * usedAgriculturalArea * affectedAreaRate;

            Double weightSumWithoutAutoConsume = weightWithoutWithSum.get(campaigns).getRight()
                    + usedAgriculturalArea * affectedAreaRate;

            Double weightedValueSumWithAutoConsume = weightedWithoutWithValueSum.get(campaigns).getLeft()
                    + withoutWithValues[WITH_AUTO_CONSUME_INDEX] * usedAgriculturalArea * affectedAreaRate;

            Double weightSumWithAutoConsume = weightWithoutWithSum.get(campaigns).getLeft()
                    + usedAgriculturalArea * affectedAreaRate;

            weightedWithoutWithValueSum.put(campaigns, Pair.of(weightedValueSumWithoutAutoConsume, weightedValueSumWithAutoConsume));
            weightWithoutWithSum.put(campaigns, Pair.of(weightSumWithoutAutoConsume, weightSumWithAutoConsume));

        }

        if (isDisplayed) {

            String comments = getMessagesForScope(MissingMessageScope.DOMAIN);

            for (Map.Entry<String, Pair<Double, Double>> weightedValueEntry : weightedWithoutWithValueSum.entrySet()) {
                String campaigns = weightedValueEntry.getKey();

                Pair<Double, Double> withoutWithAutoconsumeValues = weightedValueEntry.getValue();
                Pair<Double, Double> withoutWithAutoconsumeWeightValues = weightWithoutWithSum.get(campaigns);

                Pair<Integer, Integer> reliabilityErrorAndTotal = domainCampaignRelaibilityCount.get(campaigns);
                Integer practicedDomainREC = reliabilityErrorAndTotal.getLeft();
                Integer practicedDomainRTC = reliabilityErrorAndTotal.getRight();

                Integer reliability = computeReliabilityIndex(practicedDomainREC, practicedDomainRTC);

                if (isWithoutAutoConsumed) {
                    if (withoutWithAutoconsumeWeightValues.getLeft() != 0) {
                        // domain scale
                        double withoutAutoConsumewWeightedAverage = withoutWithAutoconsumeValues.getLeft() / withoutWithAutoconsumeWeightValues.getLeft();
                        writer.writePracticedDomain(
                                campaigns,
                                getIndicatorCategory(),
                                getIndicatorLabel(WITHOUT_AUTO_CONSUME_INDEX),
                                withoutAutoConsumewWeightedAverage,
                                reliability,
                                comments,
                                domain,
                                String.join(", ", gstc)
                        );
                    } else {
                        if (LOGGER.isWarnEnabled()) {
                            LOGGER.warn("Can't compute growing system scale with 0 weigth");
                        }
                    }
                }

                if (isWithAutoConsumed) {
                    if (withoutWithAutoconsumeWeightValues.getRight() != 0) {
                        // domain scale
                        double withAutoConsumeWeightedAverage = withoutWithAutoconsumeValues.getRight() / withoutWithAutoconsumeWeightValues.getRight();
                        writer.writePracticedDomain(
                                campaigns,
                                getIndicatorCategory(),
                                getIndicatorLabel(WITH_AUTO_CONSUME_INDEX),
                                withAutoConsumeWeightedAverage,
                                reliability,
                                comments,
                                domain,
                                String.join(", ", gstc)
                        );
                    } else {
                        if (LOGGER.isWarnEnabled()) {
                            LOGGER.warn("Can't compute growing system scale with 0 weigth");
                        }
                    }
                }

            }
        }
    }

    @Override
    public void resetPracticed(Domain domain) {
        practicedSystemsWithoutWithAutoConsumeValues.clear();
        reliabilityIndexPracticedSystemValuesTotalCounter.clear();
        reliabilityIndexPracticedSystemValuesErrorCounter.clear();
        practicedSystemFieldsErrors.clear();
        targetedErrorFieldMessages.clear();
    }

    protected void computePracticedSeasonal(
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            GrowingSystem anonymizeGrowingSystem,
            IndicatorWriter writer,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext) {

        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSystemExecutionContext.getSeasonalCropCycle();

        if (practicedSeasonalCropCycle == null) {
            return;
        }

        if (CollectionUtils.isEmpty(practicedSeasonalCropCycle.getCropCycleNodes())) {
            return;
        }

        Set<PerformancePracticedCropExecutionContext> performancePracticedCropContextExecutionContexts_ =
                practicedSystemExecutionContext.getPerformancePracticedCropContextExecutionContexts().stream()
                        .filter(
                                performanceCropExecutionContext ->
                                        Objects.isNull(performanceCropExecutionContext.getPracticedPerennialCropCycle()))
                        .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(performancePracticedCropContextExecutionContexts_)) {
            return;// no crop in cycle
        }

        List<PerformancePracticedCropExecutionContext> performancePracticedCropContextExecutionContexts = performancePracticedCropContextExecutionContexts_
                .stream()
                .sorted(Comparator.comparingInt(PerformancePracticedCropExecutionContext::getRank)).toList();

        PracticedSystem anonymizePracticedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();

        String campaigns = anonymizePracticedSystem.getCampaigns();

        PracticedSystemScaleKey practicedSystemScaleKey = new PracticedSystemScaleKey(campaigns, anonymizeGrowingSystem, anonymizePracticedSystem);

        // sum value on cycle and divide by campaigns count to get
        Map<PracticedCropCycleConnection, Double[]> cycleValues = new HashMap<>();

        AtomicBoolean seasonalInterventionFound = new AtomicBoolean(false);

        for (PerformancePracticedCropExecutionContext cropContext : performancePracticedCropContextExecutionContexts) {

            if (cropContext.getPracticedPerennialCropCycle() == null) {

                MultiKeyMap<Object, Double[]> practicedCroppingValues = new MultiKeyMap<>();
                final Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealdAverage = new HashMap<>();
                final MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter = new MultiKeyMap<>();
                final MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter = new MultiKeyMap<>();
                final MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors = new MultiKeyMap<>();

                // crop, previousCrop, rank -> value
                final MultiKeyMap<Object, Double> cumulativeFrequenciesByPracticedCropCycleConnectionCode = new MultiKeyMap<>();

                CroppingPlanEntry crop = cropContext.getCropWithSpecies().getCroppingPlanEntry();
                CroppingPlanEntry previousCrop = cropContext.getSeasonalPreviousCrop();

                String cropCode = crop.getCode();

                int rank = cropContext.getRank();
                PracticedCropCycleConnection cropConnection = cropContext.getConnection();

                String previousPlanEntryCode = previousCrop != null ? previousCrop.getCode() : "";

                cropContext.getInterventionExecutionContexts().stream()
                        .filter(interventionContext -> interventionContext.getOptionalHarvestingAction().isPresent())
                        .filter(interventionContext -> interventionContext.getIntervention().getPracticedCropCycleConnection() != null)
                        .forEach(interventionContext -> {

                            HarvestingAction harvestingAction = interventionContext.getOptionalHarvestingAction().get();

                            PracticedIntervention intervention = interventionContext.getIntervention();

                            if (!interventionContext.isFictive()) {
                                seasonalInterventionFound.set(true);
                            }

                            String interventionId = intervention.getTopiaId();

                            // as it can be on intermediate crop we use the crop from interventionContext
                            Double[] interValues = computeIndicatorForPracticedIntervention(
                                    interventionContext,
                                    harvestingAction.getValorisations()
                            );

                            if (interValues != null) {

                                // cet indicateur n'est pas écrit à l'échelle de l'intervention
                                // trow up to crop the missing fields warning
                                Set<MissingFieldMessage> missingFieldWarnings = practicedCroppingFieldsErrors.get(
                                        cropCode,
                                        previousPlanEntryCode,
                                        rank
                                );
                                if (missingFieldWarnings == null) {
                                    missingFieldWarnings = new HashSet<>();
                                    practicedCroppingFieldsErrors.put(
                                            cropCode,
                                            previousPlanEntryCode,
                                            rank,
                                            cropConnection,
                                            missingFieldWarnings
                                    );
                                }
                                Set<MissingFieldMessage> interventionWarnings = targetedErrorFieldMessages.getOrDefault(interventionId, new HashSet<>());
                                Set<MissingFieldMessage> cropMissingFieldMessages = interventionWarnings.stream()
                                        .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.CROP).isPresent() ||
                                                missingFieldMessage.getMessageForScope(MissingMessageScope.INTERVENTION).isPresent())
                                        .collect(Collectors.toSet());
                                if (CollectionUtils.isNotEmpty(cropMissingFieldMessages)) {
                                    missingFieldWarnings.addAll(cropMissingFieldMessages);
                                }

                                practicedCroppingFieldsErrors.put(
                                        cropCode,
                                        previousPlanEntryCode,
                                        rank,
                                        cropConnection,
                                        missingFieldWarnings
                                );

                                // somme pour CC le cycle (ensemble des cultures du cycle)
                                Double[] previous = practicedCroppingValues.get(
                                        cropCode,
                                        previousPlanEntryCode,
                                        rank,
                                        cropConnection
                                );

                                if (previous == null) {
                                    practicedCroppingValues.put(
                                            cropCode,
                                            previousPlanEntryCode,
                                            rank,
                                            cropConnection,
                                            interValues
                                    );
                                    if (!interventionContext.isFictive() && intervention.isIntermediateCrop()) {
                                        cropsYealdAverage.put(cropCode, cropContext.getIntermediateCropYealds());
                                    } else {
                                        cropsYealdAverage.put(cropCode, cropContext.getMainCropYealds());
                                    }

                                } else {
                                    Double[] sum = sum(previous, interValues);
                                    practicedCroppingValues.put(
                                            cropCode,
                                            previousPlanEntryCode,
                                            rank,
                                            cropConnection,
                                            sum
                                    );
                                }

                                // somme pour la culture seulement (toutes les interventions de la culture)
                                PracticedCropCycleConnection connection = interventionContext.getPracticedCropCycleConnection();
                                cycleValues.merge(connection, interValues, GenericIndicator::sum);

                                // somme pour CC
                                Integer prevReliabilityTotalCount = practicedCroppingReliabilityTotalCounter.get(
                                        cropCode,
                                        previousPlanEntryCode,
                                        rank,
                                        cropConnection
                                );

                                int interventionFieldsCounter = getTotalFieldCounterValueForTargetedId(interventionId);

                                if (prevReliabilityTotalCount == null) {
                                    practicedCroppingReliabilityTotalCounter.put(
                                            cropCode,
                                            previousPlanEntryCode,
                                            rank,
                                            cropConnection,
                                            interventionFieldsCounter
                                    );
                                } else {
                                    practicedCroppingReliabilityTotalCounter.put(
                                            cropCode,
                                            previousPlanEntryCode,
                                            rank,
                                            cropConnection,
                                            interventionFieldsCounter + prevReliabilityTotalCount
                                    );
                                }

                                Integer prevReliabilityErrorCount = practicedCroppingReliabilityFieldErrorCounter.get(
                                        cropCode,
                                        previousPlanEntryCode,
                                        rank,
                                        cropConnection
                                );

                                final int interventionMissingFieldCounter = getMissingFieldCounterValueForTargetedId(interventionId);
                                if (prevReliabilityErrorCount == null) {
                                    practicedCroppingReliabilityFieldErrorCounter.put(
                                            cropCode,
                                            previousPlanEntryCode,
                                            rank,
                                            cropConnection,
                                            interventionMissingFieldCounter
                                    );
                                } else {
                                    practicedCroppingReliabilityFieldErrorCounter.put(
                                            cropCode,
                                            previousPlanEntryCode,
                                            rank,
                                            cropConnection,
                                            interventionMissingFieldCounter + prevReliabilityErrorCount
                                    );
                                }

                                // practicedSystem scale
                                reliabilityIndexPracticedSystemValuesTotalCounter.merge(practicedSystemScaleKey, getReliabilityIndexForTargetedId(interventionId), Integer::sum);

                                reliabilityIndexPracticedSystemValuesErrorCounter.merge(practicedSystemScaleKey, interventionMissingFieldCounter, Integer::sum);

                            }

                        });// end of intervention

                cumulativeFrequenciesByPracticedCropCycleConnectionCode.put(
                        crop,
                        previousCrop,
                        rank,
                        cropConnection,
                        cropContext.getCummulativeFrequencyForCrop()
                );

                // mise à l'échelle Intervention >> Culture (CC)
                List<Map.Entry<MultiKey<?>, Double[]>> practicedSeasonalCroppingValues = practicedCroppingValues.entrySet().stream()
                        .filter(entry -> {
                            final Object previousPlanEntryCodeOrPhase = entry.getKey().getKey(1);
                            return !(previousPlanEntryCodeOrPhase instanceof PracticedCropCyclePhase);
                        })
                        .sorted(
                                Comparator.comparingInt(entry -> {
                                    final Integer _rank = (Integer) entry.getKey().getKey(2);
                                    return _rank;
                                })
                        )
                        .collect(Collectors.toList());

                writePracticedSeasonalCroppingValues(
                        writer,
                        domainContext,
                        anonymizeGrowingSystem,
                        practicedCroppingReliabilityFieldErrorCounter,
                        practicedCroppingReliabilityTotalCounter,
                        cumulativeFrequenciesByPracticedCropCycleConnectionCode,
                        cropsYealdAverage,
                        practicedSystemExecutionContext,
                        practicedCroppingFieldsErrors,
                        growingSystemContext.getIts(),
                        growingSystemContext.getIrs(),
                        anonymizePracticedSystem,
                        practicedSeasonalCroppingValues);
            }

        } // end of cycle

        // compute at practiced system scale
        if (seasonalInterventionFound.get()) {

            Set<PracticedCropPath> practicedCropPaths = practicedSystemExecutionContext.getPracticedCropPaths();
            Map<PracticedCropPath, Double[]> valuesByRoad = computeValuesByRoad(cycleValues, practicedCropPaths);

            if (!valuesByRoad.isEmpty()) {

                Double[] weightedAverageValueSum = computeWeightedAverageValueSum(valuesByRoad);

                double weightedAveragePathSize = computeWeightedAveragePathSize(valuesByRoad);
                if (weightedAverageValueSum != null && weightedAveragePathSize > 0d) {
                    Double[] result = divs(weightedAverageValueSum, weightedAveragePathSize);

                    practicedSystemsWithoutWithAutoConsumeValues.merge(practicedSystemScaleKey, result, GenericIndicator::sum);
                }
            }

        }
    }

    protected Double[] computeWeightedAverageValueSum(Map<PracticedCropPath, Double[]> valuesByRoad) {
        Double[] result = null;
        // on multiplie la somme des valeurs par chemin par le poids du chemin
        for (Map.Entry<PracticedCropPath, Double[]> practicedCropPathEntry : valuesByRoad.entrySet()) {
            Double[] practicedCropValues = practicedCropPathEntry.getValue();
            if (practicedCropValues != null) {
                PracticedCropPath path = practicedCropPathEntry.getKey();
                if (result == null) {
                    result = mults(practicedCropValues, path.getFinalFrequency());
                } else {
                    result = sum(result, mults(practicedCropValues, path.getFinalFrequency()));
                }
            }
        }
        return result;
    }

    protected double computeWeightedAveragePathSize(Map<PracticedCropPath, Double[]> valuesByRoad) {
        // Lors du calcul de la longueur de chaque branche (en nombre de campagnes),
        // il faut prendre en compte les cultures qui sont déclarées comme étant "même campagne que la culture précédente"
        return valuesByRoad.keySet().stream()
                .mapToDouble(path -> path.getFinalFrequency() *
                        Math.max(path.getConnections().stream()
                                .filter(connection -> !connection.isNotUsedForThisCampaign())
                                .map(PracticedCropCycleConnection::getTarget)
                                .filter(practicedCropCycleNode -> !practicedCropCycleNode.isSameCampaignAsPreviousNode())
                                .count(), 1))
                .sum();
    }

    protected Map<PracticedCropPath, Double[]> computeValuesByRoad(Map<PracticedCropCycleConnection, Double[]> cycleValues,
                                                                   Set<PracticedCropPath> practicedCropPaths) {
        Map<PracticedCropPath, Double[]> valueByRoad = new HashMap<>();
        // moyenne pour CA
        for (Map.Entry<PracticedCropCycleConnection, Double[]> entry : cycleValues.entrySet()) {
            Double[] values = entry.getValue();
            PracticedCropCycleConnection connection = entry.getKey();

            List<PracticedCropPath> pathsWithConnection = practicedCropPaths.stream()
                    .filter(path -> path.getConnections().contains(connection)).toList();

            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace(this.getClass().getSimpleName());
                LOGGER.trace(this.getClass().getSimpleName() + " Freq: " + connection.getCroppingPlanEntryFrequency());
                LOGGER.trace(this.getClass().getSimpleName() + " Source: X:" + connection.getSource().getRank() + " Y:" + connection.getSource().getY());
                LOGGER.trace(this.getClass().getSimpleName() + " Source Code: " + (connection.getSource() != null ? connection.getSource().getCroppingPlanEntryCode() : ""));
                LOGGER.trace(this.getClass().getSimpleName() + " Target: X:" + connection.getTarget().getRank() + " Y:" + connection.getTarget().getY());
                LOGGER.trace(this.getClass().getSimpleName() + " Target Code: " + connection.getTarget().getCroppingPlanEntryCode());

                for (PracticedCropPath practicedCropPath : pathsWithConnection) {
                    String path = practicedCropPath.getConnections().stream()
                            .map(PracticedCropCycleConnection::getTarget)
                            .sorted(Comparator.comparing(PracticedCropCycleNode::getRank))
                            .map(PracticedCropCycleNode::getCroppingPlanEntryCode)
                            .collect(Collectors.joining(" -> "));
                    double[] freqs = practicedCropPath.getConnections().stream()
                            .mapToDouble(PracticedCropCycleConnection::getCroppingPlanEntryFrequency)
                            .toArray();
                    List<String> freqs0 = new ArrayList<>();
                    for (double freq : freqs) {
                        freqs0.add(String.valueOf(freq));
                    }
                    LOGGER.trace("freqs: " + StringUtils.join(",", freqs0));
                    LOGGER.trace("Path: " + path + " Path frequency:" + practicedCropPath.getFinalFrequency());
                }
            }

            // on fait la somme des valeurs par chemin
            for (PracticedCropPath practicedCropPath : practicedCropPaths) {
                if (pathsWithConnection.contains(practicedCropPath)) {
                    valueByRoad.merge(practicedCropPath, values, GenericIndicator::sum);
                }
            }
        }
        return valueByRoad;
    }

    private void computePracticedPerennial(
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            IndicatorWriter writer,
            PerformancePracticedDomainExecutionContext domainContext,
            GrowingSystem anonymizeGrowingSystem,
            PerformanceGrowingSystemExecutionContext growingSystemContext) {

        final Optional<GrowingSystem> optionalGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();

        if (optionalGrowingSystem.isEmpty()) {
            return;
        }

        List<PracticedPerennialCropCycle> practicedPerennialCropCycles =
                practicedSystemExecutionContext.getPracticedPerennialCropCycles();

        // perennial
        if (CollectionUtils.isEmpty(practicedPerennialCropCycles)) {
            return;
        }

        Set<PerformancePracticedCropExecutionContext> performancePracticedCropContextExecutionContexts =
                practicedSystemExecutionContext.getPerformancePracticedCropContextExecutionContexts().stream()
                        .filter(
                                performanceCropExecutionContext ->
                                        Objects.nonNull(performanceCropExecutionContext.getPracticedPerennialCropCycle()))
                        .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(performancePracticedCropContextExecutionContexts)) {
            return;// no crop in cycle
        }

        PracticedSystem anonymizePracticedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();

        String campaigns = anonymizePracticedSystem.getCampaigns();

        GrowingSystem growingSystem = optionalGrowingSystem.get();

        PracticedSystemScaleKey practicedSystemScaleKey = new PracticedSystemScaleKey(campaigns, growingSystem, anonymizePracticedSystem);

        // surface totale d'occupation des cultures pérennes sur le SDC, devrait-être égal à 100.
        Double totalSolOccupationPercent = 0d;
        for (PracticedPerennialCropCycle cycle : practicedPerennialCropCycles) {
            totalSolOccupationPercent += cycle.getSolOccupationPercent();
        }
        if (totalSolOccupationPercent <= 0d) {
            totalSolOccupationPercent = null;
        }

        Map<PracticedPerennialCropCycle, Double[]> practicedSystemCycleValues = new HashMap<>();

        for (PerformancePracticedCropExecutionContext cropContext : performancePracticedCropContextExecutionContexts) {
            PracticedPerennialCropCycle cycle = cropContext.getPracticedPerennialCropCycle();
            final Map<CroppingPlanEntry, Double> perennialCropCyclePercentByCrop = new HashMap<>();
            final Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealdAverage = new HashMap<>();

            if (cycle == null) {
                // seasonal
                continue;
            }

            MultiKeyMap<Object, Double[]> practicedCroppingWithoutWithValues = new MultiKeyMap<>();

            final CropWithSpecies cropWithSpecies = cropContext.getCropWithSpecies();

            if (cropWithSpecies == null) continue;

            MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter = new MultiKeyMap<>();
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter = new MultiKeyMap<>();
            MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors = new MultiKeyMap<>();

            CroppingPlanEntry croppingPlanEntry = cropWithSpecies.getCroppingPlanEntry();
            String cropCode = croppingPlanEntry.getCode();

            perennialCropCyclePercentByCrop.put(croppingPlanEntry, cycle.getSolOccupationPercent());

            cropContext.getInterventionExecutionContexts().stream()
                    .filter(interventionContext -> interventionContext.getOptionalHarvestingAction().isPresent())
                    .forEach(interventionContext -> {

                        HarvestingAction harvestingAction = interventionContext.getOptionalHarvestingAction().get();

                        PracticedIntervention intervention = interventionContext.getIntervention();
                        String interventionId = intervention.getTopiaId();

                        PracticedCropCyclePhase phase = intervention.getPracticedCropCyclePhase();

                        Double[] interWithoutWithAutoConsumeValues = computeIndicatorForPracticedIntervention(
                                interventionContext,
                                harvestingAction.getValorisations()
                        );

                        if (interWithoutWithAutoConsumeValues != null) {

                            // trow up to crop the missing fields warning
                            Set<MissingFieldMessage> missingFieldWarnings = practicedCroppingFieldsErrors.get(
                                    anonymizePracticedSystem,
                                    cropCode,
                                    phase
                            );

                            // trow up to crop the missing fields warning
                            if (missingFieldWarnings == null) {
                                missingFieldWarnings = new HashSet<>();
                            }
                            Set<MissingFieldMessage> interventionWarnings = targetedErrorFieldMessages.getOrDefault(interventionId, new HashSet<>());
                            Set<MissingFieldMessage> cropMissingFieldMessages = interventionWarnings.stream()
                                    .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.CROP).isPresent() ||
                                            missingFieldMessage.getMessageForScope(MissingMessageScope.INTERVENTION).isPresent())
                                    .collect(Collectors.toSet());
                            missingFieldWarnings.addAll(cropMissingFieldMessages);

                            practicedCroppingFieldsErrors.put(
                                    anonymizePracticedSystem,
                                    cropCode,
                                    phase,
                                    missingFieldWarnings
                            );

                            // somme pour CC (pour toutes les cultures du cycle)
                            Double[] previous = practicedCroppingWithoutWithValues.get(cropCode, phase);
                            if (previous == null) {
                                practicedCroppingWithoutWithValues.put(
                                        cropCode,
                                        phase,
                                        interWithoutWithAutoConsumeValues
                                );
                                if (!interventionContext.isFictive() && intervention.isIntermediateCrop()) {
                                    cropsYealdAverage.put(cropCode, cropContext.getIntermediateCropYealds());
                                } else {
                                    cropsYealdAverage.put(cropCode, cropContext.getMainCropYealds());
                                }
                            } else {
                                Double[] sum = sum(previous, interWithoutWithAutoConsumeValues);
                                practicedCroppingWithoutWithValues.put(
                                        cropCode,
                                        phase,
                                        sum
                                );
                            }

                            practicedSystemCycleValues.merge(cycle, interWithoutWithAutoConsumeValues, GenericIndicator::sum);

                            // somme pour CC
                            Integer prevReliabilityTotalCount = practicedCroppingReliabilityTotalCounter.get(
                                    anonymizePracticedSystem,
                                    cropCode,
                                    phase
                            );

                            if (prevReliabilityTotalCount == null) {
                                practicedCroppingReliabilityTotalCounter.put(
                                        anonymizePracticedSystem,
                                        cropCode,
                                        phase,
                                        getTotalFieldCounterValueForTargetedId(interventionId)
                                );
                            } else {
                                practicedCroppingReliabilityTotalCounter.put(
                                        anonymizePracticedSystem,
                                        cropCode,
                                        phase,
                                        getTotalFieldCounterValueForTargetedId(interventionId) + prevReliabilityTotalCount
                                );
                            }

                            Integer prevReliabilityErrorCount = practicedCroppingReliabilityFieldErrorCounter.get(
                                    anonymizePracticedSystem,
                                    cropCode,
                                    phase
                            );

                            if (prevReliabilityErrorCount == null) {
                                practicedCroppingReliabilityFieldErrorCounter.put(
                                        anonymizePracticedSystem,
                                        cropCode,
                                        phase,
                                        getMissingFieldCounterValueForTargetedId(interventionId)
                                );
                            } else {
                                practicedCroppingReliabilityFieldErrorCounter.put(
                                        anonymizePracticedSystem,
                                        cropCode,
                                        phase,
                                        getMissingFieldCounterValueForTargetedId(interventionId) + prevReliabilityErrorCount
                                );
                            }

                            reliabilityIndexPracticedSystemValuesTotalCounter.merge(practicedSystemScaleKey, getTotalFieldCounterValueForTargetedId(interventionId), Integer::sum);

                            reliabilityIndexPracticedSystemValuesErrorCounter.merge(practicedSystemScaleKey, getMissingFieldCounterValueForTargetedId(interventionId), Integer::sum);

                            Set<MissingFieldMessage> messagesAtPracticedScope = practicedSystemFieldsErrors.computeIfAbsent(practicedSystemScaleKey, k -> new HashSet<>());

                            Set<MissingFieldMessage> practicedMissingFieldMessages = interventionWarnings.stream()
                                    .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.PRACTICED_SYSTEM).isPresent())
                                    .collect(Collectors.toSet());
                            if (CollectionUtils.isNotEmpty(practicedMissingFieldMessages)) {
                                messagesAtPracticedScope.addAll(practicedMissingFieldMessages);
                            }
                        }
                    });// fin des interventions

            List<Map.Entry<MultiKey<?>, Double[]>> practicedPerennialCroppingWithoutWithValues = practicedCroppingWithoutWithValues.entrySet().stream()
                    .filter(entry -> {
                        final Object previousPlanEntryCodeOrPhase = entry.getKey().getKey(1);
                        return previousPlanEntryCodeOrPhase instanceof PracticedCropCyclePhase;
                    })
                    .collect(Collectors.toList());

            writePracticedPerennialCroppingValues(
                    writer,
                    domainContext,
                    anonymizeGrowingSystem,
                    practicedSystemExecutionContext,
                    growingSystemContext.getIts(),
                    growingSystemContext.getIrs(),
                    anonymizePracticedSystem,
                    perennialCropCyclePercentByCrop,
                    cropsYealdAverage,
                    practicedPerennialCroppingWithoutWithValues,
                    practicedCroppingReliabilityTotalCounter,
                    practicedCroppingReliabilityFieldErrorCounter,
                    practicedCroppingFieldsErrors
            );
        } // fin des cycle de cultures

        // mise à l'échelle vers Practiced System, on tient compte de la part des cultures perennes dans le sdc
        for (Map.Entry<PracticedPerennialCropCycle, Double[]> entry : practicedSystemCycleValues.entrySet()) {
            PracticedPerennialCropCycle cycle = entry.getKey();
            Double[] cycleValues = entry.getValue();

            Double[] cropPart = addPerennialCropPart(
                    cycle.getSolOccupationPercent(),
                    totalSolOccupationPercent,
                    cycleValues
            );

            practicedSystemsWithoutWithAutoConsumeValues.merge(practicedSystemScaleKey, cropPart, GenericIndicator::sum);
        }
    }

    private Double[] computeIndicatorForPracticedIntervention(
            PerformancePracticedInterventionExecutionContext interventionContext,
            Collection<HarvestingActionValorisation> valorisations) {

        if (CollectionUtils.isEmpty(valorisations)) {
            // nothing to compute
            return null;
        }

        PracticedIntervention intervention = interventionContext.getIntervention();

        Double psci = getToolPSCi(intervention);

        String practicedSystemCampaigns = interventionContext.getPracticedOrDomainCampaigns()
                .stream()
                .map(String::valueOf)
                .collect(Collectors.joining(", "));

        CroppingPlanEntry crop = interventionContext.getCropWithSpecies().getCroppingPlanEntry();
        Double[] withoutWithValuesIndicator = computeInterWithoutWithValuesIndicator(
                valorisations,
                psci,
                interventionContext,
                crop,
                Optional.of(practicedSystemCampaigns)
        );
        if (withoutWithValuesIndicator != null) {
            interventionContext.addStandardisedProductWithoutAutoconsume(withoutWithValuesIndicator[WITHOUT_AUTO_CONSUME_INDEX]);
            interventionContext.addStandardisedProductWithAutoconsume(withoutWithValuesIndicator[WITH_AUTO_CONSUME_INDEX]);
        }
        return withoutWithValuesIndicator;
    }

    private void writePracticedSeasonalCroppingValues(
            IndicatorWriter writer,
            PerformancePracticedDomainExecutionContext domainContext,
            GrowingSystem anonymizeGrowingSystem,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter,
            MultiKeyMap<Object, Double> cumulativeFrequenciesByPracticedCropCycleConnectionCode,
            Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealdAverage,
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors,
            String its,
            String irs,
            PracticedSystem anonymizePracticedSystem,
            List<Map.Entry<MultiKey<?>, Double[]>> practicedSeasonalCroppingValues) {

        for (Map.Entry<MultiKey<?>, Double[]> entry : practicedSeasonalCroppingValues) {
            // seasonal:  cropCode, previousPlanEntryCode, rank
            MultiKey<?> practicedCroppingKey = entry.getKey();
            String croppingPlanEntryCode = (String) practicedCroppingKey.getKey(WITHOUT_AUTO_CONSUME_INDEX);
            String previousPlanEntryCode = (String) practicedCroppingKey.getKey(WITH_AUTO_CONSUME_INDEX);

            Double[] values = entry.getValue();

            CroppingPlanEntry croppingPlanEntry = domainContext.getCropByCampaignAndCode().get(
                    domainContext.getDomain().getCampaign(),
                    croppingPlanEntryCode
            );

            if (croppingPlanEntry == null) {
                CropWithSpecies cropWithSpecies = practicedSystemExecutionContext.getCropByCodeWithSpecies().get(croppingPlanEntryCode);
                croppingPlanEntry = cropWithSpecies != null ? cropWithSpecies.getCroppingPlanEntry() : null;
            }

            final Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage = cropsYealdAverage.get(croppingPlanEntryCode);

            CroppingPlanEntry previousCroppingPlanEntry = domainContext.getCropByCampaignAndCode()
                    .get(domainContext.getDomain().getCampaign(), previousPlanEntryCode);

            if (previousCroppingPlanEntry == null) {
                CropWithSpecies cropWithSpecies = practicedSystemExecutionContext.getCropByCodeWithSpecies().get(previousPlanEntryCode);
                previousCroppingPlanEntry = cropWithSpecies != null ? cropWithSpecies.getCroppingPlanEntry() : null;
            }

            writePracticedSeasonalCropSheet(
                    writer,
                    cumulativeFrequenciesByPracticedCropCycleConnectionCode,
                    anonymizeGrowingSystem,
                    irs,
                    its,
                    anonymizePracticedSystem,
                    practicedCroppingKey,
                    values,
                    croppingPlanEntry,
                    previousCroppingPlanEntry,
                    cropYealdAverage,
                    practicedCroppingReliabilityTotalCounter,
                    practicedCroppingReliabilityFieldErrorCounter,
                    practicedCroppingFieldsErrors
            );
        }
    }

    private void writePracticedPerennialCroppingValues(
            IndicatorWriter writer,
            PerformancePracticedDomainExecutionContext domainContext,
            GrowingSystem anonymizeGrowingSystem,
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            String its,
            String irs,
            PracticedSystem anonymizePracticedSystem,
            Map<CroppingPlanEntry, Double> perennialCropCyclePercentByCrop,
            Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealdAverage,
            List<Map.Entry<MultiKey<?>, Double[]>> practicedPerennialCroppingWithoutWithValues,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter,
            MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors) {

        for (Map.Entry<MultiKey<?>, Double[]> entry : practicedPerennialCroppingWithoutWithValues) {
            // perennial: cropCode, phase
            MultiKey<?> practicedCroppingKey = entry.getKey();
            String croppingPlanEntryCode = (String) practicedCroppingKey.getKey(WITHOUT_AUTO_CONSUME_INDEX);
            Object previousPlanEntryCodeOrPhase = practicedCroppingKey.getKey(WITH_AUTO_CONSUME_INDEX);

            Double[] withoutWithAutoConsumeValues = entry.getValue();

            CroppingPlanEntry croppingPlanEntry = domainContext.getCropByCampaignAndCode().get(
                    domainContext.getDomain().getCampaign(),
                    croppingPlanEntryCode
            );

            if (croppingPlanEntry == null) {
                CropWithSpecies cropWithSpecies = practicedSystemExecutionContext.getCropByCodeWithSpecies().get(croppingPlanEntryCode);
                croppingPlanEntry = cropWithSpecies != null ? cropWithSpecies.getCroppingPlanEntry() : null;
            }

            final Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage = cropsYealdAverage.get(croppingPlanEntryCode);

            final String campaigns = anonymizePracticedSystem.getCampaigns();
            writePracticedPerennialCropSheet(
                    writer,
                    anonymizeGrowingSystem,
                    irs,
                    its,
                    anonymizePracticedSystem,
                    practicedCroppingKey,
                    (PracticedCropCyclePhase) previousPlanEntryCodeOrPhase,
                    withoutWithAutoConsumeValues,
                    croppingPlanEntry,
                    campaigns,
                    cropYealdAverage,
                    perennialCropCyclePercentByCrop,
                    practicedCroppingReliabilityFieldErrorCounter,
                    practicedCroppingReliabilityTotalCounter,
                    practicedCroppingFieldsErrors
            );
        }
    }

    protected void writePracticedPerennialCropSheet(IndicatorWriter writer,
                                                    GrowingSystem anonymizeGrowingSystem,
                                                    String irs,
                                                    String its,
                                                    PracticedSystem anonymizePracticedSystem,
                                                    MultiKey<?> practicedCroppingKey,
                                                    PracticedCropCyclePhase previousPlanEntryCodeOrPhase,
                                                    Double[] withoutWithAutoconsumeValues,
                                                    CroppingPlanEntry croppingPlanEntry,
                                                    String campaigns,
                                                    Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                                    Map<CroppingPlanEntry, Double> perennialCropCyclePercentByCrop,
                                                    MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter,
                                                    MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter,
                                                    MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors) {

        Integer reliabilityErrorForCrop = practicedCroppingReliabilityFieldErrorCounter.get(practicedCroppingKey);
        Integer totalFieldCounter = practicedCroppingReliabilityTotalCounter.get(practicedCroppingKey);

        Integer reliabilityIndexForCrop = computeReliabilityIndex(reliabilityErrorForCrop, totalFieldCounter);

        Set<MissingFieldMessage> missingFields = practicedCroppingFieldsErrors.get(practicedCroppingKey);
        String reliabilityComments =
                CollectionUtils.isEmpty(missingFields) ?
                        RELIABILITY_INDEX_NO_COMMENT :
                        missingFields.stream()
                                .map(MissingFieldMessage::getMessage)
                                .collect(Collectors.joining(", "));

        // perenial crop
        if (isDisplayed && isWithoutAutoConsumed) {
            writer.writePracticedPerennialCop(
                    its,
                    irs,
                    campaigns,
                    getIndicatorCategory(),
                    getIndicatorLabel(WITHOUT_AUTO_CONSUME_INDEX),
                    withoutWithAutoconsumeValues[WITHOUT_AUTO_CONSUME_INDEX],
                    cropYealdAverage,
                    reliabilityIndexForCrop,
                    reliabilityComments,
                    anonymizeGrowingSystem.getGrowingPlan().getDomain(),
                    anonymizeGrowingSystem,
                    anonymizePracticedSystem,
                    croppingPlanEntry,
                    previousPlanEntryCodeOrPhase,
                    perennialCropCyclePercentByCrop.get(croppingPlanEntry),
                    this.getClass()
            );
        }

        if (isDisplayed && isWithAutoConsumed) {
            writer.writePracticedPerennialCop(
                    its,
                    irs,
                    campaigns,
                    getIndicatorCategory(),
                    getIndicatorLabel(WITH_AUTO_CONSUME_INDEX),
                    withoutWithAutoconsumeValues[WITH_AUTO_CONSUME_INDEX],
                    cropYealdAverage,
                    reliabilityIndexForCrop,
                    reliabilityComments,
                    anonymizeGrowingSystem.getGrowingPlan().getDomain(),
                    anonymizeGrowingSystem,
                    anonymizePracticedSystem,
                    croppingPlanEntry,
                    previousPlanEntryCodeOrPhase,
                    perennialCropCyclePercentByCrop.get(croppingPlanEntry),
                    this.getClass()
            );
        }
    }

    protected void writePracticedSeasonalCropSheet(IndicatorWriter writer,
                                                   MultiKeyMap<Object, Double> cumulativeFrequenciesByPracticedCropCycleConnectionCode,
                                                   GrowingSystem anonymizeGrowingSystem,
                                                   String irs,
                                                   String its,
                                                   PracticedSystem anonymizePracticedSystem, MultiKey<?> practicedCroppingKey,
                                                   Double[] withoutWithAutoConsumeValues,
                                                   CroppingPlanEntry croppingPlanEntry,
                                                   CroppingPlanEntry previousCroppingPlanEntry,
                                                   Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                                   MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter,
                                                   MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter,
                                                   MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors) {
        // seasonal
        int rank = (Integer) practicedCroppingKey.getKey(2);
        PracticedCropCycleConnection cropConnection = (PracticedCropCycleConnection) practicedCroppingKey.getKey(3);

        Integer reliabilityErrorForCrop = practicedCroppingReliabilityFieldErrorCounter.get(practicedCroppingKey);
        Integer totalFieldCounter = practicedCroppingReliabilityTotalCounter.get(practicedCroppingKey);

        Integer reliabilityIndexForCrop = computeReliabilityIndex(reliabilityErrorForCrop, totalFieldCounter);

        Double cumulativeFrequencies = cumulativeFrequenciesByPracticedCropCycleConnectionCode.get(
                croppingPlanEntry,
                previousCroppingPlanEntry,
                rank,
                cropConnection
        );

        Double cumulativeFrequency = cumulativeFrequencies == null ? 0 : cumulativeFrequencies;

        Set<MissingFieldMessage> missingFields = practicedCroppingFieldsErrors.get(practicedCroppingKey);
        String reliabilityComments = CollectionUtils.isEmpty(missingFields) ? RELIABILITY_INDEX_NO_COMMENT :
                missingFields.stream().map(MissingFieldMessage::getMessage).collect(Collectors.joining(", "));

        // seasonal crop
        if (isDisplayed && isWithoutAutoConsumed) {
            writer.writePracticedSeasonalCrop(
                    its,
                    irs,
                    anonymizePracticedSystem.getCampaigns(),
                    cumulativeFrequency,
                    getIndicatorCategory(),
                    getIndicatorLabel(WITHOUT_AUTO_CONSUME_INDEX),
                    withoutWithAutoConsumeValues[WITHOUT_AUTO_CONSUME_INDEX],
                    cropYealdAverage,
                    reliabilityIndexForCrop,
                    reliabilityComments,
                    rank,
                    anonymizeGrowingSystem.getGrowingPlan().getDomain(),
                    anonymizeGrowingSystem,
                    anonymizePracticedSystem,
                    croppingPlanEntry,
                    previousCroppingPlanEntry,
                    cropConnection.getTopiaId(),
                    this.getClass(),
                    cropConnection);
        }

        // seasonal crop
        if (isDisplayed && isWithAutoConsumed) {
            writer.writePracticedSeasonalCrop(
                    its,
                    irs,
                    anonymizePracticedSystem.getCampaigns(),
                    cumulativeFrequency,
                    getIndicatorCategory(),
                    getIndicatorLabel(WITH_AUTO_CONSUME_INDEX),
                    withoutWithAutoConsumeValues[WITH_AUTO_CONSUME_INDEX],
                    cropYealdAverage,
                    reliabilityIndexForCrop,
                    reliabilityComments,
                    rank,
                    anonymizeGrowingSystem.getGrowingPlan().getDomain(),
                    anonymizeGrowingSystem,
                    anonymizePracticedSystem,
                    croppingPlanEntry,
                    previousCroppingPlanEntry,
                    cropConnection.getTopiaId(),
                    this.getClass(),
                    cropConnection);
        }
    }

    @Override
    public void computeEffective(IndicatorWriter writer,
                                 PerformanceGlobalExecutionContext globalExecutionContext,
                                 PerformanceEffectiveDomainExecutionContext domainContext,
                                 PerformanceZoneExecutionContext zoneContext) {

        Plot anonymizePlot = zoneContext.getAnonymizePlot();
        Optional<GrowingSystem> optionalAnoGrowingSystem = zoneContext.getAnonymizeGrowingSystem();

        computeEffectivePerennial(zoneContext);

        computeEffectiveSeasonal(zoneContext);

        // la map effectiveZoneValues est valuée plusieurs fois avec plusieurs zones par plusieurs
        // appel pour la mise à l'echelle de la methode suivante
        // computeEffective(IndicatorWriter writer, Domain domain, GrowingSystem growingSystem, plot plot)
        // on ne genere une ligne dans le fichier de sortie que pour la zone courante (s'il y a des valeurs)
        // et non pour toutes les zones
        writeEffectiveZone(writer, domainContext, zoneContext, anonymizePlot, optionalAnoGrowingSystem);
    }

    private void writeEffectiveZone(IndicatorWriter writer,
                                    PerformanceEffectiveDomainExecutionContext domainContext,
                                    PerformanceZoneExecutionContext zoneContext,
                                    Plot anonymizePlot,
                                    Optional<GrowingSystem> optionalAnoGrowingSystem) {

        Zone anonymizeZone = zoneContext.getAnonymizeZone();
        Double[] withoutWithAutoConsumeValues = effectiveWithoutWithAutoconsumeZoneValues.get(anonymizeZone);
        if (withoutWithAutoConsumeValues != null) {

            String its = zoneContext.getIts();
            String irs = zoneContext.getIrs();

            String speciesName = zoneContext.getZoneSpeciesNames();
            String varietyNames = zoneContext.getZoneVarietiesNames();

            final Map<Pair<RefDestination, YealdUnit>, Double> zoneAverageYeald = zoneContext.getZoneAverageYeald();

            String comments = RELIABILITY_INDEX_NO_COMMENT;
            if (isDisplayed) {
                Set<MissingFieldMessage> fieldMessages = effectiveZoneFieldsErrors.get(anonymizeZone);
                if (CollectionUtils.isNotEmpty(fieldMessages)) {
                    comments = fieldMessages.stream()
                            .map(MissingFieldMessage::getMessage)
                            .collect(Collectors.joining(", "));
                }
            }

            Integer rmfzc = effectiveZoneReliabilityFieldErrorCounter.get(anonymizeZone);
            Integer rtzc = effectiveZoneReliabilityTotalCounter.get(anonymizeZone);
            Integer reliabilityIndex = computeReliabilityIndex(rmfzc, rtzc);

            // write to effective zone
            if (isDisplayed && isWithoutAutoConsumed) {
                Double resultWithoutAutoConsume = withoutWithAutoConsumeValues[WITHOUT_AUTO_CONSUME_INDEX];
                writer.writeEffectiveZone(
                        its,
                        irs,
                        getIndicatorCategory(),
                        getIndicatorLabel(WITHOUT_AUTO_CONSUME_INDEX),
                        resultWithoutAutoConsume,
                        zoneAverageYeald,
                        reliabilityIndex,
                        comments,
                        domainContext.getAnonymiseDomain(),
                        domainContext.getCroppingPlanSpecies(),
                        optionalAnoGrowingSystem,
                        anonymizePlot,
                        anonymizeZone,
                        speciesName,
                        varietyNames,
                        this.getClass()
                );
            }

            if (isDisplayed && isWithAutoConsumed) {
                Double resultWithAutoConsumme = withoutWithAutoConsumeValues[WITH_AUTO_CONSUME_INDEX];
                writer.writeEffectiveZone(
                        its,
                        irs,
                        getIndicatorCategory(),
                        getIndicatorLabel(WITH_AUTO_CONSUME_INDEX),
                        resultWithAutoConsumme,
                        zoneAverageYeald,
                        reliabilityIndex,
                        comments,
                        domainContext.getAnonymiseDomain(),
                        domainContext.getCroppingPlanSpecies(),
                        optionalAnoGrowingSystem,
                        anonymizePlot,
                        anonymizeZone,
                        speciesName,
                        varietyNames,
                        this.getClass()
                );
            }

            for (Map.Entry<EffectiveItkCropCycleScaleKey, Double[]> effectiveItkCropPrevCropPhaseKeyEntry : effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseValues.entrySet()) {

                EffectiveItkCropCycleScaleKey key = effectiveItkCropPrevCropPhaseKeyEntry.getKey();
                Double[] itkWithoutWithAutoConsumeValues = effectiveItkCropPrevCropPhaseKeyEntry.getValue();
                Integer effectiveItkCropPrevCropPhaseTotalCount = effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseTotalCounter.get(key);
                Integer effectiveItkCropPrevCropPhaseFieldErrorCount = effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseFieldErrorCounter.get(key);
                Integer effectiveItkCropPrevCropPhaseReliabilityIndex = computeReliabilityIndex(effectiveItkCropPrevCropPhaseFieldErrorCount, effectiveItkCropPrevCropPhaseTotalCount);

                String effectiveItkCropPrevCropPhaseComments;
                Set<MissingFieldMessage> effectiveItkCropPrevCropPhaseFieldMessages = effectiveItkCropPrevCropPhaseFieldsErrors.get(key);
                if (CollectionUtils.isNotEmpty(effectiveItkCropPrevCropPhaseFieldMessages)) {
                    Set<String> errors = effectiveItkCropPrevCropPhaseFieldMessages.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet());
                    effectiveItkCropPrevCropPhaseComments = String.join(IndicatorWriter.SEPARATOR, errors);
                } else {
                    effectiveItkCropPrevCropPhaseComments = RELIABILITY_INDEX_NO_COMMENT;
                }

                if (isDisplayed && isWithoutAutoConsumed) {
                    Double resultWithoutAutoConsumme = itkWithoutWithAutoConsumeValues[WITHOUT_AUTO_CONSUME_INDEX];
                    // write zone sheet
                    writer.writeEffectiveItk(
                            its,
                            irs,
                            getIndicatorCategory(),
                            getIndicatorLabel(WITHOUT_AUTO_CONSUME_INDEX),
                            resultWithoutAutoConsumme,
                            effectiveItkCropPrevCropPhaseReliabilityIndex,
                            effectiveItkCropPrevCropPhaseComments,
                            domainContext.getAnonymiseDomain(),
                            optionalAnoGrowingSystem,
                            anonymizePlot,
                            anonymizeZone,
                            this.getClass(),
                            key);
                }

                if (isDisplayed && isWithAutoConsumed) {
                    Double resultWithAutoConsumme = itkWithoutWithAutoConsumeValues[WITH_AUTO_CONSUME_INDEX];
                    // write zone sheet
                    writer.writeEffectiveItk(
                            its,
                            irs,
                            getIndicatorCategory(),
                            getIndicatorLabel(WITH_AUTO_CONSUME_INDEX),
                            resultWithAutoConsumme,
                            effectiveItkCropPrevCropPhaseReliabilityIndex,
                            effectiveItkCropPrevCropPhaseComments,
                            domainContext.getAnonymiseDomain(),
                            optionalAnoGrowingSystem,
                            anonymizePlot,
                            anonymizeZone,
                            this.getClass(),
                            key);
                }

            }

        }
        effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseValues.clear();
        effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseTotalCounter.clear();
        effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseFieldErrorCounter.clear();
        effectiveItkCropPrevCropPhaseFieldsErrors.clear();
    }

    protected void computeEffectivePerennial(PerformanceZoneExecutionContext zoneContext) {

        // perennial
        List<EffectivePerennialCropCycle> perennialCycles = zoneContext.getPerennialCropCycles();

        if (CollectionUtils.isEmpty(perennialCycles)) {
            return;
        }

        Set<PerformanceEffectiveCropExecutionContext> cropContexts = zoneContext.getPerformanceCropContextExecutionContexts()
                .stream()
                .filter(
                        performanceCropExecutionContext ->
                                !Objects.isNull(performanceCropExecutionContext.getPerennialCropCycle()
                                ))
                .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(cropContexts)) {
            return;// no crop in cycle
        }

        Zone anonymizeZone = zoneContext.getAnonymizeZone();

        for (PerformanceEffectiveCropExecutionContext cropContext : cropContexts) {

            Set<PerformanceEffectiveInterventionExecutionContext> interventionContexts = cropContext.getInterventionExecutionContexts();

            // perennial
            interventionContexts.stream()
                    .filter(interventionContext -> interventionContext.getIntervention().getEffectiveCropCyclePhase() != null)
                    .filter(interventionContext -> interventionContext.getOptionalHarvestingAction().isPresent())
                    .forEach(interventionContext -> {

                        EffectiveIntervention intervention = interventionContext.getIntervention();

                        String interventionId = intervention.getTopiaId();

                        CroppingPlanEntry croppingPlanEntry = interventionContext.getCroppingPlanEntry();

                        HarvestingAction harvestingAction = interventionContext.getOptionalHarvestingAction().get();

                        // get value for intervention
                        Double[] withoutWithValuesForEffectiveIntervention = computeWithoutWithValuesForEffectiveIntervention(
                                interventionContext,
                                harvestingAction.getValorisations());

                        if (withoutWithValuesForEffectiveIntervention != null) {

                            // cet indicateur n'est pas écrit à l'échelle de l'intervention (n'a pas de sens)
                            Set<MissingFieldMessage> interventionWarnings = targetedErrorFieldMessages.getOrDefault(interventionId, new HashSet<>());
                            Set<MissingFieldMessage> cropMissingFieldMessages = interventionWarnings.stream()
                                    .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.CROP).isPresent() ||
                                            missingFieldMessage.getMessageForScope(MissingMessageScope.INTERVENTION).isPresent())
                                    .collect(Collectors.toSet());

                            // somme pour CC
                            EffectiveCropCyclePhase phase = intervention.getEffectiveCropCyclePhase();

                            EffectiveCropCycleScaleKey effectivePerennialCropKey =
                                    new EffectiveCropCycleScaleKey(
                                            croppingPlanEntry,
                                            Optional.empty(),
                                            Optional.empty(),
                                            Optional.of(phase));

                            aggregateToCropScale(
                                    cropContext,
                                    intervention,
                                    croppingPlanEntry,
                                    withoutWithValuesForEffectiveIntervention,
                                    cropMissingFieldMessages,
                                    effectivePerennialCropKey);

                            // somme pour CA
                            effectiveWithoutWithAutoconsumeZoneValues.merge(anonymizeZone, withoutWithValuesForEffectiveIntervention, GenericIndicator::sum);

                            effectiveZoneReliabilityTotalCounter.merge(
                                    anonymizeZone,
                                    getTotalFieldCounterValueForTargetedId(interventionId),
                                    Integer::sum
                            );
                            effectiveZoneReliabilityFieldErrorCounter.merge(
                                    anonymizeZone,
                                    getMissingFieldCounterValueForTargetedId(interventionId),
                                    Integer::sum
                            );

                            Set<MissingFieldMessage> zoneMissingFieldMessages = interventionWarnings.stream()
                                    .filter(missingFieldMessage -> missingFieldMessage
                                            .getMessageForScope(MissingMessageScope.ZONE)
                                            .isPresent())
                                    .collect(Collectors.toSet());

                            Set<MissingFieldMessage> missingFieldsForZone = effectiveZoneFieldsErrors.computeIfAbsent(anonymizeZone, k -> new HashSet<>());
                            missingFieldsForZone.addAll(zoneMissingFieldMessages);

                            EffectiveItkCropCycleScaleKey effectiveItkCropCycleScaleKey =
                                    new EffectiveItkCropCycleScaleKey(
                                            zoneContext.getGrowingSystem(),
                                            zoneContext.getPlot(),
                                            anonymizeZone,
                                            croppingPlanEntry,
                                            Optional.empty(),
                                            Optional.empty(),
                                            Optional.of(phase),
                                            Optional.ofNullable(intervention.getEffectiveCropCycleNode())
                                    );

                            effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseValues.merge(
                                    effectiveItkCropCycleScaleKey,
                                    withoutWithValuesForEffectiveIntervention,
                                    GenericIndicator::sum);

                            effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseTotalCounter.merge(
                                    effectiveItkCropCycleScaleKey,
                                    getTotalFieldCounterValueForTargetedId(interventionId),
                                    Integer::sum);

                            effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseFieldErrorCounter.merge(
                                    effectiveItkCropCycleScaleKey,
                                    getMissingFieldCounterValueForTargetedId(interventionId),
                                    Integer::sum);

                            // on ajoute ceux de la zone ?
                            Set<MissingFieldMessage> missingFieldsForItkCropPrevCropPhase = effectiveItkCropPrevCropPhaseFieldsErrors.computeIfAbsent(effectiveItkCropCycleScaleKey, k -> new HashSet<>());
                            missingFieldsForItkCropPrevCropPhase.addAll(zoneMissingFieldMessages);
                        }
                    });

        }
    }

    private void aggregateToCropScale(PerformanceEffectiveCropExecutionContext cropContext,
                                      EffectiveIntervention intervention,
                                      CroppingPlanEntry crop,
                                      Double[] interWithoutWithValues,
                                      Set<MissingFieldMessage> cropMissingFieldMessages,
                                      EffectiveCropCycleScaleKey effectiveCropKey) {

        Set<MissingFieldMessage> missingFieldsForCrop = effectiveCroppingFieldsErrors.computeIfAbsent(effectiveCropKey, k -> new HashSet<>());
        // trow up to crop the missing fields warning
        if (CollectionUtils.isNotEmpty(cropMissingFieldMessages)) {
            missingFieldsForCrop.addAll(cropMissingFieldMessages);
        }

        String interventionId = intervention != null ? intervention.getTopiaId() : PerformanceInterventionContext.DUMMY;

        if (intervention != null && intervention.isIntermediateCrop()) {
            effectiveCropsYealdAverage.computeIfAbsent(crop.getCode(), k -> cropContext.getIntermediateCropYealds());
        } else {
            effectiveCropsYealdAverage.computeIfAbsent(crop.getCode(), k -> cropContext.getMainCropYealds());
        }
        // somme pour CC (pour toutes les cultures du cycle)
        effectiveCroppingWithoutWithAutoConsumeValues.merge(effectiveCropKey, interWithoutWithValues, GenericIndicator::sum);

        // somme pour CC
        effectiveCroppingReliabilityTotalCounter.merge(effectiveCropKey, getTotalFieldCounterValueForTargetedId(interventionId), Integer::sum);
        effectiveCroppingReliabilityFieldErrorCounter.merge(effectiveCropKey, getMissingFieldCounterValueForTargetedId(interventionId), Integer::sum);

    }


    protected void computeEffectiveSeasonal(PerformanceZoneExecutionContext zoneContext) {

        EffectiveSeasonalCropCycle seasonalCropCycle = zoneContext.getSeasonalCropCycle();

        if (seasonalCropCycle == null) {
            return;
        }

        Set<PerformanceEffectiveCropExecutionContext> cropContexts_ = zoneContext.getPerformanceCropContextExecutionContexts()
                .stream()
                .filter(
                        performanceCropExecutionContext ->
                                Objects.isNull(performanceCropExecutionContext.getPerennialCropCycle()
                                ))
                .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(cropContexts_)) {
            return;// no crop in cycle
        }

        List<PerformanceEffectiveCropExecutionContext> cropContexts = cropContexts_
                .stream()
                .sorted(Comparator.comparingInt(PerformanceEffectiveCropExecutionContext::getRank)).toList();

        Zone anonymizeZone = zoneContext.getAnonymizeZone();

        for (PerformanceEffectiveCropExecutionContext cropContext : cropContexts) {

            Set<PerformanceEffectiveInterventionExecutionContext> interventionContexts = cropContext.getInterventionExecutionContexts();

            if (CollectionUtils.isEmpty(interventionContexts)) {
                continue; // NOTHING TO DO
            }

            CroppingPlanEntry previousCrop = cropContext.getSeasonalPreviousCrop();

            interventionContexts.stream()
                    .filter(interventionContext -> interventionContext.getIntervention().getEffectiveCropCycleNode() != null)
                    .filter(interventionContext -> interventionContext.getOptionalHarvestingAction().isPresent())
                    .forEach(interventionContext -> {
                        // main or intermediate one
                        CroppingPlanEntry croppingPlanEntry = interventionContext.getCroppingPlanEntry();

                        HarvestingAction harvestingAction = interventionContext.getOptionalHarvestingAction().get();

                        Double[] interWithoutWithAutoConsumeValues = computeWithoutWithValuesForEffectiveIntervention(
                                interventionContext,
                                harvestingAction.getValorisations()
                        );

                        if (interWithoutWithAutoConsumeValues != null) {

                            EffectiveIntervention intervention = interventionContext.getIntervention();
                            String interventionId = intervention.getTopiaId();
                            EffectiveCropCycleNode node = intervention.getEffectiveCropCycleNode();
                            final int rank = node.getRank();

                            Set<MissingFieldMessage> interventionWarnings = targetedErrorFieldMessages.getOrDefault(interventionId, new HashSet<>());
                            Set<MissingFieldMessage> cropMissingFieldMessages = interventionWarnings.stream()
                                    .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.CROP).isPresent() ||
                                            missingFieldMessage.getMessageForScope(MissingMessageScope.INTERVENTION).isPresent())
                                    .collect(Collectors.toSet());

                            EffectiveCropCycleScaleKey effectiveSeasonalCropKey =
                                    new EffectiveCropCycleScaleKey(
                                            croppingPlanEntry,
                                            Optional.ofNullable(previousCrop),
                                            Optional.of(node.getRank()),
                                            Optional.empty());

                            aggregateToCropScale(
                                    cropContext,
                                    intervention,
                                    croppingPlanEntry,
                                    interWithoutWithAutoConsumeValues,
                                    cropMissingFieldMessages,
                                    effectiveSeasonalCropKey);

                            // somme pour CA
                            effectiveWithoutWithAutoconsumeZoneValues.merge(anonymizeZone, interWithoutWithAutoConsumeValues, GenericIndicator::sum);

                            effectiveZoneReliabilityTotalCounter.merge(
                                    anonymizeZone,
                                    getTotalFieldCounterValueForTargetedId(interventionId),
                                    Integer::sum
                            );
                            effectiveZoneReliabilityFieldErrorCounter.merge(
                                    anonymizeZone,
                                    getMissingFieldCounterValueForTargetedId(interventionId),
                                    Integer::sum
                            );

                            Set<MissingFieldMessage> zoneMissingFieldMessages = interventionWarnings.stream()
                                    .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.ZONE).isPresent())
                                    .collect(Collectors.toSet());
                            Set<MissingFieldMessage> missingFieldsForCrop = effectiveZoneFieldsErrors.computeIfAbsent(anonymizeZone, k -> new HashSet<>());
                            missingFieldsForCrop.addAll(zoneMissingFieldMessages);

                            EffectiveItkCropCycleScaleKey effectiveItkCropCycleScaleKey =
                                    new EffectiveItkCropCycleScaleKey(
                                            zoneContext.getGrowingSystem(),
                                            zoneContext.getPlot(),
                                            anonymizeZone,
                                            croppingPlanEntry,
                                            Optional.ofNullable(previousCrop),
                                            Optional.of(rank),
                                            Optional.empty(),
                                            Optional.ofNullable(intervention.getEffectiveCropCycleNode())
                                    );

                            effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseValues.merge(
                                    effectiveItkCropCycleScaleKey,
                                    interWithoutWithAutoConsumeValues,
                                    GenericIndicator::sum);

                            effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseTotalCounter.merge(
                                    effectiveItkCropCycleScaleKey,
                                    getTotalFieldCounterValueForTargetedId(interventionId),
                                    Integer::sum);

                            effectiveItkWithoutWithAutoConsumeCropPrevCropPhaseFieldErrorCounter.merge(
                                    effectiveItkCropCycleScaleKey,
                                    getMissingFieldCounterValueForTargetedId(interventionId),
                                    Integer::sum);

                            // on ajoute ceux de la zone ?
                            Set<MissingFieldMessage> missingFieldsForItkCropPrevCropPhase = effectiveItkCropPrevCropPhaseFieldsErrors.computeIfAbsent(effectiveItkCropCycleScaleKey, k -> new HashSet<>());
                            missingFieldsForItkCropPrevCropPhase.addAll(zoneMissingFieldMessages);

                        }
                    });
        }
    }

    protected Double[] computeWithoutWithValuesForEffectiveIntervention(
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            Collection<HarvestingActionValorisation> valorisations) {

        if (CollectionUtils.isEmpty(valorisations)) {
            // nothing to compute
            return null;
        }

        logNonValidValorisations(valorisations, interventionContext.getInterventionId());

        boolean isEdaplosDefaultValorisations = isEdaplosDefaultValorisations(valorisations);

        if (isEdaplosDefaultValorisations) {
            String interventionId = interventionContext.getInterventionId();
            // yealdAverage
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            addMissingFieldMessage(interventionId, messageBuilder.getMissingValorisationDestinationMessage());
            return newResult(0d, 0d);
        }

        EffectiveIntervention intervention = interventionContext.getIntervention();
        Double psci = getToolPSCi(intervention);
        CroppingPlanEntry crop = interventionContext.getCroppingPlanEntry();
        Double[] withoutWithValuesIndicator = computeInterWithoutWithValuesIndicator(
                valorisations,
                psci,
                interventionContext,
                crop,
                Optional.empty()
        );
        if (withoutWithValuesIndicator != null) {
            interventionContext.addStandardisedProductWithoutAutoconsume(withoutWithValuesIndicator[WITHOUT_AUTO_CONSUME_INDEX]);
            interventionContext.addStandardisedProductWithAutoconsume(withoutWithValuesIndicator[WITH_AUTO_CONSUME_INDEX]);
        }
        return withoutWithValuesIndicator;
    }

    @Override
    public void computeEffectiveCC(IndicatorWriter writer,
                                   Domain domain,
                                   PerformanceGrowingSystemExecutionContext growingSystemContext,
                                   Plot plot) {

        Optional<GrowingSystem> optionalAnoGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();
        String its = growingSystemContext.getIts();
        String irs = growingSystemContext.getIrs();

        // échelle culture
        // toutes les interventions culturales d’une campagne culturale (CC)
        List<Map.Entry<EffectiveCropCycleScaleKey, Double[]>> effectiveSeasonalCroppingWithoutWithAutoConsumeValues = effectiveCroppingWithoutWithAutoConsumeValues.entrySet().stream()
                .filter(entry -> entry.getKey().rang().isPresent())
                .sorted(
                        Comparator.comparingInt(entry -> {
                            final Integer rank = entry.getKey().rang().get();
                            return rank;
                        })
                )
                .toList();

        writeEffectiveSeasonalCroppingValues(
                writer,
                domain,
                plot,
                optionalAnoGrowingSystem,
                its,
                irs,
                effectiveSeasonalCroppingWithoutWithAutoConsumeValues);

        List<Map.Entry<EffectiveCropCycleScaleKey, Double[]>> effectivePerennialCroppingWithoutWithAutoConsumeValues = effectiveCroppingWithoutWithAutoConsumeValues.entrySet().stream()
                .filter(entry -> entry.getKey().phase().isPresent())
                .toList();

        writeEffectivePerennialCroppingValues(
                writer,
                domain,
                plot,
                optionalAnoGrowingSystem,
                its,
                irs,
                effectivePerennialCroppingWithoutWithAutoConsumeValues);

    }

    private void writeEffectivePerennialCroppingValues(IndicatorWriter writer,
                                                       Domain domain,
                                                       Plot plot,
                                                       Optional<GrowingSystem> optionnalAnoGrowingSystem,
                                                       String its,
                                                       String irs,
                                                       List<Map.Entry<EffectiveCropCycleScaleKey, Double[]>> effectivePerennialCroppingWithoutWithAutoConsumeValues) {

        for (Map.Entry<EffectiveCropCycleScaleKey, Double[]> entry : effectivePerennialCroppingWithoutWithAutoConsumeValues) {
            Double[] withoutWithAutoConsumeValues = entry.getValue();
            EffectiveCropCycleScaleKey effectiveCroppingKey = entry.getKey();
            CroppingPlanEntry croppingPlanEntry = effectiveCroppingKey.crop();
            Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage = effectiveCropsYealdAverage.get(croppingPlanEntry.getCode());

            Integer riec = effectiveCroppingReliabilityFieldErrorCounter.get(effectiveCroppingKey);
            Integer ritc = effectiveCroppingReliabilityTotalCounter.get(effectiveCroppingKey);

            Integer reliability = computeReliabilityIndex(riec, ritc);

            String comments = RELIABILITY_INDEX_NO_COMMENT;
            if (isDisplayed && (isWithAutoConsumed || isWithoutAutoConsumed)) {
                Set<MissingFieldMessage> messages = effectiveCroppingFieldsErrors.get(effectiveCroppingKey);
                if (CollectionUtils.isNotEmpty(messages)) {
                    comments = messages.stream()
                            .map(MissingFieldMessage::getMessage)
                            .collect(Collectors.joining(","));
                }
                EffectiveCropCyclePhase effectiveCropCyclePhase = effectiveCroppingKey.phase().orElse(null);

                // Effective perennial crop
                if (isDisplayed && isWithoutAutoConsumed) {
                    Double valueWithoutAutoConsume = withoutWithAutoConsumeValues[WITHOUT_AUTO_CONSUME_INDEX];
                    writer.writeEffectivePerennialCrop(
                            its,
                            irs,
                            domain.getCampaign(),
                            getIndicatorCategory(),
                            getIndicatorLabel(WITHOUT_AUTO_CONSUME_INDEX),
                            valueWithoutAutoConsume,
                            cropYealdAverage,
                            reliability,
                            comments,
                            domain,
                            optionnalAnoGrowingSystem,
                            plot,
                            croppingPlanEntry,
                            effectiveCropCyclePhase,
                            this.getClass()
                    );
                }

                if (isDisplayed && isWithAutoConsumed) {
                    Double valueWithAutoConsume = withoutWithAutoConsumeValues[WITH_AUTO_CONSUME_INDEX];
                    writer.writeEffectivePerennialCrop(
                            its,
                            irs,
                            domain.getCampaign(),
                            getIndicatorCategory(),
                            getIndicatorLabel(WITH_AUTO_CONSUME_INDEX),
                            valueWithAutoConsume,
                            cropYealdAverage,
                            reliability,
                            comments,
                            domain,
                            optionnalAnoGrowingSystem,
                            plot,
                            croppingPlanEntry,
                            effectiveCropCyclePhase,
                            this.getClass()
                    );
                }
            }
        }
    }

    private void writeEffectiveSeasonalCroppingValues(IndicatorWriter writer,
                                                      Domain domain,
                                                      Plot plot,
                                                      Optional<GrowingSystem> optionnalAnoGrowingSystem,
                                                      String its,
                                                      String irs,
                                                      List<Map.Entry<EffectiveCropCycleScaleKey, Double[]>> effectiveSeasonalCroppingWithoutWithAutoConsumeValues) {

        for (Map.Entry<EffectiveCropCycleScaleKey, Double[]> entry : effectiveSeasonalCroppingWithoutWithAutoConsumeValues) {
            Double[] withoutWithAutoConsumeValues = entry.getValue();
            EffectiveCropCycleScaleKey effectiveCroppingKey = entry.getKey();
            CroppingPlanEntry croppingPlanEntry = effectiveCroppingKey.crop();
            Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage = effectiveCropsYealdAverage.get(croppingPlanEntry.getCode());

            Integer riec = effectiveCroppingReliabilityFieldErrorCounter.get(effectiveCroppingKey);
            Integer ritc = effectiveCroppingReliabilityTotalCounter.get(effectiveCroppingKey);

            Integer reliability = computeReliabilityIndex(riec, ritc);

            String comments = RELIABILITY_INDEX_NO_COMMENT;
            if (isDisplayed && (isWithAutoConsumed || isWithoutAutoConsumed)) {
                Set<MissingFieldMessage> messages = effectiveCroppingFieldsErrors.get(effectiveCroppingKey);
                if (CollectionUtils.isNotEmpty(messages)) {
                    comments = messages.stream()
                            .map(MissingFieldMessage::getMessage)
                            .collect(Collectors.joining(","));
                }
            }

            CroppingPlanEntry previousCroppingPlanEntry = effectiveCroppingKey.previousCrop().orElse(null);
            Integer rank = effectiveCroppingKey.rang().orElse(null);

            if (isDisplayed && isWithoutAutoConsumed) {
                //Effective seasonal crop
                Double valueWithoutAutoConsume = withoutWithAutoConsumeValues[WITHOUT_AUTO_CONSUME_INDEX];
                writer.writeEffectiveSeasonalCrop(
                        its,
                        irs,
                        domain.getCampaign(),
                        getIndicatorCategory(),
                        getIndicatorLabel(WITHOUT_AUTO_CONSUME_INDEX),
                        valueWithoutAutoConsume,
                        cropYealdAverage,
                        reliability,
                        comments,
                        domain,
                        optionnalAnoGrowingSystem,
                        plot,
                        croppingPlanEntry,
                        rank,
                        previousCroppingPlanEntry,
                        this.getClass()
                );
            }

            if (isDisplayed && isWithAutoConsumed) {
                //Effective seasonal crop
                Double valueWithAutoConsume = withoutWithAutoConsumeValues[WITH_AUTO_CONSUME_INDEX];
                writer.writeEffectiveSeasonalCrop(
                        its,
                        irs,
                        domain.getCampaign(),
                        getIndicatorCategory(),
                        getIndicatorLabel(WITH_AUTO_CONSUME_INDEX),
                        valueWithAutoConsume,
                        cropYealdAverage,
                        reliability,
                        comments,
                        domain,
                        optionnalAnoGrowingSystem,
                        plot,
                        croppingPlanEntry,
                        rank,
                        previousCroppingPlanEntry,
                        this.getClass()
                );
            }
        }
    }

    @Override
    public void computeEffective(IndicatorWriter writer,
                                 PerformanceEffectiveDomainExecutionContext domainContext,
                                 Optional<GrowingSystem> optionalGrowingSystem,
                                 PerformancePlotExecutionContext plotContext) {

        Plot anonymisePlot = plotContext.getAnonymizePlot();

        Double[] weightedValueSum = null;
        Double[] weightSum = null;

        Integer reliabilityPlotTotal = 0;
        Integer reliabilityPlotMissingFileld = 0;
        Set<MissingFieldMessage> plotMissingFieldMessages = new HashSet<>();

        Set<String> zoneTopiaIds = new HashSet<>();
        // moyenne pondérée sur la surface
        for (Map.Entry<Zone, Double[]> entry : effectiveWithoutWithAutoconsumeZoneValues.entrySet()) {
            Zone zone = entry.getKey();
            zoneTopiaIds.add(zone.getTopiaId());
            Double[] withoutWithAutoConsumeValues = entry.getValue();

            reliabilityPlotTotal += effectiveZoneReliabilityTotalCounter.get(zone);
            reliabilityPlotMissingFileld += effectiveZoneReliabilityFieldErrorCounter.get(zone);

            Set<MissingFieldMessage> fieldMessages = effectiveZoneFieldsErrors.getOrDefault(zone, new HashSet<>());
            Set<MissingFieldMessage> zoneMissingFieldMessages = fieldMessages.stream()
                    .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.PLOT).isPresent())
                    .collect(Collectors.toSet());
            plotMissingFieldMessages.addAll(zoneMissingFieldMessages);

            if (weightedValueSum == null) {
                weightedValueSum = newArray(withoutWithAutoConsumeValues.length, 0.0);
                weightSum = newArray(withoutWithAutoConsumeValues.length, 0.0);
            }

            // zoneArea
            reliabilityPlotTotal += 1;
            for (int i = 0; i < withoutWithAutoConsumeValues.length; i++) {
                weightedValueSum[i] += withoutWithAutoConsumeValues[i] == null ? 0 : withoutWithAutoConsumeValues[i] * zone.getArea();
                weightSum[i] += zone.getArea();
            }
        }

        // Apply the plot area weighted.
        if (weightSum != null) {
            String zoneIds = String.join(", ", zoneTopiaIds);
            Map<Pair<RefDestination, YealdUnit>, Double> plotYealdAverages = plotContext.getPlotYealdAverages();

            String its = plotContext.getIts();
            String irs = plotContext.getIrs();

            Integer plotReliability = computeReliabilityIndex(reliabilityPlotMissingFileld, reliabilityPlotTotal);
            effectivePlotReliabilityFieldErrorCounter.put(anonymisePlot, reliabilityPlotMissingFileld);
            effectivePlotReliabilityTotalCounter.put(anonymisePlot, reliabilityPlotTotal);

            effectivePlotWithoutWithAutoConsumeValuesValues.put(anonymisePlot, newArray(LABELS.length, 0.0d));

            Double valueWithoutAutoConsume = weightedValueSum[WITHOUT_AUTO_CONSUME_INDEX];
            Double valueWithAutoConsume = weightedValueSum[WITH_AUTO_CONSUME_INDEX];

            Double plotValueWithoutAutoConsume = weightSum[WITHOUT_AUTO_CONSUME_INDEX] == 0 ? 0 : valueWithoutAutoConsume / weightSum[WITHOUT_AUTO_CONSUME_INDEX];
            Double plotValueWithAutoConsume = weightSum[WITH_AUTO_CONSUME_INDEX] == 0 ? 0 : valueWithAutoConsume / weightSum[WITH_AUTO_CONSUME_INDEX];

            String comments;
            if (CollectionUtils.isNotEmpty(plotMissingFieldMessages)) {
                comments = plotMissingFieldMessages.stream()
                        .map(MissingFieldMessage::getMessage)
                        .collect(Collectors.joining(","));
            } else {
                comments = RELIABILITY_INDEX_NO_COMMENT;
            }

            // Plot
            if (isDisplayed && isWithoutAutoConsumed) {
                writer.writeEffectivePlot(
                        its,
                        irs,
                        getIndicatorCategory(),
                        getIndicatorLabel(WITHOUT_AUTO_CONSUME_INDEX),
                        plotValueWithoutAutoConsume,
                        plotReliability,
                        comments,
                        domainContext.getAnonymiseDomain(), domainContext.getCroppingPlanSpecies(), optionalGrowingSystem,
                        anonymisePlot,
                        plotYealdAverages,
                        this.getClass(),
                        zoneIds);
            }

            if (isDisplayed && isWithAutoConsumed) {
                writer.writeEffectivePlot(
                        its,
                        irs,
                        getIndicatorCategory(),
                        getIndicatorLabel(WITH_AUTO_CONSUME_INDEX),
                        plotValueWithAutoConsume,
                        plotReliability,
                        comments,
                        domainContext.getAnonymiseDomain(), domainContext.getCroppingPlanSpecies(), optionalGrowingSystem,
                        anonymisePlot,
                        plotYealdAverages,
                        this.getClass(),
                        zoneIds);
            }

            // sum
            Double[] plotValues = effectivePlotWithoutWithAutoConsumeValuesValues.get(anonymisePlot);
            plotValues[WITHOUT_AUTO_CONSUME_INDEX] = plotValueWithoutAutoConsume;
            plotValues[WITH_AUTO_CONSUME_INDEX] = plotValueWithAutoConsume;

        }
    }

    @Override
    public void resetEffectiveCC() {
        // cleanup for next iteration
        effectiveCroppingReliabilityTotalCounter.clear();
        effectiveCroppingReliabilityFieldErrorCounter.clear();
        effectiveCroppingWithoutWithAutoConsumeValues.clear();
        effectiveCropsYealdAverage.clear();
        effectiveCroppingFieldsErrors.clear();
    }

    @Override
    public void resetEffectiveZones() {
        effectiveWithoutWithAutoconsumeZoneValues.clear();
        effectiveZoneReliabilityFieldErrorCounter.clear();
        effectiveZoneReliabilityTotalCounter.clear();
        effectiveZoneFieldsErrors.clear();
    }

    @Override
    public void computeEffective(IndicatorWriter writer,
                                 PerformanceEffectiveDomainExecutionContext domainContext,
                                 PerformanceGrowingSystemExecutionContext growingSystemContext) {

        // Parcelle >> SdC
        // plot: moyenne pondérée sur la surface
        Double[] weightedValueSum = null;
        Double[] weightSum = null;

        Integer reliabilityMissingFileldCounter = 0;
        Integer reliabilityTotalCounter = 0;

        Domain anonymiseDomain = domainContext.getAnonymiseDomain();
        Collection<CroppingPlanSpecies> domainAgrosystSpecies = domainContext.getCroppingPlanSpecies();

        for (Map.Entry<Plot, Double[]> entry : effectivePlotWithoutWithAutoConsumeValuesValues.entrySet()) {
            Plot plot = entry.getKey();
            Double[] withoutWithAutoConsume = entry.getValue();

            reliabilityMissingFileldCounter += effectivePlotReliabilityFieldErrorCounter.get(plot);
            reliabilityTotalCounter += effectivePlotReliabilityTotalCounter.get(plot);

            if (weightedValueSum == null) {
                weightedValueSum = newArray(withoutWithAutoConsume.length, 0.0);
                weightSum = newArray(withoutWithAutoConsume.length, 0.0);
            }

            // plotArea
            reliabilityTotalCounter += 1;

            for (int i = 0; i < withoutWithAutoConsume.length; i++) {
                weightedValueSum[i] += withoutWithAutoConsume[i] * plot.getArea();
                weightSum[i] += plot.getArea();
            }
        }

        Optional<GrowingSystem> anonymizedGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();

        if (weightSum != null && anonymizedGrowingSystem.isPresent()) {
            Integer reliability = computeReliabilityIndex(reliabilityMissingFileldCounter, reliabilityTotalCounter);

            String its = growingSystemContext.getIts();
            String irs = growingSystemContext.getIrs();

            effectiveGrowingSystemReliabilityFieldErrorCounter.put(anonymizedGrowingSystem, reliabilityMissingFileldCounter);
            effectiveGrowingSystemReliabilityTotalCounter.put(anonymizedGrowingSystem, reliabilityTotalCounter);
            String comments = getMessagesForScope(MissingMessageScope.GROWING_SYSTEM);

            Double standardisedPriceWithoutAutoConsumed = weightedValueSum[WITHOUT_AUTO_CONSUME_INDEX];
            Double standardisedPriceWithAutoConsumed = weightedValueSum[WITH_AUTO_CONSUME_INDEX];

            // Effective SDC
            Double plotValueWithoutAutoConsume = weightSum[WITHOUT_AUTO_CONSUME_INDEX] == 0 ? 0 : standardisedPriceWithoutAutoConsumed / weightSum[WITHOUT_AUTO_CONSUME_INDEX];
            if (isDisplayed && isWithoutAutoConsumed) {
                writer.writeEffectiveGrowingSystem(
                        its,
                        irs,
                        getIndicatorCategory(),
                        getIndicatorLabel(WITHOUT_AUTO_CONSUME_INDEX),
                        plotValueWithoutAutoConsume,
                        reliability,
                        comments,
                        anonymiseDomain,
                        anonymizedGrowingSystem,
                        this.getClass(),
                        domainAgrosystSpecies
                );
            }

            Double plotValueWithAutoConsume = weightSum[WITH_AUTO_CONSUME_INDEX] == 0 ? 0 : standardisedPriceWithAutoConsumed / weightSum[WITH_AUTO_CONSUME_INDEX];
            if (isDisplayed && isWithAutoConsumed) {
                writer.writeEffectiveGrowingSystem(
                        its,
                        irs,
                        getIndicatorCategory(),
                        getIndicatorLabel(WITH_AUTO_CONSUME_INDEX),
                        plotValueWithAutoConsume,
                        reliability,
                        comments,
                        anonymiseDomain,
                        anonymizedGrowingSystem,
                        this.getClass(),
                        domainAgrosystSpecies
                );
            }

            Double[] effectiveGrowingSystemWithoutWithValues = {plotValueWithoutAutoConsume, plotValueWithAutoConsume};
            effectiveStandardizedGrossIncomeGrowingSystemWithoutWithValues.put(anonymizedGrowingSystem, effectiveGrowingSystemWithoutWithValues);
        }

    }

    @Override
    public void resetEffectivePlots() {
        // cleanup for next iteration
        effectivePlotWithoutWithAutoConsumeValuesValues.clear();
        effectivePlotReliabilityFieldErrorCounter.clear();
        effectivePlotReliabilityTotalCounter.clear();
    }

    @Override
    public void computeEffective(IndicatorWriter writer, Domain domain) {
        Pair<Double, Double> weightedValueSumWithoutWithAutoConsume = null;
        Double weightSum = null;

        String reliabilityComment = null;
        Integer domaineRmfc = 0;
        Integer domaineRtfc = 0;

        List<String> gstc = new ArrayList<>();

        // moyenne pondérée sur la surface
        for (Map.Entry<Optional<GrowingSystem>, Double[]> entry : effectiveStandardizedGrossIncomeGrowingSystemWithoutWithValues.entrySet()) {
            Optional<GrowingSystem> optionalGrowingSystem = entry.getKey();

            if (optionalGrowingSystem.isEmpty()) {
                continue;
            }

            GrowingSystem growingSystem = entry.getKey().get();

            Double[] withoutWithAutoConsumeValues = entry.getValue();
            if (growingSystem.getTypeAgriculture() != null) {
                gstc.add(growingSystem.getTypeAgriculture().getReference_label());
            }

            domaineRmfc += effectiveGrowingSystemReliabilityFieldErrorCounter.get(optionalGrowingSystem);
            domaineRtfc += effectiveGrowingSystemReliabilityTotalCounter.get(optionalGrowingSystem);

            // AffectedAreaRate
            domaineRtfc += 1;

            Double affectedAreaRate = growingSystem.getAffectedAreaRate();
            if (affectedAreaRate == null) {
                if (reliabilityComment == null) {
                    reliabilityComment = "Pourcentage de surface du domaine affectée sur système de culture";
                }
                domaineRmfc += 1;
                affectedAreaRate = DEFAULT_AFFECTED_AREA_RATE;
            }

            weightedValueSumWithoutWithAutoConsume = weightedValueSumWithoutWithAutoConsume == null ?
                    Pair.of(withoutWithAutoConsumeValues[WITHOUT_AUTO_CONSUME_INDEX] * affectedAreaRate, withoutWithAutoConsumeValues[WITH_AUTO_CONSUME_INDEX] * affectedAreaRate)
                    : Pair.of(weightedValueSumWithoutWithAutoConsume.getLeft() + withoutWithAutoConsumeValues[WITHOUT_AUTO_CONSUME_INDEX] * affectedAreaRate,
                    weightedValueSumWithoutWithAutoConsume.getRight() + withoutWithAutoConsumeValues[WITH_AUTO_CONSUME_INDEX] * affectedAreaRate);

            weightSum = weightSum == null ? affectedAreaRate : weightSum + affectedAreaRate;
        }

        if (weightSum != null && isDisplayed) {

            Integer domaineReliability = computeReliabilityIndex(domaineRmfc, domaineRtfc);

            if (reliabilityComment == null) {
                reliabilityComment = "";
            }

            if (weightSum != 0) {
                // Effective Domain
                Pair<Double, Double> domainWithoutWithValues = Pair.of(weightedValueSumWithoutWithAutoConsume.getLeft() / weightSum, weightedValueSumWithoutWithAutoConsume.getRight() / weightSum);
                if (isWithoutAutoConsumed) {
                    writer.writeEffectiveDomain(
                            getIndicatorCategory(),
                            getIndicatorLabel(WITHOUT_AUTO_CONSUME_INDEX),
                            domainWithoutWithValues.getLeft(),
                            domaineReliability,
                            reliabilityComment,
                            domain,
                            String.join(", ", gstc)
                    );
                }
                if (isWithAutoConsumed) {
                    writer.writeEffectiveDomain(
                            getIndicatorCategory(),
                            getIndicatorLabel(WITH_AUTO_CONSUME_INDEX),
                            domainWithoutWithValues.getRight(),
                            domaineReliability,
                            reliabilityComment,
                            domain,
                            String.join(", ", gstc)
                    );
                }

            } else {
                if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn("Can't compute growing system scale with 0 weigth");
                }
            }
        }
    }

    @Override
    public void resetEffectiveGrowingSystems() {
        // cleanup for next iteration
        effectiveStandardizedGrossIncomeGrowingSystemWithoutWithValues.clear();
        effectiveGrowingSystemReliabilityFieldErrorCounter.clear();
        effectiveGrowingSystemReliabilityTotalCounter.clear();
        targetedErrorFieldMessages.clear();
    }

    public void init(IndicatorFilter grossIncomeIndicatorFilter) {
        isDisplayed = grossIncomeIndicatorFilter != null && grossIncomeIndicatorFilter.getComputeStandardized();
        if (isDisplayed) {
            isWithAutoConsumed = grossIncomeIndicatorFilter.getWithAutoConsumed() != null && grossIncomeIndicatorFilter.getWithAutoConsumed();
            isWithoutAutoConsumed = grossIncomeIndicatorFilter.getWithoutAutoConsumed() != null && grossIncomeIndicatorFilter.getWithoutAutoConsumed();
        }
    }

    @Getter
    private static class ValorisationPrices {
        private final List<Double> withAutoConsumePrices = new ArrayList<>();
        private final List<Double> withoutAutoConsumePrices = new ArrayList<>();

        public void addWithAutoConsumePrice(double price) {
            withAutoConsumePrices.add(price);
        }

        public void addWithoutAutoConsumePrice(double price) {
            withoutAutoConsumePrices.add(price);
        }
    }
}
