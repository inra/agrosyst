package fr.inra.agrosyst.services.async;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.async.Task;
import fr.inra.agrosyst.services.ServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.time.Duration;
import java.util.UUID;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public abstract class AbstractTaskRunner<T extends Task, R> implements TaskRunner<T> {

    private static final Log LOG = LogFactory.getLog(AbstractTaskRunner.class);

    /**
     * This method should be wrapped in a transaction.
     * It processes the file.
     */
    protected abstract R executeTask(T task, ServiceContext serviceContext) throws Exception;

    @Override
    public void runTask(T task, ServiceContext serviceContext) {
        try {
            long start = System.currentTimeMillis();
            R taskResult = executeTask(task, serviceContext);
            long end = System.currentTimeMillis();
            Duration duration = Duration.ofMillis(end - start);
            taskSucceeded(task, serviceContext, taskResult, duration);
        } catch (Exception eee) {
            if (LOG.isErrorEnabled()) {
                UUID taskId = task.getTaskId();
                String exceptionName = eee.getClass().getSimpleName();
                String description = task.getDescription();
                String message = String.format("[t=%s] %s during async task (%s)", taskId, exceptionName, description);
                LOG.error(message, eee);
            }
            taskFailed(task, serviceContext, eee);
        }
    }

    public boolean cancelTask(Task task) {
        return false;
    }

    protected void taskSucceeded(T task, ServiceContext serviceContext, R result, Duration duration) {
        if (LOG.isWarnEnabled()) {
            String message = String.format(
                    "[t=%s] Task succeeded in %ds but result is ignored. You should override method `taskSucceeded` in `%s` and use the produced result",
                    task.getTaskId(),
                    duration.getSeconds(),
                    getClass().getSimpleName());
            LOG.warn(message);
        }
    }

    protected void taskFailed(T task, ServiceContext serviceContext, Exception eee) {
        if (LOG.isWarnEnabled()) {
            String message = String.format(
                    "[t=%s] Task failed with %s. You should override method `taskFailed` in `%s` and deal with the exception",
                    task.getTaskId(),
                    eee.getMessage(),
                    getClass().getSimpleName());
            LOG.warn(message);
        }
    }

}
