package fr.inra.agrosyst.services;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.inra.agrosyst.api.entities.AgrosystTopiaDaoSupplier;
import fr.inra.agrosyst.api.entities.AgrosystTopiaPersistenceContext;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.ServiceFactory;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.commons.gson.AgrosystGsonSupplier;
import fr.inra.agrosyst.services.async.BusinessTasksManager;
import fr.inra.agrosyst.services.internal.InstancesSynchroHelper;
import fr.inra.agrosyst.services.security.SecurityContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaTransaction;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.function.Supplier;

public class DefaultServiceContext implements ServiceContext {

    protected ServiceFactory serviceFactory;

    protected final Supplier<AgrosystTopiaPersistenceContext> transactionSupplier;

    protected AgrosystTopiaPersistenceContext persistenceContext;

    protected final AgrosystServiceConfig config;

    protected final InstancesSynchroHelper synchroHelper;

    protected final BusinessTasksManager taskManager;

    // private for security reasons
    private final AuthenticatedUser authenticatedUser;
    private SecurityContext securityContext;

    public DefaultServiceContext(
            AgrosystServiceConfig config,
            Supplier<AgrosystTopiaPersistenceContext> transactionSupplier,
            InstancesSynchroHelper synchroHelper,
            BusinessTasksManager taskManager,
            AuthenticatedUser authenticatedUser) {
        this.config = config;
        this.transactionSupplier = transactionSupplier;
        this.synchroHelper = synchroHelper;
        this.taskManager = taskManager;
        this.authenticatedUser = authenticatedUser;
    }

    public DefaultServiceContext(
            AgrosystServiceConfig config,
            Supplier<AgrosystTopiaPersistenceContext> transactionSupplier,
            InstancesSynchroHelper synchroHelper,
            BusinessTasksManager taskManager) {
        this(config, transactionSupplier, synchroHelper, taskManager, null);
    }

    @Override
    public ServiceContext newServiceContext() {
        ServiceContext serviceContext = new DefaultServiceContext(config, transactionSupplier, synchroHelper, taskManager, this.authenticatedUser);
        return serviceContext;
    }

    @Override
    public ServiceContext newServiceContext(AuthenticatedUser authenticatedUser) {
        ServiceContext serviceContext = new DefaultServiceContext(config, transactionSupplier, synchroHelper, taskManager, authenticatedUser);
        return serviceContext;
    }

    @Override
    public LocalDate getCurrentDate() {
        return LocalDate.now();
    }

    @Override
    public LocalDateTime getCurrentTime() {
        return LocalDateTime.now();
    }

    @Override
    public OffsetDateTime getOffsetTime() {
        return OffsetDateTime.now();
    }

    @Override
    public TopiaTransaction getTransaction() {
        TopiaTransaction result = getTransaction(true);
        return result;
    }

    @Override
    public TopiaTransaction getTransaction(boolean create) {
        TopiaTransaction result = getTransaction0(create);
        return result;
    }

    @Override
    public AgrosystTopiaDaoSupplier getDaoSupplier() {
        AgrosystTopiaDaoSupplier result = getTransaction0(true);
        return result;
    }

    @Override
    public AgrosystTopiaPersistenceContext getPersistenceContext() {
        AgrosystTopiaPersistenceContext result = getTransaction0(true);
        return result;
    }

    @Override
    public AgrosystTopiaPersistenceContext getPersistenceContext(boolean create) {
        AgrosystTopiaPersistenceContext result = getTransaction0(create);
        return result;
    }

    @Override
    public void close() {
        // try to close non closed persistence context (false to not reopen it)
        AgrosystTopiaPersistenceContext context = getPersistenceContext(false);
        if (context != null) {
            context.close();
        }
    }

    protected AgrosystTopiaPersistenceContext getTransaction0(boolean create) {
        if (persistenceContext == null && create) {
            try {
                persistenceContext = transactionSupplier.get();
            } catch (TopiaException ex) {
                throw new AgrosystTechnicalException("Can't begin new transaction", ex);
            }
        }
        return persistenceContext;
    }

    public ServiceFactory getServiceFactory() {
        if (serviceFactory == null) {
            serviceFactory = new DefaultServiceFactory(this);
        }
        return serviceFactory;
    }

    @Override
    public <E extends AgrosystService> E newService(Class<E> clazz) {
        E result = getServiceFactory().newService(clazz);
        return result;
    }
    
    @Override
    public <E extends AgrosystService, F extends AgrosystService> F newExpectedService(Class<E> clazz, Class<F> expectedClazz) {
        F result = getServiceFactory().newExpectedService(clazz, expectedClazz);
        return result;
    }
    @Override
    public AgrosystServiceConfig getConfig() {
        return config;
    }

    @Override
    public SecurityContext getSecurityContext() {
        if (securityContext == null) {
            securityContext = new SecurityContext(getServiceFactory(), authenticatedUser);
        }
        return securityContext;
    }

    @Override
    public SecurityContext getSecurityContextAsUser(String userId) {
        SecurityContext securityContext = new SecurityContext(getServiceFactory(), authenticatedUser, userId);
        return securityContext;
    }

    @Override
    public <I> I newInstance(Class<I> clazz) {
        return getServiceFactory().newInstance(clazz);
    }

    @Override
    public Gson getGson() {
        return new AgrosystGsonSupplier().get();
    }

    @Override
    public InstancesSynchroHelper getSynchroHelper() {
        return this.synchroHelper;
    }

    @Override
    public BusinessTasksManager getTaskManager() {
        return this.taskManager;
    }
}
