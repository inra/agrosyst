package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefPrixPhyto;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhytoImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefPrixPhytoModel extends AbstractDestinationAndPriceModel<RefPrixPhyto> implements ExportModel<RefPrixPhyto> {

    public RefPrixPhytoModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Id produit", RefPrixPhyto.PROPERTY_ID_PRODUIT);
        newMandatoryColumn("Nom produit", RefPrixPhyto.PROPERTY_NOM_PRODUIT);
        newMandatoryColumn("Id traitement", RefPrixPhyto.PROPERTY_ID_TRAITEMENT, INT_PARSER);
        newMandatoryColumn("Prix", RefPrixPhyto.PROPERTY_PRICE, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("Unité", RefPrixPhyto.PROPERTY_UNIT, PRICE_UNIT_PARSER);
        newMandatoryColumn("Code AMM", RefPrixPhyto.PROPERTY_CODE__AMM);
        newMandatoryColumn("Code scénario", RefPrixPhyto.PROPERTY_CODE_SCENARIO);
        newMandatoryColumn("Scénario", RefPrixPhyto.PROPERTY_SCENARIO);
        newMandatoryColumn("Campagne", RefPrixPhyto.PROPERTY_CAMPAIGN, INTEGER_WITH_NULL_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefPrixPhyto.PROPERTY_ACTIVE, ACTIVE_PARSER);
        newOptionalColumn("Source", RefPrixPhyto.PROPERTY_SOURCE);
    }

    @Override
    public Iterable<ExportableColumn<RefPrixPhyto, Object>> getColumnsForExport() {
        ModelBuilder<RefPrixPhyto> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Id produit", RefPrixPhyto.PROPERTY_ID_PRODUIT);
        modelBuilder.newColumnForExport("Nom produit", RefPrixPhyto.PROPERTY_NOM_PRODUIT);
        modelBuilder.newColumnForExport("Id traitement", RefPrixPhyto.PROPERTY_ID_TRAITEMENT, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Prix", RefPrixPhyto.PROPERTY_PRICE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Unité", RefPrixPhyto.PROPERTY_UNIT, PRICE_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("Code AMM", RefPrixPhyto.PROPERTY_CODE__AMM);
        modelBuilder.newColumnForExport("Code scénario", RefPrixPhyto.PROPERTY_CODE_SCENARIO);
        modelBuilder.newColumnForExport("Scénario", RefPrixPhyto.PROPERTY_SCENARIO);
        modelBuilder.newColumnForExport("Campagne", RefPrixPhyto.PROPERTY_CAMPAIGN, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefPrixPhyto.PROPERTY_ACTIVE, T_F_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefPrixPhyto.PROPERTY_SOURCE);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefPrixPhyto newEmptyInstance() {
        RefPrixPhyto result = new RefPrixPhytoImpl();
        result.setActive(true);
        return result;
    }
}
