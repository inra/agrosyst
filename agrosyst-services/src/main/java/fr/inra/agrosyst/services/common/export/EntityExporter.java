package fr.inra.agrosyst.services.common.export;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.BinaryContent;
import fr.inra.agrosyst.api.services.common.ExportResult;
import org.apache.commons.compress.archivers.zip.Zip64Mode;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.SheetVisibility;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * XLS exporter utility.
 *
 * @author Eric Chatellier
 */
public class EntityExporter {

    private static final Log log = LogFactory.getLog(EntityExporter.class);

    /**
     * Nombre de lignes par feuille après lequel on déclenche le auto-size
     */
    private static final int AUTO_SIZE_COLUMNS_THRESHOLD = 42;

    // XXX AThimel 10/03/2021 : Peut-être mettre un préfixe plus différenciant tel que "__list_" ?
    public static final String DROPDOWN_SHEET_PREFIX = "_";

    /**
     * Export given export models as an XLSX file
     *
     * @param fileNameWithoutExtension The name of the output file
     * @param sheetsNG A list of models (each model represents an XLSX sheet) and elements (rows) for each one
     * @return the generated export
     */
    public ExportResult exportAsXlsx(String fileNameWithoutExtension, ExportModelAndRows<?> ... sheetsNG) {
        List<ExportModelAndRows<?>> sheets = Arrays.asList(sheetsNG);

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            // technical export
            this.exportAsXlsStream0(sheets, outputStream);

            BinaryContent binaryContent = BinaryContent.forXlsx(outputStream.toByteArray());
            ExportResult result = new ExportResult(binaryContent, fileNameWithoutExtension);
            return result;
        } catch (Exception eee) {
            throw new AgrosystTechnicalException("Can't generate file", eee);
        }
    }

    /**
     * Export map as single list data per xls sheet.
     *
     * @param sheets       A list of models (each model represents an XLSX sheet) and elements (rows) for each one
     * @param outputStream The stream in which data is written
     */
    protected void exportAsXlsStream0(List<ExportModelAndRows<?>> sheets,
                                      OutputStream outputStream) {

        try (SXSSFWorkbook workbook = new SXSSFWorkbook()) {
            workbook.setZip64Mode(Zip64Mode.Never);// see https://bugs.documentfoundation.org/show_bug.cgi?id=162944
            // Get the workbook instance for XLS file

            CellStyle boldStyle = workbook.createCellStyle(); // bold
            Font font = workbook.createFont();
            font.setBold(true);
            boldStyle.setFont(font);
            CellStyle decimalStyle = workbook.createCellStyle(); // two decimals
            decimalStyle.setDataFormat(workbook.createDataFormat().getFormat("0.##"));

            long writeDocumentStart = System.currentTimeMillis();

            DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(ExportUtils.DATE_FORMAT);
            for (ExportModelAndRows<?> sheetNG : sheets) {
                ExportModel<?> model = sheetNG.getModel();
                List values = sheetNG.getRows();

                writeSheetNG(workbook, boldStyle, decimalStyle, dateFormat, model, values);
            }

            // write file
            workbook.write(outputStream);

            if (log.isInfoEnabled()) {
                long writeDocumentEnd = System.currentTimeMillis();
                String message = String.format(
                        "%d sheets written in %dms",
                        sheets.size(),
                        writeDocumentEnd - writeDocumentStart);
                log.info(message);
            }

        } catch (IOException ex) {
            throw new AgrosystTechnicalException("Can't write XLS file", ex);
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't generate file content", ex);
        }
    }

    protected Runnable newAutoSizeRunnable(Sheet sheet, int columnCount) {
        Runnable autoSizeColumns = new Runnable() {
            /**
             * Flag permettant de ne pas effectuer 2 fois le resize
             */
            boolean autoResized = false;
            @Override
            public void run() {
                if (!autoResized) {
                    // auto size all columns
                    for (int column = 0; column < columnCount; column++) {
                        sheet.autoSizeColumn(column);
                    }
                    // Maintenant que l'autoSize est fait on désactive le tracking pour les perfs
                    if (sheet instanceof SXSSFSheet) {
                        ((SXSSFSheet) sheet).untrackAllColumnsForAutoSizing();
                    }
                    autoResized = true;
                }
            }
        };
        return autoSizeColumns;
    }

    protected <T> void writeSheetNG(Workbook workbook,
                                    CellStyle boldStyle,
                                    CellStyle decimalStyle,
                                    DateTimeFormatter dateFormat,
                                    ExportModel<T> model,
                                    List<T> values) {
        long writeSheetStart = System.currentTimeMillis();

        // Get first sheet from the workbook
        Sheet sheet = workbook.createSheet(model.getTitle());

        Map<String, String> declaredColumns = Maps.uniqueIndex(model.getColumns().keySet(), key -> key);

        Multimap<String, String> dropDowns = model.getDropDowns();

        writerHeaderRow(workbook, boldStyle, sheet, values.size(), declaredColumns, dropDowns);

        writeDataRowNG(decimalStyle, dateFormat, sheet, values, model.getColumns());

        if (log.isDebugEnabled()) {
            long writeSheetEnd = System.currentTimeMillis();
            String message = String.format(
                    "%d rows written in sheet « %s » (%d columns) in %dms",
                    Iterables.size(values) + 1,
                    model.getTitle(),
                    declaredColumns.size(),
                    writeSheetEnd - writeSheetStart);
            log.debug(message);
        }
    }

    protected <T> void writeDataRowNG(CellStyle decimalStyle,
                                      DateTimeFormatter dateFormat,
                                      Sheet sheet,
                                      List<T> values,
                                      Map<String, Function<T, ?>> columns) {

        Runnable autoSizeColumns = newAutoSizeRunnable(sheet, columns.size());

        for (int i = 0; i < values.size(); i++) {

            T entity = values.get(i);

            // write data
            Row row = sheet.createRow(i + 1); // + header
            int columnIndex = 0;
            for (Map.Entry<String, Function<T, ?>> column : columns.entrySet()) {
                Cell cell = row.createCell(columnIndex++);

                Function<T, ?> valueGetter = column.getValue();
                Object value = valueGetter.apply(entity);

                pushToCell(decimalStyle, dateFormat, cell, value);
            }

            // Si on atteint le nombre de lignes seuil, on procède au redimensionnement des colonnes
            if (i == AUTO_SIZE_COLUMNS_THRESHOLD) {
                autoSizeColumns.run();
            }
        }

        // On le rappelle à la fin au cas où on ait pas atteint le seuil. Le Runnable ne fera pas 2 fois le traitement
        autoSizeColumns.run();
    }

    protected void pushToCell(CellStyle decimalStyle,
                              DateTimeFormatter dateFormat,
                              Cell cell,
                              Object value) {
        if (value != null) {
            switch (value) {
                case Boolean b -> {
                    if (b) {
                        cell.setCellValue("Oui");
                    } else {
                        cell.setCellValue("Non");
                    }
                }
                case TemporalAccessor temporalAccessor -> cell.setCellValue(dateFormat.format(temporalAccessor));
                case Number number -> {
                    cell.setCellValue(number.doubleValue());
                    cell.setCellStyle(decimalStyle);
                }
                default -> cell.setCellValue(value.toString());
            }
        }
    }

    protected String columnKeyToSheetName(String columnKey) {
        String result = StringUtils.trimToEmpty(columnKey);
        result = result.replaceAll("[ -.]", "_");
        result = result.toLowerCase();
        result = StringUtils.stripAccents(result);
        result = WorkbookUtil.createSafeSheetName(DROPDOWN_SHEET_PREFIX + result);
        return result;
    }

    protected void writerHeaderRow(Workbook workbook,
                                   CellStyle boldStyle,
                                   Sheet sheet,
                                   int rowCount,
                                   Map<String, String> declaredColumns,
                                   Multimap<String, String> dropDowns) {

        // write headers
        Row headerRow = sheet.createRow(0);
        int columnIndex = 0;
        for (Map.Entry<String, String> displayEntry : declaredColumns.entrySet()) {
            String columnKey = displayEntry.getKey();
            Cell headerCell = headerRow.createCell(columnIndex);
            String columnTitle = displayEntry.getValue();
            headerCell.setCellValue(columnTitle);
            headerCell.setCellStyle(boldStyle);

            // add drop down list data validation if property support it
            if (dropDowns.containsKey(columnKey)) {
                Collection<String> dropDownValues = dropDowns.get(columnKey);
                setupDropDown(workbook, sheet, columnIndex, rowCount, columnKey, dropDownValues);
            }

            columnIndex++;
        }

        // to be able to auto size the column with SXSSFSheet
        if (sheet instanceof SXSSFSheet) {
            ((SXSSFSheet)sheet).trackAllColumnsForAutoSizing();
        }
    }

    protected void setupDropDown(Workbook workbook,
                                 Sheet sheet,
                                 int columnIndex,
                                 int rowCount,
                                 String columnKey,
                                 Collection<String> dropDownValues) {

        int rowIndex = 0;
        // create this into a hidden sheet (if not already exists)
        final String dropDownSheetName = columnKeyToSheetName(columnKey);
        Sheet currentSheet = workbook.getSheet(dropDownSheetName);
        if (currentSheet == null) {
            currentSheet = workbook.createSheet(dropDownSheetName);
            int index = workbook.getSheetIndex(dropDownSheetName);
            workbook.setSheetVisibility(index, SheetVisibility.HIDDEN);
            // add values into it
            for (String value : dropDownValues) {
                Row valueRow = currentSheet.createRow(rowIndex);
                Cell valueCell = valueRow.createCell(0);
                valueCell.setCellValue(value);
                rowIndex++;
            }
        } else {
            // le referentiel a déja été ajouté une fois, on suppose
            // qu'il a la même taille et on le réutilise
            rowIndex = currentSheet.getLastRowNum() + 1;
        }

        // add column validation
        if (rowIndex >= 1 && rowCount >= 1) {
            String range = dropDownSheetName + "!$A$1:$A$" + rowIndex;
            DataValidationHelper dvHelper = currentSheet.getDataValidationHelper();
            DataValidationConstraint dvConstraint = dvHelper.createFormulaListConstraint(range);
            CellRangeAddressList list = new CellRangeAddressList(1, rowCount, columnIndex, columnIndex);
            DataValidation dataValidation = dvHelper.createValidation(dvConstraint, list);
            sheet.addValidationData(dataValidation);
        }
    }

}
