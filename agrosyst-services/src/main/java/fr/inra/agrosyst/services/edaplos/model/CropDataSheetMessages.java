package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import jakarta.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Classes non présente dans le model AgroEDI regroupant les CropDataSheetMessage chargé.
 */
public class CropDataSheetMessages implements AgroEdiObject {

    @Valid
    protected final List<CropDataSheetMessage> cropDataSheetMessages = new ArrayList<>();

    public List<CropDataSheetMessage> getCropDataSheetMessages() {
        return cropDataSheetMessages;
    }

    public void addCropDataSheetMessage(CropDataSheetMessage cropDataSheetMessage) {
        cropDataSheetMessages.add(cropDataSheetMessage);
    }

    @Override
    public String getLocalizedIdentifier() {
        return "";
    }
}
