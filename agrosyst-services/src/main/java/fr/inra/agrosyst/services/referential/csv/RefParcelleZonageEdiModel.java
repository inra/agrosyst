package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDIImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Modele d'import du référentiel parcelle zonage EDI
 * 
 * <ul>
 * <li>code engagement parcelle
 * <li>libelle engagement parcelle
 * <li>source
 * </ul>
 * 
 * @author Eric Chatellier
 */
public class RefParcelleZonageEdiModel extends AbstractAgrosystModel<RefParcelleZonageEDI> implements ExportModel<RefParcelleZonageEDI> {

    public RefParcelleZonageEdiModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("code engagement parcelle", RefParcelleZonageEDI.PROPERTY_CODE_ENGAGEMENT_PARCELLE);
        newMandatoryColumn("libelle engagement parcelle", RefParcelleZonageEDI.PROPERTY_LIBELLE_ENGAGEMENT_PARCELLE);
        newMandatoryColumn("source", RefParcelleZonageEDI.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefParcelleZonageEDI.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefParcelleZonageEDI, Object>> getColumnsForExport() {
        ModelBuilder<RefParcelleZonageEDI> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("code engagement parcelle", RefParcelleZonageEDI.PROPERTY_CODE_ENGAGEMENT_PARCELLE);
        modelBuilder.newColumnForExport("libelle engagement parcelle", RefParcelleZonageEDI.PROPERTY_LIBELLE_ENGAGEMENT_PARCELLE);
        modelBuilder.newColumnForExport("source", RefParcelleZonageEDI.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefParcelleZonageEDI.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefParcelleZonageEDI newEmptyInstance() {
        RefParcelleZonageEDI refParcelleZonageEDI = new RefParcelleZonageEDIImpl();
        refParcelleZonageEDI.setActive(true);
        return refParcelleZonageEDI;
    }
}
