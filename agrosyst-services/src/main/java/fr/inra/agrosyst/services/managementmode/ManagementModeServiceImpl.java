package fr.inra.agrosyst.services.managementmode;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.managementmode.DamageType;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRule;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRuleCrop;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRuleCropTopiaDao;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRuleTopiaDao;
import fr.inra.agrosyst.api.entities.managementmode.ImplementationType;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeCategory;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeImpl;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeTopiaDao;
import fr.inra.agrosyst.api.entities.managementmode.Section;
import fr.inra.agrosyst.api.entities.managementmode.SectionTopiaDao;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.managementmode.Strategy;
import fr.inra.agrosyst.api.entities.managementmode.StrategyTopiaDao;
import fr.inra.agrosyst.api.entities.managementmode.StrategyType;
import fr.inra.agrosyst.api.entities.referential.RefAdventiceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressorTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLever;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLeverTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.entities.report.ArboAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropPestMaster;
import fr.inra.agrosyst.api.entities.report.ArboPestMaster;
import fr.inra.agrosyst.api.entities.report.CropPestMaster;
import fr.inra.agrosyst.api.entities.report.PestMaster;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.entities.report.VitiPestMaster;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.common.HistoryItem;
import fr.inra.agrosyst.api.services.common.HistoryType;
import fr.inra.agrosyst.api.services.domain.DomainDto;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.ExtendContext;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemDto;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.managementmode.DecisionRuleFilter;
import fr.inra.agrosyst.api.services.managementmode.DecisionRulesDto;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeDto;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeFilter;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.api.services.managementmode.ManagementModes;
import fr.inra.agrosyst.api.services.managementmode.SectionDto;
import fr.inra.agrosyst.api.services.managementmode.StrategyDto;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.export.EntityExporter;
import fr.inra.agrosyst.services.common.export.ExportModelAndRows;
import fr.inra.agrosyst.services.managementmode.export.DecisionRuleExportMetadata.DecisionRuleBean;
import fr.inra.agrosyst.services.managementmode.export.DecisionRuleExportMetadata.DecisionRuleModel;
import fr.inra.agrosyst.services.managementmode.export.DecisionRuleExportTask;
import fr.inra.agrosyst.services.managementmode.export.ManagementModeExportMetadata.CommonBean;
import fr.inra.agrosyst.services.managementmode.export.ManagementModeExportMetadata.MailModel;
import fr.inra.agrosyst.services.managementmode.export.ManagementModeExportMetadata.MainBean;
import fr.inra.agrosyst.services.managementmode.export.ManagementModeExportMetadata.SectionBean;
import fr.inra.agrosyst.services.managementmode.export.ManagementModeExportMetadata.SectionModel;
import fr.inra.agrosyst.services.managementmode.export.ManagementModeExportTask;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Setter
public class ManagementModeServiceImpl extends AbstractAgrosystService implements ManagementModeService {

    private static final Log log = LogFactory.getLog(ManagementModeServiceImpl.class);

    protected AnonymizeService anonymizeService;
    protected BusinessAuthorizationService authorizationService;
    protected DomainService domainService;
    protected ReferentialService referentialService;

    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    protected DecisionRuleTopiaDao decisionRuleDao;
    protected DecisionRuleCropTopiaDao decisionRuleCropDao;
    protected DomainTopiaDao domainDao;
    protected GrowingSystemTopiaDao growingSystemDao;
    protected ManagementModeTopiaDao managementModeDao;
    protected RefAdventiceTopiaDao refAdventiceDao;
    protected RefBioAgressorTopiaDao refBioAgressorDao;
    protected RefNuisibleEDITopiaDao refNuisibleEDIDao;
    protected RefStrategyLeverTopiaDao refStrategyLeverDao;

    protected SectionTopiaDao sectionDao;
    protected StrategyTopiaDao strategyDao;
    
    @Override
    public PaginationResult<DecisionRulesDto> getFilteredDecisionRules(DecisionRuleFilter filter) {
        PaginationResult<DecisionRule> decisionRules = decisionRuleDao.getFilteredDecisionRules(filter, getSecurityContext());

        PaginationParameter pager = decisionRules.getCurrentPage();
        List<DecisionRulesDto> decisionRulesDtos = new ArrayList<>(decisionRules.getElements().size());
        Binder<DecisionRule, DecisionRulesDto> binder = BinderFactory.newBinder(DecisionRule.class, DecisionRulesDto.class);

        Function<Domain, DomainDto> domainToDtoFunction = anonymizeService.getDomainToDtoFunction(false);
        for (DecisionRule decisionRule : decisionRules.getElements()) {

            // copy each result into a DTO to link a decision rule
            DecisionRulesDto decisionRulesDto = new DecisionRulesDto();

            // search max version rule
            DecisionRule maxVersionRule = decisionRuleDao.forProperties(
                        DecisionRule.PROPERTY_ACTIVE, decisionRule.isActive(),
                        DecisionRule.PROPERTY_CODE, decisionRule.getCode())
                    .setOrderByArguments(DecisionRule.PROPERTY_VERSION_NUMBER + " DESC")
                    .findFirst();
            binder.copy(maxVersionRule, decisionRulesDto);

            // link to a real domain via DecisionRule#domainCode code
            Domain relatedRulesDomain = domainDao.getAnyReadableDomainsForDecisionRuleList(decisionRule.getDomainCode(), getSecurityContext());

            if (relatedRulesDomain != null) {
                decisionRulesDto.setDomain(domainToDtoFunction.apply(relatedRulesDomain));

                decisionRulesDtos.add(decisionRulesDto);
            }
        }

        return PaginationResult.of(decisionRulesDtos, decisionRules.getCount(), pager);
    }

    @Override
    public Set<String> getFilteredDecisionRuleIds(DecisionRuleFilter filter) {
        return decisionRuleDao.getFilteredDecisionRuleIds(filter, getSecurityContext());
    }

    @Override
    public DecisionRule newDecisionRule() {
        DecisionRule decisionRule = decisionRuleDao.newInstance();

        // active by default
        decisionRule.setActive(true);
        // add defaut solution template
        decisionRule.setSolution("<p><strong>si</strong>&nbsp;</p><p><strong>alors</strong>&nbsp;</p><p><strong>sinon</strong>&nbsp;</p>");
        // add a code
        decisionRule.setCode(UUID.randomUUID().toString());
        // add a version number
        decisionRule.setVersionNumber(1);

        return decisionRule;
    }

    @Override
    public Optional<DecisionRule> getDecisionRule(String decisionRuleTopiaId) {
        final DecisionRule dr = decisionRuleDao.forTopiaIdEquals(decisionRuleTopiaId).findUniqueOrNull();
        final Optional<DecisionRule> result = Optional.ofNullable(dr);
        return result;
    }

    @Override
    public DecisionRule getLastDecisionRuleVersion(String decisionRuleCode) {
        return decisionRuleDao.getLastRuleVersion(decisionRuleCode);
    }

    @Override
    public List<CroppingPlanEntry> getDomainCodeCroppingPlanEntries(String domainCode) {
        return managementModeDao.getCroppingPlanEntryWithDomainCode(domainCode);
    }

    @Override
    public List<CroppingPlanEntry> getGrowingSystemCroppingPlanEntries(String growingSystemTopiaId) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(growingSystemTopiaId));
        GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(growingSystemTopiaId).findUnique();
        List<CroppingPlanEntry> croppingPlansBodies = domainService.getCroppingPlansBodies(growingSystem.getGrowingPlan().getDomain().getTopiaId());
        return croppingPlansBodies;
    }

    @Override
    public List<String> getCropIdsForGrowingSystemId(String growingSystemTopiaId) {
        return croppingPlanEntryDao.findCropsIdsForGrowingSystemId(growingSystemTopiaId);
    }

    @Override
    public List<String> loadCropsNames(Set<String> croppingPlanEntryIds) {
        return CollectionUtils.isEmpty(croppingPlanEntryIds) ? new ArrayList<>() : croppingPlanEntryDao.findCropsNamesByIds(croppingPlanEntryIds);
    }

    @Override
    public Collection<DecisionRule> getGrowingSystemDecisionRules(String growingSystemTopiaId) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(growingSystemTopiaId));
        GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(growingSystemTopiaId).findUnique();
        List<DecisionRule> decisionRules = decisionRuleDao.forActiveEquals(true).
                addEquals(DecisionRule.PROPERTY_DOMAIN_CODE,
                        growingSystem.getGrowingPlan().getDomain().getCode()).
                setOrderByArguments(DecisionRule.PROPERTY_CODE, DecisionRule.PROPERTY_VERSION_NUMBER + " DESC")
                .findAll();
        Map<String, DecisionRule> lastVersionDecisionRules = new HashMap<>();
        for (DecisionRule decisionRule : decisionRules) {
            lastVersionDecisionRules.putIfAbsent(decisionRule.getCode(), decisionRule);
        }

        return lastVersionDecisionRules.values();
    }

    @Override
    public DecisionRule createOrUpdateDecisionRule(DecisionRule decisionRule,
                                                   String domainCode,
                                                   String bioAgressorTopiaId,
                                                   List<DecisionRuleCrop> decisionRuleCrops) {

        authorizationService.checkCreateOrUpdateDecisionRule(decisionRule.getTopiaId());
        DecisionRule originalDecisionRule = null;

        if (decisionRule.isPersisted()) {
            Preconditions.checkArgument(isDecisionRuleActive(decisionRule), "Update decision rule with id '"
                    + decisionRule.getTopiaId()+ "' is not allowed because it is unactivated or the domains related to it or unactivated");
        }

        // growing system id
        if (decisionRule.getTopiaId() == null) {
            if (StringUtils.isBlank(domainCode)) {
                throw new AgrosystTechnicalException("Domain can't be null");
            } else {
                Preconditions.checkState(domainDao.forCodeEquals(domainCode).exists());
                decisionRule.setDomainCode(domainCode);
            }
        } else {
            originalDecisionRule = decisionRuleDao.forTopiaIdEquals(decisionRule.getTopiaId()).findUnique();
        }

        // bio agressor
        RefBioAgressor bioAgressor = null;
        if (StringUtils.isNotBlank(bioAgressorTopiaId)) {
            bioAgressor = refBioAgressorDao.forTopiaIdEquals(bioAgressorTopiaId).findUnique();
        }
        decisionRule.setBioAgressor(bioAgressor);

        DecisionRule result;
        // persist decision rule
        if (originalDecisionRule != null) {

            Binder<DecisionRule, DecisionRule> binder = BinderFactory.newBinder(DecisionRule.class);

            // it's always a copy of the original rule that we are using in the JSP
            // in the case of a modification of the rule push the modification to the original one.
            binder.copyExcluding(decisionRule, originalDecisionRule,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                    DecisionRule.PROPERTY_DECISION_RULE_CROP);

            // croping plan entry code
            arrangeDecisionRuleCropsToCreateOrUpdate(decisionRule, originalDecisionRule, decisionRuleCrops);

            result = decisionRuleDao.update(originalDecisionRule);
        } else {
            // croping plan entry code
            arrangeDecisionRuleCropsToCreateOrUpdate(decisionRule, null, decisionRuleCrops);

            result = decisionRuleDao.create(decisionRule);
        }

        // On met à jour le nom sur toutes les règles
        final String newName = result.getName();
        List<DecisionRule> relatedDecisionRules = getRelatedDecisionRules(result.getCode());
        Iterable<DecisionRule> rulesWithDifferentName = relatedDecisionRules.stream()
                .filter(related -> !newName.equals(related.getName()))
                .collect(Collectors.toList());
        for (DecisionRule related : rulesWithDifferentName) {
            related.setName(newName);
            decisionRuleDao.update(related);
        }

        getTransaction().commit();
        return result;
    }

    protected void arrangeDecisionRuleCropsToCreateOrUpdate(DecisionRule decisionRule, DecisionRule originalDecisionRule, List<DecisionRuleCrop> toCreateOrUpdateDecisionRuleCrops) {
        Collection<DecisionRuleCrop> originalDecisionRuleDecisionRuleCrop = originalDecisionRule == null ? new ArrayList<>() : originalDecisionRule.getDecisionRuleCrop();
        List<DecisionRuleCrop> decisionRuleCropsToPersist = new ArrayList<>();
        if (originalDecisionRule == null) {
            decisionRuleCropsToPersist = toCreateOrUpdateDecisionRuleCrops == null ? decisionRuleCropsToPersist : toCreateOrUpdateDecisionRuleCrops;
        } else {
            Map<String, DecisionRuleCrop> decisionRuleCropCodeByCode = new HashMap<>();
            if (originalDecisionRuleDecisionRuleCrop != null) {
                decisionRuleCropCodeByCode.putAll(Maps.uniqueIndex(originalDecisionRuleDecisionRuleCrop, GET_CROP_CODE::apply));
            }

            if (toCreateOrUpdateDecisionRuleCrops != null) {
                for (DecisionRuleCrop toCreateOrUpdateDecisionRuleCrop : toCreateOrUpdateDecisionRuleCrops) {
                    DecisionRuleCrop entity = decisionRuleCropCodeByCode.remove(toCreateOrUpdateDecisionRuleCrop.getCroppingPlanEntryCode());
                    decisionRuleCropsToPersist.add(Objects.requireNonNullElse(entity, toCreateOrUpdateDecisionRuleCrop));
                }
            }
            decisionRuleCropDao.deleteAll(decisionRuleCropCodeByCode.values());
        }

        Iterable<DecisionRuleCrop> persistedDecisionRules = decisionRuleCropDao.updateAll(decisionRuleCropsToPersist);

        decisionRuleCropsToPersist = Lists.newArrayList(persistedDecisionRules);
        decisionRule.setDecisionRuleCrop(decisionRuleCropsToPersist);
    }


    @Override
    public DecisionRule createNewDecisionRule(AgrosystInterventionType interventionType,
                                              String growingSystemTopiaId,
                                              BioAgressorType bioAgressorType,
                                              String codeGroupeCibleMaa, String bioAgressorTopiaId,
                                              List<String> cropIds,
                                              String name) {

        Preconditions.checkNotNull(growingSystemTopiaId);
        Preconditions.checkNotNull(interventionType);
        Preconditions.checkNotNull(name);

        GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(growingSystemTopiaId).findUnique();

        RefBioAgressor bioAgressor = null;
        if (!Strings.isNullOrEmpty(bioAgressorTopiaId)) {
            bioAgressor = refBioAgressorDao.forTopiaIdEquals(bioAgressorTopiaId).findUnique();
        }

        List<DecisionRuleCrop> decisionRuleCrops = null;
        if (CollectionUtils.isNotEmpty(cropIds)) {
            List<String> cropCodes = croppingPlanEntryDao.findCropsCodes(new HashSet<>(cropIds));
            decisionRuleCrops = new ArrayList<>(cropCodes.size());
            for (String cropCode : cropCodes) {
                DecisionRuleCrop decisionRuleCrop = decisionRuleCropDao.newInstance();
                decisionRuleCrop.setCroppingPlanEntryCode(cropCode);
                decisionRuleCrop = decisionRuleCropDao.create(decisionRuleCrop);
                decisionRuleCrops.add(decisionRuleCrop);
            }
        }

        DecisionRule decisionRule = newDecisionRule();
        decisionRule.setDomainCode(growingSystem.getGrowingPlan().getDomain().getCode());
        decisionRule.setBioAgressorType(bioAgressorType);
        decisionRule.setCodeGroupeCibleMaa(codeGroupeCibleMaa);
        decisionRule.setBioAgressor(bioAgressor);
        decisionRule.setName(name);
        decisionRule.setInterventionType(interventionType);
        decisionRule.setDecisionRuleCrop(decisionRuleCrops);
        DecisionRule result = decisionRuleDao.create(decisionRule);
        getTransaction().commit();
        return result;
    }

    @Override
    public PaginationResult<ManagementModeDto> getFilteredManagementModeDtos(ManagementModeFilter managementModeFilter) {

        Pair<Map<GrowingSystem, ManagementModeDto>, PaginationResult<ManagementModeDto>> mmList = managementModeDao.getFilteredManagementModeDtos(
                managementModeFilter, getSecurityContext());
        return getManagementModeDtoPaginationResult(mmList);

    }
    
    protected PaginationResult<ManagementModeDto> getManagementModeDtoPaginationResult(Pair<Map<GrowingSystem, ManagementModeDto>, PaginationResult<ManagementModeDto>> mmList) {
        Set<Map.Entry<GrowingSystem, ManagementModeDto>> entries = mmList.getLeft().entrySet();
        
        Iterable<ManagementModeDto> list = entries.stream().map(
                getEntryManagementModeDtoFunction(anonymizeService.getGrowingSystemToDtoFunction())
        ).collect(Collectors.toList());
        
        return PaginationResult.of(Lists.newArrayList(list), mmList.getRight().getCount(), mmList.getRight().getCurrentPage());
    }

    protected Function<Map.Entry<GrowingSystem, ManagementModeDto>, ManagementModeDto> getEntryManagementModeDtoFunction(
            Function<GrowingSystem, GrowingSystemDto> growingSystemToDtoFunction) {

        return input -> {
            GrowingSystemDto growingSystemDto = growingSystemToDtoFunction.apply(input.getKey());
            ManagementModeDto result = input.getValue();
            result.setGrowingSystem(growingSystemDto);
            return result;
        };
    }

    @Override
    public Set<String> getFilteredManagementModeIds(ManagementModeFilter managementModeFilter) {
        return managementModeDao.getFilteredManagementModeIds(managementModeFilter, getSecurityContext());
    }

    @Override
    public ManagementMode newManagementMode() {
        ManagementMode managementMode = managementModeDao.newInstance();

        // active by default
        managementMode.setActive(true);
        managementMode.setCategory(ManagementModeCategory.OBSERVED);

        return managementMode;
    }

    @Override
    public ManagementMode getManagementMode(String managementModeTopiaId) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(managementModeTopiaId));
        ManagementMode managementMode = managementModeDao.forTopiaIdEquals(managementModeTopiaId).findUnique();
        return anonymizeService.checkForManagementModeAnonymization(managementMode);
    }

    @Override
    public ManagementMode createOrUpdateManagementMode(ManagementMode managementMode, String growingSystemTopiaId, Collection<SectionDto> sections) {

        authorizationService.checkCreateOrUpdateManagementMode(managementMode.getTopiaId());

        Preconditions.checkNotNull(managementMode.getCategory(), String.format("No category difined for management mode with id '%s' and growing system with id %s", managementMode.getTopiaId(), growingSystemTopiaId));

        setGrowingSytemToNewManagementMode(managementMode, growingSystemTopiaId);

        checkActivated(managementMode);

        updateManagementModeSections(managementMode, sections);

        // update management mode
        if (managementMode.isPersisted()) {
            managementMode = managementModeDao.update(managementMode);
        } else {

            List<HistoryItem> histories = new ArrayList<>();
            Gson gson = context.getGson();

            // historic
            histories.add(createNewManagementModeHistory(managementMode));
            managementMode.setHistorical(gson.toJson(histories));

            managementMode = managementModeDao.create(managementMode);
        }

        getTransaction().commit();
        return managementMode;
    }

    protected void setGrowingSytemToNewManagementMode(ManagementMode managementMode, String growingSystemTopiaId) {
        // growing system id
        if (!managementMode.isPersisted()) {
            if (StringUtils.isBlank(growingSystemTopiaId)) {
                throw new AgrosystTechnicalException("Growing system can't be null");
            } else {
                GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(growingSystemTopiaId).findUnique();
                managementMode.setGrowingSystem(growingSystem);
            }
        }
    }

    protected void checkActivated(ManagementMode managementMode) {
        Preconditions.checkArgument(managementMode.isActive() && managementMode.getGrowingSystem().isActive() &&
                managementMode.getGrowingSystem().getGrowingPlan().isActive() &&
                managementMode.getGrowingSystem().getGrowingPlan().getDomain().isActive(),
                String.format("The management mode '%s', or growing system '%s' or growing plan '%s' or domain '%s' is not activated",
                        managementMode.getTopiaId(),
                        managementMode.getGrowingSystem().getTopiaId(),
                        managementMode.getGrowingSystem().getGrowingPlan().getTopiaId(),
                        managementMode.getGrowingSystem().getGrowingPlan().getDomain().getTopiaId()));

    }

    @Override
    public void createManagementModeFromReportGrowingSystem(ReportGrowingSystem reportGrowingSystem) {

        // get previous one
        GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
        String growingSystemId = reportGrowingSystem.getGrowingSystem().getTopiaId();
        ManagementMode managementMode = managementModeDao.forProperties(
                        ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID, growingSystemId,
                        ManagementMode.PROPERTY_CATEGORY, ManagementModeCategory.OBSERVED
                ).findUniqueOrNull();

        Map<String, BioAgressorType> groupesCiblesReferenceParamParCode = referentialService.getGroupesCiblesReferenceParamParCode();

        // create new one if none
        if (managementMode == null) {
            managementMode = new ManagementModeImpl();
            managementMode.setActive(true);
            managementMode.setCategory(ManagementModeCategory.OBSERVED);
            managementMode.setGrowingSystem(growingSystem);
        }

        // objects for all methods
        LocalDateTime transactionDate = context.getCurrentTime();
        List<HistoryItem> histories = getHistoryItems(managementMode.getHistorical());
        Collection<Section> sections = managementMode.getSections();
        if (sections == null) {
            sections = new ArrayList<>();
        }

        if (CollectionUtils.isNotEmpty(reportGrowingSystem.getCropPestMasters())) {
            SectionType sectionType = SectionType.RAVAGEURS;
            addSectionForCropPestMasters(sectionType, reportGrowingSystem.getCropPestMasters(), sections, histories, transactionDate, groupesCiblesReferenceParamParCode);
        }

        if (CollectionUtils.isNotEmpty(reportGrowingSystem.getCropAdventiceMasters())) {
            SectionType sectionType = SectionType.ADVENTICES;
            addSectionForCropPestMasters(sectionType, reportGrowingSystem.getCropAdventiceMasters(), sections, histories, transactionDate, groupesCiblesReferenceParamParCode);
        }

        if (CollectionUtils.isNotEmpty(reportGrowingSystem.getCropDiseaseMasters())) {
            SectionType sectionType = SectionType.MALADIES;
            addSectionForCropPestMasters(sectionType, reportGrowingSystem.getCropDiseaseMasters(), sections, histories, transactionDate, groupesCiblesReferenceParamParCode);
        }

        if (CollectionUtils.isNotEmpty(reportGrowingSystem.getVerseMasters())) {
            SectionType sectionType = SectionType.MAITRISE_DES_DOMMAGES_PHYSIQUES;
            addSectionForVerseMasters(sectionType, sections, histories, transactionDate);
        }

        if (CollectionUtils.isNotEmpty(reportGrowingSystem.getArboCropPestMasters())) {
            SectionType sectionType = SectionType.RAVAGEURS;
            addSectionForArboCropPestMasters(sectionType, reportGrowingSystem.getArboCropPestMasters(), sections, histories, transactionDate, groupesCiblesReferenceParamParCode);
        }

        if (CollectionUtils.isNotEmpty(reportGrowingSystem.getArboCropAdventiceMasters())) {
            SectionType sectionType = SectionType.ADVENTICES;
            addSectionForArboCropAdventiceMasters(sectionType, reportGrowingSystem.getArboCropAdventiceMasters(), sections, histories, transactionDate);
        }

        if (CollectionUtils.isNotEmpty(reportGrowingSystem.getArboCropDiseaseMasters())) {
            SectionType sectionType = SectionType.MALADIES;
            addSectionForArboCropPestMasters(sectionType, reportGrowingSystem.getArboCropDiseaseMasters(), sections, histories, transactionDate, groupesCiblesReferenceParamParCode);
        }

        if (CollectionUtils.isNotEmpty(reportGrowingSystem.getVitiPestMasters())) {
            SectionType sectionType = SectionType.RAVAGEURS;
            addSectionForVitiPestMasters(sectionType, reportGrowingSystem.getVitiPestMasters(), sections, histories, transactionDate, groupesCiblesReferenceParamParCode);
        }

        if (CollectionUtils.isNotEmpty(reportGrowingSystem.getVitiDiseaseMasters())) {
            SectionType sectionType = SectionType.MALADIES;
            addSectionForVitiPestMasters(sectionType, reportGrowingSystem.getVitiDiseaseMasters(), sections, histories, transactionDate, groupesCiblesReferenceParamParCode);
        }

        String historiesJson = context.getGson().toJson(histories);
        managementMode.setHistorical(historiesJson);
        managementMode.setSections(sections);

        managementModeDao.create(managementMode);
    }

    protected void addSectionForVerseMasters(SectionType sectionType, Collection<Section> sections, List<HistoryItem> histories, LocalDateTime transactionDate) {
        boolean nonExistingSection = sections.stream().noneMatch(s -> s.getDamageType() == DamageType.VERSE);

        if (nonExistingSection) {
            Section section = sectionDao.newInstance();
            section.setSectionType(sectionType);

            section.setDamageType(DamageType.VERSE);

            addHistoryItem(histories, transactionDate, null, section.getBioAgressorType(), section);

            sections.add(section);
        }
    }

    protected void addSectionForCropPestMasters(SectionType sectionType,
                                                Collection<CropPestMaster> cropPestMasters,
                                                Collection<Section> sections,
                                                List<HistoryItem> histories,
                                                LocalDateTime transactionDate,
                                                Map<String, BioAgressorType> groupesCiblesReferenceParamParCode) {

        for (CropPestMaster cropPestMaster : cropPestMasters) {
            for (PestMaster pestMaster : cropPestMaster.getPestMasters()) {

                RefBioAgressor bioAgressor = pestMaster.getAgressor();
                String codeGroupeCibleMaa = pestMaster.getCodeGroupeCibleMaa();

                if (bioAgressor != null) {
                    Optional<Section> sectionWithAgressor = sections.stream()
                            .filter(section -> bioAgressor.equals(section.getBioAgressor()))
                            .findFirst();

                    if (sectionWithAgressor.isPresent()) {
                        Section section = sectionWithAgressor.get();
                        if (section.getCodeGroupeCibleMaa() == null) {
                            section.setCodeGroupeCibleMaa(codeGroupeCibleMaa);
                        }

                    } else {
                        BioAgressorType bioAgressorType = bioAgressor.getReferenceParam();

                        Section section = sectionDao.newInstance();
                        section.setSectionType(sectionType);
                        section.setBioAgressorType(bioAgressorType);
                        section.setBioAgressor(bioAgressor);
                        section.setCodeGroupeCibleMaa(codeGroupeCibleMaa);

                        addHistoryItem(histories, transactionDate, bioAgressor, bioAgressorType, section);

                        sections.add(section);
                    }

                } else if (codeGroupeCibleMaa != null) {
                    boolean nonExistingGroupeCible = sections.stream()
                            .noneMatch(section -> section.getBioAgressor() == null
                                    && codeGroupeCibleMaa.equals(section.getCodeGroupeCibleMaa()));

                    if (nonExistingGroupeCible) {
                        BioAgressorType bioAgressorType = groupesCiblesReferenceParamParCode.get(codeGroupeCibleMaa);

                        Section section = sectionDao.newInstance();
                        section.setSectionType(sectionType);
                        section.setBioAgressorType(bioAgressorType);
                        section.setCodeGroupeCibleMaa(codeGroupeCibleMaa);

                        addHistoryItem(histories, transactionDate, null, bioAgressorType, section);

                        sections.add(section);
                    }
                }
            }
        }
    }

    protected void addSectionForVitiPestMasters(SectionType sectionType, Collection<VitiPestMaster> vitiPestMasters, Collection<Section> sections, List<HistoryItem> histories, LocalDateTime transactionDate, Map<String, BioAgressorType> groupesCiblesReferenceParamParCode) {
        for (VitiPestMaster cropPestMaster : vitiPestMasters) {

            RefBioAgressor bioAgressor = cropPestMaster.getAgressor();
            String codeGroupeCibleMaa = cropPestMaster.getCodeGroupeCibleMaa();

            if (bioAgressor != null) {
                Optional<Section> sectionWithAgressor = sections.stream()
                        .filter(section -> bioAgressor.equals(section.getBioAgressor()))
                        .findFirst();

                if (sectionWithAgressor.isPresent()) {
                    Section section = sectionWithAgressor.get();
                    if (section.getCodeGroupeCibleMaa() == null) {
                        section.setCodeGroupeCibleMaa(codeGroupeCibleMaa);
                    }

                } else {
                    BioAgressorType bioAgressorType = bioAgressor.getReferenceParam();

                    Section section = sectionDao.newInstance();
                    section.setSectionType(sectionType);
                    section.setBioAgressorType(bioAgressorType);
                    section.setBioAgressor(bioAgressor);
                    section.setCodeGroupeCibleMaa(codeGroupeCibleMaa);

                    addHistoryItem(histories, transactionDate, bioAgressor, bioAgressorType, section);

                    sections.add(section);
                }

            } else if (codeGroupeCibleMaa != null) {
                boolean nonExistingGroupeCible = sections.stream()
                        .noneMatch(section -> section.getBioAgressor() == null
                                && codeGroupeCibleMaa.equals(section.getCodeGroupeCibleMaa()));

                if (nonExistingGroupeCible) {
                    BioAgressorType bioAgressorType = groupesCiblesReferenceParamParCode.get(codeGroupeCibleMaa);

                    Section section = sectionDao.newInstance();
                    section.setSectionType(sectionType);
                    section.setBioAgressorType(bioAgressorType);
                    section.setCodeGroupeCibleMaa(codeGroupeCibleMaa);

                    addHistoryItem(histories, transactionDate, null, bioAgressorType, section);

                    sections.add(section);
                }
            }
        }
    }

    protected void addSectionForArboCropAdventiceMasters(SectionType sectionType,
                                                         Collection<ArboCropAdventiceMaster> arboCropAdventiceMasters,
                                                         Collection<Section> sections,
                                                         List<HistoryItem> histories,
                                                         LocalDateTime transactionDate) {
        for (ArboCropAdventiceMaster cropPestMaster : arboCropAdventiceMasters) {
            for (ArboAdventiceMaster pestMaster : cropPestMaster.getPestMasters()) {
                RefBioAgressor bioAgressor = pestMaster.getAgressor();

                boolean nonExistingBioAgressor = sections.stream()
                        .map(Section::getBioAgressor)
                        .filter(Objects::nonNull)
                        .noneMatch(a -> a.equals(bioAgressor));
                if (nonExistingBioAgressor) {
                    Section section = sectionDao.newInstance();

                    section.setSectionType(sectionType);

                    BioAgressorType bioAgressorType = bioAgressor.getReferenceParam();

                    section.setBioAgressor(bioAgressor);
                    section.setBioAgressorType(bioAgressorType);

                    addHistoryItem(histories, transactionDate, bioAgressor, section.getBioAgressorType(), section);

                    sections.add(section);
                }
            }
        }
    }

    protected void addSectionForArboCropPestMasters(SectionType sectionType,
                                                    Collection<ArboCropPestMaster> arboCropPestMasters,
                                                    Collection<Section> sections,
                                                    List<HistoryItem> histories,
                                                    LocalDateTime transactionDate,
                                                    Map<String, BioAgressorType> groupesCiblesReferenceParamParCode) {

        for (ArboCropPestMaster cropPestMaster : arboCropPestMasters) {
            for (ArboPestMaster pestMaster : cropPestMaster.getPestMasters()) {

                RefBioAgressor bioAgressor = pestMaster.getAgressor();
                String codeGroupeCibleMaa = pestMaster.getCodeGroupeCibleMaa();

                if (bioAgressor != null) {
                    Optional<Section> sectionWithAgressor = sections.stream()
                            .filter(section -> bioAgressor.equals(section.getBioAgressor()))
                            .findFirst();

                    if (sectionWithAgressor.isPresent()) {
                        Section section = sectionWithAgressor.get();
                        if (section.getCodeGroupeCibleMaa() == null) {
                            section.setCodeGroupeCibleMaa(codeGroupeCibleMaa);
                        }

                    } else {
                        BioAgressorType bioAgressorType = bioAgressor.getReferenceParam();

                        Section section = sectionDao.newInstance();
                        section.setSectionType(sectionType);
                        section.setBioAgressorType(bioAgressorType);
                        section.setBioAgressor(bioAgressor);
                        section.setCodeGroupeCibleMaa(codeGroupeCibleMaa);

                        addHistoryItem(histories, transactionDate, bioAgressor, bioAgressorType, section);

                        sections.add(section);
                    }

                } else if (codeGroupeCibleMaa != null) {
                    boolean nonExistingGroupeCible = sections.stream()
                            .noneMatch(section -> section.getBioAgressor() == null
                                    && codeGroupeCibleMaa.equals(section.getCodeGroupeCibleMaa()));

                    if (nonExistingGroupeCible) {
                        BioAgressorType bioAgressorType = groupesCiblesReferenceParamParCode.get(codeGroupeCibleMaa);

                        Section section = sectionDao.newInstance();
                        section.setSectionType(sectionType);
                        section.setBioAgressorType(bioAgressorType);
                        section.setCodeGroupeCibleMaa(codeGroupeCibleMaa);

                        addHistoryItemWithoutBioAgressor(histories, transactionDate, bioAgressorType, section);

                        sections.add(section);
                    }
                }
            }
        }
    }

    protected void addHistoryItemWithoutBioAgressor(List<HistoryItem> histories, LocalDateTime transactionDate, BioAgressorType bioAgressorType, Section section) {
        HistoryItem historyItem = createHistoryItemSection(
                section.getSectionType(),
                bioAgressorType,
                null,
                transactionDate,
                HistoryType.SECTION_ADD
        );
        histories.add(historyItem);
    }

    protected void addHistoryItem(List<HistoryItem> histories, LocalDateTime transactionDate, RefBioAgressor bioAgressor, BioAgressorType bioAgressorType, Section section) {
        HistoryItem historyItem = createHistoryItemSection(
                section.getSectionType(),
                bioAgressorType,
                bioAgressor,
                transactionDate,
                HistoryType.SECTION_ADD
        );
        histories.add(historyItem);
    }

    /**
     * Update management mode's section with provided DTO.
     *
     * @param managementMode management mode containing section
     * @param sections       dto
     */
    protected void updateManagementModeSections(ManagementMode managementMode, Collection<SectionDto> sections) {
        // sections
        if (sections != null) {
            final GrowingPlan growingPlan = managementMode.getGrowingSystem().getGrowingPlan();
            Domain domain = growingPlan.getDomain();
            final TypeDEPHY typeDEPHY = growingPlan.getType();
            List<CroppingPlanEntry> targetedCrops = domainService.getCroppingPlanEntriesForDomain(domain);

            LocalDateTime transactionDate = context.getCurrentTime();

            String previousHistory = managementMode.getHistorical();

            List<HistoryItem> histories = getHistoryItems(previousHistory);

            Collection<Section> currentSections = managementMode.getSections();
            List<Section> nonDeletedSection = new ArrayList<>();
            if (currentSections == null) {
                currentSections = new ArrayList<>();
                managementMode.setSections(currentSections);
            }
            Map<String, Section> sectionsIndex = Maps.uniqueIndex(currentSections, Entities.GET_TOPIA_ID::apply);
            for (SectionDto sectionDto : sections) {

                SectionType sectionType = sectionDto.getSectionType();
                Preconditions.checkNotNull(sectionType, "Missing required Parameters 'SectionType'");
                
                RefBioAgressor bioAgressor = null;
                if (StringUtils.isNotBlank(sectionDto.getBioAgressorTopiaId())) {
                    bioAgressor = refBioAgressorDao.forTopiaIdEquals(sectionDto.getBioAgressorTopiaId()).findUnique();
                }
                sectionDto.setBioAgressor(bioAgressor);

                if (TypeDEPHY.DEPHY_FERME.equals(typeDEPHY) &&
                        (SectionType.ADVENTICES.equals(sectionType) ||
                        SectionType.MALADIES.equals(sectionType) ||
                        SectionType.RAVAGEURS.equals(sectionType))
                ) {
                    Preconditions.checkNotNull(bioAgressor, String.format("Missing required Parameters 'BioAgressor', as sectionType is %s", sectionType));
                }
    
                if (TypeDEPHY.DEPHY_FERME.equals(typeDEPHY)) {
                    Preconditions.checkNotNull(sectionDto.getCategoryObjective(), "Missing required Parameters 'CategoryObjective'");
                }

                String topiaId = sectionDto.getTopiaId();
                Section section;
                if (StringUtils.isBlank(topiaId)) {
                    section = sectionDao.newInstance();

                    HistoryItem historyItem = createHistoryItemSectionAdd(sectionDto, bioAgressor, transactionDate);
                    histories.add(historyItem);

                } else {
                    section = sectionsIndex.get(topiaId);
                }
                ManagementModes.SECTION_DTO_SECTION_BINDER.copyExcluding(sectionDto, section,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        Section.PROPERTY_STRATEGIES);

                if (StringUtils.isBlank(topiaId)) {
                    currentSections.add(section);
                }
                nonDeletedSection.add(section);

                List<HistoryItem> strategiesHistories = updateSectionStrategies(section, sectionDto.getStrategiesDto(), targetedCrops, domain.getCode(), typeDEPHY);
                histories.addAll(strategiesHistories);
            }
            Collection<Section> removedSections = CollectionUtils.subtract(currentSections, nonDeletedSection);
    
            for (Section section : removedSections) {
                HistoryItem historyItem = createHistoryItemSectionRemove(section, section.getBioAgressor(), transactionDate);
                histories.add(historyItem);
            }
    
            String historiesJson = context.getGson().toJson(histories);
            managementMode.setHistorical(historiesJson);

            currentSections.retainAll(nonDeletedSection);
        }
    }

    private List<HistoryItem> getHistoryItems(String previousHistory) {
        List<HistoryItem> histories;
        Gson gson = context.getGson();
        Type type = new TypeToken<List<HistoryItem>>() {
        }.getType();

        if (previousHistory != null) {
            histories = gson.fromJson(previousHistory, type);
        } else {
            histories = new ArrayList<>();
        }
        return histories;
    }

    protected HistoryItem createHistoryItemSectionAdd(SectionDto sectionDto, RefBioAgressor bioAgressor, LocalDateTime transactionDate) {
        return createHistoryItemSection(sectionDto.getSectionType(), sectionDto.getBioAgressorType(), bioAgressor, transactionDate, HistoryType.SECTION_ADD);
    }

    protected HistoryItem createHistoryItemSectionRemove(Section section, RefBioAgressor bioAgressor, LocalDateTime transactionDate) {
        return createHistoryItemSection(section.getSectionType(), section.getBioAgressorType(), bioAgressor, transactionDate, HistoryType.SECTION_REMOVE);
    }

    protected HistoryItem createHistoryItemSection(SectionType sectionType, BioAgressorType bioAgressorType, RefBioAgressor bioAgressor, LocalDateTime transactionDate, HistoryType type) {
        HistoryItem historyItem = new HistoryItem();
        historyItem.setDate(transactionDate);
        historyItem.setType(type);
        List<String> args = historyItem.getArgs();

        args.add(sectionType.name());

        if (bioAgressorType != null) {
            args.add(bioAgressorType.name());
        } else {
            args.add(null);
        }
        if (bioAgressor != null) {
            args.add(bioAgressor.getTopiaId());
        } else {
            args.add(null);
        }
        return historyItem;
    }

    /**
     * Update management mode's section with provided DTO.
     *
     * @param section   section containing strategies
     */
    protected List<HistoryItem> updateSectionStrategies(Section section, List<StrategyDto> strategies, List<CroppingPlanEntry> targetingCrops, String targetedDomainCode, TypeDEPHY typeDEPHY) {

        List<HistoryItem> historiesItems = new ArrayList<>();
        // sections
        if (strategies != null) {

            Map<String, Strategy> strategiesIndex = new HashMap<>(Maps.uniqueIndex(CollectionUtils.emptyIfNull(section.getStrategies()), Entities.GET_TOPIA_ID::apply));

            Map<String, CroppingPlanEntry> targetingCropsByIds = Maps.uniqueIndex(targetingCrops, Entities.GET_TOPIA_ID::apply);

            Binder<StrategyDto, Strategy> strategyDtoStrategyBinder = BinderFactory.newBinder(StrategyDto.class, Strategy.class);
            for (StrategyDto strategyDto : strategies) {
    
                if (TypeDEPHY.DEPHY_FERME.equals(typeDEPHY)) {
                    Preconditions.checkArgument(CollectionUtils.isNotEmpty(strategyDto.getCropIds()), "A crop is required on a DEPHY_FERME strategies !");
                }
                
                if (!TypeDEPHY.DEPHY_FERME.equals(typeDEPHY)) {
                    Preconditions.checkNotNull(strategyDto.getStrategyType(), "Missing required Parameters 'Type de levier' !");
                }
    
                Preconditions.checkNotNull(strategyDto.getRefStrategyLever(), "Missing required Parameters 'Levier' !");
                
                String topiaId = strategyDto.getTopiaId();
                Strategy strategy;
                if (StringUtils.isBlank(topiaId)) {
                    strategy = strategyDao.newInstance();
                    section.addStrategies(strategy);
                    historiesItems.add(createStrategyHistoryAdd(strategyDto));
                } else {
                    strategy = strategiesIndex.remove(topiaId);
                }
                
                Preconditions.checkNotNull(strategy, String.format("Strategy with id '%s' was not found", topiaId));

                bindStrategyDtoToStrategy(strategyDtoStrategyBinder, strategyDto, strategy);

                validAndSetStrategyCrops(targetingCropsByIds, strategyDto, strategy);
                validAndStrategyDecisionRules(targetedDomainCode, historiesItems, strategyDto, strategy);
                setStrategyLever(strategyDto, strategy);
                
            }
            historiesItems.addAll(createStrategyHistoryRemove(strategiesIndex.values()));
    
            strategiesIndex.values().forEach(section::removeStrategies);
        }

        return historiesItems;
    }

    private void bindStrategyDtoToStrategy(Binder<StrategyDto, Strategy> strategyDtoStrategyBinder, StrategyDto strategyDto, Strategy strategy) {
        strategyDtoStrategyBinder.copyExcluding(
                strategyDto, strategy,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                Strategy.PROPERTY_CROPS,
                Strategy.PROPERTY_RULES,
                Strategy.PROPERTY_REF_STRATEGY_LEVER
        );
    }

    private void setStrategyLever(StrategyDto strategyDto, Strategy strategy) {
        RefStrategyLever refStrategyLever = null;
        if (strategyDto.getRefStrategyLever() != null) {
            String refStrategyLeverId = strategyDto.getRefStrategyLever().getTopiaId();
            refStrategyLever = refStrategyLeverDao.forTopiaIdEquals(refStrategyLeverId).findUniqueOrNull();
        }
        strategy.setRefStrategyLever(refStrategyLever);
    }

    private void validAndStrategyDecisionRules(String targetedDomainCode, List<HistoryItem> historiesItems, StrategyDto strategyDto, Strategy strategy) {
        // for rules
        Collection<DecisionRule> currentRules = strategy.getRules();
        if (currentRules == null) {
            currentRules = new ArrayList<>();
            strategy.setRules(currentRules);
        }
        Map<String, DecisionRule> indexedRules = new HashMap<>(Maps.uniqueIndex(currentRules, Entities.GET_TOPIA_ID::apply));
        currentRules.clear();
        if (strategyDto.getDecisionRuleIds() != null) {
            for (String decisionRuleId : strategyDto.getDecisionRuleIds()) {
                DecisionRule decisionRule = indexedRules.remove(decisionRuleId);
                if (decisionRule == null) {
                    decisionRule = decisionRuleDao.forTopiaIdEquals(decisionRuleId).findUnique();
                }
                // check decisionRule belong to valid domain
                if (decisionRule.getDomainCode().equals(targetedDomainCode)) {
                    currentRules.add(decisionRule);
                }
            }
        }
        historiesItems.addAll(createRulesHistoryAdd(currentRules));
        historiesItems.addAll(createRulesHistoryRemove(indexedRules.values()));
    }

    private void validAndSetStrategyCrops(Map<String, CroppingPlanEntry> targetingCropsByIds, StrategyDto strategyDto, Strategy strategy) {
        // check domain crop ids are valid
        Set<String> domainCropIds = targetingCropsByIds.keySet();
        List<String> cropIds = strategyDto.getCropIds();
        if (cropIds != null) {
            cropIds.retainAll(domainCropIds);
        }

        List<CroppingPlanEntry> croppingPlanEntries = null;
        if (CollectionUtils.isNotEmpty(cropIds)) {
            croppingPlanEntries = new ArrayList<>();
            for (String cropId : cropIds) {
                croppingPlanEntries.add(targetingCropsByIds.get(cropId));
            }
        }
        strategy.setCrops(croppingPlanEntries);
    }

    protected HistoryItem createStrategyHistoryAdd(StrategyDto strategyDto) {
        //(Boolean isMultiannual, String explanation, CroppingPlanEntry croppingPlanEntry, HistoryType type)
        return createStrategyHistory(
                strategyDto.getImplementationType(),
                strategyDto.getRefStrategyLever(),
                strategyDto.getExplanation(),
                strategyDto.getCropIds(),
                HistoryType.STRATEGY_ADD);
    }

    protected List<HistoryItem> createStrategyHistoryRemove(Collection<Strategy> strategies) {
        List<HistoryItem> result = new ArrayList<>(strategies.size());

        for (Strategy strategy : strategies) {
            List<String> cropIds = null;
            if (CollectionUtils.isNotEmpty(strategy.getCrops())) {
                cropIds = strategy.getCrops().stream().map(Entities.GET_TOPIA_ID).collect(Collectors.toList());
            }
            result.add(createStrategyHistory(
                    strategy.getImplementationType(),
                    strategy.getRefStrategyLever(),
                    strategy.getExplanation(),
                    cropIds,
                    HistoryType.STRATEGY_REMOVE));
        }

        return result;
    }

    protected List<HistoryItem> createRulesHistoryAdd(Collection<DecisionRule> rules) {
        List<HistoryItem> result = new ArrayList<>(rules.size());
        for (DecisionRule decisionRule : rules) {
            result.add(createRuleHistory(decisionRule, HistoryType.RULE_ADD));
        }
        return result;
    }

    protected List<HistoryItem> createRulesHistoryRemove(Collection<DecisionRule> rules) {
        List<HistoryItem> result = new ArrayList<>(rules.size());
        for (DecisionRule decisionRule : rules) {
            result.add(createRuleHistory(decisionRule, HistoryType.RULE_REMOVE));
        }
        return result;
    }

    protected HistoryItem createRuleHistory(DecisionRule decisionRule, HistoryType type) {
        HistoryItem item = new HistoryItem();
        item.setDate(context.getCurrentTime());
        item.setType(type);
        List<String> args = Arrays.asList(decisionRule.getTopiaId(), String.valueOf(decisionRule.getVersionNumber()));
        item.setArgs(args);
        return item;
    }

    protected HistoryItem createStrategyHistory(
            ImplementationType strategyImplementationType,
            RefStrategyLever refStrategyLever,
            String explanation,
            List<String> cropsIds,
            HistoryType type) {
        
        String croppingPlanEntryIds = null;
        if (CollectionUtils.isNotEmpty(cropsIds)) {
            croppingPlanEntryIds = StringUtils.join(cropsIds, ManagementModeService.HISTORY_CROP_IDS_SEPARATOR);
        }

        HistoryItem item = new HistoryItem();
        item.setDate(context.getCurrentTime());
        item.setType(type);
    
        String leverName = refStrategyLever != null ? refStrategyLever.getLever() : "";
        final String name = strategyImplementationType != null ? strategyImplementationType.name() : leverName;
        List<String> args = Arrays.asList(name, explanation, croppingPlanEntryIds);
        item.setArgs(args);

        return item;
    }

    @Override
    public ManagementMode getManagementModeForGrowingSystem(GrowingSystem growingSystem) {
        return managementModeDao.forGrowingSystemEquals(growingSystem).findAnyOrNull();
    }

    @Override
    public Collection<ManagementMode> getManagementModeForGrowingSystemIds(Collection<String> growingSystemIds) {
        return managementModeDao
                .forIn(ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID, growingSystemIds)
                .findAll();
    }

    @Override
    public Optional<String> getObservedManagementModeIdForGrowingSystemId(String growingSystemId) {
        ManagementMode managementMode =
                managementModeDao.forProperties(
                        ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID, growingSystemId,
                        ManagementMode.PROPERTY_CATEGORY, ManagementModeCategory.OBSERVED).findUniqueOrNull();
        return managementMode == null ? Optional.empty() : Optional.of(managementMode.getTopiaId());
    }

    protected HistoryItem createNewManagementModeHistory(ManagementMode managementMode) {
        return createManagementModeAddHistory(
                managementMode.getCategory().name());
    }

    protected HistoryItem createManagementModeCopyHistory(ManagementMode fromManagementMode, ManagementMode toManagementMode) {
        return createManagementModeHistory(
                HistoryType.MANAGEMENT_MODE_COPY,
                fromManagementMode.getCategory().name(),
                toManagementMode.getCategory().name());
    }

    protected HistoryItem createManagementModeDuplicateHistory(ManagementMode fromManagementMode, ManagementMode toManagementMode) {
        int fromCampaign = fromManagementMode.getGrowingSystem().getGrowingPlan().getDomain().getCampaign();
        int toCampaing = toManagementMode.getGrowingSystem().getGrowingPlan().getDomain().getCampaign();

        String fromManagementModeCampaignLibelle = String.format(" %d (%d - %d)", fromCampaign, fromCampaign - 1, fromCampaign);
        String toManagementModeCampaignLibelle = String.format(" %d (%d - %d)", toCampaing, toCampaing - 1, toCampaing);

        HistoryItem item = new HistoryItem();
        item.setDate(context.getCurrentTime());
        item.setType(HistoryType.MANAGEMENT_MODE_DUPLICATED);
        List<String> args = new ArrayList<>();
        args.add(fromManagementMode.getCategory().name());
        args.add(toManagementMode.getCategory().name());
        args.add(fromManagementModeCampaignLibelle);
        args.add(toManagementModeCampaignLibelle);
        item.setArgs(args);
        return item;
    }

    protected HistoryItem createManagementModeAddHistory(String toManagementModeCategoryName) {
        HistoryItem item = new HistoryItem();
        item.setDate(context.getCurrentTime());
        item.setType(HistoryType.MANAGEMENT_MODE_ADD);
        List<String> args = new ArrayList<>();
        args.add(toManagementModeCategoryName);
        item.setArgs(args);
        return item;
    }

    protected HistoryItem createManagementModeHistory(HistoryType type, String fromManagementModeCategoryName, String toManagementModeCategoryName) {
        HistoryItem item = new HistoryItem();
        item.setDate(context.getCurrentTime());
        item.setType(type);
        List<String> args = new ArrayList<>();
        if (!Strings.isNullOrEmpty(fromManagementModeCategoryName)) {
            args.add(fromManagementModeCategoryName);
        }
        args.add(toManagementModeCategoryName);
        item.setArgs(args);
        return item;
    }

    protected void cloneSections(ManagementMode managementMode, ManagementMode clonedManagementMode) {
        if (managementMode != null && managementMode.getSections() != null) {
            for (Section section : managementMode.getSections()) {
                Binder<Section, Section> sectionModeBinder = BinderFactory.newBinder(Section.class);
                Section clonedSection = sectionDao.newInstance();
                sectionModeBinder.copyExcluding(section, clonedSection,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        Section.PROPERTY_STRATEGIES);

                clonedManagementMode.addSections(clonedSection);

                // clone strategies
                if (section.getStrategies() != null) {
                    List<CroppingPlanEntry> crops = croppingPlanEntryDao.forDomainEquals(clonedManagementMode.getGrowingSystem().getGrowingPlan().getDomain()).findAll();
                    Map<String, CroppingPlanEntry> targetedCroppingPlanEntryByCodes = Maps.uniqueIndex(crops, CroppingPlanEntry::getCode);

                    for (Strategy strategy : section.getStrategies()) {
                        Binder<Strategy, Strategy> strategyModeBinder = BinderFactory.newBinder(Strategy.class);
                        Strategy clonedStrategy = strategyDao.newInstance();
                        strategyModeBinder.copyExcluding(strategy, clonedStrategy,
                                TopiaEntity.PROPERTY_TOPIA_ID,
                                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                TopiaEntity.PROPERTY_TOPIA_VERSION,
                                Strategy.PROPERTY_RULES,
                                Strategy.PROPERTY_CROPS);

                        List<CroppingPlanEntry> targetedCrops = new ArrayList<>();
                        for (CroppingPlanEntry croppingPlanEntry : strategy.getCrops()) {
                            CroppingPlanEntry targetedCPE = targetedCroppingPlanEntryByCodes.get(croppingPlanEntry.getCode());
                            targetedCrops.add(targetedCPE);
                        }
                        clonedStrategy.setCrops(targetedCrops);

                        clonedSection.addStrategies(clonedStrategy);

                        // clone strategies
                        if (strategy.getRules() != null) {
                            clonedStrategy.setRules(new ArrayList<>(strategy.getRules()));
                        }
                    }
                }
            }
        }
    }

    @Override
    public List<DecisionRule> getRelatedDecisionRules(String code) {
        return decisionRuleDao.findAllRelatedDecisionRules(code);
    }

    @Override
    public DecisionRule createNewDecisionRuleVersion(String decisionRuleCode, String comment) {
        DecisionRule decisionRule = decisionRuleDao.getLastRuleVersion(decisionRuleCode);
        authorizationService.checkCreateOrUpdateDecisionRule(decisionRule.getTopiaId());

        Binder<DecisionRule, DecisionRule> binder = BinderFactory.newBinder(DecisionRule.class);

        DecisionRule newDecisionRuleVersion = decisionRuleDao.newInstance();
        binder.copyExcluding(decisionRule, newDecisionRuleVersion,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                DecisionRule.PROPERTY_VERSION_NUMBER,
                DecisionRule.PROPERTY_VERSION_REASON,
                DecisionRule.PROPERTY_DECISION_RULE_CROP
        );
        bindDecisionRuleCrop(decisionRule, newDecisionRuleVersion);

        newDecisionRuleVersion.setVersionReason(comment);
        newDecisionRuleVersion.setVersionNumber(decisionRule.getVersionNumber() + 1);

        DecisionRule created = decisionRuleDao.create(newDecisionRuleVersion);

        getTransaction().commit();

        return created;
    }

    protected void bindDecisionRuleCrop(DecisionRule decisionRule, DecisionRule newDecisionRuleVersion) {
        if (decisionRule.getDecisionRuleCrop() != null) {
            Binder<DecisionRuleCrop, DecisionRuleCrop> decisionRuleCropBinder = BinderFactory.newBinder(DecisionRuleCrop.class);
            List<DecisionRuleCrop> newDecisionRuleCrops = Lists.newArrayListWithExpectedSize(decisionRule.getDecisionRuleCrop().size());
            newDecisionRuleVersion.setDecisionRuleCrop(newDecisionRuleCrops);
            for (DecisionRuleCrop decisionRuleCrop : decisionRule.getDecisionRuleCrop()) {
                DecisionRuleCrop newInstance = decisionRuleCropDao.newInstance();
                decisionRuleCropBinder.copyExcluding(decisionRuleCrop, newInstance,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION
                );
                newInstance = decisionRuleCropDao.create(newInstance);
                newDecisionRuleCrops.add(newInstance);
            }
        }
    }

    @Override
    public List<ManagementModeCategory> getAvailableManagementModeCategories(String growingSystemId) {
        List<ManagementModeCategory> usedManagementMode;
        if (!Strings.isNullOrEmpty(growingSystemId)) {
            usedManagementMode = managementModeDao.findManagementModeCategories(growingSystemId);
        } else {
            usedManagementMode = new ArrayList<>(0);
        }
        return ListUtils.subtract(Arrays.asList(ManagementModeCategory.values()), usedManagementMode);
    }

    @Override
    public List<ManagementMode> getRelatedManagementModes(GrowingSystem growingSystem) {
        return managementModeDao.forGrowingSystemEquals(growingSystem).
                setOrderByArguments(ManagementMode.PROPERTY_CATEGORY + " DESC").findAll();
    }

    @Override
    public ManagementMode copyManagementMode(
            String growingSystemId,
            ManagementModeCategory category,
            String mainChangesFromPlanned,
            String changeReasonFromPlanned) {

        Preconditions.checkNotNull(growingSystemId);
        Preconditions.checkNotNull(category);

        GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(growingSystemId).findUnique();

        checkValidityFromGrowwingSystem(growingSystem, "Copy management mode is not possible as on of growing system '%s' or growing plan '%s' or domain '%s' is not activated");

        ManagementMode newManagementMode = managementModeDao.newInstance();

        List<HistoryItem> histories = new ArrayList<>();
        Gson gson = context.getGson();

        ManagementMode plannedManagementMode = managementModeDao.forGrowingSystemEquals(growingSystem).findUniqueOrNull();

        if (plannedManagementMode != null) {
            Binder<ManagementMode, ManagementMode> binder = BinderFactory.newBinder(ManagementMode.class);

            binder.copyExcluding(plannedManagementMode, newManagementMode,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                    ManagementMode.PROPERTY_CATEGORY,
                    ManagementMode.PROPERTY_SECTIONS,
                    ManagementMode.PROPERTY_HISTORICAL);

            newManagementMode.setCategory(ManagementModeCategory.OBSERVED);
            newManagementMode.setChangeReasonFromPlanned(changeReasonFromPlanned);
            newManagementMode.setMainChangesFromPlanned(mainChangesFromPlanned);

            // historic
            histories.add(createManagementModeCopyHistory(plannedManagementMode, newManagementMode));
            newManagementMode.setHistorical(gson.toJson(histories));
        }

        // clone section
        cloneSections(plannedManagementMode, newManagementMode);

        ManagementMode created = managementModeDao.create(newManagementMode);

        getTransaction().commit();

        return created;
    }

    protected void checkValidityFromGrowwingSystem(GrowingSystem growingSystem, String s) {
        Preconditions.checkArgument(growingSystem.isActive() &&
                        growingSystem.getGrowingPlan().isActive() &&
                        growingSystem.getGrowingPlan().getDomain().isActive(),
                String.format(s,
                        growingSystem.getTopiaId(),
                        growingSystem.getGrowingPlan().getTopiaId(),
                        growingSystem.getGrowingPlan().getDomain().getTopiaId()));
    }

    @Override
    public List<GrowingSystem> getGrowingSystemsForManagementMode(NavigationContext navigationContext) {
        return growingSystemDao.getGrowingSystemsForManagementMode(navigationContext, getSecurityContext());
    }

    @Override
    public List<GrowingSystem> getAvailableGsForDuplication(String growingSystemId, NavigationContext navigationContext) {

        Preconditions.checkArgument(!Strings.isNullOrEmpty(growingSystemId));

        GrowingSystem managementModeGrowingSystem = growingSystemDao.forTopiaIdEquals(growingSystemId).findUnique();

        navigationContext = navigationContext != null ? navigationContext : new NavigationContext();
        Set<String> domainIds = navigationContext.getDomains();
        String domainCode = managementModeGrowingSystem.getGrowingPlan().getDomain().getCode();
        List<Domain> domains = domainDao.forCodeEquals(domainCode).findAll();
        domainIds.addAll(domains.stream().map(Domain::getTopiaId).collect(Collectors.toSet()));

        // Dans un premier temps on filtre sur les systèmes de cultures sur lesquels on a le droit en écriture
        GrowingSystemFilter growingSystemFilter = new GrowingSystemFilter();
        growingSystemFilter.setAllPageSize();
        growingSystemFilter.setNavigationContext(navigationContext);
        growingSystemFilter.setActive(true);


        PaginationResult<GrowingSystem> growingSystems = growingSystemDao.getFilteredGrowingSystems(growingSystemFilter, getSecurityContext());
        // filter on parent actives
        List<GrowingSystem> activeGSs = growingSystems.getElements().stream().filter(
                growingSystem -> growingSystem.getGrowingPlan().isActive() &&
                        growingSystem.getGrowingPlan().getDomain().isActive()).collect(Collectors.toList());
        // dans un second temps on ne garde que les sdc pointant vers un domaine du même code que celui du mode de gestion
        //  et sur lesquels il n'y a pas de mode de gestion.
        return growingSystemDao.findAllAvailableForManagementModeDuplication(activeGSs, managementModeGrowingSystem);
    }

    @Override
    public void unactivateDecisionRules(List<String> decisionRuleIds, boolean activate) {
        if (CollectionUtils.isNotEmpty(decisionRuleIds)) {

            for (String decisionRuleId : decisionRuleIds) {
                DecisionRule decisionRule = decisionRuleDao.forTopiaIdEquals(decisionRuleId).findUnique();

                // active/desactive toutes les regles qui ont le même code de duplication
                Collection<DecisionRule> decisionRules = decisionRuleDao.forCodeEquals(decisionRule.getCode()).findAll();
                for (DecisionRule decisionRule2 : decisionRules) {
                    decisionRule2.setActive(activate);
                    decisionRuleDao.update(decisionRule2);
                }
            }
            getTransaction().commit();
        }
    }

    @Override
    public DecisionRule duplicateDecisionRule(String decisionRuleId) {
        DecisionRule decisionRule = decisionRuleDao.forTopiaIdEquals(decisionRuleId).findUnique();
        DecisionRule duplicatedDecisionRule = duplicateDecisionRule(new ExtendContext(true), decisionRule);
        getTransaction().commit();
        return duplicatedDecisionRule;
    }

    @Override
    public ManagementMode duplicateManagementModes(String plannedManagementModeId, String observedManagementModeId, String duplicateManagementModeGrowingSystemId) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(duplicateManagementModeGrowingSystemId));
        // If it exist the 'Planned Management Mode' is return otherwise it's the 'Observed' one.
        ManagementMode result = null;
        if (!Strings.isNullOrEmpty(observedManagementModeId)) {
            result = duplicateManagementMode(observedManagementModeId, duplicateManagementModeGrowingSystemId);
        }
        if (!Strings.isNullOrEmpty(plannedManagementModeId)) {
            result = duplicateManagementMode(plannedManagementModeId, duplicateManagementModeGrowingSystemId);
        }

        getTransaction().commit();

        return result;
    }

    @Override
    public List<DecisionRule> getAllDecisionRules(List<String> decisionRulesIds) {
        return decisionRuleDao.forIn(DecisionRule.PROPERTY_TOPIA_ID, decisionRulesIds).findAll();
    }

    protected ManagementMode duplicateManagementMode(String managementModeId, String growingSystemId) {

        GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(growingSystemId).findUnique();

        checkValidityFromGrowwingSystem(growingSystem, "Duplicate management mode is not possible as on of growing system '%s' or growing plan '%s' or domain '%s' is not activated");

        ManagementMode originalManagementMode = managementModeDao.forTopiaIdEquals(managementModeId).findUnique();
        ManagementMode duplicatedManagementMode = managementModeDao.newInstance();
        Binder<ManagementMode, ManagementMode> managementModeManagementModeBinder = BinderFactory.newBinder(ManagementMode.class);
        managementModeManagementModeBinder.copyExcluding(originalManagementMode, duplicatedManagementMode,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                ManagementMode.PROPERTY_GROWING_SYSTEM,
                ManagementMode.PROPERTY_SECTIONS,
                ManagementMode.PROPERTY_HISTORICAL
        );

        duplicatedManagementMode.setGrowingSystem(growingSystem);

        // clone section
        cloneSections(originalManagementMode, duplicatedManagementMode);

        // historic
        List<HistoryItem> histories = new ArrayList<>();
        Gson gson = context.getGson();

        histories.add(createManagementModeDuplicateHistory(originalManagementMode, duplicatedManagementMode));
        duplicatedManagementMode.setHistorical(gson.toJson(histories));

        return managementModeDao.create(duplicatedManagementMode);
    }

    protected DecisionRule duplicateDecisionRule(ExtendContext extendContext, DecisionRule decisionRule) {

        // perform clone
        Binder<DecisionRule, DecisionRule> decisionRuleBinder = BinderFactory.newBinder(DecisionRule.class);
        DecisionRule clonedDecisionRule = decisionRuleDao.newInstance();
        decisionRuleBinder.copyExcluding(decisionRule, clonedDecisionRule,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                DecisionRule.PROPERTY_DECISION_RULE_CROP);

        bindDecisionRuleCrop(decisionRule, clonedDecisionRule);

        // if duplication, break code
        if (extendContext.isDuplicateOnly()) {
            clonedDecisionRule.setCode(UUID.randomUUID().toString());
            clonedDecisionRule.setVersionNumber(1);
        }

        // persist clone
        clonedDecisionRule = decisionRuleDao.create(clonedDecisionRule);

        return clonedDecisionRule;
    }

    @Override
    public ExportResult exportManagementModesAsXls(Collection<String> growingSystemIds) {

        List<MainBean> mainBeans = new LinkedList<>();
        List<SectionBean> sectionBeans = new LinkedList<>();

        try {
            int count = 0;
            long start = System.currentTimeMillis();

            Map<String, String> groupesCiblesParCode = referentialService.getGroupesCiblesParCode();

            for (String growingSystemId : growingSystemIds) {

                if (count % 10 == 0) {
                    // On fait un nettoyage de session Hibernate à chaque début de paquet -> c'est là qu'on peut le plus tout vider dans la session
                    managementModeDao.clear();
                }
                count++;

                List<ManagementMode> managementModes = managementModeDao.forEquals(ManagementModeTopiaDao.PROPERTY_GROWING_SYSTEM_ID, growingSystemId)
                        .stream()
                        .map(anonymizeService::checkForManagementModeAnonymization).toList();
                for (ManagementMode managementMode : managementModes) {
                    if (log.isInfoEnabled()) {
                        String message = String.format("Modèle décisionnel %s pour le SdC %d", managementMode.getCategory(), count);
                        log.info(message);
                    }
                    exportManagementMode(mainBeans, sectionBeans, managementMode, groupesCiblesParCode);
                }
            }

            if (log.isInfoEnabled() && !mainBeans.isEmpty()) {
                long end = System.currentTimeMillis();
                String message = String.format("%d modèles décisionnels en %ds : %dms par MM", mainBeans.size(), (end - start) / 1000, (end - start) / mainBeans.size());
                log.info(message);
            }
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't copy properties", ex);
        }

        // technical export
        EntityExporter exporter = new EntityExporter();

        ExportModelAndRows<MainBean> mainTab = new ExportModelAndRows<>(new MailModel(), mainBeans);
        ExportModelAndRows<SectionBean> sectionTab = new ExportModelAndRows<>(new SectionModel(), sectionBeans);

        return exporter.exportAsXlsx("Modèle-décisionnel-export", mainTab, sectionTab);
    }

    @Override
    public void exportManagementModesAsXlsAsync(Collection<String> growingSystemIds) {
        AuthenticatedUser user = getAuthenticatedUser();

        ManagementModeExportTask exportTask = new ManagementModeExportTask(user.getTopiaId(), user.getEmail(), growingSystemIds);
        getBusinessTaskManager().schedule(exportTask);
    }

    protected void exportManagementMode(List<MainBean> mainTabBeans,
                                        List<SectionBean> sectionTabBeans,
                                        ManagementMode managementMode, Map<String, String> groupesCiblesParCode) {
        // common fields
        GrowingSystem growingSystem = managementMode.getGrowingSystem();
        GrowingPlan growingPlan = growingSystem.getGrowingPlan();
        Domain domain = growingPlan.getDomain();

        String networkNames = CollectionUtils.emptyIfNull(growingSystem.getNetworks())
                .stream()
                .map(Network::getName)
                .collect(Collectors.joining(", "));
        String typeAgriculture = Optional.ofNullable(growingSystem.getTypeAgriculture())
                .map(RefTypeAgriculture::getReference_label)
                .orElse(null);

        final CommonBean baseBean = new CommonBean (
                managementMode.getCategory(),
                domain.getLocation().getDepartement(),
                networkNames,
                growingSystem.getName(),
                typeAgriculture,
                growingSystem.getDephyNumber(),
                growingPlan.getName(),
                domain.getName(),
                domain.getCampaign()
        );

        MainBean mainBean = new MainBean(baseBean);
        mainBean.setChangeReason(managementMode.getChangeReason());
        mainBean.setChangeReasonFromPlanned(managementMode.getChangeReasonFromPlanned());
        mainBean.setMainChanges(managementMode.getMainChanges());
        mainBean.setMainChangesFromPlanned(managementMode.getMainChangesFromPlanned());
        mainTabBeans.add(mainBean);

        //sections
        Collection<Section> sections = managementMode.getSections();
        for (Section section : CollectionUtils.emptyIfNull(sections)) {
            List<Strategy> strategies = section.getStrategies();
            // strategies
            if (strategies != null && !strategies.isEmpty()) {
                for (Strategy strategy : strategies) {
                    SectionBean sectionBean = new SectionBean(baseBean);

                    // section;
                    sectionBean.setSectionType(section.getSectionType());
                    sectionBean.setAgronomicObjective(section.getAgronomicObjective());
                    sectionBean.setCategoryObjective(section.getCategoryObjective());
                    sectionBean.setExpectedResult(section.getExpectedResult());
                    sectionBean.setBioAgressorType(section.getBioAgressorType());
                    sectionBean.setGroupeCible(groupesCiblesParCode.get(section.getCodeGroupeCibleMaa()));
                    Optional.ofNullable(section.getBioAgressor())
                            .map(RefBioAgressor::getLabel)
                            .ifPresent(sectionBean::setBioAgressor);

                    sectionBean.setDamageType(section.getDamageType());

                    // strategies
                    sectionBean.setImplementationType(strategy.getImplementationType());
                    sectionBean.setStrategyType(strategy.getStrategyType());
                    sectionBean.setExplanation(strategy.getExplanation());

                    Optional.ofNullable(strategy.getRefStrategyLever())
                            .map(RefStrategyLever::getLever)
                            .ifPresent(sectionBean::setStrategyLever);
                    sectionBean.setExplanation(strategy.getExplanation());

                    String joinedCropsNames = "";
                    if (CollectionUtils.isNotEmpty(strategy.getCrops())) {
                        joinedCropsNames = strategy.getCrops().stream()
                                .map(CroppingPlanEntry::getName)
                                .collect(Collectors.joining(", "));
                    }
                    sectionBean.setCrops(joinedCropsNames);

                    sectionTabBeans.add(sectionBean);
                }

            } else {
                SectionBean sectionBean = new SectionBean(baseBean);

                sectionBean.setSectionType(section.getSectionType());
                sectionBean.setAgronomicObjective(section.getAgronomicObjective());
                sectionBean.setCategoryObjective(section.getCategoryObjective());
                sectionBean.setExpectedResult(section.getExpectedResult());
                sectionBean.setBioAgressorType(section.getBioAgressorType());
                sectionBean.setSdcConcerneBioAgresseur(section.getSdcConcerneBioAgresseur() != null ? section.getSdcConcerneBioAgresseur() : false);
                sectionBean.setGroupeCible(groupesCiblesParCode.get(section.getCodeGroupeCibleMaa()));
                Optional.ofNullable(section.getBioAgressor())
                        .map(RefBioAgressor::getLabel)
                        .ifPresent(sectionBean::setBioAgressor);

                sectionTabBeans.add(sectionBean);
            }
        }
    }

    @Override
    public ExportResult exportDecisionRulesAsXls(Collection<String> decisionRuleIds) {

        List<DecisionRuleBean> beans = new LinkedList<>();
        try {
            if (CollectionUtils.isNotEmpty(decisionRuleIds)) {
                Map<String, String> cropNamesByCode = croppingPlanEntryDao.findAllCropsNamesByCodes();
                Iterable<DecisionRule> decisionRules = decisionRuleDao.forTopiaIdIn(decisionRuleIds)
                        .findAllLazy(100);
                Map<String, String> groupesCiblesParCode = referentialService.getGroupesCiblesParCode();
                for (DecisionRule decisionRule : decisionRules) {
                    DecisionRuleBean bean = generateDecisionRuleExportBean(decisionRule, cropNamesByCode, groupesCiblesParCode);
                    beans.add(bean);
                }
            }

        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't copy properties", ex);
        }
        // technical export
        EntityExporter exporter = new EntityExporter();

        ExportModelAndRows<DecisionRuleBean> mainTab = new ExportModelAndRows<>(new DecisionRuleModel(), beans);

        return exporter.exportAsXlsx("Règles-de-décisions-export", mainTab);
    }

    @Override
    public void exportDecisionRulesAsXlsAsync(Collection<String> decisionRuleIds) {
        AuthenticatedUser user = getAuthenticatedUser();

        DecisionRuleExportTask exportTask = new DecisionRuleExportTask(user.getTopiaId(), user.getEmail(), decisionRuleIds);
        getBusinessTaskManager().schedule(exportTask);
    }

    protected DecisionRuleBean generateDecisionRuleExportBean(DecisionRule decisionRule,
                                                              Map<String, String> cropNamesByCode,
                                                              Map<String, String> groupesCiblesParCode) {
        Domain domain = domainDao.forCodeEquals(decisionRule.getDomainCode()).findAny();
        domain = anonymizeService.checkForDomainAnonymization(domain);

        DecisionRuleBean bean = new DecisionRuleBean();
        bean.setDomainName(domain.getName());

        bean.setName(decisionRule.getName());
        bean.setVersionNumber(decisionRule.getVersionNumber());
        bean.setInterventionType(decisionRule.getInterventionType());
        Collection<DecisionRuleCrop> decisionRuleCrops = decisionRule.getDecisionRuleCrop();
        if (CollectionUtils.isNotEmpty(decisionRuleCrops)) {
            String decisionRuleCropNames = decisionRuleCrops.stream()
                    .map(DecisionRuleCrop::getCroppingPlanEntryCode)
                    .map(cropNamesByCode::get)
                    .filter(StringUtils::isNotEmpty)
                    .collect(Collectors.joining(", "));
            bean.setDecisionRuleCrop(decisionRuleCropNames);
        }

        bean.setBioAgressorType(decisionRule.getBioAgressorType());
        bean.setGroupeCible(groupesCiblesParCode.get(decisionRule.getCodeGroupeCibleMaa()));
        RefBioAgressor bioAgressor = decisionRule.getBioAgressor();
        Optional.ofNullable(bioAgressor)
                .map(RefBioAgressor::getLabel)
                .ifPresent(bean::setBioAgressor);
        bean.setDecisionObject(decisionRule.getDecisionObject());
        bean.setDomainValidity(decisionRule.getDomainValidity());
        bean.setSource(decisionRule.getSource());
        bean.setUsageComment(decisionRule.getUsageComment());
        bean.setObjective(decisionRule.getObjective());
        bean.setExpectedResult(decisionRule.getExpectedResult());
        bean.setSolution(decisionRule.getSolution());
        bean.setDecisionCriteria(decisionRule.getDecisionCriteria());
        bean.setResultCriteria(decisionRule.getResultCriteria());
        bean.setObservation(decisionRule.getObservation());
        bean.setSolutionComment(decisionRule.getSolutionComment());
        bean.setVersionReason(decisionRule.getVersionReason());
        return bean;
    }

    @Override
    public List<RefStrategyLever> loadRefStrategyLevers(
            Sector sector,
            String growingSystemTopiaId,
            SectionType sectionType,
            StrategyType strategyType) {

        List<RefStrategyLever> results = null;
        if (growingSystemTopiaId != null || sectionType != null) {
            if (sector != null) {
                results = referentialService.loadRefStrategyLeversForTerm(sector, sectionType, strategyType, null);
            } else {
                results = referentialService.loadRefStrategyLevers(growingSystemTopiaId, sectionType, strategyType);
            }
        }
        return results;
    }

    @Override
    public List<RefStrategyLever> loadRefStrategyLeversForTerm(
            Sector sector,
            String growingSystemTopiaId,
            SectionType sectionType,
            StrategyType strategyType,
            String term) {
    
        List<RefStrategyLever> results;
        if (sector == null && growingSystemTopiaId != null) {
            sector = growingSystemDao.forTopiaIdEquals(growingSystemTopiaId).findUnique().getSector();
        }

        results = referentialService.loadRefStrategyLeversForTerm(sector, sectionType, strategyType, term);
        
        return results;
    }

    @Override
    public void removeManagementModes(Set<String> managementModeIds) {
        if (CollectionUtils.isNotEmpty(managementModeIds)) {
            managementModeDao.deleteAll(managementModeDao.forTopiaIdIn(managementModeIds).findAllLazy());
            getTransaction().commit();
        }
    }

    @Override
    public PaginationResult<ManagementModeDto> loadWritableManagementModesForGrowingSystemIds(List<String> growingSystemIds) {
        Pair<Map<GrowingSystem, ManagementModeDto>, PaginationResult<ManagementModeDto>> result0 =
                managementModeDao.loadActiveReadableManagementModesForGrowingSystemIds(growingSystemIds, getSecurityContext());
        return getManagementModeDtoPaginationResult(result0);
    }

    @Override
    public boolean isDecisionRuleActive(DecisionRule decisionRule) {

        boolean result = decisionRule.isActive();
        if (result) {
            result = managementModeDao.isDecisionRuleActive(decisionRule);
        }
        return result;

    }
}
