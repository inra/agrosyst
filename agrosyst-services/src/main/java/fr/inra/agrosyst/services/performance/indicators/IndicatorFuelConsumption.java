package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Consommation de carburant.
 *
 * Présentation de l’indicateur
 * La consommation de carburant d’une combinaison d’outils, d’un automoteur ou d’un tracteur correspond à la quantité de carburant utilisée lors d’une intervention culturale. Les interventions concernées par cet indicateur sont toutes les interventions qui mobilisent une combinaison d’outils.
 * <p>
 * Formule de calcul
 *  Consommation(i) = Temps d'utilisation machine * Puissance * taux de charge * C
 *
 * Avec :
 * Consommation : consommation de carburant (l ha-1)
 * Puissance : puissance de l’automoteur ou du tracteur (ch). Donnée issue d’un référentiel.
 * Taux de charge : taux de charge du tracteur ou de l’automoteur (sans unité). Donnée issue d’un référentiel.
 * Si un tracteur ou un automoteur est utilisé seul, le taux de charge correspond au taux de charge par défaut de ce tracteur ou de cet automoteur.
 * Si un tracteur est utilisé avec un ou plusieurs outils, le taux de charge correspond au taux de charge de l’outil imposant le plus fort taux de charge.
 *   - C=0.24 pour les tracteurs et automoteurs de moins de 130 Ch
 *   - C=0.21 pour les tracteurs et automoteurs de plus de 130 Ch
 *   charge de 100% (l ch-1), source BCMA.
 * Echelles de présentation de la variable calculée
 * Échelle de la variable d’intérêt :
 * La consommation de carburant peut être calculée pour :
 * 1 intervention
 * x interventions (permet les regroupements d’interventions de même type EDI : traction, entretien, travail du sol, semis, épandage, traitements phytosanitaires, opérations en vert, récolte, transport, stockage, conditionnement, irrigation, transformation, divers).
 * 1 campagne culturale
 * 1 campagne agricole
 * toutes les campagnes agricoles
 *
 * Échelle spatiale :
 * La consommation de carburant peut être présentée aux échelles spatiales de :
 * la zone,
 * la parcelle,
 * la sole du SdC
 * le domaine
 *
 * Échelle temporelle :
 * La consommation de carburant peut être présentée aux échelles temporelles de :
 * 1 année
 * plusieurs années,
 *
 * Les modalités de changement d’échelle sont présentées ici.
 * Données de référence
 * Les données de référence mobilisées dans le calcul de la consommation de carburant sont : la puissance du tracteur ou de l’automoteur ainsi que le taux de charge imposé par les outils de traction ou moyen pour un tracteur ou un automoteur utilisé sans outil. Ces données sont issues du référentiel BCMA. Onglets BCMA_xxx du fichier référentiel.
 *
 * @author Eric Chatellier
 */
public class IndicatorFuelConsumption extends AbstractIndicator {
    
    private static final Log LOGGER = LogFactory.getLog(IndicatorFuelConsumption.class);
    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);
    
    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) && displayed;
    }
    
    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    // see #10061
    protected static final Double DEFAULT_REF_MATERIAL_POWER = 80d;
    protected static final Double DEFAULT_LOAD_RATE = 1d;
    
    // C=0.24 pour les tracteurs et automoteurs de moins de 130 Ch
    // C=0.21 pour les tracteurs et automoteurs de plus de 130 Ch
    // charge de 100% (l ch-1), source BCMA.
    protected static final Double TO_130_POW_FUEL_CONSUMPTION = 0.24;
    protected static final Double FROM_130_POW_FUEL_CONSUMPTION = 0.21;
    
    public IndicatorFuelConsumption() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION ,"consommation_de_carburant_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES ,"consommation_de_carburant_detail_champs_non_renseig");
    }
    
    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        
        indicatorNameToColumnName.put(getIndicatorLabel(0), "consommation_de_carburant");
        
        return indicatorNameToColumnName;
    }
    
    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_SOCIO_TECHNIC);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, "Indicator.label.fuelConsumption");
    }
    
    @Override
    public Double[] manageIntervention(
            WriterContext writerContext,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedCropExecutionContext cropContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            PracticedCropCyclePhase phase) {
        
        if (interventionContext.isFictive()) return newResult(0d);
        
        Double[] result = null;
        
        PracticedIntervention intervention = interventionContext.getIntervention();
        ToolsCoupling toolsCoupling = interventionContext.getToolsCoupling();
        Double toolsCouplingWorkingTime = interventionContext.getToolsCouplingWorkingTime();
        
        if (intervention.getType() == AgrosystInterventionType.IRRIGATION) {
            result = newResult(0.0d); // Because for Irrigation, workRate is always 0.
            
        } else if (toolsCoupling != null && !toolsCoupling.isManualIntervention() && toolsCouplingWorkingTime != null) {// Remember : only one toolsCoupling by intervention
            
            result = computeInterventionFuelConsumption(intervention, toolsCoupling, toolsCouplingWorkingTime);
            
        }
        
        if (result != null) {
            final double fuelConsumption = result[0];
            interventionContext.setFuelConsumption(fuelConsumption);
        }
        
        return result;
    }

    protected Double computePower(RefMateriel materiel) {

        Double puissance = null;
        if (materiel instanceof RefMaterielAutomoteur || materiel instanceof RefMaterielTraction) {
            puissance = materiel instanceof RefMaterielAutomoteur ?
                    ((RefMaterielAutomoteur) materiel).getPuissanceChISO() : ((RefMaterielTraction) materiel).getPuissanceChIso();
        }
        return puissance;
    }
    
    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {
        
        EffectiveIntervention intervention = interventionContext.getIntervention();
        ToolsCoupling toolsCoupling = interventionContext.getToolsCoupling();
        Double toolsCouplingWorkingTime = interventionContext.getToolsCouplingWorkingTime();
        
        Double[] result;
        if (intervention.getType() == AgrosystInterventionType.IRRIGATION) {
            result = newResult(0.0d); // Because for Irrigation, workRate is always 0.

        } else {
            result = computeInterventionFuelConsumption(intervention, toolsCoupling, toolsCouplingWorkingTime);
        }
    
        final double fuelConsumption = result[0];
        interventionContext.setFuelConsumption(fuelConsumption);

        return result;
    }
    
    protected Double[] computeInterventionFuelConsumption(TopiaEntity intervention, ToolsCoupling toolsCoupling, Double workingTime) {
        return Optional.ofNullable(toolsCoupling)
                .map(ToolsCoupling::getTractor)
                .map(tracteur -> {
                    RefMateriel tractorRefMateriel = tracteur.getRefMateriel();

                    Double puissance = computePower(tractorRefMateriel);

                    Double tauxDeCharge = computeLoadRate(toolsCoupling, tractorRefMateriel);

                    return computeFuelConsumption(intervention.getTopiaId(), workingTime, puissance, tauxDeCharge);
                })
                .orElse(newResult(0d));
    }

    protected Double computeLoadRate(ToolsCoupling toolsCoupling, RefMateriel tractorMateriel) {

        Double tractorLoadRate = computeTractorLoadRate(tractorMateriel);

        return findHigherLoadRate(tractorLoadRate, toolsCoupling);
    }

    private Double computeTractorLoadRate(RefMateriel tractorMateriel) {
        Double tauxDeCharge = null;

        if (tractorMateriel instanceof RefMaterielAutomoteur || tractorMateriel instanceof RefMaterielTraction) {
            tauxDeCharge = tractorMateriel instanceof RefMaterielAutomoteur ?
                    ((RefMaterielAutomoteur) tractorMateriel).getDonneesTauxDeChargeMoteur()
                    : ((RefMaterielTraction) tractorMateriel).getDonneesTauxDeChargeMoteur();
        }

        return tauxDeCharge;
    }

    protected Double[] computeFuelConsumption(String interventionId, Double workingTime, Double puissance, Double loadRate) {

        // puissance
        incrementAngGetTotalFieldCounterForTargetedId(interventionId);
        // tauxDeCharge
        incrementAngGetTotalFieldCounterForTargetedId(interventionId);

        Double[] result;

        if (loadRate == null) {
            // refs #8559 : use 9999 as default work rate value
            loadRate = DEFAULT_LOAD_RATE;
            addMissingFieldMessage(interventionId, messageBuilder.getMissingWorkingTimeMessage());
        }

        if (puissance == null) {
            // refs #8559 : use 9999 as default work rate value
            puissance = DEFAULT_REF_MATERIAL_POWER;
            addMissingRefMaterielPowerMessage(interventionId);
        }

        // puissance may be null if ther was no tractor
        // C=0.24 pour les tracteurs et automoteurs de moins de 130 Ch
        // C=0.21 pour les tracteurs et automoteurs de plus de 130 Ch
        double c = puissance < 130d ? TO_130_POW_FUEL_CONSUMPTION : FROM_130_POW_FUEL_CONSUMPTION;
        double fuelConsumption = workingTime * puissance * loadRate * c;
        result = newResult(fuelConsumption);

        return result;
    }

    protected Double findHigherLoadRate(Double loadRate, ToolsCoupling toolsCoupling) {
        if (loadRate != null && toolsCoupling.getEquipments() != null) {
            double equipmentLoadRate = getEquipmentHighestLoadRate(toolsCoupling);
            loadRate = loadRate > equipmentLoadRate ? loadRate : equipmentLoadRate;
        }
        return loadRate;
    }

    protected Double getEquipmentHighestLoadRate(ToolsCoupling toolsCoupling) {
        double highestLoadRate = 0d;
        for (Equipment equipment : toolsCoupling.getEquipments()) {
            if (equipment.getRefMateriel() instanceof RefMaterielOutil) {
                Double loadRate = ((RefMaterielOutil)equipment.getRefMateriel()).getDonneesTauxDeChargeMoteur();
    
                if (loadRate != null && loadRate > highestLoadRate) {
                    highestLoadRate = loadRate;
                }
            }
        }
        return highestLoadRate;
    }
    
    public void init(IndicatorFilter fuelConsumptionIndicatorFilter) {
        displayed = fuelConsumptionIndicatorFilter != null;
    }
}
