package fr.inra.agrosyst.services.effective.export;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.FormatUtils;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.ModalityDephy;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.services.performance.DataFormatter;
import fr.inra.agrosyst.services.performance.dbPersistence.Column;
import fr.inra.agrosyst.services.performance.dbPersistence.TableRowModel;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import org.apache.commons.lang3.StringUtils;

import java.sql.Date;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class EffectiveDbModel implements DataFormatter {
    
    protected static class EffectiveCommonRowModel extends TableRowModel {

        final String SEPARATOR = ", ";

        public EffectiveCommonRowModel(String id,
                                       Date performanceDate,
                                       String tableName,
                                       Collection<CroppingPlanSpecies> species,
                                       String performanceId,
                                       Domain domain,
                                       Optional<GrowingSystem> optionalGrowingSystem,
                                       String its,
                                       String irs) {
            
            super(id, tableName);
            
            String refSpeciesNames = FormatUtils.getSpeciesName(species, SEPARATOR);
            String refVarietyNames = FormatUtils.getVarietyName(species, SEPARATOR);
            
            addColumn(Column.createCommonTableColumn("performance_id", performanceId, String.class));
            addColumn(Column.createCommonTableColumn("date_calcul", performanceDate, Date.class));
            addColumn(Column.createCommonTableColumn("nom_domaine_exploitation", domain.getName(), String.class));
            addColumn(Column.createCommonTableColumn("id_domaine", domain.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("annee", domain.getCampaign(), Integer.class));
            addColumn(Column.createCommonTableColumn("domaine_type", domain.getType(), DomainType.class));
            addColumn(Column.createCommonTableColumn("departement", domain.getLocation().getDepartement(), String.class));
            addColumn(Column.createCommonTableColumn("nom_reseau_it", its, String.class));
            addColumn(Column.createCommonTableColumn("nom_reseau_ir", irs, String.class));
            addColumn(Column.createCommonTableColumn("especes", refSpeciesNames, String.class));
            addColumn(Column.createCommonTableColumn("varietes", refVarietyNames, String.class));
            addColumn(Column.createCommonTableColumn("approche_de_calcul", "réalisé", String.class));
    
            if (optionalGrowingSystem.isPresent()) {
                GrowingSystem growingSystem = optionalGrowingSystem.get();
                addColumn(Column.createCommonTableColumn("id_dispositif", growingSystem.getGrowingPlan().getTopiaId(), String.class));
                addColumn(Column.createCommonTableColumn("type_dispositif", growingSystem.getGrowingPlan().getType(), TypeDEPHY.class));
                addColumn(Column.createCommonTableColumn("filiere_sdc", growingSystem.getSector(), Sector.class));
                addColumn(Column.createCommonTableColumn("nom_sdc", growingSystem.getName(), String.class));
                addColumn(Column.createCommonTableColumn("id_sdc", growingSystem.getTopiaId(), String.class));
                addColumn(Column.createCommonTableColumn("num_dephy", growingSystem.getDephyNumber(), String.class));
                addColumn(Column.createCommonTableColumn("type_conduite_sdc", growingSystem.getTypeAgriculture() == null ? "" : growingSystem.getTypeAgriculture().getReference_label(), String.class));
                addColumn(Column.createCommonTableColumn("sdc_valide", growingSystem.isValidated(), Boolean.class));
            }
        }
    }
    
    public static class InterventionRowModel extends EffectiveCommonRowModel implements DataFormatter {
        
        public InterventionRowModel(String performanceId,
                                    Date performanceDate,
                                    EffectiveIntervention intervention,
                                    Collection<AbstractAction> actions,
                                    Collection<String> codeAmmBioControle,
                                    Domain domain,
                                    Optional<GrowingSystem> growingSystem,
                                    String its,
                                    String irs,
                                    Plot plot,
                                    Zone zone,
                                    CroppingPlanEntry crop,
                                    Integer rank,
                                    CroppingPlanEntry previousCrop,
                                    EffectiveCropCyclePhase phase,
                                    String interventionYealdAverages,
                                    Map<String, String> groupesCiblesByCode) {
    
            super(intervention.getTopiaId(),
                    performanceDate,
                    "realise_echelle_intervention",
                    crop.getCroppingPlanSpecies(),
                    performanceId,
                    domain,
                    growingSystem,
                    its,
                    irs);
    
            String startingDate = INTERVENTION_DATE_FORMAT_DDMMYYYY.format(intervention.getStartInterventionDate());
            String endingDate = INTERVENTION_DATE_FORMAT_DDMMYYYY.format(intervention.getEndInterventionDate());
            String actionNames = getActionsToString(actions);
            Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(actions, domain);
            String inputNames = getInputUsagesToString(inputUsages, actions, crop);
            String groupeCible = getGroupesCiblesToString(inputUsages, groupesCiblesByCode);
            String target = getInputUsageTargetsToString(inputUsages);
            Boolean biocontrole = isOneInputUsageBiocontrole(inputUsages, codeAmmBioControle);
            String interventionSpeciesNames = getInterventionSpeciesName(intervention);
            String interventionVarietyNames = getInterventionVarietyName(intervention);
    
            addColumn(Column.createCommonTableColumn("parcelle", plot.getName(), String.class));
            addColumn(Column.createCommonTableColumn("parcelle_id", plot.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("zone", zone.getName(), String.class));
            addColumn(Column.createCommonTableColumn("zone_id", zone.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("culture", crop.getName(), String.class));
            addColumn(Column.createCommonTableColumn("culture_id", crop.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("culture_code", crop.getCode(), String.class));
            addColumn(Column.createCommonTableColumn("rang", rank, Integer.class));
            addColumn(Column.createCommonTableColumn("especes_concernees_par_intervention", interventionSpeciesNames, String.class));
            addColumn(Column.createCommonTableColumn("varietes_concernees_par_intervention", interventionVarietyNames, String.class));
            addColumn(Column.createCommonTableColumn("phase", phase != null ? phase.getType() : null, CropCyclePhaseType.class));
            addColumn(Column.createCommonTableColumn("phase_id", phase != null ? phase.getTopiaId() : null, String.class));
            addColumn(Column.createCommonTableColumn("intervention", intervention.getName(), String.class));
            addColumn(Column.createCommonTableColumn("intervention_id", intervention.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("debut_intervention", startingDate, String.class));
            addColumn(Column.createCommonTableColumn("fin_intervention", endingDate, String.class));
            addColumn(Column.createCommonTableColumn("actions", actionNames, String.class));
            addColumn(Column.createCommonTableColumn("intrants", inputNames, String.class));
            addColumn(Column.createCommonTableColumn("nb_intrants", getNbInputUsages(inputUsages), Integer.class));
            addColumn(Column.createCommonTableColumn("groupe_cible", groupeCible, String.class));
            addColumn(Column.createCommonTableColumn("cibles_traitement", target, String.class));
            addColumn(Column.createCommonTableColumn("biocontrole", biocontrole, Boolean.class));
            addColumn(Column.createCommonTableColumn("rendement_total", StringUtils.defaultString(interventionYealdAverages), String.class));
            addColumn(Column.createCommonTableColumn("intervention_eDaplos_issuer_id", StringUtils.defaultString(intervention.getEdaplosIssuerId()), String.class));

            if (previousCrop != null) {
                addColumn(Column.createCommonTableColumn("culture_precedente", previousCrop.getName(), String.class));
                addColumn(Column.createCommonTableColumn("culture_precedente_id", previousCrop.getTopiaId(), String.class));
                addColumn(Column.createCommonTableColumn("culture_precedente_code", previousCrop.getCode(), String.class));
            }
        }
    }
    
    public static class CropRowModel extends EffectiveCommonRowModel {
        
        private static String getCropRowId(CroppingPlanEntry crop, Integer rank, CroppingPlanEntry previousCrop) {
            return crop.getTopiaId() + '-' + rank + (previousCrop == null ? "" : previousCrop.getTopiaId());
        }
        
        public CropRowModel(String performanceId,
                            Date performanceDate,
                            CroppingPlanEntry crop,
                            Integer rank,
                            CroppingPlanEntry previousCrop,
                            Domain domain,
                            Optional<GrowingSystem> optionalGrowingSystem,
                            String its,
                            String irs,
                            EffectiveCropCyclePhase phase,
                            String printableYealdAverage) {
    
            super(getCropRowId(crop, rank, previousCrop),
                    performanceDate,
                    "realise_echelle_culture",
                    crop.getCroppingPlanSpecies(),
                    performanceId,
                    domain,
                    optionalGrowingSystem,
                    its,
                    irs);
    
            addColumn(Column.createCommonTableColumn("culture", crop.getName(), String.class));
            addColumn(Column.createCommonTableColumn("culture_id", crop.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("culture_code", crop.getCode(), String.class));
            addColumn(Column.createCommonTableColumn("rang", rank, Integer.class));
            addColumn(Column.createCommonTableColumn("rendement", printableYealdAverage, String.class));
    
            if (previousCrop != null) {
                addColumn(Column.createCommonTableColumn("culture_precedente", previousCrop.getName(), String.class));
                addColumn(Column.createCommonTableColumn("culture_precedente_id", previousCrop.getTopiaId(), String.class));
                addColumn(Column.createCommonTableColumn("culture_precedente_code", previousCrop.getCode(), String.class));
            }
    
            if (phase != null) {
                addColumn(Column.createCommonTableColumn("phase", phase.getType(), CropCyclePhaseType.class));
                addColumn(Column.createCommonTableColumn("phase_id", phase.getTopiaId(), String.class));
            }
        }
    }
    
    public static class PlotRowModel extends EffectiveCommonRowModel {
        
        public PlotRowModel(String performanceId,
                            Date performanceDate,
                            Plot plot,
                            Domain domain,
                            Optional<GrowingSystem> optionalGrowingSystem,
                            String its,
                            String irs,
                            String printableYealdAverage,
                            Collection<CroppingPlanSpecies> croppingPlanSpecies,
                            String zoneIds) {
            
            super(plot.getTopiaId(),
                    performanceDate,
                    "realise_echelle_parcelle",
                    croppingPlanSpecies,
                    performanceId,
                    domain,
                    optionalGrowingSystem,
                    its,
                    irs);
            
            addColumn(Column.createCommonTableColumn("parcelle", plot.getName(), String.class));
            addColumn(Column.createCommonTableColumn("parcelle_id", plot.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("zone_id", zoneIds, String.class));
            addColumn(Column.createCommonTableColumn("rendement", printableYealdAverage, String.class));
            addColumn(Column.createCommonTableColumn("parcelle_eDaplos_issuer_id", StringUtils.defaultString(plot.geteDaplosIssuerId()), String.class));

        }
    }
    
    public static class EffectiveItkRowModel extends EffectiveCommonRowModel {
        
        public EffectiveItkRowModel(
                Date performanceDate,
                String performanceId,
                Zone zone,
                Plot plot,
                Domain domain,
                Optional<GrowingSystem> optionalGrowingSystem,
                String its,
                String irs,
                CroppingPlanEntry crop,
                Optional<Integer> rank,
                Optional<EffectiveCropCyclePhase> phase,
                Optional<CroppingPlanEntry> previousCrop,
                Optional<EffectiveCropCycleNode> effectiveCropCycleNode) {
            
            //Sdc_id * Parcelle_id * Zone_id * Phase_id * Culture_id * Precedent_id
            super(zone.getTopiaId(),
                    performanceDate,
                    "realise_echelle_ITK",
                    crop.getCroppingPlanSpecies(),
                    performanceId,
                    domain,
                    optionalGrowingSystem,
                    its,
                    irs);
            
            addColumn(Column.createCommonTableColumn("parcelle", plot.getName(), String.class));
            addColumn(Column.createCommonTableColumn("parcelle_id", plot.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("parcelle_eDaplos_issuer_id", StringUtils.defaultString(plot.geteDaplosIssuerId()), String.class));
            
            addColumn(Column.createCommonTableColumn("zone", zone.getName(), String.class));
            addColumn(Column.createCommonTableColumn("zone_id", zone.getTopiaId(), String.class));
    
            addColumn(Column.createCommonTableColumn("culture", crop.getName(), String.class));
            addColumn(Column.createCommonTableColumn("culture_id", crop.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("culture_code", crop.getCode(), String.class));
            addColumn(Column.createCommonTableColumn("rang", rank.orElse(null), Integer.class));
            
            addColumn(Column.createCommonTableColumn("culture_precedente", previousCrop.isPresent() ? previousCrop.get().getName() : "", String.class));
            addColumn(Column.createCommonTableColumn("culture_precedente_id", previousCrop.isPresent() ? previousCrop.get().getTopiaId() : "", String.class));
            addColumn(Column.createCommonTableColumn("culture_precedente_code", previousCrop.isPresent() ? previousCrop.get().getCode() : "", String.class));
            
            addColumn(Column.createCommonTableColumn("phase", phase.isPresent() ? phase.get().getType() : "", CropCyclePhaseType.class));
            addColumn(Column.createCommonTableColumn("phase_id", phase.isPresent() ? phase.get().getTopiaId() : "", String.class));

            addColumn(Column.createCommonTableColumn("noeuds_realise_id", effectiveCropCycleNode.isPresent() ? effectiveCropCycleNode.get().getTopiaId() : "", String.class));
        }
    }
    
    public static class ZoneRowModel extends EffectiveCommonRowModel {
        
        public ZoneRowModel(Date performanceDate,
                            String performanceId,
                            Zone zone,
                            Plot plot,
                            Optional<GrowingSystem> optionalGrowingSystem,
                            String its,
                            String irs,
                            String printableYealdAverage,
                            List<CroppingPlanSpecies> croppingPlanSpecies,
                            Domain domain) {
            
            super(zone.getTopiaId(),
                    performanceDate,
                    "realise_echelle_zone",
                    croppingPlanSpecies,
                    performanceId,
                    domain,
                    optionalGrowingSystem,
                    its,
                    irs);
            
            addColumn(Column.createCommonTableColumn("parcelle", plot.getName(), String.class));
            addColumn(Column.createCommonTableColumn("parcelle_id", plot.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("zone", zone.getName(), String.class));
            addColumn(Column.createCommonTableColumn("zone_id", zone.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("rendement", printableYealdAverage, String.class));
            addColumn(Column.createCommonTableColumn("parcelle_eDaplos_issuer_id", StringUtils.defaultString(plot.geteDaplosIssuerId()), String.class));
        }
    }
    
    public static class SdcRowModel extends EffectiveCommonRowModel {
        
        public SdcRowModel(String performanceId,
                           Date performanceDate,
                           String growingSystemId,
                           Collection<CroppingPlanSpecies> agrosystSpecies,
                           Domain domain,
                           Optional<GrowingSystem> optionalGrowingSystem,
                           String its,
                           String irs) {
    
            super(growingSystemId,
                    performanceDate,
                    "realise_echelle_sdc",
                    agrosystSpecies,
                    performanceId,
                    domain,
                    optionalGrowingSystem,
                    its,
                    irs);
    
            optionalGrowingSystem.ifPresent(
                    growingSystem -> addColumn(
                            Column.createCommonTableColumn("modalite_suivi_dephy",
                                    growingSystem.getModality(),
                                    ModalityDephy.class)));
        }
    }
    
}
