package fr.inra.agrosyst.services.performance.dbPersistence;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

/**
 * Représente une colomne en base de données ou CSV
 * @author Cossé David : cosse@codelutin.com
 */
public class Column {
    
    // nom de la colomne en base
    protected final String dbColumnName;
    // sa valeur à stocker
    protected Object value;
    // type de l'object
    protected Type valueType;
    
    private Column(String dbColumnName) {
        this.dbColumnName = dbColumnName;
    }
    
    private Column(String dbColumnName, Object value, Type valueType) {
        if ("nan".equals(value) || "NaN".equals(value)) {
            value = null;
        }
    
        this.dbColumnName = dbColumnName;
        this.value = valideObjectType(dbColumnName, value, valueType);
        this.valueType = valueType;
    }
    
    /**
     * La colonne représente un élément de l'indicateur
     * Cette colonne aura sa valeur de renseignée au moment du calcul
     *
     * @param dbColumnName le nom de la colomne en base
     * @return La colomne
     */
    public static Column createIndicatorColumn(String dbColumnName) {
        return new Column(dbColumnName);
    }
    
    /**
     * Affecte la valeur de l'indicateur calculé
     * @param value la valeur calculé, peut être null
     * @param type le typage de la valeur
     */
    public void setIndicatorValue(Object value, Type type) {
        if ("nan".equals(value) || "NaN".equals(value)) {
            value = null;
        }
        this.value = valideObjectType(dbColumnName, value, type);
        this.valueType = type;
    }
    
    /**
     * Cette colonne fait partie des colomnes communes à la table
     * Elle sont toujours renseigné et indépendante des indicateurs sélectionnés
     *
     * @param dbColumnName le nom de la colomne en base
     * @param value        la valeur
     * @param type         Le typage de la valeur
     * @return La colomne
     */
    public static Column createCommonTableColumn(String dbColumnName, Object value, Type type) {
        return new Column(dbColumnName, value, type);
    }
    
    /**
     * @return la valeur s'il y en a une
     */
    public Optional<Object> getValue() {
        return Optional.ofNullable(value);
    }
    
    public String getDbColumnName() {
        return dbColumnName;
    }
    
    public Type geValueType() {
        return this.valueType;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Column column = (Column) o;
        return Objects.equals(dbColumnName, column.dbColumnName);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(dbColumnName);
    }
    
    private Object valideObjectType(String dbColumnName, Object value, Type valueType) {
        Object result = null;
        if (value != null) {
            Class valueTypeClazz = (Class) valueType;
            if (value.getClass().isAssignableFrom(valueTypeClazz)) {
                if (Double.class.isAssignableFrom(valueTypeClazz)) {
                    result = Double.isNaN((Double) value) ? null : value;
                } else if (Float.class.isAssignableFrom(valueTypeClazz)) {
                    result = Float.isNaN((Float) value) ? null : value;
                } else {
                    result = value;
                }
            } else {
                if (String.class.isAssignableFrom(value.getClass())) {
                    String stValue = (String) value;
                    if (StringUtils.isNotBlank(stValue)) {
                        try {
                            if (Enum.class.isAssignableFrom(valueTypeClazz)) {
                                stValue = stValue.replace("." + valueTypeClazz.getSimpleName(), "");
                                result = Enum.valueOf(valueTypeClazz, stValue).name();
                            } else if (Boolean.class.isAssignableFrom(valueTypeClazz)) {
                                result = Boolean.valueOf(stValue);
                            } else if (Double.class.isAssignableFrom(valueTypeClazz)) {
                                result = Double.valueOf(stValue);
                            } else if (Integer.class.isAssignableFrom(valueTypeClazz)) {
                                result = Integer.valueOf(stValue);
                            } else if (Float.class.isAssignableFrom(valueTypeClazz)) {
                                result = Float.valueOf(stValue);
                            } else if (LocalDateTime.class.equals(valueType)) {
                                result = stValue;// not transform at that time
                            }
                        } catch (Exception e) {
                            throw new AgrosystTechnicalException(
                                    String.format(
                                            "Pour le colonne %s, de type %s un objet de type incompatible %s est passé en paramètre",
                                            dbColumnName,
                                            valueType,
                                            value));
                        }
                    }
                    
                } else {
                    throw new AgrosystTechnicalException(
                            String.format(
                                    "Pour le colonne %s, de type %s un objet de type incompatible %s est passé en paramètre",
                                    dbColumnName,
                                    valueType,
                                    value));
                }
            }
        }
        return result;
    }
    
    @Override
    public String toString() {
        return dbColumnName;
    }
}
