package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;

/**
 * All species export model.
 *
 * Columns:
 * <ul>
 * <li>Libellé espèce botanique
 * </ul>
 *
 * @author David Cossé
 */
public class AllSpeciesExportModel extends AbstractTranslatableExportModel<AllSpeciesExport> {

    private static final Log LOGGER = LogFactory.getLog(AllSpeciesExportModel.class);

    public AllSpeciesExportModel(Map<String, String> translations) {
        super(translations);
        newColumnForExport(AllSpeciesExport.PROPERTY_LIBELLE_ESPECE_BOTANIQUE_QUALIFIANT_AEE);
        newColumnForExport(AllSpeciesExport.PROPERTY_VARIETE_LABEL);
    }

}
