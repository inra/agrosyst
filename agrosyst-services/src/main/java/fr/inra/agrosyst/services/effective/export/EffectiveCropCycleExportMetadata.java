package fr.inra.agrosyst.services.effective.export;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.services.effective.EffectivePerennialCropCycleDto;
import fr.inra.agrosyst.services.common.export.ExportModel;
import fr.inra.agrosyst.services.export.ExportMetadata;
import lombok.Getter;
import lombok.Setter;

public class EffectiveCropCycleExportMetadata extends ExportMetadata {

    public static class EffectiveBean extends Bean {
        public EffectiveBean(String zoneName,
                             String plotName,
                             String departement,
                             String networkNames,
                             String growingSystemName,
                             String typeAgricultureLabel,
                             String dephyNumber,
                             String growingPlanName,
                             String domainName,
                             int campaign) {
            super(departement,
                    networkNames,
                    growingSystemName,
                    typeAgricultureLabel,
                    dephyNumber,
                    growingPlanName,
                    domainName,
                    campaign);
            this.zoneName = zoneName;
            this.plotName = plotName;
        }

        public static <T extends Bean> void columns(ExportModel<T> m) {
            m.newColumn("Zone", Bean::getZoneName);
            m.newColumn("Parcelle", Bean::getPlotName);
            baseColumnsWithCampaign(m);
        }
    }

    @Getter
    @Setter
    public static class CropCycleBean extends Bean {

        String cycleType;
        String cropName;
        String intermediateCropName;
        Integer rank;
        CropCyclePhaseType phase;
        Integer duration;

        EffectivePerennialCropCycleDto dto;

        public CropCycleBean(Bean baseBean) {
            super(baseBean);
        }
    }

    public static class CropCycleModel extends ExportModel<CropCycleBean> {

        @Override
        public String getTitle() {
            return "Cycles";
        }

        public CropCycleModel() {
            EffectiveBean.columns(this);

            newColumn("Cycle", CropCycleBean::getCycleType);
            newColumn("Culture", CropCycleBean::getCropName);
            newColumn("Culture Intermédiaire", CropCycleBean::getIntermediateCropName);
            newColumn("Rang", CropCycleBean::getRank);

            newColumn("Phase", CropCycleBean::getPhase);
            newColumn("Durée de la phase", CropCycleBean::getDuration);

            // Champs qui passent par délégation à un EffectivePerennialCropCycleDto
            newColumn("Année de plantation",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getPlantingYear);
            newColumn("Inter-rang de plantation",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getPlantingInterFurrow);
            newColumn("Espacement de plantation sur le rang",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getPlantingSpacing);
            newColumn("Densité de plantation",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getPlantingDensity);
            newColumn("Forme fruitière vergers",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getOrchardFrutalForm);
            newColumn("Hauteur de frondaison",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getFoliageHeight);
            newColumn("Épaisseur de frondaison",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getFoliageThickness);
            newColumn("Forme fruitière vigne",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getVineFrutalForm);
            newColumn("Orientation des rangs",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getOrientationLabel);
            newColumn("Taux de mortalité dans la plantation",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getPlantingDeathRate);
            newColumn("Année de mesure de ce taux de mortalité",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getPlantingDeathRateMeasureYear);
            newColumn("Type d'enherbement",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getWeedType);
            newColumn("Pollinisateurs",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::isPollinator);
            newColumn("% de pollinisateur",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getPollinatorPercent);
            newColumn("Mode de répartition des pollinisateurs",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getPollinatorSpreadMode);
            newColumn("Autres caractéristiques du couvert végétal",
                    CropCycleBean::getDto,
                    EffectivePerennialCropCycleDto::getOtherCharacteristics);

        }
    }

    public static class PerennialCropCycleSpeciesModel extends ExportModel<PerennialCropCycleSpeciesBean> {
        @Override
        public String getTitle() {
            return "Espèces des phases de production des cultures pérennes";
        }

        public PerennialCropCycleSpeciesModel() {
            EffectiveBean.columns(this);
            PerennialCropCycleSpeciesBean.columnsWithoutProfilVegetatifBBCH(this);
        }

    }

    @Getter
    @Setter
    public static class ItkBean extends Bean {

        protected int transitCount;

        public ItkBean(Bean baseBean) {
            super(baseBean);
        }

        public ItkBean(ItkBean itkBean) {
            super(itkBean);
            this.transitCount = itkBean.transitCount;
        }
    }

    public static class ItkModel extends ExportModel<ItkBean> {

        @Override
        public String getTitle() {
            return "Interventions";
        }

        public ItkModel() {
            EffectiveBean.columns(this);
            interventionColumns(this);
            newColumn("Nombre de passage", ItkBean::getTransitCount);
            interventionDetailsColumns(this);
        }
    }

    public static class ItkActionApplicationProduitsMinerauxModel extends ExportModel<ItkActionApplicationProduitsMinerauxBean> {

        @Override
        public String getTitle() {
            return "Application de produits minéraux";
        }


        public ItkActionApplicationProduitsMinerauxModel() {
            EffectiveBean.columns(this);
            ItkActionApplicationProduitsMinerauxBean.columns(this);
        }
    }

    public static class ItkActionApplicationProduitsPhytoModel extends ExportModel<ItkApplicationProduitsPhytoBean> {

        @Override
        public String getTitle() {
            return "Application de phyto avec AMM";
        }

        public ItkActionApplicationProduitsPhytoModel() {
            EffectiveBean.columns(this);
            ItkApplicationProduitsPhytoBean.columns(this);
        }
    }

    public static class ItkActionAutreModel extends ExportModel<ItkActionAutreBean> {

        @Override
        public String getTitle() {
            return "Autre";
        }

        public ItkActionAutreModel() {
            EffectiveBean.columns(this);
            ItkActionAutreBean.columns(this);
        }
    }

    public static class ItkActionEntretienTailleVigneModel extends ExportModel<ItkActionEntretienTailleVigneBean> {

        @Override
        public String getTitle() {
            return "Entretien-Taille de vigne et verger";
        }

        public ItkActionEntretienTailleVigneModel() {
            EffectiveBean.columns(this);
            ItkActionEntretienTailleVigneBean.columns(this);
        }
    }
    
    public static class ItkActionEpandageOrganiqueModel extends ExportModel<ItkActionEpandageOrganiqueBean> {

        @Override
        public String getTitle() {
            return "Épandage organique";
        }

        public ItkActionEpandageOrganiqueModel() {
            EffectiveBean.columns(this);
            ItkActionEpandageOrganiqueBean.columns(this);
        }
    }

    public static class ItkActionIrrigationModel extends ExportModel<ItkActionIrrigationBean> {

        @Override
        public String getTitle() {
            return "Irrigation";
        }

        public ItkActionIrrigationModel() {
            EffectiveBean.columns(this);
            ItkActionIrrigationBean.columns(this);
        }
    }

    public static class ItkActionLutteBiologiqueModel extends ExportModel<ItkActionLutteBiologiqueBean> {

        @Override
        public String getTitle() {
            return "Application de phyto sans AMM";
        }

        public ItkActionLutteBiologiqueModel() {
            EffectiveBean.columns(this);
            ItkActionLutteBiologiqueBean.columns(this);
        }
    }

    public static class ItkActionRecolteModel extends ExportModel<ItkActionRecolteBean> {

        @Override
        public String getTitle() {
            return "Récolte";
        }

        public ItkActionRecolteModel() {
            EffectiveBean.columns(this);
            ItkActionRecolteBean.columns(this);
        }
    }

    public static class ItkActionSemiModel extends ExportModel<ItkActionSemiBean> {

        @Override
        public String getTitle() {
            return "Semis";
        }

        public ItkActionSemiModel() {
            EffectiveBean.columns(this);
            ItkActionSemiBean.columns(this);
        }
    }

    public static class ItkActionTransportModel extends ExportModel<ItkActionTransportBean> {

        @Override
        public String getTitle() {
            return "Transport";
        }

        public ItkActionTransportModel() {
            EffectiveBean.columns(this);
            ItkActionTransportBean.columns(this);
        }
    }

    public static class ItkActionTravailSolModel extends ExportModel<ItkActionTravailSolBean> {

        @Override
        public String getTitle() {
            return "Travail du sol";
        }

        public ItkActionTravailSolModel() {
            EffectiveBean.columns(this);
            interventionColumns(this);
            ItkActionTravailSolBean.columns(this);
        }
    }
}
