package fr.inra.agrosyst.services.common;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by davidcosse on 31/01/17.
 */
class PricesByHarvestingProducts {
    private final List<RefHarvestingPrice> pricesForCampaign;
    private Map<String, List<RefHarvestingPrice>> pricesByHarvestingProduct;
    private PriceUnit priceUnit;
    
    public PricesByHarvestingProducts(List<RefHarvestingPrice> pricesForCampaign) {
        this.pricesForCampaign = pricesForCampaign;
    }
    
    public Map<String, List<RefHarvestingPrice>> getPricesByHarvestingProduct() {
        return pricesByHarvestingProduct;
    }
    
    public PriceUnit getPriceUnit() {
        return priceUnit;
    }
    
    public PricesByHarvestingProducts invoke() {
        pricesByHarvestingProduct = new HashMap<>();
        Collection<RefHarvestingPrice> refHarvestingPrices = CollectionUtils.emptyIfNull(pricesForCampaign);

        for (RefHarvestingPrice refHarvestingPrice : refHarvestingPrices) {
            priceUnit = priceUnit != null ? priceUnit : refHarvestingPrice.getPriceUnit();
            PriceUnit valorisationPriceUnit = refHarvestingPrice.getPriceUnit();
            boolean isValid = priceUnit.equals(valorisationPriceUnit);
            
            if (isValid) {
                String espece = refHarvestingPrice.getProduitRecolte();
                List<RefHarvestingPrice> pricesForHarvestingProduct = pricesByHarvestingProduct.computeIfAbsent(espece, k -> Lists.newArrayList());
                pricesForHarvestingProduct.add(refHarvestingPrice);
            }
        }
        return this;
    }
}
