package fr.inra.agrosyst.services.async;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import fr.inra.agrosyst.api.services.async.Task;
import fr.inra.agrosyst.services.AgrosystServiceConfig;
import fr.inra.agrosyst.services.ServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Supplier;

/**
 * S'occupe de l'ordonnancement des traitements (asynchrones). La seule méthode à utiliser est {@link #schedule(Task)}
 * qui prend en charge la tâche et rend la main. La tâche est ensuite traitée au prochain créneau adapté disponible (que
 * ce soit immédiat ou différé en attendant son tour dans la file).
 * <p>
 * Chaque tâche s'exécute ensuite au sein d'un {@link TaskRunner} dans un {@link Thread} séparé.
 *
 */
public final class BusinessTasksManager extends AbstractTasksManager {
    
    private static final Log log = LogFactory.getLog(BusinessTasksManager.class);
    
    private final AgrosystServiceConfig config;
    private final Supplier<ServiceContext> serviceContextSupplier;

    public BusinessTasksManager(AgrosystServiceConfig config, Supplier<ServiceContext> serviceContextSupplier) {
        this.config = config;
        this.serviceContextSupplier = serviceContextSupplier;
    }

    /**
     * On garde une copie des jobs en cours d'exécution pour l'affichage dans l'admin
     */
    private final ConcurrentLinkedQueue<ScheduledTask> runningTasks = new ConcurrentLinkedQueue<>();

    private final BlockingQueue<ScheduledTaskRunnable> queuedRunnables = new LinkedBlockingQueue<>();
    private final BlockingQueue<ScheduledTaskRunnable> immediateRunnables = new LinkedBlockingQueue<>();

    // FIXME AThimel 25/06/2021 Les executors devrait être fermés (.shutdown()) à l'arrêt de l'application
    private ExecutorService queueExecutor;
    private ExecutorService immediateExecutor;

    @Override
    protected Log getLog() {
        return log;
    }
    
    @Override
    protected ExecutorService getImmediateExecutor() {
        if (immediateExecutor == null) {
            int asyncImmediatePoolSize = config.getAsyncImmediatePoolSize();
            immediateExecutor = buildExecutor(asyncImmediatePoolSize, "async-immediate-%d", immediateRunnables);
        }
        return immediateExecutor;
    }

    @Override
    protected ExecutorService getQueueExecutor() {
        if (queueExecutor == null) {
            queueExecutor = buildExecutor(1, "async-queued", queuedRunnables);
        }
        return queueExecutor;
    }

    @Override
    protected Supplier<ServiceContext> newServiceContextSupplier() {
        return serviceContextSupplier;
    }

    @Override
    protected ConcurrentLinkedQueue<ScheduledTask> getRunningTasks() {
        return runningTasks;
    }

    @Override
    protected BlockingQueue<ScheduledTaskRunnable> getQueuedRunnables() {
        return queuedRunnables;
    }

    @Override
    protected BlockingQueue<ScheduledTaskRunnable> getImmediateRunnables() {
        return immediateRunnables;
    }

}
