/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.services.practiced.export;

import fr.inra.agrosyst.api.FormatUtils;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.ToolsCouplingTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDI;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.action.HarvestingActionValorisationDto;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.itk.SpeciesStadeDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleConnectionDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleNodeDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCyclePhaseDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleSpeciesDto;
import fr.inra.agrosyst.api.services.practiced.PracticedInterventionDto;
import fr.inra.agrosyst.api.services.practiced.PracticedPerennialCropCycleDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSeasonalCropCycleDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.common.export.EntityExporter;
import fr.inra.agrosyst.services.common.export.ExportModelAndRows;
import fr.inra.agrosyst.services.export.ExportContext;
import fr.inra.agrosyst.services.export.ExportMetadata;
import fr.inra.agrosyst.services.export.XlsExporterService;
import fr.inra.agrosyst.services.practiced.export.PracticedSystemMetadata.ITKModel;
import fr.inra.agrosyst.services.practiced.export.PracticedSystemMetadata.MainBean;
import fr.inra.agrosyst.services.practiced.export.PracticedSystemMetadata.MainModel;
import fr.inra.agrosyst.services.practiced.export.PracticedSystemMetadata.PerennialCropCycleBean;
import fr.inra.agrosyst.services.practiced.export.PracticedSystemMetadata.PerennialCropCycleBeanModel;
import fr.inra.agrosyst.services.practiced.export.PracticedSystemMetadata.PerennialCropCycleSpeciesBeanModel;
import fr.inra.agrosyst.services.practiced.export.PracticedSystemMetadata.SeasonalCropCycleBean;
import fr.inra.agrosyst.services.practiced.export.PracticedSystemMetadata.SeasonalCropCycleBeanModel;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Setter
public class PracticedExportXls extends XlsExporterService {

    private static final Log log = LogFactory.getLog(PracticedExportXls.class);

    protected AnonymizeService anonymizeService;
    protected DomainService domainService;
    protected PracticedSystemService practicedSystemService;

    protected PracticedSystemTopiaDao practicedSystemDao;
    protected ToolsCouplingTopiaDao toolsCouplingDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;

    protected Map<String, String> groupesCiblesParCode;

    public ExportResult exportPracticedSystemsAsXlsStream(Collection<String> practicedSystemIds) {

        groupesCiblesParCode = referentialService.getGroupesCiblesParCode();

        List<MainBean> mainBeans = new LinkedList<>();
        List<SeasonalCropCycleBean> seasonalCropCycleTab = new LinkedList<>();
        List<PerennialCropCycleBean> perennialCropCycleTab = new LinkedList<>();
        List<ExportMetadata.PerennialCropCycleSpeciesBean> perennialCropCycleSpeciesTab = new LinkedList<>();
        List<PracticedSystemMetadata.ItkBean> itkTab = new LinkedList<>();
        ExportContext xlsContext = new ExportContext();

        try {

            long start = System.currentTimeMillis();

            Map<String, String> toolsCouplingNames = toolsCouplingDao.findAllToolsCouplingNamesByCode();

            for (String practicedSystemId : CollectionUtils.emptyIfNull(practicedSystemIds)) {

                // On commence par faire du ménage pour libérer la mémoire
                practicedSystemDao.clear();

                Iterable<PracticedSystem> practicedSystems = practicedSystemDao.forTopiaIdEquals(practicedSystemId).findAll();
                for (PracticedSystem practicedSystem : practicedSystems) {

                    // anonymize practiced system
                    practicedSystem = anonymizeService.checkForPracticedSystemAnonymization(practicedSystem);

                    // main tab
                    String campaigns = practicedSystem.getCampaigns();

                    GrowingSystem growingSystem = practicedSystem.getGrowingSystem();

                    RefTypeAgriculture typeAgriculture = growingSystem.getTypeAgriculture();
                    String typeAgricultureLabel = null;
                    if (typeAgriculture != null) {
                        typeAgricultureLabel = typeAgriculture.getReference_label();
                    }

                    String networkNames = null;
                    if (growingSystem.isNetworksNotEmpty()) {
                        networkNames = growingSystem.getNetworks().stream()
                                .filter(Objects::nonNull)
                                .map(Network::getName)
                                .distinct()
                                .collect(Collectors.joining(", "));
                    }

                    GrowingPlan growingPlan = growingSystem.getGrowingPlan();
                    Domain domain = growingPlan.getDomain();

                    PracticedSystemMetadata.PracticedBean baseBean = new PracticedSystemMetadata.PracticedBean(
                            practicedSystem.getName(),
                            campaigns,
                            domain.getLocation().getDepartement(),
                            networkNames,
                            growingSystem.getName(),
                            typeAgricultureLabel,
                            growingSystem.getDephyNumber(),
                            growingPlan.getName(),
                            domain.getName(),
                            domain.getCampaign()
                    );

                    String domainCode = domain.getCode();
                    List<CroppingPlanEntry> croppingPlanCodeForDomainsAndCampaigns =
                            domainService.getCroppingPlanEntriesForDomainCodeAndCampaigns(domainCode, CommonService.GET_CAMPAIGNS_SET.apply(campaigns));
                    Map<String, CroppingPlanSpecies> croppingPlanSpeciesByCode = new HashMap<>();
                    for (CroppingPlanEntry croppingPlanEntry : croppingPlanCodeForDomainsAndCampaigns) {
                        List<CroppingPlanSpecies> cropSpecies = croppingPlanEntry.getCroppingPlanSpecies();
                        if (CollectionUtils.isNotEmpty(cropSpecies)) {
                            for (CroppingPlanSpecies croppingPlanSpecies : cropSpecies) {
                                croppingPlanSpeciesByCode.put(croppingPlanSpecies.getCode(), croppingPlanSpecies);
                            }
                        }
                    }

                    MainBean mainBean = new MainBean(baseBean);
                    mainBean.setSource(practicedSystem.getSource());
                    mainBean.setComment(practicedSystem.getComment());
                    mainBeans.add(mainBean);

                    // seasonal crops cycles
                    List<PracticedSeasonalCropCycleDto> seasonalCropCycleDtos = practicedSystemService.getAllPracticedSeasonalCropCycles(practicedSystem.getTopiaId());
                    List<PracticedPerennialCropCycleDto> perennialCycleDtos = practicedSystemService.getAllPracticedPerennialCropCycles(practicedSystem.getTopiaId());

                    for (PracticedSeasonalCropCycleDto seasonalCropCycleDto : seasonalCropCycleDtos) {

                        // build entity for seasonal crop cycle exports
                        Map<String, PracticedSeasonalEntityExport> seasonalCropCycleEntityExports = new HashMap<>();
                        List<PracticedCropCycleNodeDto> nodeDtos = seasonalCropCycleDto.getCropCycleNodeDtos();

                        for (PracticedCropCycleNodeDto node : nodeDtos) {
                            PracticedSeasonalEntityExport entityExport = new PracticedSeasonalEntityExport();
                            entityExport.setLabel(node.getLabel());
                            entityExport.setEndCycle(node.isEndCycle());
                            entityExport.setSameCampaignAsPreviousNode(node.isSameCampaignAsPreviousNode());
                            entityExport.setInitNodeFrequency(node.getInitNodeFrequency());
                            entityExport.setX(node.getX());

                            seasonalCropCycleEntityExports.put(node.getNodeId(), entityExport);
                        }

                        List<PracticedCropCycleConnectionDto> cropCycleConnectionDtos = seasonalCropCycleDto.getCropCycleConnectionDtos();
                        for (PracticedCropCycleConnectionDto connection : cropCycleConnectionDtos) {

                            // intermediateCropName
                            if (StringUtils.isNotBlank(connection.getIntermediateCropName())) {
                                PracticedSeasonalEntityExport entityExport = seasonalCropCycleEntityExports.get(connection.getTargetId());
                                entityExport.setIntermediateCropName(connection.getIntermediateCropName());
                            }
                            // previousCrop
                            if (StringUtils.isNotBlank(connection.getSourceId())) {
                                PracticedSeasonalEntityExport sourceEntityExport = seasonalCropCycleEntityExports.get(connection.getSourceId());
                                PracticedSeasonalEntityExport targetEntityExport = seasonalCropCycleEntityExports.get(connection.getTargetId());
                                if (!connection.isNotUsedForThisCampaign()) targetEntityExport.addPreviousCrop(sourceEntityExport.getLabel());
                            }
                            // croppingPlanEntryFrequency
                            if (connection.getCroppingPlanEntryFrequency() != null) {
                                PracticedSeasonalEntityExport targetEntityExport = seasonalCropCycleEntityExports.get(connection.getTargetId());
                                targetEntityExport.setCroppingPlanEntryFrequency(connection.getCroppingPlanEntryFrequency());
                            }
                        }

                        // seasonal crop cycle tab
                        for (PracticedSeasonalEntityExport entityExport : seasonalCropCycleEntityExports.values()) {
                            SeasonalCropCycleBean cropCycleBean = toSeasonalCropCycleBean(baseBean, entityExport);
                            seasonalCropCycleTab.add(cropCycleBean);
                        }

                        // sesonal ITK
                        for (PracticedCropCycleConnectionDto connection : cropCycleConnectionDtos) {
                            List<PracticedInterventionDto> itks = connection.getInterventions();

                            if (itks != null) {
                                PracticedSeasonalEntityExport node = seasonalCropCycleEntityExports.get(connection.getTargetId());
                                String cropName = node.getLabel();
                                Integer rank = node.getX();

                                for (PracticedInterventionDto itk : itks) {
                                    // Il faut seulement exporter le nom de la culture précédente "réelle" de l'itk et non pas précédents possible de la culture
                                    PracticedSeasonalEntityExport sourceEntityExport = seasonalCropCycleEntityExports.get(connection.getSourceId());
                                    String previousCropName = sourceEntityExport != null ? sourceEntityExport.getLabel() : "";
                                    PracticedSystemMetadata.PracticedBean itkBaseBean = exportCommonInterventionFields(baseBean, true,
                                            cropName, previousCropName, rank, null, itk);
                                    // add tools couplings, species, actions, inputs fields
                                    exportToolsCouplingsSpeciesActionsInputsFields(itkTab, itk, itkBaseBean, croppingPlanSpeciesByCode, toolsCouplingNames);
                                    // add actions and inputs fields
                                    Collection<AbstractActionDto> actionDtos = itk.getActionDtos();

                                    Supplier<Map<String, ExportMetadata.SpeciesAndVariety>> refEspeceByCPSCodeSupplier = newRefEspeceByCPSCodeSupplier(itk, itkBaseBean);

                                    for (AbstractActionDto actionDto : actionDtos) {
                                        exportActionAndInputFields(xlsContext, itkBaseBean, itk, actionDto, refEspeceByCPSCodeSupplier);
                                    }
                                }
                            }
                        }

                    }

                    if (CollectionUtils.isNotEmpty(perennialCycleDtos)) {
                        for (PracticedPerennialCropCycleDto cycleDto : perennialCycleDtos) {
                            String cropName = cycleDto.getCroppingPlanEntryName();
                            // phases
                            for (PracticedCropCyclePhaseDto phase : cycleDto.getCropCyclePhaseDtos()) {
                                PerennialCropCycleBean export = toPerennialCropCycleBean(baseBean, cycleDto, cropName, phase);
                                perennialCropCycleTab.add(export);

                                // itks
                                List<PracticedInterventionDto> itks = phase.getInterventions();

                                if (itks != null && !itks.isEmpty()) {
                                    for (PracticedInterventionDto itk : itks) {
                                        PracticedSystemMetadata.PracticedBean itkBaseBean = exportCommonInterventionFields(baseBean, false,
                                                cropName, null, null, phase.getType(), itk);
                                        // add tools couplings, species, actions, inputs fields
                                        exportToolsCouplingsSpeciesActionsInputsFields(itkTab, itk, itkBaseBean, croppingPlanSpeciesByCode, toolsCouplingNames);
                                        // add actions and inputs fields
                                        Collection<AbstractActionDto> actionDtos = itk.getActionDtos();

                                        Supplier<Map<String, ExportMetadata.SpeciesAndVariety>> refEspeceByCPSCodeSupplier = newRefEspeceByCPSCodeSupplier(itk, itkBaseBean);

                                        for (AbstractActionDto actionDto : actionDtos) {
                                            exportActionAndInputFields(xlsContext, itkBaseBean, itk, actionDto, refEspeceByCPSCodeSupplier);
                                        }
                                    }
                                }
                            }

                            // species
                            List<PracticedCropCycleSpeciesDto> speciesDtos = cycleDto.getSpeciesDto();
                            if (!speciesDtos.isEmpty()) {
                                for (PracticedCropCyclePhaseDto phase : cycleDto.getCropCyclePhaseDtos()) {
                                    for (PracticedCropCycleSpeciesDto speciesDto : speciesDtos) {
                                        ExportMetadata.PerennialCropCycleSpeciesBean export = toPerennialCropCycleSpeciesBean(baseBean, cycleDto, phase, speciesDto);
                                        perennialCropCycleSpeciesTab.add(export);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (log.isInfoEnabled()) {
                long end = System.currentTimeMillis();
                long duration = end - start;
                int count = practicedSystemIds.size();
                log.warn(String.format("%dms pour %d systèmes synthétisés : %dms/each", duration, count, duration / count));
            }

        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't copy properties", ex);
        }
        // technical export
        EntityExporter entityExporter = new EntityExporter();

        return entityExporter.exportAsXlsx(
                "Systemes-synthetises-export",
            new ExportModelAndRows<>(new MainModel(), mainBeans),
            new ExportModelAndRows<>(new SeasonalCropCycleBeanModel(), seasonalCropCycleTab),
            new ExportModelAndRows<>(new PerennialCropCycleBeanModel(), perennialCropCycleTab),
            new ExportModelAndRows<>(new PerennialCropCycleSpeciesBeanModel(), perennialCropCycleSpeciesTab),
            new ExportModelAndRows<>(new ITKModel(), itkTab),
            new ExportModelAndRows<>(new PracticedSystemMetadata.ItkActionApplicationProduitsMinerauxModel(), xlsContext.getApplicationProduitsMineraux()),
            new ExportModelAndRows<>(new PracticedSystemMetadata.ItkActionApplicationProduitsPhytoModel(), xlsContext.getApplicationProduitsPhytos()),
            new ExportModelAndRows<>(new PracticedSystemMetadata.ItkActionAutreModel(), xlsContext.getActionAutres()),
            new ExportModelAndRows<>(new PracticedSystemMetadata.ItkActionEntretienTailleVigneModel(), xlsContext.getActionEntretienTailleVignes()),
            new ExportModelAndRows<>(new PracticedSystemMetadata.ItkActionEpandageOrganiqueModel(), xlsContext.getActionEpandageOrganiques()),
            new ExportModelAndRows<>(new PracticedSystemMetadata.ItkActionIrrigationModel(), xlsContext.getActionIrrigations()),
            new ExportModelAndRows<>(new PracticedSystemMetadata.ItkActionLutteBiologiqueModel(), xlsContext.getActionLutteBiologiques()),
            new ExportModelAndRows<>(new PracticedSystemMetadata.ItkActionRecolteModel(), xlsContext.getActionRecoltes()),
            new ExportModelAndRows<>(new PracticedSystemMetadata.ItkActionSemiModel(), xlsContext.getActionSemis()),
            new ExportModelAndRows<>(new PracticedSystemMetadata.ItkActionTransportModel(), xlsContext.getActionTransports()),
            new ExportModelAndRows<>(new PracticedSystemMetadata.ItkActionTravailSolModel(), xlsContext.getActionTravailSolBeans())
        );
    }

    protected Supplier<Map<String, ExportMetadata.SpeciesAndVariety>> newRefEspeceByCPSCodeSupplier(PracticedInterventionDto itk, PracticedSystemMetadata.PracticedBean itkBaseBean) {
        return new Supplier<>() {
            Map<String, ExportMetadata.SpeciesAndVariety> result;
    
            @Override
            public Map<String, ExportMetadata.SpeciesAndVariety> get() {
                if (result == null) {
                    Map<String, ExportMetadata.SpeciesAndVariety> refEspeceByCPSCode = new HashMap<>();
                    String practicedSystemCampaigns = itkBaseBean.getCampaigns();
                    Set<Integer> campaigns = CommonService.GET_CAMPAIGNS_SET.apply(practicedSystemCampaigns);
                    Set<String> interventionCpSpeciesCodes = itk.getSpeciesStadesDtos()
                            .stream()
                            .map(SpeciesStadeDto::getSpeciesCode)
                            .collect(Collectors.toSet());
                    List<CroppingPlanSpecies> interventionCpSpecies = croppingPlanSpeciesDao.getCroppingPlanSpeciesForCodeAndCampaigns(interventionCpSpeciesCodes, campaigns);
                    for (CroppingPlanSpecies cpSpecies : interventionCpSpecies) {
                        refEspeceByCPSCode.put(cpSpecies.getCode(), new ExportMetadata.SpeciesAndVariety(cpSpecies.getSpecies(), cpSpecies.getVariety(), cpSpecies.getEdaplosUnknownVariety()));
                    }
                    result = refEspeceByCPSCode;
                }
                return result;
            }
        };
    }

    protected ExportMetadata.PerennialCropCycleSpeciesBean toPerennialCropCycleSpeciesBean(PracticedSystemMetadata.PracticedBean baseBean,
                                                                                           PracticedPerennialCropCycleDto cycleDto,
                                                                                           PracticedCropCyclePhaseDto phase,
                                                                                           PracticedCropCycleSpeciesDto speciesDto) {
        ExportMetadata.PerennialCropCycleSpeciesBean export = new ExportMetadata.PerennialCropCycleSpeciesBean(baseBean);

        export.setCroppingPlanEntryName(cycleDto.getCroppingPlanEntryName());
        export.setPhase(phase.getType());

        export.setSpeciesEspece(speciesDto.getSpeciesEspece());
        export.setSpeciesQualifiant(speciesDto.getSpeciesQualifiant());
        export.setSpeciesTypeSaisonnier(speciesDto.getSpeciesTypeSaisonnier());
        export.setSpeciesDestination(speciesDto.getSpeciesDestination());
        if (StringUtils.isNotBlank(speciesDto.getVarietyLibelle())) {
            export.setVarietyLibelle(speciesDto.getVarietyLibelle());
        } else if (StringUtils.isNotBlank(speciesDto.getEdaplosUnknownVariety())) {
            Locale locale = getSecurityContext().getLocale();
            export.setVarietyLibelle(I18n.l(locale,"fr.inra.agrosyst.api.services.edaplos.unknownVariety", speciesDto.getEdaplosUnknownVariety()));
        }
        export.setProfil_vegetatif_BBCH(speciesDto.getProfil_vegetatif_BBCH());
        export.setPlantCertified(speciesDto.isPlantCertified());
        export.setOverGraftDate(speciesDto.getOverGraftDate());

        String graftSupport = speciesDto.getGraftSupport() != null ? speciesDto.getGraftSupport().getLabel() : null;
        export.setGraftSupport(graftSupport);

        String graftClone = speciesDto.getGraftClone() != null ? speciesDto.getGraftClone().getLabel() : null;
        export.setGraftClone(graftClone);
        return export;
    }

    protected PerennialCropCycleBean toPerennialCropCycleBean(PracticedSystemMetadata.PracticedBean baseBean, PracticedPerennialCropCycleDto cycleDto, String cropName, PracticedCropCyclePhaseDto phase) {
        PerennialCropCycleBean export = new PerennialCropCycleBean(baseBean);
        PracticedPerennialCropCycle perennialCropCycle = cycleDto.getPracticedPerennialCropCycle();
        export.setPlantingYear(perennialCropCycle.getPlantingYear());
        export.setPlantingInterFurrow(perennialCropCycle.getPlantingInterFurrow());
        export.setPlantingSpacing(perennialCropCycle.getPlantingSpacing());
        export.setPlantingDensity(perennialCropCycle.getPlantingDensity());
        export.setOrchardFrutalForm(perennialCropCycle.getOrchardFrutalForm());
        export.setVineFrutalForm(perennialCropCycle.getVineFrutalForm());
        Optional.ofNullable(perennialCropCycle.getOrientation())
                .map(RefOrientationEDI::getReference_label)
                .ifPresent(export::setOrientation);
        export.setPlantingDeathRate(perennialCropCycle.getPlantingDeathRate());
        export.setPlantingDeathRateMeasureYear(perennialCropCycle.getPlantingDeathRateMeasureYear());
        export.setWeedType(perennialCropCycle.getWeedType());
        export.setPollinator(perennialCropCycle.isPollinator());
        export.setPollinatorPercent(perennialCropCycle.getPollinatorPercent());
        export.setPollinatorSpreadMode(perennialCropCycle.getPollinatorSpreadMode());
        export.setOtherCharacteristics(perennialCropCycle.getOtherCharacteristics());
        export.setLabel(cropName);
        export.setSolOccupationPercent(perennialCropCycle.getSolOccupationPercent());
        export.setPhaseType(phase.getType());
        export.setPhaseDuration(phase.getDuration());
        return export;
    }

    protected SeasonalCropCycleBean toSeasonalCropCycleBean(PracticedSystemMetadata.PracticedBean baseBean, PracticedSeasonalEntityExport entityExport) {
        SeasonalCropCycleBean cropCycleBean = new SeasonalCropCycleBean(baseBean);
        cropCycleBean.setLabel(entityExport.getLabel());
        cropCycleBean.setEndCycle(entityExport.getEndCycle());
        cropCycleBean.setSameCampaignAsPreviousNode(entityExport.getSameCampaignAsPreviousNode());
        cropCycleBean.setInitNodeFrequency(entityExport.getInitNodeFrequency());
        cropCycleBean.setPreviousCrops(entityExport.getPreviousCrops());
        cropCycleBean.setCroppingPlanEntryFrequency(entityExport.getCroppingPlanEntryFrequency());
        cropCycleBean.setIntermediateCropName(entityExport.getIntermediateCropName());
        cropCycleBean.setX(entityExport.getX());
        return cropCycleBean;
    }

    protected void exportToolsCouplingsSpeciesActionsInputsFields(List<PracticedSystemMetadata.ItkBean> itkEntities,
                                                                  PracticedInterventionDto itk,
                                                                  PracticedSystemMetadata.PracticedBean itkBaseBean,
                                                                  Map<String, CroppingPlanSpecies> croppingPlanSpeciesByCode,
                                                                  Map<String, String> toolsCouplingNames) {

        PracticedSystemMetadata.ItkBean exportIntervention = new PracticedSystemMetadata.ItkBean(itkBaseBean);

        // psci
        exportIntervention.setSpatialFrequency(itk.getSpatialFrequency());
        exportIntervention.setTemporalFrequency(itk.getTemporalFrequency());
        // toolsCouplings
        exportIntervention.setProgressionSpeed(itk.getProgressionSpeed());
        exportIntervention.setInvolvedPeopleNumber(itk.getInvolvedPeopleNumber());
        exportIntervention.setWorkRate(itk.getWorkRate());
        exportIntervention.setWorkRateUnit(itk.getWorkRateUnit());
        exportIntervention.setActionComment(itk.getComment());
//
//        //XXX 20190722 ymartel : TBH i don't understand why I should put this to have the information in the export, and why the one in #exportCommonInterventionFields don't work
//        exportIntervention.setExtraField("interventionId", getShortId(itk, getPersistenceContext()));
//
        Double psci = itk.getTemporalFrequency() * itk.getSpatialFrequency();
        exportIntervention.setPsci(psci);

        // On force le chargement
        Set<String> tcCodes = new HashSet<>(CollectionUtils.emptyIfNull(itk.getToolsCouplingCodes()));
        // Tool coupling
        if (CollectionUtils.isNotEmpty(tcCodes)) {
            for (String tcCode : tcCodes) {
                PracticedSystemMetadata.ItkBean exportCoupling = new PracticedSystemMetadata.ItkBean(exportIntervention);
                String tcName = toolsCouplingNames.get(tcCode);
                if (tcName == null) {
                    continue;
                }
                exportCoupling.setToolsCouplingName(tcName);
                // add species, actions, inputs fields
                exportSpeciesActionsInputsFields(itkEntities, itk, exportCoupling, croppingPlanSpeciesByCode);
            }

        } else {
            // add species, actions, inputs fields
            exportSpeciesActionsInputsFields(itkEntities, itk, exportIntervention, croppingPlanSpeciesByCode);
        }
    }

    protected void exportSpeciesActionsInputsFields(List<PracticedSystemMetadata.ItkBean> itkEntities,
                                                    PracticedInterventionDto itk,
                                                    PracticedSystemMetadata.ItkBean practicedItkBean,
                                                    Map<String, CroppingPlanSpecies> croppingPlanSpeciesByCode) {
        if (CollectionUtils.isNotEmpty(itk.getSpeciesStadesDtos())) {
            for (SpeciesStadeDto stade : itk.getSpeciesStadesDtos()) {
                // add actions and inputs fields
                Collection<AbstractActionDto> actionDtos = itk.getActionDtos();
                for (AbstractActionDto actionDto : actionDtos) {
                    exportSpeciesStadeFields(stade, practicedItkBean, croppingPlanSpeciesByCode);
                    exportActionFields(actionDto, practicedItkBean);
                    itkEntities.add(practicedItkBean);
                }
            }
        } else {
            // add actions and inputs fields
            Collection<AbstractActionDto> actionDtos = itk.getActionDtos();
            for (AbstractActionDto actionDto : actionDtos) {
                exportActionFields(actionDto, practicedItkBean);
                itkEntities.add(practicedItkBean);
            }
        }
    }

    protected void exportSpeciesStadeFields(SpeciesStadeDto speciesStadeDto,
                                            ExportMetadata.Bean itkBean,
                                            Map<String, CroppingPlanSpecies> croppingPlanSpeciesByCode) {
        String speciesCode = speciesStadeDto.getSpeciesCode();
        CroppingPlanSpecies species = croppingPlanSpeciesByCode.get(speciesCode);
        if (species != null) {
            CroppingPlanEntry croppingPlanEntry = species.getCroppingPlanEntry();
            String speciesAndVarietiesNames = FormatUtils.getCroppingPlanEntrySpecies(croppingPlanEntry);
            itkBean.setSpeciesName(speciesAndVarietiesNames);

            itkBean.setItkSpecies(species.getSpecies().getLibelle_espece_botanique_Translated());
            RefVariete variete = species.getVariety();
            if (variete != null) {
                itkBean.setItkVariety(variete.getLabel());
            } else if (StringUtils.isNotBlank(species.getEdaplosUnknownVariety())) {
                Locale locale = getSecurityContext().getLocale();
                itkBean.setItkVariety(I18n.l(locale, "fr.inra.agrosyst.api.services.edaplos.unknownVariety", species.getEdaplosUnknownVariety()));
            }
        }
        String stadeMin = speciesStadeDto.getStadeMin() != null ? speciesStadeDto.getStadeMin().getLabel() : null;
        itkBean.setStadeMin(stadeMin);
        String stadeMax = speciesStadeDto.getStadeMax() != null ? speciesStadeDto.getStadeMax().getLabel() : null;
        itkBean.setStadeMax(stadeMax);
    }

    protected void exportActionFields(AbstractActionDto action, ExportMetadata.Bean export) {
        export.setActionType(action.getMainActionInterventionAgrosyst());
        export.setAction(action.getMainActionReference_label().orElse(""));
        export.setMainAction(StringUtils.isNotBlank(action.getToolsCouplingCode().orElse("")));
    }

    protected PracticedSystemMetadata.PracticedBean exportCommonInterventionFields(
            PracticedSystemMetadata.PracticedBean baseBean,
            boolean isSeasonal,
            String cropName,
            String previousCropName,
            Integer rank,
            CropCyclePhaseType phaseType,
            PracticedInterventionDto itk) {

        PracticedSystemMetadata.PracticedBean export = new PracticedSystemMetadata.PracticedBean(baseBean);

        String cycle = isSeasonal ? "Assolé" : "Pérenne";
        export.setItkCycle(cycle);

        export.setItkCrop(cropName);

        export.setItkPreviousCrop(previousCropName);

        export.setItkRank(rank == null ? null : rank + 1);

        export.setItkPhase(phaseType);

        export.setInterventionName(itk.getName());
        export.setType(itk.getType());
        export.setIntermediateCrop(itk.isIntermediateCrop());

        export.setInterventionId(getPersistenceContext().getTopiaIdFactory().getRandomPart(itk.getTopiaId()));
        export.setItkStart(itk.getStartingPeriodDate());
        export.setItkEnd(itk.getEndingPeriodDate());

        return export;
    }



    @Override
    protected void fillHarvestingActionSpeciesAndVarieties(ExportMetadata.ItkActionRecolteBean newBean, List<SpeciesStadeDto> speciesStadeDtos, Map<String, ExportMetadata.SpeciesAndVariety> refEspeceByCPSCode, HarvestingActionValorisationDto valorisation) {
        ExportMetadata.SpeciesAndVariety speciesAndVariety = refEspeceByCPSCode.get(valorisation.getSpeciesCode());
        if (speciesAndVariety != null) {
            newBean.setActionSpecies(speciesAndVariety.getSpeciesName());
            newBean.setActionVarieties(speciesAndVariety.getVarietyName());
        }
    }
}
