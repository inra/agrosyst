package fr.inra.agrosyst.services.performance;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.ModalityDephy;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.PerformanceTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.services.common.HarvestingValorisationPriceSummary;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.RefScenariosInputPricesByDomainInput;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.effective.export.EffectiveDbModel;
import fr.inra.agrosyst.services.performance.dbPersistence.Column;
import fr.inra.agrosyst.services.performance.dbPersistence.IndicatorModel;
import fr.inra.agrosyst.services.performance.dbPersistence.TableRowModel;
import fr.inra.agrosyst.services.performance.indicators.EffectiveItkCropCycleScaleKey;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.practiced.export.PracticedSystemDbModel;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Date;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class SqlWriter implements IndicatorWriter {
    
    private static final Log LOGGER = LogFactory.getLog(SqlWriter.class);
    private final String performanceId;
    private final java.sql.Date performanceDate;
    
    protected PerformanceTopiaDao performanceDao;

    // pojo décrivant l'ensemble des colonnes d'un indicateurs à calculer
    final Map<Class<? extends GenericIndicator>, IndicatorModel> indicatorModelsByIndicatorClass = new HashMap<>();
    // représente une ligne en base/csv pour les exports des systèmes synthétisé à l'échelle du système synthétisé
    protected final Map<String, TableRowModel> practicedSystemValues = new HashMap<>();
    // représente une ligne en base/csv pour les exports des réalisés à l'échecle du SDC
    protected final Map<String, TableRowModel> effectiveSdcValues = new HashMap<>();
    // représente une ligne en base/csv pour les exports des systèmes synthétisé à l'échelle du culture
    protected final Map<String, TableRowModel> practicedSystemCropValues = new HashMap<>();
    // représente une ligne en base/csv pour les exports des réalisés à l'échelle culture
    protected final Map<String, TableRowModel> effectiveCropValues = new HashMap<>();
    // représente une ligne en base/csv pour les exports des réalisés à l'échelle parcelle
    protected final Map<String, TableRowModel> effectivePlotValues = new HashMap<>();
    // représente une ligne en base/csv pour les exports des réalisés à l'échelle de la zone
    protected final Map<String, TableRowModel> effectiveZoneValues = new HashMap<>();
    // représente une ligne en base/csv pour les exports des réalisés à l'échelle intervention
    protected final Map<String, TableRowModel> effectiveInterventionValues = new HashMap<>();
    // représente une ligne en base/csv pour les exports des réalisés à l'échelle ITK
    protected final Map<EffectiveItkCropCycleScaleKey, TableRowModel> effectiveItkValues = new HashMap<>();
    // représente une ligne en base/csv pour les exports des synthétisé à l'échelle intervention
    protected final Map<String, TableRowModel> practicedInterventionValues = new HashMap<>();
    // représente une ligne en base/csv pour les exports des réalisés à l'échelle de l'intrant
    protected final Map<String, TableRowModel> inputValues = new HashMap<>();
    
    public void setPerformanceDao(PerformanceTopiaDao performanceDao) {
        this.performanceDao = performanceDao;
    }
    
    public static final Function<Map<Pair<RefDestination, YealdUnit>, Double>, String> GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION = yealdAverages -> {
        List<String> allPrintablePlotYealdAverage = new ArrayList<>();
        if (yealdAverages != null) {
            for (Map.Entry<Pair<RefDestination, YealdUnit>, Double> pairDoubleEntry : yealdAverages.entrySet()) {
                
                final Pair<RefDestination, YealdUnit> destAndUnit = pairDoubleEntry.getKey();
                final String unit = AgrosystI18nService.getEnumTraductionWithDefaultLocale(destAndUnit.getRight());
                final String destination = destAndUnit.getLeft().getDestination();
                final Double yealdAverage = pairDoubleEntry.getValue();
                
                String printableYeald = String.format("[%s]|%f|%s", destination, yealdAverage, unit);
                allPrintablePlotYealdAverage.add(printableYeald);
            }
        }
        return String.join("#", allPrintablePlotYealdAverage);
    };
    
    public SqlWriter(PerformanceTopiaDao performanceDao, String performanceId, OffsetDateTime performanceDate) {
        this.performanceDao = performanceDao;
        this.performanceId = performanceId;
        this.performanceDate = new Date(performanceDate.toEpochSecond());
    }
    
    protected Optional<IndicatorModel> getIndicatorModel(Class<? extends GenericIndicator> indicatorClass) {
        IndicatorModel indicatorModel = indicatorModelsByIndicatorClass.get(indicatorClass);
        
        if (indicatorModelsByIndicatorClass.isEmpty() || indicatorModelsByIndicatorClass.get(indicatorClass) == null) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Pas de model pour l'indicateur '" + indicatorClass.getSimpleName() + "'");
            }
            return Optional.empty();
        }
        return Optional.ofNullable(indicatorModel);
    }
    
    @Override
    public void init() {
    }
    
    @Override
    public void addComputedIndicators(GenericIndicator... indicators) {
        // en fonction des choix de l'utilisateur sont ajouté les indicateurs à calculer
        for (GenericIndicator indicator : indicators) {
            Map<String, String> indicatorNameToDbColumnName = indicator.getIndicatorNameToDbColumnName();
            if (indicatorNameToDbColumnName != null) {
                indicatorModelsByIndicatorClass.put(indicator.getClass(), new IndicatorModel(indicator));
            }
        }
    }

    @Override
    public void finish() {
    }

    @Override
    public void afterAllIndicatorsAreWritten() {
    
        practicedSystemValues.values()
                .forEach(row -> performanceDao.insertIntoTable(row));
        practicedSystemValues.clear();
        
        effectiveSdcValues.values()
                .forEach(row -> performanceDao.insertIntoTable(row));
        effectiveSdcValues.clear();
    
        practicedSystemCropValues.values()
                .forEach(row -> performanceDao.insertIntoTable(row));
        practicedSystemCropValues.clear();
    
        effectiveCropValues.values().
                forEach(row -> performanceDao.insertIntoTable(row));
        effectiveCropValues.clear();
    
        effectivePlotValues.values().
                forEach(row -> performanceDao.insertIntoTable(row));
        effectivePlotValues.clear();
    
        effectiveZoneValues.values().
                forEach(row -> performanceDao.insertIntoTable(row));
        effectiveZoneValues.clear();
    
        effectiveItkValues.values()
                .forEach(row -> performanceDao.insertIntoTable(row));
        effectiveItkValues.clear();
        
        effectiveInterventionValues.values().
                forEach(row -> performanceDao.insertIntoTable(row));
        effectiveInterventionValues.clear();
    
        practicedInterventionValues.values().
                forEach(row -> performanceDao.insertIntoTable(row));
        practicedInterventionValues.clear();
    
        inputValues.values().
                forEach(row -> performanceDao.insertIntoTable(row));
        inputValues.clear();
    }
    
    @Override
    public void writePracticedSeasonalIntervention(
            String its,
            String irs,
            String campaigns,
            String indicatorCategory,
            String indicatorName,
            Object value,
            Integer reliabilityIndex,
            String comment,
            List<String> referenceDosages,
            Domain domain,
            GrowingSystem growingSystem,
            PracticedSystem practicedSystem,
            CroppingPlanEntry croppingPlanEntry,
            int rank,
            CroppingPlanEntry previousPlanEntry,
            CroppingPlanEntry intermediateCrop,
            PracticedIntervention intervention,
            Collection<AbstractAction> actions,
            Collection<String> codeAmmBioControle,
            Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage,
            Map<String, String> groupesCiblesByCode,
            Class<? extends GenericIndicator> indicatorClass) {
        Optional<IndicatorModel> optionalIndicatorModel = getIndicatorModel(indicatorClass);

        if (optionalIndicatorModel.isEmpty()) return;// rien à sauvegarder
        IndicatorModel indicatorModel = optionalIndicatorModel.get();

        Map<String, String> indicatorsNameToColumns = indicatorModel.getIndicatorsNameToColumns();

        CroppingPlanEntry writerCrop;
        if (intervention.isIntermediateCrop()) {
            writerCrop = ObjectUtils.firstNonNull(intermediateCrop, croppingPlanEntry);
        } else {
            writerCrop = croppingPlanEntry;
        }

        TableRowModel row = getPracticedSystemInterventionTableRowModel(
                its,
                irs,
                domain,
                growingSystem,
                practicedSystem,
                writerCrop,
                previousPlanEntry,
                rank,
                null,
                null,
                intervention,
                actions,
                codeAmmBioControle,
                GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(interventionYealdAverage),
                indicatorsNameToColumns,
                groupesCiblesByCode
        );

        final String columnName = indicatorsNameToColumns.get(indicatorName);
        row.addIndicatorValue(columnName, value);

        row.addExtraValues(reliabilityIndex, indicatorModel, comment, String.join(", ", referenceDosages), null);
    }

    @Override
    public void writeInputUsage(
            String its,
            String irs,
            String indicatorCategory,
            String indicatorName,
            AbstractInputUsage usage,
            AbstractAction action,
            EffectiveIntervention effectiveIntervention,
            PracticedIntervention practicedIntervention,
            Collection<String> codeAmmBioControle,
            Domain domain,
            GrowingSystem growingSystem,
            Plot plot,
            Zone zone,
            PracticedSystem practicedSystem,
            CroppingPlanEntry croppingPlanEntry,
            PracticedCropCyclePhase practicedCropCyclePhase,
            Double solOccupationPercent,
            EffectiveCropCyclePhase effectiveCropCyclePhase,
            Integer rank,
            CroppingPlanEntry previousCrop,
            CroppingPlanEntry intermediateCrop,
            Class<? extends GenericIndicator> indicatorClass,
            Object value,
            Integer reliabilityIndexForInputId,
            String reliabilityCommentForInputId,
            String iftRefDoseUserInfos,
            String iftRefDoseValue,
            String iftRefDoseUnit,
            Map<String, String> groupesCiblesByCode) {
        
        Optional<IndicatorModel> optionalIndicatorModel = getIndicatorModel(indicatorClass);
    
        if (optionalIndicatorModel.isEmpty()) return;// rien à sauvegarder
    
        String inputTopiaId = null;
        if (usage == null && (action instanceof IrrigationAction)) {
            // consider irrig or seeding action as input
            inputTopiaId = action.getTopiaId();
        } else if (usage == null && (action instanceof SeedingActionUsage)) {
            inputTopiaId = action.getTopiaId();
        } else if(usage != null) {
            inputTopiaId = usage.getTopiaId();
        }
    
        if (StringUtils.isEmpty(inputTopiaId)) return;// rien à sauvegarder
    
        IndicatorModel indicatorModel = optionalIndicatorModel.get();
    
        Map<String, String> indicatorsNameToColumns = indicatorModel.getIndicatorsNameToColumns();
    
        TableRowModel row = inputValues.get(inputTopiaId);
        if (row == null) {
        
            row = new InputRowModel(
                    inputTopiaId,
                    performanceId,
                    performanceDate,
                    its,
                    irs,
                    usage,
                    action,
                    effectiveIntervention,
                    practicedIntervention,
                    codeAmmBioControle,
                    groupesCiblesByCode,
                    domain,
                    growingSystem,
                    plot,
                    zone,
                    practicedSystem,
                    croppingPlanEntry,
                    practicedCropCyclePhase,
                    solOccupationPercent,
                    effectiveCropCyclePhase,
                    rank,
                    previousCrop,
                    intermediateCrop);
        
            inputValues.put(inputTopiaId, row);
        }
    
        row.addIndicatorsColumns(indicatorsNameToColumns);
    
        final String columnName = indicatorsNameToColumns.get(indicatorName);
        row.addIndicatorValue(columnName, value);
    
        row.addExtraValues(reliabilityIndexForInputId, indicatorModel, reliabilityCommentForInputId, iftRefDoseValue, iftRefDoseUnit);
    }
    
    public static class InputRowModel extends TableRowModel implements DataFormatter {
    
        public InputRowModel(String inputOrIrrigActionId,
                             String performanceId,
                             Date performanceDate,
                             String its,
                             String irs,
                             AbstractInputUsage usage,
                             AbstractAction action,
                             EffectiveIntervention effectiveIntervention,
                             PracticedIntervention practicedIntervention,
                             Collection<String> codeAmmBioControle,
                             Map<String, String> groupesCiblesParCode,
                             Domain domain,
                             GrowingSystem growingSystem,
                             Plot plot,
                             Zone zone,
                             PracticedSystem practicedSystem,
                             CroppingPlanEntry croppingPlanEntry,
                             PracticedCropCyclePhase practicedCropCyclePhase,
                             Double solOccupationPercent,
                             EffectiveCropCyclePhase effectiveCropCyclePhase,
                             Integer rank,
                             CroppingPlanEntry previousCrop,
                             CroppingPlanEntry intermediateCrop0) {

            super(inputOrIrrigActionId, "echelle_intrant");
        
            String actionName = getActionToString(action);

            CroppingPlanEntry intermediateCrop = null;
            if ((practicedIntervention != null && practicedIntervention.isIntermediateCrop()) ||
                    (effectiveIntervention != null && effectiveIntervention.isIntermediateCrop())) {
                intermediateCrop = intermediateCrop0;
            }
        
            String inputProductName = getInputUsageProductName(usage, action, intermediateCrop, croppingPlanEntry, false);
            Pair<String, String> inputUsageQtAvgAndUnit = getInputUsageQtAvgAndUnit(usage);

            String groupeCible = null;
            String target = null;

            // Là-dedans : revoir le calcul du biocontrôle pour les irrigations
            Boolean biocontrole = null;
            if (usage != null) {
                groupeCible = getGroupesCiblesToString(usage, groupesCiblesParCode);
                target = getTargetsToString(usage);
                final Optional<Boolean> maybeInputUsageBiocontrole = isInputUsageBiocontrole(usage, codeAmmBioControle);
                biocontrole = maybeInputUsageBiocontrole.orElse(null);
            }

            Pair<String, String> speciesAndVarietyNames = getSpeciesAndVarietyName(usage, action, croppingPlanEntry);
            String speciesNames = speciesAndVarietyNames.getLeft();
            String varietyNames = speciesAndVarietyNames.getRight();

            String interventionName;
            String interventionId;
            String startingDate;
            String endingDate;
            String interventionSpeciesNames;
            String interventionVarietyNames;
            String approche_de_calcul;
        
            if (effectiveIntervention != null) {
                interventionName = effectiveIntervention.getName();
                interventionId = effectiveIntervention.getTopiaId();
                startingDate = INTERVENTION_DATE_FORMAT_DDMMYYYY.format(effectiveIntervention.getStartInterventionDate());
                endingDate = INTERVENTION_DATE_FORMAT_DDMMYYYY.format(effectiveIntervention.getEndInterventionDate());
                interventionSpeciesNames = getInterventionSpeciesName(effectiveIntervention);
                interventionVarietyNames = getInterventionVarietyName(effectiveIntervention);
                approche_de_calcul = "réalisé";
            } else {
                assert practicedIntervention != null;
                interventionName = practicedIntervention.getName();
                interventionId = practicedIntervention.getTopiaId();
                startingDate = practicedIntervention.getStartingPeriodDate();
                endingDate = practicedIntervention.getEndingPeriodDate();
                interventionSpeciesNames = getInterventionSpeciesName(croppingPlanEntry, practicedIntervention);
                interventionVarietyNames = getInterventionVarietyName(croppingPlanEntry, practicedIntervention);
                approche_de_calcul = "synthétisé";
            }

            if (usage != null && StringUtils.isNotBlank(usage.getDeprecatedId())) {
                addColumn(Column.createCommonTableColumn("deprecatedqtavg", usage.getDeprecatedQtAvg(), Double.class));
                addColumn(Column.createCommonTableColumn("deprecatedunit", usage.getDeprecatedUnit(), String.class));
                addColumn(Column.createCommonTableColumn("deprecatedid", usage.getDeprecatedId(), String.class));
            }

            addColumn(Column.createCommonTableColumn("intervention", interventionName, String.class));
            addColumn(Column.createCommonTableColumn("intervention_id", interventionId, String.class));
            addColumn(Column.createCommonTableColumn("debut_intervention", startingDate, String.class));
            addColumn(Column.createCommonTableColumn("fin_intervention", endingDate, String.class));
            addColumn(Column.createCommonTableColumn("especes_concernees_par_intervention", interventionSpeciesNames, String.class));
            addColumn(Column.createCommonTableColumn("varietes_concernees_par_intervention", interventionVarietyNames, String.class));
            addColumn(Column.createCommonTableColumn("approche_de_calcul", approche_de_calcul, String.class));
        
            if (practicedSystem != null) {
                addColumn(Column.createCommonTableColumn("nom_systeme_synthetise", practicedSystem.getName(), String.class));
                addColumn(Column.createCommonTableColumn("id_systeme_synthetise", practicedSystem.getTopiaId(), String.class));
                addColumn(Column.createCommonTableColumn("synthetise_valide", practicedSystem.isValidated(), Boolean.class));
                addColumn(Column.createCommonTableColumn("synthetise_campagnes", practicedSystem.getCampaigns(), String.class));
            }
        
            if (growingSystem != null) {
                addColumn(Column.createCommonTableColumn("id_dispositif", growingSystem.getGrowingPlan().getTopiaId(), String.class));
                addColumn(Column.createCommonTableColumn("type_dispositif", growingSystem.getGrowingPlan().getType(), TypeDEPHY.class));
                addColumn(Column.createCommonTableColumn("filiere_sdc", growingSystem.getSector(), Sector.class));
                addColumn(Column.createCommonTableColumn("nom_sdc", growingSystem.getName(), String.class));
                addColumn(Column.createCommonTableColumn("id_sdc", growingSystem.getTopiaId(), String.class));
                addColumn(Column.createCommonTableColumn("num_dephy", growingSystem.getDephyNumber(), String.class));
                addColumn(Column.createCommonTableColumn("type_conduite_sdc", growingSystem.getTypeAgriculture() == null ? "" : growingSystem.getTypeAgriculture().getReference_label(), String.class));
                addColumn(Column.createCommonTableColumn("sdc_valide", growingSystem.isValidated(), Boolean.class));
                addColumn(Column.createCommonTableColumn("modalite_suivi_dephy", growingSystem.getModality(), ModalityDephy.class));
            }
        
            if (plot != null) {
                addColumn(Column.createCommonTableColumn("parcelle", plot.getName(), String.class));
                addColumn(Column.createCommonTableColumn("parcelle_id", plot.getTopiaId(), String.class));
            }
        
            if (zone != null) {
                addColumn(Column.createCommonTableColumn("zone", zone.getName(), String.class));
                addColumn(Column.createCommonTableColumn("zone_id", zone.getTopiaId(), String.class));
            }
        
            if (effectiveCropCyclePhase != null) {
                addColumn(Column.createCommonTableColumn("phase", effectiveCropCyclePhase.getType(), CropCyclePhaseType.class));
                addColumn(Column.createCommonTableColumn("phase_id", effectiveCropCyclePhase.getTopiaId(), String.class));
            }
        
            if (practicedCropCyclePhase != null) {
                addColumn(Column.createCommonTableColumn("phase", practicedCropCyclePhase.getType(), CropCyclePhaseType.class));
                addColumn(Column.createCommonTableColumn("phase_id", practicedCropCyclePhase.getTopiaId(), String.class));
            }
        
            if (previousCrop != null) {
                addColumn(Column.createCommonTableColumn("culture_precedente", previousCrop.getName(), String.class));
                addColumn(Column.createCommonTableColumn("culture_precedente_id", previousCrop.getTopiaId(), String.class));
                addColumn(Column.createCommonTableColumn("culture_precedente_code", previousCrop.getCode(), String.class));
            }
        
            if (practicedIntervention != null) {
                addColumn(Column.createCommonTableColumn("pourcentage_culture_perennes", solOccupationPercent, Double.class));
            }
        
            if (croppingPlanEntry != null) {
                addColumn(Column.createCommonTableColumn("culture", croppingPlanEntry.getName(), String.class));
                addColumn(Column.createCommonTableColumn("culture_id", croppingPlanEntry.getTopiaId(), String.class));
                addColumn(Column.createCommonTableColumn("culture_code", croppingPlanEntry.getCode(), String.class));
            }
        
            addColumn(Column.createCommonTableColumn("performance_id", performanceId, String.class));
            addColumn(Column.createCommonTableColumn("date_calcul", performanceDate, Date.class));
            addColumn(Column.createCommonTableColumn("nom_domaine_exploitation", domain.getName(), String.class));
            addColumn(Column.createCommonTableColumn("id_domaine", domain.getTopiaId(), String.class));
            addColumn(Column.createCommonTableColumn("domaine_campagne", domain.getCampaign(), Integer.class));
            addColumn(Column.createCommonTableColumn("domaine_type", domain.getType(), DomainType.class));
            addColumn(Column.createCommonTableColumn("departement", domain.getLocation().getDepartement(), String.class));
            addColumn(Column.createCommonTableColumn("nom_reseau_it", its, String.class));
            addColumn(Column.createCommonTableColumn("nom_reseau_ir", irs, String.class));
            addColumn(Column.createCommonTableColumn("rang", rank, Integer.class));
            addColumn(Column.createCommonTableColumn("especes", speciesNames, String.class));
            addColumn(Column.createCommonTableColumn("varietes", varietyNames, String.class));
            addColumn(Column.createCommonTableColumn("id_action", action != null ? action.getTopiaId() : "", String.class));
            addColumn(Column.createCommonTableColumn("action", actionName, String.class));
            addColumn(Column.createCommonTableColumn("id_usage", usage != null ? usage.getTopiaId() : "", String.class));
            addColumn(Column.createCommonTableColumn("intrant_nom", inputProductName, String.class));
            addColumn(Column.createCommonTableColumn("dose_appliquee", inputUsageQtAvgAndUnit.getLeft(), String.class));
            addColumn(Column.createCommonTableColumn("dose_appliquee_unite", inputUsageQtAvgAndUnit.getRight(), String.class));
            addColumn(Column.createCommonTableColumn("groupe_cible", groupeCible, String.class));
            addColumn(Column.createCommonTableColumn("cibles_traitement", target, String.class));
            addColumn(Column.createCommonTableColumn("biocontrole", biocontrole, Boolean.class));
        }
    }
    
    @Override
    public void writePracticedPerennialIntervention(
            String its,
            String irs,
            String campaigns,
            String indicatorCategory,
            String indicatorName,
            Object value,
            Integer reliabilityIndex,
            String comment,
            List<String> referenceDosages,
            Domain domain,
            GrowingSystem growingSystem,
            PracticedSystem practicedSystem,
            CroppingPlanEntry croppingPlanEntry,
            PracticedCropCyclePhase phase,
            Double perennialCropCyclePercent,
            PracticedIntervention intervention,
            Collection<AbstractAction> actions,
            Collection<String> codeAmmBioControle,
            Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage,
            Map<String, String> groupesCiblesByCode,
            Class<? extends GenericIndicator> indicatorClass) {
        Optional<IndicatorModel> optionalIndicatorModel = getIndicatorModel(indicatorClass);

        if (optionalIndicatorModel.isEmpty()) return;// rien à sauvegarder
        IndicatorModel indicatorModel = optionalIndicatorModel.get();

        Map<String, String> indicatorsNameToColumns = indicatorModel.getIndicatorsNameToColumns();

        TableRowModel row = getPracticedSystemInterventionTableRowModel(
                its,
                irs,
                domain,
                growingSystem,
                practicedSystem,
                croppingPlanEntry,
                null,
                null,
                phase,
                perennialCropCyclePercent,
                intervention,
                actions,
                codeAmmBioControle,
                GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(interventionYealdAverage),
                indicatorsNameToColumns,
                groupesCiblesByCode
        );

        final String columnName = indicatorsNameToColumns.get(indicatorName);
        row.addIndicatorValue(columnName, value);

        row.addExtraValues(reliabilityIndex, indicatorModel, comment, String.join(", ", referenceDosages), null);
    }
    
    @Override
    public void writePracticedSeasonalCrop(String its,
                                           String irs,
                                           String campaigns,
                                           Double frequency,
                                           String indicatorCategory,
                                           String indicatorName,
                                           Object value,
                                           Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                           Integer reliabilityIndex,
                                           String comment,
                                           Integer rank,
                                           Domain domain,
                                           GrowingSystem growingSystem,
                                           PracticedSystem practicedSystem,
                                           CroppingPlanEntry croppingPlanEntry,
                                           CroppingPlanEntry previousPlanEntry,
                                           String culturePrecedentRangId,
                                           Class<? extends GenericIndicator> indicatorClass,
                                           PracticedCropCycleConnection practicedCropCycleConnection) {
    
        Optional<IndicatorModel> optionalIndicatorModel = getIndicatorModel(indicatorClass);
    
        if (optionalIndicatorModel.isEmpty() || croppingPlanEntry == null) return;// rien à sauvegarder
    
        IndicatorModel indicatorModel = optionalIndicatorModel.get();
        Map<String, String> indicatorsNameToColumns = indicatorModel.getIndicatorsNameToColumns();
    
        TableRowModel row = getPracticedSystemCropPreviousCropTableRowModel(
                croppingPlanEntry,
                rank + 1,
                previousPlanEntry,
                domain,
                growingSystem,
                its,
                irs,
                practicedSystem,
                null,
                null,
                GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(cropYealdAverage),
                culturePrecedentRangId,
                indicatorsNameToColumns,
                practicedCropCycleConnection);
    
        final String columnName = indicatorsNameToColumns.get(indicatorName);
        row.addIndicatorValue(columnName, value);
    
        row.addExtraValues(reliabilityIndex, indicatorModel, comment, null, null);
    }
    
    @Override
    public void writePracticedPerennialCop(String its,
                                           String irs,
                                           String campaigns,
                                           String indicatorCategory,
                                           String indicatorName,
                                           Object value,
                                           Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                           Integer reliabilityIndex,
                                           String comment,
                                           Domain domain,
                                           GrowingSystem growingSystem,
                                           PracticedSystem practicedSystem,
                                           CroppingPlanEntry croppingPlanEntry,
                                           PracticedCropCyclePhase phase,
                                           Double perennialCropCyclePercent,
                                           Class<? extends GenericIndicator> indicatorClass) {
    
        Optional<IndicatorModel> optionalIndicatorModel = getIndicatorModel(indicatorClass);
    
        if (optionalIndicatorModel.isEmpty() || croppingPlanEntry == null) return;// rien à sauvegarder
    
        IndicatorModel indicatorModel = optionalIndicatorModel.get();
    
        Map<String, String> indicatorsNameToColumns = indicatorModel.getIndicatorsNameToColumns();
    
        TableRowModel row = getPracticedSystemCropPreviousCropTableRowModel(
                croppingPlanEntry,
                null,
                null,
                domain,
                growingSystem,
                its,
                irs,
                practicedSystem,
                phase,
                perennialCropCyclePercent,
                GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(cropYealdAverage),
                null,
                indicatorsNameToColumns,
                null);
    
        final String columnName = indicatorsNameToColumns.get(indicatorName);
        row.addIndicatorValue(columnName, value);
    
        row.addExtraValues(reliabilityIndex, indicatorModel, comment, null, null);
    }
    
    @Override
    public void writePracticedSystem(String its,
                                     String irs,
                                     String campaigns,
                                     String indicatorCategory,
                                     String indicatorName,
                                     Object value,
                                     Integer reliabilityIndex,
                                     String comment,
                                     Domain domain,
                                     GrowingSystem growingSystem,
                                     PracticedSystem practicedSystem,
                                     Class<? extends GenericIndicator> indicatorClass) {
    
        Optional<IndicatorModel> optionalIndicatorModel = getIndicatorModel(indicatorClass);
    
        if (optionalIndicatorModel.isEmpty()) return;// rien à sauvegarder
        IndicatorModel indicatorModel = optionalIndicatorModel.get();
    
        Map<String, String> indicatorsNameToColumns = indicatorModel.getIndicatorsNameToColumns();
    
        TableRowModel row = getPracticedSystemTableRowModel(practicedSystem, domain, growingSystem, its, irs, indicatorsNameToColumns);
    
        final String columnName = indicatorsNameToColumns.get(indicatorName);
        row.addIndicatorValue(columnName, value);
    
        row.addExtraValues(reliabilityIndex, indicatorModel, comment, null, null);
    }
    
    @Override
    public void writePracticedDomain(String campaigns,
                                     String indicatorCategory,
                                     String indicatorName,
                                     Object value,
                                     Integer reliabilityIndex,
                                     String comment,
                                     Domain domain,
                                     String growingSystemsTypeAgricultureLabels) {
        // NOTHING TO DO
    }

    @Override
    public void writePrices(
            RefCampaignsInputPricesByDomainInput domainInputPriceRefPrices,
            List<HarvestingPrice> harvestingPrices,
            Map<String, HarvestingValorisationPriceSummary> refHarvestingPrices,
            List<Equipment> equipments,
            Domain domain,
            PracticedSystem practicedSystem, RefScenariosInputPricesByDomainInput refScenariosInputPricesByDomainInput,
            MultiKeyMap<String, List<RefHarvestingPrice>> refHarvestingPricesByCodeEspeceAndCodeDestination,
            List<CroppingPlanSpecies> croppingPlanSpecies) {
        // NOTHING TO DO
    }
    
    @Override
    public void writeEffectiveSeasonalIntervention(
            String its,
            String irs,
            int campaign,
            String indicatorCategory,
            String indicatorName,
            Object value,
            Integer reliabilityIndex,
            String reliabilityComment,
            List<String> referenceDosages,
            Domain domain,
            Optional<GrowingSystem> optionalGrowingSystem,
            Plot plot,
            Zone zone,
            CroppingPlanEntry croppingPlanEntry,
            CroppingPlanEntry intermediateCrop,
            int rank,
            CroppingPlanEntry previousPlanEntry,
            EffectiveIntervention intervention,
            Collection<AbstractAction> actions,
            Collection<String> codeAmmBioControle,
            Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverages,
            Map<String, String> groupesCiblesByCode,
            Class<? extends GenericIndicator> indicatorClass) {
        Optional<IndicatorModel> optionalIndicatorModel = getIndicatorModel(indicatorClass);

        if (optionalIndicatorModel.isEmpty()) return;// rien à sauvegarder
        IndicatorModel indicatorModel = optionalIndicatorModel.get();

        Map<String, String> indicatorsNameToColumns = indicatorModel.getIndicatorsNameToColumns();

        TableRowModel row = getEffectiveInterventionTableRowModel(
                its,
                irs,
                domain,
                optionalGrowingSystem,
                plot,
                zone,
                croppingPlanEntry,
                rank + 1,
                previousPlanEntry,
                null,
                intervention,
                actions,
                codeAmmBioControle,
                GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(interventionYealdAverages),
                indicatorsNameToColumns,
                groupesCiblesByCode
        );

        final String columnName = indicatorsNameToColumns.get(indicatorName);
        row.addIndicatorValue(columnName, value);

        row.addExtraValues(reliabilityIndex, indicatorModel, reliabilityComment, String.join(", ",referenceDosages), null);
    }
    
    @Override
    public void writeEffectivePerennialIntervention(
            String its,
            String irs,
            String indicatorCategory,
            String indicatorName,
            Object value,
            Integer reliabilityIndex,
            String comment,
            List<String> referenceDosages,
            Domain domain,
            Optional<GrowingSystem> optionalGrowingSystem,
            Plot plot,
            Zone zone,
            CroppingPlanEntry croppingPlanEntry,
            EffectiveCropCyclePhase phase,
            EffectiveIntervention intervention,
            Collection<AbstractAction> actions,
            Collection<String> codeAmmBioControle,
            Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage,
            Map<String, String> groupesCiblesByCode,
            Class<? extends GenericIndicator> indicatorClass) {
        Optional<IndicatorModel> optionalIndicatorModel = getIndicatorModel(indicatorClass);

        if (optionalIndicatorModel.isEmpty()) return;// rien à sauvegarder
        IndicatorModel indicatorModel = optionalIndicatorModel.get();

        Map<String, String> indicatorsNameToColumns = indicatorModel.getIndicatorsNameToColumns();

        TableRowModel row = getEffectiveInterventionTableRowModel(
                its,
                irs,
                domain,
                optionalGrowingSystem,
                plot,
                zone,
                croppingPlanEntry,
                null,
                null,
                phase,
                intervention,
                actions,
                codeAmmBioControle,
                GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(interventionYealdAverage),
                indicatorsNameToColumns,
                groupesCiblesByCode
        );

        final String columnName = indicatorsNameToColumns.get(indicatorName);
        row.addIndicatorValue(columnName, value);

        row.addExtraValues(reliabilityIndex, indicatorModel, comment, String.join(", ",referenceDosages), null);
    }
    
    @Override
    public void writeEffectiveSeasonalCrop(String its,
                                           String irs,
                                           int campaign,
                                           String indicatorCategory,
                                           String indicatorName,
                                           Object value,
                                           Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                           Integer reliabilityIndex,
                                           String comment,
                                           Domain domain,
                                           Optional<GrowingSystem> optionalGrowingSystem,
                                           Plot plot,
                                           CroppingPlanEntry croppingPlanEntry,
                                           Integer rank,
                                           CroppingPlanEntry previousPlanEntry,
                                           Class<? extends GenericIndicator> indicatorClass) {
        
        Optional<IndicatorModel> optionalIndicatorModel = getIndicatorModel(indicatorClass);
    
        if (optionalIndicatorModel.isEmpty() || croppingPlanEntry == null) return;// rien à sauvegarder
        IndicatorModel indicatorModel = optionalIndicatorModel.get();
        
        Map<String, String> indicatorsNameToColumns = indicatorModel.getIndicatorsNameToColumns();

        TableRowModel row = getEffectiveCropTableRowModel(
                its,
                irs,
                domain,
                optionalGrowingSystem,
                croppingPlanEntry,
                rank + 1,
                previousPlanEntry,
                null,
                indicatorsNameToColumns,
                GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(cropYealdAverage)
        );
    
        final String columnName = indicatorsNameToColumns.get(indicatorName);
        row.addIndicatorValue(columnName, value);
    
        row.addExtraValues(reliabilityIndex, indicatorModel, comment, null, null);
    }
    
    @Override
    public void writeEffectivePerennialCrop(String its,
                                            String irs,
                                            int campaign,
                                            String indicatorCategory,
                                            String indicatorName,
                                            Object value,
                                            Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverages,
                                            Integer reliabilityIndex,
                                            String comment,
                                            Domain domain,
                                            Optional<GrowingSystem> optionalGrowingSystem,
                                            Plot plot,
                                            CroppingPlanEntry croppingPlanEntry,
                                            EffectiveCropCyclePhase phase,
                                            Class<? extends GenericIndicator> indicatorClass) {
        
        Optional<IndicatorModel> optionalIndicatorModel = getIndicatorModel(indicatorClass);
    
        if (optionalIndicatorModel.isEmpty() || croppingPlanEntry == null) return;// rien à sauvegarder
        IndicatorModel indicatorModel = optionalIndicatorModel.get();
        
        Map<String, String> indicatorsNameToColumns = indicatorModel.getIndicatorsNameToColumns();
        
        TableRowModel row = getEffectiveCropTableRowModel(
                its,
                irs,
                domain,
                optionalGrowingSystem,
                croppingPlanEntry,
                null,
                null,
                phase,
                indicatorsNameToColumns,
                GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(cropYealdAverages)
        );
    
        final String columnName = indicatorsNameToColumns.get(indicatorName);
        row.addIndicatorValue(columnName, value);
    
        row.addExtraValues(reliabilityIndex, indicatorModel, comment, null, null);
    }
    
    @Override
    public void writeEffectiveZone(String its,
                                   String irs,
                                   String indicatorCategory,
                                   String indicatorName,
                                   Object value,
                                   Map<Pair<RefDestination, YealdUnit>, Double> zoneAverageYeald,
                                   Integer reliabilityIndex,
                                   String comment,
                                   Domain anonymiseDomain,
                                   List<CroppingPlanSpecies> domainCroppingPlanSpecies,
                                   Optional<GrowingSystem> optionalGrowingSystem,
                                   Plot plot,
                                   Zone zone,
                                   String speciesNames,
                                   String varietyNames,
                                   Class<? extends GenericIndicator> indicatorClass) {
        
        Optional<IndicatorModel> optionalIndicatorModel = getIndicatorModel(indicatorClass);
        
        if (optionalIndicatorModel.isEmpty()) return;// rien à sauvegarder
        IndicatorModel indicatorModel = optionalIndicatorModel.get();
        
        Map<String, String> indicatorsNameToColumns = indicatorModel.getIndicatorsNameToColumns();
        
        final String zoneId = zone.getTopiaId();
        
        TableRowModel row = effectiveZoneValues.get(zoneId);
        if (row == null) {
            row = new EffectiveDbModel.ZoneRowModel(
                    performanceDate,
                    performanceId,
                    zone,
                    plot,
                    optionalGrowingSystem,
                    its,
                    irs,
                    GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(zoneAverageYeald),
                    domainCroppingPlanSpecies,
                    anonymiseDomain);
            effectiveZoneValues.put(zoneId, row);
        }
        
        row.addIndicatorsColumns(indicatorsNameToColumns);
        
        final String columnName = indicatorsNameToColumns.get(indicatorName);
        row.addIndicatorValue(columnName, value);
        
        row.addExtraValues(reliabilityIndex, indicatorModel, comment, null, null);
    }
    
    @Override
    public void writeEffectivePlot(String its,
                                   String irs,
                                   String indicatorCategory,
                                   String indicatorName,
                                   Object indicateurValue,
                                   Integer reliabilityIndex,
                                   String comment,
                                   Domain anonymiseDomain,
                                   Collection<CroppingPlanSpecies> domainCroppingPlanSpecies,
                                   Optional<GrowingSystem> optionalGrowingSystem,
                                   Plot plot,
                                   Map<Pair<RefDestination, YealdUnit>, Double> plotYealdAverage,
                                   Class<? extends GenericIndicator> indicatorClass,
                                   String zoneIds) {
        
        Optional<IndicatorModel> optionalIndicatorModel = getIndicatorModel(indicatorClass);
        
        if (optionalIndicatorModel.isEmpty()) return;// rien à sauvegarder
        IndicatorModel indicatorModel = optionalIndicatorModel.get();
        
        Map<String, String> indicatorsNameToColumns = indicatorModel.getIndicatorsNameToColumns();
    
        final String plotId = plot.getTopiaId();
    
        TableRowModel row = effectivePlotValues.get(plotId);
        if (row == null) {
            row = new EffectiveDbModel.PlotRowModel(
                    performanceId,
                    performanceDate,
                    plot,
                    anonymiseDomain,
                    optionalGrowingSystem,
                    its,
                    irs,
                    GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(plotYealdAverage),
                    domainCroppingPlanSpecies,
                    zoneIds);
            effectivePlotValues.put(plotId, row);
        }
    
        row.addIndicatorsColumns(indicatorsNameToColumns);
    
        final String columnName = indicatorsNameToColumns.get(indicatorName);
        row.addIndicatorValue(columnName, indicateurValue);
    
        row.addExtraValues(reliabilityIndex, indicatorModel, comment, null, null);
    }
    
    @Override
    public void writeEffectiveGrowingSystem(String its,
                                            String irs,
                                            String indicatorCategory,
                                            String indicatorName,
                                            Object indicateurValue,
                                            Integer reliabilityIndex,
                                            String comment,
                                            Domain domain,
                                            Optional<GrowingSystem> optionalGrowingSystem,
                                            Class<? extends GenericIndicator> indicatorClass,
                                            Collection<CroppingPlanSpecies> agrosystSpecies) {
    
        Optional<IndicatorModel> optionalIndicatorModel = getIndicatorModel(indicatorClass);
    
        if (optionalIndicatorModel.isEmpty()) return;// rien à sauvegarder
        IndicatorModel indicatorModel = optionalIndicatorModel.get();
    
        Map<String, String> indicatorsNameToColumns = indicatorModel.getIndicatorsNameToColumns();
    
        TableRowModel row = getEffectiveSdcTableRowModel(its, irs, domain, optionalGrowingSystem, agrosystSpecies, indicatorsNameToColumns);
    
        final String columnName = indicatorsNameToColumns.get(indicatorName);
        row.addIndicatorValue(columnName, indicateurValue);
    
        row.addExtraValues(reliabilityIndex, indicatorModel, comment, null, null);
    }
    
    @Override
    public void writeEffectiveDomain(String indicatorCategory,
                                     String indicatorName,
                                     Object value,
                                     Integer reliabilityIndex,
                                     String comment,
                                     Domain domain,
                                     String growingSystemsTypeAgricultureLabels) {
        // NOTHING TO DO
    }

    protected TableRowModel getPracticedSystemInterventionTableRowModel(String its,
                                                                        String irs,
                                                                        Domain domain,
                                                                        GrowingSystem growingSystem,
                                                                        PracticedSystem practicedSystem,
                                                                        CroppingPlanEntry crop,
                                                                        CroppingPlanEntry previousCrop,
                                                                        Integer rank,
                                                                        PracticedCropCyclePhase phase,
                                                                        Double perennialCropCyclePercent,
                                                                        PracticedIntervention intervention,
                                                                        Collection<AbstractAction> actions,
                                                                        Collection<String> codeAmmBioControle,
                                                                        String interventionYealdAverages,
                                                                        Map<String, String> indicatorsNameToColumns,
                                                                        Map<String, String> groupesCiblesParCode) {
        String interventionId = intervention.getTopiaId();
        TableRowModel row = practicedInterventionValues.get(interventionId);
        if (row == null) {

            row = new PracticedSystemDbModel.InterventionRowModel(
                    performanceId,
                    performanceDate,
                    intervention,
                    domain,
                    growingSystem,
                    its,
                    irs,
                    practicedSystem,
                    crop,
                    rank,
                    previousCrop,
                    phase,
                    perennialCropCyclePercent,
                    actions,
                    codeAmmBioControle,
                    groupesCiblesParCode,
                    interventionYealdAverages
            );
            practicedInterventionValues.put(interventionId, row);
        }

        row.addIndicatorsColumns(indicatorsNameToColumns);

        return row;
    }

    protected TableRowModel getPracticedSystemCropPreviousCropTableRowModel(CroppingPlanEntry crop,
                                                                            Integer rank,
                                                                            CroppingPlanEntry previousCrop,
                                                                            Domain domain,
                                                                            GrowingSystem growingSystem,
                                                                            String its, String irs,
                                                                            PracticedSystem practicedSystem,
                                                                            PracticedCropCyclePhase phase, Double perennialCropCyclePercent,
                                                                            String printableYealdAverage,
                                                                            String culturePrecedentRangId,
                                                                            Map<String, String> indicatorsNameToColumns,
                                                                            PracticedCropCycleConnection practicedCropCycleConnection) {
        final String rowId = getCropRowId(crop, rank, previousCrop, practicedSystem);

        TableRowModel row = practicedSystemCropValues.get(rowId);
        if (row == null) {

            row = new PracticedSystemDbModel.CropRowModel(
                    performanceId,
                    performanceDate,
                    rowId,
                    domain,
                    growingSystem,
                    its,
                    irs,
                    practicedSystem,
                    crop,
                    rank,
                    previousCrop,
                    phase,
                    perennialCropCyclePercent,
                    printableYealdAverage,
                    culturePrecedentRangId,
                    practicedCropCycleConnection);
            practicedSystemCropValues.put(rowId, row);
        }
        
        row.addIndicatorsColumns(indicatorsNameToColumns);
        
        return row;
    }
    
    protected TableRowModel getPracticedSystemTableRowModel(PracticedSystem practicedSystem,
                                                            Domain domain,
                                                            GrowingSystem growingSystem,
                                                            String its, String irs,
                                                            Map<String, String> indicatorsNameToColumns) {
        String practicedSystemId = practicedSystem.getTopiaId();
        
        TableRowModel row = practicedSystemValues.get(practicedSystemId);
        if (row == null) {
            row = new PracticedSystemDbModel.PracticedSystemRowModel(performanceId, performanceDate, practicedSystem, domain, growingSystem, its, irs);
            practicedSystemValues.put(practicedSystemId, row);
        }
        
        row.addIndicatorsColumns(indicatorsNameToColumns);
        
        return row;
    }

    protected TableRowModel getEffectiveInterventionTableRowModel(String its, String irs,
                                                                  Domain domain,
                                                                  Optional<GrowingSystem> optionalGrowingSystem,
                                                                  Plot plot,
                                                                  Zone zone,
                                                                  CroppingPlanEntry crop,
                                                                  Integer rank,
                                                                  CroppingPlanEntry previousCrop,
                                                                  EffectiveCropCyclePhase phase,
                                                                  EffectiveIntervention intervention,
                                                                  Collection<AbstractAction> actions,
                                                                  Collection<String> codeAmmBioControle,
                                                                  String interventionYealdAverages,
                                                                  Map<String, String> indicatorsNameToColumns,
                                                                  Map<String, String> groupesCiblesByCode) {

        final String interventionId = intervention.getTopiaId();

        TableRowModel row = effectiveInterventionValues.get(interventionId);
        if (row == null) {

            row = new EffectiveDbModel.InterventionRowModel(
                    performanceId,
                    performanceDate,
                    intervention,
                    actions,
                    codeAmmBioControle,
                    domain,
                    optionalGrowingSystem,
                    its,
                    irs,
                    plot,
                    zone,
                    crop,
                    rank,
                    previousCrop,
                    phase,
                    interventionYealdAverages,
                    groupesCiblesByCode
            );
            effectiveInterventionValues.put(interventionId, row);
        }

        row.addIndicatorsColumns(indicatorsNameToColumns);

        return row;
    }

    protected TableRowModel getEffectiveCropTableRowModel(String its, String irs,
                                                          Domain domain,
                                                          Optional<GrowingSystem> optionalGrowingSystem,
                                                          CroppingPlanEntry crop,
                                                          Integer rank,
                                                          CroppingPlanEntry previousCrop,
                                                          EffectiveCropCyclePhase phase,
                                                          Map<String, String> indicatorsNameToColumns,
                                                          String printableYealdAverage) {
    
        final String rowId = getCropRowId(crop, rank, previousCrop, null);
    
        TableRowModel row = effectiveCropValues.get(rowId);
        if (row == null) {
    
            row = new EffectiveDbModel.CropRowModel(performanceId,
                    performanceDate,
                    crop,
                    rank,
                    previousCrop,
                    domain,
                    optionalGrowingSystem,
                    its,
                    irs,
                    phase,
                    printableYealdAverage);
            effectiveCropValues.put(rowId, row);
        }
    
        row.addIndicatorsColumns(indicatorsNameToColumns);
    
        return row;
    }

    private String getCropRowId(CroppingPlanEntry crop, Integer rank, CroppingPlanEntry previousCrop, PracticedSystem practicedSystem) {
        return crop.getTopiaId() + '-' + rank + (previousCrop == null ? "" : previousCrop.getTopiaId()) + (practicedSystem == null ? "" : practicedSystem.getTopiaId());
    }
    
    protected TableRowModel getEffectiveSdcTableRowModel(String its,
                                                         String irs,
                                                         Domain domain,
                                                         Optional<GrowingSystem> optionalGrowingSystem,
                                                         Collection<CroppingPlanSpecies> agrosystSpecies,
                                                         Map<String, String> indicatorsNameToColumns) {

        String growingSystemId = optionalGrowingSystem.map(GrowingSystem::getTopiaId).orElse("NONE");

        TableRowModel row = effectiveSdcValues.computeIfAbsent(growingSystemId,
                id -> new EffectiveDbModel.SdcRowModel(
                        performanceId,
                        performanceDate,
                        id,
                        agrosystSpecies,
                        domain,
                        optionalGrowingSystem,
                        its,
                        irs
                )
        );

        row.addIndicatorsColumns(indicatorsNameToColumns);
        return row;
    }
    
    protected TableRowModel getEffectiveItkRowModel(String its, String irs,
                                                    Domain domain,
                                                    Optional<GrowingSystem> optionalGrowingSystem,
                                                    Plot plot,
                                                    Zone zone,
                                                    Map<String, String> indicatorsNameToColumns,
                                                    EffectiveItkCropCycleScaleKey key) {
        
        TableRowModel row = effectiveItkValues.get(key);
        if (row == null) {
            row = new EffectiveDbModel.EffectiveItkRowModel(
                    performanceDate,
                    performanceId,
                    zone,
                    plot,
                    domain,
                    optionalGrowingSystem,
                    its,
                    irs,
                    key.crop(),
                    key.rang(),
                    key.phase(),
                    key.previousCrop(),
                    key.effectiveCropCycleNode());
            effectiveItkValues.put(key, row);
        }
        
        row.addIndicatorsColumns(indicatorsNameToColumns);
        
        return row;
    }
    
    @Override
    public void writeEffectiveItk(
            String its,
            String irs,
            String indicatorCategory,
            String indicatorLabel,
            Object itkValue,
            Integer effectiveItkCropPrevCropPhaseReliabilityIndex,
            String effectiveItkCropPrevCropPhaseComments,
            Domain anonymiseDomain,
            Optional<GrowingSystem> optionalAnoGrowingSystem,
            Plot plot,
            Zone anonymizeZone,
            Class<? extends GenericIndicator> indicatorClass,
            EffectiveItkCropCycleScaleKey key) {
        
        Optional<IndicatorModel> optionalIndicatorModel = getIndicatorModel(indicatorClass);
    
        if (optionalIndicatorModel.isEmpty()) return;// rien à sauvegarder
        IndicatorModel indicatorModel = optionalIndicatorModel.get();
    
        Map<String, String> indicatorsNameToColumns = indicatorModel.getIndicatorsNameToColumns();
        
        TableRowModel  row = getEffectiveItkRowModel(
                its,
                irs,
                anonymiseDomain,
                optionalAnoGrowingSystem,
                plot,
                anonymizeZone,
                indicatorsNameToColumns,
                key);
    
        final String columnName = indicatorsNameToColumns.get(indicatorLabel);
        row.addIndicatorValue(columnName, itkValue);

        row.addExtraValues(effectiveItkCropPrevCropPhaseReliabilityIndex, indicatorModel, effectiveItkCropPrevCropPhaseComments, null, null);
    }
    
}
