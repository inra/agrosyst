package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Locale;

/**
 * Modèle d'import pour les régions agricoles
 * 
 * Columns:
 * <ul>
 * <li>Région
 * <li>Département
 * <li>Arrondissement
 * <li>Pseudo-canton
 * <li>Codecommune
 * <li>Régionagricole
 * <li>Petiterégionagricole
 * <li>Nomdelapetiterégionagricole
 * <li>Préfixecommune
 * <li>Nomdelacommune
 * </ul>
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class CommuneRegionAgricoleModel extends AbstractAgrosystModel<RefLocationDto> {

    public CommuneRegionAgricoleModel() {
        super('\t');

        newIgnoredColumn("Région");
        newIgnoredColumn("Département");
        newIgnoredColumn("Arrondissement");
        newIgnoredColumn("Pseudo-canton");
        newMandatoryColumn("Codecommune", "codeInsee");
        newIgnoredColumn("Régionagricole");
        newMandatoryColumn("Petiterégionagricole", "codePetiteRegionAgricole");
        newMandatoryColumn("Nomdelapetiterégionagricole", "nomPetiteRegionAgricole");
        newIgnoredColumn("Préfixecommune");
        newIgnoredColumn("Nomdelacommune");
    }

    @Override
    public RefLocationDto newEmptyInstance() {
        RefLocationDto instance = new RefLocationDto();
        instance.setPays(Locale.FRANCE.getISO3Country().toLowerCase());
        return instance;
    }

}
