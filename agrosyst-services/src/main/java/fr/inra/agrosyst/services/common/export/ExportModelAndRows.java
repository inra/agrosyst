package fr.inra.agrosyst.services.common.export;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import lombok.Getter;

/**
 * Classe permettant d'assurer la cohérence de typage entre un model et la liste d'éléments
 */
@Getter
public class ExportModelAndRows<T> {

    protected final ExportModel<T> model;
    protected final ImmutableList<T> rows;

    public ExportModelAndRows(ExportModel<T> model, Iterable<T> rows) {
        this.model = model;
        this.rows = ImmutableList.copyOf(rows);
    }

}
