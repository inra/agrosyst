package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigoImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Modele d'import du référentiel sol profondeur INDIGO
 * 
 * <ul>
 * <li>Classe de profondeur INDIGO
 * <li>Profondeur
 * <li>libelle classe
 * <li>h (cm)
 * <li>source
 * </ul>
 * 
 * @author Eric Chatellier
 */
public class RefSolProfondeurIndigoModel extends AbstractAgrosystModel<RefSolProfondeurIndigo> implements ExportModel<RefSolProfondeurIndigo> {

    public RefSolProfondeurIndigoModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("Classe de profondeur INDIGO", RefSolProfondeurIndigo.PROPERTY_CLASSE_DE_PROFONDEUR__INDIGO);
        newMandatoryColumn("Profondeur", RefSolProfondeurIndigo.PROPERTY_PROFONDEUR);
        newMandatoryColumn("libelle classe", RefSolProfondeurIndigo.PROPERTY_LIBELLE_CLASSE);
        newMandatoryColumn("Profondeur par défaut (cm)", RefSolProfondeurIndigo.PROPERTY_PROFONDEUR_PAR_DEFAUT, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("source", RefSolProfondeurIndigo.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefSolProfondeurIndigo.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefSolProfondeurIndigo, Object>> getColumnsForExport() {
        ModelBuilder<RefSolProfondeurIndigo> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Classe de profondeur INDIGO", RefSolProfondeurIndigo.PROPERTY_CLASSE_DE_PROFONDEUR__INDIGO);
        modelBuilder.newColumnForExport("Profondeur", RefSolProfondeurIndigo.PROPERTY_PROFONDEUR);
        modelBuilder.newColumnForExport("libelle classe", RefSolProfondeurIndigo.PROPERTY_LIBELLE_CLASSE);
        modelBuilder.newColumnForExport("Profondeur par défaut (cm)", RefSolProfondeurIndigo.PROPERTY_PROFONDEUR_PAR_DEFAUT, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("source", RefSolProfondeurIndigo.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefSolProfondeurIndigo.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefSolProfondeurIndigo newEmptyInstance() {
        RefSolProfondeurIndigo refSolProfondeurIndigo = new RefSolProfondeurIndigoImpl();
        refSolProfondeurIndigo.setActive(true);
        return refSolProfondeurIndigo;
    }
}
