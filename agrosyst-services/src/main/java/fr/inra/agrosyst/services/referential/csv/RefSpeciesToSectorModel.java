package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSector;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSectorImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefSpeciesToSectorModel extends AbstractDestinationAndPriceModel<RefSpeciesToSector> implements ExportModel<RefSpeciesToSector> {

    public RefSpeciesToSectorModel() {
        super(CSV_SEPARATOR);
    }

    public RefSpeciesToSectorModel(RefEspeceTopiaDao refEspeceDao) {
        super(CSV_SEPARATOR);
        this.refEspeceDao = refEspeceDao;
        getUpperCodeEspeceBotaniqueToCodeEspeceBotanique();

        setColumnsForImport();
    }

    protected void setColumnsForImport() {
        newMandatoryColumn("code_espece_botanique", RefSpeciesToSector.PROPERTY_CODE_ESPECE_BOTANIQUE, CODE_ESPECE_BOTANIQUE_PARSER);
        newMandatoryColumn("code_qualifiant_AEE", RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE, EMPTY_TO_NULL);
        newMandatoryColumn("libelle_espece_botanique", RefSpeciesToSector.PROPERTY_LIBELLE_ESPECE_BOTANIQUE);
        newMandatoryColumn("Filiere", RefSpeciesToSector.PROPERTY_SECTOR, RefDestinationModel.SECTOR_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefSpeciesToSector.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefSpeciesToSector, Object>> getColumnsForExport() {
        ModelBuilder<RefSpeciesToSector> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("code_espece_botanique", RefSpeciesToSector.PROPERTY_CODE_ESPECE_BOTANIQUE);
        modelBuilder.newColumnForExport("code_qualifiant_AEE", RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE);
        modelBuilder.newColumnForExport("libelle_espece_botanique", RefSpeciesToSector.PROPERTY_LIBELLE_ESPECE_BOTANIQUE);
        modelBuilder.newColumnForExport("Filiere", RefSpeciesToSector.PROPERTY_SECTOR, RefDestinationModel.GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefSpeciesToSector.PROPERTY_ACTIVE, T_F_FORMATTER);

        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefSpeciesToSector newEmptyInstance() {
        RefSpeciesToSector result = new RefSpeciesToSectorImpl();
        result.setActive(true);
        return result;
    }
}
