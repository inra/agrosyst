package fr.inra.agrosyst.services.effective;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.services.effective.EffectivePerennialCropCycleDto;
import fr.inra.agrosyst.api.services.effective.EffectiveSeasonalCropCycleDto;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class CreateOrUpdateEffectiveCroCyclesContext {

    protected final Zone zone;
    protected final String zoneId;
    protected final List<EffectiveSeasonalCropCycleDto> effectiveSeasonalCropCycles;
    protected final List<EffectivePerennialCropCycleDto> effectivePerennialCropCycles;
    protected final Collection<AbstractDomainInputStockUnit> domainInputStocks;

    protected final Domain domain;
    protected final List<CroppingPlanEntry> targetedZoneCroppingPlanEntries;
    protected final Map<String, ToolsCoupling> toolsCouplingsByCode;
    
    protected EffectiveSeasonalCropCycleDto effectiveSeasonalCropCycleDto;
    protected EffectiveSeasonalCropCycle effectiveSeasonalCropCycle;
    
    protected EffectivePerennialCropCycle effectivePerennialCropCycle;
    protected EffectivePerennialCropCycleDto effectivePerennialCropCycleDto;
    
    protected final Map<String, HarvestingActionValorisation> savedActionValorisationsByOriginalIds = new HashMap<>();
    protected final Map<String, AbstractAction> savedActionsByOriginalIds = new HashMap<>();

    protected final Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
    protected final Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant;

    
    public CreateOrUpdateEffectiveCroCyclesContext(
            Zone zone,
            List<EffectiveSeasonalCropCycleDto> effectiveSeasonalCropCycles,
            List<EffectivePerennialCropCycleDto> effectivePerennialCropCycles,
            List<CroppingPlanEntry> targetedZoneCroppingPlanEntries,
            Map<String, ToolsCoupling> toolsCouplingByCode,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Collection<AbstractDomainInputStockUnit> domainInputStocks) {
        this.zone = zone;
        this.zoneId = zone.getTopiaId();
        this.effectiveSeasonalCropCycles = effectiveSeasonalCropCycles;
        this.effectivePerennialCropCycles = effectivePerennialCropCycles;
        this.speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
        this.sectorByCodeEspceBotaniqueCodeQualifiant = sectorByCodeEspceBotaniqueCodeQualifiant;
        this.domainInputStocks = domainInputStocks;
        this.targetedZoneCroppingPlanEntries = targetedZoneCroppingPlanEntries;
        this.toolsCouplingsByCode = toolsCouplingByCode;
        this.domain = zone.getPlot().getDomain();
    }
    
    public void addAllSavedAction(Map<String, AbstractAction> savedActionsByOriginalIds) {
        this.savedActionsByOriginalIds.putAll(savedActionsByOriginalIds);
    }
    
    public void addSavedActionValorisationsByOriginalIds(Map<String, HarvestingActionValorisation> savedActionValorisationsByOriginalIds) {
        this.savedActionValorisationsByOriginalIds.putAll(savedActionValorisationsByOriginalIds);
    }
}
