package fr.inra.agrosyst.services.performance.indicators;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;

import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;


/**
 * Marge brute réelle sans/avec l’autoconsommation (syntéhtisé) refs#8509
 * Présentation de l’indicateur
 * La marge brute réelle est exprimée en €/ha. Elle est calculée à toutes les échelles pour des soucis de lisibilité de calculs (on détecte uniquement à l’échelle des interventions les approximations de calcul faites) même si n’a pas de sens agronomique de calculer une marge à des échelles si fines.
 * <p>
 * La marge correspond à la différence entre le produit brut réel, les charges opérationnelles d’intrants réelles de cette culture.
 * Deux variantes peuvent être calculées :
 * La marge brute réelle sans l’autoconsommation tenant compte du produit brut réel sans l’autoconsommation ;
 * La marge brute réelle avec l’autoconsommation tenant compte du produit brut réel avec l’autoconsommation.
 * <p>
 * Formule de calcul
 * Marge brute réelle sans l’autoconsommation (MB réelle sans autoconso)
 * 〖MB réelle sans autoconso 〗_ = 〖PB réelle sans autoconso 〗_ - CO〖i réelles 〗_
 * <p>
 * Marge brute réelle avec l’autoconsommation (MB réelle avec autoconso)
 * 〖MB réelle avec autoconso 〗_ = 〖PB réel avec autoconso 〗_ - COi〖 réel 〗_
 * <p>
 * 〖MB réelle sans autoconso 〗_ (€/ha) : Marge brute réelle sans l’autoconsommation
 * 〖MB réelle avec autoconso 〗_ (€/ha) : Marge brute réelle avec l’autoconsommation
 * 〖PB réel sans autoconso 〗_ (€/ha) : Produit brut réel sans l’autoconsommation
 * 〖PB réel avec autoconso 〗_ (€/ha) : Produit brut réel avec l’autoconsommation
 * C〖Oi réelles 〗_ (€/ha) : Charges opérationnelles (intrants)
 * <p>
 * <p>
 * Note : depuis l'évolution #10574, le coût du carburant en cas d'intervention mécanisée n'est plus pris en compte dans le calcul.
 * <p>
 * <p>
 * Données de référence
 * Aucune donnée de référence pour ce calcul
 * <p>
 * Echelles de calcul
 * Réaliser le calcul pour toutes les échelles habituelles d’agrégation (méthode d’agrégation identique pour tous les indicateurs).
 * Condition(s) de non calcul de l’indicateur
 * L’indicateur sera toujours calculé à toutes les échelles sauf si (à l’échelle intervention), les COi réelles ne le sont pas.
 * Il n’y a qu’une seule condition dans laquelle cela arrive et cette condition est en plus temporaire :
 * <p>
 * RAPPEL Clause de non calcul des COi réelles
 * « A termes, l’indicateur sera toujours calculé.
 * Par contre pour rappel, actuellement, nous sommes allés au plus simple concernant la gestion des incohérences entre les unités des doses d’intrants appliquées et les unités de prix. (cf. paragraphe ci-dessus).
 * Et donc l’indicateur ne sera pas calculé à l’échelle de l’intervention si toutes les unités de doses de tous les intrants associés sont « incompatibles » avec les unités de prix de ces intrants.
 * Par contre, si c’est le cas uniquement pour un des intrants, alors, le calcul est effectué mais par contre il faut bien spécifié via le taux de complétion
 * et la colonne « valeurs par défaut » que le calcul ne tient pas compte de l’intrant dont l’unité de dose est (dans un premier temps) incompatible avec l’unité de prix (texte exact proposé dans le jeu de données).
 * Dans un second temps, nous intègrerons à la formule de calcul des moulinettes de conversion de ces unités de doses grâce à la prise en compte de champs supplémentaires ;
 * le calcul de cet indicateur sera donc systématiquement fait si l’unité de dose d’application diffère de l’unité du prix de référence). »
 */
public class IndicatorGrossMargin extends AbstractIndicator {

    private static final String COLUMN_NAME = "marge_brute";
    private static final String MARGE_BRUTE_REELLE_TX_COMP = COLUMN_NAME + "_reelle_taux_de_completion";
    private static final String MARGE_BRUTE_STD_MIL_TX_COMP = COLUMN_NAME + "_std_mil_taux_de_completion";
    private static final String MARGE_BRUTE_REELLE_DETAIL = COLUMN_NAME + "_reelle_detail_champs_non_renseig";
    private static final String MARGE_BRUTE_STD_MIL_DETAIL = COLUMN_NAME + "_std_mil_detail_champs_non_renseig";

    private static final String[] LABELS = new String[]{
            "Indicator.label.grossMarginRealWithoutAutoConsume",
            "Indicator.label.grossMarginRealWithAutoConsume",
            "Indicator.label.grossMarginStandardisedWithoutAutoConsume",
            "Indicator.label.grossMarginStandardisedWithAutoConsume",
    };

    private boolean isWithAutoConsumed = true;
    private boolean isWithoutAutoConsumed = true;
    private boolean computeReal = true;
    private boolean computeStandardised = true;

    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    public IndicatorGrossMargin() {
        extraFields.put(
                OptionalExtraColumnEnumKey.MARGE_BRUTE_REELLE_TX_COMP,
                MARGE_BRUTE_REELLE_TX_COMP
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.MARGE_BRUTE_REELLE_DETAIL,
                MARGE_BRUTE_REELLE_DETAIL
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.MARGE_BRUTE_STD_MIL_TX_COMP,
                MARGE_BRUTE_STD_MIL_TX_COMP
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.MARGE_BRUTE_STD_MIL_DETAIL,
                MARGE_BRUTE_STD_MIL_DETAIL
        );
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) &&
                displayed && (
                        i == 0 && isWithoutAutoConsumed && computeReal ||
                                i == 1 && isWithAutoConsumed && computeReal ||
                                i == 2 && isWithoutAutoConsumed && computeStandardised ||
                                i == 3 && isWithAutoConsumed && computeStandardised
        );
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();

        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME + "_reelle_sans_autoconso");
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME + "_reelle_avec_autoconso");
        indicatorNameToColumnName.put(getIndicatorLabel(2), COLUMN_NAME + "_std_mil_sans_autoconso");
        indicatorNameToColumnName.put(getIndicatorLabel(3), COLUMN_NAME + "_std_mil_avec_autoconso");

        indicatorNameToColumnName.put(MARGE_BRUTE_REELLE_TX_COMP, MARGE_BRUTE_REELLE_TX_COMP);
        indicatorNameToColumnName.put(MARGE_BRUTE_STD_MIL_TX_COMP, MARGE_BRUTE_STD_MIL_TX_COMP);
        indicatorNameToColumnName.put(MARGE_BRUTE_REELLE_DETAIL, MARGE_BRUTE_REELLE_DETAIL);
        indicatorNameToColumnName.put(MARGE_BRUTE_STD_MIL_DETAIL, MARGE_BRUTE_STD_MIL_DETAIL);

        return indicatorNameToColumnName;
    }

    /**
     * Marge brute réelle sans l’autoconsommation (MB réel sans autoconso)
     * 〖MB réelle sans autoconso 〗_ = 〖PB réel sans autoconso 〗_ - CO〖i réelles 〗_
     * <p>
     * Marge brute réelle avec l’autoconsommation (MB réel avec autoconso)
     * 〖MB réelle avec autoconso 〗_ = 〖PB réel avec autoconso 〗_ - COi〖 réelles 〗_
     * <p>
     * 〖MB réelle sans autoconso 〗_ (€/ha) : Marge brute réelle sans l’autoconsommation
     * 〖MB réelle avec autoconso 〗_ (€/ha) : Marge brute réelle avec l’autoconsommation
     * <p>
     * 〖PB réel sans autoconso 〗_ (€/ha) : Produit brut réel sans l’autoconsommation
     * 〖PB réel avec autoconso 〗_ (€/ha) : Produit brut réel avec l’autoconsommation
     * <p>
     * C〖Oi réelles 〗_ (€/ha) : Charges opérationnelles (intrants)
     */
    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {

        double realProductWithAutoconsume = interventionContext.getRealProductWithAutoconsume();// computed on IndicatorGrossIncome
        double realProductWithoutAutoconsume = interventionContext.getRealProductWithoutAutoconsume();// computed on IndicatorGrossIncome
        double standardisedProductWithAutoconsume = interventionContext.getStandardisedProductWithAutoconsume();// computed on IndicatorGrossIncome
        double standardisedProductWithoutAutoconsume = interventionContext.getStandardisedProductWithoutAutoconsume();// computed on IndicatorGrossIncome

        double realExpenses = interventionContext.getOperatingExpenses();// computed on IndicatorOperatingExpenses
        double standardisedExpenses = interventionContext.getStandardisedOperatingExpenses();// computed on IndicatorOperatingExpenses

        double realGrossMarginWithoutAutoconsume = realProductWithoutAutoconsume - realExpenses;
        double realGrossMarginWithAutoconsume = realProductWithAutoconsume - realExpenses;
        double standardisedGrossMarginWithoutAutoconsume = standardisedProductWithoutAutoconsume - standardisedExpenses;
        double standardisedGrossMarginWithAutoconsume = standardisedProductWithAutoconsume - standardisedExpenses;

        Double[] result = newResult(realGrossMarginWithoutAutoconsume, realGrossMarginWithAutoconsume, standardisedGrossMarginWithoutAutoconsume, standardisedGrossMarginWithAutoconsume);

        if (result != null) {
            interventionContext.setRealGrossMarginWithoutAutoconsume(result[0]);
            interventionContext.setRealGrossMarginWithAutoconsume(result[1]);
            interventionContext.setStandardisedGrossMarginWithoutAutoconsume(result[2]);
            interventionContext.setStandardisedGrossMarginWithAutoconsume(result[3]);
        }

        return result;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {

        if (interventionContext.isFictive()) return newArray(LABELS.length, 0d);

        double realProductWithAutoconsume = interventionContext.getRealProductWithAutoconsume();
        double realProductWithoutAutoconsume = interventionContext.getRealProductWithoutAutoconsume();
        double standardisedProductWithAutoconsume = interventionContext.getStandardisedProductWithAutoconsume();
        double standardisedProductWithoutAutoconsume = interventionContext.getStandardisedProductWithoutAutoconsume();

        double realExpenses = interventionContext.getOperatingExpenses();
        double standardisedExpenses = interventionContext.getStandardisedOperatingExpenses();

        Double realGrossMarginWithoutAutoconsume = realProductWithoutAutoconsume - realExpenses;
        Double realGrossMarginWithAutoconsume = realProductWithAutoconsume - realExpenses;
        Double standardisedGrossMarginWithoutAutoconsume = standardisedProductWithoutAutoconsume - standardisedExpenses;
        Double standardisedGrossMarginWithAutoconsume = standardisedProductWithAutoconsume - standardisedExpenses;

        Double[] result = newResult(realGrossMarginWithoutAutoconsume, realGrossMarginWithAutoconsume, standardisedGrossMarginWithoutAutoconsume, standardisedGrossMarginWithAutoconsume);

        if (result != null) {
            interventionContext.setRealGrossMarginWithoutAutoconsume(result[0]);
            interventionContext.setRealGrossMarginWithAutoconsume(result[1]);
            interventionContext.setStandardisedGrossMarginWithoutAutoconsume(result[2]);
            interventionContext.setStandardisedGrossMarginWithAutoconsume(result[3]);
        }

        return result;
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_ECONOMIC);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
        if (displayed) {
            isWithAutoConsumed = filter.getWithAutoConsumed() != null && filter.getWithAutoConsumed();
            isWithoutAutoConsumed = filter.getWithoutAutoConsumed() != null && filter.getWithoutAutoConsumed();
            computeReal = filter.getComputeReal() != null && filter.getComputeReal();
            computeStandardised = filter.getComputeStandardized() != null && filter.getComputeStandardized();
        }
    }
}
