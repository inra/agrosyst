package fr.inra.agrosyst.services.generic;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCultureMAA;
import fr.inra.agrosyst.api.entities.referential.RefCultureMAATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntity;
import fr.inra.agrosyst.api.entities.referential.ReferentialI18nEntry;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.generic.GenericEntityService;
import fr.inra.agrosyst.api.services.generic.GenericFilter;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.referential.csv.RefEspeceDto;
import fr.inra.agrosyst.services.referential.csv.RefLocationExtendedDto;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class GenericEntityServiceImpl extends AbstractAgrosystService implements GenericEntityService {

    private static final Log LOGGER = LogFactory.getLog(GenericEntityServiceImpl.class);

    protected static final String PROPERTY_VALEUR = "Valeur";

    protected static final Function<Object, Map<String, String>> ENUM_NAME_AS_MAP = enumConstant -> {
        Enum<?> anEnum = (Enum<?>) enumConstant;
        LinkedHashMap<String, String> result = Maps.newLinkedHashMap();
        result.put(PROPERTY_VALEUR, anEnum.name());
        return result;
    };

    protected static final Function<String, Class<?>> GET_CLASS = input -> {
        try {
            return Class.forName(input);
        } catch (ClassNotFoundException e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("An exception occurred", e);
            }
            throw new AgrosystTechnicalException("Class not found", e);
        }
    };
    
    protected BusinessAuthorizationService authorizationService;
    
    public void setAuthorizationService(BusinessAuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    protected PaginationResult<?> refLocationsListEntities(GenericFilter filter) {
        PaginationResult<?> result;

        StringBuilder query = new StringBuilder(String.format(" FROM %s refLocation ", RefLocation.class.getName()));
        query.append(" WHERE 1 = 1 ");
        Map<String, Object> args = Maps.newLinkedHashMap();

        // TODO: filter
        Set<Map.Entry<String, String>> filterEntries = Optional.ofNullable(filter)
                .map(GenericFilter::getPropertyNamesAndValues)
                .map(Map::entrySet)
                .orElse(new HashSet<>());
        for (Map.Entry<String, String> entry : filterEntries) {
            String propertyName = entry.getKey();
            String value = entry.getValue();
            if (StringUtils.isEmpty(value)) {
                continue;
            }
            Class returnType = getReturnType(RefLocationExtendedDto.class, propertyName);
            if (returnType != null) {
                if (RefLocationExtendedDto.PROPERTY_PAYS.equals(propertyName)) {
                    String subQuery = " AND EXISTS (SELECT 1 FROM refLocation." + RefLocation.PROPERTY_REF_COUNTRY + " refCountry WHERE 1 = 1 ";
                    subQuery += DaoUtils.andAttributeLike("refCountry", RefCountry.PROPERTY_FRENCH_NAME, args, value);
                    subQuery += ")";
                    query.append(subQuery);
                } else if (returnType.isAssignableFrom(String.class)) {
                    String subQuery = DaoUtils.andAttributeLike("refLocation", propertyName, args, value);
                    query.append(subQuery);
                } else {
                    try {
                        Object arg;
                        if (StringUtils.lowerCase(Boolean.class.getName()).contains(returnType.getName())) {
                            arg = CommonService.BOOLEAN_PARSER.parse(value);
                        } else {
                            arg = ConvertUtils.convert(value, returnType);
                        }

                        String subQuery = DaoUtils.andAttributeEquals("refLocation", propertyName, args, arg);
                        query.append(subQuery);
                    } catch (NumberFormatException | ParseException ex) {
                        LOGGER.debug("Invalid value for numeric field", ex);
                    }
                }
            } else {
                LOGGER.warn("Can't get return type for property " + propertyName + " on " + RefLocationExtendedDto.class.getName());
            }
        }

        // active
        if (filter != null) {
            String subQuery = DaoUtils.andAttributeEquals("refLocation", "active", args, filter.getActive());
            query.append(subQuery);
        }

        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;

        TopiaJpaSupport jpaSupport = getPersistenceContext().getJpaSupport();
        List<RefLocation> entities = jpaSupport.find(query +
                " ORDER BY refLocation." + TopiaEntity.PROPERTY_TOPIA_CREATE_DATE +
                ", refLocation." + TopiaEntity.PROPERTY_TOPIA_ID, startIndex, endIndex, args);
        long totalCount = ((Number) jpaSupport.findUnique("SELECT COUNT(*) " + query, args)).longValue();

        Binder<RefLocation, RefLocationExtendedDto> refLocationBinder = BinderFactory.newBinder(RefLocation.class, RefLocationExtendedDto.class);
        List<RefLocationExtendedDto> refLocationDtos = new ArrayList<>();
        for (RefLocation entity : entities) {
            RefLocationExtendedDto dto = new RefLocationExtendedDto(entity.getTopiaId());
            refLocationBinder.copyExcluding(entity, dto, RefLocation.PROPERTY_REF_COUNTRY);
            dto.setPays(entity.getRefCountry().getFrenchName());
            refLocationDtos.add(dto);
        }

        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        result = PaginationResult.of(refLocationDtos, totalCount, pager);
        return result;
    }
    
    protected PaginationResult<?> refEspecesListEntities(GenericFilter filter) {
        PaginationResult<?> result;
        
        StringBuilder query = new StringBuilder(String.format(" FROM %s refEspece ", RefEspece.class.getName()));
        query.append(" WHERE 1 = 1 ");
        Map<String, Object> args = Maps.newLinkedHashMap();
        
        if (filter != null) {
            Map<String, String> propertyNamesAndValues = filter.getPropertyNamesAndValues();
            if (propertyNamesAndValues != null) {
                for (Map.Entry<String, String> entry : propertyNamesAndValues.entrySet()) {
                    String propertyName = entry.getKey();
                    String value = entry.getValue();
                    if (value.isEmpty()) {
                        continue;
                    }
                    Class returnType = getReturnType(RefEspeceDto.class, propertyName);
                    
                    if (returnType != null) {
                        try {
                            if (RefCultureMAA.PROPERTY_CODE_CULTURE_MAA.equals(propertyName) || RefCultureMAA.PROPERTY_CULTURE_MAA.equals(propertyName)) {
                                String subQuery = " AND EXISTS (SELECT 1 FROM refEspece." + RefEspece.PROPERTY_CULTURES_MAA + " cmaa WHERE 1 = 1 ";
                                subQuery += DaoUtils.andAttributeLike("cmaa", propertyName, args, value);
                                subQuery += ")";
                                query.append(subQuery);
                            } else if (returnType.isAssignableFrom(String.class)) {
                                String subQuery = DaoUtils.andAttributeLike("refEspece", propertyName, args, value);
                                query.append(subQuery);
                            } else {
                                
                                Object arg;
                                if (StringUtils.lowerCase(Boolean.class.getName()).contains(returnType.getName())) {
                                    arg = CommonService.BOOLEAN_PARSER.parse(value);
                                } else {
                                    arg = ConvertUtils.convert(value, returnType);
                                }
                                
                                String subQuery = DaoUtils.andAttributeEquals("refEspece", propertyName, args, arg);
                                query.append(subQuery);
                            }
                        } catch (NumberFormatException | ParseException ex) {
                            if (LOGGER.isDebugEnabled()) {
                                LOGGER.debug("Invalid value for numeric field", ex);
                            }
                        }
                    } else {
                        if (LOGGER.isWarnEnabled()) {
                            LOGGER.warn("Can't get return type for property " + propertyName + " on " + RefEspece.class.getName());
                        }
                    }
                }
            }
            
            // active
            String subQuery = DaoUtils.andAttributeEquals("refEspece", "active", args, filter.getActive());
            query.append(subQuery);
        }
        
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
        
        TopiaJpaSupport jpaSupport = getPersistenceContext().getJpaSupport();
        List<RefEspece> entities = jpaSupport.find(query +
                " ORDER BY refEspece." + TopiaEntity.PROPERTY_TOPIA_CREATE_DATE +
                ", refEspece." + TopiaEntity.PROPERTY_TOPIA_ID, startIndex, endIndex, args);
        long totalCount = ((Number) jpaSupport.findUnique("SELECT COUNT(*) " + query, args)).longValue();
        
        Binder<RefEspece, RefEspeceDto> refEspeceBinder = BinderFactory.newBinder(RefEspece.class, RefEspeceDto.class);
        List<RefEspeceDto> refEspeceDtos = new ArrayList<>();
        for (RefEspece entity : entities) {
            
            final Collection<RefCultureMAA> culturesMaas = entity.getCulturesMaa();
            if (CollectionUtils.isNotEmpty(culturesMaas)) {
                for (RefCultureMAA culturesMaa : culturesMaas) {
                    RefEspeceDto dto = new RefEspeceDto(culturesMaa.getTopiaId());
                    refEspeceBinder.copyExcluding(entity, dto, RefEspece.PROPERTY_CULTURES_MAA, RefEspece.PROPERTY_ACTIVE);
                    dto.setCode_culture_maa(culturesMaa.getCode_culture_maa());
                    dto.setCulture_maa(culturesMaa.getCulture_maa());
                    dto.setActive(culturesMaa.isActive());
                    refEspeceDtos.add(dto);
                }
                
            } else {
                RefEspeceDto dto = new RefEspeceDto(entity.getTopiaId());
                refEspeceBinder.copyExcluding(entity, dto, RefEspece.PROPERTY_CULTURES_MAA);
                refEspeceDtos.add(dto);
            }
        }
        
        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        result = PaginationResult.of(refEspeceDtos, totalCount, pager);
        return result;
    }

    protected PaginationResult<ReferentialI18nEntry> referentialI18nEntries(Class<? extends ReferentialI18nEntry> klass, GenericFilter filter) {
        PaginationResult<ReferentialI18nEntry> result;

        StringBuilder query = new StringBuilder(String.format(" FROM %s refTrad ", klass.getName()));
        query.append(" WHERE 1 = 1 ");
        Map<String, Object> args = Maps.newLinkedHashMap();

        Set<Map.Entry<String, String>> filterEntries = Optional.ofNullable(filter)
                .map(GenericFilter::getPropertyNamesAndValues)
                .map(Map::entrySet)
                .orElse(new HashSet<>());
        for (Map.Entry<String, String> entry : filterEntries) {
            String propertyName = entry.getKey();
            String value = entry.getValue();
            if (StringUtils.isNotEmpty(value)) {
                String subQuery = DaoUtils.andAttributeLike("refTrad", propertyName, args, value);
                query.append(subQuery);
            }
        }

        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;

        TopiaJpaSupport jpaSupport = getPersistenceContext().getJpaSupport();
        List<ReferentialI18nEntry> entities = jpaSupport.find(query + " ORDER BY refTrad."
                        + ReferentialI18nEntry.PROPERTY_LANG
                        + ", refTrad." + ReferentialI18nEntry.PROPERTY_TRADUCTION,
                    startIndex, endIndex, args);
        long totalCount = ((Number) jpaSupport.findUnique("SELECT COUNT(*) " + query, args)).longValue();

        PaginationParameter pager = PaginationParameter.of(page, count);
        result = PaginationResult.of(entities, totalCount, pager);
        return result;
    }

    @Override
    public PaginationResult<?> listEntities(Class<?> klass, GenericFilter filter) {
        authorizationService.checkIsAdmin();
        
        if (RefEspeceDto.class == klass) {
            return refEspecesListEntities(filter);
        } else if (RefLocationExtendedDto.class == klass) {
            return refLocationsListEntities(filter);
        } else if (ReferentialI18nEntry.class.isAssignableFrom(klass)) {
            return referentialI18nEntries((Class<? extends ReferentialI18nEntry>)klass, filter);
        }
        
        Preconditions.checkArgument(!AgrosystUser.class.equals(klass), "Forbidden entity: " + klass);
        
        PaginationResult<?> result;
        
        if (klass.isEnum()) {
            
            Object[] enumConstants = klass.getEnumConstants();
            
            Iterable<Map<String, String>> transformed = Lists.newArrayList(enumConstants).stream().map(ENUM_NAME_AS_MAP).collect(Collectors.toList());

            PaginationParameter pager = PaginationParameter.of(0, enumConstants.length);
            result = PaginationResult.of(Lists.newArrayList(transformed), enumConstants.length, pager);

        } else if (TopiaEntity.class.isAssignableFrom(klass)) {

            Class<? extends TopiaEntity> entityKlass = (Class<? extends TopiaEntity>) klass;
            TopiaDao dao = context.getDaoSupplier().getDao(entityKlass);

            StringBuilder query = new StringBuilder(String.format(" FROM %s t ", dao.getEntityClass().getName()));
            query.append(" WHERE 1 = 1 ");
            Map<String, Object> args = Maps.newLinkedHashMap();

            if (filter != null) {
                Map<String, String> propertyNamesAndValues = filter.getPropertyNamesAndValues();
                if (propertyNamesAndValues != null) {
                    for (Map.Entry<String, String> entry : propertyNamesAndValues.entrySet()) {
                        String propertyName = entry.getKey();
                        String value = entry.getValue();
                        if (value.isEmpty()) {
                            continue;
                        }
                        Class returnType = getReturnType(entityKlass, propertyName);

                        if (returnType != null) {
                            try {
                                if (returnType.isAssignableFrom(String.class) || returnType.isEnum()) {
                                    String subQuery = DaoUtils.andAttributeLike("t", propertyName, args, value);
                                    query.append(subQuery);
                                } else if (returnType.isAssignableFrom(Collection.class)) {
                                    // convert in generic type (for enum only)
                                    ParameterizedType genericReturnType = (ParameterizedType)getGenericReturnType(entityKlass, propertyName);
                                    Class<?> genericParam = (Class<?>)genericReturnType.getActualTypeArguments()[0];
                                    if (genericParam.isEnum()) {
                                        Iterable<String> split = Splitter.on(',').trimResults().omitEmptyStrings().split(value);
                                        query.append(" AND ( 1 = 1");
                                        for (String s : split) {
                                            Object arg = EnumUtils.getEnum((Class<? extends Enum>)genericParam, s);
                                            String subQuery = DaoUtils.andAttributeInElements("t", propertyName, args, arg);
                                            query.append(subQuery);
                                        }
                                        query.append(")");
                                    }
                                } else {

                                    Object arg;
                                    if (StringUtils.lowerCase(Boolean.class.getName()).contains(returnType.getName())) {
                                        arg = CommonService.BOOLEAN_PARSER.parse(value);
                                    } else {
                                        arg = ConvertUtils.convert(value, returnType);
                                    }

                                    String subQuery = DaoUtils.andAttributeEquals("t", propertyName, args, arg);
                                    query.append(subQuery);
                                }
                            } catch (NumberFormatException | ParseException ex) {
                                if (LOGGER.isDebugEnabled()) {
                                    LOGGER.debug("Invalid value for numeric field", ex);
                                }
                            }
                        } else {
                            if (LOGGER.isWarnEnabled()) {
                                LOGGER.warn("Can't get return type for property " + propertyName + " on " + entityKlass.getName());
                            }
                        }
                    }
                }

                // active
                String subQuery = DaoUtils.andAttributeEquals("t", "active", args, filter.getActive());
                query.append(subQuery);
            }

            int page = filter != null ? filter.getPage() : 0;
            int count = filter != null ? filter.getPageSize() : 10;
            int startIndex = page * count;
            int endIndex = page * count + count - 1;

            TopiaJpaSupport jpaSupport = getPersistenceContext().getJpaSupport();
            List<?> entities = jpaSupport.find(query + " ORDER BY t." + TopiaEntity.PROPERTY_TOPIA_CREATE_DATE
                    + ", t." + TopiaEntity.PROPERTY_TOPIA_ID, startIndex, endIndex, args);
            long totalCount = ((Number) jpaSupport.findUnique("SELECT COUNT(*) " + query, args)).longValue();

            // build result bean
            PaginationParameter pager = PaginationParameter.of(page, count);
            result = PaginationResult.of(entities, totalCount, pager);
        } else {
            throw new UnsupportedOperationException("This class cannot be used in this service: " + klass);
        }

        return result;
    }

    /**
     * Recursive getReturn type method that get method return type by checking inhéritance too.
     *
     * @return return type, or null if undetermined
     */
    protected Class getReturnType(Class klass, String methodName) {
        Class result = null;

        try {
            if (klass.getName().contains(RefFertiMinUNIFA.class.getName()) && "sO3".contentEquals(methodName)) {
                Method method = klass.getDeclaredMethod("getsO3");
                result = method.getReturnType();
            } else {
                Method method = klass.getDeclaredMethod("get" + StringUtils.capitalize(methodName));
                result = method.getReturnType();
            }
        } catch (NoSuchMethodException ex0) {
            try {
                Method method = klass.getDeclaredMethod("is" + StringUtils.capitalize(methodName));
                result = method.getReturnType();
            } catch (NoSuchMethodException ex) {
                // not found here, try on super interfaces
                Class[] superKlass = klass.getInterfaces();
                for (int i = 0; i < superKlass.length && result == null; i++) {
                    result = getReturnType(superKlass[i], methodName);
                }
            } catch (Exception ex) {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("Can't get method return type", ex);
                }
            }
        }

        return result;
    }

    /**
     * Recursive getReturn type method that get method return type by checking inhéritance too.
     *
     * @return return type, or null if undetermined
     */
    protected Type getGenericReturnType(Class klass, String methodName) {
        Type result = null;
        try {
            Method method = klass.getDeclaredMethod("get" + StringUtils.capitalize(methodName));
            result = method.getGenericReturnType();
        } catch (NoSuchMethodException ex) {
            // not found here, try on super interfaces
            Class[] superKlass = klass.getInterfaces();
            for (int i = 0; i < superKlass.length && result == null; i++) {
                result = getGenericReturnType(superKlass[i], methodName);
            }
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Can't get method return type", ex);
            }
        }
        return result;
    }

    @Override
    public Map<String, Long> countEntities(Class<?>... classes) {
        List<Class<?>> list = Arrays.asList(classes);
        Map<String, Long> result = countEntities0(list);
        return result;
    }

    protected Map<String, Long> countEntities0(Iterable<Class<?>> classes) {
        authorizationService.checkIsAdmin();
        Map<String, Long> result = Maps.newLinkedHashMap();

        for (Class<?> klass : classes) {
    
            if (klass.isEnum()) {
        
                Object[] enumConstants = klass.getEnumConstants();
                long count = enumConstants == null ? 0L : enumConstants.length;
                result.put(klass.getName(), count);
            } else if (RefEspeceDto.class.isAssignableFrom(klass)) {
                TopiaDao dao = context.getDaoSupplier().getRefEspeceDao();

                long count = dao.count();
                result.put(klass.getName(), count);
            } else if (RefLocationExtendedDto.class.isAssignableFrom(klass)) {
                long count = context.getDaoSupplier().getRefLocationDao().count();
                result.put(klass.getName(), count);
            } else if (ReferentialI18nEntry.class.isAssignableFrom(klass)) {
                TopiaJpaSupport jpaSupport = getPersistenceContext().getJpaSupport();
                long count = ((Number)jpaSupport.findUnique(
                        String.format("SELECT COUNT(*) FROM %s", klass.getName()), new HashMap<>()))
                        .longValue();
                result.put(klass.getName(), count);
            } else if (TopiaEntity.class.isAssignableFrom(klass)) {
        
                Class<? extends TopiaEntity> entityKlass = (Class<? extends TopiaEntity>) klass;
                TopiaDao dao = context.getDaoSupplier().getDao(entityKlass);
        
                long count = dao.count();
                result.put(klass.getName(), count);
            } else {
                throw new UnsupportedOperationException("This class cannot be used in this service: " + klass);
            }
        }

        return result;
    }

    @Override
    public List<String> getProperties(Class<?> klass) {
        authorizationService.checkIsAdmin();

        List<String> result = new ArrayList<>();
    
        if (klass.isEnum()) {
        
            result.add(PROPERTY_VALEUR);
        
        } else if (TopiaEntity.class.isAssignableFrom(klass) || RefEspeceDto.class.isAssignableFrom(klass)) {

            Class<? extends TopiaEntity> entityKlass = (Class<? extends TopiaEntity>) klass;

            Set<Class<? extends TopiaEntity>> allClasses = getAllClassesExceptTopiaEntity(entityKlass);
            for (Class<? extends TopiaEntity> aClass : allClasses) {
                for (Field declaredField : aClass.getDeclaredFields()) {
                    if (declaredField.getName().startsWith("PROPERTY_")) {
                        try {
                            String value = (String) declaredField.get(null);
                            result.add(value);
                        } catch (IllegalAccessException e) {
                            if (LOGGER.isErrorEnabled()) {
                                LOGGER.error("Un exception occured", e);
                            }
                        }
                    }
                }
            }
        } else if (ReferentialI18nEntry.class.isAssignableFrom(klass)) {
            result.addAll(ReferentialI18nEntry.getAllProperties());
        } else if (RefLocationExtendedDto.class.isAssignableFrom(klass)) {
            result.addAll(RefLocationExtendedDto.getAllProperties());
        } else {
            throw new UnsupportedOperationException("This class cannot be used in this service: " + klass);
        }
        return result;
    }

    protected Set<Class<? extends TopiaEntity>> getAllClassesExceptTopiaEntity(Class<? extends TopiaEntity> entityClass) {
        Set<Class<? extends TopiaEntity>> result = Sets.newLinkedHashSet();
        Class<?>[] interfaces = entityClass.getInterfaces();
        if (interfaces != null) {
            for (Class<?> aClass : interfaces) {
                if (!TopiaEntity.class.equals(aClass) && TopiaEntity.class.isAssignableFrom(aClass)) {
                    Class<? extends TopiaEntity> parentClass = (Class<? extends TopiaEntity>) aClass;
                    Set<Class<? extends TopiaEntity>> parentClasses = getAllClassesExceptTopiaEntity(parentClass);
                    result.addAll(parentClasses);
                }
            }
        }
        result.add(entityClass); // Add at the end in order to get always the same order
        return result;
    }

    @Override
    public void unactivateEntities(Class<?> klass, List<String> entityIds, boolean activate) {
        authorizationService.checkIsAdmin();

        if (RefEspeceDto.class == klass) {
            RefCultureMAATopiaDao refCultureMAADao = context.getDaoSupplier().getRefCultureMAADao();
            RefEspeceTopiaDao refEspeceDao = context.getDaoSupplier().getRefEspeceDao();

            for (String entityId : entityIds) {

                if (entityId.startsWith(RefCultureMAA.class.getName())) {
                    RefCultureMAA entity = refCultureMAADao.forTopiaIdEquals(entityId).findUnique();
                    entity.setActive(activate);
                    refCultureMAADao.update(entity);

                    RefEspece espece = refEspeceDao.forCulturesMaaContains(entity).findUnique();
                    espece.setActive(espece.getCulturesMaa().stream().anyMatch(RefCultureMAA::isActive));
                    refEspeceDao.update(espece);

                } else if (entityId.startsWith(RefEspece.class.getName())) {
                    RefEspece entity = refEspeceDao.forTopiaIdEquals(entityId).findUnique();
                    entity.setActive(activate);
                    refEspeceDao.update(entity);

                } else {
                    throw new UnsupportedOperationException("This class cannot be used in this service: " + entityId);
                }
            }
        } else if (RefLocationExtendedDto.class == klass) {
            RefLocationTopiaDao refLocationDao = context.getDaoSupplier().getRefLocationDao();
            for (String entityId : entityIds) {
                if (entityId.startsWith(RefLocation.class.getName())) {
                    RefLocation entity = refLocationDao.forTopiaIdEquals(entityId).findUnique();
                    entity.setActive(activate);
                    refLocationDao.update(entity);
                } else {
                    throw new UnsupportedOperationException("This class cannot be used in this service: " + entityId);
                }
            }

        } else if (ReferentialEntity.class.isAssignableFrom(klass)) {

            Class<? extends ReferentialEntity> entityKlass = (Class<? extends ReferentialEntity>) klass;
            TopiaDao dao = context.getDaoSupplier().getDao(entityKlass);

            for (String entityId : entityIds) {

                TopiaEntity entity = dao.forTopiaIdEquals(entityId).findUnique();
                ((ReferentialEntity) entity).setActive(activate);
                dao.update(entity);
            }

        } else {
            throw new UnsupportedOperationException("This class cannot be used in this service: " + klass);
        }
        getTransaction().commit();
    }

    @Override
    public Map<String, Long> countEntitiesFromString(List<String> classesList) {
        Iterable<Class<?>> classes = classesList.stream().map(GET_CLASS::apply).collect(Collectors.toList());
        Map<String, Long> result = countEntities0(classes);
        return result;
    }

    @Override
    public PaginationResult<?> listEntitiesFromString(String className, GenericFilter filter) {
        PaginationResult<?> result = listEntities(GET_CLASS.apply(className), filter);
        return result;
    }

    @Override
    public List<String> getPropertiesFromString(String className) {
        List<String> result = getProperties(GET_CLASS.apply(className));
        return result;
    }

    @Override
    public void unactivateEntitiesFromString(String className, List<String> entityIds, boolean activate) {
        unactivateEntities(GET_CLASS.apply(className), entityIds, activate);
    }
}
