package fr.inra.agrosyst.services.performance.dbPersistence;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.services.performance.utils.PriceConverterKeysToRate;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public record DbCommonReferentiels(RefSolTextureGeppa defaultRefSolTextureGeppa, RefSolProfondeurIndigo defaultSolDepth,
                                   RefLocation defaultLocation,
                                   PriceConverterKeysToRate priceConverterKeysToRate) implements Serializable {

    @Serial
    private static final long serialVersionUID = -3974507712451378201L;

}
