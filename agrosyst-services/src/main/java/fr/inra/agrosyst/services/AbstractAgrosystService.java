package fr.inra.agrosyst.services;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AgrosystTopiaPersistenceContext;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.async.BusinessTasksManager;
import fr.inra.agrosyst.services.common.CacheDiscriminator;
import fr.inra.agrosyst.services.common.CacheService;
import fr.inra.agrosyst.services.security.SecurityContext;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaTransaction;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Tous les services doivent hériter de cette classe.
 *
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public abstract class AbstractAgrosystService implements AgrosystService {
    
    /**
     * On va utiliser le contexte par délégation
     */
    protected ServiceContext context;

    public ServiceContext getContext() {
        return context;
    }

    public void setContext(ServiceContext context) {
        this.context = context;
    }

    public TopiaTransaction getTransaction() {
        return context.getTransaction();
    }

    public AgrosystTopiaPersistenceContext getPersistenceContext() {
        return context.getPersistenceContext();
    }

    protected AgrosystServiceConfig getConfig() {
        return context.getConfig();
    }

    public SecurityContext getSecurityContext() {
        return context.getSecurityContext();
    }

    protected SecurityContext getSecurityContextAsUser(String userId) {
        return context.getSecurityContextAsUser(userId);
    }

    public <I> I newInstance(Class<I> clazz) {
        return context.newInstance(clazz);
    }

    public <K extends TopiaEntity> void easyBind(TopiaDao<K> dao, List<K> existingRaw, List<K> newItemsRaw, String... exclusions) {
        easyBind(dao, existingRaw, newItemsRaw, null, null, exclusions);
    }

    public <K extends TopiaEntity> void easyBind(TopiaDao<K> dao, List<K> existingRaw, List<K> newItemsRaw,
                                                  Consumer<K> onNewItem, String... exclusions) {
        easyBind(dao, existingRaw, newItemsRaw, onNewItem, null, exclusions);
    }

    public <K extends TopiaEntity> void easyBind(TopiaDao<K> dao, List<K> existingRaw, List<K> newItemsRaw,
                                                 Consumer<K> onNewItem, Consumer<K> afterBind,
                                                 String... exclusions) {

        // This list represents the elements to remove at the end of the loop
        List<K> toDelete = easyBindNoDelete(dao, existingRaw, newItemsRaw, onNewItem, afterBind, null, exclusions);

        // Now delete all unused items
        dao.deleteAll(toDelete);
    }

    public <K extends TopiaEntity> List<K> easyBindNoDelete(TopiaDao<K> dao,
                                                            List<K> existingRaw,
                                                            List<K> newItemsRaw,
                                                            Consumer<K> onNewItem,
                                                            Consumer<K> afterBind,
                                                            Consumer<K> afterPersist,
                                                            String... exclusions) {

        List<K> existing = MoreObjects.firstNonNull(existingRaw, new ArrayList<>());
        List<K> newItems = MoreObjects.firstNonNull(newItemsRaw, new ArrayList<>());

        Set<String> exclusionsSet = Sets.newHashSet(TopiaEntity.PROPERTY_TOPIA_CREATE_DATE);
        if (exclusions != null) {
            Iterables.addAll(exclusionsSet, Sets.newHashSet(exclusions));
        }
        String[] exclusionsArray = Iterables.toArray(exclusionsSet, String.class);

        // This list represents the elements to remove at the end of the loop
        List<K> toDelete = Lists.newArrayList(existing);

        // Maintain an index of the existing entities
        Map<String, K> existingIndex = Maps.uniqueIndex(existing, Entities.GET_TOPIA_ID::apply);

        // Loop over each element
        for (K item : newItems) {
            String topiaId = item.getTopiaId();
            // Look for the element in the existing element's cache
            K entityEntry = existingIndex.get(topiaId);
            if (entityEntry == null) {
                entityEntry = item;
            } else {
                // If found, that means the element is still used, do not schedule its deletion
                toDelete.remove(entityEntry);
            }

            Class<K> clazz = (Class<K>) item.getClass();
            Binder<K, K> binder = BinderFactory.newBinder(clazz);

            binder.copyExcluding(item, entityEntry, exclusionsArray);

            if (afterBind != null) {
                afterBind.accept(entityEntry);
            }

            if (entityEntry.isPersisted()) {
                dao.update(entityEntry);
            } else {
                if (onNewItem != null) {
                    onNewItem.accept(entityEntry);
                }
                dao.create(entityEntry);
            }
            if (afterPersist != null) {
                afterPersist.accept(entityEntry);
            }
        }

        // No deletion made in this method, just return the list
        return toDelete;
    }

    protected BusinessTasksManager getBusinessTaskManager() {
        BusinessTasksManager result = context.getTaskManager();
        return result;
    }

    protected void clearCacheAndSync() {
        // On commence par purger le cache local ...
        context.newService(CacheService.class).clear();
        // ... puis on demande aux éventuelles autres instances d'en faire de même
        context.getSynchroHelper().emitClearCache();
    }

    protected void clearSingleCacheAndSync(CacheDiscriminator discriminator) {
        // On commence par purger le cache local ...
        context.newService(CacheService.class).clearSingle(discriminator);
        // ... puis on demande aux éventuelles autres instances d'en faire de même
        context.getSynchroHelper().emitClearSingleCache(discriminator);
    }

    /**
     * Permet de récupérer l'instance de UserDto de l'utilisateur accédant actuellement aux services. C'est l'instance
     * qui a été injectée dans le service context. Cette méthode ne fait aucun accès à la base !
     */
    protected AuthenticatedUser getAuthenticatedUser() {
        return context.getSecurityContext().getAuthenticatedUser();
    }
    
//    public abstract void persistOrDeleteAbstractPhytoProductUsageInputUsages(Collection<PhytoProductInputUsageDto> phytoProductInputUsageDtos, Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds, Collection<PesticideProductInputUsage> pesticideProductInputs, InputType applicationDeProduitsPhytosanitaires);
}
