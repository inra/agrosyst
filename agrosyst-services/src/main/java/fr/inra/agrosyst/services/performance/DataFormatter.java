package fr.inra.agrosyst.services.performance;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.FormatUtils;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PhytoProductTarget;
import fr.inra.agrosyst.api.entities.action.PotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.SubstrateInputUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

public interface DataFormatter {

    DateTimeFormatter INTERVENTION_DATE_FORMAT_DDMMYYYY = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    String NO_PREVIOUS_CROP = "IndicatorWriter.previousNotSpecified";
    String CROP_NAME_RANK = "IndicatorWriter.rank";
    String DECIMAL_FORMAT = "0.0##";
    String SEPARATOR = ", ";

    /**
     * Join actions type into string.
     */
    default String getActionsToString(Collection<AbstractAction> actions) {
        String result = null;
        if (actions != null) {
            result = actions.stream()
                    .map(input -> AgrosystI18nService.getEnumTraduction(getLocale(), input.getMainAction().getIntervention_agrosyst()))
                    .collect(Collectors.joining(SEPARATOR));
        }
        return StringUtils.defaultString(result);
    }

    default String getActionToString(AbstractAction action) {
        String result = null;
        if (action != null) {
            result = AgrosystI18nService.getEnumTraduction(getLocale(), action.getMainAction().getIntervention_agrosyst());
        }
        return StringUtils.defaultString(result);
    }

    default String getInputUsageProductName(
            AbstractInputUsage inputUsage,
            AbstractAction action,
            CroppingPlanEntry intermediateCrop,
            CroppingPlanEntry mainCrop,
            boolean appendQtAvgAndUnit) {

        if (inputUsage == null) {
            if (action instanceof SeedingActionUsage) {
                CroppingPlanEntry crop = ObjectUtils.firstNonNull(intermediateCrop, mainCrop);
                return crop != null ? crop.getName() : l(getLocale(), "IndicatorWriter.crop");//Culture
            }
            if (action instanceof IrrigationAction) {
                return l(getLocale(), "IndicatorWriter.irrigationWater");
            }
            return "";
        }

        String productName_ = null;

        switch (inputUsage.getInputType()) {
            case SEMIS -> {
                if (inputUsage instanceof SeedSpeciesInputUsage seedSpeciesInputUsage) {
                    productName_ = seedSpeciesInputUsage.getDomainSeedSpeciesInput().getInputName();
                } else if (inputUsage instanceof SeedLotInputUsage seedLotInputUsage) {
                    productName_ = seedLotInputUsage.getDomainSeedLotInput().getInputName();
                }
                if (StringUtils.isEmpty(productName_)) {
                    CroppingPlanEntry crop = ObjectUtils.firstNonNull(intermediateCrop, mainCrop);
                    productName_ = crop != null ? crop.getName() : l(getLocale(), "IndicatorWriter.crop");//Culture
                }
            }
            case TRAITEMENT_SEMENCE, LUTTE_BIOLOGIQUE, APPLICATION_DE_PRODUITS_PHYTOSANITAIRES -> {
                AbstractPhytoProductInputUsage phytoProductInput = (AbstractPhytoProductInputUsage) inputUsage;
                final DomainPhytoProductInput domainPhytoProductInput = phytoProductInput.getDomainPhytoProductInput();
                String productName = domainPhytoProductInput.getInputName();
                final RefActaTraitementsProduit refInput = domainPhytoProductInput.getRefInput();

                StringBuilder builder = new StringBuilder();
                if (StringUtils.isNotEmpty(productName)) {
                    builder.append(productName);
                } else if (refInput != null) {
                    // If no productName, use phyto product name
                    builder.append(refInput.getNom_produit());
                } else {
                    builder.append(l(getLocale(), "IndicatorWriter.undefined"));
                }
                if (appendQtAvgAndUnit) {
                    builder.append(" (")
                            .append(phytoProductInput.getQtAvg()).append(" ")
                            .append(AgrosystI18nService.getEnumTraduction(getLocale(), domainPhytoProductInput.getUsageUnit()))
                            .append(")");
                }
                productName_ = builder.toString();
            }
            case EPANDAGES_ORGANIQUES -> {
                OrganicProductInputUsage organicProductInput = (OrganicProductInputUsage) inputUsage;
                final DomainOrganicProductInput domainOrganicProductInput = organicProductInput.getDomainOrganicProductInput();
                String productName = domainOrganicProductInput.getInputName();
                String composition =
                        "N: " + domainOrganicProductInput.getN() +
                                ",P₂O₅: " + domainOrganicProductInput.getP2O5() +
                                ",K₂O: " + domainOrganicProductInput.getK2O() +
                                ",CaO: " + (domainOrganicProductInput.getCaO() != null ? String.valueOf(domainOrganicProductInput.getCaO()) : "") +
                                ",MgO: " + (domainOrganicProductInput.getMgO() != null ? String.valueOf(domainOrganicProductInput.getMgO()) : "") +
                                ",S: " + (domainOrganicProductInput.getS() != null ? String.valueOf(domainOrganicProductInput.getS()) : "");
                if (StringUtils.isEmpty(productName)) {
                    productName_ = composition;
                } else {
                    productName_ = productName + " (" + composition + ")";
                }
            }
            case SUBSTRAT -> {
                SubstrateInputUsage substrateInput = (SubstrateInputUsage) inputUsage;
                final DomainSubstrateInput domainSubstrateInput = substrateInput.getDomainSubstrateInput();
                String productName = domainSubstrateInput.getInputName();
                RefSubstrate refInput = domainSubstrateInput.getRefInput();
                LinkedHashSet<String> caracteristics = new LinkedHashSet<>();
                if (!refInput.getTopiaId().equals(ReferentialService.DEFAULT_REF_POT_ID)) {
                    caracteristics.add(StringUtils.trimToNull(refInput.getCaracteristic1()));
                    caracteristics.add(StringUtils.trimToNull(refInput.getCaracteristic2()));
                }
                caracteristics.add(productName);
                productName_ = caracteristics.stream().filter(Objects::nonNull).collect(Collectors.joining(" - "));
            }
            case POT -> {
                PotInputUsage potInput = (PotInputUsage) inputUsage;
                DomainPotInput domainPotInput = potInput.getDomainPotInput();
                RefPot refInput = domainPotInput.getRefInput();
                String productName = domainPotInput.getInputName();
                LinkedHashSet<String> caracteristics = new LinkedHashSet<>();
                if (!refInput.getTopiaId().equals(ReferentialService.DEFAULT_REF_POT_ID)) {
                    caracteristics.add(StringUtils.trimToNull(refInput.getCaracteristic1()));
                }
                caracteristics.add(productName);
                productName_ = caracteristics.stream().filter(Objects::nonNull).collect(Collectors.joining(" - "));
            }
            // #9313 Ajouter les doses des produits phyto
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                MineralProductInputUsage mineralProductInput = (MineralProductInputUsage) inputUsage;
                String productName = mineralProductInput.getDomainMineralProductInput().getInputName();
                if (StringUtils.isEmpty(productName)) {
                    productName_ = l(getLocale(), "IndicatorWriter.undefined");
                }
                if (appendQtAvgAndUnit) {
                    productName_ = String.format(
                            "%s (%s %s)",
                            productName,
                            mineralProductInput.getQtAvg(),
                            AgrosystI18nService.getEnumTraduction(getLocale(), mineralProductInput.getDomainMineralProductInput().getUsageUnit())
                    );
                }
            }
            case AUTRE -> {
                OtherProductInputUsage otherProductInput = (OtherProductInputUsage) inputUsage;
                LinkedHashSet<String> caracteristics = new LinkedHashSet<>();
                DomainOtherInput domainOtherInput = otherProductInput.getDomainOtherInput();
                String productName = domainOtherInput.getInputName();
                final RefOtherInput refInput1 = domainOtherInput.getRefInput();
                if (refInput1.getTopiaId().equals(ReferentialService.DEFAULT_REF_OTHER_INPUT_ID)) {
                    caracteristics.add(refInput1.getCaracteristic1());
                } else {
                    caracteristics.add(StringUtils.trimToNull(refInput1.getInputType_c0()));
                    caracteristics.add(StringUtils.trimToNull(refInput1.getCaracteristic1()));
                    caracteristics.add(StringUtils.trimToNull(refInput1.getCaracteristic2()));
                    caracteristics.add(StringUtils.trimToNull(refInput1.getCaracteristic3()));
                }
                caracteristics.add(productName);

                productName_ = caracteristics.stream().filter(Objects::nonNull).collect(Collectors.joining(" - "));
            }
            default -> {
            }
        }

        // dans les autres cas, on affiche N/A
        if (StringUtils.isEmpty(productName_)) {
            productName_ = l(getLocale(), "IndicatorWriter.undefined");
        }

        return productName_;
    }

    default Pair<String, String> getInputUsageQtAvgAndUnit(AbstractInputUsage inputUsage) {
        Pair<String, String> qtAvgAndUnit;

        switch (inputUsage.getInputType()) {
            case TRAITEMENT_SEMENCE, LUTTE_BIOLOGIQUE, APPLICATION_DE_PRODUITS_PHYTOSANITAIRES -> {
                final AbstractPhytoProductInputUsage phytoProductInput = (AbstractPhytoProductInputUsage) inputUsage;
                qtAvgAndUnit = Pair.of(
                        phytoProductInput.getQtAvg() != null ? phytoProductInput.getQtAvg().toString() : null,
                        phytoProductInput.getDomainPhytoProductInput() != null ? AgrosystI18nService.getEnumTraduction(getLocale(), phytoProductInput.getDomainPhytoProductInput().getUsageUnit()) : null
                );
            }
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                final MineralProductInputUsage mineralProductInput = (MineralProductInputUsage) inputUsage;
                qtAvgAndUnit = Pair.of(
                        mineralProductInput.getQtAvg() != null ? mineralProductInput.getQtAvg().toString() : null,
                        mineralProductInput.getDomainMineralProductInput() != null ? AgrosystI18nService.getEnumTraduction(getLocale(), mineralProductInput.getDomainMineralProductInput().getUsageUnit()) : null
                );
            }
            default -> qtAvgAndUnit = Pair.of(l(getLocale(), "IndicatorWriter.undefined"),l(getLocale(), "IndicatorWriter.undefined"));
        }

        return qtAvgAndUnit;
    }

    default String getInputUsagesToString(
            Collection<AbstractInputUsage> inputs,
            Collection<AbstractAction> actions,
            CroppingPlanEntry crop) {

        String result = getInputUsagesToString(inputs, actions, crop, null);
        return result;
    }

    default String getInputUsagesToString(
            Collection<AbstractInputUsage> inputs,
            Collection<AbstractAction> actions,
            CroppingPlanEntry mainCrop,
            CroppingPlanEntry intermediateCrop) {

        String result;
        Set<String> inputNames = new HashSet<>();
        if (inputs != null) {
            inputNames.addAll(inputs.stream()
                    .map(input -> getInputUsageProductName(input, null, intermediateCrop, mainCrop, true))
                    .toList());
        } else if (actions != null) {
            inputNames.addAll(actions.stream()
                    .map(action -> getInputUsageProductName(null, action, intermediateCrop, mainCrop, true))
                    .toList());
        }
        result = inputNames
                .stream()
                .filter(StringUtils::isNotBlank)
                .collect(Collectors.joining(SEPARATOR));
        return result;
    }

    default String getGroupesCiblesToString(Collection<AbstractInputUsage> usages, Map<String, String> groupesCiblesByCode) {
        String result = "";
        if (usages != null) {
            Set<PhytoProductTarget> groupCibles = new HashSet<>();
            List<AbstractInputUsage> abstractPhytoProductInputUsages = new HashSet<>(usages)
                    .stream()
                    .filter(usage -> (
                            usage instanceof BiologicalProductInputUsage ||
                                    usage instanceof SeedProductInputUsage ||
                                    usage instanceof PesticideProductInputUsage)).toList();
            abstractPhytoProductInputUsages.forEach(
                    abstractInputUsage -> {
                        Collection<PhytoProductTarget> targets = ((AbstractPhytoProductInputUsage) abstractInputUsage).getTargets();
                        groupCibles.addAll(CollectionUtils.emptyIfNull(targets).stream().filter(target -> target.getCodeGroupeCibleMaa() != null).toList());
                    }
            );

            Set<PhytoProductTarget> seedLotProductUsages = usages.stream()
                    .filter(input -> input instanceof SeedLotInputUsage)
                    .map(sli_ -> ((SeedLotInputUsage) sli_))
                    .map(SeedLotInputUsage::getSeedingSpecies)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .map(SeedSpeciesInputUsage::getSeedProductInputUsages)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .map(SeedProductInputUsage::getTargets)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .filter(target -> target.getCodeGroupeCibleMaa() != null)
                    .collect(Collectors.toSet());
            groupCibles.addAll(seedLotProductUsages);

            result = groupCibles.stream()
                    .map(target -> groupesCiblesByCode.getOrDefault(target.getCodeGroupeCibleMaa(), target.getCodeGroupeCibleMaa()))
                    .collect(Collectors.joining(SEPARATOR));
        }
        return result;
    }

    default String getGroupesCiblesToString(AbstractInputUsage input, Map<String, String> groupesCiblesByCode) {
        return getGroupesCiblesToString(Collections.singleton(input), groupesCiblesByCode);
    }

    default String getInputUsageTargetsToString(Collection<AbstractInputUsage> usages) {
        // get all targets from all inputs
        Set<RefBioAgressor> aggregates = new HashSet<>();
        if (usages != null) {
            for (AbstractInputUsage input : usages) {
                Collection<RefBioAgressor> targetsForInput = getInputUsageTarget(input);
                if (targetsForInput != null) {
                    aggregates.addAll(targetsForInput);
                }
            }
        }

        // join all found targets
        return aggregates.stream().map(RefBioAgressor::getLabel).collect(Collectors.joining(SEPARATOR));
    }

    default String getTargetsToString(AbstractInputUsage input) {
        String result = "";

        if (input == null) return result;// for irrig

        Collection<RefBioAgressor> targetsForInput = getInputUsageTarget(input);
        if (CollectionUtils.isNotEmpty(targetsForInput)) {
            result = targetsForInput.stream().map(RefBioAgressor::getLabel).collect(Collectors.joining(SEPARATOR));
        }
        return result;
    }

    default Collection<RefBioAgressor> getInputUsageTarget(AbstractInputUsage usage) {
        Collection<RefBioAgressor> targets = null;
        if (usage instanceof BiologicalProductInputUsage || usage instanceof SeedProductInputUsage || usage instanceof PesticideProductInputUsage) {
            AbstractPhytoProductInputUsage phytoProductInputUsage = (AbstractPhytoProductInputUsage) usage;
            if (CollectionUtils.isNotEmpty(phytoProductInputUsage.getTargets())) {
                targets = phytoProductInputUsage.getTargets().stream()
                        .map(PhytoProductTarget::getTarget)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toSet());
            }
        } else if(usage instanceof SeedLotInputUsage seedLotInputUsage) {
            targets = CollectionUtils.emptyIfNull(seedLotInputUsage.getSeedingSpecies()).stream()
                    .filter(Objects::nonNull)
                    .map(SeedSpeciesInputUsage::getSeedProductInputUsages)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .map(SeedProductInputUsage::getTargets)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .map(PhytoProductTarget::getTarget)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
        }
        return targets;
    }

    default String getNbInputUsages(Collection<AbstractInputUsage> inputs) {
        int nbInput = CollectionUtils.emptyIfNull(inputs).size();
        return Integer.toString(nbInput);
    }

    default String getCropSpeciesName(CroppingPlanEntry croppingPlanEntry) {
        String speciesNames = "";
        if (croppingPlanEntry != null) {
            speciesNames = FormatUtils.getSpeciesName(croppingPlanEntry.getCroppingPlanSpecies(), SEPARATOR);
        }
        return speciesNames;
    }

    default String getCropVarietyName(CroppingPlanEntry croppingPlanEntry) {
        String varietyName = "";
        if (croppingPlanEntry != null) {
            varietyName = FormatUtils.getVarietyName(croppingPlanEntry.getCroppingPlanSpecies(), SEPARATOR);
        }
        return varietyName;
    }

    default String getInterventionSpeciesName(EffectiveIntervention intervention) {
        String speciesNames = "";
        if (CollectionUtils.isNotEmpty(intervention.getSpeciesStades())) {
            Set<CroppingPlanSpecies> interventionSpecies = intervention.getSpeciesStades().stream()
                    .map(EffectiveSpeciesStade::getCroppingPlanSpecies)
                    .collect(Collectors.toSet());
            speciesNames = FormatUtils.getSpeciesName(interventionSpecies, SEPARATOR);
        }
        return speciesNames;
    }

    default String getInterventionSpeciesName(CroppingPlanEntry croppingPlanEntry, PracticedIntervention intervention) {
        Set<String> interventionSpeciesCode = CollectionUtils.emptyIfNull(intervention.getSpeciesStades()).stream()
                .map(PracticedSpeciesStade::getSpeciesCode)
                .collect(Collectors.toSet());
        return FormatUtils.getInterventionSpeciesName(croppingPlanEntry, interventionSpeciesCode, SEPARATOR);
    }

    default String getInterventionVarietyName(EffectiveIntervention intervention) {
        String varietyName = "";
        if (CollectionUtils.isNotEmpty(intervention.getSpeciesStades())) {
            Set<CroppingPlanSpecies> interventionSpecies = CollectionUtils.emptyIfNull(intervention.getSpeciesStades()).stream()
                    .map(EffectiveSpeciesStade::getCroppingPlanSpecies)
                    .collect(Collectors.toSet());
            varietyName = FormatUtils.getVarietyName(interventionSpecies, SEPARATOR);
        }
        return varietyName;
    }

    default String getInterventionVarietyName(CroppingPlanEntry croppingPlanEntry, PracticedIntervention intervention) {
        Set<String> interventionSpeciesCode = CollectionUtils.emptyIfNull(intervention.getSpeciesStades()).stream()
                .map(PracticedSpeciesStade::getSpeciesCode)
                .collect(Collectors.toSet());
        return FormatUtils.getInterventionVarietiesName(croppingPlanEntry, interventionSpeciesCode, SEPARATOR);
    }



    /**
     * Récupérer le nom de l'espèce et de la variété à partir de l'usage s'il existe, sinon à partir de
     * l'instance de CroppingPlanEntry fournie.
     *
     * @return une paire avec à gauche, le nom de l'espèce et à droite le nom de la variété quand il existe, null sinon.
     */
    default Pair<String, String> getSpeciesAndVarietyName(AbstractInputUsage usage, AbstractAction action, CroppingPlanEntry croppingPlanEntry) {
        String speciesNames;
        String varietyNames = null;
        CroppingPlanSpecies croppingPlanSpecies = null;

        // Quand c'est possible, récupérer le nom de l'espèce du lot de semences concerné par l'action ou l'usage.
        if (usage instanceof SeedSpeciesInputUsage seedSpeciesInputUsage) {
            croppingPlanSpecies = seedSpeciesInputUsage.getDomainSeedSpeciesInput().getSpeciesSeed();
        } else if (action instanceof SeedingActionUsage seedingAction) {
            final Optional<SeedSpeciesInputUsage> maybeSeedSpeciesInputUsage = seedingAction.getSeedLotInputUsage()
                    .stream()
                    .flatMap(seedLotInputUsage -> seedLotInputUsage.getSeedingSpecies().stream())
                    .filter(seedingSpecies ->
                            seedingSpecies.getSeedProductInputUsages()
                                    .stream()
                                    .anyMatch(seedProductInputUsage -> seedProductInputUsage.equals(usage))
                    )
                    .findFirst();

            if (maybeSeedSpeciesInputUsage.isPresent()) {
                SeedSpeciesInputUsage seedSpeciesInputUsage = maybeSeedSpeciesInputUsage.get();
                croppingPlanSpecies = seedSpeciesInputUsage.getDomainSeedSpeciesInput().getSpeciesSeed();
            }
        }

        if (croppingPlanSpecies != null) {
            RefEspece espece = croppingPlanSpecies.getSpecies();
            RefVariete variete = croppingPlanSpecies.getVariety();

            speciesNames = espece.getLibelle_espece_botanique_Translated();
            if (variete != null) {
                varietyNames = variete.getLabel();
            }
        } else {
            // Récupération du nom de l'espèce et de la variété du lot de semences. Mécanisme par défaut.
            speciesNames = getCropSpeciesName(croppingPlanEntry);
            varietyNames = getCropVarietyName(croppingPlanEntry);
        }

        return Pair.of(speciesNames, varietyNames);
    }

    default String getDomainName(Domain domain) {
        return domain != null ? domain.getName() + " (" + domain.getCampaign() + ")" : "";
    }

    default String getGrowingSystemName(Optional<GrowingSystem> optionalGrowingSystem) {
        return optionalGrowingSystem.map(GrowingSystem::getName)
                .orElse(l(getLocale(), "IndicatorWriter.none"));
    }

    default String getApprovedGrowingSystem(Optional<GrowingSystem> optionalGrowingSystem) {
        return optionalGrowingSystem.map(growingSystem -> getBooleanAsString(growingSystem.isValidated()))
                .orElse("");
    }

    default String geDephyNumber(Optional<GrowingSystem> optionalGrowingSystem) {
        return optionalGrowingSystem.map(GrowingSystem::getDephyNumber)
                .orElse("");
    }

    default String getTypeDeConduiteSDC(Optional<GrowingSystem> optionalGrowingSystem) {
        return optionalGrowingSystem.map(GrowingSystem::getTypeAgriculture)
                .map(RefTypeAgriculture::getReference_label)
                .orElse("");
    }

    default Boolean isOneInputUsageBiocontrole(Collection<AbstractInputUsage> inputUsages, Collection<String> codeAmmBioControle) {
        return inputUsages != null && inputUsages.stream().anyMatch(input -> {
            final Optional<Boolean> maybeInputUsageBiocontrole = isInputUsageBiocontrole(input, codeAmmBioControle);
            return maybeInputUsageBiocontrole.isPresent() && maybeInputUsageBiocontrole.get();
        });
    }

    /**
     * Dans le cas où on n'a pas à calculer la valeur de biocontrole, un Optional vide est retourné.
     */
    default Optional<Boolean> isInputUsageBiocontrole(AbstractInputUsage inputUsage, Collection<String> codeAmmBioControle) {
        Boolean result = null;
        if (inputUsage instanceof AbstractPhytoProductInputUsage) {
            RefActaTraitementsProduit phytoProduct = ((AbstractPhytoProductInputUsage) inputUsage).getDomainPhytoProductInput().getRefInput();
            if (phytoProduct != null) {
                result = phytoProduct.isNodu() || codeAmmBioControle.contains(phytoProduct.getCode_AMM());
            }
        }
        return Optional.ofNullable(result);
    }

    default String getBooleanAsString(boolean b) {
        return l(getLocale(), b ? "IndicatorWriter.yes" : "IndicatorWriter.no").toUpperCase();
    }

    default Locale getLocale() {
        return Locale.FRANCE;
    }
}
