package fr.inra.agrosyst.services.security;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingPlanTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.NetworkTopiaDao;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.entities.security.AgrosystUserTopiaDao;
import fr.inra.agrosyst.api.entities.security.ComputedUserPermission;
import fr.inra.agrosyst.api.entities.security.ComputedUserPermissionTopiaDao;
import fr.inra.agrosyst.api.entities.security.PermissionObjectType;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.entities.security.UserRole;
import fr.inra.agrosyst.api.entities.security.UserRoleTopiaDao;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanFilter;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.network.NetworkFilter;
import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.api.services.referential.ImportResult;
import fr.inra.agrosyst.api.services.security.AuthorizationService;
import fr.inra.agrosyst.api.services.security.UserRoleDto;
import fr.inra.agrosyst.api.services.security.UserRoleEntityDto;
import fr.inra.agrosyst.api.services.users.UserDto;
import fr.inra.agrosyst.api.services.users.Users;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.CacheDiscriminator;
import fr.inra.agrosyst.services.common.CacheService;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;
import org.nuiton.util.pagination.PaginationResult;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Setter
public class AuthorizationServiceImpl extends AbstractAgrosystService implements AuthorizationService {

    private static final Log LOGGER = LogFactory.getLog(AuthorizationServiceImpl.class);
    public static final String N_C = "n/c";

    protected static String toDomainEntityLabel(Domain domain) {
        if (domain == null) {
            return N_C;
        }
        return String.format("%s (%s)", domain.getName(), domain.getLocation().getCodePostal());
    }

    protected static String toGrowingPlanEntityLabel(GrowingPlan growingPlan) {
        if (growingPlan == null) {
            return N_C;
        }
        return String.format("%s (%s)", growingPlan.getName(), growingPlan.getDomain().getName());
    }

    protected static String toGrowingSystemEntityLabel(GrowingSystem growingSystem) {
        if (growingSystem == null) {
            return N_C;
        }
        return String.format("%s (%s)", growingSystem.getName(), growingSystem.getGrowingPlan().getDomain().getName());
    }

    protected static UserRoleEntityDto toDomainUserRoleEntityDto(Domain input) {
            UserRoleEntityDto result = new UserRoleEntityDto();
            result.setIdentifier(input.getCode());
            result.setLabel(toDomainEntityLabel(input));
            return result;
    }

    protected static UserRoleEntityDto toGrowingSystemUserRoleEntityDto(GrowingSystem input) {
        UserRoleEntityDto result = new UserRoleEntityDto();
        result.setIdentifier(input.getCode());
        result.setLabel(toGrowingSystemEntityLabel(input));
        return result;
    }

    protected static UserRoleEntityDto toGrowingPlanUserRoleEntityDto(GrowingPlan input) {
        UserRoleEntityDto result = new UserRoleEntityDto();
        result.setIdentifier(input.getCode());
        result.setLabel(toGrowingPlanEntityLabel(input));
        return result;
    }

    protected static UserRoleEntityDto toNetworkUserRoleEntityDto(Network input) {
        UserRoleEntityDto result = new UserRoleEntityDto();
        result.setIdentifier(input.getTopiaId());
        result.setLabel(input.getName());
        return result;
    }

    protected GrowingSystemService growingSystemService;
    protected DomainService domainService;
    protected NetworkService networkService;
    protected GrowingPlanService growingPlanService;
    protected TrackerServiceImpl trackerService;
    protected CacheService cacheService;

    protected UserRoleTopiaDao userRoleDao;
    protected AgrosystUserTopiaDao agrosystUserDao;
    protected ComputedUserPermissionTopiaDao computedUserPermissionDao;
    protected NetworkTopiaDao networkDao;
    protected DomainTopiaDao domainDao;
    protected GrowingPlanTopiaDao growingPlanDao;
    protected GrowingSystemTopiaDao growingSystemDao;

    protected String getUserIdAndComputePermissions() {
        String userId = getSecurityContext().getAuthenticatedUserId();
        String result = checkComputedPermissionsFromUserId(userId);
        return result;
    }
    
    protected String getUserId() {
        String result = getSecurityContext().getAuthenticatedUserId();
        return result;
    }

    protected boolean hasRoleFromUserId(final String userId, final RoleType roleType) {

        Callable<Boolean> loader = () -> {
            UserRole role = userRoleDao.findRole(userId, roleType);
            return role != null;
        };

        return cacheService.get(CacheDiscriminator.hasRole(), userId + "_" + roleType, loader);
    }

    public boolean isAdmin() {
        boolean result = getSecurityContext().isAdmin();
        return result;
    }

    @Override
    public boolean isAdminFromUserId(String userId) {
        boolean result = hasRoleFromUserId(userId, RoleType.ADMIN);
        return result;
    }

    public boolean isIsDataProcessor() {
        String userId = getSecurityContext().getAuthenticatedUserId();
        boolean result = isIsDataProcessorFromUserId(userId);
        return result;
    }

    @Override
    public boolean isIsDataProcessorFromUserId(String userId) {
        boolean result = hasRoleFromUserId(userId, RoleType.IS_DATA_PROCESSOR);
        return result;
    }

    @Override
    public void dropComputedPermissionsFromUserId(String userId) {

        dropComputedPermissions0(userId);

        getTransaction().commit();
    }

    protected void dropComputedPermissions0(String userId) {
        List<ComputedUserPermission> permissions = computedUserPermissionDao.forUserIdEquals(userId).findAll();
        if (CollectionUtils.isNotEmpty(permissions)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format("%d permission(s) are about to be deleted for user '%s'", permissions.size(), userId));
            }
            computedUserPermissionDao.deleteAll(permissions);
        }
    }

    public String checkComputedPermissionsFromUserId(String userId, String doAsUserId) {
        if (StringUtils.isNotEmpty(doAsUserId) && isAdminFromUserId(userId)) {
            return checkComputedPermissionsFromUserId(doAsUserId);
        } else {
            return checkComputedPermissionsFromUserId(userId);
        }
    }

    public String checkComputedPermissionsFromUserId(String userId) {
        long start = System.currentTimeMillis();
        int newPermissionsCount = 0;
        
        // permissions has to be reloaded if there is no permission or if one of them is dirty
        final long usersPermissionCount = computedUserPermissionDao.getUsersPermissionCount(userId);
        final long usersDirtyCount = computedUserPermissionDao.getUsersDirtyCount(userId);
        
        if (usersPermissionCount == 0L || usersDirtyCount >= 1L) {

            if (usersPermissionCount >= 1L) {
                if (LOGGER.isInfoEnabled()) {
                    LOGGER.info(String.format("for user '%s', %d permission(s) are about to be deleted, %d are dirty and will be recomputed", userId, usersPermissionCount, usersDirtyCount));
                }
                dropComputedPermissions0(userId);
            }

            long computeStart = System.currentTimeMillis();
            Collection<ComputedUserPermission> permissions = computeUserPermissions(userId);
            newPermissionsCount = permissions.size();

            if (usersPermissionCount > 0L  || newPermissionsCount > 0L) {
                long persistStart = System.currentTimeMillis();
                //TODO DCosse optimiser ce code pour éviter une création unitaire
                computedUserPermissionDao.createAll(permissions);

                if (LOGGER.isInfoEnabled()) {
                    long persistEnd = System.currentTimeMillis();
                    String message = String.format(
                            "for user '%s', %d permission(s) have been created (compute=%dms ; persist=%dms)",
                            userId,
                            newPermissionsCount,
                            persistStart - computeStart,
                            persistEnd - persistStart);
                    LOGGER.info(message);
                }

                getTransaction().commit();
            }
        }

        if (LOGGER.isTraceEnabled()) {
            long duration = System.currentTimeMillis() - start;
            String text = "User's permissions computation took %dms. %d new permission(s) created.";
            String message = String.format(text, duration, newPermissionsCount);
            LOGGER.trace(message);
        }

        return userId;
    }

    protected Function<UserRole, UserRoleDto> getUserRoleToDtoFunction(final boolean includeEntity, final boolean includeUser) {

        return input -> {
            UserRoleDto result = new UserRoleDto();
            result.setTopiaId(input.getTopiaId());
            result.setType(input.getType());

            if (includeEntity) {
                if (!RoleType.IS_DATA_PROCESSOR.equals(input.getType()) && !RoleType.ADMIN.equals(input.getType())) {
                    UserRoleEntityDto entityDto = new UserRoleEntityDto();
                    result.setEntity(entityDto);

                    String identifier;
                    String label;
                    Integer campaign = null;
    
                    switch (input.getType()) {
                        case NETWORK_RESPONSIBLE, NETWORK_SUPERVISOR -> {
                            identifier = input.getNetworkId();
                            label = networkDao.forTopiaIdEquals(identifier).findUnique().getName();
                        }
                        case DOMAIN_RESPONSIBLE -> {
                            identifier = input.getDomainCode();
                            Domain domain = domainDao.forCodeEquals(identifier).findAny();
                            label = toDomainEntityLabel(domain);
                        }
                        case GROWING_PLAN_RESPONSIBLE -> {
                            identifier = input.getGrowingPlanCode();
                            GrowingPlan growingPlan = growingPlanDao.forCodeEquals(identifier).findAny();
                            label = toGrowingPlanEntityLabel(growingPlan);
                        }
                        case GS_DATA_PROCESSOR -> {
                            GrowingSystem growingSystem;
                            if (StringUtils.isNotEmpty(input.getGrowingSystemId())) {
                                growingSystem = growingSystemDao.forTopiaIdEquals(input.getGrowingSystemId()).findUnique();
                                campaign = growingSystem.getGrowingPlan().getDomain().getCampaign();
                            } else {
                                growingSystem = growingSystemDao.forCodeEquals(input.getGrowingSystemCode()).findAny();
                            }
                            identifier = growingSystem.getCode();
                            label = toGrowingSystemEntityLabel(growingSystem);
                        }
                        default -> throw new UnsupportedOperationException("Not supposed to happen");
                    }

                    entityDto.setIdentifier(identifier);
                    entityDto.setLabel(label);
                    entityDto.setCampaign(campaign);
                }
            } else if (RoleType.GS_DATA_PROCESSOR.equals(input.getType())) {
                // C'est un cas particulier : On ne demande pas l'entité mais il faut malgré tout indiquer la campagne
                if (StringUtils.isNotEmpty(input.getGrowingSystemId())) {
                    UserRoleEntityDto entityDto = new UserRoleEntityDto();
                    GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(input.getGrowingSystemId()).findUnique();
                    Integer campaign = growingSystem.getGrowingPlan().getDomain().getCampaign();
                    entityDto.setCampaign(campaign);
                    result.setEntity(entityDto);
                }
            }

            if (includeUser) {
                AgrosystUser user = input.getAgrosystUser();
                UserDto userDto = Users.TO_USER_DTO.apply(user);
                result.setUser(userDto);
            }

            return result;

        };
    }

    @Override
    public List<UserRoleDto> getUserRoles(String userId) {
        List<UserRole> roles = userRoleDao.findAllForUserId(userId);
        return roles.stream()
                .map(getUserRoleToDtoFunction(true, false))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserRoleDto> getEntityRoles(RoleType roleType, String entityCode) {
        List<UserRole> roles = getUserRoles0(roleType, entityCode);
        return roles.stream()
                .map(getUserRoleToDtoFunction(false, true))
                .collect(Collectors.toList());
    }

    protected List<UserRole> getUserRoles0(RoleType roleType, String entityCode) {
        Map<String, Object> properties = new HashMap<>();
        properties.put(UserRole.PROPERTY_TYPE, roleType);
        switch (roleType) {
            case DOMAIN_RESPONSIBLE -> properties.put(UserRole.PROPERTY_DOMAIN_CODE, entityCode);
            case GROWING_PLAN_RESPONSIBLE -> properties.put(UserRole.PROPERTY_GROWING_PLAN_CODE, entityCode);
            case GS_DATA_PROCESSOR -> properties.put(UserRole.PROPERTY_GROWING_SYSTEM_CODE, entityCode);
            default -> throw new UnsupportedOperationException("Unexcpected role type: " + roleType);
        }

        List<UserRole> roles = userRoleDao.forProperties(properties).findAll();

        // Cas particulier des exploitants SdC où il se peut que seule la propriété growingSystemId soir valuée
        if (RoleType.GS_DATA_PROCESSOR.equals(roleType)) {
            // On recherche tous les SdC avec le code et pour chacun d'entre eux, on prend les rôles associés
            List<String> relatedGrowingSystemIds = growingSystemDao.forCodeEquals(entityCode).findAllIds();
            List<UserRole> gsCampaignRoles = userRoleDao.newQueryBuilder().
                    addEquals(UserRole.PROPERTY_TYPE, RoleType.GS_DATA_PROCESSOR).
                    addIn(UserRole.PROPERTY_GROWING_SYSTEM_ID, relatedGrowingSystemIds)
                    .findAll();
            roles.addAll(gsCampaignRoles);
        }
        return roles;
    }

    @Override
    public List<UserRoleEntityDto> searchPossibleEntities(RoleType roleType, String termRaw) {
        String term = StringUtils.defaultString(termRaw);
        // TODO AThimel 03/10/13 Add security filtering according to user permissions
        List<UserRoleEntityDto> dtos;
        switch (roleType) {
            case ADMIN, IS_DATA_PROCESSOR -> dtos = new LinkedList<>();
            case DOMAIN_RESPONSIBLE -> {
                DomainFilter domainFilter = new DomainFilter();
                domainFilter.setDomainName(term);
                domainFilter.setAllPageSize();
                PaginationResult<Domain> filteredDomains = domainService.getFilteredDomains(domainFilter);
                dtos = filteredDomains.getElements().stream()
                        .map(AuthorizationServiceImpl::toDomainUserRoleEntityDto)
                        .collect(Collectors.toList());
            }
            case NETWORK_RESPONSIBLE, NETWORK_SUPERVISOR -> {
                NetworkFilter networkFilter = new NetworkFilter();
                networkFilter.setNetworkName(term);
                networkFilter.setAllPageSize();
                PaginationResult<Network> filteredNetworks = networkService.getFilteredNetworks(networkFilter);
                dtos = filteredNetworks.getElements().stream()
                        .map(AuthorizationServiceImpl::toNetworkUserRoleEntityDto)
                        .collect(Collectors.toList());
            }
            case GROWING_PLAN_RESPONSIBLE -> {
                GrowingPlanFilter growingPlanFilter = new GrowingPlanFilter();
                growingPlanFilter.setGrowingPlanName(term);
                growingPlanFilter.setAllPageSize();
                PaginationResult<GrowingPlan> filteredGrowingPlans = growingPlanService.getFilteredGrowingPlans(growingPlanFilter);
                dtos = filteredGrowingPlans.getElements().stream()
                        .map(AuthorizationServiceImpl::toGrowingPlanUserRoleEntityDto)
                        .collect(Collectors.toList());
            }
            case GS_DATA_PROCESSOR -> {
                GrowingSystemFilter growingSystemFilter = new GrowingSystemFilter();
                growingSystemFilter.setGrowingSystemName(term);
                growingSystemFilter.setAllPageSize();
                PaginationResult<GrowingSystem> filteredGrowingSystems = growingSystemService.getFilteredGrowingSystems(growingSystemFilter);
                dtos = filteredGrowingSystems.getElements().stream()
                        .map(AuthorizationServiceImpl::toGrowingSystemUserRoleEntityDto)
                        .collect(Collectors.toList());
            }
            default -> throw new UnsupportedOperationException("Unexpected type: " + roleType);
        }

        Multimap<String, UserRoleEntityDto> index = Multimaps.index(dtos, UserRoleEntityDto::getIdentifier);

        Set<String> keySet = index.keySet();
        List<UserRoleEntityDto> result = new ArrayList<>(keySet.size());

        for (String identifier : keySet) {
            Collection<UserRoleEntityDto> identifierEntities = index.get(identifier);
            UserRoleEntityDto entity = Iterables.getFirst(identifierEntities, null);
            result.add(entity);
        }
        return result;
    }

    @Override
    public List<UserRoleEntityDto> searchEntities(RoleType roleType, String termRaw, Integer campaign) {
        // TODO AThimel 03/10/13 Add security filtering according to user permissions
        List<UserRoleEntityDto> dtos;
        switch (roleType) {
            case ADMIN, IS_DATA_PROCESSOR -> dtos = new LinkedList<>();
            case DOMAIN_RESPONSIBLE -> {
                List<Domain> filteredDomains = domainService.getDomainWithName(termRaw);
                dtos = filteredDomains.stream()
                        .map(AuthorizationServiceImpl::toDomainUserRoleEntityDto)
                        .collect(Collectors.toList());
            }
            case NETWORK_RESPONSIBLE, NETWORK_SUPERVISOR -> {
                List<Network> filteredNetworks = networkService.getNetworkWithName(termRaw);
                dtos = filteredNetworks.stream()
                        .map(AuthorizationServiceImpl::toNetworkUserRoleEntityDto)
                        .collect(Collectors.toList());
            }
            case GROWING_PLAN_RESPONSIBLE -> {
                List<GrowingPlan> filteredGrowingPlans = growingPlanService.getGrowingPlanWithName(termRaw);
                dtos = filteredGrowingPlans.stream()
                        .map(AuthorizationServiceImpl::toGrowingPlanUserRoleEntityDto)
                        .collect(Collectors.toList());
            }
            case GS_DATA_PROCESSOR -> {
                List<GrowingSystem> filteredGrowingSystems = growingSystemService.getGrowingSystemWithNameAndCampaign(termRaw, campaign);
                // all growing systems must have same code.
                Map<String, GrowingSystem> growingSystemByCodes = new HashMap<>();
                for (GrowingSystem growingSystem : filteredGrowingSystems) {
                    growingSystemByCodes.putIfAbsent(growingSystem.getCode(), growingSystem);
                }
                if (growingSystemByCodes.size() == 1) {
                    dtos = growingSystemByCodes.values().stream()
                            .map(AuthorizationServiceImpl::toGrowingSystemUserRoleEntityDto)
                            .collect(Collectors.toList());
                } else {
                    dtos = new LinkedList<>();
                }
            }
            default -> throw new UnsupportedOperationException("Unexpected type: " + roleType);
        }

        Multimap<String, UserRoleEntityDto> index = Multimaps.index(dtos, UserRoleEntityDto::getIdentifier);

        Set<String> keySet = index.keySet();
        List<UserRoleEntityDto> result = new ArrayList<>(keySet.size());

        for (String identifier : keySet) {
            Collection<UserRoleEntityDto> identifierEntities = index.get(identifier);
            UserRoleEntityDto entity = Iterables.getFirst(identifierEntities, null);
            result.add(entity);
        }
        return result;
    }

    @Override
    public void saveUserRoles(String userId, List<UserRoleDto> userRoles) {
        createOrUpdateUserRoles(Collections.singleton(userId), userRoles, true);
    }

    @Override
    public void addUserRoles(String userId, List<UserRoleDto> userRoles) {
        createOrUpdateUserRoles(Collections.singleton(userId), userRoles, false);
    }

    protected void createOrUpdateUserRoles(Collection<String> userIds, List<UserRoleDto> assignedRoles, boolean removePreviousRoles) {

        for (String userId : userIds) {
            List<UserRole> roles = userRoleDao.findAllForUserId(userId);
            AgrosystUser agrosystUser = agrosystUserDao.forTopiaIdEquals(userId).findUnique();

            ImmutableMap<String, UserRole> rolesIndex = Maps.uniqueIndex(roles, Entities.GET_TOPIA_ID::apply);
            Set<String> rolesIds = new HashSet<>(rolesIndex.keySet());

            List<UserRoleDto> userRoles = filterDuplicatedNewRoles(roles, assignedRoles);
            doCreateOrUpdateUserRoles(userRoles, agrosystUser, rolesIndex, rolesIds);

            if (removePreviousRoles) {
                removePreviousRoles(rolesIndex, rolesIds);
            }

            dropComputedPermissions0(userId);
        }

        getTransaction().commit();

        clearSingleCacheAndSync(CacheDiscriminator.hasRole());
    }

    protected List<UserRoleDto> filterDuplicatedNewRoles(List<UserRole> currentRoles, List<UserRoleDto> assignedRoles) {
        Set<String> currentStringifiedRoles = currentRoles.stream()
                .map(getUserRoleToDtoFunction(true, false))
                .map(dto -> {
                    dto.setTopiaId(null);
                    return dto.toString();
                }).collect(Collectors.toSet());

        List<UserRoleDto> result = assignedRoles.stream()
                .filter(role -> {
                    if (StringUtils.isEmpty(role.getTopiaId())) {
                        // Si nouveau role, vérification qu'il ne s'agit pas d'un doublon
                        return !currentStringifiedRoles.contains(role.toString());
                    } else {
                        return true;
                    }
                }).collect(Collectors.toList());
        return result;
    }

    protected void removePreviousRoles(ImmutableMap<String, UserRole> rolesIndex, Set<String> rolesIds) {
        for (String rolesId : rolesIds) {
            UserRole userRole = rolesIndex.get(rolesId);
            userRoleDao.delete(userRole);
            trackerService.roleRemoved(userRole);
        }
    }

    protected void doCreateOrUpdateUserRoles(List<UserRoleDto> userRoles,
                                             AgrosystUser agrosystUser,
                                             ImmutableMap<String, UserRole> rolesIndex,
                                             Set<String> rolesIds) {
        if (userRoles != null) {
            for (UserRoleDto userRole : userRoles) {
                String roleId = userRole.getTopiaId();
                UserRole roleEntity;
                if (StringUtils.isEmpty(roleId)) {
                    roleEntity = userRoleDao.newInstance();
                    roleEntity.setAgrosystUser(agrosystUser);
                } else {
                    roleEntity = rolesIndex.get(roleId);
                    Preconditions.checkState(roleEntity.getAgrosystUser().equals(agrosystUser));
                    rolesIds.remove(roleId);
                }

                String identifier = null;
                if (userRole.getEntity() != null) {
                    identifier = userRole.getEntity().getIdentifier();
                }
                RoleType type = userRole.getType();
                roleEntity.setType(type);
                switch (type) {
                    case ADMIN:
                    case IS_DATA_PROCESSOR:
                        break;
                    case DOMAIN_RESPONSIBLE:
                        Preconditions.checkState(identifier != null);
                        roleEntity.setDomainCode(identifier);
                        break;
                    case NETWORK_RESPONSIBLE:
                    case NETWORK_SUPERVISOR:
                        Preconditions.checkState(identifier != null);
                        roleEntity.setNetworkId(identifier);
                        break;
                    case GROWING_PLAN_RESPONSIBLE:
                        Preconditions.checkState(identifier != null);
                        roleEntity.setGrowingPlanCode(identifier);
                        break;
                    case GS_DATA_PROCESSOR:
                        Preconditions.checkState(identifier != null);
                        Integer campaign = userRole.getEntity().getCampaign();
                        if (campaign == null) {
                            roleEntity.setGrowingSystemCode(identifier);
                            roleEntity.setGrowingSystemId(null);
                        } else {
                            Set<Integer> set = Collections.singleton(campaign);
                            List<GrowingSystem> list = growingSystemDao.findAllByCodeAndCampaign(identifier, set);
                            GrowingSystem growingSystem = list.iterator().next();
                            roleEntity.setGrowingSystemCode(null);
                            roleEntity.setGrowingSystemId(growingSystem.getTopiaId());
                        }
                        break;
                    default:
                        throw new UnsupportedOperationException("Unexpected type: " + type);
                }

                if (StringUtils.isEmpty(roleId)) {
                    userRoleDao.create(roleEntity);
                    trackerService.roleAdded(roleEntity);
                } else {
                    userRoleDao.update(roleEntity);
                }
            }
        }
    }

    @Override
    public void saveEntityUserRoles(RoleType roleType, String entityCode, List<UserRoleDto> roleDtos) {

        List<UserRole> roles = getUserRoles0(roleType, entityCode);

        ImmutableMap<String, UserRole> rolesIndex = Maps.uniqueIndex(roles, Entities.GET_TOPIA_ID::apply);
        Set<String> rolesIds = new HashSet<>(rolesIndex.keySet());

        for (UserRoleDto userRole : roleDtos) {
            String userId = userRole.getUser().getTopiaId();
            AgrosystUser user = agrosystUserDao.forTopiaIdEquals(userId).findUnique();

            String roleId = userRole.getTopiaId();
            UserRole roleEntity;
            if (StringUtils.isEmpty(roleId)) {
                roleEntity = userRoleDao.newInstance();
            } else {
                roleEntity = rolesIndex.get(roleId);
                rolesIds.remove(roleId);
            }
            roleEntity.setAgrosystUser(user);

            RoleType type = userRole.getType();
            roleEntity.setType(type);
            switch (type) {
                case DOMAIN_RESPONSIBLE -> roleEntity.setDomainCode(entityCode);
                case GROWING_PLAN_RESPONSIBLE -> roleEntity.setGrowingPlanCode(entityCode);
                case GS_DATA_PROCESSOR -> {
                    Integer campaign = null;
                    if (userRole.getEntity() != null) { // Entity may be null if no campaign is specified
                        campaign = userRole.getEntity().getCampaign();
                    }
                    if (campaign == null) {
                        roleEntity.setGrowingSystemCode(entityCode);
                        roleEntity.setGrowingSystemId(null);
                    } else {
                        Set<Integer> set = Collections.singleton(campaign);
                        List<GrowingSystem> list = growingSystemDao.findAllByCodeAndCampaign(entityCode, set);
                        GrowingSystem growingSystem = list.iterator().next();
                        roleEntity.setGrowingSystemCode(null);
                        roleEntity.setGrowingSystemId(growingSystem.getTopiaId());
                    }
                }
                default -> throw new UnsupportedOperationException("Unexpected type: " + type);
            }
            if (StringUtils.isEmpty(roleId)) {
                userRoleDao.create(roleEntity);
                trackerService.roleAdded(roleEntity);
            } else {
                userRoleDao.update(roleEntity);
            }

            dropComputedPermissions0(userId);
        }

        for (String rolesId : rolesIds) {
            UserRole userRole = rolesIndex.get(rolesId);
            userRoleDao.delete(userRole);
            trackerService.roleRemoved(userRole);
        }

        getTransaction().commit();

        clearSingleCacheAndSync(CacheDiscriminator.hasRole());
    }

    @Override
    public List<UserDto> getDomainResponsibles(String domainCode) {
        List<AgrosystUser> users = userRoleDao.findAllRoleUsers(
                RoleType.DOMAIN_RESPONSIBLE, UserRole.PROPERTY_DOMAIN_CODE, domainCode);
        return users.stream().map(Users.TO_USER_DTO).collect(Collectors.toList());
    }

    @Override
    public List<UserDto> getGrowingPlanResponsibles(String growingPlanCode) {
        List<AgrosystUser> users = userRoleDao.findAllRoleUsers(
                RoleType.GROWING_PLAN_RESPONSIBLE, UserRole.PROPERTY_GROWING_PLAN_CODE, growingPlanCode);
        return users.stream().map(Users.TO_USER_DTO).collect(Collectors.toList());
    }

    protected Set<ComputedUserPermission> computeUserPermissions(String userId) {

        long begin = System.currentTimeMillis();

        Set<ComputedUserPermission> result = new LinkedHashSet<>();

        List<UserRole> roles = userRoleDao.findAllForUserId(userId);

        if (roles != null) {
            
            boolean isAdmin = roles.stream().anyMatch(r -> RoleType.ADMIN == r.getType());
            
            if (isAdmin) return result;// DCosse 25/02/2020 No permission to generate
    
            boolean isDataProcessor = roles.stream().anyMatch(r -> RoleType.IS_DATA_PROCESSOR == r.getType());

            if (isDataProcessor) {
                List<ComputedUserPermission> permissions = computeIsDataProcessorPermissions(userId);
                result.addAll(permissions);
            }

            // On regroupe les UserRole par type de rôle
            Multimap<RoleType, UserRole> rolesByTypeIndex = Multimaps.index(roles, UserRole::getType);

            for (Map.Entry<RoleType, Collection<UserRole>> entry : rolesByTypeIndex.asMap().entrySet()) {

                RoleType roleType = entry.getKey();

                switch (roleType) {
                    case ADMIN:
                        // AThimel 26/09/13 No permission to generate
                        break;
                    case IS_DATA_PROCESSOR: {
                        // DCosse 25/02/2020 compute upper
                        break;
                    }
                    case DOMAIN_RESPONSIBLE: {
                        List<ComputedUserPermission> permissions = computeDomainResponsiblePermissions(userId, entry.getValue());
                        result.addAll(permissions);
                        break;
                    }
                    case GROWING_PLAN_RESPONSIBLE: {
                        List<ComputedUserPermission> permissions = computeGrowingPlanResponsiblePermissions(userId, entry.getValue());
                        result.addAll(permissions);
                        break;
                    }
                    case GS_DATA_PROCESSOR: {
                        for (UserRole role : entry.getValue()) {
                            List<ComputedUserPermission> permissions = computeGrowingSystemDataProcessorPermissions(userId, role);
                            result.addAll(permissions);
                        }
                        break;
                    }
                    case NETWORK_RESPONSIBLE: {
                        List<ComputedUserPermission> permissions = computeNetworkResponsiblePermissions(userId, entry.getValue());
                        result.addAll(permissions);
                        break;
                    }
                    case NETWORK_SUPERVISOR: {
                        List<ComputedUserPermission> permissions = computeNetworkSupervisorPermissions(userId, entry.getValue());
                        result.addAll(permissions);
                        break;
                    }
                    default:
                        throw new UnsupportedOperationException("Unexpected type: " + roleType);
                }

            }
        }

        if (LOGGER.isDebugEnabled()) {
            long end = System.currentTimeMillis();
            LOGGER.debug(String.format("%d permissions computed in %dms (userId=%s)", result.size(), end - begin, userId));
        }

        return result;
    }

    protected List<ComputedUserPermission> computeIsDataProcessorPermissions(String userId) {

        List<ComputedUserPermission> result = new LinkedList<>();

        // TODO AThimel 05/12/13 Maybe it would be better to process a different way, with no permission generated and role managed on all queries ?

        Set<String> domains = domainDao.getAllDomainCodes();
        for (String domain : domains) {
            ComputedUserPermission permission = computedUserPermissionDao.newDomainReadValidatedPermission(userId, domain);
            result.add(permission);
        }
        Set<String> growingPlans = growingPlanDao.getAllGrowingPlanCodes();
        for (String growingPlan : growingPlans) {
            ComputedUserPermission permission = computedUserPermissionDao.newGrowingPlanReadValidatedPermission(userId, growingPlan);
            result.add(permission);
        }
        Set<String> growingSystems = growingSystemDao.getAllGrowingSystemCodes();
        for (String growingSystem : growingSystems) {
            ComputedUserPermission permission = computedUserPermissionDao.newGrowingSystemReadValidatedPermission(userId, growingSystem);
            result.add(permission);
        }

        return result;
    }


    // XXX AThimel 01/10/13 Use a separate class in order to do the following treatments ?

    protected List<ComputedUserPermission> computeGrowingSystemDataProcessorPermissions(String userId, UserRole role) {

        List<ComputedUserPermission> result = new LinkedList<>();

        String growingSystemCode = role.getGrowingSystemCode();
        String growingSystemId = role.getGrowingSystemId();
        Preconditions.checkState(StringUtils.isNotEmpty(growingSystemCode) || StringUtils.isNotEmpty(growingSystemId));

        {
            ComputedUserPermission permission;
            if (StringUtils.isNotEmpty(growingSystemCode)) {
                permission = computedUserPermissionDao.newGrowingSystemReadValidatedPermission(userId, growingSystemCode);
            } else {
                permission = computedUserPermissionDao.newSpecificGrowingSystemReadValidatedPermission(userId, growingSystemId);
            }
            result.add(permission);
        }

        {
            ComputedUserPermission domainPermission;
            ComputedUserPermission growingPlanPermission;
            String forReportProperty;
            String forReportIdentifier;
            if (StringUtils.isNotEmpty(growingSystemCode)) {
                GrowingSystem growingSystem = growingSystemDao.forCodeEquals(growingSystemCode).findAny();
                GrowingPlan growingPlan = growingSystem.getGrowingPlan();
                String growingPlanCode = growingPlan.getCode();
                growingPlanPermission = computedUserPermissionDao.newGrowingPlanReadValidatedPermission(userId, growingPlanCode);
                String domainCode = growingPlan.getDomain().getCode();
                domainPermission = computedUserPermissionDao.newDomainReadValidatedPermission(userId, domainCode);

                forReportProperty = ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_CODE;
                forReportIdentifier = growingSystemCode;
            } else {
                GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(growingSystemId).findUnique();
                GrowingPlan growingPlan = growingSystem.getGrowingPlan();
                String growingPlanId = growingPlan.getTopiaId();
                growingPlanPermission = computedUserPermissionDao.newSpecificGrowingPlanReadValidatedPermission(userId, growingPlanId);
                String domainId = growingPlan.getDomain().getTopiaId();
                domainPermission = computedUserPermissionDao.newSpecificDomainReadValidatedPermission(userId, domainId);

                forReportProperty = ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID;
                forReportIdentifier = growingSystemId;
            }
            result.add(growingPlanPermission);
            result.add(domainPermission);

            // report regional
            Set<String> reportRegionalIds = computedUserPermissionDao.getProjectionHelper()
                    .growingSystemsToReportRegional(forReportProperty, forReportIdentifier);
            for (String reportRegionalId : reportRegionalIds) {
                result.add(computedUserPermissionDao.newReportRegionalReadPermission(userId, reportRegionalId));
            }
        }

        return result;
    }

    protected List<ComputedUserPermission> computeGrowingPlanResponsiblePermissions(String userId, Collection<UserRole> roles) {

        boolean expectedType = roles.stream()
                .allMatch(role -> role.getType().equals(RoleType.GROWING_PLAN_RESPONSIBLE));
        Preconditions.checkArgument(expectedType, "Tous les rôles ne sont pas de type responsable de dispositif: " + userId);


        List<ComputedUserPermission> result = new LinkedList<>();

        final Set<String> growingPlanCodes = roles.stream()
                .map(UserRole::getGrowingPlanCode)
                .collect(Collectors.toSet());

        for (String growingPlanCode : growingPlanCodes) {
            ComputedUserPermission adminPermission = computedUserPermissionDao.newGrowingPlanAdminPermission(userId, growingPlanCode);
            result.add(adminPermission);

            GrowingPlan growingPlan = growingPlanDao.forCodeEquals(growingPlanCode).findAny();
            String domainCode = growingPlan.getDomain().getCode();
            ComputedUserPermission permission = computedUserPermissionDao.newDomainReadPermission(userId, domainCode);
            result.add(permission);
        }

        Callable<LinkedHashSet<String>> gp2gsLoader = () -> computedUserPermissionDao.getProjectionHelper().growingPlansToGrowingSystemsCode(growingPlanCodes);
        LinkedHashSet<String> growingSystemCodes = cacheService.get(CacheDiscriminator.growingPlansToGrowingSystemsCode(), growingPlanCodes, gp2gsLoader);
        if (growingSystemCodes != null) {
            for (String growingSystemCode : growingSystemCodes) {
                ComputedUserPermission permission = computedUserPermissionDao.newGrowingSystemAdminPermission(userId, growingSystemCode);
                result.add(permission);
            }
        }

        // Permission on reportRegional
        Callable<LinkedHashSet<String>> d2rrLoader = () -> growingPlanDao.getProjectionHelper().growingPlanToReportRegional(growingPlanCodes);
        LinkedHashSet<String> reportRegionalIds = cacheService.get(CacheDiscriminator.growingPlanToReportRegional(), growingSystemCodes, d2rrLoader);
        if (reportRegionalIds != null) {
            for (String reportRegionalId : reportRegionalIds) {
                ComputedUserPermission permission = computedUserPermissionDao.newReportRegionalReadPermission(userId, reportRegionalId);
                result.add(permission);
            }
        }

        return result;
    }

    protected List<ComputedUserPermission> computeDomainResponsiblePermissions(String userId, Collection<UserRole> roles) {

        boolean expectedType = roles.stream()
                .allMatch(role -> role.getType().equals(RoleType.DOMAIN_RESPONSIBLE));
        Preconditions.checkArgument(expectedType, "Tous les rôles ne sont pas de type responsable de domaine: " + userId);

        List<ComputedUserPermission> result = new LinkedList<>();

        final Set<String> domainCodes = roles.stream()
                .map(UserRole::getDomainCode)
                .collect(Collectors.toSet());

        for (String domainCode : domainCodes) {
            ComputedUserPermission permission = computedUserPermissionDao.newDomainAdminPermission(userId, domainCode);
            result.add(permission);
        }

        Callable<LinkedHashSet<String>> d2gpLoader = () -> growingPlanDao.domainsToGrowingPlansCode(domainCodes);
        Set<String> growingPlanCodes = cacheService.get(CacheDiscriminator.domainToGrowingPlanCodes(), domainCodes, d2gpLoader);
        if (growingPlanCodes != null) {
            for (String growingPlanCode : growingPlanCodes) {
                ComputedUserPermission permission = computedUserPermissionDao.newGrowingPlanReadPermission(userId, growingPlanCode);
                result.add(permission);
            }
        }

        Callable<LinkedHashSet<String>> d2gsLoader = () -> growingPlanDao.getProjectionHelper().domainsToGrowingSystemsCode(domainCodes);
        LinkedHashSet<String> growingSystemCodes = cacheService.get(CacheDiscriminator.domainToGrowingSystemCodes(), domainCodes, d2gsLoader);
        if (growingSystemCodes != null) {
            for (String growingSystemCode : growingSystemCodes) {
                ComputedUserPermission permission = computedUserPermissionDao.newGrowingSystemReadPermission(userId, growingSystemCode);
                result.add(permission);
            }
        }

        // Permission on reportRegional
        Callable<LinkedHashSet<String>> d2rrLoader = () -> growingPlanDao.getProjectionHelper().domainsToReportRegional(domainCodes);
        LinkedHashSet<String> reportRegionalIds = cacheService.get(CacheDiscriminator.growingSystemCodesToReportRegional(), growingSystemCodes, d2rrLoader);
        if (reportRegionalIds != null) {
            for (String reportRegionalId : reportRegionalIds) {
                ComputedUserPermission permission = computedUserPermissionDao.newReportRegionalReadPermission(userId, reportRegionalId);
                result.add(permission);
            }
        }

        return result;
    }

    protected List<ComputedUserPermission> computeNetworkResponsiblePermissions(String userId, Collection<UserRole> roles) {

        boolean expectedType = roles.stream()
                .allMatch(role -> RoleType.NETWORK_RESPONSIBLE.equals(role.getType()));
        Preconditions.checkArgument(expectedType, "Tous les rôles ne sont pas de type responsable de réseau: " + userId);

        List<ComputedUserPermission> result = new LinkedList<>();

        // Permission on networks
        final Set<String> networkIds = roles.stream()
                .map(UserRole::getNetworkId)
                .collect(Collectors.toSet());

        for (String networkId : networkIds) {
            ComputedUserPermission permission = computedUserPermissionDao.newNetworkAdminPermission(userId, networkId);
            result.add(permission);
        }

        // Permissions on domains
        Set<String> domainCodes = computedUserPermissionDao.getProjectionHelper().networksToDomainsCode(networkIds);
        if (domainCodes != null) {
            for (String domainCode : domainCodes) {
                ComputedUserPermission permission = computedUserPermissionDao.newDomainReadPermission(userId, domainCode);
                result.add(permission);
            }
        }

        // Permissions on growingPlans
        Set<String> growingPlanCodes = computedUserPermissionDao.getProjectionHelper().networksToGrowingPlansCode(networkIds);
        if (growingPlanCodes != null) {
            for (String growingPlanCode : growingPlanCodes) {
                ComputedUserPermission permission = computedUserPermissionDao.newGrowingPlanReadPermission(userId, growingPlanCode);
                result.add(permission);
            }
        }

        // Permissions on growingSystems
        Set<String> growingSystemCodes = computedUserPermissionDao.getProjectionHelper().networksToGrowingSystemsCode(networkIds);
        if (growingSystemCodes != null) {
            for (String growingSystemCode : growingSystemCodes) {
                ComputedUserPermission permission = computedUserPermissionDao.newGrowingSystemReadPermission(userId, growingSystemCode);
                result.add(permission);
            }
        }

        // Permission on reportRegional (read-write on self networks and sub networks)
        Set<String> reportRegionalIds = computedUserPermissionDao.getProjectionHelper().networksToReportRegional(networkIds);
        if (reportRegionalIds != null) {
            for (String reportRegionalId : reportRegionalIds) {
                ComputedUserPermission permission = computedUserPermissionDao.newReportRegionalAdminPermission(userId, reportRegionalId);
                result.add(permission);
            }
        }

        return result;
    }

    protected List<ComputedUserPermission> computeNetworkSupervisorPermissions(String userId, Collection<UserRole> roles) {

        boolean expectedType = roles.stream()
                .allMatch(role -> RoleType.NETWORK_SUPERVISOR.equals(role.getType()));
        Preconditions.checkArgument(expectedType, "Tous les rôles ne sont pas de type responsable de réseau (écriture) : " + userId);

        List<ComputedUserPermission> result = new LinkedList<>();

        // Permission on networks
        final Set<String> networkIds = roles.stream()
                .map(UserRole::getNetworkId)
                .collect(Collectors.toSet());

        for (String networkId : networkIds) {
            ComputedUserPermission permission = computedUserPermissionDao.newNetworkAdminPermission(userId, networkId);
            result.add(permission);
        }

        // Permissions on domains
        Set<String> domainCodes = computedUserPermissionDao.getProjectionHelper().networksToDomainsCode(networkIds);
        if (domainCodes != null) {
            for (String domainCode : domainCodes) {
                ComputedUserPermission permission = computedUserPermissionDao.newDomainReadPermission(userId, domainCode);
                result.add(permission);
                permission = computedUserPermissionDao.newDomainWritePermission(userId, domainCode);
                result.add(permission);
            }
        }

        // Permissions on growingPlans
        Set<String> growingPlanCodes = computedUserPermissionDao.getProjectionHelper().networksToGrowingPlansCode(networkIds);
        if (growingPlanCodes != null) {
            for (String growingPlanCode : growingPlanCodes) {
                ComputedUserPermission permission = computedUserPermissionDao.newGrowingPlanReadPermission(userId, growingPlanCode);
                result.add(permission);
                permission = computedUserPermissionDao.newGrowingPlanWritePermission(userId, growingPlanCode);
                result.add(permission);
            }
        }

        // Permissions on growingSystems
        Set<String> growingSystemCodes = computedUserPermissionDao.getProjectionHelper().networksToGrowingSystemsCode(networkIds);
        if (growingSystemCodes != null) {
            for (String growingSystemCode : growingSystemCodes) {
                ComputedUserPermission permission = computedUserPermissionDao.newGrowingSystemReadPermission(userId, growingSystemCode);
                result.add(permission);
                permission = computedUserPermissionDao.newGrowingSystemWritePermission(userId, growingSystemCode);
                result.add(permission);
            }
        }

        // Permission on reportRegional (read-write on self networks and sub networks)
        Set<String> reportRegionalIds = computedUserPermissionDao.getProjectionHelper().networksToReportRegional(networkIds);
        if (reportRegionalIds != null) {
            for (String reportRegionalId : reportRegionalIds) {
                ComputedUserPermission permission = computedUserPermissionDao.newReportRegionalAdminPermission(userId, reportRegionalId);
                result.add(permission);
            }
        }

        return result;
    }

    protected void markPermissionsAsDirty(Iterable<ComputedUserPermission> toDirty) {
        for (ComputedUserPermission permission : toDirty) {
            permission.setDirty(true);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format("ComputedUserPermission marked as dirty: %s", permission));
            }
            computedUserPermissionDao.update(permission);
        }
    }

    protected void objectsAreDirty(PermissionObjectType type) {
        final List<ComputedUserPermission> toDirty = computedUserPermissionDao.forProperties(
                ComputedUserPermission.PROPERTY_TYPE, type,
                ComputedUserPermission.PROPERTY_DIRTY, false).findAll();

        if (LOGGER.isInfoEnabled() && !toDirty.isEmpty()) {
            LOGGER.info(String.format("%d ComputedUserPermission[type=%s] will be marked as dirty", toDirty.size(), type));
        }

        markPermissionsAsDirty(toDirty);
    }

    protected void objectIsDirty(PermissionObjectType type, String object) {
        final List<ComputedUserPermission> toDirty = computedUserPermissionDao.forProperties(
                ComputedUserPermission.PROPERTY_TYPE, type,
                ComputedUserPermission.PROPERTY_OBJECT, object,
                ComputedUserPermission.PROPERTY_DIRTY, false).findAll();

        if (LOGGER.isInfoEnabled() && !toDirty.isEmpty()) {
            LOGGER.info(String.format("%d ComputedUserPermission[type=%s, object=%s] will be marked as dirty", toDirty.size(), type, object));
        }

        markPermissionsAsDirty(toDirty);
    }

    @Override
    public ImportResult importRoles(InputStream rolesFileStream) {

        UserRolesImportModel model = new UserRolesImportModel();

        ImportResult result = new ImportResult();
        try (Import<ImportUserRoleDto> importer = Import.newImport(model, rolesFileStream)) {

            Map<String, List<UserRoleDto>> userRolesDtoMap = new HashMap<>();

            for (ImportUserRoleDto entity : importer) {

                String email = entity.getUserEmail();
                Preconditions.checkArgument(StringUtils.isNotEmpty(email));
                // make sure email is in lower case
                email = entity.getUserEmail().toLowerCase();

                AgrosystUser agrosystUser = agrosystUserDao.forEmailEquals(email).findAnyOrNull();
                if (agrosystUser == null) {
                    // as requested, do nothing for existing accounts
                    result.incIgnored();
                } else if (entity.getType() == RoleType.ADMIN || entity.getType() == RoleType.IS_DATA_PROCESSOR){

                    List<UserRoleDto> userRoleDtos = userRolesDtoMap.computeIfAbsent(agrosystUser.getTopiaId(), k -> new ArrayList<>());
                    UserRoleDto userRoleDto = new UserRoleDto();
                    userRoleDto.setType(entity.getType());
                    userRoleDtos.add(userRoleDto);
                } else {

                    // targeted entity
                    String term = entity.getTargetedEntity();

                    List<UserRoleEntityDto> userRoleEntityDtos =
                            new ArrayList<>(searchEntities(entity.getType(), term, entity.getCampaign()));

                    if (userRoleEntityDtos.size() == 1) {
                        // valid there are no ambiguity for the targeted entity
                        List<UserRoleDto> userRoleDtos = userRolesDtoMap.computeIfAbsent(agrosystUser.getTopiaId(), k -> new ArrayList<>());
                        UserRoleDto userRoleDto = new UserRoleDto();
                        userRoleDto.setType(entity.getType());
                        UserRoleEntityDto userRoleEntityDto = userRoleEntityDtos.getFirst();
                        userRoleEntityDto.setCampaign(entity.getCampaign());
                        userRoleDto.setEntity(userRoleEntityDto);
                        userRoleDtos.add(userRoleDto);

                    } else if (userRoleEntityDtos.isEmpty()){
                        // no entity found
                        if (LOGGER.isWarnEnabled()) {
                            LOGGER.warn("Can't find any entity for term " + term);
                        }
                        result.incIgnored();
                    } else {
                        // too many entities return to be able to have unique target
                        if (LOGGER.isWarnEnabled()) {
                            LOGGER.warn("Too many entities founds for term " + term);
                        }
                        result.incIgnored();
                    }
                }
            }
            for (Map.Entry<String, List<UserRoleDto>> entry : userRolesDtoMap.entrySet()) {
                String key = entry.getKey();
                List<UserRoleDto> userRoleDtos = entry.getValue();
                saveUserRoles(key, userRoleDtos);
                result.incCreated();
            }

            getTransaction().commit();
        }

        return result;
    }

    @Override
    public long countPermissions() {
        return computedUserPermissionDao.count();
    }

    @Override
    public void copyUserRoles(String fromUserId, List<String> toUserIds) {
        Preconditions.checkArgument(StringUtils.isNotBlank(fromUserId) && CollectionUtils.isNotEmpty(toUserIds),
                "Roles copy need a source user and target users");

        List<UserRoleDto> userRoles = getUserRoles(fromUserId);
        userRoles.forEach(userRole -> userRole.setTopiaId(null));
        createOrUpdateUserRoles(toUserIds, userRoles, false);
    }

    @Override
    public boolean isInMaintenanceMode() {
        return getConfig().isMaintenanceModeActive();
    }

    @Override
    public String getMaintenanceModeMessage() {
        return getConfig().getMaintenanceModeMessage();
    }

    @Override
    public boolean mustDisconnetAllUsers() {
        return getConfig().isMaintenanceModeDisconnectAllUsers();
    }
    
    @Override
    public void setMaintenanceMode(boolean active, String message, boolean disconnectAllUsers) {
        getConfig().setMaintenanceMode(active, message, disconnectAllUsers);

        // On demande aux autres instances de recharger la configuration
        reloadConfig();
    }
    
    @Override
    public void reloadConfig() {
        // On demande aux autres instances de recharger la configuration
        context.getSynchroHelper().emitConfigReload();
    }

    @Override
    public void periodicCheckForComputedPermissionsDeletion() {

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -12);
        Date obsoleteDate = calendar.getTime();

        Set<String> dirtyPermsUsers = computedUserPermissionDao.findAllUsersWithObsolete(obsoleteDate, true);
        Set<String> nonDirtyPermsUsers = computedUserPermissionDao.findAllUsersWithObsolete(obsoleteDate, false);

        // On procède en 2 temps

        // Étape 1 : d'abord on marque les permissions qui ne sont pas du tout dirty en dirty pour laisser à l'application les recalculer si elle en a besoin
        Sets.SetView<String> nonDirtyOnlyUsers = Sets.difference(nonDirtyPermsUsers, dirtyPermsUsers);
        if (!nonDirtyOnlyUsers.isEmpty()) {
            computedUserPermissionDao.markAsDirtyForUserIds(nonDirtyOnlyUsers);
        }

        // Étape 2 : On supprime toutes les permissions qui sont dirty uniquement
        Sets.SetView<String> dirtyOnlyUsers = Sets.difference(dirtyPermsUsers, nonDirtyPermsUsers);
        if (!dirtyOnlyUsers.isEmpty()) {
            computedUserPermissionDao.deleteByUserIds(dirtyOnlyUsers);
        }

        getTransaction().commit();
    }
    
    @Override
    public void markAsDirtyForAllUsers() {
        computedUserPermissionDao.markAsDirtyForAllUsers();
        getTransaction().commit();
    }

}
