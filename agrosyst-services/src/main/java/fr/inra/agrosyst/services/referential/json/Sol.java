package fr.inra.agrosyst.services.referential.json;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2016 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.referential.ImportStatus;

/**
 * Created by davidcosse on 02/12/15.
 */
public class Sol {

    // fields from api
    protected String id_type_sol;
    protected String region;
    protected String calcaire;
    protected String nom_arvalis;
    protected String pierrosite;
    protected String texture;
    protected String profondeur;
    protected String nom_referenciel_pedologique_francais;
    protected String hydromorphie;
    protected String nom_sol;

    // fileds for export to csv
    protected String agrosystId;
    protected ImportStatus status;
    protected boolean active;
    protected String source;

    public String getId_type_sol() {
        return id_type_sol;
    }

    public void setId_type_sol(String id_type_sol) {
        this.id_type_sol = id_type_sol;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCalcaire() {
        return calcaire;
    }

    public void setCalcaire(String calcaire) {
        this.calcaire = calcaire;
    }

    public String getNom_arvalis() {
        return nom_arvalis;
    }

    public void setNom_arvalis(String nom_arvalis) {
        this.nom_arvalis = nom_arvalis;
    }

    public String getPierrosite() {
        return pierrosite;
    }

    public void setPierrosite(String pierrosite) {
        this.pierrosite = pierrosite;
    }

    public String getTexture() {
        return texture;
    }

    public void setTexture(String texture) {
        this.texture = texture;
    }

    public String getProfondeur() {
        return profondeur;
    }

    public void setProfondeur(String profondeur) {
        this.profondeur = profondeur;
    }

    public String getNom_referenciel_pedologique_francais() {
        return nom_referenciel_pedologique_francais;
    }

    public void setNom_referenciel_pedologique_francais(String nom_referenciel_pedologique_francais) {
        this.nom_referenciel_pedologique_francais = nom_referenciel_pedologique_francais;
    }

    public String getHydromorphie() {
        return hydromorphie;
    }

    public void setHydromorphie(String hydromorphie) {
        this.hydromorphie = hydromorphie;
    }

    public String getNom_sol() {
        return nom_sol;
    }

    public void setNom_sol(String nom_sol) {
        this.nom_sol = nom_sol;
    }

    public String getAgrosystId() {
        return agrosystId;
    }

    public void setAgrosystId(String agrosystId) {
        this.agrosystId = agrosystId;
    }

    public ImportStatus getStatus() {
        return status;
    }

    public void setStatus(ImportStatus status) {
        this.status = status;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getSol_region_code() {
        return 0;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

}
