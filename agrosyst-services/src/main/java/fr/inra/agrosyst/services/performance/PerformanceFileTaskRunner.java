package fr.inra.agrosyst.services.performance;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.async.Task;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.services.ServiceContext;
import fr.inra.agrosyst.services.async.AbstractTaskRunner;
import fr.inra.agrosyst.services.common.EmailService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.time.Duration;

public class PerformanceFileTaskRunner extends AbstractTaskRunner<PerformanceFileTask, String> {

    private static final Log LOGGER = LogFactory.getLog(PerformanceFileTaskRunner.class);

//    @Override
//    protected void informUser(PerformanceTask parameters, Optional<PerformanceTaskResult> result, Optional<Exception> exception) {
//        LOGGER.info("Informe user");
//    }

    @Override
    protected String executeTask(PerformanceFileTask task, ServiceContext serviceContext) throws Exception {
        /* Performance to generate file. */
        String performanceId = task.getPerformanceId();

        long ts = System.currentTimeMillis();
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info(task.getUserEmail() +": Starting performance thread for performance " + performanceId);
        }

        PerformanceService performanceService = serviceContext.newService(PerformanceService.class);

        task.setPerformanceService(performanceService);

        // start compute all indicators
        String performanceName = performanceService.generatePerformanceFile(performanceId);

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info(String.format(task.getUserEmail() +": Performance thread finished for %s in %d s", performanceId, (System.currentTimeMillis() - ts) / 1000));
        }

        return performanceName;
    }

    @Override
    public boolean cancelTask(Task task_) {
        PerformanceFileTask task = (PerformanceFileTask) task_;
        PerformanceService performanceService = task.getPerformanceService();
        if (performanceService != null) {
            performanceService.cancelPerformance(task.getPerformanceId());
        }
        return true;
    }

    @Override
    protected void taskSucceeded(PerformanceFileTask task, ServiceContext serviceContext, String performanceName, Duration duration) {
        // Envoi un email de fin de traitement
        String userEmail = task.getUserEmail();
        EmailService emailService = serviceContext.newService(EmailService.class);
        emailService.notifyPerformanceSuccess(userEmail, performanceName, duration.getSeconds());
    }

    @Override
    protected void taskFailed(PerformanceFileTask task, ServiceContext serviceContext, Exception eee) {
        String userEmail = task.getUserEmail();
        EmailService emailService = serviceContext.newService(EmailService.class);
        emailService.notifyPerformanceError(userEmail, task.getPerformanceId(), eee);
    }

}
