package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefPrixFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiOrgaImpl;
import fr.inra.agrosyst.services.common.CommonService;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefPrixFertiOrgaModel extends AbstractDestinationAndPriceModel<RefPrixFertiOrga> implements ExportModel<RefPrixFertiOrga> {

    public RefPrixFertiOrgaModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Idtypeeffluent", RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT);
        newMandatoryColumn("Nom", RefPrixFertiOrga.PROPERTY_NOM);
        newMandatoryColumn("Prix", RefPrixFertiOrga.PROPERTY_PRICE, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("Unité", RefPrixFertiOrga.PROPERTY_UNIT, PRICE_UNIT_PARSER);
        newMandatoryColumn("Code scénario", RefPrixFertiOrga.PROPERTY_CODE_SCENARIO);
        newMandatoryColumn("Scénario", RefPrixFertiOrga.PROPERTY_SCENARIO);
        newMandatoryColumn("Campagne", RefPrixFertiOrga.PROPERTY_CAMPAIGN, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("Organic", RefPrixFertiOrga.PROPERTY_ORGANIC, CommonService.BOOLEAN_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefPrixFertiOrga.PROPERTY_ACTIVE, ACTIVE_PARSER);
        newOptionalColumn("Source", RefPrixFertiOrga.PROPERTY_SOURCE);
    }

    @Override
    public Iterable<ExportableColumn<RefPrixFertiOrga, Object>> getColumnsForExport() {
        ModelBuilder<RefPrixFertiOrga> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Idtypeeffluent", RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT);
        modelBuilder.newColumnForExport("Nom", RefPrixFertiOrga.PROPERTY_NOM);
        modelBuilder.newColumnForExport("Prix", RefPrixFertiOrga.PROPERTY_PRICE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Unité", RefPrixFertiOrga.PROPERTY_UNIT, PRICE_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("Code scénario", RefPrixFertiOrga.PROPERTY_CODE_SCENARIO);
        modelBuilder.newColumnForExport("Scénario", RefPrixFertiOrga.PROPERTY_SCENARIO);
        modelBuilder.newColumnForExport("Campagne", RefPrixFertiOrga.PROPERTY_CAMPAIGN, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Organic", RefPrixFertiOrga.PROPERTY_ORGANIC, O_N_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefPrixFertiOrga.PROPERTY_ACTIVE, T_F_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefPrixFertiOrga.PROPERTY_SOURCE);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefPrixFertiOrga newEmptyInstance() {
        RefPrixFertiOrga result = new RefPrixFertiOrgaImpl();
        result.setActive(true);
        return result;
    }
}
