package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.MaintenancePruningVinesAction;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OtherAction;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

/**
 * Les charges opérationnelles réelles sont exprimées en €/ha.
 * Elles correspondent aux dépenses liées à l’achat des intrants (semences et plants,
 * produits fertilisants minéraux  et  organiques, traitement de semences et plants,
 * irrigation, produits phytosanitaires, produits de lutte biologique, autres intrants, substrats, pots).
 * <p>
 * Le calcul se fait sur la base des prix saisis par l’utilisateur.
 * <p>
 * Les interventions concernées par cet indicateur sont toutes les interventions
 * contenant un intrant et/ou une action de type « Semis » et/ou une action de
 * type « Irrigation ».
 * <p>
 *
 * <p>
 * Cette classe est une composante de {@link IndicatorOperatingExpenses} chargée de calculer les charges relatives au semis.
 * </p>
 * Rappel de la formule globale de calcul:
 *
 * <pre>
 * CI réel_i:
 *
 * = PSCi * ( sum(Q_ev * PA_ev) + sum(Q_j * PA_j) + sum(Q_e * PA_e) + sum(Q_a * PA_a)) + PSCi_phyto * sum(Q_k * PA_k) + PSCi_luttebio * sum(Q_h * PA_h)
 *
 * Avec :
 * - CI réel_i : charges intrants de l’intervention i
 * - PSCi (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i.
 *   PSCi est calculé sur la base des données saisies par l’utilisateur.
 *
 * - Autres intrants
 *   - Q_a (diverses unités) : quantité de l'intrant autre. Donnée saisie par l’utilisateur.
 *   - PA_a (unité unique) : pour le moment, l’utilisateur ne peut saisir un prix que dans une seule unité : Euros/ha.
 *     Mais il faut prévoir à termes que l’utilisateur puisse saisir d’autres unités
 *     et donc adopter le même fonctionnement que pour tous les autres intrants. Donnée saisie par l’utilisateur.
 *
 *
 * </pre>
 *
 * @author David Cossé (cosse@codelutin.com)
 */
public class IndicatorOtherProductInputOperatingExpenses extends IndicatorInputProductOperatingExpenses {

    protected static final Log LOGGER = LogFactory.getLog(IndicatorOtherProductInputOperatingExpenses.class);

    public IndicatorOtherProductInputOperatingExpenses(boolean displayed, boolean computeReal, boolean computStandardised, Locale locale) {
        super(displayed, computeReal, computStandardised, locale);
    }

    @Override
    public Pair<Optional<InputPrice>, Optional<InputRefPrice>> getDomainInputPrice(AbstractInputUsage usage, RefCampaignsInputPricesByDomainInput refCampaignsInputPricesByDomainInputAndCampaigns) {
        final DomainOtherInput input = ((OtherProductInputUsage) usage).getDomainOtherInput();
        final InputPrice inputPrice = input.getInputPrice();
        Optional<InputRefPrice> inputRePrice = refCampaignsInputPricesByDomainInputAndCampaigns.otherRefPriceForInput().get(input);
        inputRePrice = inputRePrice == null ? Optional.empty() : inputRePrice;
        return Pair.of(Optional.ofNullable(inputPrice), inputRePrice);
    }

    // practiced
    protected Double[] computeOperatingExpenses(
            WriterContext writerContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> otherProductConverters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        PracticedSystem practicedSystem = practicedSystemContext.getPracticedSystem();
        long start = System.currentTimeMillis();

        final PracticedIntervention intervention = interventionContext.getIntervention();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(
                    "computeOtherProductIndicator intervention:" +
                            intervention.getName() + " (" + intervention.getTopiaId() + ")" +
                            ", practicedSystem:" + practicedSystem.getName() +
                            " ) calculé en :" + (System.currentTimeMillis() - start) + "ms");
        }

        double psci = getToolPSCi(intervention);

        final Collection<AbstractAction> actions = CollectionUtils.emptyIfNull(interventionContext.getActions());

        Map<AgrosystInterventionType, List<AbstractAction>> actionsByAgrosystInterventionTypes = getActionsByAgrosystInterventionTypes(actions);

        final RefCampaignsInputPricesByDomainInput refInputPricesForCampaignsByInput = practicedSystemContext.getPracticedSystemInputRefPricesByDomainInput();

        final Double[] result = computeOtherInputsOperatingExpenses(
                writerContext,
                psci,
                actionsByAgrosystInterventionTypes,
                refInputPricesForCampaignsByInput,
                otherProductConverters,
                intervention.getTopiaId(),
                indicatorClass,
                labels);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(
                    String.format(
                            Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_PRACTICED,
                            "computeOtherProductIndicator",
                            intervention.getName(),
                            intervention.getTopiaId(),
                            practicedSystem.getName(),
                            practicedSystem.getCampaigns(),
                            System.currentTimeMillis() - start));
        }

        return result;
    }

    private Double[] computeOtherInputsOperatingExpenses(
            WriterContext writerContext,
            double psci,
            Map<AgrosystInterventionType, List<AbstractAction>> actionsByAgrosystInterventionTypes,
            RefCampaignsInputPricesByDomainInput refInputPricesForCampaignsByInput, Collection<RefInputUnitPriceUnitConverter> otherProductConverters,
            String interventionId,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        Double[] result = new Double[]{
                IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};

        for (Map.Entry<AgrosystInterventionType, List<AbstractAction>> actionsForAgrosystInterventionType : actionsByAgrosystInterventionTypes.entrySet()) {
            List<AbstractAction> actions = actionsForAgrosystInterventionType.getValue();
            for (AbstractAction abstractAction : actions) {
                Collection<OtherProductInputUsage> otherProductInputUsages = getOtherProductInputUsage(abstractAction);

                final Double[] actionResult;
                if (CollectionUtils.isEmpty(otherProductInputUsages)) {
                    // no other product cost
                    actionResult = new Double[]{
                            IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                            IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};
                } else {
                    //PA a (unité unique) : pour le moment, l’utilisateur ne peut saisir un prix que dans une seule unité :
                    //Euros/ha. Mais il faut prévoir à termes que l’utilisateur puisse saisir d’autres unités et donc adopter le
                    //même fonctionnement que pour tous les autres intrants. Donnée saisie par l’utilisateur.
                    Pair<Double, Double> inputsCharges = computeIndicatorForAction(
                            writerContext,
                            abstractAction,
                            otherProductInputUsages,
                            refInputPricesForCampaignsByInput,
                            psci,
                            otherProductConverters,
                            interventionId,
                            indicatorClass,
                            labels,
                            new HashMap<>());

                    actionResult = new Double[]{inputsCharges.getLeft(), inputsCharges.getRight()};

                }
                result = sum(result, actionResult);
            }
        }
        return result;
    }

    // effective
    protected Double[] computeOperatingExpenses(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            Zone zone,
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> otherProductConverters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        long start = System.currentTimeMillis();

        EffectiveIntervention intervention = interventionContext.getIntervention();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(
                    "computeOtherProductIndicator intervention:" +
                            intervention.getName() + " (" + intervention.getTopiaId() + ")" +
                            ", zone:" + zone.getName() +
                            " ) calculé en :" + (System.currentTimeMillis() - start) + "ms");
        }

        // inputsCharges for price, inputsCharges for refprice
        double psci = getToolPSCi(intervention);

        final Collection<AbstractAction> actions = CollectionUtils.emptyIfNull(interventionContext.getActions());

        Map<AgrosystInterventionType, List<AbstractAction>> actionsByAgrosystInterventionTypes = getActionsByAgrosystInterventionTypes(actions);

        final RefCampaignsInputPricesByDomainInput refInputPricesForCampaignsByInput = domainContext.getRefInputPricesForCampaignsByInput();

        final Double[] result = computeOtherInputsOperatingExpenses(
                writerContext,
                psci,
                actionsByAgrosystInterventionTypes,
                refInputPricesForCampaignsByInput,
                otherProductConverters,
                intervention.getTopiaId(),
                indicatorClass,
                labels);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(
                    "computeOtherProductIndicator intervention:" +
                            intervention.getName() +
                            " (" + intervention.getTopiaId() + ")" +
                            ", zone:" + zone.getTopiaId() +
                            " calculé en :" + (System.currentTimeMillis() - start));
        }

        return result;
    }

    protected static Map<AgrosystInterventionType, List<AbstractAction>> getActionsByAgrosystInterventionTypes(Collection<AbstractAction> actions) {
        Map<AgrosystInterventionType, List<AbstractAction>> indexedActions = new HashMap<>();
        for (AbstractAction action : actions) {
            List<AbstractAction> sameTypeActions = indexedActions.computeIfAbsent(action.getMainAction().getIntervention_agrosyst(), k -> new ArrayList<>());
            sameTypeActions.add(action);
        }
        return indexedActions;
    }

    public static Collection<OtherProductInputUsage> getOtherProductInputUsage(AbstractAction action) {

        Collection<OtherProductInputUsage> result = new ArrayList<>();

        AgrosystInterventionType agrosystInterventionType = action.getMainAction().getIntervention_agrosyst();
        switch (agrosystInterventionType) {
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                final MineralFertilizersSpreadingAction action_ = (MineralFertilizersSpreadingAction) action;
                result.addAll(action_.getOtherProductInputUsages());
            }
            case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES -> {
                final PesticidesSpreadingAction action_ = (PesticidesSpreadingAction) action;
                result.addAll(action_.getOtherProductInputUsages());
            }
            case AUTRE -> {
                final OtherAction action_ = (OtherAction) action;
                result.addAll(action_.getOtherProductInputUsages());
            }
            case ENTRETIEN_TAILLE_VIGNE_ET_VERGER -> {
                final MaintenancePruningVinesAction action_ = (MaintenancePruningVinesAction) action;
                result.addAll(action_.getOtherProductInputUsages());
            }
            case EPANDAGES_ORGANIQUES -> {
                final OrganicFertilizersSpreadingAction action_ = (OrganicFertilizersSpreadingAction) action;
                result.addAll(action_.getOtherProductInputUsages());
            }
            case IRRIGATION -> {
                final IrrigationAction action_ = (IrrigationAction) action;
                result.addAll(action_.getOtherProductInputUsages());
            }
            case LUTTE_BIOLOGIQUE -> {
                final BiologicalControlAction action_ = (BiologicalControlAction) action;
                result.addAll(action_.getOtherProductInputUsages());
            }
            case RECOLTE -> {
                final HarvestingAction action_ = (HarvestingAction) action;
                result.addAll(action_.getOtherProductInputUsages());
            }
            case SEMIS -> {
                final SeedingActionUsage action_ = (SeedingActionUsage) action;
                result.addAll(action_.getOtherProductInputUsages());
            }
            case TRANSPORT, TRAVAIL_DU_SOL -> {
            }
        }

        return result;
    }

    protected Double[] sum(Double[] arr1, Double[] arr2) {
        Double[] arr3 = new Double[arr1.length];
        for (int i = 0; i < Math.min(arr1.length, arr2.length); i++) {
            arr3[i] = arr1[i] + arr2[i];
        }
        return arr3;
    }

}
