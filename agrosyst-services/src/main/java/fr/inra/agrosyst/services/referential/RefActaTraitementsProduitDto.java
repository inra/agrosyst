package fr.inra.agrosyst.services.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2025 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCountry;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class RefActaTraitementsProduitDto {

    /**
     * Nom de l'attribut en BD : id_produit
     */
    protected String id_produit;

    /**
     * Nom de l'attribut en BD : nom_produit
     */
    protected String nom_produit;

    /**
     * Nom de l'attribut en BD : id_traitement
     */
    protected int id_traitement;

    /**
     * Nom de l'attribut en BD : code_traitement
     */
    protected String code_traitement;

    /**
     * Nom de l'attribut en BD : nom_traitement
     */
    protected String nom_traitement;

    /**
     * Nom de l'attribut en BD : nodu
     */
    protected boolean nodu;

    /**
     * Nom de l'attribut en BD : etat_usage
     */
    protected String etat_usage;

    /**
     * Nom de l'attribut en BD : date_retrait_produit
     */
    protected LocalDate date_retrait_produit;

    /**
     * Nom de l'attribut en BD : date_autorisation_produit
     */
    protected LocalDate date_autorisation_produit;

    /**
     * Nom de l'attribut en BD : source
     */
    protected String source;

    /**
     * Nom de l'attribut en BD : code_AMM
     */
    protected String code_AMM;

    /**
     * Nom de l'attribut en BD : code_traitement_maa
     */
    protected String code_traitement_maa;

    /**
     * Nom de l'attribut en BD : nom_traitement_maa
     */
    protected String nom_traitement_maa;

    /**
     * Nom de l'attribut en BD : active
     */
    protected boolean active;

    /**
     * Nom de l'attribut en BD : refCountry
     */
    protected RefCountry refCountry;
}
