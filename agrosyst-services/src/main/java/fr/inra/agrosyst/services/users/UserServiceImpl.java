package fr.inra.agrosyst.services.users;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.entities.security.AgrosystUserTopiaDao;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.entities.security.UserRoleTopiaDao;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.referential.ImportResult;
import fr.inra.agrosyst.api.services.security.AuthorizationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.api.services.users.FeedbackContext;
import fr.inra.agrosyst.api.services.users.UserDto;
import fr.inra.agrosyst.api.services.users.UserFilter;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.api.services.users.Users;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.CacheService;
import fr.inra.agrosyst.services.common.EmailService;
import fr.inra.agrosyst.services.security.TrackerServiceImpl;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;
import org.nuiton.util.StringUtil;
import org.nuiton.util.pagination.PaginationResult;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class UserServiceImpl extends AbstractAgrosystService implements UserService {
    
    private static final Log LOGGER = LogFactory.getLog(UserServiceImpl.class);
    
    public static final int COST = 12;

    public static String hashPassword(String clearPassword) {
        // to avoid user to ask to renew there password we use a two step authentication.
        // 1rst we use SHA1 as previous to keep compatibility
        // 2nd we transform to Bcrypt to enforce security
        String sha1 = StringUtil.encodeSHA1(Strings.nullToEmpty(clearPassword));
        String hashedPassword = BCrypt.withDefaults().hashToString(COST, sha1.toCharArray());
        return hashedPassword;
    }

    protected EmailService emailService;
    protected TrackerServiceImpl trackerService;
    protected AuthorizationService authorizationService;
    protected CacheService cacheService;

    protected AgrosystUserTopiaDao agrosystUserDao;
    protected UserRoleTopiaDao userRoleDao;

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public void setTrackerService(TrackerServiceImpl trackerService) {
        this.trackerService = trackerService;
    }

    public void setAgrosystUserDao(AgrosystUserTopiaDao agrosystUserDao) {
        this.agrosystUserDao = agrosystUserDao;
    }

    public void setUserRoleDao(UserRoleTopiaDao userRoleDao) {
        this.userRoleDao = userRoleDao;
    }

    public void setAuthorizationService(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    protected void copyFromDto(UserDto dto, AgrosystUser entity) {
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setEmail(StringUtils.lowerCase(dto.getEmail()));
        entity.setOrganisation(dto.getOrganisation());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setBanner(dto.getBanner());
        entity.setItEmail(dto.getItEmail());
        entity.setUserLang(dto.getUserLang());
        entity.setActive(dto.isActive());
    }

    @Override
    public boolean isEmailInUse(String email, String currentUserId) {
        boolean result = false;
        if (!Strings.isNullOrEmpty(email)) {

            AgrosystUser user = agrosystUserDao.forEmailEquals(email.toLowerCase()).findAnyOrNull();
            result = user != null && !user.getTopiaId().equals(currentUserId);
        }
        return result;
    }

    @Override
    public boolean isUserActive(String email) {
        boolean result = false;
        if (!Strings.isNullOrEmpty(email)) {

            AgrosystUser user = agrosystUserDao.forEmailEquals(email.toLowerCase()).findAnyOrNull();
            result = user != null && user.isActive();
        }
        return result;
    }

    @Override
    public boolean isValidEmail(String email) {
        Optional<AgrosystUser> any = agrosystUserDao.forEmailEquals(email.toLowerCase()).tryFindUnique();
        return any.isPresent();
    }

    @Override
    public UserDto createUser(UserDto dto, String password) {

        AgrosystUser user = agrosystUserDao.newInstance();
        copyFromDto(dto, user);
        String hashedPassword = hashPassword(password);
        user.setPassword(hashedPassword);

        AgrosystUser createdUser = agrosystUserDao.create(user);

        trackerService.userCreated(createdUser);

        getTransaction().commit();
        cacheService.clear();
        UserDto result = toUserDto(createdUser);
        return result;
    }

    @Override
    public UserDto getUser(String topiaId) {

        AgrosystUser user = agrosystUserDao.forTopiaIdEquals(topiaId).findUnique();

        UserDto result = toUserDto(user);
        return result;
    }

    @Override
    public UserDto getCurrentUser() {
        String userId = getSecurityContext().getAuthenticatedUserId();
        UserDto result = getUser(userId);
        return result;
    }

    @Override
    public UserDto updateUser(UserDto dto, String password) {

        AgrosystUser user = agrosystUserDao.forTopiaIdEquals(dto.getTopiaId()).findUnique();
        copyFromDto(dto, user);
        boolean passwordChange = false;
        if (!Strings.isNullOrEmpty(password)) {
            String hashedPassword = hashPassword(password);
            user.setPassword(hashedPassword);
            passwordChange = true;
        }
        AgrosystUser updatedUser = agrosystUserDao.update(user);

        trackerService.userModified(updatedUser, passwordChange);

        getTransaction().commit();
        cacheService.clear();
        UserDto result = toUserDto(updatedUser);
        return result;
    }

    @Override
    public void unactivateUsers(Set<String> topiaIds, boolean activate) {
        if (topiaIds != null && !topiaIds.isEmpty()) {

            for (String topiaId : topiaIds) {
                AgrosystUser user = agrosystUserDao.forTopiaIdEquals(topiaId).findUnique();
                user.setActive(activate);
                AgrosystUser updatedUser = agrosystUserDao.update(user);

                trackerService.userActivation(updatedUser, activate);
            }

            getTransaction().commit();
        }
    }

    @Override
    public PaginationResult<UserDto> getFilteredUsers(UserFilter userFilter, boolean includeRoles) {

        PaginationResult<AgrosystUser> usersList = agrosystUserDao.getFilteredUsers(userFilter);

        List<UserDto> transformedElements = usersList.getElements().stream().map(Users.TO_USER_DTO).collect(Collectors.toList());
        PaginationResult<UserDto> result = PaginationResult.of(transformedElements, usersList.getCount(), usersList.getCurrentPage());

        // TODO AThimel 02/10/13 Improve
        if (includeRoles) {
            for (UserDto userDto : result.getElements()) {
                ImmutableSet<RoleType> roleTypes = userRoleDao.findUserRoleTypes(userDto.getTopiaId());
                userDto.setRoles(roleTypes);
            }
        }

        return result;
    }

    @Override
    public List<UserDto> getNameFilteredActiveUsers(String research, Integer nbResult) {

        List<AgrosystUser> usersList = agrosystUserDao.getNameFilteredActiveUsers(research, nbResult);
        List<UserDto> result = usersList.stream()
                .map(Users.TO_USER_DTO)
                .collect(Collectors.toList());
        return result;
    }

    @Override
    public long getUsersCount(Boolean active) {
        if (active == null) {
            return agrosystUserDao.count();
        }
        return agrosystUserDao.forActiveEquals(active).count();
    }

    @Override
    public boolean askForPasswordReminder(String email, String next) {
        Optional<AgrosystUser> any = agrosystUserDao.forEmailEquals(email.toLowerCase()).tryFindUnique();
        boolean result = false;
        if (any.isPresent() && any.get().isActive()) {
            String token = UUID.randomUUID().toString();
            AgrosystUser agrosystUser = any.get();

            agrosystUser.setReminderToken(token);
            getTransaction().commit();
            
            // to ne redirect to login page after connexion
            if (StringUtils.isEmpty(next)) {
                next="index.action";
            }

            emailService.sendPasswordReminder(agrosystUser, token, next);
            result = true;
        }
        return result;
    }

    @Override
    public UserDto preparePasswordChange(String token, String userId) {
        UserDto result = null;
        Optional<AgrosystUser> optional = agrosystUserDao.forProperties(
                AgrosystUser.PROPERTY_TOPIA_ID, userId,
                AgrosystUser.PROPERTY_REMINDER_TOKEN, token).tryFindUnique();
        if (optional.isPresent()) {
            result = Users.TO_USER_DTO.apply(optional.get());
        }
        return result;
    }

    @Override
    public UserDto updatePassword(String token, String userId, String password) {

        Optional<AgrosystUser> optional = agrosystUserDao.forProperties(
                AgrosystUser.PROPERTY_TOPIA_ID, userId,
                AgrosystUser.PROPERTY_REMINDER_TOKEN, token).tryFindUnique();
        if (optional.isPresent()) {
            AgrosystUser agrosystUser = optional.get();
            agrosystUser.setReminderToken(null);
            agrosystUser.setPassword(hashPassword(password));
            getTransaction().commit();
            UserDto userDto = Users.TO_USER_DTO.apply(agrosystUser);
            return userDto;
        }
        return null;
    }

    @Override
    public FeedbackContext sendFeedback(FeedbackContext feedbackContext) {
        Preconditions.checkArgument(feedbackContext != null);
        try {
            //user = String.format("%s %s <%s>", agrosystUser.getFirstName(), agrosystUser.getLastName(), agrosystUser.getEmail());
            final AuthenticatedUser userDto = getAuthenticatedUser();
            feedbackContext.setAgrosystUser(userDto);
        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Unable to get current user", eee);
            }
        }
        return emailService.sendFeedback(feedbackContext);
    }

    @Override
    public ImportResult importUsers(InputStream userFileStream) {

        ImportResult result = new ImportResult();
        UserImportModel model = new UserImportModel();
        try (Import<AgrosystUser> importer = Import.newImport(model, userFileStream)) {
            for (AgrosystUser entity : importer) {

                // make sure email is in lower case
                if (entity.getEmail() != null) {
                    entity.setEmail(entity.getEmail().toLowerCase());
                }

                AgrosystUser agrosystUser = agrosystUserDao.forEmailEquals(entity.getEmail()).findAnyOrNull();
                if (agrosystUser != null) {
                    // as requested, do nothing for existing accounts
                    result.incIgnored();
                } else {

                    // create token
                    String token = UUID.randomUUID().toString();
                    entity.setReminderToken(hashPassword(token));

                    // generate password
                    String password = RandomStringUtils.randomAlphanumeric(8);
                    entity.setPassword(hashPassword(password));

                    // create user
                    entity.setActive(true);
                    agrosystUser = agrosystUserDao.create(entity);

                    // send notification
                    try {
                        emailService.sendCreatedAccountNotification(agrosystUser, token);
                    } catch (AgrosystTechnicalException exception) {
                        String errorMessage = "Unable to send created account email, for user user with email :%s.";
                        errorMessage = String.format(errorMessage, agrosystUser.getEmail());
                        result.addError(errorMessage);
                    }

                    result.incCreated();

                    trackerService.userCreated(agrosystUser);
                }
            }

            getTransaction().commit();
        }
        
        return result;
    }

    @Override
    public void acceptCharter() {

        String userId = getSecurityContext().getUserId();
        AgrosystUser agrosystUser = agrosystUserDao.forTopiaIdEquals(userId).findUnique();
        agrosystUser.setCharterVersion(Users.CURRENT_CHART_VERSION);

        agrosystUserDao.update(agrosystUser);

        getTransaction().commit();
    }

    protected UserDto toUserDto(AgrosystUser agrosystUser) {
        UserDto userDto = Users.TO_USER_DTO.apply(agrosystUser);
        ImmutableSet<RoleType> roleTypes = userRoleDao.findUserRoleTypes(agrosystUser.getTopiaId());
        userDto.setRoles(roleTypes);
        return userDto;
    }

    @Override
    public java.util.Optional<LocalDateTime> getLastMessageReadDate(String userId) {
        AgrosystUser agrosystUser = agrosystUserDao.forTopiaIdEquals(userId).findUnique();
        LocalDateTime date = agrosystUser.getLastMessageReadDate();
        java.util.Optional<LocalDateTime> result = java.util.Optional.ofNullable(date);
        return result;
    }

    @Override
    public void readInfoMessages() {

        String userId = getSecurityContext().getAuthenticatedUserId();
        AgrosystUser agrosystUser = agrosystUserDao.forTopiaIdEquals(userId).findUnique();
        agrosystUser.setLastMessageReadDate(LocalDateTime.now());

        agrosystUserDao.update(agrosystUser);

        getTransaction().commit();
    }

}
