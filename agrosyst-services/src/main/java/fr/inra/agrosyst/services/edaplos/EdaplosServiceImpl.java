package fr.inra.agrosyst.services.edaplos;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.entities.Downloadable;
import fr.inra.agrosyst.api.entities.DownloadableTopiaDao;
import fr.inra.agrosyst.api.entities.edaplos.EdaplosImport;
import fr.inra.agrosyst.api.entities.edaplos.EdaplosImportImpl;
import fr.inra.agrosyst.api.entities.edaplos.EdaplosImportTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.security.AgrosystUserTopiaDao;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.edaplos.EdaplosParsingResult;
import fr.inra.agrosyst.api.services.edaplos.EdaplosParsingStatus;
import fr.inra.agrosyst.api.services.edaplos.EdaplosService;
import fr.inra.agrosyst.api.services.security.AuthorizationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.common.EmailService;
import fr.inra.agrosyst.services.edaplos.model.CropDataSheetMessages;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Services to upload eDaplos
 *
 * @author eancelet@orleans.inra.fr
 */
public class EdaplosServiceImpl extends AbstractAgrosystService implements EdaplosService {

    private static final Log LOGGER = LogFactory.getLog(EdaplosServiceImpl.class);
    
    protected EmailService emailService;
    protected AuthorizationService authorizationService;
    
    protected EdaplosImportTopiaDao edaplosImportTopiaDao;
    protected AgrosystUserTopiaDao agrosystUserTopiaDao;
    protected RefLocationTopiaDao refLocationDao;
    protected DownloadableTopiaDao downloadableTopiaDao;

    @Override
    public String storeEdaplosFile(InputStream inputStream, String fileName, String contentType) {

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime expiresOn = now.plusDays(1);

        byte[] bytes;
        try {
            bytes = IOUtils.toByteArray(inputStream);
        } catch (IOException ioe) {
            throw new AgrosystTechnicalException("Unable to read stream", ioe);
        }

        // copy file into blob
        Session hibernateSession = getPersistenceContext().getHibernateSupport().getHibernateSession();
        Blob blob = hibernateSession.getLobHelper().createBlob(bytes);

        final Downloadable downloadable = downloadableTopiaDao.create(
                Downloadable.PROPERTY_FILE_NAME, fileName,
                Downloadable.PROPERTY_EXPIRES_ON, expiresOn,
                Downloadable.PROPERTY_MIME_TYPE, contentType,
                Downloadable.PROPERTY_CONTENT, blob
        );

        // Fichier écrit en base, on a besoin de commiter
        getTransaction().commit();

        String topiaId = downloadable.getTopiaId();
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info(String.format("eDaplos file '%s' stored with id='%s'", fileName, topiaId));
        }
        return topiaId;
    }

    @Override
    public EdaplosParsingResult validEdaplosData(String edaplosFileId) {
        Downloadable downloadable = downloadableTopiaDao.forTopiaIdEquals(edaplosFileId).findAny();

        try (InputStream inputStream = downloadable.getContent().getBinaryStream()) {
            return this.validEdaplosData(inputStream, downloadable.getFileName());
        } catch (SQLException | IOException ex) {
            throw new AgrosystTechnicalException("Can't get file content from database", ex);
        }
    }

    @Override
    public EdaplosParsingResult importEdaplos(String edaplosFileId) {
        Downloadable downloadable = downloadableTopiaDao.forTopiaIdEquals(edaplosFileId).findAny();

        try (InputStream inputStream = downloadable.getContent().getBinaryStream()) {
            return this.importEdaplos(inputStream, downloadable.getFileName());
        } catch (SQLException | IOException ex) {
            throw new AgrosystTechnicalException("Can't get file content from database", ex);
        }
    }

    @Override
    public InputStream exportCSVEdaplosReport(String edaplosFileId) {
        Downloadable downloadable = downloadableTopiaDao.forTopiaIdEquals(edaplosFileId).findAny();

        try (InputStream inputStream = downloadable.getContent().getBinaryStream()) {
            return this.exportCSVEdaplosReport(inputStream, downloadable.getFileName());
        } catch (SQLException | IOException ex) {
            throw new AgrosystTechnicalException("Can't get file content from database", ex);
        }
    }

    /**
     * return the result of Edaplos parsing
     * @param inputStream the Edaplos file
     * @return the result of Edaplos parsing
     */
    public EdaplosParsingResult validEdaplosData(InputStream inputStream, String fileName) {
        try {
            EdaplosParsingResult edaplosParsingResult = loadAndSave(inputStream);
            edaplosParsingResult.setFilename(fileName);
            return edaplosParsingResult;
        } finally {
            getTransaction().rollback(); // important
        }
    }

    /**
     * Charge le fichier est sauvegarde tout ce qui est possible en base, sans committer ou rollbacker la transaction.
     */
    protected EdaplosParsingResult loadAndSave(InputStream inputStream) {
        EdaplosParsingResult results;

        try (InputStream is = new BufferedInputStream(inputStream)) {
            CropDataSheetMessages cropDataSheetMessages = loadZipOrRegularFile(is);
            results = validateMemoryModel(cropDataSheetMessages);
        } catch (IOException | SAXException ex) {
            throw new AgrosystTechnicalException("Can't parse xml file", ex);
        }

        return results;
    }

    protected CropDataSheetMessages loadZipOrRegularFile(InputStream inputStream) throws IOException, SAXException {

        EdaplosDigesterParser parser = new EdaplosDigesterParser();

        // test to read input stream once with zip inut stream
        // ma it to reset read after failure
        inputStream.mark(1024);

        try (ZipInputStream zipInputStream = new ZipInputStream(inputStream)){
            ZipEntry nextEntry = zipInputStream.getNextEntry();
            boolean isZipped = nextEntry != null;
            if (isZipped) {
                zipEntry(parser, zipInputStream, nextEntry);
            } else {
                // Not a zip : just get the input stream
                inputStream.reset();
                parser.parse(inputStream);
            }
        }

        return parser.getCropDataSheetMessages();
    }

    private void zipEntry(EdaplosDigesterParser parser, ZipInputStream zipInputStream, ZipEntry nextEntry) throws IOException, SAXException {
        while (nextEntry != null) {
            try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                IOUtils.copy(zipInputStream, out);
                try(InputStream is = new ByteArrayInputStream(out.toByteArray())) {
                    parser.parse(is);
                }

                // hack to set file name
                parser.getCropDataSheetMessages().getCropDataSheetMessages()
                        .get(parser.getCropDataSheetMessages().getCropDataSheetMessages().size() - 1)
                        .setFilename(nextEntry.getName());
            }

            // next
            zipInputStream.closeEntry();
            nextEntry = zipInputStream.getNextEntry();
        }
    }

    protected EdaplosParsingResult validateMemoryModel(CropDataSheetMessages cropDataSheetMessages) {
        EdaplosParsingResult results = new EdaplosParsingResult();

        EdaplosBeanValidator beanValidator = new EdaplosBeanValidator();
        beanValidator.validate(cropDataSheetMessages, results);

        String defaultEquipmentCode = getConfig().getRefMaterielDefaultEquipmentCode();
        String defaultIrrigationCode = getConfig().getRefMaterielDefaultIrrigationCode();
        String defaultTractorCode = getConfig().getRefMaterielDefaultTractorCode();
        String defaultAutomoteurCode = getConfig().getRefMaterielDefaultAutomoteurCode();
        
        // parsing
        EdaplosPersister edaplosPersister = newInstance(EdaplosPersister.class);
        edaplosPersister.importDefaultReferential();
        edaplosPersister.persist(results, defaultEquipmentCode, defaultIrrigationCode, defaultTractorCode, defaultAutomoteurCode, cropDataSheetMessages);

        return results;
    }

    /**
     * Import parse data
     * @return import result as class name, result
     */
    @Override
    public EdaplosParsingResult importEdaplos(InputStream inputStream, String fileName) {

        Preconditions.checkArgument(StringUtils.isNotEmpty(fileName), "Le nom de fichier est obligatoire");
        EdaplosParsingResult edaplosParsingResult;

        AuthenticatedUser authenticatedUser = getAuthenticatedUser();
        try {
            edaplosParsingResult = loadAndSave(inputStream);
            edaplosParsingResult.setFilename(fileName);

            if (edaplosParsingResult.getEdaplosParsingStatus() == EdaplosParsingStatus.SUCCESS) {

                if (authenticatedUser != null && authenticatedUser.getEmail() != null) { // if for test
                    sendReportEmail(authenticatedUser, edaplosParsingResult);
                }

                if (authenticatedUser != null && authenticatedUser.getTopiaId() != null) { // if for test
                    addImportLogEntry(authenticatedUser, edaplosParsingResult);
                }
                
                authorizationService.markAsDirtyForAllUsers();
                
                getTransaction().commit();
            } else {
                getTransaction().rollback();
            }
        } catch (Exception ex) {
            edaplosParsingResult = new EdaplosParsingResult();
            edaplosParsingResult.addErrorMessage("L'import eDaplos a échoué, veuillez contacter l'équipe Agrosyst", "");
            edaplosParsingResult.setEdaplosParsingStatus(EdaplosParsingStatus.EXCEPTION);
            if (authenticatedUser != null && authenticatedUser.getEmail() != null) { // if for test
                try {
                    sendExceptionFeedbackEmail(ex, " Import Edaplos", fileName);
                } catch (Exception e) {
                    // noting to do
                }
            }
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Can't import edaplos", ex);
            }

        } finally {
            getTransaction().rollback();
        }

        return edaplosParsingResult;
    }

    private void addImportLogEntry(AuthenticatedUser authenticatedUser, EdaplosParsingResult edaplosParsingResult) {

        EdaplosImport edaplosImport = new EdaplosImportImpl();
        edaplosImport.setImportDate(context.getCurrentTime());
        edaplosImport.setAgrosystUser(agrosystUserTopiaDao.forTopiaIdEquals(authenticatedUser.getTopiaId()).findUnique());
        edaplosImport.setImportFileName(edaplosParsingResult.getFilename());
        edaplosImport.setDocumentIdentification(edaplosParsingResult.getDocumentIdentification());
        edaplosImport.setDocumentTypeCode(edaplosParsingResult.getDocumentTypeCode());
        edaplosImport.setIssuerIdentification(edaplosParsingResult.getIssuerIdentification());
        edaplosImport.setIssuerName(edaplosParsingResult.getIssuerName());
        edaplosImport.setSoftwareName(edaplosParsingResult.getSoftwareName());
        edaplosImport.setSoftwareVersion(edaplosParsingResult.getSoftwareVersion());
        edaplosImport.setCampaigns(edaplosParsingResult.getStudiedCampaigns());
        edaplosImportTopiaDao.create(edaplosImport);
    }

    @Override
    public InputStream exportCSVEdaplosReport(InputStream inputStream, String fileName) {
        EdaplosParsingResult edaplosParsingResult = validEdaplosData(inputStream, fileName);
        return reportAsCsv(edaplosParsingResult);
    }

    @Override
    public void sendExceptionFeedbackEmail(Exception exception, String context, String fileName) {
        AuthenticatedUser authenticatedUser = getAuthenticatedUser();
        emailService.sendEDaplosExceptionFeedback(authenticatedUser, exception, context, fileName);
    }

    @Override
    public void sendExceptionFeedbackEmail(String edaplosFileId, Exception exception, String context) {
        Downloadable downloadable = downloadableTopiaDao.forTopiaIdEquals(edaplosFileId).findAny();
        sendExceptionFeedbackEmail(exception, context, downloadable.getFileName());
    }

    protected InputStream reportAsCsv(EdaplosParsingResult edaplosParsingResult) {
        EdaplosCSVExporter exporter = new EdaplosCSVExporter();
        return exporter.export(edaplosParsingResult);
    }

    protected void sendReportEmail(AuthenticatedUser authenticatedUser, EdaplosParsingResult edaplosParsingResult) {
        InputStream stream = reportAsCsv(edaplosParsingResult);
        emailService.sendEDaplosReport(authenticatedUser, edaplosParsingResult.getFilename(), stream);
    }
    
    @Override
    public RefLocation searchCommune(String postCode, String researchedCommuneName) {
        
        List<RefLocation> sameCPLocations = refLocationDao.forCodePostalEquals(postCode).addEquals(RefLocation.PROPERTY_ACTIVE, true).findAll();
        
        RefLocation location = null;
        String normalizedResearchedCommuneName = CommonService.NORMALISE.apply(researchedCommuneName);
        for (RefLocation sameCPLocation : sameCPLocations) {
            String nomralisedName = CommonService.NORMALISE.apply(sameCPLocation.getCommune());
            if (normalizedResearchedCommuneName.equals(nomralisedName)) {
                location = sameCPLocation;
                break;
            }
        }
        
        return location;
    }
    
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }
    
    public void setAuthorizationService(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }
    
    public void setEdaplosImportTopiaDao(EdaplosImportTopiaDao edaplosImportTopiaDao) {
        this.edaplosImportTopiaDao = edaplosImportTopiaDao;
    }
    
    public void setAgrosystUserTopiaDao(AgrosystUserTopiaDao agrosystUserTopiaDao) {
        this.agrosystUserTopiaDao = agrosystUserTopiaDao;
    }
    
    public void setRefLocationDao(RefLocationTopiaDao refLocationDao) {
        this.refLocationDao = refLocationDao;
    }
    
    public void setDownloadableTopiaDao(DownloadableTopiaDao downloadableTopiaDao) {
        this.downloadableTopiaDao = downloadableTopiaDao;
    }
}
