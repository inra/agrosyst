package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.VariableType;
import fr.inra.agrosyst.api.entities.measure.MeasurementType;
import fr.inra.agrosyst.api.entities.referential.RefMesure;
import fr.inra.agrosyst.api.entities.referential.RefMesureImpl;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

/**
 * Data model import for {@link RefMesure}.
 *
 * @author <a href="mailto:sebastien.grimault@makina-corpus.com">S. Grimault</a>
 */
public class RefMesureModel extends AbstractAgrosystModel<RefMesure> implements ExportModel<RefMesure> {

    protected static final ValueFormatter<MeasurementType> MEASUREMENT_TYPE_FORMATTER = value -> {
        String result;
        if (value != null) {
            result = value.name().toLowerCase();
        } else {
            result = "";
        }
        return result;
    };

    protected static final ValueParser<MeasurementType> MEASUREMENT_TYPE_PARSER = value -> {
        MeasurementType result = null;
        if (!Strings.isNullOrEmpty(value)) {
            String strValue = StringUtils.stripAccents(value);
            strValue = strValue.replaceAll("\\W", "_"); // non word chars
            strValue = strValue.toUpperCase();
            result = MeasurementType.valueOf(strValue);
        }
        return result;
    };

    protected static final ValueFormatter<VariableType> VARIABLE_TYPE_FORMATTER = value -> {
        String result;
        if (value != null) {
            result = value.name().toLowerCase();
        } else {
            result = "";
        }
        return result;
    };

    protected static final ValueParser<VariableType> VARIABLE_TYPE_PARSER = value -> {
        VariableType result = null;
        if (!Strings.isNullOrEmpty(value)) {
            String strValue = StringUtils.stripAccents(value);
            strValue = strValue.replaceAll("\\W", "_"); // non word chars
            strValue = strValue.toUpperCase();
            result = VariableType.valueOf(strValue);
        }
        return result;
    };

    public RefMesureModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Catégorie de mesure", RefMesure.PROPERTY_CATEGORIE_DE_MESURE, MEASUREMENT_TYPE_PARSER);
        newMandatoryColumn("Type variable mesurée", RefMesure.PROPERTY_TYPE_VARIABLE_MESUREE, VARIABLE_TYPE_PARSER);
        newMandatoryColumn("variable mesurée", RefMesure.PROPERTY_VARIABLE_MESUREE);
        newMandatoryColumn("unité", RefMesure.PROPERTY_UNITE);
        newMandatoryColumn("valeur attendue", RefMesure.PROPERTY_VALEUR_ATTENDUE);
        newMandatoryColumn("Remarque", RefMesure.PROPERTY_REMARQUE);
        newOptionalColumn(COLUMN_ACTIVE, RefMesure.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public RefMesure newEmptyInstance() {
        RefMesureImpl refMesure = new RefMesureImpl();
        refMesure.setActive(true);
        return refMesure;
    }

    @Override
    public Iterable<ExportableColumn<RefMesure, Object>> getColumnsForExport() {
        ModelBuilder<RefMesure> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Catégorie de mesure", RefMesure.PROPERTY_CATEGORIE_DE_MESURE, MEASUREMENT_TYPE_FORMATTER);
        modelBuilder.newColumnForExport("Type variable mesurée", RefMesure.PROPERTY_TYPE_VARIABLE_MESUREE, VARIABLE_TYPE_FORMATTER);
        modelBuilder.newColumnForExport("variable mesurée", RefMesure.PROPERTY_VARIABLE_MESUREE);
        modelBuilder.newColumnForExport("unité", RefMesure.PROPERTY_UNITE);
        modelBuilder.newColumnForExport("valeur attendue", RefMesure.PROPERTY_VALEUR_ATTENDUE);
        modelBuilder.newColumnForExport("Remarque", RefMesure.PROPERTY_REMARQUE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefMesure.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
}
