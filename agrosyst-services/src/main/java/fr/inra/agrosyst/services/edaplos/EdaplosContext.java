package fr.inra.agrosyst.services.edaplos;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.services.edaplos.EdaplosParsingResult;
import fr.inra.agrosyst.services.edaplos.model.PlotAgriculturalProcess;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
public class EdaplosContext {

    protected final Map<SeedType, String> seedTypeTranslations;
    protected EdaplosParsingResult result;

    protected Domain domain;
    protected Plot plot;
    protected Zone zone;
    protected CroppingPlanEntry croppingPlanEntry;
    protected EffectiveCropCyclePhase effectivePhase;
    protected EffectiveCropCycleNode effectiveNode;
    protected EffectiveIntervention effectiveIntervention;
    protected Set<String> issuerIds;
    protected PlotAgriculturalProcess plotAgriculturalProcess;
    protected Map<String, CroppingPlanSpecies> croppingPlanSpeciesByTypeCode = new HashMap<>();

    // nombre à ajouter aux faux identifiants (ex : faux siret)
    // pour pouvoir les ajouter dans la map et que les erreurs s'affichent pour les utilisateurs
    // incrémenté de 1 à chaque utilisation pour ne pas avoir de doublons
    protected int idSuffix = 1;
    
    protected Set<Pair<String, String>> requestingEquipmentNameForCodeProd = new HashSet<>();
    
    // use to log warning message (display it once for domaine)
    protected boolean isTractorWithDefaultCode = false;
    protected boolean isIrrigationMaterialWithDefaultCode = false;
    protected boolean isEquipmentWithDefaultCode = false;
    protected boolean isAutomoteurDefaultCode = false;
    protected boolean isIncorrectSeedingUnit = false;
    protected List<EdaplosConstants.UnitCode> incorrectUnits = new ArrayList<>();
    protected final Map<String, CroppingPlanSpecies> codeGnisToSpecies = new HashMap<>();
    protected final Map<String, String> speciesCodeToSpeciesBotanicalCode = new HashMap<>();
    protected String croppingName;
    protected Map<InputType, List<AbstractDomainInputStockUnit>> domainInputsByTypes;
    //private ArrayList<Object> domainSeedLotWithTemporaryUsageUnit;// for domain context and sub context

    public EdaplosContext(Map<SeedType, String> seedTypeTranslations, EdaplosParsingResult result) {
        this.seedTypeTranslations = seedTypeTranslations;
        this.result = result;
    }
    
    protected EdaplosContext(EdaplosContext other) {
        this(other.seedTypeTranslations, other.result);

        this.domain = other.domain;
        this.plot = other.plot;
        this.zone = other.zone;
        this.croppingPlanEntry = other.croppingPlanEntry;
        this.effectivePhase = other.effectivePhase;
        this.effectiveNode = other.effectiveNode;
        this.effectiveIntervention = other.effectiveIntervention;
        this.issuerIds = other.issuerIds;
        this.plotAgriculturalProcess = other.plotAgriculturalProcess;
        this.croppingPlanSpeciesByTypeCode = other.getCroppingPlanSpeciesByTypeCode();
        this.domainInputsByTypes = other.getDomainInputsByTypes();

        isTractorWithDefaultCode = other.isTractorWithDefaultCode;
        isIrrigationMaterialWithDefaultCode = other.isTractorWithDefaultCode;
        isEquipmentWithDefaultCode = other.isEquipmentWithDefaultCode;
        isAutomoteurDefaultCode = other.isAutomoteurDefaultCode;
        //domainSeedLotWithTemporaryUsageUnit = other.domainSeedLotWithTemporaryUsageUnit;

    }

    public int getNextSuffix() {
        return idSuffix++;
    }
    
    public void addStudiedCampaign(int campaign) {
        result.addDomainCampaigns(campaign);
    }

    public void formatInfo(String message, Object... args) {
        final String message1 = args == null || args.length == 0 ? message : String.format(message, args);
        result.addInfoMessage(message1, getCurrentPath());
    }
    
    public void formatWarning(String message, Object... args) {
        final String message1 = args == null || args.length == 0 ? message : String.format(message, args);
        result.addWarningMessage(message1, getCurrentPath());
    }

    public void formatError(String message, Object... args) {
        final String message1 = args == null || args.length == 0 ? message : String.format(message, args);
        result.addErrorMessage(message1, getCurrentPath());
    }

    public void setMissingDestinationWarning() {
        result.addInterventionsWithoutDestinationWarningMessage();
    }
    
    public String getSoilOccupation() {
        String result = null;
        if (effectivePhase != null) {
            result = effectivePhase.getType() + " (" + croppingPlanEntry.getName() + ")";
        } else if (effectiveNode != null) {
            result = "Rang " + (effectiveNode.getRank() + 1) + " (" + croppingPlanEntry.getName() + ")";
        }
        return result;
    }
    
    public EdaplosContext ofDomain(Domain domain, Map<InputType, List<AbstractDomainInputStockUnit>> domainInputsByTypes) {
        EdaplosContext subContext = new EdaplosContext(this);
        subContext.domain = domain;
        subContext.domainInputsByTypes = domainInputsByTypes;
        return subContext;
    }


//    public void addDomainSeedLotWithTemporaryUsageUnit(DomainSeedLotInput domainSeedLotInput) {
//        this.domainSeedLotWithTemporaryUsageUnit.add(domainSeedLotInput);
//    }

//    public DomainSeedLotInput removeAndGetDomainSeedLotWithTemporaryUsageUnit(DomainSeedLotInput domainSeedLotInput) {
//        boolean remove = this.domainSeedLotWithTemporaryUsageUnit.remove(domainSeedLotInput);
//        if (remove) {
//            return domainSeedLotInput;
//        } else {
//            return null;
//        }
//    }

    public EdaplosContext ofPlotAndZone(Plot plot, Zone zone) {
        EdaplosContext subContext = new EdaplosContext(this);
        subContext.plot = plot;
        subContext.zone = zone;
        return subContext;
    }

    public EdaplosContext ofCroppingPlanEntry(CroppingPlanEntry croppingPlanEntry) {
        EdaplosContext subContext = new EdaplosContext(this);
        subContext.croppingPlanEntry = croppingPlanEntry;
        subContext.codeGnisToSpecies.putAll(this.codeGnisToSpecies);
        subContext.speciesCodeToSpeciesBotanicalCode.putAll(this.speciesCodeToSpeciesBotanicalCode);
        return subContext;
    }

    public EdaplosContext ofEffectivePhase(EffectiveCropCyclePhase effectivePhase) {
        EdaplosContext subContext = new EdaplosContext(this);
        subContext.effectivePhase = effectivePhase;
        subContext.codeGnisToSpecies.putAll(this.codeGnisToSpecies);
        subContext.speciesCodeToSpeciesBotanicalCode.putAll(this.speciesCodeToSpeciesBotanicalCode);
        return subContext;
    }

    public EdaplosContext ofEffectiveNode(EffectiveCropCycleNode effectiveNode) {
        EdaplosContext subContext = new EdaplosContext(this);
        subContext.effectiveNode = effectiveNode;
        subContext.codeGnisToSpecies.putAll(this.codeGnisToSpecies);
        subContext.speciesCodeToSpeciesBotanicalCode.putAll(this.speciesCodeToSpeciesBotanicalCode);
        return subContext;
    }

    public EdaplosContext ofEffectiveIntervention(EffectiveIntervention effectiveIntervention) {
        EdaplosContext subContext = new EdaplosContext(this);
        subContext.effectiveIntervention = effectiveIntervention;
        subContext.codeGnisToSpecies.putAll(this.codeGnisToSpecies);
        subContext.speciesCodeToSpeciesBotanicalCode.putAll(this.speciesCodeToSpeciesBotanicalCode);
        return subContext;
    }

    public String getCurrentPath() {
        StringBuilder builder = new StringBuilder();

        if (domain != null) {
            builder.append("Domaine : ").append(domain.getName()).append(" - ").append(domain.getCampaign())
                    .append(" (siret = ").append(domain.getSiret()).append(")")
                    .append('\n');
        }
        if (plot != null) {
            builder.append("Parcelle : ").append(plot.getName())
                    .append(" (id = ").append(plot.geteDaplosIssuerId()).append(")")
                    .append('\n');
        }
        if (zone != null) {
            builder.append("Zone : ").append(Strings.nullToEmpty(zone.getName())).append('\n');
        }
        if (croppingPlanEntry != null) {
            builder.append("Culture : ").append(croppingPlanEntry.getName()).append('\n');
        }
        if (effectiveIntervention != null) {
            builder.append("Intervention : ").append(effectiveIntervention.getName());
            if (effectiveIntervention.getEndInterventionDate() != null) {
                builder.append(" du ")
                        .append(EdaplosUtils.formatInterventionDate(effectiveIntervention.getStartInterventionDate()));
            }
            if (effectiveIntervention.getEndInterventionDate() != null) {
                builder.append(" au ")
                        .append(EdaplosUtils.formatInterventionDate(effectiveIntervention.getEndInterventionDate()));
            }
            builder.append(" (id = ").append(effectiveIntervention.getEdaplosIssuerId()).append(")");
            builder.append('\n');
        }
        if (effectivePhase != null) {
            builder.append("Phase : ").append(effectivePhase.getType())
                    .append(" (id = ").append(effectivePhase.getEdaplosIssuerId()).append(")")
                    .append('\n');
        }
        if (effectiveNode != null) {
            builder.append("Rang : ").append(effectiveNode.getRank() + 1)
                    .append(" (id = ").append(effectiveNode.getEdaplosIssuerId()).append(")")
                    .append('\n');
        }
        return builder.toString();
    }
    
    public void addGnisCodeToVariety(String varietyCode, CroppingPlanSpecies speciesWithVariety) {
        if (StringUtils.isNotBlank(varietyCode) && speciesWithVariety != null) {
            String lvarietyCode = varietyCode.toLowerCase(Locale.ROOT).strip();
            codeGnisToSpecies.put(lvarietyCode, speciesWithVariety);
        }
    }

    public void addSpeciesCodeToSpeciesBotanicalCode(String speciesBotanicalCode, CroppingPlanSpecies species) {
        if (StringUtils.isNotBlank(speciesBotanicalCode) && species != null) {
            speciesCodeToSpeciesBotanicalCode.put(species.getCode(), speciesBotanicalCode.toLowerCase(Locale.ROOT).strip());
        }
    }

    public void addIncorrectUnitCode(EdaplosConstants.UnitCode unitCode) {
        this.incorrectUnits.add(unitCode);
    }
}
