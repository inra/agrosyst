package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.inra.agrosyst.services.edaplos.EdaplosParsingPayload;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * {@code
 * <!--Parsée car considérée comme une inter réalisée (sans qualifiant)-->
 * <ram:ApplicablePlotAgriculturalProcess>
 * <ram:Identification>ID</ram:Identification>
 * <ram:IDGroup></ram:IDGroup>
 * <ram:Status>9</ram:Status>
 * <ram:SubordinateTypeCode listAgencyName="FR, AGRO EDI EUROPE" listAgencyID="312">ABD02</ram:SubordinateTypeCode>
 *
 * <ram:UsedAgriculturalProcessCropInput>
 * <ram:Identification>Code Azonte</ram:Identification>
 * <ram:TypeCode listAgencyName="FR, AGRO EDI EUROPE" listAgencyID="312">ZJC</ram:TypeCode> <!--Fertilisation minérale-->
 * <ram:AppliedSurfaceUnitValueMeasure unitCode="NAR">161</ram:AppliedSurfaceUnitValueMeasure>
 * </ram:UsedAgriculturalProcessCropInput>
 *
 * <ram:OccurrenceAgriculturalArea>
 * <ram:TypeCode>A38</ram:TypeCode> <!--SPE lisiers-->
 * <ram:ActualMeasure>14.79</ram:ActualMeasure>
 * </ram:OccurrenceAgriculturalArea>
 *
 * <ram:OccurrenceDelimitedPeriod>
 * <ram:StartDateTime>2008-03-08T00:00:00Z</ram:StartDateTime>
 * </ram:OccurrenceDelimitedPeriod>
 *
 * <!-- Travail associé, Message d'erreur si absent -->
 * <ram:PerformedAgriculturalProcessWorkItem>
 * <ram:TypeCode listAgencyName="FR, AGRO EDI EUROPE" listAgencyID="312">SET</ram:TypeCode>
 * </ram:PerformedAgriculturalProcessWorkItem>
 * </ram:ApplicablePlotAgriculturalProcess>
 * }
 * </pre>
 */
public class PlotAgriculturalProcess implements AgroEdiObject {

    public static final String TYPE_EVENEMENT_REALISE = "ZK2";

    /*
    * The list is refered in ticket #9641
    * see https://forge.codelutin.com/issues/8641
    */
    public static final Map<String, String> INTERVENTION_NAME_PER_TYPE_CODE = ImmutableMap.of("ZF9", "Autres travaux agricoles",
            "ZG1", "Indicateur de décision",
            "ZG7", "Intrant",
            "ZF7", "Récolte",
            "ZF8", "Travail du sol");

    protected String identification;

    protected String iDGroup;

    protected String status;

    protected String typeCode;

    protected String subordinateTypeCode;

    protected String description;

    protected String tripNumber;

    protected List<AgriculturalProcessCropInput> usedAgriculturalProcessCropInputs = new ArrayList<>();

    protected List<DelimitedPeriod> occurrenceDelimitedPeriods = new ArrayList<>();

    protected List<AgriculturalProcessWorkItem> performedAgriculturalProcessWorkItems = new ArrayList<>();

    protected List<AgriculturalProcessReason> specifiedAgriculturalProcessReasons = new ArrayList<>();

    protected List<SpecifiedAgriculturalDevice> usedSpecifiedAgriculturalDevices = new ArrayList<>();

    protected List<AgriculturalArea> occurrenceAgriculturalAreas = new ArrayList<>();

    protected List<AgriculturalProduce> harvestedAgriculturalProduces = new ArrayList<>();

    protected List<TechnicalCharacteristic> applicableTechnicalCharacteristics = new ArrayList<>();

    protected List<AgriculturalProcessTargetObject> specifiedAgriculturalProcessTargetObjects = new ArrayList<>();

    @Valid
    @Size(max = 1, message = "Seul le premier stade de culture de l'intervention {identification} est pris en compte", payload = EdaplosParsingPayload.INFO.class)
    protected List<AgriculturalProcessCropStage> specifiedAgriculturalProcessCropStages = new ArrayList<>();

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getiDGroup() {
        return iDGroup;
    }

    public void setiDGroup(String iDGroup) {
        this.iDGroup = iDGroup;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<AgriculturalProcessCropInput> getUsedAgriculturalProcessCropInputs() {
        return usedAgriculturalProcessCropInputs;
    }

    public void setUsedAgriculturalProcessCropInputs(List<AgriculturalProcessCropInput> usedAgriculturalProcessCropInputs) {
        this.usedAgriculturalProcessCropInputs = usedAgriculturalProcessCropInputs;
    }

    public void addUsedAgriculturalProcessCropInput(AgriculturalProcessCropInput usedAgriculturalProcessCropInput) {
        usedAgriculturalProcessCropInputs.add(usedAgriculturalProcessCropInput);
    }

    public List<DelimitedPeriod> getOccurrenceDelimitedPeriods() {
        return occurrenceDelimitedPeriods;
    }

    public void setOccurrenceDelimitedPeriods(List<DelimitedPeriod> occurrenceDelimitedPeriods) {
        this.occurrenceDelimitedPeriods = occurrenceDelimitedPeriods;
    }

    public void addOccurrenceDelimitedPeriod(DelimitedPeriod occurrenceDelimitedPeriod) {
        occurrenceDelimitedPeriods.add(occurrenceDelimitedPeriod);
    }

    public List<AgriculturalProcessWorkItem> getPerformedAgriculturalProcessWorkItems() {
        return performedAgriculturalProcessWorkItems;
    }

    public void setPerformedAgriculturalProcessWorkItems(List<AgriculturalProcessWorkItem> performedAgriculturalProcessWorkItems) {
        this.performedAgriculturalProcessWorkItems = performedAgriculturalProcessWorkItems;
    }

    public void addPerformedAgriculturalProcessWorkItem(AgriculturalProcessWorkItem performedAgriculturalProcessWorkItem) {
        performedAgriculturalProcessWorkItems.add(performedAgriculturalProcessWorkItem);
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getSubordinateTypeCode() {
        return subordinateTypeCode;
    }

    public void setSubordinateTypeCode(String subordinateTypeCode) {
        this.subordinateTypeCode = subordinateTypeCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AgriculturalProcessReason> getSpecifiedAgriculturalProcessReasons() {
        return specifiedAgriculturalProcessReasons;
    }

    public void setSpecifiedAgriculturalProcessReasons(List<AgriculturalProcessReason> specifiedAgriculturalProcessReasons) {
        this.specifiedAgriculturalProcessReasons = specifiedAgriculturalProcessReasons;
    }

    public void addSpecifiedAgriculturalProcessReason(AgriculturalProcessReason specifiedAgriculturalProcessReason) {
        specifiedAgriculturalProcessReasons.add(specifiedAgriculturalProcessReason);
    }

    public List<SpecifiedAgriculturalDevice> getUsedSpecifiedAgriculturalDevices() {
        return usedSpecifiedAgriculturalDevices;
    }

    public void setUsedSpecifiedAgriculturalDevices(List<SpecifiedAgriculturalDevice> usedSpecifiedAgriculturalDevices) {
        this.usedSpecifiedAgriculturalDevices = usedSpecifiedAgriculturalDevices;
    }

    public void addUsedSpecifiedAgriculturalDevice(SpecifiedAgriculturalDevice usedSpecifiedAgriculturalDevice) {
        usedSpecifiedAgriculturalDevices.add(usedSpecifiedAgriculturalDevice);
    }

    public List<AgriculturalProcessCropStage> getSpecifiedAgriculturalProcessCropStages() {
        return specifiedAgriculturalProcessCropStages;
    }

    public void setSpecifiedAgriculturalProcessCropStages(List<AgriculturalProcessCropStage> specifiedAgriculturalProcessCropStages) {
        this.specifiedAgriculturalProcessCropStages = specifiedAgriculturalProcessCropStages;
    }

    public void addSpecifiedAgriculturalProcessCropStage(AgriculturalProcessCropStage specifiedAgriculturalProcessCropStage) {
        specifiedAgriculturalProcessCropStages.add(specifiedAgriculturalProcessCropStage);
    }

    public List<AgriculturalArea> getOccurrenceAgriculturalAreas() {
        return occurrenceAgriculturalAreas;
    }

    public void setOccurrenceAgriculturalAreas(List<AgriculturalArea> occurrenceAgriculturalAreas) {
        this.occurrenceAgriculturalAreas = occurrenceAgriculturalAreas;
    }

    public void addOccurrenceAgriculturalArea(AgriculturalArea occurrenceAgriculturalArea) {
        occurrenceAgriculturalAreas.add(occurrenceAgriculturalArea);
    }

    public List<AgriculturalProduce> getHarvestedAgriculturalProduces() {
        return harvestedAgriculturalProduces;
    }

    public void setHarvestedAgriculturalProduces(List<AgriculturalProduce> harvestedAgriculturalProduces) {
        this.harvestedAgriculturalProduces = harvestedAgriculturalProduces;
    }

    public void addHarvestedAgriculturalProduce(AgriculturalProduce harvestedAgriculturalProduce) {
        harvestedAgriculturalProduces.add(harvestedAgriculturalProduce);
    }

    public List<TechnicalCharacteristic> getApplicableTechnicalCharacteristics() {
        return applicableTechnicalCharacteristics;
    }

    public void setApplicableTechnicalCharacteristics(List<TechnicalCharacteristic> applicableTechnicalCharacteristics) {
        this.applicableTechnicalCharacteristics = applicableTechnicalCharacteristics;
    }

    public void addApplicableTechnicalCharacteristic(TechnicalCharacteristic applicableTechnicalCharacteristic) {
        this.applicableTechnicalCharacteristics.add(applicableTechnicalCharacteristic);
    }

    public List<AgriculturalProcessTargetObject> getSpecifiedAgriculturalProcessTargetObjects() {
        return specifiedAgriculturalProcessTargetObjects;
    }

    public void setSpecifiedAgriculturalProcessTargetObjects(List<AgriculturalProcessTargetObject> specifiedAgriculturalProcessTargetObjects) {
        this.specifiedAgriculturalProcessTargetObjects = specifiedAgriculturalProcessTargetObjects;
    }

    public void addSpecifiedAgriculturalProcessTargetObject(AgriculturalProcessTargetObject agriculturalProcessTargetObject) {
        specifiedAgriculturalProcessTargetObjects.add(agriculturalProcessTargetObject);
    }

    public String getTripNumber() {
        return tripNumber;
    }

    public void setTripNumber(String tripNumber) {
        this.tripNumber = tripNumber;
    }

    @Override
    public String getLocalizedIdentifier() {
        return "intervention '" + identification + "'";
    }
}
