package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.services.edaplos.EdaplosConstants;
import org.apache.commons.lang3.EnumUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

/**
 * <pre>
 * {@code
 * <ram:AgriculturalProcessCropInput>
 * <ram:Identification>Code Azonte</ram:Identification>
 * <ram:Description>solAZ 39 %</ram:Description>
 * <ram:TypeCode listAgencyName="FR, AGRO EDI EUROPE" listAgencyID="312">ZJC</ram:TypeCode> <!--Fertilisation minérale-->
 * <ram:AppliedSurfaceUnitValueMeasure unitCode="NAR">161</ram:AppliedSurfaceUnitValueMeasure>
 *
 * <ram:CropInputChemical>
 * <ram:TypeCode listAgencyName="FR, AGRO EDI EUROPE" listAgencyID="312">NT</ram:TypeCode>
 * <ram:PresencePercent>39</ram:PresencePercent>
 * <ram:ValueMeasure unitCode="NAR">59</ram:ValueMeasure>
 * </ram:CropInputChemical>
 *
 * </ram:AgriculturalProcessCropInput>
 * }
 * </pre>
 */
public class AgriculturalProcessCropInput implements AgroEdiObject {

    public static final String TRAITEMENT_SEMENCE = "ZJG";
    public static final String SEMENCE = "ZJF";
    public static final String PLANTS = "ZJT";

    public static final String CODE_SEMENCE_CERTIFIEES = "ZQA";
    public static final String CODE_SEMENCE_DE_FERME = "ZQD";
    public static final String CODE_SEMENCE_NON_CERTIFIEES = "U50";

    public static final Function<Set<String>, SeedType> FROM_CODE_SEMENCE_TO_SEED_TYPE = codes -> {

        if (codes.containsAll(Arrays.asList(CODE_SEMENCE_CERTIFIEES, CODE_SEMENCE_DE_FERME)) || codes.containsAll(Arrays.asList(CODE_SEMENCE_CERTIFIEES, CODE_SEMENCE_NON_CERTIFIEES))) {
            return SeedType.SEMENCES_DE_FERME_ET_CERTIFIEES;
        } else if (codes.contains(CODE_SEMENCE_CERTIFIEES)) {
            return SeedType.SEMENCES_CERTIFIEES;
        } else if (codes.contains(CODE_SEMENCE_DE_FERME) || codes.contains(CODE_SEMENCE_NON_CERTIFIEES)) {
            return SeedType.SEMENCES_DE_FERME;
        }
        return SeedType.SEMENCES_CERTIFIEES;
    };

    /** Identifiant (dépend du type d’intrant). */
    protected String identification;

    /** Type. */
    protected String typeCode;

    /** Qualifiant (dépend del’intrant, voir http://agroedieurope.fr/fr/referentiels/6.html). */
    protected String subordinateTypeCode;
    
    /** Libellé (nom de l’intrant). */
    protected String description;

    /** Quantité réellement épandue. */
    protected String valueMeasure;

    /** Unité quantité réellement épandue. */
    protected String valueMeasureUnitCode;

    /** Quantité par unité de surface. */
    protected String appliedSurfaceUnitValueMeasure;

    /** Unité de la quantité par unité de surface. */
    protected String appliedSurfaceUnitValueMeasureUnitCode;

    /** Coût unitaire de l'intrant. */
    protected String costUnit;

    /** Unité du coût unitaire de l'intrant. */
    protected String costUnitUnitCode;

    protected List<TechnicalCharacteristic> applicableTechnicalCharacteristics = new ArrayList<>();

    protected List<CropInputChemical> componentCropInputChemicals = new ArrayList<>();

    protected List<AgriculturalProcessTargetObject> specifiedAgriculturalProcessTargetObjects = new ArrayList<>();

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getSubordinateTypeCode() {
        return subordinateTypeCode;
    }

    public void setSubordinateTypeCode(String subordinateTypeCode) {
        this.subordinateTypeCode = subordinateTypeCode;
    }

    public String getAppliedSurfaceUnitValueMeasure() {
        return appliedSurfaceUnitValueMeasure;
    }

    public void setAppliedSurfaceUnitValueMeasure(String appliedSurfaceUnitValueMeasure) {
        this.appliedSurfaceUnitValueMeasure = appliedSurfaceUnitValueMeasure;
    }

    public EdaplosConstants.UnitCode getAppliedSurfaceUnitValueMeasureUnitCode() {
        return EnumUtils.getEnum(EdaplosConstants.UnitCode.class, appliedSurfaceUnitValueMeasureUnitCode);
    }

    public void setAppliedSurfaceUnitValueMeasureUnitCode(String appliedSurfaceUnitValueMeasureUnitCode) {
        this.appliedSurfaceUnitValueMeasureUnitCode = appliedSurfaceUnitValueMeasureUnitCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValueMeasure() {
        return valueMeasure;
    }

    public void setValueMeasure(String valueMeasure) {
        this.valueMeasure = valueMeasure;
    }

    public EdaplosConstants.UnitCode getValueMeasureUnitCode() {
        return EnumUtils.getEnum(EdaplosConstants.UnitCode.class, valueMeasureUnitCode);
    }
    
    public String getValueMeasureUnitCodeOriginalValue() {
        return valueMeasureUnitCode;
    }

    public void setValueMeasureUnitCode(String valueMeasureUnitCode) {
        this.valueMeasureUnitCode = valueMeasureUnitCode;
    }

    public String getCostUnit() {
        return costUnit;
    }

    public void setCostUnit(String costUnit) {
        this.costUnit = costUnit;
    }

    public EdaplosConstants.UnitCode getCostUnitUnitCode() {
        return EnumUtils.getEnum(EdaplosConstants.UnitCode.class, costUnitUnitCode);
    }

    public void setCostUnitUnitCode(String costUnitUnitCode) {
        this.costUnitUnitCode = costUnitUnitCode;
    }

    public List<TechnicalCharacteristic> getApplicableTechnicalCharacteristics() {
        return applicableTechnicalCharacteristics;
    }

    public void setApplicableTechnicalCharacteristics(List<TechnicalCharacteristic> applicableTechnicalCharacteristics) {
        this.applicableTechnicalCharacteristics = applicableTechnicalCharacteristics;
    }

    public void addApplicableTechnicalCharacteristic(TechnicalCharacteristic technicalCharacteristic) {
        applicableTechnicalCharacteristics.add(technicalCharacteristic);
    }

    public List<CropInputChemical> getComponentCropInputChemicals() {
        return componentCropInputChemicals;
    }

    public void setComponentCropInputChemicals(List<CropInputChemical> componentCropInputChemicals) {
        this.componentCropInputChemicals = componentCropInputChemicals;
    }

    public void addComponentCropInputChemical(CropInputChemical componentCropInputChemical) {
        componentCropInputChemicals.add(componentCropInputChemical);
    }

    public List<AgriculturalProcessTargetObject> getSpecifiedAgriculturalProcessTargetObjects() {
        return specifiedAgriculturalProcessTargetObjects;
    }

    public void setSpecifiedAgriculturalProcessTargetObjects(List<AgriculturalProcessTargetObject> specifiedAgriculturalProcessTargetObjects) {
        this.specifiedAgriculturalProcessTargetObjects = specifiedAgriculturalProcessTargetObjects;
    }

    public void addSpecifiedAgriculturalProcessTargetObject(AgriculturalProcessTargetObject agriculturalProcessTargetObject) {
        specifiedAgriculturalProcessTargetObjects.add(agriculturalProcessTargetObject);
    }

    @Override
    public String getLocalizedIdentifier() {
        return "intrant '" + identification + "'";
    }
}
