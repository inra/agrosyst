package fr.inra.agrosyst.services.cron;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Classe qui sert de Helper pour Quartz pour éviter d'utiliser Quartz directement depuis l'application context.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.61
 */
public class CronHelper {

    private static final Log log = LogFactory.getLog(CronHelper.class);

    protected Scheduler cronScheduler;
    protected JobDataMap jobDataMap;

    public CronHelper(Object applicationContext) {

        if (log.isDebugEnabled()) {
            log.debug("Starting CronHelper");
        }

        try {

            jobDataMap = new JobDataMap();
            jobDataMap.put(AbstractAgrosystScheduledJob.CRON_JOB_DATA_APPLICATION_CONTEXT_KEY, applicationContext);

            cronScheduler = new StdSchedulerFactory().getScheduler();
            cronScheduler.start();

        } catch (SchedulerException se) {
            throw new AgrosystTechnicalException("Unable to start cronScheduler", se);
        }
    }

    /**
     * Programme un job de type {@code jobClass} pour une exécution toutes les {@code minutes} minutes.
     *
     * @param minutes  nombre de minutes entre chaque exécution
     * @param jobClass Class du job
     */
    public void scheduleJob(int minutes, Class<? extends AbstractAgrosystScheduledJob> jobClass) {

        if (log.isDebugEnabled()) {
            log.debug(String.format("Programmation de la classe %s toutes les %d minutes", jobClass.getSimpleName(), minutes));
        }

        SimpleScheduleBuilder schedule = SimpleScheduleBuilder
                .simpleSchedule()
                .withIntervalInMinutes(minutes)
                .repeatForever();

        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withSchedule(
                        schedule)
                .build();

        JobDetail job = JobBuilder.newJob(jobClass)
                .usingJobData(jobDataMap)
                .build();

        try {
            cronScheduler.scheduleJob(job, trigger);
        } catch (SchedulerException se) {
            throw new AgrosystTechnicalException("Unable to schedule job for class " + jobClass.getName(), se);
        }
    }

    public void shutdown() {

        if (log.isDebugEnabled()) {
            log.debug("Shutting down CronHelper");
        }

        try {
            if (cronScheduler != null && !cronScheduler.isShutdown()) {
                cronScheduler.shutdown();
            }
        } catch (SchedulerException se) {
            throw new AgrosystTechnicalException("Unable to shutdown cronScheduler", se);
        }

    }
}
