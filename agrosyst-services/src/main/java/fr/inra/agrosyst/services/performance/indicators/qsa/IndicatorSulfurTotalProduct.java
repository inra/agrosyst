package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.performance.UsageQsaResult;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Classe calculant l'indicateur QSA Soufre Total.
 * <p>
 * QSA Soufre total = QSA Soufre phytosanitaire + QSA Soufre fertilisation
 */
public class IndicatorSulfurTotalProduct extends AbstractIndicator {

    private static final String COLUMN_NAME = "qsa_soufre_tot";
    private static final String COLUMN_NAME_HTS = "qsa_soufre_tot_hts";

    protected final String[] indicators = {
            "Indicator.label.sulfurTotalProduct",
            "Indicator.label.sulfurTotalProduct_hts"
    };

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME_HTS);
        return indicatorNameToColumnName;
    }

    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INPUT,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    protected boolean isWithSeedingTreatment = true;
    protected boolean isWithoutSeedingTreatment = true;

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {
        if (interventionContext.isFictive()) {
            return null;
        }
        //  pour la version ferti, il n'y a pas de version d'indicateur hors traitement de semence puisqu'un traitement de semence ne peut pas être un fertilisant
        Optional<Double[]> optionalResult = this.computeQuantities(writerContext, interventionContext);
        return optionalResult.orElse(null);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {
        //  pour la version ferti, il n'y a pas de version d'indicateur hors traitement de semence puisqu'un traitement de semence ne peut pas être un fertilisant
        Optional<Double[]> optionalResult = this.computeQuantities(writerContext, interventionContext);
        return optionalResult.orElse(null);
    }

    private Optional<Double[]> computeQuantities(WriterContext writerContext, PerformanceInterventionContext interventionContext) {
        Double[] quantities = newArray(indicators.length, 0.0d);


        IndicatorWriter writer = writerContext.getWriter();
        List<UsageQsaResult> usageFertiSQuantities = interventionContext.getUsageFertiSQuantities();
        writeUsageInput(usageFertiSQuantities, writer, writerContext, 0);

        List<UsageQsaResult> usagePhytoSQuantities = interventionContext.getUsagePhytoSQuantities();
        writeUsageInput(usagePhytoSQuantities, writer, writerContext, 0);

        List<UsageQsaResult> usagePhytoSQuantitiesHts = interventionContext.getUsagePhytoSQuantitiesHts();
        writeUsageInput(usagePhytoSQuantitiesHts, writer, writerContext, 1);

        if (interventionContext.getFertiSQuantity() != null || interventionContext.getPhytoSQuantity() != null || interventionContext.getPhytoSQuantityHts() != null) {
            quantities[0] = getDouble(interventionContext.getFertiSQuantity()) + getDouble(interventionContext.getPhytoSQuantity());
            quantities[1] = getDouble(interventionContext.getFertiSQuantity()) + getDouble(interventionContext.getPhytoSQuantityHts());

            return Optional.of(quantities);
        }

        return Optional.empty();
    }

    private void writeUsageInput(List<UsageQsaResult> usageFertiCuQuantities, IndicatorWriter writer, WriterContext writerContext, int i) {
        if (isDisplayed(ExportLevel.INPUT, i)) {
            for (UsageQsaResult usageResult : usageFertiCuQuantities) {
                AbstractAction action = usageResult.abstractAction();
                AbstractInputUsage usage = usageResult.usage();
                Double value = usageResult.value();

                Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(usage.getTopiaId());
                String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(
                        usage.getTopiaId(),
                        MissingMessageScope.INPUT);

                writer.writeInputUsage(
                        writerContext.getIts(),
                        writerContext.getIrs(),
                        getIndicatorCategory(),
                        getIndicatorLabel(i),
                        usage,
                        action,
                        writerContext.getEffectiveIntervention(),
                        writerContext.getPracticedIntervention(),
                        writerContext.getCodeAmmBioControle(),
                        writerContext.getAnonymizeDomain(),
                        writerContext.getAnonymizeGrowingSystem(),
                        writerContext.getPlot(),
                        writerContext.getZone(),
                        writerContext.getPracticedSystem(),
                        writerContext.getCroppingPlanEntry(),
                        writerContext.getPracticedPhase(),
                        writerContext.getSolOccupationPercent(),
                        writerContext.getEffectivePhase(),
                        writerContext.getRank(),
                        writerContext.getPreviousPlanEntry(),
                        writerContext.getIntermediateCrop(),
                        this.getClass(),
                        value,
                        reliabilityIndexForInputId,
                        reliabilityCommentForInputId,
                        "",
                        "",
                        "",
                        writerContext.getGroupesCiblesByCode());
            }
        }
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, "Indicator.category.activeSubstances");
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return isRelevant(atLevel) && displayed && (i == 0 && isWithSeedingTreatment || i == 1 && isWithoutSeedingTreatment);
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, indicators[i]);
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
        if (displayed) {
            isWithSeedingTreatment = filter.getWithSeedingTreatment();
            isWithoutSeedingTreatment = filter.getWithoutSeedingTreatment();
        }
    }
}
