package fr.inra.agrosyst.services.common;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.referential.RefInputPriceImpl;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiOrga;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class RefOrganicInputPriceServiceImpl {

    protected Map<? extends AbstractDomainInputStockUnit, ? extends Map<String, Optional<InputRefPrice>>> getRefInputPriceForScenario(
            Collection<String> scenarioCodes,
            Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters,
            Collection<DomainOrganicProductInput> organicInputs,
            Collection<RefPrixFertiOrga> refPrixFertiOrgas) {
    
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> refInputPriceByScenarioCodes = new HashMap<>();
        
        if (CollectionUtils.isEmpty(refPrixFertiOrgas)) {
            for (DomainOrganicProductInput organicInput : organicInputs) {
                scenarioCodes.forEach(scenarioCode -> {
                    Map<String, Optional<InputRefPrice>> rePriceMap = refInputPriceByScenarioCodes.computeIfAbsent(organicInput, k -> new HashMap<>());
                    rePriceMap.put(scenarioCode, Optional.empty());
                });
            }
        } else {
            
            Map<OrganicProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByProductUnit = RefInputPriceServiceImpl.getDoseUnitToPriceUnitConverterByProductUnit(refInputUnitPriceUnitConverters);
            
            Map<String, Set<RefPrixFertiOrga>> refPrixFertiOrgaByIdTypeEffluent = new HashMap<>();
            for (RefPrixFertiOrga refPrixFertiOrga : refPrixFertiOrgas) {
                String idTypeEffluent = refPrixFertiOrga.getIdTypeEffluent();
                Set<RefPrixFertiOrga> refPrices = refPrixFertiOrgaByIdTypeEffluent.computeIfAbsent(idTypeEffluent, k -> new HashSet<>());
                refPrices.add(refPrixFertiOrga);
            }
            
            for (DomainOrganicProductInput organicInput : organicInputs) {
                
                String idtypeeffluent = organicInput.getRefInput().getIdtypeeffluent();
                Set<RefPrixFertiOrga> refPrixFertiOrgas_ = refPrixFertiOrgaByIdTypeEffluent.get(idtypeeffluent);
                Set<RefPrixFertiOrga> refPrices = CollectionUtils.emptyIfNull(refPrixFertiOrgas_)
                        .stream().filter(refPrixFertiOrga -> organicInput.isOrganic() == refPrixFertiOrga.isOrganic())
                        .collect(Collectors.toSet());
                
                // no result found
                if (CollectionUtils.isEmpty(refPrices)) {
                    scenarioCodes.forEach(scenarioCode -> {
                        Map<String, Optional<InputRefPrice>> rePriceMap = refInputPriceByScenarioCodes.computeIfAbsent(organicInput, k -> new HashMap<>());
                        rePriceMap.put(scenarioCode, Optional.empty());
                    });
                }
                
                OrganicProductUnit productUnit = organicInput.getUsageUnit();
                List<RefInputUnitPriceUnitConverter> doseUnitConverters = convertersByProductUnit.get(productUnit);
                
                Map<String, Set<RefPrixFertiOrga>> refPrixFertiOrgaByScenarioCode = new HashMap<>();
                refPrices.forEach(refPrice -> {
                    String scenarioCode = refPrice.getCode_scenario();
                    Set<RefPrixFertiOrga> refPrixFertiOrgasForScenarioCode = refPrixFertiOrgaByScenarioCode.computeIfAbsent(scenarioCode, k -> new HashSet<>());
                    refPrixFertiOrgasForScenarioCode.add(refPrice);
                });
                
                for (String scenarioCode : scenarioCodes) {
    
                    Map<String, Optional<InputRefPrice>> rePriceMap = refInputPriceByScenarioCodes.computeIfAbsent(organicInput, k -> new HashMap<>());
    
                    Set<RefPrixFertiOrga> refPrixFertiOrgasForScenario = refPrixFertiOrgaByScenarioCode.get(scenarioCode);
                    if (refPrixFertiOrgasForScenario == null) {
                        rePriceMap.put(scenarioCode, Optional.empty());
                        continue;
                    }
                    
                    Optional<PriceAndUnit> optPriceAndUnit = RefInputPriceServiceImpl.getAverageRefPrice(refPrixFertiOrgasForScenario, doseUnitConverters, organicInput);
                    InputRefPrice inputRefPrice = optPriceAndUnit.map(
                        scenarioPriceAndUnit -> {
                            double price = scenarioPriceAndUnit.value();
                            PriceUnit priceUnit = scenarioPriceAndUnit.unit();

                            double conversionRate = getConversionRateForInputProduct(organicInput, doseUnitConverters, priceUnit);

                            //L’utilisateur peut saisir un prix en €/ha pour les intrants. Dans ce cas-là, on considère que la dose
                            //d’application de l’intrant vaut 1.
                            double inputsCharges = PricesService.DEFAULT_PRICE_UNIT == priceUnit ? price : conversionRate * price;

                            //L’utilisateur peut saisir un prix en €/ha pour les intrants. Dans ce cas-là, on considère que la dose
                            //d’application de l’intrant vaut 1.
                            return RefInputPriceServiceImpl.getScenarioInputRePrice(organicInput, scenarioCode, new PriceAndUnit(inputsCharges, priceUnit));
                        }
                    ).orElse(null);
                    
                    rePriceMap.put(scenarioCode, Optional.ofNullable(inputRefPrice));

                }
            }
        }
        return refInputPriceByScenarioCodes;
    }
    
    protected Map<? extends AbstractDomainInputStockUnit, ? extends Map<Integer, Optional<InputRefPrice>>> getRefInputPriceForCampaigns(
            Collection<Integer> campaigns,
            Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters,
            Collection<DomainOrganicProductInput> organicInputs,
            Collection<RefPrixFertiOrga> refPrixFertiOrgas) {

        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> refPrixPhytoByCampaigns = new HashMap<>();
        
        if (CollectionUtils.isEmpty(refPrixFertiOrgas)) {
            for (DomainOrganicProductInput organicInput : organicInputs) {
                campaigns.forEach(campaign -> {
                    Map<Integer, Optional<InputRefPrice>> rePriceMap = refPrixPhytoByCampaigns.computeIfAbsent(organicInput, k -> new HashMap<>());
                    rePriceMap.put(campaign, Optional.empty());
                });
            }

        } else {
            
            Map<OrganicProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByProductUnit = RefInputPriceServiceImpl.getDoseUnitToPriceUnitConverterByProductUnit(refInputUnitPriceUnitConverters);
            
            Map<String, Set<RefPrixFertiOrga>> refPrixFertiOrgaByIdTypeEffluent = new HashMap<>();
            for (RefPrixFertiOrga refPrixFertiOrga : refPrixFertiOrgas) {
                String idTypeEffluent = refPrixFertiOrga.getIdTypeEffluent();
                Set<RefPrixFertiOrga> refPrices = refPrixFertiOrgaByIdTypeEffluent.computeIfAbsent(idTypeEffluent, k -> new HashSet<>());
                refPrices.add(refPrixFertiOrga);
            }
            
            for (DomainOrganicProductInput organicInput : organicInputs) {
                
                String idTypeEffluent = organicInput.getRefInput().getIdtypeeffluent();
                Collection<RefPrixFertiOrga> refPrixFertiOrgasForIdTypeEffluent = refPrixFertiOrgaByIdTypeEffluent.get(idTypeEffluent);
                Set<RefPrixFertiOrga> refPrices = CollectionUtils.emptyIfNull(refPrixFertiOrgasForIdTypeEffluent)
                        .stream().filter(refPrixFertiOrga -> organicInput.isOrganic() == refPrixFertiOrga.isOrganic())
                        .collect(Collectors.toSet());
                
                // no result found
                if (CollectionUtils.isEmpty(refPrices)) {
                    campaigns.forEach(campaign -> {
                        Map<Integer, Optional<InputRefPrice>> rePriceMap = refPrixPhytoByCampaigns.computeIfAbsent(organicInput, k -> new HashMap<>());
                        rePriceMap.put(campaign, Optional.empty());
                    });
                }
                
                OrganicProductUnit productUnit = organicInput.getUsageUnit();
                List<RefInputUnitPriceUnitConverter> doseUnitPriceUnitConverters = convertersByProductUnit.get(productUnit);
                
                Map<Integer, Set<RefPrixFertiOrga>> refPrixFertiOrgaByCampaings = new HashMap<>();
                refPrices.forEach(refPrice -> {
                    Integer campaign = refPrice.getCampaign();
                    Set<RefPrixFertiOrga> refPrixFertiOrgasForCampaign = refPrixFertiOrgaByCampaings.computeIfAbsent(campaign, k -> new HashSet<>());
                    refPrixFertiOrgasForCampaign.add(refPrice);
                });
                
                
                for (Integer campaign : campaigns) {
    
                    Map<Integer, Optional<InputRefPrice>> rePriceMap = refPrixPhytoByCampaigns.computeIfAbsent(organicInput, k -> new HashMap<>());
    
                    Set<RefPrixFertiOrga> refPrixFertiOrgasForCampaign = refPrixFertiOrgaByCampaings.get(campaign);
                    if (refPrixFertiOrgasForCampaign == null) {
                        rePriceMap.put(campaign, Optional.empty());
                        
                        continue;
                    }
    
                    Optional<PriceAndUnit> optPriceAndUnit = RefInputPriceServiceImpl.getAverageRefPrice(refPrixFertiOrgasForCampaign, doseUnitPriceUnitConverters, organicInput);
                    InputRefPrice inputRefPrice = optPriceAndUnit.map(
                        priceAndUnit -> {
                            double price = priceAndUnit.value();
                            PriceUnit priceUnit = priceAndUnit.unit();
    
                            double conversionRate = getConversionRateForInputProduct(organicInput, doseUnitPriceUnitConverters, priceUnit);
    
                            //L’utilisateur peut saisir un prix en €/ha pour les intrants. Dans ce cas-là, on considère que la dose
                            //d’application de l’intrant vaut 1.
                            double inputsCharges = PricesService.DEFAULT_PRICE_UNIT == priceUnit ? price : conversionRate * price;
    
                            //L’utilisateur peut saisir un prix en €/ha pour les intrants. Dans ce cas-là, on considère que la dose
                            //d’application de l’intrant vaut 1.
                            PriceAndUnit priceAndUnitValue = new PriceAndUnit(inputsCharges, priceUnit);
                            return RefInputPriceServiceImpl.getCampaignInputRePrice(organicInput, campaign, priceAndUnitValue, false);
                        }
                    ).orElse(null);
    
                    rePriceMap.put(campaign, Optional.ofNullable(inputRefPrice));
                }
            }
        }
        return refPrixPhytoByCampaigns;
    }
    
    protected double getConversionRateForInputProduct(AbstractDomainInputStockUnit input,
                                                      List<RefInputUnitPriceUnitConverter> doseUnitConverters,
                                                      PriceUnit priceUnit) {
        double conversionRate = 0d;
        
        if (doseUnitConverters != null) {
            
            Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> doseUnitToPriceUnitConverterByPriceUnit =
                    RefInputPriceServiceImpl.getConvertersByPriceUnits(doseUnitConverters);
            
            // Map<PriceUnit, RefInputUnitPriceUnitConverter>
            Pair<OrganicProductUnit, Double> conversionRateForDoseUnit = getDoseUnitToPriceUnitConversionRate(
                    doseUnitToPriceUnitConverterByPriceUnit,
                    priceUnit,
                    input);
            
            if (conversionRateForDoseUnit != null && conversionRateForDoseUnit.getRight() != null) {
                conversionRate = conversionRateForDoseUnit.getRight();
            }
            
        }
        return conversionRate;
    }

    // TODO factoriser as RefOrganicInputPriceServiceImpl.getDoseUnitToPriceUnitConversionRate() RefInputPriceServiceImpl.getDoseUnitToPriceUnitConversionRate()
    protected Pair<OrganicProductUnit, Double> getDoseUnitToPriceUnitConversionRate(
            Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> doseUnitToPriceUnitConverterByPriceUnit,
            PriceUnit priceUnit,
            AbstractDomainInputStockUnit input) {
        
        Double conversionRate = null;
        Pair<OrganicProductUnit, Double> conversionRateForDoseUnit = null;
        
        if (doseUnitToPriceUnitConverterByPriceUnit != null) {
            
            List<RefInputUnitPriceUnitConverter> doseUnitToPriceUnitConverters = doseUnitToPriceUnitConverterByPriceUnit.get(priceUnit);
            
            if (CollectionUtils.isNotEmpty(doseUnitToPriceUnitConverters)) {

                conversionRateForDoseUnit = getConversionRateForInput(input, doseUnitToPriceUnitConverters);
            }
            
            if (conversionRateForDoseUnit == null || conversionRateForDoseUnit.getValue() == null) {
                // si pas trouver essayer avec une unité cohérente
                Iterator<Map.Entry<PriceUnit, List<RefInputUnitPriceUnitConverter>>> iter = doseUnitToPriceUnitConverterByPriceUnit.entrySet().iterator();
                while (iter.hasNext() && conversionRate == null) {
                    Map.Entry<PriceUnit, List<RefInputUnitPriceUnitConverter>> priceUnitListEntry = iter.next();
                    PriceUnit to = priceUnitListEntry.getKey();
                    doseUnitToPriceUnitConverters = doseUnitToPriceUnitConverterByPriceUnit.get(to);
                    Double priceUnitConversionRate = RefInputPriceImpl.GET_PRICE_UNIT_TO_OTHER_PRICE_UNIT_CONVERSION_RATE.apply(Pair.of(priceUnit, to));
                    if (priceUnitConversionRate != null && priceUnitConversionRate != 0 && CollectionUtils.isNotEmpty(doseUnitToPriceUnitConverters)) {
                        conversionRateForDoseUnit = getConversionRateForInput(input, doseUnitToPriceUnitConverters);
                        final Double optionalValue = conversionRateForDoseUnit.getRight();
                        conversionRate = optionalValue != null ? optionalValue * priceUnitConversionRate : null;
                        conversionRateForDoseUnit = Pair.of(conversionRateForDoseUnit.getLeft(), conversionRate);
                    }
                }
            }
        }
        
        return conversionRateForDoseUnit;
    }

    protected Pair<OrganicProductUnit, Double> getConversionRateForInput(
            AbstractDomainInputStockUnit input,
            List<RefInputUnitPriceUnitConverter> doseUnitToPriceUnitConverters) {

        OrganicProductUnit productUnit = ((DomainOrganicProductInput) input).getUsageUnit();
        Double conversionRate = doseUnitToPriceUnitConverters.stream()
                .filter(obj -> obj.getOrganicProductUnit() != null && obj.getOrganicProductUnit().equals(productUnit))
                .toList()
                .stream()
                .map(RefInputUnitPriceUnitConverter::getConvertionRate).findAny().orElse(null);

        Pair<OrganicProductUnit, Double> conversionRateForDoseUnit = Pair.of(productUnit, conversionRate);

        return conversionRateForDoseUnit;
    }
}
