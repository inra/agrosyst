package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefGesEngrais;
import fr.inra.agrosyst.api.entities.referential.RefGesEngraisImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Type d'engrais, GES indirects FE (i) (kq eq-CO2/kg N), GES indirects FE (i) (kq eq-CO2/kg P205), GES indirects FE (i) (kq eq-CO2/kg K2O), Source
 * 
 * @author Eric Chatellier
 */
public class RefGesEngraisModel extends AbstractAgrosystModel<RefGesEngrais> implements ExportModel<RefGesEngrais> {

    public RefGesEngraisModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("Type d'engrais", RefGesEngrais.PROPERTY_TYPE_ENGRAIS);
        newMandatoryColumn("GES indirects FE (i) (kq eq-CO2/kg N)", RefGesEngrais.PROPERTY_GES_N, DOUBLE_PARSER);
        newMandatoryColumn("GES indirects FE (i) (kq eq-CO2/kg P205)", RefGesEngrais.PROPERTY_GES_P2O5, DOUBLE_PARSER);
        newMandatoryColumn("GES indirects FE (i) (kq eq-CO2/kg K2O)", RefGesEngrais.PROPERTY_GES_K2O, DOUBLE_PARSER);
        newMandatoryColumn("Source", RefGesEngrais.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefGesEngrais.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefGesEngrais, Object>> getColumnsForExport() {
        ModelBuilder<RefGesEngrais> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Type d'engrais", RefGesEngrais.PROPERTY_TYPE_ENGRAIS);
        modelBuilder.newColumnForExport("GES indirects FE (i) (kq eq-CO2/kg N)", RefGesEngrais.PROPERTY_GES_N, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("GES indirects FE (i) (kq eq-CO2/kg P205)", RefGesEngrais.PROPERTY_GES_P2O5, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("GES indirects FE (i) (kq eq-CO2/kg K2O)", RefGesEngrais.PROPERTY_GES_K2O, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefGesEngrais.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefGesEngrais.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefGesEngrais newEmptyInstance() {
        RefGesEngrais refGesEngrais = new RefGesEngraisImpl();
        refGesEngrais.setActive(true);
        return refGesEngrais;
    }
}
