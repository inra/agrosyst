package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefEspeceToVariete;
import fr.inra.agrosyst.api.entities.referential.RefEspeceToVarieteImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefEspeceToVarieteModel extends AbstractAgrosystModel<RefEspeceToVariete> implements ExportModel<RefEspeceToVariete> {

    public RefEspeceToVarieteModel() {
        super(CSV_SEPARATOR);
        newIgnoredColumn("nom_bota_edi");
        newIgnoredColumn("nom_bota_autre_referentiel");
        newMandatoryColumn("code_espece_edi", RefEspeceToVariete.PROPERTY_CODE_ESPECE_EDI);
        newMandatoryColumn("code_espece_autre_referentiel1", RefEspeceToVariete.PROPERTY_CODE_ESPECE_AUTRE_REFERENTIEL);
        newMandatoryColumn("referentiel source", RefEspeceToVariete.PROPERTY_REFERENTIEL_SOURCE);
        newMandatoryColumn("source", RefEspeceToVariete.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefEspeceToVariete.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefEspeceToVariete, Object>> getColumnsForExport() {
        ModelBuilder<RefEspeceToVariete> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("code_espece_edi", RefEspeceToVariete.PROPERTY_CODE_ESPECE_EDI);
        modelBuilder.newColumnForExport("code_espece_autre_referentiel1", RefEspeceToVariete.PROPERTY_CODE_ESPECE_AUTRE_REFERENTIEL);
        modelBuilder.newColumnForExport("referentiel source", RefEspeceToVariete.PROPERTY_REFERENTIEL_SOURCE);
        modelBuilder.newColumnForExport("source", RefEspeceToVariete.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefEspeceToVariete.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefEspeceToVariete newEmptyInstance() {
        RefEspeceToVariete result = new RefEspeceToVarieteImpl();
        result.setActive(true);
        return result;
    }

}
