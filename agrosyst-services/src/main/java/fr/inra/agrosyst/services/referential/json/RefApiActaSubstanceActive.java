package fr.inra.agrosyst.services.referential.json;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActiveImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.Serial;

/**
 * Created by davidcosse on 11/01/16.
 */
public class RefApiActaSubstanceActive extends RefActaSubstanceActiveImpl implements ApiImportResults {
    @Serial
    private static final long serialVersionUID = -1514724282415567797L;

    public static final String SOURCE_API = "API";
    public static final String PROPERTY_STATUS = "status";

    protected String idProduit;

    protected String nomProduit;

    protected String nom_commun_SA;

    protected String concentration_valeur;
    
    public double getConcentration_valeur() {
        double result;
        if (StringUtils.isNotBlank(concentration_valeur)) {
            result = NumberUtils.createDouble(StringUtils.replace(concentration_valeur, ",", "."));

        } else  {
            result = super.concentration_valeur;
        }
        return result;
    }

    public void setIdProduit(String idProduit) {
        this.idProduit = idProduit;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public void setNom_commun_SA(String nom_commun_SA) {
        this.nom_commun_SA = nom_commun_SA;
    }

    public void setConcentration_valeur(String concentration_valeur) {
        this.concentration_valeur = concentration_valeur;
    }

    @Override
    public void setCode_AMM(String code_AMM) {
        this.code_AMM = code_AMM;
    }

    @Override
    public String getId_produit() {
        String result = StringUtils.isNoneBlank(idProduit) ? idProduit : super.id_produit;
        return result;
    }

    @Override
    public String getNom_produit() {
        String result = StringUtils.isNoneBlank(nomProduit) ? nomProduit : super.nom_produit;
        return result;
    }

    @Override
    public String getNom_commun_sa() {
        String result = StringUtils.isNoneBlank(nom_commun_SA) ? nom_commun_SA : super.nom_commun_sa;
        return result;
    }

    @Override
    public String getSource() {
        String result = StringUtils.isNoneBlank(super.source) ? source :SOURCE_API;
        return result;
    }

    @Override
    public boolean isActive() {
        boolean result = this.active;
        return result;
    }

}
