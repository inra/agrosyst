package fr.inra.agrosyst.services.domain;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.Aliment;
import fr.inra.agrosyst.api.entities.AlimentTopiaDao;
import fr.inra.agrosyst.api.entities.Cattle;
import fr.inra.agrosyst.api.entities.CattleTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryImpl;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.EquipmentTopiaDao;
import fr.inra.agrosyst.api.entities.GeoPoint;
import fr.inra.agrosyst.api.entities.GeoPointTopiaDao;
import fr.inra.agrosyst.api.entities.Ground;
import fr.inra.agrosyst.api.entities.GroundTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingPlanTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.HarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.LivestockUnit;
import fr.inra.agrosyst.api.entities.LivestockUnitTopiaDao;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PlotTopiaDao;
import fr.inra.agrosyst.api.entities.Ration;
import fr.inra.agrosyst.api.entities.RationTopiaDao;
import fr.inra.agrosyst.api.entities.SeedPrice;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.ToolsCouplingTopiaDao;
import fr.inra.agrosyst.api.entities.WeatherStation;
import fr.inra.agrosyst.api.entities.WeatherStationTopiaDao;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.PhytoProductTargetTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnection;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveInvolvedRule;
import fr.inra.agrosyst.api.entities.effective.EffectiveInvolvedRuleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRule;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRuleCrop;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRuleCropTopiaDao;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRuleTopiaDao;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeTopiaDao;
import fr.inra.agrosyst.api.entities.managementmode.Strategy;
import fr.inra.agrosyst.api.entities.managementmode.StrategyTopiaDao;
import fr.inra.agrosyst.api.entities.measure.Measure;
import fr.inra.agrosyst.api.entities.measure.MeasureTopiaDao;
import fr.inra.agrosyst.api.entities.measure.MeasurementSession;
import fr.inra.agrosyst.api.entities.measure.MeasurementSessionTopiaDao;
import fr.inra.agrosyst.api.entities.measure.Observation;
import fr.inra.agrosyst.api.entities.measure.ObservationTopiaDao;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceFile;
import fr.inra.agrosyst.api.entities.performance.PerformanceFileTopiaDao;
import fr.inra.agrosyst.api.entities.performance.PerformanceTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlotTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefAnimalTypeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCattleAnimalTypeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCattleRationAlimentTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCountryTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatus;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatusTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefOTEX;
import fr.inra.agrosyst.api.entities.referential.RefOTEXTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalisTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefStationMeteoTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.RefVarieteTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.security.ComputedUserPermissionTopiaDao;
import fr.inra.agrosyst.api.entities.security.UserRoleTopiaDao;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.action.HarvestingPriceDto;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.common.UsageList;
import fr.inra.agrosyst.api.services.domain.CropPersistResult;
import fr.inra.agrosyst.api.services.domain.CroppingPlanEntryDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlans;
import fr.inra.agrosyst.api.services.domain.DomainDeletionException;
import fr.inra.agrosyst.api.services.domain.DomainDto;
import fr.inra.agrosyst.api.services.domain.DomainExtendException;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.DomainSummaryDto;
import fr.inra.agrosyst.api.services.domain.ExtendContext;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedLotInputDto;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.api.utils.TopiaUtils;
import fr.inra.agrosyst.commons.gson.AgrosystGsonSupplier;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.common.EntityUsageService;
import fr.inra.agrosyst.services.common.export.EntityExporter;
import fr.inra.agrosyst.services.common.export.ExportModelAndRows;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.CroppingBean;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.CroppingModel;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.DomainBean;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.DomainModel;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.EquipmentBean;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.EquipmentModel;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.GpsBean;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.GpsModel;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.GroundBean;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.GroundModel;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.InputBean;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.InputModel;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.LivestockUnitBean;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.LivestockUnitModel;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.PlotBean;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.PlotModel;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.StatusBean;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.StatusModel;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.ToolsCouplingBean;
import fr.inra.agrosyst.services.domain.export.DomainExportBeansAndModels.ToolsCouplingModel;
import fr.inra.agrosyst.services.domain.export.DomainExportTask;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.nuiton.util.pagination.PaginationResult;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Setter
public class DomainServiceImpl extends AbstractAgrosystService implements DomainService {

    private static final Log LOGGER = LogFactory.getLog(DomainServiceImpl.class);
    

    protected static final Predicate<CroppingPlanSpeciesDto> IS_CROPPING_SPECIES_EMPTY = input -> {
        String speciesId = input.getSpeciesId();
        return StringUtils.isEmpty(speciesId);
    };

    protected static final Predicate<? super CroppingPlanEntryDto> IS_CROPPING_PLAN_ENTRY_EMPTY = input -> {
        boolean result = true;
        if (input != null) {
            result = StringUtils.isEmpty(input.getName())
                    && (input.getSellingPrice() == null || input.getSellingPrice() == 0d)
                    && (input.getSpecies().isEmpty());
        }
        return result;
    };
    
    protected AnonymizeService anonymizeService;
    protected BusinessAuthorizationService authorizationService;
    protected DomainInputStockUnitService domainInputStockUnitService;
    protected EntityUsageService entityUsageService;
    protected GrowingPlanService growingPlanService;
    protected PlotService plotService;
    protected PricesService pricesService;
    protected ReferentialService referentialService;
    protected AgrosystI18nService i18nService;
    protected PerformanceService performanceService;
    
    protected AbstractActionTopiaDao abstractActionDao;
    protected AlimentTopiaDao alimentDao;
    protected CattleTopiaDao cattleDao;
    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    protected DomainTopiaDao domainDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDao;
    protected EquipmentTopiaDao equipmentDao;
    protected GeoPointTopiaDao geoPointDao;
    protected GroundTopiaDao groundDao;
    protected GrowingPlanTopiaDao growingPlanDao;
    protected HarvestingActionTopiaDao harvestingActionDao;
    protected LivestockUnitTopiaDao livestockUnitDao;
    protected PlotTopiaDao plotDao;
    protected PracticedInterventionTopiaDao practicedInterventionDao;
    protected RationTopiaDao rationDao;
    protected RefDestinationTopiaDao refDestinationDao;
    protected RefEspeceTopiaDao refEspeceDao;
    protected RefLegalStatusTopiaDao refLegalStatusDao;
    protected RefLocationTopiaDao locationDao;
    protected RefMaterielTopiaDao refMaterielTopiaDao;
    protected RefOTEXTopiaDao refOTEXDao;
    protected RefStationMeteoTopiaDao refStationMeteoDao;
    protected RefVarieteTopiaDao refVarieteDao;
    protected ToolsCouplingTopiaDao toolsCouplingDao;
    protected WeatherStationTopiaDao weatherStationDao;
    protected GrowingSystemTopiaDao growingSystemDao;
    protected ReportGrowingSystemTopiaDao reportGrowingSystemDao;
    protected ManagementModeTopiaDao managementModeDao;
    protected ZoneTopiaDao zoneDao;
    protected EffectiveInvolvedRuleTopiaDao effectiveInvolvedRulesDao;
    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleDao;
    protected EffectiveSeasonalCropCycleTopiaDao effectiveSeasonalCropCycleDao;
    protected EffectiveCropCycleNodeTopiaDao effectiveCropCycleNodeDao;
    protected EffectiveCropCycleConnectionTopiaDao effectiveCropCycleConnectionDao;
    protected PerformanceTopiaDao performanceDao;
    protected PerformanceFileTopiaDao performanceFileTopiaDao;
    protected MeasurementSessionTopiaDao measurementSessionDao;
    protected MeasureTopiaDao measureDao;
    protected ObservationTopiaDao observationDao;
    protected DecisionRuleTopiaDao decisionRuleDao;
    protected DecisionRuleCropTopiaDao decisionRuleCropDao;
    protected UserRoleTopiaDao userRoleDao;
    protected ComputedUserPermissionTopiaDao computedUserPermissionDao;
    protected PhytoProductTargetTopiaDao phytoProductTargetDao;
    protected PracticedSystemTopiaDao practicedSystemDao;
    protected PracticedCropCycleTopiaDao practicedCropCycleDao;
    protected PracticedCropCycleConnectionTopiaDao practicedCropCycleConnectionDao;
    protected PracticedPlotTopiaDao practicedPlotDao;
    protected StrategyTopiaDao strategyDao;
    protected HarvestingPriceTopiaDao harvestingPriceTopiaDao;
    protected EffectiveSpeciesStadeTopiaDao effectiveSpeciesStadeDao;

    protected RefAnimalTypeTopiaDao refAnimalTypeTopiaDao;
    protected RefCattleAnimalTypeTopiaDao refCattleAnimalTypeTopiaDao;
    protected RefCattleRationAlimentTopiaDao refCattleRationAlimentTopiaDao;
    protected RefSolArvalisTopiaDao refSolArvalisTopiaDao;
    protected RefCountryTopiaDao refCountryTopiaDao;

    protected static final Predicate<CroppingPlanSpeciesDto> HAS_NO_SPECIES = speciesDto -> speciesDto.getSpeciesId() == null;
    protected Binder<CroppingPlanEntryDto, CroppingPlanEntry> cpeBinder = BinderFactory.newBinder(CroppingPlanEntryDto.class, CroppingPlanEntry.class);
    protected Binder<LivestockUnit, LivestockUnit> LivestockUnitBinder = BinderFactory.newBinder(LivestockUnit.class);
    protected Binder<Cattle, Cattle> cattleBinder = BinderFactory.newBinder(Cattle.class);
    protected Binder<Ration, Ration> rationBinder = BinderFactory.newBinder(Ration.class);
    protected Binder<Aliment, Aliment> alimentBinder = BinderFactory.newBinder(Aliment.class);
    
    @Override
    public List<Domain> getAllDomains() {
        return domainDao.findAll();
    }
    
    @Override
    public Domain getDomain(String domainId) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(domainId));
        Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
        return anonymizeService.checkForDomainAnonymization(domain);
    }

    @Override
    public Domain newDomain() {
        Domain result = domainDao.newInstance();

        // Le domain est actif à sa création
        result.setActive(true);

        return result;
    }

    @Override
    public GeoPoint newGpsData() {
        return geoPointDao.newInstance();
    }

    @Override
    public WeatherStation newWeatherStation() {
        return weatherStationDao.newInstance();
    }
    
    @Override
    public Aliment newAliment(){
        return alimentDao.newInstance();
    }
    
    @Override
    public Ration newRation() {
        return rationDao.newInstance();
    }
    
    @Override
    public Cattle newCattle() {
        return cattleDao.newInstance();
    }
    
    @Override
    public LivestockUnit newLivestockUnit() {
        return livestockUnitDao.newInstance();
    }
    
    @Override
    public CroppingPlanEntry newCroppingPlanEntry() {
        return croppingPlanEntryDao.newInstance();
    }
    
    @Override
    public CroppingPlanSpecies newCroppingSpecies() {
        return croppingPlanSpeciesDao.newInstance();
    }
    
    @Override
    public DomainDto getActiveOrUnactiveDomainForCode(String domainCode) {
        Domain domain = domainDao.forCodeEquals(domainCode).setOrderByArguments(Domain.PROPERTY_ACTIVE + " DESC, " + Domain.PROPERTY_CAMPAIGN + " DESC").findFirst();
        return anonymizeService.getDomainToDtoFunction(false).apply(domain);
    }

    @Override
    public Domain createOrUpdateDomain(
            final Domain domain,
            String locationId,
            String legalStatusId,
            List<GeoPoint> geoPoints,
            List<CroppingPlanEntryDto> croppingPlan,
            Integer otex18code,
            Integer otex70code,
            List<Ground> grounds,
            List<Equipment> equipments,
            List<ToolsCoupling> toolsCouplings,
            List<LivestockUnit> livestockUnits,
            Collection<DomainInputDto> domainInputStockUnits,
            List<HarvestingPriceDto> harvestingPriceDtos) {
        
        Domain persistedDomain = createOrUpdateDomainWithoutCommit(
                domain,
                locationId,
                legalStatusId,
                geoPoints,
                croppingPlan,
                otex18code,
                otex70code,
                grounds,
                equipments,
                toolsCouplings,
                livestockUnits,
                domainInputStockUnits,
                harvestingPriceDtos
        );
        
        // Now validation is automatic https://forge.codelutin.com/issues/4549
        return validateAndCommit(persistedDomain.getTopiaId());
    }

    @Override
    public Domain createOrUpdateDomainWithoutCommit(
            final Domain domain,
            String locationId,
            String legalStatusId,
            List<GeoPoint> geoPoints,
            List<CroppingPlanEntryDto> croppingPlan,
            Integer otex18code,
            Integer otex70code,
            List<Ground> grounds,
            List<Equipment> equipments,
            List<ToolsCoupling> toolsCouplings,
            List<LivestockUnit> livestockUnits,
            Collection<DomainInputDto> domainInputStockUnits,
            List<HarvestingPriceDto> harvestingPriceDtos) {

        String domainId = domain.getTopiaId();
        authorizationService.checkCreateOrUpdateDomain(domainId);

        final Domain persistedDomain = createOrUpdateDomainWithoutValidationAndCommit(
                domain,
                locationId,
                legalStatusId,
                geoPoints,
                croppingPlan,
                otex18code,
                otex70code,
                grounds,
                equipments,
                toolsCouplings,
                livestockUnits,
                domainInputStockUnits,
                harvestingPriceDtos
        );

        if (StringUtils.isBlank(domainId)) {
            authorizationService.domainCreated(persistedDomain);
        } else {
            Preconditions.checkArgument(domain.isActive(), "The domain is not active, you are not allowed to modified it !");
        }

        // Now validation is automatic https://forge.codelutin.com/issues/4549
        return persistedDomain;
    }

    @Override
    public Map<String, Double> safelyConvertDomainSpeciesToAreaJson(String json) {
        Map<String, Double> speciesToArea = new LinkedHashMap<>();
        if (StringUtils.isNotBlank(json)) {
            try {
                Type type = new TypeToken<Map<String, Double>>() {
                }.getType();
                Gson gson = new AgrosystGsonSupplier().get();
                speciesToArea = gson.fromJson(json, type);
            } catch (Exception e) {
                json = json.replace("{", "");
                json = json.replace("}", "");
                String[] parts = json.split(",\"");
                for (String part : parts) {
                    String[] aSpeciesPart = part.split(":");
                    if (aSpeciesPart.length == 2) {
                        String speciesIdentifier = aSpeciesPart[0];
                        String sanitizedStId = speciesIdentifier.replace("\"", "");
                        String stSpeciesArea = aSpeciesPart[1];
                        String sanitizedStArea = stSpeciesArea.replace(",", ".");
                        try {
                            double area = Double.parseDouble(sanitizedStArea);
                            speciesToArea.put(sanitizedStId, area);
                        } catch (NumberFormatException ex) {
                            LOGGER.error(String.format("domain '%s',  area %s from %s could not be convert to double ", "?", sanitizedStArea, stSpeciesArea));
                        }
                    }
                }
            }

        }
        return speciesToArea;
    }

    @Override
    public String sanitizedDomainSpeciesToAreaJson(String json) {
        Map<String, Double> speciesToArea = safelyConvertDomainSpeciesToAreaJson(json);
        if (!speciesToArea.isEmpty()) {
            Gson gson = context.getGson();
            Type type = new TypeToken<Map<String, Double>>() {}.getType();
            return gson.toJson(speciesToArea, type);
        }
        return null;
    }

    /**
     * Return the given domain persisted with it's related components
     * If a given collection parameters is null, it will be ignored
     * if it is empty, all elements from persisted ones will be removed.
     *
     * @param domain                the domain to save
     * @param locationId            the ref location id
     * @param legalStatusId         the ref legal status id
     * @param geoPoints             GPS coordinate center
     * @param croppingPlanDtos      Crop DTOs
     * @param otex18code            otex18
     * @param otex70code            otex70
     * @param grounds               the domain grounds
     * @param equipments            the domain equipments
     * @param toolsCouplings        the domain tools couping including manual intervention
     * @param livestockUnits        the live stock unit
     * @param domainInputStockUnits the input stock unit
     * @param harvestingPriceDtos   the harvesting price
     * @return the persisted domain
     */
    protected Domain createOrUpdateDomainWithoutValidationAndCommit(
            Domain domain,
            String locationId,
            String legalStatusId,
            List<GeoPoint> geoPoints,
            Collection<CroppingPlanEntryDto> croppingPlanDtos,
            Integer otex18code,
            Integer otex70code,
            List<Ground> grounds,
            List<Equipment> equipments,
            List<ToolsCoupling> toolsCouplings,
            List<LivestockUnit> livestockUnits,
            Collection<DomainInputDto> domainInputStockUnits,
            List<HarvestingPriceDto> harvestingPriceDtos) {
        
        final boolean domainCreation = StringUtils.isBlank(domain.getTopiaId());
        
        validPreconditions(domain, locationId, geoPoints, toolsCouplings, croppingPlanDtos, equipments, livestockUnits);
        
        // Le statut légal n'est défini que pour les types FERME
        if (!DomainType.EXPLOITATION_AGRICOLE.equals(domain.getType()) || !DomainType.FERME_DE_LYCEE_AGRICOLE.equals(domain.getType())) {
            domain.setLegalStatus(null);
        }

        // set location before create (not null)
        setLocation(domain, locationId);
        
        domain.setUpdateDate(context.getCurrentTime());

        domain.setSiret(StringUtils.trimToNull(StringUtils.replace(domain.getSiret(), " ", "")));

        setDomainSpeciesToArea(domain, croppingPlanDtos);

        addDomainLegalStatus(legalStatusId, domain);

        addDomainOtex18(otex18code, domain);

        addDomainOtex70(otex70code, domain);

        final Domain persistedDomain = saveDomain(domain);
        
        bindDomainToLivestockUnits(livestockUnits, persistedDomain, domainCreation);
        
        bindDomainToGrounds(grounds, persistedDomain, domainCreation);
        
        bindDomainToGeoPoint(geoPoints, persistedDomain, domainCreation);

        manageEquipmentAndToolsCoupling(persistedDomain, equipments, toolsCouplings, domainCreation);
    
        Map<String, CropPersistResult> cropByOriginalIds = persistCrops(croppingPlanDtos, persistedDomain, domainInputStockUnits, domainCreation);

        domainInputStockUnitService.createOrUpdateDomainInputStock(domain, domainInputStockUnits, cropByOriginalIds);
        
        pricesService.updateHarvestingPriceValues(harvestingPriceDtos);

        return persistedDomain;
    }

    protected void setDomainSpeciesToArea(Domain domain, Collection<CroppingPlanEntryDto> croppingPlanDtos) {
        if (croppingPlanDtos != null) {
            Set<String> domainSpeciesAreaSpeciesKeys = new HashSet<>();
            croppingPlanDtos.removeIf(IS_CROPPING_PLAN_ENTRY_EMPTY);
            croppingPlanDtos.forEach(dtoEntry -> {
                Collection<CroppingPlanSpeciesDto> dtoSpeciesList = dtoEntry.getSpecies();
                dtoSpeciesList.forEach(speciesDto -> {
                    //croppingPlanSpecies.code_espece_botanique + "_" + croppingPlanSpecies.code_qualifiant_AEE + "_" + croppingPlanSpecies.code_type_saisonier + "_" + croppingPlanSpecies.code_destination_aee
                    List<String> keyPart = new ArrayList<>();
                    String code_qualifiant_AEE = ObjectUtils.firstNonNull(speciesDto.getCode_qualifiant_AEE(), "");
                    String code_type_saisonier = ObjectUtils.firstNonNull(speciesDto.getCode_type_saisonier(), "");
                    String code_destination_aee = ObjectUtils.firstNonNull(speciesDto.getCode_destination_aee(), "");

                    keyPart.add(speciesDto.getCode_espece_botanique());
                    keyPart.add(code_qualifiant_AEE);
                    keyPart.add(code_type_saisonier);
                    keyPart.add(code_destination_aee);
                    domainSpeciesAreaSpeciesKeys.add(String.join("_", keyPart).toLowerCase());
                });
            });
            Map<String, Double> speciesToArea = safelyConvertDomainSpeciesToAreaJson(domain.getSpeciesToArea());
            Set<String> notValidKeys = speciesToArea.keySet().stream().filter(key -> !domainSpeciesAreaSpeciesKeys.contains(key)).collect(Collectors.toSet());
            notValidKeys.forEach(speciesToArea::remove);
            String validSpeciesToArea = context.getGson().toJson(speciesToArea);
            domain.setSpeciesToArea(validSpeciesToArea);
        }
    }
    
    public static Map<String, CropPersistResult> getCropPersistResultForPersistedOnes(
            List<CroppingPlanEntry> existingCroppingPlans) {

        Map<String, CropPersistResult> result = new HashMap<>();
        for (CroppingPlanEntry existingCroppingPlan : existingCroppingPlans) {
            final String topiaId = existingCroppingPlan.getTopiaId();
            final Collection<CroppingPlanSpecies> croppingPlanSpecies = CollectionUtils.emptyIfNull(existingCroppingPlan.getCroppingPlanSpecies());
            Map<String, CroppingPlanSpecies> cpsByIds = Maps.uniqueIndex(croppingPlanSpecies, TopiaEntity::getTopiaId);
            CropPersistResult r = new CropPersistResult(topiaId, existingCroppingPlan, cpsByIds);
            result.put(topiaId, r);
        }
        return result;
    }

    protected void bindDomainToLivestockUnits(List<LivestockUnit> livestockUnits, Domain persistedDomain, boolean domainCreation) {
        if (livestockUnits != null) {
            List<LivestockUnit> existingLiveStocks = domainCreation ? new ArrayList<>() : livestockUnitDao.forDomainEquals(persistedDomain).findAll();
            int otex18 = persistedDomain.getOtex18() != null ? persistedDomain.getOtex18().getCode_OTEX_18_postes() : 0;
            boolean isLivestockUnitsValides = otex18 == 41 || otex18 == 42 || otex18 == 43 || otex18 == 44 || otex18 == 50 || otex18 == 60 || otex18 == 71 || otex18 == 72 || otex18 == 81 || otex18 == 82;
            livestockUnits = isLivestockUnitsValides ? livestockUnits : new ArrayList<>();

            Map<String, LivestockUnit> existingLiveStocksByTopiaIds = existingLiveStocks.stream().collect(Collectors.toMap((TopiaEntity::getTopiaId), Function.identity()));
            livestockUnits.forEach(
                lsu -> {
                    LivestockUnit persistedLivestockUnit = existingLiveStocksByTopiaIds.remove(lsu.getTopiaId());

                    // put existingLiveStocksByTopiaIds to remove live stock that are no more used
                    if (persistedLivestockUnit == null) {

                        createNewLiveStockUnit(persistedDomain, lsu);

                    } else {

                        LivestockUnitBinder.copyExcluding(lsu, persistedLivestockUnit, LivestockUnit.PROPERTY_DOMAIN, LivestockUnit.PROPERTY_CODE, LivestockUnit.PROPERTY_CATTLES, TopiaEntity.PROPERTY_TOPIA_ID, TopiaEntity.PROPERTY_TOPIA_CREATE_DATE);

                        Map<String, Cattle> existingCattlesByTopiaIds = persistedLivestockUnit.getCattles().stream()
                                .collect(Collectors.toMap((TopiaEntity::getTopiaId), Function.identity()));

                        lsu.getCattles().forEach(cattle -> {
                            Cattle currentCattle = existingCattlesByTopiaIds.remove(cattle.getTopiaId());
                            if (currentCattle != null) {

                                bindCattle(cattle, cattleBinder, currentCattle, rationBinder, alimentBinder);

                            } else {
                                cattle.setCode(UUID.randomUUID().toString());
                                persistedLivestockUnit.addCattles(cattle);
                            }
                            // otherwise nothing to do, they will be added by composition
                        });

                        existingCattlesByTopiaIds.values().forEach(persistedLivestockUnit::removeCattles);
                    }
                }
            );
            livestockUnitDao.deleteAll(existingLiveStocksByTopiaIds.values());
        }
    }

    private void createNewLiveStockUnit(Domain persistedDomain, LivestockUnit lsu) {
        lsu.setDomain(persistedDomain);
        lsu.setCode(UUID.randomUUID().toString());
        CollectionUtils.emptyIfNull(lsu.getCattles()).forEach(cattle -> cattle.setCode(UUID.randomUUID().toString()));
        livestockUnitDao.create(lsu);
    }

    protected void bindCattle(Cattle cattle, Binder<Cattle, Cattle> cattleBinder, Cattle currentCattle, Binder<Ration, Ration> rationBinder, Binder<Aliment, Aliment> alimentBinder) {

        cattleBinder.copyExcluding(cattle, currentCattle, Cattle.PROPERTY_CODE, Cattle.PROPERTY_RATIONS, TopiaEntity.PROPERTY_TOPIA_ID, TopiaEntity.PROPERTY_TOPIA_CREATE_DATE);

        // map by Ids to update persisted ones and add new ones
        Map<String, Ration> rationByIds = currentCattle.getRations().stream().collect(Collectors.toMap((TopiaEntity::getTopiaId), Function.identity()));

        // update existing ration, otherwise nothing to do, they will be added by composition
        new ArrayList<>(cattle.getRations()).forEach(
            ration -> {
                Ration currentRation = rationByIds.remove(ration.getTopiaId());
                if (currentRation != null) {

                    bindRation(rationBinder, alimentBinder, ration, currentRation);

                } else {
                    currentCattle.addRations(ration);
                }
            }
        );

        rationByIds.values().forEach(currentCattle::removeRations);
    }

    protected void bindRation(Binder<Ration, Ration> rationBinder, Binder<Aliment, Aliment> alimentBinder, Ration ration, Ration currentRation) {

        rationBinder.copyExcluding(ration, currentRation, Ration.PROPERTY_ALIMENTS, TopiaEntity.PROPERTY_TOPIA_ID, TopiaEntity.PROPERTY_TOPIA_CREATE_DATE);
        ArrayList<Aliment> aliments = new ArrayList<>(currentRation.getAliments());

        // map by Ids to update persisted ones and add new ones
        Map<String, Aliment> alimentByIds = aliments.stream().collect(Collectors.toMap((TopiaEntity::getTopiaId), Function.identity()));

        ration.getAliments().forEach(
            aliment -> {
                Aliment currentAliment = alimentByIds.remove(aliment.getTopiaId());
                if (currentAliment != null) {

                    alimentBinder.copy(aliment, currentAliment);

                } else {
                    ration.addAliments(aliment);
                }
            }
        );

        alimentByIds.values().forEach(ration::removeAliments);
    }

    private void setLocation(Domain domain, String locationId) {
        RefLocation location = locationDao.forTopiaIdEquals(locationId).findUnique();
        domain.setLocation(location);
    }

    private List<CroppingPlanEntry> findExistingCroppingPlans(String domainId) {
        List<CroppingPlanEntry> existingCroppingPlan;
        if (StringUtils.isNotBlank(domainId)) {
            existingCroppingPlan = getCroppingPlan0(domainId);
        } else {
            existingCroppingPlan = new ArrayList<>();
        }
        return existingCroppingPlan;
    }

    protected Domain saveDomain(final Domain domain) {
        Domain result;
        if (StringUtils.isBlank(domain.getTopiaId())) {
            // create a random domain code, used to link domains each other
            setDomainCode(domain);

            result = domainDao.create(domain);

        } else {
            result = domainDao.update(domain);
        }
        return result;
    }

    private void setDomainCode(Domain domain) {
        if (StringUtils.isBlank(domain.getCode())) {
            domain.setCode(UUID.randomUUID().toString());
        }
    }
    
    /**
     * @param croppingPlanDtos      null if not used otherwise it must be empty or contains some element
     * @param persistedDomain       the domain the crop belong to
     * @param domainInputStockUnits domain input stocks that are related to the domain crop, use to not remove crop that remain used by inputs
     * @param domainCreation        true if it's a new domain
     */
    private Map<String, CropPersistResult> persistCrops(
            Collection<CroppingPlanEntryDto> croppingPlanDtos,
            Domain persistedDomain,
            Collection<DomainInputDto> domainInputStockUnits,
            boolean domainCreation) {
    
        Map<String, CropPersistResult> result;
        List<CroppingPlanEntry> existingCroppingPlans = domainCreation ? new ArrayList<>() : findExistingCroppingPlans(persistedDomain.getTopiaId());

        if (croppingPlanDtos != null) {
            Set<String> domainSeedLotCropIds = new HashSet<>();
            Set<String> domainSeedLotSpeciesIds = new HashSet<>();
            List<DomainInputDto> domainSeedLotDtos = CollectionUtils.emptyIfNull(domainInputStockUnits).stream().filter(dl -> dl instanceof DomainSeedLotInputDto).toList();
            domainSeedLotDtos.forEach(
                    di -> {
                        DomainSeedLotInputDto dsl = (DomainSeedLotInputDto) di;
                        CroppingPlanEntryDto cropSeedDto = dsl.getCropSeedDto();
                        domainSeedLotCropIds.add(cropSeedDto.getTopiaId());
                        CollectionUtils.emptyIfNull(dsl.getSpeciesInputs()).forEach(
                                dss -> domainSeedLotSpeciesIds.add(dss.getSpeciesSeedDto().getTopiaId())
                        );
                    }
            );

            result = new HashMap<>();
            croppingPlanDtos.removeIf(IS_CROPPING_PLAN_ENTRY_EMPTY);
            
            Map<String, RefEspece> especesIndex = new HashMap<>();
            Map<String, RefVariete> varietesIndex = new HashMap<>();
            loadCropSpeciesAndVarieties(croppingPlanDtos, especesIndex, varietesIndex);
            
            List<CroppingPlanEntry> croppingPlanEntriesToDelete = new ArrayList<>(existingCroppingPlans);
            Map<String, CroppingPlanEntry> entityEntriesIndex = Maps.uniqueIndex(existingCroppingPlans, Entities.GET_TOPIA_ID::apply);
            for (CroppingPlanEntryDto dtoEntry : croppingPlanDtos) {
                String cpEntryId = dtoEntry.getTopiaId();
                CroppingPlanEntry entityEntry = entityEntriesIndex.get(cpEntryId);
                if (entityEntry == null) {
                    entityEntry = croppingPlanEntryDao.newInstance();
                    entityEntry.setCode(getOrCreateCropCode(dtoEntry));
                    entityEntry.setDomain(persistedDomain);
                } else {
                    croppingPlanEntriesToDelete.remove(entityEntry);
                }
                bindCropDtoToCrop(dtoEntry, entityEntry);
                removeNoneValidFields(entityEntry);
                
                // Remove useless dtos
                Collection<CroppingPlanSpeciesDto> dtoSpeciesList = dtoEntry.getSpecies();
                
                List<CroppingPlanSpecies> species = removeUnusedCroppingPlanSpecies(entityEntry, dtoSpeciesList);
                
                Map<String, CroppingPlanSpecies> cropSpeciesPersistResult = persistCropSpecies(dtoSpeciesList, especesIndex, varietesIndex, entityEntry, species);

                String tmpId;
                if (entityEntry.isPersisted()) {
                    tmpId = entityEntry.getTopiaId();
                    entityEntry = croppingPlanEntryDao.update(entityEntry);
                } else {
                    tmpId = dtoEntry.getTopiaId();
                    entityEntry = croppingPlanEntryDao.create(entityEntry);
                }
                CropPersistResult cropPersistResult = new CropPersistResult(tmpId, entityEntry, cropSpeciesPersistResult);
                result.put(tmpId, cropPersistResult);
            }
            
            // All cropping plan species belonging to any cropping plan entry to delete has to be deleted
            for (CroppingPlanEntry croppingPlanEntryToDelete : croppingPlanEntriesToDelete) {
                List<CroppingPlanSpecies> croppingPlanSpeciesToDelete = croppingPlanEntryToDelete.getCroppingPlanSpecies();
                if (croppingPlanSpeciesToDelete != null) {
                    List<CroppingPlanSpecies> croppingPlanSpeciesToRemove = croppingPlanSpeciesToDelete.stream().filter(cps -> !domainSeedLotSpeciesIds.contains(cps.getTopiaId())).toList();
                    croppingPlanSpeciesToRemove.forEach(croppingPlanEntryToDelete::removeCroppingPlanSpecies);
                    croppingPlanSpeciesDao.deleteAll(croppingPlanSpeciesToRemove);
                }
            }

            List<CroppingPlanEntry> croppingPlanEntriesToRemove = croppingPlanEntriesToDelete.stream().filter(cpe -> !domainSeedLotCropIds.contains(cpe.getTopiaId())).toList();
            List<CroppingPlanEntry> toKeep = ListUtils.subtract(croppingPlanEntriesToDelete, croppingPlanEntriesToRemove);
            for (CroppingPlanEntry croppingPlanEntry : toKeep) {
                Map<String, CroppingPlanSpecies> cropSpeciesById = CollectionUtils.emptyIfNull(croppingPlanEntry.getCroppingPlanSpecies()).stream().collect(Collectors.toMap(CroppingPlanSpecies::getTopiaId, Function.identity()));
                CropPersistResult cropPersistResult = new CropPersistResult(croppingPlanEntry.getTopiaId(), croppingPlanEntry, cropSpeciesById);
                result.put(croppingPlanEntry.getTopiaId(), cropPersistResult);
            }

            // Now species are deleted, delete cropping plan entry
            croppingPlanEntryDao.deleteAll(croppingPlanEntriesToRemove);
            
        } else {
            result = getCropPersistResultForPersistedOnes(existingCroppingPlans);
        }
        return result;
    }
    
    protected Map<String, CroppingPlanSpecies> persistCropSpecies(
            Collection<CroppingPlanSpeciesDto> dtoSpeciesList,
            Map<String, RefEspece> especesIndex,
            Map<String, RefVariete> varietesIndex,
            CroppingPlanEntry entityEntry,
            List<CroppingPlanSpecies> species) {
    
        Map<String, CroppingPlanSpecies> croppingPlanSpeciesByTmpIds = new HashMap<>();
        
        // Make an index of existing entities
        Map<String, CroppingPlanSpecies> speciesById = Maps.uniqueIndex(species, Entities.GET_TOPIA_ID::apply);
        
        for (CroppingPlanSpeciesDto dtoSpecies : dtoSpeciesList) {
    
            
            final String speciesTopiaId = dtoSpecies.getTopiaId();
            final boolean isNewSpecies = StringUtils.isEmpty(speciesTopiaId) || speciesTopiaId.contains(DomainService.NEW_SPECIES_PREFIX);
    
            CroppingPlanSpecies entitySpecies;
            if (isNewSpecies) {
                entitySpecies = croppingPlanSpeciesDao.newInstance();
                entitySpecies.setCode(getOrCreateSpeciesCode(dtoSpecies));
                entityEntry.addCroppingPlanSpecies(entitySpecies);
            } else {
                entitySpecies = speciesById.get(speciesTopiaId);
                Preconditions.checkState(entitySpecies != null, "CroppingPlanSpecies non trouvée: " + speciesTopiaId);
        
            }
    
            croppingPlanSpeciesByTmpIds.put(speciesTopiaId, entitySpecies);
            entitySpecies.setSpeciesArea(dtoSpecies.getSpeciesArea());
            entitySpecies.setCompagne(dtoSpecies.getCompagne());

            String especeId = dtoSpecies.getSpeciesId();
            RefEspece refEspece = especesIndex.get(especeId);
            Preconditions.checkState(refEspece != null, "Espece non trouvée: " + especeId);
            entitySpecies.setSpecies(refEspece);
    
            String entityVarietyId = null;
            RefVariete entityVariety = entitySpecies.getVariety();
            if (entityVariety != null) {
                entityVarietyId = entityVariety.getTopiaId();
            }
            String varietyId = dtoSpecies.getVarietyId();
    
            final boolean isVarietyChange = !StringUtils.equals(entityVarietyId, varietyId);
    
            if (isVarietyChange) {
                RefVariete refVariete = null;
                if (StringUtils.isNotEmpty(varietyId)) {
                    refVariete = varietesIndex.get(varietyId);
                    Preconditions.checkState(refVariete != null, "Variété non trouvée: " + varietyId);
                    Preconditions.checkArgument(
                            referentialService.validVarietesFromCodeEspeceEdi(
                                    refVariete,
                                    refEspece.getCode_espece_botanique()),
                            String.format("Variété %s non valide pour l'espèce %s",
                                    refVariete.getLabel(),
                                    refEspece.getLibelle_espece_botanique_Translated()));
                }
                entitySpecies.setVariety(refVariete);
                // user could not choose an invalid variety.
                entitySpecies.setEdaplosUnknownVariety(null);
            }
        }
        return croppingPlanSpeciesByTmpIds;
    }
    
    private void removeNoneValidFields(CroppingPlanEntry entityEntry) {
        if (entityEntry.getAverageIFT() == null) {
            entityEntry.setBiocontrolIFT(null);
            entityEntry.setEstimatingIftRules(null);
            entityEntry.setIftSeedsType(null);
            entityEntry.setDoseType(null);
        }
    }
    
    private void bindCropDtoToCrop(CroppingPlanEntryDto dtoEntry, CroppingPlanEntry entityEntry) {
        cpeBinder.copyExcluding(dtoEntry, entityEntry,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                CroppingPlanEntry.PROPERTY_CODE,
                CroppingPlanEntry.PROPERTY_DOMAIN,
                CroppingPlanEntry.PROPERTY_CROPPING_PLAN_SPECIES);
    }

    protected String getOrCreateCropCode(CroppingPlanEntryDto dtoEntry) {
        String code = StringUtils.isNotBlank(dtoEntry.getCode()) ? dtoEntry.getCode() : UUID.randomUUID().toString();
        return code;
    }
    
    protected String getOrCreateSpeciesCode(CroppingPlanSpeciesDto dtoSpecies) {
        String code = StringUtils.isNotBlank(dtoSpecies.getCode()) ? dtoSpecies.getCode() : UUID.randomUUID().toString();
        return code;
    }

    protected List<CroppingPlanSpecies> removeUnusedCroppingPlanSpecies(CroppingPlanEntry entityEntry, Collection<CroppingPlanSpeciesDto> dtoSpeciesList) {
        dtoSpeciesList.removeIf(IS_CROPPING_SPECIES_EMPTY);
    
        // Make an index for entities having an id
        final ImmutableListMultimap<String, CroppingPlanSpeciesDto> dtoSpeciesIndex = Multimaps.index(dtoSpeciesList, input -> {
            assert input != null;
            return StringUtils.defaultString(input.getTopiaId());
        });
    
        // Remove unused entities
        List<CroppingPlanSpecies> entitySpeciesList = entityEntry.getCroppingPlanSpecies();
        if (entitySpeciesList == null) {
            entitySpeciesList = new ArrayList<>();
        }
        entitySpeciesList.removeIf(species -> {
            assert species != null;
            String speciesTopiaId = species.getTopiaId();
            return !dtoSpeciesIndex.containsKey(speciesTopiaId);
        });
        return entitySpeciesList;
    }

    protected void manageEquipmentAndToolsCoupling(
            final Domain persistedDomain,
            List<Equipment> equipments,
            List<ToolsCoupling> toolsCouplings,
            boolean domainCreation) {

        final Map<String, Equipment> equipmentsCache = new HashMap<>();

        List<Equipment> existingEquipments;
        List<ToolsCoupling> existingToolsCouplings;
        if (domainCreation) {
            existingEquipments = new ArrayList<>();
            existingToolsCouplings = new ArrayList<>();
        } else {
            existingEquipments = equipmentDao.forDomainEquals(persistedDomain).findAll();
            existingToolsCouplings = toolsCouplingDao.forDomainEquals(persistedDomain).findAll();

            toolsCouplings = toolsCouplings == null ? existingToolsCouplings : toolsCouplings;
            equipments = equipments == null ? existingEquipments : equipments;
        }

        List<Equipment> equipmentsToDelete = bindDomainToEquipments(persistedDomain, equipments, equipmentsCache, existingEquipments);

        List<ToolsCoupling> toolsCouplingsToDelete = bindDomainToToolsCouplings(persistedDomain, toolsCouplings, equipmentsCache, existingToolsCouplings);

        // Now delete the toolsCoupling and equipments in the correct order
        deleteNotUsedToolsCouplings(toolsCouplingsToDelete);

        equipmentDao.deleteAll(equipmentsToDelete);
    }

    protected void deleteNotUsedToolsCouplings(List<ToolsCoupling> toolsCouplingsToDelete) {
        for (ToolsCoupling toolsCouplingsToDel : toolsCouplingsToDelete) {
            // XXX for ticket #4812: Problèmes à la suppression d'un matériel
            toolsCouplingsToDel.setTractor(null);
            toolsCouplingDao.delete(toolsCouplingsToDel);
        }
    }

    // Precondition have to be validated before
    protected List<ToolsCoupling> bindDomainToToolsCouplings(
            final Domain persistedDomain,
            List<ToolsCoupling> toolsCouplings,
            final Map<String, Equipment> equipmentsCache,
            List<ToolsCoupling> existingToolsCouplings) {

        Consumer<ToolsCoupling> newToolsCouplingSpecificTreatment = input -> {
            if (StringUtils.isBlank(input.getCode())) {
                input.setCode(UUID.randomUUID().toString());
            }
            input.setDomain(persistedDomain);
        };

        Consumer<ToolsCoupling> afterBindToolsCouplingTreatment = input -> {
            // Replace with the correct tractor
            if (input.isManualIntervention()) {
                input.setWorkRateUnit(MaterielWorkRateUnit.HA_H);
            }
            Equipment tractor = input.getTractor();
            if (tractor != null) {
                String tractorId = input.getTractor().getTopiaId();
                Equipment domainTractor = equipmentsCache.get(tractorId);
                input.setTractor(domainTractor);
            }

            // Inject the good topiaId instead
            Collection<Equipment> equipments = new ArrayList<>(CollectionUtils.emptyIfNull(input.getEquipments()));
            input.clearEquipments();
            for (Equipment equipment : equipments) {
                String equipmentId = equipment.getTopiaId();
                Equipment fromCache = equipmentsCache.get(equipmentId);
                if (fromCache != null) {
                    input.addEquipments(fromCache);
                }
            }
        };

        return easyBindNoDelete(
                toolsCouplingDao,
                existingToolsCouplings,
                toolsCouplings,
                newToolsCouplingSpecificTreatment,
                afterBindToolsCouplingTreatment,
                null, ToolsCoupling.PROPERTY_DOMAIN);
    }

    protected List<Equipment> bindDomainToEquipments(final Domain persistedDomain, List<Equipment> equipments, final Map<String, Equipment> equipmentsCache, List<Equipment> existingEquipments) {

        Consumer<Equipment> onNewEquipment = input -> {
            if (StringUtils.isBlank(input.getCode())) {
                input.setCode(UUID.randomUUID().toString());
            }
            input.setDomain(persistedDomain);
        };

        Consumer<Equipment> afterEquipmentBind = equipment -> {
            String topiaId = equipment.getTopiaId();
            equipmentsCache.put(topiaId, equipment);
            if (topiaId.startsWith(NEW_EQUIPMENT)) {
                TopiaUtils.cleanTopiaId(equipment);
            }
        };

        Consumer<Equipment> afterPersist = equipment -> {
            String topiaId = equipment.getTopiaId();
            equipmentsCache.put(topiaId, equipment);
        };

        List<Equipment> equipment = easyBindNoDelete(
                equipmentDao,
                existingEquipments,
                equipments,
                onNewEquipment,
                afterEquipmentBind,
                afterPersist,
                Equipment.PROPERTY_DOMAIN);

        return equipment;
    }

    protected void bindDomainToGeoPoint(List<GeoPoint> geoPoints, final Domain persistedDomain, boolean domainCreation) {
        List<GeoPoint> existingGeoPoints = domainCreation ?
                new ArrayList<>() : geoPointDao.forDomainEquals(persistedDomain).findAll();
        easyBind(geoPointDao, existingGeoPoints, geoPoints,
                input -> input.setDomain(persistedDomain),
                GeoPoint.PROPERTY_DOMAIN);
    }

    protected void bindDomainToGrounds(List<Ground> grounds, final Domain persistedDomain, boolean domainCreation) {
        List<Ground> existingGrounds = domainCreation ? new ArrayList<>() : groundDao.forDomainEquals(persistedDomain).findAll();
        easyBind(groundDao, existingGrounds, grounds,
                input -> input.setDomain(persistedDomain),
                Ground.PROPERTY_DOMAIN);
    }

    protected void addDomainOtex70(Integer otex70code, Domain persistedDomain) {
        if (otex70code != null) {
            RefOTEX refOtex70 = refOTEXDao.forCode_OTEX_70_postesEquals(otex70code).findAny();
            persistedDomain.setOtex70(refOtex70);
        } else {
            persistedDomain.setOtex70(null);
        }
    }

    protected void addDomainOtex18(Integer otex18code, Domain persistedDomain) {
        if (otex18code != null) {
            RefOTEX refOtex18 = refOTEXDao.forCode_OTEX_18_postesEquals(otex18code).findAny();
            persistedDomain.setOtex18(refOtex18);
        } else {
            persistedDomain.setOtex18(null);
        }
    }

    protected void addDomainLegalStatus(String legalStatusId, Domain persistedDomain) {
        if (!StringUtils.isBlank(legalStatusId) && (persistedDomain.getType().equals(DomainType.EXPLOITATION_AGRICOLE) || persistedDomain.getType().equals(DomainType.FERME_DE_LYCEE_AGRICOLE))) {
            RefLegalStatus legalStatus = refLegalStatusDao.forTopiaIdEquals(legalStatusId).findUnique();
            persistedDomain.setLegalStatus(legalStatus);
        }
    }

    protected void validPreconditions(Domain domain, String locationId, List<GeoPoint> geoPoints, List<ToolsCoupling> toolsCouplings, Collection<CroppingPlanEntryDto> croppingPlanEntryDtos, List<Equipment> equipments, List<LivestockUnit> livestockUnits) {
        validGeoPoints(geoPoints);
        validateEquipments(equipments);
        validateToolsCooplings(toolsCouplings);
        validateCrops(croppingPlanEntryDtos);
        validLiveStock(livestockUnits);

        Preconditions.checkArgument(CommonService.getInstance().areCampaignsValids(Integer.toString(domain.getCampaign())), "Campagne non valide pour le domaine");
        Preconditions.checkArgument(StringUtils.isNotEmpty(domain.getMainContact()), "Aucun contact de renseigné");
        Preconditions.checkArgument(StringUtils.isNotEmpty(domain.getName()), "Le nom du domaine est obligatoire");
        Preconditions.checkArgument(StringUtils.isNotEmpty(locationId), "Commune non renseignée");
        Preconditions.checkNotNull(domain.getType(), "Le type de domaine est obligatoire");
    }

    private void validGeoPoints(Collection<GeoPoint> geoPoints) {
        if (geoPoints != null) {
            geoPoints.removeAll(Collections.singleton(null));
            for (GeoPoint geoPoint : geoPoints) {
                Preconditions.checkArgument(StringUtils.isNotBlank(geoPoint.getName()), "Le nom du centre est obligatoire sur une coordonées d'un centre opérationnels");
            }
        }
    }
    
    protected void validLiveStock(Collection<LivestockUnit> livestockUnits) {
        // validation des ateliers d’élevage
        if (CollectionUtils.isNotEmpty(livestockUnits)) {
            livestockUnits.removeAll(Collections.singleton(null));
            for (LivestockUnit livestockUnit : livestockUnits) {
                Preconditions.checkNotNull(livestockUnit.getRefAnimalType(), "Le type d’animal est obligatoire pour un atelier d’élevage");
            }
        }
    }

    protected void validateCrops(Collection<CroppingPlanEntryDto> croppingPlanEntryDtos) {
        if (croppingPlanEntryDtos != null) {
            croppingPlanEntryDtos.removeAll(Collections.singleton(null));
            for (CroppingPlanEntryDto croppingPlanEntryDto : croppingPlanEntryDtos) {
                Preconditions.checkArgument(StringUtils.isNotBlank(croppingPlanEntryDto.getName()));
                Preconditions.checkNotNull(croppingPlanEntryDto.getType(), "Type de culture non défini");
                if (croppingPlanEntryDto.getYealdAverage() != null) {
                    Preconditions.checkNotNull(croppingPlanEntryDto.getYealdUnit(), "Unité de rendement non renseigné");
                }
                Collection<CroppingPlanSpeciesDto> speciesDtos = croppingPlanEntryDto.getSpecies();
                for (CroppingPlanSpeciesDto speciesDto : speciesDtos) {
                    Preconditions.checkNotNull(speciesDto.getSpeciesId(), "Identifiant de l'espèce non renseigné");
                }
            }
        }
    }

    protected void validateToolsCooplings(Collection<ToolsCoupling> toolsCouplings) {
        if (toolsCouplings != null) {
            toolsCouplings.removeAll(Collections.singleton(null));
            for (ToolsCoupling toolsCoupling : toolsCouplings) {
                Preconditions.checkArgument(StringUtils.isNoneBlank(toolsCoupling.getToolsCouplingName()), "Aucun nom de définie sur la combinaison d'outil");
                Preconditions.checkArgument(CollectionUtils.isNotEmpty(toolsCoupling.getMainsActions()), "Aucune action principale de définie");
                if (toolsCoupling.isManualIntervention()){
                    Preconditions.checkArgument(toolsCoupling.getTractor() == null);
                } else {
                    Collection<Equipment> tcEquipments = toolsCoupling.getEquipments();
                    boolean irrigationEquipmentsPresent = false;
                    boolean nonIrrigationEquipmentsPresent = false;
                    if (CollectionUtils.isNotEmpty(tcEquipments)) {
                        for (Equipment tcEquipment : tcEquipments) {
                            Preconditions.checkArgument(StringUtils.isNotBlank(tcEquipment.getName()), "Aucun nom de définit sur l'eqipement");
                            boolean isIrrigationEquipment = tcEquipment.getRefMateriel() != null && tcEquipment.getRefMateriel() instanceof RefMaterielIrrigation;
                            if (isIrrigationEquipment) {
                                irrigationEquipmentsPresent = true;
                            } else {
                                nonIrrigationEquipmentsPresent = true;
                            }
                        }
                    }

                    Equipment tractor = toolsCoupling.getTractor();
                    if (tractor == null) {
                        Preconditions.checkArgument(irrigationEquipmentsPresent,"Combinaison d'outils non valide, aucun tracteur/automoteur/matériel d'irrigation présent");
                        Preconditions.checkArgument(!nonIrrigationEquipmentsPresent,"Combinaison d'outils non valide, du matériel d'irrigation ne peut être associé avec du matériel autre qu'un tracteur ou un automoteur");
                    } else {
                        // if tractor is not auto-motor's type, it must be associated to 1+n equipment
                        final RefMateriel refMateriel = tractor.getRefMateriel();
                        if (!(refMateriel instanceof RefMaterielAutomoteur)) {
                            Preconditions.checkArgument(CollectionUtils.isNotEmpty(tcEquipments), "Combinaison d'outils non valide, aucun équipement présent avec le tracteur");
                        }

                        if (CollectionUtils.isNotEmpty(tcEquipments)) {
                            Preconditions.checkArgument(!(irrigationEquipmentsPresent && nonIrrigationEquipmentsPresent), "Combinaison d'outils non valide, du matériel d'irrigation ne peut être associé avec du matériel autre qu'un tracteur ou un automoteur");
                        }

                        Preconditions.checkArgument(!(refMateriel instanceof RefMaterielOutil), "Combinaison d'outils non valide, un équipement de type outil ne peut être utilisé comme tacteur");

                    }
                }
            }
        }
    }

    @Override
    public PaginationResult<Domain> getFilteredDomains(DomainFilter filter) {
        PaginationResult<Domain> result = getFilteredDomains0(filter);
        result = anonymizeService.checkForDomainsAnonymization(result);
        return result;
    }

    @Override
    public List<Domain> getDomainWithName(String name) {
        Preconditions.checkNotNull(name);
        return domainDao.forNameEquals(name).findAll();
    }

    protected PaginationResult<Domain> getFilteredDomains0(DomainFilter filter) {
        return domainDao.getFilteredDomains(filter, getSecurityContext());
    }

    @Override
    public PaginationResult<DomainDto> getFilteredDomainsDto(DomainFilter filter) {
        PaginationResult<Domain> domains = getFilteredDomains0(filter);
        return domains.transform(anonymizeService.getDomainToDtoFunction(true));
    }

    @Override
    public PaginationResult<DomainSummaryDto> getFilteredDomainSummariesDto(DomainFilter filter) {
        return domainDao.getFilteredDomainSummaries(filter, getSecurityContext());
    }

    @Override
    public Set<String> getFilteredDomainIds(DomainFilter filter) {
        return domainDao.getFilteredDomainIds(filter, getSecurityContext());
    }

    @Override
    public List<DomainDto> getDomains(Collection<String> domainIds) {
        List<Domain> domains = domainDao.forTopiaIdIn(domainIds).findAll();
        return domains.stream().map(anonymizeService.getDomainToDtoFunction(true)).collect(Collectors.toList());
    }

    @Override
    public void unactivateDomains(List<String> domainIds, boolean activate) {
        // TODO: 03/02/17 Valider le fait qu'il n'y ai pas de controle de droit sur la désactivation
        if (domainIds != null && !domainIds.isEmpty()) {
            List<Domain> domainsToUpdates = new ArrayList<>(domainIds.size());
            List<Domain> domains = domainDao.forTopiaIdIn(domainIds).findAll();
            if (domainIds.size() == domains.size()) {
                for (Domain domain : domains) {
                    if (domain.isActive() != activate) {
                        boolean prevoiusDomainState = domain.isActive();
                        domain.setActive(activate);
                        domain.setUpdateDate(context.getCurrentTime());
                        domainsToUpdates.add(domain);
                        if (LOGGER.isInfoEnabled()) {
                            LOGGER.info(String.format("L'utilisateur avec comme ID '%s' a changé le status actif du domaine '%s' avec comme ID '%s' de '%s' à '%s'",
                                    getSecurityContext().getUserId(),
                                    domain.getName(), domain.getTopiaId(),
                                    prevoiusDomainState, domain.isActive()));
                        }
                    }

                }
            } else {
                throw new AgrosystTechnicalException(String.format(
                        "Le nombre de domaines à metre à jour est différent du nombre de domaine retrouvés %d/%d",
                        domainIds.size(), domains.size()));
            }

            domainDao.updateAll(domainsToUpdates);
            getTransaction().commit();
        }
    }
    
    @Override
    public Equipment newMateriel() {
        return equipmentDao.newInstance();
    }

    @Override
    public Ground newSol() {
        return groundDao.newInstance();
    }

    @Override
    public Domain extendDomain(String domainTopiaId, int extendCampaign) throws DomainExtendException {

        Preconditions.checkArgument(extendCampaign >= CommonService.getInstance().lowerCampaignBound && extendCampaign <= CommonService.getInstance().upperCampaignBound,
                String.format("Campagne %d hors des limites authorisées [%d, %d]", extendCampaign, CommonService.getInstance().lowerCampaignBound, CommonService.getInstance().upperCampaignBound));
        Preconditions.checkArgument(StringUtils.isNotEmpty(domainTopiaId), "Aucun domaine à prolonger");

        Domain domain = extendDomainNoCommit(domainTopiaId, extendCampaign);

        getTransaction().commit();
        return domain;
    }

    @Override
    public Domain extendDomainNoCommit(String domainTopiaId, int extendCampaign) throws DomainExtendException {
        Preconditions.checkNotNull(domainTopiaId);
    
        authorizationService.checkCreateOrUpdateDomain(domainTopiaId);

        Preconditions.checkArgument(CommonService.getInstance().areCampaignsValids(String.valueOf(extendCampaign)), String.format("Campaigns %d is not valid (%d, %d)", extendCampaign, CommonService.getInstance().getCampaignsBounds().getLeft(), CommonService.getInstance().getCampaignsBounds().getRight()));

        Domain clonedDomainDraft;

        // get domain to duplicate
        Domain sourceDomain = domainDao.forTopiaIdEquals(domainTopiaId).findUnique();
    
        ExtendContext extendContext = new ExtendContext();

        // get all related domains
        // find the last one
        // check that the extend campaign year is valid
        LinkedHashMap<Integer, String> relatedDomains = domainDao.findAllRelatedDomains(sourceDomain.getCode(), true);

        if (relatedDomains != null && !relatedDomains.isEmpty()) {
            Integer closestLowerCampaign = null;
            for (Map.Entry<Integer, String> relatedDomain : relatedDomains.entrySet()) {
                Integer currentCampaign = relatedDomain.getKey();
                // The campaign must not by copy on a already copied campaign.
                if (currentCampaign == extendCampaign) {
                    throw new DomainExtendException("The domain is already extended for this campaign");

                    // The closest lower campaign from the required one is chosen.
                } else if (currentCampaign < extendCampaign && (closestLowerCampaign == null || (currentCampaign > closestLowerCampaign))) {
                    closestLowerCampaign = currentCampaign;
                }
            }
            // case where all campaign are > to the required extend campaign
            //  in this case the first next campaign is return.
            Set<Integer> relatedCampaigns = relatedDomains.keySet();
            if (closestLowerCampaign == null) {
                closestLowerCampaign = Iterables.getLast(relatedCampaigns);
            }
            String lastDomainId = relatedDomains.get(closestLowerCampaign);
            sourceDomain = domainDao.forTopiaIdEquals(lastDomainId).findUnique();
        } else if (sourceDomain.getCampaign() == extendCampaign) {
            throw new DomainExtendException("The domain is already extended for this campaign");
        }

        try {
            // perform clone
            Binder<Domain, Domain> domainBinder = BinderFactory.newBinder(Domain.class);
            clonedDomainDraft = domainDao.newInstance();
            domainBinder.copyExcluding(sourceDomain, clonedDomainDraft,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                    Domain.PROPERTY_WEATHER_STATIONS,
                    Domain.PROPERTY_DEFAULT_WEATHER_STATION,
                    Domain.PROPERTY_PRACTICED_PLOT);
            clonedDomainDraft.setTopiaId(null);
            clonedDomainDraft.setCampaign(extendCampaign);

            if (sourceDomain.getWeatherStations() != null) {
                final Binder<WeatherStation, WeatherStation> weatherStationBinder = BinderFactory.newBinder(WeatherStation.class);
                for (WeatherStation ws : sourceDomain.getWeatherStations()) {
                    WeatherStation clone = weatherStationDao.newInstance();
                    weatherStationBinder.copyExcluding(ws, clone,
                            TopiaEntity.PROPERTY_TOPIA_ID,
                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                            TopiaEntity.PROPERTY_TOPIA_VERSION);
                    clonedDomainDraft.addWeatherStations(clone);
                    // default
                    if (ws.equals(sourceDomain.getDefaultWeatherStation())) {
                        clonedDomainDraft.setDefaultWeatherStation(clone);
                    }
                }
            }

            // Now the new domain is automatically validated https://forge.codelutin.com/issues/4549
            LocalDateTime currentDate = context.getCurrentTime();
            clonedDomainDraft.setUpdateDate(currentDate);
            clonedDomainDraft.setValidated(true);
            clonedDomainDraft.setValidationDate(currentDate);

            // persist clone
            final Domain clonedDomain = domainDao.create(clonedDomainDraft);

            // duplicate collections
            List<GeoPoint> geoPoints = geoPointDao.forDomainEquals(sourceDomain).findAll();
            if (geoPoints != null && !geoPoints.isEmpty()) {
                final Binder<GeoPoint, GeoPoint> gpsDataBinder = BinderFactory.newBinder(GeoPoint.class);
                for (GeoPoint geoPoint : geoPoints) {

                    GeoPoint clone = geoPointDao.newInstance();
                    gpsDataBinder.copyExcluding(geoPoint, clone,
                            TopiaEntity.PROPERTY_TOPIA_ID,
                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                            TopiaEntity.PROPERTY_TOPIA_VERSION);
                    clone.setDomain(clonedDomain);
                    geoPointDao.create(clone);
                }
            }

            List<LivestockUnit> livestockUnits = livestockUnitDao.forDomainEquals(sourceDomain).findAll();
            if (CollectionUtils.isNotEmpty(livestockUnits)){
                List<LivestockUnit> livestockUnitsToCreate = new ArrayList<>(livestockUnits.size());
                List<Cattle> cattlesToCreate = new ArrayList<>();
                List<Ration> rationsToCreate = new ArrayList<>();
                List<Aliment> alimentsToCreate = new ArrayList<>();

                final Binder<LivestockUnit, LivestockUnit> livestockUnitsBinder = BinderFactory.newBinder(LivestockUnit.class);
                final Binder<Cattle, Cattle> cattlesBinder = BinderFactory.newBinder(Cattle.class);
                final Binder<Ration, Ration> rationsBinder = BinderFactory.newBinder(Ration.class);
                final Binder<Aliment, Aliment> alimentsBinder = BinderFactory.newBinder(Aliment.class);

                for (LivestockUnit livestockUnit : livestockUnits) {
                    LivestockUnit clone = livestockUnitDao.newInstance();
                    livestockUnitsBinder.copyExcluding(livestockUnit, clone,
                            TopiaEntity.PROPERTY_TOPIA_ID,
                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                            TopiaEntity.PROPERTY_TOPIA_VERSION,
                            LivestockUnit.PROPERTY_CATTLES);

                    clone.setDomain(clonedDomain);

                    if (livestockUnit.getCattles() != null) {
                        for (Cattle cattle : livestockUnit.getCattles()) {
                            Cattle clonedCattle = cattleDao.newInstance();
                            cattlesBinder.copyExcluding(cattle, clonedCattle,
                                    TopiaEntity.PROPERTY_TOPIA_ID,
                                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                                    Cattle.PROPERTY_RATIONS);

                            if (cattle.getRations() != null) {
                                for (Ration ration : cattle.getRations()) {
                                    Ration clonedRation = rationDao.newInstance();
                                    rationsBinder.copyExcluding(ration, clonedRation,
                                            TopiaEntity.PROPERTY_TOPIA_ID,
                                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                            TopiaEntity.PROPERTY_TOPIA_VERSION,
                                            Ration.PROPERTY_ALIMENTS);

                                    if (ration.getAliments() != null) {
                                        for (Aliment aliment : ration.getAliments()) {
                                            Aliment clonedAliment = alimentDao.newInstance();
                                            alimentsBinder.copyExcluding(aliment, clonedAliment,
                                                    TopiaEntity.PROPERTY_TOPIA_ID,
                                                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                                    TopiaEntity.PROPERTY_TOPIA_VERSION);
                                            alimentsToCreate.add(clonedAliment);
                                            clonedRation.addAliments(clonedAliment);
                                        }
                                        rationsToCreate.add(clonedRation);
                                        clonedCattle.addRations(clonedRation);
                                    }
                                }
                            }
                            cattlesToCreate.add(clonedCattle);
                            clone.addCattles(clonedCattle);
                        }
                        livestockUnitsToCreate.add(clone);
                    }
                }
                alimentDao.createAll(alimentsToCreate);
                rationDao.createAll(rationsToCreate);
                cattleDao.createAll(cattlesToCreate);
                livestockUnitDao.createAll(livestockUnitsToCreate);
            }

            List<Ground> grounds = groundDao.forDomainEquals(sourceDomain).findAll();
            if (grounds != null && !grounds.isEmpty()) {
                Binder<Ground, Ground> solBinder = BinderFactory.newBinder(Ground.class);
                for (Ground sol : grounds) {
                    Ground cloneSol = groundDao.newInstance();
                    solBinder.copyExcluding(sol, cloneSol,
                            TopiaEntity.PROPERTY_TOPIA_ID,
                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                            TopiaEntity.PROPERTY_TOPIA_VERSION);
                    cloneSol.setDomain(clonedDomain);
                    cloneSol = groundDao.create(cloneSol);
                    extendContext.addGround(sol, cloneSol);
                }
            }

            // The map's key is the topiaId of the equipement which has been used as source for duplication
            final Map<String, Equipment> equipmentClones = new HashMap<>();
            List<Equipment> equipments = equipmentDao.forDomainEquals(sourceDomain).findAll();
            if (equipments != null && !equipments.isEmpty()) {
                final Binder<Equipment, Equipment> binder = BinderFactory.newBinder(Equipment.class);
                for (Equipment equipment : equipments) {

                    Equipment clone = equipmentDao.newInstance();
                    binder.copyExcluding(equipment, clone,
                            TopiaEntity.PROPERTY_TOPIA_ID,
                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                            TopiaEntity.PROPERTY_TOPIA_VERSION);
                    clone.setDomain(clonedDomain);
                    clone = equipmentDao.create(clone);
                    equipmentClones.put(equipment.getTopiaId(), clone);
                }
            }

            List<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(sourceDomain).findAll();
            if (toolsCouplings != null && !toolsCouplings.isEmpty()) {
                for (ToolsCoupling toolsCoupling : toolsCouplings) {
                    Binder<ToolsCoupling, ToolsCoupling> binder = BinderFactory.newBinder(ToolsCoupling.class);
                    ToolsCoupling clonedToolsCoupling = toolsCouplingDao.newInstance();
                    binder.copyExcluding(toolsCoupling, clonedToolsCoupling,
                            TopiaEntity.PROPERTY_TOPIA_ID,
                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                            TopiaEntity.PROPERTY_TOPIA_VERSION,
                            ToolsCoupling.PROPERTY_MAINS_ACTIONS,
                            ToolsCoupling.PROPERTY_TRACTOR,
                            ToolsCoupling.PROPERTY_EQUIPMENTS);

                    clonedToolsCoupling.setDomain(clonedDomain);
                    if (toolsCoupling.getTractor() != null) {
                        clonedToolsCoupling.setTractor(equipmentClones.get(toolsCoupling.getTractor().getTopiaId()));
                    }
                    Set<RefInterventionAgrosystTravailEDI> clonedMainsActions = new HashSet<>(toolsCoupling.getMainsActions());
                    clonedToolsCoupling.setMainsActions(clonedMainsActions);

                    if (toolsCoupling.getEquipments() != null) {
                        List<Equipment> clonedEquipements = new ArrayList<>();
                        for (Equipment equipement : toolsCoupling.getEquipments()) {
                            clonedEquipements.add(equipmentClones.get(equipement.getTopiaId()));
                        }
                        clonedToolsCoupling.setEquipments(clonedEquipements);
                    }

                    toolsCouplingDao.create(clonedToolsCoupling);
                }
            }

            // Assolement (croppingPlan)
            Collection<CroppingPlanEntry> sourceCroppingPlans = getCroppingPlan0(sourceDomain.getTopiaId());
            Collection<CroppingPlanEntry> extendCroppingPlans = new ArrayList<>();
            if (sourceCroppingPlans != null) {
                Binder<CroppingPlanEntry, CroppingPlanEntry> binder = BinderFactory.newBinder(CroppingPlanEntry.class);
                final Binder<CroppingPlanSpecies, CroppingPlanSpecies> speciesBinder = BinderFactory.newBinder(CroppingPlanSpecies.class);
                for (CroppingPlanEntry cpEntry : sourceCroppingPlans) {
                    final CroppingPlanEntry clonedCpEntry = croppingPlanEntryDao.newInstance();
                    binder.copyExcluding(cpEntry, clonedCpEntry,
                            TopiaEntity.PROPERTY_TOPIA_ID,
                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                            TopiaEntity.PROPERTY_TOPIA_VERSION,
                            CroppingPlanEntry.PROPERTY_CROPPING_PLAN_SPECIES);
                    clonedCpEntry.setDomain(clonedDomain);
                    croppingPlanEntryDao.create(clonedCpEntry);
                    extendCroppingPlans.add(clonedCpEntry);
                    
                    if (cpEntry.getCroppingPlanSpecies() != null) {
                        List<CroppingPlanSpecies> interventionDtos = cpEntry.getCroppingPlanSpecies().stream().map(
                                getCroppingPlanSpeciesCroppingPlanSpeciesFunction(speciesBinder, clonedCpEntry, croppingPlanSpeciesDao)).collect(Collectors.toList());
                        List<CroppingPlanSpecies> croppingPlanSpecies = ImmutableList.copyOf(interventionDtos);
                        clonedCpEntry.setCroppingPlanSpecies(croppingPlanSpecies);
                    }
                }
            }
            
            domainInputStockUnitService.duplicateDomainInputStocks(
                    sourceDomain,
                    clonedDomain,
                    extendCroppingPlans);

            // others entities to clone
            List<GrowingPlan> growingPlans = growingPlanDao.forDomainEquals(sourceDomain).addEquals(GrowingPlan.PROPERTY_ACTIVE, true).findAll();
            for (GrowingPlan growingPlan : growingPlans) {
                growingPlanService.duplicateGrowingPlan(extendContext, clonedDomain, growingPlan, true);
            }

            List<Plot> plots = plotDao.forDomainEquals(sourceDomain).addEquals(Plot.PROPERTY_ACTIVE, true).findAll();
            for (Plot plot : plots) {
                Map<GrowingSystem, GrowingSystem> growingSystemCache = extendContext.getGrowingSystemCache();
                GrowingSystem cloneGrowingSystem = growingSystemCache.get(plot.getGrowingSystem());
                
                plotService.extendPlot(extendContext, clonedDomain, cloneGrowingSystem, plot);

            }

            return clonedDomain;
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't clone object", ex);
        }

    }

    protected static Function<CroppingPlanSpecies, CroppingPlanSpecies> getCroppingPlanSpeciesCroppingPlanSpeciesFunction(Binder<CroppingPlanSpecies, CroppingPlanSpecies> speciesBinder, CroppingPlanEntry clonedCpEntry, CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao) {
        return input -> {
            CroppingPlanSpecies clone = croppingPlanSpeciesDao.newInstance();
            speciesBinder.copyExcluding(input, clone,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION);
            clone.setCroppingPlanEntry(clonedCpEntry);
            return croppingPlanSpeciesDao.create(clone);
            };
    }

    @Override
    public LinkedHashMap<Integer, String> getRelatedDomains(String domainCode) {
        return domainDao.findAllRelatedDomains(domainCode, false);
    }

    @Override
    public ToolsCoupling newToolsCoupling() {
        return toolsCouplingDao.newInstance();
    }

    protected List<CroppingPlanEntry> getCroppingPlan0(String domainId) {

        String propertyDomainId = CroppingPlanEntry.PROPERTY_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID;
        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryDao.forProperties(propertyDomainId, domainId).setOrderByArguments(CroppingPlanEntry.PROPERTY_NAME).findAll();

        ReferentialTranslationMap translationMap = i18nService.fillCroppingPlanEntryTranslationMaps(croppingPlanEntries);

        croppingPlanEntries.forEach(cpe -> CroppingPlans.translateCroppingPlanEntry(cpe, translationMap));

        return croppingPlanEntries;
    }

    protected List<CroppingPlanSpecies> getCroppingPlanSpecies(Domain domain) {

        String query = CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY + "." + CroppingPlanEntry.PROPERTY_DOMAIN;
        return croppingPlanSpeciesDao.forProperties(query, domain).findAll();
    }

    @Override
    public ExportResult exportDomainAsXls(Collection<String> domainIds) {

        List<DomainBean> domainBeans = new LinkedList<>();
        List<GpsBean> gpsBeans = new LinkedList<>();
        List<LivestockUnitBean> livestockUnitBeans = new LinkedList<>();
        List<GroundBean> groundBeans = new LinkedList<>();
        List<StatusBean> statusBeans = new LinkedList<>();
        List<EquipmentBean> equipmentBeans = new LinkedList<>();
        List<ToolsCouplingBean> toolsCouplingBeans = new LinkedList<>();
        List<CroppingBean> croppingBeans = new LinkedList<>();
        List<PlotBean> plotBeans = new LinkedList<>();
        List<InputBean> inputBeans = new LinkedList<>();
        List<DomainExportBeansAndModels.HarvestingPriceBean> harvestingPriceBeans = new LinkedList<>();
        // missing Price

        try {
            if (CollectionUtils.isNotEmpty(domainIds)) {
                Locale locale = getSecurityContext().getLocale();

                long start = System.currentTimeMillis();
                Iterable<List<String>> chunks = Iterables.partition(domainIds, 10);
                for (List<String> chunk : chunks) {

                    domainDao.clear();

                    List<Domain> domains = domainDao.forTopiaIdIn(chunk).findAll();
                    for (Domain domain : domains) {

                        // anonymize domain
                        domain = anonymizeService.checkForDomainAnonymization(domain);

                        // Common data for all tabs
                        DomainExportBeansAndModels.CommonBean baseBean = new DomainExportBeansAndModels.CommonBean(
                                domain.getType(),
                                domain.getLocation().getDepartement(),
                                domain.getLocation().getCodePostal(),
                                domain.getName(),
                                domain.getCampaign(),
                                domain.getMainContact()
                        );

                        // main tab
                        DomainBean domainBean = new DomainBean(baseBean);
                        domainBean.setPetiteRegionAgricole(Optional.ofNullable(domain.getLocation()).map(RefLocation::getPetiteRegionAgricoleNom).orElse(null));
                        domainBean.setZoning(domain.getZoning());
                        domainBean.setUaaVulnarablePart(domain.getUaaVulnarablePart());
                        domainBean.setUaaStructuralSurplusAreaPart(domain.getUaaStructuralSurplusAreaPart());
                        domainBean.setUaaActionPart(domain.getUaaActionPart());
                        domainBean.setUaaNatura2000Part(domain.getUaaNatura2000Part());
                        domainBean.setUaaErosionRegionPart(domain.getUaaErosionRegionPart());
                        domainBean.setUaaWaterResourceProtectionPart(domain.getUaaWaterResourceProtectionPart());
                        domainBean.setStakesTourist(domain.getStakesTourist());
                        domainBeans.add(domainBean);

                        // gps data tab
                        List<GeoPoint> domainGeoPoints = geoPointDao.forDomainEquals(domain).findAll();
                        for (GeoPoint domainGeoPoint : domainGeoPoints) {
                            GpsBean gpsBean = new GpsBean(baseBean);
                            gpsBean.setGeoPointName(domainGeoPoint.getName());
                            gpsBean.setGeoPointLatitude(domainGeoPoint.getLatitude());
                            gpsBean.setGeoPointLongitude(domainGeoPoint.getLongitude());
                            gpsBean.setGeoPointDescription(domainGeoPoint.getDescription());
                            gpsBeans.add(gpsBean);
                        }

                        // LivestockUnits
                        List<LivestockUnit> livestockUnits = livestockUnitDao.forDomainEquals(domain).findAll();
                        for (LivestockUnit livestockUnit : livestockUnits) {
                            Supplier<LivestockUnitBean> supplier = () -> {
                                LivestockUnitBean instance = new LivestockUnitBean(baseBean);
                                instance.setRef_animal_type(Optional.ofNullable(livestockUnit.getRefAnimalType()).map(RefAnimalType::getAnimalType).orElse(""));
                                instance.setLivestock_unit_size(livestockUnit.getLivestockUnitSize());
                                instance.setPermanent_grassland_area(livestockUnit.getPermanentGrasslandArea());
                                instance.setTemporary_grassland_area(livestockUnit.getTemporaryGrasslandArea());
                                instance.setPermanent_mowed_grassland_area(livestockUnit.getPermanentMowedGrasslandArea());
                                instance.setTemporary_mowed_grassland_area(livestockUnit.getTemporaryMowedGrasslandArea());
                                instance.setForage_crops_area(livestockUnit.getForageCropsArea());
                                instance.setProducing_food_area(livestockUnit.getProducingFoodArea());
                                instance.setHolding_straw_area(livestockUnit.getHoldingStrawArea());
                                instance.setMass_self_sustaining_percent(livestockUnit.getMassSelfSustainingPercent());
                                instance.setAverage_straw_for_bedding(livestockUnit.getAverageStrawForBedding());
                                instance.setAverage_manure_produced(livestockUnit.getAverageManureProduced());
                                instance.setAverage_liquid_effluent(livestockUnit.getAverageLiquidEffluent());
                                instance.setAverage_composted_effluent(livestockUnit.getAverageCompostedEffluent());
                                instance.setAverage_methanised_effluent(livestockUnit.getAverageMethanisedEffluent());
                                instance.setComment(livestockUnit.getComment());
                                return instance;
                            };

                            Collection<Cattle> cattles = livestockUnit.getCattles();
                            if (CollectionUtils.isEmpty(cattles)) {
                                LivestockUnitBean livestockUnitBean = supplier.get();
                                livestockUnitBeans.add(livestockUnitBean);
                            } else {
                                for (Cattle cattle : cattles) {
                                    Collection<Ration> rations = cattle.getRations();
                                    for (Ration ration : rations) {
                                        Collection<Aliment> aliments = ration.getAliments();
                                        for (Aliment aliment : aliments) {
                                            LivestockUnitBean livestockUnitBean = supplier.get();
                                            livestockUnitBean.setNumber_of_heads(cattle.getNumberOfHeads());
                                            livestockUnitBean.setAnimal_type(cattle.getAnimalType().getAnimalType());

                                            livestockUnitBean.setStarting_half_month(ration.getStartingHalfMonth());
                                            livestockUnitBean.setEnding_half_month(ration.getEndingHalfMonth());

                                            livestockUnitBean.setQuantity(aliment.getQuantity());

                                            livestockUnitBean.setAliment_type(aliment.getAliment().getAlimentType());
                                            livestockUnitBean.setAliment_unit(aliment.getAliment().getAlimentUnit());

                                            livestockUnitBeans.add(livestockUnitBean);
                                        }
                                    }
                                }
                            }
                        }

                        // sol data tab
                        List<Ground> grounds = groundDao.forDomainEquals(domain).findAll();
                        for (Ground ground : grounds) {
                            GroundBean groundBean = new GroundBean(baseBean);
                            Optional<RefSolArvalis> solArvalis = Optional.ofNullable(ground.getRefSolArvalis());
                            groundBean.setRegion(solArvalis.map(RefSolArvalis::getSol_region).orElse(null));
                            groundBean.setSolArvalis(solArvalis.map(RefSolArvalis::getSol_nom).orElse(null));
                            groundBean.setLocalName(ground.getName());
                            groundBean.setImportance(ground.getImportance());
                            groundBean.setComment(ground.getComment());
                            groundBeans.add(groundBean);
                        }

                        // status data tab
                        StatusBean statusBean = new StatusBean(baseBean);
                        statusBean.setLegal_status(Optional.ofNullable(domain.getLegalStatus()).map(RefLegalStatus::getLibelle_INSEE).orElse(null));
                        statusBean.setStatus_comment(domain.getStatusComment());
                        statusBean.setSiret(domain.getSiret());
                        statusBean.setChief_birth_year(domain.getChiefBirthYear());
                        statusBean.setOtex18(Optional.ofNullable(domain.getOtex18()).map(RefOTEX::getLibelle_OTEX_18_postes).orElse(null));
                        statusBean.setOtex70(Optional.ofNullable(domain.getOtex70()).map(RefOTEX::getLibelle_OTEX_70_postes).orElse(null));
                        statusBean.setOrientation(domain.getOrientation());
                        statusBean.setPartners_number(domain.getPartnersNumber());
                        statusBean.setObjectives(domain.getObjectives());
                        statusBean.setDomain_assets(domain.getDomainAssets());
                        statusBean.setDomain_constraints(domain.getDomainConstraints());
                        statusBean.setDescription(domain.getDescription());
                        statusBean.setDomain_likely_trends(domain.getDomainLikelyTrends());
                        statusBean.setCooperative_member(domain.isCooperativeMember());
                        statusBean.setDevelopment_group_member(domain.isDevelopmentGroupMember());
                        statusBean.setCuma_member(domain.isCumaMember());
                        statusBean.setOther_work_force(domain.getOtherWorkForce());
                        statusBean.setPermanent_employees_work_force(domain.getPermanentEmployeesWorkForce());
                        statusBean.setTemporary_employees_work_force(domain.getTemporaryEmployeesWorkForce());
                        statusBean.setOperator_work_force(domain.getOperatorWorkForce());
                        statusBean.setNon_seasonal_work_force(domain.getNonSeasonalWorkForce());
                        statusBean.setSeasonal_work_force(domain.getSeasonalWorkForce());
                        statusBean.setVolunteer_work_force(domain.getVolunteerWorkForce());
                        statusBean.setWorkforce_comment(domain.getWorkforceComment());
                        statusBean.setUsed_agricultural_area(domain.getUsedAgriculturalArea());
                        statusBean.setUsed_agricultural_area_for_farming(domain.getUsedAgriculturalAreaForFarming());
                        statusBean.setIrrigable_area(domain.getIrrigableArea());
                        statusBean.setFallow_area(domain.getFallowArea());
                        statusBean.setAnnual_crop_area(domain.getAnnualCropArea());
                        statusBean.setVineyard_and_orchard_area(domain.getVineyardAndOrchardArea());
                        statusBean.setMeadow_area(domain.getMeadowArea());
                        statusBean.setMeadow_always_with_grass_area(domain.getMeadowAlwaysWithGrassArea());
                        statusBean.setMeadow_other_area(domain.getMeadowOtherArea());
                        statusBean.setMeadow_only_pastured_area(domain.getMeadowOnlyPasturedArea());
                        statusBean.setMeadow_only_mowed_area(domain.getMeadowOnlyMowedArea());
                        statusBean.setMeadow_pastured_and_mowed_area(domain.getMeadowPasturedAndMowedArea());
                        statusBean.setHeathland_and_routes_area(domain.getHeathlandAndRoutesArea());
                        statusBean.setSummer_and_mountain_pasture_area(domain.getSummerAndMountainPastureArea());
                        statusBean.setCollective_heathland_and_routes_area(domain.getCollectiveHeathlandAndRoutesArea());
                        statusBean.setCollective_summer_and_mountain_pasture_area(domain.getCollectiveSummerAndMountainPastureArea());
                        statusBean.setTotal_other_areas(domain.getTotalOtherAreas());
                        statusBean.setFamily_work_force_wage(domain.getFamilyWorkForceWage());
                        statusBean.setWage_costs(domain.getWageCosts());
                        statusBean.setMsa_fee(domain.getMsaFee());
                        statusBean.setAverage_tenant_farming(domain.getAverageTenantFarming());
                        statusBean.setDecoupled_assistance(domain.getDecoupledAssistance());
                        statusBeans.add(statusBean);

                        // materiel tab
                        List<Equipment> domainEquipments = equipmentDao.forDomainEquals(domain).findAll();
                        for (Equipment equipment : domainEquipments) {
                            EquipmentBean equipmentBean = new EquipmentBean(baseBean);
                            equipmentBean.setRefMateriel(equipment.getRefMateriel());
                            equipmentBean.setName(equipment.getName());
                            equipmentBean.setDescription(equipment.getDescription());
                            equipmentBean.setMaterielEta(equipment.isMaterielETA());
                            equipmentBean.setHomemadeMaterial(equipment.isHomemadeMaterial());
                            equipmentBean.setJerryRigged(equipment.isJerryRigged());
                            equipmentBean.setWeakenedMaterial(equipment.isWeakenedMaterial());
                            equipmentBeans.add(equipmentBean);
                        }

                        // toolsCouplings tab
                        List<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(domain).findAll();
                        for (ToolsCoupling toolsCoupling : toolsCouplings) {
                            Collection<RefInterventionAgrosystTravailEDI> mainsActions = toolsCoupling.getMainsActions();
                            for (RefInterventionAgrosystTravailEDI mainAction : mainsActions) {

                                Supplier<ToolsCouplingBean> supplier = () -> {
                                    ToolsCouplingBean instance = new ToolsCouplingBean(baseBean);
                                    instance.setTools_coupling_name(toolsCoupling.getToolsCouplingName());
                                    instance.setWorkforce(toolsCoupling.getWorkforce());
                                    instance.setWork_rate(toolsCoupling.getWorkRate());
                                    instance.setWork_rate_unit(toolsCoupling.getWorkRateUnit());
                                    instance.setComment(toolsCoupling.getComment());
                                    instance.setAgrosystInterventionTypes(mainAction.getIntervention_agrosyst());
                                    instance.setMainsActions(mainAction.getReference_label());
                                    instance.setTractor(Optional.ofNullable(toolsCoupling.getTractor()).map(Equipment::getName).orElse(null));
                                    return instance;
                                };

                                Collection<Equipment> equipments = toolsCoupling.getEquipments();
                                if (equipments.isEmpty()) {
                                    ToolsCouplingBean toolsCouplingBean = supplier.get();
                                    toolsCouplingBeans.add(toolsCouplingBean);
                                } else {
                                    for (Equipment equipment : equipments) {
                                        ToolsCouplingBean toolsCouplingBean = supplier.get();
                                        toolsCouplingBean.setEquipments(equipment.getName());
                                        toolsCouplingBeans.add(toolsCouplingBean);
                                    }
                                }
                            }
                        }

                        // assolement tab
                        List<CroppingPlanEntry> croppingPlan = getCroppingPlan0(domain.getTopiaId());
                        for (CroppingPlanEntry croppingPlanEntry : croppingPlan) {

                            Supplier<CroppingBean> supplier = () -> {
                                CroppingBean instance = new CroppingBean(baseBean);
                                instance.setName(croppingPlanEntry.getName());
                                instance.setType(croppingPlanEntry.getType());
                                instance.setTemporary_meadow(croppingPlanEntry.getTemporaryMeadow());
                                instance.setPastured_meadow(croppingPlanEntry.isPasturedMeadow());
                                instance.setMowed_meadow(croppingPlanEntry.isMowedMeadow());
                                instance.setYeald_average(croppingPlanEntry.getYealdAverage());
                                instance.setYeald_unit(croppingPlanEntry.getYealdUnit());
                                instance.setAverage_ift(croppingPlanEntry.getAverageIFT());
                                instance.setBiocontrol_ift(croppingPlanEntry.getBiocontrolIFT());
                                instance.setEstimating_ift_rules(croppingPlanEntry.getEstimatingIftRules());
                                instance.setIft_seeds_type(croppingPlanEntry.getIftSeedsType());
                                instance.setDose_type(croppingPlanEntry.getDoseType());
                                instance.setSelling_price(croppingPlanEntry.getSellingPrice());
                                return instance;
                            };

                            List<CroppingPlanSpecies> species = croppingPlanEntry.getCroppingPlanSpecies();
                            if (CollectionUtils.isEmpty(species)) {
                                CroppingBean croppingBean = supplier.get();
                                croppingBeans.add(croppingBean);
                            } else {
                                for (CroppingPlanSpecies aSpecies : species) {

                                    CroppingBean croppingBean = supplier.get();
                                    Optional<RefEspece> especeOptional = Optional.ofNullable(aSpecies.getSpecies());
                                    especeOptional.map(RefEspece::getLibelle_espece_botanique_Translated).ifPresent(croppingBean::setLibelle_espece_botanique);
                                    especeOptional.map(RefEspece::getLibelle_qualifiant_AEE_Translated).ifPresent(croppingBean::setLibelle_qualifiant__aee);
                                    especeOptional.map(RefEspece::getLibelle_type_saisonnier_AEE_Translated).ifPresent(croppingBean::setLibelle_type_saisonnier__aee);
                                    especeOptional.map(RefEspece::getLibelle_destination_AEE_Translated).ifPresent(croppingBean::setLibelle_destination__aee);
                                    if (aSpecies.getVariety() != null) {
                                        croppingBean.setLabel(aSpecies.getVariety().getLabel());
                                    } else if (StringUtils.isNotBlank(aSpecies.getEdaplosUnknownVariety())) {
                                        croppingBean.setLabel(I18n.l(locale, "fr.inra.agrosyst.api.services.edaplos.unknownVariety", aSpecies.getEdaplosUnknownVariety()));
                                    }
                                    croppingBeans.add(croppingBean);
                                }
                            }
                        }

                        // plot tab
                        PlotBean plotBean = new PlotBean(baseBean);
                        plotBean.setNb_plot(domain.getNbPlot());
                        plotBean.setFurthest_plot_distance(domain.getFurthestPlotDistance());
                        plotBean.setArea_around_hq(domain.getAreaAroundHQ());
                        plotBean.setGrouped_plots(domain.isGroupedPlots());
                        plotBean.setRather_grouped_plots(domain.isRatherGroupedPlots());
                        plotBean.setScattered_plots(domain.isScatteredPlots());
                        plotBean.setRather_scattered_plots(domain.isRatherScatteredPlots());
                        plotBean.setJoined_plots(domain.isJoinedPlots());
                        plotBeans.add(plotBean);

                        // Input tab
                        final Collection<AbstractDomainInputStockUnit> domainInputs = domainInputStockUnitService.loadDomainInputStockByIds(domain).values();
                        List<AbstractDomainInputStockUnit> seedInputs = domainInputs.stream()
                                .filter(di -> InputType.SEMIS.equals(di.getInputType()) || InputType.TRAITEMENT_SEMENCE.equals(di.getInputType()) || InputType.PLAN_COMPAGNE.equals(di.getInputType()))
                                .toList();

                        List<AbstractDomainInputStockUnit> seedLotInput = seedInputs.stream().filter(di -> di instanceof DomainSeedLotInput).toList();
                        for (AbstractDomainInputStockUnit input : seedLotInput) {
                            DomainSeedLotInput domainSeedLotInput = (DomainSeedLotInput) input;
                            final String lotName = input.getInputName() + "(" + domainSeedLotInput.getCropSeed().getName() + ")";

                            Collection<DomainSeedSpeciesInput> domainSeedSpeciesInput = domainSeedLotInput.getDomainSeedSpeciesInput();
                            if (CollectionUtils.isEmpty(domainSeedSpeciesInput)) {

                                InputBean lotInputBean = new InputBean(baseBean);
                                lotInputBean.setInputType(input.getInputType());
                                lotInputBean.setLotName(lotName);

                                SeedPrice inputLotPrice = domainSeedLotInput.getSeedPrice();
                                if (domainSeedLotInput.getInputPrice() != null) {
                                    lotInputBean.setRealPrice(inputLotPrice.getPrice());
                                    lotInputBean.setRealPriceUnit(inputLotPrice.getPriceUnit().name());
                                    lotInputBean.setPriceIncludeTreatment(inputLotPrice.isIncludedTreatment());
                                }

                                inputBeans.add(lotInputBean);
                            } else {
                                for (DomainSeedSpeciesInput seedSpeciesInput : domainSeedSpeciesInput) {

                                    CroppingPlanSpecies speciesSeed = seedSpeciesInput.getSpeciesSeed();
                                    RefEspece species = speciesSeed.getSpecies();
                                    String speciesName = species.getLibelle_espece_botanique_Translated();
                                    RefVariete variete = speciesSeed.getVariety();
                                    if (variete != null) {
                                        speciesName += " (" + variete.getLabel() + ")";
                                    }
                                    String finalSpeciesName = speciesName;

                                    Supplier<InputBean> speciesSupplier = () -> {
                                        InputBean inputBean = new InputBean(baseBean);
                                        inputBean.setInputType(InputType.SEMIS);
                                        inputBean.setLotName(lotName);
                                        inputBean.setLotSpeciesName(finalSpeciesName);
                                        inputBean.setBiologicalSeedInoculation(seedSpeciesInput.isBiologicalSeedInoculation());
                                        inputBean.setChemicalTreatment(seedSpeciesInput.isChemicalTreatment());
                                        return inputBean;
                                    };

                                    Collection<DomainPhytoProductInput> speciesPhytoInputs = seedSpeciesInput.getSpeciesPhytoInputs();
                                    InputBean speciesInputBean = speciesSupplier.get();
                                    SeedPrice speciesSeedPrice = seedSpeciesInput.getSeedPrice();
                                    if (speciesSeedPrice != null) {
                                        speciesInputBean.setRealPrice(speciesSeedPrice.getPrice());
                                        speciesInputBean.setRealPriceUnit(speciesSeedPrice.getPriceUnit().name());
                                        speciesInputBean.setPriceIncludeTreatment(speciesSeedPrice.isIncludedTreatment());
                                    }
                                    inputBeans.add(speciesInputBean);

                                    if (CollectionUtils.isNotEmpty(speciesPhytoInputs)) {
                                        for (DomainPhytoProductInput speciesPhytoInput : speciesPhytoInputs) {
                                            InputBean phytoSpeciesInputBean = speciesSupplier.get();
                                            phytoSpeciesInputBean.setInputType(speciesPhytoInput.getInputType());
                                            phytoSpeciesInputBean.setProductName(speciesPhytoInput.getInputName());
                                            if (speciesPhytoInput.getInputPrice() != null) {
                                                phytoSpeciesInputBean.setRealPrice(speciesPhytoInput.getInputPrice().getPrice());
                                                phytoSpeciesInputBean.setRealPriceUnit(speciesPhytoInput.getInputPrice().getPriceUnit().name());
                                            }
                                            ProductType productType = speciesPhytoInput.getProductType();
                                            String productTypeName = productType != null ? productType.name() : null;
                                            phytoSpeciesInputBean.setProductType(productTypeName);
                                            phytoSpeciesInputBean.setAmmNumber(speciesPhytoInput.getRefInput().getCode_AMM());

                                            inputBeans.add(phytoSpeciesInputBean);
                                        }
                                    }
                                }
                            }

                        }

                        List<AbstractDomainInputStockUnit> abstractDomainInputStockUnits = ListUtils.subtract(new ArrayList<>(domainInputs), seedInputs);

                        for (AbstractDomainInputStockUnit input : abstractDomainInputStockUnits) {
                            InputBean inputBean = new InputBean(baseBean);
                            inputBean.setInputType(input.getInputType());
                            inputBean.setProductName(input.getInputName());
                            if (input.getInputPrice() != null) {
                                inputBean.setRealPrice(input.getInputPrice().getPrice());
                                inputBean.setRealPriceUnit(input.getInputPrice().getPriceUnit().name());
                            }

                            switch (input.getInputType()) {
                                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                                    DomainMineralProductInput product = (DomainMineralProductInput) input;
                                    RefFertiMinUNIFA refInput = product.getRefInput();

                                    inputBean.setProductType(refInput.getType_produit());
                                    String composition = buildComposition(
                                            Pair.of("N", String.valueOf(refInput.getN())),
                                            Pair.of("K₂O", String.valueOf(refInput.getK2O())),
                                            Pair.of("P₂O₅", String.valueOf(refInput.getP2O5())),
                                            Pair.of("B", String.valueOf(refInput.getBore())),
                                            Pair.of("Ca", String.valueOf(refInput.getCalcium())),
                                            Pair.of("Fe", String.valueOf(refInput.getFer())),
                                            Pair.of("Mg", String.valueOf(refInput.getManganese())),
                                            Pair.of("Mo", String.valueOf(refInput.getMolybdene())),
                                            Pair.of("MgO", String.valueOf(refInput.getMgO())),
                                            Pair.of("NaO", String.valueOf(refInput.getOxyde_de_sodium())),
                                            Pair.of("SO₃", String.valueOf(refInput.getsO3())),
                                            Pair.of("Cu", String.valueOf(refInput.getCuivre())),
                                            Pair.of("Zn", String.valueOf(refInput.getZinc())));
                                    inputBean.setComposition(composition);
                                }
                                case EPANDAGES_ORGANIQUES -> {
                                    DomainOrganicProductInput product = (DomainOrganicProductInput) input;
                                    inputBean.setProductType(product.getRefInput().getLibelle());

                                    // Components are entered in kg/T, which is a ‰, so we divide by 10 to have a %
                                    String composition = buildComposition(
                                            Pair.of("N", String.valueOf(product.getN() / 10.0)),
                                            Pair.of("P₂O₅", String.valueOf(product.getP2O5() / 10.0)),
                                            Pair.of("K₂O", String.valueOf(product.getK2O() / 10.0)),
                                            Pair.of("CaO", product.getCaO() != null ? String.valueOf(product.getCaO() / 10.0) : ""),
                                            Pair.of("MgO", product.getMgO() != null ? String.valueOf(product.getMgO() / 10.0) : ""),
                                            Pair.of("S", product.getS() != null ? String.valueOf(product.getS() / 10.0) : ""));
                                    inputBean.setComposition(composition);
                                }
                                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, LUTTE_BIOLOGIQUE, TRAITEMENT_SEMENCE -> {
                                    DomainPhytoProductInput product = (DomainPhytoProductInput) input;
                                    String productTypeName = product.getProductType() != null ? product.getProductType().name() : null;
                                    inputBean.setProductType(productTypeName);
                                    inputBean.setAmmNumber(product.getRefInput().getCode_AMM());
                                }
                                case CARBURANT, IRRIGATION, MAIN_OEUVRE_MANUELLE, MAIN_OEUVRE_TRACTORISTE -> inputBean.setProductName(input.getInputType().name());
                                case AUTRE, POT, SEMIS, SUBSTRAT, PLAN_COMPAGNE -> {}
                            }
                            inputBeans.add(inputBean);
                        }

                        List<HarvestingPrice> harvestingPrices = pricesService.loadHarvestingPrices(domain.getTopiaId());
                        for (HarvestingPrice harvestingPrice : harvestingPrices) {
                            DomainExportBeansAndModels.HarvestingPriceBean harvestingPriceBean = new DomainExportBeansAndModels.HarvestingPriceBean(baseBean);
                            PracticedSystem practicedSystem = harvestingPrice.getPracticedSystem();
                            GrowingSystem growingSystem;
                            String plotOrPracticedSystemName;
                            String campaigns;
                            if (practicedSystem != null) {
                                harvestingPriceBean.setPracticed(true);
                                growingSystem = practicedSystem.getGrowingSystem();
                                plotOrPracticedSystemName = practicedSystem.getName();
                                campaigns = practicedSystem.getCampaigns();
                            } else {
                                Plot plot = harvestingPrice.getZone().getPlot();
                                growingSystem = plot.getGrowingSystem();
                                plotOrPracticedSystemName = plot.getName();
                                campaigns = String.valueOf(domain.getCampaign());
                            }
                            if (growingSystem != null) {
                                harvestingPriceBean.setGrowingSystemName(growingSystem.getName());
                            }
                            harvestingPriceBean.setPlotOrPracticedSystemName(plotOrPracticedSystemName);
                            harvestingPriceBean.setCampaigns(campaigns);
                            harvestingPriceBean.setCropName(harvestingPrice.getDisplayName());
                            HarvestingActionValorisation harvestingActionValorisation = harvestingPrice.getHarvestingActionValorisation();
                            harvestingPriceBean.setDestination(harvestingActionValorisation.getDestination().getDestination());
                            harvestingPriceBean.setYealdAverage(harvestingActionValorisation.getYealdAverage());
                            harvestingPriceBean.setRealPrice(harvestingPrice.getPrice());
                            harvestingPriceBean.setRealPriceUnit(harvestingPrice.getPriceUnit().name());

                            harvestingPriceBeans.add(harvestingPriceBean);
                        }

                    }
                }

                if (LOGGER.isInfoEnabled()) {
                    long duration = System.currentTimeMillis() - start;
                    int count = domainIds.size();
                    LOGGER.warn(String.format("%d domaines exportés en %dms (%dms/domaine)", count, duration, duration / count));
                }
            }
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't copy properties", ex);
        }

        ExportModelAndRows<DomainBean> domainBeanExportModelAndRows = new ExportModelAndRows<>(new DomainModel(), domainBeans);
        ExportModelAndRows<GpsBean> gpsBeanExportModelAndRows = new ExportModelAndRows<>(new GpsModel(), gpsBeans);
        ExportModelAndRows<LivestockUnitBean> livestockUnitBeanExportModelAndRows = new ExportModelAndRows<>(new LivestockUnitModel(refAnimalTypeTopiaDao, refCattleAnimalTypeTopiaDao, refCattleRationAlimentTopiaDao), livestockUnitBeans);
        ExportModelAndRows<GroundBean> groundBeanExportModelAndRows = new ExportModelAndRows<>(new GroundModel(refSolArvalisTopiaDao), groundBeans);
        ExportModelAndRows<StatusBean> statusBeanExportModelAndRows = new ExportModelAndRows<>(new StatusModel(refOTEXDao, refLegalStatusDao), statusBeans);
        ExportModelAndRows<EquipmentBean> equipmentBeanExportModelAndRows = new ExportModelAndRows<>(new EquipmentModel(refMaterielTopiaDao), equipmentBeans);
        ExportModelAndRows<ToolsCouplingBean> toolsCouplingBeanExportModelAndRows = new ExportModelAndRows<>(new ToolsCouplingModel(), toolsCouplingBeans);
        ExportModelAndRows<CroppingBean> croppingBeanExportModelAndRows = new ExportModelAndRows<>(new CroppingModel(refEspeceDao), croppingBeans);
        ExportModelAndRows<PlotBean> plotExportModelAndRows = new ExportModelAndRows<>(new PlotModel(), plotBeans);
        ExportModelAndRows<InputBean> inputBeanExportModelAndRows = new ExportModelAndRows<>(new InputModel(), inputBeans);
        ExportModelAndRows<DomainExportBeansAndModels.HarvestingPriceBean> harvestingPriceBeanExportModelAndRows = new ExportModelAndRows<>(new DomainExportBeansAndModels.HarvestingPriceModel(), harvestingPriceBeans);

        EntityExporter exporter = new EntityExporter();
        ExportResult result = exporter.exportAsXlsx("domains-export",
                domainBeanExportModelAndRows,
                gpsBeanExportModelAndRows,
                livestockUnitBeanExportModelAndRows,
                groundBeanExportModelAndRows,
                statusBeanExportModelAndRows,
                equipmentBeanExportModelAndRows,
                toolsCouplingBeanExportModelAndRows,
                inputBeanExportModelAndRows,
                croppingBeanExportModelAndRows,
                plotExportModelAndRows,
                harvestingPriceBeanExportModelAndRows
        );
        return result;
    }

    /**
     * Build a composition from the given parts
     * Example: ‘N (33%) - K2O (5%)’
     * @param parts an array of element/percentage pairs
     */
    private String buildComposition(Pair<String ,String> ...parts) {
        List<String> components = new ArrayList<>();
        for (Pair<String, String> componentWithValue : parts) {
            String component = componentWithValue.getKey() + (StringUtils.isNoneBlank(componentWithValue.getValue()) ? " (" + componentWithValue.getValue() + "%)" : " ");
            components.add(component);
        }
        return StringUtils.join(components, " - ");
    }

    @Override
    public void exportDomainAsXlsAsync(Collection<String> domainIds) {

        AuthenticatedUser user = getAuthenticatedUser();

        DomainExportTask exportTask = new DomainExportTask(user.getTopiaId(), user.getEmail(), domainIds);
        getBusinessTaskManager().schedule(exportTask);
    }

    @Override
    public boolean checkDomainExistence(String domainName) {
        long domain = domainDao.countDomainWithName(domainName);
        return domain > 0;
    }

    @Override
    public long getDomainsCount(Boolean active) {
        if (active == null) {
            return domainDao.count();
        }
        return domainDao.forActiveEquals(active).count();
    }


    @Override
    public Domain validateAndCommit(String domainId) {

        authorizationService.checkValidateDomain(domainId);

        // AThimel 09/12/13 Perform DB update is more powerful
        domainDao.validateDomain(domainId, context.getCurrentTime());

        getTransaction().commit();

        // AThimel 09/12/13 The next line makes sure that cache state is synchronized with database state
        getPersistenceContext().getHibernateSupport().getHibernateSession().clear();

        return domainDao.forTopiaIdEquals(domainId).findUnique();
    }

    @Override
    public List<GeoPoint> getGeoPoints(String domainId) {
        Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
        List<GeoPoint> result = geoPointDao.forDomainEquals(domain).findAll();
        result = anonymizeService.checkForGeoPointAnonymization(result);
        return result;
    }

    @Override
    public List<Equipment> getEquipments(String domainId) {
        Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
        return getTranslatedEquipmentsForDomain(domain);
    }

    @Override
    public List<Equipment> getTranslatedEquipmentsForDomain(Domain domain) {
        List<Equipment> equipments = equipmentDao.forDomainEquals(domain).findAll();
        Set<String> refMaterielIds = equipments.stream()
                .filter(e -> e.getRefMateriel() != null)
                .map(e -> e.getRefMateriel().getTopiaId())
                .collect(Collectors.toSet());

        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(getSecurityContext().getLanguage());
        refMaterielTopiaDao.fillTranslations(refMaterielIds, translationMap);

        AgrosystI18nService.translateEquipments(equipments, translationMap);
        return equipments;
    }

    @Override
    public List<Equipment> getEquipmentsForDomain(Domain domain) {
        List<Equipment> equipments = equipmentDao.forDomainEquals(domain).findAll();
        return equipments;
    }

    @Override
    public boolean isDefaultEdaplosEquipmentForDomain(List<Equipment> equipments) {
        Set<RefMateriel> materiels = equipments.stream().map(Equipment::getRefMateriel).filter(Objects::nonNull).collect(Collectors.toSet());
        for (RefMateriel materiel : materiels) {
            try {
                // use the codetype as it is common to all RefMaterial sub class and has the same value as codeRef witch has not same getter
                String codetype = (String) materiel.getClass().getMethod("getCodetype").invoke(materiel);
                if (codetype.equals(getConfig().getRefMaterielDefaultTractorCode()) ||
                        codetype.equals(getConfig().getRefMaterielDefaultIrrigationCode()) ||
                        codetype.equals(getConfig().getRefMaterielDefaultEquipmentCode()) ||
                        codetype.equals(getConfig().getRefMaterielDefaultAutomoteurCode())
                ) {
                    return true;
                }
            } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                LOGGER.error("An access on 'getCodetype' has failed on a RefMateriel SubClass", e);
            }
        }
        return false;
    }

    @Override
    public List<Ground> getGrounds(String domainId) {
        Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
        List<Ground> grounds = groundDao.forDomainEquals(domain).findAll();
        List<RefSolArvalis> solArvalisList = grounds.stream()
                .map(Ground::getRefSolArvalis)
                .toList();
        Language language = getSecurityContext().getLanguage();
        i18nService.fillRefSolArvalisTranslations(language, solArvalisList);
        return grounds;
    }

    @Override
    public List<DomainInputDto> getInputStockUnits(String domainId) {
        return domainInputStockUnitService.loadDomainInputStockDtos(domainId)
                .stream().toList();
    }

    @Override
    public List<ToolsCoupling> getToolsCouplings(String domainId) {
        Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
        return getToolsCouplingsForDomain(domain);
    }

    @Override
    public List<ToolsCoupling> getToolsCouplingsForDomain(Domain domain) {
        return toolsCouplingDao.forDomainEquals(domain).findAll();
    }

    @Override
    public List<ToolsCoupling> getToolsCouplingsForAllCampaignsDomain(Domain domain) {
        return toolsCouplingDao.findToolsCouplingsForAllCampaignsDomain(domain);
    }

    @Override
    public UsageList<ToolsCoupling> getToolsCouplingAndUsage(String domainId) {
        Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
        List<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(domain).findAll();

        i18nService.fillToolsCouplingTraductions(toolsCouplings);

        UsageList<ToolsCoupling> toolsCouplingUsageList = entityUsageService.getToolsCouplingUsageList(toolsCouplings, domain.getCampaign());
        return toolsCouplingUsageList;
    }

    @Override
    public Map<InputType, UsageList<DomainInputDto>> getDomainInputDtoUsageByTypes(String domainId) {
        final Collection<DomainInputDto> domainInputDtos = domainInputStockUnitService.loadDomainInputStockDtos(domainId);
        Map<InputType, List<DomainInputDto>> domainInputDtosByInputType = new HashMap<>();
        domainInputDtos.forEach(dto -> {
            List<DomainInputDto> dtosForInputType = domainInputDtosByInputType.computeIfAbsent(dto.getInputType(), k -> new ArrayList<>());
            dtosForInputType.add(dto);
        });

        Map<InputType, UsageList<DomainInputDto>> result = entityUsageService.getDomainInputUsageList(domainInputDtosByInputType);
        return result;
    }

    @Override
    public List<DomainInputDto> getDomainInputDtoForType(String domainIdentifier, String growingSystemId, InputType inputType) {
        String domainId;
        if (StringUtils.isNoneBlank(growingSystemId)) {
            final GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(growingSystemId).findUnique();
            domainId = growingSystem.getGrowingPlan().getDomain().getTopiaId();
        } else {
            domainId = domainIdentifier;
        }
        Preconditions.checkNotNull(domainId);

        return domainInputStockUnitService.loadDomainInputStockDtos(domainId, inputType);
    }

    @Override
    public List<String> getCroppingPlanCodeForDomainsAndCampaigns(String code, Set<Integer> campaigns) {
        return domainDao.findCroppingPlanCodeForDomainsAndCampaigns(code, campaigns);
    }

    @Override
    public Map<String, List<CroppingPlanSpecies>> getCroppingPlanSpeciesForDomainAndCampaignsByCropCode(String domainCode, Set<Integer> campaigns) {
        return domainDao.findCroppingPlanSpeciesForDomainAndCampaignsByCropCode(domainCode, campaigns);
    }

    @Override
    public List<CroppingPlanEntry> getCroppingPlanEntriesForDomainCodeAndCampaigns(String domainCode, Set<Integer> campaigns) {
        return domainDao.findCroppingPlanEntriesForDomainAndCampaignsByCropCode(domainCode, campaigns);
    }

    @Override
    public List<String> getToolsCouplingCodeForDomainsAndCampaigns(String code, Set<Integer> campaigns) {
        return domainDao.findToolsCouplingCodeForDomainsAndCampaigns(code, campaigns);
    }

    @Override
    public List<ToolsCoupling> getToolsCouplingsForDomainCodeAndCampaigns(String code, Set<Integer> campaigns) {
        List<ToolsCoupling> toolsCouplings = domainDao.findToolsCouplingsForDomainCodeAndCampaigns(code, campaigns);
        i18nService.fillToolsCouplingTraductions(toolsCouplings);
        return toolsCouplings;
    }

    @Override
    public List<Equipment> getEquipmentsForDomainCodeAndCampaigns(String code, Set<Integer> campaigns) {
        return domainDao.findEquipmentsForDomainCodeAndCampaigns(code, campaigns);
    }

    @Override
    public List<CroppingPlanEntryDto> getCroppingPlanDtos(String domainId) {
        List<CroppingPlanEntry> entities = getCroppingPlan0(domainId);

        ReferentialTranslationMap translationMap = i18nService.fillCroppingPlanEntryTranslationMaps(entities);

        List<CroppingPlanEntryDto> result = entities.stream()
                .map(cp -> CroppingPlans.getDtoForCroppingPlanEntry(cp, translationMap))
                .collect(Collectors.toList());
        return result;
    }

    @Override
    public List<CroppingPlanEntry> getCroppingPlanEntriesForDomain(Domain domain) {
        return croppingPlanEntryDao.forDomainEquals(domain).findAll();
    }

    @Override
    public List<CroppingPlanEntry> getCroppingPlanEntriesForAllCampaignsDomain(Domain domain) {
        return croppingPlanEntryDao.findCroppingPlanEntriesForAllCampaignsDomain(domain);
    }

    @Override
    public List<CroppingPlanEntry> getCroppingPlansBodies(String domainId) {
        List<CroppingPlanEntry> croppingPlanEntries0 = getCroppingPlan0(domainId);
        List<CroppingPlanEntry> result = new ArrayList<>(croppingPlanEntries0.size());

        Binder<CroppingPlanEntry, CroppingPlanEntry> binder = BinderFactory.newBinder(CroppingPlanEntry.class);
        for (CroppingPlanEntry croppingPlanEntry : croppingPlanEntries0) {
            CroppingPlanEntry target = new CroppingPlanEntryImpl();
            binder.copyExcluding(croppingPlanEntry, target,
                    CroppingPlanEntry.PROPERTY_DOMAIN,
                    CroppingPlanEntry.PROPERTY_CROPPING_PLAN_SPECIES);
            result.add(target);
        }
        return result;
    }

    @Override
    public UsageList<CroppingPlanEntryDto> getCroppingPlanEntryDtoAndUsage(String domainId) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(domainId));
        Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
        List<CroppingPlanEntry> croppingPlanEntries = getCroppingPlan0(domain.getTopiaId());

        ReferentialTranslationMap translationMap = i18nService.fillCroppingPlanEntryTranslationMaps(croppingPlanEntries);

        return entityUsageService.getCroppingPlanEntryDtoAndUsage(croppingPlanEntries, domain.getCampaign(), translationMap);
    }

    @Override
    public Map<String, Boolean> getCroppingPlanSpeciesUsage(String domainId) {
        Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
        List<CroppingPlanSpecies> elements = getCroppingPlanSpecies(domain);
        return entityUsageService.getCroppingPlanSpeciesUsageMap(elements, domain.getCampaign(), domain.getCode());
    }

    @Override
    public Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>> getEntryAndSpeciesFromCode(String croppingPlanEntryCode, Set<Integer> campaignsSet) {
        Preconditions.checkArgument(campaignsSet != null && !campaignsSet.isEmpty());

        // Maybe we have multiple entries, merge them
        List<CroppingPlanEntry> entries = croppingPlanEntryDao.findEntriesFromCode(croppingPlanEntryCode, campaignsSet);
        Preconditions.checkState(entries != null && !entries.isEmpty(), String.format("No CroppingPlanEntry found on campaigns %s with code: %s", campaignsSet, croppingPlanEntryCode));

        // Get all species
        Map<String, CroppingPlanSpecies> species = Maps.newLinkedHashMap();
        for (CroppingPlanEntry entry : entries) {
            if (entry.getCroppingPlanSpecies() != null) {
                Map<String, CroppingPlanSpecies> speciesByCode = Maps.uniqueIndex(entry.getCroppingPlanSpecies(), CroppingPlans.GET_SPECIES_CODE::apply);
                species.putAll(speciesByCode);
            }
        }

        CroppingPlanEntry entry = entries.getFirst();
        return Pair.of(entry, species);
    }

    @Override
    public Map<String, Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>>> findSpeciesByCropBySpeciesCodeForCropCodesOnCampaigns(List<String> croppingPlanEntryCodes, Set<Integer> campaignsSet) {
        Map<String, Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>>> result = new HashMap<>();
        if (CollectionUtils.isNotEmpty(croppingPlanEntryCodes) && CollectionUtils.isNotEmpty(campaignsSet)) {

            for (String croppingPlanEntryCode : croppingPlanEntryCodes) {
                // une Pair <Culture, Map<code espece, Espece>>
                Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>> pair = getEntryAndSpeciesFromCode(croppingPlanEntryCode, campaignsSet);
                if (pair != null && pair.getValue() != null) {
                    result.put(pair.getKey().getCode(), pair);
                }
            }
        }

        return result;
    }

    @Override
    public String getDomainCodeForGrowingSystem(String growingSystemId) {
        return domainDao.getDomainCodeForGrowingSystem(growingSystemId);
    }

    @Override
    public List<Domain> getActiveWritableDomainsForDecisionRuleCreation(DomainFilter domainFilter) {
        return domainDao.getActiveWritableDomainsForDecisionRuleCreation(getSecurityContext(), domainFilter);
    }

    @Override
    public List<String> getDomainIdsForGrowingSystemAndCampaigns(String gsID, Set<Integer> campaigns, boolean includeCropsFromInactiveDomains) {
        return domainDao.findDomainIdsForGrowingSystemIdAndCampaigns(gsID, campaigns, includeCropsFromInactiveDomains);
    }

    @Override
    public String getDomainIdForGrowingSystem(String gsID) {
        return domainDao.getDomainIdForGrowingSystem(gsID);
    }

    @Override
    public List<String> getDomainIdsForDomainAndCampaigns(String domainId, Set<Integer> campaigns, boolean includeCropsFromInactiveDomains) {
        return domainDao.findDomainIdsForDomainIdAndCampaigns(domainId, campaigns, includeCropsFromInactiveDomains);
    }

    protected void validateEquipments(List<Equipment> equipments) {
        // validation des materiels
        if (equipments != null) {
            equipments.removeAll(Collections.singleton(null));
            for (Equipment equipment : equipments) {
                Preconditions.checkArgument(StringUtils.isNotBlank(equipment.getName()));
            }
        }
    }

    @Override
    public List<String> copyTools(String fromDomain, List<String> toDomainIds, Boolean includeToolCouplings, List<Equipment> equipmentsToCopy, List<ToolsCoupling> toolsCouplingsToCopy) {

        Preconditions.checkArgument(StringUtils.isNotBlank(fromDomain) && CollectionUtils.isNotEmpty(toDomainIds), "Tools Copy can not be done because from Domain and to domains are not set !");

        // List of "domain name - campaign"
        List<String> result = new ArrayList<>(toDomainIds.size());

        validateEquipments(equipmentsToCopy);
        if (includeToolCouplings) {
            validateToolsCooplings(toolsCouplingsToCopy);
        }

        Domain domainFrom = domainDao.forTopiaIdEquals(fromDomain).findUnique();
        Collection<Domain> toDomains = domainDao.forTopiaIdIn(toDomainIds).findAll();

        if (equipmentsToCopy == null) {
            equipmentsToCopy = new ArrayList<>();
        }

        Map<String, Equipment> indexedEquipments = new HashMap<>();
        for (Equipment equipment : equipmentsToCopy) {
            indexedEquipments.put(Entities.GET_TOPIA_ID.apply(equipment), equipment);
        }

        if(toDomains != null && !toDomains.isEmpty()) {
            Binder<Equipment, Equipment> binder = BinderFactory.newBinder(Equipment.class);

            for (Domain toDomain : toDomains) {
                Map<String, Equipment> copiedEquipments = Maps.newHashMapWithExpectedSize(indexedEquipments.size());
                // copy equipments to the domain
                if (toDomain.getCode().equals(domainFrom.getCode())) {
                    // if same domain on other campaign Equipment Code is preserve to keep relation
                    // avoid duplicated equipment
                    for (Equipment equipment : equipmentsToCopy) {
                        Equipment toEquipment = null;
                        if (StringUtils.isNotBlank(equipment.getCode())) {
                            toEquipment = equipmentDao.forProperties(Equipment.PROPERTY_DOMAIN, toDomain, Equipment.PROPERTY_CODE, equipment.getCode()).findUniqueOrNull();
                        }
                        if (toEquipment == null) {
                            String originalId = equipment.getTopiaId();
                            Equipment copiedEquipment = equipmentDao.newInstance();
                            binder.copyExcluding(equipment, copiedEquipment,
                                    TopiaEntity.PROPERTY_TOPIA_ID,
                                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                                    Equipment.PROPERTY_DOMAIN);
                            // case of new equipment
                            if (StringUtils.isEmpty(copiedEquipment.getCode())) {
                                copiedEquipment.setCode(UUID.randomUUID().toString());
                            }
                            copiedEquipment.setDomain(toDomain);
                            copiedEquipment = equipmentDao.create(copiedEquipment);
                            copiedEquipments.put(originalId, copiedEquipment);
                        } else {

                            // the equipment already exists but it is added to the list to use it with tools couplings
                            copiedEquipments.put(equipment.getTopiaId(), toEquipment);
                        }
                    }
                } else {
                    for(Equipment equipment:equipmentsToCopy) {
                        Equipment copiedEquipment = equipmentDao.newInstance();
                        binder.copyExcluding(equipment, copiedEquipment,
                                TopiaEntity.PROPERTY_TOPIA_ID,
                                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                TopiaEntity.PROPERTY_TOPIA_VERSION,
                                Equipment.PROPERTY_CODE,
                                Equipment.PROPERTY_DOMAIN);
                        copiedEquipment.setCode(UUID.randomUUID().toString());
                        copiedEquipment.setDomain(toDomain);
                        copiedEquipment = equipmentDao.create(copiedEquipment);
                        copiedEquipments.put(equipment.getTopiaId(), copiedEquipment);
                    }
                }

                //copy toolsCoupling to domain
                if(includeToolCouplings && toolsCouplingsToCopy != null && !toolsCouplingsToCopy.isEmpty()){
                    List<ToolsCoupling> copiedToolsCouplings = new ArrayList<>();

                        Binder<ToolsCoupling, ToolsCoupling> toolsCouplingBinder = BinderFactory.newBinder(ToolsCoupling.class);

                        if (toDomain.getCode().equals(domainFrom.getCode())) {
                            // if same domain on other campaign ToolsCoupling Code is preserve to keep relation
                            for (ToolsCoupling toolsCoupling : toolsCouplingsToCopy) {
                                // avoid duplicated toolsCoupling
                                ToolsCoupling toToolsCoupling = null;
                                if (StringUtils.isNotBlank(toolsCoupling.getCode())) {
                                    toToolsCoupling = toolsCouplingDao.forProperties(ToolsCoupling.PROPERTY_DOMAIN, toDomain, ToolsCoupling.PROPERTY_CODE, toolsCoupling.getCode()).findUniqueOrNull();
                                }
                                if (toToolsCoupling == null) {
                                    ToolsCoupling copiedToolsCoupling = toolsCouplingDao.newInstance();
                                    toolsCouplingBinder.copyExcluding(toolsCoupling, copiedToolsCoupling,
                                            TopiaEntity.PROPERTY_TOPIA_ID,
                                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                            TopiaEntity.PROPERTY_TOPIA_VERSION,
                                            ToolsCoupling.PROPERTY_DOMAIN,
                                            ToolsCoupling.PROPERTY_TRACTOR,
                                            ToolsCoupling.PROPERTY_EQUIPMENTS,
                                            ToolsCoupling.PROPERTY_MAINS_ACTIONS
                                    );
                                    // case of new tools coupling
                                    if (StringUtils.isEmpty(copiedToolsCoupling.getCode())) {
                                        copiedToolsCoupling.setCode(UUID.randomUUID().toString());
                                    }
                                    copyRelatedEntitiesToToolsCoupling(copiedEquipments, copiedToolsCouplings, copiedToolsCoupling, toolsCoupling, toDomain);
                                }
                            }
                        } else {
                            for (ToolsCoupling toolsCoupling : toolsCouplingsToCopy) {
                                ToolsCoupling copiedToolsCoupling = toolsCouplingDao.newInstance();
                                toolsCouplingBinder.copyExcluding(toolsCoupling, copiedToolsCoupling,
                                        TopiaEntity.PROPERTY_TOPIA_ID,
                                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                                        ToolsCoupling.PROPERTY_DOMAIN,
                                        ToolsCoupling.PROPERTY_TRACTOR,
                                        ToolsCoupling.PROPERTY_EQUIPMENTS,
                                        ToolsCoupling.PROPERTY_MAINS_ACTIONS,
                                        ToolsCoupling.PROPERTY_CODE
                                );
                                copiedToolsCoupling.setCode(UUID.randomUUID().toString());
                                copyRelatedEntitiesToToolsCoupling(copiedEquipments, copiedToolsCouplings, copiedToolsCoupling, toolsCoupling, toDomain);
                            }
                        }
    
                    toolsCouplingDao.createAll(copiedToolsCouplings);
                }
                result.add(toDomain.getName() + " (" + toDomain.getCampaign() + ")");
            }
        }
        getTransaction().commit();
        return result;
    }

    @Override
    public List<String> copyInputs(String fromDomain, List<String> inputIds, List<String> toDomainIds, boolean forceCopy) {
        List<String> result = domainInputStockUnitService.copyInputStocks(fromDomain, inputIds, toDomainIds, forceCopy);
        getTransaction().commit();
        return result;
    }

    @Override
    public List<String> pasteCrops(String fromDomainId, List<String> toDomainIds, List<CroppingPlanEntryDto> cropDtos) {
    
        // List of "domain name - campaign"
        List<String> result = new ArrayList<>(toDomainIds.size());
    
        if (CollectionUtils.isNotEmpty(toDomainIds) && CollectionUtils.isNotEmpty(cropDtos)) {
            cropDtos = cropDtos.stream().filter(Objects::nonNull).toList();
            if (cropDtos.isEmpty()) {
                // nothing to do
                return result;
            }
        
            Domain fromDomain = domainDao.forTopiaIdEquals(fromDomainId).findUnique();
            List<Domain> toDomains = domainDao.forTopiaIdIn(toDomainIds).findAll();
        
            toDomains.forEach(toDomain -> result.add(toDomain.getName() + " (" + toDomain.getCampaign() + ")"));
        
            Set<String> refEspecebyIds = cropDtos.stream()
                    .map(CroppingPlanEntryDto::getSpecies)
                    .flatMap(Collection::stream)
                    .map(CroppingPlanSpeciesDto::getSpeciesId)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
            List<RefEspece> refEspeces = refEspeceDao.forTopiaIdIn(refEspecebyIds).findAll();
            Map<String, RefEspece> refEspeceByIds = Maps.uniqueIndex(refEspeces, Entities.GET_TOPIA_ID::apply);
            
            Set<String> refVarietyIds = cropDtos.stream()
                    .map(CroppingPlanEntryDto::getSpecies)
                    .flatMap(Collection::stream)
                    .map(CroppingPlanSpeciesDto::getVarietyId)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());

            Set<String> refDestinationIds = cropDtos.stream()
                    .map(CroppingPlanEntryDto::getSpecies)
                    .flatMap(Collection::stream)
                    .map(CroppingPlanSpeciesDto::getDestinationId)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());

            List<RefVariete> refVarietes = refVarieteDao.forTopiaIdIn(refVarietyIds).findAll();
            Map<String, RefVariete> refVarietyByIds = Maps.uniqueIndex(refVarietes, Entities.GET_TOPIA_ID::apply);

            Map<String, RefDestination> refDestinationByIds = Maps.uniqueIndex(refDestinationDao.forTopiaIdIn(refDestinationIds).findAll(), Entities.GET_TOPIA_ID::apply);
            
            if (toDomains.remove(fromDomain)) {
                pasteCropsToSameDomain(cropDtos, fromDomain, refEspeceByIds, refVarietyByIds, refDestinationByIds);
            }
            
            List<Domain> relatedDomain = toDomains.stream()
                    .filter(
                            domain -> fromDomain.getCode().equals(domain.getCode()))
                    .collect(Collectors.toList());
            
            if (CollectionUtils.isNotEmpty(relatedDomain)) {
                pasteCropsToRelatedDomains(cropDtos, refEspeceByIds, refVarietyByIds, refDestinationByIds, relatedDomain);
            }
            
            toDomains.removeAll(relatedDomain);
            if (CollectionUtils.isNotEmpty(toDomains)) {
                pasteCropsToOtherDomains(cropDtos, toDomains, refEspeceByIds, refVarietyByIds, refDestinationByIds);
            }
        }
        
        getPersistenceContext().commit();
        return result;
    }
    
    /**
     * Paste crops from given cropDto to same domain on same campaign:
     * - a new code is generated to dissociate crop and species
     *
     * @param cropDtos            crops to copies
     * @param fromToDomain        domain from crop where copied and where crop are paste
     * @param refEspeceByIds      refEspece by topiaIds
     * @param refVarietyByIds     refVariety by topiaIds
     * @param refDestinationByIds refDestination by TopiaIds
     */
    protected void pasteCropsToSameDomain(
            List<CroppingPlanEntryDto> cropDtos,
            Domain fromToDomain,
            Map<String, RefEspece> refEspeceByIds,
            Map<String, RefVariete> refVarietyByIds,
            Map<String, RefDestination> refDestinationByIds) {
        
        authorizationService.checkCreateOrUpdateDomain(fromToDomain.getTopiaId());
        
        pastCropsRemoveRelation(cropDtos, refEspeceByIds, refVarietyByIds, refDestinationByIds, fromToDomain);
        
    }
    
    /**
     * Copie crops from same domain on different campaign to given related domains
     * - if a crop match an existing crop by code, a new copied crop will be created without sharing there code
     * - if the copied crop mach any by code the crop is copied with original crop code, the relation ship is keep
     *
     * @param cropDtos            crops to copies
     * @param refEspeceByIds      refEspece by topiaIds
     * @param refVarietyByIds     refVariety by topiaIds
     * @param refDestinationByIds refDestination by topiaIds
     * @param relatedDomains      same domains according code to copy crops
     */
    protected void pasteCropsToRelatedDomains(
            List<CroppingPlanEntryDto> cropDtos,
            Map<String, RefEspece> refEspeceByIds,
            Map<String, RefVariete> refVarietyByIds,
            Map<String, RefDestination> refDestinationByIds,
            List<Domain> relatedDomains) {

        for (Domain toDomain : relatedDomains) {
            authorizationService.checkCreateOrUpdateDomain(toDomain.getTopiaId());
            
            List<CroppingPlanEntry> prevCrops = croppingPlanEntryDao.forDomainEquals(toDomain).findAll();
            Set<String> prevCropCodes = prevCrops.stream().map(CroppingPlanEntry::getCode).collect(Collectors.toSet());
            
            for (CroppingPlanEntryDto cropDto : cropDtos) {
                if (cropDto != null && StringUtils.isNotBlank(cropDto.getName()) &&
                        (cropDto.getYealdAverage() == null || cropDto.getYealdUnit() != null)) {
                    
                    final String cropCode = cropDto.getCode();
                    
                    List<CroppingPlanSpeciesDto> cpsDtos = cropDto.getSpecies();
                    cpsDtos.removeAll(Collections.singleton(null));
                    Iterables.removeIf(cpsDtos, HAS_NO_SPECIES::test);
                    
                    if (prevCropCodes.contains(cropCode)) {
                        // crop exists with same code
                        // a new crop will be created not related to existing one
                        cropDto.setCode(UUID.randomUUID().toString());
                        cpsDtos.forEach(speciesDto ->
                                speciesDto.setCode(UUID.randomUUID().toString()));
                    }
                    
                    CroppingPlanEntry cropEntity = croppingPlanEntryDao.newInstance();
                    cpeBinder.copyExcluding(cropDto, cropEntity,
                            TopiaEntity.PROPERTY_TOPIA_ID,
                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                            TopiaEntity.PROPERTY_TOPIA_VERSION,
                            CroppingPlanEntry.PROPERTY_CROPPING_PLAN_SPECIES);
                    cropEntity.setDomain(toDomain);
                    
                    removeNoneValidFields(cropEntity);
                    
                    createCroppingPlanSpeciesFromDto(refEspeceByIds, refVarietyByIds, refDestinationByIds, cpsDtos, cropEntity);
                    
                    croppingPlanEntryDao.create(cropEntity);
                    
                }
            }
        }
    }
    
    /**
     * Copie crops from an other domain to given toDomains
     * Crop and species avec new code generated
     *
     * @param cropDtos            crops to paste
     * @param toDomains           domain to past crops
     * @param refEspeceByIds      refEspece by topiaIds
     * @param refVarietyByIds     refVariety by topiaIds
     */
    protected void pasteCropsToOtherDomains(
            List<CroppingPlanEntryDto> cropDtos,
            List<Domain> toDomains,
            Map<String, RefEspece> refEspeceByIds,
            Map<String, RefVariete> refVarietyByIds, Map<String, RefDestination> refDestinationByIds) {
        
        for (Domain toDomain : toDomains) {
            authorizationService.checkCreateOrUpdateDomain(toDomain.getTopiaId());
            pastCropsRemoveRelation(cropDtos, refEspeceByIds, refVarietyByIds, refDestinationByIds, toDomain);
        }
    }
    
    protected void pastCropsRemoveRelation(
            List<CroppingPlanEntryDto> cropDtos,
            Map<String, RefEspece> refEspeceByIds,
            Map<String, RefVariete> refVarietyByIds,
            Map<String, RefDestination> refDestinationByIds,
            Domain toDomain) {

        for (CroppingPlanEntryDto cropDto : cropDtos) {
            if (cropDto != null && StringUtils.isNotBlank(cropDto.getName()) &&
                    (cropDto.getYealdAverage() == null || cropDto.getYealdUnit() != null)) {
                
                List<CroppingPlanSpeciesDto> cpsDtos = cropDto.getSpecies();
                cpsDtos.removeAll(Collections.singleton(null));
                Iterables.removeIf(cpsDtos, HAS_NO_SPECIES::test);
                
                cpsDtos.forEach(speciesDto ->
                        speciesDto.setCode(UUID.randomUUID().toString()));
                
                CroppingPlanEntry cropEntity = croppingPlanEntryDao.newInstance();
                cpeBinder.copyExcluding(cropDto, cropEntity,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        CroppingPlanEntry.PROPERTY_CODE,
                        CroppingPlanEntry.PROPERTY_CROPPING_PLAN_SPECIES);
                cropEntity.setDomain(toDomain);
                cropEntity.setCode(UUID.randomUUID().toString());
                
                removeNoneValidFields(cropEntity);
                
                createCroppingPlanSpeciesFromDto(refEspeceByIds, refVarietyByIds, refDestinationByIds, cpsDtos, cropEntity);
                
                croppingPlanEntryDao.create(cropEntity);
                
            }
        }
    }
    
    /**
     * Create new croppingPlanSpecies from given cpsDtos
     * /!\ be sure to have correct code set on DTO
     *
     * @param refEspeceByIds      needed RefEspece by topiaIds
     * @param refVarietyByIds     needed RefVariete by topiaIds
     * @param refDestinationByIds if destination
     * @param cpsDtos             CroppingPlanSpeciesDtos to create
     * @param cropEntity          CroppingPlanEntry to add copied crops
     */
    protected void createCroppingPlanSpeciesFromDto(
            Map<String, RefEspece> refEspeceByIds,
            Map<String, RefVariete> refVarietyByIds,
            Map<String, RefDestination> refDestinationByIds,
            List<CroppingPlanSpeciesDto> cpsDtos,
            CroppingPlanEntry cropEntity) {
        
        for (CroppingPlanSpeciesDto cpsDto : cpsDtos) {
            CroppingPlanSpecies cpsEntity = croppingPlanSpeciesDao.newInstance();
            cpsEntity.setCode(cpsDto.getCode());
            cpsEntity.setSpeciesArea(cpsDto.getSpeciesArea());
            cpsEntity.setCompagne(cpsDto.getCompagne());
            cpsEntity.setCroppingPlanEntry(cropEntity);
            cpsEntity.setValidated(cpsDto.isValidated());
            
            if (StringUtils.isNotBlank(cpsDto.getSpeciesId())) {
                RefEspece refEspece = refEspeceByIds.get(cpsDto.getSpeciesId());
                cpsEntity.setSpecies(refEspece);
            }
            
            if (StringUtils.isNotBlank(cpsDto.getVarietyId())) {
                RefVariete refVariete = refVarietyByIds.get(cpsDto.getVarietyId());
                cpsEntity.setVariety(refVariete);
            }

            if (StringUtils.isNotBlank(cpsDto.getDestinationId())) {
                RefDestination refDestination = refDestinationByIds.get(cpsDto.getDestinationId());
                cpsEntity.setDestination(refDestination);
            }
            
            cropEntity.addCroppingPlanSpecies(cpsEntity);
            
        }
    }
    
    /**
     * Set tractor, equipments and actions to the copied tools coupling
     *
     * @param copiedEquipments     map of original equipment topiaId to copied equipment
     * @param copiedToolsCouplings target domain's tools coupling
     * @param copiedToolsCoupling  new copied tools coupling
     * @param toolsCoupling        original tools coupling
     * @param toDomain             targeted domain
     */
    protected void copyRelatedEntitiesToToolsCoupling(Map<String, Equipment> copiedEquipments, List<ToolsCoupling> copiedToolsCouplings, ToolsCoupling copiedToolsCoupling, ToolsCoupling toolsCoupling, Domain toDomain) {
        // find tractor from new Equipment
        if (toolsCoupling.getTractor() != null) {
            copiedToolsCoupling.setTractor(copiedEquipments.get(toolsCoupling.getTractor().getTopiaId()));
        }

        // find equipments from new Equipements
        if (toolsCoupling.getEquipments() != null && !toolsCoupling.getEquipments().isEmpty()) {
            List<Equipment> copiedTCEquipments = new ArrayList<>(toolsCoupling.getEquipments().size());
            for (Equipment toolsCouplingEquipment : toolsCoupling.getEquipments()) {
                copiedTCEquipments.add(copiedEquipments.get(toolsCouplingEquipment.getTopiaId()));
            }
            copiedToolsCoupling.setEquipments(copiedTCEquipments);
        }
        Set<RefInterventionAgrosystTravailEDI> clonedMainsActions = new HashSet<>(toolsCoupling.getMainsActions());
        copiedToolsCoupling.setMainsActions(clonedMainsActions);
        copiedToolsCoupling.setDomain(toDomain);

        copiedToolsCouplings.add(copiedToolsCoupling);
    }


    @Override
    public double getDomainSAUArea(String domainId) {
        Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
        Double result = domain.getUsedAgriculturalArea();
        if (result == null) {
            result = plotDao.getDomainPlotTotalArea(domainId);
        }
        return result;
    }

    @Override
    public List<LivestockUnit> loadLivestockUnitsForDomainId(String domainId) {
        return livestockUnitDao.forDomain(domainId, getSecurityContext().getLanguage());
    }
    
    @Override
    public List<LivestockUnit> loadLivestockUnitsForDomainCode(String domainCode, Set<Integer> campaigns) {
        return livestockUnitDao.forAllDomainCampaigns(domainCode, campaigns, getSecurityContext().getLanguage());
    }
    
    @Override
    public Map<String, UsageList> getUsedLivestocksAndCattles(String domainId) {
        Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
        
        List<LivestockUnit> livestockUnits = loadLivestockUnitsForDomainId(domainId);
        Set<String> cattleCodes = new HashSet<>();
        for (LivestockUnit livestockUnit : livestockUnits) {
            cattleCodes.addAll(livestockUnit.getCattles().stream().map(Cattle::getCode).collect(Collectors.toSet()));
        }
    
        Set<String> usedCattleCodes = entityUsageService.filterOnCattleCodeUsed(cattleCodes, domain.getCampaign(), domain.getCode());
    
        Map<String, Boolean> livestockUsageMap = new HashMap<>();
        Map<String, Boolean> cattleUsageMap = new HashMap<>();
        List<Cattle> allCattle = new ArrayList<>();
        
        for (LivestockUnit livestockUnit : livestockUnits) {
            boolean used = livestockUnit.getCattles().stream().anyMatch(cattle -> usedCattleCodes.contains(cattle.getCode()));
            livestockUsageMap.put(livestockUnit.getTopiaId(), used);
            Collection<Cattle> cattles = livestockUnit.getCattles();
            if (CollectionUtils.isNotEmpty(cattles)) {
                for (Cattle cattle : cattles) {
                    used = usedCattleCodes.contains(cattle.getCode());
                    cattleUsageMap.put(cattle.getTopiaId(), used);
                }
                allCattle.addAll(cattles);
            }
        }
        UsageList<LivestockUnit> livestockUsageList = UsageList.of(livestockUnits, livestockUsageMap);
        UsageList<Cattle> cattleUsageList = UsageList.of(allCattle, cattleUsageMap);
        
        Map<String, UsageList> result = new HashMap<>();
        result.put(LivestockUnit.class.getName(), livestockUsageList);
        result.put(Cattle.class.getName(), cattleUsageList);
        
        return result;
    }
    
    @Override
    public Integer getCampaignForDomain(String domainId) {
        return domainDao.findCampaignForDomainId(domainId);
    }
    
    private void loadCropSpeciesAndVarieties(
            Collection<CroppingPlanEntryDto> croppingPlanDtos,
            Map<String, RefEspece> specieses,
            Map<String, RefVariete> varieties) {

        final Set<String> speciesIds = new HashSet<>();
        final Set<String> varietyIds = new HashSet<>();

        for (CroppingPlanEntryDto entry : croppingPlanDtos) {
            for (CroppingPlanSpeciesDto species : entry.getSpecies()) {
                String speciesId = species.getSpeciesId();
                speciesIds.add(speciesId);

                String varietyId = species.getVarietyId();
                varietyIds.add(varietyId);
            }
        }
        varietyIds.removeIf(Objects::isNull);

        List<RefEspece> refEspeces = (refEspeceDao.forTopiaIdIn(speciesIds).findAll());
        List<RefVariete> refVarietes = (refVarieteDao.forTopiaIdIn(varietyIds).findAll());

        specieses.putAll(Maps.uniqueIndex(refEspeces, Entities.GET_TOPIA_ID::apply));
        varieties.putAll(Maps.uniqueIndex(refVarietes, Entities.GET_TOPIA_ID::apply));
    }

    @Override
    public void deleteDomain(String domainId) throws DomainDeletionException {
        authorizationService.checkIsAdmin();

        Domain domain = domainDao.forTopiaIdEquals(domainId).findUnique();
        String domainCode = domain.getCode();

        // Domain -> Growing plan
        List<GrowingPlan> growingPlans = growingPlanDao.forDomainEquals(domain).findAll();
        Collection<String> growingPlanIds = growingPlans.stream().map(GrowingPlan::getTopiaId).collect(Collectors.toSet());

        // Domain -> Growing system
        List<GrowingSystem> growingSystems = growingSystemDao.forGrowingPlanIn(growingPlans).findAll();
        Collection<String> growingSystemIds = growingSystems.stream().map(GrowingSystem::getTopiaId).collect(Collectors.toSet());

        // Domain -> Growing system -> PracticedSystems
        List<PracticedSystem> practicedSystems = practicedSystemDao.forGrowingSystemIn(growingSystems).findAll();
        if (practicedSystems.stream().anyMatch(practicedSystem -> practicedSystem.getCampaigns().contains(","))) {
            throw new DomainDeletionException("Vous ne pouvez pas supprimer un domaine qui a un système synthétisé pluri-annuel");
        }

        List<PracticedCropCycle> practicedCropCycles = practicedCropCycleDao.forPracticedSystemIn(practicedSystems).findAll();
        Iterable<PracticedPlot> practicedPlots = practicedSystems.stream()
                .map(practicedSystem -> practicedPlotDao.forPracticedSystemContains(practicedSystem).findAllLazy())
                .reduce(Iterables::concat)
                .orElse(IterableUtils.emptyIterable());


        List<PracticedCropCyclePhase> practicedCropCyclePhases = practicedCropCycles.stream()
                .filter(practicedCropCycle -> practicedCropCycle instanceof PracticedPerennialCropCycle)
                .map(PracticedPerennialCropCycle.class::cast)
                .flatMap(practicedPerennialCropCycle -> practicedPerennialCropCycle.getCropCyclePhases().stream())
                .collect(Collectors.toList());

        List<PracticedCropCycleNode> practicedCropCycleNodes = practicedCropCycles.stream()
                .filter(practicedCropCycle -> practicedCropCycle instanceof PracticedSeasonalCropCycle)
                .map(PracticedSeasonalCropCycle.class::cast)
                .flatMap(practicedSeasonalCropCycle -> practicedSeasonalCropCycle.getCropCycleNodes().stream())
                .collect(Collectors.toList());

        Set<PracticedCropCycleConnection> practicedCropCycleConnections = new HashSet<>();
        practicedCropCycleConnections.addAll(practicedCropCycleConnectionDao.forSourceIn(practicedCropCycleNodes).findAll());
        practicedCropCycleConnections.addAll(practicedCropCycleConnectionDao.forTargetIn(practicedCropCycleNodes).findAll());

        Set<PracticedIntervention> practicedInterventions = new HashSet<>();
        practicedInterventions.addAll(practicedInterventionDao.forPracticedCropCyclePhaseIn(practicedCropCyclePhases).findAll());
        practicedInterventions.addAll(practicedInterventionDao.forPracticedCropCycleConnectionIn(practicedCropCycleConnections).findAll());

        // Domain -> Growing system -> ReportGrowingSystem
        Iterable<ReportGrowingSystem> reportGrowingSystems = reportGrowingSystemDao.forGrowingSystemIn(growingSystems).findAllLazy();

        // Decision rule + crop
        List<DecisionRule> decisionRules = decisionRuleDao.forDomainCodeEquals(domainCode).findAll();

        // Domain -> Growing system -> Management mode
        Iterable<ManagementMode> managementModes = managementModeDao.forGrowingSystemIn(growingSystems).findAllLazy();

        // Domain -> CroppingPlanEntry
        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryDao.forDomainEquals(domain).findAll();

        // Domain -> CroppingPlanEntry -> EffectiveCropCycleNode
        List<EffectiveCropCycleNode> effectiveCropCycleNodes = effectiveCropCycleNodeDao.forCroppingPlanEntryIn(croppingPlanEntries).findAll();

        // Domain -> Plots
        List<Plot> plots = plotDao.forDomainEquals(domain).findAll();

        // Domain -> Plots -> Zones
        List<Zone> zones = zoneDao.forPlotIn(plots).findAll();

        // Domain -> CroppingPlanEntry -> EffectivePerennialCropCycle
        Set<EffectivePerennialCropCycle> effectivePerennialCropCycles = new HashSet<>();
        effectivePerennialCropCycles.addAll(effectivePerennialCropCycleDao.forCroppingPlanEntryIn(croppingPlanEntries).findAll());
        // Domain -> Plots -> Zones -> EffectivePerennialCropCycle
        effectivePerennialCropCycles.addAll(effectivePerennialCropCycleDao.forZoneIn(zones).findAll());

        // Domain -> Plots -> Zones -> EffectiveSeasonalCropCycle
        List<EffectiveSeasonalCropCycle> effectiveSeasonalCropCycles = effectiveSeasonalCropCycleDao.forZoneIn(zones).findAll();

        // Domain -> Plots -> Zones -> EffectiveSeasonalCropCycle -> effectiveCropCycleNode
        Set<EffectiveCropCycleNode> allEffectiveCropCycleNodes = effectiveSeasonalCropCycles.stream()
                .flatMap(effectiveSeasonalCropCycle -> effectiveSeasonalCropCycle.getNodes().stream())
                .collect(Collectors.toSet());
        allEffectiveCropCycleNodes.addAll(effectiveCropCycleNodes);

        // Domain -> Plots -> Zones -> EffectiveSeasonalCropCycle -> EffectiveCropCycleNode -> EffectiveCropCycleConnection
        // Domain -> CroppingPlanEntry -> EffectiveCropCycleNode -> EffectiveCropCycleConnection
        Set<EffectiveCropCycleConnection> effectiveCropCycleConnections = new HashSet<>();
        effectiveCropCycleConnections.addAll(effectiveCropCycleConnectionDao.forTargetIn(effectiveCropCycleNodes).findAll());
        effectiveCropCycleConnections.addAll(effectiveCropCycleConnectionDao.forIntermediateCroppingPlanEntryIn(croppingPlanEntries).findAll());

        // Domain -> * -> EffectiveCropCyclePhase -> EffectiveIntervention
        // Domain -> * -> EffectiveCropCycleNode -> EffectiveIntervention
        List<EffectiveCropCyclePhase> effectiveCropCyclePhases = effectivePerennialCropCycles.stream()
                .map(EffectivePerennialCropCycle::getPhase)
                .collect(Collectors.toList());

        Set<EffectiveIntervention> effectiveInterventions = new HashSet<>();
        effectiveInterventions.addAll(effectiveInterventionDao.forEffectiveCropCyclePhaseIn(effectiveCropCyclePhases).findAll());
        effectiveInterventions.addAll(effectiveInterventionDao.forEffectiveCropCycleNodeIn(allEffectiveCropCycleNodes).findAll());

        // Domain -> * -> EffectiveIntervention -> Actions
        // Domain -> * -> PracticedIntervention -> Actions
        Set<AbstractAction> actions = new HashSet<>();
        actions.addAll(abstractActionDao.forEffectiveInterventionIn(effectiveInterventions).findAll());
        actions.addAll(abstractActionDao.forPracticedInterventionIn(practicedInterventions).findAll());

        Collection<HarvestingActionValorisation> harvestingActionValorisations = actions.stream()
                .filter(action -> action instanceof HarvestingAction)
                .map(HarvestingAction.class::cast)
                .flatMap(action -> action.getValorisations().stream())
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        // Domain -> Plots -> Zones -> Measurement session
        // Domain -> CroppingPlanEntry -> Measurement session
        Set<MeasurementSession> measurementSessions = new HashSet<>();
        measurementSessions.addAll(measurementSessionDao.forCroppingPlanEntryIn(croppingPlanEntries).findAll());
        measurementSessions.addAll(measurementSessionDao.forZoneIn(zones).findAll());

        List<CroppingPlanSpecies> croppingPlanSpecies = croppingPlanEntries.stream()
                .flatMap(croppingPlanEntry -> CollectionUtils.emptyIfNull(croppingPlanEntry.getCroppingPlanSpecies()).stream())
                .collect(Collectors.toList());
        List<Measure> measures = measureDao.forCroppingPlanSpeciesIn(croppingPlanSpecies).findAll();
        List<Observation> observations = observationDao.forCroppingPlanSpeciesIn(croppingPlanSpecies).findAll();
        List<EffectiveSpeciesStade> effectiveSpeciesStades = effectiveSpeciesStadeDao.forCroppingPlanSpeciesIn(croppingPlanSpecies).findAll();

        // Domain -> Performance
        List<Performance> performances = performanceDao.forDomainsContains(domain).findAll();
        List<PerformanceFile> performanceFiles = performanceFileTopiaDao.forPerformanceIn(performances).findAll();

        // Domain -> Price
        Set<HarvestingPrice> prices = new HashSet<>();
        prices.addAll(harvestingPriceTopiaDao.forDomainEquals(domain).findAll());
        prices.addAll(harvestingPriceTopiaDao.forPracticedSystemIn(practicedSystems).findAll());
        prices.addAll(harvestingPriceTopiaDao.forHarvestingActionValorisationIn(harvestingActionValorisations).findAll());

        // Domain -> LivestockUnit
        Iterable<LivestockUnit> livestockUnits = livestockUnitDao.forDomainEquals(domain).findAll();
    
        // Domain -> Ground
        Iterable<Ground> grounds = groundDao.forDomainEquals(domain).findAll();
    
        // Domain -> GeoPoint
        Iterable<GeoPoint> geoPoints = geoPointDao.forDomainEquals(domain).findAll();
    
        // Domain -> ToolsCoupling
        Iterable<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(domain).findAll();
    
        // Domain -> Equipment
        Iterable<Equipment> equipments = equipmentDao.forDomainEquals(domain).findAll();

        // pour les cas ou l'on cherche à exécuter un traitement uniquement si'il n'y a qu'un seul domain avec le code.
        Collection<String> domainCodes = new HashSet<>();
        if (domainDao.forCodeEquals(domainCode).count() == 1) {
            domainCodes.add(domainCode);
        }
        Collection<String> growingPlanCodes = growingPlans.stream()
                .map(GrowingPlan::getCode)
                .filter(growingPlanCode -> growingPlanDao.forCodeEquals(growingPlanCode).count() == 1)
                .collect(Collectors.toSet());
        Collection<String> growingSystemCodes = growingSystems.stream()
                .map(GrowingSystem::getCode)
                .filter(growingSystemCode -> growingSystemDao.forCodeEquals(growingSystemCode).count() == 1)
                .collect(Collectors.toSet());
        // user role
        userRoleDao.deleteAllUserRolesForDomains(domainCodes, growingPlanCodes, growingSystemCodes, growingSystemIds);
    
        // computed user permission
        computedUserPermissionDao.markAsDirtyComputedUserPermissionsForDomains(Collections.singleton(domainCode), Collections.singleton(domainId),
                growingPlanCodes, growingPlanIds, growingSystemCodes, growingSystemIds);
    
        // Domain -> * -> EffectiveIntervention -> Actions -> Inputs
        reportGrowingSystemDao.deleteAll(reportGrowingSystems);

        List<EffectiveInvolvedRule> effectiveInvolvedRules = effectiveInvolvedRulesDao.forDecisionRuleIn(decisionRules).findAll();
        effectiveInvolvedRulesDao.deleteAll(effectiveInvolvedRules);

        decisionRules.forEach(
                dr -> {
                    List<Strategy> strategies = strategyDao.forRulesContains(dr).findAll();
                    strategies.forEach(st -> st.removeRules(dr));
                }
        );

        decisionRuleDao.deleteAll(decisionRules);

        Set<DecisionRuleCrop> decisionRuleCrops = decisionRules.stream()
                .flatMap(decisionRule -> decisionRule.getDecisionRuleCrop().stream())
                .collect(Collectors.toSet());
        decisionRuleCropDao.deleteAll(decisionRuleCrops);

        managementModeDao.deleteAll(managementModes);
    
        harvestingPriceTopiaDao.deleteAll(prices);
        // inputs supprimés via le service
    
        effectiveSpeciesStadeDao.deleteAll(effectiveSpeciesStades);
    
        practicedInterventionDao.deleteAll(practicedInterventions);
        practicedCropCycleConnectionDao.deleteAll(practicedCropCycleConnections);
        practicedCropCycleDao.deleteAll(practicedCropCycles);
        abstractActionDao.deleteAll(actions);
    
        effectiveInterventionDao.deleteAll(effectiveInterventions);
        effectivePerennialCropCycleDao.deleteAll(effectivePerennialCropCycles);
        effectiveCropCycleConnectionDao.deleteAll(effectiveCropCycleConnections);
        effectiveSeasonalCropCycleDao.deleteAll(effectiveSeasonalCropCycles);
        effectiveCropCycleNodeDao.deleteAll(effectiveCropCycleNodes);
    
        practicedPlotDao.deleteAll(practicedPlots);
        practicedSystemDao.deleteAll(practicedSystems);
    
        growingSystemDao.deleteAll(growingSystems);
        growingPlanDao.deleteAll(growingPlans);

        domainInputStockUnitService.deleteDomainInputStockUnit(domain);

        croppingPlanEntries.forEach(CroppingPlanEntry::clearCroppingPlanSpecies);
        croppingPlanSpeciesDao.deleteAll(croppingPlanSpecies);
        measureDao.deleteAll(measures);
        observationDao.deleteAll(observations);
        measurementSessionDao.deleteAll(measurementSessions);
        croppingPlanEntryDao.deleteAll(croppingPlanEntries);
        zoneDao.deleteAll(zones);
        plotDao.deleteAll(plots);
    
        livestockUnitDao.deleteAll(livestockUnits);
        groundDao.deleteAll(grounds);
        geoPointDao.deleteAll(geoPoints);
        toolsCouplingDao.deleteAll(toolsCouplings);
        equipmentDao.deleteAll(equipments);
        performanceFileTopiaDao.deleteAll(performanceFiles);
        performanceDao.deleteAll(performances);
    

        domainDao.delete(domain);
    
        getTransaction().commit();
    }
    
    @Override
    public Domain getDomainForCampaign(String code, Integer campaign) {
        return domainDao.forCodeEquals(code).addEquals(Domain.PROPERTY_CAMPAIGN, campaign).findUniqueOrNull();
    }
    
    @Override
    public List<Integer> getCampaignsForDomain(Domain domain) {
        return domainDao.findCampaignForDomain(domain.getCode());
    }

    @Override
    public Map<String, String> getCountries() {
        return refCountryTopiaDao.getCountryNamesByTopiaId(getSecurityContext().getLanguage());
    }

    @Override
    public RefCountry getFranceRefCountry() {
        String trigram = Locale.FRANCE.getISO3Country().toLowerCase();
        RefCountry result = refCountryTopiaDao.forTrigramEquals(trigram).findUnique();
        return result;
    }

    @Override
    public String getDefaultCountryTopiaId() {
        String trigram = getSecurityContext().getLocale().getISO3Country().toLowerCase();
        Optional<RefCountry> maybeCountry = refCountryTopiaDao.forTrigramEquals(trigram).stream().findFirst();
        if (maybeCountry.isEmpty()) {
            return getFranceRefCountry().getTopiaId();
        } else {
            return maybeCountry.get().getTopiaId();
        }
    }

    @Override
    public Domain setPracticedPlotsForDomain(Domain domain, Collection<PracticedPlot> newPracticedPlots) {

        Collection<PracticedPlot> practicedPlots = TopiaUtils.updateCollection(
                BinderFactory.newBinder(PracticedPlot.class),
                domain.getPracticedPlot(),
                newPracticedPlots,
                PracticedPlot.PROPERTY_SOL_HORIZON
        );
        domain.clearPracticedPlot();
        domain.addAllPracticedPlot(practicedPlots);
        return domain;
    }

    @Override
    public RefCountry getCountry() {
        String trigram = getSecurityContext().getLocale().getISO3Country().toLowerCase();
        Optional<RefCountry> maybeCountry = refCountryTopiaDao.forTrigramEquals(trigram).stream().findFirst();
        return maybeCountry.orElseGet(this::getFranceRefCountry);
    }
}
