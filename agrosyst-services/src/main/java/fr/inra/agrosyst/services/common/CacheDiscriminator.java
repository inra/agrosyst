package fr.inra.agrosyst.services.common;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;

public record CacheDiscriminator(String discriminatorAsString) {

    private CacheDiscriminator(String discriminatorPrefix, Language language) {
        this(String.join("__", discriminatorPrefix, language.getTrigram()));
    }

    public static CacheDiscriminator hasRole() {
        return new CacheDiscriminator("hasRole");
    }

    public static CacheDiscriminator networksHierarchy() {
        return new CacheDiscriminator("networksHierarchyCache");
    }

    public static CacheDiscriminator activeTypeMateriel1WithoutManualTools(Language language, boolean filterEdaplos) {
        return new CacheDiscriminator("activeTypeMateriel1WithoutManualTools" + filterEdaplos, language);
    }

    public static CacheDiscriminator activeTypeMateriel1WithManualTools(Language language) {
        return new CacheDiscriminator("activeTypeMateriel1WithManualTools" + language.getTrigram());
    }

    public static CacheDiscriminator activeOtex18Code(Language language) {
        return new CacheDiscriminator("activeOtex18Code__" + language.getTrigram());
    }

    public static CacheDiscriminator activeRefOrientationEDI(Language language) {
        return new CacheDiscriminator("activeRefOrientationEDI__" + language.getTrigram());
    }

    public static CacheDiscriminator activeRefParcelleZonageEDI() {
        return new CacheDiscriminator("activeRefParcelleZonageEDI");
    }

    public static CacheDiscriminator activeRefSolTextureGeppa() {
        return new CacheDiscriminator("activeRefSolTextureGeppa");
    }

    public static CacheDiscriminator activeNotFrenchRefSolTextureGeppa(Language language) {
        return new CacheDiscriminator("activeNotFrenchRefSolTextureGeppa", language);
    }

    public static CacheDiscriminator activeRefSolProfondeurIndigo(Language language) {
        return new CacheDiscriminator("activeRefSolProfondeurIndigo", language);
    }

    public static CacheDiscriminator activeRefSolCaracteristiqueIndigo() {
        return new CacheDiscriminator("activeRefSolCaracteristiqueIndigo");
    }

    public static CacheDiscriminator activeMineralProductType(Language language) {
        return new CacheDiscriminator("activeMineralProductType", language);
    }

    public static CacheDiscriminator activeRefFertiOrga(Language language) {
        return new CacheDiscriminator("activeRefFertiOrga", language);
    }

    public static CacheDiscriminator inactiveRefFertiOrga(Language language) {
        return new CacheDiscriminator("inactiveRefFertiOrga", language);
    }

    public static CacheDiscriminator activeActaTreatementCodesAndNames() {
        return new CacheDiscriminator("activeActaTreatementCodesAndNames");
    }

    public static CacheDiscriminator activeActaTreatmentProductTypes() {
        return new CacheDiscriminator("activeActaTreatmentProductTypes");
    }

    public static CacheDiscriminator activeBioAgressorTreatmentTarget(Language language) {
        return new CacheDiscriminator("activeBioAgressorTreatmentTarget", language);
    }

    public static CacheDiscriminator activeAgrosystActions(Language language) {
        return new CacheDiscriminator("activeAgrosystActions", language);
    }

    public static CacheDiscriminator activeNuisibleEDITreatmentTarget(Language language) {
        return new CacheDiscriminator("activeNuisibleEDITreatmentTarget", language);
    }

    public static CacheDiscriminator activeRefActaTraitementsProduit() {
        return new CacheDiscriminator("activeRefActaTraitementsProduit");
    }

    public static CacheDiscriminator networksProjection() {
        return new CacheDiscriminator("networksProjection");
    }

    public static CacheDiscriminator networksReportProjection() {
        return new CacheDiscriminator("networksReportProjection");
    }

    public static CacheDiscriminator domainsToDomainCodesProjection() {
        return new CacheDiscriminator("domainsToDomainCodesProjection");
    }

    public static CacheDiscriminator growingPlansToDomainCodes() {
        return new CacheDiscriminator("growingPlansToDomainCodes");
    }

    public static CacheDiscriminator growingSystemsToReportRegional() {
        return new CacheDiscriminator("growingSystemsToReportRegional");
    }

    public static CacheDiscriminator activeAllRefStationMeteo() {
        return new CacheDiscriminator("activeAllRefStationMeteo");
    }

    public static CacheDiscriminator activeGroupesCibles(Language language) {
        return new CacheDiscriminator("activeGroupesCibles", language);
    }

    public static CacheDiscriminator growingPlansToGrowingSystemsCode() {
        return new CacheDiscriminator("growingPlansToGrowingSystemsCode");
    }

    public static CacheDiscriminator growingPlanToReportRegional() {
        return new CacheDiscriminator("growingPlanToReportRegional");
    }

    public static CacheDiscriminator domainToGrowingPlanCodes() {
        return new CacheDiscriminator("domainToGrowingPlanCodes");
    }

    public static CacheDiscriminator domainToGrowingSystemCodes() {
        return new CacheDiscriminator("domainToGrowingSystemCodes");
    }

    public static CacheDiscriminator growingSystemCodesToReportRegional() {
        return new CacheDiscriminator("growingSystemCodesToReportRegional");
    }

    public static CacheDiscriminator computeReferenceDoseForIFTLegacy() {
        return new CacheDiscriminator("computeReferenceDoseForIFTLegacy");
    }

    public static CacheDiscriminator computeReferenceDoseForIFTcibleNonMillesime() {
        return new CacheDiscriminator("computeReferenceDoseForIFTcibleNonMillesime");
    }

    public static CacheDiscriminator computeWritePerformance() {
        return new CacheDiscriminator("writePerformance");
    }

    public static CacheDiscriminator activeDomainPhytoProductInputSearchResults() {
        return new CacheDiscriminator("computeActiveDomainPhytoProductInputSearchResults");
    }
}
