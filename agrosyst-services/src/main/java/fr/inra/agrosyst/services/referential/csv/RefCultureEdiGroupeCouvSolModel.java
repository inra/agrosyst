package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSolImpl;
import fr.inra.agrosyst.api.entities.referential.TypeCulture;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

/**
 * code espèce botanique;libellé espèce botanique;code qualifiant AEE;libellé qualifiant AEE;code type saisonnier AEE;libellé type saisonnier AEE;code destination AEE;
 * libellé destination AEE;Type_Culture;vitesse couv;Taux de couverture max
 * 
 * @author Eric Chatellier
 *
 */
public class RefCultureEdiGroupeCouvSolModel extends AbstractAgrosystModel<RefCultureEdiGroupeCouvSol> implements ExportModel<RefCultureEdiGroupeCouvSol> {

    protected static final ValueParser<TypeCulture> TYPE_CULTURE_PARSER = value -> {
        TypeCulture result = TypeCulture.valueOf(value.replace('-', '_'));
        return result;
    };

    protected static final ValueFormatter<TypeCulture> TYPE_CULTURE_FORMATTER = Enum::name;

    public RefCultureEdiGroupeCouvSolModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("code espèce botanique", RefCultureEdiGroupeCouvSol.PROPERTY_CODE_ESPECE_BOTANIQUE);
        newMandatoryColumn("libellé espèce botanique", RefCultureEdiGroupeCouvSol.PROPERTY_LIBELLE_ESPECE_BOTANIQUE);
        newMandatoryColumn("code qualifiant AEE", RefCultureEdiGroupeCouvSol.PROPERTY_CODE_QUALIFIANT_AEE);
        newMandatoryColumn("libellé qualifiant AEE", RefCultureEdiGroupeCouvSol.PROPERTY_LIBELLE_QUALIFIANT_AEE);
        newMandatoryColumn("code type saisonnier AEE", RefCultureEdiGroupeCouvSol.PROPERTY_CODE_TYPE_SAISONNIER_AEE);
        newMandatoryColumn("libellé type saisonnier AEE", RefCultureEdiGroupeCouvSol.PROPERTY_LIBELLE_TYPE_SAISONNIER_AEE);
        newMandatoryColumn("code destination AEE", RefCultureEdiGroupeCouvSol.PROPERTY_CODE_DESTINATION_AEE);
        newMandatoryColumn("libellé destination AEE", RefCultureEdiGroupeCouvSol.PROPERTY_LIBELLE_DESTINATION_AEE);
        newMandatoryColumn("Type_Culture", RefCultureEdiGroupeCouvSol.PROPERTY_TYPE_CULTURE, TYPE_CULTURE_PARSER);
        newMandatoryColumn("vitesse couv", RefCultureEdiGroupeCouvSol.PROPERTY_VITESSE_COUV, VITESSE_COUV_PARSER);
        newMandatoryColumn("Taux de couverture max", RefCultureEdiGroupeCouvSol.PROPERTY_TAUX_COUVERTURE_MAX, PERCENT_DOUBLE_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefCultureEdiGroupeCouvSol.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefCultureEdiGroupeCouvSol, Object>> getColumnsForExport() {
        ModelBuilder<RefCultureEdiGroupeCouvSol> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("code espèce botanique", RefCultureEdiGroupeCouvSol.PROPERTY_CODE_ESPECE_BOTANIQUE);
        modelBuilder.newColumnForExport("libellé espèce botanique", RefCultureEdiGroupeCouvSol.PROPERTY_LIBELLE_ESPECE_BOTANIQUE);
        modelBuilder.newColumnForExport("code qualifiant AEE", RefCultureEdiGroupeCouvSol.PROPERTY_CODE_QUALIFIANT_AEE);
        modelBuilder.newColumnForExport("libellé qualifiant AEE", RefCultureEdiGroupeCouvSol.PROPERTY_LIBELLE_QUALIFIANT_AEE);
        modelBuilder.newColumnForExport("code type saisonnier AEE", RefCultureEdiGroupeCouvSol.PROPERTY_CODE_TYPE_SAISONNIER_AEE);
        modelBuilder.newColumnForExport("libellé type saisonnier AEE", RefCultureEdiGroupeCouvSol.PROPERTY_LIBELLE_TYPE_SAISONNIER_AEE);
        modelBuilder.newColumnForExport("code destination AEE", RefCultureEdiGroupeCouvSol.PROPERTY_CODE_DESTINATION_AEE);
        modelBuilder.newColumnForExport("libellé destination AEE", RefCultureEdiGroupeCouvSol.PROPERTY_LIBELLE_DESTINATION_AEE);
        modelBuilder.newColumnForExport("Type_Culture", RefCultureEdiGroupeCouvSol.PROPERTY_TYPE_CULTURE, TYPE_CULTURE_FORMATTER);
        modelBuilder.newColumnForExport("vitesse couv", RefCultureEdiGroupeCouvSol.PROPERTY_VITESSE_COUV, VITESSE_COUV_FORMATTER);
        modelBuilder.newColumnForExport("Taux de couverture max", RefCultureEdiGroupeCouvSol.PROPERTY_TAUX_COUVERTURE_MAX, PERCENT_DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefCultureEdiGroupeCouvSol.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefCultureEdiGroupeCouvSol newEmptyInstance() {
        RefCultureEdiGroupeCouvSol result = new RefCultureEdiGroupeCouvSolImpl();
        result.setActive(true);
        return result;
    }
}
