package fr.inra.agrosyst.services.network;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.NetworkImpl;

import java.io.Serial;

public class NetworkImportDto extends NetworkImpl {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = 8435193675746925772L;

    public static final String PROPERTY_RESPONSIBLE = "responsible";
    public static final String PROPERTY_PARENT_NETWORKS = "parentNetworks";

    protected String responsible;

    protected String parentNetworks;

    public String getResponsible() {
        return responsible;
    }

    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }

    public String getParentNetworks() {
        return parentNetworks;
    }

    public void setParentNetworks(String parentNetworks) {
        this.parentNetworks = parentNetworks;
    }
}
