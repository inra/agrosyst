package fr.inra.agrosyst.services.referential.json;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitImpl;
import fr.inra.agrosyst.api.services.referential.ImportStatus;
import org.apache.commons.lang3.StringUtils;

import java.io.Serial;

/**
 * Created by davidcosse on 11/01/16.
 */
public class RefApiActaTraitementsProduit extends RefActaTraitementsProduitImpl implements ApiImportResults {
    @Serial
    private static final long serialVersionUID = -4320167193449167087L;

    public static final String SOURCE_API = "API";
    public static final String PROPERTY_STATUS = "status";

    protected String idProduit;

    protected String nomProduit;

    protected Integer idTraitement;

    protected String codeTraitement;

    protected String nomTraitement;
    
    protected String code_AMM;

    public void setIdProduit(String idProduit) {
        this.idProduit = idProduit;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public void setIdTraitement(Integer idTraitement) {
        this.idTraitement = idTraitement;
    }

    public void setCodeTraitement(String codeTraitement) {
        this.codeTraitement = codeTraitement;
    }

    public void setNomTraitement(String nomTraitement) {
        this.nomTraitement = nomTraitement;
    }

    @Override
    public void setCode_AMM(String code_AMM) {
        this.code_AMM = code_AMM;
    }

    @Override
    public String getId_produit() {
        String result = StringUtils.isNoneBlank(idProduit) ? idProduit : super.id_produit;
        return result;
    }

    @Override
    public String getNom_produit() {
        String result = StringUtils.isNoneBlank(nomProduit) ? nomProduit : super.nom_produit;
        return result;
    }

    @Override
    public int getId_traitement() {
        int result = idTraitement == null ? super.id_traitement : idTraitement;
        return result;
    }

    @Override
    public String getCode_traitement() {
        String result = StringUtils.isNoneBlank(codeTraitement) ? codeTraitement : super.code_traitement;
        return result;
    }

    @Override
    public String getNom_traitement() {
        String result = StringUtils.isNoneBlank(nomTraitement) ? nomTraitement : super.nom_traitement;
        return result;
    }

    @Override
    public String getCode_AMM() {
        String result = StringUtils.isNoneBlank(code_AMM) ? code_AMM : super.code_AMM;
        return result;
    }

    @Override
    public String getSource() {
        return RefApiActaSubstanceActive.SOURCE_API;
    }

    public ImportStatus getStatus() {
        return status;
    }

    public void setStatus(ImportStatus status) {
        this.status = status;
    }
}
