package fr.inra.agrosyst.services.performance.indicators;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.services.performance.MissingFieldMessage;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.performancehelper.MissingFieldMessageBuilder;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class IndicatorFieldsStatistics {
    
    protected static final String RELIABILITY_INDEX_NO_COMMENT = "";
    public static final int NB_MONTH = 12;
    
    public final Map<String, AtomicInteger> totalFieldCounter = new HashMap<>();
    protected final Map<String, AtomicInteger> missingFieldCounter = new HashMap<>();
    
    // comments are at intervention level only
    public final Map<String, Set<MissingFieldMessage>> targetedErrorFieldMessages = new HashMap<>();

    @Getter
    protected Locale locale;

    protected final MissingFieldMessageBuilder messageBuilder;

    public IndicatorFieldsStatistics() {
        messageBuilder = new MissingFieldMessageBuilder();
    }

    public IndicatorFieldsStatistics(Locale locale) {
        messageBuilder = new MissingFieldMessageBuilder(locale);
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
        this.messageBuilder.setLocale(locale);
    }

    /**
     * Pour le réalisé PSCi est égal à
     * <pre>PSCi = nombre de passage * fréquence spatial d'intervention</pre>
     *
     * @param intervention intervention
     * @return effective PSCi
     */
    protected double getToolPSCi(EffectiveIntervention intervention) {

        // TransitCount()
        incrementAngGetTotalFieldCounterForTargetedId(intervention.getTopiaId());
        // SpatialFrequency()
        incrementAngGetTotalFieldCounterForTargetedId(intervention.getTopiaId());

        return intervention.getTransitCount() * intervention.getSpatialFrequency();
    }

    /**
     * Pour le synthétisé PSCi est égal à :
     * <pre>PSCi = fréquence temporelle d'intervention * fréquence spatial d'intervention</pre>
     *
     * @param intervention intervention
     * @return practiced PSCi
     */
    protected double getToolPSCi(PracticedIntervention intervention) {
        // TemporalFrequency
        incrementAngGetTotalFieldCounterForTargetedId(intervention.getTopiaId());
        // SpatialFrequency
        incrementAngGetTotalFieldCounterForTargetedId(intervention.getTopiaId());

        return intervention.getTemporalFrequency() * intervention.getSpatialFrequency();
    }

    protected void addMissingDeprecatedInputQtyOrUnitIfRequired(
            AbstractInputUsage inputUsage) {

        if (StringUtils.isNotBlank(inputUsage.getDeprecatedId())) {
            if (inputUsage.getDeprecatedQtAvg() == null) {
                addMissingFieldMessage(
                        inputUsage.getTopiaId(),
                        messageBuilder.getMissingDeprecateDoseMessage(inputUsage.getInputType(), MissingMessageScope.INPUT));
            }
            if (inputUsage.getDeprecatedUnit() == null) {
                addMissingFieldMessage(
                        inputUsage.getTopiaId(),
                        messageBuilder.getMissingDeprecatedDoseUnitMessage(inputUsage.getInputType(), MissingMessageScope.INPUT));
            }
        }
    }

    protected void addMissingFieldMessage(String targetedId, MissingFieldMessage message) {
        AtomicInteger counter = missingFieldCounter.computeIfAbsent(targetedId, k -> new AtomicInteger());
        counter.incrementAndGet();
    
        Set<MissingFieldMessage> errorFields = targetedErrorFieldMessages.computeIfAbsent(
                targetedId, k -> Sets.newHashSet());
        errorFields.add(message);
    }

    protected void addMissingIndicatorMessage(String targetedId, MissingFieldMessage message) {
        Set<MissingFieldMessage> errorFields = targetedErrorFieldMessages.computeIfAbsent(
                targetedId, k -> Sets.newHashSet());
        errorFields.add(message);
    }

    protected void addMissingInputUnitPriceUnitConverter(
            String inputId, InputType inputType, Enum doseUnit, PriceUnit priceUnit) {
        
        // counter is not incremented as it is not a completion error
        Set<MissingFieldMessage> errorFields = targetedErrorFieldMessages.computeIfAbsent(inputId, k -> Sets.newHashSet());
        MissingFieldMessage message = messageBuilder.getMissingInputUnitPriceUnitConverterMessage(
                inputType, doseUnit, priceUnit);
        
        errorFields.add(message);
    }
    
    protected void addMissingRefAnnualWorkAmountMessage(String interventionId) {
        // counter is not incremented as it is not a completion error
        Set<MissingFieldMessage> errorFields = targetedErrorFieldMessages.computeIfAbsent(interventionId, k -> Sets.newHashSet());
        MissingFieldMessage message = messageBuilder.getMissingRefAnnualWorkAmountMessage();
        
        errorFields.add(message);
    }
    
    protected void addMissingRefStandardizedPriceMessage(String inputId) {
        // counter is not incremented as it is not a completion error
        Set<MissingFieldMessage> errorFields = targetedErrorFieldMessages.computeIfAbsent(inputId, k -> Sets.newHashSet());
        MissingFieldMessage message = messageBuilder.getMissingRefStandardizedPriceMessage();
        
        errorFields.add(message);
    }
    
    protected void addMissingScenarioPriceMessage(String inputId, String scenarioCode) {
        // counter is not incremented as it is not a completion error
        Set<MissingFieldMessage> errorFields = targetedErrorFieldMessages.computeIfAbsent(inputId, k -> Sets.newHashSet());
        MissingFieldMessage message = messageBuilder.getMissingRefScenarioPriceMessage(scenarioCode);
        
        errorFields.add(message);
    }
    
    protected void addMissingRefMaterielPowerMessage(String interventionId) {
        // counter is not incremented as it is not a completion error
        Set<MissingFieldMessage> errorFields = targetedErrorFieldMessages.computeIfAbsent(interventionId, k -> Sets.newHashSet());
        MissingFieldMessage message = messageBuilder.getMissingRefMaterielPowerMessage();
        
        errorFields.add(message);
    }
    
    protected void addMissingRefConversionRateMessageMessage(String interventionId) {
        // counter is not incremented as it is not a completion error
        Set<MissingFieldMessage> errorFields = targetedErrorFieldMessages.computeIfAbsent(interventionId, k -> Sets.newHashSet());
        MissingFieldMessage message = messageBuilder.getMissingRefConversionRateMessage();
        
        errorFields.add(message);
    }
    
    protected void addMissingRefDosageMessageMessage(String inputId) {
        // counter is not incremented as it is not a completion error
        Set<MissingFieldMessage> errorFields = targetedErrorFieldMessages.computeIfAbsent(inputId, k -> Sets.newHashSet());
        MissingFieldMessage message = messageBuilder.getMissingRefDosageMessage();
        
        errorFields.add(message);
    }
    
    protected String getReliabilityCommentForTargetedId(String targetedId, MissingMessageScope scope) {
    
        Set<MissingFieldMessage> targetedFieldErrors = targetedErrorFieldMessages.get(targetedId);
        Set<MissingFieldMessage> messagesForScope =
                targetedFieldErrors == null ?
                        new HashSet<>() :
                        targetedFieldErrors.stream()
                                .filter(
                                        missingFieldMessage ->
                                                missingFieldMessage.getMessageForScope(scope)
                                                        .isPresent())
                                .collect(Collectors.toSet());
        String comment = messagesForScope.isEmpty() ? RELIABILITY_INDEX_NO_COMMENT :
                messagesForScope.stream().map(MissingFieldMessage::getMessage).collect(Collectors.joining(", "));
        return comment;
    
    }
    
    protected int getReliabilityIndexForTargetedId(String targetedId) {
        
        int missingFieldCounterValue = getMissingFieldCounterValueForTargetedId(targetedId);
        int totalFieldCounterValue = getTotalFieldCounterValueForTargetedId(targetedId);
        
        int result = computeReliabilityIndex(
                missingFieldCounterValue,
                totalFieldCounterValue);
        
        return result;
    }
    
    protected int getMissingFieldCounterValueForTargetedId(String targetedId) {
        AtomicInteger missingFieldCounter = this.missingFieldCounter.get(targetedId);
        return missingFieldCounter != null ? missingFieldCounter.get() : 0;
    }
    
    protected int getTotalFieldCounterValueForTargetedId(String targetedId) {
        AtomicInteger totalFieldCounter = this.totalFieldCounter.get(targetedId);
        return totalFieldCounter != null ? totalFieldCounter.get() : 0;
    }
    
    protected int computeReliabilityIndex(Integer missingFieldCounter, Integer totalFieldCounter) {
        int result = 0;
        if (totalFieldCounter != null && totalFieldCounter != 0) {
            missingFieldCounter = missingFieldCounter == null ? 0 : missingFieldCounter;
            result = (totalFieldCounter - missingFieldCounter) == 0 ? 0 :
                    (int) Math.floor(((totalFieldCounter - missingFieldCounter) * 100.0) / (totalFieldCounter));
        }
        return result;
    }
    
    protected void incrementAngGetTotalFieldCounterForTargetedId(String targetId) {
        AtomicInteger ai = totalFieldCounter.computeIfAbsent(targetId, k -> new AtomicInteger());
        ai.incrementAndGet();
    }
    
    protected String getMessagesForScope(MissingMessageScope scope) {
        String comments = RELIABILITY_INDEX_NO_COMMENT;
        Set<MissingFieldMessage> messages =
                targetedErrorFieldMessages.values()
                        .stream().flatMap(
                        Collection::stream)
                        .filter(missingFieldMessage ->
                                missingFieldMessage.getMessageForScope(scope).isPresent())
                        .collect(Collectors.toSet());
    
        if (CollectionUtils.isNotEmpty(messages)) {
            Set<String> errors = messages.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet());
            comments = String.join(",", errors);
        }
        return comments;
    }
}
