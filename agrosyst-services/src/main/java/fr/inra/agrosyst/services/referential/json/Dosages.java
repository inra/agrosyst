package fr.inra.agrosyst.services.referential.json;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2016 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

/**
 * Created by davidcosse on 06/01/16.
 */
public class Dosages {
    protected List<RefActaDosageSaRootExport> dosagesSA;
    protected List<RefActaDosageSpcRootExport> dosagesSPC;
    protected List<RefActaProduitRootExport> produits;
    protected List<RefApiActaSubstanceActive> substancesActives;
    protected List<RefApiActaTraitementsProduit> traitements;

    public List<RefActaDosageSaRootExport> getDosagesSA() {
        return dosagesSA;
    }

    public void setDosagesSA(List<RefActaDosageSaRootExport> dosagesSA) {
        this.dosagesSA = dosagesSA;
    }

    public List<RefActaDosageSpcRootExport> getDosagesSPC() {
        return dosagesSPC;
    }

    public void setDosagesSPC(List<RefActaDosageSpcRootExport> dosagesSPC) {
        this.dosagesSPC = dosagesSPC;
    }

    public List<RefActaProduitRootExport> getProduits() {
        return produits;
    }

    public void setProduits(List<RefActaProduitRootExport> produits) {
        this.produits = produits;
    }

    public List<RefApiActaSubstanceActive> getSubstancesActives() {
        return substancesActives;
    }

    public void setSubstancesActives(List<RefApiActaSubstanceActive> substancesActives) {
        this.substancesActives = substancesActives;
    }

    public List<RefApiActaTraitementsProduit> getTraitements() {
        return traitements;
    }

    public void setTraitements(List<RefApiActaTraitementsProduit> traitements) {
        this.traitements = traitements;
    }
}
