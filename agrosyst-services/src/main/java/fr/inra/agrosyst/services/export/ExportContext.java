package fr.inra.agrosyst.services.export;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;

import java.util.LinkedList;
import java.util.List;

public class ExportContext {
    protected final List<ExportMetadata.PerennialCropCycleSpeciesBean> perennialCropCycleSpecies;
    protected final List<ExportMetadata.ItkActionApplicationProduitsMinerauxBean> applicationProduitsMineraux;
    protected final List<ExportMetadata.ItkApplicationProduitsPhytoBean> applicationProduitsPhytos;
    protected final List<ExportMetadata.ItkActionAutreBean> actionAutres;
    protected final List<ExportMetadata.ItkActionEntretienTailleVigneBean> actionEntretienTailleVignes;
    protected final List<ExportMetadata.ItkActionEpandageOrganiqueBean> actionEpandageOrganiques;
    protected final List<ExportMetadata.ItkActionIrrigationBean> actionIrrigations;
    protected final List<ExportMetadata.ItkActionLutteBiologiqueBean> actionLutteBiologiques;
    protected final List<ExportMetadata.ItkActionRecolteBean> actionRecoltes;
    protected final List<ExportMetadata.ItkActionSemiBean> actionSemis;
    protected final List<ExportMetadata.ItkActionTransportBean> actionTransports;
    protected final List<ExportMetadata.ItkActionTravailSolBean> actionTravailSolBeans;

    public ExportContext() {

        this.perennialCropCycleSpecies = new LinkedList<>();
        this.applicationProduitsMineraux = new LinkedList<>();
        this.applicationProduitsPhytos = new LinkedList<>();
        this.actionAutres = new LinkedList<>();
        this.actionEntretienTailleVignes = new LinkedList<>();
        this.actionEpandageOrganiques = new LinkedList<>();
        this.actionIrrigations = new LinkedList<>();
        this.actionLutteBiologiques = new LinkedList<>();
        this.actionRecoltes = new LinkedList<>();
        this.actionSemis = new LinkedList<>();
        this.actionTransports = new LinkedList<>();
        this.actionTravailSolBeans = new LinkedList<>();

    }

    public void addPerennialSpecie(ExportMetadata.PerennialCropCycleSpeciesBean input) {
        this.perennialCropCycleSpecies.add(input);
    }

    public List<ExportMetadata.PerennialCropCycleSpeciesBean> getPerennialCropCycleSpecies() {
        return ImmutableList.copyOf(perennialCropCycleSpecies);
    }

    public void addApplicationProduitsMineraux(ExportMetadata.ItkActionApplicationProduitsMinerauxBean input) {
        this.applicationProduitsMineraux.add(input);
    }

    public List<ExportMetadata.ItkActionApplicationProduitsMinerauxBean> getApplicationProduitsMineraux() {
        return ImmutableList.copyOf(applicationProduitsMineraux);
    }

    public void addApplicationProduitsPhyto(ExportMetadata.ItkApplicationProduitsPhytoBean input) {
        this.applicationProduitsPhytos.add(input);
    }

    public List<ExportMetadata.ItkApplicationProduitsPhytoBean> getApplicationProduitsPhytos() {
        return ImmutableList.copyOf(applicationProduitsPhytos);
    }

    public void addActionAutre(ExportMetadata.ItkActionAutreBean input) {
        this.actionAutres.add(input);
    }

    public ImmutableList<ExportMetadata.ItkActionAutreBean> getActionAutres() {
        return ImmutableList.copyOf(actionAutres);
    }

    public void addActionEntretienTailleVigne(ExportMetadata.ItkActionEntretienTailleVigneBean input) {
        this.actionEntretienTailleVignes.add(input);
    }

    public ImmutableList<ExportMetadata.ItkActionEntretienTailleVigneBean> getActionEntretienTailleVignes() {
        return ImmutableList.copyOf(actionEntretienTailleVignes);
    }

    public void addActionEpandageOrganique(ExportMetadata.ItkActionEpandageOrganiqueBean input) {
        this.actionEpandageOrganiques.add(input);
    }

    public ImmutableList<ExportMetadata.ItkActionEpandageOrganiqueBean> getActionEpandageOrganiques() {
        return ImmutableList.copyOf(actionEpandageOrganiques);
    }

    public void addActionIrrigation(ExportMetadata.ItkActionIrrigationBean input) {
        this.actionIrrigations.add(input);
    }

    public ImmutableList<ExportMetadata.ItkActionIrrigationBean> getActionIrrigations() {
        return ImmutableList.copyOf(actionIrrigations);
    }

    public void addActionLutteBiologique(ExportMetadata.ItkActionLutteBiologiqueBean input) {
        this.actionLutteBiologiques.add(input);
    }

    public ImmutableList<ExportMetadata.ItkActionLutteBiologiqueBean> getActionLutteBiologiques() {
        return ImmutableList.copyOf(actionLutteBiologiques);
    }

    public void addActionRecolte(ExportMetadata.ItkActionRecolteBean input) {
        this.actionRecoltes.add(input);
    }

    public ImmutableList<ExportMetadata.ItkActionRecolteBean> getActionRecoltes() {
        return ImmutableList.copyOf(actionRecoltes);
    }

    public void addActionSemi(ExportMetadata.ItkActionSemiBean input) {
        this.actionSemis.add(input);
    }

    public ImmutableList<ExportMetadata.ItkActionSemiBean> getActionSemis() {
        return ImmutableList.copyOf(actionSemis);
    }

    public void addActionTransport(ExportMetadata.ItkActionTransportBean input) {
        this.actionTransports.add(input);
    }

    public ImmutableList<ExportMetadata.ItkActionTransportBean> getActionTransports() {
        return ImmutableList.copyOf(actionTransports);
    }

    public void addActionTravailSol(ExportMetadata.ItkActionTravailSolBean input) {
        this.actionTravailSolBeans.add(input);
    }

    public List<ExportMetadata.ItkActionTravailSolBean> getActionTravailSolBeans() {
        return ImmutableList.copyOf(actionTravailSolBeans);
    }

}
