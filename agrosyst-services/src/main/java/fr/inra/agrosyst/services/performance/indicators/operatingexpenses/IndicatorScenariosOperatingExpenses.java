package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.IrrigationInputUsage;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherAction;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.PotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.SubstrateInputUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.performance.RefScenariosInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.Scenario;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static fr.inra.agrosyst.services.performance.indicators.operatingexpenses.IndicatorOtherProductInputOperatingExpenses.getActionsByAgrosystInterventionTypes;
import static java.util.Optional.ofNullable;
import static org.nuiton.i18n.I18n.l;

/**
 * Présentation de l’indicateur¶
 * Les charges opérationnelles standardisées sont exprimées en €/ha.
 * <p>
 * Elles correspondent aux dépenses liées à l’achat des intrants
 * (semences et plants, produits fertilisants minéraux et organiques, traitement de semences et plants, irrigation,
 * produits phytosanitaires et produits de lutte biologique).
 * pour les intrants de type 'Autres' contrairement à l’indicateur version réelle,
 * on ne peut pas prendre en compte les intrants car nous n’avons pas de donnée de référence dessus.
 * Il est calculé sur la base des prix des intrants de référence pour les campagnes sur lesquelles portent les pratiques
 * ET sur la base des prix de intrants de référence pour un scénario de prix choisi par l'utilisateur.
 * Les interventions concernées par cet indicateur sont toutes les interventions contenant un intrant
 * et/ou une action de type « Semis » et/ou une action de type « Irrigation ».
 * <p>
 * Comme explicité précédemment, 2 prix de référence peuvent être pris en compte :
 * - Les prix de référence calculés dans l'interface pour les campagnes sur lesquelles portent les pratiques <=> CO standardisées - campagne(s)
 * - Les prix de référence définis pour un scénario donné (scénario qui sera choisi via le menu d'export par l'utilisateur ; ticket à venir),
 * indépendant des prix pour les campagnes sur lesquelles portent les pratiques <=> CO standardisées - scénario xxxxx
 * <p>
 * En pratique: la version de cet indicateur calculé pour les campagnes sur lesquelles portent les pratiques devra être calculée systématiquement
 * dans les exports de performance. Pour la version "scenario",
 * il faudra attendre des futurs développements pour améliorer l'interface d'export afin de permettre la sélection d'un scenario par l'utilisateur.
 * <p>
 * Informations importantes à considérer avant la mise en place de la formule de calcul
 * /!\ Prix saisi en €/ha
 * L’utilisateur peut saisir un prix en €/ha pour les intrants. Dans ce cas-là, on considère que la dose d’application de l’intrant vaut 1.
 * <p>
 * /!\ Cohérence entre les unités des doses d’intrants appliquées et les unités de prix
 * Depuis la mise à jour de la saisie des prix des intrants,
 * l’unité de prix proposée à l’utilisateur est égale à l’unité de prix de référence de l’intrant + les unités compatibles (alors que dans la SPEC
 * d’Ophélie, on gardait une cohérence entre l’unité de la dose appliquée et l’unité de prix).
 * Ce qui signifie que parfois, il va y avoir des problèmes de cohérence entre l’unité de la dose appliquée de l’intrant et l’unité de prix.
 * Ex : appli d’un produit phyto (insecticide K-OBIOL CE 25 PB), sur pommiers ; pas de dose de référence remontée donc pas d’unité de dose imposée;
 * l’utilisateur saisi 3 kg/m².
 * Or, le prix de référence dont on dispose est en euros/L;
 * dans ce cas, impossible de convertir simplement la dose appliquée dans une unité compatible avec l’unité de prix de référence.
 * Dans ces cas-là, dans un premier temps, nous allons aller au plus simple : pour tous les couples d’unités de doses d’application
 * et d’unité de prix de référence n’appartenant pas au référentiel de conversion d’unité d’intrant vers unité de prix , alors on ne calcule pas l’indicateur.
 * Par contre, dans les exports, dans la colonne « Détails des champs requis non renseignés », on affiche le contenu suivant
 * « l’unité de la quantité d’intrant n’est pas compatible avec l’unité xxx [<-champ correspondant à l’unité de prix] du prix de l’intrant;
 * calcul réalisable dans la prochaine version des indicateurs».
 * Pour les unités appartenant à cette table, si un taux de conversion est renseigné,
 * alors multiplier la dose d’application déclarée par l’utilisateur par ce facteur dans la formule ci-dessous.
 * <p>
 * Formule de calcul:
 *
 * <pre>
 * CI réel_i:
 *
 * = PSCi * ( sum(Q_ev * PA_ev) + sum(Q_j * PA_j) + sum(Q_e * PA_e) + PSCi_phyto * sum(Q_k * PA_k) + PSCi_luttebio * sum(Q_h * PA_h)
 *
 * Avec :
 * - CI réel_i : charges intrants de l’intervention i
 * - PSCi (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i.
 *   PSCi est calculé sur la base des données saisies par l’utilisateur.
 *
 *  - Semis :
 *   - Q_ev (diverses unités) : quantité semée du couple EV, EV appartenant à la liste des couples EV semés  dans l’action semis de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *   - PA_ev (diverses unités) : prix d’achat du couple EV pour ce type de semence (de ferme, certifiées ...),
 *     EV appartenant à la liste des couples EV semés au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *      - ATTENTION :  dès lors que la case « traitement inclus ... » est cochée, ne plus réaliser l’étape décrite dans
 *        le paragraphe ci-dessous pour ce qui est du traitement de semence (par contre, réaliser l’opération pour les
 *        intrants de fertilisation). Car il ne faut pas compter cet intrant 2 fois.
 *
 * - Fertilisation minérale et organique  – Traitements de semences
 *   - Q_j (diverses unités) : quantité de l’intrant j,
 *     j appartenant à la liste des intrants de type
 *       « Traitements de semences »,
 *       « Engrais/amendement (organo) minéral » ou
 *       « Engrais/amendement organique » appliqués au cours de l’intervention i.
 *       Donnée saisie par l’utilisateur.
 *   - PA_j (diverses unités) : prix d’achat de l’intrant j,
 *     j appartenant à la liste des intrants de type
 *     « Traitements de semences »,
 *     « Engrais/amendement (organo) minéral » ou
 *     « Engrais/amendement organique » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *
 * - Irrigation
 *   - Q_e (diverses unités) : quantité d’eau déclarée dans l’action de type Irrigation au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *   - PA_e (€/m³) : prix d’achat de l’eau.
 *
 * - Phytosanitaire
 *   - PSCi_phyto (sans unité) : proportion de surface concernée par l’action de type application de produit phytosanitaire.
 *   - Q_k (diverses unités) : quantité de l’intrant k,
 *     k appartenant à la liste des intrants de type
 *     « Phytosanitaire » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *   - PA_k (diverses unités) : prix d’achat de l ’intrant k,
 *     k appartenant à la liste des intrants de type
 *     « Phytosanitaire » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *
 * - Lutte biologique
 *   - PSCi_luttebio (sans unité) : proportion de surface concernée par l’action de type lutte biologique.
 *   - Q_h (diverses unités) : quantité de l’intrant h,
 *     h appartenant à la liste des intrants de type
 *     « Lutte biologique » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *   - PA_h (diverses unités) : prix d’achat de l’intrant h,
 *     h appartenant à la liste des intrants de type
 *     « Lutte biologique » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *
 * </pre>
 *
 * @author David Cossé (cosse@codelutin.com)
 */
public class IndicatorScenariosOperatingExpenses extends AbstractIndicator {

    protected static final Log LOGGER = LogFactory.getLog(IndicatorOperatingExpenses.class);

    protected final Map<String, Scenario> scenariosByCode = new HashMap<>();
    protected final List<String> scenarioCodes = new ArrayList<>();

    public IndicatorScenariosOperatingExpenses() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, "charges_operationnelles_tot_std_scenarios_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, "charges_operationnelles_tot_std_scenarios_detail_champs_non_ren");
    }

    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INPUT,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return true;
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return true;
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_ECONOMIC);
    }

    @Override
    public String getIndicatorLabel(int i) {
        String scenarioCode = scenarioCodes.get(i);
        return l(locale, "Indicator.label.operatingExpensesStandardScenario", scenariosByCode.get(scenarioCode).label());
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        for (int i = 0; i < scenarioCodes.size(); i++) {
            String scenarioCode = scenarioCodes.get(i);
            String codeNumber = scenarioCode.replaceAll("[^0-9]", "");
            if (StringUtils.isBlank(codeNumber)) continue;
            String label = getIndicatorLabel(i);
            String scenarioIndexForDbColumn = StringUtils.leftPad(codeNumber, 2, '0');
            indicatorNameToColumnName.put(label, String.format("charges_operationnelles_tot_std_scenario_%s", scenarioIndexForDbColumn));
        }
        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(
            WriterContext writerContext,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedCropExecutionContext cropContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            PracticedCropCyclePhase phase) {

        Double[] resultForScenario = newArray(this.scenarioCodes.size(), 0.0);// 1 by scenario + 1 by ref price

        if (interventionContext.isFictive()) return resultForScenario;

        // phyto
        resultForScenario = sum(resultForScenario,
                computePhytoProductInputIndicator(
                        writerContext,
                        domainContext,
                        practicedSystemContext,
                        interventionContext));
        // irrig
        resultForScenario = sum(resultForScenario,
                computeIrrigationIndicator(
                        writerContext,
                        domainContext,
                        practicedSystemContext,
                        interventionContext));
        // seeding
        resultForScenario = sum(resultForScenario,
                computeSeedingIndicator(
                        writerContext,
                        domainContext,
                        practicedSystemContext,
                        interventionContext
                ));
        // MineralProductIndicator
        resultForScenario = sum(resultForScenario,
                computeMineralProductIndicator(
                        writerContext,
                        domainContext,
                        practicedSystemContext,
                        interventionContext));
        // organicProductIndicator
        resultForScenario = sum(resultForScenario,
                computeOrganicProductIndicator(
                        writerContext,
                        domainContext,
                        practicedSystemContext,
                        interventionContext));

        // other
        resultForScenario = sum(resultForScenario,
                computeOtherProductIndicator(
                        writerContext,
                        domainContext,
                        practicedSystemContext,
                        interventionContext));

        // substrate
        resultForScenario = sum(resultForScenario,
                computeSubstrateProductIndicator(
                        writerContext,
                        domainContext,
                        practicedSystemContext,
                        interventionContext));
        // pot
        resultForScenario = sum(resultForScenario,
                computePotIndicator(
                        writerContext,
                        domainContext,
                        practicedSystemContext,
                        interventionContext));

        return resultForScenario;
    }

    private Double[] computePotIndicator(
            WriterContext writerContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        final Optional<List<OtherAction>> optionalAction_ = interventionContext.getOptionalOtherActions();
        if (optionalAction_.isEmpty()) {
            return newArray(this.scenarioCodes.size(), 0d);
        }

        Double[] inputsCharges = newArray(this.scenarioCodes.size(), 0d);
        List<OtherAction> otherActions = optionalAction_.get();
        for (OtherAction action : otherActions) {

            Collection<PotInputUsage> inputUsages = action.getPotInputUsages();

            if (CollectionUtils.isEmpty(inputUsages)) {
                return newArray(this.scenarioCodes.size(), 0d);
            }

            PracticedIntervention intervention = interventionContext.getIntervention();

            final double toolPSCi = getToolPSCi(intervention);

            Double[] inputsCharges_ = computeIndicatorForAction(
                    writerContext,
                    action,
                    inputUsages,
                    domainContext.getRefScenariosInputPricesByDomainInput()
            );

            inputsCharges = sum(inputsCharges, mults(inputsCharges_, toolPSCi));

            if (LOGGER.isDebugEnabled()) {
                PracticedSystem practicedSystem = practicedSystemContext.getPracticedSystem();
                LOGGER.debug(
                        String.format(
                                Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_PRACTICED,
                                "computePotUsageIndicator",
                                intervention.getName(),
                                intervention.getTopiaId(),
                                practicedSystem.getName(),
                                practicedSystem.getCampaigns(),
                                (System.currentTimeMillis() - start)));
            }

        }

        return inputsCharges;
    }

    private Double[] computeSubstrateProductIndicator(WriterContext writerContext, PerformancePracticedDomainExecutionContext domainContext, PerformancePracticedSystemExecutionContext practicedSystemContext, PerformancePracticedInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        final Optional<List<OtherAction>> optionalAction_ = interventionContext.getOptionalOtherActions();
        if (optionalAction_.isEmpty()) {
            return newArray(this.scenarioCodes.size(), 0d);
        }

        Double[] inputsCharges = newArray(this.scenarioCodes.size(), 0d);
        List<OtherAction> otherActions = optionalAction_.get();
        for (OtherAction action : otherActions) {

            Collection<SubstrateInputUsage> inputUsages = action.getSubstrateInputUsages();

            if (CollectionUtils.isEmpty(inputUsages)) {
                return newArray(this.scenarioCodes.size(), 0d);
            }

            PracticedIntervention intervention = interventionContext.getIntervention();

            final double toolPSCi = getToolPSCi(intervention);

            Double[] inputsCharges_ = computeIndicatorForAction(
                    writerContext,
                    action,
                    inputUsages,
                    domainContext.getRefScenariosInputPricesByDomainInput()
            );

            inputsCharges = sum(inputsCharges, mults(inputsCharges_, toolPSCi));

            if (LOGGER.isDebugEnabled()) {
                PracticedSystem practicedSystem = practicedSystemContext.getPracticedSystem();
                LOGGER.debug(
                        String.format(
                                Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_PRACTICED,
                                "computeSubstrateProductIndicator",
                                intervention.getName(),
                                intervention.getTopiaId(),
                                practicedSystem.getName(),
                                practicedSystem.getCampaigns(),
                                (System.currentTimeMillis() - start)));
            }

        }

        return inputsCharges;
    }

    private Double[] computeOtherProductIndicator(
            WriterContext writerContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        Map<AgrosystInterventionType, List<AbstractAction>> actionsByAgrosystInterventionTypes = getActionsByAgrosystInterventionTypes(interventionContext.getActions());

        Double[] inputsCharges = newArray(this.scenarioCodes.size(), 0d);

        for (Map.Entry<AgrosystInterventionType, List<AbstractAction>> actionsForAgrosystInterventionType : actionsByAgrosystInterventionTypes.entrySet()) {
            List<AbstractAction> actions = actionsForAgrosystInterventionType.getValue();
            for (AbstractAction abstractAction : actions) {
                Collection<OtherProductInputUsage> otherProductInputUsages = IndicatorOtherProductInputOperatingExpenses.getOtherProductInputUsage(abstractAction);

                final Double[] actionResult;
                if (CollectionUtils.isEmpty(otherProductInputUsages)) {
                    // no other product cost
                    actionResult = newArray(this.scenarioCodes.size(), 0d);
                } else {
                    //PA a (unité unique) : pour le moment, l’utilisateur ne peut saisir un prix que dans une seule unité :
                    //Euros/ha. Mais il faut prévoir à termes que l’utilisateur puisse saisir d’autres unités et donc adopter le
                    //même fonctionnement que pour tous les autres intrants. Donnée saisie par l’utilisateur.
                    actionResult = computeIndicatorForAction(
                            writerContext,
                            abstractAction,
                            otherProductInputUsages,
                            domainContext.getRefScenariosInputPricesByDomainInput()
                    );

                }
                inputsCharges = sum(actionResult, inputsCharges);
            }
        }

        PracticedIntervention intervention = interventionContext.getIntervention();
        final double toolPSCi = getToolPSCi(intervention);

        inputsCharges = sum(inputsCharges, mults(inputsCharges, toolPSCi));

        if (LOGGER.isDebugEnabled()) {
            PracticedSystem practicedSystem = practicedSystemContext.getPracticedSystem();
            LOGGER.debug(
                    String.format(
                            Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_PRACTICED,
                            "computeOtherProductIndicator",
                            intervention.getName(),
                            intervention.getTopiaId(),
                            practicedSystem.getName(),
                            practicedSystem.getCampaigns(),
                            (System.currentTimeMillis() - start)));
        }

        return inputsCharges;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {

        Double[] resultForScenario = newArray(this.scenarioCodes.size(), 0.0);// 1 by scenario + 1 by ref price

        // phyto
        resultForScenario = sum(resultForScenario,
                computePhytoProductInputIndicator(
                        writerContext,
                        domainContext,
                        zoneContext,
                        interventionContext));
        // irrig
        resultForScenario = sum(resultForScenario,
                computeIrrigationIndicator(
                        writerContext,
                        domainContext,
                        zoneContext,
                        interventionContext));
        // seeding
        resultForScenario = sum(resultForScenario,
                computeSeedingIndicator(
                        writerContext,
                        domainContext,
                        zoneContext,
                        interventionContext
                ));
        // MineralProductIndicator
        resultForScenario = sum(resultForScenario,
                computeMineralProductIndicator(
                        writerContext,
                        domainContext,
                        zoneContext,
                        interventionContext));
        // organicProductIndicator
        resultForScenario = sum(resultForScenario,
                computeOrganicProductIndicator(
                        writerContext,
                        domainContext,
                        zoneContext,
                        interventionContext));

        // other
        resultForScenario = sum(resultForScenario,
                computeOtherProductIndicator(
                        writerContext,
                        domainContext,
                        zoneContext,
                        interventionContext));

        // substrate
        resultForScenario = sum(resultForScenario,
                computeSubstrateProductIndicator(
                        writerContext,
                        domainContext,
                        zoneContext,
                        interventionContext));
        // pot
        resultForScenario = sum(resultForScenario,
                computePotIndicator(
                        writerContext,
                        domainContext,
                        zoneContext,
                        interventionContext));

        return resultForScenario;
    }

    private Double[] computePotIndicator(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        final Optional<List<OtherAction>> optionalAction_ = interventionContext.getOptionalOtherActions();
        if (optionalAction_.isEmpty()) {
            return newArray(this.scenarioCodes.size(), 0d);
        }

        Double[] inputsCharges = newArray(this.scenarioCodes.size(), 0d);

        List<OtherAction> actions = optionalAction_.get();
        for (OtherAction action : actions) {

            Collection<PotInputUsage> inputUsages = action.getPotInputUsages();

            if (CollectionUtils.isEmpty(inputUsages)) {
                return newArray(this.scenarioCodes.size(), 0d);
            }

            EffectiveIntervention intervention = interventionContext.getIntervention();

            final double toolPSCi = getToolPSCi(intervention);

            Double[] inputsCharges_ = computeIndicatorForAction(
                    writerContext,
                    action,
                    inputUsages,
                    domainContext.getRefScenariosInputPricesByDomainInput()
            );

            inputsCharges = sum(inputsCharges, mults(inputsCharges_, toolPSCi));

            if (LOGGER.isDebugEnabled()) {
                Zone zone = zoneContext.getZone();
                LOGGER.debug(
                        String.format(
                                Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_EFFECTIVE,
                                "computePotUsageIndicator",
                                intervention.getName(),
                                intervention.getTopiaId(),
                                zone.getName(),
                                (System.currentTimeMillis() - start)));
            }

        }

        return inputsCharges;
    }
    private Double[] computeSubstrateProductIndicatorForAction(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            AbstractAction action,
            Collection<SubstrateInputUsage> inputUsages){

        if (CollectionUtils.isEmpty(inputUsages)) {
            return newArray(this.scenarioCodes.size(), 0d);
        }

        Double[] inputsCharges = computeIndicatorForAction(
                writerContext,
                action,
                inputUsages,
                domainContext.getRefScenariosInputPricesByDomainInput()
        );

        return inputsCharges;
    }

    private Double[] computeSubstrateProductIndicator(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        final Optional<List<OtherAction>> optionalAction_ = interventionContext.getOptionalOtherActions();
        final Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();
        if (optionalAction_.isEmpty() && optionalSeedingActionUsage.isEmpty()) {
            return newArray(this.scenarioCodes.size(), 0d);
        }

        EffectiveIntervention intervention = interventionContext.getIntervention();
        final double toolPSCi = getToolPSCi(intervention);

        Double[] inputsCharges = newArray(this.scenarioCodes.size(), 0d);

        if (optionalAction_.isPresent()) {
            List<OtherAction> otherActions = optionalAction_.get();
            for (OtherAction action : otherActions) {

                Collection<SubstrateInputUsage> inputUsages = action.getSubstrateInputUsages();

                Double[] inputsCharges_ = computeSubstrateProductIndicatorForAction(writerContext,
                        domainContext,
                        action,
                        inputUsages);
                inputsCharges = sum(inputsCharges, mults(inputsCharges_, toolPSCi));
            }
        }

        if (optionalSeedingActionUsage.isPresent()) {
            SeedingActionUsage seedingActionUsage = optionalSeedingActionUsage.get();
            Collection<SubstrateInputUsage> inputUsages = seedingActionUsage.getSubstrateInputUsage();

            Double[] inputsCharges_ = computeSubstrateProductIndicatorForAction(writerContext,
                    domainContext,
                    seedingActionUsage,
                    inputUsages);
            inputsCharges = sum(inputsCharges, mults(inputsCharges_, toolPSCi));
        }

        if (LOGGER.isDebugEnabled()) {
            Zone zone = zoneContext.getZone();
            LOGGER.debug(
                    String.format(
                            Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_EFFECTIVE,
                            "computeSubstrateProductIndicator",
                            intervention.getName(),
                            intervention.getTopiaId(),
                            zone.getName(),
                            (System.currentTimeMillis() - start)));
        }

        return inputsCharges;
    }

    private Double[] computeOtherProductIndicator(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        Map<AgrosystInterventionType, List<AbstractAction>> actionsByAgrosystInterventionTypes = getActionsByAgrosystInterventionTypes(interventionContext.getActions());

        Double[] inputsCharges = newArray(this.scenarioCodes.size(), 0d);

        for (Map.Entry<AgrosystInterventionType, List<AbstractAction>> actionsForAgrosystInterventionType : actionsByAgrosystInterventionTypes.entrySet()) {
            List<AbstractAction> actions = actionsForAgrosystInterventionType.getValue();
            for (AbstractAction abstractAction : actions) {
                Collection<OtherProductInputUsage> otherProductInputUsages = IndicatorOtherProductInputOperatingExpenses.getOtherProductInputUsage(abstractAction);

                final Double[] actionResult;
                if (CollectionUtils.isEmpty(otherProductInputUsages)) {
                    // no other product cost
                    actionResult = newArray(this.scenarioCodes.size(), 0d);
                } else {
                    //PA a (unité unique) : pour le moment, l’utilisateur ne peut saisir un prix que dans une seule unité :
                    //Euros/ha. Mais il faut prévoir à termes que l’utilisateur puisse saisir d’autres unités et donc adopter le
                    //même fonctionnement que pour tous les autres intrants. Donnée saisie par l’utilisateur.
                    actionResult = computeIndicatorForAction(
                            writerContext,
                            abstractAction,
                            otherProductInputUsages,
                            domainContext.getRefScenariosInputPricesByDomainInput()
                    );

                }
                inputsCharges = sum(actionResult, inputsCharges);
            }
        }

        EffectiveIntervention intervention = interventionContext.getIntervention();
        final double toolPSCi = getToolPSCi(intervention);

        inputsCharges = sum(inputsCharges, mults(inputsCharges, toolPSCi));

        if (LOGGER.isDebugEnabled()) {
            Zone zone = zoneContext.getZone();
            LOGGER.debug(
                    String.format(
                            Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_EFFECTIVE,
                            "computeOtherProductIndicator",
                            intervention.getName(),
                            intervention.getTopiaId(),
                            zone.getName(),
                            (System.currentTimeMillis() - start)));
        }

        return inputsCharges;
    }

    private Double[] computeOrganicProductIndicator(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        final Optional<OrganicFertilizersSpreadingAction> optionalAction = interventionContext.getOptionalOrganicFertilizersSpreadingAction();
        if (optionalAction.isEmpty()) {
            return newArray(this.scenarioCodes.size(), 0d);
        }

        final OrganicFertilizersSpreadingAction action = optionalAction.get();
        Collection<OrganicProductInputUsage> inputUsages = action.getOrganicProductInputUsages();

        if (CollectionUtils.isEmpty(inputUsages)) {
            return newArray(this.scenarioCodes.size(), 0d);
        }

        EffectiveIntervention intervention = interventionContext.getIntervention();

        final double toolPSCi = getToolPSCi(intervention);

        Double[] inputsCharges = computeIndicatorForAction(
                writerContext,
                action,
                inputUsages,
                domainContext.getRefScenariosInputPricesByDomainInput()
        );

        inputsCharges = sum(inputsCharges, mults(inputsCharges, toolPSCi));

        if (LOGGER.isDebugEnabled()) {
            Zone zone = zoneContext.getZone();
            LOGGER.debug(
                    String.format(
                            Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_EFFECTIVE,
                            "computeOrganicProductIndicator",
                            intervention.getName(),
                            intervention.getTopiaId(),
                            zone.getName(),
                            (System.currentTimeMillis() - start)));
        }

        return inputsCharges;
    }

    private Double[] computeMineralProductIndicator(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        final Optional<MineralFertilizersSpreadingAction> optionalMineralFertilizersSpreadingAction = interventionContext.getOptionalMineralFertilizersSpreadingAction();

        if (optionalMineralFertilizersSpreadingAction.isEmpty()) {
            return newArray(this.scenarioCodes.size(), 0d);//inputsCharges;
        }

        MineralFertilizersSpreadingAction action = optionalMineralFertilizersSpreadingAction.get();
        Collection<MineralProductInputUsage> mineralProductInputUsages = action.getMineralProductInputUsages();
        if (CollectionUtils.isEmpty(mineralProductInputUsages)) {
            return newArray(this.scenarioCodes.size(), 0d);//inputsCharges;
        }

        EffectiveIntervention intervention = interventionContext.getIntervention();

        final double toolPSCi = getToolPSCi(intervention);

        Double[] inputsCharges = computeIndicatorForAction(
                writerContext,
                action,
                mineralProductInputUsages,
                domainContext.getRefScenariosInputPricesByDomainInput()
        );

        inputsCharges = sum(inputsCharges, mults(inputsCharges, toolPSCi));

        if (LOGGER.isDebugEnabled()) {
            Zone zone = zoneContext.getZone();
            LOGGER.debug(
                    String.format(
                            Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_EFFECTIVE,
                            "computeMineralProductIndicator",
                            intervention.getName(),
                            intervention.getTopiaId(),
                            zone.getName(),
                            (System.currentTimeMillis() - start)));
        }

        return inputsCharges;
    }

    private Double[] computeSeedingIndicator(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        EffectiveIntervention intervention = interventionContext.getIntervention();
        CroppingPlanEntry crop = interventionContext.getCroppingPlanEntry();

        Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();

        if (optionalSeedingActionUsage.isEmpty()) {
            // return 0 to not take consideration of this indicator
            return newArray(this.scenarioCodes.size(), 0d);
        }

        if (crop == null) {
            // no counter to increment
            return newArray(this.scenarioCodes.size(), 0d);
        }

        final SeedingActionUsage seedingActionUsage = optionalSeedingActionUsage.get();
        final String seedingActionTopiaId = seedingActionUsage.getTopiaId();

        incrementAngGetTotalFieldCounterForTargetedId(seedingActionTopiaId);

        final RefScenariosInputPricesByDomainInput scenarioPriceByDomainInput = domainContext.getRefScenariosInputPricesByDomainInput();
        final Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> inputRefPricesByDomainInput = scenarioPriceByDomainInput.speciesRefPriceByScenarioForInput();

        final double toolPSCi = getToolPSCi(intervention);

        if (inputRefPricesByDomainInput == null) {
            // if there is a seeding action without seeding price and seeding ref price
            addMissingFieldMessage(seedingActionTopiaId, messageBuilder.getMissingSeedingPriceMessage(crop));
            return newArray(this.scenarioCodes.size(), 0d);
        }

        Zone zone = zoneContext.getZone();

        Double[] seedingIndicator = computeCropOperatingExpenses(
                writerContext,
                seedingActionUsage,
                domainContext.getRefScenariosInputPricesByDomainInput()
        );

        seedingIndicator = mults(seedingIndicator, toolPSCi);

        if (LOGGER.isDebugEnabled()) {

            LOGGER.debug(
                    String.format(
                            Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_EFFECTIVE,
                            "computeSeedInputIndicator",
                            intervention.getName(),
                            intervention.getTopiaId(),
                            zone.getName(),
                            (System.currentTimeMillis() - start)));
        }

        return seedingIndicator;
    }

    private Double[] computeIrrigationIndicator(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        Optional<IrrigationAction> optionalIrrigationAction = interventionContext.getOptionalIrrigationAction();

        if (optionalIrrigationAction.isEmpty()) {
            return newArray(scenarioCodes.size(), 0d);
        }

        EffectiveIntervention intervention = interventionContext.getIntervention();

        final double toolPSCi = getToolPSCi(intervention);

        RefScenariosInputPricesByDomainInput scenarioPriceByDomainInput = domainContext.getRefScenariosInputPricesByDomainInput();

        final Double[] irrigActionResult = computeIrrigIndicator(
                writerContext,
                optionalIrrigationAction.get(),
                scenarioPriceByDomainInput);

        final Double[] irrigInterventionResult = mults(irrigActionResult, toolPSCi);

        if (LOGGER.isDebugEnabled()) {
            Zone zone = zoneContext.getZone();
            LOGGER.debug(
                    String.format(
                            Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_EFFECTIVE,
                            "computeIrrigationIndicator",
                            intervention.getName(),
                            intervention.getTopiaId(),
                            zone.getName(),
                            (System.currentTimeMillis() - start)));
        }

        return irrigInterventionResult;
    }

    private Double[] computePhytoProductInputIndicator(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        // do not consider Seeding ones
        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionContext.getOptionalBiologicalControlAction();
        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction();
        if (optionalBiologicalControlAction.isEmpty() && optionalPesticidesSpreadingAction.isEmpty()) {
            return newArray(this.scenarioCodes.size(), 0d);//inputsCharges;
        }

        final Collection<BiologicalProductInputUsage> biologicalProductInputUsages = optionalBiologicalControlAction.isPresent() ? CollectionUtils.emptyIfNull(optionalBiologicalControlAction.get().getBiologicalProductInputUsages()) : new ArrayList<>();
        final Collection<PesticideProductInputUsage> pesticideProductInputUsages = optionalPesticidesSpreadingAction.isPresent() ? CollectionUtils.emptyIfNull(optionalPesticidesSpreadingAction.get().getPesticideProductInputUsages()) : new ArrayList<>();

        Collection<AbstractPhytoProductInputUsage> productInputUsages = new ArrayList<>(biologicalProductInputUsages);
        productInputUsages.addAll(pesticideProductInputUsages);

        if (CollectionUtils.isEmpty(productInputUsages)) {
            // no phyto product cost
            return newArray(this.scenarioCodes.size(), 0d);//inputsCharges;
        }

        EffectiveIntervention intervention = interventionContext.getIntervention();
        final String interventionId = intervention.getTopiaId();

        final RefScenariosInputPricesByDomainInput scenarioPriceByDomainInput = domainContext.getRefScenariosInputPricesByDomainInput();

        final double toolPSCi = getToolPSCi(intervention);

        final Double[] result = computeOperatingExpenses(
                writerContext,
                optionalBiologicalControlAction,
                optionalPesticidesSpreadingAction,
                interventionId,
                scenarioPriceByDomainInput,
                toolPSCi);

        if (LOGGER.isDebugEnabled()) {
            Zone zone = zoneContext.getAnonymizeZone();
            LOGGER.debug(
                    String.format(
                            Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_EFFECTIVE,
                            "computePhytoProductInputIndicator",
                            intervention.getName(),
                            intervention.getTopiaId(),
                            zone.getName(),
                            (System.currentTimeMillis() - start)));
        }

        return result;
    }

    protected Double[] computePhytoProductInputIndicator(
            WriterContext writerContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        // do not consider Seeding ones
        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionContext.getOptionalBiologicalControlAction();
        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction();
        if (optionalBiologicalControlAction.isEmpty() && optionalPesticidesSpreadingAction.isEmpty()) {
            return newArray(this.scenarioCodes.size(), 0d);//inputsCharges;
        }

        final Collection<BiologicalProductInputUsage> biologicalProductInputUsages = optionalBiologicalControlAction.isPresent() ? CollectionUtils.emptyIfNull(optionalBiologicalControlAction.get().getBiologicalProductInputUsages()) : new ArrayList<>();
        final Collection<PesticideProductInputUsage> pesticideProductInputUsages = optionalPesticidesSpreadingAction.isPresent() ? CollectionUtils.emptyIfNull(optionalPesticidesSpreadingAction.get().getPesticideProductInputUsages()) : new ArrayList<>();

        Collection<AbstractPhytoProductInputUsage> productInputUsages = new ArrayList<>(biologicalProductInputUsages);
        productInputUsages.addAll(pesticideProductInputUsages);

        if (CollectionUtils.isEmpty(productInputUsages)) {
            // no phyto product cost
            return newArray(this.scenarioCodes.size(), 0d);//inputsCharges;
        }

        PracticedIntervention intervention = interventionContext.getIntervention();
        final String interventionId = intervention.getTopiaId();

        final RefScenariosInputPricesByDomainInput scenarioPriceByDomainInput = domainContext.getRefScenariosInputPricesByDomainInput();

        final double toolPSCi = getToolPSCi(intervention);

        final Double[] result = computeOperatingExpenses(
                writerContext,
                optionalBiologicalControlAction,
                optionalPesticidesSpreadingAction,
                interventionId,
                scenarioPriceByDomainInput,
                toolPSCi);

        if (LOGGER.isDebugEnabled()) {
            PracticedSystem practicedSystem = practicedSystemContext.getAnonymizePracticedSystem();
            LOGGER.debug(
                    String.format(
                            Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_PRACTICED,
                            "computePhytoProductInputIndicator",
                            intervention.getName(),
                            intervention.getTopiaId(),
                            practicedSystem.getName(),
                            practicedSystem.getCampaigns(),
                            (System.currentTimeMillis() - start)));
        }

        return result;
    }

    private Double[] computeOperatingExpenses(
            WriterContext writerContext,
            Optional<BiologicalControlAction> optionalBiologicalControlAction,
            Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction,
            String interventionId,
            RefScenariosInputPricesByDomainInput inputRefPricesByDomainInput,
            double toolPSCi) {

        Double[] phytoInterventionResult = newArray(this.scenarioCodes.size(), 0d);
        if (inputRefPricesByDomainInput == null) {
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            addMissingFieldMessage(
                    interventionId,
                    messageBuilder.getMissingInputUsagePriceMessage(
                            InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                            null,
                            null));
            return newArray(this.scenarioCodes.size(), 0d);
        }

        incrementAngGetTotalFieldCounterForTargetedId(interventionId);//treatedSurface (1 for all inputs)

        if (optionalBiologicalControlAction.isPresent()) {
            final BiologicalControlAction biologicalControlAction = optionalBiologicalControlAction.get();
            final Collection<BiologicalProductInputUsage> biologicalProductInputs = biologicalControlAction.getBiologicalProductInputUsages();

            Double[] phytoActionResult = computeIndicatorForAction(
                    writerContext,
                    biologicalControlAction,
                    biologicalProductInputs,
                    inputRefPricesByDomainInput
            );
            // seulement pour phyto sinon 1
            double treatedSurface = getPhytoInputTreatedSurfaceFactor(biologicalControlAction, interventionId);
            double psci = toolPSCi * treatedSurface;

            phytoInterventionResult = sum(phytoInterventionResult, mults(phytoActionResult, psci));
        }

        if (optionalPesticidesSpreadingAction.isPresent()) {

            final PesticidesSpreadingAction pesticidesSpreadingAction = optionalPesticidesSpreadingAction.get();
            final Collection<PesticideProductInputUsage> pesticideProductInputs = pesticidesSpreadingAction.getPesticideProductInputUsages();

            Double[] phytoActionResult = computeIndicatorForAction(
                    writerContext,
                    pesticidesSpreadingAction,
                    pesticideProductInputs,
                    inputRefPricesByDomainInput
            );
            // seulement pour phyto sinon 1
            double treatedSurface = getPhytoInputTreatedSurfaceFactor(pesticidesSpreadingAction, interventionId);
            double psci = toolPSCi * treatedSurface;

            phytoInterventionResult = sum(phytoInterventionResult, mults(phytoActionResult, psci));
        }


        return phytoInterventionResult;
    }

    protected Double[] computeIndicatorForAction(
            WriterContext writerContext,
            AbstractAction action,
            Collection<? extends AbstractInputUsage> inputUsages,
            RefScenariosInputPricesByDomainInput refCampaignsInputPricesByDomainInputAndCampaigns) {

        Double[] inputsCharges = newArray(this.scenarioCodes.size(), 0d);

        for (AbstractInputUsage inputUsage : inputUsages) {

            Double[] inputResult = computeInputProductOperatingExpenses(
                    writerContext,
                    refCampaignsInputPricesByDomainInputAndCampaigns,
                    action,
                    inputUsage
            );

            inputsCharges = sum(inputsCharges, inputResult);
        }

        return inputsCharges;
    }

    public Map<String, Optional<InputRefPrice>> getInputScenarioPrice(AbstractInputUsage usage, RefScenariosInputPricesByDomainInput refCampaignsInputPricesByDomainInputAndCampaigns) {
        final Map<String, Optional<InputRefPrice>> optionalInputScenarioPriceByScenarioCode = new HashMap<>();
        if (usage instanceof BiologicalProductInputUsage || usage instanceof PesticideProductInputUsage || usage instanceof SeedProductInputUsage) {
            final DomainPhytoProductInput input = ((AbstractPhytoProductInputUsage) usage).getDomainPhytoProductInput();
            Map<String, Optional<InputRefPrice>> inputRefPrices = refCampaignsInputPricesByDomainInputAndCampaigns.phytoRefPriceByScenarioForInput().get(input);
            if (inputRefPrices != null) {
                optionalInputScenarioPriceByScenarioCode.putAll(inputRefPrices);
            }
        } else if (usage instanceof OrganicProductInputUsage inputUsage) {
            DomainOrganicProductInput input = inputUsage.getDomainOrganicProductInput();
            Map<String, Optional<InputRefPrice>> inputRefPrices = refCampaignsInputPricesByDomainInputAndCampaigns.fertiOrgaRefPriceByScenarioForInput().get(input);
            if (inputRefPrices != null) {
                optionalInputScenarioPriceByScenarioCode.putAll(inputRefPrices);
            }
        } else if (usage instanceof OtherProductInputUsage inputUsage) {
            DomainOtherInput input = inputUsage.getDomainOtherInput();
            Map<String, Optional<InputRefPrice>> inputRefPrices = refCampaignsInputPricesByDomainInputAndCampaigns.otherRefPriceByScenarioForInput().get(input);
            if (inputRefPrices != null) {
                optionalInputScenarioPriceByScenarioCode.putAll(inputRefPrices);
            }
        } else if (usage instanceof MineralProductInputUsage inputUsage) {
            DomainMineralProductInput input = inputUsage.getDomainMineralProductInput();
            Map<String, Optional<InputRefPrice>> inputRefPrices = refCampaignsInputPricesByDomainInputAndCampaigns.mineralRefPriceByScenarioForInput().get(input);
            if (inputRefPrices != null) {
                optionalInputScenarioPriceByScenarioCode.putAll(inputRefPrices);
            }
        } else if (usage instanceof SeedLotInputUsage inputUsage) {
            DomainSeedLotInput input = inputUsage.getDomainSeedLotInput();
            Map<String, Optional<InputRefPrice>> inputRefPrices = refCampaignsInputPricesByDomainInputAndCampaigns.speciesRefPriceByScenarioForInput().get(input);
            if (inputRefPrices != null) {
                optionalInputScenarioPriceByScenarioCode.putAll(inputRefPrices);
            }
        } else if (usage instanceof SeedSpeciesInputUsage inputUsage) {
            DomainSeedSpeciesInput input = inputUsage.getDomainSeedSpeciesInput();
            Map<String, Optional<InputRefPrice>> inputRefPrices = refCampaignsInputPricesByDomainInputAndCampaigns.speciesRefPriceByScenarioForInput().get(input);
            if (inputRefPrices != null) {
                optionalInputScenarioPriceByScenarioCode.putAll(inputRefPrices);
            }
        } else if (usage instanceof PotInputUsage inputUsage) {
            DomainPotInput input = inputUsage.getDomainPotInput();
            Map<String, Optional<InputRefPrice>> inputRefPrices = refCampaignsInputPricesByDomainInputAndCampaigns.potRefPriceByScenarioForInput().get(input);
            if (inputRefPrices != null) {
                optionalInputScenarioPriceByScenarioCode.putAll(inputRefPrices);
            }
        } else if (usage instanceof SubstrateInputUsage inputUsage) {
            DomainSubstrateInput input = inputUsage.getDomainSubstrateInput();
            Map<String, Optional<InputRefPrice>> inputRefPrices = refCampaignsInputPricesByDomainInputAndCampaigns.substrateRefPriceByScenarioForInput().get(input);
            if (inputRefPrices != null) {
                optionalInputScenarioPriceByScenarioCode.putAll(inputRefPrices);
            }
        } else if (usage instanceof IrrigationInputUsage inputUsage) {
            DomainIrrigationInput input = inputUsage.getDomainIrrigationInput();
            Map<String, Optional<InputRefPrice>> inputRefPrices = refCampaignsInputPricesByDomainInputAndCampaigns.irrigRefPriceByScenarioForInput().get(input);
            if (inputRefPrices != null) {
                optionalInputScenarioPriceByScenarioCode.putAll(inputRefPrices);
            }
        }
        return optionalInputScenarioPriceByScenarioCode;
    }

    private Double[] computeInputProductOperatingExpenses(
            WriterContext writerContext,
            RefScenariosInputPricesByDomainInput scenarioPriceByDomainInput,
            AbstractAction action,
            AbstractInputUsage inputUsage) {

        Double[] inputResult = newArray(this.scenarioCodes.size(), 0.0);

        final String usageTopiaId = inputUsage.getTopiaId();

        incrementAngGetTotalFieldCounterForTargetedId(usageTopiaId);// da
        incrementAngGetTotalFieldCounterForTargetedId(usageTopiaId);// product price

        Double da = inputUsage.getQtAvg();

        addMissingDeprecatedInputQtyOrUnitIfRequired(inputUsage);

        if (da == null) {
            // #10124 - si le champ 'dose appliquée' n'est pas renseignée
            // pour un intrant de type 'produit phytosanitaire',
            // alors la valeur par défaut est la dose de référence
            // pour ce produit phytosanitaire sur la culture concernée (ce qui donne un IFT de 1, modulo PSCI);
            //        - si Agrosyst ne trouve pas de dose de référence pour ce produit phytosanitaire
            //          sur la culture concernée,
            //        alors l'IFT associée à l'intrant est 1*PSCI (comme c'est déjà le cas)
            da = Indicator.DEFAULT_IFT;
            addMissingFieldMessage(
                    usageTopiaId,
                    messageBuilder.getMissingDoseMessage(inputUsage.getInputType(), MissingMessageScope.INPUT));
        }

        Map<String, Optional<InputRefPrice>> scenarioPriceByCodes = getInputScenarioPrice(inputUsage, scenarioPriceByDomainInput);

        // #10063 Si aucun prix de référence n’est disponible pour le prix de l’intrant,
        // alors on prend la valeur « 0 » comme référence.
        for (Map.Entry<String, Scenario> scenarioCodeToPrice : scenariosByCode.entrySet()) {
            double inputsCharges = 0d;
            String scenarioCode = scenarioCodeToPrice.getValue().code();
            final Optional<InputRefPrice> optionalScenarioPrice = scenarioPriceByCodes.get(scenarioCode);
            if (optionalScenarioPrice != null && optionalScenarioPrice.isPresent()) {
                PriceAndUnit scenarioPriceAndUnit = optionalScenarioPrice.get().averageRefPrice();

                if (scenarioPriceAndUnit != null) {
                    inputsCharges = IndicatorOperatingExpenses.computeCi(da, scenarioPriceAndUnit.value(), scenarioPriceAndUnit.unit(), 1);
                }
            } else {
                addMissingScenarioPriceMessage(inputUsage.getTopiaId(), scenarioCode);
            }

            inputResult[scenarioCodes.indexOf(scenarioCode)] += inputsCharges;

        }

        writeInputUsage(
                writerContext,
                action,
                inputUsage,
                inputResult);

        return inputResult;
    }

    protected Double[] computeOrganicProductIndicator(
            WriterContext writerContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        final Optional<OrganicFertilizersSpreadingAction> optionalAction = interventionContext.getOptionalOrganicFertilizersSpreadingAction();
        if (optionalAction.isEmpty()) {
            return newArray(this.scenarioCodes.size(), 0d);
        }

        final OrganicFertilizersSpreadingAction action = optionalAction.get();
        Collection<OrganicProductInputUsage> inputUsages = action.getOrganicProductInputUsages();

        if (CollectionUtils.isEmpty(inputUsages)) {
            return newArray(this.scenarioCodes.size(), 0d);
        }

        PracticedIntervention intervention = interventionContext.getIntervention();

        final double toolPSCi = getToolPSCi(intervention);

        Double[] inputsCharges = computeIndicatorForAction(
                writerContext,
                action,
                inputUsages,
                domainContext.getRefScenariosInputPricesByDomainInput()
        );

        inputsCharges = sum(inputsCharges, mults(inputsCharges, toolPSCi));

        if (LOGGER.isDebugEnabled()) {
            PracticedSystem practicedSystem = practicedSystemContext.getPracticedSystem();
            LOGGER.debug(
                    String.format(
                            Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_PRACTICED,
                            "computeOrganicProductIndicator",
                            intervention.getName(),
                            intervention.getTopiaId(),
                            practicedSystem.getName(),
                            practicedSystem.getCampaigns(),
                            (System.currentTimeMillis() - start)));
        }

        return inputsCharges;
    }

    protected Double[] computeMineralProductIndicator(
            WriterContext writerContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        final Optional<MineralFertilizersSpreadingAction> optionalMineralFertilizersSpreadingAction = interventionContext.getOptionalMineralFertilizersSpreadingAction();

        if (optionalMineralFertilizersSpreadingAction.isEmpty()) {
            return newArray(this.scenarioCodes.size(), 0d);//inputsCharges;
        }

        MineralFertilizersSpreadingAction action = optionalMineralFertilizersSpreadingAction.get();
        Collection<MineralProductInputUsage> mineralProductInputUsages = action.getMineralProductInputUsages();
        if (CollectionUtils.isEmpty(mineralProductInputUsages)) {
            return newArray(this.scenarioCodes.size(), 0d);//inputsCharges;
        }

        PracticedIntervention intervention = interventionContext.getIntervention();

        final double toolPSCi = getToolPSCi(intervention);

        Double[] inputsCharges = computeIndicatorForAction(
                writerContext,
                action,
                mineralProductInputUsages,
                domainContext.getRefScenariosInputPricesByDomainInput()
        );

        inputsCharges = sum(inputsCharges, mults(inputsCharges, toolPSCi));

        if (LOGGER.isDebugEnabled()) {
            PracticedSystem practicedSystem = practicedSystemContext.getPracticedSystem();
            LOGGER.debug(
                    String.format(
                            Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_PRACTICED,
                            "computeMineralProductIndicator",
                            intervention.getName(),
                            intervention.getTopiaId(),
                            practicedSystem.getName(),
                            practicedSystem.getCampaigns(),
                            (System.currentTimeMillis() - start)));
        }

        return inputsCharges;
    }

    /**
     * Q EV (diverses unités) : quantité semée du couple EV, EV appartenant à la liste des couples EV semés
     * dans l’action semis de l’intervention i. Donnée saisie par l’utilisateur.
     * PA EV (diverses unités) : prix d’achat du couple EV pour ce type de semence (de ferme, certifiées ...),
     * EV appartenant à la liste des couples EV semés au cours de l’intervention i. Donnée saisie par
     * l’utilisateur.
     * On considère une semence comme étant un intrant
     */
    protected Double[] computeSeedingIndicator(
            WriterContext writerContext,
            PerformancePracticedDomainExecutionContext domainExecutionContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        PracticedIntervention intervention = interventionContext.getIntervention();
        CroppingPlanEntry crop = interventionContext.getCropWithSpecies().getCroppingPlanEntry();

        Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();

        if (optionalSeedingActionUsage.isEmpty()) {
            // return 0 to not take consideration of this indicator
            return newArray(this.scenarioCodes.size(), 0d);
        }

        if (crop == null) {
            // no counter to increment
            return newArray(this.scenarioCodes.size(), 0d);
        }

        final SeedingActionUsage seedingActionUsage = optionalSeedingActionUsage.get();
        final String seedingActionTopiaId = seedingActionUsage.getTopiaId();

        incrementAngGetTotalFieldCounterForTargetedId(seedingActionTopiaId);

        final RefScenariosInputPricesByDomainInput scenarioPriceByDomainInput = domainExecutionContext.getRefScenariosInputPricesByDomainInput();
        final Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> inputRefPricesByDomainInput = scenarioPriceByDomainInput.speciesRefPriceByScenarioForInput();

        if (inputRefPricesByDomainInput == null) {
            // if there is a seeding action without seeding price and seeding ref price
            addMissingFieldMessage(seedingActionTopiaId, messageBuilder.getMissingSeedingPriceMessage(crop));
            return newArray(this.scenarioCodes.size(), 0d);
        }

        Double[] seedingIndicator = computeCropOperatingExpenses(
                writerContext,
                seedingActionUsage,
                domainExecutionContext.getRefScenariosInputPricesByDomainInput()
        );

        final double toolPSCi = getToolPSCi(intervention);

        seedingIndicator = mults(seedingIndicator, toolPSCi);

        if (LOGGER.isDebugEnabled()) {

            PracticedSystem practicedSystem = practicedSystemContext.getAnonymizePracticedSystem();

            LOGGER.debug(
                    String.format(
                            Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_PRACTICED,
                            "computeSeedInputIndicator",
                            intervention.getName(),
                            intervention.getTopiaId(),
                            practicedSystem.getName(),
                            practicedSystem.getCampaigns(),
                            (System.currentTimeMillis() - start)));
        }

        return seedingIndicator;
    }

    private Double[] computeCropOperatingExpenses(
            final WriterContext writerContext,
            final SeedingActionUsage seedingActionUsage,
            RefScenariosInputPricesByDomainInput scenarioPricesByDomainInputs) {

        final Collection<SeedLotInputUsage> seedLotInputUsages = seedingActionUsage.getSeedLotInputUsage();

        final String seedingActionUsageTopiaId = seedingActionUsage.getTopiaId();

        Double[] result = newArray(this.scenarioCodes.size(), 0.0);

        for (SeedLotInputUsage seedLotInputUsage : seedLotInputUsages) {
            final DomainSeedLotInput domainSeedLotInput = seedLotInputUsage.getDomainSeedLotInput();

            CroppingPlanEntry croppingPlanEntry = domainSeedLotInput.getCropSeed();
            final String cropName = croppingPlanEntry.getName();

            // refs #8507
            // Ne pas considérer les unités des espèces dont la quantitée semée est 0 car celle si sont a ignorer
            incrementAngGetTotalFieldCounterForTargetedId(seedingActionUsageTopiaId); // quantité + unit

            incrementAngGetTotalFieldCounterForTargetedId(seedingActionUsageTopiaId); // price

            // le prix de référence ne peut être connu à l'échelle de la culture car il manque la proportion de l'espèce dans la culture
            final Collection<SeedSpeciesInputUsage> seedingSpeciesUsages = CollectionUtils.emptyIfNull(seedLotInputUsage.getSeedingSpecies());
            double lot_da = seedingSpeciesUsages.stream().mapToDouble(SeedSpeciesInputUsage::getQtAvg).sum();// la dose appliqué correspond à la quantité de semence

            if (CollectionUtils.isEmpty(seedingSpeciesUsages)) {
                addMissingFieldMessage(seedingActionUsageTopiaId, messageBuilder.getMissingSeedingPriceMessage(cropName));

            }

            final Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> scenariosSeedPricesByInputSpecies = scenarioPricesByDomainInputs.speciesRefPriceByScenarioForInput();
            final Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> scenariosProductPricesByInputProduct = scenarioPricesByDomainInputs.phytoRefPriceByScenarioForInput();

            for (SeedSpeciesInputUsage seedSpeciesInputUsage : seedingSpeciesUsages) {

                Double species_da = seedSpeciesInputUsage.getQtAvg();

                String usageTopiaId = seedSpeciesInputUsage.getTopiaId();
                addMissingDeprecatedInputQtyOrUnitIfRequired(seedSpeciesInputUsage);

                if (species_da == null) {
                    species_da = Indicator.DEFAULT_IFT;
                    addMissingFieldMessage(
                            usageTopiaId,
                            messageBuilder.getMissingDoseMessage(seedSpeciesInputUsage.getInputType(), MissingMessageScope.INPUT));
                }
                final double speciesPart = lot_da == 0 ? 0 : species_da / lot_da;

                final DomainSeedSpeciesInput domainSeedSpeciesInput = seedSpeciesInputUsage.getDomainSeedSpeciesInput();

                final Map<String, Optional<InputRefPrice>> speciesPriceByScenario = scenariosSeedPricesByInputSpecies.get(domainSeedSpeciesInput);

                for (String scenarioCode : scenariosByCode.keySet()) {
                    final Optional<InputRefPrice> optionalScenarioPrice = ofNullable(speciesPriceByScenario).flatMap(map -> map.get(scenarioCode));
                    if (optionalScenarioPrice.isEmpty()) {
                        addMissingScenarioPriceMessage(domainSeedSpeciesInput.getTopiaId(), scenarioCode);
                    } else {
                        final InputRefPrice inputRefPrice = optionalScenarioPrice.get();
                        final PriceAndUnit priceAndUnit_ = inputRefPrice.averageRefPrice();
                        final double refPriceValue = priceAndUnit_.value() * speciesPart;
                        final double refSpeciesCi = IndicatorOperatingExpenses.computeCi(lot_da, refPriceValue, priceAndUnit_.unit(), 1);
                        result[scenarioCodes.indexOf(scenarioCode)] += refSpeciesCi;
                    }
                }

                boolean seedPriceWithTreatment = (domainSeedSpeciesInput.isChemicalTreatment() || domainSeedSpeciesInput.isBiologicalSeedInoculation())
                        && domainSeedSpeciesInput.getSeedPrice() != null
                        && domainSeedSpeciesInput.getSeedPrice().isIncludedTreatment();

                if (!seedPriceWithTreatment) {
                    final Collection<SeedProductInputUsage> seedProductInputs = seedSpeciesInputUsage.getSeedProductInputUsages();
                    for (SeedProductInputUsage seedProductInput : seedProductInputs) {
                        Double[] spoeCi = newArray(this.scenarioCodes.size(), 0d);

                        addMissingDeprecatedInputQtyOrUnitIfRequired(seedSpeciesInputUsage);

                        Double product_da = seedProductInput.getQtAvg();

                        if (product_da == null) {
                            // IFT associée à l'intrant est 1*PSCI
                            product_da = Indicator.DEFAULT_IFT;
                            addMissingFieldMessage(
                                    usageTopiaId,
                                    messageBuilder.getMissingDoseMessage(seedSpeciesInputUsage.getInputType(), MissingMessageScope.INPUT));
                        }
                        final DomainPhytoProductInput domainPhytoProductInput = seedProductInput.getDomainPhytoProductInput();

                        final Map<String, Optional<InputRefPrice>> scenarioProductPriceByScenario = scenariosProductPricesByInputProduct.get(domainPhytoProductInput);

                        for (String scenarioCode : scenariosByCode.keySet()) {
                            final Optional<InputRefPrice> optionalScenarioPrice = scenarioProductPriceByScenario.get(scenarioCode);
                            if (optionalScenarioPrice.isEmpty()) {
                                addMissingScenarioPriceMessage(domainSeedSpeciesInput.getTopiaId(), scenarioCode);
                            } else {
                                final InputRefPrice inputRefPrice = optionalScenarioPrice.get();
                                final PriceAndUnit priceAndUnit_ = inputRefPrice.averageRefPrice();
                                final double refProductCi = IndicatorOperatingExpenses.computeCi(product_da, priceAndUnit_.value(), priceAndUnit_.unit(), 1);// conversionRate already applied to refPrices
                                spoeCi[scenarioCodes.indexOf(scenarioCode)] = refProductCi;
                                result[scenarioCodes.indexOf(scenarioCode)] += refProductCi;
                            }
                        }

                        writeInputUsage(
                                writerContext,
                                seedingActionUsage,
                                seedProductInput,
                                spoeCi);

                    }
                }

                // 1 par espèce d'un lot, pour correspondre à ce qui est fait dans
                // IndicatorSeedingOperatingExpenses::computeCropOperatingExpenses
                writeActionAsInputUsage(seedingActionUsage, seedSpeciesInputUsage, writerContext, result);
            }

        }

        return result;
    }

    private void writeActionAsInputUsage(
            AbstractAction action,
            SeedSpeciesInputUsage seedSpeciesInputUsage,
            WriterContext writerContext,
            Double[] inputResult) {

        IndicatorWriter writer = writerContext.getWriter();

        String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(action.getTopiaId(), MissingMessageScope.INPUT);
        Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(action.getTopiaId());

        write(
                action,
                seedSpeciesInputUsage,
                writerContext,
                inputResult,
                writer,
                reliabilityCommentForInputId,
                reliabilityIndexForInputId);
    }

    protected Double[] computeIrrigationIndicator(
            WriterContext writerContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext) {

        long start = System.currentTimeMillis();

        Optional<IrrigationAction> optionalIrrigationAction = interventionContext.getOptionalIrrigationAction();

        if (optionalIrrigationAction.isEmpty()) {
            return newArray(scenarioCodes.size(), 0d);
        }

        PracticedIntervention intervention = interventionContext.getIntervention();

        final double toolPSCi = getToolPSCi(intervention);

        RefScenariosInputPricesByDomainInput scenarioPriceByDomainInput = domainContext.getRefScenariosInputPricesByDomainInput();

        final Double[] irrigActionResult = computeIrrigIndicator(
                writerContext,
                optionalIrrigationAction.get(),
                scenarioPriceByDomainInput);

        final Double[] irrigInterventionResult = mults(irrigActionResult, toolPSCi);

        if (LOGGER.isDebugEnabled()) {
            PracticedSystem practicedSystem = practicedSystemContext.getPracticedSystem();

            LOGGER.debug(
                    String.format(
                            Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_PRACTICED,
                            "computeIrrigationIndicator",
                            intervention.getName(),
                            intervention.getTopiaId(),
                            practicedSystem.getName(),
                            practicedSystem.getCampaigns(),
                            (System.currentTimeMillis() - start)));
        }

        return irrigInterventionResult;

    }

    protected Double[] computeIrrigIndicator(
            WriterContext writerContext,
            IrrigationAction action,
            RefScenariosInputPricesByDomainInput irrigPricesResult) {

        final IrrigationInputUsage irrigationInputUsage = action.getIrrigationInputUsage();
        final DomainIrrigationInput domainIrrigationInput = irrigationInputUsage.getDomainIrrigationInput();

        Double[] inputsCharges = newArray(scenarioCodes.size(), 0d);

        final String actionTopiaId = action.getTopiaId();
        incrementAngGetTotalFieldCounterForTargetedId(actionTopiaId);//WaterQuantityAverage, can not be null

        // exprimé en mm, soit 1mm = 1l/m2 = 10000l/ha = 10m3/ha
        double da = action.getWaterQuantityAverage();

        final Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> abstractDomainInputStockUnitMapMap = irrigPricesResult.irrigRefPriceByScenarioForInput();
        final Map<String, Optional<InputRefPrice>> scenarioPricesByCode = abstractDomainInputStockUnitMapMap.get(domainIrrigationInput);

        for (String scenarioCode : this.scenarioCodes) {
            final Optional<InputRefPrice> optionalScenarioPrice = scenarioPricesByCode.get(scenarioCode);

            double sci;

            if (optionalScenarioPrice.isEmpty()) {
                // #10063 Si aucun prix de référence n’est disponible pour le prix de l’intrant, alors on prend la valeur « 0 » comme référence
                addMissingFieldMessage(actionTopiaId, messageBuilder.getMissingRefScenarioPriceMessage(scenarioCode));
            } else {
                InputRefPrice price = optionalScenarioPrice.get();
                // PA e (€.m -3 ) : prix d’achat de l’eau.
                sci = price.averageRefPrice().value() * da * 10;
                inputsCharges[this.scenarioCodes.indexOf(scenarioCode)] += sci;
            }
        }

        IndicatorWriter writer = writerContext.getWriter();

        String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(action.getTopiaId(), MissingMessageScope.INPUT);
        Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(action.getTopiaId());

        write(
                action,
                action.getIrrigationInputUsage(),
                writerContext,
                inputsCharges,
                writer,
                reliabilityCommentForInputId,
                reliabilityIndexForInputId
        );

        return inputsCharges;
    }

    protected void writeInputUsage(
            WriterContext writerContext,
            AbstractAction action,
            AbstractInputUsage usage,
            Double[] inputResult) {

        IndicatorWriter writer = writerContext.getWriter();

        String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(usage.getTopiaId(), MissingMessageScope.INPUT);
        Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(usage.getTopiaId());

        write(
                action,
                usage,
                writerContext,
                inputResult,
                writer,
                reliabilityCommentForInputId,
                reliabilityIndexForInputId
        );
    }

    private void write(
            AbstractAction action,
            AbstractInputUsage usage,
            WriterContext writerContext,
            Double[] inputResult,
            IndicatorWriter writer,
            String reliabilityCommentForInputId,
            Integer reliabilityIndexForInputId) {

        for (int i = 0; i < inputResult.length; i++) {

            boolean isDisplayed = isDisplayed(ExportLevel.INPUT, i);

            if (isDisplayed) {
                // write input sheet
                writer.writeInputUsage(
                        writerContext.getIts(),
                        writerContext.getIrs(),
                        getIndicatorCategory(),
                        getIndicatorLabel(i),
                        usage,
                        action,
                        writerContext.getEffectiveIntervention(),
                        writerContext.getPracticedIntervention(),
                        writerContext.getCodeAmmBioControle(),
                        writerContext.getAnonymizeDomain(),
                        writerContext.getAnonymizeGrowingSystem(),
                        writerContext.getPlot(),
                        writerContext.getZone(),
                        writerContext.getPracticedSystem(),
                        writerContext.getCroppingPlanEntry(),
                        writerContext.getPracticedPhase(),
                        writerContext.getSolOccupationPercent(),
                        writerContext.getEffectivePhase(),
                        writerContext.getRank(),
                        writerContext.getPreviousPlanEntry(),
                        writerContext.getIntermediateCrop(),
                        this.getClass(),
                        inputResult[i],
                        reliabilityIndexForInputId,
                        reliabilityCommentForInputId,
                        null,
                        null,
                        null,
                        writerContext.getGroupesCiblesByCode());
            }
        }
    }

    public void init(Collection<Scenario> scenarios) {
        if (scenarios != null) {
            for (Scenario scenario : scenarios) {
                scenariosByCode.put(scenario.code(), scenario);
                scenarioCodes.add(scenario.code());
            }
        }
    }
}
