package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * <pre>
 * {@code
 * <ram:AuthorName>ISAGRI</ram:AuthorName>
 * <ram:Identification>ISAGRI!PSQDYU962</ram:Identification>
 * <ram:Name>ISAMARGE</ram:Name>
 * <ram:Version>6.5</ram:Version>
 * <ram:SpecifiedConformanceCertificate>
 * <ram:Identification>AEE565126568685656598965216549686</ram:Identification>
 * <ram:SoftwareOperatingSystem>WinXP</ram:SoftwareOperatingSystem>
 * <ram:Issue>2101-12-17T09:30:47Z</ram:Issue>
 * </ram:SpecifiedConformanceCertificate>
 * }
 * </pre>
 */
public class ProductionSoftware implements AgroEdiObject {

    protected String authorName;

    protected String identification;

    protected String name;

    protected String version;

    protected ConformanceCertificate specifiedConformanceCertificate;

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public ConformanceCertificate getSpecifiedConformanceCertificate() {
        return specifiedConformanceCertificate;
    }

    public void setSpecifiedConformanceCertificate(ConformanceCertificate specifiedConformanceCertificate) {
        this.specifiedConformanceCertificate = specifiedConformanceCertificate;
    }

    @Override
    public String getLocalizedIdentifier() {
        return "logiciel '" + identification + "'";
    }
}
