package fr.inra.agrosyst.services.common.export;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.async.Task;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.services.ServiceContext;
import fr.inra.agrosyst.services.async.AbstractTaskRunner;
import fr.inra.agrosyst.services.common.EmailService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.time.Duration;

/**
 * Runner abtrait pour le traitement des tâche d'export asynchrone puis effectuer la notification par email.
 *
 * @since 2.61
 * @author Arnaud Thimel (Code Lutin)
 */
public abstract class AbstractExportTaskRunner<T extends Task> extends AbstractTaskRunner<T, ExportResult> {

    private static final Log log = LogFactory.getLog(AbstractExportTaskRunner.class);

    @Override
    protected void taskSucceeded(T task, ServiceContext serviceContext, ExportResult result, Duration duration) {
        // Envoi un email de fin de traitement
        String userEmail = task.getUserEmail();
        EmailService emailService = serviceContext.newService(EmailService.class);
        emailService.notifyExportSuccessful(task.getTaskId(), userEmail, task.getDescription(), result);
    }

    @Override
    protected void taskFailed(T task, ServiceContext serviceContext, Exception eee) {
        if (log.isErrorEnabled()) {
            log.error("Impossible de terminer le traitement " + task.getTaskId(), eee);
        }
        String userEmail = task.getUserEmail();
        EmailService emailService = serviceContext.newService(EmailService.class);
        emailService.notifyExportFailure(userEmail, task.getDescription(), task.getTaskId());
    }

}
