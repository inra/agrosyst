package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.MultiValuedMap;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import static org.nuiton.i18n.I18n.l;

public abstract class IndicatorHazardousProductsUses extends AbstractIndicatorQSA {

    protected String[] indicatorLabels;

    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.INPUT,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    private boolean isWithSeedingTreatment = true;
    private boolean isWithoutSeedingTreatment = true;

    @Override
    public Double[] manageIntervention(WriterContext writerContext, PerformanceGlobalExecutionContext globalExecutionContext, PerformancePracticedDomainExecutionContext domainContext, PerformanceGrowingSystemExecutionContext growingSystemContext, PerformancePracticedSystemExecutionContext practicedSystemContext, PerformancePracticedCropExecutionContext cropContext, PerformancePracticedInterventionExecutionContext interventionContext, PracticedCropCyclePhase phase) {
        if (interventionContext.isFictive()) {
            return null;
        }

        double toolPSCi = getToolPSCi(interventionContext.getIntervention());
        MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> allRefPhrasesRisqueEtClassesMentionDangerByAMM = practicedSystemContext.getRefPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM();

        return this.compute(writerContext, toolPSCi, domainContext.getAllDomainSubstancesByAmm(), allRefPhrasesRisqueEtClassesMentionDangerByAMM, interventionContext);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext, PerformanceGlobalExecutionContext globalExecutionContext, PerformanceEffectiveDomainExecutionContext domainContext, PerformanceZoneExecutionContext zoneContext, PerformanceEffectiveCropExecutionContext cropContext, PerformanceEffectiveInterventionExecutionContext interventionContext) {
        double toolPSCi = getToolPSCi(interventionContext.getIntervention());
        MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> allRefPhrasesRisqueEtClassesMentionDangerByAMM = domainContext.getRefPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM();

        return this.compute(writerContext, toolPSCi, domainContext.getAllDomainSubstancesByAmm(), allRefPhrasesRisqueEtClassesMentionDangerByAMM, interventionContext);
    }

    protected Double[] compute(
            WriterContext writerContext,
            final double psci,
            Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> domainSubstancesByAmm,
            MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> allRefPhrasesRisqueEtClassesMentionDangerByAMM,
            PerformanceInterventionContext interventionContext) {
        AtomicReference<Double> result = new AtomicReference<>(0.0);

        interventionContext.getOptionalPesticidesSpreadingAction().ifPresent(pesticidesSpreadingAction -> {
            final double treatedSurface = pesticidesSpreadingAction.getProportionOfTreatedSurface() / 100.0;
            final double phytoPsci = psci * treatedSurface;

            Collection<? extends AbstractPhytoProductInputUsage> pesticideProductInputUsages = pesticidesSpreadingAction.getPesticideProductInputUsages();
            if (pesticideProductInputUsages != null) {
                pesticideProductInputUsages.stream()
                        .filter(u -> u.getDomainPhytoProductInput() != null)
                        .filter(u -> u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                        .forEach(usage -> {
                            final String codeAmm = usage.getDomainPhytoProductInput().getRefInput().getCode_AMM();
                            final boolean hazardousInput = this.isHazardousInput(domainSubstancesByAmm, allRefPhrasesRisqueEtClassesMentionDangerByAMM, codeAmm);

                            final double value = hazardousInput ? phytoPsci : 0.0;
                            result.updateAndGet(currentValue -> currentValue + value);

                            writeInput(writerContext, pesticidesSpreadingAction, usage, false, value);
                        });
            }
        });

        interventionContext.getOptionalBiologicalControlAction().ifPresent(biologicalControlAction -> {
            final double treatedSurface = biologicalControlAction.getProportionOfTreatedSurface() / 100.0;
            final double phytoPsci = psci * treatedSurface;

            Collection<? extends AbstractPhytoProductInputUsage> biologicalProductInputUsages = biologicalControlAction.getBiologicalProductInputUsages();
            if (biologicalProductInputUsages != null) {
                biologicalProductInputUsages.stream()
                        .filter(u -> u.getDomainPhytoProductInput() != null
                                && u.getDomainPhytoProductInput().getRefInput() != null
                                && u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                        .forEach(
                                usage -> {
                                    final String codeAmm = usage.getDomainPhytoProductInput().getRefInput().getCode_AMM();
                                    final boolean hazardousInput = this.isHazardousInput(domainSubstancesByAmm, allRefPhrasesRisqueEtClassesMentionDangerByAMM, codeAmm);

                                    final double value = hazardousInput ? phytoPsci : 0.0;
                                    result.updateAndGet(currentValue -> currentValue + value);

                                    writeInput(writerContext, biologicalControlAction, usage, false, value);
                                }
                        );
            }
        });

        Double indicatorValueWithoutSeedingTreatment = result.get();

        interventionContext.getOptionalSeedingActionUsage().ifPresent(seedingAction -> seedingAction.getSeedLotInputUsage()
                .stream()
                .filter(u -> u != null && u.getSeedingSpecies() != null)
                .flatMap(u -> u.getSeedingSpecies().stream())
                .filter(u -> u.getSeedProductInputUsages() != null)
                .flatMap(u -> u.getSeedProductInputUsages().stream())
                .filter(u -> u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                .forEach(
                        usage -> {
                            final String codeAmm = usage.getDomainPhytoProductInput().getRefInput().getCode_AMM();
                            final boolean hazardousInput = this.isHazardousInput(domainSubstancesByAmm, allRefPhrasesRisqueEtClassesMentionDangerByAMM, codeAmm);

                            final double value = hazardousInput ? psci : 0.0;
                            result.updateAndGet(currentValue -> currentValue + value);

                            writeInput(writerContext, seedingAction, usage, true, value);
                        }
                )
        );

        Double indicatorValueWithSeedingTreatment = result.get();

        return new Double[] {indicatorValueWithSeedingTreatment, indicatorValueWithoutSeedingTreatment};
    }

    /**
     * Selon l'implémetation : indique si l'intrant identifié par le code AMM correspond à une substance
     * dangereuse, c'est-à-dire CMR, Danger environnement ou Toxique Utilisateur.
     */
    protected abstract boolean isHazardousInput(
            Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> domainSubstancesByAmm,
            MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> allRefPhrasesRisqueEtClassesMentionDangerByAMM,
            String codeAmm
    );

    protected void writeInput(
            WriterContext writerContext,
            AbstractAction abstractAction,
            AbstractPhytoProductInputUsage usage,
            boolean seedingTreatment,
            double indicatorValue
    ) {
        IndicatorWriter writer = writerContext.getWriter();
        for (int i = 0; i < this.getIndicatorsLabels().length; i++) {
            double valueToWrite = !seedingTreatment || i == 0 ? indicatorValue : 0.0;

            String indicatorLabel = getIndicatorLabel(i);

            if (isDisplayed(ExportLevel.INPUT, i)) {
                String usageTopiaId = usage.getTopiaId();
                Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(usageTopiaId);
                String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(
                        usageTopiaId,
                        MissingMessageScope.INPUT);
                writer.writeInputUsage(
                        writerContext.getIts(),
                        writerContext.getIrs(),
                        getIndicatorCategory(),
                        indicatorLabel,
                        usage,
                        abstractAction,
                        writerContext.getEffectiveIntervention(),
                        writerContext.getPracticedIntervention(),
                        writerContext.getCodeAmmBioControle(),
                        writerContext.getAnonymizeDomain(),
                        writerContext.getAnonymizeGrowingSystem(),
                        writerContext.getPlot(),
                        writerContext.getZone(),
                        writerContext.getPracticedSystem(),
                        writerContext.getCroppingPlanEntry(),
                        writerContext.getPracticedPhase(),
                        writerContext.getSolOccupationPercent(),
                        writerContext.getEffectivePhase(),
                        writerContext.getRank(),
                        writerContext.getPreviousPlanEntry(),
                        writerContext.getIntermediateCrop(),
                        this.getClass(),
                        valueToWrite,
                        reliabilityIndexForInputId,
                        reliabilityCommentForInputId,
                        "",
                        "",
                        "",
                        writerContext.getGroupesCiblesByCode());
            }
        }
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return this.exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, "Indicator.category.activeSubstances");
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, this.getIndicatorsLabels()[i]);
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) &&
                displayed && (
                i == 0 && isWithSeedingTreatment ||
                        i == 1 && isWithoutSeedingTreatment
        );
    }

    protected String[] getIndicatorsLabels() {
        return this.indicatorLabels;
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
        if (filter != null) {
            isWithSeedingTreatment = filter.getWithSeedingTreatment();
            isWithoutSeedingTreatment = filter.getWithoutSeedingTreatment();
        } else {
            displayed = true;
            isWithSeedingTreatment = true;
            isWithoutSeedingTreatment = true;
        }
    }
}
