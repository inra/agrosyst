package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAA;
import fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAAImpl;
import fr.inra.agrosyst.services.common.CommonService;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class RefCiblesAgrosystGroupesCiblesMAAModel extends AbstractAgrosystModel<RefCiblesAgrosystGroupesCiblesMAA>
                                                    implements ExportModel<RefCiblesAgrosystGroupesCiblesMAA> {

    private final boolean exportAllHeaders;

    public RefCiblesAgrosystGroupesCiblesMAAModel(boolean exportAllHeaders) {
        super(CSV_SEPARATOR);
        this.exportAllHeaders = exportAllHeaders;
        newMandatoryColumn("cible_edi_ref_id", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_ID, STRING_MANDATORY_PARSER);
        newMandatoryColumn("cible_edi_ref_code", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_CODE);
        newMandatoryColumn("cible_edi_ref_label", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_LABEL);
        newMandatoryColumn("code_groupe_cible_maa", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CODE_GROUPE_CIBLE_MAA, STRING_MANDATORY_PARSER);
        newMandatoryColumn("groupe_cible_maa", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_GROUPE_CIBLE_MAA);
        newMandatoryColumn("cible_generique", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_GENERIQUE, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("reference_param", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_REFERENCE_PARAM, MANDATORYAGROSYST_BIO_AGRESSOR_TYPE_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefCiblesAgrosystGroupesCiblesMAA, Object>> getColumnsForExport() {
        ModelBuilder<RefCiblesAgrosystGroupesCiblesMAA> modelBuilder = new ModelBuilder<>();
        if (exportAllHeaders) {
            modelBuilder.newColumnForExport("cible_edi_ref_id", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_ID);
            modelBuilder.newColumnForExport("cible_edi_ref_code", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_CODE);
            modelBuilder.newColumnForExport("cible_edi_ref_label", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_LABEL);
            modelBuilder.newColumnForExport("code_groupe_cible_maa", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CODE_GROUPE_CIBLE_MAA);
            modelBuilder.newColumnForExport("groupe_cible_maa", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_GROUPE_CIBLE_MAA);
            modelBuilder.newColumnForExport("cible_generique", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_GENERIQUE, O_N_FORMATTER);
            modelBuilder.newColumnForExport("reference_param", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_REFERENCE_PARAM, AGROSYST_BIO_AGRESSOR_TYPE_FORMATTER);
            modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_ACTIVE, T_F_FORMATTER);
        } else {
            modelBuilder.newColumnForExport("Catégorie", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_REFERENCE_PARAM, AGROSYST_BIO_AGRESSOR_TYPE_FORMATTER);
            modelBuilder.newColumnForExport("Groupe cible", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_GROUPE_CIBLE_MAA);
            modelBuilder.newColumnForExport("Groupe cible générique", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_GENERIQUE, O_N_FORMATTER);
            modelBuilder.newColumnForExport("Cible du traitement", RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_LABEL);
        }
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefCiblesAgrosystGroupesCiblesMAA newEmptyInstance() {
        RefCiblesAgrosystGroupesCiblesMAA refCiblesAgrosystGroupesCiblesMAA = new RefCiblesAgrosystGroupesCiblesMAAImpl();
        refCiblesAgrosystGroupesCiblesMAA.setActive(true);
        return refCiblesAgrosystGroupesCiblesMAA;
    }
}
