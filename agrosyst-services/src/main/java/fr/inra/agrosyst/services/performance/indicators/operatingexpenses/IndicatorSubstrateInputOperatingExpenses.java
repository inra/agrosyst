package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.PotInputUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherAction;
import fr.inra.agrosyst.api.entities.action.PotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.SubstrateInputUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Les charges opérationnelles réelles sont exprimées en €/ha.
 * Elles correspondent aux dépenses liées à l’achat des intrants (semences et plants,
 * produits fertilisants minéraux  et  organiques, traitement de semences et plants,
 * irrigation, produits phytosanitaires, produits de lutte biologique, autres intrants, substrats, pots).
 * <p>
 * Le calcul se fait sur la base des prix saisis par l’utilisateur.
 * <p>
 * Les interventions concernées par cet indicateur sont toutes les interventions
 * contenant un intrant de type « substrat ».
 * <p>
 *
 * <p>
 * Cette classe est une composante de {@link IndicatorInputProductOperatingExpenses} chargée de calculer les charges relatives aux substrats.
 * </p>
 * Rappel de la formule globale de calcul:
 *
 * <pre>
 * CI réel_i:
 *
 * = PSCi * ( sum(Q_ev * PA_ev) + sum(Q_j * PA_j) + sum(Q_e * PA_e) + sum(Q_a * PA_a)) + PSCi_phyto * sum(Q_k * PA_k) + PSCi_luttebio * sum(Q_h * PA_h)
 *
 * Avec :
 * - CI réel_i : charges intrants de l’intervention i
 * - PSCi (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i.
 *   PSCi est calculé sur la base des données saisies par l’utilisateur.
 *
 * - Autres intrants
 *   - Q_a (diverses unités) : quantité de l'intrant substrat. Donnée saisie par l’utilisateur.
 *   - Q_h (diverses unités) : quantité de l’intrant h, h appartenant à la liste des intrants de type « substrat » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *   - PA_h (diverses unités) : prix d’achat de l’intrant h, h appartenant à la liste des intrants de type « substrat » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *
 *
 * </pre>
 *
 * @author David Cossé (cosse@codelutin.com)
 */
public class IndicatorSubstrateInputOperatingExpenses extends IndicatorInputProductOperatingExpenses {

    protected static final Log LOGGER = LogFactory.getLog(IndicatorSubstrateInputOperatingExpenses.class);
    private static final Double DEFAULT_NB_POTS = 0.0;

    public static final List<SubstrateInputUnit> SUBSTRATE_INPUT_UNIT_BY_POTS = List.of(SubstrateInputUnit.G_POT, SubstrateInputUnit.KG_POT, SubstrateInputUnit.L_POT);

    /**
     * Cas particulier du substrat quand la quantité est exprimée en L_POT, G_POT, KG_POT
     * Le calcul des charges opérationnelles nécessite alors une étape supplémentaire préalable de calcul de la quantité de substrat par unité de surface
     * QTE_SUBSTRAT_PAR_UNITE_SURFACE = QTE_SUBSTRAT x NB_POTS
     * Unité de QTE_SUBSTRAT_PAR_UNITE_SURFACE :
     * ◦ Si QTE_SUBSTRAT en L_POT et NB_POTS en POTS_M2 : L_M2
     * ◦ Si QTE_SUBSTRAT en G_POT et NB_POTS en POTS_M2 : G_M2
     * ◦ Si QTE_SUBSTRAT en KG_POT et NB_POTS en POTS_M2 : KG_M2
     * ◦ Si QTE_SUBSTRAT en L_POT et NB_POTS en POTS_HA : L_HA
     * ◦ Si QTE_SUBSTRAT en G_POT et NB_POTS en POTS_HA : G_HA
     * ◦ Si QTE_SUBSTRAT en KG_POT et NB_POTS en POTS_HA : KG_HA
     * <p>
     * Si QTE_SUBSTRAT est exprimé dans une autre unité (sans référence aux pots), alors
     * QTE_SUBSTRAT_PAR_UNITE_SURFACE = QTE_SUBSTRAT
     */
    public static Set<SubstrateInputUnit> getSubstrateInputUnits(Collection<DomainSubstrateInput> domainSubstrateInputs) {

        Set<SubstrateInputUnit> result = domainSubstrateInputs.stream()
                .map(DomainSubstrateInput::getUsageUnit)
                .filter(Objects::nonNull).collect(Collectors.toSet());

        result.addAll(Arrays.asList(SubstrateInputUnit.values()));

        return result;
    }

    public IndicatorSubstrateInputOperatingExpenses(boolean displayed, boolean computeReal, boolean computStandardised, Locale locale) {
        super(displayed, computeReal, computStandardised, locale);
    }

    @Override
    public Pair<Optional<InputPrice>, Optional<InputRefPrice>> getDomainInputPrice(AbstractInputUsage usage, RefCampaignsInputPricesByDomainInput refCampaignsInputPricesByDomainInputAndCampaigns) {
        final DomainSubstrateInput input = ((SubstrateInputUsage) usage).getDomainSubstrateInput();
        InputPrice inputPrice = input.getInputPrice();
        Optional<InputRefPrice> inputRePrice = refCampaignsInputPricesByDomainInputAndCampaigns.substrateRefPriceForInput().get(input);
        inputRePrice = inputRePrice == null ? Optional.empty() : inputRePrice;
        return Pair.of(Optional.ofNullable(inputPrice), inputRePrice);
    }

    // practiced
    protected Double[] computeOperatingExpenses(
            WriterContext writerContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> substrateProductConverters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        long start = System.currentTimeMillis();

        PracticedSystem practicedSystem = practicedSystemContext.getPracticedSystem();

        final Optional<List<OtherAction>> optionalOtherActions = interventionContext.getOptionalOtherActions();

        final Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();
        if (optionalOtherActions.isEmpty() && optionalSeedingActionUsage.isEmpty()) {
            return new Double[]{
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};
        }

        PracticedIntervention intervention = interventionContext.getIntervention();
        final String interventionId = intervention.getTopiaId();
        double psci = getToolPSCi(intervention);

        Double[] result = new Double[]{
            IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};

        final RefCampaignsInputPricesByDomainInput refInputPricesForCampaignsByInput = practicedSystemContext.getPracticedSystemInputRefPricesByDomainInput();

        if (optionalOtherActions.isPresent()) {

            List<OtherAction> otherActions = optionalOtherActions.get();
            for (OtherAction otherAction : otherActions) {

                Collection<SubstrateInputUsage> substrateInputUsages = otherAction.getSubstrateInputUsages();
                Collection<PotInputUsage> potInputUsages = otherAction.getPotInputUsages();

                Double[] r0 =  computeForActionUsages(
                        writerContext,
                        refInputPricesForCampaignsByInput,
                        substrateProductConverters,
                        indicatorClass,
                        labels,
                        substrateInputUsages,
                        potInputUsages,
                        otherAction,
                        psci,
                        interventionId);

                result = GenericIndicator.sum(result ,r0);
            }
        }

        if (optionalSeedingActionUsage.isPresent()) {
            SeedingActionUsage seedingActionUsage = optionalSeedingActionUsage.get();

            Collection<SubstrateInputUsage> substrateInputUsages = seedingActionUsage.getSubstrateInputUsage();

            Double[] r0 =  computeForActionUsages(
                    writerContext,
                    refInputPricesForCampaignsByInput,
                    substrateProductConverters,
                    indicatorClass,
                    labels,
                    substrateInputUsages,
                    null,
                    seedingActionUsage,
                    psci,
                    interventionId);

            result = GenericIndicator.sum(result ,r0);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format(
                    Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_PRACTICED,
                    "computeSubstrateIndicator",
                    intervention.getName(),
                    intervention.getTopiaId(),
                    practicedSystem.getName(),
                    practicedSystem.getCampaigns(),
                    System.currentTimeMillis() - start));
        }

        return result;
    }

    private Double @NotNull [] computeForActionUsages(
            WriterContext writerContext,
            final RefCampaignsInputPricesByDomainInput refInputPricesForCampaignsByInput,
            Collection<RefInputUnitPriceUnitConverter> substrateProductConverters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels,
            Collection<SubstrateInputUsage> substrateInputUsages,
            Collection<PotInputUsage> potInputUsages,
            AbstractAction action,
            double psci,
            String interventionId) {

        if (CollectionUtils.isEmpty(substrateInputUsages)) {
            // no phyto product cost
            Double[] r0 = {
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};
            return r0;
        }

        Pair<Double, Double> inputsCharges = computeIndicatorForSubstrateInputs(
                writerContext,
                refInputPricesForCampaignsByInput,
                substrateInputUsages,
                action,
                potInputUsages,
                psci,
                interventionId,
                substrateProductConverters,
                indicatorClass,
                labels);

        Double[] r0 = {inputsCharges.getLeft(), inputsCharges.getRight()};
        return r0;
    }

    // effective
    protected Double[] computeOperatingExpenses(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainExecutionContext,
            Zone zone,
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> substrateConverters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        long start = System.currentTimeMillis();

        EffectiveIntervention intervention = interventionContext.getIntervention();
        final String interventionId = intervention.getTopiaId();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(
                    "computeSubstrateIndicator intervention:" +
                            intervention.getName() + " (" + intervention.getTopiaId() + ")" +
                            ", zone:" + zone.getName() +
                            " ) calculé en :" + (System.currentTimeMillis() - start) + "ms");
        }
        final Optional<List<OtherAction>> optionalOtherAction_ = interventionContext.getOptionalOtherActions();
        final Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();

        if (optionalOtherAction_.isEmpty() && optionalSeedingActionUsage.isEmpty()) {
            // no product cost
            return new Double[]{
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};
        }

        double psci = getToolPSCi(intervention);

        Double[] result = new Double[]{
                IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};

        final RefCampaignsInputPricesByDomainInput refInputPricesForCampaignsByInput = domainExecutionContext.getRefInputPricesForCampaignsByInput();

        if (optionalOtherAction_.isPresent()) {

            List<OtherAction> otherActions = optionalOtherAction_.get();

            for (OtherAction otherAction : otherActions) {
                Collection<SubstrateInputUsage> substrateInputUsages = otherAction.getSubstrateInputUsages();
                Collection<PotInputUsage> potInputUsages = otherAction.getPotInputUsages();

                Double[] r0 = computeForActionUsages(
                        writerContext,
                        refInputPricesForCampaignsByInput,
                        substrateConverters,
                        indicatorClass,
                        labels,
                        substrateInputUsages,
                        potInputUsages,
                        otherAction,
                        psci,
                        interventionId);

                result = GenericIndicator.sum(result, r0);
            }
        }

        if (optionalSeedingActionUsage.isPresent()) {
            SeedingActionUsage seedingActionUsage = optionalSeedingActionUsage.get();

            Collection<SubstrateInputUsage> substrateInputUsages = seedingActionUsage.getSubstrateInputUsage();

            Double[] r0 =  computeForActionUsages(
                    writerContext,
                    refInputPricesForCampaignsByInput,
                    substrateConverters,
                    indicatorClass,
                    labels,
                    substrateInputUsages,
                    null,
                    seedingActionUsage,
                    psci,
                    interventionId);

            result = GenericIndicator.sum(result ,r0);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(
                    "computeOrganicProductIndicator intervention:" +
                            intervention.getName() + " (" + intervention.getTopiaId() + ")" +
                            ", zone:" + zone.getTopiaId() +
                            " calculé en :" + (System.currentTimeMillis() - start));
        }

        return result;
    }

    /**
     * Cas particulier du substrat quand la quantité est exprimée en L_POT, G_POT, KG_POT
     * Le calcul des charges opérationnelles nécessite alors une étape supplémentaire préalable de calcul de la quantité de substrat par unité de surface
     * QTE_SUBSTRAT_PAR_UNITE_SURFACE = QTE_SUBSTRAT x NB_POTS
     * Unité de QTE_SUBSTRAT_PAR_UNITE_SURFACE :
     * ◦ Si QTE_SUBSTRAT en L_POT et NB_POTS en POTS_M2 : L_M2
     * ◦ Si QTE_SUBSTRAT en G_POT et NB_POTS en POTS_M2 : G_M2
     * ◦ Si QTE_SUBSTRAT en KG_POT et NB_POTS en POTS_M2 : KG_M2
     * ◦ Si QTE_SUBSTRAT en L_POT et NB_POTS en POTS_HA : L_HA
     * ◦ Si QTE_SUBSTRAT en G_POT et NB_POTS en POTS_HA : G_HA
     * ◦ Si QTE_SUBSTRAT en KG_POT et NB_POTS en POTS_HA : KG_HA
     * <p>
     * Si QTE_SUBSTRAT est exprimé dans une autre unité (sans référence aux pots), alors
     * QTE_SUBSTRAT_PAR_UNITE_SURFACE = QTE_SUBSTRAT
     */
    protected Pair<Double, Double> computeIndicatorForSubstrateInputs(
            WriterContext writerContext,
            RefCampaignsInputPricesByDomainInput refInputPricesForCampaignsByInput,
            Collection<SubstrateInputUsage> substrateInputUsages,
            AbstractAction action,
            Collection<PotInputUsage> potsInputUsages,
            double psci,
            String interventionId,
            Collection<RefInputUnitPriceUnitConverter> converters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        Pair<Double, Double> inputsCharges;

        double interventionRealCi = 0d;
        double interventionStandardizedPriceCi = 0d;


        for (SubstrateInputUsage usage : substrateInputUsages) {

            Collection<RefInputUnitPriceUnitConverter> inputUsageConversionRates = getInputUsageConversionRate(usage, converters);

            Double da = usage.getQtAvg();//qté de substrat

            final DomainSubstrateInput domainSubstrateInput = usage.getDomainSubstrateInput();
            SubstrateInputUnit substrateInputUnit = domainSubstrateInput.getUsageUnit();

            if (substrateInputUnit == null && da != null) {
                addMissingFieldMessage(interventionId, messageBuilder.getMissingDoseUnitMessage(usage.getInputType()));
            }

            addMissingDeprecatedInputQtyOrUnitIfRequired(usage);

            if (substrateInputUnit != null && SUBSTRATE_INPUT_UNIT_BY_POTS.contains(substrateInputUnit)) {
                // nb pots
                incrementAngGetTotalFieldCounterForTargetedId(interventionId);

                double nbPotM2 = CollectionUtils.isEmpty(potsInputUsages) ?
                        DEFAULT_NB_POTS :
                        potsInputUsages.stream()
                                .filter(potInput -> PotInputUnit.POTS_M2.equals(potInput.getDomainPotInput().getUsageUnit()))
                                .mapToDouble(AbstractInputUsage::getQtAvg).sum();

                if (nbPotM2 == 0) {
                    addMissingFieldMessage(interventionId, messageBuilder.getMissingInputPotMessage());
                }

                if (da == null) {
                    // #10124 - si le champ 'dose appliquée' n'est pas renseignée pour un intrant de type 'produit phytosanitaire',
                    // alors la valeur par défaut est la dose de référence pour ce produit phytosanitaire sur la culture concernée
                    // (ce qui donne un IFT de 1, modulo PSCI);
                    //        - si Agrosyst ne trouve pas de dose de référence pour ce produit phytosanitaire sur la culture concernée,
                    //        alors l'IFT associée à l'intrant est 1*PSCI (comme c'est déjà le cas)
                    da = Indicator.DEFAULT_IFT;
                    addMissingFieldMessage(interventionId, messageBuilder.getMissingDoseMessage(usage.getInputType(), MissingMessageScope.INPUT));
                }

                da *= nbPotM2;

            }

            // da
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            if (da == null) {
                // #10124 - si le champ 'dose appliquée' n'est pas renseignée pour un intrant de type 'produit phytosanitaire',
                // alors la valeur par défaut est la dose de référence pour ce produit phytosanitaire sur la culture concernée
                // (ce qui donne un IFT de 1, modulo PSCI);
                //        - si Agrosyst ne trouve pas de dose de référence pour ce produit phytosanitaire sur la culture concernée,
                //        alors l'IFT associée à l'intrant est 1*PSCI (comme c'est déjà le cas)
                da = Indicator.DEFAULT_IFT;
                addMissingFieldMessage(interventionId, messageBuilder.getMissingDoseMessage(usage.getInputType(), MissingMessageScope.INPUT));
            }

            InputPrice priceForInput = domainSubstrateInput.getInputPrice();

            // #10063 Si aucun prix de référence n’est disponible pour le prix de l’intrant, alors on prend la valeur « 0 » comme référence.
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            final String inputTopiaId = usage.getTopiaId();
            if (priceForInput == null) {
                addMissingFieldMessage(
                        inputTopiaId,
                        messageBuilder.getMissingInputUsagePriceMessage(
                                usage.getInputType(),
                                usage,
                                action));
                continue;
            }

            Double price = priceForInput.getPrice();
            PriceUnit priceUnit = priceForInput.getPriceUnit();

            if (price == null) {
                addMissingFieldMessage(
                        inputTopiaId,
                        messageBuilder.getMissingInputUsagePriceMessage(
                                usage.getInputType(),
                                usage,
                                action));
            }

            //Si aucun prix n’est saisi par l’utilisateur, on prendra le prix de référence de l’intrant pour le calcul de
            //l’indicateur. Cf. bien se référer pour l’identification de ces prix de ref à la SPEC des charges
            //opérationnelles standardisées. Car l’identification des prix de référence est spécifique à chaque type
            //d’intrant.
            final Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> abstractDomainInputStockUnitOptionalMap = refInputPricesForCampaignsByInput.substrateRefPriceForInput();
            Optional<InputRefPrice> optionalRefPrice = abstractDomainInputStockUnitOptionalMap.get(domainSubstrateInput);

            double inputStandardizedPriceCi = 0d;
            double inputRealCi;

            if (optionalRefPrice != null && optionalRefPrice.isPresent()) {
                final InputRefPrice rePrice = optionalRefPrice.get();
                double standardizedPrice = rePrice.averageRefPrice().value();
                PriceUnit standardizedPriceUnit = rePrice.averageRefPrice().unit();

                //L’utilisateur peut saisir un prix en €/ha pour les intrants. Dans ce cas-là, on considère que la dose
                //d’application de l’intrant vaut 1.
                inputStandardizedPriceCi = IndicatorOperatingExpenses.computeCi(da, standardizedPrice, standardizedPriceUnit, 1);// conversionRate already applied to refPrices
                interventionStandardizedPriceCi += inputStandardizedPriceCi;
            }

            double converter = 0;

            if (priceUnit != null) {
                final PriceUnit priceUnit0 = priceUnit;
                converter = inputUsageConversionRates.stream()
                        .filter(cr -> priceUnit0.equals(cr.getPriceUnit()))
                        .findAny()
                        .map(RefInputUnitPriceUnitConverter::getConvertionRate)
                        .orElse(0d);
            }

            if (converter == 0) {
                getMissingInputUsageConversionRateMessage(usage, priceUnit, interventionId);
            }

            // #10063 Si aucun prix de référence n’est disponible pour le prix de l’intrant, alors on prend la valeur « 0 » comme référence.
            if (price != null) {
                //L’utilisateur peut saisir un prix en €/ha pour les intrants. Dans ce cas-là, on considère que la dose
                //d’application de l’intrant vaut 1.
                inputRealCi = IndicatorOperatingExpenses.computeCi(da, price, priceUnit, converter);
            } else {
                inputRealCi = inputStandardizedPriceCi;
            }

            interventionRealCi += inputRealCi;

            Double[] inputResult = new Double[]{inputRealCi, inputStandardizedPriceCi};
            writeInputUsage(action, usage, writerContext, inputResult, indicatorClass, labels);
        }

        inputsCharges = Pair.of(psci * interventionRealCi, psci * interventionStandardizedPriceCi);

        return inputsCharges;
    }

}
