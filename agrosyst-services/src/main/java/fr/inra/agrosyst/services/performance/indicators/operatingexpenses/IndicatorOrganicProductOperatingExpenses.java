package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Optional;

/**
 * Les charges opérationnelles réelles sont exprimées en €/ha.
 * Elles correspondent aux dépenses liées à l’achat des intrants (semences et plants,
 * produits fertilisants minéraux  et  organiques, traitement de semences et plants,
 * irrigation, produits phytosanitaires, produits de lutte biologique, autres intrants, substrats, pots).
 * <p>
 * Le calcul se fait sur la base des prix saisis par l’utilisateur.
 * <p>
 * Les interventions concernées par cet indicateur sont toutes les interventions
 * contenant un intrant et/ou une action de type « Semis » et/ou une action de
 * type « Irrigation ».
 * <p>
 *
 * <p>
 * Cette classe est une composante de {@link IndicatorOperatingExpenses} chargée de calculer les charges relatives au semis.
 * </p>
 * Rappel de la formule globale de calcul:
 *
 * <pre>
 * CI réel_i:
 *
 * = PSCi * ( sum(Q_ev * PA_ev) + sum(Q_j * PA_j) + sum(Q_e * PA_e) + sum(Q_a * PA_a)) + PSCi_phyto * sum(Q_k * PA_k) + PSCi_luttebio * sum(Q_h * PA_h)
 *
 * Avec :
 * - CI réel_i : charges intrants de l’intervention i
 * - PSCi (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i.
 *   PSCi est calculé sur la base des données saisies par l’utilisateur.
 *
 * - Lutte biologique
 *   - PSCi_luttebio (sans unité) : proportion de surface concernée par l’action de type lutte biologique.
 *   - Q_h (diverses unités) : quantité de l’intrant h, h appartenant à la liste des intrants de type « Lutte biologique »
 *     appliqués au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *   - PA_h (diverses unités) : prix d’achat de l’intrant h, h appartenant à la liste des intrants de type « Lutte biologique »
 *     appliqués au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *
 *
 * </pre>
 *
 * @author David Cossé (cosse@codelutin.com)
 */
public class IndicatorOrganicProductOperatingExpenses extends IndicatorInputProductOperatingExpenses {

    protected static final Log LOGGER = LogFactory.getLog(IndicatorOrganicProductOperatingExpenses.class);

    public IndicatorOrganicProductOperatingExpenses(
            boolean displayed, boolean computeReal, boolean computStandardised, Locale locale) {
        super(displayed, computeReal, computStandardised, locale);
    }

    @Override
    public Pair<Optional<InputPrice>, Optional<InputRefPrice>> getDomainInputPrice(AbstractInputUsage usage, RefCampaignsInputPricesByDomainInput refCampaignsInputPricesByDomainInputAndCampaigns) {
        final DomainOrganicProductInput input = ((OrganicProductInputUsage) usage).getDomainOrganicProductInput();
        final InputPrice inputPrice = input.getInputPrice();
        Optional<InputRefPrice> inputRePrice = refCampaignsInputPricesByDomainInputAndCampaigns.fertiOrgaRefPriceForInput().get(input);
        inputRePrice = inputRePrice == null ? Optional.empty() : inputRePrice;
        return Pair.of(Optional.ofNullable(inputPrice), inputRePrice);
    }

    // practiced
    protected Double[] computeOperatingExpenses(
            WriterContext writerContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> organicProductConverters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        long start = System.currentTimeMillis();
        PracticedSystem practicedSystem = practicedSystemContext.getPracticedSystem();

        final Optional<OrganicFertilizersSpreadingAction> optionalAction = interventionContext.getOptionalOrganicFertilizersSpreadingAction();
        if (optionalAction.isEmpty()) {
            // no phyto product cost
            Double[] result = {
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};
            return result;
        }
        final OrganicFertilizersSpreadingAction action = optionalAction.get();
        Collection<OrganicProductInputUsage> inputUsages = action.getOrganicProductInputUsages();

        if (CollectionUtils.isEmpty(inputUsages)) {
            // no phyto product cost
            Double[] result = {
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};
            return result;
        }

        PracticedIntervention intervention = interventionContext.getIntervention();
        // inputsCharges for price, inputsCharges for refprice

        double psci = getToolPSCi(intervention);

        Pair<Double, Double> inputsCharges = computeIndicatorForAction(
                writerContext,
                action,
                inputUsages,
                practicedSystemContext.getPracticedSystemInputRefPricesByDomainInput(),
                psci,
                organicProductConverters, intervention.getTopiaId(),
                indicatorClass,
                labels,
                new HashMap<>());

        Double[] result = {inputsCharges.getLeft(), inputsCharges.getRight()};

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format(
                    Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_PRACTICED,
                    "computeOrganicProductIndicator",
                    intervention.getName(),
                    intervention.getTopiaId(),
                    practicedSystem.getName(),
                    practicedSystem.getCampaigns(),
                    System.currentTimeMillis() - start));
        }

        return result;
    }

    // effective
    protected Double[] computeOperatingExpenses(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            Zone zone,
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> organicConverters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        long start = System.currentTimeMillis();

        final Optional<OrganicFertilizersSpreadingAction> optionalAction = interventionContext.getOptionalOrganicFertilizersSpreadingAction();
        if (optionalAction.isEmpty()) {
            // no phyto product cost
            Double[] result = {
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};
            return result;
        }
        final OrganicFertilizersSpreadingAction action = optionalAction.get();
        Collection<OrganicProductInputUsage> inputUsages = action.getOrganicProductInputUsages();

        if (CollectionUtils.isEmpty(inputUsages)) {
            // no phyto product cost
            Double[] result = {
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                    IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};
            return result;
        }

        EffectiveIntervention intervention = interventionContext.getIntervention();

        double psci = getToolPSCi(intervention);

        Pair<Double, Double> inputsCharges = computeIndicatorForAction(
                writerContext,
                action,
                inputUsages,
                domainContext.getRefInputPricesForCampaignsByInput(),
                psci,
                organicConverters,
                intervention.getTopiaId(),
                indicatorClass,
                labels,
                new HashMap<>());

        Double[] result = {inputsCharges.getLeft(), inputsCharges.getRight()};

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(
                    "computeOrganicProductIndicator intervention:" +
                            intervention.getName() + " (" + intervention.getTopiaId() + ")" +
                            ", zone:" + zone.getTopiaId() +
                            " calculé en :" + (System.currentTimeMillis() - start));
        }

        return result;
    }


}
