package fr.inra.agrosyst.services.plot.export;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.BufferStrip;
import fr.inra.agrosyst.api.entities.FrostProtectionType;
import fr.inra.agrosyst.api.entities.HosesPositionning;
import fr.inra.agrosyst.api.entities.IrrigationSystemType;
import fr.inra.agrosyst.api.entities.MaxSlope;
import fr.inra.agrosyst.api.entities.PompEngineType;
import fr.inra.agrosyst.api.entities.SolWaterPh;
import fr.inra.agrosyst.api.entities.WaterFlowDistance;
import fr.inra.agrosyst.api.entities.ZoneType;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinageTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigoTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppaTopiaDao;
import fr.inra.agrosyst.services.common.export.ExportModel;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class PlotExportModels {

    @Getter
    public static class CommonBean {

        final String plotName;
        final String domainName;
        final Integer campaign;

        /**
         * Constructeur avec tous les champs pour la première instanciation
         */
        public CommonBean(String plotName,
                          String domainName,
                          Integer campaign) {
            this.plotName = plotName;
            this.domainName = domainName;
            this.campaign = campaign;
        }

        /**
         * Constructeur par recopie pour faciliter le travail des sous modèles
         */
        public CommonBean(CommonBean source) {
            this(
                    source.plotName,
                    source.domainName,
                    source.campaign
            );
        }

    }

    public abstract static class CommonModel<T extends CommonBean> extends ExportModel<T> {
        public CommonModel() {
            newColumn("Parcelle", CommonBean::getPlotName);
            newColumn("Domaine", CommonBean::getDomainName);
            newColumn("Campagne", CommonBean::getCampaign);
        }
    }

    @Getter
    @Setter
    public static class MainBean extends CommonBean {

        String codePostal;
        String commune;
        String growingSystemName;
        Double area;
        Double distanceToHQ;
        Integer pacIlotNumber;
        MaxSlope maxSlope;
        WaterFlowDistance waterFlowDistance;
        BufferStrip bufferStrip;
        Double latitude;
        Double longitude;
        String comment;
        String activityEndComment;
        String zoningComment;
        String adjacentComment;

        public MainBean(CommonBean source) {
            super(source);
        }

    }

    /**
     * Current bean info.
     */
    public static class MainModel extends CommonModel<MainBean> {

        @Override
        public String getTitle() {
            return "Généralités";
        }

        public MainModel() {
            newColumn("Commune (Code Postal)", MainBean::getCodePostal);
            newColumn("Commune (Nom)", MainBean::getCommune);
            newColumn("Système De Culture", MainBean::getGrowingSystemName);
            newColumn("Surface totale", MainBean::getArea);
            newColumn("Distance au siège d'exploitation (km)", MainBean::getDistanceToHQ);
            newColumn("Numéro d'ilôt PAC", MainBean::getPacIlotNumber);
            newColumn("Pente maxi", MaxSlope.class, MainBean::getMaxSlope);
            newColumn("Distance à un cours d'eau", WaterFlowDistance.class, MainBean::getWaterFlowDistance);
            newColumn("Bande enherbée", BufferStrip.class, MainBean::getBufferStrip);
            newColumn("Latitude GPS du centre de la parcelle", MainBean::getLatitude);
            newColumn("Longitude GPS du centre de la parcelle", MainBean::getLongitude);
            newColumn("Commentaire sur la parcelle", MainBean::getComment);
            newColumn("Motif de fin d'utilisation", MainBean::getActivityEndComment);
            // zonage
            newColumn("Commentaire (zonage)", MainBean::getZoningComment);
            newColumn("Commentaire (Éléments de voisinage)", MainBean::getAdjacentComment);
        }
    }

    @Getter
    @Setter
    public static class ZoningBean extends CommonBean {

        boolean outOfZoning;
        String zonage;

        public ZoningBean(CommonBean source) {
            super(source);
        }

    }

    public static class ZoningModel extends CommonModel<ZoningBean> {

        @Override
        public String getTitle() {
            return "Zonage";
        }

        public ZoningModel() {
            newColumn("Déclarer la parcelle hors de tout zonage", ZoningBean::isOutOfZoning);
            newColumn("Zonage", ZoningBean::getZonage);
        }
    }

    @Getter
    @Setter
    public static class EquipmentBean extends CommonBean {

        boolean irrigationSystem;
        IrrigationSystemType irrigationSystemType;
        PompEngineType pompEngineType;
        HosesPositionning hosesPositionning;
        boolean fertigationSystem;
        boolean drainage;
        String waterOrigin;
        Integer drainageYear;
        boolean frostProtection;
        FrostProtectionType frostProtectionType;
        boolean hailProtection;
        boolean rainproofProtection;
        boolean pestProtection;
        String otherEquipment;
        String equipmentComment;

        public EquipmentBean(CommonBean source) {
            super(source);
        }

    }

    public static class EquipmentModel extends CommonModel<EquipmentBean> {

        @Override
        public String getTitle() {
            return "Equipements";
        }

        public EquipmentModel() {
            newColumn("Système d'irrigation", EquipmentBean::isIrrigationSystem);
            newColumn("Type de système d'irrigation", IrrigationSystemType.class, EquipmentBean::getIrrigationSystemType);
            newColumn("Type de moteur de pompe", PompEngineType.class, EquipmentBean::getPompEngineType);
            newColumn("Positionnement des tuyaux d'arrosage", HosesPositionning.class, EquipmentBean::getHosesPositionning);
            newColumn("Système de fertirrigation", EquipmentBean::isFertigationSystem);
            newColumn("Drainage", EquipmentBean::isDrainage);
            newColumn("Origine de l'eau", EquipmentBean::getWaterOrigin);
            newColumn("Année de réalisation du drainage", EquipmentBean::getDrainageYear);
            newColumn("Protection anti-gel", EquipmentBean::isFrostProtection);
            newColumn("Type de protection anti-gel", FrostProtectionType.class, EquipmentBean::getFrostProtectionType);
            newColumn("Protection anti-grêle", EquipmentBean::isHailProtection);
            newColumn("Protection anti-pluie", EquipmentBean::isRainproofProtection);
            newColumn("Protection anti-insectes", EquipmentBean::isPestProtection);
            newColumn("Autre équipement", EquipmentBean::getOtherEquipment);
            newColumn("Commentaire sur l’équipement de la parcelle", EquipmentBean::getEquipmentComment);
        }
    }

    @Getter
    @Setter
    public static class GroundBean extends CommonBean {

        String groundName;
        String surfaceTexture;
        String subSoilTexture;
        Double solStoniness;
        String solDepth;
        Integer solMaxDepth;
        Double solOrganicMaterialPercent;
        boolean solBattance;
        SolWaterPh solWaterPh;
        boolean solHydromorphisms;
        boolean solLimestone;
        Double solActiveLimestone;
        Double solTotalLimestone;
        String solComment;

        public GroundBean(CommonBean source) {
            super(source);
        }

    }

    public static class GroundModel extends CommonModel<GroundBean> {

        @Override
        public String getTitle() {
            return "Sols";
        }

        public GroundModel(RefSolTextureGeppaTopiaDao refSolTextureGeppaDao, RefSolProfondeurIndigoTopiaDao refSolProfondeurIndigoDao) {
            newColumn("Sol du domaine", GroundBean::getGroundName);
            Iterable<String> textureValues = refSolTextureGeppaDao.findPropertyDistinctValues(RefSolTextureGeppa.PROPERTY_CLASSES_TEXTURALES__GEPAA);
            newColumn("Texture moyenne de la surface", textureValues, GroundBean::getSurfaceTexture);
            newColumn("Texture moyenne du sous-sol", textureValues, GroundBean::getSubSoilTexture);
            newColumn("Pierrosité moyenne", GroundBean::getSolStoniness);
            Iterable<String> solDepthValues = refSolProfondeurIndigoDao.findPropertyDistinctValues(RefSolProfondeurIndigo.PROPERTY_LIBELLE_CLASSE);
            newColumn("Classe profondeur maxi enracinement", solDepthValues, GroundBean::getSolDepth);
            newColumn("Profondeur maxi enracinement", GroundBean::getSolMaxDepth);
            // c'est chaud : columns.put("reserveUtile", "Réserve utile", GroundBean::);
            newColumn("Pourcentage Matière Organique", GroundBean::getSolOrganicMaterialPercent);
            newColumn("Battance", GroundBean::isSolBattance);
            newColumn("PH eau", SolWaterPh.class, GroundBean::getSolWaterPh);
            newColumn("Hydromorphie", GroundBean::isSolHydromorphisms);
            newColumn("Calcaire", GroundBean::isSolLimestone);
            newColumn("Proportion calcaire actif", GroundBean::getSolActiveLimestone);
            newColumn("Proportion calcaire total", GroundBean::getSolTotalLimestone);
            newColumn("Commentaires", GroundBean::getSolComment);
        }
    }

    @Getter
    @Setter
    public static class GroundHorizonBean extends CommonBean {

        Double lowRating;
        Double stoniness;
        String solTexture;
        String comment;

        public GroundHorizonBean(CommonBean source) {
            super(source);
        }

    }

    public static class GroundHorizonModel extends CommonModel<GroundHorizonBean> {

        @Override
        public String getTitle() {
            return "Sols horizons";
        }

        public GroundHorizonModel() {
            newColumn("Cote basse (cm)", GroundHorizonBean::getLowRating);
            newColumn("Pierrosité horizon (%)", GroundHorizonBean::getStoniness);
            newColumn("Texture horizon", GroundHorizonBean::getSolTexture);
            newColumn("Commentaire horizon", GroundHorizonBean::getComment);
        }
    }

    @Getter
    @Setter
    public static class ZoneBean extends CommonBean {

        String zoneName;
        double area;
        Double latitude;
        Double longitude;
        String comment;
        ZoneType type;

        public ZoneBean(CommonBean source) {
            super(source);
        }

    }

    public static class ZoneModel extends CommonModel<ZoneBean> {

        @Override
        public String getTitle() {
            return "Zones";
        }

        public ZoneModel() {
            newColumn("Nom de la zone", ZoneBean::getZoneName);
            newColumn("Type", ZoneType.class, ZoneBean::getType);
            newColumn("Surface (ha)", ZoneBean::getArea);
            newColumn("Latitude du centre de la zone", ZoneBean::getLatitude);
            newColumn("Longitude du centre de la zone", ZoneBean::getLongitude);
            newColumn("Commentaire", ZoneBean::getComment);
        }
    }

    @Getter
    @Setter
    public static class AdjacentBean extends CommonBean {

        String adjacentElements;

        public AdjacentBean(CommonBean source) {
            super(source);
        }

    }

    public static class AdjacentModel extends CommonModel<AdjacentBean> {

        @Override
        public String getTitle() {
            return "Éléments de voisinage";
        }

        public AdjacentModel(RefElementVoisinageTopiaDao refElementVoisinageDao) {
            Iterable<String> values = refElementVoisinageDao.findPropertyDistinctValues(RefElementVoisinage.PROPERTY_IAE_NOM);
            newColumn("Éléments de voisinage", values, AdjacentBean::getAdjacentElements);
        }
    }

}
