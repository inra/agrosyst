package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.UsagePerformanceResult;
import fr.inra.agrosyst.api.services.performance.utils.PhytoProductUnitConverter;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorRefMaxYearTargetIFT;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

@Setter
public abstract class AbstractIndicatorQSA extends AbstractIndicator {

    protected Map<String, Double> conversionRatesByUnit;

    protected double computeAmountUsedInKgHa(
            final @NotNull AbstractAction action,
            final @NotNull AbstractPhytoProductInputUsage usage,
            final Double phytoPsci,
            final ReferenceDoseDTO refDose) {

        final PhytoProductUnit unitDoseProduct = usage.getDomainPhytoProductInput().getUsageUnit();
        boolean isHlUnitDoseProduct = IndicatorRefMaxYearTargetIFT.IS_HL_PHYTO_UNIT.apply(unitDoseProduct);

        if (usage.getQtAvg() != null) {

            Double qtAvg = usage.getQtAvg();

            if (isHlUnitDoseProduct) {
                // l/hl * volbouillie (hl/ha) = l/ha
                // l/ha = kg/ha via le référentiel de conversion QSA
                // TODO améliorer la prise en charge des unités autres que l/hl
                if (PhytoProductUnit.G_HL == unitDoseProduct) {
                    qtAvg = qtAvg * 0.001;
                }
                final double volBouillie_hl_ha = computeBoiledQuantityFactor(action, unitDoseProduct);
                double dose_l_ha = qtAvg * volBouillie_hl_ha * phytoPsci;
                return dose_l_ha;
            } else {
                final double coef = getInputCoefToKgHa(usage);
                return qtAvg * coef * phytoPsci;
            }

        } else if (refDose != null) {
            final Double coef = getCoefToKgHa(refDose.getUnit());
            final Double qtAvg = refDose.getValue();
            final double boiledQuantityFactor;
            if (isHlUnitDoseProduct) {
                boiledQuantityFactor = computeBoiledQuantityFactor(action, refDose.getUnit());
            } else {
                boiledQuantityFactor = 1.0;
            }
            return qtAvg != null ? (qtAvg * coef * phytoPsci * boiledQuantityFactor) : 0.0d;
        }

        return 0.0d;
    }

    protected double getInputCoefToKgHa(@NotNull AbstractPhytoProductInputUsage usage) {
        final PhytoProductUnit usageUnit = usage.getDomainPhytoProductInput().getUsageUnit();
        return getCoefToKgHa(usageUnit);
    }

    private static double getCoefToKgHa(PhytoProductUnit usageUnit) {
        double coefConversion;
        if (PhytoProductUnit.KG_HA == usageUnit) {
            coefConversion = 1.0;
        } else {
            coefConversion = PhytoProductUnitConverter.getUnitConversionRatio(usageUnit, PhytoProductUnit.KG_HA);
        }
        return coefConversion;
    }

    protected static @Nullable ReferenceDoseDTO getReferenceDoseDTO(
            AbstractPhytoProductInputUsage usage,
            PerformanceInterventionContext interventionContext) {

        Map<? extends AbstractInputUsage, UsagePerformanceResult> refDoseByUsages = interventionContext.getRefVintageTargetIftDoseByUsages();
        ReferenceDoseDTO referenceDose = null;
        UsagePerformanceResult usagePerformanceResult = refDoseByUsages.get(usage);
        if (usagePerformanceResult != null) {
            referenceDose = new ReferenceDoseDTO(usagePerformanceResult.refDoseId(), usagePerformanceResult.refDoseUnit(), usagePerformanceResult.refDoseValue());
        }
        return referenceDose;
    }
}
