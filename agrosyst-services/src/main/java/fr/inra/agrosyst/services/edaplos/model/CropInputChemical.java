package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;

import java.util.Arrays;
import java.util.Collections;

/**
 * <pre>
 * {@code
 * <ram:CropInputChemical>
 * <ram:TypeCode listAgencyName="FR, AGRO EDI EUROPE" listAgencyID="312">NT</ram:TypeCode>
 * <ram:PresencePercent>39</ram:PresencePercent>
 * <ram:ValueMeasure unitCode="NAR">59</ram:ValueMeasure>
 * </ram:CropInputChemical>
 * }
 * </pre>
 */
public class CropInputChemical implements AgroEdiObject {

    public static final String P2O5 = "PT";
    public static final String SO3 = "ST";
    public static final String N_AMMONIACAL = "NL";
    public static final String N_NITRIQUE = "NJ";
    public static final String N_TOTAL = "NT";
    public static final String N_UREIQUE = "NM";
    public static final String CA = "CA";
    public static final String MG = "MT";
    public static final String K2O = "KT";
    public static final String NA2O = "SO";
    public static final String B = "BT";
    public static final String CO = "OT";
    public static final String CU = "UT";
    public static final String FE = "FT";
    public static final String MN = "GT";
    public static final String ZN = "ZT";
    public static final String MO = "LT";

    public static final ImmutableList<String> FERTI_MIN_UNIFA_CHAUX = ImmutableList.copyOf(Arrays.asList("U08", "U10", "U07", "U09", "U06"));
    public static final ImmutableList<String> FERTI_MIN_UNIFA_SIDERURGIQUE = ImmutableList.copyOf(Collections.singletonList("U14"));

    protected String typeCode;

    /** Les partenaires fournissent des % ce qui équivaut à nos « unités fertilisantes/100 kg » ou « unité/100 L ». Pas besoin de conversion. */
    protected String presencePercent;

    /** Non utilisé */
    protected String valueMeasure;

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getPresencePercent() {
        return presencePercent;
    }

    public void setPresencePercent(String presencePercent) {
        this.presencePercent = presencePercent;
    }

    public String getValueMeasure() {
        return valueMeasure;
    }

    public void setValueMeasure(String valueMeasure) {
        this.valueMeasure = valueMeasure;
    }

    @Override
    public String getLocalizedIdentifier() {
        return "composition '" + typeCode + "'";
    }
}
