package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrapeImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefVarietePlantGrapeModel extends AbstractAgrosystModel<RefVarietePlantGrape> implements ExportModel<RefVarietePlantGrape> {

    public RefVarietePlantGrapeModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("CodeVar", RefVarietePlantGrape.PROPERTY_CODE_VAR, INT_PARSER);
        newMandatoryColumn("Variete", RefVarietePlantGrape.PROPERTY_VARIETE);
        newMandatoryColumn("Utilisation", RefVarietePlantGrape.PROPERTY_UTILISATION);
        newMandatoryColumn("Couleur", RefVarietePlantGrape.PROPERTY_COULEUR);
        newMandatoryColumn("Code GNIS", RefVarietePlantGrape.PROPERTY_CODE_GNIS);
        newMandatoryColumn("source", RefVarietePlantGrape.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefVarietePlantGrape.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefVarietePlantGrape, Object>> getColumnsForExport() {
        ModelBuilder<RefVarietePlantGrape> modelBuilder = new ModelBuilder<>();
        
        modelBuilder.newColumnForExport("CodeVar", RefVarietePlantGrape.PROPERTY_CODE_VAR, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Variete", RefVarietePlantGrape.PROPERTY_VARIETE);
        modelBuilder.newColumnForExport("Utilisation", RefVarietePlantGrape.PROPERTY_UTILISATION);
        modelBuilder.newColumnForExport("Couleur", RefVarietePlantGrape.PROPERTY_COULEUR);
        modelBuilder.newColumnForExport("Code GNIS", RefVarietePlantGrape.PROPERTY_CODE_GNIS);
        modelBuilder.newColumnForExport("source", RefVarietePlantGrape.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefVarietePlantGrape.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefVarietePlantGrape newEmptyInstance() {
        RefVarietePlantGrape result = new RefVarietePlantGrapeImpl();
        result.setActive(true);
        return result;
    }
}
