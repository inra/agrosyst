package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefGesCarburant;
import fr.inra.agrosyst.api.entities.referential.RefGesCarburantImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * libelle carburant;unité;GES (kq eq-CO2/unite);Source
 * 
 * @author Eric Chatellier
 */
public class RefGesCarburantModel extends AbstractAgrosystModel<RefGesCarburant> implements ExportModel<RefGesCarburant> {

    public RefGesCarburantModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("libelle carburant", RefGesCarburant.PROPERTY_LIBELLE_CARBURANT);
        newMandatoryColumn("unité", RefGesCarburant.PROPERTY_UNITE);
        newMandatoryColumn("GES (kq eq-CO2/unite)", RefGesCarburant.PROPERTY_GES, DOUBLE_PARSER);
        newMandatoryColumn("Source", RefGesCarburant.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefGesCarburant.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefGesCarburant, Object>> getColumnsForExport() {
        ModelBuilder<RefGesCarburant> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("libelle carburant", RefGesCarburant.PROPERTY_LIBELLE_CARBURANT);
        modelBuilder.newColumnForExport("unité", RefGesCarburant.PROPERTY_UNITE);
        modelBuilder.newColumnForExport("GES (kq eq-CO2/unite)", RefGesCarburant.PROPERTY_GES, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefGesCarburant.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefGesCarburant.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefGesCarburant newEmptyInstance() {
        RefGesCarburant refGesCarburant = new RefGesCarburantImpl();
        refGesCarburant.setActive(true);
        return refGesCarburant;
    }
}
