package fr.inra.agrosyst.services.effective;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Functions;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.Cattle;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.LivestockUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PlotTopiaDao;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.ToolsCouplingTopiaDao;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractActionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnection;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleSpecies;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrapeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDI;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.RefVarieteTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.action.ActionService;
import fr.inra.agrosyst.api.services.action.CreateOrUpdateActionsContext;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.domain.CattleDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlans;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.Equipments;
import fr.inra.agrosyst.api.services.domain.ToolsCouplingDto;
import fr.inra.agrosyst.api.services.domain.ZoneDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.effective.CopyPasteZoneByCampaigns;
import fr.inra.agrosyst.api.services.effective.CopyPasteZoneDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleConnectionDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleNodeDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCyclePhaseDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleSpeciesDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycles;
import fr.inra.agrosyst.api.services.effective.EffectiveInterventionDto;
import fr.inra.agrosyst.api.services.effective.EffectivePerennialCropCycleDto;
import fr.inra.agrosyst.api.services.effective.EffectiveSeasonalCropCycleDto;
import fr.inra.agrosyst.api.services.effective.EffectiveZoneFilter;
import fr.inra.agrosyst.api.services.effective.TargetedZones;
import fr.inra.agrosyst.api.services.itk.SpeciesStadeDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSystems;
import fr.inra.agrosyst.api.services.practiced.RefStadeEdiDto;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.security.AgrosystRuntimeException;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.effective.export.EffectiveCropCyclesExportTask;
import fr.inra.agrosyst.services.effective.export.EffectiveXlsExporter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.nuiton.util.pagination.PaginationResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Setter
public class EffectiveCropCycleServiceImpl extends AbstractAgrosystService implements EffectiveCropCycleService {

    protected static final Log LOGGER = LogFactory.getLog(EffectiveCropCycleServiceImpl.class);

    protected BusinessAuthorizationService authorizationService;
    protected ActionService actionService;
    protected PricesService pricesService;
    protected DomainInputStockUnitService domainInputStockUnitService;
    protected DomainService domainService;
    protected AnonymizeService anonymizeService;
    protected AgrosystI18nService i18nService;
    protected ReferentialService referentialService;

    protected ZoneTopiaDao zoneDao;
    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    protected ToolsCouplingTopiaDao toolsCouplingDao;
    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleDao;
    protected EffectiveSeasonalCropCycleTopiaDao effectiveSeasonalCropCycleDao;
    protected EffectiveCropCycleConnectionTopiaDao effectiveCropCycleConnectionDao;
    protected EffectiveCropCyclePhaseTopiaDao effectiveCropCyclePhaseDao;
    protected EffectiveCropCycleSpeciesTopiaDao effectiveCropCycleSpeciesDao;
    protected EffectiveCropCycleNodeTopiaDao effectiveCropCycleNodeDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDao;
    protected EffectiveSpeciesStadeTopiaDao effectiveSpeciesStadeDao;
    protected AbstractActionTopiaDao abstractActionDao;
    protected PlotTopiaDao plotDao;

    protected RefClonePlantGrapeTopiaDao refClonePlantGrapeDao;
    protected RefEspeceTopiaDao refEspeceDao;
    protected RefVarieteTopiaDao refVarieteDao;
    protected RefStadeEDITopiaDao refStadeEDIDao;
    protected RefOrientationEDITopiaDao refOrientationEDIDao;
    protected RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEDIDao;
    protected RefDestinationTopiaDao refDestinationDao;

    protected static final Function<EffectiveCropCycleConnection, String> EFFECTIVE_CONNECTION_BY_TARGET_ID = input -> input.getTarget().getTopiaId();

    protected final Function<Pair<EffectivePerennialCropCycle, Collection<EffectiveIntervention>>, CopyPasteZoneDto> EFFECTIVE_PERENNIAL_TO_CROP_CYCLE_CROP_TARGET_FUNCTION = effectiveCropCycleToInterventions -> {
        EffectivePerennialCropCycle epcc = effectiveCropCycleToInterventions.getLeft();
        CopyPasteZoneDto result = new CopyPasteZoneDto();
        result.setCroppingPlanEntryId(epcc.getCroppingPlanEntry().getTopiaId());
        result.setCroppingPlanEntryName(epcc.getCroppingPlanEntry().getName());

        EffectiveCropCyclePhase phase = epcc.getPhase();
        result.setPhaseId(phase.getTopiaId());
        result.setPhaseType(phase.getType());
        result.setNbInterventions(effectiveCropCycleToInterventions.getRight().size());
        return result;
    };

    protected final Function<Pair<EffectiveCropCycleNode, Collection<EffectiveIntervention>> , CopyPasteZoneDto> EFFECTIVE_CROP_CYCLE_NODE_TO_CROP_TARGET_FUNCTION = nodeAndInterventions -> {
        EffectiveCropCycleNode node = nodeAndInterventions.getLeft();
        Collection<EffectiveIntervention> nodeInterventions = nodeAndInterventions.getRight();
        long nbNonIntermediateInterventions = nodeInterventions.stream().filter(ei -> !ei.isIntermediateCrop()).count();
        long nbIntermediateInterventions = nodeInterventions.stream().filter(EffectiveIntervention::isIntermediateCrop).count();

        CopyPasteZoneDto result = new CopyPasteZoneDto();
        result.setCroppingPlanEntryId(node.getCroppingPlanEntry().getTopiaId());
        result.setCroppingPlanEntryName(node.getCroppingPlanEntry().getName());
        result.setRank(node.getRank());

        result.setNbInterventions(nbNonIntermediateInterventions);
        result.setNbIntermediateCropInterventions(nbIntermediateInterventions);
        result.setNodeId(node.getTopiaId());
        return result;
    };

    public static final Function<CroppingPlanSpecies, String> GET_CROPPING_PLAN_SPECIES_CODE = CroppingPlanSpecies::getCode;

    @Override
    public Zone getZone(String zoneTopiaId) {
        Zone zone = zoneDao.forTopiaIdEquals(zoneTopiaId).findUnique();
        return anonymizeService.checkForZoneAnonymization(zone);
    }

    @Override
    public PaginationResult<Zone> getFilteredZones(EffectiveZoneFilter filter) {
        PaginationResult<Zone> result = zoneDao.getFilteredZones(filter, getSecurityContext());
        // Cas particulier de ce filtre qui utilise le ComputedCacheHelper
        getTransaction().commit();
        return result;
    }

    @Override
    public PaginationResult<ZoneDto> getFilteredZonesDto(EffectiveZoneFilter filter) {
        PaginationResult<Zone> zones = getFilteredZones(filter);
        // 1 true: we want plot to be loaded
        // 2 true: do not throw up exception if user is not allowed to read the domain and it must be anonymized
        return zones.transform(anonymizeService.getZoneToDtoFunction(true, true));
    }

    @Override
    public PaginationResult<ZoneDto> getFilteredZonesAndCroppingPlanInfosDto(EffectiveZoneFilter filter) {

        PaginationResult<ZoneDto> result = getFilteredZonesDto(filter);
        // loop among Zone DTOs
        for (ZoneDto zoneDto : result.getElements()) {
            Zone zone = zoneDao.forTopiaIdEquals(zoneDto.getTopiaId()).findUnique();
            String croppingPlanInfo = getCroppingPlanInfoForZone(Collections.singleton(zone));
            zoneDto.setCroppingPlanInfo(croppingPlanInfo);
        }
        return result;
    }

    @Override
    public String getCroppingPlanInfoForZone(Collection<Zone> zones) {
        // for each PerennialCropCycle String with: The crop name (the number of interventions)
        Collection<CroppingPlanInfo> croppingPlanInfos = getEffectivePerennialCropCycleInfo(zones);
        // for each SeasonalCropCycle String with: The crop name (the number of interventions) CI (the number of interventions for intermediate crop)
        List<CroppingPlanInfo> effectiveSeasonalCropCyclesInfo = getEffectiveSeasonalCropCyclesInfo(zones);
        croppingPlanInfos.addAll(effectiveSeasonalCropCyclesInfo);

        Map<String, Long> interventionCountByCroppingPlan = new HashMap<>();
        Map<String, Long> intermediateCropInterventionCountByCroppingPlan = new HashMap<>();
        croppingPlanInfos.forEach(croppingPlanInfo -> {
            interventionCountByCroppingPlan.merge(
                    croppingPlanInfo.croppingPlanName(),
                    croppingPlanInfo.interventionCount(),
                    this::mergeCounts
            );
            intermediateCropInterventionCountByCroppingPlan.merge(
                    croppingPlanInfo.croppingPlanName(),
                    croppingPlanInfo.intermediateCropInterventionCount(),
                    this::mergeCounts
            );
        });
        return croppingPlanInfos.stream()
                .map(CroppingPlanInfo::croppingPlanName)
                .distinct()
                .map(croppingPlanName -> {
                    StringBuilder formattedCroppingPlanInfo = new StringBuilder();
                    long interventionCount = interventionCountByCroppingPlan.get(croppingPlanName);
                    long intermediateCropInterventionCount = intermediateCropInterventionCountByCroppingPlan.get(croppingPlanName);

                    if (intermediateCropInterventionCount > 0) {
                        formattedCroppingPlanInfo.append("CI (").append(intermediateCropInterventionCount).append(") ");
                    }
                    formattedCroppingPlanInfo.append(croppingPlanName);
                    if (interventionCount > 0) {
                        formattedCroppingPlanInfo.append(" (").append(interventionCount).append(")");
                    }
                    return formattedCroppingPlanInfo.toString();
                })
                .collect(Collectors.joining(" - "));
    }

    protected long mergeCounts(Long previousCount, Long interventionCount) {
        long count = 0;
        if (previousCount != null) {
            count += previousCount;
        }
        if (interventionCount != null) {
            count += interventionCount;
        }
        return count;
    }

    protected List<CroppingPlanInfo> getEffectivePerennialCropCycleInfo(Collection<Zone> zones) {
        List<CroppingPlanInfo> result = new ArrayList<>();
        List<EffectivePerennialCropCycle> cycles = effectivePerennialCropCycleDao.forZoneIn(zones).findAll();

        for (EffectivePerennialCropCycle cycle : cycles) {
            Long count = null;
            EffectiveCropCyclePhase phase = cycle.getPhase();
            if (phase != null) {
                count = effectiveInterventionDao.forEffectiveCropCyclePhaseEquals(phase).count();
            }
            result.add(new CroppingPlanInfo(cycle.getCroppingPlanEntry().getName(), count, 0L));
        }

        return result;
    }

    protected List<CroppingPlanInfo> getEffectiveCropCycleNodesInfo(Collection<EffectiveCropCycleNode> allEffectiveCropCycleNodes) {
        List<CroppingPlanInfo> result = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(allEffectiveCropCycleNodes)) {
            for (EffectiveCropCycleNode effectiveCropCycleNode : allEffectiveCropCycleNodes) {

                EffectiveCropCycleConnection effectiveCropCycleConnection = effectiveCropCycleConnectionDao.forTargetEquals(effectiveCropCycleNode).findUniqueOrNull();
                List<EffectiveIntervention> allEffectiveInterventions = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(effectiveCropCycleNode).findAll();
                int interventionCount = allEffectiveInterventions.size();
                long intermediateCropInterventionCount = 0;

                // test if the node gets a connection (the first node doesn't) and if the connection gets an intermediateCroppingPlanEntry before getting infos
                if (effectiveCropCycleConnection != null && effectiveCropCycleConnection.getIntermediateCroppingPlanEntry() != null) {
                    intermediateCropInterventionCount = allEffectiveInterventions.stream()
                            .filter(EffectiveIntervention::isIntermediateCrop)
                            .count();
                }
                // add principal croppingPlanEntry Info
                String cropName = effectiveCropCycleNode.getCroppingPlanEntry().getName();
                result.add(new CroppingPlanInfo(cropName, interventionCount - intermediateCropInterventionCount, intermediateCropInterventionCount));
            }
        }
        return result;
    }

    protected List<CroppingPlanInfo> getEffectiveSeasonalCropCyclesInfo(Collection<Zone> zones) {
        List<CroppingPlanInfo> result = new ArrayList<>();
        // get all SeasonalCycles Dtos.
        // Theoretically, there is only one seasonalCycle per zone but in case of a future update, it works even for several seasonalCycles per zone
        List<EffectiveSeasonalCropCycle> allEffectiveSeasonalCropCycles = effectiveSeasonalCropCycleDao.forZoneIn(zones).findAll();

        if (CollectionUtils.isNotEmpty(allEffectiveSeasonalCropCycles)) {
            for (EffectiveSeasonalCropCycle effectiveSeasonalCropCycle : allEffectiveSeasonalCropCycles) {
                Collection<EffectiveCropCycleNode> allEffectiveCropCycleNodes = effectiveSeasonalCropCycle.getNodes();

                result.addAll(getEffectiveCropCycleNodesInfo(allEffectiveCropCycleNodes));
            }
        }
        return result;
    }

    protected record CroppingPlanInfo(
            String croppingPlanName,
            Long interventionCount,
            Long intermediateCropInterventionCount) {
    }

    @Override
    public Set<String> getFilteredZonesAndCroppingPlanInfoIds(EffectiveZoneFilter filter) {
        Set<String> result = zoneDao.getFilteredZoneIds(filter, getSecurityContext());
        // Cas particulier de ce filtre qui utilise le ComputedCacheHelper
        getTransaction().commit();
        return result;
    }

    @Override
    public List<CroppingPlanEntry> getZoneCroppingPlanEntries(Zone zone) {
        return croppingPlanEntryDao.forDomainEquals(zone.getPlot().getDomain()).findAll();
    }

    @Override
    public List<CroppingPlanEntry> getZoneCroppingPlanEntriesWithoutDomain(Zone zone) {

        List<CroppingPlanEntry> entries = croppingPlanEntryDao.forDomainEquals(zone.getPlot().getDomain()).setOrderByArguments(CroppingPlanEntry.PROPERTY_NAME).findAll();
        i18nService.translateCroppingPlanEntry(entries);

        return entries;
    }

    @Override
    public CroppingPlanEntry getPreviousCampaignCroppingPlanEntry(String zoneTopiaId) {
        Zone zone = zoneDao.forTopiaIdEquals(zoneTopiaId).findUnique();

        EffectiveCropCycleNode node = effectiveCropCycleNodeDao.findLastNodeForPreviousCampaign(zone);
        CroppingPlanEntry result = null;
        if (node != null) {
            result = node.getCroppingPlanEntry();
        }
        return result;
    }

    @Override
    public List<EffectivePerennialCropCycleDto> getAllEffectivePerennialCropCyclesDtos(String zoneTopiaId) {

        Zone zone = zoneDao.forTopiaIdEquals(zoneTopiaId).findUnique();
        List<EffectivePerennialCropCycle> cycles = effectivePerennialCropCycleDao.forZoneEquals(zone).findAll();
        return convertPerennialCropCyclesToDto(cycles, zone);
    }

    protected List<EffectivePerennialCropCycleDto> convertPerennialCropCyclesToDto(
            List<EffectivePerennialCropCycle> cycles,
            Zone zone) {
        Language language = getSecurityContext().getLanguage();
        List<EffectivePerennialCropCycleDto> result = Lists.newArrayListWithCapacity(cycles.size());
        Domain domain = zone.getPlot().getDomain();
        for (EffectivePerennialCropCycle cycle : cycles) {
            String cropCode = cycle.getCroppingPlanEntry().getCode();

            EffectivePerennialCropCycleDto dto = EffectiveCropCycles.PERENNIAL_CROP_CYCLE_TO_DTO.apply(cycle);

            EffectiveCropCyclePhase phase = cycle.getPhase();
            if (phase != null) {
                EffectiveCropCyclePhaseDto phaseDto = EffectiveCropCycles.CROP_CYCLE_PHASE_TO_DTO.apply(phase);
                // interventions
                List<EffectiveIntervention> interventions = effectiveInterventionDao.forEffectiveCropCyclePhaseEquals(phase).findAll();

                List<EffectiveInterventionDto> interventionDtos = transformePracticedInterventionEntitiesToDtos(
                        language, domain, cropCode, interventions);

                phaseDto.setInterventions(interventionDtos);
                dto.setPhaseDtos(Lists.newArrayList(phaseDto));
            }

            String croppingPlanEntryCode = cycle.getCroppingPlanEntry().getCode();
            List<EffectiveCropCycleSpeciesDto> species = getCropCyclePerennialSpecies(
                    croppingPlanEntryCode,
                    cycle,
                    zone.getPlot().getDomain().getCampaign());
            dto.setSpeciesDtos(species);

            result.add(dto);
        }
        return result;
    }

    protected List<EffectiveInterventionDto> transformePracticedInterventionEntitiesToDtos(Language language, Domain domain, String cropCode, List<EffectiveIntervention> interventions) {
        return transformeInterventionEntitiesToDtos(
                language, domain, cropCode, interventions, null);
    }

    protected List<EffectiveInterventionDto> transformeInterventionEntitiesToDtos(Language language, Domain domain, String cropCode, List<EffectiveIntervention> interventions, String intermediateCropCode) {
        List<EffectiveInterventionDto> interventionDtos = Lists.newArrayList();
        for (EffectiveIntervention intervention : interventions) {

            ReferentialTranslationMap translationMap = new ReferentialTranslationMap(language);
            final EffectiveInterventionDto interventionDto = getTranslatedEffectiveInterventionDto(domain, intervention, translationMap);

            final List<AbstractActionDto> actionDtos = actionService.loadEffectiveActionsAndUsages(intervention);
            interventionDto.setActionDtos(actionDtos);

            boolean isIntermediate = interventionDto.isIntermediateCrop() && !Strings.isNullOrEmpty(intermediateCropCode);
            interventionDto.setIntermediateCrop(isIntermediate);
            interventionDto.setFromCropCode(isIntermediate ? intermediateCropCode : cropCode);
            interventionDtos.add(interventionDto);

        }
        return interventionDtos;
    }

    /**
     * Méthode qui charge la liste des espèces disponibles pour le cycle passé en paramètre et remplit les Dto avec les
     * informations déjà saisie dans ce cycle
     *
     * @param croppingPlanEntryCode l'identifiant de la culture à traiter
     * @param cycle                 le cycle pérenne
     * @param campaign              la campagne concernée (issue de la zone)
     * @return la liste complète des espèces complétée des informations liées au cycle
     */
    protected List<EffectiveCropCycleSpeciesDto> getCropCyclePerennialSpecies(
            String croppingPlanEntryCode,
            EffectivePerennialCropCycle cycle,
            Integer campaign) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(croppingPlanEntryCode));
        Preconditions.checkArgument(campaign != null);

        final Map<String, EffectiveCropCycleSpecies> speciesCodeToCycleEntity;

        if (cycle == null || cycle.getSpecies() == null) {
            speciesCodeToCycleEntity = new HashMap<>();
        } else {
            speciesCodeToCycleEntity = Maps.uniqueIndex(cycle.getSpecies(), EffectiveCropCycles.GET_CROP_CYCLE_PERENNIAL_SPECIES_CODE::apply);
        }

        // Chargement de la liste des espèces (sans doublon)
        Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>> entryAndSpeciesFromCode =
                domainService.getEntryAndSpeciesFromCode(croppingPlanEntryCode, Sets.newHashSet(campaign));

        ReferentialTranslationMap translationMap = i18nService.fillCroppingPlanEntryTranslationMaps(List.of(entryAndSpeciesFromCode.getLeft()));

        // Dto -> Decoration
        Function<CroppingPlanSpeciesDto, EffectiveCropCycleSpeciesDto> decorateFunction = input -> {
            String croppingPlanSpeciesCode = input.getCode();
            EffectiveCropCycleSpeciesDto result = new EffectiveCropCycleSpeciesDto(input);
            EffectiveCropCycleSpecies cropCyclePerennialSpecies = speciesCodeToCycleEntity.get(croppingPlanSpeciesCode);
            if (cropCyclePerennialSpecies != null) {
                result.setTopiaId(cropCyclePerennialSpecies.getTopiaId());
                result.setOverGraftDate(cropCyclePerennialSpecies.getOverGraftDate());
                result.setPlantsCertified(cropCyclePerennialSpecies.isPlantsCertified());
                result.setGraftSupport(PracticedSystems.REF_VARIETE_TO_GRAPH_DTO.apply(cropCyclePerennialSpecies.getGraftSupport()));
                result.setGraftClone(PracticedSystems.REF_CLONE_TO_GRAPH_DTO.apply(cropCyclePerennialSpecies.getGraftClone()));
            }
            return result;
        };
        // La transformation se fait en 2 temps : Entity -> Dto -> Decoration
        Function<CroppingPlanSpecies, EffectiveCropCycleSpeciesDto> transformFunction =
                Functions.compose(decorateFunction::apply, cps -> CroppingPlans.getDtoForCroppingPlanSpecies(cps, translationMap));

        return entryAndSpeciesFromCode.getRight().values().stream().map(transformFunction).collect(Collectors.toList());
    }

    @Override
    public List<EffectiveSeasonalCropCycleDto> getAllEffectiveSeasonalCropCyclesDtos(String zoneTopiaId) {
        Language language = getSecurityContext().getLanguage();

        final Zone zone = zoneDao.forTopiaIdEquals(zoneTopiaId).findUnique();
        final Domain domain = zone.getPlot().getDomain();

        List<EffectiveSeasonalCropCycle> cycles = effectiveSeasonalCropCycleDao.forZoneEquals(zone).findAll();
        List<EffectiveSeasonalCropCycleDto> result = Lists.newArrayListWithCapacity(cycles.size());
        for (EffectiveSeasonalCropCycle cycle : cycles) {

            EffectiveSeasonalCropCycleDto dto = EffectiveCropCycles.SEASONNAL_CROP_CYCLE_TO_DTO.apply(cycle);

            List<EffectiveCropCycleConnection> connections = effectiveCropCycleConnectionDao.findAllByEffectiveSeasonalCropCycle(cycle);
            dto.setConnectionDtos(Lists.newArrayList(connections.stream().map(EffectiveCropCycles.CROP_CYCLE_CONNECTION_TO_DTO).collect(Collectors.toList())));
            Map<String, EffectiveCropCycleConnection> connexionByTargetId = Maps.uniqueIndex(connections, EFFECTIVE_CONNECTION_BY_TARGET_ID::apply);

            if (cycle.getNodes() != null) {
                List<EffectiveCropCycleNodeDto> nodeDtos = Lists.newArrayListWithCapacity(cycle.getNodes().size());
                for (EffectiveCropCycleNode node : cycle.getNodes()) {
                    EffectiveCropCycleNodeDto nodeDto = EffectiveCropCycles.CROP_CYCLE_NODE_TO_DTO.apply(node);
                    nodeDtos.add(nodeDto);
                    String cropCode = node.getCroppingPlanEntry().getCode();

                    String intermediateCropCode = null;
                    EffectiveCropCycleConnection connection = connexionByTargetId.get(node.getTopiaId());
                    if (connection != null) {
                        CroppingPlanEntry intermediateCrop = connection.getIntermediateCroppingPlanEntry();
                        if (intermediateCrop != null) {
                            intermediateCropCode = intermediateCrop.getCode();
                            nodeDto.setIntermediateCropCode(intermediateCrop.getCode());
                            nodeDto.setIntermediateCropLabel(intermediateCrop.getName());
                        }
                    }

                    // interventions
                    List<EffectiveIntervention> interventions = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(node).findAll();

                    List<EffectiveInterventionDto> interventionDtos = transformeInterventionEntitiesToDtos(
                            language, domain, cropCode, interventions, intermediateCropCode);

                    nodeDto.setInterventions(interventionDtos);
                }
                dto.setNodeDtos(nodeDtos);
            }

            result.add(dto);
        }

        return result;
    }

    @Override
    public void updateEffectiveCropCycles(String zoneId,
                                          List<EffectiveSeasonalCropCycleDto> effectiveSeasonalCropCycleDtos,
                                          List<EffectivePerennialCropCycleDto> effectivePerennialCropCycleDtos) {

        authorizationService.checkCreateOrUpdateEffectiveCropCycles(zoneId);

        Zone zone = zoneDao.forTopiaIdEquals(zoneId).findUnique();

        checkPreconditionOnActive(zoneId, zone);

        Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(zone);
        Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant = getSectorByCodeEspceBotaniqueCodeQualifiant(zone);
        final Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds = domainInputStockUnitService.loadDomainInputStockByIds(zone.getPlot().getDomain());
        Collection<AbstractDomainInputStockUnit> domainInputStockUnits = domainInputStockUnitByIds.values().stream().toList();

        Domain domain = zone.getPlot().getDomain();
        List<CroppingPlanEntry> targetedZoneCroppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(domain);
        List<ToolsCoupling> toolsCouplings = domainService.getToolsCouplings(domain.getTopiaId());
        Map<String, ToolsCoupling> toolsCouplingByCode = Maps.uniqueIndex(toolsCouplings, ToolsCoupling::getCode);

        CreateOrUpdateEffectiveCroCyclesContext updateEffectiveCroCyclesContext = new CreateOrUpdateEffectiveCroCyclesContext(
                zone,
                effectiveSeasonalCropCycleDtos,
                effectivePerennialCropCycleDtos,
                targetedZoneCroppingPlanEntries,
                toolsCouplingByCode,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspceBotaniqueCodeQualifiant,
                domainInputStockUnits);

        updateEffectivePerennialCropCycles(updateEffectiveCroCyclesContext);

        updateEffectiveSeasonalCropCycles(updateEffectiveCroCyclesContext);

        getTransaction().commit();
    }

    protected void checkPreconditionOnActive(String zoneId, Zone zone) {
        Preconditions.checkArgument(
                zone.isActive() && zone.getPlot().isActive() && zone.getPlot().getDomain().isActive() &&
                        (zone.getPlot().getGrowingSystem() == null || zone.getPlot().getGrowingSystem().isActive()),
                "The zone, plot, growing system or domain for zone with id:" + zoneId + " is actually unactivated");
    }

    /**
     * Update seasonal crop cycles linked to zone.
     */
    protected void updateEffectiveSeasonalCropCycles(CreateOrUpdateEffectiveCroCyclesContext context) {
        List<EffectiveSeasonalCropCycleDto> effectiveSeasonalCropCycles = context.getEffectiveSeasonalCropCycles();

        if (effectiveSeasonalCropCycles != null) { // null can be valid only for test

            Zone zone = context.getZone();

            List<EffectiveSeasonalCropCycle> cycles = effectiveSeasonalCropCycleDao.forZoneEquals(zone).findAll();
            Set<EffectiveSeasonalCropCycle> toDeleteCycles = Sets.newHashSet(cycles);
            Map<String, EffectiveSeasonalCropCycle> idToCycles = Maps.uniqueIndex(cycles, Entities.GET_TOPIA_ID::apply);

            for (EffectiveSeasonalCropCycleDto cycleDto : effectiveSeasonalCropCycles) {

                context.setEffectiveSeasonalCropCycleDto(cycleDto);
                String cycleId = cycleDto.getTopiaId();
                EffectiveSeasonalCropCycle cycle = getEffectiveSeasonalCropCycle(zone, toDeleteCycles, idToCycles, cycleId);
                context.setEffectiveSeasonalCropCycle(cycle);

                // manage cycle nodes
                updateCycleNodesAndConnections(context);

                // save or remove cycle (if cycle hasn't got any node then it must be removed)
                if (CollectionUtils.isNotEmpty(cycle.getNodes())) {
                    if (StringUtils.isBlank(cycleId)) {
                        effectiveSeasonalCropCycleDao.create(cycle);
                    } else {
                        effectiveSeasonalCropCycleDao.update(cycle);
                    }
                } else if (StringUtils.isNotBlank(cycleId)) {
                    // remove cycle that has no more nodes
                    toDeleteCycles.add(cycle);
                }

            }
            removeEffectiveSeasonalCropCycles(toDeleteCycles);
        }
    }

    private EffectiveSeasonalCropCycle getEffectiveSeasonalCropCycle(Zone zone, Set<EffectiveSeasonalCropCycle> toDeleteCycles, Map<String, EffectiveSeasonalCropCycle> idToCycles, String cycleId) {
        EffectiveSeasonalCropCycle cycle;
        if (StringUtils.isBlank(cycleId)) {
            cycle = effectiveSeasonalCropCycleDao.newInstance();
            cycle.setZone(zone);
        } else {
            cycle = idToCycles.get(cycleId);
            toDeleteCycles.remove(cycle);
        }
        return cycle;
    }

    private void processCycleConnections(List<CroppingPlanEntry> targetedZoneCroppingPlanEntries,
                                         EffectiveSeasonalCropCycleDto cycleDto,
                                         Map<String, EffectiveCropCycleNode> nodeIdToEntity,
                                         EffectiveSeasonalCropCycle cycle) {
        Collection<EffectiveCropCycleConnection> cycleConnections = effectiveCropCycleConnectionDao.findAllByEffectiveSeasonalCropCycle(cycle);
        Map<String, EffectiveCropCycleConnection> connectionByTarget = Maps.newHashMap(Maps.uniqueIndex(cycleConnections, EFFECTIVE_CONNECTION_BY_TARGET_ID::apply));

        if (CollectionUtils.isNotEmpty(cycleDto.getConnectionDtos())) {
            for (EffectiveCropCycleConnectionDto connectionDto : cycleDto.getConnectionDtos()) {
                String sourceId = null;
                if (connectionDto.getSourceId() != null) {
                    if (EffectiveCropCycleNodeDto.NODE_BEFORE_ID.equals(connectionDto.getSourceId())) {
                        sourceId = EffectiveCropCycleNodeDto.NODE_BEFORE_ID;
                    } else {
                        sourceId = Entities.UNESCAPE_TOPIA_ID.apply(connectionDto.getSourceId());
                    }
                }

                String targetId = Entities.UNESCAPE_TOPIA_ID.apply(connectionDto.getTargetId());
                Preconditions.checkArgument(StringUtils.isNotBlank(targetId), "La connexion n'a pas de noeud cible");

                EffectiveCropCycleNode sourceNode = nodeIdToEntity.get(sourceId);
                EffectiveCropCycleNode targetNode = nodeIdToEntity.get(targetId);

                if (targetNode == null) {
                    // if targeted node is null we continue to remove the un valid connection
                    continue;
                }

                EffectiveCropCycleConnection connection = connectionByTarget.remove(targetId);

                if (connection == null) {
                    connection = effectiveCropCycleConnectionDao.newInstance();
                }

                connection.setEdaplosIssuerId(connectionDto.getEdaplosIssuerId());
                connection.setSource(sourceNode);
                connection.setTarget(targetNode);
                CroppingPlanEntry intermediateCrops = null;
                if (StringUtils.isNotBlank(connectionDto.getIntermediateCroppingPlanEntryId())) {
                    intermediateCrops = croppingPlanEntryDao.forTopiaIdEquals(connectionDto.getIntermediateCroppingPlanEntryId()).findUnique();
                }
                if (intermediateCrops == null) {
                    connection.setIntermediateCroppingPlanEntry(null);
                }
                if (intermediateCrops != null && targetedZoneCroppingPlanEntries.contains(intermediateCrops)) {
                    connection.setIntermediateCroppingPlanEntry(intermediateCrops);
                }
                if (connection.isPersisted()) {
                    effectiveCropCycleConnectionDao.update(connection);
                } else {
                    effectiveCropCycleConnectionDao.create(connection);
                }
            }
        }

        effectiveCropCycleConnectionDao.deleteAll(connectionByTarget.values());
    }

    private void updateCycleNodesAndConnections(CreateOrUpdateEffectiveCroCyclesContext context) {
        List<CroppingPlanEntry> targetedZoneCroppingPlanEntries = context.getTargetedZoneCroppingPlanEntries();
        EffectiveSeasonalCropCycleDto cycleDto = context.getEffectiveSeasonalCropCycleDto();
        EffectiveSeasonalCropCycle persistedCycle = context.getEffectiveSeasonalCropCycle();
        // map to link connection to nodes
        Map<String, EffectiveCropCycleNode> nodeIdToEntity = new HashMap<>();

        Collection<EffectiveCropCycleNode> cycleNodes = persistedCycle.getNodes();
        if (cycleNodes == null) {
            cycleNodes = new ArrayList<>();
            persistedCycle.setNodes(cycleNodes);
        }

        List<EffectiveCropCycleNodeDto> nodeDtos = cycleDto.getNodeDtos();

        Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = context.getSpeciesCodeToCodeEspeceBotaniqueCodeQualifiantAee();
        Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant = context.getSectorByCodeEspceBotaniqueCodeQualifiant();

        Map<String, EffectiveIntervention> interventionToRemoves = new HashMap<>();
        if (nodeDtos != null) {

            Map<String, CroppingPlanEntry> nodesCropsByIds = getNodesCropsByIds(nodeDtos);

            for (EffectiveCropCycleNodeDto nodeDto : nodeDtos) {//new-node-b486e417-08ec-45c1-f4a1-4dca84cd37c2
                String nodeId = Entities.UNESCAPE_TOPIA_ID.apply(nodeDto.getNodeId());
                Preconditions.checkNotNull(nodeId);

                // this node is boring, just ignore him ;)
                if (nodeId.equalsIgnoreCase(EffectiveCropCycleNodeDto.NODE_BEFORE_ID)) {
                    continue;
                }

                EffectiveCropCycleNode node;
                if (nodeId.startsWith(NEW_NODE_PREFIX)) {
                    node = effectiveCropCycleNodeDao.newInstance();
                } else {
                    node = TopiaEntities.findByTopiaId(persistedCycle.getNodes(), nodeId);
                }

                node.setRank(nodeDto.getX());
                node.setEdaplosIssuerId(nodeDto.getEdaplosIssuerId());

                CroppingPlanEntry croppingPlanEntry;
                if (StringUtils.isBlank(nodeDto.getCroppingPlanEntryId())) {
                    continue;
                }
                croppingPlanEntry = nodesCropsByIds.get(nodeDto.getCroppingPlanEntryId());

                Map<String, CroppingPlanEntry> targetedZoneCroppingPlanEntriesByCodes = Maps.uniqueIndex(targetedZoneCroppingPlanEntries, CroppingPlanEntry::getCode);
                CroppingPlanEntry targetedCroppingPlanEntry = targetedZoneCroppingPlanEntriesByCodes.get(croppingPlanEntry.getCode());
                if (targetedCroppingPlanEntry == null) {
                    continue;
                }

                node.setCroppingPlanEntry(targetedCroppingPlanEntry);

                if (!node.isPersisted()) {
                    cycleNodes.add(node);
                }
                nodeIdToEntity.put(nodeId, node);

                // node must be persisted for intervention saving
                if (node.isPersisted()) {
                    node = effectiveCropCycleNodeDao.update(node);
                } else {
                    node = effectiveCropCycleNodeDao.create(node);
                }

                // node's interventions
                CreateOrUpdateEffectiveInterventionContext updateInterventionsResults = updateEffectiveInterventions(
                        nodeDto.getInterventions(),
                        cycleDto,
                        getTargetedNodeByIds(node, nodeId),
                        null,
                        null,
                        context.getDomain(),
                        context.getZone(),
                        speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                        sectorByCodeEspceBotaniqueCodeQualifiant,
                        context.getToolsCouplingsByCode(),
                        context.getDomainInputStocks());

                interventionToRemoves.putAll(updateInterventionsResults.getIdsToInterventions());
                addSavedValorisationsToContext(context, updateInterventionsResults);
            }
        }
        // manage cycle connectionDtos
        processCycleConnections(targetedZoneCroppingPlanEntries, cycleDto, nodeIdToEntity, persistedCycle);

        Collection<EffectiveCropCycleNode> nodesToDelete = CollectionUtils.subtract(cycleNodes, nodeIdToEntity.values());

        // delete all intervention for node to delete
        removeConnexions(nodesToDelete);

        removeEffectiveInterventions(interventionToRemoves.values());

        // delete node (after connectionDtos)
        cycleNodes.retainAll(nodeIdToEntity.values());

    }

    private void addSavedValorisationsToContext(CreateOrUpdateEffectiveCroCyclesContext context, CreateOrUpdateEffectiveInterventionContext updateInterventionsResults) {
        List<CreateOrUpdateActionsContext> createOrUpdateActionsContexts = updateInterventionsResults.getCreateOrUpdateActionsContexts();
        if (!createOrUpdateActionsContexts.isEmpty()) {
            for (CreateOrUpdateActionsContext createOrUpdateActionsContext : createOrUpdateActionsContexts) {
                context.addSavedActionValorisationsByOriginalIds(createOrUpdateActionsContext.getSavedActionValorisationsByOriginalIds());
                context.addAllSavedAction(createOrUpdateActionsContext.getSavedActionsByOriginalIds());
            }
        }
    }

    protected Map<String, CroppingPlanEntry> getNodesCropsByIds(List<EffectiveCropCycleNodeDto> nodeDtos) {
        Set<String> cropIds = Sets.newHashSet();
        for (EffectiveCropCycleNodeDto nodeDto : nodeDtos) {
            if (StringUtils.isNotBlank(nodeDto.getCroppingPlanEntryId())) {
                cropIds.add(nodeDto.getCroppingPlanEntryId());
            }
        }
        List<CroppingPlanEntry> nodesCrops = croppingPlanEntryDao.forTopiaIdIn(cropIds).findAll();
        return Maps.uniqueIndex(nodesCrops, Entities.GET_TOPIA_ID::apply);
    }

    protected void removeEffectiveSeasonalCropCycles(Set<EffectiveSeasonalCropCycle> cycles) {
        if (cycles != null) {
            for (EffectiveSeasonalCropCycle cycle : cycles) {
                Collection<EffectiveCropCycleNode> nodes = cycle.getNodes();
                removeConnexions(nodes);
            }
            effectiveSeasonalCropCycleDao.deleteAll(cycles);// nodes are remove from compo.
        }

    }

    protected void removeConnexions(Collection<EffectiveCropCycleNode> nodes) {
        List<EffectiveCropCycleConnection> result = new ArrayList<>();
        if (nodes != null && !nodes.isEmpty()) {
            Collection<EffectiveIntervention> effectiveInterventions = effectiveInterventionDao.forEffectiveCropCycleNodeIn(nodes).findAll();
            removeEffectiveInterventions(effectiveInterventions);

            for (EffectiveCropCycleNode node : nodes) {
                EffectiveCropCycleConnection target = effectiveCropCycleConnectionDao.forTargetEquals(node).findUniqueOrNull();
                if (target != null) {
                    result.add(target);
                }
            }
            effectiveCropCycleConnectionDao.deleteAll(result);
        }
    }

    /**
     * Update perennial crop cycles linked to plot.
     */
    protected void updateEffectivePerennialCropCycles(CreateOrUpdateEffectiveCroCyclesContext context) {
        List<EffectivePerennialCropCycleDto> effectivePerennialCropCycles = context.getEffectivePerennialCropCycles();

        if (effectivePerennialCropCycles != null) { // null can be valid only for test

            Zone zone = context.getZone();

            List<CroppingPlanEntry> targetedZoneCroppingPlanEntries = context.getTargetedZoneCroppingPlanEntries();

            // a cycle is related to a cropping plan. If there are no cropping plan for the targeted zone so cycle can not be attached.
            Preconditions.checkArgument(!targetedZoneCroppingPlanEntries.isEmpty());

            Map<String, CroppingPlanSpecies> availableCroppingPlanSpecies = getCroppingPlanSpecies(targetedZoneCroppingPlanEntries);

            List<EffectivePerennialCropCycle> cycles = effectivePerennialCropCycleDao.forZoneEquals(zone).findAll();
            List<EffectivePerennialCropCycle> toDelete = Lists.newArrayList(cycles);
            Map<String, EffectivePerennialCropCycle> idToCycles = Maps.uniqueIndex(cycles, Entities.GET_TOPIA_ID::apply);

            Binder<EffectivePerennialCropCycleDto, EffectivePerennialCropCycle> binder = BinderFactory.newBinder(EffectivePerennialCropCycleDto.class, EffectivePerennialCropCycle.class);
            for (EffectivePerennialCropCycleDto cycleDto : effectivePerennialCropCycles) {

                CroppingPlanEntry croppingPlanEntry = croppingPlanEntryDao.forTopiaIdEquals(cycleDto.getCroppingPlanEntryId()).findUnique();
                if (!targetedZoneCroppingPlanEntries.contains(croppingPlanEntry)) {
                    // cycle cropping plan does not append to the targeted zone so it can no be affected.
                    continue;
                }

                // current cycle
                String dtoId = cycleDto.getTopiaId();
                EffectivePerennialCropCycle cycle = getEffectivePerennialCropCycle(zone, toDelete, idToCycles, dtoId);

                binder.copyExcluding(cycleDto, cycle,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        EffectivePerennialCropCycle.PROPERTY_ZONE,
                        EffectivePerennialCropCycle.PROPERTY_ORIENTATION,
                        EffectivePerennialCropCycle.PROPERTY_PHASE,
                        EffectivePerennialCropCycle.PROPERTY_SPECIES,
                        EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY);
                cycle.setCroppingPlanEntry(croppingPlanEntry);

                RefOrientationEDI orientation = loadRefOrientationEDI(cycleDto);
                cycle.setOrientation(orientation);

                Preconditions.checkState(cycleDto.getPhaseDtos().size() == 1);
                // cycle's phases
                context.setEffectivePerennialCropCycleDto(cycleDto);
                context.setEffectivePerennialCropCycle(cycle);

                createOrUpdatePhase(context);

                // cycle's species
                createOrUpdateEffectiveCropCycleSpecies(availableCroppingPlanSpecies, cycleDto, cycle);

                // persist cycle
                if (StringUtils.isBlank(dtoId)) {
                    effectivePerennialCropCycleDao.create(cycle);
                } else {
                    effectivePerennialCropCycleDao.update(cycle);
                }
            }
            removeEffectivePerennialCropCycles(toDelete);
        }
    }

    protected Map<String, CroppingPlanSpecies> getCroppingPlanSpecies(List<CroppingPlanEntry> targetedZoneCroppingPlanEntries) {
        Map<String, CroppingPlanSpecies> availableCroppingPlanSpecies = new HashMap<>();
        for (CroppingPlanEntry croppingPlanEntry : targetedZoneCroppingPlanEntries) {
            if (croppingPlanEntry != null) {
                if (CollectionUtils.isNotEmpty(croppingPlanEntry.getCroppingPlanSpecies())) {
                    for (CroppingPlanSpecies croppingPlanSpecies : croppingPlanEntry.getCroppingPlanSpecies()) {
                        if (croppingPlanSpecies != null) {
                            availableCroppingPlanSpecies.put(croppingPlanSpecies.getTopiaId(), croppingPlanSpecies);
                        }
                    }
                }
            }
        }
        return availableCroppingPlanSpecies;
    }

    private void createOrUpdateEffectiveCropCycleSpecies(
            Map<String, CroppingPlanSpecies> availableCroppingPlanSpecies,
            EffectivePerennialCropCycleDto cycleDto,
            EffectivePerennialCropCycle cycle) {

        if (cycleDto.getSpeciesDtos() != null) {
            Collection<EffectiveCropCycleSpecies> cycleSpeciesList = cycle.getSpecies();
            if (cycleSpeciesList == null) {
                cycleSpeciesList = new ArrayList<>();
                cycle.setSpecies(cycleSpeciesList);
            }
            Collection<EffectiveCropCycleSpecies> nonDeletedSpecies = new ArrayList<>();
            Map<String, EffectiveCropCycleSpecies> idToSpecies = Maps.uniqueIndex(cycleSpeciesList, Entities.GET_TOPIA_ID::apply);
            for (EffectiveCropCycleSpeciesDto speciesDto : cycleDto.getSpeciesDtos()) {
                String cycleSpeciesId = speciesDto.getTopiaId();
                EffectiveCropCycleSpecies cycleSpecies;
                if (StringUtils.isBlank(cycleSpeciesId)) {
                    cycleSpecies = effectiveCropCycleSpeciesDao.newInstance();
                } else {
                    cycleSpecies = idToSpecies.get(cycleSpeciesId);
                }
                if (cycleSpecies == null) {
                    throw new AgrosystRuntimeException(String.format("Required species with id '%s' on effective crop cycle '%s' is missing.", cycleSpeciesId, cycle.getTopiaId()));
                }

                String targetedCroppingPlanSpeciesId = speciesDto.getCroppingPlanSpeciesId();
                if (StringUtils.isNotBlank(targetedCroppingPlanSpeciesId) && availableCroppingPlanSpecies.containsKey(targetedCroppingPlanSpeciesId)) {
                    CroppingPlanSpecies croppingPlanSpecies = croppingPlanSpeciesDao.forTopiaIdEquals(targetedCroppingPlanSpeciesId).findUnique();
                    cycleSpecies.setCroppingPlanSpecies(croppingPlanSpecies);
                } else {
                    // EffectiveCropCycleSpecies related to a species than not belong to the zone
                    continue;
                }

                cycleSpecies.setPlantsCertified(speciesDto.isPlantsCertified());
                cycleSpecies.setOverGraftDate(speciesDto.getOverGraftDate());

                RefClonePlantGrape cloneGrape = null;
                if (speciesDto.getGraftClone() != null && StringUtils.isNotBlank(speciesDto.getGraftClone().getTopiaId())) {
                    cloneGrape = refClonePlantGrapeDao.forTopiaIdEquals(speciesDto.getGraftClone().getTopiaId()).findUnique();
                }
                cycleSpecies.setGraftClone(cloneGrape);

                RefVariete variete = null;
                if (speciesDto.getGraftSupport() != null && StringUtils.isNotBlank(speciesDto.getGraftSupport().getTopiaId())) {
                    variete = refVarieteDao.forTopiaIdEquals(speciesDto.getGraftSupport().getTopiaId()).findUnique();
                }
                cycleSpecies.setGraftSupport(variete);

                if (StringUtils.isBlank(cycleSpeciesId)) {
                    cycle.addSpecies(cycleSpecies);
                }
                nonDeletedSpecies.add(cycleSpecies);
            }
            cycleSpeciesList.retainAll(nonDeletedSpecies);
        }
    }

    private void createOrUpdatePhase(CreateOrUpdateEffectiveCroCyclesContext context) {
        EffectivePerennialCropCycleDto cycleDto = context.getEffectivePerennialCropCycleDto();
        EffectivePerennialCropCycle cycle = context.getEffectivePerennialCropCycle();

        EffectiveCropCyclePhase phase = cycle.getPhase();

        if (phase == null) {
            phase = effectiveCropCyclePhaseDao.newInstance();
        }

        Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = context.getSpeciesCodeToCodeEspeceBotaniqueCodeQualifiantAee();
        Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant = context.getSectorByCodeEspceBotaniqueCodeQualifiant();

        EffectiveCropCyclePhaseDto phaseDto = cycleDto.getPhaseDtos().getFirst();// for the moment there is only one phase

        phase.setDuration(phaseDto.getDuration());
        phase.setType(phaseDto.getType());
        phase.setEdaplosIssuerId(phaseDto.getEdaplosIssuerId());

        // phases must be persisted for intervention saving
        if (phase.isPersisted()) {
            phase = effectiveCropCyclePhaseDao.update(phase);
        } else {
            phase = effectiveCropCyclePhaseDao.create(phase);
        }
        cycle.setPhase(phase);
        // phase's interventions
        CreateOrUpdateEffectiveInterventionContext updateInterventionsResults = updateEffectiveInterventions(
                phaseDto.getInterventions(),
                null,
                null,
                cycle,
                phase,
                context.getDomain(),
                context.getZone(),
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspceBotaniqueCodeQualifiant,
                context.getToolsCouplingsByCode(),
                context.getDomainInputStocks());

        if (updateInterventionsResults.getIdsToInterventions() != null) {
            removeEffectiveInterventions(updateInterventionsResults.getIdsToInterventions().values());
        }
        addSavedValorisationsToContext(context, updateInterventionsResults);

    }

    private RefOrientationEDI loadRefOrientationEDI(EffectivePerennialCropCycleDto cycleDto) {
        RefOrientationEDI orientation = null;
        if (cycleDto.getOrientationId() != null) {
            orientation = refOrientationEDIDao.forTopiaIdEquals(cycleDto.getOrientationId()).findUnique();
        }
        return orientation;
    }

    private EffectivePerennialCropCycle getEffectivePerennialCropCycle(Zone zone, List<EffectivePerennialCropCycle> toDelete, Map<String, EffectivePerennialCropCycle> idToCycles, String dtoId) {
        EffectivePerennialCropCycle cycle;
        if (StringUtils.isBlank(dtoId)) {
            cycle = effectivePerennialCropCycleDao.newInstance();
            cycle.setZone(zone);
        } else {
            cycle = idToCycles.get(dtoId);
            toDelete.remove(cycle);
        }
        return cycle;
    }


    protected void removeEffectivePerennialCropCycles(List<EffectivePerennialCropCycle> effectivePerennialCropCycles) {
        if (effectivePerennialCropCycles != null) {
            for (EffectivePerennialCropCycle effectivePerennialCropCycle : effectivePerennialCropCycles) {
                EffectiveCropCyclePhase phase = effectivePerennialCropCycle.getPhase();
                removeEffectiveCropCyclePhaseChildrenObjects(phase);
            }
            effectivePerennialCropCycleDao.deleteAll(effectivePerennialCropCycles);
        }
    }

    protected void removeEffectiveCropCyclePhaseChildrenObjects(EffectiveCropCyclePhase phase) {
        List<EffectiveIntervention> effectiveInterventions = effectiveInterventionDao.forEffectiveCropCyclePhaseEquals(phase).findAll();
        removeEffectiveInterventions(effectiveInterventions);
    }

    private void removeEffectiveInterventions(Collection<EffectiveIntervention> effectiveInterventions) {
        if (effectiveInterventions != null) {

            List<AbstractAction> allActionsToRemove = new ArrayList<>();
            for (EffectiveIntervention intervention : effectiveInterventions) {
                List<AbstractAction> abstractActions = abstractActionDao.forEffectiveInterventionEquals(intervention).findAll();
                // ne peut-être null
                allActionsToRemove.addAll(abstractActions);
            }
            actionService.removeActionsAndUsagesDeps(allActionsToRemove);
            effectiveInterventionDao.deleteAll(effectiveInterventions);
        }
    }

    /**
     * Persist interventions on node OR phase.
     */
    protected CreateOrUpdateEffectiveInterventionContext updateEffectiveInterventions(
            List<EffectiveInterventionDto> interventionDtos,
            EffectiveSeasonalCropCycleDto seasonalCropCycleDto,
            Pair<String, EffectiveCropCycleNode> nodeIdToNode,
            EffectivePerennialCropCycle perennialCropCycle,
            EffectiveCropCyclePhase phase,
            Domain domain,
            Zone zone,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, ToolsCoupling> toolsCouplingsByCode,
            Collection<AbstractDomainInputStockUnit> domainInputStockUnits) {

        // either one of node or phase MUST be null (but not both)
        Preconditions.checkArgument(nodeIdToNode == null ^ phase == null);

        interventionDtos = interventionDtos != null ? interventionDtos : new ArrayList<>();

        // remove new interventionDtos witch domainId doesn't match the domain topiaId (to avoid copy/paste of intervention from another domain).
        filterOnDomain(interventionDtos, domain);

        CroppingPlanEntry croppingPlanEntry = null;
        CroppingPlanEntry intermediateCrop = null;

        Optional<EffectiveCropCycleConnectionDto> optionalCropCycleConnectionDto = Optional.empty();
        Collection<EffectiveIntervention> interventions = null;
        if (nodeIdToNode != null) {
            String nodeId = nodeIdToNode.getLeft();
            croppingPlanEntry = nodeIdToNode.getRight().getCroppingPlanEntry();
            optionalCropCycleConnectionDto = CollectionUtils.emptyIfNull(seasonalCropCycleDto.getConnectionDtos()).stream()
                    .filter(cdto -> nodeId.contentEquals(Entities.UNESCAPE_TOPIA_ID.apply(cdto.getTargetId()))).findAny();
            if (optionalCropCycleConnectionDto.isPresent() && optionalCropCycleConnectionDto.get().getIntermediateCroppingPlanEntryId() != null) {
                String intermediateCroppingPlanEntryId = optionalCropCycleConnectionDto.get().getIntermediateCroppingPlanEntryId();
                intermediateCrop = croppingPlanEntryDao.forTopiaIdEquals(intermediateCroppingPlanEntryId).findUnique();
            }
            if (nodeIdToNode.getRight().isPersisted()) {
                interventions = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(nodeIdToNode.getRight()).findAll();
            }
        } else if (phase.isPersisted()) { // can be null from test
            croppingPlanEntry = perennialCropCycle.getCroppingPlanEntry();
            interventions = effectiveInterventionDao.forEffectiveCropCyclePhaseEquals(phase).findAll();
        }
        interventions = CollectionUtils.emptyIfNull(interventions);

        Map<String, EffectiveIntervention> idsToInterventions = Maps.newHashMap(Maps.uniqueIndex(interventions, Entities.GET_TOPIA_ID::apply));

        List<EffectiveCropCycleConnectionDto> connectionDtos = optionalCropCycleConnectionDto.isPresent() ? optionalCropCycleConnectionDto.stream().toList() : null;

        CreateOrUpdateEffectiveInterventionContext createOrUpdateEffectiveInterventionContext = new CreateOrUpdateEffectiveInterventionContext(
                interventionDtos,
                connectionDtos,
                nodeIdToNode,
                perennialCropCycle,
                phase,
                domain,
                zone,
                idsToInterventions,
                domainInputStockUnits,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspceBotaniqueCodeQualifiant,
                toolsCouplingsByCode,
                croppingPlanEntry,
                intermediateCrop);

        saveInterventions(createOrUpdateEffectiveInterventionContext);

        return createOrUpdateEffectiveInterventionContext;
    }

    protected void filterOnDomain(List<EffectiveInterventionDto> interventionDtos, Domain domain) {
        interventionDtos.removeIf(
                interventionDto -> interventionDto.getTopiaId().contains(NEW_INTERVENTION_PREFIX)
                        && !interventionDto.getDomainId().equals(domain.getTopiaId()));
    }

    private boolean saveInterventions(
            CreateOrUpdateEffectiveInterventionContext createOrUpdateEffectiveInterventionContext) {

        boolean allToolsCouplingsAndInputsCopied = true;

        Map<String, EffectiveIntervention> idsToInterventions = createOrUpdateEffectiveInterventionContext.getIdsToInterventions();
        Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = createOrUpdateEffectiveInterventionContext.getSpeciesCodeToCodeEspeceBotaniqueCodeQualifiantAee();
        Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant = createOrUpdateEffectiveInterventionContext.getSectorByCodeEspceBotaniqueCodeQualifiant();
        CroppingPlanEntry mainCrop = createOrUpdateEffectiveInterventionContext.getMainCrop();
        CroppingPlanEntry intermediateCrop = createOrUpdateEffectiveInterventionContext.getIntermediateCrop();

        for (EffectiveInterventionDto interventionDto : createOrUpdateEffectiveInterventionContext.getInterventionDtos()) {

            // create new intervention instance
            final Pair<String, EffectiveCropCycleNode> nodeIdToNode = createOrUpdateEffectiveInterventionContext.getNodeIdToNode();

            final String interventionDtoTopiaId = interventionDto.getTopiaId();

            EffectiveIntervention intervention = getOrCreateIntervention(
                    createOrUpdateEffectiveInterventionContext,
                    interventionDtoTopiaId,
                    nodeIdToNode,
                    idsToInterventions);

            if (intervention != null) {

                CroppingPlanEntry interventionCrop = getCroppingPlanEntry(mainCrop, intermediateCrop, interventionDto);

                Map<String, CroppingPlanSpecies> phaseOrNodeSpeciesMap = getPhaseOrNodeCroppingPlanSpeciesByCode(
                        interventionDto.isIntermediateCrop(),
                        nodeIdToNode,
                        createOrUpdateEffectiveInterventionContext.getConnectionDtos(),
                        createOrUpdateEffectiveInterventionContext.getPerennialCropCycle());

                Map<String, ToolsCoupling> targetedDomainToolsCouplings = createOrUpdateEffectiveInterventionContext.getToolsCouplingByCode();

                boolean toolsCouplingAndInputsCopied = bindDataToIntervention(
                        interventionDto,
                        intervention,
                        targetedDomainToolsCouplings,
                        phaseOrNodeSpeciesMap,
                        interventionCrop);

                if (!toolsCouplingAndInputsCopied) {
                    allToolsCouplingsAndInputsCopied = false;
                }

                EffectiveIntervention persistedIntervention;

                if (interventionDtoTopiaId.contains(NEW_INTERVENTION_PREFIX)) {
                    persistedIntervention = effectiveInterventionDao.create(intervention);
                } else {
                    persistedIntervention = effectiveInterventionDao.update(intervention);
                }

                actionService.createOrUpdateEffectiveInterventionActionAndUsages(
                        persistedIntervention,
                        interventionDto.getActionDtos(),
                        createOrUpdateEffectiveInterventionContext.getDomainInputStockUnits(),
                        createOrUpdateEffectiveInterventionContext.getZone(),
                        speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                        sectorByCodeEspceBotaniqueCodeQualifiant,
                        interventionDto.getSpeciesStadesDtos(),
                        interventionCrop,
                        interventionDto.isIntermediateCrop());

            } else {
                LOGGER.error(String.format("Intervention with id %s and name %s could not be found", interventionDtoTopiaId, interventionDto.getName()));
            }
        }

        return allToolsCouplingsAndInputsCopied;
    }

    protected EffectiveIntervention getOrCreateIntervention(
            CreateOrUpdateEffectiveInterventionContext createOrUpdateEffectiveInterventionContext,
            String interventionDtoTopiaId,
            Pair<String, EffectiveCropCycleNode> nodeIdToNode,
            Map<String, EffectiveIntervention> idsToInterventions) {

        EffectiveIntervention intervention;
        if (StringUtils.isBlank(interventionDtoTopiaId) ||
                interventionDtoTopiaId.startsWith(NEW_INTERVENTION_PREFIX)) {

            intervention = effectiveInterventionDao.newInstance();

            if (nodeIdToNode != null) {
                EffectiveCropCycleNode node = nodeIdToNode.getRight();
                intervention.setEffectiveCropCycleNode(node);
            } else {
                final EffectiveCropCyclePhase phase = createOrUpdateEffectiveInterventionContext.getPhase();
                intervention.setEffectiveCropCyclePhase(phase);
            }

        } else {
            intervention = idsToInterventions.remove(interventionDtoTopiaId);
        }
        return intervention;
    }

    protected static CroppingPlanEntry getCroppingPlanEntry(CroppingPlanEntry mainCrop, CroppingPlanEntry intermediateCrop, EffectiveInterventionDto effectiveInterventionDto) {
        CroppingPlanEntry interventionCrop;
        if (effectiveInterventionDto.isIntermediateCrop()) {
            if (intermediateCrop != null) {
                interventionCrop = intermediateCrop;
            } else {
                interventionCrop = mainCrop;
                effectiveInterventionDto.setIntermediateCrop(false);
            }
        } else {
            interventionCrop = mainCrop;
        }
        return interventionCrop;
    }

    protected boolean bindDataToIntervention(
            EffectiveInterventionDto interventionDto,
            EffectiveIntervention intervention,
            Map<String, ToolsCoupling> targetedDomainToolsCouplings,
            Map<String, CroppingPlanSpecies> phaseOrNodeSpeciesBySpeciesCode,
            CroppingPlanEntry interventionCrop) {

        Binder<EffectiveInterventionDto, EffectiveIntervention> binder = BinderFactory.newBinder(EffectiveInterventionDto.class, EffectiveIntervention.class);

        binder.copyExcluding(interventionDto, intervention,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                EffectiveIntervention.PROPERTY_TOOL_COUPLINGS,
                EffectiveIntervention.PROPERTY_SPECIES_STADES,
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_NODE,
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE);

        // intervention tools couplings
        boolean allToolsCouplingsFound = bindToolsCouplingToIntervention(
                interventionDto,
                intervention,
                targetedDomainToolsCouplings);

        // intervention speciesStade
        boolean allSpeciesFound = bindSpeciesStadesToIntervention(
                interventionDto,
                intervention,
                phaseOrNodeSpeciesBySpeciesCode,
                interventionCrop);

        return allToolsCouplingsFound && allSpeciesFound;
    }

    protected boolean bindSpeciesStadesToIntervention(
            EffectiveInterventionDto interventionDto,
            EffectiveIntervention intervention,
            Map<String, CroppingPlanSpecies> phaseOrNodeSpeciesBySpeciesCode,
            CroppingPlanEntry interventionCrop) {

        Collection<EffectiveSpeciesStade> speciesStades = intervention.getSpeciesStades();
        if (speciesStades == null) {
            speciesStades = new ArrayList<>();
            intervention.setSpeciesStades(speciesStades);
        }

        Map<String, CroppingPlanSpecies> phaseOrNodeSpeciesMap = Maps.newHashMap(phaseOrNodeSpeciesBySpeciesCode);

        // auto add default speciesStades
        autoAddRegularSpeciesStades(interventionDto, phaseOrNodeSpeciesMap);

        boolean allSpeciesFound = bindSpeciesStades(
                interventionDto,
                speciesStades,
                phaseOrNodeSpeciesMap
        );

        return allSpeciesFound;
    }

    protected boolean bindSpeciesStades(
            EffectiveInterventionDto interventionDto,
            Collection<EffectiveSpeciesStade> speciesStades,
            Map<String, CroppingPlanSpecies> phaseOrNodeSpeciesMap) {

        boolean allSpeciesFound = true;

        if (interventionDto.getSpeciesStadesDtos() != null) {
            Collection<EffectiveSpeciesStade> nonDeletedStades = new ArrayList<>();

            Map<String, EffectiveSpeciesStade> idToSpeciesStade = new HashMap<>();
            for (EffectiveSpeciesStade speciesStade : speciesStades) {
                // because in case of new speciesStades topiaId is null
                if (speciesStade.isPersisted()) {
                    idToSpeciesStade.put(speciesStade.getTopiaId(), speciesStade);
                }
            }

            for (SpeciesStadeDto speciesStadeDto : interventionDto.getSpeciesStadesDtos()) {

                CroppingPlanSpecies croppingPlanSpecies = null;
                String speciesCode = speciesStadeDto.getSpeciesCode();
                if (speciesCode != null) {
                    croppingPlanSpecies = phaseOrNodeSpeciesMap.get(speciesCode);
                }

                // if the croppingPlanSpecies was not found on the available one, SpeciesStade is not valid.
                if (croppingPlanSpecies == null) {
                    allSpeciesFound = false;
                    continue;
                }

                EffectiveSpeciesStade effectiveSpeciesStade = createOrGetEffectiveSpeciesStade(
                        speciesStades,
                        idToSpeciesStade,
                        speciesStadeDto,
                        croppingPlanSpecies);
                nonDeletedStades.add(effectiveSpeciesStade);
            }
            // remove all that were not part of intervention dto
            speciesStades.retainAll(nonDeletedStades);
        }
        return allSpeciesFound;
    }

    protected void autoAddRegularSpeciesStades(
            EffectiveInterventionDto interventionDto,
            Map<String, CroppingPlanSpecies> phaseOrNodeSpeciesMap) {

        if (interventionDto.getSpeciesStadesDtos() == null) {
            // in case of no species stades where defined, default ones are created with the crop species.
            // auto add dto to interventionDto
            List<SpeciesStadeDto> speciesStadesDtos = new ArrayList<>();
            interventionDto.setSpeciesStadesDtos(speciesStadesDtos);

            for (CroppingPlanSpecies croppingPlanSpecies : phaseOrNodeSpeciesMap.values()) {
                SpeciesStadeDto speciesStadeDto = new SpeciesStadeDto();
                speciesStadeDto.setSpeciesCode(croppingPlanSpecies.getCode());
                speciesStadesDtos.add(speciesStadeDto);
            }
        }
    }

    protected EffectiveSpeciesStade createOrGetEffectiveSpeciesStade(
            Collection<EffectiveSpeciesStade> speciesStades,
            Map<String, EffectiveSpeciesStade> idToSpeciesStade,
            SpeciesStadeDto speciesStadeDto,
            CroppingPlanSpecies croppingPlanSpecies) {

        String speciesStadeId = speciesStadeDto.getTopiaId();

        EffectiveSpeciesStade effectiveSpeciesStade = null;
        if (StringUtils.isNotBlank(speciesStadeId)) {
            effectiveSpeciesStade = idToSpeciesStade.get(speciesStadeId);

        }
        if (effectiveSpeciesStade == null) {
            effectiveSpeciesStade = effectiveSpeciesStadeDao.newInstance();
        }

        effectiveSpeciesStade.setCroppingPlanSpecies(croppingPlanSpecies);

        RefStadeEDI minStade = null;
        RefStadeEdiDto stadeMinFromDto = speciesStadeDto.getStadeMin();
        if (stadeMinFromDto != null) {
            minStade = refStadeEDIDao.forTopiaIdEquals(stadeMinFromDto.getTopiaId()).findUnique();
        }
        effectiveSpeciesStade.setMinStade(minStade);
        RefStadeEDI maxStade = null;
        RefStadeEdiDto stadeMaxFromDto = speciesStadeDto.getStadeMax();
        if (stadeMaxFromDto != null) {
            maxStade = refStadeEDIDao.forTopiaIdEquals(stadeMaxFromDto.getTopiaId()).findUnique();
        }
        effectiveSpeciesStade.setMaxStade(maxStade);

        if (StringUtils.isBlank(speciesStadeId)) {
            speciesStades.add(effectiveSpeciesStade);
        }

        if (effectiveSpeciesStade.isPersisted()) {
            effectiveSpeciesStadeDao.update(effectiveSpeciesStade);
        } else {
            effectiveSpeciesStadeDao.create(effectiveSpeciesStade);
        }
        return effectiveSpeciesStade;
    }

    private boolean bindToolsCouplingToIntervention(EffectiveInterventionDto interventionDto, EffectiveIntervention intervention, Map<String, ToolsCoupling> targetedDomainToolsCouplings) {

        boolean allToolsCouplingsFound = true;

        if (interventionDto.getToolsCouplingCodes() != null) {
            Collection<ToolsCoupling> toolsCouplings = intervention.getToolCouplings();
            if (toolsCouplings == null) {
                toolsCouplings = new ArrayList<>();
                intervention.setToolCouplings(toolsCouplings);
            }
            toolsCouplings.clear();
            for (String toolsCouplingCode : interventionDto.getToolsCouplingCodes()) {
                ToolsCoupling toolsCoupling = targetedDomainToolsCouplings.get(toolsCouplingCode);
                if (toolsCoupling != null) {
                    toolsCouplings.add(toolsCoupling);
                } else {
                    allToolsCouplingsFound = false;
                }
            }
        }
        return allToolsCouplingsFound;
    }

    private Map<String, CroppingPlanSpecies> getPhaseOrNodeCroppingPlanSpeciesByCode(
            boolean isIntermediate,
            Pair<String, EffectiveCropCycleNode> nodeIdToNode,
            List<EffectiveCropCycleConnectionDto> connectionDtos,
            EffectivePerennialCropCycle perennialCropCycle) {

        // find all species from cycle crop
        Set<CroppingPlanSpecies> allowedSpecies = Sets.newHashSet();
        if (nodeIdToNode != null) {

            // add species from intermediate crops
            if (isIntermediate && connectionDtos != null) {
                if (CollectionUtils.isNotEmpty(connectionDtos)) {
                    Map<String, CroppingPlanEntry> loadedCPEcach = new HashMap<>();

                    for (EffectiveCropCycleConnectionDto connectionDto : connectionDtos) {

                        final String intermediateCroppingPlanEntryId = connectionDto.getIntermediateCroppingPlanEntryId();

                        String targetedNodeId = intermediateCroppingPlanEntryId != null
                                ? Entities.UNESCAPE_TOPIA_ID.apply(connectionDto.getTargetId()) :
                                null;

                        if (targetedNodeId != null && targetedNodeId.equals(nodeIdToNode.getLeft())) {
                            CroppingPlanEntry cpe = loadedCPEcach.get(intermediateCroppingPlanEntryId);
                            if (cpe == null) {
                                cpe = croppingPlanEntryDao.forTopiaIdEquals(intermediateCroppingPlanEntryId).findUnique();
                                loadedCPEcach.put(cpe.getTopiaId(), cpe);
                            }
                            if (cpe.getCroppingPlanSpecies() != null) {
                                allowedSpecies.addAll(cpe.getCroppingPlanSpecies());
                            }
                        }
                    }
                }
            } else {
                // all species from node
                allowedSpecies = Sets.newHashSet(CollectionUtils.emptyIfNull(nodeIdToNode.getRight().getCroppingPlanEntry().getCroppingPlanSpecies()));
            }
        } else {
            if (perennialCropCycle != null) {
                allowedSpecies = Sets.newHashSet(CollectionUtils.emptyIfNull(perennialCropCycle.getCroppingPlanEntry().getCroppingPlanSpecies()));
            }
        }
        Collection<CroppingPlanSpecies> filtratedAllowedSpecies = Collections2.filter(allowedSpecies, Objects::nonNull);
        return CollectionUtils.isNotEmpty(filtratedAllowedSpecies) ? Maps.uniqueIndex(filtratedAllowedSpecies, GET_CROPPING_PLAN_SPECIES_CODE::apply) : new HashMap<>(0);
    }

    private List<ToolsCoupling> getToolsCouplingsForZone(String zoneTopiaId) {
        Zone zone = zoneDao.forTopiaIdEquals(zoneTopiaId).findUnique();
        Domain domain = zone.getPlot().getDomain();
        List<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(domain).findAll();
        i18nService.fillToolsCouplingTraductions(toolsCouplings);
        return toolsCouplings;
    }

    @Override
    public Map<AgrosystInterventionType, List<ToolsCouplingDto>> getToolsCouplingModelForInterventionTypes(String zoneTopiaId, Set<AgrosystInterventionType> interventionTypes) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(interventionTypes));

        Map<AgrosystInterventionType, List<ToolsCouplingDto>> result = new HashMap<>();
        List<ToolsCoupling> toolsCouplings = getToolsCouplingsForZone(zoneTopiaId);

        for (AgrosystInterventionType interventionType : interventionTypes) {
            List<ToolsCouplingDto> resultForInterventionType = getToolsCouplingDtosForInterventionType(interventionType, toolsCouplings);
            result.put(interventionType, resultForInterventionType);
        }

        return result;
    }

    protected List<ToolsCouplingDto> getToolsCouplingDtosForInterventionType(AgrosystInterventionType interventionType, List<ToolsCoupling> toolsCouplings) {
        Iterable<ToolsCoupling> filtered = toolsCouplings.stream().filter(toolsCoupling -> interventionType == null || Iterables.tryFind(toolsCoupling.getMainsActions(), mainAction -> mainAction != null && interventionType.equals(mainAction.getIntervention_agrosyst())).isPresent()).collect(Collectors.toList());
        return StreamSupport.stream(filtered.spliterator(), false).map(Equipments.TOOLS_COUPLING_TO_TOOLS_COUPLING_DTO).collect(Collectors.toList());
    }

    @Override
    public void duplicateEffectiveCropCycles(String fromZoneId, String toZoneId) {

        Preconditions.checkArgument(!Strings.isNullOrEmpty(fromZoneId));
        Preconditions.checkArgument(!Strings.isNullOrEmpty(toZoneId));

        Zone toZone = zoneDao.forTopiaIdEquals(toZoneId).findUnique();

        checkPreconditionOnActive(toZoneId, toZone);
        final Domain toDomain = toZone.getPlot().getDomain();

        final List<DomainInputDto> toInputStockUnits = domainService.getInputStockUnits(toDomain.getTopiaId());
        Map<String, DomainInputDto> toInputStockUnitByCodes = toInputStockUnits.stream().collect(
                Collectors.toMap(DomainInputDto::getCode, Function.identity()));

        List<EffectiveSeasonalCropCycleDto> fromEffectiveSeasonalCropCycleDtos = getAllEffectiveSeasonalCropCyclesDtos(fromZoneId);
        List<EffectivePerennialCropCycleDto> fromEffectivePerennialCropCycleDtos = getAllEffectivePerennialCropCyclesDtos(fromZoneId);

        List<CroppingPlanEntry> toZoneCroppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(toDomain);
        List<EffectiveSeasonalCropCycleDto> duplicateEffectiveSeasonalCropCycleDtos = toNewEffectiveSeasonalCropCycleDtos(
                fromZoneId,
                toZoneId,
                toDomain,
                toInputStockUnitByCodes,
                fromEffectiveSeasonalCropCycleDtos,
                toZoneCroppingPlanEntries);

        List<EffectivePerennialCropCycleDto> duplicateEffectivePerennialCropCycleDtos = toNewEffectivePerennialCropCycleDtos(
                fromZoneId,
                toZoneId,
                toDomain,
                toInputStockUnitByCodes,
                fromEffectivePerennialCropCycleDtos,
                toZoneCroppingPlanEntries);

        List<EffectiveSeasonalCropCycleDto> toSeasonalCropCycleDtos = getAllEffectiveSeasonalCropCyclesDtos(toZoneId);
        List<EffectivePerennialCropCycleDto> toPerennialCropCycleDtos = getAllEffectivePerennialCropCyclesDtos(toZoneId);

        toSeasonalCropCycleDtos.addAll(duplicateEffectiveSeasonalCropCycleDtos);
        toPerennialCropCycleDtos.addAll(duplicateEffectivePerennialCropCycleDtos);

        // Create context and let's update all this !
        Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(toZone);
        Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant = getSectorByCodeEspceBotaniqueCodeQualifiant(toZone);
        List<ToolsCoupling> toToolsCouplings = domainService.getToolsCouplings(toDomain.getTopiaId());
        Map<String, ToolsCoupling> toToolsCouplingByCode = Maps.uniqueIndex(toToolsCouplings, ToolsCoupling::getCode);

        final Map<String, AbstractDomainInputStockUnit> toDomainInputStockByIds = domainInputStockUnitService.loadDomainInputStockByIds(toDomain);
        Collection<AbstractDomainInputStockUnit> toDomainInputStockUnits = toDomainInputStockByIds.values().stream().toList();

        CreateOrUpdateEffectiveCroCyclesContext context = new CreateOrUpdateEffectiveCroCyclesContext(
                toZone,
                toSeasonalCropCycleDtos,
                toPerennialCropCycleDtos,
                toZoneCroppingPlanEntries,
                toToolsCouplingByCode,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspceBotaniqueCodeQualifiant,
                toDomainInputStockUnits);

        updateEffectiveSeasonalCropCycles(context);
        updateEffectivePerennialCropCycles(context);

        getTransaction().commit();
    }

    private List<EffectivePerennialCropCycleDto> toNewEffectivePerennialCropCycleDtos(
            String fromZoneId,
            String toZoneId,
            Domain toDomain,
            Map<String, DomainInputDto> toInputStockUnitByCodes,
            List<EffectivePerennialCropCycleDto> fromEffectivePerennialCropCycleDtos,
            List<CroppingPlanEntry> toZoneCroppingPlanEntries) {

        List<EffectivePerennialCropCycleDto> newEffectivePerennialCropCycleDtos = new ArrayList<>();

        Binder<EffectivePerennialCropCycleDto, EffectivePerennialCropCycleDto> effectivePerennialCropCycleDtoBinder = BinderFactory.newBinder(EffectivePerennialCropCycleDto.class);
        Binder<EffectiveCropCyclePhaseDto, EffectiveCropCyclePhaseDto> phaseDtoBinder = BinderFactory.newBinder(EffectiveCropCyclePhaseDto.class);
        Binder<EffectiveCropCycleSpeciesDto, EffectiveCropCycleSpeciesDto> effectiveCropCycleSpeciesDtoBinder = BinderFactory.newBinder(EffectiveCropCycleSpeciesDto.class);

        Map<String, CroppingPlanEntry> toZoneCroppingPlanEntriesByCodes = Maps.uniqueIndex(toZoneCroppingPlanEntries, CroppingPlanEntry::getCode);

        Map<String, CroppingPlanEntry> fromPerennialCycleCropsByIds = getPerennialCropCycleCropByIds(fromEffectivePerennialCropCycleDtos);

        for (EffectivePerennialCropCycleDto fromEffectivePerennialCropCycleDto : fromEffectivePerennialCropCycleDtos) {

            CroppingPlanEntry fromCroppingPlanEntry = fromPerennialCycleCropsByIds.get(fromEffectivePerennialCropCycleDto.getCroppingPlanEntryId());

            CroppingPlanEntry toCroppingPlanEntry = toZoneCroppingPlanEntriesByCodes.get(fromCroppingPlanEntry.getCode());
            if (toCroppingPlanEntry == null) {
                continue;
            }

            EffectivePerennialCropCycleDto newEffectivePerennialCropCycleDto = new EffectivePerennialCropCycleDto();
            effectivePerennialCropCycleDtoBinder.copyExcluding(
                    fromEffectivePerennialCropCycleDto,
                    newEffectivePerennialCropCycleDto,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                    "phaseDtos",
                    "speciesDtos",
                    "croppingPlanEntryId");
            newEffectivePerennialCropCycleDto.setCroppingPlanEntryId(toCroppingPlanEntry.getTopiaId());

            newEffectivePerennialCropCycleDtos.add(newEffectivePerennialCropCycleDto);

            List<EffectiveCropCyclePhaseDto> fromPhases = fromEffectivePerennialCropCycleDto.getPhaseDtos();
            for (EffectiveCropCyclePhaseDto fromPhase : fromPhases) {
                EffectiveCropCyclePhaseDto newPhaseDto = new EffectiveCropCyclePhaseDto();
                phaseDtoBinder.copyExcluding(
                        fromPhase,
                        newPhaseDto,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        "interventions",
                        "speciesDtos",
                        "edaplosIssuerId"
                );//Do not duplicated eDaplosIssuerId
                newEffectivePerennialCropCycleDto.addPhase(newPhaseDto);

                List<EffectiveInterventionDto> fromInterventionDtos = fromPhase.getInterventions();
                newPhaseDto.setInterventions(
                        toNewInterventionDtos(
                                fromInterventionDtos,
                                getYearDifference(fromZoneId, toZoneId),
                                toDomain,
                                toInputStockUnitByCodes));
            }
            List<EffectiveCropCycleSpeciesDto> fromEffectiveCropCycleSpeciesDtos = fromEffectivePerennialCropCycleDto.getSpeciesDtos();
            for (EffectiveCropCycleSpeciesDto fromEffectiveCropCycleSpeciesDto : fromEffectiveCropCycleSpeciesDtos) {
                EffectiveCropCycleSpeciesDto newEffectiveCropCycleSpeciesDto = new EffectiveCropCycleSpeciesDto();
                effectiveCropCycleSpeciesDtoBinder.copyExcluding(
                        fromEffectiveCropCycleSpeciesDto,
                        newEffectiveCropCycleSpeciesDto,
                        "topiaId");
                newEffectivePerennialCropCycleDto.addSpeciesDto(newEffectiveCropCycleSpeciesDto);
            }
        }
        return newEffectivePerennialCropCycleDtos;
    }

    private Map<String, CroppingPlanEntry> getPerennialCropCycleCropByIds(List<EffectivePerennialCropCycleDto> fromEffectivePerennialCropCycleDtos) {
        Set<String> perennialCropIds = fromEffectivePerennialCropCycleDtos.stream()
                .map(EffectivePerennialCropCycleDto::getCroppingPlanEntryId)
                .collect(Collectors.toSet());
        Map<String, CroppingPlanEntry> fromPerennialCycleCropsByIds = croppingPlanEntryDao.forTopiaIdIn(perennialCropIds).findAll()
                .stream()
                .collect(Collectors.toMap(TopiaEntity::getTopiaId, Function.identity()));
        return fromPerennialCycleCropsByIds;
    }

    private List<EffectiveSeasonalCropCycleDto> toNewEffectiveSeasonalCropCycleDtos(
            String fromZoneId,
            String toZoneId,
            Domain toDomain,
            Map<String, DomainInputDto> toInputStockUnitByCodes,
            List<EffectiveSeasonalCropCycleDto> fromEffectiveSeasonalCropCycleDtos,
            List<CroppingPlanEntry> toZoneCroppingPlanEntries) {

        List<EffectiveSeasonalCropCycleDto> newEffectiveSeasonalCropCycleDtos = new ArrayList<>();

        Binder<EffectiveSeasonalCropCycleDto, EffectiveSeasonalCropCycleDto> effectiveSeasonalCropCycleDtoBinder = BinderFactory.newBinder(EffectiveSeasonalCropCycleDto.class);
        Binder<EffectiveCropCycleNodeDto, EffectiveCropCycleNodeDto> effectiveCropCycleNodeDtoBinder = BinderFactory.newBinder(EffectiveCropCycleNodeDto.class);
        Binder<EffectiveCropCycleConnectionDto, EffectiveCropCycleConnectionDto> effectiveCropCycleConnectionDtoBinder = BinderFactory.newBinder(EffectiveCropCycleConnectionDto.class);

        Map<String, CroppingPlanEntry> toZoneCroppingPlanEntriesByCodes = Maps.uniqueIndex(toZoneCroppingPlanEntries, CroppingPlanEntry::getCode);

        Map<String, CroppingPlanEntry> fromSeasonalCropCycleCropByIds = getSeasonalCropCycleCropByIds(fromEffectiveSeasonalCropCycleDtos);

        for (EffectiveSeasonalCropCycleDto fromSeasonalCropCycleDto : fromEffectiveSeasonalCropCycleDtos) {

            EffectiveSeasonalCropCycleDto newSeasonalCropCycleDto = new EffectiveSeasonalCropCycleDto();
            effectiveSeasonalCropCycleDtoBinder.copyExcluding(
                    fromSeasonalCropCycleDto,
                    newSeasonalCropCycleDto,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                    "nodeDtos",
                    "connectionDtos");
            newEffectiveSeasonalCropCycleDtos.add(newSeasonalCropCycleDto);

            Map<String, EffectiveCropCycleNodeDto> newNodeDtoByOriginalIds = toNewEffectiveCropCycleNodeDto(
                    fromZoneId,
                    toZoneId,
                    toDomain,
                    toInputStockUnitByCodes,
                    effectiveCropCycleNodeDtoBinder,
                    toZoneCroppingPlanEntriesByCodes,
                    fromSeasonalCropCycleCropByIds,
                    fromSeasonalCropCycleDto,
                    newSeasonalCropCycleDto);

            toNewCropCycleConnectionDtos(
                    effectiveCropCycleConnectionDtoBinder,
                    toZoneCroppingPlanEntriesByCodes,
                    fromSeasonalCropCycleCropByIds,
                    fromSeasonalCropCycleDto,
                    newSeasonalCropCycleDto,
                    newNodeDtoByOriginalIds);

        }

        return newEffectiveSeasonalCropCycleDtos;
    }

    protected static void toNewCropCycleConnectionDtos(
            Binder<EffectiveCropCycleConnectionDto, EffectiveCropCycleConnectionDto> effectiveCropCycleConnectionDtoBinder,
            Map<String, CroppingPlanEntry> toZoneCroppingPlanEntriesByCodes,
            Map<String, CroppingPlanEntry> fromSeasonalCropCycleCropByIds,
            EffectiveSeasonalCropCycleDto fromSeasonalCropCycleDto,
            EffectiveSeasonalCropCycleDto newSeasonalCropCycleDto,
            Map<String, EffectiveCropCycleNodeDto> newNodeDtoByOriginalIds) {

        Collection<EffectiveCropCycleConnectionDto> newConnectionDtos = CollectionUtils.emptyIfNull(fromSeasonalCropCycleDto.getConnectionDtos());
        for (EffectiveCropCycleConnectionDto fromCycleConnectionDto : newConnectionDtos) {
            EffectiveCropCycleNodeDto newTargetEffectiveCropCycleNodeDto = newNodeDtoByOriginalIds.get(fromCycleConnectionDto.getTargetId());
            if (newTargetEffectiveCropCycleNodeDto == null) {
                continue;
            }

            String newIntermediateCroppingPlanEntryId = null;
            String fromIntermediateCroppingPlanEntryId = fromCycleConnectionDto.getIntermediateCroppingPlanEntryId();
            if (fromIntermediateCroppingPlanEntryId != null) {
                CroppingPlanEntry fromCroppingPlanEntry = fromSeasonalCropCycleCropByIds.get(fromIntermediateCroppingPlanEntryId);

                CroppingPlanEntry toCroppingPlanEntry = toZoneCroppingPlanEntriesByCodes.get(fromCroppingPlanEntry.getCode());
                if (toCroppingPlanEntry != null) {
                    newIntermediateCroppingPlanEntryId = toCroppingPlanEntry.getTopiaId();
                }

            }
            EffectiveCropCycleConnectionDto newCycleConnectionDto = new EffectiveCropCycleConnectionDto();
            effectiveCropCycleConnectionDtoBinder.copyExcluding(
                    fromCycleConnectionDto,
                    newCycleConnectionDto,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                    "sourceId",
                    "targetId",
                    "edaplosIssuerId",
                    "intermediateCroppingPlanEntryId");//Do not duplicated eDaplosIssuerId

            newSeasonalCropCycleDto.addConnectionDto(newCycleConnectionDto);

            EffectiveCropCycleNodeDto newSourceEffectiveCropCycleNodeDto = newNodeDtoByOriginalIds.get(fromCycleConnectionDto.getSourceId());
            String sourceId = newSourceEffectiveCropCycleNodeDto != null ? newSourceEffectiveCropCycleNodeDto.getNodeId() : null;
            newCycleConnectionDto.setSourceId(sourceId);
            newCycleConnectionDto.setTargetId(newTargetEffectiveCropCycleNodeDto.getNodeId());
            newCycleConnectionDto.setIntermediateCroppingPlanEntryId(newIntermediateCroppingPlanEntryId);

            if (StringUtils.isNotBlank(fromIntermediateCroppingPlanEntryId) && StringUtils.isBlank(newIntermediateCroppingPlanEntryId)) {
                List<EffectiveInterventionDto> interventions = newTargetEffectiveCropCycleNodeDto.getInterventions();
                interventions.stream().filter(EffectiveInterventionDto::isIntermediateCrop)
                        .forEach(inter -> inter.setIntermediateCrop(false));
            }

        }
    }

    protected Map<String, EffectiveCropCycleNodeDto> toNewEffectiveCropCycleNodeDto(
            String fromZoneId,
            String toZoneId,
            Domain toDomain,
            Map<String, DomainInputDto> toInputStockUnitByCodes,
            Binder<EffectiveCropCycleNodeDto, EffectiveCropCycleNodeDto> effectiveCropCycleNodeDtoBinder,
            Map<String, CroppingPlanEntry> toZoneCroppingPlanEntriesByCodes,
            Map<String, CroppingPlanEntry> fromSeasonalCropCycleCropByIds,
            EffectiveSeasonalCropCycleDto fromSeasonalCropCycleDto,
            EffectiveSeasonalCropCycleDto newSeasonalCropCycleDto) {

        Map<String, EffectiveCropCycleNodeDto> newNodeDtoByOriginalIds = new HashMap<>();
        for (EffectiveCropCycleNodeDto fromNodeDto : fromSeasonalCropCycleDto.getNodeDtos()) {

            if (StringUtils.isBlank(fromNodeDto.getCroppingPlanEntryId())) {
                continue;
            }
            CroppingPlanEntry fromCroppingPlanEntry = fromSeasonalCropCycleCropByIds.get(fromNodeDto.getCroppingPlanEntryId());

            CroppingPlanEntry toCroppingPlanEntry = toZoneCroppingPlanEntriesByCodes.get(fromCroppingPlanEntry.getCode());
            if (toCroppingPlanEntry == null) {
                continue;
            }

            EffectiveCropCycleNodeDto newNodeDto = new EffectiveCropCycleNodeDto();
            effectiveCropCycleNodeDtoBinder.copyExcluding(
                    fromNodeDto,
                    newNodeDto,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                    "interventions",
                    "nodeId",
                    "edaplosIssuerId",
                    "croppingPlanEntryId");//Do not duplicated eDaplosIssuerId
            newSeasonalCropCycleDto.addNodeDto(newNodeDto);

            newNodeDtoByOriginalIds.put(fromNodeDto.getNodeId(), newNodeDto);
            newNodeDto.setNodeId(NEW_NODE_PREFIX + UUID.randomUUID());
            newNodeDto.setCroppingPlanEntryId(toCroppingPlanEntry.getTopiaId());

            final List<EffectiveInterventionDto> newEffectiveInterventionDtos = toNewInterventionDtos(
                    fromNodeDto.getInterventions(),
                    getYearDifference(fromZoneId, toZoneId),
                    toDomain,
                    toInputStockUnitByCodes);

            newEffectiveInterventionDtos.forEach(newNodeDto::addIntervention);
        }
        return newNodeDtoByOriginalIds;
    }

    private Map<String, CroppingPlanEntry> getSeasonalCropCycleCropByIds(List<EffectiveSeasonalCropCycleDto> fromEffectiveSeasonalCropCycleDtos) {
        Set<String> nodeCopIds = fromEffectiveSeasonalCropCycleDtos.stream()
                .map(EffectiveSeasonalCropCycleDto::getNodeDtos)
                .flatMap(Collection::stream)
                .map(EffectiveCropCycleNodeDto::getCroppingPlanEntryId)
                .collect(Collectors.toSet());

        Set<String> connexionCopIds = fromEffectiveSeasonalCropCycleDtos.stream()
                .map(EffectiveSeasonalCropCycleDto::getConnectionDtos)
                .flatMap(Collection::stream)
                .filter(Objects::nonNull)
                .map(EffectiveCropCycleConnectionDto::getIntermediateCroppingPlanEntryId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        Set<String> allCropIds = Sets.newHashSet(nodeCopIds);
        allCropIds.addAll(connexionCopIds);
        Map<String, CroppingPlanEntry> fromNodesCropsByIds = croppingPlanEntryDao.forTopiaIdIn(allCropIds).findAll()
                .stream()
                .collect(Collectors.toMap(TopiaEntity::getTopiaId, Function.identity()));
        return fromNodesCropsByIds;
    }

    protected int getYearDifference(String fromZoneId, String toZoneId) {
        int fromCampaign = zoneDao.findDomainCampaignForZoneId(fromZoneId);
        return getYearDifference(fromCampaign, toZoneId);
    }

    protected int getYearDifference(int fromCampaign, String toZoneId) {
        int toCampaign = zoneDao.findDomainCampaignForZoneId(toZoneId);
        return toCampaign - fromCampaign;
    }

    protected List<EffectiveInterventionDto> toNewInterventionDtos(
            Collection<EffectiveInterventionDto> interventionDtos,
            int addYears,
            Domain toDomain,
            Map<String, DomainInputDto> domainInputDtoByCode) {
        List<EffectiveInterventionDto> newInterventionDtos = Lists.newArrayListWithCapacity(interventionDtos.size());
        for (EffectiveInterventionDto interventionDto : interventionDtos) {
            newInterventionDtos.add(
                    toNewInterventionDto(interventionDto, addYears, toDomain, domainInputDtoByCode));
        }
        return newInterventionDtos;
    }

    protected EffectiveInterventionDto toNewInterventionDto(
            EffectiveInterventionDto interventionDto,
            int addYears,
            Domain toDomain,
            Map<String, DomainInputDto> domainInputDtoByCode) {

        EffectiveInterventionDto newEffectiveInterventionDto = new EffectiveInterventionDto(toDomain.getTopiaId());

        bindToNewEffectiveInterventionData(interventionDto, newEffectiveInterventionDto);

        newEffectiveInterventionDto.setStartInterventionDate(newEffectiveInterventionDto.getStartInterventionDate().plusYears(addYears));
        newEffectiveInterventionDto.setEndInterventionDate(newEffectiveInterventionDto.getEndInterventionDate().plusYears(addYears));

        bindSpeciesStadeDtos(interventionDto, newEffectiveInterventionDto);

        Collection<AbstractActionDto> abstractActionDtos = cloneActionsAndUsages(
                interventionDto.getActionDtos(),
                domainInputDtoByCode,
                toDomain.getTopiaId());

        newEffectiveInterventionDto.setActionDtos(abstractActionDtos);

        return newEffectiveInterventionDto;
    }

    protected Collection<AbstractActionDto> cloneActionsAndUsages(
            Collection<AbstractActionDto> actionDtos,
            Map<String, DomainInputDto> domainInputDtoByCode,
            String toDomainId) {

        if (actionDtos == null) return null;

        Collection<AbstractActionDto> clonedAction = new ArrayList<>();
        for (AbstractActionDto actionDto : actionDtos) {

            AbstractActionDto clonedActionDto = actionService.cloneActionAndUsages(
                    actionDto,
                    domainInputDtoByCode,
                    toDomainId);

            if (clonedActionDto != null) {
                clonedAction.add(clonedActionDto);
            }
        }
        return clonedAction;
    }

    protected void bindSpeciesStadeDtos(
            EffectiveInterventionDto interventionDto,
            EffectiveInterventionDto newEffectiveInterventionDto) {

        List<SpeciesStadeDto> newSpeciesStadeDtos = new ArrayList<>();
        List<SpeciesStadeDto> speciesStadeDtos = interventionDto.getSpeciesStadesDtos();
        if (speciesStadeDtos != null) {
            for (SpeciesStadeDto speciesStadeDto : speciesStadeDtos) {
                if (StringUtils.isBlank(speciesStadeDto.getSpeciesCode())) {
                    continue;
                }
                speciesStadeDto.setTopiaId(null);
                newSpeciesStadeDtos.add(speciesStadeDto);
            }
        }
        newEffectiveInterventionDto.setSpeciesStadesDtos(newSpeciesStadeDtos);
    }

    protected void bindToNewEffectiveInterventionData(EffectiveInterventionDto interventionDto, EffectiveInterventionDto newEffectiveInterventionDto) {
        Binder<EffectiveInterventionDto, EffectiveInterventionDto> binder = BinderFactory.newBinder(EffectiveInterventionDto.class);
        // edaplosIssuerId should not been duplicated, cf #8655
        binder.copyExcluding(
                interventionDto, newEffectiveInterventionDto,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                EffectiveIntervention.PROPERTY_EDAPLOS_ISSUER_ID,
                "actionDtos",
                "actions",
                "speciesStadesDtos");
        newEffectiveInterventionDto.setTopiaId(NEW_INTERVENTION_PREFIX + UUID.randomUUID());
    }


    @Override
    public List<ZoneDto> getZones(Collection<String> zoneIds) {
        List<Zone> plots = zoneDao.forTopiaIdIn(zoneIds).findAll();
        return plots.stream().map(anonymizeService.getZoneToDtoFunction(false, false)).collect(Collectors.toList());
    }

    @Override
    public ExportResult exportEffectiveCropCyclesAsXls(Collection<String> effectiveCropCycleIds) {
        EffectiveXlsExporter effectiveXlsExporter = context.newService(EffectiveXlsExporter.class);
        return effectiveXlsExporter.exportEffectiveCropCyclesAsXls(effectiveCropCycleIds);
    }

    @Override
    public void exportEffectiveCropCyclesAsXlsAsync(Collection<String> effectiveCropCycleIds) {
        AuthenticatedUser user = getAuthenticatedUser();

        EffectiveCropCyclesExportTask exportTask = new EffectiveCropCyclesExportTask(user.getTopiaId(), user.getEmail(), effectiveCropCycleIds);
        getBusinessTaskManager().schedule(exportTask);
    }

    @Override
    public CopyPasteZoneByCampaigns getAvailableZonesForCopy(String zoneTopiaId) {
        Zone zoneFrom = zoneDao.forTopiaIdEquals(zoneTopiaId).findUnique();
        Domain domain = zoneFrom.getPlot().getDomain();
        List<Zone> zones = zoneDao.findZonesFromDomainCode(domain.getCode(), getSecurityContext());

        HashMap<Integer, List<CopyPasteZoneDto>> copyPasteZoneDtoByCampaign = new HashMap<>();
        CopyPasteZoneByCampaigns result = new CopyPasteZoneByCampaigns();
        result.setZonesByCampaigns(copyPasteZoneDtoByCampaign);

        MultiValuedMap<Zone, EffectivePerennialCropCycle> allEffectivePerennialCropCyclesForZones = new HashSetValuedHashMap<>();
        effectivePerennialCropCycleDao.forZoneIn(zones).findAll().forEach(epcc -> allEffectivePerennialCropCyclesForZones.put(epcc.getZone(), epcc));
        MultiValuedMap<Zone, EffectiveSeasonalCropCycle> allEffectiveSeasonalCropCyclesForZones =  new HashSetValuedHashMap<>();
        effectiveSeasonalCropCycleDao.forZoneIn(zones).findAll().forEach(escc -> allEffectiveSeasonalCropCyclesForZones.put(escc.getZone(), escc));

        MultiValuedMap<EffectiveCropCyclePhase, EffectiveIntervention> allPhaseInterventions = new HashSetValuedHashMap<>();
                effectiveInterventionDao.forEffectiveCropCyclePhaseIn(allEffectivePerennialCropCyclesForZones.values()
                .stream()
                .map(EffectivePerennialCropCycle::getPhase).toList())
                .findAll().forEach(ei -> allPhaseInterventions.put(ei.getEffectiveCropCyclePhase(), ei));

        Collection<EffectiveCropCycleNode> nodes = allEffectiveSeasonalCropCyclesForZones.values()
                .stream()
                .map(EffectiveSeasonalCropCycle::getNodes)
                .flatMap(Collection::stream).toList();

        MultiValuedMap<EffectiveCropCycleNode, EffectiveIntervention> allNodeInterventions = new HashSetValuedHashMap<>();
        effectiveInterventionDao.forEffectiveCropCycleNodeIn(nodes).findAll().forEach(ei -> allNodeInterventions.put(ei.getEffectiveCropCycleNode(), ei));

        for (Zone zone : zones) {

            List<CopyPasteZoneDto> cropTargets = new ArrayList<>(
                    CollectionUtils.emptyIfNull(allEffectivePerennialCropCyclesForZones.get(zone))
                            .stream().map(
                                    epcc -> EFFECTIVE_PERENNIAL_TO_CROP_CYCLE_CROP_TARGET_FUNCTION.apply(
                                            Pair.of(epcc, CollectionUtils.emptyIfNull(allPhaseInterventions.get(epcc.getPhase())))))
                    .toList());

            CollectionUtils.emptyIfNull(allEffectiveSeasonalCropCyclesForZones.get(zone)).forEach(allEffectiveSeasonalCropCycle -> {
                if (allEffectiveSeasonalCropCycle.getNodes() != null) {
                    // it must have at least one crop
                    cropTargets.addAll(
                            allEffectiveSeasonalCropCycle.getNodes()
                                    .stream()
                                    .map(n -> EFFECTIVE_CROP_CYCLE_NODE_TO_CROP_TARGET_FUNCTION.apply(Pair.of(n, CollectionUtils.emptyIfNull(allNodeInterventions.get(n)))))
                                    .toList());
                }
            });

            String zoneId = zone.getTopiaId();
            String zoneName = zone.getName();
            double zoneArea = zone.getArea();
            String plotName = zone.getPlot().getName();
            double plotArea = zone.getPlot().getArea();
            String domainName = zone.getPlot().getDomain().getName();
            int domainCampaign = zone.getPlot().getDomain().getCampaign();
            GrowingSystem gs = zone.getPlot().getGrowingSystem();
            String gsName = "";
            String gpName = "";
            if (gs != null) {
                gsName = gs.getName();
                gpName = gs.getGrowingPlan().getName();
            }

            for (CopyPasteZoneDto cropTarget : cropTargets) {
                cropTarget.setZoneId(zoneId);
                cropTarget.setZoneName(zoneName);
                cropTarget.setZoneArea(zoneArea);
                cropTarget.setPlotName(plotName);
                cropTarget.setPlotArea(plotArea);
                cropTarget.setDomainName(domainName);
                cropTarget.setGrowingPlanName(gpName);
                cropTarget.setGrowingSystemName(gsName);
                cropTarget.setCampaign(domainCampaign);

                addCropTocopyPasteZoneDtoByCampaign(copyPasteZoneDtoByCampaign, cropTarget);
            }

        }
        return result;
    }

    protected void addCropTocopyPasteZoneDtoByCampaign(HashMap<Integer, List<CopyPasteZoneDto>> copyPasteZoneDtoByCampaign, CopyPasteZoneDto cropTarget) {
        List<CopyPasteZoneDto> copyPasteZoneDtosForCampaing = copyPasteZoneDtoByCampaign.computeIfAbsent(cropTarget.getCampaign(), k -> new ArrayList<>());
        copyPasteZoneDtosForCampaing.add(cropTarget);
    }

    private void debugLogs(Map<String, String> fromSpeciesCodeToSpeciesCode, Map<EffectiveInterventionDto, Map<String, CroppingPlanSpecies>> targetedSpeciesByCodesByInterventionDto) {
        if (LOGGER.isDebugEnabled()) {
            for (Map.Entry<String, String> speciesCodeToSpeciesCode : fromSpeciesCodeToSpeciesCode.entrySet()) {
                LOGGER.debug("From species code:" + speciesCodeToSpeciesCode.getKey() + " To species code:" + speciesCodeToSpeciesCode.getValue());
            }
            for (Map.Entry<EffectiveInterventionDto, Map<String, CroppingPlanSpecies>> effectiveInterventionTargetedSpeciesByCodes : targetedSpeciesByCodesByInterventionDto.entrySet()) {
                EffectiveInterventionDto intervention = effectiveInterventionTargetedSpeciesByCodes.getKey();
                LOGGER.debug("For intervention :" + intervention.getTopiaId() + "(" + intervention.getName() + ")" + " species codes:" + String.join(",", effectiveInterventionTargetedSpeciesByCodes.getValue().keySet()));
            }
        }
    }

    protected Map<String, DomainInputDto> createMissingDomainInputs(
            List<EffectiveInterventionDto> interventionDtos,
            Domain targetedDomain) {

        Map<String, DomainInputDto> toInputStockUnitByCodes = searchMissingInterventionsDomainInputs(interventionDtos, targetedDomain);
        persisteMissingDomainInputs(toInputStockUnitByCodes);

        return toInputStockUnitByCodes;
    }

    private void persisteMissingDomainInputs(Map<String, DomainInputDto> toInputStockUnitByCodes) {
        List<DomainInputDto> newDomainInputDto = new ArrayList<>();

        toInputStockUnitByCodes.values().stream()
                .filter(di -> di.getTopiaId().isEmpty())
                .filter(di -> di.getToCopyFromInputId().isPresent())
                .filter(di -> di.getDomainId().isPresent())
                .filter(di -> di.getToCopyToDomainId().isPresent())
                .forEach(
                        di -> {
                            AbstractDomainInputStockUnit domainInputStockUnit = domainInputStockUnitService.copyInputStock(
                                    di.getDomainId().get(),
                                    di.getToCopyFromInputId().get(),
                                    di.getToCopyToDomainId().get());
                            DomainInputDto newDi = di.toBuilder()
                                    .topiaId(domainInputStockUnit.getTopiaId())
                                    .code(domainInputStockUnit.getCode())
                                    .build();
                            newDomainInputDto.add(newDi);
                        }
                );
        newDomainInputDto.forEach(di -> toInputStockUnitByCodes.replace(di.getCode(), di));
    }

    private Map<String, DomainInputDto> searchMissingInterventionsDomainInputs(List<EffectiveInterventionDto> interventionDtos, Domain targetedDomain) {
        List<DomainInputDto> toInputStockUnits = domainService.getInputStockUnits(targetedDomain.getTopiaId());

        Map<String, DomainInputDto> toInputStockUnitByCodes = toInputStockUnits.stream().collect(
                Collectors.toMap(DomainInputDto::getCode, Function.identity()));

        interventionDtos.forEach(
                interventionDto -> searchMissingActionsDomainInputs(
                        interventionDto.getActionDtos(),
                        toInputStockUnitByCodes,
                        targetedDomain.getTopiaId()
                )
        );

        return toInputStockUnitByCodes;
    }

    protected void searchMissingActionsDomainInputs(
            Collection<AbstractActionDto> actionDtos,
            Map<String, DomainInputDto> domainInputDtoByCode,
            String toDomainId) {

        if (actionDtos == null) return;

        actionDtos.forEach(
                actionDto -> actionService.searchMissingActionAndUsageInput(
                        actionDto,
                        domainInputDtoByCode,
                        toDomainId)
        );
    }

    protected boolean copyInterventionsToTargetedNodes(
            List<EffectiveInterventionDto> interventionDtos,
            Zone targetedZone,
            List<String> targetedNodeIds,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant,
            Map<String, ToolsCoupling> toolsCouplingByCode,
            Map<String, List<AbstractDomainInputStockUnit>> domainInputStockUnitByKeys,
            Map<String, DomainInputDto> toInputStockUnitByCodes) {

        boolean completeSeasonalMigration = true;

        if (CollectionUtils.isNotEmpty(targetedNodeIds)) {

            Collection<AbstractDomainInputStockUnit> domainInputStockUnits = domainInputStockUnitByKeys
                    .values()
                    .stream()
                    .flatMap(Collection::stream).toList();

            // find all nodes from there ids
            List<EffectiveCropCycleNode> targetedNodes = getNodesFromJspIds(targetedNodeIds);

            // two strategies are used to find the targeted crop code.
            // 1st try to find same matching crop code on targeted zone
            // 2nd try to find equivalent crop (same RefEspece) on targeted zone
            List<CroppingPlanSpecies> fromZoneSpecies = getFromCroppingPlanSpecieses(interventionDtos);

            Map<String, String> fromSpeciesKeyToSpeciesCode = referentialService.getCroppingPlanSpeciesCodeByRefEspeceAndVarietyKey(fromZoneSpecies);

            Integer fromCampaign = domainService.getCampaignForDomain(interventionDtos.getFirst().getDomainId());

            Domain targetedDomain = targetedZone.getPlot().getDomain();

            for (EffectiveCropCycleNode targetedNode : targetedNodes) {
                EffectiveCropCycleConnection targetedConnection = effectiveCropCycleConnectionDao.forTargetEquals(targetedNode).findUniqueOrNull();

                CroppingPlanEntry targetedIntermediateCrop = targetedConnection != null ? targetedConnection.getIntermediateCroppingPlanEntry() : null;
                CroppingPlanEntry targetedCrop = targetedNode.getCroppingPlanEntry();

                Map<String, String> fromSpeciesCodeToSpeciesCode = getFromSpeciesCodeToSpeciesCode(fromSpeciesKeyToSpeciesCode, targetedNode, targetedConnection);

                // intervention need to be clear for each iteration as action and input from DTO are same as entity one
                // and have to be recreated for each intervention (and not modified otherwise)
                Collection<EffectiveInterventionDto> copiedInterventionDtos_ =
                        toNewInterventionDtos(
                                interventionDtos,
                                getYearDifference(fromCampaign, targetedZone.getTopiaId()),
                                targetedDomain,
                                toInputStockUnitByCodes);

                validIntermediateStatus(interventionDtos, targetedIntermediateCrop);

                Map<EffectiveInterventionDto, Map<String, CroppingPlanSpecies>> sameCodeEspeceBotaniqueTargetedSpeciesByCodesByInterventionDto =
                        getSameCodeEspeceBotniqueTargetedSpeciesByCodesByInterventionDto(copiedInterventionDtos_, targetedCrop, targetedIntermediateCrop);

                Map<EffectiveInterventionDto, Boolean> interventionDtoWithResults = doEffectiveInterventionsMigration(
                        targetedCrop,
                        targetedIntermediateCrop,
                        copiedInterventionDtos_,
                        fromSpeciesCodeToSpeciesCode,
                        sameCodeEspeceBotaniqueTargetedSpeciesByCodesByInterventionDto,
                        sectorByCodeEspeceBotaniqueCodeQualifiant,
                        domainInputStockUnitByKeys,
                        fromZoneSpecies);

                Collection<EffectiveInterventionDto> copiedInterventionDtos = Lists.newArrayList(interventionDtoWithResults.keySet()
                        .stream().
                        filter(effectiveInterventionDto -> CollectionUtils.isNotEmpty(effectiveInterventionDto.getActionDtos())).toList());

                boolean speciesFullyMigrated = !interventionDtoWithResults.containsValue(Boolean.FALSE);

                // add existing node intervention to not remove them
                List<EffectiveIntervention> existingTargetedEffectiveInterventions = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(targetedNode).findAll();

                List<EffectiveInterventionDto> targetedInterventionDtos = toCompletIntervetionDto(existingTargetedEffectiveInterventions, targetedDomain);
                targetedInterventionDtos.addAll(copiedInterventionDtos);

                CreateOrUpdateEffectiveInterventionContext createOrUpdateEffectiveInterventionContext = createInterventionCopyContext(
                        targetedZone,
                        targetedNode,
                        targetedInterventionDtos,
                        targetedConnection,
                        speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                        sectorByCodeEspeceBotaniqueCodeQualifiant,
                        toolsCouplingByCode,
                        domainInputStockUnits,
                        targetedCrop,
                        targetedIntermediateCrop);

                boolean isToolsCouplingMigrated = saveInterventions(createOrUpdateEffectiveInterventionContext);

                if (!speciesFullyMigrated || !isToolsCouplingMigrated) {
                    completeSeasonalMigration = false;
                }
            }
        }
        return completeSeasonalMigration;
    }

    protected CreateOrUpdateEffectiveInterventionContext createInterventionCopyContext(
            Zone targetedZone,
            EffectiveCropCycleNode targetedNode,
            List<EffectiveInterventionDto> copiedInterventionDtos,
            EffectiveCropCycleConnection targetedConnection,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, ToolsCoupling> toolsCouplingByCode,
            Collection<AbstractDomainInputStockUnit> domainInputStockUnits,
            CroppingPlanEntry targetedCrop,
            CroppingPlanEntry targetedIntermediateCrop) {

        Map<String, EffectiveIntervention> idToIntervention = getEffectiveSeasonalInterventionById(targetedNode);

        List<EffectiveCropCycleConnectionDto> connectionDtos = targetedConnection != null ? Lists.newArrayList(EffectiveCropCycles.CROP_CYCLE_CONNECTION_TO_DTO.apply(targetedConnection)) : null;

        Pair<String, EffectiveCropCycleNode> targetedNodeByIds = getTargetedNodeByIds(targetedNode, targetedNode.getTopiaId());

        return CreateOrUpdateEffectiveInterventionContext.createSeasonalInterventionCreateOrUpdateContext(
                copiedInterventionDtos,
                connectionDtos,
                targetedNodeByIds,
                targetedZone.getPlot().getDomain(),
                targetedZone,
                idToIntervention,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspceBotaniqueCodeQualifiant,
                toolsCouplingByCode,
                domainInputStockUnits,
                targetedCrop,
                targetedIntermediateCrop);
    }

    private Pair<String, EffectiveCropCycleNode> getTargetedNodeByIds(EffectiveCropCycleNode targetedNode, String topiaId) {
        return Pair.of(topiaId, targetedNode);
    }

    protected Map<String, String> getFromSpeciesCodeToSpeciesCode(Map<String, String> fromSpeciesKeyToSpeciesCode, EffectiveCropCycleNode targetedNode, EffectiveCropCycleConnection targetedConnection) {
        Map<String, String> targetedSpeciesKeyToSpeciesCode = getSpeciesKeyToSpeciesCode(targetedNode, targetedConnection);
        return getFromSpeciesCodeToSpeciesCode(fromSpeciesKeyToSpeciesCode, targetedSpeciesKeyToSpeciesCode);
    }

    protected void validIntermediateStatus(Collection<EffectiveInterventionDto> interventionDtos, CroppingPlanEntry targetedIntermediateCrop) {
        for (EffectiveInterventionDto interventionDto : interventionDtos) {
            boolean isIntermediate = interventionDto.isIntermediateCrop() && targetedIntermediateCrop != null;
            interventionDto.setIntermediateCrop(isIntermediate);
        }
    }

    protected Map<String, EffectiveIntervention> getEffectiveSeasonalInterventionById(EffectiveCropCycleNode targetedNode) {
        List<EffectiveIntervention> interventions =
                new ArrayList<>(effectiveInterventionDao.forEffectiveCropCycleNodeEquals(targetedNode).findAll());
        return Maps.newHashMap(Maps.uniqueIndex(interventions, Entities.GET_TOPIA_ID::apply));
    }

    protected List<EffectiveCropCycleNode> getNodesFromJspIds(List<String> targetedNodeIds) {
        List<EffectiveCropCycleNode> nodes = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(targetedNodeIds)) {
            List<String> nodeIds = Lists.newArrayListWithExpectedSize(targetedNodeIds.size());
            for (String nodeId : targetedNodeIds) {
                if (StringUtils.isNotBlank(nodeId)) {
                    nodeIds.add(Entities.UNESCAPE_TOPIA_ID.apply(nodeId));
                }
            }
            // nodes to copy intervention to.
            List<EffectiveCropCycleNode> zoneNodes = effectiveCropCycleNodeDao.forTopiaIdIn(nodeIds).findAll();
            nodes.addAll(zoneNodes);
        }
        return nodes;
    }

    protected Map<String, String> getSpeciesKeyToSpeciesCode(EffectiveCropCycleNode node, EffectiveCropCycleConnection connection) {
        Map<String, String> targetedSpeciesIdToCode = new HashMap<>();

        if (node != null) {
            CroppingPlanEntry croppingPlanEntry = node.getCroppingPlanEntry();
            targetedSpeciesIdToCode = doCroppingPlanEntrySpeciesKeyToSpeciesCode(croppingPlanEntry);
        }

        if (connection != null) {
            CroppingPlanEntry croppingPlanEntry = connection.getIntermediateCroppingPlanEntry();
            targetedSpeciesIdToCode.putAll(doCroppingPlanEntrySpeciesKeyToSpeciesCode(croppingPlanEntry));
        }

        return targetedSpeciesIdToCode;
    }

    protected Map<String, String> doCroppingPlanEntrySpeciesKeyToSpeciesCode(CroppingPlanEntry croppingPlanEntry) {
        Map<String, String> targetedSpeciesKeyToSpeciesCode = new HashMap<>();
        if (croppingPlanEntry != null && croppingPlanEntry.getCroppingPlanSpecies() != null) {
            Collection<CroppingPlanSpecies> croppingPlanSpecies = croppingPlanEntry.getCroppingPlanSpecies();
            targetedSpeciesKeyToSpeciesCode.putAll(referentialService.getCroppingPlanSpeciesCodeByRefEspeceAndVarietyKey(croppingPlanSpecies));
        }
        return targetedSpeciesKeyToSpeciesCode;
    }

    protected boolean copyInterventionsToTargetedPhases(
            List<EffectiveInterventionDto> interventionDtos,
            Zone zone,
            List<String> targetedPhaseIds,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, ToolsCoupling> toolsCouplingByCode,
            Map<String, List<AbstractDomainInputStockUnit>> domainInputStockUnitByKeys,
            Map<String, DomainInputDto> toInputStockUnitByCodes) {

        boolean completeMigration = true;

        if (CollectionUtils.isNotEmpty(targetedPhaseIds)) {

            final Domain targetedDomain = zone.getPlot().getDomain();
            List<EffectiveCropCyclePhase> phases =
                    new ArrayList<>(effectiveCropCyclePhaseDao.forTopiaIdIn(targetedPhaseIds).findAll());

            Collection<AbstractDomainInputStockUnit> domainInputStockUnits = domainInputStockUnitByKeys.values().stream().flatMap(Collection::stream).toList();

            // two strategies are used to find the targeted crop code.
            // 1st try to find same matching crop code on targeted zone
            // 2nd try to find equivalent crop (same RefEspece) on targeted zone
            List<CroppingPlanSpecies> fromZoneSpecies = getFromCroppingPlanSpecieses(interventionDtos);

            Map<String, String> fromSpeciesKeyToSpeciesCode = referentialService.getCroppingPlanSpeciesCodeByRefEspeceAndVarietyKey(fromZoneSpecies);

            Integer fromCampaign = domainService.getCampaignForDomain(interventionDtos.getFirst().getDomainId());

            for (EffectiveCropCyclePhase phase : phases) {
                EffectivePerennialCropCycle targetedPerennialCropCycle = effectivePerennialCropCycleDao.forPhaseEquals(phase).findUnique();

                CroppingPlanEntry targetedCrop = targetedPerennialCropCycle.getCroppingPlanEntry();

                Map<String, String> fromSpeciesCodeToSpeciesCode = getFromSpeciesCodesToSpeciesCodes(fromSpeciesKeyToSpeciesCode, targetedCrop);

                // intervention need to be clear for each iteration as action and input from DTO are same as entity one
                // and have to be recreated for each intervention (and not modified otherwise)
                Collection<EffectiveInterventionDto> copiedInterventionDtos =
                        toNewInterventionDtos(
                                interventionDtos,
                                getYearDifference(fromCampaign, zone.getTopiaId()),
                                targetedDomain,
                                toInputStockUnitByCodes);

                Map<EffectiveInterventionDto, Map<String, CroppingPlanSpecies>> sameCodeEspeceBotaniqueTargetedSpeciesByCodesByInterventionDto =
                        getSameCodeEspeceBotniqueTargetedSpeciesByCodesByInterventionDto(copiedInterventionDtos, targetedCrop, null);

                // Value 'targetedIntermediateCrop' is always 'null: no intermediate crop for phase
                Map<EffectiveInterventionDto, Boolean> interventionDtoWithResults = doEffectiveInterventionsMigration(
                        targetedCrop,
                        null,
                        copiedInterventionDtos,
                        fromSpeciesCodeToSpeciesCode,
                        sameCodeEspeceBotaniqueTargetedSpeciesByCodesByInterventionDto,
                        sectorByCodeEspceBotaniqueCodeQualifiant,
                        domainInputStockUnitByKeys,
                        fromZoneSpecies);

                interventionDtos = Lists.newArrayList(interventionDtoWithResults.keySet());
                boolean speciesFullyMigrated = !interventionDtoWithResults.containsValue(Boolean.FALSE);

                // add existing node intervention to not remove them
                List<EffectiveIntervention> existingTargetedEffectiveInterventions = effectiveInterventionDao.forEffectiveCropCyclePhaseEquals(phase).findAll();

                // add existing intervention to not remove them later
                List<EffectiveInterventionDto> targetedInterventionDtos = toCompletIntervetionDto(existingTargetedEffectiveInterventions, targetedDomain);
                targetedInterventionDtos.addAll(copiedInterventionDtos);

                Map<String, EffectiveIntervention> idToIntervention = getPerennialEffectiveInterventionsByIds(phase);
                CreateOrUpdateEffectiveInterventionContext createOrUpdateEffectiveInterventionContext = CreateOrUpdateEffectiveInterventionContext.createPerennialInterventionCreateOrUpdateContext(
                        targetedInterventionDtos,
                        targetedPerennialCropCycle,
                        phase,
                        targetedDomain,
                        zone,
                        idToIntervention,
                        speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                        sectorByCodeEspceBotaniqueCodeQualifiant,
                        toolsCouplingByCode,
                        domainInputStockUnits,
                        targetedCrop);

                boolean isToolsCouplingMigrated = saveInterventions(createOrUpdateEffectiveInterventionContext);
                if (createOrUpdateEffectiveInterventionContext.getIdsToInterventions() != null) {
                    removeEffectiveInterventions(createOrUpdateEffectiveInterventionContext.getIdsToInterventions().values());
                }

                if (!speciesFullyMigrated && isToolsCouplingMigrated) {
                    completeMigration = false;
                }
            }
        }

        return completeMigration;
    }

    /**
     * transforme intervention to intervention dto and feed it up with its actions and inputs
     */
    protected List<EffectiveInterventionDto> toCompletIntervetionDto(
            List<EffectiveIntervention> existingTargetedEffectiveInterventions,
            Domain domain) {

        Language language = getSecurityContext().getLanguage();
        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(language);

        List<EffectiveInterventionDto> targetedInterventionDtos = new ArrayList<>();

        for (EffectiveIntervention existingEffectiveIntervention : existingTargetedEffectiveInterventions) {

            final EffectiveInterventionDto effectiveInterventionDto = getTranslatedEffectiveInterventionDto(domain, existingEffectiveIntervention, translationMap);

            final List<AbstractActionDto> actionDtos = actionService.loadEffectiveActionsAndUsages(existingEffectiveIntervention);
            effectiveInterventionDto.setActionDtos(actionDtos);
            targetedInterventionDtos.add(effectiveInterventionDto);
        }
        return targetedInterventionDtos;
    }

    protected EffectiveInterventionDto getTranslatedEffectiveInterventionDto(
            Domain domain,
            EffectiveIntervention existingEffectiveIntervention,
            ReferentialTranslationMap translationMap) {

        EffectiveCropCycles.fillInterventionEspeceTranslations(existingEffectiveIntervention, translationMap,
                i18nService::fillRefEspeceTranslations);
        EffectiveCropCycles.fillInterventionStadeTranslationMap(existingEffectiveIntervention, translationMap,
                i18nService::fillRefStadeEdiTranslations);

        final EffectiveInterventionDto effectiveInterventionDto = EffectiveCropCycles.getDtoForEffectiveIntervention(
                existingEffectiveIntervention, translationMap, domain);
        return effectiveInterventionDto;
    }

    private Map<String, String> getFromSpeciesCodesToSpeciesCodes(Map<String, String> fromSpeciesKeyToSpeciesCode, CroppingPlanEntry toZoneCrop) {
        Collection<CroppingPlanSpecies> toZoneSpecies = toZoneCrop.getCroppingPlanSpecies();
        Map<String, String> targetedSpeciesKeyToSpeciesCode = referentialService.getCroppingPlanSpeciesCodeByRefEspeceAndVarietyKey(toZoneSpecies);
        return getFromSpeciesCodeToSpeciesCode(fromSpeciesKeyToSpeciesCode, targetedSpeciesKeyToSpeciesCode);
    }

    private Map<String, EffectiveIntervention> getPerennialEffectiveInterventionsByIds(EffectiveCropCyclePhase phase) {
        List<EffectiveIntervention> interventions =
                new ArrayList<>(effectiveInterventionDao.forEffectiveCropCyclePhaseEquals(phase).findAll());
        return Maps.newHashMap(Maps.uniqueIndex(interventions, Entities.GET_TOPIA_ID::apply));
    }

    protected Map<String, String> getFromSpeciesCodeToSpeciesCode(Map<String, String> fromSpeciesKeyToSpeciesCode, Map<String, String> targetedSpeciesKeyToSpeciesCode) {
        Map<String, String> fromSpeciesCodeToSpeciesCode = new HashMap<>();
        for (Map.Entry<String, String> targetedSpeciesKeyToCode : targetedSpeciesKeyToSpeciesCode.entrySet()) {
            String fromSpeciesCode = fromSpeciesKeyToSpeciesCode.get(targetedSpeciesKeyToCode.getKey());
            if (StringUtils.isNotBlank(fromSpeciesCode)) {
                fromSpeciesCodeToSpeciesCode.put(fromSpeciesCode, targetedSpeciesKeyToCode.getValue());
            }
        }
        return fromSpeciesCodeToSpeciesCode;
    }

    protected List<CroppingPlanSpecies> getFromCroppingPlanSpecieses(List<EffectiveInterventionDto> interventionDtos) {
        String fromDomainId = interventionDtos.getFirst().getDomainId();
        Set<String> speciesCodes = Sets.newHashSet();
        for (EffectiveInterventionDto interventionDto : interventionDtos) {
            List<SpeciesStadeDto> speciesStadesDtos = interventionDto.getSpeciesStadesDtos();
            if (speciesStadesDtos != null) {
                for (SpeciesStadeDto speciesStadeDto : speciesStadesDtos) {
                    speciesCodes.add(speciesStadeDto.getSpeciesCode());
                }
            }
        }
        return croppingPlanSpeciesDao.getCroppingPlanSpeciesForCodeAndDomainId(speciesCodes, fromDomainId);
    }

    @Override
    public boolean copyInterventions(
            List<TargetedZones> zonesDto, List<EffectiveInterventionDto> interventionDtos) {

        boolean completeMigration = true;

        if (CollectionUtils.isNotEmpty(interventionDtos) && CollectionUtils.isNotEmpty(zonesDto)) {

            Iterable<String> zoneIds = getZoneIdsFromZoneDtos(zonesDto);

            List<Zone> zones = zoneDao.forTopiaIdIn(Sets.newHashSet(zoneIds)).findAll();

            for (Zone zone : zones) {
                checkPreconditionOnActive(zone.getTopiaId(), zone);
            }

            Map<String, Zone> indexedZones = Maps.uniqueIndex(zones, Entities.GET_TOPIA_ID::apply);

            for (TargetedZones targetedZoneDto : zonesDto) {
                Zone targetedZone = indexedZones.get(targetedZoneDto.getZoneId());

                if (targetedZone != null) {

                    final Domain targetedDomain = targetedZone.getPlot().getDomain();
                    List<ToolsCoupling> targetedZoneToolsCouplings = domainService.getToolsCouplings(targetedDomain.getTopiaId());
                    Map<String, ToolsCoupling> toolsCouplingByCode = Maps.uniqueIndex(targetedZoneToolsCouplings, ToolsCoupling::getCode);
                    Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(targetedZone);
                    Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant = getSectorByCodeEspceBotaniqueCodeQualifiant(targetedZone);

                    Map<String, DomainInputDto> toInputStockUnitByCodes = createMissingDomainInputs(
                            interventionDtos,
                            targetedDomain);

                    final Map<String, List<AbstractDomainInputStockUnit>> domainInputStockByIKeys = domainInputStockUnitService.loadDomainInputStockByIKeys(targetedDomain);

                    boolean completSeasonalMigration = copyInterventionsToTargetedNodes(
                            interventionDtos,
                            targetedZone,
                            targetedZoneDto.getNodes(),
                            speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                            sectorByCodeEspeceBotaniqueCodeQualifiant,
                            toolsCouplingByCode,
                            domainInputStockByIKeys,
                            toInputStockUnitByCodes
                    );

                    boolean completPerennialMigration = copyInterventionsToTargetedPhases(
                            interventionDtos,
                            targetedZone,
                            targetedZoneDto.getPhases(),
                            speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                            sectorByCodeEspeceBotaniqueCodeQualifiant,
                            toolsCouplingByCode,
                            domainInputStockByIKeys,
                            toInputStockUnitByCodes);

                    if (!(completSeasonalMigration && completPerennialMigration)) {
                        completeMigration = false;
                    }
                }
            }
            getTransaction().commit();
        }

        return completeMigration;
    }

    protected Iterable<String> getZoneIdsFromZoneDtos(List<TargetedZones> zonesDto) {
        return Iterables.transform(zonesDto, TargetedZones::getZoneId);
    }

    @Override
    public Map<String, List<Pair<String, String>>> getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(String zoneId) {
        String domainId = zoneDao.findDomainIdForZoneId(zoneId);
        return referentialService.getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(Sets.newHashSet(domainId));
    }

    @Override
    public Map<String, List<Pair<String, String>>> getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(Zone zone) {
        String domainId = zone.getPlot().getDomain().getTopiaId();
        return referentialService.getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(Sets.newHashSet(domainId));
    }

    @Override
    public Map<String, List<Sector>> getSectorByCodeEspceBotaniqueCodeQualifiant(String zoneId) {
        String domainId = zoneDao.findDomainIdForZoneId(zoneId);
        List<Pair<String, String>> r0 = referentialService.getAllCodeEspeceBotaniqueCodeQualifantForDomainIds(Sets.newHashSet(Sets.newHashSet(domainId)));
        return referentialService.getSectorsByCodeEspeceBotanique_CodeQualifiantAEE(r0);
    }

    @Override
    public Map<String, List<Sector>> getSectorByCodeEspceBotaniqueCodeQualifiant(Zone zone) {
        String domainId = zone.getPlot().getDomain().getTopiaId();
        List<Pair<String, String>> r0 = referentialService.getAllCodeEspeceBotaniqueCodeQualifantForDomainIds(Sets.newHashSet(Sets.newHashSet(domainId)));
        return referentialService.getSectorsByCodeEspeceBotanique_CodeQualifiantAEE(r0);
    }

    @Override
    public Collection<CattleDto> getCattleForDomain(String domainId) {
        List<LivestockUnit> livestockUnits = domainService.loadLivestockUnitsForDomainId(domainId);

        Set<CattleDto> result = new HashSet<>();
        for (LivestockUnit livestockUnit : livestockUnits) {
            Collection<Cattle> cattles = livestockUnit.getCattles();
            if (CollectionUtils.isNotEmpty(cattles)) {
                final RefAnimalType refAnimalType = livestockUnit.getRefAnimalType();
                for (Cattle cattle : cattles) {
                    result.add(new CattleDto(cattle, refAnimalType));
                }
            }
        }
        return result;
    }

    @Override
    public EffectiveIntervention getEffectiveInterventionWithGivenId(String effectiveInterventionId) {
        EffectiveIntervention effectiveIntervention = effectiveInterventionDao.forTopiaIdEquals(effectiveInterventionId).findUnique();
        return effectiveIntervention;
    }

    @Override
    public Pair<Integer, Integer> togglePlotOrZoneActiveStatus(List<String> zoneIds, boolean plot, boolean activation) {
        int nbPlotUbpdated = 0;
        int nbZoneUbpdated = 0;
        for (String zoneId : zoneIds) {
            authorizationService.checkCreateOrUpdateEffectiveCropCycles(zoneId);
            Zone zone = zoneDao.forTopiaIdEquals(zoneId).findUnique();
            if (plot) {
                nbPlotUbpdated += togglePlotActivationStatus(activation, zone);
            } else {
                if (activation) {
                    // si les zone doivent être activées, les parcelles doivent l'être aussi
                    // l'inverse n'est pas vrai
                    nbPlotUbpdated += togglePlotActivationStatus(true, zone);
                }
                // zone
                if (zone.isActive() != activation) {
                    zone.setActive(activation);
                    zoneDao.update(zone);
                    nbZoneUbpdated += 1;
                }
            }
        }
        getTransaction().commit();
        return Pair.of(nbPlotUbpdated, nbZoneUbpdated);
    }

    private int togglePlotActivationStatus(boolean activation, Zone zone) {
        Plot plot = zone.getPlot();
        if (plot.isActive() != activation) {
            // unique key domain, edaplosissuerid, active
            boolean exists = plotDao.forDomainEquals(plot.getDomain())
                    .addEquals(Plot.PROPERTY_E_DAPLOS_ISSUER_ID, plot.geteDaplosIssuerId())
                    .addEquals(Plot.PROPERTY_ACTIVE, activation).exists();
            if (!exists) {
                plot.setActive(activation);
                plotDao.update(plot);
                return 1;
            }
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn(String.format("La parcelle %s edaplosissuerid: %s du domaine %s (%d), ne peux être changé de status à %s, à cause de la contrainte unique 'key domain, edaplosissuerid, active'",
                        plot.getName(), plot.geteDaplosIssuerId(), plot.getDomain().getName(), plot.getDomain().getCampaign(), activation ? "Active" : "Inactive"));
            }
        }
        return 0;
    }

    /**
     * Retourne une maps qui associe les codes des espèces copiées aux espèces de destination en comparant les espèces
     * par leurs code espece botanique.
     *
     * @param copiedInterventionDtos copiedInterventionDtos
     * @param toNodeOrPhaseCrop      main crop from node or phase
     * @param toIntermediateCrop     can be null
     * @return interventionDtos with their species
     */
    protected Map<EffectiveInterventionDto, Map<String, CroppingPlanSpecies>> getSameCodeEspeceBotniqueTargetedSpeciesByCodesByInterventionDto(
            Collection<EffectiveInterventionDto> copiedInterventionDtos,
            CroppingPlanEntry toNodeOrPhaseCrop, CroppingPlanEntry toIntermediateCrop) {

        Map<EffectiveInterventionDto, Map<String, CroppingPlanSpecies>> targetedSpeciesByCodesByInterventions = new HashMap<>();
        for (EffectiveInterventionDto interventionDto : copiedInterventionDtos) {
            interventionDto.setIntermediateCrop(interventionDto.isIntermediateCrop() && toIntermediateCrop != null);
            CroppingPlanEntry toCrop = interventionDto.isIntermediateCrop() ? toIntermediateCrop : toNodeOrPhaseCrop;

            if (toCrop == null) continue;

            // recuperation de la culture "from"
            CroppingPlanEntry fromCrop = croppingPlanEntryDao.forEquals(CroppingPlanEntryTopiaDao.CROPPING_PLAN_ENTRY_DOMAIN_ID, interventionDto.getDomainId())
                    .addEquals(CroppingPlanEntry.PROPERTY_CODE, interventionDto.getFromCropCode())
                    .findUniqueOrNull();
            if (fromCrop != null) {
                Map<String, CroppingPlanSpecies> toSpeciesMatchingCodeEspeceBotaniqueByCroppingPlanSpeciesCodes = new HashMap<>();
                for (CroppingPlanSpecies fromSpecies : CollectionUtils.emptyIfNull(fromCrop.getCroppingPlanSpecies())) {
                    toCrop.getCroppingPlanSpecies().stream()
                            .filter(s -> s.getSpecies().getCode_espece_botanique().equals(fromSpecies.getSpecies().getCode_espece_botanique()))
                            .findFirst().ifPresent(toSpecies -> toSpeciesMatchingCodeEspeceBotaniqueByCroppingPlanSpeciesCodes.put(fromSpecies.getCode(), toSpecies));
                }
                targetedSpeciesByCodesByInterventions.put(interventionDto, toSpeciesMatchingCodeEspeceBotaniqueByCroppingPlanSpeciesCodes);
            } else {
                LOGGER.error(String.format("Crop with code '%s' doesn't exist on domain '%s'", interventionDto.getFromCropCode(), interventionDto.getDomainId()));
                Map<String, CroppingPlanSpecies> toSpeciesMatchingCodeEspeceBotaniqueByCroppingPlanSpeciesCodes = new HashMap<>();
                targetedSpeciesByCodesByInterventions.put(interventionDto, toSpeciesMatchingCodeEspeceBotaniqueByCroppingPlanSpeciesCodes);
            }
        }
        return targetedSpeciesByCodesByInterventions;
    }

    /**
     * only add the seeding action species that match the targeted species
     */
    protected Map<EffectiveInterventionDto, Boolean> doEffectiveInterventionsMigration(
            CroppingPlanEntry targetedNodeOrPhaseCrop,
            CroppingPlanEntry targetedIntermediateCrop,
            Collection<EffectiveInterventionDto> interventionDtos,
            Map<String, String> fromSpeciesCodeToSpeciesCode,
            Map<EffectiveInterventionDto, Map<String, CroppingPlanSpecies>> targetedSpeciesByCodesByInterventionDto,
            Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant,
            Map<String, List<AbstractDomainInputStockUnit>> domainInputStockUnitByKeys,
            List<CroppingPlanSpecies> fromZoneSpecies) {

        debugLogs(fromSpeciesCodeToSpeciesCode, targetedSpeciesByCodesByInterventionDto);

        Map<EffectiveInterventionDto, Boolean> interventionWithMigratedActionDtos = new HashMap<>();

        for (EffectiveInterventionDto interventionDto : interventionDtos) {

            CroppingPlanEntry targetedCrop = interventionDto.isIntermediateCrop() ? targetedIntermediateCrop : targetedNodeOrPhaseCrop;
            Map<String, CroppingPlanSpecies> targetedSpeciesByCodes = targetedSpeciesByCodesByInterventionDto.get(interventionDto);
            // may be null
            List<SpeciesStadeDto> targetedSpeciesStades = getSpeciesStadesForInterventionFromSpeciesStadesBySpeciesCode(
                    interventionDto,
                    interventionDto.getFromCropCode(),
                    targetedCrop.getCode(),
                    fromSpeciesCodeToSpeciesCode,
                    targetedSpeciesByCodes
            );
            interventionDto.setSpeciesStadesDtos(targetedSpeciesStades);

            Collection<AbstractActionDto> interventionDtoActionDtos = interventionDto.getActionDtos();
            Collection<AbstractActionDto> seedingOrHarvestingActionDtos = CollectionUtils.emptyIfNull(interventionDtoActionDtos)
                    .stream()
                    .filter(aa -> AgrosystInterventionType.RECOLTE.equals(aa.getMainActionInterventionAgrosyst())
                            || AgrosystInterventionType.SEMIS.equals(aa.getMainActionInterventionAgrosyst()))
                    .toList();

            if (CollectionUtils.isEmpty(seedingOrHarvestingActionDtos)) {
                interventionWithMigratedActionDtos.put(interventionDto, Boolean.TRUE);
            } else {
                // les autres intrants seront récupérés à partir des usages
                // On les retire pour les rajouter si elles sont valides
                interventionDtoActionDtos.removeAll(seedingOrHarvestingActionDtos);

                Map<AbstractActionDto, Boolean> migratedSeedingOrHarvestingActionsDtos = actionService.migrateEffectiveActionsSpeciesToTargetedSpecies(
                        seedingOrHarvestingActionDtos,
                        targetedSpeciesByCodes, fromSpeciesCodeToSpeciesCode,
                        interventionDto.getFromCropCode(),
                        targetedCrop.getCode(),
                        interventionDto.getOriginalIntermediateStatus() != interventionDto.isIntermediateCrop(),
                        targetedSpeciesStades,
                        sectorByCodeEspeceBotaniqueCodeQualifiant,
                        domainInputStockUnitByKeys,
                        fromZoneSpecies);

                for (Map.Entry<AbstractActionDto, Boolean> migratedSeedingOrHarvestingActionsDto : migratedSeedingOrHarvestingActionsDtos.entrySet()) {
                    if (migratedSeedingOrHarvestingActionsDto.getValue()) {
                        interventionDtoActionDtos.add(migratedSeedingOrHarvestingActionsDto.getKey());
                    }
                }

                interventionWithMigratedActionDtos.put(interventionDto, !migratedSeedingOrHarvestingActionsDtos.containsValue(Boolean.FALSE));
            }

        }

        return interventionWithMigratedActionDtos;
    }

    protected List<SpeciesStadeDto> getSpeciesStadesForInterventionFromSpeciesStadesBySpeciesCode(
            EffectiveInterventionDto interventionDto,
            String fromCropCode,
            String toCropCode,
            Map<String, String> fromSpeciesCodeToSpeciesCode,
            Map<String, CroppingPlanSpecies> targetedSpeciesByCodes) {

        List<SpeciesStadeDto> fromSpeciesStadeDtos = ListUtils.emptyIfNull(interventionDto.getSpeciesStadesDtos());
        List<SpeciesStadeDto> validSpeciesStadeDtos = new ArrayList<>();

        // Source CP cible CP
        // Copier sur la CP.
        //  Si la CP cible a le même code que la CP source,
        //   -> cocher les espèces cibles qui sont les mêmes que les espèces sources (même refespece)
        //   -> sinon cocher toutes les espèces de la CP cible.

        // Source CI cible CI
        //  Si la CI cible a le même code que la CI source,
        //   -> cocher les espèces cibles qui sont les mêmes que les espèces sources (même refespece)
        //   -> sinon cocher toutes les espèces de la CI cible.

        // Source CI cible CP
        // Cocher toutes les espèces de la CP cible.
        if (fromCropCode.equals(toCropCode) && interventionDto.getOriginalIntermediateStatus() == interventionDto.isIntermediateCrop()) {
            for (SpeciesStadeDto fromSpeciesStadeDto : fromSpeciesStadeDtos) {
                final String speciesCode1 = fromSpeciesStadeDto.getSpeciesCode();
                String speciesCode = targetedSpeciesByCodes.get(speciesCode1) != null ? speciesCode1 : fromSpeciesCodeToSpeciesCode.get(speciesCode1);
                if (StringUtils.isNotBlank(speciesCode)) {
                    fromSpeciesStadeDto.setSpeciesCode(speciesCode);
                    validSpeciesStadeDtos.add(fromSpeciesStadeDto);
                }
            }
            // set as null as default behaviours is to create new intervention species stades if none.
            validSpeciesStadeDtos = validSpeciesStadeDtos.isEmpty() ? null : validSpeciesStadeDtos;

        } else {
            // set as null as default behaviours is to create new intervention species stades if none.
            validSpeciesStadeDtos = null;
        }
        //interventionDto.setSpeciesStadesDtos(validSpeciesStadeDtos);
        return validSpeciesStadeDtos;
    }

}
