package fr.inra.agrosyst.services.edaplos;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Certaines constantes AgroEDI.
 */
public class EdaplosConstants {

    /** Unité. */
    public enum UnitCode {
        CMT, // Centimètres
        ZIT, // Degrés Celsius
        DOS, // Dose (intrants)
        W95, // g/l
        HAR, // Hectares (code AEE)
        KGM, // Kilogramme (intrants)
        LTR, // Litre (intrant)
        ZHL, // m3 / ha (que pour l'amendement organique)
        MMT, // Millimètres (intrants)
        PCD, // Pourcentage
        W96, // Pourcentage de volume
        W97, // sans unité
        TNE, // Tonnes
        NAR, // Unité (grain, plants, capsule, diffuseur) (intrants)
        GRA, // Graines
        PLT  // Plants
    }
}
