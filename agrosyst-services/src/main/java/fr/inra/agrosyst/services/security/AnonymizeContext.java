package fr.inra.agrosyst.services.security;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.services.domain.DomainDto;
import fr.inra.agrosyst.api.services.domain.PlotDto;
import fr.inra.agrosyst.api.services.domain.ZoneDto;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanDto;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemDto;
import fr.inra.agrosyst.api.services.performance.PerformanceDto;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemDto;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.users.UserDto;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class AnonymizeContext {

    protected static final String XXXXX = "xxxxx";

    protected final BusinessAuthorizationService authorizationService;
    protected final Function<String, String> anonymizeFunction;

    protected Function<Domain, DomainDto> domainToDtoFunction;
    protected Function<GrowingPlan, GrowingPlanDto> growingPlanToDtoFunction;
    protected Function<GrowingSystem, GrowingSystemDto> growingSystemToDtoFunction;
    protected Function<Plot, PlotDto> plotToDtoFunction;
    protected Function<Zone, ZoneDto> zoneToDtoFunction;
    protected Function<PracticedSystem, PracticedSystemDto> practicedSystemToDtoFunction;
    protected Function<PracticedPlot, PracticedPlotDto> practicedPlotToDtoFunction;
    protected Function<Performance, PerformanceDto> performanceToDtoFunction;

    protected boolean allowUnreadable = false;
    protected Function<Domain, Boolean> shouldAnonymizeDomain;
    protected Function<GrowingPlan, Boolean> shouldAnonymizeGrowingPlan;

    protected boolean includeDomainResponsibles = false;
    protected boolean includeGrowingPlanResponsibles = false;
    protected boolean includeZonePlot = false;
    protected int limitPerformanceElements = Integer.MAX_VALUE;


    /**
     * Method that caches a transform result for a given Function. Using the return Function will avoid transforming
     * twice the same entity
     *
     * @param function the base function doing the transformation
     * @return the same function with cache facility
     */
    public <K, V> Function<K, V> cache(final Function<K, V> function) {
        // The cache instance storing the transformed entities
        final Map<String, V> cache = new HashMap<>();

        Function<K, V> result = input -> {
            if (input == null) {
                return null;
            }
            // Look for the entity key in the cache
            String key;
            if (input instanceof String) {
                key = (String) input;
            } else if (input instanceof TopiaEntity) {
                key = ((TopiaEntity) input).getTopiaId();
            } else {
                throw new UnsupportedOperationException("Unsupported key type : " + input.getClass());
            }
            V result1 = cache.computeIfAbsent(key, k -> function.apply(input));

            // If no result found, use the function
            return result1;
        };
        return result;
    }


    public AnonymizeContext(BusinessAuthorizationService authorizationService, Function<String, String> anonymizeFunction) {
        this.authorizationService = authorizationService;
        this.anonymizeFunction = cache(anonymizeFunction);
    }

    public AnonymizeContext includeDomainResponsibles(boolean b) {
        this.includeDomainResponsibles = b;
        return this;
    }

    public AnonymizeContext includeGrowingPlanResponsibles(boolean b) {
        includeGrowingPlanResponsibles = b;
        return this;
    }

    public AnonymizeContext includeZonePlot(boolean b) {
        includeZonePlot = b;
        return this;
    }

    public AnonymizeContext allowUnreadable(boolean b) {
        allowUnreadable = b;
        return this;
    }

    public AnonymizeContext limitPerformanceElements(int i) {
        this.limitPerformanceElements = i;
        return this;
    }


    public Function<Domain, DomainDto> getDomainToDtoFunction() {
        if (domainToDtoFunction == null) {
            domainToDtoFunction = cache(newDomainToDtoFunction());
        }
        return domainToDtoFunction;
    }

    public Function<GrowingPlan, GrowingPlanDto> getGrowingPlanToDtoFunction() {
        if (growingPlanToDtoFunction == null) {
            growingPlanToDtoFunction = cache(newGrowingPlanToDtoFunction());
        }
        return growingPlanToDtoFunction;
    }

    public Function<GrowingSystem, GrowingSystemDto> getGrowingSystemToDtoFunction() {
        if (growingSystemToDtoFunction == null) {
            growingSystemToDtoFunction = cache(newGrowingSystemToDtoFunction());
        }
        return growingSystemToDtoFunction;
    }

    public Function<Plot, PlotDto> getPlotToDtoFunction() {
        if (plotToDtoFunction == null) {
            plotToDtoFunction = cache(newPlotToDtoFunction());
        }
        return plotToDtoFunction;
    }

    public Function<Zone, ZoneDto> getZoneToDtoFunction() {
        if (zoneToDtoFunction == null) {
            Function<Zone, ZoneDto> function = newZoneToDtoFunction();
            zoneToDtoFunction = cache(function);
        }
        return zoneToDtoFunction;
    }

    public Function<PracticedSystem, PracticedSystemDto> getPracticedSystemToDtoFunction() {
        if (practicedSystemToDtoFunction == null) {
            practicedSystemToDtoFunction = cache(newPracticedSystemToDtoFunction());
        }
        return practicedSystemToDtoFunction;
    }

    public Function<PracticedPlot, PracticedPlotDto> getPracticedPlotToDtoFunction() {
        if (practicedPlotToDtoFunction == null) {
            practicedPlotToDtoFunction = cache(newPracticedPlotToDtoFunction());
        }
        return practicedPlotToDtoFunction;
    }

    public Function<Performance, PerformanceDto> getPerformanceToDtoFunction() {
        if (performanceToDtoFunction == null) {
            performanceToDtoFunction = cache(newPerformanceToDtoFunction());
        }
        return performanceToDtoFunction;
    }

    public Function<Domain, Boolean> getShouldAnonymizeDomain() {
        if (shouldAnonymizeDomain == null) {
            shouldAnonymizeDomain = cache(input -> authorizationService.shouldAnonymizeDomain(input.getTopiaId(), allowUnreadable));
        }
        return shouldAnonymizeDomain;
    }

    public Function<GrowingPlan, Boolean> getShouldAnonymizeGrowingPlan() {
        if (shouldAnonymizeGrowingPlan == null) {
            shouldAnonymizeGrowingPlan = cache(input -> authorizationService.shouldAnonymizeGrowingPlan(input.getTopiaId(), allowUnreadable));
        }
        return shouldAnonymizeGrowingPlan;
    }

    protected Function<Domain, DomainDto> newDomainToDtoFunction() {
        return input -> {
            if (input == null) {
                return null;
            }
            DomainDto result = new DomainDto();
            result.setName(input.getName());
            result.setMainContact(input.getMainContact());
            result.setTopiaId(input.getTopiaId());
            result.setCode(input.getCode());
            result.setCampaign(input.getCampaign());
            result.setType(input.getType());
            result.setLocation(input.getLocation());
            result.setActive(input.isActive());
            result.setSiret(input.getSiret());

            boolean shouldAnonymizeDomain = getShouldAnonymizeDomain().apply(input);
            if (shouldAnonymizeDomain) {
                result.setName(anonymizeFunction.apply(input.getName()));
                result.setMainContact(XXXXX);
            }

            List<UserDto> responsibles;
//                if (includeResponsibles && !shouldAnonymizeDomain) {
            if (includeDomainResponsibles) {
                responsibles = authorizationService.getDomainResponsibles(input.getCode());
            } else {
                responsibles = new ArrayList<>();
            }
            result.setResponsibles(responsibles);
            return result;
        };
    }


    protected Function<GrowingPlan, GrowingPlanDto> newGrowingPlanToDtoFunction() {
        return input -> {
            if (input == null) {
                return null;
            }
            GrowingPlanDto result = new GrowingPlanDto();
            result.setTopiaId(input.getTopiaId());
            result.setCode(input.getCode());
            result.setDomain(getDomainToDtoFunction().apply(input.getDomain()));
            result.setType(input.getType());
            result.setActive(input.isActive());

            boolean shouldAnonymizeGrowingPlan = getShouldAnonymizeGrowingPlan().apply(input);
            if (shouldAnonymizeGrowingPlan) {
                result.setName(anonymizeFunction.apply(input.getName()));
            } else {
                result.setName(input.getName());
            }

            List<UserDto> responsibles;
//                if (includeResponsibles && !shouldAnonymizeGrowingPlan) {
            if (includeGrowingPlanResponsibles) {
                responsibles = authorizationService.getGrowingPlanResponsibles(input.getCode());
            } else {
                responsibles = new ArrayList<>();
            }
            result.setResponsibles(responsibles);
            return result;
        };
    }


    protected Function<GrowingSystem, GrowingSystemDto> newGrowingSystemToDtoFunction() {
        return input -> {
            if (input == null) {
                return null;
            }
            GrowingSystemDto result = new GrowingSystemDto();
            result.setTopiaId(input.getTopiaId());
            result.setCode(input.getCode());
            result.setName(input.getName());
            result.setValidated(input.isValidated());
            result.setActive(input.isActive());
            result.setDephyNumber(input.getDephyNumber());
            result.setSector(input.getSector());
            result.setGrowingPlan(getGrowingPlanToDtoFunction().apply(input.getGrowingPlan()));
            if (input.getNetworks() != null) {
                Map<String, String> networks = Maps.newLinkedHashMap();
                result.setNetworks(networks);
                for (Network network : input.getNetworks()) {
                    networks.put(network.getTopiaId(), network.getName());
                }
            }
            return result;
        };
    }

    protected Function<Plot, PlotDto> newPlotToDtoFunction() {
        return input -> {
            if (input == null) {
                return null;
            }
            PlotDto result = new PlotDto();
            result.setTopiaId(input.getTopiaId());
            result.setCode(input.getCode());
            result.setArea(input.getArea());
            result.setGrowingSystem(getGrowingSystemToDtoFunction().apply(input.getGrowingSystem()));
            result.setDomain(getDomainToDtoFunction().apply(input.getDomain()));
            result.setActive(input.isActive());

            if (getShouldAnonymizeDomain().apply(input.getDomain())) {
                result.setName(anonymizeFunction.apply(input.getName()));
            } else {
                result.setName(input.getName());
            }
            return result;
        };

    }


    protected Function<Zone, ZoneDto> newZoneToDtoFunction() {
        return input -> {
            if (input == null) {
                return null;
            }
            ZoneDto result = new ZoneDto();
            result.setTopiaId(input.getTopiaId());
            result.setCode(input.getCode());
            result.setName(input.getName());
            result.setActive(input.isActive());
            Plot plot = input.getPlot();
            if (includeZonePlot) {
                result.setPlot(getPlotToDtoFunction().apply(plot));
            } else {
                if (plot != null) {
                    PlotDto plotDto = new PlotDto();
                    plotDto.setTopiaId(plot.getTopiaId());
                    result.setPlot(plotDto);
                }
            }
            return result;
        };

    }

    protected Function<PracticedSystem, PracticedSystemDto> newPracticedSystemToDtoFunction() {
        return input -> {
            if (input == null) {
                return null;
            }
            PracticedSystemDto result = new PracticedSystemDto();
            result.setTopiaId(input.getTopiaId());
            result.setName(input.getName());
            result.setGrowingSystem(getGrowingSystemToDtoFunction().apply(input.getGrowingSystem()));
            result.setCampaigns(input.getCampaigns());
            result.setActive(input.isActive());
            result.setValidated(input.isValidated());
            return result;
        };
    }

    protected Function<PracticedPlot, PracticedPlotDto> newPracticedPlotToDtoFunction() {
        return input -> {
            if (input == null) {
                return null;
            }
            PracticedPlotDto result = new PracticedPlotDto();
            result.setTopiaId(input.getTopiaId());
            result.setName(input.getName());
            result.setActive(input.isActive());
            result.setPracticedSystems(
                    input.getPracticedSystem().stream()
                        .map(getPracticedSystemToDtoFunction())
                        .collect(Collectors.toSet())
            );
            return result;
        };
    }

    protected Function<Performance, PerformanceDto> newPerformanceToDtoFunction() {

        return (Performance input) -> {
            if (input == null) {
                return null;
            }
            PerformanceDto result = new PerformanceDto();
            result.setTopiaId(input.getTopiaId());
            result.setName(input.getName());
            result.setComputeStatus(input.getComputeStatus());
            result.setUpdateDate(input.getUpdateDate());

            PaginationParameter page = PaginationParameter.of(0, limitPerformanceElements);
            if (input.getDomains() != null) {
                List<DomainDto> domains = input.getDomains()
                        .stream()
                        .map(getDomainToDtoFunction())
                        .limit(limitPerformanceElements)
                        .collect(Collectors.toList());
                result.setDomains(PaginationResult.of(domains, input.getDomains().size(), page));
            }
            if (input.getGrowingSystems() != null) {
                List<GrowingSystemDto> growingSystems = input.getGrowingSystems()
                        .stream()
                        .map(getGrowingSystemToDtoFunction())
                        .limit(limitPerformanceElements)
                        .collect(Collectors.toList());
                result.setGrowingSystems(PaginationResult.of(growingSystems, input.getGrowingSystems().size(), page));
            }
            if (input.getPlots() != null) {
                List<PlotDto> plots = input.getPlots()
                        .stream()
                        .map(getPlotToDtoFunction())
                        .limit(limitPerformanceElements)
                        .collect(Collectors.toList());
                result.setPlots(PaginationResult.of(plots, input.getPlots().size(), page));
            }
            if (input.getZones() != null) {
                List<ZoneDto> zones = input.getZones()
                        .stream()
                        .map(getZoneToDtoFunction())
                        .limit(limitPerformanceElements)
                        .collect(Collectors.toList());
                result.setZones(PaginationResult.of(zones, input.getZones().size(), page));
            }
            return result;
        };
    }

}
