package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefNrjCarburant;
import fr.inra.agrosyst.api.entities.referential.RefNrjCarburantImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * libelle carburant;unité;NRJ (MJ/unite);Source
 * 
 * @author Eric Chatellier
 */
public class RefNrjCarburantModel extends AbstractAgrosystModel<RefNrjCarburant> implements ExportModel<RefNrjCarburant> {

    public RefNrjCarburantModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("libelle carburant", RefNrjCarburant.PROPERTY_LIBELLE_CARBURANT);
        newMandatoryColumn("unité", RefNrjCarburant.PROPERTY_UNITE);
        newMandatoryColumn("NRJ (MJ/unite)", RefNrjCarburant.PROPERTY_NRJ, DOUBLE_PARSER);
        newMandatoryColumn("Source", RefNrjCarburant.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefNrjCarburant.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefNrjCarburant, Object>> getColumnsForExport() {
        ModelBuilder<RefNrjCarburant> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("libelle carburant", RefNrjCarburant.PROPERTY_LIBELLE_CARBURANT);
        modelBuilder.newColumnForExport("unité", RefNrjCarburant.PROPERTY_UNITE);
        modelBuilder.newColumnForExport("NRJ (MJ/unite)", RefNrjCarburant.PROPERTY_NRJ, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefNrjCarburant.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefNrjCarburant.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefNrjCarburant newEmptyInstance() {
        RefNrjCarburant refNrjCarburant = new RefNrjCarburantImpl();
        refNrjCarburant.setActive(true);
        return refNrjCarburant;
    }
}
