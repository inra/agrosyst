package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jakarta.validation.Valid;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * {@code
 * <ram:SequenceNumeric>0.0</ram:SequenceNumeric>
 * <ram:StartDateTime>2007-10-13T09:30:47Z</ram:StartDateTime>
 * <ram:EndDateTime>2008-08-18T09:30:47Z</ram:EndDateTime>
 * <ram:ProductionYearDateTime>2008-12-31T09:30:47Z</ram:ProductionYearDateTime>
 * }
 * </pre>
 */
public class AgriculturalCropProductionCycle implements AgroEdiObject {

    protected String sequenceNumeric;

    protected String startDateTime;

    protected String endDateTime;

    protected String productionYearDateTime;

    @Valid
    protected List<PlotAgriculturalProcess> applicablePlotAgriculturalProcesss = new ArrayList<>();

    public String getSequenceNumeric() {
        return sequenceNumeric;
    }

    public void setSequenceNumeric(String sequenceNumeric) {
        this.sequenceNumeric = sequenceNumeric;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getProductionYearDateTime() {
        return productionYearDateTime;
    }

    public void setProductionYearDateTime(String productionYearDateTime) {
        this.productionYearDateTime = productionYearDateTime;
    }

    public List<PlotAgriculturalProcess> getApplicablePlotAgriculturalProcesss() {
        return applicablePlotAgriculturalProcesss;
    }

    public void setApplicablePlotAgriculturalProcesss(List<PlotAgriculturalProcess> applicablePlotAgriculturalProcesss) {
        this.applicablePlotAgriculturalProcesss = applicablePlotAgriculturalProcesss;
    }

    public void addApplicablePlotAgriculturalProcess(PlotAgriculturalProcess applicablePlotAgriculturalProcess) {
        applicablePlotAgriculturalProcesss.add(applicablePlotAgriculturalProcess);
    }

    @Override
    public String getLocalizedIdentifier() {
        return "cycle '" + sequenceNumeric + "'";
    }
}
