package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
public class AllSpeciesExport {

    public static final String PROPERTY_LIBELLE_ESPECE_BOTANIQUE_QUALIFIANT_AEE = "libelle_espece_botanique_qualifiant_aee";
    public static final String PROPERTY_VARIETE_LABEL = "variete_label";

    protected String libelle_espece_botanique_qualifiant_aee;
    protected String variete_label;

    public AllSpeciesExport() {
    }

    public AllSpeciesExport(Pair<RefEspece, RefVariete> refEspeceVariete) {
        RefEspece refEspece = refEspeceVariete.getLeft();
        List<String> libelle = new ArrayList<>();

        libelle.add(refEspece.getLibelle_espece_botanique_Translated());

        if (StringUtils.isNotBlank(refEspece.getLibelle_qualifiant_AEE())) {
            libelle.add(refEspece.getLibelle_qualifiant_AEE());
        }

        libelle_espece_botanique_qualifiant_aee = String.join(" ", libelle);

        RefVariete refVariete = refEspeceVariete.getValue();
        if (refVariete != null) {
            variete_label = refVariete.getLabel();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AllSpeciesExport that = (AllSpeciesExport) o;
        return Objects.equals(libelle_espece_botanique_qualifiant_aee, that.libelle_espece_botanique_qualifiant_aee) &&
                Objects.equals(variete_label, that.variete_label);
    }

    @Override
    public int hashCode() {

        return Objects.hash(libelle_espece_botanique_qualifiant_aee, variete_label);
    }
}
