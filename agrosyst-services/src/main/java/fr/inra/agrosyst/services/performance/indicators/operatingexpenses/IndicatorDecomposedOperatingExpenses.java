package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import static org.nuiton.i18n.I18n.l;

/**
 * Les charges opérationnelles réelles décomposées sont exprimées en €/ha.
 * Elles correspondent aux dépenses liées à l’achat des intrants (semences et plants,
 * produits fertilisants minéraux et organiques, traitement de semences et plants,
 * irrigation, produits phytosanitaires, produits de lutte biologique, autres intrants, substrats, pots).
 * <p>
 * Le calcul se fait sur la base des prix saisis par l’utilisateur.
 * <p>
 * Les interventions concernées par cet indicateur sont toutes les interventions
 * contenant un intrant et/ou une action de type « Semis » et/ou une action de
 * type « Irrigation ».
 * <p>
 * Formule de calcul:
 *
 * <pre>
 * CI réel_i:
 *
 * = PSCi * ( sum(Q_ev * PA_ev) +
 *    sum(Q_j * PA_j) +
 *    sum(Q_e * PA_e) +
 *    sum(Q_a * PA_a)) +
 *    PSCi_phyto * sum(Q_k * PA_k) +
 *    PSCi_luttebio * sum(Q_h * PA_h)
 *
 * Avec :
 * - CI réel_i : charges intrants de l’intervention i
 * - PSCi (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils
 *   ou l’opération manuelle de l’intervention i.
 *   PSCi est calculé sur la base des données saisies par l’utilisateur.
 *
 *  - Semis :
 *   - Q_ev (diverses unités) : quantité semée du couple EV, EV appartenant à la liste des couples EV semés
 *     dans l’action semis de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *   - PA_ev (diverses unités) : prix d’achat du couple EV pour ce type de semence (de ferme, certifiées ...),
 *     EV appartenant à la liste des couples EV semés au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *      - ATTENTION :  dès lors que la case « traitement inclus ... » est cochée, ne plus réaliser l’étape décrite dans
 *        le paragraphe ci-dessous pour ce qui est du traitement de semence (par contre, réaliser l’opération pour les
 *        intrants de fertilisation). Car il ne faut pas compter cet intrant 2 fois.
 *
 * - Fertilisation minérale et organique  – Traitements de semences
 *   - Q_j (diverses unités) : quantité de l’intrant j, j appartenant à la liste
 *     des intrants de type « Traitements de semences »,
 *     « Engrais/amendement (organo) minéral »
 *     ou « Engrais/amendement organique » appliqués au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *   - PA_j (diverses unités) : prix d’achat de l’intrant j, j appartenant à la liste
 *     des intrants de type « Traitements de semences »,
 *     « Engrais/amendement (organo) minéral »
 *     ou « Engrais/amendement organique » appliqués au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *
 * - Irrigation
 *   - Q_e (diverses unités) : quantité d’eau déclarée dans l’action de type Irrigation
 *     au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *   - PA_e (€/m³) : prix d’achat de l’eau.
 *
 *     Si unité = €/ha, alors charge de l'intervention = PSCi x Prix en €/ha
 *
 * - Autres intrants
 *   - Q_a (diverses unités) : quantité de l'intrant autre. Donnée saisie par l’utilisateur.
 *   - PA_a (unité unique) : pour le moment, l’utilisateur ne peut saisir un prix que dans une seule unité : Euros/ha.
 *     Mais il faut prévoir à termes que l’utilisateur puisse saisir d’autres unités
 *     et donc adopter le même fonctionnement que pour tous les autres intrants. Donnée saisie par l’utilisateur.
 *
 * - Phytosanitaire
 *   - PSCi_phyto (sans unité) : proportion de surface concernée par
 *     l’action de type application de produit phytosanitaire.
 *   - Q_k (diverses unités) : quantité de l’intrant k, k appartenant à la liste des intrants de type
 *     « Phytosanitaire » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *   - PA_k (diverses unités) : prix d’achat de l ’intrant k, k appartenant à la liste des intrants de type
 *     « Phytosanitaire » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *
 * - Lutte biologique
 *   - PSCi_luttebio (sans unité) : proportion de surface concernée par l’action de type lutte biologique.
 *   - Q_h (diverses unités) : quantité de l’intrant h, h appartenant à la liste des intrants de type
 *     « Lutte biologique » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *   - PA_h (diverses unités) : prix d’achat de l’intrant h,
 *     h appartenant à la liste des intrants de type « Lutte biologique » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *
 * </pre>
 *
 * @author Geoffroy Gley (gley@codelutin.com)
 */
public class IndicatorDecomposedOperatingExpenses extends IndicatorOperatingExpenses {

    protected static final Log LOGGER = LogFactory.getLog(IndicatorDecomposedOperatingExpenses.class);

    public static final String CHARGES_OPERATIONNELLES_REELLES_DEC_TX_COMP = "charges_operationnelles_reelles_decomposees_taux_de_completion";
    public static final String CHARGES_OPERATIONNELLES_STD_MIL_DEC_TX_COMP = "charges_operationnelles_std_mil_decomposees_taux_de_completion";
    public static final String CHARGES_OPERATIONNELLES_REELLES_DEC_DETAIL = "charges_operationnelles_reelles_decomposees_detail_champs_non_renseig";
    public static final String CHARGES_OPERATIONNELLES_STD_MIL_DEC_DETAIL = "charges_operationnelles_std_mil_decomposees_detail_champs_non_renseig";

    protected static final String[] SEMIS_LABELS = {
            "Indicator.label.operatingExpensesRealSemis",
            "Indicator.label.operatingExpensesStandardSemis"
    };
    private static final String[] FERTILISATION_MINERALE_LABELS = {
            "Indicator.label.operatingExpensesRealFertilisationMinerale",
            "Indicator.label.operatingExpensesStandardFertilisationMinerale"
    };
    private static final String[] EPANDAGE_ORGANIQUE_LABELS = {
            "Indicator.label.operatingExpensesRealEpandageOrganique",
            "Indicator.label.operatingExpensesStandardEpandagesOrganiques"
    };
    protected static final String[] TRAITEMENTS_DE_SEMENCES_LABELS = {
            "Indicator.label.operatingExpensesRealTraitementsSemences",
            "Indicator.label.operatingExpensesStandardTraitementsSemences",
    };
    private static final String[] IRRIGATION_LABELS = {
            "Indicator.label.operatingExpensesRealIrrigation",
            "Indicator.label.operatingExpensesStandardIrrigation"
    };
    protected static final String[] PHYTO_AVEC_AMM_LABELS = {
            "Indicator.label.operatingExpensesRealPhytoAvecAmm",
            "Indicator.label.operatingExpensesStandardPhytoAvecAmm"
    };
    protected static final String[] PHYTO_SANS_AMM_LABELS = {
            "Indicator.label.operatingExpensesRealPhytoSansAmm",
            "Indicator.label.operatingExpensesStandardPhytoSansAmm"
    };
    private static final String[] SUBSTRAT_LABELS = {
            "Indicator.label.operatingExpensesRealSubstrat",
            "Indicator.label.operatingExpensesStandardSubstrat"
    };
    private static final String[] POTS_LABELS = {
            "Indicator.label.operatingExpensesRealPots",
            "Indicator.label.operatingExpensesStandardPots"
    };
    private static final String[] AUTRES_LABELS = {
            "Indicator.label.operatingExpensesRealIntrantsEquipementsAutres",
            "Indicator.label.operatingExpensesStandardIntrantsEquipementsAutres"
    };
    private static final String[] LABELS = Stream.of(SEMIS_LABELS, FERTILISATION_MINERALE_LABELS,
                    EPANDAGE_ORGANIQUE_LABELS, TRAITEMENTS_DE_SEMENCES_LABELS, IRRIGATION_LABELS, PHYTO_AVEC_AMM_LABELS,
                    PHYTO_SANS_AMM_LABELS, SUBSTRAT_LABELS, POTS_LABELS, AUTRES_LABELS)
            .flatMap(Stream::of)
            .toArray(String[]::new);

    private Boolean[] doesToDisplay = new Boolean[]{
            true,  // semis
            true,  // fertilisation minérale
            true,  // épandage organique
            true,  // traitements de semences
            true,  // irrigation
            true,  // produit phyto avec amm
            true,  // produit phyto sans amm
            true,  // substrat
            true,  // pots
            true,  // intrants et équipements autres
    };

    public IndicatorDecomposedOperatingExpenses() {
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_OPERATIONNELLES_REELLES_TX_COMP,
                CHARGES_OPERATIONNELLES_REELLES_DEC_TX_COMP
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_OPERATIONNELLES_REELLES_DETAIL,
                CHARGES_OPERATIONNELLES_REELLES_DEC_DETAIL
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_OPERATIONNELLES_STD_MIL_TX_COMP,
                CHARGES_OPERATIONNELLES_STD_MIL_DEC_TX_COMP
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_OPERATIONNELLES_STD_MIL_DETAIL,
                CHARGES_OPERATIONNELLES_STD_MIL_DEC_DETAIL
        );
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = new HashMap<>();

        indicatorNameToColumnName.put(getIndicatorLabel(0), "charges_operationnelles_semis_reelles");
        indicatorNameToColumnName.put(getIndicatorLabel(1), "charges_operationnelles_semis_std_mil");

        indicatorNameToColumnName.put(getIndicatorLabel(2), "charges_operationnelles_fertimin_reelles");
        indicatorNameToColumnName.put(getIndicatorLabel(3), "charges_operationnelles_fertimin_std_mil");

        indicatorNameToColumnName.put(getIndicatorLabel(4), "charges_operationnelles_epandage_orga_reelles");
        indicatorNameToColumnName.put(getIndicatorLabel(5), "charges_operationnelles_epandage_orga_std_mil");

        indicatorNameToColumnName.put(getIndicatorLabel(6), "charges_operationnelles_trait_semence_reelles");
        indicatorNameToColumnName.put(getIndicatorLabel(7), "charges_operationnelles_trait_semence_std_mil");

        indicatorNameToColumnName.put(getIndicatorLabel(8), "charges_operationnelles_irrigation_reelles");
        indicatorNameToColumnName.put(getIndicatorLabel(9), "charges_operationnelles_irrigation_std_mil");

        indicatorNameToColumnName.put(getIndicatorLabel(10), "charges_operationnelles_phyto_avec_amm_reelles");
        indicatorNameToColumnName.put(getIndicatorLabel(11), "charges_operationnelles_phyto_avec_amm_std_mil");

        indicatorNameToColumnName.put(getIndicatorLabel(12), "charges_operationnelles_phyto_sans_amm_reelles");
        indicatorNameToColumnName.put(getIndicatorLabel(13), "charges_operationnelles_phyto_sans_amm_std_mil");

        indicatorNameToColumnName.put(getIndicatorLabel(14), "charges_operationnelles_substrats_reelles");
        indicatorNameToColumnName.put(getIndicatorLabel(15), "charges_operationnelles_substrats_std_mil");

        indicatorNameToColumnName.put(getIndicatorLabel(16), "charges_operationnelles_pots_reelles");
        indicatorNameToColumnName.put(getIndicatorLabel(17), "charges_operationnelles_pots_std_mil");

        indicatorNameToColumnName.put(getIndicatorLabel(18), "charges_operationnelles_intrants_autres_reelles");
        indicatorNameToColumnName.put(getIndicatorLabel(19), "charges_operationnelles_intrants_autres_std_mil");

        indicatorNameToColumnName.put(
                CHARGES_OPERATIONNELLES_REELLES_DEC_TX_COMP, CHARGES_OPERATIONNELLES_REELLES_DEC_TX_COMP);
        indicatorNameToColumnName.put(
                CHARGES_OPERATIONNELLES_STD_MIL_DEC_TX_COMP, CHARGES_OPERATIONNELLES_STD_MIL_DEC_TX_COMP);
        indicatorNameToColumnName.put(
                CHARGES_OPERATIONNELLES_REELLES_DEC_DETAIL, CHARGES_OPERATIONNELLES_REELLES_DEC_DETAIL);
        indicatorNameToColumnName.put(
                CHARGES_OPERATIONNELLES_STD_MIL_DEC_DETAIL, CHARGES_OPERATIONNELLES_STD_MIL_DEC_DETAIL);

        return indicatorNameToColumnName;
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return doesToDisplay[i / 2] && (i % 2 == 0 && computeReal || i % 2 == 1 && computStandardised);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {
        var result = newArray(LABELS.length, 0.0d);

        if (interventionContext.isFictive()) {
            return result;
        }

        Set<SeedingActionUsage> actionCropPricesIncludedTreatment = new HashSet<>();
        // semis
        if (doesToDisplay[0]) {
            var seedingResults = computeSeedingForCrop(globalExecutionContext, writerContext, practicedSystemContext, interventionContext, actionCropPricesIncludedTreatment);
            result[0] = seedingResults.getLeft();
            result[1] = seedingResults.getRight();
        }

        // fertilisation minérale
        if (doesToDisplay[1]) {
            var mineralResult = computeMineral(globalExecutionContext, writerContext, practicedSystemContext, interventionContext, IndicatorDecomposedOperatingExpenses.class, FERTILISATION_MINERALE_LABELS);
            result[2] = mineralResult[0];
            result[3] = mineralResult[1];
        }

        // épandage organique
        if (doesToDisplay[2]) {
            var organicResult = computeOrganic(globalExecutionContext, writerContext, practicedSystemContext, interventionContext, IndicatorDecomposedOperatingExpenses.class, EPANDAGE_ORGANIQUE_LABELS);
            result[4] = organicResult[0];
            result[5] = organicResult[1];
        }

        // traitements de semences
        if (doesToDisplay[3]) {
            var seedingResults = computeSeedingForProduct(globalExecutionContext, writerContext, practicedSystemContext, interventionContext, actionCropPricesIncludedTreatment);
            result[6] = seedingResults.getLeft();
            result[7] = seedingResults.getRight();
        }

        // irrigation
        if (doesToDisplay[4]) {
            var irrigationResult = computeIrrig(writerContext, practicedSystemContext, interventionContext, IndicatorDecomposedOperatingExpenses.class, IRRIGATION_LABELS);
            result[8] = irrigationResult[0];
            result[9] = irrigationResult[1];
        }

        // produit phyto avec amm
        if (doesToDisplay[5]) {
            var phytoResult = computePesticidePhyto(globalExecutionContext, writerContext, practicedSystemContext, interventionContext);
            result[10] = phytoResult[0];
            result[11] = phytoResult[1];
        }

        // produit phyto sans amm
        if (doesToDisplay[6]) {
            var phytoResult = computeBiologicalPhyto(globalExecutionContext, writerContext, practicedSystemContext, interventionContext);
            result[12] = phytoResult[0];
            result[13] = phytoResult[1];
        }

        // substrat
        if (doesToDisplay[7]) {
            var substrateResult = computeSubstrate(globalExecutionContext, writerContext, practicedSystemContext, interventionContext, IndicatorDecomposedOperatingExpenses.class, SUBSTRAT_LABELS);
            result[14] = substrateResult[0];
            result[15] = substrateResult[1];
        }

        // pots
        if (doesToDisplay[8]) {
            var potResult = computePot(globalExecutionContext, writerContext, practicedSystemContext, interventionContext, IndicatorDecomposedOperatingExpenses.class, POTS_LABELS);
            result[16] = potResult[0];
            result[17] = potResult[1];
        }

        // intrants et équipements autres
        if (doesToDisplay[9]) {
            var otherResult = computeOther(globalExecutionContext, writerContext, practicedSystemContext, interventionContext, IndicatorDecomposedOperatingExpenses.class, AUTRES_LABELS);
            result[18] = otherResult[0];
            result[19] = otherResult[1];
        }

        return result;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {
        var result = new Double[20];
        Arrays.fill(result, 0d);

        Set<SeedingActionUsage> actionCropPricesIncludedTreatment = new HashSet<>();
        // semis
        if (doesToDisplay[0]) {
            var seedingResults = computeSeedingForCrop(globalExecutionContext, writerContext, domainContext, interventionContext, actionCropPricesIncludedTreatment);
            result[0] = seedingResults.getLeft();
            result[1] = seedingResults.getRight();
        }

        // fertilisation minérale
        if (doesToDisplay[1]) {
            var mineralResult = computeMineral(globalExecutionContext, writerContext, domainContext, zoneContext, interventionContext, IndicatorDecomposedOperatingExpenses.class, FERTILISATION_MINERALE_LABELS);
            result[2] = mineralResult[0];
            result[3] = mineralResult[1];
        }

        // épandage organique
        if (doesToDisplay[2]) {
            var organicResult = computeOrganic(globalExecutionContext, writerContext, domainContext, zoneContext, interventionContext, IndicatorDecomposedOperatingExpenses.class, EPANDAGE_ORGANIQUE_LABELS);
            result[4] = organicResult[0];
            result[5] = organicResult[1];
        }

        // traitements de semences
        if (doesToDisplay[3]) {
            var seedingResults = computeSeedingForProduct(globalExecutionContext, writerContext, domainContext, interventionContext, actionCropPricesIncludedTreatment);
            result[6] = seedingResults.getLeft();
            result[7] = seedingResults.getRight();
        }

        // irrigation
        if (doesToDisplay[4]) {
            var irrigationResult = computeIrrig(writerContext, domainContext, zoneContext, interventionContext, IndicatorDecomposedOperatingExpenses.class, IRRIGATION_LABELS);
            result[8] = irrigationResult[0];
            result[9] = irrigationResult[1];
        }

        // produit phyto avec amm
        if (doesToDisplay[5]) {
            var phytoResult = computePesticidePhyto(globalExecutionContext, writerContext, domainContext, zoneContext, interventionContext);
            result[10] = phytoResult[0];
            result[11] = phytoResult[1];
        }

        // produit phyto sans amm
        if (doesToDisplay[6]) {
            var phytoResult = computeBiologicalPhyto(globalExecutionContext, writerContext, domainContext, zoneContext, interventionContext);
            result[12] = phytoResult[0];
            result[13] = phytoResult[1];
        }

        // substrat
        if (doesToDisplay[7]) {
            var substrateResult = computeSubstrate(globalExecutionContext, writerContext, domainContext, zoneContext, interventionContext, IndicatorDecomposedOperatingExpenses.class, SUBSTRAT_LABELS);
            result[14] = substrateResult[0];
            result[15] = substrateResult[1];
        }

        // pots
        if (doesToDisplay[8]) {
            var potResult = computePot(globalExecutionContext, writerContext, domainContext, zoneContext, interventionContext, IndicatorDecomposedOperatingExpenses.class, POTS_LABELS);
            result[16] = potResult[0];
            result[17] = potResult[1];
        }

        // intrants et équipements autres
        if (doesToDisplay[9]) {
            var otherResult = computeOther(globalExecutionContext, writerContext, domainContext, zoneContext, interventionContext, IndicatorDecomposedOperatingExpenses.class, AUTRES_LABELS);
            result[18] = otherResult[0];
            result[19] = otherResult[1];
        }

        return result;
    }

    public void init(IndicatorFilter indicatorFilter) {
        super.init(indicatorFilter);
        resetDoeToDisplay();
        if (indicatorFilter != null && indicatorFilter.getDoeIndicators() != null) {
            indicatorFilter.getDoeIndicators().forEach(it -> this.doesToDisplay[it.ordinal()] = true);
        }
    }

    protected void resetDoeToDisplay() {
        this.doesToDisplay = new Boolean[]{
                false,  // semis
                false,  // fertilisation minérale
                false,  // épandage organique
                false,  // traitements de semences
                false,  // irrigation
                false,  // produit phyto avec amm
                false,  // produit phyto sans amm
                false,  // substrat
                false,  // pots
                false,  // intrants et équipements autres
        };
    }
}
