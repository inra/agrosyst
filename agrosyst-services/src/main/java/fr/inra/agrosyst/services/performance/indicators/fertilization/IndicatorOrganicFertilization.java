package fr.inra.agrosyst.services.performance.indicators.fertilization;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.OrganicFertilization;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.nuiton.i18n.I18n.l;

/**
 * Calcule les indicateurs de quantités de fertilisants organiques utilisés.
 *
 * <p>
 *     Méthode de calcul générale : on se base sur les actions d'épandage organique, qui utilise plusieurs intrants
 *     organiques. Pour chacun de ces intrants, on peut en obtenir la composition (en kg/t ou kg/m³, voir {@link fr.inra.agrosyst.api.entities.action.FertiOrgaUnit}).
 * </p>
 *
 * <p>Et l'intrant a une unité d'application qui peut être : kg/ha, m³/ha, t/ha (voir {@link fr.inra.agrosyst.api.entities.OrganicProductUnit}.</p>
 *
 * <p>
 *     On ramène toujours l'unité d'application en kg/ha, et il faut donc ramener l'unité de composition des intrants vers
 *     du kg/kg (kilogramme de substance par kilogramme d'intrant), c'est-à-dire qu'il faut diviser la quantité que
 *     l'on trouve par 1000.
 * </p>
 *
 *
 * <p>Ainsi, en prenant l'exemple de l'azote (N), on a la formule de calcul suivante :</p>
 *
 * <pre>N organique (kg/ha) = (Dose de produit (x/ha) * Composition de l'intrant (kg/x) ) * PSCI</pre>
 *
 * Dans cette formule, le x représente une unité de masse qui doit être la même pour la composition de l'intrant
 * et pour la dose de produit, dans l'implémentation du calcul nous avons fait le choix d'utiliser le kg.
 */
public class IndicatorOrganicFertilization extends AbstractIndicatorFertilization {

    private static final Double DEFAULT_COEF_INPUT_USAGE_UNIT = 1.0;
    private static final Double COEF_TO_KG_PER_KG_FOR_CONTENT = 1e-3;

    private static final List<String> FIELDS = List.of(
            "ferti_n_organique",
            "ferti_p2o5_organique",
            "ferti_k2o_organique",
            "ferti_mgo_organique",
            "ferti_s_organique",
            "ferti_cao_organique"
    );

    private boolean[] substanceToDisplay = new boolean[] {
            true, // N
            true, // P2O5
            true, // K2O
            true, // MgO
            true, // S
            true  // CaO
    };

    public IndicatorOrganicFertilization() {
        // labels
        super(new String[] {
                "Indicator.label.organicFertilization.N",
                "Indicator.label.organicFertilization.P2O5",
                "Indicator.label.organicFertilization.K2O",
                "Indicator.label.organicFertilization.MgO",
                "Indicator.label.organicFertilization.S",
                "Indicator.label.organicFertilization.CaO"
        });
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();

        for (int i = 0; i < this.labels.length; i++) {
            String indicatorName = getIndicatorLabel(i);
            String columnName = FIELDS.get(i);
            indicatorNameToColumnName.put(indicatorName, columnName);
        }
        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext, PerformanceGlobalExecutionContext globalExecutionContext, PerformancePracticedDomainExecutionContext domainContext, PerformanceGrowingSystemExecutionContext growingSystemContext, PerformancePracticedSystemExecutionContext practicedSystemContext, PerformancePracticedCropExecutionContext cropContext, PerformancePracticedInterventionExecutionContext interventionContext, PracticedCropCyclePhase phase) {
        if (interventionContext.isFictive()) {
            return null;
        }

        final Optional<OrganicFertilizersSpreadingAction> optionalOrganicFertilizersSpreadingAction = interventionContext.getOptionalOrganicFertilizersSpreadingAction();
        final double psci = this.getToolPSCi(interventionContext.getIntervention());

        Optional<Double[]> optionalResult = this.computeAmounts(writerContext, optionalOrganicFertilizersSpreadingAction, psci, interventionContext);
        return optionalResult.orElse(null);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {
        final Optional<OrganicFertilizersSpreadingAction> optionalOrganicFertilizersSpreadingAction = interventionContext.getOptionalOrganicFertilizersSpreadingAction();
        final double psci = this.getToolPSCi(interventionContext.getIntervention());

        Optional<Double[]> optionalResult = this.computeAmounts(writerContext, optionalOrganicFertilizersSpreadingAction, psci, interventionContext);
        return optionalResult.orElse(null);
    }

    private Optional<Double[]> computeAmounts(WriterContext writerContext,
                                            Optional<OrganicFertilizersSpreadingAction> optionalOrganicFertilizersSpreadingAction,
                                            double psci,
                                            PerformanceInterventionContext interventionContext) {

        AtomicBoolean computeDoneFlag = new AtomicBoolean(false);

        Double[] amounts = newArray(this.labels.length, 0.0);
        Map<Pair<AbstractAction, AbstractInputUsage>, Double[]> fertilizationAmountsByInput = new HashMap<>();

        if (optionalOrganicFertilizersSpreadingAction.isPresent()) {
            OrganicFertilizersSpreadingAction action = optionalOrganicFertilizersSpreadingAction.get();

            for (OrganicProductInputUsage organicProductInputUsage : action.getOrganicProductInputUsages()) {
                DomainOrganicProductInput domainOrganicProductInput = organicProductInputUsage.getDomainOrganicProductInput();
                final Double coefToKgHa = COEF_CONVERT_ORGANIC_PRODUCT_USAGE_TO_KG_HA.getOrDefault(domainOrganicProductInput.getUsageUnit(), DEFAULT_COEF_INPUT_USAGE_UNIT);
                final Double qtAvg = organicProductInputUsage.getQtAvg();
                if (qtAvg != null) {
                    final double productAmount = qtAvg * coefToKgHa * psci;

                    // Les teneurs dans RefFertiOrga sont en t/ha ou m³/ha (cf FertiOrgaUnit), donc il faut diviser par 1000 pour avoir la teneur en kg/kg,
                    // puisqu'on rapporte la quantité de produit utilisé en kg.

                    final double nitrogenContent = domainOrganicProductInput.getN() * COEF_TO_KG_PER_KG_FOR_CONTENT;
                    final double pContent = domainOrganicProductInput.getP2O5() * COEF_TO_KG_PER_KG_FOR_CONTENT;
                    final double kContent = domainOrganicProductInput.getK2O() * COEF_TO_KG_PER_KG_FOR_CONTENT;
                    final double mgoContent = getDouble(domainOrganicProductInput.getMgO()) * COEF_TO_KG_PER_KG_FOR_CONTENT;
                    final double sContent = getDouble(domainOrganicProductInput.getS()) * COEF_TO_KG_PER_KG_FOR_CONTENT;
                    final double caoContent = getDouble(domainOrganicProductInput.getCaO()) * COEF_TO_KG_PER_KG_FOR_CONTENT;

                    final double nitrogenAmount = nitrogenContent * productAmount;
                    final double p2O5Amount = pContent * productAmount;
                    final double k20Amount = kContent * productAmount;
                    final double mgoAmount = mgoContent * productAmount;
                    final double sAmount = sContent * productAmount;
                    final double caoAmount = caoContent * productAmount;

                    interventionContext.getSQuantityByInputUsage().put(organicProductInputUsage.getTopiaId(), sAmount);

                    final Double[] amountsForInput = newArray(labels.length, 0.0d);
                    amountsForInput[0] = nitrogenAmount;
                    amountsForInput[1] = p2O5Amount;
                    amountsForInput[2] = k20Amount;
                    amountsForInput[3] = mgoAmount;
                    amountsForInput[4] = sAmount;
                    amountsForInput[5] = caoAmount;

                    fertilizationAmountsByInput.put(Pair.of(action, organicProductInputUsage), amountsForInput);

                    this.writeInputUsageForSubstance(writerContext, organicProductInputUsage, action, nitrogenAmount, 0);
                    this.writeInputUsageForSubstance(writerContext, organicProductInputUsage, action, p2O5Amount, 1);
                    this.writeInputUsageForSubstance(writerContext, organicProductInputUsage, action, k20Amount, 2);
                    this.writeInputUsageForSubstance(writerContext, organicProductInputUsage, action, mgoAmount, 3);
                    this.writeInputUsageForSubstance(writerContext, organicProductInputUsage, action, sAmount, 4);
                    this.writeInputUsageForSubstance(writerContext, organicProductInputUsage, action, caoAmount, 5);

                    amounts = sum(amounts, amountsForInput);

                    computeDoneFlag.set(true);
                }
            }
        }

        if (computeDoneFlag.get()) {
            interventionContext.setOrganicFertilizationAmountsByInput(fertilizationAmountsByInput);
            return Optional.of(amounts);
        }
        return Optional.empty();
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, "Indicator.category.organicFertilization");
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return displayed && isRelevant(atLevel) && substanceToDisplay[i];
    }

    public void init(IndicatorFilter filter, Collection<OrganicFertilization> organicFertilizations) {
        displayed = filter != null;
        this.substanceToDisplay = new boolean[] {
                false, // N
                false, // P2O5
                false, // K2O
                false, // MgO
                false, // S
                false  // CaO
        };

        for (OrganicFertilization organicFertilization : organicFertilizations) {
            switch (organicFertilization) {
                case N -> substanceToDisplay[0] = true;
                case P2O5 -> substanceToDisplay[1] = true;
                case K2O -> substanceToDisplay[2] = true;
                case MGO -> substanceToDisplay[3] = true;
                case S -> substanceToDisplay[4] = true;
                case CAO -> substanceToDisplay[5] = true;
                default -> {}
            }
        }
    }
}
