package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigationImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

import java.util.List;

/**
 * Automoteur import model.
 * 
 * Columns:
 * <ul>
 * <li>Type materiel 1
 * <li>type materiel 2
 * <li>Type materiel 3
 * <li>Type materiel 4
 * <li>idtypemateriel
 * <li>codetype
 * <li>idsoustypemateriel
 * <li>commentaire sur materiel
 * <li>Millésime
 * <li>Coderef
 * <li>prix neuf € unité
 * <li>prix moyen achat
 * <li>unité
 * <li>unité / an
 * <li>charges fixes annuelle unité
 * <li>charges fixes €/an
 * <li>charges fixes €/unité de volume de travail annuel
 * <li>charges fixes €/unité de volume de travail annuel
 * <li>Réparations unité
 * <li>Réparations €/unité de travail annuel
 * <li>Energie unité
 * <li>Coût Energie / unité de travail
 * <li>coût total unité
 * <li>coût total  € / unité de travail annuel
 * <li>codeEDI
 * <li>source
 * </ul>
 * 
 * @author Eric Chatellier
 */
public class RefMaterielIrrigationModel extends AbstractAgrosystModel<RefMaterielIrrigation> implements ExportModel<RefMaterielIrrigation> {

    public RefMaterielIrrigationModel() {
        super(CSV_SEPARATOR);
    }

    @Override
    public void pushCsvHeaderNames(List<String> headerNames) {
        super.pushCsvHeaderNames(headerNames);

        newMandatoryColumn("Type materiel 1", RefMaterielIrrigation.PROPERTY_TYPE_MATERIEL1, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("type materiel 2", RefMaterielIrrigation.PROPERTY_TYPE_MATERIEL2, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("Type materiel 3", RefMaterielIrrigation.PROPERTY_TYPE_MATERIEL3, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("Type materiel 4", RefMaterielIrrigation.PROPERTY_TYPE_MATERIEL4, ZERO_TO_EMPTY_PARSER);
        newMandatoryColumn("idtypemateriel", RefMaterielIrrigation.PROPERTY_IDTYPEMATERIEL);
        newMandatoryColumn("codetype", RefMaterielIrrigation.PROPERTY_CODETYPE);
        newMandatoryColumn("idsoustypemateriel", RefMaterielIrrigation.PROPERTY_IDSOUSTYPEMATERIEL);
        newMandatoryColumn("commentaire sur materiel", RefMaterielIrrigation.PROPERTY_COMMENTAIRE_SUR_MATERIEL);
        newMandatoryColumn("Millésime", RefMaterielIrrigation.PROPERTY_MILLESIME, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("Coderef", RefMaterielIrrigation.PROPERTY_CODE_REF);
        newMandatoryColumn("prix neuf € unité", RefMaterielIrrigation.PROPERTY_PRIX_NEUF_UNITE);
        newMandatoryColumn("prix moyen achat", RefMaterielIrrigation.PROPERTY_PRIX_MOYEN_ACHAT, DOUBLE_PARSER);
        newMandatoryColumn("unité", RefMaterielIrrigation.PROPERTY_UNITE);
        newMandatoryColumn("unité / an", RefMaterielIrrigation.PROPERTY_UNITE_PAR_AN, DOUBLE_PARSER);
        newMandatoryColumn("charges fixes annuelle unité", RefMaterielIrrigation.PROPERTY_CHARGES_FIXES_ANNUELLE_UNITE);
        newMandatoryColumn("charges fixes €/an", RefMaterielIrrigation.PROPERTY_CHARGES_FIXES_PAR_AN, DOUBLE_PARSER);
        newMandatoryColumn("charges fixes €/unité de volume de travail annuel unité", RefMaterielIrrigation.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL_UNITE);
        newMandatoryColumn("charges fixes €/unité de volume de travail annuel", RefMaterielIrrigation.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL, DOUBLE_PARSER);
        newMandatoryColumn("Réparations unité", RefMaterielIrrigation.PROPERTY_REPARATIONS_UNITE);
        newMandatoryColumn("Réparations €/unité de travail annuel", RefMaterielIrrigation.PROPERTY_REPARATIONS_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_PARSER);
        newMandatoryColumn("Energie unité", RefMaterielIrrigation.PROPERTY_ENERGIE_UNITE);
        newMandatoryColumn("Coût Energie / unité de travail", RefMaterielIrrigation.PROPERTY_COUT_ENERGIE_PAR_UNITE_DE_TRAVAIL, DOUBLE_PARSER);
        newMandatoryColumn("coût total unité", RefMaterielIrrigation.PROPERTY_COUT_TOTAL_UNITE);
        newMandatoryColumn("coût total  € / unité de travail annuel", RefMaterielIrrigation.PROPERTY_COUT_TOTAL_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_PARSER);
        newMandatoryColumn("code materiel GES'TIM", RefMaterielIrrigation.PROPERTY_CODE_MATERIEL__GESTIM);
        newMandatoryColumn("masse (kg)", RefMaterielIrrigation.PROPERTY_MASSE, DOUBLE_PARSER);
        newMandatoryColumn("durée de vie théorique -obsolescence constructeur- (années)", RefMaterielIrrigation.PROPERTY_DUREE_VIE_THEORIQUE, INT_PARSER);
        newMandatoryColumn("Code EDI", RefMaterielIrrigation.PROPERTY_CODE_EDI);
        newMandatoryColumn("source", RefMaterielIrrigation.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefMaterielIrrigation.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefMaterielIrrigation, Object>> getColumnsForExport() {
        ModelBuilder<RefMaterielIrrigation> modelBuilder = new ModelBuilder<>();
        
        modelBuilder.newColumnForExport("Type materiel 1", RefMaterielIrrigation.PROPERTY_TYPE_MATERIEL1);
        modelBuilder.newColumnForExport("type materiel 2", RefMaterielIrrigation.PROPERTY_TYPE_MATERIEL2);
        modelBuilder.newColumnForExport("Type materiel 3", RefMaterielIrrigation.PROPERTY_TYPE_MATERIEL3);
        modelBuilder.newColumnForExport("Type materiel 4", RefMaterielIrrigation.PROPERTY_TYPE_MATERIEL4);
        modelBuilder.newColumnForExport("idtypemateriel", RefMaterielIrrigation.PROPERTY_IDTYPEMATERIEL);
        modelBuilder.newColumnForExport("codetype", RefMaterielIrrigation.PROPERTY_CODETYPE);
        modelBuilder.newColumnForExport("idsoustypemateriel", RefMaterielIrrigation.PROPERTY_IDSOUSTYPEMATERIEL);
        modelBuilder.newColumnForExport("commentaire sur materiel", RefMaterielIrrigation.PROPERTY_COMMENTAIRE_SUR_MATERIEL);
        modelBuilder.newColumnForExport("Millésime", RefMaterielIrrigation.PROPERTY_MILLESIME, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Coderef", RefMaterielIrrigation.PROPERTY_CODE_REF);
        modelBuilder.newColumnForExport("prix neuf € unité", RefMaterielIrrigation.PROPERTY_PRIX_NEUF_UNITE);
        modelBuilder.newColumnForExport("prix moyen achat", RefMaterielIrrigation.PROPERTY_PRIX_MOYEN_ACHAT, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("unité", RefMaterielIrrigation.PROPERTY_UNITE);
        modelBuilder.newColumnForExport("unité / an", RefMaterielIrrigation.PROPERTY_UNITE_PAR_AN, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("charges fixes annuelle unité", RefMaterielIrrigation.PROPERTY_CHARGES_FIXES_ANNUELLE_UNITE);
        modelBuilder.newColumnForExport("charges fixes €/an", RefMaterielIrrigation.PROPERTY_CHARGES_FIXES_PAR_AN, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("charges fixes €/unité de volume de travail annuel unité", RefMaterielIrrigation.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL_UNITE);
        modelBuilder.newColumnForExport("charges fixes €/unité de volume de travail annuel", RefMaterielIrrigation.PROPERTY_CHARGES_FIXES_PAR_UNITE_DE_VOLUME_DE_TRAVAIL_ANNUEL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Réparations unité", RefMaterielIrrigation.PROPERTY_REPARATIONS_UNITE);
        modelBuilder.newColumnForExport("Réparations €/unité de travail annuel", RefMaterielIrrigation.PROPERTY_REPARATIONS_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Energie unité", RefMaterielIrrigation.PROPERTY_ENERGIE_UNITE);
        modelBuilder.newColumnForExport("Coût Energie / unité de travail", RefMaterielIrrigation.PROPERTY_COUT_ENERGIE_PAR_UNITE_DE_TRAVAIL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("coût total unité", RefMaterielIrrigation.PROPERTY_COUT_TOTAL_UNITE);
        modelBuilder.newColumnForExport("coût total  € / unité de travail annuel", RefMaterielIrrigation.PROPERTY_COUT_TOTAL_PAR_UNITE_DE_TRAVAIL_ANNUEL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("code materiel GES'TIM", RefMaterielIrrigation.PROPERTY_CODE_MATERIEL__GESTIM);
        modelBuilder.newColumnForExport("masse (kg)", RefMaterielIrrigation.PROPERTY_MASSE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("durée de vie théorique -obsolescence constructeur- (années)", RefMaterielIrrigation.PROPERTY_DUREE_VIE_THEORIQUE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Code EDI", RefMaterielIrrigation.PROPERTY_CODE_EDI);
        modelBuilder.newColumnForExport("source", RefMaterielIrrigation.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefMaterielIrrigation.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefMaterielIrrigation newEmptyInstance() {
        RefMaterielIrrigation refMaterielIrrigation = new RefMaterielIrrigationImpl();
        refMaterielIrrigation.setActive(true);
        return refMaterielIrrigation;
    }

}
