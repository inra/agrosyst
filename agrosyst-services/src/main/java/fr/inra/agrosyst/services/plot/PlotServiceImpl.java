package fr.inra.agrosyst.services.plot;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.Ground;
import fr.inra.agrosyst.api.entities.GroundTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingPlanTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PlotTopiaDao;
import fr.inra.agrosyst.api.entities.SolHorizon;
import fr.inra.agrosyst.api.entities.SolHorizonTopiaDao;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.ZoneType;
import fr.inra.agrosyst.api.entities.measure.MeasurementSession;
import fr.inra.agrosyst.api.entities.measure.MeasurementSessionTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinageTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigoTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppaTopiaDao;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.common.UsageList;
import fr.inra.agrosyst.api.services.domain.ExtendContext;
import fr.inra.agrosyst.api.services.domain.PlotDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.api.services.plot.SolHorizonDto;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.EntityUsageService;
import fr.inra.agrosyst.services.common.export.EntityExporter;
import fr.inra.agrosyst.services.common.export.ExportModelAndRows;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.AdjacentBean;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.AdjacentModel;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.CommonBean;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.EquipmentBean;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.EquipmentModel;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.GroundBean;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.GroundHorizonBean;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.GroundHorizonModel;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.GroundModel;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.MainBean;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.MainModel;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.ZoneBean;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.ZoneModel;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.ZoningBean;
import fr.inra.agrosyst.services.plot.export.PlotExportModels.ZoningModel;
import fr.inra.agrosyst.services.plot.export.PlotExportTask;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author cosse
 */
@Setter
public class PlotServiceImpl extends AbstractAgrosystService implements PlotService {

    /** Nom de la zone principale créée automatiquement. */
    public static final String PLOT_DEFAULT_ZONE_NAME = "Zone principale";

    protected AnonymizeService anonymizeService;
    protected BusinessAuthorizationService authorizationService;
    protected EntityUsageService entityUsageService;
    protected EffectiveCropCycleService effectiveCropCycleService;

    protected PlotTopiaDao plotDao;
    protected RefLocationTopiaDao locationDao;
    protected DomainTopiaDao domainDao;
    protected RefParcelleZonageEDITopiaDao parcelleZonageEDIDao;
    protected GrowingSystemTopiaDao growingSystemDao;
    protected RefSolTextureGeppaTopiaDao refSolTextureGeppaDao;
    protected GroundTopiaDao solDao;
    protected RefSolProfondeurIndigoTopiaDao refSolProfondeurIndigoDao;
    protected SolHorizonTopiaDao solHorizonDao;
    protected ZoneTopiaDao zoneDao;
    protected GrowingPlanTopiaDao growingPlanDao;
    protected RefElementVoisinageTopiaDao refElementVoisinageDao;
    protected MeasurementSessionTopiaDao measurementSessionDao;

    private AgrosystI18nService i18nService;

    @Override
    public Plot getPlot(String plotTopiaId) {
        Plot result;
        if (StringUtils.isBlank(plotTopiaId)) {
            result = plotDao.newInstance();
            result.setActive(true);
            result.setCode(UUID.randomUUID().toString());
        } else {
            result = plotDao.forTopiaIdEquals(plotTopiaId).findUnique();
            result = anonymizeService.checkForPlotAnonymization(result);
        }
        return result;
    }

    @Override
    public Plot createOrUpdatePlot(Plot plot, String domainId, String locationId, String growingSystemId,
                                   Collection<String> selectedPlotZoningIds, String selectedSolId,
                                   String selectedSurfaceTextureId, String selectedSubSoilTextureId,
                                   String selectedSolDepthId, List<SolHorizonDto> solHorizons, Collection<Zone> zones,
                                   List<String> adjacentElementIds) {

        authorizationService.checkCreateOrUpdatePlot(plot.getTopiaId());

        Plot result = doCreateOrUpdatePlot(plot, domainId, locationId, growingSystemId,
                selectedPlotZoningIds, selectedSolId,
                selectedSurfaceTextureId, selectedSubSoilTextureId,
                selectedSolDepthId, solHorizons, zones,
                adjacentElementIds);
        getTransaction().commit();
        return result;
    }

    protected Plot createOrUpdatePlotWithoutCommit(Plot plot, String domainId, String locationId, String growingSystemId,
                                                   Collection<String> selectedPlotZoningIds, String selectedSolId,
                                                   String selectedSurfaceTextureId, String selectedSubSoilTextureId,
                                                   String selectedSolDepthId, List<SolHorizonDto> solHorizons, Collection<Zone> zones,
                                                   List<String> adjacentElementIds) {
        return doCreateOrUpdatePlot(plot, domainId, locationId, growingSystemId,
                selectedPlotZoningIds, selectedSolId,
                selectedSurfaceTextureId, selectedSubSoilTextureId,
                selectedSolDepthId, solHorizons, zones,
                adjacentElementIds);
    }

    protected void checkActivated(Plot plot) {
        if(plot.isPersisted()) {
            Preconditions.checkArgument(plot.isActive() && plot.getDomain().isActive(),
                    String.format("The plot with id '%s' and/or the domain related to it are unactivated", plot.getTopiaId()));
        }
    }

    protected void validPlotPreconditions(Plot plot, String domainId, Collection<Zone> zones) {
        Preconditions.checkArgument(StringUtils.isNotBlank(plot.getName()), "Le nom de la parcelle et requis");
        Preconditions.checkArgument(StringUtils.isNotBlank(domainId), "Aucun domaine renseigné pour la parcelle");
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(zones), "La liste des zones ne peut être vide, une zone principale est requise !");
        checkActivated(plot);
    }

    protected Plot doCreateOrUpdatePlot(Plot plot, String domainId, String locationId, String growingSystemId,
                                        Collection<String> selectedPlotZoningIds, String selectedSolId,
                                        String selectedSurfaceTextureId, String selectedSubSoilTextureId,
                                        String selectedSolDepthId, List<SolHorizonDto> solHorizons, Collection<Zone> zones,
                                        List<String> adjacentElementIds) {

        // persist plot
        Plot result;

        validPlotPreconditions(plot, domainId, zones);

        addPlotDomain(plot, domainId);

        addPlotLocation(plot, locationId);

        addPlotGrowingSystem(plot, growingSystemId);

        addZoningsEntities(plot, selectedPlotZoningIds);

        addGround(plot, selectedSolId);

        RefSolTextureGeppa surfaceTexture = getRefSolTextureGeppa(selectedSurfaceTextureId);
        plot.setSurfaceTexture(surfaceTexture);

        RefSolTextureGeppa subSoilTexture = getRefSolTextureGeppa(selectedSubSoilTextureId);
        plot.setSubSoilTexture(subSoilTexture);

        addPlotSolProfondeurIndigo(plot, selectedSolDepthId);

        persistPlotSolHorizons(plot, solHorizons);

        addPlotAdjacentElements(plot, adjacentElementIds);

        if (plot.isPersisted()) {
            result = plotDao.update(plot);
        } else {
            // create a random plot code, used to link plots each other
            setPlotCode(plot);
            result = plotDao.create(plot);
        }

        // persist zones
        persistPlotZones(plot, zones, result);

        return result;
    }

    private void setPlotCode(Plot plot) {
        if (StringUtils.isBlank(plot.getCode())) {
            plot.setCode(UUID.randomUUID().toString());
        }
    }

    protected void addPlotDomain(Plot plot, String domainId) {
        Domain domain = plot.getDomain();
        if (domain != null && !domain.getTopiaId().equals(domainId)) {
            domain = domainDao.forTopiaIdEquals(domainId).findUnique();
        } else if (domain == null) {
            domain = domainDao.forTopiaIdEquals(domainId).findUnique();
        }
        plot.setDomain(domain);
    }

    protected void addPlotLocation(Plot plot, String locationId) {
        RefLocation location = null;
        if (!StringUtils.isEmpty(locationId)) {
            location = locationDao.forTopiaIdEquals(locationId).findUnique();
        }
        plot.setLocation(location);
    }

    protected void addPlotGrowingSystem(Plot plot, String growingSystemId) {
        GrowingSystem previousGrowingSystem = plot.getGrowingSystem();

        String previousGrowingSystemId = null;

        if (previousGrowingSystem != null) {
            previousGrowingSystemId = previousGrowingSystem.getTopiaId();
        }
        // A growing system is removed
        if (StringUtils.isBlank(growingSystemId) && previousGrowingSystemId != null) {
            plot.setGrowingSystem(null);
        }
        // A new growing system is affected
        if (!StringUtils.isBlank(growingSystemId) && (previousGrowingSystemId == null || !previousGrowingSystemId.contentEquals(growingSystemId))){
            GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(growingSystemId).findUnique();
            plot.setGrowingSystem(growingSystem);
        }
    }

    protected void addZoningsEntities(Plot plot, Collection<String> selectedPlotZoningIds) {
        Collection<RefParcelleZonageEDI> plotZonings = plot.getPlotZonings();
        if (plotZonings == null) {
            plotZonings = new ArrayList<>();
            plot.setPlotZonings(plotZonings);
        }
        Collection<RefParcelleZonageEDI> nonDeletedPlotZonings = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(selectedPlotZoningIds)) {
            // si la parcelle est hors zonage, on vide la liste
            if (plot.isOutOfZoning()) {
                selectedPlotZoningIds.clear();
            }
            for (String selectedPlotZoningId : selectedPlotZoningIds) {
                RefParcelleZonageEDI parcelleZonageEDI = parcelleZonageEDIDao.forTopiaIdEquals(selectedPlotZoningId).findUnique();
                if (!plotZonings.contains(parcelleZonageEDI)) {
                    plotZonings.add(parcelleZonageEDI);
                }
                nonDeletedPlotZonings.add(parcelleZonageEDI);
            }
        }
        plotZonings.retainAll(nonDeletedPlotZonings);
    }

    protected void addGround(Plot plot, String selectedSolId) {
        Ground sol;
        if (StringUtils.isNotBlank(selectedSolId)) {
            sol = solDao.forTopiaIdEquals(selectedSolId).findUnique();
        } else {
            sol = null;
        }
        plot.setGround(sol);
    }

    protected RefSolTextureGeppa getRefSolTextureGeppa(String selectedSubSoilTextureId) {
        RefSolTextureGeppa subSoilTexture;
        if (StringUtils.isNotBlank(selectedSubSoilTextureId)) {
            subSoilTexture = refSolTextureGeppaDao.forTopiaIdEquals(selectedSubSoilTextureId).findUnique();
        } else {
            subSoilTexture = null;
        }
        return subSoilTexture;
    }

    protected void addPlotSolProfondeurIndigo(Plot plot, String selectedSolDepthId) {
        RefSolProfondeurIndigo solProfondeur;
        if (StringUtils.isNotBlank(selectedSolDepthId)) {
            solProfondeur = refSolProfondeurIndigoDao.forTopiaIdEquals(selectedSolDepthId).findUnique();
        } else {
            solProfondeur = null;
        }

        plot.setSolDepth(solProfondeur);
    }

    protected void persistPlotSolHorizons(Plot plot, List<SolHorizonDto> solHorizons) {
        if (solHorizons != null) {
            Collection<SolHorizon> currentHorizons = plot.getSolHorizon();
            if (currentHorizons == null) {
                currentHorizons = new ArrayList<>();
                plot.setSolHorizon(currentHorizons);
            }
            Map<String, SolHorizon> currentHorizonMap = Maps.uniqueIndex(currentHorizons, Entities.GET_TOPIA_ID::apply);
            Set<SolHorizon> nonDeletedHorizon = Sets.newHashSet();
            for (SolHorizonDto solHorizonDto : solHorizons) {
                String topiaId = solHorizonDto.getTopiaId();
                SolHorizon solHorizon;
                if (StringUtils.isNotBlank(topiaId)) {
                    solHorizon = currentHorizonMap.get(topiaId);
                } else {
                    solHorizon = solHorizonDao.newInstance();
                }

                solHorizon.setComment(solHorizonDto.getComment());
                solHorizon.setLowRating(solHorizonDto.getLowRating());
                solHorizon.setStoniness(solHorizonDto.getStoniness());
                String solTextureId = solHorizonDto.getSolTextureId();
                RefSolTextureGeppa horizonSolTexture = null;
                if (StringUtils.isNotBlank(solTextureId)) {
                    horizonSolTexture = refSolTextureGeppaDao.forTopiaIdEquals(solTextureId).findUnique();
                }
                solHorizon.setSolTexture(horizonSolTexture);

                nonDeletedHorizon.add(solHorizon);
                currentHorizons.add(solHorizon);
            }
            currentHorizons.retainAll(nonDeletedHorizon);
        }
    }

    protected void addPlotAdjacentElements(Plot plot, List<String> adjacentElementIds) {
        Collection<RefElementVoisinage> currentAdjacentElements = plot.getAdjacentElements();
        if (currentAdjacentElements == null) {
            currentAdjacentElements = new ArrayList<>();
            plot.setAdjacentElements(currentAdjacentElements);
        }
        Collection<RefElementVoisinage> nonDeletedAdjacentElements = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(adjacentElementIds)) {
            for (String adjacentElementId : adjacentElementIds) {
                RefElementVoisinage refElementVoisinage = refElementVoisinageDao.forTopiaIdEquals(adjacentElementId).findUnique();
                if (!currentAdjacentElements.contains(refElementVoisinage)) {
                    currentAdjacentElements.add(refElementVoisinage);
                }
                nonDeletedAdjacentElements.add(refElementVoisinage);
            }
        }
        currentAdjacentElements.retainAll(nonDeletedAdjacentElements);
    }

    protected void persistPlotZones(Plot plot, Collection<Zone> zones, Plot result) {
        List<Zone> currentZones = zoneDao.forPlotEquals(result).findAll();
        Map<String, Zone> currentZonesMap = Maps.newHashMap(Maps.uniqueIndex(currentZones, Entities.GET_TOPIA_ID::apply));
        for (Zone zoneDto : zones) {
            String topiaId = zoneDto.getTopiaId();
            Zone zone;
            if (StringUtils.isBlank(topiaId)) {
                zone = zoneDao.newInstance();
                zone.setPlot(plot);
                if (StringUtils.isBlank(zoneDto.getCode())) {
                    zone.setCode(UUID.randomUUID().toString());
                } else {
                    zone.setCode(zoneDto.getCode());
                }
            } else {
                zone = currentZonesMap.remove(topiaId);
            }

            zone.setName(zoneDto.getName());
            zone.setType(zoneDto.getType());
            zone.setArea(zoneDto.getArea());
            zone.setLatitude(zoneDto.getLatitude());
            zone.setLongitude(zoneDto.getLongitude());
            zone.setComment(zoneDto.getComment());
            zone.setActive(zoneDto.isActive());

            if (StringUtils.isBlank(topiaId)) {
                zoneDao.create(zone);
            } else {
                zoneDao.update(zone);
            }
        }

        // on peut supprimer des zones qui ne sont pas utilisées ici
        // si cela plante à cause d'une dépendance transient, ca veut dire que la suppression
        // de zone n'est pas autorisée
        zoneDao.deleteAll(currentZonesMap.values());

        // on sauvegarde l'information de zones supprimées pour pouvoir le signaler
        // à l'utilisateur
        if (!currentZonesMap.isEmpty()) {
            plot.setDeletedZones(true);
        }
    }

    @Override
    public void updatePlotsGrowingSystemRelationship(GrowingSystem growingSystem, Collection<String> plotIds) {

        String growingSystemTopiaId = growingSystem.getTopiaId();

        List<Plot> plots;

        if (growingSystemTopiaId == null) {
            plots = new ArrayList<>();
        } else {
            plots = plotDao.forGrowingSystemEquals(growingSystem).findAll();
        }

        Map<String, Plot> indexedPlots = Maps.uniqueIndex(plots, Entities.GET_TOPIA_ID::apply);

        Set<String> selectedPlotIds;
        if (plotIds != null) {
            selectedPlotIds = Sets.newHashSet(plotIds);
        } else {
            selectedPlotIds = Sets.newHashSet();
        }

        Set<String> originePlotIds = indexedPlots.keySet();
        Set<String> plotRemovedRelationships = Sets.difference(originePlotIds, selectedPlotIds);

        // remove the relationship
        if (StringUtils.isNotBlank(growingSystemTopiaId)) {
            for (String plotRemovedRelationship : plotRemovedRelationships) {
                Plot plot = plotDao.forTopiaIdEquals(plotRemovedRelationship).findUnique();
                if (plot.getGrowingSystem()!=null && plot.getGrowingSystem().getTopiaId().contentEquals(growingSystemTopiaId)) {
                    plot.setGrowingSystem(null);
                }
            }
        }


        if (plotIds != null) {
            for (String plotId : plotIds) {
                Plot plot = indexedPlots.get(plotId);
                // Case where the plot was not affected to this given GrowingSystem.
                if (plot == null) {
                    plot = plotDao.forTopiaIdEquals(plotId).findUnique();
                    // If the plot is already affected to an other GrowingSystem there is no right to change it.
                    if (plot.getGrowingSystem() == null) {
                        plot.setGrowingSystem(growingSystem);
                        plot.setActive(true);
                    }
                }
            }
        }
    }

    @Override
    public List<Plot> findAllByGrowingSystem(GrowingSystem growingSystem) {
        return plotDao.forGrowingSystemEquals(growingSystem).findAll();
    }

    @Override
    public List<PlotDto> findAllFreeAndGrowingSystemPlots(GrowingSystem growingSystem, String growingPlanTopiaId) {

        // get growing plan instance
        GrowingPlan growingPlan = growingPlanDao.forTopiaIdEquals(growingPlanTopiaId).findUnique();

        // get free plots
        List<Plot> plots1 = plotDao.findAllFreePlotInDomain(growingPlan.getDomain().getTopiaId());
        List<Plot> result = Lists.newArrayList(plots1);

        // get all plots associated to current growing system
        if (growingSystem != null) {
            List<Plot> plots2 = plotDao.forGrowingSystemEquals(growingSystem).findAll();
            result.addAll(plots2);
        }
    
        return anonymizeService.checkForPlotsAnonymization(result).stream()
            .map(plot -> {
                PlotDto plotDto = anonymizeService.getPlotToDtoFunction().apply(plot);
                plotDto.setPacIlotNumber(plot.getPacIlotNumber());
                plotDto.setActive(plot.isActive());
                List<Zone> plotZones = getPlotZones(plot);
                plotDto.setCroppingPlanInfo(effectiveCropCycleService.getCroppingPlanInfoForZone(plotZones));
                return plotDto;
            })
            .collect(Collectors.toList());
    }

    @Override
    public List<Plot> getFreePlotForGrowingPlan(String growingPlanTopiaId) {

        // get growing plan instance
        GrowingPlan growingPlan = growingPlanDao.forTopiaIdEquals(growingPlanTopiaId).findUnique();

        // get free plots
        return plotDao.findAllFreePlotInDomain(growingPlan.getDomain().getTopiaId());
    }

    @Override
    public LinkedHashMap<Integer, String> getRelatedPlots(String plotCode) {
        return plotDao.findAllRelatedPlotIdsByCampaigns(plotCode);
    }

    @Override
    public LinkedHashMap<Integer, String> getRelatedZones(String zoneCode) {
        return zoneDao.findAllRelatedZones(zoneCode);
    }

    @Override
    public boolean validMergingPlots(List<String> plotTopiaIds){
        // valid if plots have same growing system can be any.
        Long nbGS = plotDao.countAllGrowingSystemsForPlots(Sets.newConcurrentHashSet(plotTopiaIds));
        return nbGS == 0 || nbGS == 1;
    }

    @Override
    public Plot mergePlots(List<String> plotTopiaIds) {
        Preconditions.checkArgument(plotTopiaIds.size() >= 2);
        Preconditions.checkArgument(validMergingPlots(plotTopiaIds));

        // find all instance for ids
        List<Plot> plots = plotDao.forTopiaIdIn(plotTopiaIds).findAll();

        // find plot with max surface
        Plot remainPlot = plots.getFirst();
        for (int i = 1; i < plots.size(); i++) {
            Plot loopPlot = plots.get(i);
            if (loopPlot.getArea() > remainPlot.getArea()) {
                remainPlot = loopPlot;
            }
        }

        // merge plot
        for (Plot plot : plots) {
            if (!plot.equals(remainPlot)) {
                // move zones
                List<Zone> zones = zoneDao.forPlotEquals(plot).findAll();
                for (Zone zone : zones) {
                    // ajout du nom de la parcelle d'origine pour différencier les
                    // différente zone principale résultantes
                    String name = zone.getName();
                    name += " (" + plot.getName() + ")";
                    zone.setName(name);
                    // move zone
                    zone.setPlot(remainPlot);
                }
                // add area
                remainPlot.setArea(remainPlot.getArea() + plot.getArea());
                // copy previous lineage
                if (plot.getLineage() != null) {
                    remainPlot.addAllLineage(plot.getLineage());
                }
                // add lineage for delete
                remainPlot.addLineage(plot.getCode());
                // delete moved plot
                plotDao.delete(plot);
            }
        }

        getTransaction().commit();
        return remainPlot;
    }

    @Override
    public Plot duplicatePlot(String topiaId) {
        Plot plot = plotDao.forTopiaIdEquals(topiaId).findUnique();
        Plot result = extendPlot(new ExtendContext(true), plot.getDomain(), plot.getGrowingSystem(), plot);
        getTransaction().commit();
        return result;
    }

    @Override
    public Plot extendPlot(ExtendContext extendContext, Domain clonedDomain, GrowingSystem clonedGrowingSystem, Plot plot) {

        // perform clone
        Binder<Plot, Plot> binder = BinderFactory.newBinder(Plot.class);
        Plot clonedPlot = plotDao.newInstance();
        binder.copyExcluding(plot, clonedPlot,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                Plot.PROPERTY_DOMAIN,
                Plot.PROPERTY_GROWING_SYSTEM,
                Plot.PROPERTY_SOL_HORIZON,
                Plot.PROPERTY_PLOT_ZONINGS,
                Plot.PROPERTY_ADJACENT_ELEMENTS,
                Plot.PROPERTY_LINEAGE,
                Plot.PROPERTY_E_DAPLOS_ISSUER_ID,
                Plot.PROPERTY_GROUND);
        clonedPlot.setDomain(clonedDomain);
        clonedPlot.setGrowingSystem(clonedGrowingSystem);// may be null

        // always reset deleted zone
        clonedPlot.setDeletedZones(false);

        // break code is case of duplication
        if (extendContext.isDuplicateOnly()) {
            clonedPlot.setCode(UUID.randomUUID().toString());
            clonedPlot.addLineage(plot.getCode());
            clonedPlot.setGround(plot.getGround());
        } else {
            Ground ground = extendContext.getGroundCache().get(plot.getGround());
            clonedPlot.setGround(ground);
        }

        // clone sol horizon
        if (plot.getSolHorizon() != null) {
            Binder<SolHorizon, SolHorizon> binderSH = BinderFactory.newBinder(SolHorizon.class);
            for (SolHorizon solHorizon : plot.getSolHorizon()) {
                SolHorizon solHorizonClone = solHorizonDao.newInstance();
                binderSH.copyExcluding(solHorizon, solHorizonClone,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION);
                clonedPlot.addSolHorizon(solHorizonClone);
            }
        }

        // force collection copy instead of collection reference copy
        if (plot.getPlotZonings() != null) {
            clonedPlot.setPlotZonings(Lists.newArrayList(plot.getPlotZonings()));
        }
        if (plot.getAdjacentElements() != null) {
            clonedPlot.setAdjacentElements(Lists.newArrayList(plot.getAdjacentElements()));
        }

        // persist clone
        clonedPlot = plotDao.create(clonedPlot);

        // clone plot zones
        List<Zone> zones = zoneDao.forPlotEquals(plot).addEquals(Zone.PROPERTY_ACTIVE, true).findAll();
        Binder<Zone, Zone> zoneBinder = BinderFactory.newBinder(Zone.class);
        for (Zone zone : zones) {
            Zone zoneClone = zoneDao.newInstance();
            zoneBinder.copyExcluding(zone, zoneClone,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                    Zone.PROPERTY_PLOT,
                    Zone.PROPERTY_LINEAGE);
            zoneClone.setPlot(clonedPlot);

            // break code is cas of duplication
            if (extendContext.isDuplicateOnly()) {
                zoneClone.setCode(UUID.randomUUID().toString());
                zoneClone.addLineage(zone.getCode());
            }

            zoneDao.create(zoneClone);
        }

        return clonedPlot;
    }

    @Override
    public List<Plot> findAllForDomain(Domain domain) {
        List<Plot> plots = plotDao.findAllOrderByNameForDomain(domain);
        plots = anonymizeService.checkForPlotsAnonymization(plots);

        // Traduction des noms des sols
        final Language language = getSecurityContext().getLanguage();
        final List<RefSolArvalis> sols = plots.stream().filter(p -> p.getGround() != null).map(p -> p.getGround().getRefSolArvalis()).toList();
        i18nService.fillRefSolArvalisTranslations(language, sols);

        return plots;
    }

    @Override
    public List<Zone> getPlotZones(Plot plot) {
        List<Zone> result;
        if (plot.isPersisted()) {
            result = zoneDao.forPlotEquals(plot).findAll();
        } else {
            result = new ArrayList<>();

            // add default zone
            Zone defaultZone = zoneDao.newInstance();
            defaultZone.setName(PLOT_DEFAULT_ZONE_NAME);
            defaultZone.setType(ZoneType.PRINCIPALE);
            defaultZone.setActive(true);
            defaultZone.setCode(UUID.randomUUID().toString());
            result.add(defaultZone);
        }
        return result;
    }

    @Override
    public List<MeasurementSession> getPlotMeasurementSessions(String plotTopiaId) {
        return measurementSessionDao.forProperties(MeasurementSession.PROPERTY_ZONE + "." +
                Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_TOPIA_ID, plotTopiaId)
                .setOrderByArguments(MeasurementSession.PROPERTY_ZONE, MeasurementSession.PROPERTY_START_DATE, MeasurementSession.PROPERTY_END_DATE).findAll();
    }

    @Override
    public UsageList<Zone> getZonesAndUsages(String plotTopiaId) {
        List<Zone> zones = zoneDao.forProperties(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_TOPIA_ID, plotTopiaId).findAll();
        return entityUsageService.getZonesUsageList(zones);
    }

    @Override
    public void updateActivatePlotStatus(List<String> plotTopiaIds, boolean isToActivated) {
        if (CollectionUtils.isNotEmpty(plotTopiaIds)) {
            plotDao.updateStatus(plotTopiaIds, isToActivated);
            getTransaction().commit();
        }
    }

    @Override
    public List<Zone> getZonesWithoutCycle(String zoneId) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(zoneId));

        Zone zone = zoneDao.forTopiaIdEquals(zoneId).findUnique();

        return zoneDao.findZonesWithoutCycle(zoneId, zone.getPlot().getDomain().getTopiaId());
    }

    @Override
    public List<PlotDto> getPlots(Collection<String> plotIds) {
        List<Plot> plots = plotDao.forTopiaIdIn(plotIds).findAll();
        return plots.stream().map(anonymizeService.getPlotToDtoFunction()).collect(Collectors.toList());
    }

    @Override
    public ExportResult exportPlotsAsXls(Collection<String> plotIds) {

        // get all possible bean infos
        List<MainBean> mainBeans = new LinkedList<>();
        List<ZoningBean> zoningBeans = new LinkedList<>();
        List<EquipmentBean> equipmentBeans = new LinkedList<>();
        List<GroundBean> groundBeans = new LinkedList<>();
        List<AdjacentBean> adjacentBeans = new LinkedList<>();
        List<GroundHorizonBean> groundHorizonBeans = new LinkedList<>();
        List<ZoneBean> zoneBeans = new LinkedList<>();

        try {
            if (CollectionUtils.isNotEmpty(plotIds)) {
                Iterable<List<String>> plotIdsChunks = Iterables.partition(plotIds, 10);
                for (List<String> plotIdsChunk : plotIdsChunks) {

                    plotDao.clear();

                    Iterable<Plot> plots = plotDao.forTopiaIdIn(plotIdsChunk).findAll();
                    for (Plot plot : plots) {

                        // anonymize plot
                        plot = anonymizeService.checkForPlotAnonymization(plot);

                        // Common data for all tabs
                        CommonBean baseBean = new CommonBean(
                                plot.getName(),
                                plot.getDomain().getName(),
                                plot.getDomain().getCampaign()
                        );

                        // Généralités
                        MainBean mainBean = toMainBean(plot, baseBean);
                        mainBeans.add(mainBean);

                        // Zonage
                        if (plot.isOutOfZoning()) {
                            ZoningBean export = new ZoningBean(baseBean);
                            export.setOutOfZoning(true);
                            zoningBeans.add(export);
                        } else {
                            Collection<RefParcelleZonageEDI> zonings = CollectionUtils.emptyIfNull(plot.getPlotZonings());
                            for (RefParcelleZonageEDI zoning : zonings) {
                                ZoningBean export = new ZoningBean(baseBean);
                                export.setOutOfZoning(false);
                                export.setZonage(zoning.getLibelle_engagement_parcelle());
                                zoningBeans.add(export);
                            }
                        }

                        // Équipements
                        EquipmentBean equipmentBean = toEquipmentBean(plot, baseBean);
                        equipmentBeans.add(equipmentBean);

                        // Sols
                        GroundBean groundBean = toGroundBean(plot, baseBean);
                        groundBeans.add(groundBean);

                        // Voisinnage
                        CollectionUtils.emptyIfNull(plot.getAdjacentElements())
                                .stream()
                                .map(adjacentElement -> toAdjacentBean(baseBean, adjacentElement))
                                .forEach(adjacentBeans::add);

                        // horizons
                        CollectionUtils.emptyIfNull(plot.getSolHorizon())
                                .stream()
                                .map(solHorizon -> toGroundHorizonBean(baseBean, solHorizon))
                                .forEach(groundHorizonBeans::add);

                        // zones
                        List<Zone> zones = zoneDao.forPlotEquals(plot).findAll();
                        CollectionUtils.emptyIfNull(zones)
                                .stream()
                                .map(zone -> toZoneBean(baseBean, zone))
                                .forEach(zoneBeans::add);
                    }

                }
            }
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't copy properties", ex);
        }

        // technical export

        ExportModelAndRows<MainBean> mainTab = new ExportModelAndRows<>(new MainModel(), mainBeans);
        ExportModelAndRows<ZoningBean> zoningTab = new ExportModelAndRows<>(new ZoningModel(), zoningBeans);
        ExportModelAndRows<EquipmentBean> equipmentTab = new ExportModelAndRows<>(new EquipmentModel(), equipmentBeans);
        ExportModelAndRows<GroundBean> groundTab = new ExportModelAndRows<>(new GroundModel(refSolTextureGeppaDao, refSolProfondeurIndigoDao), groundBeans);
        ExportModelAndRows<AdjacentBean> adjacentTab = new ExportModelAndRows<>(new AdjacentModel(refElementVoisinageDao), adjacentBeans);
        ExportModelAndRows<GroundHorizonBean> groundHorizonTab = new ExportModelAndRows<>(new GroundHorizonModel(), groundHorizonBeans);
        ExportModelAndRows<ZoneBean> zoneTab = new ExportModelAndRows<>(new ZoneModel(), zoneBeans);

        EntityExporter exporter = new EntityExporter();
        ExportResult result = exporter.exportAsXlsx(
                "parcelle-export",
                mainTab,
                zoningTab,
                equipmentTab,
                groundTab,
                adjacentTab,
                groundHorizonTab,
                zoneTab
        );
        return result;
    }

    @Override
    public void exportPlotsAsXlsAsync(Collection<String> plotIds) {
        AuthenticatedUser user = getAuthenticatedUser();

        PlotExportTask exportTask = new PlotExportTask(user.getTopiaId(), user.getEmail(), plotIds);
        getBusinessTaskManager().schedule(exportTask);
    }

    protected MainBean toMainBean(Plot plot, CommonBean baseBean) {
        MainBean mainBean = new MainBean(baseBean);
        Optional<RefLocation> location = Optional.ofNullable(plot.getLocation());
        location.map(RefLocation::getCodePostal)
                .ifPresent(mainBean::setCodePostal);
        location.map(RefLocation::getCommune)
                .ifPresent(mainBean::setCommune);
        Optional.ofNullable(plot.getGrowingSystem())
                .map(GrowingSystem::getName)
                .ifPresent(mainBean::setGrowingSystemName);
        mainBean.setArea(plot.getArea());
        mainBean.setDistanceToHQ(plot.getDistanceToHQ());
        mainBean.setPacIlotNumber(plot.getPacIlotNumber());
        mainBean.setMaxSlope(plot.getMaxSlope());
        mainBean.setWaterFlowDistance(plot.getWaterFlowDistance());
        mainBean.setBufferStrip(plot.getBufferStrip());
        mainBean.setLatitude(plot.getLatitude());
        mainBean.setLongitude(plot.getLongitude());
        mainBean.setComment(plot.getComment());
        mainBean.setActivityEndComment(plot.getActivityEndComment());
        mainBean.setZoningComment(plot.getZoningComment());
        mainBean.setAdjacentComment(plot.getAdjacentComment());
        return mainBean;
    }

    protected GroundHorizonBean toGroundHorizonBean(CommonBean baseBean, SolHorizon solHorizon) {
        GroundHorizonBean groundHorizonBean = new GroundHorizonBean(baseBean);
        groundHorizonBean.setLowRating(solHorizon.getLowRating());
        groundHorizonBean.setStoniness(solHorizon.getStoniness());
        Optional.ofNullable(solHorizon.getSolTexture())
                .map(RefSolTextureGeppa::getClasses_texturales_GEPAA)
                .ifPresent(groundHorizonBean::setSolTexture);
        groundHorizonBean.setComment(solHorizon.getComment());
        return groundHorizonBean;
    }

    protected EquipmentBean toEquipmentBean(Plot plot, CommonBean baseBean) {
        EquipmentBean equipmentBean = new EquipmentBean(baseBean);
        equipmentBean.setIrrigationSystem(plot.isIrrigationSystem());
        equipmentBean.setIrrigationSystemType(plot.getIrrigationSystemType());
        equipmentBean.setPompEngineType(plot.getPompEngineType());
        equipmentBean.setHosesPositionning(plot.getHosesPositionning());
        equipmentBean.setFertigationSystem(plot.isFertigationSystem());
        equipmentBean.setDrainage(plot.isDrainage());
        equipmentBean.setWaterOrigin(plot.getWaterOrigin());
        equipmentBean.setDrainageYear(plot.getDrainageYear());
        equipmentBean.setFrostProtection(plot.isFrostProtection());
        equipmentBean.setFrostProtectionType(plot.getFrostProtectionType());
        equipmentBean.setHailProtection(plot.isHailProtection());
        equipmentBean.setRainproofProtection(plot.isRainproofProtection());
        equipmentBean.setPestProtection(plot.isPestProtection());
        equipmentBean.setOtherEquipment(plot.getOtherEquipment());
        equipmentBean.setEquipmentComment(plot.getEquipmentComment());
        return equipmentBean;
    }

    protected GroundBean toGroundBean(Plot plot, CommonBean baseBean) {
        GroundBean groundBean = new GroundBean(baseBean);
        Optional.ofNullable(plot.getGround())
                .map(Ground::getName)
                .ifPresent(groundBean::setGroundName);
        Optional.ofNullable(plot.getSurfaceTexture())
                .map(RefSolTextureGeppa::getClasses_texturales_GEPAA)
                .ifPresent(groundBean::setSurfaceTexture);
        Optional.ofNullable(plot.getSubSoilTexture())
                .map(RefSolTextureGeppa::getClasses_texturales_GEPAA)
                .ifPresent(groundBean::setSubSoilTexture);
        groundBean.setSolStoniness(plot.getSolStoniness());
        Optional.ofNullable(plot.getSolDepth())
                .map(RefSolProfondeurIndigo::getLibelle_classe)
                .ifPresent(groundBean::setSolDepth);
        groundBean.setSolMaxDepth(plot.getSolMaxDepth());
        groundBean.setSolOrganicMaterialPercent(plot.getSolOrganicMaterialPercent());
        groundBean.setSolBattance(plot.isSolBattance());
        groundBean.setSolWaterPh(plot.getSolWaterPh());
        groundBean.setSolHydromorphisms(plot.isSolHydromorphisms());
        groundBean.setSolLimestone(plot.isSolLimestone());
        groundBean.setSolActiveLimestone(plot.getSolActiveLimestone());
        groundBean.setSolTotalLimestone(plot.getSolTotalLimestone());
        groundBean.setSolComment(plot.getSolComment());
        return groundBean;
    }

    protected AdjacentBean toAdjacentBean(CommonBean baseBean, RefElementVoisinage adjacentElement) {
        AdjacentBean adjacentBean = new AdjacentBean(baseBean);
        Optional.ofNullable(adjacentElement)
                .map(RefElementVoisinage::getIae_nom)
                .ifPresent(adjacentBean::setAdjacentElements);
        return adjacentBean;
    }

    protected ZoneBean toZoneBean(CommonBean baseBean, Zone zone) {
        ZoneBean zoneBean = new ZoneBean(baseBean);
        zoneBean.setZoneName(zone.getName());
        zoneBean.setArea(zone.getArea());
        zoneBean.setLatitude(zone.getLatitude());
        zoneBean.setLongitude(zone.getLongitude());
        zoneBean.setComment(zone.getComment());
        zoneBean.setType(zone.getType());
        return zoneBean;
    }

    @Override
    public List<Plot> getAllGrowingSystemPlot(GrowingSystem growingSystem) {
        Preconditions.checkNotNull(growingSystem);
        return plotDao.forGrowingSystemEquals(growingSystem).findAll();
    }
}
