package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDIImpl;
import fr.inra.agrosyst.services.common.CommonService;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Modele d'import du référentiel NuisiblesEDI.
 * 
 * <ul>
 * <li>repository_id
 * <li>reference_id
 * <li>reference_param
 * <li>reference_code
 * <li>reference_label
 * <li>source
 * </ul>
 * 
 * @author Eric Chatellier
 */
public class RefNuisibleEDIModel extends AbstractAgrosystModel<RefNuisibleEDI> implements ExportModel<RefNuisibleEDI> {

    public RefNuisibleEDIModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("repository_id", RefNuisibleEDI.PROPERTY_REPOSITORY_ID, INT_PARSER);
        newMandatoryColumn("reference_id", RefNuisibleEDI.PROPERTY_REFERENCE_ID, INT_PARSER);
        newMandatoryColumn("reference_param", RefNuisibleEDI.PROPERTY_REFERENCE_PARAM, AGROSYST_BIO_AGRESSOR_TYPE_PARSER);
        newMandatoryColumn("reference_code", RefNuisibleEDI.PROPERTY_REFERENCE_CODE);
        newMandatoryColumn("reference_label", RefNuisibleEDI.PROPERTY_REFERENCE_LABEL);
        newMandatoryColumn("filieres", RefNuisibleEDI.PROPERTY_SECTORS, AGROSYST_SECTORS_PARSER);
        newMandatoryColumn("source", RefNuisibleEDI.PROPERTY_SOURCE);
        newMandatoryColumn("main", RefNuisibleEDI.PROPERTY_MAIN, CommonService.BOOLEAN_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefNuisibleEDI.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefNuisibleEDI, Object>> getColumnsForExport() {
        ModelBuilder<RefNuisibleEDI> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("repository_id", RefNuisibleEDI.PROPERTY_REPOSITORY_ID, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("reference_id", RefNuisibleEDI.PROPERTY_REFERENCE_ID, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("reference_param", RefNuisibleEDI.PROPERTY_REFERENCE_PARAM, AGROSYST_BIO_AGRESSOR_TYPE_FORMATTER);
        modelBuilder.newColumnForExport("reference_code", RefNuisibleEDI.PROPERTY_REFERENCE_CODE);
        modelBuilder.newColumnForExport("reference_label", RefNuisibleEDI.PROPERTY_REFERENCE_LABEL);
        modelBuilder.newColumnForExport("filieres", RefNuisibleEDI.PROPERTY_SECTORS, AGROSYST_SECTORS_FORMATTER);
        modelBuilder.newColumnForExport("source", RefNuisibleEDI.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport("main", RefNuisibleEDI.PROPERTY_MAIN, T_F_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefNuisibleEDI.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefNuisibleEDI newEmptyInstance() {
        RefNuisibleEDI refNuisibleEDI = new RefNuisibleEDIImpl();
        refNuisibleEDI.setActive(true);
        return refNuisibleEDI;
    }
}
