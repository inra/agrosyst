/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.services.report;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.NetworkTopiaDao;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.Section;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDITopiaDao;
import fr.inra.agrosyst.api.entities.report.ArboAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropPestMaster;
import fr.inra.agrosyst.api.entities.report.ArboPestMaster;
import fr.inra.agrosyst.api.entities.report.CropPestMaster;
import fr.inra.agrosyst.api.entities.report.FoodMaster;
import fr.inra.agrosyst.api.entities.report.IftEstimationMethod;
import fr.inra.agrosyst.api.entities.report.PestMaster;
import fr.inra.agrosyst.api.entities.report.PestPressure;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.report.ReportRegional;
import fr.inra.agrosyst.api.entities.report.ReportRegionalTopiaDao;
import fr.inra.agrosyst.api.entities.report.VerseMaster;
import fr.inra.agrosyst.api.entities.report.VitiPestMaster;
import fr.inra.agrosyst.api.entities.report.VitiPestMasterImpl;
import fr.inra.agrosyst.api.entities.report.YieldInfo;
import fr.inra.agrosyst.api.entities.report.YieldLoss;
import fr.inra.agrosyst.api.entities.report.YieldLossCause;
import fr.inra.agrosyst.api.entities.report.YieldObjective;
import fr.inra.agrosyst.api.services.common.BinaryContent;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.report.ReportExportOption;
import fr.inra.agrosyst.api.services.report.ReportFilter;
import fr.inra.agrosyst.api.services.report.ReportGrowingSystemCollections;
import fr.inra.agrosyst.api.services.report.ReportGrowingSystemDto;
import fr.inra.agrosyst.api.services.report.ReportGrowingSystemFilter;
import fr.inra.agrosyst.api.services.report.ReportGrowingSystemSection;
import fr.inra.agrosyst.api.services.report.ReportRegionalFilter;
import fr.inra.agrosyst.api.services.report.ReportService;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.api.utils.TopiaUtils;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.common.export.AgrosystDateUtils;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MultiMapUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.nuiton.util.pagination.PaginationResult;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.nuiton.i18n.I18n.t;

@Setter
public class ReportServiceImpl extends AbstractAgrosystService implements ReportService {

    private static final Log LOGGER = LogFactory.getLog(ReportServiceImpl.class);

    protected AnonymizeService anonymizeService;
    protected BusinessAuthorizationService authorizationService;
    protected ManagementModeService managementModeService;
    protected ReferentialService referentialService;

    protected ReportRegionalTopiaDao reportRegionalDao;
    protected ReportGrowingSystemTopiaDao reportGrowingSystemDao;

    protected DomainTopiaDao domainDao;
    protected GrowingSystemTopiaDao growingSystemDao;
    protected NetworkTopiaDao networkDao;
    protected RefNuisibleEDITopiaDao refNuisibleEDIDao;
    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;

    @Override
    public PaginationResult<ReportRegional> getAllReportRegional(ReportRegionalFilter filter) {
        return reportRegionalDao.findFilteredReportRegional(filter, getSecurityContext());
    }

    @Override
    public Set<String> getAllReportRegionalIds(ReportRegionalFilter filter) {
        return reportRegionalDao.findFilteredReportRegionalIds(filter, getSecurityContext());
    }

    @Override
    public ReportRegional getReportRegional(String topiaId) {
        return reportRegionalDao.forTopiaIdEquals(topiaId).findUnique();
    }

    @Override
    public List<String> deleteReportRegionals(List<String> reportRegionalIds) {
        
        List<String> relatedReportGrowingSystemNames = null;
        for (String reportRegionalId : reportRegionalIds) {
            authorizationService.checkDeleteReportRegional(reportRegionalId);
        }

        if (LOGGER.isInfoEnabled()) {
            AuthenticatedUser user = getAuthenticatedUser();
            String userEmail = user != null ? user.getEmail() : "Email utilisateur inconnu";
            LOGGER.info(userEmail + " va supprimer les Bilan de campagne / échelle régionale suivant: " + StringUtils.join(reportRegionalIds, ","));
        }

        List<ReportRegional> reportRegionals = reportRegionalDao.forTopiaIdIn(reportRegionalIds).findAll();
        
        List<ReportGrowingSystem> reportGrowingSystems = reportGrowingSystemDao.forReportRegionalIn(reportRegionals).findAll();
        if (reportGrowingSystems.isEmpty()) {
            reportRegionalDao.deleteAll(reportRegionals);
        } else {
            relatedReportGrowingSystemNames = reportGrowingSystems.stream().map(ReportGrowingSystem::getName).distinct().sorted().collect(Collectors.toList());
        }
        
        getTransaction().commit();
        
        return relatedReportGrowingSystemNames;
    }

    @Override
    public ReportRegional createOrUpdateReportRegional(ReportRegional reportRegional, List<String> reportRegionalNetworkIds,
                                                       List<PestPressure> currentDiseasePressures, List<PestPressure> currentPestPressures) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(reportRegionalNetworkIds));

        // security
        String reportRegionalId = reportRegional.getTopiaId();
        authorizationService.checkCreateOrUpdateReportRegional(reportRegionalId);

        if (!reportRegional.isPersisted()) {
            reportRegional.setCode(UUID.randomUUID().toString());
        }

        // network
        List<Network> networks = networkDao.forTopiaIdIn(reportRegionalNetworkIds).findAll();
        Preconditions.checkArgument(networks.size() == reportRegionalNetworkIds.size(), "Réseau non retrouvé !");
        reportRegional.setNetworks(networks);

        // pests (manual update)
        Collection<PestPressure> newPestPressures = TopiaUtils.updateCollection(BinderFactory.newBinder(PestPressure.class),
                reportRegional.getPestPressures(), currentPestPressures);
        reportRegional.clearPestPressures();
        reportRegional.addAllPestPressures(newPestPressures);

        // pests (manual update)
        Collection<PestPressure> newDiseasePressures = TopiaUtils.updateCollection(BinderFactory.newBinder(PestPressure.class),
                reportRegional.getDiseasePressures(), currentDiseasePressures);
        reportRegional.clearDiseasePressures();
        reportRegional.addAllDiseasePressures(newDiseasePressures);

        // save
        reportRegional = reportRegionalDao.update(reportRegional);

        // security
        if (StringUtils.isBlank(reportRegionalId)) {
            authorizationService.reportRegionalCreated(reportRegional);
        }

        getTransaction().commit();

        return reportRegional;
    }

    @Override
    public LinkedHashMap<Integer, String> getRelatedReportRegionals(String reportRegionalCode) {
        return reportRegionalDao.findAllRelatedReports(reportRegionalCode);
    }

    @Override
    public LinkedHashMap<Integer, String> getRelatedReportGrowingSystems(String reportRegionalCode) {
        return reportGrowingSystemDao.findAllRelatedReports(reportRegionalCode);
    }

    @Override
    public List<String> getCropIdsForGrowingSystemId(String growingSystemId) {
        return StringUtils.isNotBlank(growingSystemId) ?
                croppingPlanEntryDao.findCropsIdsForGrowingSystemId(growingSystemId) : new ArrayList<>();
    }

    @Override
    public List<CropPestMaster> loadCropAdventiceMasters(String reportGrowingSystemId, Sector sector) {
        List<CropPestMaster> result = StringUtils.isEmpty(reportGrowingSystemId) ? new ArrayList<>() :
                reportGrowingSystemDao.findReportGrowingSystemCollections(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_CROP_ADVENTICE_MASTERS);
    
        result = result.stream().filter(cropPestMaster -> Objects.equals(sector, cropPestMaster.getSector())).collect(Collectors.toList());
        
        // prolongation
        if (CollectionUtils.isEmpty(result)) {
            
            result = reportGrowingSystemDao.findReportGrowingSystemCollectionsForPreviousCampaign(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_CROP_ADVENTICE_MASTERS);
            
            result = result.stream().filter(cropPestMaster -> Objects.equals(sector, cropPestMaster.getSector())).collect(Collectors.toList());
            
            for (CropPestMaster cropPestMaster : result) {
                TopiaUtils.cleanTopiaId(cropPestMaster, cropPestMaster::getPestMasters);
                cropPestMaster.setIftMain(null);
                cropPestMaster.setIftOther(null);
            }
        }
        
        return result;
    }

    @Override
    public List<CropPestMaster> loadCropDiseaseMasters(String reportGrowingSystemId, Sector sector) {
        List<CropPestMaster> result = StringUtils.isEmpty(reportGrowingSystemId) ? new ArrayList<>() :
                reportGrowingSystemDao.findReportGrowingSystemCollections(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_CROP_DISEASE_MASTERS);
    
        result = result.stream().filter(cropPestMaster -> Objects.equals(sector, cropPestMaster.getSector())).collect(Collectors.toList());
        // prolongation
        if (CollectionUtils.isEmpty(result)) {
    
            result = reportGrowingSystemDao.findReportGrowingSystemCollectionsForPreviousCampaign(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_CROP_DISEASE_MASTERS);
    
            result = result.stream().filter(cropPestMaster -> Objects.equals(sector, cropPestMaster.getSector())).collect(Collectors.toList());
            
            for (CropPestMaster cropPestMaster : result) {
                TopiaUtils.cleanTopiaId(cropPestMaster, cropPestMaster::getPestMasters);
                cropPestMaster.setIftMain(null);
                cropPestMaster.setIftOther(null);
            }
        }
        return result;
    }

    @Override
    public List<CropPestMaster> loadCropPestMasters(String reportGrowingSystemId, Sector sector) {
        List<CropPestMaster> result = StringUtils.isEmpty(reportGrowingSystemId) ? new ArrayList<>() :
                reportGrowingSystemDao.findReportGrowingSystemCollections(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_CROP_PEST_MASTERS);

        result = result.stream().filter(cropPestMaster -> Objects.equals(sector, cropPestMaster.getSector())).collect(Collectors.toList());
        
        // prolongation
        if (CollectionUtils.isEmpty(result)) {
            result = reportGrowingSystemDao.findReportGrowingSystemCollectionsForPreviousCampaign(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_CROP_PEST_MASTERS);
    
            result = result.stream().filter(cropPestMaster -> Objects.equals(sector, cropPestMaster.getSector())).collect(Collectors.toList());
            
            for (CropPestMaster cropPestMaster : result) {
                TopiaUtils.cleanTopiaId(cropPestMaster, cropPestMaster::getPestMasters);
                cropPestMaster.setIftMain(null);
                cropPestMaster.setIftOther(null);
            }
        }
        return result;
    }

    @Override
    public List<VerseMaster> loadVerseMasters(String reportGrowingSystemId, Sector sector) {
        List<VerseMaster> result = StringUtils.isEmpty(reportGrowingSystemId) ? new ArrayList<>() :
                reportGrowingSystemDao.findReportGrowingSystemCollections(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_VERSE_MASTERS);
    
        result = result.stream().filter(cropPestMaster -> Objects.equals(sector, cropPestMaster.getSector())).collect(Collectors.toList());
        
        // prolongation
        if (CollectionUtils.isEmpty(result)) {
            
            result = reportGrowingSystemDao.findReportGrowingSystemCollectionsForPreviousCampaign(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_VERSE_MASTERS);
    
            result = result.stream().filter(cropPestMaster -> Objects.equals(sector, cropPestMaster.getSector())).collect(Collectors.toList());
            
            for (VerseMaster verseMaster : result) {
                TopiaUtils.cleanTopiaId(verseMaster);
                verseMaster.setIftMain(null);
            }
        }
        return result;
    }

    @Override
    public List<FoodMaster> loadFoodMasters(String reportGrowingSystemId, Sector sector) {
        List<FoodMaster> result = StringUtils.isEmpty(reportGrowingSystemId) ? new ArrayList<>() :
                reportGrowingSystemDao.findReportGrowingSystemCollections(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_FOOD_MASTERS);
    
        result = result.stream().filter(cropPestMaster -> Objects.equals(sector, cropPestMaster.getSector())).collect(Collectors.toList());
        
        // prolongation
        if (CollectionUtils.isEmpty(result)) {
            result = reportGrowingSystemDao.findReportGrowingSystemCollectionsForPreviousCampaign(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_FOOD_MASTERS);
    
            result = result.stream().filter(cropPestMaster -> Objects.equals(sector, cropPestMaster.getSector())).collect(Collectors.toList());
            
            TopiaUtils.cleanTopiaId(result);
        }
        return result;
    }

    @Override
    public List<YieldLoss> loadYieldLosses(String reportGrowingSystemId, Sector sector) {
        List<YieldLoss> result = StringUtils.isEmpty(reportGrowingSystemId) ? new ArrayList<>() :
                reportGrowingSystemDao.findReportGrowingSystemCollections(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_YIELD_LOSSES);
    
        result = result.stream().filter(cropPestMaster -> Objects.equals(sector, cropPestMaster.getSector())).collect(Collectors.toList());
        
        // prolongation
        if (CollectionUtils.isEmpty(result)) {
            result = reportGrowingSystemDao.findReportGrowingSystemCollectionsForPreviousCampaign(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_YIELD_LOSSES);
    
            result = result.stream().filter(cropPestMaster -> Objects.equals(sector, cropPestMaster.getSector())).collect(Collectors.toList());
            
            TopiaUtils.cleanTopiaId(result);
        }
        return result;
    }

    @Override
    public List<ArboCropAdventiceMaster> loadArboCropAdventiceMasters(String reportGrowingSystemId) {
        List<ArboCropAdventiceMaster> result = StringUtils.isEmpty(reportGrowingSystemId) ? new ArrayList<>() :
                reportGrowingSystemDao.findReportGrowingSystemCollections(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_ARBO_CROP_ADVENTICE_MASTERS);
        
        // prolongation
        if (CollectionUtils.isEmpty(result)) {
            result = reportGrowingSystemDao.findReportGrowingSystemCollectionsForPreviousCampaign(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_ARBO_CROP_ADVENTICE_MASTERS);
            for (ArboCropAdventiceMaster arboCropAdventiceMaster : result) {
                TopiaUtils.cleanTopiaId(arboCropAdventiceMaster);
                arboCropAdventiceMaster.setBioControlPestIFT(null);
                arboCropAdventiceMaster.setChemicalPestIFT(null);
                arboCropAdventiceMaster.setTreatmentCount(null);
            }
        }
        return result;
    }

    @Override
    public List<ArboCropPestMaster> loadArboCropDiseaseMasters(String reportGrowingSystemId) {
        List<ArboCropPestMaster> result = StringUtils.isEmpty(reportGrowingSystemId) ? new ArrayList<>() :
                reportGrowingSystemDao.findReportGrowingSystemCollections(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_ARBO_CROP_DISEASE_MASTERS);

        // prolongation
        if (CollectionUtils.isEmpty(result)) {
            result = reportGrowingSystemDao.findReportGrowingSystemCollectionsForPreviousCampaign(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_ARBO_CROP_DISEASE_MASTERS);
            for (ArboCropPestMaster arboCropPestMaster : result) {
                TopiaUtils.cleanTopiaId(arboCropPestMaster);
                arboCropPestMaster.setBioControlPestIFT(null);
                arboCropPestMaster.setChemicalPestIFT(null);
                arboCropPestMaster.setTreatmentCount(null);
            }
        }
        return result;
    }

    @Override
    public List<ArboCropPestMaster> loadArboCropPestMasters(String reportGrowingSystemId) {
        List<ArboCropPestMaster> result = StringUtils.isEmpty(reportGrowingSystemId) ? new ArrayList<>() :
                reportGrowingSystemDao.findReportGrowingSystemCollections(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_ARBO_CROP_PEST_MASTERS);

        // prolongation
        if (CollectionUtils.isEmpty(result)) {
            result = reportGrowingSystemDao.findReportGrowingSystemCollectionsForPreviousCampaign(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_ARBO_CROP_PEST_MASTERS);
            for (ArboCropPestMaster arboCropPestMaster : result) {
                TopiaUtils.cleanTopiaId(arboCropPestMaster);
                arboCropPestMaster.setBioControlPestIFT(null);
                arboCropPestMaster.setChemicalPestIFT(null);
                arboCropPestMaster.setTreatmentCount(null);
            }
        }
        return result;
    }

    @Override
    public List<YieldLoss> loadArboYieldLosses(String reportGrowingSystemId) {
        List<YieldLoss> result = StringUtils.isEmpty(reportGrowingSystemId) ? new ArrayList<>() :
                reportGrowingSystemDao.findReportGrowingSystemCollections(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_YIELD_LOSSES);
    
        result = result.stream().filter(cropPestMaster -> Sector.ARBORICULTURE.equals(cropPestMaster.getSector())).collect(Collectors.toList());
        
        // prolongation
        if (CollectionUtils.isEmpty(result)) {
            result = reportGrowingSystemDao.findReportGrowingSystemCollectionsForPreviousCampaign(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_YIELD_LOSSES);
    
            result = result.stream().filter(cropPestMaster -> Sector.ARBORICULTURE.equals(cropPestMaster.getSector())).collect(Collectors.toList());
            
            TopiaUtils.cleanTopiaId(result);
        }
        return result;
    }

    @Override
    public List<VitiPestMaster> loadVitiDiseaseMasters(String reportGrowingSystemId) {
        List<VitiPestMaster> result = StringUtils.isEmpty(reportGrowingSystemId) ? new ArrayList<>() :
                reportGrowingSystemDao.findReportGrowingSystemCollections(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_VITI_DISEASE_MASTERS);

        // prolongation
        if (CollectionUtils.isEmpty(result)) {
            result = reportGrowingSystemDao.findReportGrowingSystemCollectionsForPreviousCampaign(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_VITI_DISEASE_MASTERS);
            for (VitiPestMaster vitiPestMaster : result) {
                TopiaUtils.cleanTopiaId(vitiPestMaster);
                vitiPestMaster.setBioControlFungicideIFT(null);
                vitiPestMaster.setChemicalFungicideIFT(null);
                vitiPestMaster.setTreatmentCount(null);
            }
        }

        // cas special pour les maladies de la viticulture (maladies obligatoires)
        if (CollectionUtils.isEmpty(result)) {
            VitiPestMaster mildiou = new VitiPestMasterImpl();
            mildiou.setAgressor(refNuisibleEDIDao.forReference_idEquals(MILDIOU).findUnique()); // Mildiou
            VitiPestMaster oidium = new VitiPestMasterImpl();
            oidium.setAgressor(refNuisibleEDIDao.forReference_idEquals(OIDIUM).findUnique()); // Oïdium
            VitiPestMaster blackrot = new VitiPestMasterImpl();
            blackrot.setAgressor(refNuisibleEDIDao.forReference_idEquals(VIROSE).findUnique()); // Virose Black Rot
            VitiPestMaster botrytis = new VitiPestMasterImpl();
            botrytis.setAgressor(refNuisibleEDIDao.forReference_idEquals(BOTRYTIS).findUnique()); // Botrytis - Pourriture Grise
            result = Arrays.asList(mildiou, oidium, blackrot, botrytis);
        }
        return result;
    }

    @Override
    public List<VitiPestMaster> loadVitiPestMasters(String reportGrowingSystemId) {
        List<VitiPestMaster> result = StringUtils.isEmpty(reportGrowingSystemId) ? new ArrayList<>() :
                reportGrowingSystemDao.findReportGrowingSystemCollections(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_VITI_PEST_MASTERS);

        // prolongation
        if (CollectionUtils.isEmpty(result)) {
            result = reportGrowingSystemDao.findReportGrowingSystemCollectionsForPreviousCampaign(reportGrowingSystemId, ReportGrowingSystem.PROPERTY_VITI_PEST_MASTERS);
            for (VitiPestMaster vitiPestMaster : result) {
                TopiaUtils.cleanTopiaId(vitiPestMaster);
                vitiPestMaster.setBioControlFungicideIFT(null);
                vitiPestMaster.setChemicalFungicideIFT(null);
                vitiPestMaster.setTreatmentCount(null);
            }
        }

        // cas special pour les maladies de la viticulture (ravageurs obligatoires)
        if (CollectionUtils.isEmpty(result)) {
            VitiPestMaster flavescence = new VitiPestMasterImpl();
            flavescence.setAgressor(refNuisibleEDIDao.forReference_idEquals(CITADELLE_FLAVESCENCE).findUnique()); // Cicadelle de la flavescence dorée
            VitiPestMaster grillures = new VitiPestMasterImpl();
            grillures.setAgressor(refNuisibleEDIDao.forReference_idEquals(CITADELLE_GRILLURES).findUnique()); // Cicadelle des grillures
            VitiPestMaster tordeuse = new VitiPestMasterImpl();
            tordeuse.setAgressor(refNuisibleEDIDao.forReference_idEquals(TORDEUSE_GRAPPE).findUnique()); // Tordeuse(s) de la grappe
            result = Arrays.asList(flavescence, grillures, tordeuse);
        }
        return result;
    }

    @Override
    public PaginationResult<ReportGrowingSystemDto> getAllReportGrowingSystem(ReportGrowingSystemFilter filter) {
        PaginationResult<ReportGrowingSystem> result0 = reportGrowingSystemDao.findFilteredReportGrowingSystems(filter, getSecurityContext());
        return result0.transform(reportGrowingSystemDtoToDtoFunction());
    }
    
    
    protected Function<ReportGrowingSystem, ReportGrowingSystemDto> reportGrowingSystemDtoToDtoFunction() {
        return input -> {
            if (input == null) {
                return null;
            }
            ReportGrowingSystemDto result = new ReportGrowingSystemDto();
            result.setTopiaId(input.getTopiaId());
            result.setName(input.getName());
            result.setGrowingSystem(anonymizeService.getGrowingSystemToDtoFunction().apply(input.getGrowingSystem()));
            return result;
        };
    }

    @Override
    public Set<String> getAllReportGrowingSystemIds(ReportGrowingSystemFilter filter) {
        return reportGrowingSystemDao.findFilteredReportGrowingSystemIds(filter, getSecurityContext());
    }

    @Override
    public ReportGrowingSystem getReportGrowingSystem(String reportGrowingSystemId) {
        return StringUtils.isBlank(reportGrowingSystemId) ? null : reportGrowingSystemDao.forTopiaIdEquals(reportGrowingSystemId).findUnique();
    }

    @Override
    public void deleteReportGrowingSystems(List<String> reportGrowingSystemIds) {
        for (String reportGrowingSystemId : reportGrowingSystemIds) {
            authorizationService.checkDeleteReportGrowingSystem(reportGrowingSystemId);
        }

        if (LOGGER.isInfoEnabled()) {
            AuthenticatedUser user = getAuthenticatedUser();
            String userEmail = user != null ? user.getEmail() : "Email utilisateur inconnu";
            LOGGER.info(userEmail + " va supprimer les bilans de campagnes / échelle Système de culture suivant: " + StringUtils.join(reportGrowingSystemIds, ","));
        }

        List<ReportGrowingSystem> reportGrowingSystemsToRemoves = reportGrowingSystemDao.forTopiaIdIn(reportGrowingSystemIds).findAll();
        reportGrowingSystemDao.deleteAll(reportGrowingSystemsToRemoves);
        getTransaction().commit();
    }

    protected String computePdfReportGrowingSystemsFileName() {
        AuthenticatedUser authenticatedUser = getAuthenticatedUser();
        String filename = "Bilan de campagne échelle sdc ";
        // name
        if (StringUtils.isNotBlank(authenticatedUser.getFirstName())) {
            filename += authenticatedUser.getFirstName() + " ";
        }
        if (StringUtils.isNotBlank(authenticatedUser.getLastName())) {
            filename += authenticatedUser.getLastName() + " ";
        }
        // date
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String formattedString = LocalDate.now().format(formatter);
        filename += formattedString;
        return filename;
    }

    @Override
    public ExportResult exportPdfReportGrowingSystems(ReportExportOption options) {
        List<ReportGrowingSystem> reportGrowingSystemsToExport = reportGrowingSystemDao.forTopiaIdIn(options.getReportGrowingSystemIds()).findAll();
        Map<ReportGrowingSystem, ManagementMode> managementModes = new HashMap<>();
        for (ReportGrowingSystem reportGrowingSystem : reportGrowingSystemsToExport) {
            ManagementMode managementModeForGrowingSystem = managementModeService.getManagementModeForGrowingSystem(reportGrowingSystem.getGrowingSystem());
            managementModes.put(reportGrowingSystem, managementModeForGrowingSystem);
        }
        Map<String, String> groupesCiblesParCode = referentialService.getGroupesCiblesParCode();
        ReportPdfExport export = new ReportPdfExport();
        BinaryContent binaryContent = export.exportPdfReportGrowingSystems(options, reportGrowingSystemsToExport, managementModes, groupesCiblesParCode);
        return new ExportResult(binaryContent, computePdfReportGrowingSystemsFileName());
    }

    @Override
    public void exportPdfReportGrowingSystemsAsync(ReportExportOption options) {
        AuthenticatedUser user = getAuthenticatedUser();

        ReportGrowingSystemsExportPdfTask exportTask = new ReportGrowingSystemsExportPdfTask(user.getTopiaId(), user.getEmail(), options);
        getBusinessTaskManager().schedule(exportTask);
    }

    /**
     * Nom du fichier : Bilan de campagne_échelle régionale_nom utilisateur_date
     */
    protected String computeXlsReportRegionalsFileName() {
        AuthenticatedUser authenticatedUser = getAuthenticatedUser();
        String filename = "Bilan de campagne_échelle régionale ";
        // name
        if (StringUtils.isNotBlank(authenticatedUser.getFirstName())) {
            filename += authenticatedUser.getFirstName() + " ";
        }
        if (StringUtils.isNotBlank(authenticatedUser.getLastName())) {
            filename += authenticatedUser.getLastName() + " ";
        }
        // date
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String formattedString = LocalDate.now().format(formatter);
        filename += formattedString;
        return filename;
    }

    @Override
    public ExportResult exportXlsReportRegionals(Collection<String> reportRegionalIds) {
        Map<String, String> groupesCiblesParCode = referentialService.getGroupesCiblesParCode();
        ReportXlsExport export = new ReportXlsExport(groupesCiblesParCode);
        List<ReportRegional> reportRegionalsToExport = reportRegionalDao.forTopiaIdIn(reportRegionalIds).findAll();
        BinaryContent binaryContent = export.exportXlsReportRegionals(reportRegionalsToExport);
        return new ExportResult(binaryContent, computeXlsReportRegionalsFileName());
    }

    @Override
    public void exportXlsReportRegionalsAsync(Collection<String> reportRegionalIds) {
        AuthenticatedUser user = getAuthenticatedUser();

        ReportRegionalsExportTask exportTask = new ReportRegionalsExportTask(user.getTopiaId(), user.getEmail(), reportRegionalIds);
        getBusinessTaskManager().schedule(exportTask);
    }

    /**
     * Nom du fichier : Bilan de campagne_échelle régionale_nom utilisateur_date
     */
    public String computeXlsReportGrowingSystemsFileName() {
        AuthenticatedUser authenticatedUser = getAuthenticatedUser();
        String filename = "Bilan de campagne échelle sdc ";
        // name
        if (StringUtils.isNotBlank(authenticatedUser.getFirstName())) {
            filename += authenticatedUser.getFirstName() + " ";
        }
        if (StringUtils.isNotBlank(authenticatedUser.getLastName())) {
            filename += authenticatedUser.getLastName() + " ";
        }
        // date
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String formattedString = LocalDate.now().format(formatter);
        filename += formattedString;
        return filename;
    }

    @Override
    public ExportResult exportXlsReportGrowingSystems(Collection<String> reportGrowingSystemIds) {
        Map<String, String> groupesCiblesParCode = referentialService.getGroupesCiblesParCode();
        ReportXlsExport export = new ReportXlsExport(groupesCiblesParCode);
        List<ReportGrowingSystem> reportGrowingSystemsToExport = reportGrowingSystemDao.forTopiaIdIn(reportGrowingSystemIds).findAll();
        BinaryContent binaryContent = export.exportXlsReportGrowingSystems(reportGrowingSystemsToExport);
        return new ExportResult(binaryContent, computeXlsReportGrowingSystemsFileName());
    }

    @Override
    public void exportXlsReportGrowingSystemsAsync(Collection<String> reportGrowingSystemIds) {
        AuthenticatedUser user = getAuthenticatedUser();

        ReportGrowingSystemsExportTask exportTask = new ReportGrowingSystemsExportTask(user.getTopiaId(), user.getEmail(), reportGrowingSystemIds);
        getBusinessTaskManager().schedule(exportTask);
    }

    protected Binder<CropPestMaster, CropPestMaster> cropPestMasterBinder = BinderFactory.newBinder(CropPestMaster.class);
    protected Binder<PestMaster, PestMaster> pestMasterBinder = BinderFactory.newBinder(PestMaster.class);
    protected Binder<VerseMaster, VerseMaster> verseMasterBinder = BinderFactory.newBinder(VerseMaster.class);
    protected Binder<FoodMaster, FoodMaster> foodMasterBinder = BinderFactory.newBinder(FoodMaster.class);
    protected Binder<YieldLoss, YieldLoss> yieldLossBinder = BinderFactory.newBinder(YieldLoss.class);
    protected Binder<YieldInfo, YieldInfo> yieldInfoBinder = BinderFactory.newBinder(YieldInfo.class);
    protected Binder<ArboCropAdventiceMaster, ArboCropAdventiceMaster> arboCropAdventiceMasterBinder = BinderFactory.newBinder(ArboCropAdventiceMaster.class);
    protected Binder<ArboAdventiceMaster, ArboAdventiceMaster> arboAdventiceMasterBinder = BinderFactory.newBinder(ArboAdventiceMaster.class);
    protected Binder<ArboCropPestMaster, ArboCropPestMaster> arboCropPestMasterBinder = BinderFactory.newBinder(ArboCropPestMaster.class);
    protected Binder<ArboPestMaster, ArboPestMaster> arboPestMasterBinder = BinderFactory.newBinder(ArboPestMaster.class);
    protected Binder<VitiPestMaster, VitiPestMaster> vitiPestMasterBinder = BinderFactory.newBinder(VitiPestMaster.class);

    protected void addReportDetails(ReportGrowingSystem reportGrowingSystem, ReportGrowingSystemCollections highlights) {
        
        ReportGrowingSystem persistedRGS = reportGrowingSystemDao.forTopiaIdEquals(reportGrowingSystem.getTopiaId()).findUnique();

        // cropAdventiceMasters
        boolean isCropAdventiceMaster = highlights.getCropAdventiceMastersBySector().values().stream().anyMatch(Objects::nonNull);
        if (isCropAdventiceMaster) {
            List<CropPestMaster> allCropAdventiceMasters = getCropMasters(reportGrowingSystem.getCropAdventiceMasters(),
                    highlights.getCropAdventiceMastersBySector(), CropPestMaster::getSector);
            Collection<CropPestMaster> cropAdventiceMasters = updateCropPestMasterCollection(persistedRGS.getCropAdventiceMasters(),
                    allCropAdventiceMasters);
            persistedRGS.clearCropAdventiceMasters();
            persistedRGS.addAllCropAdventiceMasters(cropAdventiceMasters);
        }
    
        // cropDiseaseMasters
        boolean isCropDiseaseMaster = highlights.getCropDiseaseMastersBySector().values().stream().anyMatch(Objects::nonNull);
        if (isCropDiseaseMaster) {
            List<CropPestMaster> allCropDiseaseMasters = getCropMasters(reportGrowingSystem.getCropDiseaseMasters(),
                    highlights.getCropDiseaseMastersBySector(), CropPestMaster::getSector);
            Collection<CropPestMaster> cropDiseaseMasters = updateCropPestMasterCollection(persistedRGS.getCropDiseaseMasters(), allCropDiseaseMasters);
            persistedRGS.clearCropDiseaseMasters();
            persistedRGS.addAllCropDiseaseMasters(cropDiseaseMasters);
        }
        
        // cropPestMasters
        boolean isCropPestMaster = highlights.getCropPestMastersBySector().values().stream().anyMatch(Objects::nonNull);
        if (isCropPestMaster) {
            List<CropPestMaster> allCropPestMasters = getCropMasters(reportGrowingSystem.getCropPestMasters(),
                    highlights.getCropPestMastersBySector(), CropPestMaster::getSector);
            Collection<CropPestMaster> cropPestMasters = updateCropPestMasterCollection(persistedRGS.getCropPestMasters(), allCropPestMasters);
            persistedRGS.clearCropPestMasters();
            persistedRGS.addAllCropPestMasters(cropPestMasters);
        }

        // verse masters
        boolean isVerseMaster = highlights.getVerseMastersBySector().values().stream().anyMatch(Objects::nonNull);
        if (isVerseMaster) {
            List<VerseMaster> allCropVerseMasters = getCropMasters(reportGrowingSystem.getVerseMasters(),
                    highlights.getVerseMastersBySector(), VerseMaster::getSector);
            Collection<VerseMaster> cropVerseMasters = TopiaUtils.updateCollection(verseMasterBinder, persistedRGS.getVerseMasters(), allCropVerseMasters);
            persistedRGS.clearVerseMasters();
            persistedRGS.addAllVerseMasters(cropVerseMasters);
        }

        // foodMasters
        boolean isFoodMaster = highlights.getFoodMastersBySector().values().stream().anyMatch(Objects::nonNull);
        if (isFoodMaster) {
            List<FoodMaster> allCropFoodMasters = getCropMasters(reportGrowingSystem.getFoodMasters(),
                    highlights.getFoodMastersBySector(), FoodMaster::getSector);
            Collection<FoodMaster> cropFoodMasters = TopiaUtils.updateCollection(foodMasterBinder, persistedRGS.getFoodMasters(), allCropFoodMasters);
            persistedRGS.clearFoodMasters();
            persistedRGS.addAllFoodMasters(cropFoodMasters);
        }

        // yieldLosses
        boolean isYieldLosses = highlights.getYieldLossesBySector().values().stream().anyMatch(Objects::nonNull);
        if (isYieldLosses) {
            List<YieldLoss> allCropYieldLosses = getCropMasters(
                    reportGrowingSystem.getYieldLosses(),
                    highlights.getYieldLossesBySector(),
                    YieldLoss::getSector);
            
            Collection<YieldLoss> cropYieldLosses = TopiaUtils.updateCollection(yieldLossBinder, persistedRGS.getYieldLosses(), allCropYieldLosses);
            persistedRGS.clearYieldLosses();
            persistedRGS.addAllYieldLosses(cropYieldLosses);
        }

        if (highlights.getYieldInfos() != null) {
            Collection<YieldInfo> yieldInfos = highlights.getYieldInfos().stream()
                    .filter(info -> StringUtils.isNotBlank(info.getComment()))
                    .collect(Collectors.toList());
            yieldInfos = TopiaUtils.updateCollection(yieldInfoBinder, persistedRGS.getYieldInfos(), yieldInfos);
            persistedRGS.clearYieldInfos();
            persistedRGS.addAllYieldInfos(yieldInfos);
        }

        // arboCropAdventiceMasters
        if (highlights.getArboCropAdventiceMasters() != null) {
            Collection<ArboCropAdventiceMaster> arboCropAdventiceMasters = updateArboCropAdventiceMasterCollection(persistedRGS.getArboCropAdventiceMasters(), highlights.getArboCropAdventiceMasters());
            persistedRGS.clearArboCropAdventiceMasters();
            persistedRGS.addAllArboCropAdventiceMasters(arboCropAdventiceMasters);
        }

        // arboCropDiseaseMasters
        if (highlights.getArboCropDiseaseMasters() != null) {
            Collection<ArboCropPestMaster> arboCropDiseaseMasters = updateArboCropPestMasterCollection(persistedRGS.getArboCropDiseaseMasters(), highlights.getArboCropDiseaseMasters());
            persistedRGS.clearArboCropDiseaseMasters();
            persistedRGS.addAllArboCropDiseaseMasters(arboCropDiseaseMasters);
        }

        // arboCropPestMasters
        if (highlights.getArboCropPestMasters() != null) {
            Collection<ArboCropPestMaster> arboCropPestMasters = updateArboCropPestMasterCollection(persistedRGS.getArboCropPestMasters(), highlights.getArboCropPestMasters());
            persistedRGS.clearArboCropPestMasters();
            persistedRGS.addAllArboCropPestMasters(arboCropPestMasters);
        }

        // vitiDiseaseMasters
        if (highlights.getVitiDiseaseMasters() != null) {
            Collection<VitiPestMaster> newVitiDiseaseMasters = TopiaUtils.updateCollection(vitiPestMasterBinder,
                    reportGrowingSystem.getVitiDiseaseMasters(), highlights.getVitiDiseaseMasters());
            reportGrowingSystem.clearVitiDiseaseMasters();
            reportGrowingSystem.addAllVitiDiseaseMasters(newVitiDiseaseMasters);
        }

        // vitiPestMasters
        if (highlights.getVitiPestMasters() != null) {
            Collection<VitiPestMaster> newVitiPestMasters = TopiaUtils.updateCollection(vitiPestMasterBinder,
                    reportGrowingSystem.getVitiPestMasters(), highlights.getVitiPestMasters());
            reportGrowingSystem.clearVitiPestMasters();
            reportGrowingSystem.addAllVitiPestMasters(newVitiPestMasters);
        }
        
    }

    private <T> List<T> getCropMasters(Collection<T> masters, Map<Sector, List<T>> mastersBySector, Function<T, Sector> getSector) {
        List<T> allMasters = new ArrayList<>();
        MultiValuedMap<Sector, T> allPreviousMasters = MultiMapUtils.newListValuedHashMap();
        if (masters != null) {
            for (T master : masters) {
                if (master != null) {
                    allPreviousMasters.put(getSector.apply(master), master);
                }
            }
        }
        List<Sector> sectors = Arrays.stream(Sector.values()).collect(Collectors.toList());
        sectors.add(null);
        for (Sector sector : sectors) {
            List<T> sectorMasters = mastersBySector.get(sector);
            if (sectorMasters != null) {
                // the list was modified by user and need to be updated
                allMasters.addAll(sectorMasters);
            } else {
                allMasters.addAll(allPreviousMasters.get(sector));
            }
        }
        return allMasters;
    }

    protected Collection<CropPestMaster> updateCropPestMasterCollection(Collection<CropPestMaster> origCollection,
                                                                        Collection<CropPestMaster> currentCollection) {
        if (origCollection == null) {
            origCollection = new ArrayList<>();
        }
        currentCollection = CollectionUtils.emptyIfNull(currentCollection);

        Map<String, CropPestMaster> cropPestMasterByIds = origCollection.stream().collect(Collectors.toMap(TopiaEntity::getTopiaId, Function.identity()));
        List<CropPestMaster> newCropPestMasters = new ArrayList<>(origCollection);
        for (CropPestMaster toCropPestMaster : currentCollection) {
            CropPestMaster persistedCropPestMaster = cropPestMasterByIds.remove(toCropPestMaster.getTopiaId());

            if (persistedCropPestMaster == null) {
                newCropPestMasters.add(toCropPestMaster);
            } else {
                cropPestMasterBinder.copyExcluding(
                        toCropPestMaster, persistedCropPestMaster,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        CropPestMaster.PROPERTY_PEST_MASTERS);

                //  CropPestMaster.PROPERTY_PEST_MASTERS
                Collection<PestMaster> newPestMasters = TopiaUtils.updateCollection(pestMasterBinder, persistedCropPestMaster.getPestMasters(), toCropPestMaster.getPestMasters());
                persistedCropPestMaster.clearPestMasters();
                persistedCropPestMaster.addAllPestMasters(newPestMasters);
            }
        }
        newCropPestMasters.removeAll(cropPestMasterByIds.values());
        return newCropPestMasters;
    }

    protected Collection<ArboCropAdventiceMaster> updateArboCropAdventiceMasterCollection(Collection<ArboCropAdventiceMaster> origCollection, Collection<ArboCropAdventiceMaster> currentCollection) {
        origCollection = CollectionUtils.emptyIfNull(origCollection);
        currentCollection = CollectionUtils.emptyIfNull(currentCollection);

        Map<String, ArboCropAdventiceMaster> cropPestMasterByIds = origCollection.stream().collect(Collectors.toMap(TopiaEntity::getTopiaId, Function.identity()));
        List<ArboCropAdventiceMaster> newCropPestMasters = Lists.newArrayList(origCollection);
        for (ArboCropAdventiceMaster toCropPestMaster : currentCollection) {
            ArboCropAdventiceMaster persistedCropPestMaster = cropPestMasterByIds.get(toCropPestMaster.getTopiaId());

            if (persistedCropPestMaster == null) {
                newCropPestMasters.add(toCropPestMaster);
            } else {
                arboCropAdventiceMasterBinder.copyExcluding(
                        toCropPestMaster, persistedCropPestMaster,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        ArboCropAdventiceMaster.PROPERTY_PEST_MASTERS);
                cropPestMasterByIds.remove(toCropPestMaster.getTopiaId());

                //  CropPestMaster.PROPERTY_PEST_MASTERS
                Collection<ArboAdventiceMaster> newPestMasters = TopiaUtils.updateCollection(arboAdventiceMasterBinder, persistedCropPestMaster.getPestMasters(), toCropPestMaster.getPestMasters());
                persistedCropPestMaster.clearPestMasters();
                persistedCropPestMaster.addAllPestMasters(newPestMasters);
            }
        }
        newCropPestMasters.removeAll(cropPestMasterByIds.values());
        return newCropPestMasters;
    }

    protected Collection<ArboCropPestMaster> updateArboCropPestMasterCollection(Collection<ArboCropPestMaster> origCollection, Collection<ArboCropPestMaster> currentCollection) {
        origCollection = origCollection == null ? new ArrayList<>() : origCollection;
        currentCollection = CollectionUtils.emptyIfNull(currentCollection);

        Map<String, ArboCropPestMaster> cropPestMasterByIds = origCollection.stream().collect(Collectors.toMap(TopiaEntity::getTopiaId, Function.identity()));
        List<ArboCropPestMaster> newCropPestMasters = Lists.newArrayList(origCollection);
        for (ArboCropPestMaster toCropPestMaster : currentCollection) {
            ArboCropPestMaster persistedCropPestMaster = cropPestMasterByIds.get(toCropPestMaster.getTopiaId());

            if (persistedCropPestMaster == null) {
                newCropPestMasters.add(toCropPestMaster);
            } else {
                arboCropPestMasterBinder.copyExcluding(
                        toCropPestMaster, persistedCropPestMaster,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        ArboCropPestMaster.PROPERTY_PEST_MASTERS);
                cropPestMasterByIds.remove(toCropPestMaster.getTopiaId());

                //  CropPestMaster.PROPERTY_PEST_MASTERS
                Collection<ArboPestMaster> newPestMasters = TopiaUtils.updateCollection(arboPestMasterBinder, persistedCropPestMaster.getPestMasters(), toCropPestMaster.getPestMasters());
                persistedCropPestMaster.clearPestMasters();
                persistedCropPestMaster.addAllPestMasters(newPestMasters);
            }
        }
        newCropPestMasters.removeAll(cropPestMasterByIds.values());
        return newCropPestMasters;
    }

    protected void validateStrikingFacts(ReportGrowingSystem reportGrowingSystem, ReportGrowingSystemCollections highlights) {
        
        List<CropPestMaster> allCropPestMaster = new ArrayList<>();
        Map<Sector, List<CropPestMaster>> cropPestMasterBySector = highlights.getCropPestMastersBySector();
        for (List<CropPestMaster> cropPestMasters : cropPestMasterBySector.values()) {
            if (cropPestMasters != null) {
                allCropPestMaster.addAll(cropPestMasters);
            }
        }
        
        if (CollectionUtils.isNotEmpty(allCropPestMaster)) {
            for (CropPestMaster cropPestMaster : allCropPestMaster) {
                Preconditions.checkState(CollectionUtils.isNotEmpty(cropPestMaster.getCrops()));
                Preconditions.checkState(CollectionUtils.isNotEmpty(cropPestMaster.getPestMasters()));
                // other fields are validate throw model
            }
        }

        if (CollectionUtils.isNotEmpty(highlights.getArboCropDiseaseMasters())) {
            Preconditions.checkNotNull(reportGrowingSystem.getArboDiseaseQualifier());
        }

        if (CollectionUtils.isNotEmpty(highlights.getArboCropPestMasters())) {
            for (ArboCropPestMaster arboCropPestMaster : highlights.getArboCropPestMasters()) {
                Preconditions.checkState(CollectionUtils.isNotEmpty(arboCropPestMaster.getCrops()));
            }
            Preconditions.checkNotNull(reportGrowingSystem.getArboPestQualifier());
        }

        if(CollectionUtils.isNotEmpty(highlights.getArboCropAdventiceMasters())) {
            for (ArboCropAdventiceMaster arboCropAdventiceMaster : highlights.getArboCropAdventiceMasters()) {
                Preconditions.checkState(CollectionUtils.isNotEmpty(arboCropAdventiceMaster.getCrops()));
            }
        }

        if (CollectionUtils.isNotEmpty(highlights.getVitiDiseaseMasters())) {
            Preconditions.checkNotNull(reportGrowingSystem.getVitiDiseaseChemicalFungicideIFT());
            Preconditions.checkNotNull(reportGrowingSystem.getVitiDiseaseBioControlFungicideIFT());
            Preconditions.checkNotNull(reportGrowingSystem.getVitiDiseaseCopperQuantity());
            Preconditions.checkNotNull(reportGrowingSystem.getVitiDiseaseQualifier());
        }
        if (CollectionUtils.isNotEmpty(highlights.getVitiPestMasters())) {
            Preconditions.checkNotNull(reportGrowingSystem.getVitiPestChemicalPestIFT());
            Preconditions.checkNotNull(reportGrowingSystem.getVitiPestBioControlPestIFT());
            Preconditions.checkNotNull(reportGrowingSystem.getVitiPestQualifier());
        }
    
        final Collection<List<FoodMaster>> foodMasters0 = highlights.getFoodMastersBySector().values()
                .stream()
                .filter(Objects::nonNull)
                .toList();
        foodMasters0.forEach(
                foodMasters -> foodMasters.forEach(
                        foodMaster -> Preconditions.checkState(CollectionUtils.isNotEmpty(foodMaster.getCrops()))
        ));

        final Collection<List<VerseMaster>> verseMaster0 = highlights.getVerseMastersBySector().values()
                .stream()
                .filter(Objects::nonNull)
                .toList();
        verseMaster0.forEach(
                verseMasters -> verseMasters.forEach(
                        verseMaster -> Preconditions.checkState(CollectionUtils.isNotEmpty(verseMaster.getCrops()))));

        boolean isDephyFerme = isDephyFerme(reportGrowingSystem);

        validateYealdLosses(highlights, isDephyFerme, reportGrowingSystem.getVitiYieldObjective(), reportGrowingSystem.getVitiLossCause1(), reportGrowingSystem.getSectors());

    }

    private static void validateYealdLosses(
            ReportGrowingSystemCollections highlights,
            boolean isDephyFerme,
            YieldObjective vitiYieldObjective,
            YieldLossCause vitiLossCause1,
            Collection<Sector> sectors) {

        if (sectors.contains(Sector.VITICULTURE)) {
            if (isDephyFerme){
                Preconditions.checkState(
                        vitiYieldObjective != null,
                        "Objectif de rendement. non renseignée !");
    
                boolean causeRequired = YieldObjective.MORE_95 != vitiYieldObjective;
                if (causeRequired) {
                    Preconditions.checkState(vitiLossCause1 != null,
                            "cause. non renseignée !");
                }
            }
        }
        if (highlights.getYieldLossesBySector() == null) return;// the user has not gone to "Faits marquants" tab

        Map<Sector, List<YieldLoss>> yieldLossesBySector = highlights.getYieldLossesBySector();
        for (Map.Entry<Sector, List<YieldLoss>> yieldLossBysectors : yieldLossesBySector.entrySet()) {
            Sector sector = yieldLossBysectors.getKey();
            List<YieldLoss> yieldLossForSector = ListUtils.emptyIfNull(yieldLossBysectors.getValue());
            if (isDephyFerme) {
                Preconditions.checkState(!yieldLossForSector.isEmpty(),
                        "Rendement et qualité non renseignés !");
            }
            
            for (YieldLoss yieldLoss : yieldLossForSector) {

                if(!Sector.VITICULTURE.equals(sector)) {
                    Preconditions.checkState(CollectionUtils.isNotEmpty(yieldLoss.getCrops()),
                            "Culture non renseignée !");
                }

                if (isDephyFerme){
                    Preconditions.checkState(
                            yieldLoss.getYieldObjective() != null,
                            "Objectif de rendement. non renseignée !");

                    boolean causeRequired = YieldObjective.MORE_95 != yieldLoss.getYieldObjective();
                    if (causeRequired) {
                        Preconditions.checkState(yieldLoss.getCause1() != null,
                                "cause. non renseignée !");
                    }
                }
            }
        }
    }

    private static boolean isDephyFerme(ReportGrowingSystem reportGrowingSystem) {
        TypeDEPHY type = reportGrowingSystem.getGrowingSystem().getGrowingPlan().getType();
        boolean isDephyFerme = TypeDEPHY.DEPHY_FERME.equals(type);
        return isDephyFerme;
    }

    @Override
    public ReportGrowingSystem createOrUpdateReportGrowingSystem(ReportGrowingSystem reportGrowingSystem,
                                                                 String growingSystemId,
                                                                 String reportRegionalId,
                                                                 ReportGrowingSystemCollections highlights,
                                                                 boolean createManagementMode) {

        boolean persisted = reportGrowingSystem.isPersisted();
        String id = persisted ? reportGrowingSystem.getTopiaId() : "NEW REPORT GROWING SYSTEM";

        // security
        authorizationService.checkCreateOrUpdateReportGrowingSystem(reportGrowingSystem.getTopiaId());

        if (!persisted) {
            GrowingSystem gs = growingSystemDao.forTopiaIdEquals(growingSystemId).findUnique();
            reportGrowingSystem.setGrowingSystem(gs);

            if (StringUtils.isBlank(reportGrowingSystem.getName())) {
                int campaign = gs.getGrowingPlan().getDomain().getCampaign();
                reportGrowingSystem.setName(getDefaultReportGrowingSystemName(gs, campaign));
            }

            reportGrowingSystem.setCode(UUID.randomUUID().toString());
        }

        ReportRegional reportRegional = StringUtils.isNotBlank(reportRegionalId) ? reportRegionalDao.forTopiaIdEquals(reportRegionalId).findUnique() : null;
        reportGrowingSystem.setReportRegional(reportRegional);
    
        final GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
        Preconditions.checkNotNull(growingSystem, String.format("ReportGrowingSystem: no growing system selected for reportGrowingSystem with id '%s'", id));
        Preconditions.checkNotNull(StringUtils.trimToNull(reportGrowingSystem.getName()),  String.format("ReportGrowingSystem: no given name for reportGrowingSystem with id '%s'", id));
        Preconditions.checkNotNull(StringUtils.trimToNull(reportGrowingSystem.getAuthor()),  String.format("ReportGrowingSystem: author is required for reportGrowingSystem with id '%s'", id));
        Preconditions.checkState(CollectionUtils.isNotEmpty(reportGrowingSystem.getSectors()), String.format("ReportGrowingSystem: at least 1 sector is required for reportGrowingSystem with id '%s'", id));
        
        if (TypeDEPHY.DEPHY_EXPE != growingSystem.getGrowingPlan().getType()) {
            Preconditions.checkNotNull(reportRegional, String.format("ReportGrowingSystem: No regional report selected for reportGrowingSystem with id '%s'", id));
        }
    
        validateStrikingFacts(reportGrowingSystem, highlights);

        removeNoMoreUsedSectorsContents(reportGrowingSystem, highlights);

        ReportGrowingSystem createOrUpdatedReport;
        if (persisted) {
            createOrUpdatedReport = reportGrowingSystemDao.update(reportGrowingSystem);
        } else {
            createOrUpdatedReport = reportGrowingSystemDao.create(reportGrowingSystem);
        }

        addReportDetails(createOrUpdatedReport, highlights);

        if (createManagementMode) {
            managementModeService.createManagementModeFromReportGrowingSystem(createOrUpdatedReport);
        }

        getTransaction().commit();

        return createOrUpdatedReport;
    }

    protected void removeNoMoreUsedSectorsContents(ReportGrowingSystem reportGrowingSystem, ReportGrowingSystemCollections highlights) {
        Collection<Sector> sectors = reportGrowingSystem.getSectors();
        List<Sector> notUsedSectors = Lists.newArrayList(Sector.values());
        notUsedSectors.removeAll(sectors);
        
        if (notUsedSectors.remove(Sector.ARBORICULTURE)) {
            reportGrowingSystem.setArboChemicalFungicideIFT(null);
            reportGrowingSystem.setArboBioControlFungicideIFT(null);
            reportGrowingSystem.setArboBioControlPestIFT(null);
            reportGrowingSystem.setArboChemicalPestIFT(null);
            reportGrowingSystem.setArboDiseaseQualifier(null);
            highlights.setArboAdventiceMasters(new ArrayList<>());
            highlights.setArboDiseaseMasters(new ArrayList<>());
            highlights.setArboPestMasters(new ArrayList<>());
            highlights.addArboYieldLosses(new ArrayList<>());
        }
        if (notUsedSectors.remove(Sector.VITICULTURE )) {
            reportGrowingSystem.setVitiAdventicePressureFarmerComment(null);
            reportGrowingSystem.setVitiAdventicePressureScale(null);
            reportGrowingSystem.setVitiAdventiceQualifier(null);
            reportGrowingSystem.setVitiAdventiceResultFarmerComment(null);
            reportGrowingSystem.setVitiAdventicePressureScale(null);
            reportGrowingSystem.setVitiDiseaseBioControlFungicideIFT(null);
            reportGrowingSystem.setVitiDiseaseChemicalFungicideIFT(null);
            reportGrowingSystem.setVitiDiseaseCopperQuantity(null);
            reportGrowingSystem.setVitiDiseaseQualifier(null);
            reportGrowingSystem.setVitiPestChemicalPestIFT(null);
            reportGrowingSystem.setVitiPestBioControlPestIFT(null);
            reportGrowingSystem.setVitiPestQualifier(null);
            reportGrowingSystem.setVitiYieldObjective(null);
            reportGrowingSystem.setVitiLossCause1(null);
            reportGrowingSystem.setVitiLossCause2(null);
            reportGrowingSystem.setVitiLossCause3(null);
            reportGrowingSystem.setVitiYieldQuality(null);
            highlights.setVitiPestMasters(new ArrayList<>());
            highlights.setVitiDiseaseMasters(new ArrayList<>());
            highlights.addVitiFoodMasters(new ArrayList<>());
        }
        if (notUsedSectors.contains(Sector.POLYCULTURE_ELEVAGE) &&
                notUsedSectors.contains(Sector.GRANDES_CULTURES)) {
            highlights.addDephyExpeVerseMasters(Sector.POLYCULTURE_ELEVAGE, new ArrayList<>());
            highlights.addDephyExpeVerseMasters(Sector.GRANDES_CULTURES, new ArrayList<>());
        }
    
        // NOT DEPHY
        if (notUsedSectors.contains(Sector.POLYCULTURE_ELEVAGE) &&
                notUsedSectors.contains(Sector.GRANDES_CULTURES) &&
                notUsedSectors.contains(Sector.MARAICHAGE) &&
                notUsedSectors.contains(Sector.HORTICULTURE) &&
                notUsedSectors.contains(Sector.CULTURES_TROPICALES)) {
        
            highlights.addNoneDephyExpeCropDiseaseMasters(new ArrayList<>());
            highlights.addNoneDephyExpeCropAdventiceMasters(new ArrayList<>());
            highlights.addNoneDephyExpeCropPestMasters(new ArrayList<>());
            highlights.addNoneDephyExpeYieldLosses(new ArrayList<>());
            highlights.addNoneDephyExpeFoodMasters(new ArrayList<>());
        }
    
        // DEPHY
        for (Sector notUsedSector : notUsedSectors) {
            highlights.addDephyExpeCropDiseaseMasters(notUsedSector, new ArrayList<>());
            highlights.addDephyExpeCropAdventiceMasters(notUsedSector, new ArrayList<>());
            highlights.addDephyExpeCropPestMasters(notUsedSector, new ArrayList<>());
            highlights.addDephyExpeYieldLosses(notUsedSector, new ArrayList<>());
            highlights.addDephyExpeFoodMasters(notUsedSector, new ArrayList<>());
        }
    
    }

    protected String getDefaultReportGrowingSystemName(GrowingSystem growingSystem, int campaign) {
        return "BC-SdC_" + campaign + "_" + growingSystem.getName();
    }

    @Override
    public Map<String, String> getDomainsNameToId(ReportFilter rFilter) {
        Preconditions.checkState(
                rFilter.getCampaign() != null &&
                CommonService.getInstance().isCampaignValid(rFilter.getCampaign()),
                "Not Valid Campaign:" + rFilter.getCampaign());

        DomainFilter dFilter = new DomainFilter();
        dFilter.setActive(true);
        dFilter.setCampaign(rFilter.getCampaign());
        dFilter.setNavigationContext(rFilter.getNavigationContext());
        dFilter.setAllPageSize();
        PaginationResult<Domain> filteredDomains = domainDao.getFilteredDomains(dFilter, getSecurityContext());

        Map<String, String> result = new LinkedHashMap<>();
        for (Domain filteredDomain : filteredDomains.getElements()) {
            // not sure there name is unique for campaign
            result.put(filteredDomain.getTopiaId(), filteredDomain.getName());
        }
        return result;
    }

    @Override
    public List<GrowingSystemForReport> getGrowingSystemNameToId(ReportFilter rFilter) {
        Preconditions.checkState(
                rFilter.getCampaign() != null &&
                        CommonService.getInstance().isCampaignValid(rFilter.getCampaign()),
                "Not Valid Campaign:" + rFilter.getCampaign());
        List<GrowingSystemForReport> result;
        try (Stream<GrowingSystem> stream = growingSystemDao.getFilteredGrowingSystemsForReport(rFilter, getSecurityContext())) {
            result = stream
                    .map(this::toGrowingSystemForReport)
                    .collect(Collectors.toList());
        }
        return result;
    }

    @Override
    public Map<String, String> getReportRegionalNameToId(ReportFilter rFilter) {
        Preconditions.checkState(
                rFilter.getCampaign() != null &&
                        CommonService.getInstance().isCampaignValid(rFilter.getCampaign()),
                "Not Valid Campaign:" + rFilter.getCampaign());
        return reportRegionalDao.getFilteredReportRegionalForReportGrowingSystem(rFilter, getSecurityContext());
    }

    @Override
    public Set<SectionType> getReportGrowingSystemManagementModeSections(List<String> reportGrowingSystemIds) {
        Set<String> growingSystemIds = reportGrowingSystemDao.forTopiaIdIn(reportGrowingSystemIds).findAll().stream()
                .map(r -> r.getGrowingSystem().getTopiaId())
                .collect(Collectors.toSet());
        Collection<ManagementMode> managementModes = managementModeService.getManagementModeForGrowingSystemIds(growingSystemIds);

        Set<SectionType> sectionTypes = new HashSet<>();
        for (ManagementMode managementMode : managementModes) {
            for (Section section : managementMode.getSections()) {
                sectionTypes.add(section.getSectionType());
            }
        }
        return sectionTypes;
    }

    @Override
    public Map<Sector, Collection<ReportGrowingSystemSection>> getReportGrowingSystemSections(List<String> reportGrowingSystemIds) {
        Map<Sector, Collection<ReportGrowingSystemSection>> sections = new LinkedHashMap<>();
        for (Sector sector : Sector.values()) {
            sections.put(sector, new HashSet<>());
        }
        sections.put(null, new HashSet<>());

        List<ReportGrowingSystem> reportGrowingSystems = reportGrowingSystemDao.forTopiaIdIn(reportGrowingSystemIds).findAll();
        for (ReportGrowingSystem reportGrowingSystem : reportGrowingSystems) {
            Collection<Sector> sectors = reportGrowingSystem.getSectors();

            if (CollectionUtils.isNotEmpty(reportGrowingSystem.getArboCropDiseaseMasters())) {
                sections.get(Sector.ARBORICULTURE).add(ReportGrowingSystemSection.MASTER_DISEASE);
            }
            if (CollectionUtils.isNotEmpty(reportGrowingSystem.getArboCropPestMasters())) {
                sections.get(Sector.ARBORICULTURE).add(ReportGrowingSystemSection.MASTER_PEST);
            }
            if (CollectionUtils.isNotEmpty(reportGrowingSystem.getArboCropAdventiceMasters())) {
                sections.get(Sector.ARBORICULTURE).add(ReportGrowingSystemSection.MASTER_ADVENTICE);
            }

            addSections(sections, reportGrowingSystem.getCropDiseaseMasters(), ReportGrowingSystemSection.MASTER_DISEASE);
            addSections(sections, reportGrowingSystem.getCropPestMasters(), ReportGrowingSystemSection.MASTER_PEST);
            addSections(sections, reportGrowingSystem.getCropAdventiceMasters(), ReportGrowingSystemSection.MASTER_ADVENTICE);
            Collection<VerseMaster> verseMasters = reportGrowingSystem.getVerseMasters();
            if (CollectionUtils.isNotEmpty(verseMasters)) {
                verseMasters.stream()
                        .map(VerseMaster::getSector)
                        .forEach(sector -> sections.get(sector).add(ReportGrowingSystemSection.MASTER_VERSE));
            }
            Collection<FoodMaster> foodMasters = reportGrowingSystem.getFoodMasters();
            if (CollectionUtils.isNotEmpty(foodMasters)) {
                foodMasters.stream()
                        .map(FoodMaster::getSector)
                        .forEach(sector -> sections.get(sector).add(ReportGrowingSystemSection.MASTER_FOOD));
            }
            Collection<YieldLoss> yieldLosses = reportGrowingSystem.getYieldLosses();
            if (CollectionUtils.isNotEmpty(yieldLosses)) {
                yieldLosses.stream()
                        .map(YieldLoss::getSector)
                        .forEach(sector -> sections.get(sector).add(ReportGrowingSystemSection.MASTER_YIELD));
            }
            Collection<YieldInfo> yieldInfos = reportGrowingSystem.getYieldInfos();
            if (CollectionUtils.isNotEmpty(yieldInfos)) {
                yieldInfos.stream()
                        .map(YieldInfo::getSector)
                        .forEach(sector -> sections.get(sector).add(ReportGrowingSystemSection.MASTER_YIELD));
            }

            if (reportGrowingSystem.getArboBioControlFungicideIFT() != null) {
                sections.get(Sector.ARBORICULTURE).add(ReportGrowingSystemSection.MASTER_DISEASE);
            }
            if (CollectionUtils.isNotEmpty(reportGrowingSystem.getVitiDiseaseMasters()) ||
                    reportGrowingSystem.getVitiDiseaseChemicalFungicideIFT() != null ||
                    reportGrowingSystem.getVitiDiseaseCopperQuantity() != null ||
                    reportGrowingSystem.getVitiDiseaseQualifier() != null) {
                sections.get(Sector.VITICULTURE).add(ReportGrowingSystemSection.MASTER_DISEASE);
            }
            if (CollectionUtils.isNotEmpty(reportGrowingSystem.getVitiPestMasters()) ||
                    reportGrowingSystem.getVitiPestChemicalPestIFT() != null ||
                    reportGrowingSystem.getVitiPestBioControlPestIFT() != null ||
                    reportGrowingSystem.getVitiPestQualifier() != null) {
                sections.get(Sector.VITICULTURE).add(ReportGrowingSystemSection.MASTER_PEST);
            }
            if (sectors.contains(Sector.VITICULTURE) &&
                    (reportGrowingSystem.getVitiAdventicePressureScale() != null ||
                            StringUtils.isNotBlank(reportGrowingSystem.getVitiAdventicePressureFarmerComment()) ||
                            reportGrowingSystem.getVitiAdventiceQualifier() != null ||
                            reportGrowingSystem.getVitiHerboTreatmentChemical() != null ||
                            reportGrowingSystem.getVitiHerboTreatmentBioControl() != null ||
                            reportGrowingSystem.getVitiHerboTreatmentChemicalIFT() != null ||
                            reportGrowingSystem.getVitiHerboTreatmentBioControlIFT() != null ||
                            reportGrowingSystem.getVitiSuckeringChemical() != null ||
                            reportGrowingSystem.getVitiSuckeringBioControl() != null ||
                            reportGrowingSystem.getVitiSuckeringChemicalIFT() != null ||
                            reportGrowingSystem.getVitiSuckeringBioControlIFT() != null)
            ) {
                sections.get(Sector.VITICULTURE).add(ReportGrowingSystemSection.MASTER_ADVENTICE);
            }
            if (sectors.contains(Sector.VITICULTURE)
                    && (reportGrowingSystem.getVitiYieldObjective() != null ||
                    reportGrowingSystem.getVitiLossCause1() != null ||
                    reportGrowingSystem.getVitiLossCause2() != null ||
                    reportGrowingSystem.getVitiLossCause3() != null ||
                    StringUtils.isNotEmpty(reportGrowingSystem.getVitiYieldQuality()))
                    ) {
                sections.get(Sector.VITICULTURE).add(ReportGrowingSystemSection.MASTER_YIELD);
            }
        }

        return sections;
    }

    private void addSections(Map<Sector, Collection<ReportGrowingSystemSection>> sections,
                             Collection<CropPestMaster> cropPestMasters,
                             ReportGrowingSystemSection section) {
        if (CollectionUtils.isNotEmpty(cropPestMasters)) {
            cropPestMasters.stream()
                    .map(CropPestMaster::getSector)
                    .forEach(sector -> sections.get(sector).add(section));
        }
    }

    @Override
    public Set<String> getNetworkIdsUsed(ReportRegional reportRegional) {

        // BDC Régional -> Network
        //   Network > listOf(SDC)
        // BDC SDC -> SDC -> BDC Régio

        List<ReportGrowingSystem> reportGrowingSystems = reportGrowingSystemDao.forReportRegionalEquals(reportRegional).findAll();
        Set<GrowingSystem> relatedGrowingSystems = new HashSet<>();

        for (ReportGrowingSystem reportGrowingSytem : reportGrowingSystems) {
            relatedGrowingSystems.add(reportGrowingSytem.getGrowingSystem());
        }

        Set<Network> usedNetworks = new HashSet<>();
        for (GrowingSystem growingSystem : relatedGrowingSystems) {
            usedNetworks.addAll(growingSystem.getNetworks());
        }

        return usedNetworks.stream().map(Network::getTopiaId).collect(Collectors.toSet());
    }

    @Override
    public String getDefaultArboAdventiceIdForReport() {
        return getConfig().getDefaultArboAdventiceIdForReport();
    }

    @Override
    public Set<IftEstimationMethod> getIftEstimationMethods(ReportGrowingSystem reportGrowingSystem) {
        GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
        LocalDateTime reportGrowingSystemCreationDate =
                AgrosystDateUtils.toLocalDateTime(reportGrowingSystem.getTopiaCreateDate()).toJavaUtil().orElseThrow();
        return getIftEstimationMethods(growingSystem, reportGrowingSystemCreationDate);
    }

    private Set<IftEstimationMethod> getIftEstimationMethods(GrowingSystem growingSystem, LocalDateTime reportGrowingSystemCreationDate) {
        Set<IftEstimationMethod> iftEstimationMethods;
        if (isFilteredIftEstimations(growingSystem.getGrowingPlan().getType(), reportGrowingSystemCreationDate)) {
            iftEstimationMethods = ImmutableSet.of(IftEstimationMethod.COMPUTED_2016);
        } else {
            iftEstimationMethods = ImmutableSet.copyOf(IftEstimationMethod.values());
        }
        return iftEstimationMethods;
    }
    
    /**
     * a filter on IftEstimations is applied if DEPHY_FERME and After 01/11/2022
     */
    @Override
    public boolean isFilteredIftEstimations(TypeDEPHY growingSystemDephyType, LocalDateTime reportGrowingSystemCreationDate) {
        boolean result = false;
        if (growingSystemDephyType == TypeDEPHY.DEPHY_FERME) {
            LocalDate after;
            if (context.getConfig().isDemoModeEnabled()) {
                after = LocalDate.of(2022, 10, 10);
            } else {
                after = LocalDate.of(2022, 11, 1);
            }
            result = reportGrowingSystemCreationDate.toLocalDate().isAfter(after);
        }
        return result;
    }
    

    private GrowingSystemForReport toGrowingSystemForReport(GrowingSystem growingSystem) {
        final TypeDEPHY typeDEPHY = growingSystem.getGrowingPlan().getType();
        final String translatedTypeDephy = t(typeDEPHY.getClass().getName() + "." + typeDEPHY.name());
        final String nameAndDephy = growingSystem.getName() + " (" + translatedTypeDephy + ")";
        final Set<IftEstimationMethod> iftEstimationMethods = getIftEstimationMethods(growingSystem, context.getCurrentTime());
        return new GrowingSystemForReport(
                growingSystem.getTopiaId(),
                nameAndDephy,
                typeDEPHY,
                iftEstimationMethods
        );
    }
}
