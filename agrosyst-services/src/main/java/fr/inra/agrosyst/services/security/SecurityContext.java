package fr.inra.agrosyst.services.security;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.services.ServiceFactory;
import fr.inra.agrosyst.api.services.security.AuthorizationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;

import java.util.Locale;
import java.util.Optional;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class SecurityContext {

    protected final ServiceFactory serviceFactory;

    protected final AuthenticatedUser authenticatedUser;
    protected String doAsUserId;

    // transient because can be recomputed at any time
    protected transient AuthorizationService authorizationService;
    protected transient Boolean admin;
    protected transient String userId;

    public SecurityContext(ServiceFactory serviceFactory, AuthenticatedUser authenticatedUser) {
        this.serviceFactory = serviceFactory;
        this.authenticatedUser = authenticatedUser;
    }

    public SecurityContext(ServiceFactory serviceFactory, AuthenticatedUser authenticatedUser, String doAsUserId) {
        this(serviceFactory, authenticatedUser);
        this.doAsUserId = doAsUserId;
    }

    protected AuthorizationService getAuthorizationService() {
        if (authorizationService == null) {
            authorizationService = serviceFactory.newService(AuthorizationService.class);
        }
        return authorizationService;
    }

    public boolean isAdmin() {
        if (admin == null) {
            admin = getAuthorizationService().isAdminFromUserId(authenticatedUser.getTopiaId());
//            admin = authenticatedUser.isAdmin();
        }
        return admin;
    }

    public String getUserId() {
        if (userId == null) {
            if (Strings.isNullOrEmpty(doAsUserId)) {
                userId = getAuthorizationService().checkComputedPermissionsFromUserId(authenticatedUser.getTopiaId());
            } else {
                userId = getAuthorizationService().checkComputedPermissionsFromUserId(authenticatedUser.getTopiaId(), doAsUserId);
            }
        }
        return userId;
    }

    public String getAuthenticatedUserId() {
        String result = authenticatedUser.getTopiaId();
        return result;
    }

    public AuthenticatedUser getAuthenticatedUser() {
        return authenticatedUser;
    }

    public Language getLanguage() {
        return Optional.ofNullable(authenticatedUser)
                .map(AuthenticatedUser::getLanguage)
                .orElse(Language.FRENCH);
    }

    public Locale getLocale() {
        return Optional.ofNullable(authenticatedUser)
                .map(AuthenticatedUser::getLocale)
                .orElse(Language.FRENCH.getLocale());
    }
}
