package fr.inra.agrosyst.services.internal;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import net.timewalker.ffmq4.FFMQConstants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.jms.JMSException;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Hashtable;
import java.util.UUID;
import java.util.function.Consumer;

public class JmsHelper {

    private static final Log log = LogFactory.getLog(JmsHelper.class);

    private static JmsHelper INSTANCE;

    protected static final String CLIENT_ID = UUID.randomUUID().toString();

    protected final Context jmsContext;

    protected TopicConnection topicConnection;

    private JmsHelper(String jmsUrl) throws NamingException {
        Preconditions.checkArgument(jmsUrl != null, "L'URL doit être définie");

        Hashtable<String, Object> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, FFMQConstants.JNDI_CONTEXT_FACTORY);
        env.put(Context.PROVIDER_URL, jmsUrl);
        jmsContext = new InitialContext(env);
    }

    public static JmsHelper getInstance(String jmsUrl) {
        if (INSTANCE == null) {
            try {
                INSTANCE = new JmsHelper(jmsUrl);
            } catch (NamingException eee) {
                log.error("Unable to start JMS", eee);
                throw new AgrosystTechnicalException("Unable to start JMS", eee);
            }
        }
        return INSTANCE;
    }

    public void shutdown() throws JMSException {
        if (this.topicConnection != null) {
            this.topicConnection.close();
        }
    }

    protected TopicConnection startTopicConnection() throws NamingException, JMSException {
        TopicConnectionFactory f = (TopicConnectionFactory) jmsContext.lookup(FFMQConstants.JNDI_TOPIC_CONNECTION_FACTORY_NAME);
        TopicConnection con = f.createTopicConnection();
        con.setClientID(CLIENT_ID);
        con.setExceptionListener(exception -> {
            log.error("Erreur avec la connection", exception);
            connectionOnError(con);
        });
        con.start();
        return con;
    }

    protected void connectionOnError(TopicConnection conn) {
        if (topicConnection == conn) {
            topicConnection = null;
        }
    }

    public boolean isConnectionAvailable() {
        return topicConnection != null;
    }

    protected TopicConnection getTopicConnection() throws NamingException, JMSException {
        if (topicConnection == null) {
            topicConnection = startTopicConnection();
        }
        return topicConnection;
    }

    public void sendMessage(String topicName, String message) {

        String jndiTopicName = String.format("topic/%s", topicName);
        try {
            // get the Topic object
            Topic t = (Topic) jmsContext.lookup(jndiTopicName);

            // create topic session
            TopicSession ses = getTopicConnection().createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
            TopicPublisher publisher = ses.createPublisher(t);

            // create TextMessage object
            TextMessage msg = ses.createTextMessage();
            msg.setText(message);

            if (log.isTraceEnabled()) {
                log.trace(String.format("Émission d'un message sur le topic '%s': %s", topicName, message));
            }

            // write message
            publisher.publish(msg);

        } catch (NamingException | JMSException ne) {
            log.error("Erreur d'envoi sur le topic JMS", ne);
        }
    }

    public void watchMessage(String topicName, Consumer<String> consumer) {

        String jndiTopicName = String.format("topic/%s", topicName);
        try {
            //2) create topic session
            TopicSession ses = getTopicConnection().createTopicSession(false, Session.AUTO_ACKNOWLEDGE);

            //3) get the Topic object
            Topic t = (Topic) jmsContext.lookup(jndiTopicName);

            //4)create TopicSubscriber
            TopicSubscriber subscriber = ses.createSubscriber(t);

            //5) create listener object
            MessageListener messageListener = message -> {
                try {
                    TextMessage msg = (TextMessage) message;
                    String text = msg.getText();

                    if (log.isTraceEnabled()) {
                        log.trace(String.format("Réception d'un message sur le topic '%s': %s", topicName, text));
                    }

                    try {
                        consumer.accept(text);
                    } catch (Exception eee) {
                        log.error("Consumer failed to handle message", eee);
                    }

                } catch (JMSException e) {
                    log.error("Cannot read text from message", e);
                }
            };

            //6) register the listener object with subscriber
            subscriber.setMessageListener(messageListener);

        } catch (NamingException | JMSException ne) {
            log.error("Erreur d'accès au topic JMS", ne);
        }
    }

}
