/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.services.effective.export;

import com.google.common.collect.ImmutableList;
import fr.inra.agrosyst.services.export.ExportContext;

import java.util.LinkedList;
import java.util.List;

public class EffectiveXlsContext extends ExportContext {

    protected final List<EffectiveCropCycleExportMetadata.ItkBean> itks;
    protected final List<EffectiveCropCycleExportMetadata.CropCycleBean> cropCycles;

    public EffectiveXlsContext() {
        super();
        this.cropCycles = new LinkedList<>();
        this.itks = new LinkedList<>();
    }

    public void addCropCycleNG(EffectiveCropCycleExportMetadata.CropCycleBean input) {
        this.cropCycles.add(input);
    }

    public List<EffectiveCropCycleExportMetadata.CropCycleBean> getCropCycles() {
        return ImmutableList.copyOf(cropCycles);
    }

    public void addItk(EffectiveCropCycleExportMetadata.ItkBean input) {
        this.itks.add(input);
    }

    public List<EffectiveCropCycleExportMetadata.ItkBean> getItks() {
        return ImmutableList.copyOf(itks);
    }

}
