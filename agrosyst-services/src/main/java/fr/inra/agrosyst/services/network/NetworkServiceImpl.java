package fr.inra.agrosyst.services.network;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.NetworkManager;
import fr.inra.agrosyst.api.entities.NetworkManagerTopiaDao;
import fr.inra.agrosyst.api.entities.NetworkTopiaDao;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.entities.security.AgrosystUserImpl;
import fr.inra.agrosyst.api.entities.security.AgrosystUserTopiaDao;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.entities.security.UserRole;
import fr.inra.agrosyst.api.entities.security.UserRoleTopiaDao;
import fr.inra.agrosyst.api.services.common.GrowingSystemsIndicator;
import fr.inra.agrosyst.api.services.network.NetworkConnectionDto;
import fr.inra.agrosyst.api.services.network.NetworkFilter;
import fr.inra.agrosyst.api.services.network.NetworkGraph;
import fr.inra.agrosyst.api.services.network.NetworkIndicators;
import fr.inra.agrosyst.api.services.network.NetworkManagerDto;
import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.api.services.network.SmallNetworkDto;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.users.UserDto;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.CacheDiscriminator;
import fr.inra.agrosyst.services.common.CacheService;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.pagination.PaginationResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author cosse
 */
@Setter
public class NetworkServiceImpl extends AbstractAgrosystService implements NetworkService {

    private static final Log LOGGER = LogFactory.getLog(NetworkServiceImpl.class);

    protected static final Comparator<SmallNetworkDto> ALPHABETICAL_NETWORK_COMPARATOR = (o1, o2) -> {
        String label1 = StringUtils.defaultString(o1.getLabel());
        String label2 = StringUtils.defaultString(o2.getLabel());
        return label1.toLowerCase().compareTo(label2.toLowerCase());
    };
    protected static final Predicate<Network> ACTIVE_NETWORK = Network::isActive;

    protected BusinessAuthorizationService authorizationService;
    protected CacheService cacheService;

    protected NetworkTopiaDao networkDao;

    protected NetworkManagerTopiaDao networkManagerDao;

    protected AgrosystUserTopiaDao userDao;

    protected UserRoleTopiaDao userRoleDao;

    protected GrowingSystemTopiaDao growingSystemDao;
    
    @Override
    public PaginationResult<Network> getFilteredNetworks(NetworkFilter filter) {
        List<NetworkManager> managers = null;
        if (filter != null) {
            String networkManagerName = filter.getNetworkManager();
            if (StringUtils.isNotEmpty(networkManagerName)) {
                managers = networkManagerDao.getNameFilteredNetworkManager(networkManagerName);
            }
        }

        return networkDao.getFilteredNetworks(filter, managers);
    }

    @Override
    public Set<String> getFilteredNetworkIds(NetworkFilter filter) {
        List<NetworkManager> managers = null;
        if (filter != null) {
            String networkManagerName = filter.getNetworkManager();
            if (StringUtils.isNotEmpty(networkManagerName)) {
                managers = networkManagerDao.getNameFilteredNetworkManager(networkManagerName);
            }
        }
        return networkDao.getFilteredNetworkIds(filter, managers);
    }

    @Override
    public Network getNetwork(String networkTopiaId) {
        return networkDao.forTopiaIdEquals(networkTopiaId).findUnique();
    }

    @Override
    public Network newNetwork() {
        Network result = networkDao.newInstance();
        // A new network is active
        result.setActive(true);
        // ... with a manager which is the current user
        NetworkManager manager = networkManagerDao.newInstance();
        manager.setFromDate(getContext().getCurrentDate());
        manager.setActive(true);
        String userId = getSecurityContext().getUserId();
        AgrosystUserImpl agrosystUser = new AgrosystUserImpl();
        agrosystUser.setTopiaId(userId);
        manager.setAgrosystUser(agrosystUser);
        result.addManagers(manager);
        return result;
    }

    @Override
    public Network createOrUpdateNetwork(Network network, Collection<NetworkManagerDto> networkManagerDtos, List<String> parentsIds) {

        authorizationService.checkCreateOrUpdateNetwork(network.getTopiaId());

        if (network.isPersisted()) {
            Preconditions.checkArgument(network.isActive(), "The network is not active !");
        }

        // Add parents to Network
        if (parentsIds != null) {
            if (StringUtils.isBlank(network.getTopiaId())) {
                List<Network> parents = new ArrayList<>();
                for (String parentTopiaId : parentsIds) {
                    parents.add(networkDao.forTopiaIdEquals(parentTopiaId).findUnique());
                }
                network.setParents(parents);
            } else {
                Collection<Network> parents = network.getParents();
                List<Network> parentsToKeep = new ArrayList<>();
                if (parents == null) {
                    parents = new ArrayList<>();
                    network.setParents(parents);
                }
                Map<String, Network> indexedNetworkParents = Maps.uniqueIndex(parents, Entities.GET_TOPIA_ID::apply);
                for (String parentTopiaId : parentsIds) {
                    Network parent = indexedNetworkParents.get(parentTopiaId);
                    if (parent == null) {
                        parent = networkDao.forTopiaIdEquals(parentTopiaId).findUnique();
                        parents.add(parent);
                    }
                    parentsToKeep.add(parent);
                }
                parents.retainAll(parentsToKeep);
            }
        }

        Collection<NetworkManager> networkManagers;
        // add network managers to the network
        if (network.isPersisted()) {
            networkManagers = network.getManagers();
            // In case the list is persisted null ...
            if (networkManagers == null) {
                networkManagers = new ArrayList<>();
                network.setManagers(networkManagers);
            }
        } else {
            networkManagers = new ArrayList<>();
            network.setManagers(networkManagers);
        }

        Map<String, NetworkManager> indexedNetworkManagers = Maps.uniqueIndex(networkManagers, Entities.GET_TOPIA_ID::apply);
        List<NetworkManager> networkManagerToKeep = new ArrayList<>();

        for (NetworkManagerDto networkManagerDto : networkManagerDtos) {
            UserDto userDto = networkManagerDto.getUser();
            AgrosystUser user = userDao.forTopiaIdEquals(userDto.getTopiaId()).findUnique();

            String networkManagerTopiaId = networkManagerDto.getTopiaId();
            NetworkManager networkManager;
            if (StringUtils.isBlank(networkManagerTopiaId)) {
                networkManager = networkManagerDao.newInstance();
            } else {
                networkManager = indexedNetworkManagers.get(networkManagerTopiaId);
            }
            networkManager.setAgrosystUser(user);
            networkManager.setFromDate(networkManagerDto.getFromDate());
            networkManager.setToDate(networkManagerDto.getToDate());
            networkManager.setActive(networkManagerDto.isActive());

            NetworkManager persistedNetworkManager;
            if (StringUtils.isBlank(networkManager.getTopiaId())) {
                persistedNetworkManager = networkManagerDao.create(networkManager);
                networkManagers.add(persistedNetworkManager);
            } else {
                persistedNetworkManager = networkManagerDao.update(networkManager);
            }
            networkManagerToKeep.add(persistedNetworkManager);
        }
        networkManagers.retainAll(networkManagerToKeep);

        Network result;
        if (StringUtils.isBlank(network.getTopiaId())) {
            network.setActive(true);
            result = networkDao.create(network);

            authorizationService.networkCreated(result);
        } else {
            result = networkDao.update(network);

            authorizationService.networkUpdated(network);
        }

        getTransaction().commit();

        clearSingleCacheAndSync(CacheDiscriminator.networksHierarchy());

        return result;
    }

    @Override
    public NetworkManager newNetworkManager() {
        return networkManagerDao.newInstance();
    }

    @Override
    public LinkedHashMap<String, String> searchNameFilteredActiveNetworks(String research, Integer nbResult,
                                                                          Set<String> exclusions, String selfNetworkId,
                                                                          boolean onNetworkEdition, boolean onlyResponsibleNetwork) {

        if (!onNetworkEdition) {
            // It is not allowed to specify a networkId while not on network edition
            Preconditions.checkArgument(StringUtils.isEmpty(selfNetworkId));
        }

        Set<String> onlyNetworkIds = null;
        if (onlyResponsibleNetwork && !getSecurityContext().isAdmin()) {
            onlyNetworkIds = getUserNetworks();
        }

        return networkDao.getNameFilteredNetworks(research, nbResult, exclusions, selfNetworkId, onNetworkEdition, onlyNetworkIds);
    }

    protected Set<String> getUserNetworks() {
        String userId = getSecurityContext().getUserId();
        List<UserRole> roles = userRoleDao.findAllForUserId(userId);
        Set<String> result;
        if (roles != null) {
            result = roles.stream()
                    .filter(r -> r.getType() == RoleType.NETWORK_RESPONSIBLE || r.getType() == RoleType.NETWORK_SUPERVISOR)
                    .map(UserRole::getNetworkId)
                    .collect(Collectors.toSet());
        } else {
            result = new HashSet<>();
        }
        return result;
    }

    @Override
    public Set<String> findNetworksByName(String name, String excludeNetworkId) {
        Iterable<Network> networks = networkDao.findAllByNameWithoutCase(name);
        Iterable<String> networkIds = StreamSupport.stream(networks.spliterator(), false).map(Entities.GET_TOPIA_ID).collect(Collectors.toList());
        if (StringUtils.isNotEmpty(excludeNetworkId)) {
            Iterables.removeIf(networkIds, s -> Objects.equals(s, excludeNetworkId));
        }
        return Sets.newHashSet(networkIds);
    }

    @Override
    public NetworkGraph buildFullNetworkGraph() {

        NetworkGraph graph = new NetworkGraph();

        if (getConfig().isDemoModeEnabled()) {
            // find all active networks
            List<Network> allNetworks = networkDao.forActiveEquals(true).findAll();

            // build graph
            for (Network network : allNetworks) {
                graph.addNetwork(network);
            }

            escapeTopiaIds(graph);
            sortLevels(graph);
        }

        return graph;
    }

    protected void sortLevels(NetworkGraph graph) {
        for (List<SmallNetworkDto> smallNetworkDtos : graph.getNetworks()) {
            smallNetworkDtos.sort(ALPHABETICAL_NETWORK_COMPARATOR);
        }
    }

    protected void escapeTopiaIds(NetworkGraph graph) {
        for (List<SmallNetworkDto> smallNetworkDtos : graph.getNetworks()) {
            for (SmallNetworkDto smallNetworkDto : smallNetworkDtos) {
                smallNetworkDto.setId(Entities.ESCAPE_TOPIA_ID.apply(smallNetworkDto.getId()));
            }
        }
        for (NetworkConnectionDto networkConnectionDto : graph.getConnections()) {
            networkConnectionDto.setSourceId(Entities.ESCAPE_TOPIA_ID.apply(networkConnectionDto.getSourceId()));
            networkConnectionDto.setTargetId(Entities.ESCAPE_TOPIA_ID.apply(networkConnectionDto.getTargetId()));
        }
    }

    @Override
    public NetworkGraph buildNetworkGraph(String fromNetworkId) {

        NetworkGraph graph = new NetworkGraph();
        Network network = getNetwork(fromNetworkId);

        graph.addNetwork(network);

        escapeTopiaIds(graph);
        sortLevels(graph);

        return graph;
    }

    protected void findAllParents(Set<Network> result, Iterable<Network> networks) {
        for (Network network : networks) {
            result.add(network);
            Collection<Network> parents = network.getParents();
            findAllParents(result, parents);
        }
    }

    @Override
    public NetworkGraph buildGrowingSystemAndNetworkGraph(String growingSystemName, Set<String> parentNetworkIds) {

        NetworkGraph graph = new NetworkGraph();

        Set<Network> parents = Sets.newHashSet();

        for (String networkId : parentNetworkIds) {
            Network network = networkDao.forTopiaIdEquals(networkId).findUnique();
            parents.add(network);
        }

        graph.addGrowingSystem(growingSystemName, parents);

        Set<Network> parentsOfParents = Sets.newHashSet();
        findAllParents(parentsOfParents, parents);

        for (Network parentsOfParent : parentsOfParents) {
            graph.addNetwork(parentsOfParent);
        }

        escapeTopiaIds(graph);
        sortLevels(graph);

        return graph;
    }

    @Override
    public void unactivateNetworks(List<String> networkIds, boolean activate) {
        if (CollectionUtils.isNotEmpty(networkIds)) {

            for (String networkId : networkIds) {
                Network network = networkDao.forTopiaIdEquals(networkId).findUnique();
                network.setActive(activate);
                networkDao.update(network);
            }
            getTransaction().commit();
            clearSingleCacheAndSync(CacheDiscriminator.networksHierarchy());
        }
    }

    @Override
    public long getNetworksCount(Boolean active) {
        if (active == null) {
            return networkDao.count();
        }
        return networkDao.forActiveEquals(active).count();
    }
    
    @Override
    public NetworkIndicators getIndicators(String networkId) {
        NetworkIndicators result = new NetworkIndicators();
        if (StringUtils.isNotEmpty(networkId)) {
            ImmutableSet<String> set = ImmutableSet.of(networkId);
    
            Set<String> domainIds = networkDao.getProjectionHelper().networksToDomains(set);
            long activeDomainCount = getPersistenceContext().getDomainDao().forTopiaIdIn(domainIds).addEquals(Domain.PROPERTY_ACTIVE, true).count();
            
            result.setDomainsCount(domainIds.size());
            result.setActiveDomainsCount(activeDomainCount);
    
            final Set<String> growingPlanIds = networkDao.getProjectionHelper().networksToGrowingPlans(set);
            result.setGrowingPlansCount(growingPlanIds.size());
    
            long activeGrowingPlanCount = getPersistenceContext().getGrowingPlanDao().forTopiaIdIn(growingPlanIds).addEquals(GrowingPlan.PROPERTY_ACTIVE, true).count();
            result.setActiveGrowingPlansCount(activeGrowingPlanCount);

            List<GrowingSystemsIndicator> gsIndicators = growingSystemDao.networksToGrowingSystemIndicators(set);
            result.setGrowingSystems(gsIndicators);

            Set<Network> subNetworks = networkDao.loadNetworksWithDescendantHierarchy(set);
            Iterables.removeIf(subNetworks, Predicates.compose(s -> Objects.equals(s, networkId), Entities.GET_TOPIA_ID::apply));
            
            result.setSubNetworksCount(subNetworks.size());
            
            long activeSubNetworksCount = Iterables.size(Iterables.filter(subNetworks, ACTIVE_NETWORK::test));
            result.setActiveSubNetworksCount(activeSubNetworksCount);
        }

        return result;
    }

    @Override
    public List<Network> getNetworkWithName(String networkName) {
        Preconditions.checkNotNull(networkName);
        return networkDao.forNameEquals(networkName).findAll();
    }

    @Override
    public List<Network> getNetworksForUserManager(String userId) {
        return networkDao.findAllNetworksForUserManager(userId);
    }

}
