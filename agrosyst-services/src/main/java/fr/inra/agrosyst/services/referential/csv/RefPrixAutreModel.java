package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefPrixAutre;
import fr.inra.agrosyst.api.entities.referential.RefPrixAutreImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefPrixAutreModel extends AbstractDestinationAndPriceModel<RefPrixAutre> implements ExportModel<RefPrixAutre> {

    public RefPrixAutreModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("reference_id", RefOtherInput.PROPERTY_REFERENCE_ID);
        newMandatoryColumn("reference_code", RefOtherInput.PROPERTY_REFERENCE_CODE);
        newMandatoryColumn("Prix", RefPrixAutre.PROPERTY_PRICE, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("Unité", RefPrixAutre.PROPERTY_UNIT, PRICE_UNIT_PARSER);
        newMandatoryColumn("Code scénario", RefPrixAutre.PROPERTY_CODE_SCENARIO);
        newMandatoryColumn("Scénario", RefPrixAutre.PROPERTY_SCENARIO);
        newMandatoryColumn("Campagne", RefPrixAutre.PROPERTY_CAMPAIGN, INTEGER_WITH_NULL_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefPrixAutre.PROPERTY_ACTIVE, ACTIVE_PARSER);
        newOptionalColumn("Source", RefPrixAutre.PROPERTY_SOURCE);
    }

    @Override
    public Iterable<ExportableColumn<RefPrixAutre, Object>> getColumnsForExport() {
        ModelBuilder<RefPrixAutre> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("reference_id", RefOtherInput.PROPERTY_REFERENCE_ID);
        modelBuilder.newColumnForExport("reference_code", RefOtherInput.PROPERTY_REFERENCE_CODE);
        modelBuilder.newColumnForExport("Prix", RefPrixAutre.PROPERTY_PRICE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Unité", RefPrixAutre.PROPERTY_UNIT, PRICE_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("Code scénario", RefPrixAutre.PROPERTY_CODE_SCENARIO);
        modelBuilder.newColumnForExport("Scénario", RefPrixAutre.PROPERTY_SCENARIO);
        modelBuilder.newColumnForExport("Campagne", RefPrixAutre.PROPERTY_CAMPAIGN, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefPrixAutre.PROPERTY_ACTIVE, T_F_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefPrixAutre.PROPERTY_SOURCE);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefPrixAutre newEmptyInstance() {
        RefPrixAutre result = new RefPrixAutreImpl();
        result.setActive(true);
        return result;
    }
}
