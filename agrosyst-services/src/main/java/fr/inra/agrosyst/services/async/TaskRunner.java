package fr.inra.agrosyst.services.async;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.async.Task;
import fr.inra.agrosyst.services.ServiceContext;

/**
 * Représente un élément capable de traiter une {@link Task}.
 *
 */
public interface TaskRunner<T extends Task> {

    /**
     * Éxécute la {@link Task} donnée en paramètre.
     * <p>
     * Par conception, la tâche doit être terminée quand cette méthode se termine, il ne faut donc pas lancer de
     * traitement asynchrone lors de l'éxécution de ladite tâche.
     *
     * @param task la tâche à éxécuter
     */
    void runTask(T task, ServiceContext serviceContextProvider);

    boolean cancelTask(Task task);
}
