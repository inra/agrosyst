package fr.inra.agrosyst.services.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPC;
import fr.inra.agrosyst.api.entities.referential.RefActaGroupeCultures;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleRationAliment;
import fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAA;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefConversionUnitesQSA;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCouvSolAnnuelle;
import fr.inra.agrosyst.api.entities.referential.RefCouvSolPerenne;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol;
import fr.inra.agrosyst.api.entities.referential.RefCultureMAA;
import fr.inra.agrosyst.api.entities.referential.RefCultureMAATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEdaplosTypeTraitement;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceOtherTools;
import fr.inra.agrosyst.api.entities.referential.RefEspeceToVariete;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefGesCarburant;
import fr.inra.agrosyst.api.entities.referential.RefGesEngrais;
import fr.inra.agrosyst.api.entities.referential.RefGesPhyto;
import fr.inra.agrosyst.api.entities.referential.RefGesSemence;
import fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitement;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceConverter;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionTypeItemInputEDI;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatus;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefMAABiocontrole;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import fr.inra.agrosyst.api.entities.referential.RefMarketingDestination;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.entities.referential.RefMesure;
import fr.inra.agrosyst.api.entities.referential.RefNrjCarburant;
import fr.inra.agrosyst.api.entities.referential.RefNrjEngrais;
import fr.inra.agrosyst.api.entities.referential.RefNrjGesOutil;
import fr.inra.agrosyst.api.entities.referential.RefNrjPhyto;
import fr.inra.agrosyst.api.entities.referential.RefNrjSemence;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefOTEX;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDI;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import fr.inra.agrosyst.api.entities.referential.RefPhytoSubstanceActiveIphy;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixAutre;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbu;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspece;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMin;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefPrixIrrig;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhyto;
import fr.inra.agrosyst.api.entities.referential.RefPrixPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefProtocoleVgObs;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClass;
import fr.inra.agrosyst.api.entities.referential.RefSaActaIphy;
import fr.inra.agrosyst.api.entities.referential.RefSeedUnits;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSector;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefStationMeteo;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLever;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefSupportOrganeEDI;
import fr.inra.agrosyst.api.entities.referential.RefTraitSdC;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.entities.referential.RefTypeNotationEDI;
import fr.inra.agrosyst.api.entities.referential.RefUniteEDI;
import fr.inra.agrosyst.api.entities.referential.RefUnitesQualifiantEDI;
import fr.inra.agrosyst.api.entities.referential.RefValeurQualitativeEDI;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGeves;
import fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefZoneClimatiqueIphy;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntity;
import fr.inra.agrosyst.api.entities.referential.ReferentialI18nEntry;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSpcRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.referential.ExportService;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.export.EntityExporter;
import fr.inra.agrosyst.services.common.export.ExportModelAndRows;
import fr.inra.agrosyst.services.referential.csv.RefActaDosageSPCModel;
import fr.inra.agrosyst.services.referential.csv.RefActaDosageSaRootModel;
import fr.inra.agrosyst.services.referential.csv.RefActaDosageSpcRootModel;
import fr.inra.agrosyst.services.referential.csv.RefActaGroupeCulturesModel;
import fr.inra.agrosyst.services.referential.csv.RefActaProduitRootModel;
import fr.inra.agrosyst.services.referential.csv.RefActaSubstanceActiveModel;
import fr.inra.agrosyst.services.referential.csv.RefActaTraitementsProduitModel;
import fr.inra.agrosyst.services.referential.csv.RefActaTraitementsProduitsCategModel;
import fr.inra.agrosyst.services.referential.csv.RefAdventiceModel;
import fr.inra.agrosyst.services.referential.csv.RefAgsAmortissementModel;
import fr.inra.agrosyst.services.referential.csv.RefAnimalTypeModel;
import fr.inra.agrosyst.services.referential.csv.RefCattleAnimalTypeModel;
import fr.inra.agrosyst.services.referential.csv.RefCattleRationAlimentModel;
import fr.inra.agrosyst.services.referential.csv.RefCiblesAgrosystGroupesCiblesMAAModel;
import fr.inra.agrosyst.services.referential.csv.RefClonePlantGrapeModel;
import fr.inra.agrosyst.services.referential.csv.RefCompositionSubstancesActivesParNumeroAMMModel;
import fr.inra.agrosyst.services.referential.csv.RefConversionUnitesQSAModel;
import fr.inra.agrosyst.services.referential.csv.RefCountryModel;
import fr.inra.agrosyst.services.referential.csv.RefCouvSolAnnuelleModel;
import fr.inra.agrosyst.services.referential.csv.RefCouvSolPerenneModel;
import fr.inra.agrosyst.services.referential.csv.RefCultureEdiGroupeCouvSolModel;
import fr.inra.agrosyst.services.referential.csv.RefDestinationModel;
import fr.inra.agrosyst.services.referential.csv.RefEdaplosTypeTraitemenModel;
import fr.inra.agrosyst.services.referential.csv.RefElementVoisinageModel;
import fr.inra.agrosyst.services.referential.csv.RefEspeceDto;
import fr.inra.agrosyst.services.referential.csv.RefEspeceModel;
import fr.inra.agrosyst.services.referential.csv.RefEspeceOtherToolsModel;
import fr.inra.agrosyst.services.referential.csv.RefEspeceToVarieteModel;
import fr.inra.agrosyst.services.referential.csv.RefFeedbackRouterModel;
import fr.inra.agrosyst.services.referential.csv.RefFertiMinUNIFAModel;
import fr.inra.agrosyst.services.referential.csv.RefFertiOrgaModel;
import fr.inra.agrosyst.services.referential.csv.RefGesCarburantModel;
import fr.inra.agrosyst.services.referential.csv.RefGesEngraisModel;
import fr.inra.agrosyst.services.referential.csv.RefGesPhytoModel;
import fr.inra.agrosyst.services.referential.csv.RefGesSemenceModel;
import fr.inra.agrosyst.services.referential.csv.RefGroupeCibleTraitementModel;
import fr.inra.agrosyst.services.referential.csv.RefHarvestingPriceConverterModel;
import fr.inra.agrosyst.services.referential.csv.RefHarvestingPriceModel;
import fr.inra.agrosyst.services.referential.csv.RefInputUnitPriceUnitConverterModel;
import fr.inra.agrosyst.services.referential.csv.RefInterventionAgrosystTravailEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefInterventionTypeItemInputEdiModel;
import fr.inra.agrosyst.services.referential.csv.RefLegalStatusModel;
import fr.inra.agrosyst.services.referential.csv.RefLocationExportModel;
import fr.inra.agrosyst.services.referential.csv.RefMAABiocontroleModel;
import fr.inra.agrosyst.services.referential.csv.RefMAADosesRefParGroupeCibleModel;
import fr.inra.agrosyst.services.referential.csv.RefMarketingDestinationModel;
import fr.inra.agrosyst.services.referential.csv.RefMaterielAutomoteurModel;
import fr.inra.agrosyst.services.referential.csv.RefMaterielIrrigationModel;
import fr.inra.agrosyst.services.referential.csv.RefMaterielOutilModel;
import fr.inra.agrosyst.services.referential.csv.RefMaterielTracteurModel;
import fr.inra.agrosyst.services.referential.csv.RefMesureModel;
import fr.inra.agrosyst.services.referential.csv.RefNrjCarburantModel;
import fr.inra.agrosyst.services.referential.csv.RefNrjEngraisModel;
import fr.inra.agrosyst.services.referential.csv.RefNrjGesOutilModel;
import fr.inra.agrosyst.services.referential.csv.RefNrjPhytoModel;
import fr.inra.agrosyst.services.referential.csv.RefNrjSemenceModel;
import fr.inra.agrosyst.services.referential.csv.RefNuisibleEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefOTEXModel;
import fr.inra.agrosyst.services.referential.csv.RefOrientationEdiModel;
import fr.inra.agrosyst.services.referential.csv.RefOtherInputModel;
import fr.inra.agrosyst.services.referential.csv.RefParcelleZonageEdiModel;
import fr.inra.agrosyst.services.referential.csv.RefPhrasesRisqueEtClassesMentionDangerParAMMModel;
import fr.inra.agrosyst.services.referential.csv.RefPhytoSubstanceActiveIphyModel;
import fr.inra.agrosyst.services.referential.csv.RefPotModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixAutreModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixCarbuModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixEspeceModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixFertiMinModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixFertiOrgaModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixIrrigModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixPhytoModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixPotModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixSubstrateModel;
import fr.inra.agrosyst.services.referential.csv.RefProtocoleVgObsModel;
import fr.inra.agrosyst.services.referential.csv.RefQualityCriteriaClassModel;
import fr.inra.agrosyst.services.referential.csv.RefQualityCriteriaModel;
import fr.inra.agrosyst.services.referential.csv.RefSaActaIphyModel;
import fr.inra.agrosyst.services.referential.csv.RefSeedUnitsModel;
import fr.inra.agrosyst.services.referential.csv.RefSolArvalisModel;
import fr.inra.agrosyst.services.referential.csv.RefSolCaracteristiqueIndigoModel;
import fr.inra.agrosyst.services.referential.csv.RefSolProfondeurIndigoModel;
import fr.inra.agrosyst.services.referential.csv.RefSolTextureGeppaModel;
import fr.inra.agrosyst.services.referential.csv.RefSpeciesToSectorModel;
import fr.inra.agrosyst.services.referential.csv.RefStadeEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefStadeNuisibleEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefStationMeteoModel;
import fr.inra.agrosyst.services.referential.csv.RefStrategyLeverModel;
import fr.inra.agrosyst.services.referential.csv.RefSubstancesActivesCommissionEuropeenneModel;
import fr.inra.agrosyst.services.referential.csv.RefSubstrateModel;
import fr.inra.agrosyst.services.referential.csv.RefSupportOrganeEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefTraductionModel;
import fr.inra.agrosyst.services.referential.csv.RefTraitSdCModel;
import fr.inra.agrosyst.services.referential.csv.RefTypeAgricultureModel;
import fr.inra.agrosyst.services.referential.csv.RefTypeNotationEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefUniteEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefUnitesQualifiantEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefValeurQualitativeEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefVarieteGevesModel;
import fr.inra.agrosyst.services.referential.csv.RefVarietePlantGrapeModel;
import fr.inra.agrosyst.services.referential.csv.RefZoneClimatiqueIphyModel;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.csv.Export;
import org.nuiton.csv.ExportModel;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Implementation du service d'export.
 * 
 * @author Eric Chatellier
 */
public class ExportServiceImpl extends AbstractAgrosystService implements ExportService {

    protected <E extends ReferentialEntity> InputStream runSimpleExport(Iterable<String> entityIds, Class<E> clazz, ExportModel<E> exportModel) {
        InputStream result;

        try {
            // get lazy iterator of entities to export
            final TopiaDao<E> classDao = context.getDaoSupplier().getDao(clazz);
            
            Iterable<E> entities;
            if (entityIds == null) {
                entities = classDao.findAll();
            } else {
                entities = StreamSupport.stream(entityIds.spliterator(), false).map(input -> classDao.forTopiaIdEquals(input).findUnique()).collect(Collectors.toList());
            }

            // export iterator
            File tempFile = File.createTempFile("export-" + clazz.getSimpleName() + "-", ".csv");
            tempFile.deleteOnExit();
            Export.exportToFile(exportModel, entities, tempFile, StandardCharsets.UTF_8);
            result = new FileInputStream(tempFile);
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't export", ex);
        }
    
        return result;
    }

    protected <E extends ReferentialEntity> InputStream runSimpleExportActiveRows(Class<E> clazz, ExportModel<E> exportModel) {
        InputStream result;

        try {
            // get lazy iterator of entities to export
            final TopiaDao<E> classDao = context.getDaoSupplier().getDao(clazz);

            Iterable<E> entities = classDao.newQueryBuilder().addEquals("active", true).findAll();

            // export iterator
            File tempFile = File.createTempFile("export-" + clazz.getSimpleName() + "-", ".csv");
            tempFile.deleteOnExit();
            Export.exportToFile(exportModel, entities, tempFile, StandardCharsets.UTF_8);
            result = new FileInputStream(tempFile);
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't export", ex);
        }

        return result;
    }

    protected <E extends ReferentialEntity> Iterable<E> loadEntitiesToExport(Class<E> clazz) {
        Iterable<E> entities = null;
        try {
            // get lazy iterator of entities to export
            final TopiaDao<E> classDao = context.getDaoSupplier().getDao(clazz);

            entities = classDao.newQueryBuilder().addEquals("active", true).findAll();

        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't export", ex);
        }

        return entities;
    }
    
    protected InputStream runExportRefEspece(Iterable<String> entityIds) {
        InputStream result;
        
        RefEspeceModel exportModel = new RefEspeceModel();
        try {
            // get lazy iterator of entities to export
            RefEspeceTopiaDao refEspeceDao = context.getDaoSupplier().getRefEspeceDao();
            RefCultureMAATopiaDao refCultureMAADao = context.getDaoSupplier().getRefCultureMAADao();

            Iterable<RefEspece> entities0;
            if (entityIds == null) {
                entities0 = refEspeceDao.findAll();
            } else {
                entities0 = StreamSupport.stream(entityIds.spliterator(), false)
                        .map(input -> refCultureMAADao.forTopiaIdEquals(input).findUnique())
                        .map(input -> refEspeceDao.forCulturesMaaContains(input).findUnique())
                        .collect(Collectors.toSet());
            }
            
            List<RefEspeceDto> entities = new ArrayList<>();
            Binder<RefEspece, RefEspeceDto> binder = BinderFactory.newBinder(RefEspece.class, RefEspeceDto.class);
            for (RefEspece refEspece : entities0) {
                
                Collection<RefCultureMAA> cultureMaas = refEspece.getCulturesMaa();
                if (CollectionUtils.isNotEmpty(cultureMaas)) {
                    for (RefCultureMAA cultureMaa : cultureMaas) {
                        RefEspeceDto dto = new RefEspeceDto(refEspece.getTopiaId());
                        binder.copy(refEspece, dto);
                        dto.setCode_culture_maa(cultureMaa.getCode_culture_maa());
                        dto.setCulture_maa(cultureMaa.getCulture_maa());
                        entities.add(dto);
                    }
                } else {
                    RefEspeceDto dto = new RefEspeceDto(refEspece.getTopiaId());
                    binder.copy(refEspece, dto);
                    entities.add(dto);
                }
            }
            
            
            // export iterator
            File tempFile = File.createTempFile("export-" + RefEspece.class.getSimpleName() + "-", ".csv");
            tempFile.deleteOnExit();
            Export.exportToFile(exportModel, entities, tempFile, StandardCharsets.UTF_8);
            result = new FileInputStream(tempFile);
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't export", ex);
        }
        
        return result;
    }

    protected InputStream runExportReferentialI18Entry(Class<? extends ReferentialI18nEntry> klass) {
        ExportModel<ReferentialI18nEntry> exportModel = new RefTraductionModel();

        try {
            TopiaJpaSupport jpaSupport = getPersistenceContext().getJpaSupport();
            List<ReferentialI18nEntry> entries = jpaSupport.findAll(
                    String.format(" FROM %s refTrad ", klass.getName()), new HashMap<>());

            File tempFile = File.createTempFile("export-" + RefEspece.class.getSimpleName() + "-", ".csv");
            tempFile.deleteOnExit();
            Export.exportToFile(exportModel, entries, tempFile, StandardCharsets.UTF_8);

            InputStream result = new FileInputStream(tempFile);
            return result;
        } catch (Exception e) {
            throw new AgrosystTechnicalException("Can't export", e);
        }
    }

    protected InputStream runExportRefLocation(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefLocation.class,
                new RefLocationExportModel());
    }

    @Override
    public InputStream exportOrientationEdiCSV(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefOrientationEDI.class,
                new RefOrientationEdiModel());
    }
    
    @Override
    public InputStream exportSolArvalisCSV(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefSolArvalis.class,
                new RefSolArvalisModel());
    }

    @Override
    public InputStream exportMaterielTracteursCSV(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefMaterielTraction.class,
                new RefMaterielTracteurModel());
    }

    @Override
    public InputStream exportMaterielAutomoteursCSV(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefMaterielAutomoteur.class,
                new RefMaterielAutomoteurModel());
    }

    @Override
    public InputStream exportMaterielOutilsCSV(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefMaterielOutil.class,
                new RefMaterielOutilModel());
    }

    @Override
    public InputStream exportMaterielIrrigationCSV(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefMaterielIrrigation.class,
                new RefMaterielIrrigationModel());
    }

    @Override
    public InputStream exportLegalStatusCSV(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefLegalStatus.class,
                new RefLegalStatusModel());
    }

    @Override
    public InputStream exportEspeces(Iterable<String> entityIds) {
        return runExportRefEspece(entityIds);
    }

    @Override
    public InputStream exportVarietesGeves(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefVarieteGeves.class,
                new RefVarieteGevesModel());
    }

    @Override
    public InputStream exportVarietesPlantGrape(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefVarietePlantGrape.class,
                new RefVarietePlantGrapeModel());
    }

    @Override
    public InputStream exportClonesPlantGrape(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefClonePlantGrape.class,
                new RefClonePlantGrapeModel());
    }

    @Override
    public InputStream exportEspecesToVarietes(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefEspeceToVariete.class,
                new RefEspeceToVarieteModel());
    }

    @Override
    public InputStream exportOtexCSV(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefOTEX.class,
                new RefOTEXModel());
    }

    @Override
    public InputStream exportInterventionAgrosystTravailEdiCSV(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefInterventionAgrosystTravailEDI.class,
                new RefInterventionAgrosystTravailEDIModel());
    }

    @Override
    public InputStream exportStadesEdiCSV(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefStadeEDI.class,
                new RefStadeEDIModel());
    }

    @Override
    public InputStream exportSolTextureGeppa(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefSolTextureGeppa.class,
                new RefSolTextureGeppaModel());
    }

    @Override
    public InputStream exportZonageParcelleEdi(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefParcelleZonageEDI.class,
                new RefParcelleZonageEdiModel());
    }

    @Override
    public InputStream exportSolProfondeurIndigo(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefSolProfondeurIndigo.class,
                new RefSolProfondeurIndigoModel());
    }

    @Override
    public InputStream exportSolCarateristiquesIndigo(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefSolCaracteristiqueIndigo.class,
                new RefSolCaracteristiqueIndigoModel());
    }

    @Override
    public InputStream exportUniteEDI(Iterable<String> entityIds){
        return runSimpleExport(
                entityIds,
                RefUniteEDI.class,
                new RefUniteEDIModel());
    }

    @Override
    public InputStream exportFertiMinUNIFA(Iterable<String> entityIds){
        return runSimpleExport(
                entityIds,
                RefFertiMinUNIFA.class,
                new RefFertiMinUNIFAModel());
    }

    @Override
    public InputStream exportAdventices(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefAdventice.class,
                new RefAdventiceModel());
    }
    
    @Override
    public InputStream exportAgsAmortissement(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefAgsAmortissement.class,
                new RefAgsAmortissementModel());
        
    }
    
    @Override
    public InputStream exportNuisiblesEDI(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefNuisibleEDI.class,
                new RefNuisibleEDIModel());
    }

    @Override
    public InputStream exportFertiOrga(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefFertiOrga.class,
                new RefFertiOrgaModel());
    }

    @Override
    public InputStream exportStationMeteo(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefStationMeteo.class,
                new RefStationMeteoModel()
        );
    }

    @Override
    public InputStream exportGesCarburants(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefGesCarburant.class,
                new RefGesCarburantModel()
        );
    }

    @Override
    public InputStream exportGesEngrais(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefGesEngrais.class,
                new RefGesEngraisModel()
        );
    }

    @Override
    public InputStream exportGesPhyto(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefGesPhyto.class,
                new RefGesPhytoModel()
        );
    }

    @Override
    public InputStream exportGesSemences(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefGesSemence.class,
                new RefGesSemenceModel()
        );
    }

    @Override
    public InputStream exportNrjCarburants(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefNrjCarburant.class,
                new RefNrjCarburantModel()
        );
    }

    @Override
    public InputStream exportNrjEngrais(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefNrjEngrais.class,
                new RefNrjEngraisModel()
        );
    }

    @Override
    public InputStream exportNrjPhyto(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefNrjPhyto.class,
                new RefNrjPhytoModel()
        );
    }

    @Override
    public InputStream exportNrjSemences(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefNrjSemence.class,
                new RefNrjSemenceModel()
        );
    }

    @Override
    public InputStream exportNrjGesOutils(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefNrjGesOutil.class,
                new RefNrjGesOutilModel()
        );
    }

    @Override
    public InputStream exportMesure(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefMesure.class,
                new RefMesureModel()
        );
    }

    @Override
    public InputStream exportSupportOrganeEDI(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefSupportOrganeEDI.class,
                new RefSupportOrganeEDIModel()
        );
    }

    @Override
    public InputStream exportStadeNuisibleEDI(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefStadeNuisibleEDI.class,
                new RefStadeNuisibleEDIModel()
        );
    }

    @Override
    public InputStream exportTypeNotationEDI(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefTypeNotationEDI.class,
                new RefTypeNotationEDIModel()
        );
    }

    @Override
    public InputStream exportValeurQualitativeEDI(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefValeurQualitativeEDI.class,
                new RefValeurQualitativeEDIModel()
        );
    }

    @Override
    public InputStream exportUnitesQualifiantEDI(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefUnitesQualifiantEDI.class,
                new RefUnitesQualifiantEDIModel()
        );
    }

    @Override
    public InputStream exportActaTraitementsProducts(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefActaTraitementsProduit.class,
                new RefActaTraitementsProduitModel()
        );
    }

    @Override
    public InputStream exportActaSubstanceActive(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefActaSubstanceActive.class,
                new RefActaSubstanceActiveModel()
        );
    }

    @Override
    public InputStream exportProtocoleVgObs(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefProtocoleVgObs.class,
                new RefProtocoleVgObsModel()
        );
    }

    @Override
    public InputStream exportElementVoisinage(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefElementVoisinage.class,
                new RefElementVoisinageModel()
        );
    }

    @Override
    public InputStream exportPhytoSubstanceActiveIphy(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefPhytoSubstanceActiveIphy.class,
                new RefPhytoSubstanceActiveIphyModel()
        );
    }

    @Override
    public InputStream exportTypeAgriculture(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefTypeAgriculture.class,
                new RefTypeAgricultureModel()
        );
    }
 
    @Override
    public InputStream exportActaDosageSpc(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefActaDosageSPC.class,
                new RefActaDosageSPCModel()
        );
    }

    @Override
    public InputStream exportActaGroupeCultures(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefActaGroupeCultures.class,
                new RefActaGroupeCulturesModel()
        );
    }

    @Override
    public InputStream exportSaActaIphy(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefSaActaIphy.class,
                new RefSaActaIphyModel()
        );
    }

    @Override
    public InputStream exportTraitSdC(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefTraitSdC.class,
                new RefTraitSdCModel()
        );
    }

    @Override
    public InputStream exportCultureEdiGroupeCouvSol(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefCultureEdiGroupeCouvSol.class,
                new RefCultureEdiGroupeCouvSolModel()
        );
    }

    @Override
    public InputStream exportCouvSolPerenne(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefCouvSolPerenne.class,
                new RefCouvSolPerenneModel()
        );
    }

    @Override
    public InputStream exportCouvSolAnnuelle(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefCouvSolAnnuelle.class,
                new RefCouvSolAnnuelleModel()
        );
    }

    @Override
    public InputStream exportActaTraitementsProductsCateg(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefActaTraitementsProduitsCateg.class,
                new RefActaTraitementsProduitsCategModel()
        );
    }

    @Override
    public InputStream exportZoneClimatiqueIphy(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefZoneClimatiqueIphy.class,
                new RefZoneClimatiqueIphyModel()
        );
    }

    @Override
    public InputStream exportDestination(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefDestination.class,
                new RefDestinationModel()
        );
    }

    @Override
    public InputStream exportQualityCriteria(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefQualityCriteria.class,
                new RefQualityCriteriaModel()
        );
    }

    @Override
    public InputStream exportRefSpeciesToSector(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefSpeciesToSector.class,
                new RefSpeciesToSectorModel()
        );
    }

    @Override
    public InputStream exportRefHarvestingPrice(List<String> entityIds) {
        RefHarvestingPriceModel model = new RefHarvestingPriceModel();

        return runSimpleExport(
                entityIds,
                RefHarvestingPrice.class,
                model
        );
    }

    @Override
    public InputStream exportRefHarvestingPriceConverter(List<String> entityIds) {
        RefHarvestingPriceConverterModel model = new RefHarvestingPriceConverterModel();

        return runSimpleExport(
                entityIds,
                RefHarvestingPriceConverter.class,
                model
        );
    }

    @Override
    public InputStream exportRefStrategyLever(List<String> entityIds) {
        RefStrategyLeverModel model = new RefStrategyLeverModel();

        return runSimpleExport(
                entityIds,
                RefStrategyLever.class,
                model
        );
    }

    @Override
    public ExportResult exportActiveRefStrategyLeverXlsx() {
        ExportResult result;
        Iterable<RefStrategyLever> refStrategyLevers = loadEntitiesToExport(RefStrategyLever.class);
        ExportModelAndRows<RefStrategyLever> data = new ExportModelAndRows<>(new ExportActiveRefStrategyLeverModel(), refStrategyLevers);

        EntityExporter exporter = new EntityExporter();
        result = exporter.exportAsXlsx("Agrosyst types de leviers par filière",data);

        return result;
    }

    public static class ExportActiveRefStrategyLeverModel extends fr.inra.agrosyst.services.common.export.ExportModel<RefStrategyLever> {

        @Override
        public String getTitle() {
            return "Types de leviers par filière";
        }

        public ExportActiveRefStrategyLeverModel() {
            super();
            newColumn("Code", RefStrategyLever::getCode);
            newColumn("Filière", RefStrategyLever::getSector);
            newColumn("Rubrique", RefStrategyLever::getSectionType);
            newColumn("Type de gestion/levier", RefStrategyLever::getSectionType);
            newColumn("Levier", RefStrategyLever::getLever);
        }

    }

    @Override
    public ExportResult exportActiveRefBioAggressorsXlsx() {

        ExportResult result;
        Iterable<RefNuisibleEDI> entities = loadEntitiesToExport(RefNuisibleEDI.class);
        ExportModelAndRows<RefNuisibleEDI> data = new ExportModelAndRows<>(new ExportActiveRefNuisibleEDIModel(), entities);

        EntityExporter exporter = new EntityExporter();
        result = exporter.exportAsXlsx("Agrosyst liste des bio-agresseurs",data);

        return result;
    }

    public static class ExportActiveRefNuisibleEDIModel extends fr.inra.agrosyst.services.common.export.ExportModel<RefNuisibleEDI> {

        @Override
        public String getTitle() {
            return "Liste des bio-agresseurs";
        }

        public ExportActiveRefNuisibleEDIModel() {
            super();
            newColumn("reference_param", RefNuisibleEDI::getReference_param);
            newColumn("reference_label", RefNuisibleEDI::getReference_label);
            newColumn("filières", RefNuisibleEDI::getSectors);
            newColumn("source", RefNuisibleEDI::getSource);
        }

    }

    @Override
    public InputStream exportRefAnimalType(List<String> entityIds) {
        RefAnimalTypeModel model = new RefAnimalTypeModel();

        return runSimpleExport(
                entityIds,
                RefAnimalType.class,
                model
        );
    }

    @Override
    public InputStream exportRefMarketingDestination(List<String> entityIds) {
        RefMarketingDestinationModel model = new RefMarketingDestinationModel();

        return runSimpleExport(
                entityIds,
                RefMarketingDestination.class,
                model
        );
    }

    @Override
    public InputStream exportRefWorkItemInputEdi(List<String> entityIds) {
        RefInterventionTypeItemInputEdiModel model = new RefInterventionTypeItemInputEdiModel();

        return runSimpleExport(
                entityIds,
                RefInterventionTypeItemInputEDI.class,
                model
        );
    }

    @Override
    public InputStream exportRefQualityCriteriaClass(List<String> entityIds) {
        RefQualityCriteriaClassModel model = new RefQualityCriteriaClassModel();

        return runSimpleExport(
                entityIds,
                RefQualityCriteriaClass.class,
                model
        );
    }

    @Override
    public InputStream exportRefActaDosageSaRoot(List<String> entityIds) {
        RefActaDosageSaRootModel model = new RefActaDosageSaRootModel();

        return runSimpleExport(
                entityIds,
                RefActaDosageSaRoot.class,
                model
        );
    }

    @Override
    public InputStream exportRefActaProduitRoot(List<String> entityIds) {
        RefActaProduitRootModel model = new RefActaProduitRootModel();

        return runSimpleExport(
                entityIds,
                RefActaProduitRoot.class,
                model
        );
    }

    @Override
    public InputStream exportRefActaDosageSpcRoot(List<String> entityIds) {
        RefActaDosageSpcRootModel model = new RefActaDosageSpcRootModel();

        return runSimpleExport(
                entityIds,
                RefActaDosageSpcRoot.class,
                model
        );
    }

    @Override
    public InputStream exportRefPrixCarbuCSV(List<String> entityIds) {
        RefPrixCarbuModel model = new RefPrixCarbuModel();

        return runSimpleExport(
                entityIds,
                RefPrixCarbu.class,
                model
        );
    }

    @Override
    public InputStream exportRefPrixEspeceCSV(List<String> entityIds) {
        RefPrixEspeceModel model = new RefPrixEspeceModel();

        return runSimpleExport(
                entityIds,
                RefPrixEspece.class,
                model
        );
    }

    @Override
    public InputStream exportRefPrixFertiMinCSV(List<String> entityIds) {
        RefPrixFertiMinModel model = new RefPrixFertiMinModel();

        return runSimpleExport(
                entityIds,
                RefPrixFertiMin.class,
                model
        );
    }

    @Override
    public InputStream exportRefPrixFertiOrgaCSV(List<String> entityIds) {
        RefPrixFertiOrgaModel model = new RefPrixFertiOrgaModel();

        return runSimpleExport(
                entityIds,
                RefPrixFertiOrga.class,
                model
        );
    }

    @Override
    public InputStream exportRefPrixPhytoCSV(List<String> entityIds) {
        RefPrixPhytoModel model = new RefPrixPhytoModel();

        return runSimpleExport(
                entityIds,
                RefPrixPhyto.class,
                model
        );
    }

    @Override
    public InputStream exportRefPrixIrrigCSV(List<String> entityIds) {
        RefPrixIrrigModel model = new RefPrixIrrigModel();

        return runSimpleExport(
                entityIds,
                RefPrixIrrig.class,
                model
        );
    }

    @Override
    public InputStream exportRefPrixAutreCSV(List<String> entityIds) {
        RefPrixAutreModel model = new RefPrixAutreModel();

        return runSimpleExport(
                entityIds,
                RefPrixAutre.class,
                model
        );
    }

    @Override
    public InputStream exportRefInputUnitPriceUnitConverterCSV(List<String> entityIds) {
        RefInputUnitPriceUnitConverterModel model = new RefInputUnitPriceUnitConverterModel();

        return runSimpleExport(
                entityIds,
                RefInputUnitPriceUnitConverter.class,
                model
        );

    }

    @Override
    public InputStream exportRefEspeceOtherToolsCSV(List<String> entityIds) {
        RefEspeceOtherToolsModel model = new RefEspeceOtherToolsModel();

        return runSimpleExport(
                entityIds,
                RefEspeceOtherTools.class,
                model
        );
    }

    @Override
    public InputStream exportRefEdaplosTypeTraitement(List<String> entityIds) {
        RefEdaplosTypeTraitemenModel model = new RefEdaplosTypeTraitemenModel();

        return runSimpleExport(
                entityIds,
                RefEdaplosTypeTraitement.class,
                model
        );
    }

    @Override
    public InputStream exportRefCattleAnimalType(List<String> entityIds, List<RefAnimalType> animalTypes) {
        return runSimpleExport(
                entityIds,
                RefCattleAnimalType.class,
                new RefCattleAnimalTypeModel(animalTypes)
        );
    }

    @Override
    public InputStream exportRefCattleRationAliment(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefCattleRationAliment.class,
                new RefCattleRationAlimentModel()
        );
    }

    @Override
    public InputStream exportRefMAADosesRefParGroupeCible(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefMAADosesRefParGroupeCible.class,
                new RefMAADosesRefParGroupeCibleModel()
        );
    }

    @Override
    public InputStream exportRefMAABiocontrole(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefMAABiocontrole.class,
                new RefMAABiocontroleModel()
        );
    }

    @Override
    public InputStream exportRefCiblesAgrosystGroupesCiblesMAA(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefCiblesAgrosystGroupesCiblesMAA.class,
                new RefCiblesAgrosystGroupesCiblesMAAModel(true)
        );
    }
    
    @Override
    public InputStream exportRefFeedbackRouter(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefFeedbackRouter.class,
                new RefFeedbackRouterModel(new HashSet<>())
        );
    }

    @Override
    public InputStream exportRefSubstrate(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefSubstrate.class,
                new RefSubstrateModel()
        );
    }

    @Override
    public InputStream exportRefPrixSubstrate(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefPrixSubstrate.class,
                new RefPrixSubstrateModel()
        );
    }

    @Override
    public InputStream exportRefPot(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefPot.class,
                new RefPotModel()
        );
    }

    @Override
    public InputStream exportRefPrixPot(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefPrixPot.class,
                new RefPrixPotModel()
        );
    }

    @Override
    public InputStream exportRefGroupeCibleTraitement(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefGroupeCibleTraitement.class,
                new RefGroupeCibleTraitementModel()
        );
    }

    @Override
    public InputStream exportRefLocation(List<String> entityIds) {
        return runExportRefLocation(entityIds);
    }
    
    @Override
    public InputStream exportRefOtherInput(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefOtherInput.class,
                new RefOtherInputModel(true)
        );
    }

    @Override
    public InputStream exportRefSubstancesActivesCommissionEuropeenne(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefSubstancesActivesCommissionEuropeenne.class,
                new RefSubstancesActivesCommissionEuropeenneModel()
        );
    }

    @Override
    public InputStream exportRefCompositionSubstancesActivesParNumeroAMM(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefCompositionSubstancesActivesParNumeroAMM.class,
                new RefCompositionSubstancesActivesParNumeroAMMModel()
        );
    }

    @Override
    public InputStream exportRefPhrasesRisqueEtClassesMentionDangerParAMM(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefPhrasesRisqueEtClassesMentionDangerParAMM.class,
                new RefPhrasesRisqueEtClassesMentionDangerParAMMModel()
        );
    }

    @Override
    public InputStream exportRefConversionUnitesQSA(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefConversionUnitesQSA.class,
                new RefConversionUnitesQSAModel()
        );
    }

    @Override
    public InputStream exportRefCorrespodanceMaterielOutilsTS(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefCorrespondanceMaterielOutilsTS.class,
                new RefCorrespondanceMaterielOutilsTSModel()
        );
    }

    @Override
    public InputStream exportRefCountry(List<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefCountry.class,
                new RefCountryModel());
    }

    @Override
    public InputStream exportReferentialI18Entry(Class<? extends ReferentialI18nEntry> klass) {
        return runExportReferentialI18Entry(klass);
    }

    @Override
    public InputStream exportRefSeedUnits(Iterable<String> entityIds) {
        return runSimpleExport(
                entityIds,
                RefSeedUnits.class,
                new RefSeedUnitsModel());
    }

}
