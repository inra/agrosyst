/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.services.effective.export;

import com.google.common.collect.Iterables;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.ToolsCouplingTopiaDao;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.action.HarvestingActionValorisationDto;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleConnectionDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleNodeDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCyclePhaseDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleSpeciesDto;
import fr.inra.agrosyst.api.services.effective.EffectiveInterventionDto;
import fr.inra.agrosyst.api.services.effective.EffectivePerennialCropCycleDto;
import fr.inra.agrosyst.api.services.effective.EffectiveSeasonalCropCycleDto;
import fr.inra.agrosyst.api.services.itk.SpeciesStadeDto;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.services.common.export.EntityExporter;
import fr.inra.agrosyst.services.common.export.ExportModelAndRows;
import fr.inra.agrosyst.services.export.ExportMetadata;
import fr.inra.agrosyst.services.export.XlsExporterService;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Setter
public class EffectiveXlsExporter extends XlsExporterService {
    
    private static final Log LOGGER = LogFactory.getLog(EffectiveXlsExporter.class);

    protected AnonymizeService anonymizeService;
    protected EffectiveCropCycleService effectiveCropCycleService;

    protected ZoneTopiaDao zoneDao;
    protected ToolsCouplingTopiaDao toolsCouplingDao;
    protected EffectiveSpeciesStadeTopiaDao effectiveSpeciesStadeDao;

    protected Map<String, String> groupesCiblesParCode;

    public ExportResult exportEffectiveCropCyclesAsXls(Collection<String> effectiveCropCycleIds) {
        groupesCiblesParCode = referentialService.getGroupesCiblesParCode();

        final EffectiveXlsContext exportContext = new EffectiveXlsContext();

        Locale locale = getSecurityContext().getLocale();
        
        try {
            // On découpe en paquets pour alléger la mémoire nécessaire à cet export
            int batchSize = 3;
            /* La taille peut être ajustée, mais après plusieurs tests, une valeur basse semble plus adaptée :
                batchSize =   3  =>  1645 zones en 467s  soit 284ms par zone
                batchSize =   5  =>  1645 zones en 518s  soit 315ms par zone
                batchSize =   7  =>  1645 zones en 523s  soit 318ms par zone
                batchSize =  10  =>  1645 zones en 582s  soit 353ms par zone
                batchSize =  20  =>  1645 zones en 716s  soit 435ms par zone
                batchSize =  25  =>  1645 zones en 711s  soit 432ms par zone
                batchSize =  42  =>  1645 zones en 895s  soit 544ms par zone
                batchSize =  50  =>  1645 zones en 940s  soit 571ms par zone
                batchSize = 100  =>  1645 zones en 1326s soit 806ms par zone
             */

            Iterable<List<String>> batchsList = Iterables.partition(CollectionUtils.emptyIfNull(effectiveCropCycleIds), batchSize);
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info(String.format("%d zones à exporter", effectiveCropCycleIds.size()));
            }

            long start = System.currentTimeMillis();

            for (List<String> batchIds : batchsList) {

                // On fait un nettoyage de session Hibernate à chaque début de paquet -> c'est là qu'on peut le plus tout vider dans la session
                zoneDao.clear();

                List<Zone> zones = zoneDao.forTopiaIdIn(batchIds).findAll();

                for (Zone zone : zones) {

                    // anonymize zone
                    zone = anonymizeService.checkForZoneAnonymization(zone);

                    // common part
                    final Plot plot = zone.getPlot();
                    final Domain domain = plot.getDomain();
                    final GrowingSystem growingSystem = plot.getGrowingSystem();
                    String networkNames = null;
                    String growingSystemName = null;
                    String typeAgricultureLabel = null;
                    String growingPlanName = null;
                    String dephyNumber = null;
                    if (growingSystem != null) {
                        growingSystemName = growingSystem.getName();
                        final RefTypeAgriculture typeAgriculture = growingSystem.getTypeAgriculture();
                        if (typeAgriculture != null) {
                            typeAgricultureLabel = typeAgriculture.getReference_label();
                        }
                        growingPlanName = growingSystem.getGrowingPlan().getName();
                        dephyNumber = growingSystem.getDephyNumber();
                        networkNames = growingSystem.getNetworks().stream()
                                .filter(Objects::nonNull)
                                .map(Network::getName)
                                .distinct()
                                .collect(Collectors.joining(", "));
                    }

                    EffectiveCropCycleExportMetadata.EffectiveBean baseBean = new EffectiveCropCycleExportMetadata.EffectiveBean(
                            zone.getName(),
                            plot.getName(),
                            domain.getLocation().getDepartement(),
                            networkNames,
                            growingSystemName,
                            typeAgricultureLabel,
                            dephyNumber,
                            growingPlanName,
                            domain.getName(),
                            domain.getCampaign()
                    );
    
                    // cycles
                    final String zoneTopiaId = zone.getTopiaId();
                    List<EffectiveSeasonalCropCycleDto> effectiveSeasonalCropCycleDtos = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zoneTopiaId);
                    List<EffectiveCropCycleExportMetadata.CropCycleBean> seasonalCropCycleBeans = exportEffectiveSeasonalCropCycles(effectiveSeasonalCropCycleDtos, baseBean);
                    seasonalCropCycleBeans.forEach(exportContext::addCropCycleNG);
    
                    List<EffectivePerennialCropCycleDto> effectivePerennialCropCycleDtos = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zoneTopiaId);
                    List<EffectiveCropCycleExportMetadata.CropCycleBean> perennialCropCycleBeans = exportEffectivePerennialCropCycles(effectivePerennialCropCycleDtos, baseBean);
                    perennialCropCycleBeans.forEach(exportContext::addCropCycleNG);
    
                    // Espèces des phases de production des cultures pérennes
                    List<ExportMetadata.PerennialCropCycleSpeciesBean> perennialSpecies = exportEffectivePerennialSpecies(effectivePerennialCropCycleDtos, baseBean, locale);
                    perennialSpecies.forEach(exportContext::addPerennialSpecie);

                    // intervention tab
                    for (EffectiveSeasonalCropCycleDto seasonalCycle : effectiveSeasonalCropCycleDtos) {
                        List<EffectiveCropCycleNodeDto> nodes = seasonalCycle.getNodeDtos();
                        if (nodes != null) {
                            for (EffectiveCropCycleNodeDto node : nodes) {
                                List<EffectiveInterventionDto> itks = node.getInterventions();
                                if (itks != null) {
                                    for (EffectiveInterventionDto itk : itks) {
                                        ExportMetadata.Bean baseItkBean = exportCommonInterventionFields(node, null, null, itk, baseBean);
                                        // add tools couplings, species, actions, inputs fields
                                        List<EffectiveCropCycleExportMetadata.ItkBean> effectiveCropCycleExportEntities = exportToolsCouplingsSpeciesActionsInputsFields(domain, baseItkBean, itk);
                                        effectiveCropCycleExportEntities.forEach(exportContext::addItk);
                                        // add actions and inputs fields
                                        Collection<AbstractActionDto> actionDtos = itk.getActionDtos();
                                        Supplier<Map<String, ExportMetadata.SpeciesAndVariety>> refEspeceByCPSCodeSupplier = newRefEspeceByCPSCodeSupplier(itk);
                                        for (AbstractActionDto action : actionDtos) {
                                            exportActionAndInputFields(exportContext, baseItkBean, itk, action, refEspeceByCPSCodeSupplier);
                                        }
                                    }
                                }
                            }
                        }
                    }


                    for (EffectivePerennialCropCycleDto cycle : effectivePerennialCropCycleDtos) {
                        List<EffectiveCropCyclePhaseDto> phases = cycle.getPhaseDtos();
                        if (phases != null) {
                            for (EffectiveCropCyclePhaseDto phase : phases) {
                                List<EffectiveInterventionDto> itks = phase.getInterventions();
                                if (itks != null) {
                                    for (EffectiveInterventionDto itk : itks) {
                                        ExportMetadata.Bean baseItkBean = exportCommonInterventionFields(null, phase, cycle, itk, baseBean);
                                        // add tools couplings, species, actions, inputs fields
                                        List<EffectiveCropCycleExportMetadata.ItkBean> effectiveCropCycleExportEntities = exportToolsCouplingsSpeciesActionsInputsFields(domain, baseItkBean, itk);
                                        effectiveCropCycleExportEntities.forEach(exportContext::addItk);
                                        // add actions and inputs fields
                                        Collection<AbstractActionDto> actionDtos = itk.getActionDtos();
                                        Supplier<Map<String, ExportMetadata.SpeciesAndVariety>> refEspeceByCPSCodeSupplier = newRefEspeceByCPSCodeSupplier(itk);
                                        for (AbstractActionDto action : actionDtos) {
                                            exportActionAndInputFields(exportContext, baseItkBean, itk, action, refEspeceByCPSCodeSupplier);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            long end = System.currentTimeMillis();
            if (CollectionUtils.isNotEmpty(effectiveCropCycleIds) && LOGGER.isInfoEnabled()) {
                String message = String.format("%d zones en %ds : %dms par zone", effectiveCropCycleIds.size(), (end - start) / 1000, (end - start) / effectiveCropCycleIds.size());
                LOGGER.info(message);
            }
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't copy properties", ex);
        }

        // technical export
        EntityExporter exporter = new EntityExporter();

        ExportModelAndRows<EffectiveCropCycleExportMetadata.CropCycleBean> cropCycles = new ExportModelAndRows<>(new EffectiveCropCycleExportMetadata.CropCycleModel(), exportContext.getCropCycles());
        ExportModelAndRows<ExportMetadata.PerennialCropCycleSpeciesBean> perennialCropCycleSpecies = new ExportModelAndRows<>(new EffectiveCropCycleExportMetadata.PerennialCropCycleSpeciesModel(), exportContext.getPerennialCropCycleSpecies());
        ExportModelAndRows<EffectiveCropCycleExportMetadata.ItkBean> itks = new ExportModelAndRows<>(new EffectiveCropCycleExportMetadata.ItkModel(), exportContext.getItks());
        ExportModelAndRows<ExportMetadata.ItkActionApplicationProduitsMinerauxBean> applicationProduitsMineraux = new ExportModelAndRows<>(new EffectiveCropCycleExportMetadata.ItkActionApplicationProduitsMinerauxModel(), exportContext.getApplicationProduitsMineraux());
        ExportModelAndRows<ExportMetadata.ItkApplicationProduitsPhytoBean> applicationProduitsPhytos = new ExportModelAndRows<>(new EffectiveCropCycleExportMetadata.ItkActionApplicationProduitsPhytoModel(), exportContext.getApplicationProduitsPhytos());
        ExportModelAndRows<ExportMetadata.ItkActionAutreBean> actionAutres = new ExportModelAndRows<>(new EffectiveCropCycleExportMetadata.ItkActionAutreModel(), exportContext.getActionAutres());
        ExportModelAndRows<ExportMetadata.ItkActionEntretienTailleVigneBean> actionEntretienTailleVignes = new ExportModelAndRows<>(new EffectiveCropCycleExportMetadata.ItkActionEntretienTailleVigneModel(), exportContext.getActionEntretienTailleVignes());
        ExportModelAndRows<ExportMetadata.ItkActionEpandageOrganiqueBean> actionEpandageOrganiques = new ExportModelAndRows<>(new EffectiveCropCycleExportMetadata.ItkActionEpandageOrganiqueModel(), exportContext.getActionEpandageOrganiques());
        ExportModelAndRows<ExportMetadata.ItkActionIrrigationBean> actionIrrigations = new ExportModelAndRows<>(new EffectiveCropCycleExportMetadata.ItkActionIrrigationModel(), exportContext.getActionIrrigations());
        ExportModelAndRows<ExportMetadata.ItkActionLutteBiologiqueBean> actionLutteBiologique = new ExportModelAndRows<>(new EffectiveCropCycleExportMetadata.ItkActionLutteBiologiqueModel(), exportContext.getActionLutteBiologiques());
        ExportModelAndRows<ExportMetadata.ItkActionRecolteBean> actionRecoltes = new ExportModelAndRows<>(new EffectiveCropCycleExportMetadata.ItkActionRecolteModel(), exportContext.getActionRecoltes());
        ExportModelAndRows<ExportMetadata.ItkActionSemiBean> actionSemis = new ExportModelAndRows<>(new EffectiveCropCycleExportMetadata.ItkActionSemiModel(), exportContext.getActionSemis());
        ExportModelAndRows<ExportMetadata.ItkActionTransportBean> actionTransports = new ExportModelAndRows<>(new EffectiveCropCycleExportMetadata.ItkActionTransportModel(), exportContext.getActionTransports());
        ExportModelAndRows<ExportMetadata.ItkActionTravailSolBean> actionTravailSols = new ExportModelAndRows<>(new EffectiveCropCycleExportMetadata.ItkActionTravailSolModel(), exportContext.getActionTravailSolBeans());

        ExportResult result = exporter.exportAsXlsx(
                "Interventions-culturales-realisees-export",
                cropCycles,
                perennialCropCycleSpecies,
                itks,
                applicationProduitsMineraux,
                applicationProduitsPhytos,
                actionAutres,
                actionEntretienTailleVignes,
                actionEpandageOrganiques,
                actionIrrigations,
                actionLutteBiologique,
                actionRecoltes,
                actionSemis,
                actionTransports,
                actionTravailSols
        );
        return result;
    }

    private Supplier<Map<String, ExportMetadata.SpeciesAndVariety>> newRefEspeceByCPSCodeSupplier(EffectiveInterventionDto intervention) {
        return new Supplier<>() {

            Map<String, ExportMetadata.SpeciesAndVariety> result;

            @Override
            public Map<String, ExportMetadata.SpeciesAndVariety> get() {
                if (result == null) {
                    Map<String, ExportMetadata.SpeciesAndVariety> refEspeceByCPSCode = new HashMap<>();
                    List<SpeciesStadeDto> speciesStadeDtos = ListUtils.emptyIfNull(intervention.getSpeciesStadesDtos());
                    Set<String> effectiveSpeciesStadeIds = speciesStadeDtos.stream().map(SpeciesStadeDto::getTopiaId).collect(Collectors.toSet());
                    List<EffectiveSpeciesStade> effectiveSpeciesStades = effectiveSpeciesStadeDao.forTopiaIdIn(effectiveSpeciesStadeIds).findAll();
                    effectiveSpeciesStades.stream()
                            .map(EffectiveSpeciesStade::getCroppingPlanSpecies)
                            .forEach(cpSpecies -> refEspeceByCPSCode.put(cpSpecies.getCode(), new ExportMetadata.SpeciesAndVariety(cpSpecies.getSpecies(), null, null)));

                    result = refEspeceByCPSCode;
                }
                return result;
            }
        };
    }

    @Override
    protected void fillHarvestingActionSpeciesAndVarieties(ExportMetadata.ItkActionRecolteBean newBean, List<SpeciesStadeDto> speciesStadeDtos, Map<String, ExportMetadata.SpeciesAndVariety> refEspeceByCPSCode, HarvestingActionValorisationDto valorisation) {
        speciesStadeDtos.stream()
                .filter(dto -> dto.getSpeciesCode().equals(valorisation.getSpeciesCode()))
                .findFirst()
                .ifPresent(speciesStadeDto -> {
                    newBean.setActionSpecies(speciesStadeDto.getSpeciesName());
                    newBean.setActionVarieties(speciesStadeDto.getVarietyName());
                });
    }

    protected List<ExportMetadata.PerennialCropCycleSpeciesBean> exportEffectivePerennialSpecies(
            List<EffectivePerennialCropCycleDto> effectivePerennialCropCycleDtos,
            EffectiveCropCycleExportMetadata.EffectiveBean baseBean,
            Locale locale) {
        List<ExportMetadata.PerennialCropCycleSpeciesBean> perennialSpecies = new LinkedList<>();
        for (EffectivePerennialCropCycleDto perennialCropCycleDto : effectivePerennialCropCycleDtos) {
            List<EffectiveCropCycleSpeciesDto> speciesDtos = perennialCropCycleDto.getSpeciesDtos();
            if (!speciesDtos.isEmpty()) {
                String croppingPlanEntryName = perennialCropCycleDto.getCroppingPlanEntryName();
                CropCyclePhaseType phaseType = perennialCropCycleDto.getPhaseDtos().getFirst().getType();

                for (EffectiveCropCycleSpeciesDto speciesDto : speciesDtos) {
                    ExportMetadata.PerennialCropCycleSpeciesBean export = new ExportMetadata.PerennialCropCycleSpeciesBean(baseBean);
                    export.setCroppingPlanEntryName(croppingPlanEntryName);
                    export.setPhase(phaseType);
                    export.setSpeciesEspece(speciesDto.getSpeciesEspece());
                    export.setSpeciesQualifiant(speciesDto.getSpeciesQualifiant());
                    export.setSpeciesTypeSaisonnier(speciesDto.getSpeciesTypeSaisonnier());
                    export.setSpeciesDestination(speciesDto.getSpeciesDestination());
                    export.setVarietyLibelle(
                    StringUtils.isNotBlank(speciesDto.getVarietyLibelle()) ?
                            speciesDto.getVarietyLibelle() :
                            I18n.l(locale, "fr.inra.agrosyst.api.services.edaplos.unknownVariety", speciesDto.getEdaplosUnknownVariety())
                    );


                    String graftSupport = speciesDto.getGraftSupport() != null ? speciesDto.getGraftSupport().getLabel() : null;
                    export.setGraftSupport(graftSupport);

                    String graftClone = speciesDto.getGraftClone() != null ? speciesDto.getGraftClone().getLabel() : null;
                    export.setGraftClone(graftClone);
                    
                    export.setPlantCertified(speciesDto.isPlantsCertified());
                    export.setOverGraftDate(speciesDto.getOverGraftDate());
                    perennialSpecies.add(export);
                }
            }
        }
        return perennialSpecies;
    }

    protected List<EffectiveCropCycleExportMetadata.CropCycleBean> exportEffectiveSeasonalCropCycles(
            List<EffectiveSeasonalCropCycleDto> seasonalCropCycleDtos,
            EffectiveCropCycleExportMetadata.EffectiveBean baseBean
    ) {
        List<EffectiveCropCycleExportMetadata.CropCycleBean> cropCycles = new LinkedList<>();
        for (EffectiveSeasonalCropCycleDto seasonalCropCycleDto : seasonalCropCycleDtos) {

            List<EffectiveCropCycleNodeDto> nodeDtos = seasonalCropCycleDto.getNodeDtos();
            Collection<EffectiveCropCycleConnectionDto> connectionDtos = CollectionUtils.emptyIfNull(seasonalCropCycleDto.getConnectionDtos());

            // create export entity for seasonal crop cycle
            Map<String, EffectiveSeasonalCropCycleExport> seasonalCropCycleExports = new HashMap<>();
            for (EffectiveCropCycleNodeDto nodeDto : nodeDtos) {
                EffectiveSeasonalCropCycleExport nodeExport = new EffectiveSeasonalCropCycleExport();
                nodeExport.setCropName(nodeDto.getLabel());
                nodeExport.setRank(nodeDto.getX());

                seasonalCropCycleExports.put(nodeDto.getNodeId(), nodeExport);
            }
            for (EffectiveCropCycleConnectionDto connectionDto : connectionDtos) {
                // set intermediate crop cycle name if necessary
                if (StringUtils.isNotBlank(connectionDto.getIntermediateCroppingPlanEntryName())) {
                    EffectiveSeasonalCropCycleExport nodeExport = seasonalCropCycleExports.get(connectionDto.getTargetId());
                    nodeExport.setIntermediateCropName(connectionDto.getIntermediateCroppingPlanEntryName());
                }
            }

            List<EffectiveSeasonalCropCycleExport> nodeExports = new ArrayList<>(seasonalCropCycleExports.values());
            if (!CollectionUtils.isEmpty(nodeExports)) {
                for (EffectiveSeasonalCropCycleExport seasonalCropCycleExport : nodeExports) {

                    EffectiveCropCycleExportMetadata.CropCycleBean export = new EffectiveCropCycleExportMetadata.CropCycleBean(baseBean);
                    export.setCycleType("Assolé");
                    export.setCropName(seasonalCropCycleExport.getCropName());
                    export.setIntermediateCropName(seasonalCropCycleExport.getIntermediateCropName());
                    Integer rank = seasonalCropCycleExport.getRank();
                    export.setRank(rank == null ? null : rank + 1);
                    cropCycles.add(export);
                }
            }

        }
        return cropCycles;
    }

    protected List<EffectiveCropCycleExportMetadata.CropCycleBean> exportEffectivePerennialCropCycles(List<EffectivePerennialCropCycleDto> perennialCropCycleDtos,
                                                                                    EffectiveCropCycleExportMetadata.EffectiveBean baseBean) {
        List<EffectiveCropCycleExportMetadata.CropCycleBean> cropCycles = new LinkedList<>();
        for (EffectivePerennialCropCycleDto perennialCropCycleDto : perennialCropCycleDtos) {

            String cropName = perennialCropCycleDto.getCroppingPlanEntryName();

            List<EffectiveCropCyclePhaseDto> phaseDtos = perennialCropCycleDto.getPhaseDtos();

            // create export entity for seasonal crop cycle
            for (EffectiveCropCyclePhaseDto phaseDto : phaseDtos) {

                EffectiveCropCycleExportMetadata.CropCycleBean export = new EffectiveCropCycleExportMetadata.CropCycleBean(baseBean);
                export.setCycleType("Pérenne");
                export.setCropName(cropName);
                export.setPhase(phaseDto.getType());
                export.setDuration(phaseDto.getDuration());

                export.setDto(perennialCropCycleDto);

                cropCycles.add(export);

            }

        }
        return cropCycles;
    }

    protected List<EffectiveCropCycleExportMetadata.ItkBean> exportToolsCouplingsSpeciesActionsInputsFields(Domain domain,
                                                                                                         ExportMetadata.Bean baseBean,
                                                                                                         EffectiveInterventionDto itk) {

        // intervention
        EffectiveCropCycleExportMetadata.ItkBean exportIntervention = new EffectiveCropCycleExportMetadata.ItkBean(baseBean);
        exportIntervention.setComment(itk.getComment());
        exportIntervention.setSpatialFrequency(itk.getSpatialFrequency());
        exportIntervention.setTransitCount(itk.getTransitCount());
        exportIntervention.setWorkRate(itk.getWorkRate());
        exportIntervention.setWorkRateUnit(itk.getWorkRateUnit());
        exportIntervention.setProgressionSpeed(itk.getProgressionSpeed());
        exportIntervention.setInvolvedPeopleNumber(itk.getInvolvedPeopleCount());

        exportIntervention.setIntermediateCrop(itk.isIntermediateCrop());

        // FIXME pas de place dans le modèle ?
//        exportIntervention.set(itk.getEdaplosIssuerId());

        Double psci = itk.getTransitCount() * itk.getSpatialFrequency();
        exportIntervention.setPsci(psci);

        List<EffectiveCropCycleExportMetadata.ItkBean> itkEntities = new LinkedList<>();
        // tools coupling
        if (CollectionUtils.isEmpty(itk.getToolsCouplingCodes())) {
            // add species, actions, inputs fields
            List<EffectiveCropCycleExportMetadata.ItkBean> subList = exportSpeciesActionsInputsFields(itk, exportIntervention);
            itkEntities.addAll(subList);
        } else {
            Set<String> tcCodes = itk.getToolsCouplingCodes();
            for (String tcCode : tcCodes) {
                ToolsCoupling toolsCoupling = toolsCouplingDao.forProperties(ToolsCoupling.PROPERTY_CODE, tcCode, ToolsCoupling.PROPERTY_DOMAIN, domain)
                        .findAny();
                exportIntervention.setToolsCouplingName(toolsCoupling.getToolsCouplingName());
                // add species, actions, inputs fields
                List<EffectiveCropCycleExportMetadata.ItkBean> subList = exportSpeciesActionsInputsFields(itk, exportIntervention);
                itkEntities.addAll(subList);
            }
        }

        return itkEntities;
    }

    protected List<EffectiveCropCycleExportMetadata.ItkBean> exportSpeciesActionsInputsFields(EffectiveInterventionDto itk, EffectiveCropCycleExportMetadata.ItkBean baseItkBean){
        List<EffectiveCropCycleExportMetadata.ItkBean> itkEntities = new LinkedList<>();

        List<SpeciesStadeDto> speciesStadesDtos = itk.getSpeciesStadesDtos();
        if (CollectionUtils.isEmpty(speciesStadesDtos)) {
            itkEntities.add(baseItkBean);
        } else {
            for (SpeciesStadeDto stade : speciesStadesDtos) {
                EffectiveCropCycleExportMetadata.ItkBean itkBean = new EffectiveCropCycleExportMetadata.ItkBean(baseItkBean);
                itkBean.setSpeciesName(stade.getMixSpeciesName());
                itkBean.setItkSpecies(stade.getSpeciesName());
                itkBean.setItkVariety(stade.getVarietyName());

                String stadeMin = stade.getStadeMin() != null ? stade.getStadeMin().getLabel() : null;
                itkBean.setStadeMin(stadeMin);
                String stadeMax = stade.getStadeMax() != null ? stade.getStadeMax().getLabel() : null;
                itkBean.setStadeMax(stadeMax);
                itkEntities.add(itkBean);
            }
        }
        return itkEntities;
    }

    /**
     * Extract common part of intervention for all actions.
     *
     * @param node               node (can be null)
     * @param phase              phase (can be null)
     * @param perennialCropCycle cycle (can be null)
     * @param itk                itk
     * @param baseBean           common base model
     * @return common intervention model
     */
    protected ExportMetadata.Bean exportCommonInterventionFields(EffectiveCropCycleNodeDto node,
                                                                 EffectiveCropCyclePhaseDto phase,
                                                                 EffectivePerennialCropCycleDto perennialCropCycle,
                                                                 EffectiveInterventionDto itk,
                                                                 EffectiveCropCycleExportMetadata.EffectiveBean baseBean) {
        ExportMetadata.Bean export =  new ExportMetadata.Bean(baseBean);
        String cycle = node != null ? "Assolé" : "Pérenne";
        export.setItkCycle(cycle);

        String cropField = node != null ? node.getLabel() : perennialCropCycle.getCroppingPlanEntryName();
        export.setItkCrop(cropField);

        Integer rank = node != null ? node.getX() : null;
        export.setItkRank(rank == null ? null : rank + 1);

        CropCyclePhaseType phaseType = phase != null ? phase.getType() : null;
        export.setItkPhase(phaseType);

        export.setInterventionName(itk.getName());
        export.setType(itk.getType());
        export.setIntermediateCrop(itk.isIntermediateCrop());

        String shortId = getPersistenceContext().getTopiaIdFactory().getRandomPart(itk.getTopiaId());
        export.setInterventionId(shortId);
        export.setItkStart(itk.getStartInterventionDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        export.setItkEnd(itk.getEndInterventionDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        return export;
    }

}
