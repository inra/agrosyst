package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrapeImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefClonePlantGrapeModel extends AbstractAgrosystModel<RefClonePlantGrape> implements ExportModel<RefClonePlantGrape> {

    public RefClonePlantGrapeModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("CodeClone", RefClonePlantGrape.PROPERTY_CODE_CLONE, INT_PARSER);
        newMandatoryColumn("CodeVar", RefClonePlantGrape.PROPERTY_CODE_VAR, INT_PARSER);
        newMandatoryColumn("AnneeAgrement", RefClonePlantGrape.PROPERTY_ANNEE_AGREMENT, INT_PARSER);
        newMandatoryColumn("Origine", RefClonePlantGrape.PROPERTY_ORIGINE);
        newMandatoryColumn("source", RefClonePlantGrape.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefClonePlantGrape.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public RefClonePlantGrape newEmptyInstance() {
        RefClonePlantGrapeImpl result = new RefClonePlantGrapeImpl();
        result.setActive(true);
        return result;
    }

    @Override
    public Iterable<ExportableColumn<RefClonePlantGrape, Object>> getColumnsForExport() {
        ModelBuilder<RefClonePlantGrape> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("CodeClone", RefClonePlantGrape.PROPERTY_CODE_CLONE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("CodeVar", RefClonePlantGrape.PROPERTY_CODE_VAR, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("AnneeAgrement", RefClonePlantGrape.PROPERTY_ANNEE_AGREMENT, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Origine", RefClonePlantGrape.PROPERTY_ORIGINE);
        modelBuilder.newColumnForExport("source", RefClonePlantGrape.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefClonePlantGrape.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
}
