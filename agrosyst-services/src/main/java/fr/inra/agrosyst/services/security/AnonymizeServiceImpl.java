package fr.inra.agrosyst.services.security;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.google.common.hash.Hashing;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainImpl;
import fr.inra.agrosyst.api.entities.GeoPoint;
import fr.inra.agrosyst.api.entities.GeoPointImpl;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingPlanImpl;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemImpl;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PlotImpl;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneImpl;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeImpl;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemImpl;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationImpl;
import fr.inra.agrosyst.api.entities.security.HashedValueTopiaDao;
import fr.inra.agrosyst.api.services.domain.DomainDto;
import fr.inra.agrosyst.api.services.domain.PlotDto;
import fr.inra.agrosyst.api.services.domain.ZoneDto;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanDto;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemDto;
import fr.inra.agrosyst.api.services.performance.PerformanceDto;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemDto;
import fr.inra.agrosyst.api.services.security.AgrosystAccessDeniedException;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.nuiton.util.pagination.PaginationResult;

import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 * @since 0.8
 */
@Setter
public class AnonymizeServiceImpl extends AuthorizationServiceImpl implements AnonymizeService {

    protected static final String XXXXX = "xxxxx";
    protected static final int XXXXX_INT = -88888;
    protected static final double XXXXX_DOUBLE = -8888.8d;

    protected static final Function<RefLocation, RefLocation> ANONYMIZE_LOCATION = input -> {
        Binder<RefLocation, RefLocation> gpsDataBinder = BinderFactory.newBinder(RefLocation.class);
        RefLocation result = new RefLocationImpl();
        gpsDataBinder.copyExcluding(input, result,
                RefLocation.PROPERTY_LATITUDE,
                RefLocation.PROPERTY_LONGITUDE,
                RefLocation.PROPERTY_CODE_INSEE,
                RefLocation.PROPERTY_CODE_POSTAL,
                RefLocation.PROPERTY_COMMUNE
        );

        result.setLongitude(XXXXX_DOUBLE);
        result.setLatitude(XXXXX_DOUBLE);
        result.setCodeInsee(XXXXX);
        result.setCodePostal(XXXXX);
        result.setCommune(XXXXX);
        return result;
    };

    protected BusinessAuthorizationService authorizationService;

    protected HashedValueTopiaDao hashedValuesDao;
    
    public AnonymizeContext newContext() {
        return new AnonymizeContext(authorizationService, getAnonymizeFunction());
    }

    public Function<String, String> getAnonymizeFunction() {
        return this::anonymize;
    }

    @Override
    public String anonymize(String clear) {
        String userId = getUserIdAndComputePermissions();
        Preconditions.checkState(!Strings.isNullOrEmpty(userId));

        String hashed = Hashing.crc32().hashString(clear, StandardCharsets.UTF_8).toString();

        if (hashedValuesDao.checkValue(clear, hashed)) {
            getTransaction().commit();
        }

        return hashed;
    }

    protected Function<Domain, Domain> getAnonymizeDomainFunction() {
        return getAnonymizeDomainFunction(false);
    }

    protected Function<Domain, Domain> getAnonymizeDomainFunction(final boolean allowUnreadable) {
        return input -> {

            if (input == null) {
                return null;
            }
            Domain result1;
            String domainId = input.getTopiaId();
            Pair<Boolean, Boolean> anonymizeDomain = authorizationService.getShouldAnonymizeCanReadDomain(domainId, allowUnreadable);
            Boolean shouldAnonymizeDomain = anonymizeDomain.getLeft();
            if (shouldAnonymizeDomain) {
                // AThimel 12/12/13 It is very important to copy entity before changing the value to avoid changes value to be written to DB
                Binder<Domain, Domain> binder = BinderFactory.newBinder(Domain.class);
                result1 = new DomainImpl();
                binder.copyExcluding(input, result1,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        Domain.PROPERTY_NAME,
                        Domain.PROPERTY_MAIN_CONTACT,
                        Domain.PROPERTY_SIRET,
                        Domain.PROPERTY_LOCATION
                );

                result1.setName(anonymize(input.getName()));
                result1.setMainContact(XXXXX);
                result1.setSiret(XXXXX);
                result1.setLocation(ANONYMIZE_LOCATION.apply(input.getLocation()));

                Boolean canReadDomain = anonymizeDomain.getRight();
                if (canReadDomain) {
                    result1.setTopiaId(input.getTopiaId());
                }
            } else {
                // No need to modify instance
                result1 = input;
            }

            return result1;
        };
    }

    @Override
    public Function<Domain, DomainDto> getDomainToDtoFunction(final boolean includeResponsibles) {

        Function<Domain, DomainDto> result;
        try {
            result = newContext().
                    includeDomainResponsibles(includeResponsibles).
                    getDomainToDtoFunction();
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits", e);
        }
        return result;
    }

    @Override
    public Domain checkForDomainAnonymization(Domain domain) {
        Domain result;
        try {
            result = getAnonymizeDomainFunction().apply(domain);
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le Domain" + domain.getTopiaId(), e);
        }
        return result;
    }

    @Override
    public Collection<Domain> checkForDomainsAnonymization(Collection<Domain> domains) {
        Collection<Domain> result;
        try {
            result = domains.stream().map(getAnonymizeDomainFunction()).collect(Collectors.toList());
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur un Domain", e);
        }
        return result;
    }

    @Override
    public PaginationResult<Domain> checkForDomainsAnonymization(PaginationResult<Domain> domains) {
        PaginationResult<Domain> result;
        try {
            result = domains.transform(getAnonymizeDomainFunction());
        }
        catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur un Domain", e);
        }
        return result;
    }

    @Override
    public Map<String, String> getDomainsAsMap(Iterable<Domain> domains) {

        Map<String, String> result = Maps.newLinkedHashMap();

        Domain currentDomain = null;
        try {
            for (Domain domain : domains) {
                String domainId = domain.getTopiaId();
                currentDomain = domain;
                if (authorizationService.shouldAnonymizeDomain(domainId, false)) {
                    result.put(domainId, String.format("%s (%s)", anonymize(domain.getName()), domain.getCampaign()));
                } else {
                    result.put(domainId, String.format("%s (%s)", domain.getName(), domain.getCampaign()));
                }
            }
        } catch (IllegalStateException e) {
            String domainId = currentDomain != null ? currentDomain.getTopiaId() : "?";
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le Domain " + domainId, e);
        }
        return result;
    }

    @Override
    public Plot checkForPlotAnonymization(Plot plot) {
        Plot result;
        try {
            result = getAnonymizePlotFunction().apply(plot);
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur le Plot " + plot.getTopiaId(), e);
        }
        return result;
    }

    @Override
    public List<Plot> checkForPlotsAnonymization(List<Plot> plots) {
        List<Plot> result;
        try {
            result = plots.stream().map(getAnonymizePlotFunction(true)).collect(Collectors.toList());
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur une parcelle", e);
        }
        return result;
    }

    protected Function<GeoPoint, GeoPoint> getAnonymizeGeoPointFunction() {
        return input -> {

            GeoPoint result1 = null;
            if (input != null) {
                Domain domain = input.getDomain();
                String domainId = domain.getTopiaId();
                if (authorizationService.shouldAnonymizeDomain(domainId, false)) {
                    // AThimel 12/12/13 It is very important to copy entity before changing the value to avoid changes value to be written to DB
                    Binder<GeoPoint, GeoPoint> binder = BinderFactory.newBinder(GeoPoint.class);
                    result1 = new GeoPointImpl();
                    binder.copyExcluding(input, result1,
                            GeoPoint.PROPERTY_LATITUDE,
                            GeoPoint.PROPERTY_LONGITUDE,
                            GeoPoint.PROPERTY_DOMAIN
                    );

                    result1.setLatitude(XXXXX_DOUBLE);
                    result1.setLongitude(XXXXX_DOUBLE);
                    result1.setDomain(null); // Do not push domain value
                } else {
                    // No need to modify instance
                    result1 = input;
                }
            }

            return result1;
        };
    }

    @Override
    public List<GeoPoint> checkForGeoPointAnonymization(List<GeoPoint> geoPoints) {
        List<GeoPoint> result;
        try {
            result = geoPoints.stream().map(getAnonymizeGeoPointFunction()).collect(Collectors.toList());
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur une geoPoints", e);
        }
        return result;
    }

    protected Function<Plot, Plot> getAnonymizePlotFunction() {
        return getAnonymizePlotFunction(false);
    }

    protected Function<Plot, Plot> getAnonymizePlotFunction(final boolean allowUnreadable) {
        final Function<Domain, Domain> anonymizeDomainFunction = getAnonymizeDomainFunction(allowUnreadable);
        final Function<GrowingSystem, GrowingSystem> anonymizeGrowingSystemFunction = getAnonymizeGrowingSystemFunction(allowUnreadable);
        return input -> {

            Plot result1 = null;
            if (input != null) {
                Domain domain = input.getDomain();
                String domainId = domain.getTopiaId();
                Pair<Boolean, Boolean> domainReadableStatus = authorizationService.getShouldAnonymizeCanReadDomain(domainId, allowUnreadable);
                boolean shouldAnonymizeDomain = domainReadableStatus.getLeft();

                boolean shouldAnonymizeGrowingPlan = false;
                boolean plotReadable = true;
                if (input.getGrowingSystem() != null) {
                    String growingPlanId = input.getGrowingSystem().getGrowingPlan().getTopiaId();
                    Pair<Boolean, Boolean> gpReadableStatus = authorizationService.getShouldAnonymizeCanReadGrowingPlan(growingPlanId, allowUnreadable);
                    shouldAnonymizeGrowingPlan = gpReadableStatus.getLeft();
                    plotReadable = authorizationService.isGrowingSystemReadable(input.getGrowingSystem().getTopiaId());
                }
                if (shouldAnonymizeDomain || shouldAnonymizeGrowingPlan) {
                    // AThimel 12/12/13 It is very important to copy entity before changing the value to avoid changes value to be written to DB
                    Binder<Plot, Plot> binder = BinderFactory.newBinder(Plot.class);
                    result1 = new PlotImpl();
                    binder.copyExcluding(input, result1,
                            TopiaEntity.PROPERTY_TOPIA_ID,
                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                            TopiaEntity.PROPERTY_TOPIA_VERSION,
                            Plot.PROPERTY_NAME,
                            Plot.PROPERTY_PAC_ILOT_NUMBER,
                            Plot.PROPERTY_LOCATION,
                            Plot.PROPERTY_LATITUDE,
                            Plot.PROPERTY_LONGITUDE,
                            Plot.PROPERTY_DOMAIN,
                            Plot.PROPERTY_GROWING_SYSTEM,
                            Plot.PROPERTY_E_DAPLOS_ISSUER_ID
                    );

                    if (shouldAnonymizeDomain) {
                        result1.setName(anonymize(input.getName()));
                        result1.setPacIlotNumber(XXXXX_INT);
                        result1.setLocation(ANONYMIZE_LOCATION.apply(input.getLocation()));
                        result1.setLatitude(XXXXX_DOUBLE);
                        result1.setLongitude(XXXXX_DOUBLE);
                        result1.setDomain(anonymizeDomainFunction.apply(domain));
                    } else {
                        result1.setName(input.getName());
                        result1.setPacIlotNumber(input.getPacIlotNumber());
                        result1.setLocation(input.getLocation());
                        result1.setLatitude(input.getLatitude());
                        result1.setLongitude(input.getLongitude());
                        result1.setDomain(domain);
                    }

                    if (shouldAnonymizeGrowingPlan) {
                        result1.setGrowingSystem(anonymizeGrowingSystemFunction.apply(input.getGrowingSystem()));
                    } else {
                        result1.setGrowingSystem(input.getGrowingSystem());
                    }

                    if (plotReadable) {
                        result1.setTopiaId(input.getTopiaId());
                    }
                } else {
                    // No need to modify instance
                    result1 = input;
                }
            }

            return result1;
        };
    }

    @Override
    public Function<Plot, PlotDto> getPlotToDtoFunction() {
        Function<Plot, PlotDto> result;
        try {
            result = newContext().
                    getPlotToDtoFunction();
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur une parcelle", e);
        }
        return result;
    }

    protected Function<Zone, Zone> getAnonymizeZoneFunction(boolean allowUnreadable) {
        final Function<Plot, Plot> anonymizePlotFunction = getAnonymizePlotFunction(allowUnreadable);

        return input -> {

            if (input == null) {
                return null;
            }

            Zone result1;
            String domainId = input.getPlot().getDomain().getTopiaId();
            boolean shouldAnonymizeDomain = authorizationService.shouldAnonymizeDomain(domainId, allowUnreadable);
            if (shouldAnonymizeDomain) {
                // AThimel 12/12/13 It is very important to copy entity before changing the value to avoid changes value to be written to DB
                Binder<Zone, Zone> binder = BinderFactory.newBinder(Zone.class);
                result1 = new ZoneImpl();
                binder.copyExcluding(input, result1,
                        Zone.PROPERTY_PLOT
                );

                result1.setPlot(anonymizePlotFunction.apply(input.getPlot()));
            } else {
                // No need to modify instance
                result1 = input;
            }

            return result1;
        };
    }

    @Override
    public Zone checkForZoneAnonymization(Zone zone) {
        Zone result;
        try {
            result = getAnonymizeZoneFunction(true).apply(zone);
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur une zone", e);
        }
        return result;
    }

    @Override
    public Function<Zone, ZoneDto> getZoneToDtoFunction(final boolean includePlot, final boolean allowUnreadable) {
        Function<Zone, ZoneDto> result;
        try {
            result = newContext().
                    includeZonePlot(includePlot).
                    allowUnreadable(allowUnreadable).
                    getZoneToDtoFunction();
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur une zone", e);
        }
        return result;
    }

    protected Function<GrowingPlan, GrowingPlan> getAnonymizeGrowingPlanFunction() {
        return getAnonymizeGrowingPlanFunction(false);
    }

    protected Function<GrowingPlan, GrowingPlan> getAnonymizeGrowingPlanFunction(final boolean allowUnreadable) {
        final Function<Domain, Domain> anonymizeDomainFunction = getAnonymizeDomainFunction(allowUnreadable);
        return input -> {

            if (input == null) {
                return null;
            }
            GrowingPlan result1;
            String growingPlanId = input.getTopiaId();
            String domainId = input.getDomain().getTopiaId();
            Pair<Boolean, Boolean> anonymizeGrowingPlan = authorizationService.getShouldAnonymizeCanReadGrowingPlan(growingPlanId, allowUnreadable);
            boolean shouldAnonymizeGrowingPlan = anonymizeGrowingPlan.getLeft();
            boolean canReadGrowingPlan = anonymizeGrowingPlan.getRight();
            boolean shouldAnonymizeDomain = authorizationService.shouldAnonymizeDomain(domainId, allowUnreadable);
            if (shouldAnonymizeGrowingPlan || shouldAnonymizeDomain) {
                // AThimel 12/12/13 It is very important to copy entity before changing the value to avoid changes value to be written to DB
                Binder<GrowingPlan, GrowingPlan> binder = BinderFactory.newBinder(GrowingPlan.class);
                result1 = new GrowingPlanImpl();
                binder.copyExcluding(input, result1,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        GrowingPlan.PROPERTY_NAME,
                        GrowingPlan.PROPERTY_DOMAIN
                );

                if (shouldAnonymizeGrowingPlan) {
                    result1.setName(anonymize(input.getName()));
                } else {
                    result1.setName(input.getName());
                }

                if (canReadGrowingPlan) {
                    result1.setTopiaId(input.getTopiaId());
                }
                result1.setDomain(anonymizeDomainFunction.apply(input.getDomain()));
            } else {
                // No need to modify instance
                result1 = input;
            }

            return result1;
        };
    }

    @Override
    public GrowingPlan checkForGrowingPlanAnonymization(GrowingPlan growingPlan) {
        GrowingPlan result;
        try {
            result = getAnonymizeGrowingPlanFunction().apply(growingPlan);
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur un dispositif", e);
        }
        return result;
    }

    @Override
    public Function<GrowingPlan, GrowingPlanDto> getGrowingPlanToDtoFunction(final boolean includeResponsibles) {
        Function<GrowingPlan, GrowingPlanDto> result;
        try {
            result = newContext().
                    includeGrowingPlanResponsibles(includeResponsibles).
                    getGrowingPlanToDtoFunction();
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur un dispositif", e);
        }
        return result;
    }

    @Override
    public Map<String, String> getGrowingPlansAsMap(Iterable<GrowingPlan> growingPlans) {
        Map<String, String> result;
        try {
            result = Maps.newLinkedHashMap();
            for (GrowingPlan growingPlan : growingPlans) {
                String growingPlanId = growingPlan.getTopiaId();
                if (authorizationService.shouldAnonymizeGrowingPlan(growingPlanId, false)) {
                    result.put(growingPlanId, String.format("%s (%s)", anonymize(growingPlan.getName()), growingPlan.getDomain().getCampaign()));
                } else {
                    result.put(growingPlanId, String.format("%s (%s)", growingPlan.getName(), growingPlan.getDomain().getCampaign()));
                }
            }
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur un dispositif", e);
        }
        return result;
    }

    protected Function<GrowingSystem, GrowingSystem> getAnonymizeGrowingSystemFunction() {
        return getAnonymizeGrowingSystemFunction(true);
    }

    protected Function<GrowingSystem, GrowingSystem> getAnonymizeGrowingSystemFunction(final boolean allowUnreadable) {
        final Function<GrowingPlan, GrowingPlan> anonymizeGrowingPlanFunction = getAnonymizeGrowingPlanFunction(allowUnreadable);
        return input -> {

            if (input == null) {
                return null;
            }
            GrowingSystem result1;
            String growingPlanId = input.getGrowingPlan().getTopiaId();
            String domainId = input.getGrowingPlan().getDomain().getTopiaId();
            boolean shouldAnonymizeGrowingPlan = authorizationService.shouldAnonymizeGrowingPlan(growingPlanId, allowUnreadable);
            boolean shouldAnonymizeDomain = authorizationService.shouldAnonymizeDomain(domainId, allowUnreadable);
            if (shouldAnonymizeGrowingPlan || shouldAnonymizeDomain) {
                // AThimel 12/12/13 It is very important to copy entity before changing the value to avoid changes value to be written to DB
                Binder<GrowingSystem, GrowingSystem> binder = BinderFactory.newBinder(GrowingSystem.class);
                result1 = new GrowingSystemImpl();
                binder.copyExcluding(input, result1,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        GrowingSystem.PROPERTY_GROWING_PLAN
                );

                if (authorizationService.isGrowingSystemReadable(input.getTopiaId())) {
                    result1.setTopiaId(input.getTopiaId());
                }

                result1.setGrowingPlan(anonymizeGrowingPlanFunction.apply(input.getGrowingPlan()));
            } else {
                // No need to modify instance
                result1 = input;
            }

            return result1;
        };
    }

    @Override
    public GrowingSystem checkForGrowingSystemAnonymization(GrowingSystem growingSystem) {
        GrowingSystem result;
        try {
            result = getAnonymizeGrowingSystemFunction().apply(growingSystem);
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur un SDC", e);
        }
        return result;
    }

    @Override
    public Function<GrowingSystem, GrowingSystemDto> getGrowingSystemToDtoFunction() {
        Function<GrowingSystem, GrowingSystemDto> result;
        try {
            result = newContext().
                    allowUnreadable(true).
                    getGrowingSystemToDtoFunction();
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur un SDC", e);
        }
        return result;
    }

    protected Function<PracticedSystem, PracticedSystem> getAnonymizePracticedSystemFunction() {
        final Function<GrowingSystem, GrowingSystem> anonymizeGrowingSystemFunction = getAnonymizeGrowingSystemFunction();
        return input -> {

            if (input == null) {
                return null;
            }
            PracticedSystem result1;
            String growingPlanId = input.getGrowingSystem().getGrowingPlan().getTopiaId();
            String domainId = input.getGrowingSystem().getGrowingPlan().getDomain().getTopiaId();
            boolean shouldAnonymizeGrowingPlan = authorizationService.shouldAnonymizeGrowingPlan(growingPlanId, true);
            boolean shouldAnonymizeDomain = authorizationService.shouldAnonymizeDomain(domainId, true);
            if (shouldAnonymizeGrowingPlan || shouldAnonymizeDomain) {
                // AThimel 12/12/13 It is very important to copy entity before changing the value to avoid changes value to be written to DB
                Binder<PracticedSystem, PracticedSystem> binder = BinderFactory.newBinder(PracticedSystem.class);
                result1 = new PracticedSystemImpl();
                binder.copyExcluding(input, result1, PracticedSystem.PROPERTY_GROWING_SYSTEM);
                result1.setGrowingSystem(anonymizeGrowingSystemFunction.apply(input.getGrowingSystem()));
            } else {
                // No need to modify instance
                result1 = input;
            }

            return result1;
        };
    }

    @Override
    public PracticedSystem checkForPracticedSystemAnonymization(PracticedSystem practicedSystem) {
        PracticedSystem result;
        try {
            result = getAnonymizePracticedSystemFunction().apply(practicedSystem);
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur un synthétisé", e);
        }
        return result;
    }

    @Override
    public Function<PracticedSystem, PracticedSystemDto> getPracticedSystemToDtoFunction(boolean allowUnreadable) {
        return newContext().
        allowUnreadable(allowUnreadable).
        getPracticedSystemToDtoFunction();
    }

    protected Function<ManagementMode, ManagementMode> getAnonymizeManagementModeFunction() {
        final Function<GrowingSystem, GrowingSystem> anonymizeGrowingSystemFunction = getAnonymizeGrowingSystemFunction();
        return input -> {

            if (input == null) {
                return null;
            }
            ManagementMode result1;
            String growingPlanId = input.getGrowingSystem().getGrowingPlan().getTopiaId();
            String domainId = input.getGrowingSystem().getGrowingPlan().getDomain().getTopiaId();
            boolean shouldAnonymizeGrowingPlan = authorizationService.shouldAnonymizeGrowingPlan(growingPlanId, false);
            boolean shouldAnonymizeDomain = authorizationService.shouldAnonymizeDomain(domainId, false);
            if (shouldAnonymizeGrowingPlan || shouldAnonymizeDomain) {
                // AThimel 12/12/13 It is very important to copy entity before changing the value to avoid changes value to be written to DB
                Binder<ManagementMode, ManagementMode> binder = BinderFactory.newBinder(ManagementMode.class);
                result1 = new ManagementModeImpl();
                binder.copyExcluding(input, result1,
                        ManagementMode.PROPERTY_GROWING_SYSTEM
                );

                result1.setGrowingSystem(anonymizeGrowingSystemFunction.apply(input.getGrowingSystem()));
            } else {
                // No need to modify instance
                result1 = input;
            }

            return result1;
        };
    }

    @Override
    public ManagementMode checkForManagementModeAnonymization(ManagementMode managementMode) {
        ManagementMode result;
        try {
            result = getAnonymizeManagementModeFunction().apply(managementMode);
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur un mode de gestion", e);
        }
        return result;
    }

    @Override
    public Function<PracticedPlot, PracticedPlotDto> getPracticedPlotToDtoFunction() {
        Function<PracticedPlot, PracticedPlotDto> result;
        try {
            result = newContext().getPracticedPlotToDtoFunction();
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits sur une parcelle type", e);
        }
        return result;
    }


    @Override
    public PaginationResult<Performance> checkForPerformancesAnonymization(PaginationResult<Performance> performances) {
        PaginationResult<Performance> result;
        try {
            result = performances.transform(getAnonymizePerformanceFunction());
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits", e);
        }
        return result;
    }

    protected Function<Performance, Performance> getAnonymizePerformanceFunction() {
        final Function<Domain, Domain> anonymizeDomainFunction = getAnonymizeDomainFunction();
        final Function<GrowingSystem, GrowingSystem> anonymizeGrowingSystemFunction = getAnonymizeGrowingSystemFunction();
        final Function<Plot, Plot> anonymizePlotFunction = getAnonymizePlotFunction();
        final Function<Zone, Zone> anonymizeZoneFunction = getAnonymizeZoneFunction(false);

        return input -> {

            Performance result1 = null;
            if (input != null) {

                Binder<Performance, Performance> binder = BinderFactory.newBinder(Performance.class);
                result1 = new PerformanceImpl();
                binder.copyExcluding(input, result1,
                        Performance.PROPERTY_DOMAINS,
                        Performance.PROPERTY_GROWING_SYSTEMS,
                        Performance.PROPERTY_PLOTS,
                        Performance.PROPERTY_ZONES
                );

                if (input.getDomains() != null) {
                    result1.setDomains(input.getDomains().stream().map(anonymizeDomainFunction).collect(Collectors.toList()));
                }
                if (input.getGrowingSystems() != null) {
                    result1.setGrowingSystems(input.getGrowingSystems().stream().map(anonymizeGrowingSystemFunction).collect(Collectors.toList()));
                }
                if (input.getPlots() != null) {
                    result1.setPlots(input.getPlots().stream().map(anonymizePlotFunction).collect(Collectors.toList()));
                }
                if (input.getZones() != null) {
                    result1.setZones(input.getZones().stream().map(anonymizeZoneFunction).collect(Collectors.toList()));
                }
            }
            return result1;
        };
    }

    @Override
    public Function<Performance, PerformanceDto> getPerformanceToDtoFunction(int subItemsLimit) {
        Function<Performance, PerformanceDto> result;
        try {
            result = newContext().
                    allowUnreadable(true).
                    includeZonePlot(false).
                    limitPerformanceElements(subItemsLimit).
                    getPerformanceToDtoFunction();
        } catch (IllegalStateException e) {
            throw new AgrosystAccessDeniedException("L'utilisateur n'a pas les droits", e);
        }
        return result;
    }

}
