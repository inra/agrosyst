package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefOtherInputImpl;
import fr.inra.agrosyst.api.entities.referential.RefPrixPot;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class RefOtherInputModel extends AbstractAgrosystModel<RefOtherInput> implements ExportModel<RefOtherInput> {

    /**
     * L'export pour l'interface d'administration doit avoir toutes les colonnes.
     */
    private final boolean isExportForAdmin;
    
    public RefOtherInputModel(boolean isExportForAdmin) {
        super(CSV_SEPARATOR);

        this.isExportForAdmin = isExportForAdmin;

        if (this.isExportForAdmin) {
            newMandatoryColumn("reference_id", RefOtherInput.PROPERTY_REFERENCE_ID);
            newMandatoryColumn("reference_code", RefOtherInput.PROPERTY_REFERENCE_CODE);
        }

        newMandatoryColumn("Type_Intrants", RefOtherInput.PROPERTY_INPUT_TYPE_C0);
        newMandatoryColumn("Caracteristique_1", RefOtherInput.PROPERTY_CARACTERISTIC1);
        newMandatoryColumn("Caracteristique_2", RefOtherInput.PROPERTY_CARACTERISTIC2);
        newMandatoryColumn("Caracteristique_3", RefOtherInput.PROPERTY_CARACTERISTIC3);
        newMandatoryColumn("Unite", RefOtherInput.PROPERTY_UNIT, AGROSYST_OTHER_PRODUCT_INPUT_UNIT_PARSER);
        newMandatoryColumn("Duree de vie", RefOtherInput.PROPERTY_LIFETIME, DOUBLE_PARSER);
        newMandatoryColumn("filieres", RefOtherInput.PROPERTY_SECTOR, AGROSYST_SECTORS_PARSER);
        newMandatoryColumn("source", RefOtherInput.PROPERTY_SOURCE);

        if (this.isExportForAdmin) {
            newOptionalColumn(COLUMN_ACTIVE, RefOtherInput.PROPERTY_ACTIVE, ACTIVE_PARSER);
        }
    }
    
    @Override
    public Iterable<ExportableColumn<RefOtherInput, Object>> getColumnsForExport() {
        ModelBuilder<RefPrixPot> modelBuilder = new ModelBuilder<>();

        if (this.isExportForAdmin) {
            modelBuilder.newColumnForExport("reference_id", RefOtherInput.PROPERTY_REFERENCE_ID);
            modelBuilder.newColumnForExport("reference_code", RefOtherInput.PROPERTY_REFERENCE_CODE);
        }

        modelBuilder.newColumnForExport("Type_Intrants", RefOtherInput.PROPERTY_INPUT_TYPE_C0);
        modelBuilder.newColumnForExport("Caracteristique_1", RefOtherInput.PROPERTY_CARACTERISTIC1);
        modelBuilder.newColumnForExport("Caracteristique_2", RefOtherInput.PROPERTY_CARACTERISTIC2);
        modelBuilder.newColumnForExport("Caracteristique_3", RefOtherInput.PROPERTY_CARACTERISTIC3);
        modelBuilder.newColumnForExport("Unite", RefOtherInput.PROPERTY_UNIT, AGROSYST_OTHER_PRODUCT_INPUT_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("Duree de vie", RefOtherInput.PROPERTY_LIFETIME, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("filieres", RefOtherInput.PROPERTY_SECTOR, AGROSYST_SECTORS_FORMATTER);
        modelBuilder.newColumnForExport("source", RefOtherInput.PROPERTY_SOURCE);

        if (this.isExportForAdmin) {
            modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefPrixPot.PROPERTY_ACTIVE, T_F_FORMATTER);
        }

        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefOtherInput newEmptyInstance() {
        RefOtherInput result = new RefOtherInputImpl();
        result.setActive(true);
        return result;
    }
    
}
