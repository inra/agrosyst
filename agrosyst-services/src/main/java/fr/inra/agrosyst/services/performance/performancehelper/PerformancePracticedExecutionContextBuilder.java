package fr.inra.agrosyst.services.performance.performancehelper;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PotInputUnit;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbu;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.common.RefInputPriceService;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.performance.CropWithSpecies;
import fr.inra.agrosyst.api.services.performance.EquipmentUsageRange;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInputAndCampaigns;
import fr.inra.agrosyst.api.services.performance.RefScenariosInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.TraitementProduitWithSpecies;
import fr.inra.agrosyst.api.services.performance.utils.PriceConverterKeysToRate;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.security.AgrosystAccessDeniedException;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.CommonPerformanceService;
import fr.inra.agrosyst.services.performance.indicators.operatingexpenses.IndicatorSubstrateInputOperatingExpenses;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static fr.inra.agrosyst.services.performance.indicators.IndicatorManualWorkforceExpenses.STANDARD_MANUAL_WORK_COST_VALUE;
import static fr.inra.agrosyst.services.performance.indicators.IndicatorMechanizedWorkforceExpenses.STANDARD_MECHANIZED_WORK_COST_VALUE;

public class PerformancePracticedExecutionContextBuilder {

    private static final Log LOGGER = LogFactory.getLog(PerformancePracticedExecutionContextBuilder.class);

    protected final AnonymizeService anonymizeService;
    protected final DomainInputStockUnitService domainInputStockUnitService;
    protected final DomainService domainService;
    protected final InputPriceService inputPriceService;
    protected final CommonPerformanceService performanceService;
    protected final ReferentialService referentialService;
    protected final RefInputPriceService refInputPriceService;
    protected final PricesService pricesService;
    protected final Collection<IndicatorFilter> indicatorFilters;

    @Getter
    protected final PerformanceGlobalExecutionContext performanceGlobalExecutionContext;
    protected PerformancePracticedDomainExecutionContext domainExecutionContext;

    protected final Set<String> scenarioCodes;

    // price converter (code_destination_A, PriceUnit) -> convertion rate
    protected final PriceConverterKeysToRate priceConverterKeysToRate;
    protected final boolean executeAsAdmin;

    public PerformancePracticedExecutionContextBuilder(AnonymizeService anonymizeService,
                                                       DomainInputStockUnitService domainInputStockUnitService,
                                                       DomainService domainService,
                                                       InputPriceService inputPriceService,
                                                       CommonPerformanceService performanceService,
                                                       ReferentialService referentialService,
                                                       RefInputPriceService refInputPriceService,
                                                       PricesService pricesService,
                                                       Collection<IndicatorFilter> indicatorFilters,
                                                       boolean executeAsAdmin,
                                                       PriceConverterKeysToRate priceConverterKeysToRate,
                                                       RefSolTextureGeppa defaultRefSolTextureGeppa,
                                                       Set<String> scenarioCodes,
                                                       RefLocation defaultLocation,
                                                       RefSolProfondeurIndigo defaultSolDepth) {
        this.anonymizeService = anonymizeService;
        this.domainInputStockUnitService = domainInputStockUnitService;
        this.domainService = domainService;
        this.inputPriceService = inputPriceService;
        this.performanceService = performanceService;
        this.referentialService = referentialService;
        this.refInputPriceService = refInputPriceService;
        this.pricesService = pricesService;
        this.indicatorFilters = indicatorFilters;

        this.executeAsAdmin = executeAsAdmin;
        this.priceConverterKeysToRate = priceConverterKeysToRate;
        this.scenarioCodes = scenarioCodes;

        performanceGlobalExecutionContext = new PerformanceGlobalExecutionContext(
                defaultRefSolTextureGeppa,
                defaultSolDepth,
                defaultLocation,
                referentialService.getGroupesCiblesParCode()
        );
    }

    public PerformancePracticedDomainExecutionContext buildPerformanceDomainExecutionContext(Pair<Domain, Domain> domainAndAnonymizeDomain,
                                                                                             List<GrowingSystem> growingSystems) {

        long chronoT0 = System.currentTimeMillis();

        Domain domain = domainAndAnonymizeDomain.getLeft();

        List<ToolsCoupling> toolsCouplings = domainService.getToolsCouplingsForAllCampaignsDomain(domain);

        Map<String, ToolsCoupling> toolsCouplingByCode = getClosetToolsCoupling(toolsCouplings, domain);

        Map<RefMateriel, RefAgsAmortissement> deprecationRateByEquipments = performanceService.getDeprecationRateByEquipments(toolsCouplings);

        Collection<EquipmentUsageRange> equipmentUsageRanges = performanceService.getEquipmentUsageRanges(toolsCouplings);

        List<CroppingPlanEntry> crops = domainService.getCroppingPlanEntriesForAllCampaignsDomain(domain);

        Map<String, Set<Integer>> campaignsForCropCode = new HashMap<>();
        Map<String, Set<Integer>> campaignsForSpeciesCropCode = new HashMap<>();

        MultiKeyMap<Object, CroppingPlanEntry> cropByCampaignAndCodes = new MultiKeyMap<>();
        Map<String, CroppingPlanSpecies> speciesByCode = new HashMap<>();

        for (CroppingPlanEntry croppingPlanEntry : crops) {

            final int cropAndSpeciesCampaign = croppingPlanEntry.getDomain().getCampaign();

            final String cropCode = croppingPlanEntry.getCode();
            cropByCampaignAndCodes.put(cropAndSpeciesCampaign, cropCode, croppingPlanEntry);
            Set<Integer> campaignsForCrop = campaignsForCropCode.computeIfAbsent(cropCode, k -> new HashSet<>());
            campaignsForCrop.add(cropAndSpeciesCampaign);

            // many crops can have the same code so we keep the one that have the more species on it
            final List<CroppingPlanSpecies> croppingPlanSpecies = croppingPlanEntry.getCroppingPlanSpecies();
            if (CollectionUtils.isNotEmpty(croppingPlanSpecies)) {
                // add species to domainContext
                for (CroppingPlanSpecies species : croppingPlanSpecies) {

                    final String speciesCode = species.getCode();
                    Set<Integer> campaignsForSpecies = campaignsForSpeciesCropCode.computeIfAbsent(speciesCode, k -> new HashSet<>());
                    campaignsForSpecies.add(cropAndSpeciesCampaign);

                    // if for a campaign a species is declared with species area the we use this one
                    CroppingPlanSpecies prevCropSpecies = speciesByCode.get(speciesCode);
                    if (species.getSpeciesArea() != null || prevCropSpecies == null || prevCropSpecies.getSpeciesArea() == null) {
                        speciesByCode.putIfAbsent(speciesCode, species);
                    }
                }
            }
        }

        long t0 = System.currentTimeMillis();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Chargement des Code Culture Maa par espèce pour le domain %s (%d) synthétisé: %d ms", domain.getName(), domain.getCampaign(), (System.currentTimeMillis() - t0)));
        }

        // comment for the moment as Iphy indicator is not enable
        Map<String, RefCultureEdiGroupeCouvSol> speciesMaxCouvSolForCrop = new HashMap<>();

        Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStock = domainInputStockUnitService.loadDomainInputStock(domain);

        Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters = referentialService.loadAllRefInputUnitPriceUnitConverter();

        List<Integer> campaigns = domainService.getCampaignsForDomain(domain);

        RefCampaignsInputPricesByDomainInputAndCampaigns refInputPricesForCampaignsByInput = refInputPriceService.getRefInputPricesForCampaignsByInput(
                domainInputStock,
                refInputUnitPriceUnitConverters,
                campaigns);

        RefScenariosInputPricesByDomainInput refScenariosInputPricesByDomainInput = refInputPriceService.getRefInputPricesForScenariosByInput(
                domainInputStock,
                refInputUnitPriceUnitConverters,
                this.scenarioCodes);
        MultiValuedMap<String, RefHarvestingPrice> refHarvestingPricesByValorisationKeyForDomain = performanceService.getRefHarvestingPricesByValorisationKeyForDomain(domain, scenarioCodes);

        final Collection<AbstractDomainInputStockUnit> manualWorkforceInputs = CollectionUtils.emptyIfNull(domainInputStock.get(InputType.MAIN_OEUVRE_MANUELLE));
        double manualWorkforceCost = manualWorkforceInputs.stream()
                .map(AbstractDomainInputStockUnit::getInputPrice)
                .filter(Objects::nonNull)
                .filter(price -> InputPriceCategory.MAIN_OEUVRE_MANUELLE_INPUT.equals(price.getCategory()) && price.getPrice() != null)
                .map(InputPrice::getPrice)
                .sorted().findFirst()
                .orElse(STANDARD_MANUAL_WORK_COST_VALUE);

        final Collection<AbstractDomainInputStockUnit> mechanizedWorkforceInputs = CollectionUtils.emptyIfNull(domainInputStock.get(InputType.MAIN_OEUVRE_TRACTORISTE));
        double mechanizedWorkforceCost = mechanizedWorkforceInputs.stream()
                .map(AbstractDomainInputStockUnit::getInputPrice)
                .filter(Objects::nonNull)
                .filter(price -> InputPriceCategory.MAIN_OEUVRE_TRACTORISTE_INPUT.equals(price.getCategory()) && price.getPrice() != null)
                .map(InputPrice::getPrice)
                .sorted().findFirst()
                .orElse(STANDARD_MECHANIZED_WORK_COST_VALUE);

        Optional<RefPrixCarbu> refFuelPrice = refInputPriceService.getFuelRefPriceForCampaign(domain.getCampaign());
        List<HarvestingPrice> harvestingPrices = pricesService.loadHarvestingPrices(domain.getTopiaId());

        List<RefCompositionSubstancesActivesParNumeroAMM> domainRefCompositionSubstancesActivesParNumeroAMM = referentialService.getDomainRefCompositionSubstancesActivesParNumeroAMM(domain);
        Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> allDomainSubstancesByAmm = new HashMap<>();

        domainRefCompositionSubstancesActivesParNumeroAMM.forEach(csa -> {
            List<RefCompositionSubstancesActivesParNumeroAMM> refCompositionSubstancesActivesParNumeroAMMS = allDomainSubstancesByAmm.computeIfAbsent(csa.getNumero_AMM(), k -> new ArrayList<>());
            refCompositionSubstancesActivesParNumeroAMMS.add(csa);
        });

        domainExecutionContext = new PerformancePracticedDomainExecutionContext(
                domainInputStock,
                domainAndAnonymizeDomain,
                refInputPricesForCampaignsByInput,
                refScenariosInputPricesByDomainInput,
                manualWorkforceCost,
                mechanizedWorkforceCost,
                refFuelPrice,
                harvestingPrices,
                refHarvestingPricesByValorisationKeyForDomain,
                toolsCouplingByCode,
                deprecationRateByEquipments,
                equipmentUsageRanges,
                cropByCampaignAndCodes,
                speciesByCode,
                speciesMaxCouvSolForCrop,
                campaignsForCropCode,
                campaignsForSpeciesCropCode,
                allDomainSubstancesByAmm);

        addSeedSpeciesUnitPriceUnitConverterToGlobalPerformance();
        addPhytoProductUnitPriceUnitConverterToGlobalPerformance(domainExecutionContext);
        addMineralProductUnitPriceUnitConverterToGlobalPerformance(domainExecutionContext);
        addOrganicProductUnitPriceUnitConverterToGlobalPerformance(domainExecutionContext);
        addSubstrateUnitPriceUnitConverterToGlobalPerformance(domainExecutionContext);
        addPotUnitPriceUnitConverterToGlobalPerformance(domainExecutionContext);

        buildExecutionContext(domainExecutionContext, growingSystems);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format(this.getClass().getSimpleName() + ": for domain with id : '%s' elapse time for buildPerformanceDomainExecutionContext() :%d ms", domain.getTopiaId(), System.currentTimeMillis() - chronoT0));
        }

        return domainExecutionContext;
    }

    protected Map<String, ToolsCoupling> getClosetToolsCoupling(List<ToolsCoupling> toolsCouplings, Domain domain) {
        Map<String, ToolsCoupling> toolsCouplingByCode = new HashMap<>();

        toolsCouplings.stream()
                .filter(tc -> tc.getDomain().equals(domain)).forEach(tc -> toolsCouplingByCode.put(tc.getCode(), tc));

        for (ToolsCoupling toolsCoupling : toolsCouplings) {
            // par défaut, on prend la combinaison d'outil du domain en question
            // dans le cas où il n'y a pas de combinaison d'outils sur ce domain précis,
            // on recherche la combinaison d'outils à la campagne inférieure la plus proche
            // dans le cas ou il n'y en a pas, on recherche la combinaison d'outils
            // à la campagne supérieure la plus proche.
            if (!toolsCouplingByCode.containsKey(toolsCoupling.getCode())) {
                ToolsCoupling closestCampaignTc = toolsCouplings.stream()
                        .filter(tc -> tc.getCode().equals(toolsCoupling.getCode()))
                        .filter(tc -> tc.getDomain().getCampaign() < domain.getCampaign())
                        .max(Comparator.comparingInt(toolsCoupling1 -> toolsCoupling1.getDomain().getCampaign()))
                        .orElse(null);
                if (closestCampaignTc == null) {
                    closestCampaignTc = toolsCouplings.stream()
                            .filter(tc -> tc.getCode().equals(toolsCoupling.getCode()))
                            .filter(tc -> tc.getDomain().getCampaign() > domain.getCampaign())
                            .min(Comparator.comparingInt(toolsCoupling1 -> toolsCoupling1.getDomain().getCampaign()))
                            .orElse(null);
                }
                if (closestCampaignTc == null) {
                    closestCampaignTc = toolsCoupling;
                }
                toolsCouplingByCode.put(closestCampaignTc.getCode(), closestCampaignTc);
            }

        }
        return toolsCouplingByCode;
    }

    protected void addSubstrateUnitPriceUnitConverterToGlobalPerformance(PerformancePracticedDomainExecutionContext context) {
        final Set<AbstractDomainInputStockUnit> inputs = context.getRefInputPricesForCampaignsByInput().substrateRefPriceByCampaignForInput().keySet();

        Collection<DomainSubstrateInput> substrateInputs = new ArrayList<>();
        inputs.forEach(is -> substrateInputs.add((DomainSubstrateInput) is));

        Set<SubstrateInputUnit> substrateInputUnits = IndicatorSubstrateInputOperatingExpenses.getSubstrateInputUnits(substrateInputs);

        Map<SubstrateInputUnit, List<RefInputUnitPriceUnitConverter>> convertersBySubstrateUnit =
                performanceGlobalExecutionContext.getConvertersBySubstrateUnit();

        Set<SubstrateInputUnit> substrateUnitsWithConverters = convertersBySubstrateUnit.keySet();
        substrateInputUnits.removeAll(substrateUnitsWithConverters);

        if (!substrateInputUnits.isEmpty()) {
            List<RefInputUnitPriceUnitConverter> allConverters = performanceService.getPriceUnitConvertersForSubstrateUnit(substrateInputUnits);

            for (RefInputUnitPriceUnitConverter converter : allConverters) {
                SubstrateInputUnit substrateInputUnit = converter.getSubstrateInputUnit();
                List<RefInputUnitPriceUnitConverter> converters = convertersBySubstrateUnit.computeIfAbsent(substrateInputUnit, k -> new ArrayList<>());
                converters.add(converter);
            }
        }
    }

    protected void addPotUnitPriceUnitConverterToGlobalPerformance(PerformancePracticedDomainExecutionContext context) {
        final Set<AbstractDomainInputStockUnit> inputs = context.getRefInputPricesForCampaignsByInput().potRefPriceByCampaignForInput().keySet();

        Set<PotInputUnit> potInputUnits = CollectionUtils.emptyIfNull(inputs).stream()
                .map(input -> ((DomainPotInput) input).getUsageUnit())
                .collect(Collectors.toSet());

        Map<PotInputUnit, List<RefInputUnitPriceUnitConverter>> globalConvertersByPotInputUnit =
                performanceGlobalExecutionContext.getConvertersByPotUnit();
        Set<PotInputUnit> potUnitsWithConverters = globalConvertersByPotInputUnit.keySet();
        potInputUnits.removeAll(potUnitsWithConverters);

        if (!potInputUnits.isEmpty()) {
            List<RefInputUnitPriceUnitConverter> allConverters = performanceService.getPriceUnitConvertersForPotUnit(potInputUnits);

            for (RefInputUnitPriceUnitConverter converter : allConverters) {
                PotInputUnit potInputUnit = converter.getPotInputUnit();
                List<RefInputUnitPriceUnitConverter> converters = globalConvertersByPotInputUnit.computeIfAbsent(potInputUnit, k -> new ArrayList<>());
                converters.add(converter);
            }
        }
    }

    protected void addOrganicProductUnitPriceUnitConverterToGlobalPerformance(PerformancePracticedDomainExecutionContext context) {
        final Set<AbstractDomainInputStockUnit> inputs = context.getRefInputPricesForCampaignsByInput().fertiOrgaRefPriceByCampaignForInput().keySet();

        Set<OrganicProductUnit> organicProductUnits = CollectionUtils.emptyIfNull(inputs)
                .stream()
                .map(input -> ((DomainOrganicProductInput) input).getUsageUnit()).collect(Collectors.toSet());

        Map<OrganicProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByOrganicProductUnit =
                performanceGlobalExecutionContext.getConvertersByOrganicProductUnit();

        Set<OrganicProductUnit> organicProductUnitsWithConverters = convertersByOrganicProductUnit.keySet();
        organicProductUnits.removeAll(organicProductUnitsWithConverters);

        if (!organicProductUnits.isEmpty()) {
            List<RefInputUnitPriceUnitConverter> allConverters = performanceService.getPriceUnitConvertersForOrganicProductUnit(organicProductUnits);

            for (RefInputUnitPriceUnitConverter converter : allConverters) {
                OrganicProductUnit organicProductUnit = converter.getOrganicProductUnit();
                List<RefInputUnitPriceUnitConverter> converters = convertersByOrganicProductUnit.computeIfAbsent(organicProductUnit, k -> new ArrayList<>());
                converters.add(converter);
            }
        }
    }

    protected void addMineralProductUnitPriceUnitConverterToGlobalPerformance(PerformancePracticedDomainExecutionContext context) {
        Set<AbstractDomainInputStockUnit> inputs = context.getRefInputPricesForCampaignsByInput().mineralRefPriceByCampaignForInput().keySet();
        Set<MineralProductUnit> mineralProductUnits = CollectionUtils.emptyIfNull(inputs)
                .stream()
                .map(input -> ((DomainMineralProductInput) input).getUsageUnit()).collect(Collectors.toSet());

        Map<MineralProductUnit, List<RefInputUnitPriceUnitConverter>> globalConvertersByMineralProductUnit =
                performanceGlobalExecutionContext.getConvertersByMineralProductUnit();

        Set<MineralProductUnit> mineralProductUnitsWithConverters = globalConvertersByMineralProductUnit.keySet();
        mineralProductUnits.removeAll(mineralProductUnitsWithConverters);

        if (!mineralProductUnits.isEmpty()) {
            List<RefInputUnitPriceUnitConverter> allConverters = performanceService.getPriceUnitConvertersForMineralProductUnit(mineralProductUnits);

            for (RefInputUnitPriceUnitConverter converter : allConverters) {
                MineralProductUnit mineralProductUnit = converter.getMineralProductUnit();
                List<RefInputUnitPriceUnitConverter> converters =
                        globalConvertersByMineralProductUnit.computeIfAbsent(mineralProductUnit, k -> new ArrayList<>());
                converters.add(converter);
            }
        }
    }

    protected void addPhytoProductUnitPriceUnitConverterToGlobalPerformance(PerformancePracticedDomainExecutionContext context) {
        final Set<AbstractDomainInputStockUnit> inputs = context.getRefInputPricesForCampaignsByInput().phytoRefPriceByCampaignForInput().keySet();


        Collection<AbstractDomainInputStockUnit> abstractDomainInputStockUnits = CollectionUtils.emptyIfNull(inputs);
        Set<PhytoProductUnit> phytoProductUnits;
        if (context.getRefInputPricesForCampaignsByInput().speciesRefPriceByCampaignForInput() != null) {
            phytoProductUnits = context.getRefInputPricesForCampaignsByInput().speciesRefPriceByCampaignForInput()
                    .keySet()
                    .stream()
                    .filter(adis -> adis instanceof DomainSeedSpeciesInput)
                    .map(adis -> ((DomainSeedSpeciesInput) adis).getSpeciesPhytoInputs())
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .map(DomainPhytoProductInput::getUsageUnit)
                    .collect(Collectors.toSet());
        } else {
            phytoProductUnits = new HashSet<>();
        }
        phytoProductUnits.addAll(abstractDomainInputStockUnits
                .stream()
                .map(input -> ((DomainPhytoProductInput) input).getUsageUnit()).collect(Collectors.toSet()));

        Map<PhytoProductUnit, List<RefInputUnitPriceUnitConverter>> globalConvertersByPhytoProductUnit =
                performanceGlobalExecutionContext.getConvertersByPhytoProductUnit();
        Set<PhytoProductUnit> phytoProductUnitsWithConverters = globalConvertersByPhytoProductUnit.keySet();
        phytoProductUnits.removeAll(phytoProductUnitsWithConverters);

        if (!phytoProductUnits.isEmpty()) {
            List<RefInputUnitPriceUnitConverter> allConverters = performanceService.getPriceUnitConvertersForPhytoProductUnits(phytoProductUnits);

            for (RefInputUnitPriceUnitConverter converter : allConverters) {
                PhytoProductUnit phytoProductUnit = converter.getPhytoProductUnit();
                List<RefInputUnitPriceUnitConverter> converters = globalConvertersByPhytoProductUnit.computeIfAbsent(phytoProductUnit, k -> new ArrayList<>());
                converters.add(converter);
            }
        }
    }

    protected void addSeedSpeciesUnitPriceUnitConverterToGlobalPerformance() {

        Set<SeedPlantUnit> seedPlantUnits = Sets.newHashSet(SeedPlantUnit.values());

        Map<SeedPlantUnit, List<RefInputUnitPriceUnitConverter>> globalConvertersByPhytoProductUnit = performanceGlobalExecutionContext.getConvertersBySeedingProductUnit();
        Set<SeedPlantUnit> seedPlantUnitsWithConverters = globalConvertersByPhytoProductUnit.keySet();
        seedPlantUnits.removeAll(seedPlantUnitsWithConverters);

        if (!seedPlantUnits.isEmpty()) {
            List<RefInputUnitPriceUnitConverter> allConverters = performanceService.getPriceUnitConvertersForSeedPlantUnit(seedPlantUnits);

            for (RefInputUnitPriceUnitConverter converter : allConverters) {
                SeedPlantUnit phytoProductUnit = converter.getSeedPlantUnit();
                List<RefInputUnitPriceUnitConverter> converters = globalConvertersByPhytoProductUnit.computeIfAbsent(phytoProductUnit, k -> new ArrayList<>());
                converters.add(converter);
            }
        }
    }

    protected void buildExecutionContext(PerformancePracticedDomainExecutionContext performanceDomainExecutionContext,
                                         List<GrowingSystem> growingSystems) {

        List<PracticedSystem> practicedSystemsForGrowingSystems = performanceService.getPracticedSystemsForGrowingSystems(growingSystems);

        Map<GrowingSystem, Set<PerformancePracticedSystemExecutionContext>> growingSystemPerformancePracticedSystemExecutionContextsByGrowingSystems =
                buildPracticedSystemsExecutionContexts(practicedSystemsForGrowingSystems, performanceDomainExecutionContext);

        Set<PerformanceGrowingSystemExecutionContext> performanceGrowingSystemExecutionContexts =
                buildGrowingSystemsExecutionContexts(growingSystems, growingSystemPerformancePracticedSystemExecutionContextsByGrowingSystems);

        performanceDomainExecutionContext.setPerformanceGrowingSystemExecutionContexts(performanceGrowingSystemExecutionContexts);
    }

    protected Set<PerformanceGrowingSystemExecutionContext> buildGrowingSystemsExecutionContexts(
            List<GrowingSystem> growingSystems,
            Map<GrowingSystem, Set<PerformancePracticedSystemExecutionContext>> growingSystemPerformancePracticedSystemExecutionContextsByGrowingSystems) {

        Set<PerformanceGrowingSystemExecutionContext> performanceGrowingSystemExecutionContexts = new HashSet<>();

        for (GrowingSystem growingSystem : growingSystems) {
            final Set<PerformancePracticedSystemExecutionContext> practicedSystemExecutionContexts =
                    growingSystemPerformancePracticedSystemExecutionContextsByGrowingSystems.get(growingSystem);
            if (practicedSystemExecutionContexts != null) {

                GrowingSystem anonymizeGrowingSystem = null;
                if (executeAsAdmin) {
                    anonymizeGrowingSystem = growingSystem;
                } else {
                    try {
                        anonymizeGrowingSystem = anonymizeService.checkForGrowingSystemAnonymization(growingSystem);
                    } catch (AgrosystAccessDeniedException e) {
                        if (LOGGER.isWarnEnabled()) {
                            LOGGER.warn(e);
                        }
                    }
                }

                if (anonymizeGrowingSystem != null) {
                    Set<Network> its = performanceService.getIts(growingSystem);
                    Set<Network> irs = performanceService.getIRs(growingSystem);

                    Pair<Optional<GrowingSystem>, Optional<GrowingSystem>> growingSystemAndAnonymizeGrowingSystem =
                            Pair.of(Optional.of(growingSystem), Optional.of(anonymizeGrowingSystem));

                    Map<String, MultiValuedMap<String, RefHarvestingPrice>> harvestingScenarioPricesByKey =
                            performanceService.getHarvestingScenarioPricesByKeyForGrowingSystemByPracticedSystemIds(growingSystem, scenarioCodes);

                    PerformanceGrowingSystemExecutionContext gsContext = new PerformanceGrowingSystemExecutionContext(
                            growingSystemAndAnonymizeGrowingSystem,
                            practicedSystemExecutionContexts,
                            harvestingScenarioPricesByKey,
                            irs,
                            its);

                    performanceGrowingSystemExecutionContexts.add(gsContext);
                }
            }
        }

        return performanceGrowingSystemExecutionContexts;
    }

    protected Map<GrowingSystem, Set<PerformancePracticedSystemExecutionContext>> buildPracticedSystemsExecutionContexts(
            List<PracticedSystem> practicedSystemsForGrowingSystems, PerformancePracticedDomainExecutionContext performanceDomainExecutionContext) {

        Map<GrowingSystem, Set<PerformancePracticedSystemExecutionContext>> gsPerformancePsExecutionContextsByGrowingSystems = new HashMap<>();

        Map<PracticedSystem, PracticedSeasonalCropCycle> practicedSeasonalCropCyclesByPracticedSystems =
                performanceService.getPracticedSeasonalCropCyclesByPracticedSystems(practicedSystemsForGrowingSystems);

        Map<PracticedSystem, List<PracticedPerennialCropCycle>> practicedPerennialCropCyclesByPracticedSystems =
                performanceService.getPracticedPerennialCropCyclesByPracticedSystems(practicedSystemsForGrowingSystems);

        Map<PracticedSystem, PracticedPlot> practicedPlotForPracticedSystems =
                performanceService.getPracticedPlotsByPracticedSystems(practicedSystemsForGrowingSystems);

        for (PracticedSystem practicedSystem : practicedSystemsForGrowingSystems) {

            PracticedPlot practicedPlot = practicedPlotForPracticedSystems.get(practicedSystem);

            GrowingSystem practicedSystemGrowingSystem = practicedSystem.getGrowingSystem();
            Set<PerformancePracticedSystemExecutionContext> performancePracticedSystemExecutionContexts = gsPerformancePsExecutionContextsByGrowingSystems.
                    computeIfAbsent(practicedSystemGrowingSystem, k -> new HashSet<>());
            Optional<PerformancePracticedSystemExecutionContext> optionalPerformancePracticedSystemExecutionContext = buildPerformancePracticedSystemExecutionContext(
                    practicedSeasonalCropCyclesByPracticedSystems,
                    practicedPerennialCropCyclesByPracticedSystems,
                    practicedSystem,
                    practicedPlot,
                    performanceDomainExecutionContext);

            optionalPerformancePracticedSystemExecutionContext.ifPresent(performancePracticedSystemExecutionContexts::add);
        }

        return gsPerformancePsExecutionContextsByGrowingSystems;
    }

    protected Optional<PerformancePracticedSystemExecutionContext> buildPerformancePracticedSystemExecutionContext(
            Map<PracticedSystem, PracticedSeasonalCropCycle> practicedSeasonalCropCyclesByPracticedSystems,
            Map<PracticedSystem, List<PracticedPerennialCropCycle>> practicedPerennialCropCyclesByPracticedSystems,
            PracticedSystem practicedSystem,
            PracticedPlot practicedPlot,
            PerformancePracticedDomainExecutionContext performanceDomainExecutionContext) {

        PracticedSeasonalCropCycle seasonalCropCycle = practicedSeasonalCropCyclesByPracticedSystems.get(practicedSystem);
        long seasonalNbCampaign = seasonalCropCycle != null ? performanceService.getCampaignsCount(CollectionUtils.emptyIfNull(seasonalCropCycle.getCropCycleNodes())) : 0;

        List<PracticedPerennialCropCycle> practicedPerennialCropCycles = practicedPerennialCropCyclesByPracticedSystems.get(practicedSystem);

        PracticedSystem anonymizePracticedSystem ;
        if (executeAsAdmin) {
            anonymizePracticedSystem = practicedSystem;
        } else {
            try {
                anonymizePracticedSystem = anonymizeService.checkForPracticedSystemAnonymization(practicedSystem);
            } catch (AgrosystAccessDeniedException e) {
                LOGGER.warn(e);
                return Optional.empty();
            }
        }

        Pair<PracticedSystem, PracticedSystem> practicedSystemPair = Pair.of(practicedSystem, anonymizePracticedSystem);

        Collection<String> codeAmmBioControle = referentialService.getCodeAmmBioControle(practicedSystem.getCampaigns());

        final RefCampaignsInputPricesByDomainInputAndCampaigns refInputPricesForCampaignsByInput = performanceDomainExecutionContext.getRefInputPricesForCampaignsByInput();
        final LinkedHashSet<Integer> campaigns = CommonService.GET_CAMPAIGNS_SET.apply(practicedSystem.getCampaigns());
        RefCampaignsInputPricesByDomainInput practicedSystemInputRefPricesByDomainInput = refInputPriceService.getRefInputPricesForCampaignsByInput(refInputPricesForCampaignsByInput, campaigns);

        boolean isQsaIndicators = false;
        List<String> qsaIndicatorClassNames = performanceService.getQSAIndicatorClassNames();
        if (indicatorFilters.stream().map(IndicatorFilter::getClazz).anyMatch(qsaIndicatorClassNames::contains)) {
            isQsaIndicators = true;
        }

        final MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode;
        final MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM;

        if (isQsaIndicators) {
            Domain domain = performanceDomainExecutionContext.getDomain();
            allSubstancesActivesCommissionEuropeenneByAmmCode = referentialService.getSubstancesActivesCommissionEuropeenneByAmmCodeForDomain(domain);
            refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM = this.referentialService.getRefPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM(domain);
        } else {
            allSubstancesActivesCommissionEuropeenneByAmmCode = new HashSetValuedHashMap<>();
            refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM = new HashSetValuedHashMap<>();
        }

        List<HarvestingPrice> harvestingPrices = performanceDomainExecutionContext.getHarvestingprices();
        PerformancePracticedSystemExecutionContext performancePracticedSystemExecutionContext =
                new PerformancePracticedSystemExecutionContext(
                        practicedSystemPair,
                        seasonalCropCycle,
                        seasonalNbCampaign,
                        practicedPerennialCropCycles,
                        practicedPlot,
                        harvestingPrices,
                        codeAmmBioControle,
                        practicedSystemInputRefPricesByDomainInput,
                        allSubstancesActivesCommissionEuropeenneByAmmCode,
                        refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM);

        Set<PerformancePracticedCropExecutionContext> practicedCropContextExecutionContexts =
                buildPerformancePracticedCropContextExecutionContexts(performancePracticedSystemExecutionContext);

        performancePracticedSystemExecutionContext.setPerformancePracticedCropContextExecutionContexts(practicedCropContextExecutionContexts);

        return Optional.of(performancePracticedSystemExecutionContext);

    }

    protected Set<PerformancePracticedCropExecutionContext> buildPerformancePracticedCropContextExecutionContexts(
            PerformancePracticedSystemExecutionContext practicedSystemContext) {

        Set<PerformancePracticedCropExecutionContext> practicedCropContextExecutionContexts = new HashSet<>();

        PracticedSeasonalCropCycle seasonalCropCycle = practicedSystemContext.getSeasonalCropCycle();
        List<PracticedPerennialCropCycle> practicedPerennialCropCycles = practicedSystemContext.getPracticedPerennialCropCycles();

        if (seasonalCropCycle == null && CollectionUtils.isEmpty(practicedPerennialCropCycles)) {
            return practicedCropContextExecutionContexts;// noting to do
        }

        setPracticedCropWithSpecies(practicedSystemContext);

        buildPerformanceSeasonalPracticedCropContextExecutionContexts(practicedSystemContext, practicedCropContextExecutionContexts);

        buildPerformancePerennialPracticedCropContextExecutionContexts(practicedSystemContext, practicedCropContextExecutionContexts);

        return practicedCropContextExecutionContexts;
    }

    protected void buildPerformancePerennialPracticedCropContextExecutionContexts(
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            Set<PerformancePracticedCropExecutionContext> practicedCropContextExecutionContexts) {

        List<PracticedPerennialCropCycle> practicedPerennialCropCycles = practicedSystemContext.getPracticedPerennialCropCycles();

        if (CollectionUtils.isNotEmpty(practicedPerennialCropCycles)) {

            Map<String, CropWithSpecies> cropByCodeWithSpecies = practicedSystemContext.getCropByCodeWithSpecies();
            Map<String, CropWithSpecies> cropByCodeWithSpeciesFallback = practicedSystemContext.getCropByCodeWithSpeciesFallback();

            List<PracticedIntervention> cropInterventions =
                    performanceService.getPracticedInterventionForPracticedPerennialCropCycles(practicedPerennialCropCycles);

            List<AbstractAction> cropActions = performanceService.getPracticedActionsForCropInterventions(cropInterventions);

            for (PracticedPerennialCropCycle perennialCropCycle : practicedPerennialCropCycles) {

                WeedType weedType = perennialCropCycle.getWeedType();
                Collection<PracticedCropCyclePhase> phases = perennialCropCycle.getCropCyclePhases();
                List<PracticedIntervention> cycleInterventions = cropInterventions.stream().
                        filter(practicedIntervention -> phases.contains(practicedIntervention.getPracticedCropCyclePhase()))
                        .collect(Collectors.toList());

                if (CollectionUtils.isEmpty(cycleInterventions)) continue;// NOTING TO DO

                final String requestingCropCode = perennialCropCycle.getCroppingPlanEntryCode();

                CropWithSpecies croppingPlanEntryAndSpecies = null;
                if (cropByCodeWithSpecies != null) {
                    if (cropByCodeWithSpecies.containsKey(requestingCropCode)) {
                        croppingPlanEntryAndSpecies = cropByCodeWithSpecies.get(requestingCropCode);
                    } else {
                        croppingPlanEntryAndSpecies = cropByCodeWithSpeciesFallback.get(requestingCropCode);
                        LOGGER.warn(String.format("Cropping plan entry with code %s not found, try in fallback map", requestingCropCode));
                    }
                }
                CroppingPlanEntry crop = croppingPlanEntryAndSpecies != null ? croppingPlanEntryAndSpecies.getCroppingPlanEntry() : null;

                if (crop == null) {
                    if (LOGGER.isWarnEnabled()) {
                        String practicedSystemId = practicedSystemContext.getPracticedSystem().getTopiaId();
                        String domainCode = domainExecutionContext.getDomain().getCode();
                        LOGGER.warn(String.format("For practicedSystem %s Crop with code %s could not be find into domain code %s",
                                practicedSystemId,
                                requestingCropCode,
                                domainCode));
                    }
                    continue;// not normal
                }

                Set<CroppingPlanSpecies> cropSpecies = croppingPlanEntryAndSpecies.getCroppingPlanSpecies();

                // look for RefLienCulturesEdiActa for all connexion species, try first to get it from domain cache
                Set<RefEspece> refEspeces = cropSpecies.stream()
                        .map(CroppingPlanSpecies::getSpecies)
                        .collect(Collectors.toSet());

                PerformancePracticedCropExecutionContext cropContext = new PerformancePracticedCropExecutionContext(croppingPlanEntryAndSpecies,
                        perennialCropCycle,
                        refEspeces,
                        weedType
                );
                practicedCropContextExecutionContexts.add(cropContext);

                Set<PerformancePracticedInterventionExecutionContext> performancePracticedInterventionExecutionContext =
                        buildPerformancePracticedInterventionExecutionContexts(
                                practicedSystemContext,
                                cycleInterventions,
                                cropActions,
                                cropContext
                        );
                cropContext.setInterventionExecutionContexts(performancePracticedInterventionExecutionContext);

                getAndSetCropYealdAverage(cropContext);
            }

        }
    }

    protected void getAndSetCropYealdAverage(PerformancePracticedCropExecutionContext cropContext) {

        Map<Pair<RefDestination, YealdUnit>, List<Double>> interventionsYealds = cropContext.getInterventionsYealds();
        Map<Pair<RefDestination, YealdUnit>, Double> cropYealds = performanceService.computeYealdAverageByDestinationAndUnit(interventionsYealds);
        cropContext.setMainCropYealds(cropYealds);

        Map<Pair<RefDestination, YealdUnit>, List<Double>> intermediateInterventionsYealds = cropContext.getIntermediateInterventionsYealds();
        Map<Pair<RefDestination, YealdUnit>, Double> intermediateCropYealds = performanceService.computeYealdAverageByDestinationAndUnit(intermediateInterventionsYealds);
        cropContext.setIntermediateCropYealds(intermediateCropYealds);
    }

    protected void buildPerformanceSeasonalPracticedCropContextExecutionContexts(
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            Set<PerformancePracticedCropExecutionContext> practicedCropContextExecutionContexts) {

        PracticedSeasonalCropCycle seasonalCropCycle = practicedSystemContext.getSeasonalCropCycle();

        if (seasonalCropCycle != null) {

            Collection<PracticedCropCycleNode> cycleNodes = seasonalCropCycle.getCropCycleNodes();

            if (CollectionUtils.isEmpty(cycleNodes)) {
                return;// nothing to do
            }

            List<PracticedCropCycleConnection> connections = performanceService.getPracticedConnectionsForNodes(cycleNodes);

            if (CollectionUtils.isEmpty(connections)) {
                return;// nothing to do
            }

            List<PracticedIntervention> cropInterventions = performanceService.getPracticedInterventionForConnections(connections);

            if (CollectionUtils.isEmpty(cropInterventions)) {
                return;// nothing to do
            }

            Map<PracticedCropCycleConnection, Double> cumulativeFrequenciesByConnection = performanceService.computeCumulativeFrequencies(connections);

            practicedSystemContext.setPracticedCropPaths(performanceService.retrievePracticedPaths(connections));

            // filter on crop valid for practiced campaigns
            Map<String, CropWithSpecies> cropByCodeWithSpecies = practicedSystemContext.getCropByCodeWithSpecies();
            Map<String, CropWithSpecies> cropByCodeWithSpeciesFallback = practicedSystemContext.getCropByCodeWithSpeciesFallback();

            long nbRank = practicedSystemContext.getSeasonalNbCampaign();
            practicedSystemContext.setCumulativeFrequenciesByConnection(cumulativeFrequenciesByConnection);

            List<AbstractAction> cycleActions = performanceService.getPracticedActionsForCropInterventions(cropInterventions);

            for (PracticedCropCycleConnection connection : connections) {

                List<PracticedIntervention> connectionInterventions = cropInterventions.stream()
                        .filter(practicedIntervention -> connection.equals(practicedIntervention.getPracticedCropCycleConnection()))
                        .collect(Collectors.toList());

                if (connectionInterventions.isEmpty() && !connection.isNotUsedForThisCampaign()) {
                    continue;// noting to do
                }

                Double cumulativeFrequencyForConnection = (cumulativeFrequenciesByConnection.get(connection) / nbRank) * 100d;

                final PracticedCropCycleNode targetNode = connection.getTarget();
                final String croppingPlanEntryCode = targetNode.getCroppingPlanEntryCode();
                final String intermediateCroppingPlanEntryCode = connection.getIntermediateCroppingPlanEntryCode();

                final String previousCropCode = connection.getSource() != null ? connection.getSource().getCroppingPlanEntryCode() : null;

                CropWithSpecies croppingPlanEntryAndSpecies = null;
                if (cropByCodeWithSpecies != null) {
                    if (cropByCodeWithSpecies.containsKey(croppingPlanEntryCode)) {
                        croppingPlanEntryAndSpecies = cropByCodeWithSpecies.get(croppingPlanEntryCode);
                    } else {
                        croppingPlanEntryAndSpecies = cropByCodeWithSpeciesFallback.get(croppingPlanEntryCode);
                        LOGGER.warn(String.format("Cropping plan entry with code %s not found, try in fallback map", croppingPlanEntryCode));
                    }
                }
                CroppingPlanEntry crop = croppingPlanEntryAndSpecies != null ? croppingPlanEntryAndSpecies.getCroppingPlanEntry() : null;

                if (crop == null) {
                    if (LOGGER.isWarnEnabled()) {
                        String practicedSystemId = practicedSystemContext.getPracticedSystem().getTopiaId();
                        String domainCode = domainExecutionContext.getDomain().getCode();
                        LOGGER.warn(String.format("For practicedSytem %s Crop with code %s could not be find into domain code %s",
                                practicedSystemId,
                                croppingPlanEntryCode,
                                domainCode));
                    }
                    continue; // not normal
                }

                Set<CroppingPlanSpecies> intermediateCropSpecies = new HashSet<>();
                CropWithSpecies intermediateCropAndSpecies = null;
                if (StringUtils.isNotBlank(intermediateCroppingPlanEntryCode)) {
                    intermediateCropAndSpecies = cropByCodeWithSpecies.get(intermediateCroppingPlanEntryCode);
                    if (intermediateCropAndSpecies != null) {
                        intermediateCropSpecies = intermediateCropAndSpecies.getCroppingPlanSpecies();
                    }
                }

                // look for RefLienCulturesEdiActa for all connexion species, try first to get it from domain cache
                Set<CroppingPlanSpecies> allSpecies = new HashSet<>();
                allSpecies.addAll(croppingPlanEntryAndSpecies.getCroppingPlanSpecies());
                allSpecies.addAll(intermediateCropSpecies);
                Set<RefEspece> refEspeces = allSpecies.stream()
                        .map(CroppingPlanSpecies::getSpecies)
                        .collect(Collectors.toSet());

                RefCultureEdiGroupeCouvSol speciesMaxCouvSolForCrop = domainExecutionContext.getSpeciesMaxCouvSolForCrops().get(croppingPlanEntryCode);
                RefCultureEdiGroupeCouvSol intermediateSpeciesMaxCouvSolForCrop = intermediateCropAndSpecies != null ?
                        domainExecutionContext.getSpeciesMaxCouvSolForCrops().get(intermediateCropAndSpecies.getCroppingPlanEntry().getCode()) : null;

                CroppingPlanEntry previousCrop = null;
                if (StringUtils.isNotBlank(previousCropCode)) {
                    final CropWithSpecies previousCropAndSpecies = cropByCodeWithSpecies.get(previousCropCode);
                    if (previousCropAndSpecies != null) {
                        previousCrop = previousCropAndSpecies.getCroppingPlanEntry();
                    }
                }

                PerformancePracticedCropExecutionContext cropContext = new PerformancePracticedCropExecutionContext(
                        croppingPlanEntryAndSpecies,
                        previousCrop,
                        connection,
                        targetNode.getRank(),
                        intermediateCropAndSpecies,
                        refEspeces,
                        cumulativeFrequenciesByConnection,
                        cumulativeFrequencyForConnection,
                        nbRank,
                        speciesMaxCouvSolForCrop,
                        intermediateSpeciesMaxCouvSolForCrop);
                practicedCropContextExecutionContexts.add(cropContext);
                Set<PerformancePracticedInterventionExecutionContext> performancePracticedInterventionExecutionContext =
                        buildPerformancePracticedInterventionExecutionContexts(
                                practicedSystemContext,
                                connectionInterventions,
                                cycleActions,
                                cropContext);
                cropContext.setInterventionExecutionContexts(performancePracticedInterventionExecutionContext);

                getAndSetCropYealdAverage(cropContext);
            }

        }
    }

    /**
     * the practiced System Context contain the domain crop filter on matching campaigns
     */
    protected void setPracticedCropWithSpecies(PerformancePracticedSystemExecutionContext practicedSystemContext) {
        Set<Integer> practicedSystemCampaigns = CommonService.GET_CAMPAIGNS_SET.apply(practicedSystemContext.getPracticedSystem().getCampaigns());
        final int directCampaign = practicedSystemContext.getPracticedSystem().getGrowingSystem().getGrowingPlan().getDomain().getCampaign();
        MultiKeyMap<Object, CroppingPlanEntry> domainCropByCampaignAndCode = domainExecutionContext.getCropByCampaignAndCode();
        Map<String, CropWithSpecies> cropByCodeWithSpecies = new HashMap<>();
        Map<String, CropWithSpecies> cropByCodeWithSpeciesFallback = new HashMap<>();

        Map<String, Set<Integer>> campaignsForCropCode = domainExecutionContext.getCampaignsForCropCode();

        // try find crop that is related to the growing system
        if (CollectionUtils.containsAny(practicedSystemCampaigns, directCampaign)) {
            domainCropByCampaignAndCode.forEach((key, croppingPlanEntry) -> {
                final int campaign = (Integer) key.getKey(0);
                if (campaign == directCampaign) {
                    final String cropCode = (String) key.getKey(1);
                    CropWithSpecies cropWithSpecies = cropByCodeWithSpecies.get(cropCode);
                    if (cropWithSpecies != null) {
                        // if one of the crop is declared as mix we consider using this one
                        final CroppingPlanEntry cro0 = cropWithSpecies.getCroppingPlanEntry();
                        final boolean previousCropIsMix = isCropIsMix(cro0);
                        final boolean currentCropIsMix = isCropIsMix(croppingPlanEntry);
                        if (currentCropIsMix && !previousCropIsMix) {
                            cropByCodeWithSpecies.put(cropCode, new CropWithSpecies(croppingPlanEntry, new HashSet<>()));
                        }
                    } else {
                        cropByCodeWithSpecies.put(cropCode, new CropWithSpecies(croppingPlanEntry, new HashSet<>()));
                    }
                }
            });
        }

        // Todo: this is probably not necessary anymore to load cropping plan entry from other domain
        // add cropd that are not related to the growing system
        domainCropByCampaignAndCode.forEach((key, croppingPlanEntry) -> {
            String cropCode = (String) key.getKey(1);
            Set<Integer> campaigns = campaignsForCropCode.get(cropCode);
            if (CollectionUtils.containsAny(practicedSystemCampaigns, campaigns)) {
                CropWithSpecies cropWithSpecies = cropByCodeWithSpeciesFallback.get(cropCode);
                if (cropWithSpecies != null) {
                    // if one of the crop is declared as mix we consider using this one
                    final CroppingPlanEntry cro0 = cropWithSpecies.getCroppingPlanEntry();
                    final boolean previousCropIsMix = isCropIsMix(cro0);
                    final boolean currentCropIsMix = isCropIsMix(croppingPlanEntry);
                    if (currentCropIsMix && !previousCropIsMix) {
                        cropByCodeWithSpeciesFallback.put(cropCode, new CropWithSpecies(croppingPlanEntry, new HashSet<>()));
                    }
                } else {
                    cropByCodeWithSpeciesFallback.put(cropCode, new CropWithSpecies(croppingPlanEntry, new HashSet<>()));
                }
            }
        });

        // filter on species valid for practiced campaigns
        Map<String, CroppingPlanSpecies> domainSpeciesByCode = domainExecutionContext.getSpeciesByCode();

        Map<String, Set<Integer>> campaignsForSpeciesCropCode = domainExecutionContext.getCampaignsForSpeciesCropCode();
        for (Map.Entry<String, CroppingPlanSpecies> speciesCodeToSpecies : domainSpeciesByCode.entrySet()) {
            Set<Integer> campaigns = campaignsForSpeciesCropCode.get(speciesCodeToSpecies.getKey());
            if (CollectionUtils.containsAny(practicedSystemCampaigns, campaigns)) {
                final CroppingPlanSpecies cpeSpecies = speciesCodeToSpecies.getValue();
                final CroppingPlanEntry croppingPlanEntry = cpeSpecies.getCroppingPlanEntry();
                CropWithSpecies cropAndSpecies = cropByCodeWithSpecies.get(croppingPlanEntry.getCode());
                CropWithSpecies cropAndSpeciesFallback = cropByCodeWithSpeciesFallback.get(croppingPlanEntry.getCode());
                if (cropAndSpecies == null) {
                    cropAndSpecies = new CropWithSpecies(croppingPlanEntry, new HashSet<>());
                    cropByCodeWithSpecies.put(croppingPlanEntry.getCode(), cropAndSpecies);
                }
                if (cropAndSpeciesFallback == null) {
                    cropAndSpeciesFallback = new CropWithSpecies(croppingPlanEntry, new HashSet<>());
                    cropByCodeWithSpeciesFallback.put(croppingPlanEntry.getCode(), cropAndSpeciesFallback);
                }
                Set<CroppingPlanSpecies> speciesForCrop = cropAndSpecies.getCroppingPlanSpecies();
                speciesForCrop.add(cpeSpecies);
                Set<CroppingPlanSpecies> speciesForCropFallback = cropAndSpeciesFallback.getCroppingPlanSpecies();
                speciesForCropFallback.add(cpeSpecies);
            }
        }

        practicedSystemContext.setCropByCodeWithSpecies(cropByCodeWithSpecies);
        practicedSystemContext.setCropByCodeWithSpeciesFallback(cropByCodeWithSpecies);
    }

    private boolean isCropIsMix(CroppingPlanEntry croppingPlanEntry) {
        return CollectionUtils.isNotEmpty(croppingPlanEntry.getCroppingPlanSpecies()) &&
                croppingPlanEntry.getCroppingPlanSpecies().size() > 1 &&
                (croppingPlanEntry.isMixVariety() || croppingPlanEntry.isMixSpecies());
    }

    protected Set<PerformancePracticedInterventionExecutionContext> buildPerformancePracticedInterventionExecutionContexts(
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            List<PracticedIntervention> interventions,
            List<AbstractAction> cycleActions,
            PerformancePracticedCropExecutionContext cropContext) {

        PracticedSystem practicedSystem = practicedSystemContext.getPracticedSystem();

        Set<PerformancePracticedInterventionExecutionContext> result = new HashSet<>();

        Set<Integer> practicedCampaigns = CommonService.GET_CAMPAIGNS_SET.apply(practicedSystem.getCampaigns());

        if (cropContext.isNotUsedForThisCampaign()) {
            // in that case there are no intervention on it but an intervention context is created to compute indicator at crop level

            PerformancePracticedInterventionExecutionContext interventionContext = PerformancePracticedInterventionExecutionContext.
                    createNotUsedCropPerformancePracticedInterventionExecutionContext(
                            cropContext.getConnection(),
                            practicedCampaigns,
                            cropContext.getCropWithSpecies());

            result.add(interventionContext);

        }

        MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode = practicedSystemContext.getAllSubstancesActivesCommissionEuropeenneByAmmCode();
        String practicedSystemId = practicedSystemContext.getPracticedSystem().getTopiaId();
        List<HarvestingPrice> harvestingPrices = practicedSystemContext.getHarvestingPrices();

        for (PracticedIntervention intervention : interventions) {
            long start = System.currentTimeMillis();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("DEBUT CHARGEMENT DONNÉES POUR INTERVENTION '" + intervention.getName() + "'");
            }

            CropWithSpecies interventionCrop = intervention.isIntermediateCrop() ?
                    cropContext.getIntermediateCropWithSpecies() :
                    cropContext.getCropWithSpecies();

            if (interventionCrop == null) {
                if (LOGGER.isWarnEnabled()) {
                    String domainCode = domainExecutionContext.getDomain().getCode();
                    LOGGER.warn(String.format("For practicedSystem %s and intervention %s the crop could not be find into domain code %s",
                            practicedSystemId,
                            intervention.getTopiaId(),
                            domainCode));
                }
                continue;// not normal
            }

            // filter on intervention actions and inputs
            List<AbstractAction> interventionActions = cycleActions.stream()
                    .filter(action -> intervention.equals(action.getPracticedIntervention()))
                    .toList();

            ToolsCoupling toolsCoupling = getInterventionContextToolsCoupling(intervention);

            PerformancePracticedInterventionExecutionContext interventionContext = PerformancePracticedInterventionExecutionContext.
                    createPerformancePracticedInterventionExecutionContext(
                            intervention,
                            practicedCampaigns,
                            toolsCoupling,
                            interventionCrop,
                            interventionActions,
                            priceConverterKeysToRate,
                            harvestingPrices,
                            allSubstancesActivesCommissionEuropeenneByAmmCode
                    );

            result.add(interventionContext);

            Optional<HarvestingAction> optionalHarvestingAction = interventionContext.getOptionalHarvestingAction();
            Map<Pair<RefDestination, YealdUnit>, Double> yealdAveragesByDestinations =
                    performanceService.extractYealdAverage(optionalHarvestingAction, interventionCrop.getCroppingPlanEntry());
            interventionContext.setYealdAveragesByDestinations(yealdAveragesByDestinations);

            if (intervention.isIntermediateCrop()) {
                cropContext.addIntermediateInterventionYealdAverage(yealdAveragesByDestinations);
            } else {
                cropContext.addInterventionYealdAverage(yealdAveragesByDestinations);
            }

            addPerformancePracticedInterventionExecutionContextSpecies(intervention, interventionContext);

            addInterventionContextRefActaDosageSPCForPhytoInputs(cropContext, interventionContext);

            addInterventionContextRefActaTraitementsProduits(interventionContext);

            addAtciveSubstancesByProductsForPhytoProduct(interventionContext);

            if (LOGGER.isDebugEnabled()) {
                long end = System.currentTimeMillis();
                LOGGER.debug("FIN CHARGEMENT DONNÉES POUR INTERVENTION '" + intervention.getName() + "' réalisé en :" + ((end - start) / 1000) + " ms");
            }
        }

        Set<RefMateriel> materiels = result.stream()
                .filter(ic -> ic.getToolsCoupling() != null)
                .filter(ic -> ic.getToolsCoupling().getEquipments() != null)
                .filter(ic -> !ic.getToolsCoupling().getEquipments().isEmpty())
                .flatMap(ic -> ic.getToolsCoupling().getEquipments().stream().map(Equipment::getRefMateriel))
                .collect(Collectors.toSet());
        var correspondances = referentialService.findRefCorrespondanceMaterielOutilsTSForTools(materiels);
        result.forEach(ic -> ic.setCorrespondanceByRefMateriel(correspondances));

        return result;
    }

    protected void addInterventionContextRefActaTraitementsProduits(PerformancePracticedInterventionExecutionContext interventionContext) {

        Set<RefActaTraitementsProduit> refActaTraitementsProduits = getInterventionInputUsageRefActaTraitementsProduits(interventionContext);
        interventionContext.setRefActaTraitementsProduits(refActaTraitementsProduits);

        final List<DomainPhytoProductInput> domainPhytoProductInputs = new ArrayList<>();
        if (interventionContext.getOptionalSeedingActionUsage().isPresent()) {

            final List<DomainPhytoProductInput> phytoProductInputs = interventionContext.getOptionalSeedingActionUsage().get().getSeedLotInputUsage()
                    .stream()
                    .map(SeedLotInputUsage::getSeedingSpecies)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .map(SeedSpeciesInputUsage::getSeedProductInputUsages)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .map(SeedProductInputUsage::getDomainPhytoProductInput)
                    .filter(Objects::nonNull)
                    .toList();

            domainPhytoProductInputs.addAll(phytoProductInputs);

        }
        if (interventionContext.getOptionalBiologicalControlAction().isPresent() && CollectionUtils.isNotEmpty(interventionContext.getOptionalBiologicalControlAction().get().getBiologicalProductInputUsages())) {
            final List<DomainPhytoProductInput> phytoProductInputs = interventionContext.getOptionalBiologicalControlAction().get().getBiologicalProductInputUsages()
                    .stream()
                    .map(BiologicalProductInputUsage::getDomainPhytoProductInput)
                    .toList();
            domainPhytoProductInputs.addAll(phytoProductInputs);
        }
        if (interventionContext.getOptionalPesticidesSpreadingAction().isPresent() &&
                CollectionUtils.isNotEmpty(interventionContext.getOptionalPesticidesSpreadingAction().get().getPesticideProductInputUsages())) {

            final List<DomainPhytoProductInput> phytoProductInputs = interventionContext.getOptionalPesticidesSpreadingAction().get().getPesticideProductInputUsages()
                    .stream()
                    .map(PesticideProductInputUsage::getDomainPhytoProductInput)
                    .toList();
            domainPhytoProductInputs.addAll(phytoProductInputs);
        }

        refActaTraitementsProduits.addAll(domainPhytoProductInputs.stream().map(DomainPhytoProductInput::getRefInput).toList());

        if (CollectionUtils.isNotEmpty(refActaTraitementsProduits)) {
            Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> result = performanceService.getRefActaTraitementsProduitsCategFor(refActaTraitementsProduits);
            interventionContext.addTraitementProduitCategByIdTraitements(result);
        }
    }

    protected void addAtciveSubstancesByProductsForPhytoProduct(PerformancePracticedInterventionExecutionContext interventionContext) {

        Map<String, RefActaTraitementsProduit> newProductByIds = interventionContext.getRefIdProduitToProducts();

        Map<RefActaTraitementsProduit, List<RefActaSubstanceActive>> activeSubstancesByProducts = performanceService.
                getAtciveSubstancesByProductsForPhytoProductById(newProductByIds);

        performanceGlobalExecutionContext.addActiveSubstancesByProducts(activeSubstancesByProducts);

    }

    protected void addInterventionContextRefActaDosageSPCForPhytoInputs(PerformancePracticedCropExecutionContext cropContext,
                                                                        PerformancePracticedInterventionExecutionContext interventionContext) {

        List<CroppingPlanSpecies> species = interventionContext.getInterventionSpecies();
        if (CollectionUtils.isEmpty(species)) {
            return;
        }

        Set<RefActaTraitementsProduit> actaTraitementProducts = getInterventionInputUsageRefActaTraitementsProduits(interventionContext);

        if (CollectionUtils.isEmpty(actaTraitementProducts)) {
            return;
        }

        Set<RefEspece> refEspeces = species.stream()
                .map(CroppingPlanSpecies::getSpecies)
                .collect(Collectors.toSet());
        CropWithSpecies cropWithSpecies = interventionContext.getCropWithSpecies();

        if (species.size() == cropWithSpecies.getCroppingPlanSpecies().size()) {
            // all species are selected so we can use the RefActaDosageSPC from crop
            Map<TraitementProduitWithSpecies, ReferenceDoseDTO> cropDoseForProducts = cropContext.getDoseForProducts();
            if (cropDoseForProducts == null) {
                Map<RefActaTraitementsProduit, ReferenceDoseDTO> doseForProducts =
                        performanceService.getLegacyDoseForActaProducts(actaTraitementProducts, refEspeces);
                cropDoseForProducts = addCropWithSpeciesToMapKeys(doseForProducts, cropWithSpecies);
                cropContext.setDoseForProducts(cropDoseForProducts);
            } else {
                Set<TraitementProduitWithSpecies> traitementProduitWithSpecies = actaTraitementProducts.stream()
                        .map(traitementsProduit -> new TraitementProduitWithSpecies(traitementsProduit, cropWithSpecies))
                        .collect(Collectors.toSet());
                traitementProduitWithSpecies.removeAll(cropDoseForProducts.keySet());
                if (!traitementProduitWithSpecies.isEmpty()) {
                    Set<RefActaTraitementsProduit> actaTraitementProducts0 = traitementProduitWithSpecies.stream()
                            .map(TraitementProduitWithSpecies::refActaTraitementsProduit)
                            .collect(Collectors.toSet());
                    Map<RefActaTraitementsProduit, ReferenceDoseDTO> doseForProducts =
                            performanceService.getLegacyDoseForActaProducts(actaTraitementProducts0, refEspeces);
                    cropDoseForProducts.putAll(addCropWithSpeciesToMapKeys(doseForProducts, cropWithSpecies));
                }
            }
            interventionContext.setLegacyRefDosageForPhytoInputs(cropDoseForProducts);

        } else {

            Map<RefActaTraitementsProduit, ReferenceDoseDTO> doseForProducts =
                    performanceService.getLegacyDoseForActaProducts(actaTraitementProducts, refEspeces);
            Map<TraitementProduitWithSpecies, ReferenceDoseDTO> cropDoseForProducts = addCropWithSpeciesToMapKeys(doseForProducts, cropWithSpecies);
            interventionContext.setLegacyRefDosageForPhytoInputs(cropDoseForProducts);

        }

    }

    private Set<RefActaTraitementsProduit> getInterventionInputUsageRefActaTraitementsProduits(PerformancePracticedInterventionExecutionContext interventionContext) {
        Set<RefActaTraitementsProduit> actaTraitementProducts = new HashSet<>();

        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionContext.getOptionalBiologicalControlAction();
        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction();

        actaTraitementProducts.addAll(getBiologicalControlActionRefActaTraitementsProduits(optionalBiologicalControlAction));
        actaTraitementProducts.addAll(getPesticidesSpreadingActionRefActaTraitementsProduits(optionalPesticidesSpreadingAction));

        return actaTraitementProducts;
    }

    protected static Set<RefActaTraitementsProduit> getPesticidesSpreadingActionRefActaTraitementsProduits(Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction) {
        Set<RefActaTraitementsProduit> actaTraitementProducts = new HashSet<>();
        if (optionalPesticidesSpreadingAction.isPresent()) {
            final Collection<PesticideProductInputUsage> pesticideProductInputUsages = optionalPesticidesSpreadingAction.get().getPesticideProductInputUsages();
            if (CollectionUtils.isNotEmpty(pesticideProductInputUsages)) {
                for (PesticideProductInputUsage pesticideProductInputUsage : pesticideProductInputUsages) {
                    actaTraitementProducts.add(pesticideProductInputUsage.getDomainPhytoProductInput().getRefInput());
                }
            }
        }
        return actaTraitementProducts;
    }

    protected static Set<RefActaTraitementsProduit> getBiologicalControlActionRefActaTraitementsProduits(Optional<BiologicalControlAction> optionalBiologicalControlAction) {
        Set<RefActaTraitementsProduit> actaTraitementProducts = new HashSet<>();
        if (optionalBiologicalControlAction.isPresent()) {
            final Collection<BiologicalProductInputUsage> biologicalProductInputUsages = optionalBiologicalControlAction.get().getBiologicalProductInputUsages();
            if (CollectionUtils.isNotEmpty(biologicalProductInputUsages)) {
                for (BiologicalProductInputUsage biologicalProductInputUsage : biologicalProductInputUsages) {
                    actaTraitementProducts.add(biologicalProductInputUsage.getDomainPhytoProductInput().getRefInput());
                }
            }
        }
        return actaTraitementProducts;
    }

    protected Map<TraitementProduitWithSpecies, ReferenceDoseDTO> addCropWithSpeciesToMapKeys(Map<RefActaTraitementsProduit, ReferenceDoseDTO> doseForProducts,
                                                                                              CropWithSpecies cropWithSpecies) {
        Map<TraitementProduitWithSpecies, ReferenceDoseDTO> cropDoseForProducts = new HashMap<>();
        for (RefActaTraitementsProduit refActaTraitementsProduit : doseForProducts.keySet()) {
            cropDoseForProducts.put(new TraitementProduitWithSpecies(refActaTraitementsProduit, cropWithSpecies),
                    doseForProducts.get(refActaTraitementsProduit));
        }
        return cropDoseForProducts;
    }

    protected void addPerformancePracticedInterventionExecutionContextSpecies(PracticedIntervention intervention,
                                                                              PerformancePracticedInterventionExecutionContext interventionContext) {
        List<CroppingPlanSpecies> interventionSpecies = new ArrayList<>();
        final Collection<PracticedSpeciesStade> speciesStades = intervention.getSpeciesStades();
        final Set<CroppingPlanSpecies> interventionCropSpecies = interventionContext.getCropWithSpecies().getCroppingPlanSpecies();

        if (CollectionUtils.isNotEmpty(speciesStades) && CollectionUtils.isNotEmpty(interventionCropSpecies)) {

            for (PracticedSpeciesStade speciesStade : speciesStades) {
                String speciesCode = speciesStade.getSpeciesCode();
                Optional<CroppingPlanSpecies> optionalCroppingPlanSpecies = interventionCropSpecies.stream().
                        filter(croppingPlanSpecies -> croppingPlanSpecies.getCode().contentEquals(speciesCode)).
                        findAny();
                optionalCroppingPlanSpecies.ifPresent(interventionSpecies::add);
            }
        }

        interventionContext.setInterventionSpecies(interventionSpecies);
    }

    protected ToolsCoupling getInterventionContextToolsCoupling(
            PracticedIntervention intervention) {

        ToolsCoupling toolsCoupling = null;
        Collection<String> toolsCouplingCodes = intervention.getToolsCouplingCodes();
        if (CollectionUtils.isNotEmpty(toolsCouplingCodes)) {
            // must have only one
            String toolsCouplingCode = toolsCouplingCodes.iterator().next();
            toolsCoupling = domainExecutionContext.getToolsCouplingByCode().get(toolsCouplingCode);
        }
        return toolsCoupling;
    }

}
