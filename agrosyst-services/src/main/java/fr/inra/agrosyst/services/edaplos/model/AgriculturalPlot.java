package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jakarta.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * {@code
 * <ram:Identification>ID parcelle DAPLOS</ram:Identification> <!-- eDaplosIssuerId. Message d'erreur si non présent -->
 * <ram:Previous_Identification>45896625488858</ram:Previous_Identification>
 * <ram:Description>Cimetierre/ 07 08</ram:Description> <!-- Sert de nom, Message d'erreur si non présent -->
 * <ram:DataSheetStartDateTime>2007-10-14T09:30:47Z</ram:DataSheetStartDateTime>
 * <ram:DataSheetEndDateTime>2008-09-17T09:30:47Z</ram:DataSheetEndDateTime>
 * <ram:SequenceNumeric>0.0</ram:SequenceNumeric>
 * <ram:IssuerInternalReference>87HQQ55d995EFERF</ram:IssuerInternalReference>
 * <ram:HarvestedYear>2001</ram:HarvestedYear> <!-- Message d'erreur si non présent ou si invalide -->
 * }
 * </pre>
 */
public class AgriculturalPlot implements AgroEdiObject {

    protected String identification;

    protected String previousIdentification;

    protected String description;

    protected String dataSheetStartDateTime;

    protected String dataSheetEndDateTime;

    protected String sequenceNumeric;

    protected String issuerInternalReference;

    protected String harvestedYear;

    protected List<AgriculturalCountrySubDivision> includedInAgriculturalCountrySubDivisions = new ArrayList<>();

    @Valid
    protected List<TechnicalCharacteristic> applicableTechnicalCharacteristics = new ArrayList<>();

    @Valid
    protected List<AgriculturalArea> includedAgriculturalAreas = new ArrayList<>();

    @Valid
    protected List<PlotSoilOccupation> appliedPlotSoilOccupations = new ArrayList<>();

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getPreviousIdentification() {
        return previousIdentification;
    }

    public void setPreviousIdentification(String previousIdentification) {
        this.previousIdentification = previousIdentification;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDataSheetStartDateTime() {
        return dataSheetStartDateTime;
    }

    public void setDataSheetStartDateTime(String dataSheetStartDateTime) {
        this.dataSheetStartDateTime = dataSheetStartDateTime;
    }

    public String getDataSheetEndDateTime() {
        return dataSheetEndDateTime;
    }

    public void setDataSheetEndDateTime(String dataSheetEndDateTime) {
        this.dataSheetEndDateTime = dataSheetEndDateTime;
    }

    public String getSequenceNumeric() {
        return sequenceNumeric;
    }

    public void setSequenceNumeric(String sequenceNumeric) {
        this.sequenceNumeric = sequenceNumeric;
    }

    public String getIssuerInternalReference() {
        return issuerInternalReference;
    }

    public void setIssuerInternalReference(String issuerInternalReference) {
        this.issuerInternalReference = issuerInternalReference;
    }

    public String getHarvestedYear() {
        return harvestedYear;
    }

    public void setHarvestedYear(String harvestedYear) {
        this.harvestedYear = harvestedYear;
    }

    public List<AgriculturalCountrySubDivision> getIncludedInAgriculturalCountrySubDivisions() {
        return includedInAgriculturalCountrySubDivisions;
    }

    public void setIncludedInAgriculturalCountrySubDivisions(List<AgriculturalCountrySubDivision> includedInAgriculturalCountrySubDivisions) {
        this.includedInAgriculturalCountrySubDivisions = includedInAgriculturalCountrySubDivisions;
    }

    public void addIncludedInAgriculturalCountrySubDivision(AgriculturalCountrySubDivision includedInAgriculturalCountrySubDivision) {
        includedInAgriculturalCountrySubDivisions.add(includedInAgriculturalCountrySubDivision);
    }

    public List<TechnicalCharacteristic> getApplicableTechnicalCharacteristics() {
        return applicableTechnicalCharacteristics;
    }

    public void setApplicableTechnicalCharacteristics(List<TechnicalCharacteristic> applicableTechnicalCharacteristics) {
        this.applicableTechnicalCharacteristics = applicableTechnicalCharacteristics;
    }

    public void addApplicableTechnicalCharacteristic(TechnicalCharacteristic applicableTechnicalCharacteristic) {
        applicableTechnicalCharacteristics.add(applicableTechnicalCharacteristic);
    }

    public void addIncludedAgriculturalArea(AgriculturalArea agriculturalArea) {
        includedAgriculturalAreas.add(agriculturalArea);
    }

    public List<AgriculturalArea> getIncludedAgriculturalAreas() {
        return includedAgriculturalAreas;
    }

    public void setIncludedAgriculturalAreas(List<AgriculturalArea> includedAgriculturalAreas) {
        this.includedAgriculturalAreas = includedAgriculturalAreas;
    }

    public List<PlotSoilOccupation> getAppliedPlotSoilOccupations() {
        return appliedPlotSoilOccupations;
    }

    public void setAppliedPlotSoilOccupations(List<PlotSoilOccupation> appliedPlotSoilOccupations) {
        this.appliedPlotSoilOccupations = appliedPlotSoilOccupations;
    }

    public void addAppliedPlotSoilOccupation(PlotSoilOccupation appliedPlotSoilOccupation) {
        appliedPlotSoilOccupations.add(appliedPlotSoilOccupation);
    }

    @Override
    public String getLocalizedIdentifier() {
        return "parcelle '" + identification + "'";
    }
}
