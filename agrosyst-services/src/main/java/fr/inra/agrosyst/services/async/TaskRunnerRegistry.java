package fr.inra.agrosyst.services.async;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import fr.inra.agrosyst.api.services.async.Task;
import fr.inra.agrosyst.services.domain.export.DomainExportTask;
import fr.inra.agrosyst.services.domain.export.DomainExportTaskRunner;
import fr.inra.agrosyst.services.effective.export.EffectiveCropCyclesExportTask;
import fr.inra.agrosyst.services.effective.export.EffectiveCropCyclesExportTaskRunner;
import fr.inra.agrosyst.services.growingplan.export.GrowingPlanExportTask;
import fr.inra.agrosyst.services.growingplan.export.GrowingPlanExportTaskRunner;
import fr.inra.agrosyst.services.growingsystem.export.GrowingSystemExportTask;
import fr.inra.agrosyst.services.growingsystem.export.GrowingSystemExportTaskRunner;
import fr.inra.agrosyst.services.managementmode.export.DecisionRuleExportTask;
import fr.inra.agrosyst.services.managementmode.export.DecisionRuleExportTaskRunner;
import fr.inra.agrosyst.services.managementmode.export.ManagementModeExportTask;
import fr.inra.agrosyst.services.managementmode.export.ManagementModeExportTaskRunner;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportTask;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportTaskRunner;
import fr.inra.agrosyst.services.performance.PerformanceDbTask;
import fr.inra.agrosyst.services.performance.PerformanceDbTaskRunner;
import fr.inra.agrosyst.services.performance.PerformanceFileTask;
import fr.inra.agrosyst.services.performance.PerformanceFileTaskRunner;
import fr.inra.agrosyst.services.plot.export.PlotExportTask;
import fr.inra.agrosyst.services.plot.export.PlotExportTaskRunner;
import fr.inra.agrosyst.services.practiced.export.PracticedSystemExportTask;
import fr.inra.agrosyst.services.practiced.export.PracticedSystemExportTaskRunner;
import fr.inra.agrosyst.services.referential.maa.MaaApiImportTask;
import fr.inra.agrosyst.services.referential.maa.MaaApiImportTaskRunner;
import fr.inra.agrosyst.services.report.ReportGrowingSystemsExportPdfTask;
import fr.inra.agrosyst.services.report.ReportGrowingSystemsExportPdfTaskRunner;
import fr.inra.agrosyst.services.report.ReportGrowingSystemsExportTask;
import fr.inra.agrosyst.services.report.ReportGrowingSystemsExportTaskRunner;
import fr.inra.agrosyst.services.report.ReportRegionalsExportTask;
import fr.inra.agrosyst.services.report.ReportRegionalsExportTaskRunner;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class TaskRunnerRegistry {

    private static final Log LOGGER = LogFactory.getLog(TaskRunnerRegistry.class);

    private static volatile TaskRunnerRegistry instance = null;

    public TaskRunnerRegistry() {
        super();
    }

    public static TaskRunnerRegistry getInstance() {
        //Le "Double-Checked Singleton"/"Singleton doublement vérifié" permet
        //d'éviter un appel coûteux à synchronized,
        //une fois que l'instanciation est faite.
        if (TaskRunnerRegistry.instance == null) {
            // Le mot-clé synchronized sur ce bloc empêche toute instanciation
            // multiple même par différents "threads".
            // Il est TRES important.
            synchronized(TaskRunnerRegistry.class) {
                if (TaskRunnerRegistry.instance == null) {
                    TaskRunnerRegistry.instance = new TaskRunnerRegistry();
                }
            }
        }
        return TaskRunnerRegistry.instance;
    }

    public static final ImmutableMap<Class<?>, Class<?>> RUNNERS;

    private static <K extends Task, V extends TaskRunner<K>> void register(ImmutableMap.Builder<Class<?>, Class<?>> builder, Class<K> taskClass, Class<V> runnerClass) {
        builder.put(taskClass, runnerClass);
    }

    static {
        ImmutableMap.Builder<Class<?>, Class<?>> builder = ImmutableMap.builder();
        register(builder, PerformanceDbTask.class, PerformanceDbTaskRunner.class);
        register(builder, PerformanceFileTask.class, PerformanceFileTaskRunner.class);
        register(builder, DomainExportTask.class, DomainExportTaskRunner.class);
        register(builder, GrowingPlanExportTask.class, GrowingPlanExportTaskRunner.class);
        register(builder, GrowingSystemExportTask.class, GrowingSystemExportTaskRunner.class);
        register(builder, PlotExportTask.class, PlotExportTaskRunner.class);
        register(builder, EffectiveCropCyclesExportTask.class, EffectiveCropCyclesExportTaskRunner.class);
        register(builder, MeasurementExportTask.class, MeasurementExportTaskRunner.class);
        register(builder, DecisionRuleExportTask.class, DecisionRuleExportTaskRunner.class);
        register(builder, ManagementModeExportTask.class, ManagementModeExportTaskRunner.class);
        register(builder, PracticedSystemExportTask.class, PracticedSystemExportTaskRunner.class);
        register(builder, ReportGrowingSystemsExportTask.class, ReportGrowingSystemsExportTaskRunner.class);
        register(builder, ReportGrowingSystemsExportPdfTask.class, ReportGrowingSystemsExportPdfTaskRunner.class);
        register(builder, ReportRegionalsExportTask.class, ReportRegionalsExportTaskRunner.class);
        register(builder, MaaApiImportTask.class, MaaApiImportTaskRunner.class);
        RUNNERS = builder.build();
    }

    protected <G extends Task> Class<TaskRunner<G>> findTaskRunnerClass(G task) {
        Class<? extends Task> taskClass = task.getClass();
        // On cherche par match parfait de la classe
        Class<TaskRunner<G>> runnerClass = (Class<TaskRunner<G>>) RUNNERS.get(taskClass);
        // Si on ne trouve pas, on cherche par hiérarchie
        if (runnerClass == null) {
            Optional<Map.Entry<Class<?>, Class<?>>> optional = Iterables.tryFind(RUNNERS.entrySet(), entry -> entry.getKey().isAssignableFrom(taskClass));
            runnerClass = (Class<TaskRunner<G>>) optional.transform(Map.Entry::getValue).orNull();
        }
        Preconditions.checkState(runnerClass != null, "Unsupported type: " + task.getClass().getName());
        return runnerClass;
    }

    public <G extends Task> TaskRunner<G> getRunner(G task) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class<TaskRunner<G>> runnerClass = findTaskRunnerClass(task);
        TaskRunner<G> runner = runnerClass.getConstructor().newInstance();
        return runner;
    }

}
