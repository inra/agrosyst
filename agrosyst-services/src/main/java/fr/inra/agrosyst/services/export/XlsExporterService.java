package fr.inra.agrosyst.services.export;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.Cattle;
import fr.inra.agrosyst.api.entities.LivestockUnit;
import fr.inra.agrosyst.api.entities.LivestockUnitTopiaDao;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.referential.PastureType;
import fr.inra.agrosyst.api.entities.referential.QualityAttributeType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressorTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrgaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaTopiaDao;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.action.ActionWithOtherProductInputUsagesDto;
import fr.inra.agrosyst.api.services.action.BiologicalControlActionDto;
import fr.inra.agrosyst.api.services.action.CarriageActionDto;
import fr.inra.agrosyst.api.services.action.HarvestingActionDto;
import fr.inra.agrosyst.api.services.action.HarvestingActionValorisationDto;
import fr.inra.agrosyst.api.services.action.IrrigationActionDto;
import fr.inra.agrosyst.api.services.action.MaintenancePruningVinesActionDto;
import fr.inra.agrosyst.api.services.action.MineralFertilizersSpreadingActionDto;
import fr.inra.agrosyst.api.services.action.OrganicFertilizersSpreadingActionDto;
import fr.inra.agrosyst.api.services.action.OtherActionDto;
import fr.inra.agrosyst.api.services.action.PesticidesSpreadingActionDto;
import fr.inra.agrosyst.api.services.action.QualityCriteriaDto;
import fr.inra.agrosyst.api.services.action.SeedingActionUsageDto;
import fr.inra.agrosyst.api.services.action.TillageActionDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainMineralProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainOrganicProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedSpeciesInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSubstrateInputDto;
import fr.inra.agrosyst.api.services.export.InterventionDto;
import fr.inra.agrosyst.api.services.input.MineralProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.OrganicProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.OtherProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.PhytoProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.PhytoProductTargetDto;
import fr.inra.agrosyst.api.services.input.PotInputUsageDto;
import fr.inra.agrosyst.api.services.input.SeedLotInputUsageDto;
import fr.inra.agrosyst.api.services.input.SeedSpeciesInputUsageDto;
import fr.inra.agrosyst.api.services.input.SubstrateInputUsageDto;
import fr.inra.agrosyst.api.services.itk.SpeciesStadeDto;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.CacheDiscriminator;
import fr.inra.agrosyst.services.common.CacheService;
import fr.inra.agrosyst.services.effective.export.EffectiveXlsContext;
import fr.inra.agrosyst.services.practiced.export.PracticedSystemMetadata;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static fr.inra.agrosyst.api.services.common.PricesService.LOGGER;


@Setter
public abstract class XlsExporterService extends AbstractAgrosystService {
    protected CacheService cacheService;
    protected ReferentialService referentialService;

    protected LivestockUnitTopiaDao livestockUnitDao;
    private RefBioAgressorTopiaDao refBioAgressorTopiaDao;

    protected RefFertiOrgaTopiaDao refFertiOrgaTopiaDao;
    protected RefActaTraitementsProduitTopiaDao refActaTraitementsProduitTopiaDao;

    private RefQualityCriteriaTopiaDao refQualityCriteriaTopiaDao;

    protected void exportActionAndInputFields(ExportContext exportContext,
                                              PracticedSystemMetadata.Bean baseItkBean,
                                              InterventionDto intervention,
                                              AbstractActionDto action,
                                              Supplier<Map<String, ExportMetadata.SpeciesAndVariety>> refEspeceByCPSCodeSupplier) {

        ExportMetadata.Bean baseActionBean = new ExportMetadata.Bean(baseItkBean);

        boolean effective = exportContext instanceof EffectiveXlsContext;

        // common part
        baseActionBean.setActionType(action.getMainActionInterventionAgrosyst());
        baseActionBean.setAction(action.getMainActionReference_label().orElse(""));
        baseActionBean.setMainAction(StringUtils.isNotBlank(action.getToolsCouplingCode().orElse("")));
        baseActionBean.setActionComment(action.getComment().orElse(""));

        List<SpeciesStadeDto> speciesStadeDtos = ListUtils.emptyIfNull(intervention.getSpeciesStadesDtos());
        Map<String, ExportMetadata.SpeciesAndVariety> refEspeceByCPSCode = refEspeceByCPSCodeSupplier.get();

        // specific part
        switch (action) {
            case final MineralFertilizersSpreadingActionDto mineralFertilizersSpreadingAction -> {
                List<ExportMetadata.ItkActionApplicationProduitsMinerauxBean> subList = exportMineralFertilizersSpreadingAction(baseActionBean, intervention, mineralFertilizersSpreadingAction);
                subList.forEach(exportContext::addApplicationProduitsMineraux);
            }
            case PesticidesSpreadingActionDto pesticidesSpreadingActionDto -> {
                List<ExportMetadata.ItkApplicationProduitsPhytoBean> subList = exportPesticidesSpreadingAction(baseActionBean, pesticidesSpreadingActionDto, refEspeceByCPSCode);
                subList.forEach(exportContext::addApplicationProduitsPhyto);
            }
            case OtherActionDto otherActionDto -> {
                List<ExportMetadata.ItkActionAutreBean> subList = exportOtherAction(baseActionBean, otherActionDto);
                subList.forEach(exportContext::addActionAutre);
            }
            case MaintenancePruningVinesActionDto maintenancePruningVinesActionDto -> {
                List<ExportMetadata.ItkActionEntretienTailleVigneBean> subList = exportMaintenancePruningVinesAction(baseActionBean, maintenancePruningVinesActionDto);
                subList.forEach(exportContext::addActionEntretienTailleVigne);
            }
            case OrganicFertilizersSpreadingActionDto organicFertilizersSpreadingActionDto -> {
                List<ExportMetadata.ItkActionEpandageOrganiqueBean> subList = exportOrganicFertilizersSpreadingAction(baseActionBean, intervention, organicFertilizersSpreadingActionDto);
                subList.forEach(exportContext::addActionEpandageOrganique);
            }
            case IrrigationActionDto irrigationActionDto -> {
                List<ExportMetadata.ItkActionIrrigationBean> subList = exportIrrigationAction(baseActionBean, irrigationActionDto);
                subList.forEach(exportContext::addActionIrrigation);
            }
            case BiologicalControlActionDto biologicalControlActionDto -> {
                List<ExportMetadata.ItkActionLutteBiologiqueBean> subList = exportBiologicalControlAction(baseActionBean, biologicalControlActionDto, refEspeceByCPSCode);
                subList.forEach(exportContext::addActionLutteBiologique);
            }
            case HarvestingActionDto harvestingActionDto -> {
                List<ExportMetadata.ItkActionRecolteBean> subList = exportHarvestingAction(baseActionBean, speciesStadeDtos, refEspeceByCPSCodeSupplier, harvestingActionDto, intervention.getDomainId(), effective);
                subList.forEach(exportContext::addActionRecolte);
            }
            case SeedingActionUsageDto seedingActionUsageDto -> {
                List<ExportMetadata.ItkActionSemiBean> subList = exportSeedingAction(speciesStadeDtos, baseActionBean, seedingActionUsageDto, refEspeceByCPSCode);
                subList.forEach(exportContext::addActionSemi);
            }
            case CarriageActionDto carriageActionDto -> {
                ExportMetadata.ItkActionTransportBean exportCarriageAction = exportCarriageAction(baseActionBean, carriageActionDto);
                exportContext.addActionTransport(exportCarriageAction);
            }
            case TillageActionDto tillageActionDto -> {
                ExportMetadata.ItkActionTravailSolBean exportTillageAction = exportTillageAction(baseActionBean, tillageActionDto);
                exportContext.addActionTravailSol(exportTillageAction);
            }
            default -> throw new AgrosystTechnicalException("Unhandled action : " + action);
        }
    }

    protected List<ExportMetadata.ItkActionSemiBean> exportSeedingAction(List<SpeciesStadeDto> speciesStadeDtos,
                                                                         ExportMetadata.Bean export,
                                                                         SeedingActionUsageDto action,
                                                                         Map<String, ExportMetadata.SpeciesAndVariety> refEspeceByCPSCode) {

        List<ExportMetadata.ItkActionSemiBean> result = new LinkedList<>();

        Map<String, String> groupesCiblesParCode = referentialService.getGroupesCiblesParCode();

        Supplier<ExportMetadata.ItkActionSemiBean> actionSupplier = () -> {
            ExportMetadata.ItkActionSemiBean newBean = new ExportMetadata.ItkActionSemiBean(export);
            newBean.setSeedType(action.getSeedType());
            newBean.setYieldTarget(action.getYealdTarget());
            newBean.setYealdUnit(action.getYealdUnit());
            return newBean;
        };

        // species
        Set<String> usedSpeciesCodes = action.getSeedLotInputUsageDtos().stream()
                .flatMap(usage -> usage.getSeedingSpeciesDtos().orElse(Lists.newArrayList()).stream())
                .map(seedSpeciesInputUsageDto -> seedSpeciesInputUsageDto.getDomainSeedSpeciesInputDto().getSpeciesSeedDto().getCode())
                .collect(Collectors.toSet());

        Set<String> refEspeceIds = new HashSet<>();

        for (SpeciesStadeDto speciesStadeDto : speciesStadeDtos) {
            String speciesCode = speciesStadeDto.getSpeciesCode();
            if (!usedSpeciesCodes.contains(speciesCode)) {
                ExportMetadata.ItkActionSemiBean exportSpecies = getItkActionSemiBean(
                        action,
                        refEspeceByCPSCode,
                        actionSupplier,
                        refEspeceIds,
                        speciesCode,
                        speciesStadeDto.getSpeciesName(),
                        speciesStadeDto.getVarietyName()
                );
                exportSpecies.setSeedingsActionSpeciesComment("Aucune quantité semée n'est saisie pour cette espèce");
                result.add(exportSpecies);

            } else {
                for (SeedLotInputUsageDto lot : action.getSeedLotInputUsageDtos()) {
                    for (SeedSpeciesInputUsageDto speciesUsage : lot.getSeedingSpeciesDtos().orElse(Lists.newArrayList())) {
                        DomainSeedSpeciesInputDto input = speciesUsage.getDomainSeedSpeciesInputDto();
                        CroppingPlanSpeciesDto species = input.getSpeciesSeedDto();

                        if (speciesCode.equals(species.getCode())) {
                            ExportMetadata.ItkActionSemiBean exportSpecies = getItkActionSemiBean(
                                    action,
                                    refEspeceByCPSCode,
                                    actionSupplier,
                                    refEspeceIds,
                                    speciesCode,
                                    speciesStadeDto.getSpeciesName(), speciesStadeDto.getVarietyName());
                            exportSpecies.setSeedType(input.getSeedType());
                            exportSpecies.setTreatment(input.isChemicalTreatment());
                            exportSpecies.setBiologicalSeedInoculation(input.isBiologicalSeedInoculation());
                            exportSpecies.setQuantity(speciesUsage.getQtAvg());
                            exportSpecies.setSeedPlantUnit(speciesUsage.getUsageUnit());
                            exportSpecies.setDeepness(lot.getDeepness());
                            exportSpecies.setSeedingsActionSpeciesComment(speciesUsage.getComment());
                            result.add(exportSpecies);

                        }
                    }
                }
            }
        }

        String actionSpecies = result.stream()
                .map(ExportMetadata.ItkActionSemiBean::getActionSpecies)
                .distinct()
                .collect(Collectors.joining(", "));
        String actionVarieties = result.stream()
                .map(ExportMetadata.ItkActionSemiBean::getActionVarieties)
                .distinct()
                .collect(Collectors.joining(", "));

        // input
        for (SeedLotInputUsageDto seedLotInputUsageDto : action.getSeedLotInputUsageDtos()) {
            List<SeedSpeciesInputUsageDto> seedSpeciesInputUsageDtos = seedLotInputUsageDto.getSeedingSpeciesDtos().orElse(Lists.newArrayList());
            for (SeedSpeciesInputUsageDto seedSpeciesInputUsageDto : seedSpeciesInputUsageDtos) {
                ExportMetadata.ItkActionSemiBean exportInput = actionSupplier.get();
                exportInput.setActionSpecies(actionSpecies);
                exportInput.setActionVarieties(actionVarieties);

                for (PhytoProductInputUsageDto phytoProductInputUsageDto : seedSpeciesInputUsageDto.getSeedProductInputDtos().orElse(Lists.newArrayList())) {

                    List<PhytoProductTargetDto> targets = phytoProductInputUsageDto.getTargets().orElse(Lists.newArrayList());
                    if (!targets.isEmpty()) {
                        String groupesCiblesLabels = targets.stream()
                                .map(PhytoProductTargetDto::getCodeGroupeCibleMaa)
                                .filter(Objects::nonNull)
                                .map(groupesCiblesParCode::get)
                                .collect(Collectors.joining(", "));
                        exportInput.setGroupesCibles(groupesCiblesLabels);

                        String targetsString = targets.stream()
                                .map(PhytoProductTargetDto::getRefBioAggressorLabel)
                                .collect(Collectors.joining(", "));
                        exportInput.setTargets(targetsString);
                    }
                    String productName = seedSpeciesInputUsageDto.getProductName().orElse(phytoProductInputUsageDto.getProductName().orElse(""));
                    RefActaTraitementsProduit phytoProduct = refActaTraitementsProduitTopiaDao.forTopiaIdEquals(phytoProductInputUsageDto.getDomainPhytoProductInputDto().getRefInputId()).findAnyOrNull();
                    if (phytoProduct != null) {
                        exportInput.setPhytoProduct(phytoProduct.getNom_traitement());
                        if (CollectionUtils.isNotEmpty(refEspeceIds)) {
                            Optional<ReferenceDoseDTO> referenceDose = findDoseWithValueUseCacheIFTancienne(phytoProduct.getTopiaId(), refEspeceIds);
                            referenceDose.ifPresent(dose -> {
                                exportInput.setReferenceDose(dose.getValue());
                                exportInput.setReferenceDoseUnit(dose.getUnit());
                            });
                        }
                    }
                    exportInput.setSeedType(seedSpeciesInputUsageDto.getDomainSeedSpeciesInputDto().getSeedType());
                    exportInput.setProductName(productName);
                    exportInput.setInputType(seedSpeciesInputUsageDto.getInputType());
                    exportInput.setQtMin(seedSpeciesInputUsageDto.getQtMin());
                    exportInput.setQtAvg(seedSpeciesInputUsageDto.getQtAvg());
                    exportInput.setQtMed(seedSpeciesInputUsageDto.getQtMed());
                    exportInput.setQtMax(seedSpeciesInputUsageDto.getQtMax());
                    exportInput.setInputUnit(seedSpeciesInputUsageDto.getUsageUnit().toString());

                    result.add(exportInput);
                }
            }

            // other input
            Supplier<ExportMetadata.ItkActionSemiBean> supplierWithSpeciesAndVarieties = () -> {
                ExportMetadata.ItkActionSemiBean exportInput = actionSupplier.get();
                exportInput.setActionSpecies(actionSpecies);
                exportInput.setActionVarieties(actionVarieties);
                return exportInput;
            };

            fillWithOtherProductInputUsageDtos(action, supplierWithSpeciesAndVarieties, result);

            // substrate
            Supplier<ExportMetadata.ItkActionSemiBean> substratSupplierWithSpeciesAndVarieties = () -> {
                ExportMetadata.ItkActionSemiBean exportInput = actionSupplier.get();
                exportInput.setActionSpecies(actionSpecies);
                exportInput.setActionVarieties(actionVarieties);
                return exportInput;
            };
            fillWithSubstrateInputUsageDtos(action, substratSupplierWithSpeciesAndVarieties, result);
        }
        return result;
    }

    private ExportMetadata.ItkActionSemiBean getItkActionSemiBean(SeedingActionUsageDto action,
                                                                 Map<String, ExportMetadata.SpeciesAndVariety> refEspeceByCPSCode,
                                                                 Supplier<ExportMetadata.ItkActionSemiBean> actionSupplier,
                                                                 Set<String> refEspeceIds,
                                                                 String speciesCode,
                                                                 String speciesStadeDtoName,
                                                                 String speciesStadeDtoVarietyName) {
        ExportMetadata.ItkActionSemiBean exportSpecies = actionSupplier.get();
        ExportMetadata.SpeciesAndVariety speciesAndVariety = refEspeceByCPSCode.get(speciesCode);
        if (speciesAndVariety != null) {
            refEspeceIds.add(speciesAndVariety.species().getTopiaId());
            exportSpecies.setActionSpecies(StringUtils.firstNonBlank(speciesAndVariety.getSpeciesName(), speciesStadeDtoName));
            exportSpecies.setActionVarieties(StringUtils.firstNonBlank(speciesAndVariety.getVarietyName(), speciesStadeDtoVarietyName));
        } else if (LOGGER.isErrorEnabled()) {
            LOGGER.error(String.format(
                    "No refEspece found for species code '%s' on action '%s'. The export for this species is rejected.",
                    speciesCode,
                    action.getTopiaId()
            ));
        }
        return exportSpecies;
    }

    protected List<ExportMetadata.ItkActionApplicationProduitsMinerauxBean> exportMineralFertilizersSpreadingAction(
            ExportMetadata.Bean baseActionBean,
            InterventionDto intervention,
            MineralFertilizersSpreadingActionDto action) {

        List<ExportMetadata.ItkActionApplicationProduitsMinerauxBean> result = new LinkedList<>();

        // action
        Supplier<ExportMetadata.ItkActionApplicationProduitsMinerauxBean> supplier = () -> {
            ExportMetadata.ItkActionApplicationProduitsMinerauxBean newBean = new ExportMetadata.ItkActionApplicationProduitsMinerauxBean(baseActionBean);
            newBean.setBurial(action.isBurial());
            newBean.setLocalizedSpreading(action.isLocalizedSpreading());
            return newBean;
        };

        double psci = intervention.getTemporalFrequencyOrTransitCount() * intervention.getSpatialFrequency();

        // input
        Collection<MineralProductInputUsageDto> usages = action.getMineralProductInputUsageDtos().orElse(Lists.newArrayList());
        for (MineralProductInputUsageDto usage : usages) {
            ExportMetadata.ItkActionApplicationProduitsMinerauxBean exportInput = supplier.get();

            exportInput.setInputType(usage.getInputType());
            DomainMineralProductInputDto inputDto = usage.getDomainMineralProductInputDto();
            Double qtAvg = usage.getQtAvg();

            if (inputDto != null) {
                exportInput.setTypeProduit(inputDto.getType_produit());
                exportInput.setForme(inputDto.getForme());

                exportInput.setN(inputDto.getN());
                exportInput.setP2_O5(inputDto.getP2o5());
                exportInput.setK2_O(inputDto.getK2o());
                exportInput.setBore(inputDto.getBore());
                exportInput.setCalcium(inputDto.getCalcium());
                exportInput.setFer(inputDto.getFer());
                exportInput.setManganese(inputDto.getManganese());
                exportInput.setMolybdene(inputDto.getMolybdene());
                exportInput.setMG_O(inputDto.getMgo());
                exportInput.setOxydeDeSodium(inputDto.getOxyde_de_sodium());
                exportInput.setS_O3(inputDto.getSo3());
                exportInput.setCuivre(inputDto.getCuivre());
                exportInput.setZinc(inputDto.getZinc());

                // Since #10450 : add new calculated columns
                // Conversion fixed in #10626
                // L'information à mettre dans ces colonnes n'est pas déjà calculée par Agrosyst. Elle se calcule de la façon suivante
                //  - Si unité d'apport = Kg/ha ou L/ha, alors u_N = ؉N * dose moyenne / 100 * PSCI
                //  - Si unité d'apport = T/ha, alors u_N = ؉N * dose moyenne / 10 * PSCI

                double ratio = 10.0d;
                if (Set.of(MineralProductUnit.KG_HA, MineralProductUnit.L_HA).contains(inputDto.getUsageUnit())) {
                    ratio = 1d / 100d;
                }

                if (qtAvg != null) {
                    double commonValue = ratio * qtAvg * psci;
                    exportInput.setUN(inputDto.getN() * commonValue);
                    exportInput.setUP2_O5(inputDto.getP2o5() * commonValue);
                    exportInput.setUK2_O(inputDto.getK2o() * commonValue);
                    exportInput.setUBore(inputDto.getBore() * commonValue);
                    exportInput.setUCalcium(inputDto.getCalcium() * commonValue);
                    exportInput.setUFer(inputDto.getFer() * commonValue);
                    exportInput.setUManganese(inputDto.getManganese() * commonValue);
                    exportInput.setUMolybdene(inputDto.getMolybdene() * commonValue);
                    exportInput.setUMG_O(inputDto.getMgo() * commonValue);
                    exportInput.setUOxydeDeSodium(inputDto.getOxyde_de_sodium() * commonValue);
                    exportInput.setUS_O3(inputDto.getSo3() * commonValue);
                    exportInput.setUCuivre(inputDto.getCuivre() * commonValue);
                    exportInput.setUZinc(inputDto.getZinc() * commonValue);
                }

                exportInput.setProductName(inputDto.getInputName());
                exportInput.setPhytoEffect(inputDto.isPhytoEffect());
                exportInput.setQtMin(usage.getQtMin());
                exportInput.setQtAvg(qtAvg);
                exportInput.setQtMed(usage.getQtMed());
                exportInput.setQtMax(usage.getQtMax());
                exportInput.setInputUnit(inputDto.getUsageUnit().toString());

            }
            result.add(exportInput);
        }

        // other input
        fillWithOtherProductInputUsageDtos(action, supplier, result);

        // default
        if (result.isEmpty()) {
            ExportMetadata.ItkActionApplicationProduitsMinerauxBean defaultInput = supplier.get();
            result.add(defaultInput);
        }

        return result;
    }

    protected List<ExportMetadata.ItkApplicationProduitsPhytoBean> exportPesticidesSpreadingAction(ExportMetadata.Bean baseActionBean,
                                                                                                   PesticidesSpreadingActionDto action,
                                                                                                   Map<String, ExportMetadata.SpeciesAndVariety> refEspeceByCPSCode) {

        List<ExportMetadata.ItkApplicationProduitsPhytoBean> result = new LinkedList<>();

        // input
        Collection<PhytoProductInputUsageDto> usages = action.getPhytoProductInputUsageDtos().orElse(Lists.newArrayList());

        Set<String> refEspeceIds = refEspeceByCPSCode.values()
                .stream()
                .map(ExportMetadata.SpeciesAndVariety::species)
                .map(RefEspece::getTopiaId)
                .collect(Collectors.toSet());

        Supplier<ExportMetadata.ItkApplicationProduitsPhytoBean> supplier = () -> {
            ExportMetadata.ItkApplicationProduitsPhytoBean newBean = new ExportMetadata.ItkApplicationProduitsPhytoBean(baseActionBean);
            newBean.setProportionOfTreatedSurface(action.getProportionOfTreatedSurface());
            newBean.setBoiledQuantity(action.getBoiledQuantity());
            newBean.setBoiledQuantityPerTrip(action.getBoiledQuantityPerTrip());
            newBean.setAntiDriftNozzle(action.isAntiDriftNozzle());
            newBean.setTripFrequency(action.getTripFrequency());
            return newBean;
        };

        Map<String, String> groupesCiblesParCode = referentialService.getGroupesCiblesParCode();

        for (PhytoProductInputUsageDto usage : usages) {

            // action
            ExportMetadata.ItkApplicationProduitsPhytoBean exportInput = supplier.get();

            exportInput.setInputType(usage.getInputType());

            List<PhytoProductTargetDto> targets = usage.getTargets().orElse(Lists.newArrayList());

            String groupesCiblesLabels = targets.stream()
                    .map(PhytoProductTargetDto::getCodeGroupeCibleMaa)
                    .filter(Objects::nonNull)
                    .map(groupesCiblesParCode::get)
                    .collect(Collectors.joining(", "));
            exportInput.setGroupesCibles(groupesCiblesLabels);

            String targetsString = targets.stream()
                    .map(PhytoProductTargetDto::getRefBioAgressorTargetId)
                    .filter(Objects::nonNull)
                    .flatMap(targetId -> refBioAgressorTopiaDao.forTopiaIdEquals(targetId).stream())
                    .filter(Objects::nonNull)
                    .map(RefBioAgressor::getLabel)
                    .collect(Collectors.joining(", "));
            exportInput.setTargets(targetsString);

            Collection<String> targetIdOrReferenceCodes = targets.stream()
                    .map(PhytoProductTargetDto::getIdentifiantOrReference_code)
                    .collect(Collectors.toSet());

            Collection<String> groupesCibles = targets.stream()
                    .map(PhytoProductTargetDto::getCodeGroupeCibleMaa)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());

            DomainPhytoProductInputDto inputDto = usage.getDomainPhytoProductInputDto();
            if (inputDto != null) {
                exportInput.setProductType(inputDto.getProductType());
                exportInput.setProductName(inputDto.getInputName());
                if (CollectionUtils.isNotEmpty(refEspeceIds)) {
                    String refInputId = inputDto.getRefInputId();

                    Optional<ReferenceDoseDTO> referenceDoseIFTancienne =
                            findDoseWithValueUseCacheIFTancienne(refInputId, refEspeceIds);
                    referenceDoseIFTancienne.ifPresent(dose -> {
                        exportInput.setReferenceDoseIFTancienne(dose.getValue());
                        exportInput.setReferenceDoseUnitIFTancienne(dose.getUnit());
                    });

                    Optional<ReferenceDoseDTO> referenceDoseIFTcibleNonMillesime =
                            findDoseWithValueUseCacheIFTcibleNonMillesime(refInputId, refEspeceIds, targetIdOrReferenceCodes, groupesCibles);
                    referenceDoseIFTcibleNonMillesime.ifPresent(dose -> {
                        exportInput.setReferenceDoseIFTcibleNonMillesime(dose.getValue());
                        exportInput.setReferenceDoseUnitIFTcibleNonMillesime(dose.getUnit());
                    });
                }

                exportInput.setQtMin(usage.getQtMin());
                exportInput.setQtAvg(usage.getQtAvg());
                exportInput.setQtMed(usage.getQtMed());
                exportInput.setQtMax(usage.getQtMax());
                exportInput.setInputUnit(inputDto.getUsageUnit().toString());
            }

            result.add(exportInput);
        }

        // other input
        fillWithOtherProductInputUsageDtos(action, supplier, result);

        // default
        if (result.isEmpty()) {
            ExportMetadata.ItkApplicationProduitsPhytoBean exportInput = supplier.get();
            result.add(exportInput);
        }

        return result;
    }

    protected Optional<ReferenceDoseDTO> findDoseWithValueUseCacheIFTancienne(String refInputId, Set<String> refEspeceIds) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(refEspeceIds));
        String reEspecesIdsSorted = refEspeceIds.stream().sorted().collect(Collectors.joining());
        String key = String.join("||", refInputId, reEspecesIdsSorted);
        com.google.common.base.Optional<ReferenceDoseDTO> optional = cacheService.get(CacheDiscriminator.computeReferenceDoseForIFTLegacy(),
                key, () -> com.google.common.base.Optional.fromJavaUtil(
                        findDoseWithValueIFTancienne(refInputId, refEspeceIds)
                )
        );
        return optional.toJavaUtil();
    }

    protected Optional<ReferenceDoseDTO> findDoseWithValueIFTancienne(String refInputId, Set<String> refEspeceIds) {
        ReferenceDoseDTO doseDTO = referentialService.computeReferenceDoseForIFT(refInputId, refEspeceIds);
        if (doseDTO == null || doseDTO.getValue() == null) {
            return Optional.empty();
        }
        return Optional.of(doseDTO);
    }

    protected Optional<ReferenceDoseDTO> findDoseWithValueIFTcibleNonMillesime(String refInputId,
                                                                               Collection<String> refEspeceIds,
                                                                               Collection<String> targetIds,
                                                                               Collection<String> groupesCibles) {

        ReferenceDoseDTO doseDTO = referentialService.computeReferenceDoseForIFTCibleNonMillesime(
                refInputId, refEspeceIds, targetIds, groupesCibles);

        if (doseDTO == null || doseDTO.getValue() == null) {
            return Optional.empty();
        }
        return Optional.of(doseDTO);
    }

    protected Optional<ReferenceDoseDTO> findDoseWithValueUseCacheIFTcibleNonMillesime(String refInputId,
                                                                                       Collection<String> refEspeceIds,
                                                                                       Collection<String> targetIds,
                                                                                       Collection<String> groupesCibles) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(refEspeceIds));
        String refEspecesIdsSorted = refEspeceIds.stream().sorted().collect(Collectors.joining());
        String targetIdsSorted = targetIds != null ? targetIds.stream().sorted().collect(Collectors.joining()) : "";
        String key = String.join("||", refInputId, refEspecesIdsSorted, targetIdsSorted);
        com.google.common.base.Optional<ReferenceDoseDTO> optional = cacheService.get(
                CacheDiscriminator.computeReferenceDoseForIFTcibleNonMillesime(),
                key,
                () -> com.google.common.base.Optional.fromJavaUtil(
                        findDoseWithValueIFTcibleNonMillesime(refInputId, refEspeceIds, targetIds, groupesCibles)
                )
        );
        return optional.toJavaUtil();
    }

    protected List<ExportMetadata.ItkActionAutreBean> exportOtherAction(ExportMetadata.Bean baseActionBean,
                                                          OtherActionDto action) {

        List<ExportMetadata.ItkActionAutreBean> result = new LinkedList<>();

        Collection<SubstrateInputUsageDto> usages = action.getSubstrateInputUsageDtos().orElse(Lists.newArrayList());
        for (SubstrateInputUsageDto usage : usages) {
            ExportMetadata.ItkActionAutreBean exportInput = new ExportMetadata.ItkActionAutreBean(baseActionBean);
            exportInput.setInputType(usage.getInputType());
            DomainSubstrateInputDto substrateInputDto = usage.getDomainSubstrateInputDto();
            exportInput.setProductName(substrateInputDto.getCharacteristic1() + " - " + substrateInputDto.getCharacteristic2());
            exportInput.setQtMin(usage.getQtMin());
            exportInput.setQtAvg(usage.getQtAvg());
            exportInput.setQtMed(usage.getQtMed());
            exportInput.setQtMax(usage.getQtMax());
            exportInput.setInputUnit(substrateInputDto.getUsageUnit().toString());
            result.add(exportInput);
        }

        Collection<PotInputUsageDto> potUsages = action.getPotInputUsageDtos().orElse(org.apache.commons.compress.utils.Lists.newArrayList());
        for (PotInputUsageDto usage : potUsages) {
            ExportMetadata.ItkActionAutreBean exportInput = new ExportMetadata.ItkActionAutreBean(baseActionBean);
            exportInput.setInputType(usage.getInputType());
            exportInput.setProductName(usage.getDomainPotInputDto().getCaracteristic1());
            exportInput.setQtMin(usage.getQtMin());
            exportInput.setQtAvg(usage.getQtAvg());
            exportInput.setQtMed(usage.getQtMed());
            exportInput.setQtMax(usage.getQtMax());
            exportInput.setInputUnit(usage.getDomainPotInputDto().getUsageUnit().toString());
            result.add(exportInput);
        }

        fillWithOtherProductInputUsageDtos(action, () -> new ExportMetadata.ItkActionAutreBean(baseActionBean), result);

        return result;
    }

    protected List<ExportMetadata.ItkActionEntretienTailleVigneBean> exportMaintenancePruningVinesAction(ExportMetadata.Bean baseActionBean,
                                                                                                         MaintenancePruningVinesActionDto action) {

        List<ExportMetadata.ItkActionEntretienTailleVigneBean> result = new LinkedList<>();

        // input
        fillWithOtherProductInputUsageDtos(action, () -> new ExportMetadata.ItkActionEntretienTailleVigneBean(baseActionBean), result);
        if (result.isEmpty()) {
            ExportMetadata.ItkActionEntretienTailleVigneBean exportAction = new ExportMetadata.ItkActionEntretienTailleVigneBean(baseActionBean);
            result.add(exportAction);
        }

        return result;
    }

    protected List<ExportMetadata.ItkActionEpandageOrganiqueBean> exportOrganicFertilizersSpreadingAction(ExportMetadata.Bean export,
                                                                                                          InterventionDto intervention,
                                                                                                          OrganicFertilizersSpreadingActionDto action) {

        List<ExportMetadata.ItkActionEpandageOrganiqueBean> result = new LinkedList<>();

        // action
        Supplier<ExportMetadata.ItkActionEpandageOrganiqueBean> actionSupplier = () -> {
            ExportMetadata.ItkActionEpandageOrganiqueBean newBean = new ExportMetadata.ItkActionEpandageOrganiqueBean(export);
            newBean.setLandfilledWaste(action.isLandfilledWaste());
            return newBean;
        };

        // input
        Collection<OrganicProductInputUsageDto> usages = action.getOrganicProductInputUsageDtos().orElse(Lists.newArrayList());
        for (OrganicProductInputUsageDto usage : usages) {
            ExportMetadata.ItkActionEpandageOrganiqueBean exportInput = actionSupplier.get();

            exportInput.setInputType(usage.getInputType());
            DomainOrganicProductInputDto organicProduct = usage.getDomainOrganicProductInputDto();
            if (organicProduct != null) {
                RefFertiOrga refFertiOrga = refFertiOrgaTopiaDao.forTopiaIdEquals(organicProduct.getRefInputId()).findAnyOrNull();
                if (refFertiOrga != null) {
                    exportInput.setTypeDeProduitFertilisant(refFertiOrga.getLibelle());
                }
                exportInput.setProductName(organicProduct.getInputName());

                exportInput.setN(organicProduct.getN());
                exportInput.setP2O5(organicProduct.getP2O5());
                exportInput.setK2O(organicProduct.getK2O());
                exportInput.setCaO(organicProduct.getCaO());
                exportInput.setMgO(organicProduct.getMgO());
                exportInput.setS(organicProduct.getS());
                exportInput.setQtMin(usage.getQtMin());
                exportInput.setQtAvg(usage.getQtAvg());
                exportInput.setQtMed(usage.getQtMed());
                exportInput.setQtMax(usage.getQtMax());
                exportInput.setInputUnit(organicProduct.getUsageUnit().toString());

                // Since #10450 : add new calculated columns
                // L'information à mettre dans ces colonnes n'est pas déjà calculée par Agrosyst. Elle se calcule de la façon suivante
                //  - Si unité d'apport = T/ha ou M3/ha, alors u_N = ؉N * dose moyenne * PSCI
                //  - Si unité d'apport = kg/ha, alors u_N = 0.001 * ؉N * dose moyenne * PSCI

                double psci = intervention.getTemporalFrequencyOrTransitCount() * intervention.getSpatialFrequency();

                double ratio = 1.0d;
                if (OrganicProductUnit.KG_HA.equals(organicProduct.getUsageUnit())) {
                    ratio = 1d / 1000d;
                }
                Double qtAvg = usage.getQtAvg();
                if (qtAvg != null) {
                    double commonValue = ratio * qtAvg * psci;
                    exportInput.setUN(organicProduct.getN() * commonValue);
                    exportInput.setUP2O5(organicProduct.getP2O5() * commonValue);
                    exportInput.setUK2O(organicProduct.getK2O() * commonValue);
                    if(organicProduct.getCaO() != null) exportInput.setUCaO(organicProduct.getCaO() * commonValue);
                    if (organicProduct.getMgO() != null) exportInput.setUMgO(organicProduct.getMgO() * commonValue);
                    if (organicProduct.getS() != null) exportInput.setUS(organicProduct.getS() * commonValue);
                }
            }

            result.add(exportInput);
        }

        // other input
        fillWithOtherProductInputUsageDtos(action, actionSupplier, result);

        // default
        if (result.isEmpty()) {
            ExportMetadata.ItkActionEpandageOrganiqueBean exportAction = actionSupplier.get();
            result.add(exportAction);
        }

        return result;
    }

    protected List<ExportMetadata.ItkActionIrrigationBean> exportIrrigationAction(ExportMetadata.Bean baseActionBean,
                                                                                  IrrigationActionDto action) {

        Supplier<ExportMetadata.ItkActionIrrigationBean> supplier = () -> {
            ExportMetadata.ItkActionIrrigationBean newBean = new ExportMetadata.ItkActionIrrigationBean(baseActionBean);
            newBean.setWaterQuantityMin(action.getWaterQuantityMin());
            newBean.setWaterQuantityAverage(action.getWaterQuantityAverage());
            newBean.setWaterQuantityMedian(action.getWaterQuantityMedian());
            newBean.setWaterQuantityMax(action.getWaterQuantityMax());
            newBean.setAzoteQuantity(action.getAzoteQuantity());
            return newBean;
        };

        List<ExportMetadata.ItkActionIrrigationBean> result = new LinkedList<>();

        // input
        fillWithOtherProductInputUsageDtos(action, supplier, result);
        if (result.isEmpty()) {
            ExportMetadata.ItkActionIrrigationBean exportAction = supplier.get();
            result.add(exportAction);
        }

        return result;
    }

    protected List<ExportMetadata.ItkActionLutteBiologiqueBean> exportBiologicalControlAction(ExportMetadata.Bean export,
                                                                                              BiologicalControlActionDto action,
                                                                                              Map<String, ExportMetadata.SpeciesAndVariety> refEspeceByCPSCode) {
        List<ExportMetadata.ItkActionLutteBiologiqueBean> result = new LinkedList<>();

        Supplier<ExportMetadata.ItkActionLutteBiologiqueBean> actionSupplier = () -> {
            ExportMetadata.ItkActionLutteBiologiqueBean newBean = new ExportMetadata.ItkActionLutteBiologiqueBean(export);
            newBean.setProportionOfTreatedSurface(action.getProportionOfTreatedSurface());
            newBean.setBoiledQuantity(action.getBoiledQuantity());
            newBean.setBoiledQuantityPerTrip(action.getBoiledQuantityPerTrip());
            newBean.setTripFrequency(action.getTripFrequency());
            return newBean;
        };

        // input
        Collection<PhytoProductInputUsageDto> usages = action.getPhytoProductInputUsageDtos().orElse(Lists.newArrayList());
        Set<String> refEspeceIds = refEspeceByCPSCode.values().stream()
                .map(ExportMetadata.SpeciesAndVariety::species)
                .map(RefEspece::getTopiaId)
                .collect(Collectors.toSet());

        Map<String, String> groupesCiblesParCode = referentialService.getGroupesCiblesParCode();

        for (PhytoProductInputUsageDto usageDto : usages) {
            ExportMetadata.ItkActionLutteBiologiqueBean exportInput = actionSupplier.get();
            exportInput.setInputType(usageDto.getInputType());

            List<PhytoProductTargetDto> targets = usageDto.getTargets().orElse(Lists.newArrayList());
            String groupesCiblesLabels = targets.stream()
                    .map(PhytoProductTargetDto::getCodeGroupeCibleMaa)
                    .filter(Objects::nonNull)
                    .map(groupesCiblesParCode::get)
                    .collect(Collectors.joining(", "));
            exportInput.setGroupesCibles(groupesCiblesLabels);

            String targetsString = targets.stream()
                    .map(PhytoProductTargetDto::getRefBioAggressorLabel)
                    .collect(Collectors.joining(", "));
            exportInput.setTargets(targetsString);

            Collection<String> targetIdOrReferenceCodes = targets.stream()
                    .map(PhytoProductTargetDto::getIdentifiantOrReference_code)
                    .collect(Collectors.toSet());

            Collection<String> groupesCibles = targets.stream()
                    .map(PhytoProductTargetDto::getCodeGroupeCibleMaa)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());

            RefActaTraitementsProduit phytoProduct = refActaTraitementsProduitTopiaDao.forTopiaIdEquals(usageDto.getDomainPhytoProductInputDto().getRefInputId()).findAnyOrNull();
            if (phytoProduct != null) {
                exportInput.setPhytoProduct(phytoProduct.getNom_traitement());
                if (CollectionUtils.isNotEmpty(refEspeceIds)) {

                    Optional<ReferenceDoseDTO> referenceDoseIFTancienne =
                            findDoseWithValueUseCacheIFTancienne(phytoProduct.getTopiaId(), refEspeceIds);
                    referenceDoseIFTancienne.ifPresent(dose -> {
                        exportInput.setReferenceDoseIFTancienne(dose.getValue());
                        exportInput.setReferenceDoseUnitIFTancienne(dose.getUnit());
                    });

                    Optional<ReferenceDoseDTO> referenceDoseIFTcibleNonMillesime =
                            findDoseWithValueUseCacheIFTcibleNonMillesime(phytoProduct.getTopiaId(), refEspeceIds, targetIdOrReferenceCodes, groupesCibles);
                    referenceDoseIFTcibleNonMillesime.ifPresent(dose -> {
                        exportInput.setReferenceDoseIFTcibleNonMillesime(dose.getValue());
                        exportInput.setReferenceDoseUnitIFTcibleNonMillesime(dose.getUnit());
                    });
                }
            }
            String productName = usageDto.getDomainPhytoProductInputDto().getInputName();
            if (StringUtils.isBlank(productName) && phytoProduct != null) {
                productName = phytoProduct.getNom_produit();
            }
            exportInput.setProductName(productName);

            exportInput.setQtMin(usageDto.getQtMin());
            exportInput.setQtAvg(usageDto.getQtAvg());
            exportInput.setQtMed(usageDto.getQtMed());
            exportInput.setQtMax(usageDto.getQtMax());
            exportInput.setInputUnit(usageDto.getDomainPhytoProductInputDto().getUsageUnit().toString());

            result.add(exportInput);
        }

        // other
        fillWithOtherProductInputUsageDtos(action, actionSupplier, result);

        // default
        if (result.isEmpty()) {
            ExportMetadata.ItkActionLutteBiologiqueBean exportAction = actionSupplier.get();
            result.add(exportAction);
        }

        return result;
    }

    protected List<ExportMetadata.ItkActionRecolteBean> exportHarvestingAction(ExportMetadata.Bean baseActionBean,
                                                                               List<SpeciesStadeDto> speciesStadeDtos,
                                                                               Supplier<Map<String, ExportMetadata.SpeciesAndVariety>> refEspeceByCPSCodeSupplier,
                                                                               HarvestingActionDto action,
                                                                               String domainIdentifier,
                                                                               boolean effective) {

        Map<String, ExportMetadata.SpeciesAndVariety> refEspeceByCPSCode = refEspeceByCPSCodeSupplier.get();

        List<ExportMetadata.ItkActionRecolteBean> result = new LinkedList<>();

        Optional<String> cattleCode = action.getCattleCode();
        String cattleName;
        if (cattleCode.isPresent()) {
            Language language = getSecurityContext().getLanguage();
            String definitiveCattleCode = cattleCode.get();

            LivestockUnit livestockUnit;
            if (effective) {
                livestockUnit = livestockUnitDao.findLivestockUnitForCattleCodeAndDomainId(definitiveCattleCode, domainIdentifier, language);
            } else {
                livestockUnit = livestockUnitDao.findLivestockUnitForCattleCodeAndDomainCode(definitiveCattleCode, domainIdentifier, language);
            }

            Cattle cattle = livestockUnit.getCattles().stream()
                    .filter(c -> definitiveCattleCode.equals(c.getCode()))
                    .findFirst()
                    .orElseThrow();

            cattleName =  livestockUnit.getRefAnimalType().getAnimalType() + " / " + cattle.getAnimalType().getAnimalType();

        } else {
            cattleName = null;
        }

        Integer pastureLoad = action.getPastureLoad();
        PastureType pastureType = action.getPastureType().orElse(null);
        boolean pasturingAtNight = action.isPasturingAtNight();

        // valorisation
        List<HarvestingActionValorisationDto> valorisations = action.getValorisationDtos();

        for (final HarvestingActionValorisationDto valorisation : valorisations) {

            Supplier<ExportMetadata.ItkActionRecolteBean> supplier = () -> {
                ExportMetadata.ItkActionRecolteBean newBean = new ExportMetadata.ItkActionRecolteBean(baseActionBean);
                newBean.setOrganicCrop(valorisation.isOrganicCrop());
                newBean.setYieldMin(valorisation.getYealdMin());
                newBean.setYieldAverage(valorisation.getYealdAverage());
                newBean.setYieldMedian(valorisation.getYealdMedian());
                newBean.setYieldMax(valorisation.getYealdMax());
                newBean.setYieldUnit(valorisation.getYealdUnit());
                newBean.setSalesPercent(valorisation.getSalesPercent());
                newBean.setSelfConsumedPercent(valorisation.getSelfConsumedPersent());
                newBean.setNoValorisationPercent(valorisation.getNoValorisationPercent());
                newBean.setDestination(valorisation.getDestinationName());
                newBean.setCattleName(cattleName);
                newBean.setPastureLoad(pastureLoad);
                newBean.setPastureType(pastureType);
                newBean.setPasturingAtNight(pasturingAtNight);

                fillHarvestingActionSpeciesAndVarieties(newBean, speciesStadeDtos, refEspeceByCPSCode, valorisation);

                return newBean;
            };

            Collection<QualityCriteriaDto> qualityCriteria = valorisation.getQualityCriteriaDtos().orElse(Lists.newArrayList());
            if (CollectionUtils.isNotEmpty(qualityCriteria)) {
                for (QualityCriteriaDto qualityCriterion : qualityCriteria) {

                    ExportMetadata.ItkActionRecolteBean exportCriteria = supplier.get();

                    RefQualityCriteria criteria = refQualityCriteriaTopiaDao.forTopiaIdEquals(qualityCriterion.getRefQualityCriteriaId()).findAnyOrNull();
                    if (criteria != null) {
                        exportCriteria.setQualityCriteriaLabel(criteria.getQualityCriteriaLabel());
                        if (criteria.getQualityAttributeType() == QualityAttributeType.BINAIRE) {
                            exportCriteria.setQuantitativeValue(qualityCriterion.getBinaryValue());
                        } else {
                            exportCriteria.setQuantitativeValue(qualityCriterion.getQuantitativeValue());
                        }
                    }

                    result.add(exportCriteria);
                }
            } else {
                ExportMetadata.ItkActionRecolteBean exportValorisation = supplier.get();
                result.add(exportValorisation);
            }
        }

        String actionSpecies = result.stream()
                .map(ExportMetadata.ItkActionRecolteBean::getActionSpecies)
                .distinct()
                .collect(Collectors.joining(", "));
        String actionVarieties = result.stream()
                .map(ExportMetadata.ItkActionRecolteBean::getActionVarieties)
                .distinct()
                .collect(Collectors.joining(", "));

        // input
        Supplier<ExportMetadata.ItkActionRecolteBean> otherSupplier = () -> {
            ExportMetadata.ItkActionRecolteBean exportInput = new ExportMetadata.ItkActionRecolteBean(baseActionBean);
            exportInput.setActionSpecies(actionSpecies);
            exportInput.setActionVarieties(actionVarieties);
            return exportInput;
        };

        fillWithOtherProductInputUsageDtos(action, otherSupplier, result);

        return result;
    }

    protected abstract void fillHarvestingActionSpeciesAndVarieties(ExportMetadata.ItkActionRecolteBean newBean, List<SpeciesStadeDto> speciesStadeDtos, Map<String, ExportMetadata.SpeciesAndVariety> refEspeceByCPSCode, HarvestingActionValorisationDto valorisation);

    protected <U extends ExportMetadata.Bean> void fillWithOtherProductInputUsageDtos(ActionWithOtherProductInputUsagesDto action, Supplier<U> actionSupplier, List<U> result) {
        Collection<OtherProductInputUsageDto> otherUsages = action.getOtherProductInputUsageDtos().orElse(org.apache.commons.compress.utils.Lists.newArrayList());
        for (OtherProductInputUsageDto usageDto : otherUsages) {
            U exportInput = actionSupplier.get();
            exportInput.setInputType(usageDto.getInputType());
            exportInput.setProductName(usageDto.getDomainOtherProductInputDto().getInputName());

            exportInput.setQtMin(usageDto.getQtMin());
            exportInput.setQtAvg(usageDto.getQtAvg());
            exportInput.setQtMed(usageDto.getQtMed());
            exportInput.setQtMax(usageDto.getQtMax());
            exportInput.setInputUnit(usageDto.getDomainOtherProductInputDto().getUsageUnit().toString());
            result.add(exportInput);
        }

    }

    protected <U extends ExportMetadata.Bean> void fillWithSubstrateInputUsageDtos(SeedingActionUsageDto action, Supplier<U> actionSupplier, List<U> result) {

        Collection<SubstrateInputUsageDto> usages = action.getSubstrateInputUsageDtos().orElse(Lists.newArrayList());
        for (SubstrateInputUsageDto usage : usages) {
            U exportInput = actionSupplier.get();
            exportInput.setInputType(usage.getInputType());
            DomainSubstrateInputDto substrateInputDto = usage.getDomainSubstrateInputDto();
            exportInput.setProductName(substrateInputDto.getCharacteristic1() + " - " + substrateInputDto.getCharacteristic2());
            exportInput.setQtMin(usage.getQtMin());
            exportInput.setQtAvg(usage.getQtAvg());
            exportInput.setQtMed(usage.getQtMed());
            exportInput.setQtMax(usage.getQtMax());
            exportInput.setInputUnit(substrateInputDto.getUsageUnit().toString());
            result.add(exportInput);
        }

    }

    protected ExportMetadata.ItkActionTransportBean exportCarriageAction(ExportMetadata.Bean baseActionBean, CarriageActionDto action) {
        ExportMetadata.ItkActionTransportBean exportAction = new ExportMetadata.ItkActionTransportBean(baseActionBean);
        exportAction.setLoadCapacity(action.getLoadCapacity());
        exportAction.setCapacityUnit(action.getCapacityUnit());
        exportAction.setTripFrequency(action.getTripFrequency());
        return exportAction;
    }

    protected ExportMetadata.ItkActionTravailSolBean exportTillageAction(ExportMetadata.Bean baseActionBean, TillageActionDto action) {
        ExportMetadata.ItkActionTravailSolBean exportAction = new ExportMetadata.ItkActionTravailSolBean(baseActionBean);
        exportAction.setTillageDepth(action.getTillageDepth());
        exportAction.setOtherSettingTool(action.getOtherSettingTool());
        return exportAction;
    }

}
