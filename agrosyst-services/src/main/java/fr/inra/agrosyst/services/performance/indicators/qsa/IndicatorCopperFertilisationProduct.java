package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.indicators.fertilization.AbstractIndicatorFertilization;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.nuiton.i18n.I18n.l;

/**
 * Classe calculant l'indicateur QSA Cuivre Fertilisation.
 * <p>
 * Cet indicateur est identique à l’indicateur de fertilisation « Cu minéral ».
 * <p>
 * QSA Cuivre fertilisation = PSCI * Qf * Ccu
 * Avec :
 *   Qf = quantité de fertilisant minéral appliquée
 *   Ccu = concentration en cuivre du fertilisant.
 */
public class IndicatorCopperFertilisationProduct extends AbstractIndicatorFertilization {

    //  il n'y a pas de version d'indicateur hors traitement de semence puisqu'un traitement de semence ne peut pas être un fertilisant
    private static final String COLUMN_NAME = "qsa_cuivre_ferti";

    public IndicatorCopperFertilisationProduct() {
        // labels
        super(new String[]{
                "Indicator.label.copperFertiProduct", // QSA Cuivre fertilisation
        });
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {
        if (interventionContext.isFictive()) {
            return null;
        }

        final Optional<MineralFertilizersSpreadingAction> optionalMineralFertilizersSpreadingAction = interventionContext.getOptionalMineralFertilizersSpreadingAction();

        Optional<Double[]> optionalResult = this.computeQuantity(writerContext, optionalMineralFertilizersSpreadingAction, interventionContext);
        return optionalResult.orElse(null);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {
        final Optional<MineralFertilizersSpreadingAction> optionalMineralFertilizersSpreadingAction = interventionContext.getOptionalMineralFertilizersSpreadingAction();

        Optional<Double[]> optionalResult = this.computeQuantity(writerContext, optionalMineralFertilizersSpreadingAction, interventionContext);
        return optionalResult.orElse(null);
    }

    private Optional<Double[]> computeQuantity(WriterContext writerContext,
                                             Optional<MineralFertilizersSpreadingAction> optionalMineralFertilizersSpreadingAction,
                                             PerformanceInterventionContext interventionContext) {

        AtomicBoolean computeDoneFlag = new AtomicBoolean(false);

        Double[] quantities = newArray(this.labels.length, 0.0d);

        if (optionalMineralFertilizersSpreadingAction.isPresent()) {
            MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction = optionalMineralFertilizersSpreadingAction.get();
            for (MineralProductInputUsage mineralProductInputUsage : mineralFertilizersSpreadingAction.getMineralProductInputUsages()) {
                final double cuQuantity = interventionContext.getCuQuantityByInputUsage().getOrDefault(mineralProductInputUsage.getTopiaId(), 0.0d);

                this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, cuQuantity, 0);

                quantities[0] += cuQuantity;
                interventionContext.addUsageFertiCuQuantity(mineralFertilizersSpreadingAction, mineralProductInputUsage, cuQuantity);

                computeDoneFlag.set(true);
            }
        }

        if (computeDoneFlag.get()) {
            interventionContext.setFertiCuQuantity(quantities[0]);
            return Optional.of(quantities);
        }
        return Optional.empty();
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, "Indicator.category.activeSubstances");
    }


    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return isRelevant(atLevel) && displayed;
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
    }
}
