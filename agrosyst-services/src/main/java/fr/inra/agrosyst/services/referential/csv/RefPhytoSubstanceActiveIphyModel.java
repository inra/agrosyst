package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefPhytoSubstanceActiveIphy;
import fr.inra.agrosyst.api.entities.referential.RefPhytoSubstanceActiveIphyImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Référentiel Acta substance active.
 * 
 * Nom SA;Coefficient d'adsoption KOC;Demi-vie (DT50) (jour);Constante Henry (KH) (sans unité);DJA (mg/kg/j);Aquatox (mg/L);Presion vapeur (Pa);Solubilité (mg/L);Source
 *
 */
public class RefPhytoSubstanceActiveIphyModel extends AbstractAgrosystModel<RefPhytoSubstanceActiveIphy> implements ExportModel<RefPhytoSubstanceActiveIphy> {

    public RefPhytoSubstanceActiveIphyModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Nom SA", RefPhytoSubstanceActiveIphy.PROPERTY_NOM_SA);
        newMandatoryColumn("Coefficient d'adsoption KOC", RefPhytoSubstanceActiveIphy.PROPERTY_KOC, DOUBLE_PARSER);
        newMandatoryColumn("Demi-vie (DT50) (jour)", RefPhytoSubstanceActiveIphy.PROPERTY_DT50, DOUBLE_PARSER);
        newMandatoryColumn("Constante Henry (KH) (sans unité)", RefPhytoSubstanceActiveIphy.PROPERTY_KH, DOUBLE_PARSER);
        newMandatoryColumn("DJA (mg/kg/j)", RefPhytoSubstanceActiveIphy.PROPERTY_DJA, DOUBLE_PARSER);
        newMandatoryColumn("Aquatox (mg/L)", RefPhytoSubstanceActiveIphy.PROPERTY_AQUATOX, DOUBLE_PARSER);
        newMandatoryColumn("Presion vapeur (Pa)", RefPhytoSubstanceActiveIphy.PROPERTY_PRESSION_VAPEUR, DOUBLE_PARSER);
        newMandatoryColumn("Solubilité (mg/L)", RefPhytoSubstanceActiveIphy.PROPERTY_SOLUBILITE, DOUBLE_PARSER);
        newMandatoryColumn("Source", RefPhytoSubstanceActiveIphy.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefPhytoSubstanceActiveIphy.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefPhytoSubstanceActiveIphy, Object>> getColumnsForExport() {
        ModelBuilder<RefPhytoSubstanceActiveIphy> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Nom SA", RefPhytoSubstanceActiveIphy.PROPERTY_NOM_SA);
        modelBuilder.newColumnForExport("Coefficient d'adsoption KOC", RefPhytoSubstanceActiveIphy.PROPERTY_KOC, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Demi-vie (DT50) (jour)", RefPhytoSubstanceActiveIphy.PROPERTY_DT50, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Constante Henry (KH) (sans unité)", RefPhytoSubstanceActiveIphy.PROPERTY_KH, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("DJA (mg/kg/j)", RefPhytoSubstanceActiveIphy.PROPERTY_DJA, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Aquatox (mg/L)", RefPhytoSubstanceActiveIphy.PROPERTY_AQUATOX, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Presion vapeur (Pa)", RefPhytoSubstanceActiveIphy.PROPERTY_PRESSION_VAPEUR, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Solubilité (mg/L)", RefPhytoSubstanceActiveIphy.PROPERTY_SOLUBILITE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefPhytoSubstanceActiveIphy.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefPhytoSubstanceActiveIphy.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefPhytoSubstanceActiveIphy newEmptyInstance() {
        RefPhytoSubstanceActiveIphy refPhytoSubstanceActiveIphy = new RefPhytoSubstanceActiveIphyImpl();
        refPhytoSubstanceActiveIphy.setActive(true);
        return refPhytoSubstanceActiveIphy;
    }
}
