package fr.inra.agrosyst.services.performance.performancehelper;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielTransportUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SubstrateInputUsage;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.performance.MissingFieldMessage;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class MissingFieldMessageBuilder {

    @Setter
    protected Locale locale;

    public MissingFieldMessageBuilder() {
        this.locale = Locale.FRANCE;
    }

    public MissingFieldMessageBuilder(Locale locale) {
        this.locale = locale;
    }

    final Map<InputType, String> inputTypeNames = AgrosystI18nService.getEnumAsMapWithDefaultLocale(null, InputType.values());

    public MissingFieldMessage getMissingDeprecateDoseMessage(InputType inputType, MissingMessageScope missingMessageScope) {
        final String message = l(locale, "IndicatorWriter.missingField.deprecatedDose", inputTypeNames.get(inputType));
        return new MissingFieldMessage(message, missingMessageScope);
    }

    public MissingFieldMessage getMissingDeprecatedDoseUnitMessage(InputType inputType, MissingMessageScope missingMessageScope) {
        final String message = l(locale, "IndicatorWriter.missingField.deprecatedDoseUnit", inputTypeNames.get(inputType));
        return new MissingFieldMessage(message, missingMessageScope);
    }

    public MissingFieldMessage getMissingDoseMessage(InputType inputType, MissingMessageScope missingMessageScope) {
        final String message = l(locale, "IndicatorWriter.missingField.dose", inputTypeNames.get(inputType));
        return new MissingFieldMessage(message, missingMessageScope);
    }

    public MissingFieldMessage getMissingDoseUnitMessage(InputType inputType) {
        final String message = l(locale, "IndicatorWriter.missingField.doseUnit", inputTypeNames.get(inputType));
        return new MissingFieldMessage(message, MissingMessageScope.INPUT);
    }

    public MissingFieldMessage getMissingInputUnitPriceUnitConverterMessage(InputType inputType, Enum doseUnit, PriceUnit priceUnit) {
        final String message = l(locale, "IndicatorWriter.missingField.inputPriceUnitConverter", inputTypeNames.get(inputType), doseUnit == null ? "?" : doseUnit, priceUnit);
        return new MissingFieldMessage(message, MissingMessageScope.INPUT);
    }
    
    public MissingFieldMessage getMissingInputUnitTransitUnitConverterMessage(InputType inputType, Enum doseUnit, MaterielTransportUnit transitVolumeUnit) {
        final String message = l(locale, "IndicatorWriter.missingField.inputTransitUnitConverter",  inputTypeNames.get(inputType), doseUnit == null ? "?" : doseUnit, transitVolumeUnit);
        return new MissingFieldMessage(message, MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingRefAnnualWorkAmountMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.refAnnualWorkAmount"), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingRefStandardizedPriceMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.refStandardizedPrice"), MissingMessageScope.INPUT);
    }

    public MissingFieldMessage getNotAccurateRefStandardizedPriceMessage(String fallbackValue) {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.notAccurateRefStandardizedPrice", fallbackValue), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingRefScenarioPriceMessage() {
        return new MissingFieldMessage(l(locale,"IndicatorWriter.missingField.refScenariosPrice"), MissingMessageScope.INPUT);
    }
    
    public MissingFieldMessage getMissingRefScenarioPriceMessage(String scenarioCode) {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.refScenarioPrice", scenarioCode), MissingMessageScope.INPUT);
    }
    
    public MissingFieldMessage getMissingRefConversionRateMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.refConversionRate"), MissingMessageScope.INTERVENTION);
    }

    public MissingFieldMessage getMissingRefConversionRateForValuesMessage(String usageUnitName, String priceUnitName) {
        return new MissingFieldMessage(String.format(l(locale, "IndicatorWriter.missingField.refConversionRateForUnit"), priceUnitName, usageUnitName), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingRefMaterielPowerMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.refMaterielPower"), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingValorisationDestinationMessage() {
        final String message = l(locale, "IndicatorWriter.missingField.valorisationDestination");
        return new MissingFieldMessage(message, MissingMessageScope.INTERVENTION, MissingMessageScope.CROP,  MissingMessageScope.ZONE, MissingMessageScope.PLOT, MissingMessageScope.GROWING_SYSTEM, MissingMessageScope.PRACTICED_SYSTEM);
    }
    
    public MissingFieldMessage getMissingTractorMessage(String interventionName, String interventionId, String toolsCouplingName) {
        final String message = l(locale, "IndicatorWriter.missingField.tractor", interventionName, interventionId, toolsCouplingName);
        return new MissingFieldMessage(message, MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingBalHourWorkRateMessage() {
        return new MissingFieldMessage(l(locale, "Débit de chantier en (bal/h)"), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingWorkRateMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.workRate"), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getIrrigationMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.irrigation"), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getManualToolUsageMessage(Equipment equipment) {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.manualToolUsage", equipment.getName()), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingWorkingTimeMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.workTime"), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingSeedingPriceMessage(CroppingPlanEntry crop) {
        return getMissingSeedingPriceMessage(crop.getName());
    }

    public MissingFieldMessage getMissingSeedingPriceMessage(String cropName) {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.cropSeedingPrice", cropName), MissingMessageScope.INPUT);
    }
    
    public MissingFieldMessage getMissingSeedingPriceMessage(CroppingPlanSpecies croppingPlanSpecies) {
        return getMissingSeedingSpeciesPriceMessage(croppingPlanSpecies.getSpecies().getLibelle_espece_botanique_Translated());
    }
    
    public MissingFieldMessage getMissingSeedingSpeciesPriceMessage(String speciesName) {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.speciesSeedingPrice", speciesName), MissingMessageScope.INPUT);
    }
    
    public MissingFieldMessage getMissingHarvestingPriceMessage(CroppingPlanEntry crop) {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.harvestingPrice", crop.getName()), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingSurfaceMessage(CroppingPlanSpecies species) {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.surface",
                species.getSpecies().getLibelle_espece_botanique_Translated() +
                        (species.getVariety() != null ? "(" + species.getVariety().getLabel() + ")" : "")),
                MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingInputUsagePriceMessage(InputType inputType, AbstractInputUsage inputUsage, AbstractAction action) {
        final String details = getInputUsageName(inputUsage) + (action != null ? " " + action.getMainAction().getReference_label() : "");
        final String message = l(locale, "IndicatorWriter.missingField.inputUsagePrice", inputTypeNames.get(inputType), StringUtils.isEmpty(details) ? "-" : details);
        return new MissingFieldMessage(message, MissingMessageScope.INTERVENTION, MissingMessageScope.INPUT);
    }
    
    public MissingFieldMessage getMissingInputProductMessage(InputType inputType) {
        final String message = l(locale, "IndicatorWriter.missingField.inputProduct", inputTypeNames.get(inputType));
        return new MissingFieldMessage(message, MissingMessageScope.INPUT);
    }

    public MissingFieldMessage getMissingIrrigationPriceMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.irrigationPrice"), MissingMessageScope.INPUT);
    }

    public MissingFieldMessage getMissingSeedingActionSpeciesMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.seedingActionSpecies"), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingInterventionSpeciesMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.interventionSpecies"), MissingMessageScope.INPUT);
    }
    
    public MissingFieldMessage getMissingRefDosageMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.referenceDose"), MissingMessageScope.INPUT);
    }
    
    public MissingFieldMessage getWrongDosageMessage(InputType inputType) {
        final String message = l(locale, "IndicatorWriter.missingField.wrongDosage", inputTypeNames.get(inputType));
        return new MissingFieldMessage(message, MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingInvolvedMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.involved"), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingBoiledQuantityMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.boiledQuantity"), MissingMessageScope.INPUT);
    }
    
    public MissingFieldMessage getMissingTransitVolumeMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.transitVolume"), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getMissingNbBallsMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.nbBalls"), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getHarvestingYealdIncoherenceMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.incoherentHarvestingYeald"), MissingMessageScope.INTERVENTION);
    }
    
    public MissingFieldMessage getSeedingQuantityUnitsIncoherenceMessage(String cropName) {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.incoherentSeedingQuantity", cropName), MissingMessageScope.INPUT);
    }
    
    public MissingFieldMessage getSeedingUnitsPriceUnitFutureFeatureMessage(Enum doseUnit, PriceUnit priceUnit) {
        return new MissingFieldMessage(l(locale, "L’unité de la quantité de semence '%s' n’est pas compatible avec l’unité de prix '%s'; calcul réalisable dans la prochaine version des indicateurs", doseUnit == null ? "?" : doseUnit, priceUnit), MissingMessageScope.INPUT);
    }
    
    public MissingFieldMessage getDomainMissingSAUMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.sau"), MissingMessageScope.DOMAIN);
    }
    
    public MissingFieldMessage getDomainMissingAffectedAreaMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.affectedArea"), MissingMessageScope.DOMAIN);
    }
    
    public MissingFieldMessage getMissingInputPotMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.inputPot"), MissingMessageScope.INTERVENTION);
    }

    public MissingFieldMessage getMissingToolsCouplingWorkingTimeMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingIndicatorComputation.toolsCouplingWorkingTime"), MissingMessageScope.INDICATOR);
    }

    public MissingFieldMessage getMissingManualWorkTimeMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingIndicatorComputation.manualWorkTime"), MissingMessageScope.INDICATOR);
    }

    public MissingFieldMessage getMissingMechanizedWorkTimeMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingIndicatorComputation.mechanizedWorkTime"), MissingMessageScope.INDICATOR);
    }

    public MissingFieldMessage getMissingManualWorkforceExpensesMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingIndicatorComputation.manualWorkforceExpenses"), MissingMessageScope.INDICATOR);
    }

    public MissingFieldMessage getMissingMechanizedWorkforceExpensesMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingIndicatorComputation.mechanizedWorkforceExpenses"), MissingMessageScope.INDICATOR);
    }

    public MissingFieldMessage getMissingTotalWorkforceExpensesMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingIndicatorComputation.totalWorkforceExpenses"), MissingMessageScope.INDICATOR);
    }

    public MissingFieldMessage getMissingSemiNetMarginWithAutoconsumeMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingIndicatorComputation.semiNetMarginWithAutoconsume"), MissingMessageScope.INDICATOR);
    }

    public MissingFieldMessage getMissingSemiNetMarginWithoutAutoconsumeMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingIndicatorComputation.semiNetMarginWithoutAutoconsume"), MissingMessageScope.INDICATOR);
    }

    protected String getInputUsageName(AbstractInputUsage inputUsage) {
        String result = "";
        if (inputUsage != null) {
            switch (inputUsage.getInputType()) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                    DomainMineralProductInput domainMineralProductInput = ((MineralProductInputUsage) inputUsage).getDomainMineralProductInput();
                    final RefFertiMinUNIFA mineralProduct = domainMineralProductInput.getRefInput();
                    result = mineralProduct != null ? InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(mineralProduct) : domainMineralProductInput.getInputName();
                }
                case AUTRE -> {
                    List<String> caracteristics = new ArrayList<>();
                    final RefOtherInput refInput1 = ((OtherProductInputUsage)inputUsage).getDomainOtherInput().getRefInput();
                    caracteristics.add(refInput1.getInputType_c0());
                    caracteristics.add(refInput1.getCaracteristic1());
                    caracteristics.add(refInput1.getCaracteristic2());
                    caracteristics.add(refInput1.getCaracteristic3());
                    result = caracteristics.stream().filter(Objects::nonNull).collect(Collectors.joining(" - "));
                }
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, LUTTE_BIOLOGIQUE, TRAITEMENT_SEMENCE -> {
                    DomainPhytoProductInput domainPhytoProductInput = ((AbstractPhytoProductInputUsage) inputUsage).getDomainPhytoProductInput();
                    final RefActaTraitementsProduit phytoProduct = domainPhytoProductInput.getRefInput();
                    result = phytoProduct != null ? phytoProduct.getNom_produit() : domainPhytoProductInput.getInputName();
                }
                case EPANDAGES_ORGANIQUES -> {
                    DomainOrganicProductInput domainOrganicProductInput = ((OrganicProductInputUsage) inputUsage).getDomainOrganicProductInput();
                    final RefFertiOrga refFertiOrga = domainOrganicProductInput.getRefInput();
                    result = refFertiOrga != null ? refFertiOrga.getLibelle() : domainOrganicProductInput.getInputName();
                }
                case SUBSTRAT -> {
                    DomainSubstrateInput domainSubstrateInput = ((SubstrateInputUsage) inputUsage).getDomainSubstrateInput();
                    RefSubstrate substrate = domainSubstrateInput.getRefInput();
                    result = substrate != null ? substrate.getCaracteristic1() + " - " + substrate.getCaracteristic2() : domainSubstrateInput.getInputName();
                }
                case POT -> {
                    DomainPotInput domainPotInput = ((PotInputUsage) inputUsage).getDomainPotInput();
                    RefPot pot = domainPotInput.getRefInput();
                    result = pot != null ? pot.getCaracteristic1() + " (" + pot.getVolume() + ")" : domainPotInput.getInputName();
                }
                case SEMIS ->  {
                    if (inputUsage instanceof SeedLotInputUsage seedLotInputUsage) {
                        result = seedLotInputUsage.getDomainSeedLotInput().getInputName();
                    } else if(inputUsage instanceof SeedSpeciesInputUsage speciesInputUsage) {
                        result = speciesInputUsage.getDomainSeedSpeciesInput().getInputName();
                    }
                }
                default -> throw new AgrosystTechnicalException(l(locale, "IndicatorWriter.missingField.inputUsage") + " " + inputUsage.getInputType());
            }
        }
        return result;
    }
    
    public MissingFieldMessage getInvalidValorisationMessage() {
        return new MissingFieldMessage(l(locale, "IndicatorWriter.missingField.invalidValorisation"), MissingMessageScope.INTERVENTION);
    }
}
