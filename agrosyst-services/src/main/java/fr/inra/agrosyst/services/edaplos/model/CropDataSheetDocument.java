package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.edaplos.annotations.ValidCode;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class CropDataSheetDocument implements AgroEdiObject {

    @NotBlank(message = "Le document ne contient pas d'Id")
    protected String identification;

    @NotBlank(message = "Le document ne contient pas de TypeCode")
    @ValidCode(codes = {"415", "AAA10", "AAA11"}, message = "Le TypeCode du document n'est pas le bon ${validatedValue}")
    protected String typeCode;

    protected String creationDateTime;

    protected String copyIndicator;

    protected ProductionSoftware usedProductionSoftware;
    
    @Valid
    @NotNull(message = "Le document ne contient pas les informations nécessaires pour l'émetteur")
    protected CropDataSheetParty issuerCropDataSheetParty;

    @Valid
    protected RecipientCropDataSheetParty recipientCropDataSheetParty;

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(String creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public String getCopyIndicator() {
        return copyIndicator;
    }

    public void setCopyIndicator(String copyindicator) {
        this.copyIndicator = copyindicator;
    }

    public ProductionSoftware getUsedProductionSoftware() {
        return usedProductionSoftware;
    }

    public void setUsedProductionSoftware(ProductionSoftware usedProductionSoftware) {
        this.usedProductionSoftware = usedProductionSoftware;
    }

    public CropDataSheetParty getIssuerCropDataSheetParty() {
        return issuerCropDataSheetParty;
    }

    public void setIssuerCropDataSheetParty(CropDataSheetParty issuerCropDataSheetParty) {
        this.issuerCropDataSheetParty = issuerCropDataSheetParty;
    }

    public RecipientCropDataSheetParty getRecipientCropDataSheetParty() {
        return recipientCropDataSheetParty;
    }

    public void setRecipientCropDataSheetParty(RecipientCropDataSheetParty recipientCropDataSheetParty) {
        this.recipientCropDataSheetParty = recipientCropDataSheetParty;
    }

    @Override
    public String getLocalizedIdentifier() {
        return "domain '" + identification + "'";
    }

}
