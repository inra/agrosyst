package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AutorisationFR;
import fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenneImpl;
import fr.inra.agrosyst.services.common.CommonService;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueParser;

public class RefSubstancesActivesCommissionEuropeenneModel extends AbstractAgrosystModel<RefSubstancesActivesCommissionEuropeenne>  implements ExportModel<RefSubstancesActivesCommissionEuropeenne> {

    private static final ValueParser<AutorisationFR> AUTORISATION_FR_PARSER = value -> (AutorisationFR) getGenericEnumParser(AutorisationFR.class, value);

    public RefSubstancesActivesCommissionEuropeenneModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("ID_SA", RefSubstancesActivesCommissionEuropeenne.PROPERTY_ID_SA);
        newOptionalColumn("Libelle_SA_EU", RefSubstancesActivesCommissionEuropeenne.PROPERTY_LIBELLE_SA_EU);
        newOptionalColumn("Statut_regl_1107_2009", RefSubstancesActivesCommissionEuropeenne.PROPERTY_STATUT_REGL_1107_2009);
        newOptionalColumn("Libelle_SA_FR", RefSubstancesActivesCommissionEuropeenne.PROPERTY_LIBELLE_SA_FR);
        newOptionalColumn("Libelle_SA_variant_FR", RefSubstancesActivesCommissionEuropeenne.PROPERTY_LIBELLE_SA_VARIANT_FR);
        newOptionalColumn("Numero_CAS_FR", RefSubstancesActivesCommissionEuropeenne.PROPERTY_NUMERO_CAS);
        newOptionalColumn("Autorisation_FR", RefSubstancesActivesCommissionEuropeenne.PROPERTY_AUTORISATION_FR, AUTORISATION_FR_PARSER);
        newOptionalColumn("Coef_ponderation", RefSubstancesActivesCommissionEuropeenne.PROPERTY_COEF_PONDERATION, DOUBLE_PARSER);
        newOptionalColumn("Campagne", RefSubstancesActivesCommissionEuropeenne.PROPERTY_CAMPAGNE, INT_PARSER);
        newOptionalColumn("Date_approbation_EU", RefSubstancesActivesCommissionEuropeenne.PROPERTY_DATE_APPROBATION_EU, LOCAL_DATE_PARSER);
        newOptionalColumn("Date_expiration_EU", RefSubstancesActivesCommissionEuropeenne.PROPERTY_DATE_EXPIRATION_EU, LOCAL_DATE_PARSER);
        newOptionalColumn("SA_candidates_substitution", RefSubstancesActivesCommissionEuropeenne.PROPERTY_SA_CANDIDATES_SUBSTITUTION, CommonService.BOOLEAN_PARSER);
        newOptionalColumn("Substances_de_base", RefSubstancesActivesCommissionEuropeenne.PROPERTY_SUBSTANCES_DE_BASE, CommonService.BOOLEAN_PARSER);
        newOptionalColumn("SA_faible_risque", RefSubstancesActivesCommissionEuropeenne.PROPERTY_SA_FAIBLE_RISQUE, CommonService.BOOLEAN_PARSER);
        newOptionalColumn("Cuivre", RefSubstancesActivesCommissionEuropeenne.PROPERTY_CUIVRE, CommonService.BOOLEAN_PARSER);
        newOptionalColumn("Soufre", RefSubstancesActivesCommissionEuropeenne.PROPERTY_SOUFRE, CommonService.BOOLEAN_PARSER);
        newOptionalColumn("Pays", RefSubstancesActivesCommissionEuropeenne.PROPERTY_PAYS);
        newOptionalColumn("Source", RefSubstancesActivesCommissionEuropeenne.PROPERTY_SOURCE);
        newOptionalColumn("Active", RefSubstancesActivesCommissionEuropeenne.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefSubstancesActivesCommissionEuropeenne, Object>> getColumnsForExport() {
        ModelBuilder<RefFeedbackRouter> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("ID_SA", RefSubstancesActivesCommissionEuropeenne.PROPERTY_ID_SA);
        modelBuilder.newColumnForExport("Libelle_SA_EU", RefSubstancesActivesCommissionEuropeenne.PROPERTY_LIBELLE_SA_EU);
        modelBuilder.newColumnForExport("Statut_regl_1107_2009", RefSubstancesActivesCommissionEuropeenne.PROPERTY_STATUT_REGL_1107_2009);
        modelBuilder.newColumnForExport("Libelle_SA_FR", RefSubstancesActivesCommissionEuropeenne.PROPERTY_LIBELLE_SA_FR);
        modelBuilder.newColumnForExport("Libelle_SA_variant_FR", RefSubstancesActivesCommissionEuropeenne.PROPERTY_LIBELLE_SA_VARIANT_FR);
        modelBuilder.newColumnForExport("Numero_CAS_FR", RefSubstancesActivesCommissionEuropeenne.PROPERTY_NUMERO_CAS);
        modelBuilder.newColumnForExport("Autorisation_FR", RefSubstancesActivesCommissionEuropeenne.PROPERTY_AUTORISATION_FR, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Coef_ponderation", RefSubstancesActivesCommissionEuropeenne.PROPERTY_COEF_PONDERATION, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Campagne", RefSubstancesActivesCommissionEuropeenne.PROPERTY_CAMPAGNE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Date_approbation_EU", RefSubstancesActivesCommissionEuropeenne.PROPERTY_DATE_APPROBATION_EU, LOCAL_DATE_FORMATTER);
        modelBuilder.newColumnForExport("Date_expiration_EU", RefSubstancesActivesCommissionEuropeenne.PROPERTY_DATE_EXPIRATION_EU, LOCAL_DATE_FORMATTER);
        modelBuilder.newColumnForExport("SA_candidates_substitution", RefSubstancesActivesCommissionEuropeenne.PROPERTY_SA_CANDIDATES_SUBSTITUTION, O_N_FORMATTER);
        modelBuilder.newColumnForExport("Substances_de_base", RefSubstancesActivesCommissionEuropeenne.PROPERTY_SUBSTANCES_DE_BASE, O_N_FORMATTER);
        modelBuilder.newColumnForExport("SA_faible_risque", RefSubstancesActivesCommissionEuropeenne.PROPERTY_SA_FAIBLE_RISQUE, O_N_FORMATTER);
        modelBuilder.newColumnForExport("Cuivre", RefSubstancesActivesCommissionEuropeenne.PROPERTY_CUIVRE, O_N_FORMATTER);
        modelBuilder.newColumnForExport("Soufre", RefSubstancesActivesCommissionEuropeenne.PROPERTY_SOUFRE, O_N_FORMATTER);
        modelBuilder.newColumnForExport("Pays", RefSubstancesActivesCommissionEuropeenne.PROPERTY_PAYS);
        modelBuilder.newColumnForExport("Source", RefSubstancesActivesCommissionEuropeenne.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport("Active", RefSubstancesActivesCommissionEuropeenne.PROPERTY_ACTIVE, T_F_FORMATTER);

        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefSubstancesActivesCommissionEuropeenne newEmptyInstance() {
        RefSubstancesActivesCommissionEuropeenne entity = new RefSubstancesActivesCommissionEuropeenneImpl();
        entity.setActive(true);
        return entity;
    }
}
