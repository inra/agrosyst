package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Surface par UTH.

 * Présentation de l’indicateur
 * La surface par UTH est calculée sur la base de l’indicateur de Systerre1. C’est une indication de
 * la charge de travail des travailleurs d’un domaine. Cet indicateur est exprimé en ha UTH-1.
 * Formule de calcul
 * <pre>
 *                   SAU totale
 * Surface par UTH = ----------
 *                   MO totale
 * </pre>
 * 
 * Avec,
 * Surface par UTH : Surface par unité de travail humain (ha UTH-1)
 * SAU totale : Surface Agricole Utile totale du domaine (ha). Donnée saisie par l’utilisateur.
 * MO totale : Main d’œuvre totale du domaine (UMO). Donnée saisie par l’utilisateur.
 * Echelles de présentation de la variable calculée
 * Le calcul est directement réalisé à l’échelle du domaine, en utilisant des données saisies au niveau du domaine.
 * Échelle temporelle :
 * La surface par UTH peut être présentée aux échelles temporelles de :
 * 1 année
 * plusieurs années

 * Les modalités de changement d’échelle sont présentées ici.

 * Données de référence
 * Aucune donnée de référence pour ce calcul
 *
 * @author Eric Chatellier
 */
public class IndicatorSurfaceUTH extends AbstractIndicator {

    protected boolean exportToFile = true;

    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);
    
    public IndicatorSurfaceUTH() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, "surface_par_unite_de_travail_humain_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, "surface_par_unite_de_travail_humain_detail_champs_non_renseig");
    }
    
    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), "surface_par_unite_de_travail_humain");
        return indicatorNameToColumnName;
    }
    
    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
//        L'indicateur surface par UTH est calculé uniquement à l'echelle domaine.
//        Sauf que dans les exports en masse, il n'y a pas cette échelle.
//        Pour cette raison on l'ajoute aux échelles "sdc" et "synthetisé"
        return (ExportLevel.DOMAIN == atLevel || (exportLevels.contains(atLevel) && !exportToFile)) &&
                displayed;
    }
    
    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }
    
    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_SOCIO_TECHNIC);
    }
    
    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, "Indicator.label.surfaceUTH");
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {
        // rien à calculer à cette échelle
        return null;
    }

    @Override
    public Double[] manageIntervention(
            WriterContext writerContext,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveCropExecutionContext cropContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {
        // rien à calculer à cette échelle
        return null;
    }

    @Override
    public void computeEffective(
            IndicatorWriter writer,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext) {
        // rien à calculer à cette échelle
    }

    @Override
    public void computePracticed(IndicatorWriter writer, Domain domain) {
        if (domain.getUsedAgriculturalArea() != null) {
            double totalMO = getDomainTotalMO(domain);
            Double usedAgriArea = domain.getUsedAgriculturalArea();
            if (totalMO != 0 && usedAgriArea != null) {
                double areaBYMo = usedAgriArea / totalMO;
                writer.writePracticedDomain(
                        String.valueOf(domain.getCampaign()),
                        getIndicatorCategory(),
                        getIndicatorLabel(0),
                        areaBYMo,
                        DEFAULT_RELIABILITY_INDEX,
                        RELIABILITY_INDEX_NO_COMMENT,
                        domain,
                        "");
            }
        }

    }

    @Override
    public void computeEffective(IndicatorWriter writer, Domain domain) {
        if (domain.getUsedAgriculturalArea() != null) {
            double totalMO = getDomainTotalMO(domain);// je passe ici
            Double usedAgriArea = domain.getUsedAgriculturalArea();
            if (totalMO != 0 && usedAgriArea != null) {
                double areaBYMo = usedAgriArea / totalMO;
                writer.writeEffectiveDomain(
                        getIndicatorCategory(),
                        getIndicatorLabel(0),
                        areaBYMo,
                        DEFAULT_RELIABILITY_INDEX,
                        RELIABILITY_INDEX_NO_COMMENT,
                        domain,
                        "");
            }
        }
    }

    @Override
    public void computeEffective(IndicatorWriter writer,
                                 PerformanceEffectiveDomainExecutionContext domainContext,
                                 PerformanceGrowingSystemExecutionContext growingSystemContext) {

        //        L'indicateur surface par UTH est calculé uniquement à l'echelle domaine.
        //        Sauf que dans les exports en masse, il n'y a pas cette échelle.
        //        Pour cette raison on l'ajoute aux echelles "sdc" et "synthetisé"

        if (domainContext.getDomainIndicatorSurfaceUTH() == null) {
            Domain domain = domainContext.getDomain();
            Double areaBYMo = computSurfaceUTH(domain);
            domainContext.setDomainIndicatorSurfaceUTH(areaBYMo);
        }

        Domain anonymiseDomain = domainContext.getAnonymiseDomain();
        Collection<CroppingPlanSpecies> domainAgrosystSpecies = domainContext.getCroppingPlanSpecies();

        if (domainContext.getDomainIndicatorSurfaceUTH() != null) {

            Optional<GrowingSystem> anonymizeGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();

            String its = growingSystemContext.getIts();
            String irs = growingSystemContext.getIrs();

            boolean isDisplayed = isDisplayed(ExportLevel.GROWING_SYSTEM, 0);
            if (isDisplayed) {
                // write SDC sheet
                writer.writeEffectiveGrowingSystem(its,
                        irs,
                        getIndicatorCategory(),
                        getIndicatorLabel(0),
                        domainContext.getDomainIndicatorSurfaceUTH(),
                        DEFAULT_RELIABILITY_INDEX,
                        RELIABILITY_INDEX_NO_COMMENT,
                        anonymiseDomain,
                        anonymizeGrowingSystem,
                        this.getClass(),
                        domainAgrosystSpecies);
            }
        }
    }

    @Override
    public void computePracticed(
            IndicatorWriter writer,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            PerformancePracticedDomainExecutionContext domainContext) {

        if (growingSystemContext.getAnonymizeGrowingSystem().isEmpty()) {
            return;
        }

        Optional<GrowingSystem> optionalGnonymizeGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();

//        L'indicateur surface par UTH est calculé uniquement à l'échelle domaine.
//        Sauf que dans les exports en masse, il n'y a pas cette échelle.
//        Pour cette raison, on l'ajoute aux échelles "sdc" et "synthetisé"

        Domain domain = domainContext.getDomain();
        Double domainIndicatorSurfaceUTH = computSurfaceUTH(domain);

        if (domainIndicatorSurfaceUTH == null) {
            return;
        }

        Domain anonymizeDomain = domainContext.getAnonymiseDomain();

        String its = growingSystemContext.getIts();
        String irs = growingSystemContext.getIrs();

        for (PerformancePracticedSystemExecutionContext practicedSystemExecutionContext : growingSystemContext.getPracticedSystemExecutionContexts()) {

            boolean isDisplayed = isDisplayed(ExportLevel.PRACTICED_SYSTEM, 0);
            if (isDisplayed) {
                // write practiced system sheet
                writer.writePracticedSystem(
                        its,
                        irs,
                        practicedSystemExecutionContext.getPracticedSystem().getCampaigns(),
                        getIndicatorCategory(),
                        getIndicatorLabel(0),
                        domainIndicatorSurfaceUTH,
                        DEFAULT_RELIABILITY_INDEX,
                        RELIABILITY_INDEX_NO_COMMENT,
                        anonymizeDomain,
                        optionalGnonymizeGrowingSystem.get(),
                        practicedSystemExecutionContext.getAnonymizePracticedSystem(),
                        this.getClass());
            }
        }
    }

    protected Double computSurfaceUTH(Domain domain) {
        Double reuslt = null;
        if (domain.getUsedAgriculturalArea() != null) {
            double totalMO = getDomainTotalMO(domain);
            Double usedAgriArea = domain.getUsedAgriculturalArea();
            if (totalMO != 0 && usedAgriArea != null) {
                reuslt = usedAgriArea / totalMO;
            }
        }
        return reuslt;
    }

    protected double getDomainTotalMO(Domain domain) {
        double result = 0;
        if (domain.getOtherWorkForce() != null) {
            result += domain.getOtherWorkForce();
        }
        if (domain.getPermanentEmployeesWorkForce() != null) {
            result += domain.getPermanentEmployeesWorkForce();
        }
        if (domain.getTemporaryEmployeesWorkForce() != null) {
            result += domain.getTemporaryEmployeesWorkForce();
        }
        return result;
    }
    
    public void init(IndicatorFilter filter, boolean exportToFile) {
        displayed = filter != null;
        this.exportToFile = exportToFile;
    }
}
