package fr.inra.agrosyst.services.security;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRule;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.entities.report.ReportRegional;
import fr.inra.agrosyst.api.entities.security.ComputedUserPermission;
import fr.inra.agrosyst.api.entities.security.PermissionObjectType;

import java.util.Map;
import java.util.Optional;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 * @since 0.8
 */
public class SecurityHelper {

    public static final int PERMISSION_READ_VALIDATED = 1;
    public static final int PERMISSION_READ_RAW = 3;
    public static final int PERMISSION_WRITE = 7;
    public static final int PERMISSION_ADMIN = 15;

    // Le fait d'utiliser EXISTS au lieu de IN permet d'optimiser la requête pour PostgreSQL qui peut plus facilement
    // utiliser les INDEX plutôt que de remonter toutes les valeurs de la sous requête pour ensuite les tester.
    protected static final String IN_SELECT_CUP_ACTION_HIGHER_THAN =
            " %s.%s IN ( " +
                    "   SELECT cup." + ComputedUserPermission.PROPERTY_OBJECT +
                    "   FROM " + ComputedUserPermission.class.getName() + " cup" +
                    "   WHERE cup." + ComputedUserPermission.PROPERTY_USER_ID + " = :cup_userId" +
                    "   AND   cup." + ComputedUserPermission.PROPERTY_TYPE + " = :%s" +
                    "   AND   cup." + ComputedUserPermission.PROPERTY_ACTION + " >= :cup_action" +
                    " ) ";
    protected static final String IN_SELECT_CUP_ACTION_EQUALS =
            " %s.%s IN ( " +
                    "   SELECT cup." + ComputedUserPermission.PROPERTY_OBJECT +
                    "   FROM " + ComputedUserPermission.class.getName() + " cup" +
                    "   WHERE cup." + ComputedUserPermission.PROPERTY_USER_ID + " = :cup_userId" +
                    "   AND   cup." + ComputedUserPermission.PROPERTY_TYPE + " = :%s" +
                    "   AND   cup." + ComputedUserPermission.PROPERTY_ACTION + " = :cup_action_read_validated" +
                    " ) ";

    protected static void addMinimumActionSecurityFilter(StringBuilder query,
                                                         Map<String, Object> args,
                                                         SecurityContext securityContext,
                                                         String alias,
                                                         PermissionObjectType cupTypeCode,
                                                         PermissionObjectType cupTypeId,
                                                         String propCode,
                                                         String propId,
                                                         int action) {

        if (!securityContext.isAdmin()) {

            String userId = securityContext.getUserId();

            String codeInReadRaw = String.format(IN_SELECT_CUP_ACTION_HIGHER_THAN, alias, propCode, "cup_type_code");
            String topiaIdInReadRaw = String.format(IN_SELECT_CUP_ACTION_HIGHER_THAN, alias, propId, "cup_type_id");

            String securityQuery = String.format(
                    " AND ( %s OR %s ) ",
                    codeInReadRaw,
                    topiaIdInReadRaw);

            query.append(securityQuery);
            args.put("cup_userId", userId);
            args.put("cup_type_code", cupTypeCode);
            args.put("cup_type_id", cupTypeId);
            args.put("cup_action", action);
        }
    }

    protected static void addMinimumActionSecurityFilter(StringBuilder query,
                                                         Map<String, Object> args,
                                                         SecurityContext securityContext,
                                                         String alias,
                                                         PermissionObjectType cupTypeId,
                                                         String propId,
                                                         int action) {

        if (!securityContext.isAdmin()) {

            String userId = securityContext.getUserId();

            String topiaIdInReadRaw = String.format(IN_SELECT_CUP_ACTION_HIGHER_THAN, alias, propId, "cup_type_id");

            String securityQuery = String.format(
                    " AND %s ", topiaIdInReadRaw);

            query.append(securityQuery);
            args.put("cup_userId", userId);
            args.put("cup_type_id", cupTypeId);
            args.put("cup_action", action);
        }
    }

    protected static void addReadSecurityFilter(StringBuilder query,
                                                Map<String, Object> args,
                                                SecurityContext securityContext,
                                                String alias,
                                                PermissionObjectType cupTypeId,
                                                String propId) {

        addMinimumActionSecurityFilter(query, args, securityContext, alias, cupTypeId, propId,
                SecurityHelper.PERMISSION_READ_RAW);
    }

    protected static void addReadValidatedSecurityFilter(StringBuilder query,
                                                         Map<String, Object> args,
                                                         SecurityContext securityContext,
                                                         String alias,
                                                         PermissionObjectType cupTypeCode,
                                                         PermissionObjectType cupTypeId,
                                                         String propCode,
                                                         String propId) {

        addMinimumActionSecurityFilter(query, args, securityContext, alias,
                cupTypeCode, cupTypeId, propCode, propId,
                SecurityHelper.PERMISSION_READ_VALIDATED);
    }

    protected static void addWriteSecurityFilter(StringBuilder query,
                                                 Map<String, Object> args,
                                                 SecurityContext securityContext,
                                                 String alias,
                                                 PermissionObjectType cupTypeCode,
                                                 PermissionObjectType cupTypeId,
                                                 String propCode,
                                                 String propId) {
        addMinimumActionSecurityFilter(query, args, securityContext, alias,
                cupTypeCode, cupTypeId, propCode, propId,
                SecurityHelper.PERMISSION_WRITE);
    }

    protected static void addReadSecurityFilterOnValidableEntity(StringBuilder query,
                                                                 Map<String, Object> args,
                                                                 SecurityContext securityContext,
                                                                 String alias,
                                                                 PermissionObjectType cupTypeCode,
                                                                 PermissionObjectType cupTypeId,
                                                                 String propCode,
                                                                 String propId,
                                                                 String propValidated) {

        if (!securityContext.isAdmin()) {

            String userId = securityContext.getUserId();

            String codeInReadRaw = String.format(IN_SELECT_CUP_ACTION_HIGHER_THAN, alias, propCode, "cup_type_code");
            String topiaIdInReadRaw = String.format(IN_SELECT_CUP_ACTION_HIGHER_THAN, alias, propId, "cup_type_id");
            String validated = alias + "." + propValidated;
            String codeInReadValidated = String.format(IN_SELECT_CUP_ACTION_EQUALS, alias, propCode, "cup_type_code");
            String topiaIdInReadValidated = String.format(IN_SELECT_CUP_ACTION_EQUALS, alias, propId, "cup_type_id");

            String securityQuery = String.format(
                    " AND ( ( %s OR %s ) OR ( %s = true AND ( %s OR %s ) ) ) ",
                    codeInReadRaw,
                    topiaIdInReadRaw,
                    validated,
                    codeInReadValidated,
                    topiaIdInReadValidated);

            query.append(securityQuery);
            args.put("cup_userId", userId);
            args.put("cup_type_code", cupTypeCode);
            args.put("cup_type_id", cupTypeId);
            args.put("cup_action", SecurityHelper.PERMISSION_READ_RAW);
            args.put("cup_action_read_validated", SecurityHelper.PERMISSION_READ_VALIDATED);
        }

    }

    public static void addDomainFilter(StringBuilder query,
                                       Map<String, Object> args,
                                       SecurityContext securityContext,
                                       String alias) {
        addReadSecurityFilterOnValidableEntity(query, args, securityContext, alias,
                PermissionObjectType.DOMAIN_CODE, PermissionObjectType.DOMAIN_ID,
                Domain.PROPERTY_CODE, Domain.PROPERTY_TOPIA_ID, Domain.PROPERTY_VALIDATED);
    }

    public static void addWritableDomainFilterForDecisionRuleCreation(StringBuilder query,
                                                                      Map<String, Object> args,
                                                                      SecurityContext securityContext,
                                                                      String alias) {

        StringBuilder subQuery1 = new StringBuilder();
        addWriteSecurityFilter(subQuery1, args, securityContext, alias,
                PermissionObjectType.DOMAIN_CODE, PermissionObjectType.DOMAIN_ID,
                Domain.PROPERTY_CODE, Domain.PROPERTY_TOPIA_ID);

        StringBuilder subQuery2 = new StringBuilder();
        if (!securityContext.isAdmin()) {

            subQuery2.append(
                    String.format(
                            IN_SELECT_CUP_ACTION_HIGHER_THAN,
                            alias,
                            Domain.PROPERTY_TOPIA_ID + " IN ( SELECT gp." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID + " FROM " + GrowingPlan.class.getName() + " gp WHERE gp." + GrowingPlan.PROPERTY_CODE,
                            "cup_gp_code"));
            subQuery2.append(" ) "); // Because of opened bracket

            args.put("cup_gp_code", PermissionObjectType.GROWING_PLAN_CODE);

        }

        if (!subQuery1.isEmpty() && !subQuery2.isEmpty()) {
            String hql = subQuery1.toString();
            Preconditions.checkState(hql.endsWith(" ) "));
            hql = hql.substring(0, hql.length() - 3);
            hql += " OR ";
            hql += subQuery2.toString();
            hql += " ) ";
            query.append(hql);
        }
    }

    public static void addReadValidatedDomainFilterForDecisionRuleList(StringBuilder query,
                                                                  Map<String, Object> args,
                                                                  SecurityContext securityContext,
                                                                  String alias) {

        StringBuilder subQuery1 = new StringBuilder();
        addReadValidatedSecurityFilter(subQuery1, args, securityContext, alias,
                PermissionObjectType.DOMAIN_CODE, PermissionObjectType.DOMAIN_ID,
                Domain.PROPERTY_CODE, Domain.PROPERTY_TOPIA_ID);
        query.append(subQuery1);
    }

    public static void addGrowingPlanFilter(StringBuilder query,
                                            Map<String, Object> args,
                                            SecurityContext securityContext,
                                            String alias) {
        addReadSecurityFilterOnValidableEntity(query, args, securityContext, alias,
                PermissionObjectType.GROWING_PLAN_CODE, PermissionObjectType.GROWING_PLAN_ID,
                GrowingPlan.PROPERTY_CODE, GrowingPlan.PROPERTY_TOPIA_ID, GrowingPlan.PROPERTY_VALIDATED);
    }

    public static void addGrowingSystemFilter(StringBuilder query,
                                              Map<String, Object> args,
                                              SecurityContext securityContext,
                                              String alias) {
        addReadSecurityFilterOnValidableEntity(query, args, securityContext, alias,
                PermissionObjectType.GROWING_SYSTEM_CODE, PermissionObjectType.GROWING_SYSTEM_ID,
                GrowingSystem.PROPERTY_CODE, GrowingSystem.PROPERTY_TOPIA_ID, GrowingSystem.PROPERTY_VALIDATED);
    }

    public static void addManagementModeFilter(StringBuilder query,
                                               Map<String, Object> args,
                                               SecurityContext securityContext,
                                               String alias) {
        addReadSecurityFilterOnValidableEntity(query, args, securityContext, alias,
                PermissionObjectType.GROWING_SYSTEM_CODE, PermissionObjectType.GROWING_SYSTEM_ID,
                ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_CODE,
                ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID,
                ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_VALIDATED);
    }

    public static void addZoneFilter(StringBuilder query,
                                     Map<String, Object> args,
                                     SecurityContext securityContext,
                                     String alias) {

        if (!securityContext.isAdmin()) {

            // NB AThimel 15/09/2020 Le IN a été réécrit en EXISTS pour optimiser la requête pour PostgreSQL qui peut
            // plus facilement utiliser les INDEX plutôt que de remonter toutes les valeurs de la sous requête pour
            // ensuite les tester.

            String zonePlotTopiaId = alias + "." + Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_TOPIA_ID;

            // Pas de growingsystem, il faut le droit sur le domaine
            StringBuilder subQuery1 = new StringBuilder(" SELECT 1 FROM " + Plot.class.getName() + " p1 ");
            subQuery1.append(" WHERE p1.topiaId = " + zonePlotTopiaId + " ");
            subQuery1.append(" AND p1." + Plot.PROPERTY_GROWING_SYSTEM + " IS NULL ");

            addReadSecurityFilterOnValidableEntity(subQuery1, args, securityContext, "p1",
                    PermissionObjectType.DOMAIN_CODE, PermissionObjectType.DOMAIN_ID,
                    String.join(".", Plot.PROPERTY_DOMAIN, Domain.PROPERTY_CODE),
                    String.join(".", Plot.PROPERTY_DOMAIN, Domain.PROPERTY_TOPIA_ID),
                    String.join(".", Plot.PROPERTY_DOMAIN, Domain.PROPERTY_VALIDATED)
            );

            // Dans l'une des 2 requêtes, il faut renommer les propriétés cup_type_code et cup_type_id
            String subQuery1String = subQuery1.toString();
            for (String toReplace : Sets.newHashSet("cup_type_code", "cup_type_id")) {
                final String dotPlusToReplace = ":" + toReplace;
                while (subQuery1String.contains(dotPlusToReplace)) {
                    int index = subQuery1String.indexOf(dotPlusToReplace);
                    String part0 = subQuery1String.substring(0, index);
                    String part1 = ":domain_" + toReplace;
                    String part2 = subQuery1String.substring(index + dotPlusToReplace.length());
                    subQuery1String = part0 + part1 + part2;
                }
            }
            args.remove("cup_type_code");
            args.remove("cup_type_id");
            args.put("domain_cup_type_code", PermissionObjectType.DOMAIN_CODE);
            args.put("domain_cup_type_id", PermissionObjectType.DOMAIN_ID);

            // growing system, il faut le droit sur le sdc
            StringBuilder subQuery2 = new StringBuilder(" SELECT 1 FROM " + Plot.class.getName() + " p2 ");
            subQuery2.append(" WHERE p2.topiaId = " + zonePlotTopiaId + " ");
            subQuery2.append(" AND p2." + Plot.PROPERTY_GROWING_SYSTEM + " IS NOT NULL ");
            addReadSecurityFilterOnValidableEntity(subQuery2, args, securityContext, "p2",
                    PermissionObjectType.GROWING_SYSTEM_CODE, PermissionObjectType.GROWING_SYSTEM_ID,
                    String.join(".", Plot.PROPERTY_GROWING_SYSTEM, GrowingSystem.PROPERTY_CODE),
                    String.join(".", Plot.PROPERTY_GROWING_SYSTEM, GrowingSystem.PROPERTY_TOPIA_ID),
                    String.join(".", Plot.PROPERTY_GROWING_SYSTEM, GrowingSystem.PROPERTY_VALIDATED)
            );

            query.append(String.format(
                    " AND ( " +
                            " EXISTS ( %s ) " +
                            " OR " +
                            " EXISTS ( %s ) " +
                            " ) ", subQuery1String, subQuery2));

        }

    }

    /**
     * Créé une requête HQL de filtrage des Plot en fonction des droits de l'utilisateur. Cette méthode déduit la liste
     * des Plot en se basant sur les droits au niveau des domaines
     */
    public static Optional<String> createPlotFilterQueryForDomain(Map<String, Object> args,
                                                                  SecurityContext securityContext) {

        if (securityContext.isAdmin()) {
            return Optional.empty();
        } else {

            // Pas de growingsystem, il faut le droit sur le domaine
            StringBuilder subQuery1 = new StringBuilder(" SELECT p1.topiaId FROM " + Plot.class.getName() + " p1 ");
            subQuery1.append(" WHERE p1." + Plot.PROPERTY_GROWING_SYSTEM + " IS NULL ");

            addReadSecurityFilterOnValidableEntity(subQuery1, args, securityContext, "p1",
                    PermissionObjectType.DOMAIN_CODE, PermissionObjectType.DOMAIN_ID,
                    String.join(".", Plot.PROPERTY_DOMAIN, Domain.PROPERTY_CODE),
                    String.join(".", Plot.PROPERTY_DOMAIN, Domain.PROPERTY_TOPIA_ID),
                    String.join(".", Plot.PROPERTY_DOMAIN, Domain.PROPERTY_VALIDATED)
            );

            return Optional.of(subQuery1.toString());
        }

    }

    /**
     * Créé une requête HQL de filtrage des Plot en fonction des droits de l'utilisateur. Cette méthode déduit la liste
     * des Plot en se basant sur les droits au niveau des systèmes de culture
     */
    public static Optional<String> createPlotFilterQueryForGrowingSystem(Map<String, Object> args,
                                                                         SecurityContext securityContext) {

        if (securityContext.isAdmin()) {
            return Optional.empty();
        } else {

            StringBuilder subQuery2 = new StringBuilder(" SELECT p2.topiaId FROM " + Plot.class.getName() + " p2 ");
            subQuery2.append(" WHERE p2." + Plot.PROPERTY_GROWING_SYSTEM + " IS NOT NULL ");

            addReadSecurityFilterOnValidableEntity(subQuery2, args, securityContext, "p2",
                    PermissionObjectType.GROWING_SYSTEM_CODE, PermissionObjectType.GROWING_SYSTEM_ID,
                    String.join(".", Plot.PROPERTY_GROWING_SYSTEM, GrowingSystem.PROPERTY_CODE),
                    String.join(".", Plot.PROPERTY_GROWING_SYSTEM, GrowingSystem.PROPERTY_TOPIA_ID),
                    String.join(".", Plot.PROPERTY_GROWING_SYSTEM, GrowingSystem.PROPERTY_VALIDATED)
            );

            return Optional.of(subQuery2.toString());
        }

    }

    public static void addWritableGrowingSystemFilter(StringBuilder query,
                                                      Map<String, Object> args,
                                                      SecurityContext securityContext,
                                                      String alias) {
        addWriteSecurityFilter(query, args, securityContext, alias,
                PermissionObjectType.GROWING_SYSTEM_CODE, PermissionObjectType.GROWING_SYSTEM_ID,
                GrowingSystem.PROPERTY_CODE, GrowingSystem.PROPERTY_TOPIA_ID);
    }

    public static void addDecisionRuleFilter(StringBuilder query,
                                             Map<String, Object> args,
                                             SecurityContext securityContext,
                                             String alias) {

        StringBuilder subQuery2 = new StringBuilder();

        // Les domaines sont tous validés
        addReadValidatedSecurityFilter(subQuery2, args, securityContext, alias,
                PermissionObjectType.DOMAIN_CODE,
                PermissionObjectType.DOMAIN_ID,
                DecisionRule.PROPERTY_DOMAIN_CODE,
                "domainCode IN ( SELECT d.code FROM " + Domain.class.getName() + " d WHERE d.topiaId ");

        if (!subQuery2.isEmpty()) {
            subQuery2.append(" ) "); // Because of opened bracket
        }
        query.append(subQuery2);

    }

    public static void addPracticedSystemFilter(StringBuilder query,
                                                Map<String, Object> args,
                                                SecurityContext securityContext,
                                                String alias) {

        String propertyValidated = PracticedSystem.PROPERTY_VALIDATED + " = true " +
                " AND " + alias + "." + String.join(".", PracticedSystem.PROPERTY_GROWING_SYSTEM, GrowingSystem.PROPERTY_VALIDATED
        );

        addReadSecurityFilterOnValidableEntity(query, args, securityContext, alias,
                PermissionObjectType.GROWING_SYSTEM_CODE, PermissionObjectType.GROWING_SYSTEM_ID,
                String.join(".", PracticedSystem.PROPERTY_GROWING_SYSTEM, GrowingSystem.PROPERTY_CODE),
                String.join(".", PracticedSystem.PROPERTY_GROWING_SYSTEM, GrowingSystem.PROPERTY_TOPIA_ID),
                propertyValidated);
    }

    public static void addPracticedPlotFilter(StringBuilder query,
                                              Map<String, Object> args,
                                              SecurityContext securityContext,
                                              String alias) {

        StringBuilder subQuery = new StringBuilder();
        addPracticedSystemFilter(subQuery, args, securityContext, "ps");

        if (!subQuery.isEmpty()) {
            String subFilter = String.format(
                    " AND %s.%s IN ( SELECT ps.%s FROM %s ps WHERE 1=1 %s ) ",
                    alias,
                    PracticedSystem.PROPERTY_TOPIA_ID,
                    PracticedSystem.PROPERTY_TOPIA_ID,
                    PracticedSystem.class.getName(),
                    subQuery
            );
            query.append(subFilter);
        }

    }

    public static void addReportRegionalFilter(StringBuilder query,
                                       Map<String, Object> args,
                                       SecurityContext securityContext,
                                       String alias) {
        addReadSecurityFilter(query, args, securityContext, alias,
                PermissionObjectType.REPORT_REGIONAL_ID,
                ReportRegional.PROPERTY_TOPIA_ID);
    }

    public static void addReportGrowingSystemFilter(StringBuilder query,
                                                    Map<String, Object> args,
                                                    SecurityContext securityContext,
                                                    String alias) {

        if (!securityContext.isAdmin()) {

            // soit un système de culture
            StringBuilder subQuery1 = new StringBuilder(" SELECT gs.topiaId FROM " + GrowingSystem.class.getName() + " gs ");
            subQuery1.append(" WHERE 1 = 1 ");
            addGrowingSystemFilter(subQuery1, args, securityContext, "gs");

            // Dans l'une des 2 requêtes, il faut renommer les propriétés cup_type_code et cup_type_id
            String subQuery1String = subQuery1.toString();
            for (String toReplace : Sets.newHashSet("cup_type_code", "cup_type_id")) {
                final String dotPlusToReplace = ":" + toReplace;
                while (subQuery1String.contains(dotPlusToReplace)) {
                    int index = subQuery1String.indexOf(dotPlusToReplace);
                    String part0 = subQuery1String.substring(0, index);
                    String part1 = ":gs_" + toReplace;
                    String part2 = subQuery1String.substring(index + dotPlusToReplace.length());
                    subQuery1String = part0 + part1 + part2;
                }
            }
            args.remove("cup_type_code");
            args.remove("cup_type_id");
            args.put("gs_cup_type_code", PermissionObjectType.GROWING_SYSTEM_CODE);
            args.put("gs_cup_type_id", PermissionObjectType.GROWING_SYSTEM_ID);

            query.append(String.format(" AND " + alias + "." + ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID + " IN ( %s )", subQuery1String));

        }

    }
}
