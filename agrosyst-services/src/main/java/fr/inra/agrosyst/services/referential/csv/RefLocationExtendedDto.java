package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.Arrays;
import java.util.Collection;

@Getter
@Setter
@RequiredArgsConstructor
public class RefLocationExtendedDto {

    public static final String PROPERTY_CODE_INSEE = "codeInsee";
    public static final String PROPERTY_COMMUNE = "commune";
    public static final String PROPERTY_PETITE_REGION_AGRICOLE_CODE = "petiteRegionAgricoleCode";
    public static final String PROPERTY_PETITE_REGION_AGRICOLE_NOM = "petiteRegionAgricoleNom";
    public static final String PROPERTY_DEPARTEMENT = "departement";
    public static final String PROPERTY_CODE_POSTAL = "codePostal";
    public static final String PROPERTY_REGION = "region";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_LATITUDE = "latitude";
    public static final String PROPERTY_LONGITUDE = "longitude";
    public static final String PROPERTY_PAYS = "pays";
    public static final String PROPERTY_ALTITUDE_MOY = "altitude_moy";
    public static final String PROPERTY_AIRE_ATTR = "aire_attr";
    public static final String PROPERTY_ARRONDISSEMENT_CODE = "arrondissement_code";
    public static final String PROPERTY_BASSIN_VIE = "bassin_vie";
    public static final String PROPERTY_INTERCOMMUNALITE_CODE = "intercommunalite_code";
    public static final String PROPERTY_UNITE_URBAINE = "unite_urbaine";
    public static final String PROPERTY_ZONE_EMPLOI = "zone_emploi";
    public static final String PROPERTY_BASSIN_VITICOLE = "bassin_viticole";
    public static final String PROPERTY_ANCIENNE_REGION = "ancienne_region";

    public static Collection<String> getAllProperties() {
        return Arrays.asList(
                PROPERTY_CODE_INSEE, PROPERTY_COMMUNE,
                PROPERTY_PETITE_REGION_AGRICOLE_CODE, PROPERTY_PETITE_REGION_AGRICOLE_NOM,
                PROPERTY_DEPARTEMENT, PROPERTY_CODE_POSTAL, PROPERTY_REGION,
                PROPERTY_PAYS, PROPERTY_LATITUDE, PROPERTY_LONGITUDE, PROPERTY_ACTIVE,
                PROPERTY_ALTITUDE_MOY, PROPERTY_AIRE_ATTR, PROPERTY_ARRONDISSEMENT_CODE,
                PROPERTY_BASSIN_VIE, PROPERTY_INTERCOMMUNALITE_CODE, PROPERTY_UNITE_URBAINE,
                PROPERTY_ZONE_EMPLOI, PROPERTY_BASSIN_VITICOLE, PROPERTY_ANCIENNE_REGION);
    }

    protected final String topiaId;

    public String codeInsee;

    public String commune;

    public String petiteRegionAgricoleCode;

    public String petiteRegionAgricoleNom;

    public String departement;

    public String codePostal;

    public int region;

    public boolean active;

    public double latitude;

    public double longitude;

    public Integer altitude_moy;

    public String aire_attr;

    public String arrondissement_code;

    public String bassin_vie;

    public String intercommunalite_code;

    public String unite_urbaine;

    public String zone_emploi;

    public String bassin_viticole;

    public String ancienne_region;

    public String pays;

}
