package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import org.apache.commons.collections4.MultiValuedMap;
import org.jetbrains.annotations.NotNull;

/**
 *  La formule de calcul est la même que pour HRI-1.
 * La différence est que pour chaque indicateur,
 * on ne considèrera que les substances actives prenant une certaine valeur dans la colonne "coef_ponderation" du référentiel "RefSubstancesActivesCommissionEuropeenne"

 * - HRI-1 Groupe 1 : Seulement pour le coeff "1"
 * - HRI-1 Groupe 2 : Seulement pour le coeff "8"
 * - HRI-1 Groupe 3 : Seulement pour le coeff "16"
 * - HRI-1 Groupe 4 : Seulement pour le coeff "64"

 * Comme pour les indicateurs QSA, et HRI-1, chaque indicateur aura une version avec et sans traitements de semences.
 */
public abstract class IndicatorHRI1_Group extends IndicatorTotalActiveSubstanceAmount {

    protected static final double IGNORE_COEF_PONDERATION = 0.0;

    /**
     * HRI-1 Groupe 1 : Seulement pour le coeff "1"
     */
    @Override
    protected double getCoef(@NotNull RefCompositionSubstancesActivesParNumeroAMM activeSubstance, PerformanceInterventionContext interventionContext) {

        MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode = interventionContext.getAllSubstancesActivesCommissionEuropeenneByAmmCode();
        double coef = allSubstancesActivesCommissionEuropeenneByAmmCode.values()
                .stream().filter(ref -> ref.getId_sa().contentEquals(activeSubstance.getId_sa()))
                .map(RefSubstancesActivesCommissionEuropeenne::getCoef_ponderation)
                .filter(coefPonderation -> coefPonderation.equals(getPonderation()))
                .findFirst()
                .orElse(IGNORE_COEF_PONDERATION);

        return super.getCoef(activeSubstance, interventionContext) * coef;
    }

    protected abstract double getPonderation();
}
