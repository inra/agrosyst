package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * <pre>
 * {@code
 * <ram:ConformanceCertificate>
 * <ram:Identification>AEE565126568685656598965216549686</ram:Identification>
 * <ram:SoftwareOperatingSystem>WinXP</ram:SoftwareOperatingSystem>
 * <ram:Issue>2101-12-17T09:30:47Z</ram:Issue>
 * </ram:ConformanceCertificate>
 * }
 * </pre>
 */
public class ConformanceCertificate implements AgroEdiObject {

    protected String identification;

    protected String softwareOperatingSystem;

    protected String issue;

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getSoftwareOperatingSystem() {
        return softwareOperatingSystem;
    }

    public void setSoftwareOperatingSystem(String softwareOperatingSystem) {
        this.softwareOperatingSystem = softwareOperatingSystem;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    @Override
    public String getLocalizedIdentifier() {
        return "conformance '" + identification + "'";
    }
}
