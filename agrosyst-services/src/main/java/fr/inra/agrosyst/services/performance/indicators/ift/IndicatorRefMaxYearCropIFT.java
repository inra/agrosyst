package fr.inra.agrosyst.services.performance.indicators.ift;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import fr.inra.agrosyst.api.services.performance.UsagePerformanceResult;
import fr.inra.agrosyst.services.referential.csv.RefActaDosageSPCModel;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.nuiton.i18n.I18n.l;

/**
 * Pour chaque intrant de type ‘Phytosanitaire’
 * • Repérer les espèces cultivées de la culture concernée (le plus souvent il n’y en a qu’une, mais il peut y en avoir plusieurs)
 * • Sur la base de la table de correspondance Correspondance_CulturesAgrosyst_CulturesMAA, établir la liste des espèces MAA.
 * • Dans le référentiel MAA_Doses_Ref_par_GroupeCibles, Repérer les Dose_Ref correspondant au quadruplets Campagne_Max|code_AMM|CultureMAA|Traitement_MAA . Retenir comme Dose_Ref la plus petite des valeurs trouvées
 * où Campagne_Max est la valeur la plus élevée du champ ‘Campagne’ de la table MAA_Doses_Ref_par_GroupeCibles
 * o Si on ne trouve pas le quadruplet Campagne_Max|code_AMM|CultureMAA| Traitement_MAA dans MAA_Doses_Ref_par_GroupeCibles, alors rechercher si on trouve le triplet code_AMM|CultureMAA|Traitement_MAA , et retenir comme Dose_Ref la plus petite des valeurs trouvées.
 * o Si on ne trouve pas le triplet code_AMM|CultureMAA|Unité de dose dans MAA_Doses_Ref_par_GroupeCibles, alors rechercher si on trouve le triplet Code_AMM|EspèceEDI|Unité de dose dans ACTA_Dosage_SPC_Complet. Retenir la plus petite valeur comme Dose_REF.
 * o Si on ne trouve pas le triplet Code_AMM|EspèceEDI|Unité de dose dans ACTA_Dosage_SPC_Complet, alors :
 * IFT_Culture_NonMillesimé = PSCI_PHYTO * 1
 * <p>
 * • Si le produit appliqué est identifié comme Biocontrôle = ‘O’ dans la table MAA_Biocontrôle pour la campagne Campagne_Max, alors tous les IFT ift_Culture_NonMillesimé_total, ift_Culture_NonMillesimé_tot_hts, ift_Culture_NonMillesimé_h, ift_Culture_NonMillesimé_f, ift_Culture_NonMillesimé_i, ift_Culture_NonMillesimé_ts, ift_Culture_NonMillesimé_a, ift_Culture_NonMillesimé_hh prennent la valeur 0, et l’IFT_Culture calculé alimente l’IFT_Culture_Biocontrôle, (comme pour le calcul de l’IFT « à l’ancienne »).
 */
public class IndicatorRefMaxYearCropIFT extends IndicatorRefMaxYearTargetIFT {

    protected static final String[] FIELDS = new String[]{
            "ift_a_la_culture_non_mil_ift_chimique_total",            // 0
            "ift_a_la_culture_non_mil_ift_chimique_tot_hts",          // 1
            "ift_a_la_culture_non_mil_ift_h",                         // 2
            "ift_a_la_culture_non_mil_ift_f",                         // 3
            "ift_a_la_culture_non_mil_ift_i",                         // 4
            "ift_a_la_culture_non_mil_ift_ts",                        // 5
            "ift_a_la_culture_non_mil_ift_a",                         // 6
            "ift_a_la_culture_non_mil_ift_hh",                        // 7
            "ift_a_la_culture_non_mil_ift_biocontrole",               // 8
    };

    protected static final String[] LABELS = new String[]{
            "Indicator.label.ift_a_la_culture_non_mil_ift_chimique_total",            // 0 IFT phytosanitaire chimique (ex total, refs #10576)
            "Indicator.label.ift_a_la_culture_non_mil_ift_chimique_tot_hts",          // 1 IFT phytosanitaire chimique total hors traitement de semence
            "Indicator.label.ift_a_la_culture_non_mil_ift_h",                         // 2 IFT herbicide
            "Indicator.label.ift_a_la_culture_non_mil_ift_f",                         // 3 IFT fongicide
            "Indicator.label.ift_a_la_culture_non_mil_ift_i",                         // 4 IFT insecticide
            "Indicator.label.ift_a_la_culture_non_mil_ift_ts",                        // 5 IFT traitement de semence
            "Indicator.label.ift_a_la_culture_non_mil_ift_a",                         // 6 IFT phytosanitaire autres
            "Indicator.label.ift_a_la_culture_non_mil_ift_hh",                        // 7 IFT phytosanitaire hors herbicide
            "Indicator.label.ift_a_la_culture_non_mil_ift_biocontrole",               // 8 IFT vert
    };
    public static final String DOSE_UI_LIBELLE = "Dose pour '(%s-%s):%s %s'";
    public static final String N_A = "N/A";

    /**
     * Constructeur initialisant la map des champs optionnels
     */
    public IndicatorRefMaxYearCropIFT() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, "ift_a_la_culture_non_mil_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, "ift_a_la_culture_non_mil_detail_champs_non_renseig");
        extraFields.put(OptionalExtraColumnEnumKey.DOSE_REFERENCE, "ift_a_la_culture_non_mil_dose_reference");
        extraFields.put(OptionalExtraColumnEnumKey.DOSE_REFERENCE_UNIT, "ift_a_la_culture_non_mil_dose_reference_unite");
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_CROP_IFT);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {

        HashMap<String, String> indicatorNameToColumnName = new HashMap<>();
        for (Map.Entry<OptionalExtraColumnEnumKey, String> extraField : extraFields.entrySet()) {
            indicatorNameToColumnName.put(indicatorName + "_" + extraField.getKey(), extraField.getValue());
        }

        for (int i = 0; i < LABELS.length; i++) {
            String indicatorName = getIndicatorLabel(i);
            String columnName = FIELDS[i];
            indicatorNameToColumnName.put(indicatorName, columnName);
        }
        return indicatorNameToColumnName;
    }

    @Override
    protected Optional<RefMAADosesRefParGroupeCible> getRefMAADosesRefParGroupeCible(
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
            AbstractPhytoProductInputUsage phytoProductInputUsage,
            int maxCampaign) {

        Optional<RefMAADosesRefParGroupeCible> refMAADosesRefParGroupeCible = getRefMAADosesRefForTraitment(
                doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
                maxCampaign);

        return refMAADosesRefParGroupeCible;
    }

    protected Optional<RefMAADosesRefParGroupeCible> getMaaDoseRefForNoTargets(int refDoseCampaign, List<RefMAADosesRefParGroupeCible> doseRefs) {
        return Optional.empty();
    }

    protected Optional<RefMAADosesRefParGroupeCible> getRefMAADosesRefForTraitment(
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
            Integer maxCampaign) {

        if (CollectionUtils.isEmpty(doseRefsFor_CodeAMM_CultureMAA_TraitementMAA)) return Optional.empty();

        Comparator<RefMAADosesRefParGroupeCible> minComparator = Comparator.comparing(d -> Optional.ofNullable(d.getDose_ref_maa()).orElse(Double.MAX_VALUE));

        // Dans le référentiel MAA_Doses_Ref_par_GroupeCibles, Repérer les Dose_Ref correspondant au quadruplets Campagne_Max|code_AMM|CultureMAA|Traitement_MAA .
        // Retenir comme Dose_Ref la plus petite des valeurs trouvées
        // où Campagne_Max est la valeur la plus élevée du champ ‘Campagne’ de la table MAA_Doses_Ref_par_GroupeCibles
        Optional<RefMAADosesRefParGroupeCible> optionalDoseRef = doseRefsFor_CodeAMM_CultureMAA_TraitementMAA.stream()
                .filter(dose -> maxCampaign.equals(dose.getCampagne()))
                .min(minComparator);


        // Dans le référentiel MAA_Doses_Ref_par_GroupeCibles,
        // Repérer les Dose_Ref correspondant au quadruplet Campagne|code_AMM|CultureMAA|Traitement_MAA..
        // Retenir la dose de référence la plus petite comme dose de référence (Dose_REF) pour ce traitement
        if (optionalDoseRef.isEmpty()) {
            optionalDoseRef = doseRefsFor_CodeAMM_CultureMAA_TraitementMAA.stream().min(minComparator);
        }

        return optionalDoseRef;
    }

    @Override
    protected UsagePerformanceResult getIndicatorReferenceDoseDTO(
            AbstractPhytoProductInputUsage usage,
            ComputeInputIftContext context,
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA) {

        UsagePerformanceResult usagePerformanceResult;

        // pour l'indicateur non-millésimé, c'est la dose la plus récente
        int refDoseCampaign = getStudiedCampaign(context, doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);

        Optional<RefMAADosesRefParGroupeCible> optionalDoseRef = getRefMAADosesRefParGroupeCible(doseRefsFor_CodeAMM_CultureMAA_TraitementMAA, usage, refDoseCampaign);

        if (optionalDoseRef.isEmpty()) {
            // Implementation depend of the indicator, there is none on IFT base on Crop
            optionalDoseRef = getMaaDoseRefForNoTargets(
                    refDoseCampaign,
                    doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);
        }

        RefActaTraitementsProduit refInput = usage.getDomainPhytoProductInput().getRefInput();
        String referencesDosageUI = DOSE_UI_LIBELLE;
        if (optionalDoseRef.isPresent() && optionalDoseRef.get().getDose_ref_maa() != null) {

            RefMAADosesRefParGroupeCible refMAADosesRefParGroupeCible = optionalDoseRef.get();

            referencesDosageUI = String.format(
                    referencesDosageUI,
                    refInput.getNom_produit(),
                    refInput.getNom_traitement(),
                    refMAADosesRefParGroupeCible.getDose_ref_maa(),
                    RefActaDosageSPCModel.PHYTO_PRODUCT_UNIT_FORMATTER.format(refMAADosesRefParGroupeCible.getUnit_dose_ref_maa()));

            usagePerformanceResult = new UsagePerformanceResult(
                    newArray(LABELS.length, 0.0),
                    referencesDosageUI,
                    refMAADosesRefParGroupeCible.getTopiaId(),
                    refMAADosesRefParGroupeCible.getDose_ref_maa(),
                    refMAADosesRefParGroupeCible.getUnit_dose_ref_maa(),
                    false);
        } else {

            referencesDosageUI = String.format(
                    referencesDosageUI,
                    refInput.getNom_produit(),
                    refInput.getNom_traitement(),
                    N_A,
                    usage.getDomainPhytoProductInput().getUsageUnit());
            usagePerformanceResult = new UsagePerformanceResult(
                    newArray(LABELS.length, 0.0),
                    referencesDosageUI,
                    referencesDosageUI,
                    null,
                    null,
                    false);
        }

        return usagePerformanceResult;
    }
}
