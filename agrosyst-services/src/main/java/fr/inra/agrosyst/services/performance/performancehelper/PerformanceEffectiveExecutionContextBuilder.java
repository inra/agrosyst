package fr.inra.agrosyst.services.performance.performancehelper;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PotInputUnit;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnection;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbu;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.common.RefInputPriceService;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.performance.EquipmentUsageRange;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePlotExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.RefScenariosInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.TraitementProduitWithCroppingPlanEntry;
import fr.inra.agrosyst.api.services.performance.utils.PriceConverterKeysToRate;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.security.AgrosystAccessDeniedException;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.performance.CommonPerformanceService;
import fr.inra.agrosyst.services.performance.indicators.operatingexpenses.IndicatorSubstrateInputOperatingExpenses;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static fr.inra.agrosyst.services.performance.indicators.IndicatorManualWorkforceExpenses.STANDARD_MANUAL_WORK_COST_VALUE;
import static fr.inra.agrosyst.services.performance.indicators.IndicatorMechanizedWorkforceExpenses.STANDARD_MECHANIZED_WORK_COST_VALUE;

public class PerformanceEffectiveExecutionContextBuilder {

    private static final Log LOGGER = LogFactory.getLog(PerformanceEffectiveExecutionContextBuilder.class);

    protected final AnonymizeService anonymizeService;
    protected final DomainInputStockUnitService domainInputStockUnitService;
    protected final DomainService domainService;
    protected final CommonPerformanceService performanceService;
    protected final InputPriceService inputPriceService;
    protected final PricesService pricesService;
    protected final ReferentialService referentialService;
    protected final RefInputPriceService refInputPriceService;
    protected final Collection<IndicatorFilter> indicatorFilters;

    @Getter
    protected final PerformanceGlobalExecutionContext performanceGlobalExecutionContext;
    @Getter
    protected PerformanceEffectiveDomainExecutionContext domainExecutionContext;

    protected final Set<String> scenarioCodes;

    protected final Pair<Domain, Domain> domainAndAnonymizeDomain;

    protected final Collection<String> codeAmmBioControle;

    protected Map<Plot, PerformancePlotExecutionContext> plotContexts;
    protected final Map<GrowingSystem, GrowingSystem> growingSystemToAnonymizeGrowingSystems = new HashMap<>();

    protected final boolean executeAsAdmin;

    protected final PriceConverterKeysToRate priceConverterKeysToRate;

    public PerformanceEffectiveExecutionContextBuilder(
            AnonymizeService anonymizeService,
            DomainInputStockUnitService domainInputStockUnitService,
            DomainService domainService,
            CommonPerformanceService performanceService,
            InputPriceService inputPriceService,
            PricesService pricesService,
            RefInputPriceService refInputPriceService,
            ReferentialService referentialService,
            Collection<String> codeAmmBioControle,
            Pair<Domain, Domain> domainAndAnonymizeDomain,
            RefSolTextureGeppa defaultRefSolTextureGeppa,
            boolean executeAsAdmin,
            PriceConverterKeysToRate priceConverterKeysToRate,
            Set<String> scenarioCodes,
            RefLocation defaultLocation,
            RefSolProfondeurIndigo defaultSolDepth,
            Map<String, String> groupesCiblesParCode,
            Collection<Plot> plots,
            Collection<IndicatorFilter> indicatorFilters) {

        this.anonymizeService = anonymizeService;
        this.domainInputStockUnitService = domainInputStockUnitService;
        this.domainService = domainService;
        this.performanceService = performanceService;
        this.inputPriceService = inputPriceService;
        this.refInputPriceService = refInputPriceService;
        this.referentialService = referentialService;

        this.codeAmmBioControle = codeAmmBioControle;
        this.domainAndAnonymizeDomain = domainAndAnonymizeDomain;
        this.executeAsAdmin = executeAsAdmin;
        this.priceConverterKeysToRate = priceConverterKeysToRate;
        this.scenarioCodes = scenarioCodes;
        this.pricesService = pricesService;
        this.indicatorFilters = indicatorFilters;

        initPlotToContext(plots);

        performanceGlobalExecutionContext = new PerformanceGlobalExecutionContext(
                defaultRefSolTextureGeppa,
                defaultSolDepth,
                defaultLocation,
                groupesCiblesParCode
        );

    }

    protected void initPlotToContext(Collection<Plot> plots) {
        plotContexts = new HashMap<>();
        for (Plot plot : plots) {
            PerformancePlotExecutionContext plotExecutionContext = new PerformancePlotExecutionContext(plot);
            if (executeAsAdmin) {
                plotExecutionContext.setAnonymizePlot(plot);
                plotContexts.put(plot, plotExecutionContext);
            } else {
                try {
                    Plot anonymizedPlot = anonymizeService.checkForPlotAnonymization(plot);
                    plotExecutionContext.setAnonymizePlot(anonymizedPlot);
                    plotContexts.put(plot, plotExecutionContext);
                } catch (AgrosystAccessDeniedException e) {
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn(e);
                    }
                }
            }
        }
    }

    protected Map<RefMateriel, RefAgsAmortissement> getDeprecationRateByEquipments(List<ToolsCoupling> toolsCouplings) {
        return performanceService.getDeprecationRateByEquipments(toolsCouplings);
    }

    public void buildPerformanceExecutionContext(Collection<Zone> zones) {

        long chronoT0 = System.currentTimeMillis();

        Domain domain = domainAndAnonymizeDomain.getLeft();

        List<ToolsCoupling> toolsCouplings = domainService.getToolsCouplingsForAllCampaignsDomain(domain);

        Map<RefMateriel, RefAgsAmortissement> deprecationRateByEquipments = getDeprecationRateByEquipments(toolsCouplings);

        Collection<EquipmentUsageRange> equipmentUsageRanges = performanceService.getEquipmentUsageRanges(toolsCouplings);

        List<CroppingPlanEntry> crops = domainService.getCroppingPlanEntriesForDomain(domain);

        final List<CroppingPlanSpecies> croppingPlanSpecies = getCroppingPlanSpecies(crops);

        Map<CroppingPlanEntry, RefCultureEdiGroupeCouvSol> speciesMaxCouvSolForCrop = new HashMap<>();

        MultiValuedMap<String, RefHarvestingPrice> refScenarioHarvestingPricesByValorisationKey =
                performanceService.getRefHarvestingPricesByValorisationKeyForDomain(domain, scenarioCodes);

        Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStock = domainInputStockUnitService.loadDomainInputStock(domain);

        final Collection<AbstractDomainInputStockUnit> fuelInputs = CollectionUtils.emptyIfNull(domainInputStock.get(InputType.CARBURANT));
        double fuelPricePerLiter = fuelInputs.stream()
                .map(AbstractDomainInputStockUnit::getInputPrice)
                .filter(Objects::nonNull)
                .filter(price -> InputPriceCategory.FUEL_INPUT.equals(price.getCategory()) && price.getPrice() != null)
                .map(InputPrice::getPrice)
                .sorted().findFirst()
                .orElse(getDomainPricePerLiter(domain));

        final Collection<AbstractDomainInputStockUnit> manualWorkforceInputs = CollectionUtils.emptyIfNull(domainInputStock.get(InputType.MAIN_OEUVRE_MANUELLE));
        double manualWorkforceCost = manualWorkforceInputs.stream()
                .map(AbstractDomainInputStockUnit::getInputPrice)
                .filter(Objects::nonNull)
                .filter(price -> InputPriceCategory.MAIN_OEUVRE_MANUELLE_INPUT.equals(price.getCategory()) && price.getPrice() != null)
                .map(InputPrice::getPrice)
                .sorted().findFirst()
                .orElse(STANDARD_MANUAL_WORK_COST_VALUE);

        final Collection<AbstractDomainInputStockUnit> mechanizedWorkforceInputs = CollectionUtils.emptyIfNull(domainInputStock.get(InputType.MAIN_OEUVRE_TRACTORISTE));
        double mechanizedWorkforceCost = mechanizedWorkforceInputs.stream()
                .map(AbstractDomainInputStockUnit::getInputPrice)
                .filter(Objects::nonNull)
                .filter(price -> InputPriceCategory.MAIN_OEUVRE_TRACTORISTE_INPUT.equals(price.getCategory()) && price.getPrice() != null)
                .map(InputPrice::getPrice)
                .sorted().findFirst()
                .orElse(STANDARD_MECHANIZED_WORK_COST_VALUE);

        Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters = referentialService.loadAllRefInputUnitPriceUnitConverter();

        RefCampaignsInputPricesByDomainInput refInputPricesForCampaignsByInput = refInputPriceService.getRefInputPricesForCampaignByInput(
                domainInputStock,
                refInputUnitPriceUnitConverters,
                domain.getCampaign());

        RefScenariosInputPricesByDomainInput refScenariosInputPricesByDomainInput = refInputPriceService.getRefInputPricesForScenariosByInput(
                domainInputStock,
                refInputUnitPriceUnitConverters,
                this.scenarioCodes);

        Optional<RefPrixCarbu> refFuelPrice = refInputPriceService.getFuelRefPriceForCampaign(domain.getCampaign());
        List<HarvestingPrice> harvestingPrices = pricesService.loadHarvestingPrices(domain.getTopiaId());

        long t0 = System.currentTimeMillis();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Chargement des Code Culture Maa par espèce pour le domain %s (%d) réalisé: %d", domain.getName(), domain.getCampaign(), (System.currentTimeMillis() - t0)));
        }

        boolean isQsaIndicators = false;
        List<String> qsaIndicatorClassNames = performanceService.getQSAIndicatorClassNames();
        if (indicatorFilters.stream().map(IndicatorFilter::getClazz).anyMatch(qsaIndicatorClassNames::contains)) {
            isQsaIndicators = true;
        }

        final Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> allDomainSubstancesByAmm = new HashMap<>();
        final MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode;
        final MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM;

        if (isQsaIndicators) {
            List<RefCompositionSubstancesActivesParNumeroAMM> domainRefCompositionSubstancesActivesParNumeroAMM = referentialService.getDomainRefCompositionSubstancesActivesParNumeroAMM(domain);

            domainRefCompositionSubstancesActivesParNumeroAMM.forEach(csa -> {
                List<RefCompositionSubstancesActivesParNumeroAMM> refCompositionSubstancesActivesParNumeroAMMS = allDomainSubstancesByAmm.computeIfAbsent(csa.getNumero_AMM(), k -> new ArrayList<>());
                refCompositionSubstancesActivesParNumeroAMMS.add(csa);
            });

            allSubstancesActivesCommissionEuropeenneByAmmCode = referentialService.getSubstancesActivesCommissionEuropeenneByAmmCodeForDomain(domain);
            refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM = this.referentialService.getRefPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM(domain);
        } else {
            allSubstancesActivesCommissionEuropeenneByAmmCode = new HashSetValuedHashMap<>();
            refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM = new HashSetValuedHashMap<>();
        }

        domainExecutionContext = new PerformanceEffectiveDomainExecutionContext(
                domainAndAnonymizeDomain,
                toolsCouplings,
                deprecationRateByEquipments,
                equipmentUsageRanges,
                crops,
                croppingPlanSpecies,
                speciesMaxCouvSolForCrop,
                refInputPricesForCampaignsByInput,
                refScenariosInputPricesByDomainInput,
                refScenarioHarvestingPricesByValorisationKey,
                fuelPricePerLiter,
                manualWorkforceCost,
                mechanizedWorkforceCost,
                codeAmmBioControle,
                refFuelPrice,
                harvestingPrices,
                allDomainSubstancesByAmm,
                allSubstancesActivesCommissionEuropeenneByAmmCode,
                refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM);

        addSeedSpeciesUnitPriceUnitConverterToGlobalPerformance();
        addPhytoProductUnitPriceUnitConverterToGlobalPerformance(domainExecutionContext);
        addMineralProductUnitPriceUnitConverterToGlobalPerformance(domainExecutionContext);
        addOrganicProductUnitPriceUnitConverterToGlobalPerformance(domainExecutionContext);
        addSubstrateUnitPriceUnitConverterToGlobalPerformance(domainExecutionContext);
        addPotUnitPriceUnitConverterToGlobalPerformance(domainExecutionContext);

        buildZoneExecutionContexts(zones, domainExecutionContext);

        zones.clear();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format(this.getClass().getSimpleName() + ": for domain with id : '%s' elapse time for buildPerformanceExecutionContext() :%d ms", domain.getTopiaId(), System.currentTimeMillis() - chronoT0));
        }

    }

    protected double getDomainPricePerLiter(Domain domain) {

        double fuelPricePerLiter = 0d;

        Optional<RefPrixCarbu> optionalRefPrixCarbu = refInputPriceService.getFuelRefPriceForCampaign(domain.getCampaign());

        if (optionalRefPrixCarbu != null && optionalRefPrixCarbu.isPresent() && optionalRefPrixCarbu.get().getPrice() != null) {
            fuelPricePerLiter = optionalRefPrixCarbu.get().getPrice();
        }

        return fuelPricePerLiter;
    }

    protected List<CroppingPlanSpecies> getCroppingPlanSpecies(List<CroppingPlanEntry> crops) {
        final List<CroppingPlanSpecies> croppingPlanSpecies = new ArrayList<>();

        for (CroppingPlanEntry croppingPlanEntry : crops) {
            final List<CroppingPlanSpecies> croppingPlanSpecies0 = croppingPlanEntry.getCroppingPlanSpecies();
            if (CollectionUtils.isNotEmpty(croppingPlanSpecies0)) {
                croppingPlanSpecies.addAll(croppingPlanSpecies0);
            }
        }
        return croppingPlanSpecies;
    }

    protected void buildZoneExecutionContexts(Collection<Zone> zones, PerformanceEffectiveDomainExecutionContext domainExecutionContext) {

        Map<Zone, EffectiveSeasonalCropCycle> effectiveSeasonalCropCyclesByPracticedSystems = performanceService.getEffectiveSeasonalCropCyclesByZones(zones);
        Map<Zone, List<EffectivePerennialCropCycle>> effectivePerennialCropCyclesByZones = performanceService.getEffectivePerennialCropCyclesByZones(zones);

        for (Zone zone : zones) {

            EffectiveSeasonalCropCycle effectiveSeasonalCropCycle = effectiveSeasonalCropCyclesByPracticedSystems.get(zone);
            List<EffectivePerennialCropCycle> effectivePerennialCropCycles = effectivePerennialCropCyclesByZones.get(zone);

            Optional<PerformanceZoneExecutionContext> optionalZoneContext = buildPerformanceZoneExecutionContext(
                    zone,
                    effectiveSeasonalCropCycle,
                    effectivePerennialCropCycles,
                    domainExecutionContext);
            optionalZoneContext.ifPresent(
                    performanceZoneExecutionContext ->
                            this.domainExecutionContext.addZoneContext(performanceZoneExecutionContext));

        }

    }

    protected Optional<PerformanceZoneExecutionContext> buildPerformanceZoneExecutionContext(
            Zone zone,
            EffectiveSeasonalCropCycle seasonalCropCycle,
            List<EffectivePerennialCropCycle> perennialCropCycles,
            PerformanceEffectiveDomainExecutionContext domainExecutionContext) {

        GrowingSystem growingSystem = zone.getPlot().getGrowingSystem();// can be null
        Optional<GrowingSystem> optionalGrowingSystem = Optional.ofNullable(growingSystem);

        PerformanceGrowingSystemExecutionContext growingSystemContext = getPerformanceGrowingSystemExecutionContext(
                zone,
                optionalGrowingSystem);

        Optional<PerformancePlotExecutionContext> optionalPlotContext = getPlotContext(
                zone,
                growingSystemContext);

        PerformanceZoneExecutionContext zoneExecutionContext = null;
        if (optionalPlotContext.isPresent()) {
            PerformancePlotExecutionContext plotContext = optionalPlotContext.get();
            growingSystemContext.addPlot(plotContext);
            Zone anonymizeZone;
            if (executeAsAdmin) {
                anonymizeZone = zone;
            } else {
                try {
                    anonymizeZone = anonymizeService.checkForZoneAnonymization(zone);
                } catch (AgrosystAccessDeniedException e) {
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn(e);
                    }
                    return Optional.empty();
                }
            }
            Pair<Zone, Zone> zoneToAnonymiseZone = Pair.of(zone, anonymizeZone);

            zoneExecutionContext = new PerformanceZoneExecutionContext(
                    growingSystemContext,
                    zoneToAnonymiseZone,
                    plotContext,
                    seasonalCropCycle,
                    perennialCropCycles,
                    domainExecutionContext.getHarvestingPrices());

            Set<PerformanceEffectiveCropExecutionContext> cropContextExecutionContexts = buildPerformanceCropExecutionContexts(zoneExecutionContext);

            zoneExecutionContext.setPerformanceCropContextExecutionContexts(cropContextExecutionContexts);
        }

        return Optional.ofNullable(zoneExecutionContext);

    }

    protected PerformanceGrowingSystemExecutionContext getPerformanceGrowingSystemExecutionContext(
            Zone zone,
            Optional<GrowingSystem> optionalGrowingSystem) {

        PerformanceGrowingSystemExecutionContext growingSystemContext;

        growingSystemContext = domainExecutionContext.getGrowingSystemContextByGrowingSystems().get(optionalGrowingSystem);

        if (growingSystemContext == null) {

            Pair<Optional<GrowingSystem>, Optional<GrowingSystem>> growingSystemAndAnonymizeGrowingSystem;

            if (optionalGrowingSystem.isPresent()) {
                Optional<Pair<GrowingSystem, GrowingSystem>> optionalGs = getGrowingSystemAnonymizeGrowingSystem(zone);
                growingSystemAndAnonymizeGrowingSystem = optionalGs.map(growingSystemGrowingSystemPair -> Pair.of(
                        Optional.of(growingSystemGrowingSystemPair.getLeft()),
                        Optional.of(growingSystemGrowingSystemPair.getRight()))).orElseGet(() -> Pair.of(Optional.empty(), Optional.empty()));
            } else {
                growingSystemAndAnonymizeGrowingSystem = Pair.of(Optional.empty(), Optional.empty());
            }

            Set<Network> irs = null;
            Set<Network> its = null;
            if (optionalGrowingSystem.isPresent()) irs = performanceService.getIRs(optionalGrowingSystem.get());
            if (optionalGrowingSystem.isPresent()) its = performanceService.getIts(optionalGrowingSystem.get());

            growingSystemContext = new PerformanceGrowingSystemExecutionContext(
                    growingSystemAndAnonymizeGrowingSystem,
                    irs,
                    its);
        }

        domainExecutionContext.addGrowingSystemContext(growingSystemContext);

        return growingSystemContext;
    }

    protected Optional<Pair<GrowingSystem, GrowingSystem>> getGrowingSystemAnonymizeGrowingSystem(Zone zone) {
        GrowingSystem growingSystem = zone.getPlot().getGrowingSystem();
        Pair<GrowingSystem, GrowingSystem> growingSystemAndAnonymizeGrowingSystem = null;
        if (growingSystem != null) {
            GrowingSystem anonymizeGrowingSystem = growingSystemToAnonymizeGrowingSystems.get(growingSystem);
            if (anonymizeGrowingSystem == null) {
                if (executeAsAdmin) {
                    anonymizeGrowingSystem = growingSystem;
                } else {
                    try {
                        anonymizeGrowingSystem = anonymizeService.checkForGrowingSystemAnonymization(growingSystem);
                    } catch (AgrosystAccessDeniedException e) {
                        if (LOGGER.isWarnEnabled()) {
                            LOGGER.warn(e);
                        }
                        return Optional.empty();
                    }
                }
                growingSystemToAnonymizeGrowingSystems.put(growingSystem, anonymizeGrowingSystem);
            }
            growingSystemAndAnonymizeGrowingSystem = Pair.of(growingSystem, anonymizeGrowingSystem);
        }
        return Optional.ofNullable(growingSystemAndAnonymizeGrowingSystem);
    }

    protected Optional<PerformancePlotExecutionContext> getPlotContext(Zone zone, PerformanceGrowingSystemExecutionContext growingSystemContext) {

        final Plot plot = zone.getPlot();
        PerformancePlotExecutionContext plotContext = plotContexts.get(plot);
        Plot anonymizePlot = plotContext.getAnonymizePlot();
        if (anonymizePlot == null) {
            if (executeAsAdmin) {
                anonymizePlot = plot;
            } else {
                try {
                    anonymizePlot = anonymizeService.checkForPlotAnonymization(plot);
                } catch (AgrosystAccessDeniedException e) {
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn(e);
                    }
                    plotContexts.remove(plot);
                    return Optional.empty();
                }
            }

            plotContext.setAnonymizePlot(anonymizePlot);
        }

        if (growingSystemContext.getGrowingSystem().isPresent()) {
            plotContext.setIrs(growingSystemContext.getIrs());
            plotContext.setIts(growingSystemContext.getIts());
        }

        return Optional.of(plotContext);
    }

    protected Set<PerformanceEffectiveCropExecutionContext> buildPerformanceCropExecutionContexts(PerformanceZoneExecutionContext zoneContext) {

        Set<PerformanceEffectiveCropExecutionContext> cropContexts = new HashSet<>();

        EffectiveSeasonalCropCycle seasonalCropCycle = zoneContext.getSeasonalCropCycle();
        List<EffectivePerennialCropCycle> perennialCropCycles = zoneContext.getPerennialCropCycles();

        if (seasonalCropCycle == null && CollectionUtils.isEmpty(perennialCropCycles)) {
            return cropContexts;// noting to do
        }

        cropContexts.addAll(buildPerformanceSeasonalCropExecutionContexts(zoneContext));

        cropContexts.addAll(buildPerformancePerennialCropContextExecutionContexts(zoneContext));

        return cropContexts;
    }

    protected List<PerformanceEffectiveCropExecutionContext> buildPerformancePerennialCropContextExecutionContexts(
            PerformanceZoneExecutionContext zoneContext) {

        List<PerformanceEffectiveCropExecutionContext> cropContexts = new ArrayList<>();
        List<EffectivePerennialCropCycle> perennialCropCycles = zoneContext.getPerennialCropCycles();

        if (CollectionUtils.isNotEmpty(perennialCropCycles)) {

            List<EffectiveIntervention> cropInterventions = performanceService.getEffectiveInterventionsForEffectivePerennialCropCycles(perennialCropCycles);

            List<AbstractAction> cropActions = performanceService.getEffectiveActionsForCropInterventions(cropInterventions);

            for (EffectivePerennialCropCycle perennialCropCycle : perennialCropCycles) {

                EffectiveCropCyclePhase phase = perennialCropCycle.getPhase();
                List<EffectiveIntervention> cycleInterventions = cropInterventions.stream()
                        .filter(practicedIntervention -> phase.equals(practicedIntervention.getEffectiveCropCyclePhase()))
                        .collect(Collectors.toList());

                if (CollectionUtils.isEmpty(cycleInterventions)) continue;// NOTING TO DO

                final CroppingPlanEntry crop = perennialCropCycle.getCroppingPlanEntry();

                Set<RefEspece> refEspeces = getCropRefEspeces(crop);

                WeedType weedType = perennialCropCycle.getWeedType();
                RefCultureEdiGroupeCouvSol speciesMaxCouvSolForCrop = domainExecutionContext.getSpeciesMaxCouvSolForCrops().get(crop);

                PerformanceEffectiveCropExecutionContext cropContext = new PerformanceEffectiveCropExecutionContext(
                        crop,
                        perennialCropCycle,
                        refEspeces,
                        weedType,
                        speciesMaxCouvSolForCrop);

                buildAndAddInterventionsContextToCropContext(
                        zoneContext,
                        cropActions,
                        cycleInterventions,
                        cropContext);

                cropContexts.add(cropContext);

                zoneContext.addCropYealdAverage(getAndSetZoneCropYealdAverage(cropContext));
            }

        }

        return cropContexts;
    }

    protected void setCropYealdAverage(PerformanceEffectiveCropExecutionContext cropContext) {

        Map<Pair<RefDestination, YealdUnit>, List<Double>> interventionsYealds = cropContext.getInterventionsYealds();
        Map<Pair<RefDestination, YealdUnit>, Double> cropYealds = performanceService.computeYealdAverageByDestinationAndUnit(interventionsYealds);
        cropContext.setMainCropYealds(cropYealds);

        Map<Pair<RefDestination, YealdUnit>, List<Double>> intermediateInterventionsYealds = cropContext.getIntermediateInterventionsYealds();
        Map<Pair<RefDestination, YealdUnit>, Double> intermediateCropYealds = performanceService.computeYealdAverageByDestinationAndUnit(intermediateInterventionsYealds);
        cropContext.setIntermediateCropYealds(intermediateCropYealds);

    }

    protected Map<Pair<RefDestination, YealdUnit>, Double> getAndSetZoneCropYealdAverage(PerformanceEffectiveCropExecutionContext cropContext) {
        Map<Pair<RefDestination, YealdUnit>, List<Double>> allInterventionsYields = new HashMap<>(cropContext.getInterventionsYealds());

        /*
         * La destination pouvant être la même entre rendements des cultures principales et intermédiaires, il peut
         * y avoir des clés en commun. Il faut donc mettre à jour la map correctement, en fusionnant les valeurs
         * pour les mêmes clés.
         */
        Map<Pair<RefDestination, YealdUnit>, List<Double>> intermediateYields = cropContext.getIntermediateInterventionsYealds();
        for (Pair<RefDestination, YealdUnit> yieldDestination : intermediateYields.keySet()) {
            allInterventionsYields.merge(yieldDestination, intermediateYields.get(yieldDestination), (currentYields, yieldsToAdd) -> {
                currentYields.addAll(yieldsToAdd);
                return currentYields;
            });
        }

        Map<Pair<RefDestination, YealdUnit>, Double> cropYealds = performanceService.computeYealdAverageByDestinationAndUnit(allInterventionsYields);

        return cropYealds;
    }

    protected List<PerformanceEffectiveCropExecutionContext> buildPerformanceSeasonalCropExecutionContexts(PerformanceZoneExecutionContext zoneContext) {

        List<PerformanceEffectiveCropExecutionContext> cropExecutionContexts = new ArrayList<>();

        EffectiveSeasonalCropCycle seasonalCropCycle = zoneContext.getSeasonalCropCycle();

        if (seasonalCropCycle != null) {

            Collection<EffectiveCropCycleNode> cycleNodes = seasonalCropCycle.getNodes();

            if (CollectionUtils.isEmpty(cycleNodes)) {
                return cropExecutionContexts;// nothing to do
            }

            cycleNodes = cycleNodes.stream().sorted(Comparator.comparing(EffectiveCropCycleNode::getRank)).collect(Collectors.toList());

            List<EffectiveIntervention> cropInterventions = performanceService.getEffectiveInterventionForNodes(cycleNodes);

            if (CollectionUtils.isEmpty(cropInterventions)) {
                return cropExecutionContexts;// nothing to do
            }

            Set<EffectiveCropCycleNode> nodeWithIntermediateInterventions = new HashSet<>();
            for (EffectiveIntervention cropIntervention : cropInterventions) {
                if (cropIntervention.isIntermediateCrop()) {
                    nodeWithIntermediateInterventions.add(cropIntervention.getEffectiveCropCycleNode());
                }
            }

            Map<EffectiveCropCycleNode, EffectiveCropCycleConnection> intermediateConnectionForNodes = new HashMap<>();
            if (!nodeWithIntermediateInterventions.isEmpty()) {

                List<EffectiveCropCycleConnection> connectionsWithIntermediateCrop = performanceService
                        .getEffectiveConnectionsForNodes(nodeWithIntermediateInterventions);

                for (EffectiveCropCycleConnection connection : connectionsWithIntermediateCrop) {
                    intermediateConnectionForNodes.put(connection.getTarget(), connection);
                }
            }

            List<AbstractAction> cycleActions = performanceService.getEffectiveActionsForCropInterventions(cropInterventions);

            EffectiveCropCycleNode previousNode = null;
            for (EffectiveCropCycleNode node : cycleNodes) {

                List<EffectiveIntervention> nodeInterventions = cropInterventions.stream()
                        .filter(effectiveIntervention -> node.equals(effectiveIntervention.getEffectiveCropCycleNode()))
                        .collect(Collectors.toList());

                if (nodeInterventions.isEmpty()) {
                    previousNode = node;
                    continue;// noting to do
                }

                final CroppingPlanEntry crop = node.getCroppingPlanEntry();
                CroppingPlanEntry intermediateCrop = null;
                final EffectiveCropCycleConnection intermediateConnection = intermediateConnectionForNodes.get(node);
                if (intermediateConnection != null) {
                    intermediateCrop = intermediateConnection.getIntermediateCroppingPlanEntry();
                }

                CroppingPlanEntry previousCrop = previousNode != null ? previousNode.getCroppingPlanEntry() : null;

                Set<RefEspece> refEspeces = getConnectionRefEspeces(crop, intermediateCrop);

                RefCultureEdiGroupeCouvSol speciesMaxCouvSolForCrop = domainExecutionContext.getSpeciesMaxCouvSolForCrops().get(crop);

                RefCultureEdiGroupeCouvSol intermediateSpeciesMaxCouvSolForCrop = null;
                if (intermediateCrop != null) {
                    intermediateSpeciesMaxCouvSolForCrop = domainExecutionContext.getSpeciesMaxCouvSolForCrops().get(intermediateCrop);
                }

                PerformanceEffectiveCropExecutionContext cropContext = new PerformanceEffectiveCropExecutionContext(
                        crop,
                        intermediateCrop,
                        previousCrop,
                        node.getRank(),
                        refEspeces,
                        speciesMaxCouvSolForCrop,
                        intermediateSpeciesMaxCouvSolForCrop);

                buildAndAddInterventionsContextToCropContext(
                        zoneContext,
                        cycleActions,
                        nodeInterventions,
                        cropContext);

                cropExecutionContexts.add(cropContext);

                zoneContext.addCropYealdAverage(getAndSetZoneCropYealdAverage(cropContext));

                previousNode = node;
            }

        }

        return cropExecutionContexts;
    }

    private void buildAndAddInterventionsContextToCropContext(PerformanceZoneExecutionContext zoneContext,
                                                              List<AbstractAction> cycleActions,
                                                              List<EffectiveIntervention> nodeInterventions,
                                                              PerformanceEffectiveCropExecutionContext cropContext) {

        Set<PerformanceEffectiveInterventionExecutionContext> performanceInterventionExecutionContext =
                buildPerformanceEffectiveInterventionExecutionContexts(
                        zoneContext,
                        nodeInterventions,
                        cycleActions,
                        cropContext
                );

        cropContext.setInterventionExecutionContexts(performanceInterventionExecutionContext);

        setCropYealdAverage(cropContext);
    }

    protected Set<RefEspece> getConnectionRefEspeces(CroppingPlanEntry crop, CroppingPlanEntry intermediateCrop) {

        Set<RefEspece> connectionSpecies = getCropRefEspeces(crop);

        if (intermediateCrop != null) {
            connectionSpecies.addAll(getCropRefEspeces(intermediateCrop));
        }

        return connectionSpecies;
    }

    protected Set<RefEspece> getCropRefEspeces(CroppingPlanEntry crop) {
        Set<RefEspece> connectionSpecies = new HashSet<>();
        List<CroppingPlanSpecies> species = crop.getCroppingPlanSpecies();
        if (species != null) {
            for (CroppingPlanSpecies croppingPlanSpecies : species) {
                connectionSpecies.add(croppingPlanSpecies.getSpecies());
            }
        }
        return connectionSpecies;
    }

    protected String getOutputInterventionYealdAverage(Map<Pair<RefDestination, YealdUnit>, Double> yealdAveragesByDestinations) {
        List<String> outList = new ArrayList<>();
        for (Map.Entry<Pair<RefDestination, YealdUnit>, Double> yealdAveragesForDestinationUnit : yealdAveragesByDestinations.entrySet()) {
            Pair<RefDestination, YealdUnit> destinationYealdUnit = yealdAveragesForDestinationUnit.getKey();

            String destination = destinationYealdUnit.getLeft().getDestination();
            String yealdUnitTrad = AgrosystI18nService.getEnumTraductionWithDefaultLocale(destinationYealdUnit.getRight());

            double averageYeald = yealdAveragesForDestinationUnit.getValue();

            outList.add(averageYeald + " " + " (" + destination + "/" + yealdUnitTrad + ")");
        }

        return String.join(", ", outList);

    }

    protected Set<PerformanceEffectiveInterventionExecutionContext> buildPerformanceEffectiveInterventionExecutionContexts(
            PerformanceZoneExecutionContext zoneContext,
            List<EffectiveIntervention> interventions,
            List<AbstractAction> cycleActions,
            PerformanceEffectiveCropExecutionContext cropContext) {

        Set<PerformanceEffectiveInterventionExecutionContext> result = new HashSet<>();

        Domain domain = domainExecutionContext.getDomain();

        List<HarvestingPrice> harvestingPrices = zoneContext.getHarvestingPrices();

        for (EffectiveIntervention intervention : interventions) {
            long start = System.currentTimeMillis();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("DEBUT CHARGEMENT DONNÉED POUR INTERVENTION '" + intervention.getName() + "'");
            }

            CroppingPlanEntry interventionCrop = intervention.isIntermediateCrop() ? cropContext.getIntermediateCrop() : cropContext.getCrop();

            if (interventionCrop == null) {
                if (LOGGER.isWarnEnabled()) {
                    String zoneId = zoneContext.getZone().getTopiaId();
                    String domainCode = domain.getCode();

                    LOGGER.warn(
                            String.format("For zone %s and intervention %s the crop could not be find into domain code %s",
                                    zoneId,
                                    intervention.getTopiaId(),
                                    domainCode)
                    );
                }
                continue;// not normal
            }

            ToolsCoupling toolsCoupling = getInterventionContextToolsCoupling(intervention);

            // filter on intervention actions and inputs
            List<AbstractAction> interventionActions = cycleActions.stream()
                    .filter(action -> intervention.equals(action.getEffectiveIntervention())).toList();

            MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode = domainExecutionContext.getAllSubstancesActivesCommissionEuropeenneByAmmCode();

            PerformanceEffectiveInterventionExecutionContext interventionContext = new PerformanceEffectiveInterventionExecutionContext(
                    intervention,
                    domain.getCampaign(),
                    toolsCoupling,
                    interventionActions,
                    interventionCrop,
                    priceConverterKeysToRate,
                    harvestingPrices,
                    allSubstancesActivesCommissionEuropeenneByAmmCode);

            result.add(interventionContext);

            Optional<HarvestingAction> optionalHarvestingAction = interventionContext.getOptionalHarvestingAction();

            Map<Pair<RefDestination, YealdUnit>, Double> yealdAveragesByDestinations = performanceService.extractYealdAverage(optionalHarvestingAction, interventionCrop);
            interventionContext.setYealdAveragesByDestinations(yealdAveragesByDestinations);
            interventionContext.setInterventionYealdAveragesTrad(getOutputInterventionYealdAverage(yealdAveragesByDestinations));

            if (intervention.isIntermediateCrop()) {
                cropContext.addIntermediateInterventionYealdAverage(yealdAveragesByDestinations);
            } else {
                cropContext.addInterventionYealdAverage(yealdAveragesByDestinations);
            }

            Set<RefActaTraitementsProduit> actaTraitementProducts = getInterventionInputUsageRefActaTraitementsProduits(interventionContext);
            interventionContext.setRefActaTraitementsProduits(actaTraitementProducts);

            addInterventionContextSpecies(interventionContext);

            addInterventionContextRefActaDosageSPCForPhytoInputs(cropContext, interventionContext);

            addInterventionContextActiveSubstancesByProductsForPhytoProduct(interventionContext);

            addInterventionContextRefActaTraitementsProduits(interventionContext);

            interventionContext.setFuelPricePerLiter(domainExecutionContext.getFuelPricePerLiter());
            interventionContext.setManualWorkforceCost(domainExecutionContext.getManualWorkforceCost());
            interventionContext.setMechanizedWorkforceCost(domainExecutionContext.getMechanizedWorkforceCost());

            interventionContext.setRefEspeces(cropContext.getRefEspeces());

            if (LOGGER.isDebugEnabled()) {
                long end = System.currentTimeMillis();
                LOGGER.debug("FIN CHARGEMENT DONNÉES POUR INTERVENTION '" + intervention.getName() + "' réalisé en :" + ((end - start) / 1000) + " ms");
            }
        }

        var materiels = result.stream()
                .filter(ic -> ic.getToolsCoupling() != null)
                .filter(ic -> !ic.getToolsCoupling().getEquipments().isEmpty())
                .flatMap(ic -> ic.getToolsCoupling().getEquipments().stream()
                        .map(Equipment::getRefMateriel))
                .collect(Collectors.toSet());
        var correspondances = referentialService.findRefCorrespondanceMaterielOutilsTSForTools(materiels);
        result.forEach(ic -> ic.setCorrespondanceByRefMateriel(correspondances));

        return result;
    }

    protected void addInterventionContextRefActaTraitementsProduits(PerformanceEffectiveInterventionExecutionContext interventionContext) {

        Set<RefActaTraitementsProduit> refActaTraitementsProduits = getInterventionInputUsageRefActaTraitementsProduits(interventionContext);
        interventionContext.setRefActaTraitementsProduits(refActaTraitementsProduits);

        final List<DomainPhytoProductInput> domainPhytoProductInputs = new ArrayList<>();
        if (interventionContext.getOptionalSeedingActionUsage().isPresent()) {
            domainPhytoProductInputs.addAll(interventionContext.getOptionalSeedingActionUsage().get().getSeedLotInputUsage()
                    .stream()
                    .map(SeedLotInputUsage::getSeedingSpecies)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .map(SeedSpeciesInputUsage::getSeedProductInputUsages)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .map(SeedProductInputUsage::getDomainPhytoProductInput)
                    .filter(Objects::nonNull)
                    .toList());

        }

        if (interventionContext.getOptionalBiologicalControlAction().isPresent() && CollectionUtils.isNotEmpty(interventionContext.getOptionalBiologicalControlAction().get().getBiologicalProductInputUsages())) {
            final List<DomainPhytoProductInput> phytoProductInputs = interventionContext.getOptionalBiologicalControlAction().get().getBiologicalProductInputUsages()
                    .stream()
                    .map(BiologicalProductInputUsage::getDomainPhytoProductInput)
                    .toList();
            domainPhytoProductInputs.addAll(phytoProductInputs);
        }
        if (interventionContext.getOptionalPesticidesSpreadingAction().isPresent() && CollectionUtils.isNotEmpty(interventionContext.getOptionalPesticidesSpreadingAction().get().getPesticideProductInputUsages())) {
            final List<DomainPhytoProductInput> phytoProductInputs = interventionContext.getOptionalPesticidesSpreadingAction().get().getPesticideProductInputUsages()
                    .stream()
                    .map(PesticideProductInputUsage::getDomainPhytoProductInput)
                    .toList();
            domainPhytoProductInputs.addAll(phytoProductInputs);
        }

        refActaTraitementsProduits.addAll(domainPhytoProductInputs.stream().map(DomainPhytoProductInput::getRefInput).toList());

        if (CollectionUtils.isNotEmpty(refActaTraitementsProduits)) {
            Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> result = performanceService.getRefActaTraitementsProduitsCategFor(refActaTraitementsProduits);
            interventionContext.addTraitementProduitCategByIdTraitements(result);
        }
    }

    protected void addOrganicProductUnitPriceUnitConverterToGlobalPerformance(PerformanceEffectiveDomainExecutionContext context) {

        final Set<AbstractDomainInputStockUnit> inputs = context.getRefInputPricesForCampaignsByInput().fertiOrgaRefPriceForInput().keySet();

        Set<OrganicProductUnit> organicProductUnits = CollectionUtils.emptyIfNull(inputs)
                .stream()
                .map(input -> ((DomainOrganicProductInput) input).getUsageUnit()).collect(Collectors.toSet());

        Map<OrganicProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByOrganicProductUnit =
                performanceGlobalExecutionContext.getConvertersByOrganicProductUnit();

        Set<OrganicProductUnit> organicProductUnitsWithConverters = convertersByOrganicProductUnit.keySet();
        organicProductUnits.removeAll(organicProductUnitsWithConverters);

        if (!organicProductUnits.isEmpty()) {
            List<RefInputUnitPriceUnitConverter> allConverters = performanceService.getPriceUnitConvertersForOrganicProductUnit(organicProductUnits);

            for (RefInputUnitPriceUnitConverter converter : allConverters) {
                OrganicProductUnit organicProductUnit = converter.getOrganicProductUnit();
                List<RefInputUnitPriceUnitConverter> converters = convertersByOrganicProductUnit.computeIfAbsent(organicProductUnit, k -> new ArrayList<>());
                converters.add(converter);
            }
        }

    }

    protected void addSubstrateUnitPriceUnitConverterToGlobalPerformance(PerformanceEffectiveDomainExecutionContext context) {

        final Set<AbstractDomainInputStockUnit> inputs = context.getRefInputPricesForCampaignsByInput().substrateRefPriceForInput().keySet();

        Collection<DomainSubstrateInput> substrateInputs = new ArrayList<>();
        inputs.forEach(is -> substrateInputs.add((DomainSubstrateInput) is));

        Set<SubstrateInputUnit> substrateInputUnits = IndicatorSubstrateInputOperatingExpenses.getSubstrateInputUnits(substrateInputs);

        Map<SubstrateInputUnit, List<RefInputUnitPriceUnitConverter>> convertersBySubstrateUnit =
                performanceGlobalExecutionContext.getConvertersBySubstrateUnit();

        Set<SubstrateInputUnit> substrateUnitsWithConverters = convertersBySubstrateUnit.keySet();
        substrateInputUnits.removeAll(substrateUnitsWithConverters);

        if (!substrateInputUnits.isEmpty()) {
            List<RefInputUnitPriceUnitConverter> allConverters = performanceService.getPriceUnitConvertersForSubstrateUnit(substrateInputUnits);

            for (RefInputUnitPriceUnitConverter converter : allConverters) {
                SubstrateInputUnit substrateInputUnit = converter.getSubstrateInputUnit();
                List<RefInputUnitPriceUnitConverter> converters = convertersBySubstrateUnit.computeIfAbsent(substrateInputUnit, k -> new ArrayList<>());
                converters.add(converter);
            }
        }
    }

    protected void addPotUnitPriceUnitConverterToGlobalPerformance(PerformanceEffectiveDomainExecutionContext context) {
        final Set<AbstractDomainInputStockUnit> inputs = context.getRefInputPricesForCampaignsByInput().potRefPriceForInput().keySet();

        Set<PotInputUnit> potInputUnits = CollectionUtils.emptyIfNull(inputs)
                .stream()
                .map(input -> ((DomainPotInput) input).getUsageUnit()).collect(Collectors.toSet());

        Map<PotInputUnit, List<RefInputUnitPriceUnitConverter>> convertersByPotUnit = performanceGlobalExecutionContext.getConvertersByPotUnit();
        Set<PotInputUnit> potUnitsWithConverters = convertersByPotUnit.keySet();
        potInputUnits.removeAll(potUnitsWithConverters);

        if (!potInputUnits.isEmpty()) {
            List<RefInputUnitPriceUnitConverter> allConverters = performanceService.getPriceUnitConvertersForPotUnit(potInputUnits);

            for (RefInputUnitPriceUnitConverter converter : allConverters) {
                PotInputUnit potInputUnit = converter.getPotInputUnit();
                List<RefInputUnitPriceUnitConverter> converters = convertersByPotUnit.computeIfAbsent(potInputUnit, k -> new ArrayList<>());
                converters.add(converter);
            }
        }
    }

    protected void addSeedSpeciesUnitPriceUnitConverterToGlobalPerformance() {

        Set<SeedPlantUnit> seedPlantUnits = Sets.newHashSet(SeedPlantUnit.values());

        Map<SeedPlantUnit, List<RefInputUnitPriceUnitConverter>> globalConvertersByPhytoProductUnit = performanceGlobalExecutionContext.getConvertersBySeedingProductUnit();
        Set<SeedPlantUnit> seedPlantUnitsWithConverters = globalConvertersByPhytoProductUnit.keySet();
        seedPlantUnits.removeAll(seedPlantUnitsWithConverters);

        if (!seedPlantUnits.isEmpty()) {
            List<RefInputUnitPriceUnitConverter> allConverters = performanceService.getPriceUnitConvertersForSeedPlantUnit(seedPlantUnits);

            for (RefInputUnitPriceUnitConverter converter : allConverters) {
                SeedPlantUnit phytoProductUnit = converter.getSeedPlantUnit();
                List<RefInputUnitPriceUnitConverter> converters = globalConvertersByPhytoProductUnit.computeIfAbsent(phytoProductUnit, k -> new ArrayList<>());
                converters.add(converter);
            }
        }
    }

    protected void addPhytoProductUnitPriceUnitConverterToGlobalPerformance(PerformanceEffectiveDomainExecutionContext context) {

        final Set<AbstractDomainInputStockUnit> inputs = context.getRefInputPricesForCampaignsByInput().phytoRefPriceForInput().keySet();

        Collection<AbstractDomainInputStockUnit> abstractDomainInputStockUnits = CollectionUtils.emptyIfNull(inputs);
        Set<PhytoProductUnit> phytoProductUnits;
        if (context.getRefInputPricesForCampaignsByInput().speciesRefPriceForInput() != null) {
            phytoProductUnits = context.getRefInputPricesForCampaignsByInput().speciesRefPriceForInput()
                    .keySet()
                    .stream()
                    .filter(adis -> adis instanceof DomainSeedSpeciesInput)
                    .map(adis -> ((DomainSeedSpeciesInput) adis).getSpeciesPhytoInputs())
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .map(DomainPhytoProductInput::getUsageUnit)
                    .collect(Collectors.toSet());
        } else {
            phytoProductUnits = new HashSet<>();
        }
        phytoProductUnits.addAll(abstractDomainInputStockUnits
                .stream()
                .map(input -> ((DomainPhytoProductInput) input).getUsageUnit()).collect(Collectors.toSet()));

        Map<PhytoProductUnit, List<RefInputUnitPriceUnitConverter>> globalConvertersByPhytoProductUnit =
                performanceGlobalExecutionContext.getConvertersByPhytoProductUnit();
        Set<PhytoProductUnit> phytoProductUnitsWithConverters = globalConvertersByPhytoProductUnit.keySet();
        phytoProductUnits.removeAll(phytoProductUnitsWithConverters);

        if (!phytoProductUnits.isEmpty()) {
            List<RefInputUnitPriceUnitConverter> allConverters = performanceService.getPriceUnitConvertersForPhytoProductUnits(phytoProductUnits);

            for (RefInputUnitPriceUnitConverter converter : allConverters) {
                PhytoProductUnit phytoProductUnit = converter.getPhytoProductUnit();
                List<RefInputUnitPriceUnitConverter> converters = globalConvertersByPhytoProductUnit.computeIfAbsent(phytoProductUnit, k -> new ArrayList<>());
                converters.add(converter);
            }
        }

    }

    protected void addMineralProductUnitPriceUnitConverterToGlobalPerformance(PerformanceEffectiveDomainExecutionContext domainContext) {

        Set<AbstractDomainInputStockUnit> inputs = domainContext.getRefInputPricesForCampaignsByInput().mineralRefPriceForInput().keySet();
        Set<MineralProductUnit> mineralProductUnits = CollectionUtils.emptyIfNull(inputs)
                .stream()
                .map(input -> ((DomainMineralProductInput) input).getUsageUnit()).collect(Collectors.toSet());

        Map<MineralProductUnit, List<RefInputUnitPriceUnitConverter>> globalConvertersByMineralProductUnit =
                performanceGlobalExecutionContext.getConvertersByMineralProductUnit();

        Set<MineralProductUnit> mineralProductUnitsWithConverters = globalConvertersByMineralProductUnit.keySet();
        mineralProductUnits.removeAll(mineralProductUnitsWithConverters);

        if (!mineralProductUnits.isEmpty()) {
            List<RefInputUnitPriceUnitConverter> allConverters = performanceService.getPriceUnitConvertersForMineralProductUnit(mineralProductUnits);

            for (RefInputUnitPriceUnitConverter converter : allConverters) {
                MineralProductUnit mineralProductUnit = converter.getMineralProductUnit();
                List<RefInputUnitPriceUnitConverter> converters =
                        globalConvertersByMineralProductUnit.computeIfAbsent(mineralProductUnit, k -> new ArrayList<>());
                converters.add(converter);
            }
        }

    }

    protected void addInterventionContextActiveSubstancesByProductsForPhytoProduct(PerformanceEffectiveInterventionExecutionContext interventionContext) {

        Map<String, RefActaTraitementsProduit> newProductByIds = interventionContext.getRefIdProduitToProducts();

        Map<RefActaTraitementsProduit, List<RefActaSubstanceActive>> activeSubstancesByProducts =
                performanceService.getAtciveSubstancesByProductsForPhytoProductById(newProductByIds);
        performanceGlobalExecutionContext.addActiveSubstancesByProducts(activeSubstancesByProducts);

    }

    protected void addInterventionContextRefActaDosageSPCForPhytoInputs(
            PerformanceEffectiveCropExecutionContext cropContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {

        List<CroppingPlanSpecies> species = interventionContext.getInterventionSpecies();
        if (CollectionUtils.isEmpty(species)) {
            return;
        }

        Set<RefActaTraitementsProduit> actaTraitementProducts = getInterventionInputUsageRefActaTraitementsProduits(interventionContext);

        if (CollectionUtils.isEmpty(actaTraitementProducts)) {
            return;
        }

        Set<RefEspece> refEspeces = species.stream()
                .map(CroppingPlanSpecies::getSpecies)
                .collect(Collectors.toSet());
        CroppingPlanEntry crop = interventionContext.getCroppingPlanEntry();

        if (species.size() == CollectionUtils.emptyIfNull(crop.getCroppingPlanSpecies()).size()) {
            // all species are selected so we can use the RefActaDosageSPC from crop
            Map<TraitementProduitWithCroppingPlanEntry, ReferenceDoseDTO> legacyCropDoseForProducts = cropContext.getLegacyDoseForProducts();
            if (legacyCropDoseForProducts == null) {
                // this doses does not filter on target
                Map<RefActaTraitementsProduit, ReferenceDoseDTO> legacyDoseForActaProducts =
                        performanceService.getLegacyDoseForActaProducts(actaTraitementProducts, refEspeces);

                legacyCropDoseForProducts = addCropWithSpeciesToMapKeys(legacyDoseForActaProducts, crop);
                cropContext.setLegacyDoseForProducts(legacyCropDoseForProducts);
            } else {
                Set<TraitementProduitWithCroppingPlanEntry> traitementProduitWithCroppingPlanEntry = actaTraitementProducts.stream()
                        .map(traitementsProduit -> new TraitementProduitWithCroppingPlanEntry(traitementsProduit, crop))
                        .collect(Collectors.toSet());
                traitementProduitWithCroppingPlanEntry.removeAll(legacyCropDoseForProducts.keySet());
                if (!traitementProduitWithCroppingPlanEntry.isEmpty()) {

                    Set<RefActaTraitementsProduit> actaTraitementProducts0 = traitementProduitWithCroppingPlanEntry.stream()
                            .map(TraitementProduitWithCroppingPlanEntry::refActaTraitementsProduit)
                            .collect(Collectors.toSet());

                    Map<RefActaTraitementsProduit, ReferenceDoseDTO> legacyDoseForActaProducts =
                            performanceService.getLegacyDoseForActaProducts(actaTraitementProducts0, refEspeces);

                    legacyCropDoseForProducts.putAll(addCropWithSpeciesToMapKeys(legacyDoseForActaProducts, crop));
                }
            }
            interventionContext.setLegacyRefDosageForPhytoInputs(legacyCropDoseForProducts);

        } else {
            Map<RefActaTraitementsProduit, ReferenceDoseDTO> legacyDoseForActaProducts =
                    performanceService.getLegacyDoseForActaProducts(actaTraitementProducts, refEspeces);

            Map<TraitementProduitWithCroppingPlanEntry, ReferenceDoseDTO> cropDoseForProducts = addCropWithSpeciesToMapKeys(legacyDoseForActaProducts, crop);
            interventionContext.setLegacyRefDosageForPhytoInputs(cropDoseForProducts);
        }
    }

    protected Map<TraitementProduitWithCroppingPlanEntry, ReferenceDoseDTO> addCropWithSpeciesToMapKeys(
            Map<RefActaTraitementsProduit, ReferenceDoseDTO> doseForProducts,
            CroppingPlanEntry crop) {

        Map<TraitementProduitWithCroppingPlanEntry, ReferenceDoseDTO> cropDoseForProducts = new HashMap<>();
        for (RefActaTraitementsProduit refActaTraitementsProduit : doseForProducts.keySet()) {
            cropDoseForProducts.put(new TraitementProduitWithCroppingPlanEntry(refActaTraitementsProduit, crop),
                    doseForProducts.get(refActaTraitementsProduit));
        }
        return cropDoseForProducts;
    }

    private Set<RefActaTraitementsProduit> getInterventionInputUsageRefActaTraitementsProduits(PerformanceEffectiveInterventionExecutionContext interventionContext) {
        Set<RefActaTraitementsProduit> actaTraitementProducts = new HashSet<>();

        final Optional<BiologicalControlAction> optionalBiologicalControlAction = interventionContext.getOptionalBiologicalControlAction();
        final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction();

        actaTraitementProducts.addAll(PerformancePracticedExecutionContextBuilder.getBiologicalControlActionRefActaTraitementsProduits(optionalBiologicalControlAction));
        actaTraitementProducts.addAll(PerformancePracticedExecutionContextBuilder.getPesticidesSpreadingActionRefActaTraitementsProduits(optionalPesticidesSpreadingAction));

        return actaTraitementProducts;
    }

    protected void addInterventionContextSpecies(PerformanceEffectiveInterventionExecutionContext interventionContext) {
        List<CroppingPlanSpecies> interventionSpecies = new ArrayList<>();
        EffectiveIntervention intervention = interventionContext.getIntervention();
        final Collection<EffectiveSpeciesStade> speciesStades = intervention.getSpeciesStades();
        if (CollectionUtils.isNotEmpty(speciesStades)) {
            for (EffectiveSpeciesStade speciesStade : speciesStades) {
                CroppingPlanSpecies species = speciesStade.getCroppingPlanSpecies();
                if (species != null) {
                    interventionSpecies.add(species);
                }
            }
        }
        interventionContext.setInterventionSpecies(interventionSpecies);
    }

    protected ToolsCoupling getInterventionContextToolsCoupling(EffectiveIntervention intervention) {
        ToolsCoupling toolsCoupling = null;
        Collection<ToolsCoupling> toolsCouplings = intervention.getToolCouplings();// remember it can hava only one
        if (CollectionUtils.isNotEmpty(toolsCouplings)) {
            toolsCoupling = toolsCouplings.iterator().next();
        }
        return toolsCoupling;
    }

}
