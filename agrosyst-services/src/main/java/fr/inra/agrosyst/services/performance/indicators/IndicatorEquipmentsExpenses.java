package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.MaterielTransportUnit;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbu;
import fr.inra.agrosyst.api.services.performance.EquipmentUsageRange;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static fr.inra.agrosyst.api.entities.MaterielWorkRateUnit.VOY_H;
import static org.nuiton.i18n.I18n.l;

/**
 * ### Présentation de l’indicateur ###
 * <p>
 * Les charges de mécanisation réelles d’une intervention sont exprimées en €/ha. Les interventions
 * concernées par cet indicateur sont les interventions qui mobilisent une combinaison d’outils.
 * <p>
 * Les charges de mécanisation réelles correspondent aux coûts fixes (amortissement du matériel) et aux
 * coûts variables (consommation de carburant, réparation, lubrification, entretien des pneumatiques).
 * <p>
 * Les charges de mécanisation sont est calculées selon la méthode du « BCMA ». Il n’intègre pas les frais
 * liés à la main d’œuvre.
 * <p>
 * Les charges de mécanisation réelles prennent en compte le matériel déclaré par l’utilisateur pour
 * chaque intervention, le niveau d’utilisation de matériel et si ce matériel appartient ou non à une ETA/CUMA.
 * <p>
 * #### Formule de calcul ####
 * <p>
 * ### Charges de mécanisation réelles – échelle intervention – unité €/ha ###
 * <p>
 * CMréelles i = Coût Carburant i + (PSCi × [CM fixes i + Charges de réparations i + Charges pneumatiques i + Charges huile i ])
 * <p>
 * PSCi = nb de passages x proportion de surface concernée par l’intervention
 * <p>
 * *Sauf si l’un des matériels de l’intervention est associé à un champ « unité » (associé au champ «
 * UnitéParAn » dans le référentiel Matériel (BCMA)) différent de « ha » ou « heure ».
 * <p>
 * Dans ce cas :
 * CMréelles i = PSCi × 40
 * (charges méca réelles arrondies à 40€/ha)
 * <p>
 * ### Charges réelles fixes – échelle intervention – unité €/ha ###
 * <p>
 * CMréelles fixes i = ∑ ( Prix d′ achat e × 0.01 × TauxGlobal e / Utilisation e)
 * <p>
 * e : équipement appartenant à la liste des équipements (traction, outils) de la combinaison d’outils
 * utilisée dans l’intervention i.
 * <p>
 * Prix d ′ achat e (€) : Prix d’achat de l’équipement e, e appartenant à la liste des équipements (traction,
 * outils) de la combinaison d’outils utilisée dans l’intervention i. Donnée issue du référentiel Matériels
 * (colonne « prix moyen d’achat »).
 * <p>
 * Tauxglobal e (%) : Taux d’amortissement annuel du matériel e (outil, tracteur, automoteur) intégrant
 * la durée d’amortissement, le taux d’amortissement, les intérêts, les frais de remisage et d’assurance.
 * Ce taux est fourni dans une table produite par l’APCA-Bureau Agroéquipement. La valeur de
 * <p>
 * TauxGlobal e est choisie en fonction du type de matériel (Traction, Automoteur, outil, et des variables
 * Donnéeamortissement 1 et Donnéeamortissement 2).
 * <p>
 * Attention /!\ Pour le matériel d’irrigation, la charge fixe de l’équipement est plus simplement la
 * variable « chargesFixesParUniteDeVolumeDeTravailAnnuel » du référentiel « Matériel (BCMA)
 * Irrigation », correspondant à l’identifiant du matériel (Type matériel 1, 2, 3, 4 et « unitésParAn »)
 * <p>
 *  Si « chargesFixesParUniteDeVolumeDeTravailAnnuelUnite » = €/ha
 * alors CMréelles fixes e = chargesFixesParUniteDeVolumeDeTravailAnnuel
 *  Si « chargesFixesParUniteDeVolumeDeTravailAnnuelUnite » = €/m3
 * alors
 * CMréelles fixes e = chargesFixesParUniteDeVolumeDeTravailAnnuel * 10 * Quantité moy.
 * où Quantité moy. est le volume d’eau d’irrigation de l’intervention en mm
 * <p>
 * Utilisation e (ha/an) : Utilisation annuelle de l’équipement e, e appartenant à la liste des
 * équipements (traction, outils) de la combinaison d’outils utilisée dans l’intervention i.
 * <p>
 * /!\ Quelle valeur prendre pour l’utilisation annuelle de l’équipement e ?
 * Pour chaque matériel, l’utilisateur doit déclarer un niveau d’utilisation à l’échelle du domaine. Ce
 * niveau d’utilisation influe sur le calcul des charges de mécanisation. En effet, moins le matériel est
 * utilisé sur sa durée d’amortissement, plus le coût du passage rapporté à l’ha augmente.
 * <p>
 * Actuellement, l’utilisateur peut choisir parmi 3 valeurs d’utilisation annuelle « approximative »
 * proposées par le BCMA. La nouveauté proposée ici est que l’utilisateur puisse, après son choix parmi
 * les 3 valeurs BCMA (« utilisation annuelle approximative »), préciser la valeur d’utilisation annuelle
 * du matériel qu’il a estimé avec l’agriculteur (« utilisation annuelle précise »),
 * EN UTILISANT LA MÊME UNITE.
 * <p>
 * Les règles pour définir la valeur d’utilisation annuelle utilisée dans le calcul des charges de
 * mécanisation seraient alors les suivantes :
 * <p>
 * Utilisation annuelle approximative
 * Utilisation annuelle précise
 * |
 * La valeur précise est plus faible que la plus faible
 * des 3 valeurs d'utilisation proposée par le BCMA
 * |
 * ------------------------------------------------------
 * Oui                                                  Non
 * |                                                     |
 * Matériel appartenant à une                           Matériel appartenant à une
 * ETA/CUMA ?                                            ETA/CUMA ?
 * |                                                     |
 * -----------------------------                         -----------------------------
 * |                           |                         |                           |
 * Oui                         Non                       Oui                        Non
 * |                           |                         |                           |
 * Utilisation e = valeur    Utilisation e = valeur    Utilisation e = valeur          Utilisation e =
 * la plus élevée du         la plus faible du          la plus élevée du         Utilisation annuelle
 * référentiel               référentiel                référentiel             précise déclarée par
 * l'utilsiateur (en
 * <p>
 * Avec cette méthode, on considère toujours que le matériel utilisé en ETA/BCMA a la valeur la
 * plus grande d’utilisation annuelle proposée par le BCMA, ce qui rend son coût de passage
 * faible.
 * <p>
 * Si le matériel n’appartient pas à une ETA ou CUMA, et si la valeur d’utilisation annuelle du
 * matériel déclarée par l’utilisateur est inférieure à la plus faible des valeurs proposées par le
 * BCMA, on prendra tout de même pour utilisation annuelle la plus faible des valeurs proposées
 * par le BCMA. Cela permet de ne pas surestimer le coût de passage. L’hypothèse sous-jacente
 * que si le matériel est très peu utilisé, l’agriculteur a utilisé une stratégie lui permettant de ne
 * pas avoir un coût de passage trop important (partage du matériel, etc.).
 * <p>
 * Attention /!\ Utilisation e doit être exprimée en ha/an avant d’entrer dans le calcul de
 * CMréelles fixes i
 * <p>
 * Si « unité » (associé au champ « UnitéParAn » dans le référentiel Matériel (BCMA)) est ha
 * >> pas de problème
 * Si « unité » (associé au champ « UnitéParAn » dans le référentiel Matériel (BCMA)) est h
 * >> transformer Utilisation e en multipliant la valeur par le débit de chantier de l’intervention
 * exprimé en ha/h (ou en divisant la valeur par le débit de chantier de l’intervention exprimé en
 * ha/h)
 * Pour mémoire, s’il y a plusieurs outils dans la combinaison d’outil standardisée, on retient le
 * débit de chantier le plus lent = le plus élevé en h/ha = le plus faible en ha/h
 * <p>
 * Si « unité » (associé au champ « UnitéParAn » dans le référentiel Matériel (BCMA)) est
 * différent, alors
 * CMréelles i = PSCi × 40
 * (il s’agit bien de l’ensemble des
 * charges, fixes + variables)
 * Dans ces cas compliqués, on simplifie pour l’instant en considérant que le coût de
 * mécanisation associé à l’intervention est de 40 Euros/ha (sauf si solutions intermédiaires
 * proposées)
 * <p>
 * ### Consommation de carburant réelle – échelle intervention – unité L/ha ###
 * <p>
 * Conso_carburant_réelle i = Temps de travail x Taux de charge x Puissance x C
 * <p>
 * Temps de travail = débit de chantier en H/ha x PSCi
 * (temps de travail par ha du Système de culture)
 * Pour mémoire, s’il y a plusieurs outils dans la combinaison d’outil standardisée, on retient le
 * débit de chantier le plus lent = le plus élevé en h/ha = le plus faible en ha/h
 * Attention : l’unité la plus fréquente de débit de chantier (champ « performance_unité ») est
 * ha/h. Il faut donc faire la transformation en h/ha
 * Pour les autres unités de « performance_unité », David tu peux voir s’il y a moyen de calculer
 * le débit de chantier avec la quantité d’intrants ou la quantité récoltée. Si c’est impossible
 * alors on considère que la consommation réelle est égale à 8 L/ha (modulo PSCi), la
 * consommation moyenne pour une intervention de fertilisation organique.
 * <p>
 * Taux de charge = max(Taux de charge[outils])
 * (taux de charge max sur les différents outils ; ne pas prendre ne compte le taux de charge du
 * tracteur, qui est un taux de charge moyen annuel estimé par le BCMA)
 * REF_OUTILS > colonne donneesTauxDeChargeMoteur
 * Pour les automoteurs : Taux de charge = Taux de charge[automoteur]
 * REF_AUTOMOTEUR > colonne donneesTauxDeChargeMoteur
 * <p>
 * Puissance = Puissance du tracteur
 * REF_TRACTION > colonne puissanceChISO
 * REF_AUTOMOTEUR > colonne puissanceChISO
 * <p>
 * C = consommation par cheval (Attention, nouveauté : C varie avec la puissance)
 * o C=0.24 pour les tracteurs et automoteurs de moins de 130 Ch
 * o C=0.21 pour les tracteurs et automoteurs de plus de 130 Ch
 * Si « PuissanceChISO » du tracteur (ou de l’automoteur) > 130, alors C=0.21
 * Sinon C=0.24
 * <p>
 * ### Coût du carburant réel– échelle intervention – unité €/ha ###
 * <p>
 * Coût Carburant réel i = Conso_carburant_réelle i × Prix carbu.
 * <p>
 * Remarques :
 * <p>
 *  En Réalisé, le prix du carburant est le prix déclaré par l’utilisateur pour la campagne concernée.
 * Par défaut, s’il n’y a pas de prix saisi, on utilise le prix du référentiel Prix_Reference_Carburant
 * pour la campagne concernée
 * <p>
 *  En Synthétisé, le prix du carburant est la moyenne des prix saisis sur l’ensemble des campagnes
 * concernées par le synthétisé. S’il n’y a aucun prix saisis, on fait la moyenne sur les campagnes
 * concernées des prix du référentiel Prix_Reference_Carburant
 * <p>
 * ### Charges de réparations réelles– échelle intervention – unité €/ha ###
 * <p>
 * Charges de réparations_réelles i = ∑ Charges de réparations_réelles e
 *                                     e
 * Charges de réparations_réelles e (€/ha) : charges de réparation de l’équipement e, e appartenant
 * à la liste des équipements (traction, outils) de la combinaison d’outils utilisée dans l’intervention i.
 * Donnée issue du référentiel Matériels (colonne « Réparations €/unité de travail annuel »).
 * <p>
 * Attention, les charges de réparation ne sont pas toujours exprimées en €/ha. Si « reparationsUnite »
 * est en €/h, transformer en €/ha en divisant par le débit de chantier de l’intervention en ha/h (ou en
 * multipliant par le débit de chantier en h/ha).
 * <p>
 * Si « reparationsUnite » est exprimé dans une autre unité, alors on considère que les charges de
 * réparation sont nulles (il y a de fortes chances qu’on soit dans le cas CMréelles i = PSCi × 40) (sauf
 * si solutions intermédiaires)
 * <p>
 * ### Charges pneumatiques réelles – échelle intervention – unité €/ha ###
 * <p>
 * Charges pneumatiques_réelles i = ∑ Charges pneumatiques_réelle e
 *                                   e
 * Charges pneumatiques_réelles e (€/ha) : charges de remplacement des pneumatiques de
 * l’équipement e (uniquement tracteurs et automoteurs, a priori il n’y a qu’un seul matériel concerné
 * pour une intervention donnée) de la combinaison d’outils utilisée dans l’intervention i (champ « pneusParUniteDeTravail » des référentiels « Matériels (BCMA) : Traction » et « Matériels (BCMA) :
 * automoteurs »)
 * <p>
 * Attention, les charges de pneumatiques ne sont pas toujours exprimées en €/ha. Si « pneusCoutUnite »
 * est en €/h, transformer en €/ha en divisant « pneusParUniteDeTravail » par le débit de chantier de
 * l’intervention en ha/h (ou en multipliant par le débit de chantier en h/ha).
 * <p>
 * Si « pneusCoutUnite » est dans une autre unité, alors on considère que les charges de pneumatiques
 * sont nulles (il y a de fortes chances qu’on soit dans le cas CMréelles i = PSCi × 40) (sauf si solutions
 * intermédiaires proposées)
 * <p>
 * ### Charges Huile réelles – échelle intervention – unité €/ha ###
 * <p>
 * Charges huile_réelle i = ∑ Charges huile_réelle e
 *                           e
 * Charges huile_réelle e (€/ha) : charges de lubrifiant de l’équipement e (uniquement tracteurs et
 * automoteurs) de la combinaison d’outils utilisée dans l’intervention i.
 * <p>
 * Donnée issue du référentiel Matériels (colonne « lubrifiantParUniteDeTravail»).
 * <p>
 * Attention, les charges de lubrifiant ne sont pas toujours exprimées en €/ha. Si « lubrifiantCoutUnite »
 * est en €/h, transformer en €/ha en divisant « lubrifiantParUniteDeTravail » par le débit de chantier de
 * l’intervention en ha/h (ou en multipliant par le débit de chantier en h/ha).
 * <p>
 * Si « lubrifiantCoutUnite » est dans une autre unité, alors on considère que les charges de lubrifiants
 * sont nulles (il y a de fortes chances qu’on soit dans le cas CMréelles i = PSCi × 40) (sauf si solutions
 * intermédiaires proposées par David)
 * <p>
 * ### Données de référence ###
 * <p>
 * Référentiel BCMA
 * Prix de référence du carburant
 *
 * </pre>
 *
 * @author David Cossé (cosse@codelutin.com)
 */
public class IndicatorEquipmentsExpenses extends AbstractIndicator {

    private static final Log LOGGER = LogFactory.getLog(IndicatorEquipmentsExpenses.class);

    private static final String COLUMN_NAME = "charges_de_mecanisation";
    private static final String CHARGES_MECANISATION_REELLES_TX_COMP = COLUMN_NAME + "_reelles_taux_de_completion";
    private static final String CHARGES_MECANISATION_STD_MIL_TX_COMP = COLUMN_NAME + "_std_mil_taux_de_completion";
    private static final String CHARGES_MECANISATION_REELLES_DETAIL = COLUMN_NAME + "_reelles_detail_champs_non_renseig";
    private static final String CHARGES_MECANISATION_STD_MIL_DETAIL = COLUMN_NAME + "_std_mil_detail_champs_non_renseig";

    public static final String PURCHASE_COST_UNIT = "€";

    public static final double DEFAULT_WORK_RATE_VALUE = 0.0d;
    public static final double DEFAULT_EXPENSE = 40.0d;

    public static final double ZERO = 0.0d;
    public static final String IRRIG_ANUAL_COST_UNIT_EURO_HA = "€/ha";
    public static final String IRRIG_ANUAL_COST_UNIT_EURO_M_3 = "€/m3";

    public static final String REPARE_COST_EURO_HA = "€/ha";
    public static final String REPARE_COST_EURO_H = "€/h";
    public static final String REPARE_COST_EURO_VOY = "€/voy";
    public static final String REPARE_COST_EURO_BAL = "€/bal";

    public static final String TYRE_COST_EURO_HA = "€/ha";
    public static final String TYRE_COST_EURO_H = "€/h";

    public static final String OIL_COST_EURO_HA = "€/ha";
    public static final String OIL_COST_EURO_H = "€/h";
    private static final String VOY = "Voy";

    private final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    private boolean computeReal = true;
    private boolean computeStandardised = true;

    public IndicatorEquipmentsExpenses() {
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_MECANISATION_REELLES_TX_COMP,
                CHARGES_MECANISATION_REELLES_TX_COMP
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_MECANISATION_REELLES_DETAIL,
                CHARGES_MECANISATION_REELLES_DETAIL
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_MECANISATION_STD_MIL_TX_COMP,
                CHARGES_MECANISATION_STD_MIL_TX_COMP
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_MECANISATION_STD_MIL_DETAIL,
                CHARGES_MECANISATION_STD_MIL_DETAIL
        );
    }

    private static final String[] LABELS = {
            "Indicator.label.equipmentsExpensesReal",
            "Indicator.label.equipmentsExpensesStandard"
    };

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();

        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME + "_reelles");
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME + "_std_mil");

        indicatorNameToColumnName.put(CHARGES_MECANISATION_REELLES_TX_COMP, CHARGES_MECANISATION_REELLES_TX_COMP);
        indicatorNameToColumnName.put(CHARGES_MECANISATION_STD_MIL_TX_COMP, CHARGES_MECANISATION_STD_MIL_TX_COMP);
        indicatorNameToColumnName.put(CHARGES_MECANISATION_REELLES_DETAIL, CHARGES_MECANISATION_REELLES_DETAIL);
        indicatorNameToColumnName.put(CHARGES_MECANISATION_STD_MIL_DETAIL, CHARGES_MECANISATION_STD_MIL_DETAIL);

        return indicatorNameToColumnName;
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale,INDICATOR_CATEGORY_ECONOMIC);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) && displayed && (i == 0 && computeReal || i == 1 && computeStandardised);
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {

        // Charges de mécanisation réelles – échelle intervention – unité €/ha
        //
        // CMréelles i = Coût Carburant i + (PSCi × [CM fixes i + Charges de réparations i
        //               + Charges pneumatiques i + Charges huile i ])

        if (interventionContext.isFictive()) return newArray(LABELS.length, 0.0d);

        Double[] result = newArray(LABELS.length, 0.0d);

        ToolsCoupling tc = interventionContext.getToolsCoupling();

        if (tc != null && !tc.isManualIntervention()) {

            final PracticedIntervention intervention = interventionContext.getIntervention();
            final String interventionId = intervention.getTopiaId();

            if (tc.getTractor() == null || tc.getTractor().getRefMateriel() == null) {
                addMissingFieldMessage(tc.getToolsCouplingName(), messageBuilder.getMissingTractorMessage(intervention.getName(), interventionContext.getInterventionId(), tc.getToolsCouplingName()));
                LOGGER.warn(String.format("Pour l'intervention '%s (%s)', la combinaison d'outils '%s' nest pas correcte, aucun matériel de traction y est associé", intervention.getName(), interventionContext.getInterventionId(), tc.getToolsCouplingName()));
                return null;
            }

            // workRate
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);

            Double workRateValue = intervention.getWorkRate();
            MaterielWorkRateUnit workRateUnit = intervention.getWorkRateUnit();

            if (workRateValue == null || workRateUnit == null) {
                workRateValue = tc.getWorkRate();
                workRateUnit = tc.getWorkRateUnit();
            }

            if (workRateValue == null || workRateUnit == null) {
                workRateValue = Indicator.DEFAULT_WORK_RATE_VALUE;
                workRateUnit = MaterielWorkRateUnit.H_HA;
                addMissingFieldMessage(interventionId, messageBuilder.getMissingWorkRateMessage());
            }

            final Pair<Double, MaterielWorkRateUnit> workRate = Pair.of(workRateValue, workRateUnit);

            final Collection<OrganicProductInputUsage> organicProductInputUsages = interventionContext.getOptionalOrganicFertilizersSpreadingAction().map(OrganicFertilizersSpreadingAction::getOrganicProductInputUsages).orElse(null);

            final Map<RefMateriel, RefAgsAmortissement> deprecationRateByEquipments = domainContext.getDeprecationRateByEquipments();

            final Collection<EquipmentUsageRange> equipmentUsageRanges = domainContext.getEquipmentUsageRanges();

            final Double organicProductsTotalQuantity = VOY_H.equals(workRateUnit) ? getOrganicProductsTotalQuantity(organicProductInputUsages) : null;
            final OrganicProductUnit organicProductFirstUnit = VOY_H.equals(workRateUnit) && organicProductInputUsages != null ?
                    getFirstOrganicProductUnit(organicProductInputUsages) : null;

            // transitvolume
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            final Double transitVolume = intervention.getTransitVolume();
            final MaterielTransportUnit transitVolumeUnit = intervention.getTransitVolumeUnit();

            final Integer nbBalls = intervention.getNbBalls();
            final double toolPSCi = getToolPSCi(intervention);

            // count fields for: OrganicInput dose, transitVolume dose, nbBalls, YealdUnit
            final double toolUsageTime = getEquipmentsToolsUsage(
                    tc,
                    workRate,
                    interventionId,
                    transitVolume,
                    transitVolumeUnit,
                    nbBalls,
                    organicProductInputUsages,
                    Optional.empty());

            final double workingTime = toolPSCi * toolUsageTime;

            Double fixedCost = getFixedCostForMaterial(
                    deprecationRateByEquipments,
                    equipmentUsageRanges,
                    tc,
                    workingTime,
                    interventionContext.getOptionalIrrigationAction(),
                    interventionContext.getInterventionId(),
                    nbBalls,
                    workRateUnit,
                    organicProductsTotalQuantity,
                    organicProductFirstUnit);

            if (fixedCost == null) {
                return newResult(DEFAULT_EXPENSE * toolPSCi, DEFAULT_EXPENSE * toolPSCi);
            }

            Pair<Double, Double> fuelExpenses = getRealAndStandardisedFuelExpenses(interventionContext, practicedSystemContext, domainContext);

            double repairCost = getRepairCost(tc, workingTime, organicProductsTotalQuantity, transitVolume, transitVolumeUnit, nbBalls, organicProductFirstUnit);

            double tyreCost = getTyreCost(tc, workingTime);

            double oilCost = getOilCostForMaterial(tc, workingTime);

            double realEquipmentsExpenses = fuelExpenses.getLeft() + (toolPSCi * (fixedCost + repairCost + tyreCost + oilCost));
            double standardisedEquipmentsExpenses = fuelExpenses.getRight() + (toolPSCi * (fixedCost + repairCost + tyreCost + oilCost));
            result = newResult(realEquipmentsExpenses, standardisedEquipmentsExpenses);

            interventionContext.setFuelExpense(fuelExpenses.getLeft());

            interventionContext.setRealEquipmentsExpenses(realEquipmentsExpenses);
            interventionContext.setStandardisedEquipmentsExpenses(standardisedEquipmentsExpenses);
        }

        return result;
    }

    private OrganicProductUnit getFirstOrganicProductUnit(Collection<OrganicProductInputUsage> organicProductInputUsages) {
        return organicProductInputUsages.stream()
                .map(OrganicProductInputUsage::getDomainOrganicProductInput)
                .map(DomainOrganicProductInput::getUsageUnit)
                .findFirst()
                .orElse(null);
    }

    protected Pair<Double, Double> getRealAndStandardisedFuelExpenses(PerformancePracticedInterventionExecutionContext interventionContext,
                                                                      PerformancePracticedSystemExecutionContext practicedSystemContext,
                                                                      PerformancePracticedDomainExecutionContext domainContext) {
        double fuelConsumption = interventionContext.getFuelConsumption() != null ? interventionContext.getFuelConsumption() : 0.0d;

        final Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> fuelRefPriceForInput = practicedSystemContext.getPracticedSystemInputRefPricesByDomainInput().fuelRefPriceForInput();
        Optional<AbstractDomainInputStockUnit> optionalDomainFuelPrice = fuelRefPriceForInput.keySet().stream().findAny();
        Optional<RefPrixCarbu> refPrixCarbu = domainContext.getOptionalRefFuelPricePerLiter();
        double realFuelPricePerLiter = 0.0d;
        double standardisedFuelPricePerLiter = 0.0d;
        if (optionalDomainFuelPrice.isPresent() && optionalDomainFuelPrice.get().getInputPrice() != null) {
            realFuelPricePerLiter = optionalDomainFuelPrice.get().getInputPrice().getPrice();
        } else if (refPrixCarbu.isPresent() && refPrixCarbu.get().getPrice() != null) {
            realFuelPricePerLiter = refPrixCarbu.get().getPrice();
        }

        if (refPrixCarbu.isPresent() && refPrixCarbu.get().getPrice() != null) {
            standardisedFuelPricePerLiter = refPrixCarbu.get().getPrice();
        }

        return getRealAndStandardisedFuelExpenses(fuelConsumption, realFuelPricePerLiter, standardisedFuelPricePerLiter);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {

        // Charges de mécanisation réelles – échelle intervention – unité €/ha
        //
        // CMréelles i = Coût Carburant i + (PSCi × [CM fixes i + Charges de réparations i
        //               + Charges pneumatiques i + Charges huile i ])

        Double[] result = newArray(LABELS.length, 0.0d);

        ToolsCoupling tc = interventionContext.getToolsCoupling();

        if (tc != null) {

            final EffectiveIntervention intervention = interventionContext.getIntervention();

            final String interventionId = interventionContext.getInterventionId();
            if (!tc.isManualIntervention() && (tc.getTractor() == null || tc.getTractor().getRefMateriel() == null)) {
                addMissingFieldMessage(tc.getToolsCouplingName(), messageBuilder.getMissingTractorMessage(intervention.getName(), interventionId, tc.getToolsCouplingName()));
                LOGGER.warn(String.format("Pour l'intervention '%s (%s)', la combinaison d'outils '%s' nest pas correcte, aucun matériel de traction y est associé", intervention.getName(), interventionId, tc.getToolsCouplingName()));

            } else {

                Pair<Double, MaterielWorkRateUnit> workRate = getEffectiveInterventionWorkRate(domainContext, interventionContext, DEFAULT_WORK_RATE_VALUE, MaterielWorkRateUnit.H_HA);
                MaterielWorkRateUnit workRateUnit = workRate.getRight();

                final Collection<OrganicProductInputUsage> organicProductInputUsages = interventionContext.getOptionalOrganicFertilizersSpreadingAction().map(OrganicFertilizersSpreadingAction::getOrganicProductInputUsages).orElse(Collections.emptyList());

                Map<RefMateriel, RefAgsAmortissement> deprecationRateByEquipments = domainContext.getDeprecationRateByEquipments();

                Collection<EquipmentUsageRange> equipmentUsageRanges = domainContext.getEquipmentUsageRanges();

                Double organicProductsTotalQuantity = VOY_H.equals(workRateUnit) ? getOrganicProductsTotalQuantity(organicProductInputUsages) : null;
                final OrganicProductUnit organicProductFirstUnit = VOY_H.equals(workRateUnit) ?
                        getFirstOrganicProductUnit(organicProductInputUsages) : null;

                // transitvolume
                incrementAngGetTotalFieldCounterForTargetedId(interventionId);
                Double transitVolume = intervention.getTransitVolume();
                MaterielTransportUnit transitVolumeUnit = intervention.getTransitVolumeUnit();

                final Integer nbBalls = intervention.getNbBalls();
                final double toolPSCi = getToolPSCi(intervention);

                // count fields for: OrganicInput dose, transitVolume dose, nbBalls, YealdUnit
                double toolUsageTime = getEquipmentsToolsUsage(
                        tc,
                        workRate,
                        intervention.getTopiaId(),
                        transitVolume,
                        transitVolumeUnit,
                        nbBalls,
                        organicProductInputUsages,
                        Optional.empty());

                final double workingTime = toolPSCi * toolUsageTime;

                Double fixedCost = getFixedCostForMaterial(
                        deprecationRateByEquipments,
                        equipmentUsageRanges,
                        tc,
                        workingTime,
                        interventionContext.getOptionalIrrigationAction(),
                        interventionId,
                        nbBalls,
                        workRateUnit,
                        organicProductsTotalQuantity,
                        organicProductFirstUnit);

                if (fixedCost == null) {
                    result = newResult(DEFAULT_EXPENSE * toolPSCi, DEFAULT_EXPENSE * toolPSCi);

                } else {

                    Pair<Double, Double> fuelExpenses = getRealAndStandardisedFuelExpenses(interventionContext, domainContext);

                    double repairCost = getRepairCost(tc, workingTime, organicProductsTotalQuantity, transitVolume, transitVolumeUnit, nbBalls, organicProductFirstUnit);

                    double tyreCost = getTyreCost(tc, workingTime);

                    double oilCost = getOilCostForMaterial(tc, workingTime);

                    double realEquipmentsExpenses = fuelExpenses.getLeft() + (toolPSCi * (fixedCost + repairCost + tyreCost + oilCost));
                    double standardisedEquipmentsExpenses = fuelExpenses.getRight() + (toolPSCi * (fixedCost + repairCost + tyreCost + oilCost));
                    result = newResult(realEquipmentsExpenses, standardisedEquipmentsExpenses);

                    interventionContext.setFuelExpense(fuelExpenses.getLeft());

                    interventionContext.setRealEquipmentsExpenses(realEquipmentsExpenses);
                    interventionContext.setStandardisedEquipmentsExpenses(standardisedEquipmentsExpenses);
                }
            }
        }

        return result;
    }

    protected Double getOrganicProductsTotalQuantity(Collection<OrganicProductInputUsage> organicProductInputs) {
        Double organicProductsTotalQuantity = null;

        if (CollectionUtils.isNotEmpty(organicProductInputs)) {
            Map<OrganicProductUnit, List<OrganicProductInputUsage>> organicProductInputsByProductUnit = new HashMap<>();

            organicProductInputs.stream()
                    .forEach(
                            organicProductInputUsage ->
                            {
                                DomainOrganicProductInput domainOrganicProductInput = organicProductInputUsage.getDomainOrganicProductInput();
                                organicProductInputsByProductUnit.computeIfAbsent(domainOrganicProductInput.getUsageUnit(), k -> new ArrayList<>());
                                organicProductInputsByProductUnit.get(domainOrganicProductInput.getUsageUnit()).add(organicProductInputUsage);
                            }
                    );

            for (Map.Entry<OrganicProductUnit, List<OrganicProductInputUsage>> organicProductsForUnit : organicProductInputsByProductUnit.entrySet()) {
                OrganicProductUnit unit0 = organicProductsForUnit.getKey();
                List<OrganicProductInputUsage> inputUsages = organicProductsForUnit.getValue();
                double totalQuantityForUnit = inputUsages.stream().map(OrganicProductInputUsage::getQtAvg)
                        .map(qtAvg -> qtAvg != null ? qtAvg : DEFAULT_ORGANIC_INPUT_QTE_AVG_DOSE)
                        .mapToDouble(qtAvg -> qtAvg)
                        .sum();

                if (0 != totalQuantityForUnit) {
                    organicProductsTotalQuantity = organicProductsTotalQuantity == null ? 0 : organicProductsTotalQuantity;
                    // si Unité de Qté d'engrais apporté pour l'intervention = kg/ha, alors Qté apportée en t/ha = 0.001 x Qté apportée en kg/ha
                    //KG_HA,
                    if (OrganicProductUnit.KG_HA.equals(unit0)) {
                        organicProductsTotalQuantity += totalQuantityForUnit * 0.001;
                    }
                    if (OrganicProductUnit.T_HA.equals(unit0) || OrganicProductUnit.M_CUB_HA.equals(unit0)) {
                        organicProductsTotalQuantity += totalQuantityForUnit;
                    }
                }
            }
        }
        return organicProductsTotalQuantity;
    }

    private double getTyreCost(ToolsCoupling tc, double equipmentsUsage_H_HA) {
        /*
        Charges pneumatiques_réelles i = ∑ Charges pneumatiques_réelle e
                                         e
        Charges pneumatiques_réelles e (€/ha) : charges de remplacement des pneumatiques de
        l’équipement e (uniquement tracteurs et automoteurs, a priori il n’y a qu’un seul matériel concerné
        pour une intervention donnée) de la combinaison d’outils utilisée dans l’intervention i (champ« pneusParUniteDeTravail » des référentiels « Matériels (BCMA) : Traction » et « Matériels (BCMA) :
        automoteurs »)
        
        Attention, les charges de pneumatiques ne sont pas toujours exprimées en €/ha.
        
        Si « pneusCoutUnite » est en €/h, transformer en €/ha en divisant « pneusParUniteDeTravail » par le débit de chantier de
        l’intervention en ha/h (ou en multipliant par le débit de chantier en h/ha).
        
         */
        return Optional.ofNullable(tc.getTractor())
                .map(Equipment::getRefMateriel)
                .filter(materiel -> materiel instanceof RefMaterielTraction) // TODO dcosse 21/02/2019 pas de prix des pneumatiques trouvé pour les automoteurs
                .map(materiel -> {
                    RefMaterielTraction tractor = (RefMaterielTraction) materiel;
                    String unite = tractor.getPneusCoutUnite();
                    double factor;
                    if (TYRE_COST_EURO_HA.equals(unite)) {
                        factor = 1d;
                    } else if (TYRE_COST_EURO_H.equals(unite)) {
                        factor = equipmentsUsage_H_HA;
                    } else {
                        factor = ZERO;
                    }
                    return tractor.getPneusParUniteDeTravail() * factor;
                })
                .orElse(0d);
    }

    private double getRepairCost(ToolsCoupling tc, double equipmentsUsage_H_HA, Double organicInputQty, Double transitVolume, MaterielTransportUnit transitVolumeUnit, Integer nbBal_Ha, OrganicProductUnit organicProductFirstUnit) {
        /*
         * Charges de réparations_réelles i = ∑ Charges de réparations_réelles e
         *                                    e
         * Charges de réparations_réelles e (€/ha) : charges de réparation de l’équipement e, e appartenant
         * à la liste des équipements (traction, outils) de la combinaison d’outils utilisée dans l’intervention i.
         * Donnée issue du référentiel Matériels (colonne « Réparations €/unité de travail annuel »).
         *
         * Attention, les charges de réparation ne sont pas toujours exprimées en €/ha. Si « reparationsUnite »
         * est en €/h, transformer en €/ha en divisant par le débit de chantier de l’intervention en ha/h (ou en
         * multipliant par le débit de chantier en h/ha).
         *
         * Si « reparationsUnite » est exprimé dans une autre unité, alors on considère que les charges de
         * réparation sont nulles (il y a de fortes chances qu’on soit dans le cas CMréelles i = PSCi × 40)
         */

        Set<RefMateriel> allEquipments = getToolsCouplingRefMateriels(tc);

        double globalCost = 0.0d;
        for (RefMateriel equipment : allEquipments) {
            String unite = equipment.getReparationsUnite();
            double factor = getRepareCostFactor(unite, tc.getWorkRateUnit(), equipmentsUsage_H_HA, organicInputQty, transitVolume, transitVolumeUnit, nbBal_Ha, organicProductFirstUnit);
            globalCost += equipment.getReparationsParUniteDeTravailAnnuel() * factor;
        }
        return globalCost;
    }

    protected double getRepareCostFactor(String unite, MaterielWorkRateUnit workRateUnit, double equipmentsUsage_H_HA, Double organicInputQty, Double transitVolume, MaterielTransportUnit transitVolumeUnit, Integer nbBal_Ha, OrganicProductUnit organicProductFirstUnit) {
        if (unite == null) {
            return 0;
        }
        if (REPARE_COST_EURO_HA.equals(unite)) {

            return 1;

        } else if (REPARE_COST_EURO_H.equals(unite)) {

            return equipmentsUsage_H_HA;

        } else if (VOY_H.equals(workRateUnit) &&
                REPARE_COST_EURO_VOY.equals(unite) &&
                (MaterielTransportUnit.M3.equals(transitVolumeUnit) || MaterielTransportUnit.T.equals(transitVolumeUnit)) &&
                organicInputQty != null) {

            if (transitVolume != null) {
                return transitVolume == 0 ? 0 : organicInputQty / transitVolume;
            } else {
                if (organicProductFirstUnit != null) {
                    return organicInputQty / DEFAULT_TRANSIT_VOLUME.get(organicProductFirstUnit);
                } else {
                    return 0;
                }
            }

        } else if (MaterielWorkRateUnit.BAL_H.equals(workRateUnit) &&
                REPARE_COST_EURO_BAL.equals(unite) &&
                nbBal_Ha != null) {

            return nbBal_Ha;
        }
        return 0;
    }

    protected Set<RefMateriel> getToolsCouplingRefMateriels(ToolsCoupling tc) {
        Set<RefMateriel> allEquipments = tc.getEquipments().stream()
                .map(Equipment::getRefMateriel)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        Optional.ofNullable(tc.getTractor())
                .map(Equipment::getRefMateriel)
                .ifPresent(allEquipments::add);
        return allEquipments;
    }

    private Pair<Double, Double> getRealAndStandardisedFuelExpenses(PerformanceEffectiveInterventionExecutionContext interventionContext, PerformanceEffectiveDomainExecutionContext domainContext) {
        Double fuelConsumption = interventionContext.getFuelConsumption();

        final Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> abstractDomainInputStockUnitRefPrixCarbuMap = domainContext.getRefInputPricesForCampaignsByInput().fuelRefPriceForInput();
        Optional<AbstractDomainInputStockUnit> optionalDomainFuelPrice = abstractDomainInputStockUnitRefPrixCarbuMap.keySet().stream().findAny();
        Optional<RefPrixCarbu> refPrixCarbu = domainContext.getOptionalRefFuelPricePerLiter();
        double realFuelPricePerLiter = 0.0d;
        double standardisedFuelPricePerLiter = 0.0d;
        if (optionalDomainFuelPrice.isPresent() && optionalDomainFuelPrice.get().getInputPrice() != null) {
            realFuelPricePerLiter = optionalDomainFuelPrice.get().getInputPrice().getPrice();
        } else if (refPrixCarbu.isPresent() && refPrixCarbu.get().getPrice() != null) {
            realFuelPricePerLiter = refPrixCarbu.get().getPrice();
        }

        if (refPrixCarbu.isPresent() && refPrixCarbu.get().getPrice() != null) {
            standardisedFuelPricePerLiter = refPrixCarbu.get().getPrice();
        }

        return getRealAndStandardisedFuelExpenses(fuelConsumption, realFuelPricePerLiter, standardisedFuelPricePerLiter);
    }

    private Pair<Double, Double> getRealAndStandardisedFuelExpenses(Double fuelConsumption, double realFuelPricePerLiter, double standardisedFuelPricePerLiter) {
        double realFuelPrice = 0.0d; // en cas d'intervention manuelle
        double standardisedFuelPrice = 0.0d; // en cas d'intervention manuelle
        if (fuelConsumption != null) {// Peut-être null en cas d'intervention manuelle
            realFuelPrice = realFuelPricePerLiter * fuelConsumption;
            standardisedFuelPrice = standardisedFuelPricePerLiter * fuelConsumption;
        }
        return Pair.of(realFuelPrice, standardisedFuelPrice);
    }

    protected Double getFixedCostForMaterial(Map<RefMateriel, RefAgsAmortissement> deprecationRateByEquipments,
                                             Collection<EquipmentUsageRange> equipmentUsageRanges,
                                             ToolsCoupling tc,
                                             double equipmentsUsage,
                                             Optional<IrrigationAction> irrigAction,
                                             String interventionId,
                                             Integer nbBalls_ha,
                                             MaterielWorkRateUnit workRateUnit,
                                             Double organicProductsTotalQuantity,
                                             OrganicProductUnit organicProductFirstUnit) {
        /*
            Charges réelles fixes – échelle intervention – unité €/ha
            
                                     Prix d′achat e × TauxGlobal e
            CMréelles fixes i = ∑   --------------------------------------
                                e               Utilisation e
                                
            Prix d ′ achat e (€) : Prix d’achat de l’équipement e, e appartenant à la liste des équipements (traction,
            outils) de la combinaison d’outils utilisée dans l’intervention i. Donnée issue du référentiel Matériels
            (colonne « prix moyen d’achat »).
            
            Tauxglobal e (%) : Taux d’amortissement annuel du matériel e (outil, tracteur, automoteur) intégrant
            la durée d’amortissement, le taux d’amortissement, les intérêts, les frais de remisage et d’assurance.
            Ce taux est fourni dans une table produite par l’APCA-Bureau Agroéquipement. La valeur de
            
            TauxGlobal e est choisie en fonction du type de matériel (Traction, Automoteur, outil, et des variables
            DonnéeAmmortissement 1 et DonnéeAmmortissement 2).
            
            Attention /!\ Pour le matériel d’irrigation, la charge fixe de l’équipement est plus simplement la
            variable « chargesFixesParUniteDeVolumeDeTravailAnnuel » du référentiel « Matériel (BCMA)
            Irrigation », correspondant à l’identifiant du matériel (Type matériel 1, 2, 3, 4 et « unitésParAn »)
             Si « chargesFixesParUniteDeVolumeDeTravailAnnuelUnite » = €/ha
              alors CMréelles fixes e = chargesFixesParUniteDeVolumeDeTravailAnnuel
             Si « chargesFixesParUniteDeVolumeDeTravailAnnuelUnite » = €/m3
              alors CMréelles fixes e = chargesFixesParUniteDeVolumeDeTravailAnnuel * 10 * Quantité moy.
              où Quantité moy. est le volume d’eau d’irrigation de l’intervention en mm
            
            
            Utilisation e (ha/an) : Utilisation annuelle de l’équipement e, e appartenant à la liste des
            équipements (traction, outils) de la combinaison d’outils utilisée dans l’intervention i.
            
            Actuellement, l’utilisateur peut choisir parmi 3 valeurs d’utilisation annuelle « approximative »
            proposées par le BCMA. La nouveauté proposée ici est que l’utilisateur puisse, après son choix parmi
            les 3 valeurs BCMA (« utilisation annuelle approximative »), préciser la valeur d’utilisation annuelle
            du matériel qu’il a estimé avec l’agriculteur (« utilisation annuelle précise »), EN UTILISANT LA MÊME
            UNITE.
            
        */

        // exemple pour 1 matériel:
        // (Prix d'achat (Euros) du tracteur * (0.01 * Tx amortissement global (%) du tracteur) / (Utilisation réelle (estimée) du tracteur * Tx amortissement global (%) du matériel))
        //     + (Prix d'achat (Euros) Materiel * 0.01 * Tx amortissement global (%) du matériel) / Utilisation réelle (estimée) du materiel))

        // Tx amortissement global (%) (TauxGlobal):  est choisie dans le référentiel RefAgsAmortissement en fonction du type de matériel
        // (Traction, Automoteur, outil, et des variables Donnéeamortissement 1 et Donnéeamortissement 2).

        Optional<Equipment> tractor = Optional.ofNullable(tc.getTractor());
        Optional<RefMateriel> tractorMateriel = tractor.map(Equipment::getRefMateriel);
        Collection<Equipment> equipments = tc.getEquipments().stream()
                .filter(equipment -> equipment.getRefMateriel() instanceof RefMaterielOutil)
                .collect(Collectors.toList());
        boolean isPressInter = equipments.stream()
                .anyMatch(equipment -> TYPE_MAT_PRESSES.equalsIgnoreCase(equipment.getRefMateriel().getTypeMateriel1()));

        // donnée issus du fichier de test
        // = (E8*I8/ (D8*L9) ) + (E9*0,01*I9 / D9)
        // = ( part0    /  part1  ) + (   part2   / part3 )

        // part0: E8*I8: Prix d'achat (Euros) du tracteur * 0.01 * Tx amortissement global (%) du tracteur
        // part1: D8*L9:      Utilisation réelle (estimée) du tracteur * Débit chantier (Ha/h) du matériel
        // part2: E9*0.01*I9: prixMoyenAchat Prix d'achat (Euros) du matériel * Tx ammortissement global (%) du matériel
        // part3: D9:         Utilisation réelle (estimée) du matériel


        double tractorDeprecationRateValue;

        Pair<Double, String> tractorCostUnit = tractorMateriel.map(this::getTractorCostAndUnit).orElse(null);

        if (tractorCostUnit == null || tractorCostUnit.getLeft() == null || !PURCHASE_COST_UNIT.equals(tractorCostUnit.getRight())) {
            return 0.0d;
        }

        RefAgsAmortissement tractorDeprecationRate = deprecationRateByEquipments.get(tractorMateriel.orElse(null));// Tx amortissement global (%) du tracteur

        tractorDeprecationRateValue = tractorDeprecationRate == null ? 0 : tractorDeprecationRate.getTauxGlobal();

        //Si « unité » (associé au champ « UnitéParAn » dans le référentiel Matériel (BCMA)) est ha
        //>> pas de problème
        //Si « unité » (associé au champ « UnitéParAn » dans le référentiel Matériel (BCMA)) est h
        //>> transformer Utilisation e en multipliant la valeur par le débit de chantier de l’intervention
        //exprimé en ha/h (ou en divisant la valeur par le débit de chantier de l’intervention exprimé en
        //ha/h)
        //si unité de débit=Voy/h (principalement outils de fertilisation organique), et si on connaît la quantité d'engrais apportée
        //alors, Utilisation = Utilisation réelle (en Voy) x Volume par Voyage en t / Qté apportée en t/ha
        //avec si Unité de volume par voyage = m3/Voyage, alors Volume par Voyage en t = Volume par voyage en m3/voy
        //si Unité de volume par voyage = kg/Voyage, alors Volume par Voyage en t = 0.001 x Volume par voyage en kg/voy
        //si Unité de Qté d'engrais apporté pour l'intervention = m3/ha, alors Qté apportée en t/ha = Qté apportée en m3/ha
        //si Unité de Qté d'engrais apporté pour l'intervention = kg/ha, alors Qté apportée en t/ha = 0.001 x Qté apportée en kg/ha
        //si unité de débit=Bal/h (principalement outils de pressage), alors Utilisation = Utilisation réelle (en Bal) / Nb Bal/ha

        //Pour mémoire, s’il y a plusieurs outils dans la combinaison d’outil standardisée, on retient le
        //débit de chantier le plus lent = le plus élevé en h/ha = le plus faible en ha/h

        final RefMateriel refMateriel = tractorMateriel.get();
        final String unite = refMateriel.getUnite();
        final boolean isUnitParHa = unite.equals("ha");
        final boolean isUnitPerYearHours = unite.equals("heure") || unite.equals("he") || unite.equals("h");
        final boolean isUnitPerVoy = unite.contains("Voy");

        double part0 = 1;
        Double tratorFactor = null;
        double equipmentFactor = 1.0d;
        if (isUnitParHa) {
            tratorFactor = 1.0d;
        } else if (equipmentsUsage != 0 && ((isUnitPerYearHours && !isPressInter) || isUnitPerVoy)) {
            tratorFactor = 1 / equipmentsUsage;
        } else if (isPressInter && nbBalls_ha != null && equipmentsUsage != 0) {
            part0 = equipmentsUsage;
            tratorFactor = 1.0d;
            equipmentFactor = 1 / equipmentsUsage;
        }

        if (tratorFactor == null) {

            final boolean isTypePresses = TYPE_MAT_PRESSES.equalsIgnoreCase(refMateriel.getTypeMateriel1());
            if (isTypePresses) {
                incrementAngGetTotalFieldCounterForTargetedId(interventionId);
                if (!MaterielWorkRateUnit.BAL_H.equals(workRateUnit)) {
                    addMissingFieldMessage(interventionId, messageBuilder.getMissingBalHourWorkRateMessage());
                }
            }
            return null;// can't be computed
        }

        part0 = part0 * tractorCostUnit.getLeft() * tractorDeprecationRateValue;

        double part1 = tratorFactor * getMaterialUsage(tractor.get(), equipmentUsageRanges, interventionId);

        double tractorCost = part1 == 0 ? 0 : (part0 / part1);// can't be computed

        return tractorCost + equipmentFactor * getEquipmentsCost(irrigAction, interventionId, deprecationRateByEquipments, equipments, equipmentUsageRanges, tc, organicProductsTotalQuantity, organicProductFirstUnit);
    }

    private double getEquipmentsCost(Optional<IrrigationAction> optionalIrrigationAction, String interventionId, Map<RefMateriel, RefAgsAmortissement> deprecationRateByEquipments, Collection<Equipment> equipments, Collection<EquipmentUsageRange> equipmentUsageRanges, ToolsCoupling tc, Double organicProductsTotalQuantity, OrganicProductUnit organicProductFirstUnit) {
        double result = 0.0d;

        for (Equipment equipment : equipments) {

            double equipmentResult;

            if (equipment instanceof RefMaterielIrrigation) {

                equipmentResult = getIrrigEquipmentCost(optionalIrrigationAction, interventionId, (RefMaterielIrrigation) equipment);

            } else {

                equipmentResult = getToolsEquipmentCost(deprecationRateByEquipments, equipment, equipmentUsageRanges, interventionId, tc, organicProductsTotalQuantity, organicProductFirstUnit);
            }

            result += equipmentResult;
        }
        return result;
    }

    private double getToolsEquipmentCost(Map<RefMateriel, RefAgsAmortissement> deprecationRateByEquipments, Equipment equipment, Collection<EquipmentUsageRange> equipmentUsageRanges, String interventionId, ToolsCoupling tc, Double organicProductsTotalQuantity, OrganicProductUnit organicProductFirstUnit) {
        double equipmentResult = 0.0d;

        final RefMaterielOutil refEquipment = (RefMaterielOutil) equipment.getRefMateriel();

        if (refEquipment == null) {
            return equipmentResult;
        }

        String prixAchatUnite = refEquipment.getPrixNeufUnite().toLowerCase().trim();

        RefAgsAmortissement refDeprecationRate = deprecationRateByEquipments.get(refEquipment);// Tx amortissement global (%) du tracteur

        if (!prixAchatUnite.equals(PURCHASE_COST_UNIT) || refDeprecationRate == null) {
            return equipmentResult;
        }

        double purchasePrice = refEquipment.getPrixMoyenAchat();

        double part2 = purchasePrice * refDeprecationRate.getTauxGlobal();
        double part3 = getMaterialUsage(equipment, equipmentUsageRanges, interventionId);

        double factor = 1.0d;
        if (refEquipment.getUnite()!= null && VOY.equals(refEquipment.getUnite()) &&
                tc.getWorkRateUnit() != null && VOY_H.equals(tc.getWorkRateUnit()) &&
                organicProductsTotalQuantity != null) {
            if (tc.getTransitVolume() != null) {
                factor = tc.getTransitVolume() / organicProductsTotalQuantity;
            } else {
                if (organicProductFirstUnit != null) {
                    factor = DEFAULT_TRANSIT_VOLUME.get(organicProductFirstUnit) / organicProductsTotalQuantity;
                }
            }
        }

        equipmentResult = (part3 <= -Double.MAX_VALUE || part3 == 0) ? 0 : (part2 / (part3 * factor));
        return equipmentResult;
    }

    protected Pair<Double, String> getTractorCostAndUnit(RefMateriel tractorMateriel) {

        Double tractorPurchasePrice = null;
        String prixNeufUnite = null;

        if (tractorMateriel instanceof RefMaterielTraction) {
            prixNeufUnite = tractorMateriel.getPrixNeufUnite();
            tractorPurchasePrice = tractorMateriel.getPrixMoyenAchat();

        } else if (tractorMateriel instanceof RefMaterielAutomoteur) {
            prixNeufUnite = tractorMateriel.getPrixNeufUnite();
            tractorPurchasePrice = tractorMateriel.getPrixMoyenAchat();
        }

        String prixAchatUnite = StringUtils.isNotBlank(prixNeufUnite) ? prixNeufUnite.toLowerCase().trim() : null;

        return Pair.of(tractorPurchasePrice, prixAchatUnite);
    }

    private double getIrrigEquipmentCost(Optional<IrrigationAction> optionalIrrigationAction, String interventionid, RefMaterielIrrigation tractorMateriel) {
        double cost = 0.0d;
        String unite = tractorMateriel.getChargesFixesParUniteDeVolumeDeTravailAnnuelUnite();
        if (IRRIG_ANUAL_COST_UNIT_EURO_HA.equals(unite)) {
            incrementAngGetTotalFieldCounterForTargetedId(interventionid);
            cost = tractorMateriel.getChargesFixesParUniteDeVolumeDeTravailAnnuel();
        } else if (IRRIG_ANUAL_COST_UNIT_EURO_M_3.equals(unite) && optionalIrrigationAction.isPresent()) {
            incrementAngGetTotalFieldCounterForTargetedId(interventionid);
            cost = tractorMateriel.getChargesFixesParUniteDeVolumeDeTravailAnnuel() * 10 * optionalIrrigationAction.get().getWaterQuantityAverage();
        } else if (optionalIrrigationAction.isEmpty()) {
            incrementAngGetTotalFieldCounterForTargetedId(interventionid);
            addMissingFieldMessage(interventionid, messageBuilder.getIrrigationMessage());
        } else {
            addMissingRefAnnualWorkAmountMessage(interventionid);
        }
        return cost;
    }

    private double getMaterialUsage(Equipment equipment, Collection<EquipmentUsageRange> equipmentUsageRanges, String interventionId) {
        //                                  Utilisation annuelle approximative
        //                                     Utilisation annuelle précise
        //                                                  |
        //                           La valeur précise est plus faible que la plus faible
        //                           des 3 valeurs d'utilisation proposée par le BCMA
        //                                                  |
        //                      ------------------------------------------------------
        //                     Oui                                                  Non
        //                      |                                                     |
        //          Matériel appartenant à une                           Matériel appartenant à une
        //                  ETA/CUMA ?                                            ETA/CUMA ?
        //                      |                                                     |
        //        -----------------------------                         -----------------------------
        //        |                           |                         |                           |
        //       Oui                         Non                       Oui                        Non
        //        |                           |                         |                           |
        // Utilisation e = valeur    Utilisation e = valeur    Utilisation e = valeur          Utilisation e =
        //  la plus élevée du         la plus faible du          la plus élevée du         Utilisation annuelle
        //     référentiel               référentiel                référentiel             précise déclarée par
        //                                                                                     l'utilsiateur (en

        double equipmentUsage = 0.0d;

        incrementAngGetTotalFieldCounterForTargetedId(interventionId);
        Double preciseUsageValue = equipment.getRealUsage();
        if (preciseUsageValue == null) {
            addMissingFieldMessage(interventionId, messageBuilder.getManualToolUsageMessage(equipment));
        }

        final RefMateriel refMateriel = equipment.getRefMateriel();

        if (refMateriel == null) {
            return equipmentUsage;
        }

        Optional<EquipmentUsageRange> optionalEquipmentUsageRange = equipmentUsageRanges.stream().filter(equipmentUsageRange -> refMateriel.equals(equipmentUsageRange.getRefMateriel())).findAny();

        if (optionalEquipmentUsageRange.isPresent()) {
            EquipmentUsageRange equipmentUsageRange = optionalEquipmentUsageRange.get();
            double lowestRefUsage = equipmentUsageRange.getLowestValueByHa();
            double higerRefUsage = equipmentUsageRange.getHighestValueByHa();

            if (lowestRefUsage == Double.MAX_VALUE && higerRefUsage == -Double.MAX_VALUE) {
                if (-Double.MAX_VALUE != equipmentUsageRange.getHighestValueByH() && Double.MAX_VALUE != equipmentUsageRange.getLowestValueByH()) {
                    lowestRefUsage = equipmentUsageRange.getLowestValueByH();
                    higerRefUsage = equipmentUsageRange.getHighestValueByH();

                } else if (-Double.MAX_VALUE != equipmentUsageRange.getHighestValueByVoy() && Double.MAX_VALUE != equipmentUsageRange.getLowestValueByVoy()) {
                    lowestRefUsage = equipmentUsageRange.getLowestValueByVoy();
                    higerRefUsage = equipmentUsageRange.getHighestValueByVoy();

                } else if (-Double.MAX_VALUE != equipmentUsageRange.getHighestValueByBall() && Double.MAX_VALUE != equipmentUsageRange.getLowestValueByBall()) {
                    lowestRefUsage = equipmentUsageRange.getLowestValueByBall();
                    higerRefUsage = equipmentUsageRange.getHighestValueByBall();
                }
            }

            if (preciseUsageValue != null && preciseUsageValue < lowestRefUsage) {
                if (equipment.isMaterielETA()) {
                    equipmentUsage = higerRefUsage; // e = valeur la plus élevée du référentiel
                } else {
                    equipmentUsage = lowestRefUsage; // e = valeur la plus faible du référentiel
                }
            } else {
                // e = utilisation annuelle approximative
                if (equipment.isMaterielETA()) {
                    equipmentUsage = higerRefUsage; // e = valeur la plus élevée du référentiel
                } else
                    equipmentUsage = Objects.requireNonNullElseGet(preciseUsageValue, refMateriel::getUniteParAn); // e = la valeur précise
            }

        }

        return equipmentUsage;
    }

    protected double getOilCostForMaterial(ToolsCoupling tc, double equipmentsUsage_H_HA) {
        
        /*
        Charges huile_réelle i = ∑ Charges huile_réelle e
                                 e
        Charges huile_réelle e (€/ha) : charges de lubrifiant de l’équipement e (uniquement tracteurs et
        automoteurs) de la combinaison d’outils utilisée dans l’intervention i.
        Donnée issue du référentiel Matériels (colonne « lubrifiantParUniteDeTravail»).
        
        Attention, les charges de lubrifiant ne sont pas toujours exprimées en €/ha. Si « lubrifiantCoutUnite »
        est en €/h, transformer en €/ha en divisant « lubrifiantParUniteDeTravail » par le débit de chantier de
        l’intervention en ha/h (ou en multipliant par le débit de chantier en h/ha).
        
        Si « lubrifiantCoutUnite » est dans une autre unité, alors on considère que les charges de lubrifiants
        sont nulles (il y a de fortes chances qu’on soit dans le cas CMréelles i = PSCi × 40)
         */

        RefMateriel materiel = Optional.ofNullable(tc.getTractor())
                .map(Equipment::getRefMateriel)
                .orElse(null);

        double cost = 0.0d;
        String costUnit = null;

        if (materiel instanceof RefMaterielAutomoteur) {

            cost = ((RefMaterielAutomoteur) materiel).getLubrifiantParUniteDeTravail();
            costUnit = ((RefMaterielAutomoteur) materiel).getLubrifiantCoutUnite();

        } else if (materiel instanceof RefMaterielTraction) {

            cost = ((RefMaterielTraction) materiel).getLubrifiantParUniteDeTravail();
            costUnit = ((RefMaterielTraction) materiel).getLubrifiantCoutUnite();

        }

        double factor;
        if (OIL_COST_EURO_HA.equals(costUnit)) {
            factor = 1d;
        } else if (OIL_COST_EURO_H.equals(costUnit)) {
            factor = equipmentsUsage_H_HA;
        } else {
            factor = ZERO;
        }
        return cost * factor;
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
        computeReal = displayed && filter.getComputeReal();
        computeStandardised = displayed && filter.getComputeStandardized();
    }
}
