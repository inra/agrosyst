package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Charges de main d'œuvre manuelle.
 * <p>
 * Les charges de main d’œuvre manuelles sont exprimées en €/ha. Elles sont calculées à toutes les échelles hormis l'intrant et concernent les interventions manuelles (aucune combinaison d’outils mobilisée). Elles seront à utilisées dans le calcul des Charges de main d'œuvre totales.
 * <p>
 * Il est calculé suivant la formule suivante :
 * Charges de main d'œuvre manuelle 𝑖 = Temps de travail manuel 𝑖 × Coût horaire manuel
 * <p>
 * Avec :
 * Charges de main d'œuvre manuelle 𝑖 (€/ha) : Charges de main d'œuvre manuelle de l’intervention i.
 * Temps de travail manuel 𝑖 (h/ha) : Temps de travail manuel de l’intervention i. Indicateur calculé par Agrosyst.
 * Coût horaire manuel (€/h) : Coût horaire pour les travaux manuels, toutes charges comprises soit 14€/h.
 *
 * @author Geoffroy Gley (gley@codelutin.com)
 */
public class IndicatorManualWorkforceExpenses extends AbstractIndicator {

    public static final String CHARGES_MANUELLES_REELLES_TX_COMP = "charges_main_oeuvre_manuelle_reelles_taux_de_completion";
    public static final String CHARGES_MANUELLES_STD_MIL_TX_COMP = "charges_main_oeuvre_manuelle_std_mil_taux_de_completion";
    public static final String CHARGES_MANUELLES_REELLES_DETAIL = "charges_main_oeuvre_manuelle_reelles_detail_champs_non_renseig";
    public static final String CHARGES_MANUELLES_STD_MIL_DETAIL = "charges_main_oeuvre_manuelle_std_mil_detail_champs_non_renseig";

    public static final double STANDARD_MANUAL_WORK_COST_VALUE = 14.0d;
    protected static final Double DEFAULT_REAL_VALUE = 0.0d;
    protected static final Double DEFAULT_STANDARDIZED_VALUE = 0.0d;
    protected static final Pair<Double, Double> DEFAULT_REAL_AND_STANDARDIZED_VALUES = Pair.of(DEFAULT_REAL_VALUE, DEFAULT_STANDARDIZED_VALUE);

    protected boolean computeReal = true;
    protected boolean computeStandardised = true;

    protected static final String[] LABELS = {
            "Indicator.label.manualWorkforceExpensesReal",
            "Indicator.label.manualWorkforceExpensesStandard"
    };

    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    public IndicatorManualWorkforceExpenses() {
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_MANUELLES_REELLES_TX_COMP,
                CHARGES_MANUELLES_REELLES_TX_COMP
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_MANUELLES_REELLES_DETAIL,
                CHARGES_MANUELLES_REELLES_DETAIL
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_MANUELLES_STD_MIL_TX_COMP,
                CHARGES_MANUELLES_STD_MIL_TX_COMP
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.CHARGES_MANUELLES_STD_MIL_DETAIL,
                CHARGES_MANUELLES_STD_MIL_DETAIL
        );
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_ECONOMIC);
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return displayed && (i == 0 && computeReal || i == 1 && computeStandardised);
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }
    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = new HashMap<>();

        indicatorNameToColumnName.put(getIndicatorLabel(0), "charges_main_oeuvre_manuelle_reelles");
        indicatorNameToColumnName.put(getIndicatorLabel(1), "charges_main_oeuvre_manuelle_std_mil");

        indicatorNameToColumnName.put(CHARGES_MANUELLES_REELLES_TX_COMP, CHARGES_MANUELLES_REELLES_TX_COMP);
        indicatorNameToColumnName.put(CHARGES_MANUELLES_STD_MIL_TX_COMP, CHARGES_MANUELLES_STD_MIL_TX_COMP);
        indicatorNameToColumnName.put(CHARGES_MANUELLES_REELLES_DETAIL, CHARGES_MANUELLES_REELLES_DETAIL);
        indicatorNameToColumnName.put(CHARGES_MANUELLES_STD_MIL_DETAIL, CHARGES_MANUELLES_STD_MIL_DETAIL);

        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {
        PracticedIntervention intervention = interventionContext.getIntervention();
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId());
        if (interventionContext.isFictive()) return newArray(LABELS.length, 0.0d);
        Double manualWorkTime = interventionContext.getManualWorkTime();
        Pair<Double, Double> manualExpenses = computeManualExpenses(manualWorkTime, domainContext.getManualWorkforceCost(), intervention.getTopiaId());
        interventionContext.setManualWorkforceExpenses(manualExpenses);
        return newResult(manualExpenses.getLeft(), manualExpenses.getRight());
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {
        EffectiveIntervention intervention = interventionContext.getIntervention();
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId());
        Double manualWorkTime = interventionContext.getManualWorkTime();
        Pair<Double, Double> manualExpenses = computeManualExpenses(manualWorkTime, domainContext.getManualWorkforceCost(), intervention.getTopiaId());
        interventionContext.setManualWorkforceExpenses(manualExpenses);
        return newResult(manualExpenses.getLeft(), manualExpenses.getRight());
    }

    private Pair<Double, Double> computeManualExpenses(Double manualWorkTime, double realManualWorkCostValue, String interventionId) {
        double realManualExpenses = DEFAULT_REAL_VALUE;
        double standardizedManualExpenses = DEFAULT_STANDARDIZED_VALUE;
        if (manualWorkTime != null) {
            realManualExpenses = manualWorkTime * realManualWorkCostValue;
            standardizedManualExpenses = manualWorkTime * STANDARD_MANUAL_WORK_COST_VALUE;
        } else {
            addMissingIndicatorMessage(interventionId, messageBuilder.getMissingManualWorkTimeMessage());
        }
        return Pair.of(realManualExpenses, standardizedManualExpenses);
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
        computeReal = displayed && filter.getComputeReal();
        computeStandardised = displayed && filter.getComputeStandardized();
    }
}
