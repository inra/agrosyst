package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCountryTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLocation;

import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author kmorin : kmorin@codelutin.com
 * @since 0.8
 */
public class RefLocationCommunesFranceImportModel extends InternationalizationReferentialModel<RefLocationDto> {

    public RefLocationCommunesFranceImportModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("codeInsee", RefLocation.PROPERTY_CODE_INSEE);
        newMandatoryColumn("commune", RefLocation.PROPERTY_COMMUNE);
        newMandatoryColumn("region", RefLocation.PROPERTY_REGION, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("departement", RefLocation.PROPERTY_DEPARTEMENT);
        newMandatoryColumn("codePostal", RefLocation.PROPERTY_CODE_POSTAL);
        newMandatoryColumn("petiteRegionAgricoleCode", RefLocation.PROPERTY_PETITE_REGION_AGRICOLE_CODE);
        newMandatoryColumn("petiteRegionAgricoleNom", RefLocation.PROPERTY_PETITE_REGION_AGRICOLE_NOM);
        newMandatoryColumn("latitude", RefLocation.PROPERTY_LATITUDE, DOUBLE_PARSER);
        newMandatoryColumn("longitude", RefLocation.PROPERTY_LONGITUDE, DOUBLE_PARSER);
        newOptionalColumn("altitudeMoyenne", RefLocation.PROPERTY_ALTITUDE_MOY, INTEGER_WITH_NULL_PARSER);
        newOptionalColumn("aireAttraction", RefLocation.PROPERTY_AIRE_ATTR);
        newOptionalColumn("arrondissementCode", RefLocation.PROPERTY_ARRONDISSEMENT_CODE);
        newOptionalColumn("bassinVie", RefLocation.PROPERTY_BASSIN_VIE);
        newOptionalColumn("intercommunaliteCode", RefLocation.PROPERTY_INTERCOMMUNALITE_CODE);
        newOptionalColumn("uniteUrbaine", RefLocation.PROPERTY_UNITE_URBAINE);
        newOptionalColumn("zoneEmploi", RefLocation.PROPERTY_ZONE_EMPLOI);
        newOptionalColumn("bassinViticole", RefLocation.PROPERTY_BASSIN_VITICOLE);
        newOptionalColumn("regionPre2016", RefLocation.PROPERTY_ANCIENNE_REGION);
        newMandatoryColumn(COLUMN_ACTIVE, ACTIVE_PARSER);
        newIgnoredColumn("pays");
        newIgnoredColumn("GEOFLA2015_id");
        newIgnoredColumn("temp_checking");
        newIgnoredColumn("ex_topiaid");
    }

    @Override
    public RefLocationDto newEmptyInstance() {
        RefLocationDto instance = new RefLocationDto();
        instance.setPays(Locale.FRANCE.getISO3Country().toLowerCase());
        return instance;
    }

}
