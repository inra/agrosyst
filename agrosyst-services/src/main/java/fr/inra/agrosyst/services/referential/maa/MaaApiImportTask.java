package fr.inra.agrosyst.services.referential.maa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.async.AbstractTask;
import lombok.Getter;

/**
 * Classe qui permet de représenter une demande d'import de l'api maa
 */
@Getter
public class MaaApiImportTask extends AbstractTask {

    protected final int campaign;
    protected final Integer numeroAmmIdMetier;

    public MaaApiImportTask(String userId, String userEmail, int campaign, Integer numeroAmmIdMetier) {
        super(userId, userEmail);
        this.campaign = campaign;
        this.numeroAmmIdMetier = numeroAmmIdMetier;
    }

    @Override
    public String getDescription() {
        return String.format("Import des données de l'API MAA pour la campagne %s", campaign);
    }

    @Override
    public boolean mustBeQueued() {
        return true;
    }

}
