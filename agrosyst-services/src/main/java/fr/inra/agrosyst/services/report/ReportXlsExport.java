/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.services.report;

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.report.ArboAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropPestMaster;
import fr.inra.agrosyst.api.entities.report.ArboPestMaster;
import fr.inra.agrosyst.api.entities.report.CropPestMaster;
import fr.inra.agrosyst.api.entities.report.FoodMaster;
import fr.inra.agrosyst.api.entities.report.PestMaster;
import fr.inra.agrosyst.api.entities.report.PestPressure;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.entities.report.ReportRegional;
import fr.inra.agrosyst.api.entities.report.SectorSpecies;
import fr.inra.agrosyst.api.entities.report.VerseMaster;
import fr.inra.agrosyst.api.entities.report.VitiPestMaster;
import fr.inra.agrosyst.api.entities.report.YieldInfo;
import fr.inra.agrosyst.api.entities.report.YieldLoss;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.BinaryContent;
import fr.inra.agrosyst.api.services.report.ReportGrowingSystemCollections;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.performance.PoiIndicatorWriter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

/**
 * All stuff related to Report XLS export.
 */
@RequiredArgsConstructor
public class ReportXlsExport {

    protected final Map<String, String> groupesCiblesParCode;

    public BinaryContent exportXlsReportRegionals(List<ReportRegional> reportRegionalsToExport) {
        return exportXlsGeneric(reportRegionalsToExport, this::exportXlsReportRegionals);
    }

    public BinaryContent exportXlsReportGrowingSystems(List<ReportGrowingSystem> reportGrowingSystemsToExport) {
        return exportXlsGeneric(reportGrowingSystemsToExport, this::exportXlsReportGrowingSystems);
    }

    /**
     * Prepare input stream and call callback to fill WorkBook related to it.
     *
     * @param entities entities for callback
     * @param callback callback
     * @param <T> entity types
     * @return input stream of XLS file
     */
    protected <T> BinaryContent exportXlsGeneric(List<T> entities, BiConsumer<Workbook, List<T>> callback) {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream(); Workbook workbook = new XSSFWorkbook()) {
        
            // callback
            callback.accept(workbook, entities);
        
            // autosize columns
            for (int s = 0; s < workbook.getNumberOfSheets(); s++) {
                Sheet sheet = workbook.getSheetAt(s);
                Row row = sheet.getRow(0);
                for (int column = 0; column < row.getPhysicalNumberOfCells(); column++) {
                    sheet.autoSizeColumn(column);
                }
            }
        
            // write
            workbook.write(outputStream);

            return BinaryContent.forXlsx(outputStream.toByteArray());
        } catch (IOException ex) {
            throw new AgrosystTechnicalException("can't export", ex);
        }
    }

    protected void exportXlsReportRegionals(Workbook workbook, List<ReportRegional> reportRegionalsToExport) {
        exportReportRegionalMain(workbook, reportRegionalsToExport);
        exportReportRegionalHighlights(workbook, reportRegionalsToExport);
    }

    protected void exportXlsReportGrowingSystems(Workbook workbook, List<ReportGrowingSystem> reportGrowingSystemsToExport) {
        List<ReportGrowingSystem> expeReportGrowingSystems = reportGrowingSystemsToExport.stream().filter(reportGrowingSystem -> {
            GrowingPlan growingPlan = reportGrowingSystem.getGrowingSystem().getGrowingPlan();
            return growingPlan != null && TypeDEPHY.DEPHY_EXPE.equals(growingPlan.getType());
        }).collect(Collectors.toList());
        boolean hasExpeGS = !expeReportGrowingSystems.isEmpty();

        List<ReportGrowingSystem> notExpeReportGrowingSystems = ListUtils.removeAll(reportGrowingSystemsToExport, expeReportGrowingSystems);
        boolean hasNotExpeGS = !notExpeReportGrowingSystems.isEmpty();

        exportReportGrowingSystemMain(workbook, reportGrowingSystemsToExport);
        exportReportGrowingSystemArbo(workbook, reportGrowingSystemsToExport);

        if (hasNotExpeGS) {
            exportReportGrowingSystemAll(workbook, notExpeReportGrowingSystems);
        }
        if (hasExpeGS) {
            exportReportGrowingSystemExpe(workbook, expeReportGrowingSystems);
        }

        exportReportGrowingSystemViti(workbook, reportGrowingSystemsToExport);
    }

    protected void addHeaders(Workbook workbook, Sheet sheet, String... headers) {
        Row row0 = sheet.createRow(0);
        CellStyle style = workbook.createCellStyle(); // bold
        Font font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);
        int header = 0;
        for (String displayName : headers) {
            Cell cell0 = row0.createCell(header++);
            cell0.setCellValue(displayName);
            cell0.setCellStyle(style);

        }
    }

    protected void writeToSheet(Sheet sheet, Object... columns) {
        // new line
        int line = sheet.getPhysicalNumberOfRows();
        if (line > PoiIndicatorWriter.MAXIMUM_NOMBER_OF_ROWS) {
            return;
        }
        Row row = sheet.createRow(line);

        int column = 0;

        // dynamic cell
        for (Object dynColumn : columns) {
            Cell cellx = row.createCell(column++);
            if (dynColumn == null || "null".equals(dynColumn)) {
                cellx.setCellValue((String) null);
            } else if (dynColumn instanceof Number) {
                cellx.setCellValue(((Number) dynColumn).doubleValue());
            } else {
                cellx.setCellValue(String.valueOf(dynColumn));
            }
        }
    }

    protected void exportReportRegionalMain(Workbook workbook, List<ReportRegional> reportRegionalsToExport) {
        Sheet mainSheet = workbook.createSheet("Généralités");
        addHeaders(workbook, mainSheet,
                "ID du Bilan de campagne",
                "Rédacteur du bilan de campagne",
                "Nom du Bilan de campagne",
                "Réseau",
                "Campagne",
                "Faits marquants de l’année (climat, bioagresseurs…)",
                "Pluviométrie : nombre de mm du 1er mars au 30 juin",
                "Nombre de jours de pluie du 1er mars au 30 juin",
                "Pluviométrie : nombre de mm du 1er juillet au 31 octobre",
                "INOKI : nombre de contaminations primaires",
                "INOKI : nombre de jours avec contaminations primaires",
                "RIM Pro : nombre de contaminations secondaires (fruit)",
                "RIM Pro : somme des RIM sur l’année",
                "Pluviométrie (Tropical) : nombre de mm du 1er janvier au 31 mars",
                "Nombre de jours de pluie (Tropical) du 1er janvier au 31 mars",
                "Pluviométrie (Tropical) : nombre de mm du 1er avril au 30 juin",
                "Nombre de jours de pluie (Tropical) du 1er avril au 30 juin",
                "Pluviométrie (Tropical) : nombre de mm du 1er juillet au 30 septembre",
                "Nombre de jours de pluie (Tropical) du 1er juillet au 30 septembre",
                "Pluviométrie (Tropical) : nombre de mm du 1er octobre au 31 décembre",
                "Nombre de jours de pluie (Tropical) du 1er octobre au 31 décembre");
        for (ReportRegional reportRegional : reportRegionalsToExport) {
            if (reportRegional.getSectors().contains(Sector.ARBORICULTURE) && reportRegional.getSectorSpecies().contains(SectorSpecies.POMMIER_POIRIER)) {
                writeToSheet(mainSheet,
                    reportRegional.getTopiaId(),
                    reportRegional.getAuthor(),
                    reportRegional.getName(),
                    reportRegional.getNetworks().stream()
                            .filter(Objects::nonNull)
                            .map(Network::getName)
                            .distinct()
                            .collect(Collectors.joining(",")),
                    reportRegional.getCampaign(),
                    reportRegional.getHighlights(),
                    reportRegional.getRainfallMarchJune(),
                    reportRegional.getRainyDaysMarchJune(),
                    reportRegional.getRainfallJulyOctober(),
                    reportRegional.getPrimaryContaminations(),
                    reportRegional.getNumberOfDaysWithPrimaryContaminations(),
                    reportRegional.getSecondaryContaminations(),
                    reportRegional.getRimSum(),
                    reportRegional.getTropicalRainfallJanuaryMarch(),
                    reportRegional.getTropicalRainyDaysJanuaryMarch(),
                    reportRegional.getTropicalRainfallAprilJune(),
                    reportRegional.getTropicalRainyDaysAprilJune(),
                    reportRegional.getTropicalRainfallJulySeptember(),
                    reportRegional.getTropicalRainyDaysJulySeptember(),
                    reportRegional.getTropicalRainfallOctoberDecember(),
                    reportRegional.getTropicalRainyDaysOctoberDecember()
                );
            } else {
                writeToSheet(mainSheet,
                    reportRegional.getTopiaId(),
                    reportRegional.getAuthor(),
                    reportRegional.getName(),
                    reportRegional.getNetworks().stream()
                            .filter(Objects::nonNull)
                            .map(Network::getName)
                            .distinct()
                            .collect(Collectors.joining(",")),
                    reportRegional.getCampaign(),
                    reportRegional.getHighlights(),
                    reportRegional.getRainfallMarchJune(),
                    reportRegional.getRainyDaysMarchJune(),
                    reportRegional.getRainfallJulyOctober(),
                    "",
                    "",
                    "",
                    "",
                    reportRegional.getTropicalRainfallJanuaryMarch(),
                    reportRegional.getTropicalRainyDaysJanuaryMarch(),
                    reportRegional.getTropicalRainfallAprilJune(),
                    reportRegional.getTropicalRainyDaysAprilJune(),
                    reportRegional.getTropicalRainfallJulySeptember(),
                    reportRegional.getTropicalRainyDaysJulySeptember(),
                    reportRegional.getTropicalRainfallOctoberDecember(),
                    reportRegional.getTropicalRainyDaysOctoberDecember());
            }
        }
    }

    protected void exportReportRegionalHighlights(Workbook workbook, List<ReportRegional> reportRegionalsToExport) {
        Sheet highlightSheet = workbook.createSheet("Faits marquants");
        addHeaders(workbook, highlightSheet,
                "ID du Bilan de campagne",
                "Nom du Bilan de campagne",
                "Type de pression",
                "Groupe cible",
                "Nom maladie/ravageur",
                "Cultures concernées",
                "Pression de l’année (échelle régionale)",
                "Evolution de la pression par rapport à la campagne précédente",
                "Commentaires");

        for (ReportRegional reportRegional : reportRegionalsToExport) {
            Collection<PestPressure> pestPressures = reportRegional.getPestPressures();
            if (pestPressures != null) {
                for (PestPressure pestPressure : pestPressures) {
                    writeToSheet(highlightSheet,
                            reportRegional.getTopiaId(),
                            reportRegional.getName(),
                            "ravageurs",
                            Objects.toString(groupesCiblesParCode.get(pestPressure.getCodeGroupeCibleMaa()), "N/A"),
                            pestPressure.getAgressors().stream().map(RefBioAgressor::getLabel).collect(Collectors.joining(", ")),
                            pestPressure.getCrops(),
                            AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestPressure.getPressureScale()),
                            AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestPressure.getPressureEvolution()),
                            pestPressure.getComment()
                    );
                }
            }
            Collection<PestPressure> diseasePressures = reportRegional.getDiseasePressures();
            if (diseasePressures != null) {
                for (PestPressure diseasePressure : diseasePressures) {
                    writeToSheet(highlightSheet,
                            reportRegional.getTopiaId(),
                            reportRegional.getName(),
                            "maladies",
                            Objects.toString(groupesCiblesParCode.get(diseasePressure.getCodeGroupeCibleMaa()), "N/A"),
                            diseasePressure.getAgressors().stream().map(RefBioAgressor::getLabel).collect(Collectors.joining(", ")),
                            diseasePressure.getCrops(),
                            AgrosystI18nService.getEnumTraductionWithDefaultLocale(diseasePressure.getPressureScale()),
                            AgrosystI18nService.getEnumTraductionWithDefaultLocale(diseasePressure.getPressureEvolution()),
                            diseasePressure.getComment()
                    );
                }
            }
        }
    }

    protected void exportReportGrowingSystemMain(Workbook workbook, List<ReportGrowingSystem> reportGrowingSystemsToExport) {
        Sheet mainSheet = workbook.createSheet("Généralités");
        addHeaders(workbook, mainSheet,
                "ID du Bilan de campagne",
                "Rédacteur du bilan de campagne",
                "Nom du Bilan de campagne",
                "Campagne",
                "Département",
                "Réseau de rattachement du SDC",
                "Système de culture",
                "Type de conduite du SDC",
                "Exploitation ou domaine expérimental",
                "Dispositif",
                "Numéro DEPHY",
                "Bilan de campagne / échelle régionale correspondant",
                "Méthode d’estimation des IFT déclarés",
                "Quelles sont les principales évolutions du système de culture depuis le début du suivi ?",
                "Quelles mesures spécifiques aux conditions de l’année ont été mises en place sur le système de culture ?",
                "Quels sont les faits marquants de l’année en terme de conduite des cultures et de performances techniques (rendement, qualité…) ?",
                "Quels enseignements pour améliorer le système de culture ?",
                "Commentaires – Résultats obtenus et faits marquants",
                "IFT-fongicide du système de culture (chimique) – Arbo",
                "IFT-fongicide biocontrôle du système de culture – Arbo",
                "Quantité de cuivre appliquée (Kg Cu/ha) – Arbo",
                "Niveau global de maîtrise des maladies – Arbo",
                "IFT-ravageurs du système de culture (chimique) – Arbo",
                "IFT-ravageurs biocontrôle du système de culture – Arbo",
                "Niveau global de maîtrise des ravageurs – Arbo",
                "IFT-fongicide du système de culture (chimique) – Viti",
                "IFT-fongicide biocontrôle du système de culture – Viti",
                "Quantité de cuivre appliquée (Kg Cu/ha) – Viti",
                "Niveau global de maîtrise des maladies – Viti",
                "IFT-ravageurs du système de culture (chimique) – Viti",
                "IFT-ravageurs biocontrôle du système de culture – Viti",
                "Niveau global de maîtrise des ravageurs – Viti",
                "Échelle de pression",
                "Pression des adventices  - Expression de l’agriculteur",
                "Résultats obtenus, niveau de maîtrise finale",
                "Niveau de maîtrise  - Expression de l’agriculteur",
                "Nombre de traitements herbicides chimiques (hors épamprage) – Viti",
                "Nombre de traitements herbicides biocontrôle (hors épamprage) – Viti",
                "Nombre de traitements épamprage chimiques – Viti",
                "Nombre de traitements épamprage biocontrôle – Viti",
                "IFT herbicides chimiques (hors épamprage) – Viti",
                "IFT herbicides biocontrôle (hors épamprage) – Viti",
                "IFT épamprage chimiques – Viti",
                "IFT épamprage biocontrôle – Viti",
                "L’objectif de rendement est-il atteint ? – Viti",
                "Si les objectifs de rendement ne sont pas atteints - Cause 1 – Viti",
                "Si les objectifs de rendement ne sont pas atteints - Cause 2 – Viti",
                "Si les objectifs de rendement ne sont pas atteints - Cause 3 – Viti",
                "Commentaires sur la qualité  – Viti",
                "Commentaires – Résultats obtenus et faits marquants – Viti");

        for (ReportGrowingSystem reportGrowingSystem : reportGrowingSystemsToExport) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            List<Object> args = new ArrayList<>();
            // ID du Bilan de campagne
            args.add(reportGrowingSystem.getTopiaId());
            // Rédacteur du bilan de campagne
            args.add(reportGrowingSystem.getAuthor());
            // Nom du Bilan de campagne
            args.add(reportGrowingSystem.getName());
            // Campagne
            args.add(domain.getCampaign());
            // Département
            args.add(domain.getLocation().getDepartement());
            // Réseau de rattachement du SDC
            args.add(growingSystem.isNetworksEmpty() ? "" :
                    growingSystem.getNetworks().stream()
                            .filter(Objects::nonNull)
                            .map(Network::getName)
                            .distinct()
                            .collect(Collectors.joining(", ")));
            // Système de culture
            args.add(growingSystem.getName());
            // Type de conduite du SDC
            args.add(Optional.ofNullable(growingSystem.getTypeAgriculture())
                    .map(RefTypeAgriculture::getReference_label)
                    .orElse(null));
            // Exploitation ou domaine expérimental
            args.add(growingSystem.getGrowingPlan().getDomain().getName());
            // Dispositif
            args.add(growingPlan.getName());
            // Numéro DEPHY
            args.add(growingSystem.getDephyNumber());
            // Bilan de campagne / échelle régionale correspondant
            final String reportRegionalName = getReportRegionalName(reportGrowingSystem);
            args.add(reportRegionalName);
            // Méthode d’estimation des IFT déclarés
            args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystem.getIftEstimationMethod()));
            // Quelles sont les principales évolutions du système de culture depuis le début du suivi ?
            args.add(reportGrowingSystem.getHighlightsEvolutions());
            // Quelles mesures spécifiques aux conditions de l’année ont été mises en place sur le système de culture ?
            args.add(reportGrowingSystem.getHighlightsMeasures());
            // Quels sont les faits marquants de l’année en terme de conduite des cultures et de performances techniques (rendement, qualité…) ?
            args.add(reportGrowingSystem.getHighlightsPerformances());
            // Quels enseignements pour améliorer le système de culture ?
            args.add(reportGrowingSystem.getHighlightsTeachings());
            // Commentaires – Résultats obtenus et faits marquants
            args.add(reportGrowingSystem.isYieldInfosEmpty() ? null :
                    reportGrowingSystem.getYieldInfos().stream()
                            .map(YieldInfo::getComment)
                            .collect(Collectors.joining(", ")));
            // IFT-fongicide du système de culture (chimique) – Arbo
            args.add(reportGrowingSystem.getArboChemicalFungicideIFT());
            // IFT-fongicide biocontrôle du système de culture – Arbo
            args.add(reportGrowingSystem.getArboBioControlFungicideIFT());
            // Quantité de cuivre appliquée (Kg Cu/ha) – Arbo
            args.add(reportGrowingSystem.getArboCopperQuantity());
            // Niveau global de maîtrise des maladies – Arbo
            args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystem.getArboDiseaseQualifier()));
            // IFT-ravageurs du système de culture (chimique) – Arbo
            args.add(reportGrowingSystem.getArboChemicalPestIFT());
            // IFT-ravageurs biocontrôle du système de culture – Arbo
            args.add(reportGrowingSystem.getArboBioControlPestIFT());
            // Niveau global de maîtrise des ravageurs – Arbo
            args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystem.getArboPestQualifier()));
            // IFT-fongicide du système de culture (chimique) – Viti
            args.add(reportGrowingSystem.getVitiDiseaseChemicalFungicideIFT());
            // IFT-fongicide biocontrôle du système de culture – Viti
            args.add(reportGrowingSystem.getVitiDiseaseBioControlFungicideIFT());
            // Quantité de cuivre appliquée (Kg Cu/ha) – Viti
            args.add(reportGrowingSystem.getVitiDiseaseCopperQuantity());
            // Niveau global de maîtrise des maladies – Viti
            args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystem.getVitiDiseaseQualifier()));
            // IFT-ravageurs du système de culture (chimique) – Viti
            args.add(reportGrowingSystem.getVitiPestChemicalPestIFT());
            // IFT-ravageurs biocontrôle du système de culture – Viti
            args.add(reportGrowingSystem.getVitiPestBioControlPestIFT());
            // Niveau global de maîtrise des ravageurs – Viti
            args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystem.getVitiPestQualifier()));
            // Échelle de pression
            args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystem.getVitiAdventicePressureScale()));
            // Pression des adventices  - Expression de l’agriculteur
            args.add(reportGrowingSystem.getVitiAdventicePressureFarmerComment());
            // Résultats obtenus, niveau de maîtrise finale
            args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystem.getVitiAdventiceQualifier()));
            // Niveau de maîtrise  - Expression de l’agriculteur
            args.add(reportGrowingSystem.getVitiAdventiceResultFarmerComment());
            // Nombre de traitements herbicides chimiques (hors épamprage) – Viti
            args.add(reportGrowingSystem.getVitiHerboTreatmentChemical());
            // Nombre de traitements herbicides biocontrôle (hors épamprage) – Viti
            args.add(reportGrowingSystem.getVitiHerboTreatmentBioControl());
            // Nombre de traitements épamprage chimiques – Viti
            args.add(reportGrowingSystem.getVitiSuckeringChemical());
            // Nombre de traitements épamprage biocontrôle – Viti
            args.add(reportGrowingSystem.getVitiSuckeringBioControl());
            // IFT herbicides chimiques (hors épamprage) – Viti
            args.add(reportGrowingSystem.getVitiHerboTreatmentChemicalIFT());
            // IFT herbicides biocontrôle (hors épamprage) – Viti
            args.add(reportGrowingSystem.getVitiHerboTreatmentBioControlIFT());
            // IFT épamprage chimiques – Viti
            args.add(reportGrowingSystem.getVitiSuckeringChemicalIFT());
            // IFT épamprage biocontrôle – Viti
            args.add(reportGrowingSystem.getVitiSuckeringBioControlIFT());
            // L’objectif de rendement est-il atteint ? – Viti
            args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystem.getVitiYieldObjective()));
            // Si les objectifs de rendement ne sont pas atteints - Cause 1 – Viti
            args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystem.getVitiLossCause1()));
            // Si les objectifs de rendement ne sont pas atteints - Cause 2 – Viti
            args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystem.getVitiLossCause2()));
            // Si les objectifs de rendement ne sont pas atteints - Cause 3 – Viti
            args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystem.getVitiLossCause3()));
            // Commentaires sur la qualité  – Viti
            args.add(reportGrowingSystem.getVitiYieldQuality());
            // Commentaires – Résultats obtenus et faits marquants – Viti
            args.add(reportGrowingSystem.isYieldInfosEmpty() ? null :
                    reportGrowingSystem.getYieldInfos().stream()
                            .map(YieldInfo::getComment)
                            .collect(Collectors.joining(", ")));

            writeToSheet(mainSheet, args.toArray());
        }
    }
    
    private String getReportRegionalName(ReportGrowingSystem reportGrowingSystem) {
        final ReportRegional reportRegional = reportGrowingSystem.getReportRegional();
        return reportRegional != null ? reportRegional.getName() : "";
    }
    
    protected void exportReportGrowingSystemAll(Workbook workbook, List<ReportGrowingSystem> notExpeReportGrowingSystems) {
        exportReportGrowingSystemsForSector(workbook, ReportGrowingSystemCollections.NONE_DEPHY_EXPE, "Cultures assolées", notExpeReportGrowingSystems);
    }

    protected void exportReportGrowingSystemExpe(Workbook workbook, List<ReportGrowingSystem> expeReportGrowingSystems) {
        Set<Sector> sectors = expeReportGrowingSystems.stream()
                .flatMap(reportGrowingSystem -> reportGrowingSystem.getSectors().stream())
                .filter(sector -> !Sector.ARBORICULTURE.equals(sector) && !Sector.VITICULTURE.equals(sector))
                .collect(Collectors.toSet());

        for (Sector sector : sectors) {
            String sheetName = AgrosystI18nService.getEnumTraductionWithDefaultLocale(sector) + " - Expe";
            exportReportGrowingSystemsForSector(workbook, sector, sheetName, expeReportGrowingSystems);
        }
    }

    private void exportReportGrowingSystemsForSector(Workbook workbook,
                                                     Sector sector,
                                                     String sheetName,
                                                     List<ReportGrowingSystem> reportGrowingSystems) {
        Sheet sectorSheet = workbook.createSheet(sheetName);
        addHeaders(workbook, sectorSheet,
                "ID du Bilan de campagne",
                "Nom du Bilan de campagne",
                "Campagne",
                "Département",
                "Réseau de rattachement du SDC",
                "Système de culture",
                "Type de conduite du SDC",
                "Numéro DEPHY",
                "Type de thématique",
                "Culture",
                "Espèces",
                "Variétés",
                "Groupe cible",
                "Adventice/Maladie/Ravageur",
                "Pression/risque avant traitement - Échelle de pression",
                "Pression/risque avant traitement - expression de l’agriculteur",
                "Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise",
                "Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise",
                "Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur",
                "IFT-Herbicide",
                sector == Sector.MARAICHAGE || sector == Sector.HORTICULTURE ? "IFT-Fongicide chimique" : "IFT-Fongicide",
                sector == Sector.MARAICHAGE || sector == Sector.HORTICULTURE ? "IFT-Insecticide chimique" : "IFT-Insecticide",
                "IFT-Hors Biocontrôle",
                "IFT-autres ravageurs",
                "IFT-régulateurs",
                sector == Sector.MARAICHAGE || sector == Sector.HORTICULTURE ? "Commentaire de l'expérimentateur" : "Commentaire du conseiller",
                "Irrigation",
                "Stress hydrique",
                sector == Sector.HORTICULTURE ? "Alimentation minérale" : "Azote",
                sector == Sector.HORTICULTURE ? "Alimentation minérale (commentaire)" : "Alimentation minérale (hors azote)",
                "Température et rayonnement",
                "Commentaires – Résultats obtenus et faits marquants",
                "L’objectif de rendement est-il atteint ?",
                "Si les objectifs de rendement ne sont pas atteints, cause 1",
                "Si les objectifs de rendement ne sont pas atteints, cause 2",
                "Si les objectifs de rendement ne sont pas atteints, cause 3",
                "Commentaires sur la qualité"
        );

        for (ReportGrowingSystem reportGrowingSystem : reportGrowingSystems) {
            exportReportGrowingSystemAllAdventice(sectorSheet, reportGrowingSystem, sector);
            exportReportGrowingSystemAllDisease(sectorSheet, reportGrowingSystem, sector);
            exportReportGrowingSystemAllPest(sectorSheet, reportGrowingSystem, sector);
            exportReportGrowingSystemAllVerse(sectorSheet, reportGrowingSystem, sector);
            exportReportGrowingSystemAllFood(sectorSheet, reportGrowingSystem, sector);
            exportReportGrowingSystemAllYield(sectorSheet, reportGrowingSystem, sector);
        }
    }

    private void exportReportGrowingSystemAllAdventice(Sheet allSheet, ReportGrowingSystem reportGrowingSystem, Sector sector) {
        Collection<CropPestMaster> cropAdventiceMasters = reportGrowingSystem.getCropAdventiceMasters();
        if (cropAdventiceMasters != null) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            cropAdventiceMasters = cropAdventiceMasters.stream()
                    .filter(cropPestMaster -> Objects.equals(cropPestMaster.getSector(), sector))
                    .toList();
            for (CropPestMaster cropAdventiceMaster : cropAdventiceMasters) {
                Collection<CroppingPlanEntry> crops = cropAdventiceMaster.getCrops();

                for (PestMaster pestMaster : cropAdventiceMaster.getPestMasters()) {
                    List<Object> args = new ArrayList<>();
                    exportCommonData(args, reportGrowingSystem, growingSystem, domain, "Adventices", crops);
                    // Groupe cible
                    args.add("");
                    // Adventice/Maladie/Ravageur (champ vide pour les autres thématiques)
                    RefBioAgressor agressor = pestMaster.getAgressor();
                    args.add(agressor != null ? agressor.getLabel() : "N/A");
                    // Pression/risque avant traitement - Échelle de pression
                    if (pestMaster.getPressureScaleInt() != null) {
                        args.add(pestMaster.getPressureScaleInt());
                    } else {
                        args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale("Adventice", pestMaster.getPressureScale()));
                    }
                    // Pression/risque avant traitement - expression de l’agriculteur
                    args.add(pestMaster.getPressureFarmerComment());
                    // Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise
                    if (pestMaster.getMasterScaleInt() != null) {
                        args.add(pestMaster.getMasterScaleInt());
                    } else {
                        args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale("Adventice", pestMaster.getMasterScale()));
                    }
                    // Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getQualifier()));
                    // Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur
                    args.add(pestMaster.getResultFarmerComment());
                    // IFT-Herbicide
                    args.add(cropAdventiceMaster.getIftMain());
                    // IFT-Fongicide
                    args.add("");
                    // IFT-Insecticide
                    args.add("");
                    // IFT-Hors-Biocontrôle
                    args.add("");
                    // IFT-autres ravageurs
                    args.add("");
                    // IFT-régulateurs
                    args.add("");
                    // Commentaire du conseiller
                    args.add(cropAdventiceMaster.getAdviserComments());
                    // Irrigation
                    args.add("");
                    // Stress hydrique
                    args.add("");
                    // Azote
                    args.add("");
                    // Alimentation minérale (hors azote)
                    args.add("");
                    // Température et rayonnement
                    args.add("");
                    // Commentaires – Résultats obtenus et faits marquants
                    args.add("");
                    // L’objectif de rendement est-il atteint ?
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 1
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 2
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 3
                    args.add("");
                    // Commentaires sur la qualité
                    args.add("");

                    writeToSheet(allSheet, args.toArray());
                }
            }
        }
    }

    private void exportReportGrowingSystemAllDisease(Sheet allSheet, ReportGrowingSystem reportGrowingSystem, Sector sector) {
        Collection<CropPestMaster> cropDiseaseMasters = reportGrowingSystem.getCropDiseaseMasters();
        if (cropDiseaseMasters != null) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            cropDiseaseMasters = cropDiseaseMasters.stream()
                    .filter(cropPestMaster -> Objects.equals(cropPestMaster.getSector(), sector))
                    .toList();
            for (CropPestMaster cropDiseaseMaster : cropDiseaseMasters) {
                Collection<CroppingPlanEntry> crops = cropDiseaseMaster.getCrops();

                for (PestMaster pestMaster : cropDiseaseMaster.getPestMasters()) {
                    List<Object> args = new ArrayList<>();
                    exportCommonData(args, reportGrowingSystem, growingSystem, domain,"Maladies", crops);
                    // Groupe cible
                    args.add(Objects.toString(groupesCiblesParCode.get(pestMaster.getCodeGroupeCibleMaa()), "N/A"));
                    // Adventice/Maladie/Ravageur (champ vide pour les autres thématiques)
                    RefBioAgressor agressor = pestMaster.getAgressor();
                    args.add(agressor != null ? agressor.getLabel() : "N/A");
                    // Pression/risque avant traitement - Échelle de pression
                    if (pestMaster.getPressureScaleInt() != null) {
                        args.add(pestMaster.getPressureScaleInt());
                    } else {
                        args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getPressureScale()));
                    }
                    // Pression/risque avant traitement - expression de l’agriculteur
                    args.add(pestMaster.getPressureFarmerComment());
                    // Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise
                    if (pestMaster.getMasterScaleInt() != null) {
                        args.add(pestMaster.getMasterScaleInt());
                    } else {
                        args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getMasterScale()));
                    }
                    // Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getQualifier()));
                    // Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur
                    args.add(pestMaster.getResultFarmerComment());
                    // IFT-Herbicide
                    args.add("");
                    // IFT-Fongicide
                    args.add(cropDiseaseMaster.getIftMain());
                    // IFT-Insecticide
                    args.add("");
                    // IFT-Hors-Biocontrôle
                    args.add(cropDiseaseMaster.getIftHorsBiocontrole());
                    // IFT-autres ravageurs
                    args.add("");
                    // IFT-régulateurs
                    args.add("");
                    // Commentaire du conseiller
                    args.add(cropDiseaseMaster.getAdviserComments());
                    // Irrigation
                    args.add("");
                    // Stress hydrique
                    args.add("");
                    // Azote
                    args.add("");
                    // Alimentation minérale (hors azote)
                    args.add("");
                    // Température et rayonnement
                    args.add("");
                    // Commentaires – Résultats obtenus et faits marquants
                    args.add("");
                    // L’objectif de rendement est-il atteint ?
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 1
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 2
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 3
                    args.add("");
                    // Commentaires sur la qualité
                    args.add("");

                    writeToSheet(allSheet, args.toArray());
                }
            }
        }
    }

    private void exportReportGrowingSystemAllPest(Sheet allSheet, ReportGrowingSystem reportGrowingSystem, Sector sector) {
        Collection<CropPestMaster> cropPestMasters = reportGrowingSystem.getCropPestMasters();
        if (cropPestMasters != null) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            cropPestMasters = cropPestMasters.stream()
                    .filter(cropPestMaster -> Objects.equals(cropPestMaster.getSector(), sector))
                    .toList();
            for (CropPestMaster cropPestMaster : cropPestMasters) {
                Collection<CroppingPlanEntry> crops = cropPestMaster.getCrops();

                for (PestMaster pestMaster : cropPestMaster.getPestMasters()) {
                    List<Object> args = new ArrayList<>();
                    exportCommonData(args, reportGrowingSystem, growingSystem, domain,"Ravageurs", crops);
                    // Groupe cible
                    args.add( Objects.toString(groupesCiblesParCode.get(pestMaster.getCodeGroupeCibleMaa()), "N/A"));
                    // Adventice/Maladie/Ravageur (champ vide pour les autres thématiques)
                    RefBioAgressor agressor = pestMaster.getAgressor();
                    args.add(agressor != null ? agressor.getLabel() : "N/A");
                    // Pression/risque avant traitement - Échelle de pression
                    if (pestMaster.getPressureScaleInt() != null) {
                        args.add(pestMaster.getPressureScaleInt());
                    } else {
                        args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getPressureScale()));
                    }
                    // Pression/risque avant traitement - expression de l’agriculteur
                    args.add(pestMaster.getPressureFarmerComment());
                    // Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise
                    if (pestMaster.getMasterScaleInt() != null) {
                        args.add(pestMaster.getMasterScaleInt());
                    } else {
                        args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getMasterScale()));
                    }
                    // Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getQualifier()));
                    // Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur
                    args.add(pestMaster.getResultFarmerComment());
                    // IFT-Herbicide
                    args.add("");
                    // IFT-Fongicide
                    args.add("");
                    // IFT-Insecticide
                    args.add(cropPestMaster.getIftMain());
                    // IFT-Hors-Biocontrôle
                    args.add(cropPestMaster.getIftHorsBiocontrole());
                    // IFT-autres ravageurs
                    args.add(cropPestMaster.getIftOther());
                    // IFT-régulateurs
                    args.add("");
                    // Commentaire du conseiller
                    args.add(cropPestMaster.getAdviserComments());
                    // Irrigation
                    args.add("");
                    // Stress hydrique
                    args.add("");
                    // Azote
                    args.add("");
                    // Alimentation minérale (hors azote)
                    args.add("");
                    // Température et rayonnement
                    args.add("");
                    // Commentaires – Résultats obtenus et faits marquants
                    args.add("");
                    // L’objectif de rendement est-il atteint ?
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 1
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 2
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 3
                    args.add("");
                    // Commentaires sur la qualité
                    args.add("");

                    writeToSheet(allSheet, args.toArray());
                }
            }
        }
    }

    private void exportReportGrowingSystemAllVerse(Sheet allSheet, ReportGrowingSystem reportGrowingSystem, Sector sector) {
        Collection<VerseMaster> verseMasters = reportGrowingSystem.getVerseMasters();
        if (verseMasters != null) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            verseMasters = verseMasters.stream()
                    .filter(verseMaster -> Objects.equals(verseMaster.getSector(), sector))
                    .toList();
            for (VerseMaster verseMaster : verseMasters) {
                Collection<CroppingPlanEntry> crops = verseMaster.getCrops();

                List<Object> args = new ArrayList<>();
                exportCommonData(args, reportGrowingSystem, growingSystem, domain, "Verse", crops);
                // Groupe cible
                args.add("");
                // Adventice/Maladie/Ravageur (champ vide pour les autres thématiques)
                args.add("");
                // Pression/risque avant traitement - Échelle de pression
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(verseMaster.getRiskScale()));
                // Pression/risque avant traitement - expression de l’agriculteur
                args.add(verseMaster.getRiskFarmerComment());
                // Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale("Verse", verseMaster.getMasterScale()));
                // Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(verseMaster.getQualifier()));
                // Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur
                args.add(verseMaster.getResultFarmerComment());
                // IFT-Herbicide
                args.add("");
                // IFT-Fongicide
                args.add("");
                // IFT-Insecticide
                args.add("");
                // IFT-Hors-Biocontrôle
                args.add("");
                // IFT-autres ravageurs
                args.add("");
                // IFT-régulateurs
                args.add(verseMaster.getIftMain());
                // Commentaire du conseiller
                args.add(verseMaster.getAdviserComments());
                // Irrigation
                args.add("");
                // Stress hydrique
                args.add("");
                // Azote
                args.add("");
                // Alimentation minérale (hors azote)
                args.add("");
                // Température et rayonnement
                args.add("");
                // Commentaires – Résultats obtenus et faits marquants
                args.add("");
                // L’objectif de rendement est-il atteint ?
                args.add("");
                // Si les objectifs de rendement ne sont pas atteints, cause 1
                args.add("");
                // Si les objectifs de rendement ne sont pas atteints, cause 2
                args.add("");
                // Si les objectifs de rendement ne sont pas atteints, cause 3
                args.add("");
                // Commentaires sur la qualité
                args.add("");

                writeToSheet(allSheet, args.toArray());
            }
        }
    }

    private void exportReportGrowingSystemAllFood(Sheet allSheet, ReportGrowingSystem reportGrowingSystem, Sector sector) {
        Collection<FoodMaster> foodMasters = reportGrowingSystem.getFoodMasters();
        if (foodMasters != null) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            foodMasters = foodMasters.stream()
                    .filter(foodMaster -> Objects.equals(foodMaster.getSector(), sector))
                    .toList();
            for (FoodMaster foodMaster : foodMasters) {
                Collection<CroppingPlanEntry> crops = foodMaster.getCrops();

                List<Object> args = new ArrayList<>();
                exportCommonData(args, reportGrowingSystem, growingSystem, domain, "Alimentation hydrique et minérale", crops);
                // Groupe cible
                args.add("");
                // Adventice/Maladie/Ravageur (champ vide pour les autres thématiques)
                args.add("");
                // Pression/risque avant traitement - Échelle de pression
                args.add("");
                // Pression/risque avant traitement - expression de l’agriculteur
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur
                args.add("");
                // IFT-Herbicide
                args.add("");
                // IFT-Fongicide
                args.add("");
                // IFT-Insecticide
                args.add("");
                // IFT-Hors-Biocontrôle
                args.add("");
                // IFT-autres ravageurs
                args.add("");
                // IFT-régulateurs
                args.add("");
                // Commentaire du conseiller
                args.add("");
                // Irrigation
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(foodMaster.getFoodIrrigation()));
                // Stress hydrique
                if (foodMaster.getHydriqueStressInt() != null) {
                    args.add(foodMaster.getHydriqueStressInt());
                } else {
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(foodMaster.getHydriqueStress()));
                }
                // Azote
                if (foodMaster.getAzoteStressInt() != null) {
                    args.add(foodMaster.getAzoteStressInt());
                } else {
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(foodMaster.getAzoteStress()));
                }
                // Alimentation minérale (hors azote)
                args.add(foodMaster.getMineralFood());
                // Température et rayonnement
                if (foodMaster.getTempStressInt() != null) {
                    args.add(foodMaster.getTempStressInt());
                } else {
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(foodMaster.getTempStress()));
                }
                // Commentaires – Résultats obtenus et faits marquants
                args.add(foodMaster.getComment());
                // L’objectif de rendement est-il atteint ?
                args.add("");
                // Si les objectifs de rendement ne sont pas atteints, cause 1
                args.add("");
                // Si les objectifs de rendement ne sont pas atteints, cause 2
                args.add("");
                // Si les objectifs de rendement ne sont pas atteints, cause 3
                args.add("");
                // Commentaires sur la qualité
                args.add("");

                writeToSheet(allSheet, args.toArray());
            }
        }
    }

    private void exportReportGrowingSystemAllYield(Sheet allSheet, ReportGrowingSystem reportGrowingSystem, Sector sector) {
        Collection<YieldLoss> yieldLosses = reportGrowingSystem.getYieldLosses();
        if (yieldLosses != null) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            yieldLosses = yieldLosses.stream()
                    .filter(yieldLoss -> Objects.equals(yieldLoss.getSector(), sector))
                    .toList();
            for (YieldLoss yieldLoss : yieldLosses) {
                Collection<CroppingPlanEntry> crops = yieldLoss.getCrops();

                List<Object> args = new ArrayList<>();
                exportCommonData(args, reportGrowingSystem, growingSystem, domain, "Rendement et qualité", crops);
                // Groupe cible
                args.add("");
                // Adventice/Maladie/Ravageur (champ vide pour les autres thématiques)
                args.add("");
                // Pression/risque avant traitement - Échelle de pression
                args.add("");
                // Pression/risque avant traitement - expression de l’agriculteur
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur
                args.add("");
                // IFT-Herbicide
                args.add("");
                // IFT-Fongicide
                args.add("");
                // IFT-Insecticide
                args.add("");
                // IFT-Hors-Biocontrôle
                args.add("");
                // IFT-autres ravageurs
                args.add("");
                // IFT-régulateurs
                args.add("");
                // Commentaire du conseiller
                args.add("");
                // Irrigation
                args.add("");
                // Stress hydrique
                args.add("");
                // Azote
                args.add("");
                // Alimentation minérale (hors azote)
                args.add("");
                // Température et rayonnement
                args.add("");
                // Commentaires – Résultats obtenus et faits marquants
                args.add("");
                // L’objectif de rendement est-il atteint ?
                // Pression/risque avant traitement - Échelle de pression
                if (yieldLoss.getYieldObjectiveInt() != null) {
                    args.add(yieldLoss.getYieldObjectiveInt());
                } else {
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(yieldLoss.getYieldObjective()));
                }
                // Si les objectifs de rendement ne sont pas atteints, cause 1
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(yieldLoss.getCause1()));
                // Si les objectifs de rendement ne sont pas atteints, cause 2
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(yieldLoss.getCause2()));
                // Si les objectifs de rendement ne sont pas atteints, cause 3
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(yieldLoss.getCause3()));
                // Commentaires sur la qualité
                args.add(yieldLoss.getComment());
        
                writeToSheet(allSheet, args.toArray());
            }
        }
    }
    
    protected void exportReportGrowingSystemArbo(Workbook workbook, List<ReportGrowingSystem> reportGrowingSystemsToExport) {
        Sheet arboSheet = workbook.createSheet("Arboriculture");
        addHeaders(workbook, arboSheet,
                "ID du Bilan de campagne",
                "Nom du Bilan de campagne",
                "Campagne",
                "Département",
                "Réseau de rattachement du SDC",
                "Système de culture",
                "Type de conduite du SDC",
                "Numéro DEPHY",
                "Type de thématique",
                "Culture",
                "Espèces",
                "Variétés",
                "Groupe cible",
                "Adventice/Maladie/Ravageur",
                "Pression sur le système de culture - Échelle de pression",
                "Pression sur le système de culture - Evolution par rapport à l’année précédente",
                "Pression sur le système de culture – Expression de l’agriculteur",
                "Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise",
                "Résultats obtenus, niveau de maîtrise finale - % parcelles touchées",
                "Résultats obtenus, niveau de maîtrise finale - % arbres touchés",
                "Résultats obtenus, niveau de maîtrise finale - % dommages sur fruits",
                "Résultats obtenus, niveau de maîtrise finale - % dommages sur pousses ou feuilles",
                "Résultats obtenus, niveau de maîtrise finale - Inoculum pour les années suivantes",
                "Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise",
                "Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur",
                "Commentaires du conseiller",
                "IFT (chimique)",
                "IFT biocontrôle",
                "Nombre de traitements",
                "Irrigation",
                "Stress hydrique",
                "Azote",
                "Alimentation minérale (hors azote)",
                "Température et rayonnement",
                "Commentaires – Résultats obtenus et faits marquants",
                "L’objectif de rendement est-il atteint ?",
                "Si les objectifs de rendement ne sont pas atteints, cause 1",
                "Si les objectifs de rendement ne sont pas atteints, cause 2",
                "Si les objectifs de rendement ne sont pas atteints, cause 3",
                "Commentaires sur la qualité"
        );

        for (ReportGrowingSystem reportGrowingSystem : reportGrowingSystemsToExport) {
            if (reportGrowingSystem.getSectors().contains(Sector.ARBORICULTURE)) {
                exportReportGrowingSystemArboDisease(arboSheet, reportGrowingSystem);
                exportReportGrowingSystemArboPest(arboSheet, reportGrowingSystem);
                exportReportGrowingSystemArboAdventice(arboSheet, reportGrowingSystem);
                exportReportGrowingSystemArboFood(arboSheet, reportGrowingSystem);
                exportReportGrowingSystemArboYield(arboSheet, reportGrowingSystem);
            }
        }
    }

    private void exportReportGrowingSystemArboDisease(Sheet arboSheet, ReportGrowingSystem reportGrowingSystem) {
        Collection<ArboCropPestMaster> arboCropPestMasters = reportGrowingSystem.getArboCropDiseaseMasters();
        if (arboCropPestMasters != null) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            for (ArboCropPestMaster arboCropPestMaster : arboCropPestMasters) {
                Collection<CroppingPlanEntry> crops = arboCropPestMaster.getCrops();

                for (ArboPestMaster pestMaster : arboCropPestMaster.getPestMasters()) {
                    List<Object> args = new ArrayList<>();
                    exportCommonData(args, reportGrowingSystem, growingSystem, domain, "Maladies", crops);
                    // Groupe cible
                    args.add( Objects.toString(groupesCiblesParCode.get(pestMaster.getCodeGroupeCibleMaa()), "N/A"));
                    // Adventice/Maladie/Ravageur
                    RefBioAgressor agressor = pestMaster.getAgressor();
                    args.add(agressor != null ? agressor.getLabel() : "N/A");
                    // Pression sur le système de culture - Échelle de pression
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getPreviousYearInoculum()));
                    // Pression sur le système de culture - Evolution par rapport à l’année précédente
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getPressureEvolution()));
                    // Pression sur le système de culture – Expression de l’agriculteur
                    args.add("");
                    // Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getMasterScale()));
                    // Résultats obtenus, niveau de maîtrise finale - % parcelles touchées
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getPercentAffectedPlots()));
                    // Résultats obtenus, niveau de maîtrise finale - % arbres touchées
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getPercentAffectedTrees()));
                    // Résultats obtenus, niveau de maîtrise finale - % dommages sur fruits
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getPercentDamageFruits()));
                    // Résultats obtenus, niveau de maîtrise finale - % dommages sur pousses ou feuilles
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getPercentDamageLeafs()));
                    // Résultats obtenus, niveau de maîtrise finale - Inoculum pour les années suivantes
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getNextYearInoculum()));
                    // Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getQualifier()));
                    // Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur
                    args.add(pestMaster.getResultFarmerComment());
                    // Commentaires du conseiller
                    args.add(pestMaster.getAdviserComments());
                    // IFT (chimique)
                    args.add(arboCropPestMaster.getChemicalPestIFT());
                    // IFT biocontrôle
                    args.add(arboCropPestMaster.getBioControlPestIFT());
                    // Nombre de traitements
                    args.add(arboCropPestMaster.getTreatmentCount());
                    // Irrigation
                    args.add("");
                    // Stress hydrique
                    args.add("");
                    // Azote
                    args.add("");
                    // Alimentation minérale (hors azote)
                    args.add("");
                    // Température et rayonnement
                    args.add("");
                    // Commentaires – Résultats obtenus et faits marquants
                    args.add("");
                    // L’objectif de rendement est-il atteint ?
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 1
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 2
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 3
                    args.add("");
                    // Commentaires sur la qualité
                    args.add("");

                    writeToSheet(arboSheet, args.toArray());
                }
            }
        }
    }

    private void exportReportGrowingSystemArboPest(Sheet arboSheet, ReportGrowingSystem reportGrowingSystem) {
        Collection<ArboCropPestMaster> arboCropPestMasters = reportGrowingSystem.getArboCropPestMasters();
        if (arboCropPestMasters != null) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            for (ArboCropPestMaster arboCropPestMaster : arboCropPestMasters) {
                Collection<CroppingPlanEntry> crops = arboCropPestMaster.getCrops();

                for (ArboPestMaster pestMaster : arboCropPestMaster.getPestMasters()) {
                    List<Object> args = new ArrayList<>();
                    exportCommonData(args, reportGrowingSystem, growingSystem, domain, "Ravageurs", crops);
                    // Groupe cible
                    args.add( Objects.toString(groupesCiblesParCode.get(pestMaster.getCodeGroupeCibleMaa()), "N/A"));
                    // Adventice/Maladie/Ravageur
                    RefBioAgressor agressor = pestMaster.getAgressor();
                    args.add(agressor != null ? agressor.getLabel() : "N/A");
                    // Pression sur le système de culture - Échelle de pression
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getPreviousYearInoculum()));
                    // Pression sur le système de culture - Evolution par rapport à l’année précédente
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getPressureEvolution()));
                    // Pression sur le système de culture – Expression de l’agriculteur
                    args.add("");
                    // Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getMasterScale()));
                    // Résultats obtenus, niveau de maîtrise finale - % parcelles touchées
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getPercentAffectedPlots()));
                    // Résultats obtenus, niveau de maîtrise finale - % arbres touchées
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getPercentAffectedTrees()));
                    // Résultats obtenus, niveau de maîtrise finale - % dommages sur fruits
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getPercentDamageFruits()));
                    // Résultats obtenus, niveau de maîtrise finale - % dommages sur pousses ou feuilles
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getPercentDamageLeafs()));
                    // Résultats obtenus, niveau de maîtrise finale - Inoculum pour les années suivantes
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getNextYearInoculum()));
                    // Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getQualifier()));
                    // Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur
                    args.add(pestMaster.getResultFarmerComment());
                    // Commentaires du conseiller
                    args.add(pestMaster.getAdviserComments());
                    // IFT (chimique)
                    args.add(arboCropPestMaster.getChemicalPestIFT());
                    // IFT biocontrôle
                    args.add(arboCropPestMaster.getBioControlPestIFT());
                    // Nombre de traitements
                    args.add(arboCropPestMaster.getTreatmentCount());
                    // Irrigation
                    args.add("");
                    // Stress hydrique
                    args.add("");
                    // Azote
                    args.add("");
                    // Alimentation minérale (hors azote)
                    args.add("");
                    // Température et rayonnement
                    args.add("");
                    // Commentaires – Résultats obtenus et faits marquants
                    args.add("");
                    // L’objectif de rendement est-il atteint ?
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 1
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 2
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 3
                    args.add("");
                    // Commentaires sur la qualité
                    args.add("");

                    writeToSheet(arboSheet, args.toArray());
                }
            }
        }
    }

    protected void exportReportGrowingSystemArboAdventice(Sheet arboSheet, ReportGrowingSystem reportGrowingSystem) {
        Collection<ArboCropAdventiceMaster> arboCropAdventiceMasters = reportGrowingSystem.getArboCropAdventiceMasters();
        if (arboCropAdventiceMasters != null) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            for (ArboCropAdventiceMaster arboCropAdventiceMaster : arboCropAdventiceMasters) {
                Collection<CroppingPlanEntry> crops = arboCropAdventiceMaster.getCrops();

                for (ArboAdventiceMaster pestMaster : arboCropAdventiceMaster.getPestMasters()) {
                    List<Object> args = new ArrayList<>();
                    exportCommonData(args, reportGrowingSystem, growingSystem, domain,"Adventices", crops);
                    // Groupe cible
                    args.add("");
                    // Adventice/Maladie/Ravageur
                    RefBioAgressor agressor = pestMaster.getAgressor();
                    args.add(agressor != null ? agressor.getLabel() : "N/A");
                    // Pression sur le système de culture - Échelle de pression
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getGrassingLevel()));
                    // Pression sur le système de culture - Evolution par rapport à l’année précédente
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getGrassingEvolution()));
                    // Pression sur le système de culture – Expression de l’agriculteur
                    args.add("");
                    // Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale("ArboAdventice", pestMaster.getMasterScale()));
                    // Résultats obtenus, niveau de maîtrise finale - % parcelles touchées
                    args.add("");
                    // Résultats obtenus, niveau de maîtrise finale - % arbres touchées
                    args.add("");
                    // Résultats obtenus, niveau de maîtrise finale - % dommages sur fruits
                    args.add("");
                    // Résultats obtenus, niveau de maîtrise finale - % dommages sur pousses ou feuilles
                    args.add("");
                    // Résultats obtenus, niveau de maîtrise finale - Inoculum pour les années suivantes
                    args.add("");
                    // Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise
                    args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(pestMaster.getQualifier()));
                    // Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur
                    args.add(pestMaster.getResultFarmerComment());
                    // Commentaires du conseiller
                    args.add(pestMaster.getAdviserComments());
                    // IFT (chimique)
                    args.add(arboCropAdventiceMaster.getChemicalPestIFT());
                    // IFT biocontrôle
                    args.add(arboCropAdventiceMaster.getBioControlPestIFT());
                    // Nombre de traitements
                    args.add(arboCropAdventiceMaster.getTreatmentCount());
                    // Irrigation
                    args.add("");
                    // Stress hydrique
                    args.add("");
                    // Azote
                    args.add("");
                    // Alimentation minérale (hors azote)
                    args.add("");
                    // Température et rayonnement
                    args.add("");
                    // Commentaires – Résultats obtenus et faits marquants
                    args.add("");
                    // L’objectif de rendement est-il atteint ?
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 1
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 2
                    args.add("");
                    // Si les objectifs de rendement ne sont pas atteints, cause 3
                    args.add("");
                    // Commentaires sur la qualité
                    args.add("");

                    writeToSheet(arboSheet, args.toArray());
                }
            }
        }
    }

    private void exportReportGrowingSystemArboFood(Sheet arboSheet, ReportGrowingSystem reportGrowingSystem) {
        Collection<FoodMaster> arboFoodMasters = reportGrowingSystem.getFoodMasters();
        if (arboFoodMasters != null) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            arboFoodMasters = arboFoodMasters.stream()
                    .filter(foodMaster -> Sector.ARBORICULTURE.equals(foodMaster.getSector()))
                    .toList();
            for (FoodMaster arboFoodMaster : arboFoodMasters) {
                Collection<CroppingPlanEntry> crops = arboFoodMaster.getCrops();

                List<Object> args = new ArrayList<>();
                exportCommonData(args, reportGrowingSystem, growingSystem, domain, "Alimentation hydrique et minérale", crops);
                // Groupe cible
                args.add("");
                // Adventice/Maladie/Ravageur
                args.add("");
                // Pression sur le système de culture - Échelle de pression
                args.add("");
                // Pression sur le système de culture - Evolution par rapport à l’année précédente
                args.add("");
                // Pression sur le système de culture – Expression de l’agriculteur
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - % parcelles touchées
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - % arbres touchées
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - % dommages sur fruits
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - % dommages sur pousses ou feuilles
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Inoculum pour les années suivantes
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur
                args.add("");
                // Commentaires du conseiller
                args.add("");
                // IFT (chimique)
                args.add("");
                // IFT biocontrôle
                args.add("");
                // Nombre de traitements
                args.add("");
                // Irrigation
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(arboFoodMaster.getFoodIrrigation()));
                // Stress hydrique
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(arboFoodMaster.getHydriqueStress()));
                // Azote
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(arboFoodMaster.getAzoteStress()));
                // Alimentation minérale (hors azote)
                args.add(arboFoodMaster.getMineralFood());
                // Température et rayonnement
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(arboFoodMaster.getTempStress()));
                // Commentaires – Résultats obtenus et faits marquants
                args.add(arboFoodMaster.getComment());
                // L’objectif de rendement est-il atteint ?
                args.add("");
                // Si les objectifs de rendement ne sont pas atteints, cause 1
                args.add("");
                // Si les objectifs de rendement ne sont pas atteints, cause 2
                args.add("");
                // Si les objectifs de rendement ne sont pas atteints, cause 3
                args.add("");
                // Commentaires sur la qualité
                args.add("");

                writeToSheet(arboSheet, args.toArray());
            }
        }
    }

    private void exportReportGrowingSystemArboYield(Sheet arboSheet, ReportGrowingSystem reportGrowingSystem) {
        Collection<YieldLoss> arboYieldLosses = reportGrowingSystem.getYieldLosses();
        if (arboYieldLosses != null) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            arboYieldLosses = arboYieldLosses.stream().filter(yieldLoss -> Sector.ARBORICULTURE.equals(yieldLoss.getSector())).toList();
            for (YieldLoss yieldLoss : arboYieldLosses) {
                Collection<CroppingPlanEntry> crops = yieldLoss.getCrops();

                List<Object> args = new ArrayList<>();
                exportCommonData(args, reportGrowingSystem, growingSystem, domain, "Rendement et qualité", crops);
                // Groupe cible
                args.add("");
                // Adventice/Maladie/Ravageur
                args.add("");
                // Pression sur le système de culture - Échelle de pression
                args.add("");
                // Pression sur le système de culture - Evolution par rapport à l’année précédente
                args.add("");
                // Pression sur le système de culture – Expression de l’agriculteur
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - % parcelles touchées
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - % arbres touchées
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - % dommages sur fruits
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - % dommages sur pousses ou feuilles
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Inoculum pour les années suivantes
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur
                args.add("");
                // Commentaires du conseiller
                args.add("");
                // IFT (chimique)
                args.add("");
                // IFT biocontrôle
                args.add("");
                // Nombre de traitements
                args.add("");
                // Irrigation
                args.add("");
                // Stress hydrique
                args.add("");
                // Azote
                args.add("");
                // Alimentation minérale (hors azote)
                args.add("");
                // Température et rayonnement
                args.add("");
                // Commentaires – Résultats obtenus et faits marquants
                args.add("");
                // L’objectif de rendement est-il atteint ?
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(yieldLoss.getYieldObjective()));
                // Si les objectifs de rendement ne sont pas atteints, cause 1
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(yieldLoss.getCause1()));
                // Si les objectifs de rendement ne sont pas atteints, cause 2
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(yieldLoss.getCause2()));
                // Si les objectifs de rendement ne sont pas atteints, cause 3
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(yieldLoss.getCause3()));
                // Commentaires sur la qualité
                args.add(yieldLoss.getComment());

                writeToSheet(arboSheet, args.toArray());
            }
        }
    }

    protected void exportReportGrowingSystemViti(Workbook workbook, List<ReportGrowingSystem> reportGrowingSystemsToExport) {
        Sheet vitiSheet = workbook.createSheet("Viticulture");
        addHeaders(workbook, vitiSheet,
                "ID du Bilan de campagne",
                "Nom du Bilan de campagne",
                "Campagne",
                "Département",
                "Réseau de rattachement du SDC",
                "Système de culture",
                "Type de conduite du SDC",
                "Numéro DEPHY",
                "Type de thématique",
                "Culture",
                "Espèces",
                "Variétés",
                "Groupe cible",
                "Maladie/Ravageur (champ vide pour les autres thématiques)",
                "Pression sur le système de culture - Échelle de pression",
                "Pression sur le système de culture - Evolution par rapport à l’année précédente",
                "Pression sur le système de culture – Expression de l’agriculteur",
                "Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise",
                "Résultats obtenus, niveau de maîtrise finale - Note globale d’attaque sur feuilles",
                "Résultats obtenus, niveau de maîtrise finale - Note de fréquence d’attaque sur feuilles",
                "Résultats obtenus, niveau de maîtrise finale - Note d'intensité d’attaque sur feuilles",
                "Résultats obtenus, niveau de maîtrise finale - Note globale d’attaque sur grappes",
                "Résultats obtenus, niveau de maîtrise finale - Note de fréquence d’attaque sur grappes",
                "Résultats obtenus, niveau de maîtrise finale - Note d'intensité d’attaque sur grappes",
                "Résultats obtenus, niveau de maîtrise finale - Nombre de perforations pour 100 grappes",
                "Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise",
                "Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur",
                "Résultats obtenus, niveau de maîtrise finale - Commentaires du conseiller",
                "IFT (chimique)",
                "IFT biocontrôle",
                "Nombre de traitements",
                "dont nombre de traitements obligatoires",
                "Irrigation",
                "Stress hydrique",
                "Azote",
                "Alimentation minérale (hors azote)",
                "Température et rayonnement",
                "Commentaires – Résultats obtenus et faits marquants"
        );

        for (ReportGrowingSystem reportGrowingSystem : reportGrowingSystemsToExport) {
            if (reportGrowingSystem.getSectors().contains(Sector.VITICULTURE)) {
                exportReportGrowingSystemVitiDisease(vitiSheet, reportGrowingSystem);
                exportReportGrowingSystemVitiPest(vitiSheet, reportGrowingSystem);
                exportReportGrowingSystemVitiFood(vitiSheet, reportGrowingSystem);
            }
        }
    }

    private void exportReportGrowingSystemVitiDisease(Sheet vitiSheet, ReportGrowingSystem reportGrowingSystem) {
        Collection<VitiPestMaster> vitiPestMasters = reportGrowingSystem.getVitiDiseaseMasters();
        if (vitiPestMasters != null) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            for (VitiPestMaster vitiPestMaster : vitiPestMasters) {
                List<Object> args = new ArrayList<>();
                exportCommonData(args, reportGrowingSystem, growingSystem, domain, "Maladies", null);
                // Groupe cible
                args.add( Objects.toString(groupesCiblesParCode.get(vitiPestMaster.getCodeGroupeCibleMaa()), "N/A"));
                // Maladie/Ravageur (champ vide pour les autres thématiques)
                RefBioAgressor agressor = vitiPestMaster.getAgressor();
                args.add(agressor != null ? agressor.getLabel() : "N/A");
                // Pression sur le système de culture - Échelle de pression
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(vitiPestMaster.getPressureScale()));
                // Pression sur le système de culture - Evolution par rapport à l’année précédente
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(vitiPestMaster.getPressureEvolution()));
                // Pression sur le système de culture – Expression de l’agriculteur
                args.add(vitiPestMaster.getPressureFarmerComment());
                // Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale("VitiDisease", vitiPestMaster.getMasterScale()));
                // Résultats obtenus, niveau de maîtrise finale - Note globale d’attaque sur feuilles
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(vitiPestMaster.getLeafDiseaseAttackRate()));
                // Résultats obtenus, niveau de maîtrise finale - Note de fréquence d’attaque sur feuilles
                args.add(vitiPestMaster.getLeafDiseaseAttackRatePreciseValue());
                // Résultats obtenus, niveau de maîtrise finale - Note d'intensité d’attaque sur feuilles
                args.add(vitiPestMaster.getLeafDiseaseAttackIntensityPreciseValue());
                // Résultats obtenus, niveau de maîtrise finale - Note globale d’attaque sur grappes
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(vitiPestMaster.getGrapeDiseaseAttackRate()));
                // Résultats obtenus, niveau de maîtrise finale - Note de fréquence d’attaque sur grappes
                args.add(vitiPestMaster.getGrapeDiseaseAttackRatePreciseValue());
                // Résultats obtenus, niveau de maîtrise finale - Note d'intensité d’attaque sur grappes
                args.add(vitiPestMaster.getGrapeDiseaseAttackIntensityPreciseValue());
                // Résultats obtenus, niveau de maîtrise finale - Nombre de perforations pour 100 grappes
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(vitiPestMaster.getQualifier()));
                // Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur
                args.add(vitiPestMaster.getResultFarmerComment());
                // Résultats obtenus, niveau de maîtrise finale - Commentaires du conseiller
                args.add(vitiPestMaster.getAdviserComments());
                // IFT (chimique)
                args.add(vitiPestMaster.getChemicalFungicideIFT());
                // IFT biocontrôle
                args.add(vitiPestMaster.getBioControlFungicideIFT());
                // Nombre de traitements
                args.add(vitiPestMaster.getTreatmentCount());
                // dont nombre de traitements obligatoires
                args.add("");
                // Irrigation
                args.add("");
                // Stress hydrique
                args.add("");
                // Azote
                args.add("");
                // Alimentation minérale (hors azote)
                args.add("");
                // Température et rayonnement
                args.add("");
                // Commentaires – Résultats obtenus et faits marquants
                args.add("");

                writeToSheet(vitiSheet, args.toArray());
            }
        }
    }

    private void exportReportGrowingSystemVitiPest(Sheet vitiSheet, ReportGrowingSystem reportGrowingSystem) {
        Collection<VitiPestMaster> vitiPestMasters = reportGrowingSystem.getVitiPestMasters();
        if (vitiPestMasters != null) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            for (VitiPestMaster vitiPestMaster : vitiPestMasters) {
                List<Object> args = new ArrayList<>();
                exportCommonData(args, reportGrowingSystem, growingSystem, domain, "Ravageurs", null);
                // Groupe cible
                args.add( Objects.toString(groupesCiblesParCode.get(vitiPestMaster.getCodeGroupeCibleMaa()), "N/A"));
                // Maladie/Ravageur (champ vide pour les autres thématiques)
                RefBioAgressor agressor = vitiPestMaster.getAgressor();
                args.add(agressor != null ? agressor.getLabel() : "N/A");
                // Pression sur le système de culture - Échelle de pression
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(vitiPestMaster.getPressureScale()));
                // Pression sur le système de culture - Evolution par rapport à l’année précédente
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(vitiPestMaster.getPressureEvolution()));
                // Pression sur le système de culture – Expression de l’agriculteur
                args.add(vitiPestMaster.getPressureFarmerComment());
                // Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale("VitiPest", vitiPestMaster.getMasterScale()));
                // Résultats obtenus, niveau de maîtrise finale - Note globale d’attaque sur feuilles
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(vitiPestMaster.getLeafPestAttackRate()));
                // Résultats obtenus, niveau de maîtrise finale - Note de fréquence d’attaque sur feuilles
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Note d'intensité d’attaque sur feuilles
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Note globale d’attaque sur grappes
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(vitiPestMaster.getGrapePestAttackRate()));
                // Résultats obtenus, niveau de maîtrise finale - Note de fréquence d’attaque sur grappes
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Note d'intensité d’attaque sur grappes
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Nombre de perforations pour 100 grappes
                args.add(vitiPestMaster.getNbPestGrapePerforation());
                // Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(vitiPestMaster.getQualifier()));
                // Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur
                args.add(vitiPestMaster.getResultFarmerComment());
                // Résultats obtenus, niveau de maîtrise finale - Commentaires du conseiller
                args.add(vitiPestMaster.getAdviserComments());
                // IFT (chimique)
                args.add(vitiPestMaster.getChemicalFungicideIFT());
                // IFT biocontrôle
                args.add(vitiPestMaster.getBioControlFungicideIFT());
                // Nombre de traitements
                args.add(vitiPestMaster.getTreatmentCount());
                // dont nombre de traitements obligatoires
                args.add(vitiPestMaster.getNbPestTraitementRequired());
                // Irrigation
                args.add("");
                // Stress hydrique
                args.add("");
                // Azote
                args.add("");
                // Alimentation minérale (hors azote)
                args.add("");
                // Température et rayonnement
                args.add("");
                // Commentaires – Résultats obtenus et faits marquants
                args.add("");

                writeToSheet(vitiSheet, args.toArray());
            }
        }
    }

    private void exportReportGrowingSystemVitiFood(Sheet vitiSheet, ReportGrowingSystem reportGrowingSystem) {
        Collection<FoodMaster> vitiFoodMasters = reportGrowingSystem.getFoodMasters();
        if (vitiFoodMasters != null) {
            GrowingSystem growingSystem = reportGrowingSystem.getGrowingSystem();
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            Domain domain = growingPlan.getDomain();

            vitiFoodMasters = vitiFoodMasters.stream().filter(foodMaster -> Sector.VITICULTURE.equals(foodMaster.getSector())).toList();
            for (FoodMaster vitiFoodMaster : vitiFoodMasters) {
                Collection<CroppingPlanEntry> crops = vitiFoodMaster.getCrops();

                List<Object> args = new ArrayList<>();
                exportCommonData(args, reportGrowingSystem, growingSystem, domain, "Alimentation hydrique et minérale", crops);
                // Groupe cible
                args.add("");
                // Maladie/Ravageur (champ vide pour les autres thématiques)
                args.add("");
                // Pression sur le système de culture - Échelle de pression
                args.add("");
                // Pression sur le système de culture - Evolution par rapport à l’année précédente
                args.add("");
                // Pression sur le système de culture – Expression de l’agriculteur
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Échelle de maîtrise
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Note globale d’attaque sur feuilles
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Note de fréquence d’attaque sur feuilles
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Note d'intensité d’attaque sur feuilles
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Note globale d’attaque sur grappes
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Note de fréquence d’attaque sur grappes
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Note d'intensité d’attaque sur grappes
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Nombre de perforations pour 100 grappes
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Qualification du niveau de maîtrise
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - expression de l’agriculteur
                args.add("");
                // Résultats obtenus, niveau de maîtrise finale - Commentaires du conseiller
                args.add("");
                // IFT (chimique)
                args.add("");
                // IFT biocontrôle
                args.add("");
                // Nombre de traitements
                args.add("");
                // dont nombre de traitements obligatoires
                args.add("");
                // Irrigation
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(vitiFoodMaster.getFoodIrrigation()));
                // Stress hydrique
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(vitiFoodMaster.getHydriqueStress()));
                // Azote
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(vitiFoodMaster.getAzoteStress()));
                // Alimentation minérale (hors azote)
                args.add(vitiFoodMaster.getMineralFood());
                // Température et rayonnement
                args.add(AgrosystI18nService.getEnumTraductionWithDefaultLocale(vitiFoodMaster.getTempStress()));
                // Commentaires – Résultats obtenus et faits marquants
                args.add(vitiFoodMaster.getComment());

                writeToSheet(vitiSheet, args.toArray());
            }
        }
    }

    private void exportCommonData(List<Object> args,
                                  ReportGrowingSystem reportGrowingSystem,
                                  GrowingSystem growingSystem,
                                  Domain domain,
                                  String typeThematique,
                                  Collection<CroppingPlanEntry> crops) {
        // ID du Bilan de campagne
        args.add(reportGrowingSystem.getTopiaId());
        // Nom du Bilan de campagne
        args.add(reportGrowingSystem.getName());
        // Campagne
        args.add(domain.getCampaign());
        // Département
        args.add(domain.getLocation().getDepartement());
        // Réseau de rattachement du SDC
        args.add(growingSystem.isNetworksEmpty() ? "" :
                growingSystem.getNetworks().stream()
                        .filter(Objects::nonNull)
                        .map(Network::getName)
                        .distinct()
                        .collect(Collectors.joining(", ")));
        // Système de culture
        args.add(growingSystem.getName());
        // Type de conduite du SDC
        args.add(Optional.ofNullable(growingSystem.getTypeAgriculture())
                .map(RefTypeAgriculture::getReference_label)
                .orElse(null));
        // Numéro DEPHY
        args.add(growingSystem.getDephyNumber());
        // Type de thématique (adventices vs. maladies vs. ravageurs vs. verse vs. alimentation hydrique et minérale vs. Rendement et qualité)
        args.add(typeThematique);
        // Culture
        args.add(crops == null ? null :
                crops.stream()
                        .map(CroppingPlanEntry::getName)
                        .collect(Collectors.joining(", ")));
        // Espèces
        args.add(crops == null ? null :
                crops.stream()
                        .flatMap(crop -> CollectionUtils.emptyIfNull(crop.getCroppingPlanSpecies()).stream())
                        .map(CroppingPlanSpecies::getSpecies)
                        .filter(Objects::nonNull)
                        .map(RefEspece::getLibelle_espece_botanique_Translated)
                        .collect(Collectors.joining(", ")));
        // Variétés
        args.add(crops == null ? null :
                crops.stream()
                        .flatMap(crop -> CollectionUtils.emptyIfNull(crop.getCroppingPlanSpecies()).stream())
                        .map(CroppingPlanSpecies::getVariety)
                        .filter(Objects::nonNull)
                        .map(RefVariete::getLabel)
                        .collect(Collectors.joining(", ")));
    }
}
