package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefEdaplosTypeTraitement;
import fr.inra.agrosyst.api.entities.referential.RefEdaplosTypeTraitementImpl;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot;
import fr.inra.agrosyst.services.referential.json.RefActaProduitRootExport;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefEdaplosTypeTraitemenModel extends AbstractAgrosystModel<RefEdaplosTypeTraitement> implements ExportModel<RefEdaplosTypeTraitement> {

    public RefEdaplosTypeTraitemenModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("reference_code", RefEdaplosTypeTraitement.PROPERTY_REFERENCE_CODE);
        newMandatoryColumn("reference_label", RefEdaplosTypeTraitement.PROPERTY_REFERENCE_LABEL);
        newMandatoryColumn("id_traitement", RefEdaplosTypeTraitement.PROPERTY_ID_TRAITEMENT, INT_PARSER);
        newMandatoryColumn("code_traitement", RefEdaplosTypeTraitement.PROPERTY_CODE_TRAITEMENT);
        newMandatoryColumn("nom_traitement", RefEdaplosTypeTraitement.PROPERTY_NOM_TRAITEMENT);
        newOptionalColumn(COLUMN_ACTIVE, RefEdaplosTypeTraitement.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefEdaplosTypeTraitement, Object>> getColumnsForExport() {
        ModelBuilder<RefActaProduitRoot> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("reference_code", RefEdaplosTypeTraitement.PROPERTY_REFERENCE_CODE);
        modelBuilder.newColumnForExport("reference_label", RefEdaplosTypeTraitement.PROPERTY_REFERENCE_LABEL);
        modelBuilder.newColumnForExport("id_traitement", RefEdaplosTypeTraitement.PROPERTY_ID_TRAITEMENT, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("code_traitement", RefEdaplosTypeTraitement.PROPERTY_CODE_TRAITEMENT);
        modelBuilder.newColumnForExport("nom_traitement", RefEdaplosTypeTraitement.PROPERTY_NOM_TRAITEMENT);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefActaProduitRootExport.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefEdaplosTypeTraitement newEmptyInstance() {
        RefEdaplosTypeTraitement refEdaplosTypeTraitement = new RefEdaplosTypeTraitementImpl();
        refEdaplosTypeTraitement.setActive(true);
        return refEdaplosTypeTraitement;
    }
}
