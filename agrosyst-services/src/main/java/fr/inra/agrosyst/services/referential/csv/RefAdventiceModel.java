package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefAdventiceImpl;
import fr.inra.agrosyst.services.common.CommonService;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Modele d'import du référentiel Adventices.
 * 
 * <ul>
 * <li>adventice
 * <li>identifiant
 * <li>Famille de culture
 * <li>Source
 * </ul>
 * 
 * @author Eric Chatellier
 */
public class RefAdventiceModel extends AbstractAgrosystModel<RefAdventice> implements ExportModel<RefAdventice> {

    public RefAdventiceModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("adventice", RefAdventice.PROPERTY_ADVENTICE);
        newMandatoryColumn("identifiant", RefAdventice.PROPERTY_IDENTIFIANT);
        newMandatoryColumn("Famille de culture", RefAdventice.PROPERTY_FAMILLE_DE_CULTURE);
        newMandatoryColumn("Source", RefAdventice.PROPERTY_SOURCE);
        newMandatoryColumn("main", RefAdventice.PROPERTY_MAIN, CommonService.BOOLEAN_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefAdventice.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefAdventice, Object>> getColumnsForExport() {
        ModelBuilder<RefAdventice> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("adventice", RefAdventice.PROPERTY_ADVENTICE);
        modelBuilder.newColumnForExport("identifiant", RefAdventice.PROPERTY_IDENTIFIANT);
        modelBuilder.newColumnForExport("Famille de culture", RefAdventice.PROPERTY_FAMILLE_DE_CULTURE);
        modelBuilder.newColumnForExport("Source", RefAdventice.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport("main", RefAdventice.PROPERTY_MAIN, T_F_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefAdventice.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefAdventice newEmptyInstance() {
        RefAdventice refAdventice = new RefAdventiceImpl();
        refAdventice.setActive(true);
        return refAdventice;
    }
}
