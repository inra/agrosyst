package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceImpl;
import fr.inra.agrosyst.services.common.CommonService;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefHarvestingPriceModel extends AbstractDestinationAndPriceModel<RefHarvestingPrice> implements ExportModel<RefHarvestingPrice> {

    public RefHarvestingPriceModel() {
        super(CSV_SEPARATOR);
    }

    public RefHarvestingPriceModel(RefDestinationTopiaDao refDestinationsDao, RefEspeceTopiaDao refEspeceDao) {
        super(CSV_SEPARATOR);
        super.refDestinationsDao = refDestinationsDao;
        super.refEspeceDao = refEspeceDao;
        getUpperCodeEspeceBotaniqueToCodeEspeceBotanique();
        getRefDestinationLabelByNormalisedLabeled();
        getRefDestinationByCodes();

        newMandatoryColumn("Code scenario", RefHarvestingPrice.PROPERTY_CODE_SCENARIO, STRING_NOT_NULL_PARSER);
        newMandatoryColumn("Code_destination_A", RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A, CODE_DESTINATION_A_PARSER);
        newOptionalColumn("Destination", RefHarvestingPrice.PROPERTY_DESTINATION, DESTINATION_LABEL_PARSER);
        newOptionalColumn("Code espece botanique", RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE);
        newOptionalColumn("Code qualifiant AEE", RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE);
        newOptionalColumn("Produit récolté", RefHarvestingPrice.PROPERTY_PRODUIT_RECOLTE);
        newOptionalColumn("Libellé qualifiant AEE", RefHarvestingPrice.PROPERTY_LIBELLE_QUALIFIANT__AEE);
        newMandatoryColumn("Type agriculture (AB / Autre)", RefHarvestingPrice.PROPERTY_ORGANIC, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("Période de commercialisation - Mois", RefHarvestingPrice.PROPERTY_MARKETING_PERIOD, MARKETING_PERIOD_PARSER);
        newMandatoryColumn("Période de commercialisation - Décade", RefHarvestingPrice.PROPERTY_MARKETING_PERIOD_DECADE, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("Unité de prix", RefHarvestingPrice.PROPERTY_PRICE_UNIT, PRICE_UNIT_PARSER);
        newMandatoryColumn("Prix", RefHarvestingPrice.PROPERTY_PRICE, DOUBLE_PARSER);
        newMandatoryColumn("Campagne", RefHarvestingPrice.PROPERTY_CAMPAIGN, INT_PARSER);
        newMandatoryColumn("Scénario", RefHarvestingPrice.PROPERTY_SCENARIO);
        newOptionalColumn(COLUMN_ACTIVE, RefHarvestingPrice.PROPERTY_ACTIVE, ACTIVE_PARSER);
        newOptionalColumn("Source", RefHarvestingPrice.PROPERTY_SOURCE);
    }

    @Override
    public Iterable<ExportableColumn<RefHarvestingPrice, Object>> getColumnsForExport() {
        ModelBuilder<RefDestination> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Code scenario", RefHarvestingPrice.PROPERTY_CODE_SCENARIO);
        modelBuilder.newColumnForExport("Code_destination_A", RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A);
        modelBuilder.newColumnForExport("Destination", RefHarvestingPrice.PROPERTY_DESTINATION);
        modelBuilder.newColumnForExport("Code espece botanique", RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE);
        modelBuilder.newColumnForExport("Code qualifiant AEE", RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE);
        modelBuilder.newColumnForExport("Produit récolté", RefHarvestingPrice.PROPERTY_PRODUIT_RECOLTE);
        modelBuilder.newColumnForExport("Libellé qualifiant AEE", RefHarvestingPrice.PROPERTY_LIBELLE_QUALIFIANT__AEE);
        modelBuilder.newColumnForExport("Type agriculture (AB / Autre)", RefHarvestingPrice.PROPERTY_ORGANIC, O_N_FORMATTER);
        modelBuilder.newColumnForExport("Période de commercialisation - Mois", RefHarvestingPrice.PROPERTY_MARKETING_PERIOD, MARKETING_PERIOD_FORMATTER);
        modelBuilder.newColumnForExport("Période de commercialisation - Décade", RefHarvestingPrice.PROPERTY_MARKETING_PERIOD_DECADE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Unité de prix", RefHarvestingPrice.PROPERTY_PRICE_UNIT, PRICE_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("Prix", RefHarvestingPrice.PROPERTY_PRICE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Campagne", RefHarvestingPrice.PROPERTY_CAMPAIGN, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Scénario", RefHarvestingPrice.PROPERTY_SCENARIO);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefHarvestingPrice.PROPERTY_ACTIVE, T_F_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefHarvestingPrice.PROPERTY_SOURCE);

        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefHarvestingPrice newEmptyInstance() {
        RefHarvestingPrice refHarvestingPrice = new RefHarvestingPriceImpl();
        refHarvestingPrice.setActive(true);
        return refHarvestingPrice;
    }
}
