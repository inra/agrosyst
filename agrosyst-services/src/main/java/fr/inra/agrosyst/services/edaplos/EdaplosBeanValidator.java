package fr.inra.agrosyst.services.edaplos;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.inra.agrosyst.api.services.edaplos.EdaplosParsingResult;
import fr.inra.agrosyst.api.services.edaplos.EdaplosParsingStatus;
import fr.inra.agrosyst.api.services.edaplos.EdaplosResultLog;
import fr.inra.agrosyst.services.edaplos.model.AgroEdiObject;
import fr.inra.agrosyst.services.edaplos.model.CropDataSheetMessages;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Path;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Validation via bean validation api.
 */
public class EdaplosBeanValidator {

    private static final Log LOGGER = LogFactory.getLog(EdaplosBeanValidator.class);

    public void validate(CropDataSheetMessages cropDataSheetMessages, EdaplosParsingResult result) {
        EdaplosParsingStatus status = EdaplosParsingStatus.SUCCESS;

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<CropDataSheetMessages>> violations = validator.validate(cropDataSheetMessages);
        for (ConstraintViolation<CropDataSheetMessages> violation : violations) {
            String message = violation.getMessage();

            // convert level
            EdaplosResultLog.EdaplosResultLevel level;
            if (violation.getConstraintDescriptor().getPayload().contains(EdaplosParsingPayload.INFO.class)) {
                level = EdaplosResultLog.EdaplosResultLevel.INFO;
            } else if (violation.getConstraintDescriptor().getPayload().contains(EdaplosParsingPayload.WARNING.class)) {
                level = EdaplosResultLog.EdaplosResultLevel.WARNING;
            } else {
                status = EdaplosParsingStatus.FAIL;
                level = EdaplosResultLog.EdaplosResultLevel.ERROR;
            }

            // add log
            EdaplosResultLog log = new EdaplosResultLog(level, message, null);
            log.setPath(computePath(violation));
            result.addMessage(log);

        }
        factory.close();

        result.setEdaplosParsingStatus(status);
    }

    /**
     * Contruit l'information de path de l'erreur.
     *
     * @param violation l'erreur
     * @return le path
     */
    protected String computePath(ConstraintViolation<CropDataSheetMessages> violation) {
        List<String> paths = new ArrayList<>();

        Object currentBean = violation.getRootBean();

        try {
            // 0 = root

            for (Path.Node node : violation.getPropertyPath()) {
                // leaf is not usefull
                if (!currentBean.equals(violation.getLeafBean())) {
                    if (node.getIndex() != null) {
                        currentBean = ((List) currentBean).get(node.getIndex());
                        paths.add(computeElementInfo(currentBean));
                    }

                    if (!currentBean.equals(violation.getLeafBean())) {
                        currentBean = PropertyUtils.getProperty(currentBean, node.getName());
                        if (!(currentBean instanceof Collection)) {
                            paths.add(computeElementInfo(currentBean));
                        }
                    }
                }
            }
        } catch (IllegalAccessException|InvocationTargetException|NoSuchMethodException ex) {
            LOGGER.error("Can't access property");
        }

        return paths.stream()
                .filter(StringUtils::isNotBlank)
                .collect(Collectors.joining(", "));
    }

    protected String computeElementInfo(Object currentBean) {
        String info = ((AgroEdiObject)currentBean).getLocalizedIdentifier();
        return info;
    }
}
