package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefNrjGesOutil;
import fr.inra.agrosyst.api.entities.referential.RefNrjGesOutilImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Code type de materiel GES'TIM;libelle type de materiel GES'TIM;GES (kq eq-CO2/kg outil);NRJ (MJ/kg outil);source
 * 
 * @author Eric Chatellier
 */
public class RefNrjGesOutilModel extends AbstractAgrosystModel<RefNrjGesOutil> implements ExportModel<RefNrjGesOutil> {

    public RefNrjGesOutilModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Code type de materiel GES'TIM", RefNrjGesOutil.PROPERTY_CODE_TYPE_MATERIEL);
        newMandatoryColumn("libelle type de materiel GES'TIM", RefNrjGesOutil.PROPERTY_LIBELLE_TYPE_MATERIEL);
        newMandatoryColumn("GES (kq eq-CO2/kg outil)", RefNrjGesOutil.PROPERTY_GES, DOUBLE_PARSER);
        newMandatoryColumn("NRJ (MJ/kg outil)", RefNrjGesOutil.PROPERTY_NRJ, DOUBLE_PARSER);
        newMandatoryColumn("source", RefNrjGesOutil.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefNrjGesOutil.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefNrjGesOutil, Object>> getColumnsForExport() {
        ModelBuilder<RefNrjGesOutil> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Code type de materiel GES'TIM", RefNrjGesOutil.PROPERTY_CODE_TYPE_MATERIEL);
        modelBuilder.newColumnForExport("libelle type de materiel GES'TIM", RefNrjGesOutil.PROPERTY_LIBELLE_TYPE_MATERIEL);
        modelBuilder.newColumnForExport("GES (kq eq-CO2/kg outil)", RefNrjGesOutil.PROPERTY_GES, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("NRJ (MJ/kg outil)", RefNrjGesOutil.PROPERTY_NRJ, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("source", RefNrjGesOutil.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefNrjGesOutil.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefNrjGesOutil newEmptyInstance() {
        RefNrjGesOutil refNrjGesOutil = new RefNrjGesOutilImpl();
        refNrjGesOutil.setActive(true);
        return refNrjGesOutil;
    }
}
