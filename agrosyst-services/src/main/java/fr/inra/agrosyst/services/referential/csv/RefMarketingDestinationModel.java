package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefMarketingDestination;
import fr.inra.agrosyst.api.entities.referential.RefMarketingDestinationImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;


public class RefMarketingDestinationModel extends AbstractAgrosystModel<RefMarketingDestination> implements ExportModel<RefMarketingDestination> {

    public RefMarketingDestinationModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Filière", RefMarketingDestination.PROPERTY_SECTOR, SECTOR_PARSER);
        newMandatoryColumn("Mode de commercialisation", RefMarketingDestination.PROPERTY_MARKETING_DESTINATION);
        newOptionalColumn(COLUMN_ACTIVE, RefMarketingDestination.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefMarketingDestination, Object>> getColumnsForExport() {
        ModelBuilder<RefMarketingDestination> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Filière", RefMarketingDestination.PROPERTY_SECTOR, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Mode de commercialisation", RefMarketingDestination.PROPERTY_MARKETING_DESTINATION);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefMarketingDestination.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefMarketingDestination newEmptyInstance() {
        RefMarketingDestination refMarketingDestination = new RefMarketingDestinationImpl();
        refMarketingDestination.setActive(true);
        return refMarketingDestination;
    }

}
