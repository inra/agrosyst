package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ValueParserFormatter;

public class EuropeanProductModel extends AbstractAgrosystModel<EuropeanProductDto> implements ExportModel<EuropeanProductDto> {

    public EuropeanProductModel() {
        super(CSV_SEPARATOR);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_AMM_UE);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_NOM_PRODUIT);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_SA_1);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_DOSE_1, DOUBLE_PARSER_FORMATTER);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_UNITE_DOSE_1);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_SA_2);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_DOSE_2, DOUBLE_PARSER_FORMATTER);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_UNITE_DOSE_2);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_SA_3);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_DOSE_3, DOUBLE_PARSER_FORMATTER);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_UNITE_DOSE_3);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_SA_4);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_DOSE_4, DOUBLE_PARSER_FORMATTER);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_UNITE_DOSE_4);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_SA_5);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_DOSE_5, DOUBLE_PARSER_FORMATTER);
        newColumnForImportExport(EuropeanProductDto.PROPERTY_UNITE_DOSE_5);
        newColumnForExport(EuropeanProductDto.PROPERTY_AMM_NOM_PRODUIT);
        newColumnForExport(EuropeanProductDto.PROPERTY_AMM_SA);
        newColumnForExport(EuropeanProductDto.PROPERTY_AMM_PROBABLE);
        newColumnForExport(EuropeanProductDto.PROPERTY_REF_DATA);
    }
    
    @Override
    public EuropeanProductDto newEmptyInstance() {
        return new EuropeanProductDto();
    }

    @Override
    public Iterable<ExportableColumn<EuropeanProductDto, Object>> getColumnsForExport() {
        return (Iterable) this.modelBuilder.getColumnsForExport();
    }

    protected void newColumnForImportExport(String property) {
        this.modelBuilder.newColumnForImportExport(property, property);
    }

    protected void newColumnForImportExport(String property, ValueParserFormatter<?> parserFormatter) {
        this.modelBuilder.newColumnForImportExport(property, property, parserFormatter);
    }

    protected void newColumnForExport(String property) {
        this.modelBuilder.newColumnForExport(property, property);
    }

}
