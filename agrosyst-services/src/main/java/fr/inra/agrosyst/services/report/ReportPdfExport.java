/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.services.report;

import com.google.common.collect.Lists;
import com.itextpdf.text.DocumentException;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.Section;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.report.ArboAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropPestMaster;
import fr.inra.agrosyst.api.entities.report.ArboPestMaster;
import fr.inra.agrosyst.api.entities.report.CropPestMaster;
import fr.inra.agrosyst.api.entities.report.FoodMaster;
import fr.inra.agrosyst.api.entities.report.PestMaster;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.entities.report.YieldLoss;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.BinaryContent;
import fr.inra.agrosyst.api.services.report.ReportExportOption;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import freemarker.cache.ClassTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * All stuff related to Report PDF export.
 */
public class ReportPdfExport {
    
    final static Version VERSION = Configuration.VERSION_2_3_29;

    public BinaryContent exportPdfReportGrowingSystems(ReportExportOption options,
                                                      List<ReportGrowingSystem> reportGrowingSystems,
                                                      Map<ReportGrowingSystem, ManagementMode> managementModes,
                                                      Map<String, String> groupesCiblesParCode) {

        try {
            byte[] bytes;
            if (reportGrowingSystems.size() == 1) {
                try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                    ReportGrowingSystem reportGrowingSystem = reportGrowingSystems.getFirst();
                    ManagementMode managementMode = managementModes.get(reportGrowingSystem);
                    ReportExportContext context = new ReportExportContext(options, reportGrowingSystem, managementMode, groupesCiblesParCode);
                    exportPdfGrowingSystem(outputStream, context);
                    bytes = outputStream.toByteArray();
                }

                return BinaryContent.forPdf(bytes);

            } else {
                // zip stream
                try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream(); ZipOutputStream zos = new ZipOutputStream(outputStream)) {
                    // main directory
                    String directoryName = "Bilan de campagne pdf/";
                    zos.putNextEntry(new ZipEntry(directoryName));
                    zos.closeEntry();

                    // export
                    Set<String> usedNames = new HashSet<>();
                    for (ReportGrowingSystem reportGrowingSystem : reportGrowingSystems) {
                        ManagementMode managementMode = managementModes.get(reportGrowingSystem);
                        ReportExportContext context = new ReportExportContext(options, reportGrowingSystem, managementMode, groupesCiblesParCode);
                        exportPdfGrowingSystemZip(zos, directoryName, usedNames, context);
                    }
                    zos.finish();
                    bytes = outputStream.toByteArray();
                }

                return BinaryContent.forZip(bytes);
            }

        } catch (IOException ex) {
            throw new AgrosystTechnicalException("Can't export pdf", ex);
        }
    }

    /**
     * Export a single ReportGrowingSystem into zip file.
     * @param zos zip output stream
     * @param usedNames already used name for report
     * @param context report context
     */
    protected void exportPdfGrowingSystemZip(ZipOutputStream zos,
                                             String directoryName,
                                             Set<String> usedNames,
                                             ReportExportContext context) throws IOException {
        ReportGrowingSystem reportGrowingSystem = context.getReportGrowingSystem();

        // get entry name
        String entryName = directoryName +
                AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystem.getSectors().iterator().next())
                + "_" + reportGrowingSystem.getGrowingSystem().getGrowingPlan().getDomain().getName()
                + "_" + reportGrowingSystem.getName()
                + "_" + reportGrowingSystem.getGrowingSystem().getGrowingPlan().getDomain().getCampaign()
                + ".pdf";

        // add suffix if name is already taken
        int number = 1;
        while (usedNames.contains(entryName)) {
            entryName = directoryName +
                    AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystem.getSectors().iterator().next())
                    + "_" + reportGrowingSystem.getGrowingSystem().getGrowingPlan().getDomain().getName()
                    + "_" + reportGrowingSystem.getName()
                    + "_" + reportGrowingSystem.getGrowingSystem().getGrowingPlan().getDomain().getCampaign()
                    + "_" + number
                    + ".pdf";
            number++;
        }

        usedNames.add(entryName);
        zos.putNextEntry(new ZipEntry(entryName));
        exportPdfGrowingSystem(zos, context);
        zos.closeEntry();
    }

    protected void exportPdfGrowingSystem(OutputStream os, ReportExportContext context) throws IOException {
        try (ByteArrayOutputStream osTemp = new ByteArrayOutputStream()){
            String htmlContent = exportPdfGrowingSystemHtmlContent(context);

            // convert into pdf
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(htmlContent);
            renderer.layout();
            renderer.createPDF(osTemp);
            os.write(osTemp.toByteArray());
        } catch (DocumentException |TemplateException ex) {
            throw new IOException(ex);
        }
    }

    protected String exportPdfGrowingSystemHtmlContent(ReportExportContext context) throws IOException, TemplateException {
        // freemarker configuration
        Configuration freemarkerConfiguration = new Configuration(VERSION);
        freemarkerConfiguration.setEncoding(Locale.getDefault(), "UTF-8");
        ClassTemplateLoader templateLoader = new ClassTemplateLoader(ReportGrowingSystem.class, "/ftl");
        freemarkerConfiguration.setTemplateLoader(templateLoader);
        freemarkerConfiguration.setObjectWrapper(new BeansWrapper(VERSION));

        // render freemarker template
        Template mapTemplate = freemarkerConfiguration.getTemplate("reportGrowingSystem.ftl");
        Map<String, Object> dataModel = new HashMap<>();
        ReportGrowingSystem reportGrowingSystem = context.getReportGrowingSystem();
        dataModel.put("reportGrowingSystem", reportGrowingSystem);
        GrowingPlan growingPlan = reportGrowingSystem.getGrowingSystem().getGrowingPlan();
        dataModel.put("dephyExpe", growingPlan != null && TypeDEPHY.DEPHY_EXPE.equals(growingPlan.getType()));
        if (context.isIncludeReportRegional()) {
            dataModel.put("reportRegional", reportGrowingSystem.getReportRegional());
        }
        dataModel.put("reportGrowingSystemSections", context.getReportGrowingSystemSections());
        dataModel.put("managementSections", context.getManagementSections());
        dataModel.put("enum", new ReportEnumUtils());
        dataModel.put("groupesCiblesParCode", context.getGroupesCiblesParCode());

        // transform pest masters collections
        tranformHighlightCollections(dataModel, reportGrowingSystem);

        // add management mode
        addManagementModeCollections(dataModel, context.getManagementMode());

        // final rendering
        Writer out = new StringWriter();
        mapTemplate.process(dataModel, out);
        out.flush();
        return out.toString();
    }

    /**
     * Transform each collection to collection of collection of Three simple element only.
     * Collection&lt;Collection&lt;WithCropObject&gt;&gt;
     *
     * @param dataModel freemarker data model
     * @param reportGrowingSystem report
     */
    protected void tranformHighlightCollections(Map<String, Object> dataModel, ReportGrowingSystem reportGrowingSystem) {

        // withCropAdventiceMasters
        if (reportGrowingSystem.getCropAdventiceMasters() != null) {
            dataModel.put("withCropAdventiceMastersSplitted", tranformCropPestMasterCollections(reportGrowingSystem.getCropAdventiceMasters()));
        }
        // withCropDiseaseMasters
        if (reportGrowingSystem.getCropDiseaseMasters() != null) {
            dataModel.put("withCropDiseaseMastersSplitted", tranformCropPestMasterCollections(reportGrowingSystem.getCropDiseaseMasters()));
        }
        // withCropPestMasters
        if (reportGrowingSystem.getCropPestMasters() != null) {
            dataModel.put("withCropPestMastersSplitted", tranformCropPestMasterCollections(reportGrowingSystem.getCropPestMasters()));
        }
        // verse
        if (reportGrowingSystem.getVerseMasters() != null) {
            dataModel.put("verseMastersSplitted", splitIn3(reportGrowingSystem.getVerseMasters()));
        }
        // food
        if (reportGrowingSystem.getFoodMasters() != null) {
            List<FoodMaster> foodMasters = reportGrowingSystem.getFoodMasters().stream().filter(
                    foodMaster -> !Sector.ARBORICULTURE.equals(foodMaster.getSector()) &&
                            !Sector.VITICULTURE.equals(foodMaster.getSector())).collect(Collectors.toList());
            dataModel.put("foodMastersSplitted", splitIn3(foodMasters));
        }
        // yieldLosses
        if (reportGrowingSystem.getYieldLosses() != null) {
            List<YieldLoss> allYieldLoss = reportGrowingSystem.getYieldLosses().stream().filter(yieldLoss ->
                    !Sector.ARBORICULTURE.equals(yieldLoss.getSector())).collect(Collectors.toList());
            dataModel.put("yieldLossesSplitted", splitIn3(allYieldLoss));
        }
        // withCropAdventiceMasters
        if (reportGrowingSystem.getArboCropAdventiceMasters() != null) {
            dataModel.put("withArboCropAdventiceMastersSplitted", tranformArboCropAdventiceMasterCollections(reportGrowingSystem.getArboCropAdventiceMasters()));
        }
        // withCropDiseaseMasters
        if (reportGrowingSystem.getArboCropDiseaseMasters() != null) {
            dataModel.put("withArboCropDiseaseMastersSplitted", tranformArboCropPestMasterCollections(reportGrowingSystem.getArboCropDiseaseMasters()));
        }
        // withCropPestMasters
        if (reportGrowingSystem.getArboCropPestMasters() != null) {
            dataModel.put("withArboCropPestMastersSplitted", tranformArboCropPestMasterCollections(reportGrowingSystem.getArboCropPestMasters()));
        }
        // arbofood
        if (reportGrowingSystem.getFoodMasters() != null) {
            List<FoodMaster> arboFoodMasters = reportGrowingSystem.getFoodMasters().stream().filter(
                    foodMaster -> Sector.ARBORICULTURE.equals(foodMaster.getSector())).collect(Collectors.toList());
            dataModel.put("arboFoodMastersSplitted", splitIn3(arboFoodMasters));
        }
        // arboyieldLosses
        if (reportGrowingSystem.getYieldLosses() != null) {
            List<YieldLoss> arboYieldLoss = reportGrowingSystem.getYieldLosses().stream().filter(yieldLoss ->
                    Sector.ARBORICULTURE.equals(yieldLoss.getSector())).collect(Collectors.toList());
            dataModel.put("arboYieldLossesSplitted", splitIn3(arboYieldLoss));
        }
        // vitiDiseaseMasters
        if (reportGrowingSystem.getVitiDiseaseMasters() != null) {
            dataModel.put("vitiDiseaseMastersSplitted", splitIn3(reportGrowingSystem.getVitiDiseaseMasters()));
        }
        // vitiPestMasters
        if (reportGrowingSystem.getVitiPestMasters() != null) {
            dataModel.put("vitiPestMastersSplitted", splitIn3(reportGrowingSystem.getVitiPestMasters()));
        }
        // vitifood
        if (reportGrowingSystem.getFoodMasters() != null) {
            List<FoodMaster> vitiFoodMasters = reportGrowingSystem.getFoodMasters().stream().filter(
                    foodMaster -> Sector.VITICULTURE.equals(foodMaster.getSector())).collect(Collectors.toList());
            dataModel.put("vitiFoodMastersSplitted", splitIn3(vitiFoodMasters));
        }
    }

    protected Map<String, List<List<WithCropObject>>> tranformCropPestMasterCollections(Collection<CropPestMaster> cropPestMasters) {
        Map<String, List<List<WithCropObject>>> result = new HashMap<>();
        MultiValuedMap<Sector, CropPestMaster> cropPestMastersBySector = new HashSetValuedHashMap<>();
        cropPestMasters.forEach(cropPestMaster -> cropPestMastersBySector.put(cropPestMaster.getSector(), cropPestMaster));
        cropPestMastersBySector.keySet().forEach(sector -> {
            List<WithCropObject> withCropPestMaster = new ArrayList<>();
            for (CropPestMaster cropPestMaster : cropPestMastersBySector.get(sector)) {
                Collection<PestMaster> pestMasters = cropPestMaster.getPestMasters();
                for (PestMaster pestMaster : pestMasters) {
                    WithCropObject wco = new WithCropObject();
                    wco.setObject(pestMaster);
                    wco.setCrop(cropPestMaster);
                    wco.setSpan(1);
                    withCropPestMaster.add(wco);
                }
            }
            result.put(String.valueOf(sector), fixCropSpan(splitIn3(withCropPestMaster)));
        });
        return result;
    }

    protected List<List<WithCropObject>> tranformArboCropPestMasterCollections(Collection<ArboCropPestMaster> arboCropPestMasters) {
        List<WithCropObject> withCropPestMaster = new ArrayList<>();
        for (ArboCropPestMaster arboCropPestMaster : arboCropPestMasters) {
            Collection<ArboPestMaster> arboPestMasters = arboCropPestMaster.getPestMasters();
            for (ArboPestMaster arboPestMaster : arboPestMasters) {
                WithCropObject wco = new WithCropObject();
                wco.setObject(arboPestMaster);
                wco.setCrop(arboCropPestMaster);
                wco.setSpan(1);
                withCropPestMaster.add(wco);
            }
        }
        return fixCropSpan(splitIn3(withCropPestMaster));
    }

    protected List<List<WithCropObject>> tranformArboCropAdventiceMasterCollections(Collection<ArboCropAdventiceMaster> arboCropAdventiceMasters) {
        List<WithCropObject> withCropPestMaster = new ArrayList<>();
        for (ArboCropAdventiceMaster arboCropAdventiceMaster : arboCropAdventiceMasters) {
            Collection<ArboAdventiceMaster> arboAdventiceMasters = arboCropAdventiceMaster.getPestMasters();
            for (ArboAdventiceMaster arboAdventiceMaster : arboAdventiceMasters) {
                WithCropObject wco = new WithCropObject();
                wco.setObject(arboAdventiceMaster);
                wco.setCrop(arboCropAdventiceMaster);
                wco.setSpan(1);
                withCropPestMaster.add(wco);
            }
        }
        return fixCropSpan(splitIn3(withCropPestMaster));
    }

    /**
     * Découpe une collection en plusieurs collections de 3 elements.
     *
     * @param withCropObjects collection
     * @param <T> type generique
     * @return splitted collection
     */
    protected static <T> List<List<T>> splitIn3(Collection<T> withCropObjects) {
        // split collection in  collection of three
        List<T> withCropObjects0 = withCropObjects == null ? new ArrayList<>() : Lists.newArrayList(withCropObjects);
        List<List<T>> withCropObjectsSplitted = new ArrayList<>();
        int index = 0;
        for (T withCropObject : withCropObjects0) {
            if (index % 3 == 0) {
                withCropObjectsSplitted.add(new ArrayList<>());
            }
            List<T> objects = withCropObjectsSplitted.get(withCropObjectsSplitted.size() - 1);
            objects.add(withCropObject);
            index++;
        }

        return withCropObjectsSplitted;
    }

    /**
     * Remplace les spans des éléments s'il se suivent dans la même collection.
     *
     * @param withCropObjectsSplitted collection
     * @return same collection
     */
    protected List<List<WithCropObject>> fixCropSpan(List<List<WithCropObject>> withCropObjectsSplitted) {

        // fix crop span
        for (List<WithCropObject> withCropObjectSplitted : withCropObjectsSplitted) {
            for (int i = withCropObjectSplitted.size() - 1; i >= 1; i--) { // warning : really >= 1 (not 0)
                WithCropObject current = withCropObjectSplitted.get(i);
                WithCropObject previous = withCropObjectSplitted.get(i - 1);
                if (current.getCrop().equals(previous.getCrop())) {
                    previous.setSpan(previous.getSpan() + current.getSpan());
                    current.setSpan(0);
                }
            }
        }

        return withCropObjectsSplitted;
    }

    /**
     * Ajout des collections des modes de gestion.
     *
     * @param dataModel freemarker data model
     * @param managementMode management mode
     */
    protected void addManagementModeCollections(Map<String, Object> dataModel, ManagementMode managementMode) {
        if (managementMode != null) {

            List<Section> sectionCycleCultures = managementMode.getSections().stream()
                    .filter(section -> section.getSectionType() == SectionType.CYCLE_PLURIANNUEL_DE_CULTURE)
                    .collect(Collectors.toList());
            dataModel.put("sectionCycleCultures", sectionCycleCultures);

            List<Section> sectionTravailSols = managementMode.getSections().stream()
                    .filter(section -> section.getSectionType() == SectionType.TRAVAIL_DU_SOL)
                    .collect(Collectors.toList());
            dataModel.put("sectionTravailSols", sectionTravailSols);

            List<Section> sectionAdventices = managementMode.getSections().stream()
                    .filter(section -> section.getSectionType() == SectionType.ADVENTICES)
                    .collect(Collectors.toList());
            dataModel.put("sectionAdventices", sectionAdventices);

            List<Section> sectionMaladies = managementMode.getSections().stream()
                    .filter(section -> section.getSectionType() == SectionType.MALADIES)
                    .collect(Collectors.toList());
            dataModel.put("sectionMaladies", sectionMaladies);

            List<Section> sectionRavageurs = managementMode.getSections().stream()
                    .filter(section -> section.getSectionType() == SectionType.RAVAGEURS)
                    .collect(Collectors.toList());
            dataModel.put("sectionRavageurs", sectionRavageurs);

            List<Section> sectionFertiliteSols = managementMode.getSections().stream()
                    .filter(section -> section.getSectionType() == SectionType.FERTILITE_SOL_CULTURES)
                    .collect(Collectors.toList());
            dataModel.put("sectionFertiliteSols", sectionFertiliteSols);

            List<Section> sectionProductions = managementMode.getSections().stream()
                    .filter(section -> section.getSectionType() == SectionType.PRODUCTION)
                    .collect(Collectors.toList());
            dataModel.put("sectionProductions", sectionProductions);
        }
    }

}
