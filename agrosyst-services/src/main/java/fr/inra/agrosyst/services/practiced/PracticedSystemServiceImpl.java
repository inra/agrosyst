package fr.inra.agrosyst.services.practiced;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Functions;
import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.Cattle;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.LivestockUnit;
import fr.inra.agrosyst.api.entities.LivestockUnitTopiaDao;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.SolHorizon;
import fr.inra.agrosyst.api.entities.SolHorizonTopiaDao;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.ToolsCouplingTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.BiologicalControlActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.HarvestingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.IrrigationActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.OtherActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.TillageActionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleSpecies;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycleImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlotTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrapeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.RefVarieteTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.exceptions.AgrosystDuplicationException;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.action.ActionService;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.domain.CattleDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlanEntryDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlans;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.Equipments;
import fr.inra.agrosyst.api.services.domain.ToolsCouplingDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.itk.SpeciesStadeDto;
import fr.inra.agrosyst.api.services.practiced.CropCycleModelDto;
import fr.inra.agrosyst.api.services.practiced.OperationStatus;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleConnectionDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleNodeDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCyclePhaseDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleSpeciesDto;
import fr.inra.agrosyst.api.services.practiced.PracticedDuplicateCropCyclesContext;
import fr.inra.agrosyst.api.services.practiced.PracticedInterventionDto;
import fr.inra.agrosyst.api.services.practiced.PracticedPerennialCropCycleDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSeasonalCropCycleDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemFilter;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.api.services.practiced.PracticedSystems;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.security.AgrosystAccessDeniedException;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.practiced.export.PracticedExportXls;
import fr.inra.agrosyst.services.practiced.export.PracticedSystemExportTask;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.nuiton.util.pagination.PaginationResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Setter
public class PracticedSystemServiceImpl extends AbstractAgrosystService implements PracticedSystemService {

    protected BusinessAuthorizationService authorizationService;
    protected DomainService domainService;
    protected AgrosystI18nService i18nService;
    protected PricesService pricesService;
    protected ActionService actionService;
    protected DomainInputStockUnitService domainInputStockUnitService;
    protected AnonymizeService anonymizeService;
    protected ReferentialService referentialService;
    protected GrowingSystemService growingSystemService;

    protected PracticedExportXls practicedExportXls;

    protected LivestockUnitTopiaDao livestockUnitDao;
    protected PracticedSystemTopiaDao practicedSystemDao;
    protected GrowingSystemTopiaDao growingSystemDao;
    protected PracticedInterventionTopiaDao practicedInterventionDao;
    protected MineralFertilizersSpreadingActionTopiaDao mineralFertilizersSpreadingActionDao;
    protected PracticedCropCycleSpeciesTopiaDao practicedCropCycleSpeciesDao;
    protected PracticedPerennialCropCycleTopiaDao practicedPerennialCropCycleDao;
    protected PracticedSeasonalCropCycleTopiaDao practicedSeasonalCropCycleDao;
    protected PracticedCropCycleConnectionTopiaDao practicedCropCycleConnectionDao;
    protected PracticedCropCycleNodeTopiaDao practicedCropCycleNodeDao;
    protected AbstractActionTopiaDao abstractActionDao;
    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    protected PracticedCropCyclePhaseTopiaDao practicedCropCyclePhaseDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    protected ToolsCouplingTopiaDao toolsCouplingDao;
    protected OtherActionTopiaDao otherActionDao;
    protected HarvestingActionTopiaDao harvestingActionDao;
    protected PesticidesSpreadingActionTopiaDao pesticidesSpreadingActionDao;
    protected IrrigationActionTopiaDao irrigationActionDao;
    protected OrganicFertilizersSpreadingActionTopiaDao organicFertilizersSpreadingActionDao;
    protected BiologicalControlActionTopiaDao biologicalControlActionDao;
    protected TillageActionTopiaDao tillageActionDao;
    protected PracticedSpeciesStadeTopiaDao practicedSpeciesStadeDao;
    protected PracticedPlotTopiaDao practicedPlotDao;
    protected SolHorizonTopiaDao solHorizonDao;
    protected RefClonePlantGrapeTopiaDao refClonePlantGrapeDao;
    protected RefVarieteTopiaDao varieteDao;
    protected RefStadeEDITopiaDao refStadeEDIDao;
    protected RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEDIDao;
    protected RefDestinationTopiaDao refDestinationDao;


    private static final Log LOGGER = LogFactory.getLog(PracticedSystemServiceImpl.class);

    protected static final Binder<PracticedIntervention, PracticedInterventionDto> PRACTICED_INTERVENTION_TO_DTO_BINDER = BinderFactory.newBinder(PracticedIntervention.class, PracticedInterventionDto.class);
    protected static final Binder<PracticedSpeciesStade, PracticedSpeciesStade> PRACTICED_SPECIES_STADE_BINDER = BinderFactory.newBinder(PracticedSpeciesStade.class);

    protected static final Function<CroppingPlanEntryDto, CropCycleModelDto> CROPPING_PLAN_ENTRY_TO_CROP_CYCLE_MODEL_DTO = input -> {
        CropCycleModelDto result = new CropCycleModelDto();
        result.setCroppingPlanEntryCode(input.getCode());
        result.setLabel(input.getName());
        result.setCroppingPlanSellingPrice(input.getSellingPrice());
        result.setIntermediate(input.isIntermediate());
        result.setMixSpecies(input.isMixSpecies());
        result.setMixVariety(input.isMixVariety());
        result.setMixCompanion(input.isMixCompanion());
        result.setType(input.getType());
        result.setCatchCrop(input.isCatchCrop());
        return result;
    };

    protected static final Function<PracticedSpeciesStade, String> GET_PRACTICED_SPECIES_STADE_CODE = PracticedSpeciesStade::getSpeciesCode;

    protected static final Binder<PracticedCropCycleNode, PracticedCropCycleNode> SEASONAL_NODE_BINDER = BinderFactory.newBinder(PracticedCropCycleNode.class);


    protected List<PracticedInterventionDto> practicedInterventionToInterventionDtos(
            final String domainCode,
            final Collection<PracticedIntervention> interventions,
            String croppingPlanEntryCode,
            String intermediateCroppingPlanEntryCode) {

        Language language = getSecurityContext().getLanguage();
        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(language);
        fillSpeciesStadeTraductions(translationMap, interventions.stream().map(PracticedIntervention::getSpeciesStades).flatMap(Collection::stream).toList());

        return interventions.stream()
                .map(intervention -> {
                    PracticedInterventionDto interventionDto = new PracticedInterventionDto();
                    bindInterventionToInterventionDto(intervention, interventionDto);
                    interventionDto.setSpeciesStadesDtos(getInterventionSpeciesStadeDtos(intervention, translationMap));
                    final Collection<AbstractActionDto> abstractActionDtos = actionService.loadPracticedActionsAndUsages(intervention);
                    interventionDto.setActionDtos(abstractActionDtos);
                    interventionDto.setDomainId(domainCode);
                    String interCropCode = intervention.isIntermediateCrop() ? intermediateCroppingPlanEntryCode : croppingPlanEntryCode;
                    interventionDto.setFromCropCode(interCropCode);
                    return interventionDto;
                })
                .sorted(Comparator.comparing(PracticedInterventionDto::getRank))
                .collect(Collectors.toList());
    }

    @Override
    public PracticedSystem getPracticedSystem(String practicedSystemId) {
        PracticedSystem result;
        if (StringUtils.isEmpty(practicedSystemId)) {
            result = practicedSystemDao.newInstance();
        } else {
            PracticedSystem practicedSystem = practicedSystemDao.forTopiaIdEquals(practicedSystemId).findUnique();
            result = anonymizeService.checkForPracticedSystemAnonymization(practicedSystem);
        }
        return result;
    }

    @Override
    public PaginationResult<PracticedSystem> getFilteredPracticedSystems(PracticedSystemFilter filter) {
        return practicedSystemDao.getFilteredPracticedSystems(filter, getSecurityContext());
    }

    @Override
    public PaginationResult<PracticedSystemDto> getFilteredPracticedSystemsDto(PracticedSystemFilter filter) {
        PaginationResult<PracticedSystem> practicedSystems = practicedSystemDao.getFilteredPracticedSystems(filter, getSecurityContext());
        PaginationResult<PracticedSystemDto> result = practicedSystems.transform(anonymizeService.getPracticedSystemToDtoFunction(true));
        for (PracticedSystemDto practicedSystem : result.getElements()) {
            boolean canValidate = authorizationService.isPracticedSystemValidable(practicedSystem.getTopiaId());
            practicedSystem.setUserCanValidate(canValidate);
        }
        return result;
    }

    @Override
    public Set<String> getFilteredPracticedSystemIds(PracticedSystemFilter filter) {
        return practicedSystemDao.getFilteredPracticedSystemIds(filter, getSecurityContext());
    }

    protected List<CroppingPlanEntryDto> getCropCycleCroppingPlans(String growingSystemId) {
        String domainId = getDomainIdForGrowingSystem(growingSystemId);
        return domainService.getCroppingPlanDtos(domainId);
    }

    @Override
    public List<String> getToolsCouplingsFromGrowingSystemAndCampaigns(String growingSystemId, String campaigns) {
        String domainCode = getDomainCode(growingSystemId);
        Set<Integer> intCampaigns = getIntCampaigns(campaigns);

        return domainService.getToolsCouplingCodeForDomainsAndCampaigns(domainCode, intCampaigns);
    }

    @Override
    public List<String> getToolsCouplingsCodesFromDomainAndCampaigns(String domainCode, Integer targetedCampaign) {
        Set<Integer> campaigns = Collections.singleton(targetedCampaign);
        return domainService.getToolsCouplingCodeForDomainsAndCampaigns(domainCode, campaigns);
    }

    private List<String> getDomainIdsForGrowingSystemAndCampaigns(String growingSystemId, String campaigns, boolean includeCropsFromInactiveDomains) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(growingSystemId));
        List<String> result;
        Set<Integer> intCampaigns = getIntCampaigns(campaigns);
        if (!intCampaigns.isEmpty()) {
            result = domainService.getDomainIdsForGrowingSystemAndCampaigns(growingSystemId, intCampaigns, includeCropsFromInactiveDomains);
        } else {
            result = new ArrayList<>();
        }
        return result;
    }

    private String getDomainIdForGrowingSystem(String growingSystemId) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(growingSystemId));
        return domainService.getDomainIdForGrowingSystem(growingSystemId);
    }

    private List<String> getDomainIdsForDomainAndCampaigns(String domainId, String campaigns) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(domainId));
        List<String> result;
        Set<Integer> intCampaigns = getIntCampaigns(campaigns);
        if (!intCampaigns.isEmpty()) {
            result = domainService.getDomainIdsForDomainAndCampaigns(domainId, intCampaigns, false);
        } else {
            result = new ArrayList<>();
        }
        return result;
    }

    @Override
    public List<String> getCropCodesFromGrowingSystemIdForCampaigns(String growingSystemId, String campaigns) {
        Set<Integer> intCampaigns = getIntCampaigns(campaigns);
        String domainCode = domainService.getDomainCodeForGrowingSystem(growingSystemId);
        return domainService.getCroppingPlanCodeForDomainsAndCampaigns(domainCode, intCampaigns);
    }

    @Override
    public List<String> getCropCodesFromDomainIdForCampaigns(String domainId, String campaigns) {
        Set<Integer> intCampaigns = getIntCampaigns(campaigns);
        String domainCode = domainService.getDomain(domainId).getCode();
        return domainService.getCroppingPlanCodeForDomainsAndCampaigns(domainCode, intCampaigns);
    }

    @Override
    public Map<CropCycleModelDto, List<CroppingPlanSpeciesDto>> getCropCycleModelMap(String growingSystemId, boolean includeIntermediate) {
        return getCropCycleModelMap0(growingSystemId, includeIntermediate);
    }

    @Override
    public Map<CropCycleModelDto, List<CroppingPlanSpeciesDto>> getIpmworksCropCycleModelMap(String domainId) {
        List<CroppingPlanEntryDto> croppingPlans = domainService.getCroppingPlanDtos(domainId);
        Multimap<String, CroppingPlanEntryDto> cropDtosByCode = Multimaps.index(croppingPlans, CroppingPlanEntryDto::getCode);

        Map<CropCycleModelDto, List<CroppingPlanSpeciesDto>> result = buildCropCycleModelMap(cropDtosByCode);
        return result;
    }

    @Override
    public Map<String, List<Pair<String, String>>> getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(String growingSystemId, String campaigns) {
        List<String> domainIds = getDomainIdsForGrowingSystemAndCampaigns(growingSystemId, campaigns, false);
        return referentialService.getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(new HashSet<>(domainIds));
    }

    @Override
    public Map<String, List<Sector>> getSectorByCodeEspceBotaniqueCodeQualifiant(String growingSystemId, String campaigns) {
        List<String> domainIds = getDomainIdsForGrowingSystemAndCampaigns(growingSystemId, campaigns, false);

        Map<String, List<Sector>> sectorsByCodeEspeceBotanique0;
        if (CollectionUtils.isNotEmpty(domainIds)) {
            List<Pair<String, String>> r0 = referentialService.getAllCodeEspeceBotaniqueCodeQualifantForDomainIds(new HashSet<>(domainIds));
            sectorsByCodeEspeceBotanique0 = referentialService.getSectorsByCodeEspeceBotanique_CodeQualifiantAEE(r0);
        } else {
            sectorsByCodeEspeceBotanique0 = new HashMap<>();
        }

        return sectorsByCodeEspeceBotanique0;
    }

    @Override
    public Map<String, List<Pair<String, String>>> getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds2(String domainId, String campaigns) {
        List<String> domainIds = getDomainIdsForDomainAndCampaigns(domainId, campaigns);
        return referentialService.getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(new HashSet<>(domainIds));
    }

    @Override
    public Map<String, List<Sector>> getSectorByCodeEspceBotaniqueCodeQualifiant2(String domainId, String campaigns) {
        List<String> domainIds = getDomainIdsForDomainAndCampaigns(domainId, campaigns);

        Map<String, List<Sector>> sectorsByCodeEspeceBotanique0;
        if (CollectionUtils.isNotEmpty(domainIds)) {
            List<Pair<String, String>> r0 = referentialService.getAllCodeEspeceBotaniqueCodeQualifantForDomainIds(new HashSet<>(domainIds));
            sectorsByCodeEspeceBotanique0 = referentialService.getSectorsByCodeEspeceBotanique_CodeQualifiantAEE(r0);
        } else {
            sectorsByCodeEspeceBotanique0 = new HashMap<>();
        }

        return sectorsByCodeEspeceBotanique0;
    }

    /*
     * Set as max values as possible that can be find from all crops
     */
    protected CroppingPlanEntryDto getGlobalCampaignCroppingPlanEntryDto(Collection<CroppingPlanEntryDto> from) {
        CroppingPlanEntryDto to = null;
        if (CollectionUtils.isNotEmpty(from)) {
            to = new CroppingPlanEntryDto();
            Binder<CroppingPlanEntryDto, CroppingPlanEntryDto> binder = BinderFactory.newBinder(CroppingPlanEntryDto.class, CroppingPlanEntryDto.class);

            List<CroppingPlanEntryDto> fromCopy = new ArrayList<>(from);
            CroppingPlanEntryDto from_ = fromCopy.removeFirst();
            binder.copy(from_, to); // copy all basics

            // if there are other crops keep the boolean true value
            for (CroppingPlanEntryDto from0 : fromCopy) {
                to.setMixSpecies(to.isMixSpecies() || from0.isMixSpecies());
                to.setMixVariety(to.isMixVariety() || from0.isMixVariety());
                to.setMixCompanion(to.isMixCompanion() || from0.isMixCompanion());
                to.setValidated(to.isValidated() || from0.isValidated());
                to.setPasturedMeadow(to.isPasturedMeadow() || from0.isPasturedMeadow());
                to.setMowedMeadow(to.isMowedMeadow() || from0.isMowedMeadow());
                Boolean toTemporaryMeadow = to.getTemporaryMeadow();
                Boolean fromTemporaryMeadow = from0.getTemporaryMeadow();
                toTemporaryMeadow = getTrueFalseOrNull(toTemporaryMeadow, fromTemporaryMeadow);
                to.setTemporaryMeadow(toTemporaryMeadow);
                to.setType(from0.getType());
            }
        }
        return to;
    }

    /*
     * Set as max values as possible that can be find from all species
     */
    protected void getGlobalCampaignCroppingPlanSpeciesDto(CroppingPlanSpeciesDto from, CroppingPlanSpeciesDto to) {
        to.setValidated(to.isValidated() || from.isValidated());
        to.setSpeciesArea(from.getSpeciesArea() == null ? to.getSpeciesArea() : from.getSpeciesArea());
        to.setSpeciesId(StringUtils.isBlank(from.getSpeciesId()) ? to.getSpeciesId() : from.getSpeciesId());
        to.setSpeciesEspece(StringUtils.isBlank(from.getSpeciesEspece()) ? to.getSpeciesEspece() : from.getSpeciesEspece());
        to.setSpeciesQualifiant(StringUtils.isBlank(from.getSpeciesQualifiant()) ? to.getSpeciesQualifiant() : from.getSpeciesQualifiant());
        to.setSpeciesTypeSaisonnier(StringUtils.isBlank(from.getSpeciesTypeSaisonnier()) ? to.getSpeciesTypeSaisonnier() : from.getSpeciesTypeSaisonnier());
        to.setSpeciesDestination(StringUtils.isBlank(from.getSpeciesDestination()) ? to.getSpeciesDestination() : from.getSpeciesDestination());
        to.setCode_destination_aee(StringUtils.isBlank(from.getCode_destination_aee()) ? to.getCode_destination_aee() : from.getCode_destination_aee());
        to.setVarietyId(StringUtils.isBlank(from.getVarietyId()) ? to.getVarietyId() : from.getVarietyId());
        to.setVarietyLibelle(StringUtils.isBlank(from.getVarietyLibelle()) ? to.getVarietyLibelle() : from.getVarietyLibelle());
        to.setEdaplosUnknownVariety(StringUtils.isBlank(from.getEdaplosUnknownVariety()) ? to.getEdaplosUnknownVariety() : from.getEdaplosUnknownVariety());
        to.setProfil_vegetatif_BBCH(StringUtils.isBlank(from.getProfil_vegetatif_BBCH()) ? to.getProfil_vegetatif_BBCH() : from.getProfil_vegetatif_BBCH());
        to.setCode_espece_botanique(StringUtils.isBlank(from.getCode_espece_botanique()) ? to.getCode_espece_botanique() : from.getCode_espece_botanique());
        to.setCode_qualifiant_AEE(StringUtils.isBlank(from.getCode_qualifiant_AEE()) ? to.getCode_qualifiant_AEE() : from.getCode_qualifiant_AEE());
        to.setCompagne(from.getCompagne() == null ? to.getCompagne() : from.getCompagne());
    }

    private Boolean getTrueFalseOrNull(Boolean toAffected, Boolean fromAffected) {
        if (fromAffected != null) {
            if (fromAffected) {
                toAffected = true;
            } else if (toAffected == null) {
                toAffected = false;
            }
        }
        return toAffected;
    }

    protected Map<CropCycleModelDto, List<CroppingPlanSpeciesDto>> getCropCycleModelMap0(String growingSystemId,
                                                                                         boolean includeIntermediate) {
        List<CroppingPlanEntryDto> croppingPlans = getCropCycleCroppingPlans(growingSystemId);
        if (!includeIntermediate) {
            Iterables.removeIf(croppingPlans, CroppingPlans.IS_ENTRY_INTERMEDIATE::test);
        }
        Multimap<String, CroppingPlanEntryDto> cropDtosByCode = Multimaps.index(croppingPlans, CroppingPlanEntryDto::getCode);

        Map<CropCycleModelDto, List<CroppingPlanSpeciesDto>> result = buildCropCycleModelMap(cropDtosByCode);
        return result;
    }

    private Map<CropCycleModelDto, List<CroppingPlanSpeciesDto>> buildCropCycleModelMap(Multimap<String, CroppingPlanEntryDto> cropDtosByCode) {
        Map<CropCycleModelDto, List<CroppingPlanSpeciesDto>> result = new LinkedHashMap<>();
        for (Map.Entry<String, Collection<CroppingPlanEntryDto>> entry : cropDtosByCode.asMap().entrySet()) {
            Collection<CroppingPlanEntryDto> dtos = entry.getValue();

            CroppingPlanEntryDto globalCropDto = getGlobalCampaignCroppingPlanEntryDto(dtos);

            if (globalCropDto == null) continue;

            CropCycleModelDto modelDto = CROPPING_PLAN_ENTRY_TO_CROP_CYCLE_MODEL_DTO.apply(globalCropDto);
            Map<String, CroppingPlanSpeciesDto> species = new LinkedHashMap<>();

            for (CroppingPlanEntryDto dto : dtos) {
                // Get all species
                for (CroppingPlanSpeciesDto croppingPlanSpeciesDto : dto.getSpecies()) {
                    CroppingPlanSpeciesDto globalSpeciesDto = species.get(croppingPlanSpeciesDto.getCode());
                    if (globalSpeciesDto != null) {
                        getGlobalCampaignCroppingPlanSpeciesDto(croppingPlanSpeciesDto, globalSpeciesDto);
                    } else {
                        species.put(croppingPlanSpeciesDto.getCode(), croppingPlanSpeciesDto);
                    }
                }
            }
            result.put(modelDto, new ArrayList<>(species.values()));
        }
        return result;
    }

    protected List<ToolsCoupling> getConcernedToolsCouplings(String growingSystemId, String campaigns) {

        Preconditions.checkArgument(StringUtils.isNotEmpty(growingSystemId) && StringUtils.isNotEmpty(campaigns));

        List<ToolsCoupling> toolsCouplings = null;
        if (!campaigns.isEmpty()) {
            String domainCode = getDomainCode(growingSystemId);
            Set<Integer> intCampaigns = getIntCampaigns(campaigns);
            toolsCouplings = domainService.getToolsCouplingsForDomainCodeAndCampaigns(domainCode, intCampaigns);

        }

        return toolsCouplings;
    }

    private Set<Integer> getIntCampaigns(String campaigns) {
        Set<Integer> campaignsInt;
        try {
            campaignsInt = CommonService.GET_CAMPAIGNS_SET.apply(campaigns);
        } catch (Exception eee) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Unable to parse campaigns: " + campaigns, eee);
            }
            campaignsInt = new HashSet<>();
        }
        return campaignsInt;
    }

    @Override
    public PracticedSystem createOrUpdatePracticedSystem(PracticedSystem practicedSystem,
                                                         List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos,
                                                         List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos) {
        authorizationService.checkCreateOrUpdatePracticedSystem(practicedSystem.getTopiaId());

        practicedSystem.setCampaigns(CommonService.ARRANGE_CAMPAIGNS.apply(practicedSystem.getCampaigns()));
        Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(practicedSystem.getGrowingSystem().getTopiaId(), practicedSystem.getCampaigns());
        Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant = getSectorByCodeEspceBotaniqueCodeQualifiant(practicedSystem.getGrowingSystem().getTopiaId(), practicedSystem.getCampaigns());
        final Map<String, AbstractDomainInputStockUnit> domainInputStockByIds = domainInputStockUnitService.loadDomainInputStockByIds(practicedSystem.getGrowingSystem().getGrowingPlan().getDomain());

        Domain practicedSystemDirectDomain = practicedSystem.getGrowingSystem().getGrowingPlan().getDomain();
        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryDao.forDomainEquals(practicedSystemDirectDomain).findAll();
        Map<String, CroppingPlanEntry> croppingPlanEntryMap = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getCode, Function.identity()));
        Map<String, CroppingPlanSpecies> croppingPlanSpeciesMap = croppingPlanEntries.stream()
                .map(CroppingPlanEntry::getCroppingPlanSpecies)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .collect(Collectors.toMap(CroppingPlanSpecies::getCode, Function.identity()));

        CreateOrUpdatePracticedSystemContext createOrUpdatePracticedSystemContext = new CreateOrUpdatePracticedSystemContext(
                practicedSystem,
                practicedPerennialCropCycleDtos,
                practicedSeasonalCropCycleDtos,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspeceBotaniqueCodeQualifiant,
                domainInputStockByIds,
                croppingPlanEntryMap,
                croppingPlanSpeciesMap);

        createOrUpdatePracticedSystemWithoutCommit(createOrUpdatePracticedSystemContext);

        getTransaction().commit();

        return practicedSystem;
    }

    @Override
    public boolean isConnectionMissingInCropCycles(Collection<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos) {
        return practicedSeasonalCropCycleDtos.stream().anyMatch(practicedSeasonalCropCycleDto -> {
            List<PracticedCropCycleNodeDto> nodeDtos = practicedSeasonalCropCycleDto.getCropCycleNodeDtos();
            Set<String> connectionTargetIds = practicedSeasonalCropCycleDto.getCropCycleConnectionDtos().stream()
                    .map(PracticedCropCycleConnectionDto::getTargetId)
                    .collect(Collectors.toSet());
            return nodeDtos.stream().anyMatch(node -> node.getX() == 0 && !connectionTargetIds.contains(node.getNodeId()))
                    && nodeDtos.stream().anyMatch(PracticedCropCycleNodeDto::isEndCycle);
        });
    }

    protected void createOrUpdatePracticedSystemWithoutCommit(
            CreateOrUpdatePracticedSystemContext createOrUpdatePracticedSystemContext) {

        // PracticedSystem part
        PracticedSystem result;

        PracticedSystem practicedSystem = createOrUpdatePracticedSystemContext.getPracticedSystem();

        if (practicedSystem.isPersisted()) {
            Preconditions.checkArgument(isActivated(practicedSystem), "The practiced system ancestor or unactivated");
        }

        practicedSystem.setCampaigns(CommonService.ARRANGE_CAMPAIGNS.apply(practicedSystem.getCampaigns()));
        practicedSystem.setUpdateDate(context.getCurrentTime());

        setValidCampaignGrowingSystem(practicedSystem);

        if (StringUtils.isBlank(practicedSystem.getTopiaId())) {
            // On create, Practiced System should be active (PracticedSystem#active = true)
            practicedSystem.setActive(true);
            result = practicedSystemDao.create(practicedSystem);
        } else {
            result = practicedSystemDao.update(practicedSystem);
        }

        createOrUpdatePracticedSystemContext.setPracticedSystem(result);
        loadCyclesToContext(createOrUpdatePracticedSystemContext, result);


        List<String> practicedSystemAvailableCropCodes = createOrUpdatePracticedSystemContext.getCroppingPlanEntryMap().keySet().stream().toList();//getCropCodesFromGrowingSystemIdForCampaigns(createOrUpdatePracticedSystemContext.getGrowingSystem().getTopiaId(), practicedSystem.getCampaigns());

        createOrUpdatePracticedSystemContext.setPracticedSystemAvailableCropCodes(practicedSystemAvailableCropCodes);

        createOrUpdatePracticedPerennialCropCycle(createOrUpdatePracticedSystemContext);

        createOrUpdatePracticedSeasonalCropCycle0(createOrUpdatePracticedSystemContext);

    }

    private void setValidCampaignGrowingSystem(PracticedSystem practicedSystem) {
        int directCampaign = practicedSystem.getGrowingSystem().getGrowingPlan().getDomain().getCampaign();
        if (!practicedSystem.getCampaigns().contains(Integer.toString(directCampaign))) {
            Set<Integer> practicedSystemCampaigns = CommonService.GET_CAMPAIGNS_SET.apply(practicedSystem.getCampaigns());
            GrowingSystem currentGs = practicedSystem.getGrowingSystem();

            GrowingSystem sameCodeGrowingSystem = growingSystemService.getLastGrowingSystemForCampaigns(currentGs.getCode(), practicedSystemCampaigns);
            if (sameCodeGrowingSystem != null) {
                practicedSystem.setGrowingSystem(sameCodeGrowingSystem);
            } else {
                throw new AgrosystTechnicalException(String.format("No growing system found for code '%s' and campaigns in '%s'", currentGs.getCode(), practicedSystem.getCampaigns()));
            }
        }
    }

    private void loadCyclesToContext(CreateOrUpdatePracticedSystemContext createOrUpdatePracticedSystemContext, PracticedSystem result) {
        List<PracticedPerennialCropCycle> practicedPerennialCropCycles = practicedPerennialCropCycleDao.forPracticedSystemEquals(result).findAll();
        List<PracticedSeasonalCropCycle> practicedSeasonalCropCycles = practicedSeasonalCropCycleDao.forPracticedSystemEquals(result).findAll();
        createOrUpdatePracticedSystemContext.setPracticedPerennialCropCycles(practicedPerennialCropCycles);
        createOrUpdatePracticedSystemContext.setPracticedSeasonalCropCycles(practicedSeasonalCropCycles);
    }

    protected void createOrUpdatePracticedSeasonalCropCycle0(CreateOrUpdatePracticedSystemContext context) {

        PracticedSystem practicedSystem = context.getPracticedSystem();
        List<PracticedSeasonalCropCycle> practicedSeasonalCropCycles = context.getPracticedSeasonalCropCycles();
        Map<String, PracticedSeasonalCropCycle> practicedSeasonalCropCyclesById = new HashMap<>(Maps.uniqueIndex(practicedSeasonalCropCycles, TopiaEntities.getTopiaIdFunction()));

        List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos = context.getPracticedSeasonalCropCycleDtos();
        if (practicedSeasonalCropCycleDtos != null) {

            List<String> practicedSystemCroppingPlanEntryCodes = new ArrayList<>(context.getPracticedSystemAvailableCropCodes());

            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = context.getSpeciesCodeToCodeEspeceBotaniqueCodeQualifiantAee();
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant = context.getSectorByCodeEspceBotaniqueCodeQualifiant();
            for (PracticedSeasonalCropCycleDto practicedSeasonalCropCycleDto : practicedSeasonalCropCycleDtos) {

                // find cycle to create or update by topiaId
                String topiaId = practicedSeasonalCropCycleDto.getTopiaId();
                PracticedSeasonalCropCycle cycle;
                if (StringUtils.isBlank(topiaId)) {
                    cycle = practicedSeasonalCropCycleDao.newInstance();
                } else {
                    cycle = practicedSeasonalCropCyclesById.remove(topiaId);
                }

                // update cycle with dto data
                List<PracticedCropCycleNodeDto> nodeDtos0 = practicedSeasonalCropCycleDto.getCropCycleNodeDtos();
                List<PracticedCropCycleConnectionDto> connectionDtos0 = practicedSeasonalCropCycleDto.getCropCycleConnectionDtos();

                // filter on domain crop codes
                List<PracticedCropCycleNodeDto> nodeDtos = new ArrayList<>(nodeDtos0
                        .stream()
                        .filter(n -> practicedSystemCroppingPlanEntryCodes.contains(n.getCroppingPlanEntryCode()))
                        .toList());

                List<PracticedCropCycleConnectionDto> connectionDtos = new ArrayList<>(connectionDtos0
                        .stream()
                        .filter(c -> c.getIntermediateCroppingPlanEntryCode() == null ||
                                practicedSystemCroppingPlanEntryCodes.contains(c.getIntermediateCroppingPlanEntryCode()))
                        .toList());

                practicedSeasonalCropCycleDto.setCropCycleNodeDtos(nodeDtos);
                practicedSeasonalCropCycleDto.setCropCycleConnectionDtos(connectionDtos);

                Set<String> connectionTargetIds = connectionDtos.stream()
                        .map(PracticedCropCycleConnectionDto::getTargetId)
                        .collect(Collectors.toSet());
                Set<PracticedCropCycleNodeDto> unreachableNodes = nodeDtos.stream()
                        .filter(node -> node.getX() == 0 && !connectionTargetIds.contains(node.getNodeId()))
                        .collect(Collectors.toSet());

                if (!unreachableNodes.isEmpty()) {
                    Set<String> endCycleNodes = nodeDtos.stream()
                            .filter(PracticedCropCycleNodeDto::isEndCycle)
                            .map(PracticedCropCycleNodeDto::getNodeId)
                            .collect(Collectors.toSet());

                    if (!endCycleNodes.isEmpty()) {
                        MultiValuedMap<String, PracticedCropCycleConnectionDto> endCycleConnectionsBySourceId = new HashSetValuedHashMap<>();
                        for (PracticedCropCycleConnectionDto connection : connectionDtos) {
                            if (endCycleNodes.contains(connection.getSourceId())) {
                                endCycleConnectionsBySourceId.put(connection.getSourceId(), connection);
                            }
                        }
                        if (endCycleConnectionsBySourceId.isEmpty()) {
                            double newFrequency = 100d / unreachableNodes.size();
                            for (PracticedCropCycleNodeDto node : nodeDtos) {
                                if (endCycleNodes.contains(node.getNodeId())) {
                                    createNewConnectionsToUnreachableNodes(connectionDtos, unreachableNodes, node.getNodeId(), newFrequency);
                                }
                            }
                        } else {
                            endCycleConnectionsBySourceId.keySet().forEach(sourceId -> {
                                Collection<PracticedCropCycleConnectionDto> connections = endCycleConnectionsBySourceId.get(sourceId);

                                int totalConnectionNb = connections.size() + unreachableNodes.size();
                                double newFrequency = 100d / totalConnectionNb;

                                createNewConnectionsToUnreachableNodes(connectionDtos, unreachableNodes, sourceId, newFrequency);

                                for (PracticedCropCycleConnectionDto connection : connections) {
                                    Double frequency = connection.getCroppingPlanEntryFrequency();
                                    if (frequency == null) {
                                        frequency = newFrequency;
                                    } else {
                                        frequency = frequency * connections.size() / totalConnectionNb;
                                    }
                                    connection.setCroppingPlanEntryFrequency(frequency);
                                }

                            });
                        }
                    }
                }

                CreateOrUpdatePracticedSeasonalCropCycleContext practicedSeasonalCropCycleContext =
                        new CreateOrUpdatePracticedSeasonalCropCycleContext(
                                cycle,
                                practicedSystem,
                                nodeDtos,
                                connectionDtos,
                                practicedSeasonalCropCycleDto,
                                practicedSystemCroppingPlanEntryCodes,
                                context.getDomain(),
                                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                                sectorByCodeEspceBotaniqueCodeQualifiant,
                                context.getDomainInputStockByIds(),
                                context.getCroppingPlanEntryMap(),
                                context.getCroppingPlanSpeciesMap()
                        );

                createOrUpdatePracticedSeasonalCropCycle(practicedSeasonalCropCycleContext);

            }
        }

        // delete remaining cycles
        for (PracticedSeasonalCropCycle practicedSeasonalCropCycle : practicedSeasonalCropCyclesById.values()) {
            List<PracticedCropCycleConnection> practicedCropCycleConnections = practicedCropCycleConnectionDao.findAllByCropCycle(practicedSeasonalCropCycle.getTopiaId());
            removePracticedCropCycleConnections(practicedCropCycleConnections);
            practicedSeasonalCropCycleDao.delete(practicedSeasonalCropCycle);
        }

    }

    private void createNewConnectionsToUnreachableNodes(List<PracticedCropCycleConnectionDto> connectionDtos,
                                                        Set<PracticedCropCycleNodeDto> unreachableNodes,
                                                        String sourceId,
                                                        double newFrequency) {

        for (PracticedCropCycleNodeDto unreachableNode : unreachableNodes) {
            PracticedCropCycleConnectionDto newConnection = new PracticedCropCycleConnectionDto();
            newConnection.setSourceId(sourceId);
            newConnection.setTargetId(unreachableNode.getNodeId());
            newConnection.setCroppingPlanEntryFrequency(newFrequency);
            connectionDtos.add(newConnection);
        }
    }

    protected void createOrUpdatePracticedPerennialCropCycle(CreateOrUpdatePracticedSystemContext context) {

        List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos = context.getPracticedPerennialCropCycleDtos();
        PracticedSystem practicedSystem = context.getPracticedSystem();
        List<String> practicedSystemCroppingPlanEntries = new ArrayList<>(context.getPracticedSystemAvailableCropCodes());
        List<PracticedPerennialCropCycle> practicedPerennialCropCycles = context.getPracticedPerennialCropCycles();

        if (practicedPerennialCropCycleDtos != null) {

            if (practicedPerennialCropCycles == null) {
                practicedPerennialCropCycles = new ArrayList<>();
            }
            Map<String, PracticedPerennialCropCycle> currentPracticedPerennialCropCycles = practicedPerennialCropCycles.stream().collect(Collectors.toMap(Entities.GET_TOPIA_ID, Function.identity()));

            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = context.getSpeciesCodeToCodeEspeceBotaniqueCodeQualifiantAee();
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant = context.getSectorByCodeEspceBotaniqueCodeQualifiant();
            for (PracticedPerennialCropCycleDto practicedPerennialCropCycleDto : practicedPerennialCropCycleDtos) {

                PracticedPerennialCropCycle newPracticedPerennialCropCycle = practicedPerennialCropCycleDto.getPracticedPerennialCropCycle();

                // valid that crop exists on domain
                Preconditions.checkArgument(
                        newPracticedPerennialCropCycle != null &&
                                StringUtils.isNotBlank(newPracticedPerennialCropCycle.getCroppingPlanEntryCode()) &&
                                practicedSystemCroppingPlanEntries.contains(newPracticedPerennialCropCycle.getCroppingPlanEntryCode()), String.format("The croppingPlanEntry with the code %s was not found on domain %s for campaigns %s", newPracticedPerennialCropCycle == null ? "unknown" : newPracticedPerennialCropCycle.getCroppingPlanEntryCode(), context.getDomain().getName(), practicedSystem.getCampaigns()));

                String cycleTopiaId = newPracticedPerennialCropCycle.getTopiaId();

                PracticedPerennialCropCycle cycle = currentPracticedPerennialCropCycles.remove(cycleTopiaId);
                if (cycle == null) {
                    cycle = practicedPerennialCropCycleDao.newInstance();
                }

                Binder<PracticedPerennialCropCycle, PracticedPerennialCropCycle> binder = BinderFactory.newBinder(PracticedPerennialCropCycle.class);
                binder.copyExcluding(newPracticedPerennialCropCycle, cycle,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        PracticedPerennialCropCycle.PROPERTY_CROP_CYCLE_PHASES,
                        PracticedPerennialCropCycle.PROPERTY_PRACTICED_CROP_CYCLE_SPECIES);

                List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos = practicedPerennialCropCycleDto.getCropCyclePhaseDtos();
                List<PracticedCropCycleSpeciesDto> cropCyclePerennialSpeciesDto = practicedPerennialCropCycleDto.getSpeciesDto();

                CreateOrUpdatePracticedPerennialCropCycleContext createOrUpdatePracticedPerennialCropCycleContext =
                        new CreateOrUpdatePracticedPerennialCropCycleContext(
                                context.getDomain().getCode(),
                                context.getPracticedSystem(),
                                cycle,
                                cropCyclePhaseDtos,
                                cropCyclePerennialSpeciesDto,
                                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                                sectorByCodeEspceBotaniqueCodeQualifiant,
                                context.domainInputStockByIds,
                                context.getCroppingPlanEntryMap(),
                                context.getCroppingPlanSpeciesMap()
                        );

                createOrUpdatePracticedPerennialCropCycle(createOrUpdatePracticedPerennialCropCycleContext);

            }
            // cycle to remove
            for (PracticedPerennialCropCycle practicedPerennialCropCycle : currentPracticedPerennialCropCycles.values()) {
                removePracticedCropCyclePhasesChildrenObjects(practicedPerennialCropCycle.getCropCyclePhases());
            }
            practicedPerennialCropCycleDao.deleteAll(currentPracticedPerennialCropCycles.values());
        } else {
            removePracticedPerennialCropCycle(practicedPerennialCropCycles);
        }
    }

    @Override
    public Map<AgrosystInterventionType, List<ToolsCouplingDto>> getToolsCouplingModelForInterventionTypes(String growingSystemId, String campaigns, Set<AgrosystInterventionType> interventionTypes) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(interventionTypes));

        Map<AgrosystInterventionType, List<ToolsCouplingDto>> toolsCouplingForInterventionTypes = new HashMap<>();

        List<ToolsCoupling> toolsCouplings = getConcernedToolsCouplings(growingSystemId, campaigns);

        for (AgrosystInterventionType interventionType : interventionTypes) {
            List<ToolsCouplingDto> models = getToolsCouplingsModelForInterventionType(interventionType, toolsCouplings);
            toolsCouplingForInterventionTypes.put(interventionType, models);
        }
        return toolsCouplingForInterventionTypes;
    }

    private List<ToolsCouplingDto> getToolsCouplingsModelForInterventionType(AgrosystInterventionType interventionType, List<ToolsCoupling> toolsCouplings) {
        Map<String, ToolsCouplingDto> allCampaignsToolsCouplings = new HashMap<>();
        if (toolsCouplings != null) {
            for (ToolsCoupling toolsCoupling : toolsCouplings) {
                boolean found = false;
                for (RefInterventionAgrosystTravailEDI mainAction : toolsCoupling.getMainsActions()) {
                    if (mainAction.getIntervention_agrosyst() == interventionType) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    String code = toolsCoupling.getCode();
                    if (!allCampaignsToolsCouplings.containsKey(code)) {
                        ToolsCouplingDto dto = Equipments.TOOLS_COUPLING_TO_TOOLS_COUPLING_DTO.apply(toolsCoupling);
                        allCampaignsToolsCouplings.put(code, dto);
                    }
                }
            }
        }

        return new ArrayList<>(allCampaignsToolsCouplings.values());
    }

    @Override
    public List<Equipment> getEquipmentsForInterventionPracticedSystem(PracticedSystem practicedSystem, Domain domain) {
        Preconditions.checkNotNull(practicedSystem);
        Preconditions.checkNotNull(domain);

        Set<Integer> intCampaigns = getIntCampaigns(practicedSystem.getCampaigns());
        return domainService.getEquipmentsForDomainCodeAndCampaigns(domain.getCode(), intCampaigns);
    }

    @Override
    public List<PracticedPerennialCropCycleDto> getAllPracticedPerennialCropCycles(String practicedSystemId) {
        PracticedSystem practicedSystem = practicedSystemDao.forTopiaIdEquals(practicedSystemId).findUnique();
        List<PracticedPerennialCropCycle> perennialCropCycles = practicedPerennialCropCycleDao.forPracticedSystemEquals(practicedSystem).findAll();
        return convertPerennialCropCyclesToDto(perennialCropCycles, practicedSystem);
    }

    protected List<PracticedPerennialCropCycleDto> convertPerennialCropCyclesToDto(
            List<PracticedPerennialCropCycle> practicedPerennialCropCycles, PracticedSystem practicedSystem) {

        List<PracticedPerennialCropCycleDto> result = new ArrayList<>();

        for (PracticedPerennialCropCycle perennialCropCycle : practicedPerennialCropCycles) {
            PracticedPerennialCropCycleDto dto = new PracticedPerennialCropCycleDto();

            PracticedPerennialCropCycle lightPracticedPerennialCropCycle = new PracticedPerennialCropCycleImpl();
            Binder<PracticedPerennialCropCycle, PracticedPerennialCropCycle> binder = BinderFactory.newBinder(PracticedPerennialCropCycle.class);
            binder.copyExcluding(perennialCropCycle, lightPracticedPerennialCropCycle,
                    PracticedPerennialCropCycle.PROPERTY_CROP_CYCLE_PHASES,
                    PracticedPerennialCropCycle.PROPERTY_PRACTICED_CROP_CYCLE_SPECIES
            );

            dto.setPracticedPerennialCropCycle(lightPracticedPerennialCropCycle);
            String cpEntryCode = perennialCropCycle.getCroppingPlanEntryCode();
            dto.setCroppingPlanEntryName(getCroppingPlanEntryName(cpEntryCode));

            List<PracticedCropCyclePhaseDto> phases = getPhasesDTOs(perennialCropCycle);
            dto.setCropCyclePhaseDtos(phases);

            List<PracticedCropCycleSpeciesDto> species = getCropCyclePerennialSpecies(cpEntryCode, perennialCropCycle, practicedSystem.getCampaigns());
            dto.setSpeciesDto(species);

            result.add(dto);
        }
        return result;
    }

    @Override
    public List<PracticedSeasonalCropCycleDto> getAllPracticedSeasonalCropCycles(String practicedSystemId) {
        PracticedSystem practicedSystem = practicedSystemDao.forTopiaIdEquals(practicedSystemId).findUnique();
        List<PracticedSeasonalCropCycle> seasonalCropCycles = practicedSeasonalCropCycleDao.forPracticedSystemEquals(practicedSystem).findAll();
        return convertSeasonalCropCyclesToDto(seasonalCropCycles);
    }

    /**
     * Convert database seasonal entity to dto.
     */
    protected List<PracticedSeasonalCropCycleDto> convertSeasonalCropCyclesToDto(List<PracticedSeasonalCropCycle> practicedSeasonalCropCycles) {

        List<PracticedSeasonalCropCycleDto> result = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(practicedSeasonalCropCycles)) {
            Set<String> practicedSeasonalCropCyclesIds = practicedSeasonalCropCycles.stream()
                    .map(PracticedSeasonalCropCycle::getTopiaId)
                    .collect(Collectors.toSet());

            List<PracticedCropCycleConnection> connections = practicedCropCycleConnectionDao.findAllByCropCycles(practicedSeasonalCropCyclesIds);
            Multimap<String, PracticedCropCycleConnection> connectionsByCycleId = Multimaps.index(
                    connections,
                    connection -> connection.getTarget().getPracticedSeasonalCropCycle().getTopiaId());

            List<PracticedIntervention> interventions = practicedInterventionDao.forPracticedCropCycleConnectionIn(connections)
                    .setOrderByArguments(PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION, PracticedIntervention.PROPERTY_RANK)
                    .findAll();
            Multimap<PracticedCropCycleConnection, PracticedIntervention> interventionsByConnection = Multimaps.index(
                    interventions,
                    PracticedIntervention::getPracticedCropCycleConnection);

            for (PracticedSeasonalCropCycle practicedSeasonalCropCycle : practicedSeasonalCropCycles) {
                String domainCode = practicedSeasonalCropCycle.getPracticedSystem()
                        .getGrowingSystem()
                        .getGrowingPlan()
                        .getDomain()
                        .getCode();

                PracticedSeasonalCropCycleDto dto = new PracticedSeasonalCropCycleDto();
                dto.setTopiaId(practicedSeasonalCropCycle.getTopiaId());

                // graph data for each cycle
                List<PracticedCropCycleNodeDto> nodes = getNodesOnlyForCycle(practicedSeasonalCropCycle);
                dto.setCropCycleNodeDtos(nodes);

                Collection<PracticedCropCycleConnection> cycleConnections = connectionsByCycleId.get(practicedSeasonalCropCycle.getTopiaId());

                List<PracticedCropCycleConnectionDto> connectionDtos = getPracticedCropCycleConnectionDtos(
                        interventionsByConnection,
                        domainCode,
                        cycleConnections);

                dto.setCropCycleConnectionDtos(connectionDtos);

                // add dto
                result.add(dto);
            }
        }

        return result;
    }

    private List<PracticedCropCycleConnectionDto> getPracticedCropCycleConnectionDtos(
            Multimap<PracticedCropCycleConnection, PracticedIntervention> interventionsByConnection,
            String domainCode,
            Collection<PracticedCropCycleConnection> cycleConnections) {
        List<PracticedCropCycleConnectionDto> connectionDtos = new ArrayList<>();

        if (cycleConnections != null) {
            for (PracticedCropCycleConnection connection : cycleConnections) {
                PracticedCropCycleConnectionDto connectionDto = PracticedSystems.CROP_CYCLE_CONNECTION_TO_DTO.apply(connection);

                String intermediateCroppingPlanEntryCode = connectionDto.getIntermediateCroppingPlanEntryCode();

                if (StringUtils.isNotBlank(intermediateCroppingPlanEntryCode)) {
                    String cropName = getCroppingPlanEntryName(intermediateCroppingPlanEntryCode);
                    connectionDto.setIntermediateCropName(cropName);
                }
                String croppingPlanEntryCode = connection.getTarget().getCroppingPlanEntryCode();

                // add all connection interventions
                Collection<PracticedIntervention> connectionInterventions = interventionsByConnection.get(connection);
                if (!connectionInterventions.isEmpty()) {
                    List<PracticedInterventionDto> interventionDtos = practicedInterventionToInterventionDtos(
                            domainCode,
                            connectionInterventions,
                            croppingPlanEntryCode,
                            intermediateCroppingPlanEntryCode);
                    connectionDto.setInterventions(interventionDtos);
                }
                // add connection
                connectionDtos.add(connectionDto);
            }
        }
        return connectionDtos;
    }

    @Override
    public PracticedSeasonalCropCycle getPracticedSeasonalCropCycle(String cycleId) {
        PracticedSeasonalCropCycle result;
        if (StringUtils.isEmpty(cycleId)) {
            result = practicedSeasonalCropCycleDao.newInstance();
        } else {
            result = practicedSeasonalCropCycleDao.forTopiaIdEquals(cycleId).findUniqueOrNull();
        }
        Preconditions.checkState(result != null, "Expected PracticedSeasonalCropCycle not found: " + cycleId);
        return result;
    }

    @Override
    public PracticedPerennialCropCycle getPracticedPerennialCropCycle(String cycleId) {
        PracticedPerennialCropCycle result;
        if (StringUtils.isEmpty(cycleId)) {
            result = practicedPerennialCropCycleDao.newInstance();
        } else {
            result = practicedPerennialCropCycleDao.forTopiaIdEquals(cycleId).findUnique();
        }

        Preconditions.checkState(result != null, "Expected practicedPerennialCropCycle not found: " + cycleId);
        return result;
    }


    protected void createOrUpdatePracticedSeasonalCropCycle(CreateOrUpdatePracticedSeasonalCropCycleContext context) {

        PracticedSeasonalCropCycle cycle = context.getCycle();
        List<String> practicedSystemAvailableCropCodes = context.getPracticedSystemAvailableCropCodes();

        PracticedSystem practicedSystem = context.getPracticedSystem();
        List<PracticedCropCycleNodeDto> nodes = context.getNodeDtos();
        List<PracticedCropCycleConnectionDto> connections = context.getConnectionDtos();

        Preconditions.checkNotNull(cycle);
        Preconditions.checkNotNull(nodes);
        Preconditions.checkNotNull(connections);

        // Make multimap of current connectionDtos
        MultiKeyMap<String, PracticedCropCycleConnection> cycleConnBySourceTarget = new MultiKeyMap<>();

        PracticedSeasonalCropCycle result = createOrUpdatePracticedSeasonalCropCycleEntity(cycle, practicedSystem, cycleConnBySourceTarget);

        // Make sure list is not empty to avoid breaking code
        if (cycle.getCropCycleNodes() == null) {
            cycle.setCropCycleNodes(new ArrayList<>());
        }

        final Map<String, PracticedCropCycleNode> nodeIdToEntity = new HashMap<>();

        // manage cycle nodes
        Collection<PracticedCropCycleNode> cycleNodes = getPracticedCropCycleNodes(cycle);

        createOrUpdatePracticedSeasonalCropCycleNodes(context, cycle, practicedSystemAvailableCropCodes, practicedSystem, nodes, result, nodeIdToEntity);

        createOrUpdatePracticedSeasonalCropCycleConnections(context, practicedSystemAvailableCropCodes, practicedSystem, connections, cycleConnBySourceTarget, nodeIdToEntity);

        // connectionDtos have first to be removed
        deleteRemovedNodesAndConnctions(cycleConnBySourceTarget, nodeIdToEntity, cycleNodes);

        // delete node (after connectionDtos)
        cycleNodes.retainAll(nodeIdToEntity.values());

    }

    protected Collection<PracticedCropCycleNode> getPracticedCropCycleNodes(PracticedSeasonalCropCycle cycle) {
        Collection<PracticedCropCycleNode> cycleNodes = cycle.getCropCycleNodes();
        if (cycleNodes == null) {
            cycleNodes = new ArrayList<>();
            cycle.setCropCycleNodes(cycleNodes);
        }
        return cycleNodes;
    }

    protected PracticedSeasonalCropCycle createOrUpdatePracticedSeasonalCropCycleEntity(PracticedSeasonalCropCycle cycle, PracticedSystem practicedSystem, MultiKeyMap<String, PracticedCropCycleConnection> cycleConnBySourceTarget) {
        PracticedSeasonalCropCycle result;
        if (!cycle.isPersisted()) {
            Preconditions.checkNotNull(practicedSystem);
            cycle.setPracticedSystem(practicedSystem);
            result = practicedSeasonalCropCycleDao.create(cycle);
        } else {
            result = practicedSeasonalCropCycleDao.update(cycle);
            List<PracticedCropCycleConnection> cycleConnections = practicedCropCycleConnectionDao.findAllByCropCycle(result.getTopiaId());
            for (PracticedCropCycleConnection conn : cycleConnections) {
                cycleConnBySourceTarget.put(conn.getSource().getTopiaId(), conn.getTarget().getTopiaId(), conn);
            }
        }
        return result;
    }

    protected void deleteRemovedNodesAndConnctions(MultiKeyMap<String, PracticedCropCycleConnection> cycleConnBySourceTarget, Map<String, PracticedCropCycleNode> nodeIdToEntity, Collection<PracticedCropCycleNode> cycleNodes) {
        Set<PracticedCropCycleConnection> unusedPracticedCropCycleConnections = new HashSet<>(cycleConnBySourceTarget.values());
        Collection<PracticedCropCycleNode> toDeleteNodes = CollectionUtils.subtract(cycleNodes, nodeIdToEntity.values());
        for (PracticedCropCycleNode nodeToDelete : toDeleteNodes) {
            unusedPracticedCropCycleConnections.addAll(practicedCropCycleConnectionDao.forTargetEquals(nodeToDelete).findAll());
            unusedPracticedCropCycleConnections.addAll(practicedCropCycleConnectionDao.forSourceEquals(nodeToDelete).findAll());

        }
        removePracticedCropCycleConnections(unusedPracticedCropCycleConnections);
    }

    protected void createOrUpdatePracticedSeasonalCropCycleConnections(
            CreateOrUpdatePracticedSeasonalCropCycleContext context,
            List<String> practicedSystemAvailableCropCodes,
            PracticedSystem practicedSystem,
            List<PracticedCropCycleConnectionDto> connections,
            MultiKeyMap<String, PracticedCropCycleConnection> cycleConnBySourceTarget,
            Map<String, PracticedCropCycleNode> nodeIdToEntity) {

        Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(
                practicedSystem.getGrowingSystem().getTopiaId(), practicedSystem.getCampaigns());
        Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant = getSectorByCodeEspceBotaniqueCodeQualifiant(
                practicedSystem.getGrowingSystem().getTopiaId(), practicedSystem.getCampaigns());

        for (PracticedCropCycleConnectionDto connectionDto : connections) {

            if (StringUtils.isNotBlank(connectionDto.getIntermediateCroppingPlanEntryCode())) {
                Preconditions.checkArgument(
                        practicedSystemAvailableCropCodes.contains(connectionDto.getIntermediateCroppingPlanEntryCode()),
                        "The croppingPlanEntry with the code:" + connectionDto.getIntermediateCroppingPlanEntryCode() + " was not found");
            }
            Preconditions.checkArgument(StringUtils.isNotBlank(connectionDto.getSourceId()));
            Preconditions.checkArgument(StringUtils.isNotBlank(connectionDto.getTargetId()));

            String sourceId = Entities.UNESCAPE_TOPIA_ID.apply(connectionDto.getSourceId());
            String targetId = Entities.UNESCAPE_TOPIA_ID.apply(connectionDto.getTargetId());

            List<PracticedIntervention> interventions = new ArrayList<>();

            // get from cache and removed if (still exists to be not deleted after loop)
            PracticedCropCycleConnection connection = cycleConnBySourceTarget.removeMultiKey(sourceId, targetId);
            if (connection == null) {
                PracticedCropCycleNode sourceNode = nodeIdToEntity.get(sourceId);
                if (sourceNode == null) {
                    sourceNode = practicedCropCycleNodeDao.forTopiaIdEquals(sourceId).findUnique();
                }

                PracticedCropCycleNode targetNode = nodeIdToEntity.get(targetId);
                if (targetNode == null) {
                    targetNode = practicedCropCycleNodeDao.forTopiaIdEquals(targetId).findUnique();
                }

                connection = practicedCropCycleConnectionDao.create(PracticedCropCycleConnection.PROPERTY_SOURCE, sourceNode,
                        PracticedCropCycleConnection.PROPERTY_TARGET, targetNode);
            } else {
                interventions = this.getCropCycleNodeConnectionInterventions(connection);
            }

            // culuture intermédiaire de la connexion
            connection.setIntermediateCroppingPlanEntryCode(connectionDto.getIntermediateCroppingPlanEntryCode());
            Double croppingPlanEntryFrequency = connectionDto.getCroppingPlanEntryFrequency();
            croppingPlanEntryFrequency = croppingPlanEntryFrequency == null ? 0 : croppingPlanEntryFrequency;
            connection.setCroppingPlanEntryFrequency(croppingPlanEntryFrequency);
            connection.setNotUsedForThisCampaign(connectionDto.isNotUsedForThisCampaign());

            CreateOrUpdatePracticedInterventionsContext interventionsContext =
                    CreateOrUpdatePracticedInterventionsContext.createOrUpdateSeasonalPracticedInterventionsContext(
                            context.getPracticedSystem(),
                            context.getDomainInputStockByIds(),
                            interventions,
                            connection,
                            connectionDto,
                            context.getDomainCode(),
                            practicedSystem.getCampaigns(),
                            speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                            sectorByCodeEspeceBotaniqueCodeQualifiant,
                            context.getCroppingPlanEntryMap(),
                            context.getCroppingPlanSpeciesMap());

            saveInterventions(interventionsContext);

        }
    }

    protected void createOrUpdatePracticedSeasonalCropCycleNodes(CreateOrUpdatePracticedSeasonalCropCycleContext context, PracticedSeasonalCropCycle cycle, List<String> practicedSystemAvailaibleCropCodes, PracticedSystem practicedSystem, List<PracticedCropCycleNodeDto> nodes, PracticedSeasonalCropCycle result, Map<String, PracticedCropCycleNode> nodeIdToEntity) {
        for (PracticedCropCycleNodeDto dtoNode : nodes) {
            String croppingPlanEntryCode = dtoNode.getCroppingPlanEntryCode();

            Preconditions.checkArgument(
                    practicedSystemAvailaibleCropCodes.contains(croppingPlanEntryCode),
                    String.format("The croppingPlanEntry with the code %s was not found on domain %s for campaigns %s",
                            croppingPlanEntryCode, context.getDomainName(), practicedSystem.getCampaigns()));

            String nodeId = Entities.UNESCAPE_TOPIA_ID.apply(dtoNode.getNodeId());
            PracticedCropCycleNode entityNode;
            if (nodeId.startsWith(PracticedSystemService.NEW_NODE_PREFIX)) {
                entityNode = practicedCropCycleNodeDao.newInstance();
                entityNode.setCroppingPlanEntryCode(croppingPlanEntryCode);
                // the node is added to the practicedSeasonalCropCycle nodes collection
                result.addCropCycleNodes(entityNode);
            } else {
                entityNode = TopiaEntities.findByTopiaId(cycle.getCropCycleNodes(), nodeId);
            }
            entityNode.setRank(dtoNode.getX());
            entityNode.setY(dtoNode.getY());
            entityNode.setEndCycle(dtoNode.isEndCycle());
            entityNode.setSameCampaignAsPreviousNode(dtoNode.isSameCampaignAsPreviousNode());
            entityNode.setInitNodeFrequency(dtoNode.getInitNodeFrequency());

            PracticedCropCycleNode persistentNode;
            if (entityNode.isPersisted()) {
                persistentNode = practicedCropCycleNodeDao.update(entityNode);
            } else {
                persistentNode = practicedCropCycleNodeDao.create(entityNode);
            }

            nodeIdToEntity.put(nodeId, persistentNode);
        }
    }

    protected void createOrUpdatePracticedPerennialCropCycle(CreateOrUpdatePracticedPerennialCropCycleContext context) {

        PracticedPerennialCropCycle cycle = context.getPracticedPerennialCropCycle();
        PracticedSystem practicedSystem = context.getPracticedSystem();
        List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos = context.getCropCyclePhaseDtos();

        Preconditions.checkNotNull(cycle);
        Preconditions.checkNotNull(practicedSystem);
        Preconditions.checkNotNull(cropCyclePhaseDtos);
        Preconditions.checkArgument(!cropCyclePhaseDtos.isEmpty());

        cycle.setPracticedSystem(practicedSystem);
        if (cycle.isPersisted()) {
            practicedPerennialCropCycleDao.update(cycle);
        } else {
            practicedPerennialCropCycleDao.create(cycle);
        }

        Map<PracticedCropCyclePhaseDto, PracticedCropCyclePhase> phasesDtoPhase = convertCropCyclePhaseDto(cropCyclePhaseDtos, cycle);

        context.setPhasesDtoPhase(phasesDtoPhase);

        convertCropCyclePerennialSpeciesDto(context);

        convertPerennialInterventionDto(context);
    }

    protected void removePracticedCropCycleConnections(Collection<PracticedCropCycleConnection> practicedCropCycleConnections) {
        if (practicedCropCycleConnections != null) {
            for (PracticedCropCycleConnection practicedCropCycleConnection : practicedCropCycleConnections) {
                List<PracticedIntervention> practicedInterventions = practicedInterventionDao.forPracticedCropCycleConnectionEquals(practicedCropCycleConnection).findAll();
                removePracticedInterventions(practicedInterventions);
            }
            practicedCropCycleConnectionDao.deleteAll(practicedCropCycleConnections);
        }
    }

    protected void removePracticedPerennialCropCycle(List<PracticedPerennialCropCycle> practicedPerennialCropCycles) {
        if (practicedPerennialCropCycles != null) {
            for (PracticedPerennialCropCycle practicedPerennialCropCycle : practicedPerennialCropCycles) {
                Collection<PracticedCropCyclePhase> practicedCropCyclePhases = practicedPerennialCropCycle.getCropCyclePhases();
                removePracticedCropCyclePhasesChildrenObjects(practicedCropCyclePhases);
            }
            practicedPerennialCropCycleDao.deleteAll(practicedPerennialCropCycles);
        }
    }

    protected void removePracticedCropCyclePhasesChildrenObjects(Collection<PracticedCropCyclePhase> practicedCropCyclePhases) {
        // practicedCropCyclePhases ne peut être null
        for (PracticedCropCyclePhase practicedCropCyclePhase : practicedCropCyclePhases) {
            List<PracticedIntervention> practicedInterventions = practicedInterventionDao.forPracticedCropCyclePhaseEquals(practicedCropCyclePhase).findAll();
            removePracticedInterventions(practicedInterventions);
        }
    }

    protected void removePracticedInterventions(Collection<PracticedIntervention> practicedInterventions) {
        if (practicedInterventions != null) {
            List<AbstractAction> allActionsToRemove = new ArrayList<>();
            for (PracticedIntervention practicedIntervention : practicedInterventions) {
                List<AbstractAction> abstractActions = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).findAll();
                allActionsToRemove.addAll(abstractActions);
            }
            actionService.removeActionsAndUsagesDeps(allActionsToRemove);
            practicedInterventionDao.deleteAll(practicedInterventions);
        }
    }

    protected void convertCropCyclePerennialSpeciesDto(CreateOrUpdatePracticedPerennialCropCycleContext context) {
        List<PracticedCropCycleSpeciesDto> speciesDtos = context.getCropCyclePerennialSpeciesDto();
        PracticedPerennialCropCycle cycle = context.getPracticedPerennialCropCycle();

        Collection<PracticedCropCycleSpecies> currentSpecies = cycle.getPracticedCropCycleSpecies();
        Collection<PracticedCropCycleSpecies> nonDeleted = new ArrayList<>();
        if (currentSpecies == null) {
            currentSpecies = new ArrayList<>();
            cycle.setPracticedCropCycleSpecies(currentSpecies);
        }

        // update list with dto
        Map<String, PracticedCropCycleSpecies> currentSpeciesMap = Maps.uniqueIndex(currentSpecies, PracticedCropCycleSpecies::getCroppingPlanSpeciesCode);

        if (speciesDtos != null) {
            // get domain croppingPlanSpecies codes
            Set<String> cpsCodes = context.getSpeciesCodeToCodeEspeceBotaniqueCodeQualifiantAee() != null ? context.getSpeciesCodeToCodeEspeceBotaniqueCodeQualifiantAee().keySet() : new HashSet<>();

            for (PracticedCropCycleSpeciesDto cropCyclePerennialSpeciesDto : speciesDtos) {
                String code = cropCyclePerennialSpeciesDto.getCode();
                // check if croppingPlanSpecies is part of domain
                if (!cpsCodes.contains(code)) continue;

                PracticedCropCycleSpecies cropCyclePerennialSpecies = currentSpeciesMap.get(code);
                if (cropCyclePerennialSpecies == null) {
                    cropCyclePerennialSpecies = practicedCropCycleSpeciesDao.newInstance();
                    cropCyclePerennialSpecies.setCroppingPlanSpeciesCode(code);
                    cropCyclePerennialSpecies.setCycle(cycle);
                    cycle.addPracticedCropCycleSpecies(cropCyclePerennialSpecies);
                }

                cropCyclePerennialSpecies.setPlantsCertified(cropCyclePerennialSpeciesDto.isPlantCertified());
                cropCyclePerennialSpecies.setOverGraftDate(cropCyclePerennialSpeciesDto.getOverGraftDate());

                if (cropCyclePerennialSpeciesDto.getGraftClone() != null) {
                    String graftTopiaId = cropCyclePerennialSpeciesDto.getGraftClone().getTopiaId();
                    if (StringUtils.isNotBlank(graftTopiaId)) {
                        RefClonePlantGrape clonePlantGrape = refClonePlantGrapeDao.forTopiaIdEquals(graftTopiaId).findUnique();
                        cropCyclePerennialSpecies.setGraftClone(clonePlantGrape);
                    }
                }
                if (cropCyclePerennialSpeciesDto.getGraftSupport() != null) {
                    String graftTopiaId = cropCyclePerennialSpeciesDto.getGraftSupport().getTopiaId();
                    if (StringUtils.isNotBlank(graftTopiaId)) {
                        RefVariete variete = varieteDao.forTopiaIdEquals(graftTopiaId).findUnique();
                        cropCyclePerennialSpecies.setGraftSupport(variete);
                    }
                }

                if (cropCyclePerennialSpecies.isPersisted()) {
                    practicedCropCycleSpeciesDao.update(cropCyclePerennialSpecies);
                } else {
                    practicedCropCycleSpeciesDao.create(cropCyclePerennialSpecies);
                }
                nonDeleted.add(cropCyclePerennialSpecies);
            }
        }
        currentSpecies.retainAll(nonDeleted);
    }

    protected Map<PracticedCropCyclePhaseDto, PracticedCropCyclePhase> convertCropCyclePhaseDto(List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos, PracticedPerennialCropCycle cycle) {

        Map<PracticedCropCyclePhaseDto, PracticedCropCyclePhase> result = new HashMap<>();
        // update list with dto
        Collection<PracticedCropCyclePhase> nonNullPhases = MoreObjects.firstNonNull(cycle.getCropCyclePhases(), new LinkedList<>());
        Map<String, PracticedCropCyclePhase> immutCurrentPhasesMap = Maps.uniqueIndex(nonNullPhases, Entities.GET_TOPIA_ID::apply);
        Map<String, PracticedCropCyclePhase> currentPhasesMap = new HashMap<>(immutCurrentPhasesMap);

        // there is at least one CropCyclePhase
        for (PracticedCropCyclePhaseDto cropCyclePhaseDto : cropCyclePhaseDtos) {
            String phaseId = cropCyclePhaseDto.getTopiaId();
            PracticedCropCyclePhase phase;
            if (StringUtils.isBlank(phaseId)) {
                phase = practicedCropCyclePhaseDao.newInstance();
                phase.setPracticedPerennialCropCycle(cycle);
            } else {
                phase = currentPhasesMap.remove(phaseId);
            }
            result.put(cropCyclePhaseDto, phase);

            phase.setType(cropCyclePhaseDto.getType());
            phase.setDuration(cropCyclePhaseDto.getDuration());
            if (StringUtils.isBlank(phaseId)) {
                cycle.addCropCyclePhases(phase);
            }
            if (!phase.isPersisted()) {
                practicedCropCyclePhaseDao.create(phase);
            } else {
                practicedCropCyclePhaseDao.update(phase);
            }
        }

        removePracticedCropCyclePhasesChildrenObjects(currentPhasesMap.values());
        currentPhasesMap.values()
                .forEach(cycle::removeCropCyclePhases);

        return result;
    }

    protected void convertPerennialInterventionDto(CreateOrUpdatePracticedPerennialCropCycleContext context) {
        Map<PracticedCropCyclePhaseDto, PracticedCropCyclePhase> phaseDtosPhases = context.getPhasesDtoPhase();

        for (Map.Entry<PracticedCropCyclePhaseDto, PracticedCropCyclePhase> phasesDtoEntry : phaseDtosPhases.entrySet()) {
            PracticedCropCyclePhaseDto phasesDto = phasesDtoEntry.getKey();
            PracticedCropCyclePhase phase = phasesDtoEntry.getValue();

            List<PracticedInterventionDto> interventionDtos = phasesDto.getInterventions();
            List<PracticedIntervention> interventions = getCropCyclePhaseInterventions(phase);

            CreateOrUpdatePracticedInterventionsContext interventionsContext =
                    CreateOrUpdatePracticedInterventionsContext.createOrUpdatePerennialPracticedInterventionsContext(
                            context.getPracticedSystem(),
                            context.getDomainInputStockByIds(),
                            interventionDtos,
                            interventions,
                            context.getDomainCode(),
                            phase,
                            context.getPracticedSystem().getCampaigns(),
                            context.getSpeciesCodeToCodeEspeceBotaniqueCodeQualifiantAee(),
                            context.getSectorByCodeEspceBotaniqueCodeQualifiant(),
                            context.getCroppingPlanEntryMap(),
                            context.getCroppingPlanSpeciesMap()
                    );

            saveInterventions(interventionsContext);
        }
    }

    protected void saveInterventions(CreateOrUpdatePracticedInterventionsContext context) {

        List<PracticedInterventionDto> interventionDtos = context.getInterventionDtos();
        List<PracticedIntervention> interventions = context.getInterventions();
        PracticedCropCyclePhase phase = context.getPhase();
        PracticedCropCycleConnection connection = context.getConnection();

        boolean intermediateCrop = context.isIntermediateCrop();

        if (interventions == null) {
            interventions = new ArrayList<>();
        }

        // remove new interventionDtos witch domainId doesn't match the domain code.
        if (interventionDtos != null) {
            Iterator<PracticedInterventionDto> validInterventionDtos = interventionDtos.iterator();
            while (validInterventionDtos.hasNext()) {
                PracticedInterventionDto interventionDto = validInterventionDtos.next();
                String interventionId = interventionDto.getTopiaId() != null && interventionDto.getTopiaId().startsWith(NEW_INTERVENTION_PREFIX) ? null : interventionDto.getTopiaId();
                if (StringUtils.isEmpty(interventionId) && (StringUtils.isEmpty(interventionDto.getDomainId()) ||
                        !interventionDto.getDomainId().equals(context.getDomainCode()))) {
                    validInterventionDtos.remove();
                }
            }
        }

        String croppingPlanEntryCode = null;
        String intermediateCroppingPlanEntryCode = null;
        if (connection != null) {
            croppingPlanEntryCode = connection.getTarget().getCroppingPlanEntryCode();
            intermediateCroppingPlanEntryCode = connection.getIntermediateCroppingPlanEntryCode();
        } else if (phase != null) {
            croppingPlanEntryCode = phase.getPracticedPerennialCropCycle().getCroppingPlanEntryCode();
        }

        Map<String, PracticedIntervention> currentInterventionsMap = interventions.stream().collect(Collectors.toMap(Entities.GET_TOPIA_ID, Function.identity()));
        Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = context.getSpeciesCodeToCodeEspeceBotaniqueCodeQualifiantAee();
        Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant = context.getSectorByCodeEspceBotaniqueCodeQualifiant();

        Set<String> cpsCodes = speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee != null ? speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee.keySet() : new HashSet<>();

        if (interventionDtos != null && !interventionDtos.isEmpty()) {

            for (int i = 0; i < interventionDtos.size(); i++) {
                // reordered interventions according the position into the list
                PracticedInterventionDto interventionDto = interventionDtos.get(i);
                interventionDto.setRank(i);

                if (!intermediateCrop) {
                    interventionDto.setIntermediateCrop(false);
                }

                if (interventionDto.isIntermediateCrop()) {
                    interventionDto.setFromCropCode(intermediateCroppingPlanEntryCode);
                } else {
                    interventionDto.setFromCropCode(croppingPlanEntryCode);
                }

                PracticedIntervention intervention = currentInterventionsMap.remove(interventionDto.getTopiaId());

                if (intervention == null) {
                    intervention = practicedInterventionDao.newInstance();
                }

                addAllSpeciesStades(intervention, interventionDto, cpsCodes, context);
                interventionDto.setPracticedCropCyclePhase(phase);
                interventionDto.setPracticedCropCycleConnection(connection);

                Binder<PracticedInterventionDto, PracticedIntervention> binder = BinderFactory.newBinder(PracticedInterventionDto.class, PracticedIntervention.class);
                binder.copyExcluding(interventionDto, intervention,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        PracticedIntervention.PROPERTY_SPECIES_STADES);

                final boolean isNewIntervention = interventionDto.getTopiaId().contains(NEW_INTERVENTION_PREFIX);

                PracticedIntervention persistedIntervention;
                if (isNewIntervention) {
                    persistedIntervention = practicedInterventionDao.create(intervention);
                    interventions.add(persistedIntervention);
                } else {
                    persistedIntervention = practicedInterventionDao.update(intervention);
                }

                actionService.createOrUpdatePracticedInterventionActionAndUsages(
                        persistedIntervention,
                        interventionDto.getActionDtos(),
                        context.getDomainInputStockUnitByIds().values(),
                        context.getPracticedSystem(),
                        speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                        sectorByCodeEspeceBotaniqueCodeQualifiant,
                        interventionDto.getSpeciesStadesDtos()
                );

            }

        }

        removePracticedInterventions(currentInterventionsMap.values());
    }

    protected void addAllSpeciesStades(
            PracticedIntervention intervention,
            PracticedInterventionDto interventionDto,
            Set<String> cpsCodes,
            CreateOrUpdatePracticedInterventionsContext context) {

        String fromCropCode = interventionDto.getFromCropCode();
        CroppingPlanEntry croppingPlanEntry = context.getCroppingPlanEntryMap().get(fromCropCode);
        List<String> croppingPlanSpeciesCode = CollectionUtils.emptyIfNull(croppingPlanEntry.getCroppingPlanSpecies())
                .stream()
                .filter(Objects::nonNull)
                .map(CroppingPlanSpecies::getCode)
                .collect(Collectors.toList());

        List<SpeciesStadeDto> speciesStadesDtos = interventionDto.getSpeciesStadesDtos();

        Collection<PracticedSpeciesStade> allOriginalSpeciesStades = intervention.getSpeciesStades();
        if (allOriginalSpeciesStades == null) {
            allOriginalSpeciesStades = new ArrayList<>();
            intervention.setSpeciesStades(allOriginalSpeciesStades);
        }

        List<PracticedSpeciesStade> nonDeleted = new ArrayList<>();

        if (speciesStadesDtos != null && !speciesStadesDtos.isEmpty()) {

            Map<String, PracticedSpeciesStade> allOriginalSpeciesStadesMap = Maps.uniqueIndex(allOriginalSpeciesStades, Entities.GET_TOPIA_ID::apply);

            ListIterator<SpeciesStadeDto> speciesStadeDtoListIterator = speciesStadesDtos.listIterator();
            while (speciesStadeDtoListIterator.hasNext()) {

                SpeciesStadeDto speciesStadeDto = speciesStadeDtoListIterator.next();

                String speciesCode = speciesStadeDto.getSpeciesCode();

                if (!cpsCodes.contains(speciesCode) || !croppingPlanSpeciesCode.contains(speciesCode)) {
                    speciesStadeDtoListIterator.remove();
                    continue;
                }

                PracticedSpeciesStade editedSpeciesStades;

                if (StringUtils.isBlank(speciesStadeDto.getTopiaId())) {
                    editedSpeciesStades = practicedSpeciesStadeDao.newInstance();
                    allOriginalSpeciesStades.add(editedSpeciesStades);
                } else {
                    editedSpeciesStades = allOriginalSpeciesStadesMap.get(speciesStadeDto.getTopiaId());
                    if (editedSpeciesStades == null) {// on arrive dans ce cas
                        editedSpeciesStades = practicedSpeciesStadeDao.forTopiaIdEquals(speciesStadeDto.getTopiaId()).findAnyOrNull();
                        if (editedSpeciesStades != null) {
                            allOriginalSpeciesStades.add(editedSpeciesStades);
                        }
                    }
                }

                if (editedSpeciesStades == null) {
                    continue;
                }

                editedSpeciesStades.setSpeciesCode(speciesCode);
                // if there are no sepciesStadeMin
                if (speciesStadeDto.getStadeMin() == null) {
                    editedSpeciesStades.setStadeMin(null);
                } else {
                    String refStadeEdiTopiaIdMin = speciesStadeDto.getStadeMin().getTopiaId();
                    // we look for the sepciesStadeMin if there were no previous ones or if the previous one was different.
                    if (editedSpeciesStades.getStadeMin() == null ||
                            !editedSpeciesStades.getStadeMin().getTopiaId().contentEquals(refStadeEdiTopiaIdMin)) {
                        editedSpeciesStades.setStadeMin(refStadeEDIDao.forTopiaIdEquals(refStadeEdiTopiaIdMin).findUnique());
                    }

                    // if no sepciesStadeMax
                    if (speciesStadeDto.getStadeMax() == null) {
                        editedSpeciesStades.setStadeMax(null);
                    } else {
                        String refStadeEdiTopiaIdMax = speciesStadeDto.getStadeMax().getTopiaId();

                        // we look for the sepciesStadeMax if there were no previous ones or if the previous one was different.
                        if (editedSpeciesStades.getStadeMax() == null ||
                                !refStadeEdiTopiaIdMax.contentEquals(editedSpeciesStades.getStadeMax().getTopiaId())) {
                            // no needs to look for the speciesStadeMax's refStadeEDIDao if it's the same of speciesStadeMin
                            if (!refStadeEdiTopiaIdMin.contentEquals(refStadeEdiTopiaIdMax)) {
                                editedSpeciesStades.setStadeMax(refStadeEDIDao.forTopiaIdEquals(refStadeEdiTopiaIdMax).findUnique());
                            } else {
                                // the sepciesStadeMax is the same of the speciesStadeMin
                                editedSpeciesStades.setStadeMax(editedSpeciesStades.getStadeMin());
                            }
                        }
                    }
                }

                PracticedSpeciesStade persistedSpeciesStade;
                if (editedSpeciesStades.isPersisted()) {
                    persistedSpeciesStade = practicedSpeciesStadeDao.update(editedSpeciesStades);
                } else {
                    persistedSpeciesStade = practicedSpeciesStadeDao.create(editedSpeciesStades);
                }

                nonDeleted.add(persistedSpeciesStade);
            }
        }
        allOriginalSpeciesStades.retainAll(nonDeleted);
    }

    protected List<PracticedCropCyclePhaseDto> getPhasesDTOs(PracticedPerennialCropCycle cycle) {

        String domainCode = cycle.getPracticedSystem().getGrowingSystem().getGrowingPlan().getDomain().getCode();
        Collection<PracticedCropCyclePhase> currentPhases = cycle.getCropCyclePhases();
        List<PracticedCropCyclePhaseDto> currentPhasesDtos = new ArrayList<>(currentPhases.size());
        for (PracticedCropCyclePhase currentPhase : currentPhases) {
            PracticedCropCyclePhaseDto currentPhaseDto = new PracticedCropCyclePhaseDto();
            currentPhaseDto.setType(currentPhase.getType());
            currentPhaseDto.setDuration(currentPhase.getDuration());
            currentPhaseDto.setTopiaId(currentPhase.getTopiaId());

            List<PracticedIntervention> interventions = getCropCyclePhaseInterventions(currentPhase);
            if (interventions != null) {
                String croppingPlanEntryCode = currentPhase.getPracticedPerennialCropCycle().getCroppingPlanEntryCode();
                List<PracticedInterventionDto> interventionDtos = practicedInterventionToInterventionDtos(
                        domainCode,
                        interventions,
                        croppingPlanEntryCode,
                        null);
                currentPhaseDto.setInterventions(interventionDtos);
            }
            currentPhasesDtos.add(currentPhaseDto);

        }

        return currentPhasesDtos;
    }

    protected static void bindInterventionToInterventionDto(
            PracticedIntervention intervention,
            PracticedInterventionDto interventionDto) {
        PracticedSystemServiceImpl.PRACTICED_INTERVENTION_TO_DTO_BINDER.copyExcluding(intervention, interventionDto,
                PracticedIntervention.PROPERTY_SPECIES_STADES,
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE,
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION
        );
    }

    protected List<SpeciesStadeDto> getInterventionSpeciesStadeDtos(PracticedIntervention intervention, ReferentialTranslationMap translationMap) {
        Collection<PracticedSpeciesStade> speciesStades = intervention.getSpeciesStades();
        return speciesStades.stream()
                .map(stade -> PracticedSystems.getDtoForPracticedSpeciesStade(stade, translationMap))
                .collect(Collectors.toList());
    }

    private void fillSpeciesStadeTraductions(
            ReferentialTranslationMap translationMap,
            Collection<PracticedSpeciesStade> speciesStades) {

        Set<String> refStadeEdiTopiaIds = new HashSet<>();
        speciesStades.forEach(s -> {
            if (s.getStadeMin() != null) {
                refStadeEdiTopiaIds.add(s.getStadeMin().getTopiaId());
            }
            if (s.getStadeMax() != null) {
                refStadeEdiTopiaIds.add(s.getStadeMax().getTopiaId());
            }
        });
        i18nService.fillRefStadeEdiTranslations(refStadeEdiTopiaIds, translationMap);
    }

    protected List<PracticedCropCycleNodeDto> getNodesOnlyForCycle(PracticedSeasonalCropCycle cycle) {
        Collection<PracticedCropCycleNode> allByPracticedCropCycle = CollectionUtils.emptyIfNull(cycle.getCropCycleNodes());

        String campaigns = cycle.getPracticedSystem().getCampaigns();
        Set<Integer> campaignsSet = getIntCampaigns(campaigns);

        return allByPracticedCropCycle.stream()
                .map(nodeEntity -> {
                    PracticedCropCycleNodeDto nodeDto = PracticedSystems.CROP_CYCLE_NODE_TO_DTO.apply(nodeEntity);
                    String croppingPlanEntryCode = nodeEntity.getCroppingPlanEntryCode();
                    String name = croppingPlanEntryDao.findFirstEntryNameFromCode(croppingPlanEntryCode, campaignsSet);
                    nodeDto.setLabel(name);
                    return nodeDto;
                })
                .collect(Collectors.toList());
    }

    protected Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>> findEntryAndSpeciesFromCode(String croppingPlanEntryCode, String campaigns) {
        Set<Integer> campaignsSet = getIntCampaigns(campaigns);

        return domainService.getEntryAndSpeciesFromCode(croppingPlanEntryCode, campaignsSet);
    }

    protected List<PracticedIntervention> getCropCyclePhaseInterventions(PracticedCropCyclePhase cropCyclePhase) {
        return practicedInterventionDao.
                forPracticedCropCyclePhaseEquals(cropCyclePhase).
                setOrderByArguments(PracticedIntervention.PROPERTY_RANK).
                findAll();
    }

    protected List<PracticedIntervention> getCropCycleNodeConnectionInterventions(PracticedCropCycleConnection cropCycleNodeConnection) {
        return practicedInterventionDao.
                forPracticedCropCycleConnectionEquals(cropCycleNodeConnection).
                setOrderByArguments(PracticedIntervention.PROPERTY_RANK).
                findAll();
    }

    @Override
    public List<PracticedCropCycleSpeciesDto> getCropCyclePerennialSpecies(String croppingPlanEntryCode,
                                                                           PracticedPerennialCropCycle cycle,
                                                                           String campaigns) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(croppingPlanEntryCode));
        Preconditions.checkArgument(StringUtils.isNotEmpty(campaigns));

        final Map<String, PracticedCropCycleSpecies> speciesCodeToCycleEntity;

        if (cycle == null) {
            speciesCodeToCycleEntity = new HashMap<>();
        } else {
            speciesCodeToCycleEntity = Maps.uniqueIndex(cycle.getPracticedCropCycleSpecies(), PracticedSystems.GET_CROP_CYCLE_PERENNIAL_SPECIES_CODE::apply);
        }

        // Chargement de la liste des espèces (sans doublon)
        Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>> entryAndSpeciesFromCode =
                findEntryAndSpeciesFromCode(croppingPlanEntryCode, campaigns);

        ReferentialTranslationMap translationMap = i18nService.fillCroppingPlanEntryTranslationMaps(List.of(entryAndSpeciesFromCode.getLeft()));

        // Dto -> Decoration
        Function<CroppingPlanSpeciesDto, PracticedCropCycleSpeciesDto> decorateFunction = input -> {
            String croppingPlanSpeciesCode = input.getCode();
            PracticedCropCycleSpeciesDto result = new PracticedCropCycleSpeciesDto(input);
            PracticedCropCycleSpecies cropCyclePerennialSpecies = speciesCodeToCycleEntity.get(croppingPlanSpeciesCode);
            if (cropCyclePerennialSpecies != null) {
                result.setOverGraftDate(cropCyclePerennialSpecies.getOverGraftDate());
                result.setPlantCertified(cropCyclePerennialSpecies.isPlantsCertified());
                result.setGraftSupport(PracticedSystems.REF_VARIETE_TO_GRAPH_DTO.apply(cropCyclePerennialSpecies.getGraftSupport()));
                result.setGraftClone(PracticedSystems.REF_CLONE_TO_GRAPH_DTO.apply(cropCyclePerennialSpecies.getGraftClone()));
            }
            return result;
        };
        // La transformation se fait en 2 temps : Entity -> Dto -> Decoration
        Function<CroppingPlanSpecies, PracticedCropCycleSpeciesDto> transformFunction =
                Functions.compose(decorateFunction::apply, cps -> CroppingPlans.getDtoForCroppingPlanSpecies(cps, translationMap));
        return entryAndSpeciesFromCode.getRight().values().stream()
                .filter(Objects::nonNull)
                .map(transformFunction)
                .collect(Collectors.toList());
    }

    @Override
    public String getCroppingPlanEntryName(String croppingPlanEntryCode) {
        return croppingPlanEntryDao.findCropNameForCode(croppingPlanEntryCode);
    }

    @Override
    public PracticedSystem duplicatePracticedSystemWithUsages(String fromPracticedSystemId, String toGrowingSystemId) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(fromPracticedSystemId));
        Preconditions.checkArgument(StringUtils.isNotEmpty(toGrowingSystemId));

        PracticedSystem fromPracticedSystem = practicedSystemDao.forTopiaIdEquals(fromPracticedSystemId).findUnique();
        GrowingSystem targetedGrowingSystem = growingSystemDao.forTopiaIdEquals(toGrowingSystemId).findUnique();

        // perform clone
        checkGrowingSystemAndParentsActivated(fromPracticedSystem.getGrowingSystem());
        checkGrowingSystemAndParentsActivated(targetedGrowingSystem);

        PracticedDuplicateCropCyclesContext duplicateContext = getDuplicateCropCyclesContextForInputUsages(fromPracticedSystem, targetedGrowingSystem);

        PracticedSystem duplicatedPracticedSystem = bindPracticedSystemCommonData(fromPracticedSystem, duplicateContext);

        duplicateSeasonalCycles(fromPracticedSystem, duplicatedPracticedSystem, duplicateContext);

        duplicatePerennialCropCycles(fromPracticedSystem, duplicatedPracticedSystem, duplicateContext);

        duplicatePracticedSystemPlot(fromPracticedSystem, duplicatedPracticedSystem);

        getTransaction().commit();
        return duplicatedPracticedSystem;
    }

    private PracticedDuplicateCropCyclesContext getDuplicateCropCyclesContextForInputUsages(PracticedSystem fromPracticedSystem, GrowingSystem targetedGrowingSystem) {
        final PracticedDuplicateCropCyclesContext duplicateCropCyclesContext = getDuplicateCropCyclesContext(fromPracticedSystem, targetedGrowingSystem);
        duplicateCropCyclesContext.setWithInputUsage(true);
        boolean isSameGrowingSystemCode = targetedGrowingSystem.getCode().contentEquals(fromPracticedSystem.getGrowingSystem().getCode());
        duplicateCropCyclesContext.setIsSameDomainCode(isSameGrowingSystemCode);// same GS code == same Domaine Code

        return duplicateCropCyclesContext;
    }

    protected void checkGrowingSystemAndParentsActivated(GrowingSystem growingSystem) {

        final boolean growingSystemActive = growingSystem.isActive();
        final GrowingPlan growingPlan = growingSystem.getGrowingPlan();
        final boolean growingPlanActive = growingPlan.isActive();
        final Domain domain = growingPlan.getDomain();
        final boolean domainActive = domain.isActive();

        if (!growingSystemActive || !growingPlanActive || !domainActive) {
            final int domainCampaign = domain.getCampaign();
            String errorMessage = "La duplication n'est pas possible car vous avez:<br><ul>%s%s%s</ul>";
            String toGrowingSystemErrorMessage = !growingSystemActive ? String.format("<li>le syst&egrave;me de culture '%s (%d)' est inactif</li>", growingSystem.getName(), domainCampaign) : "";
            String growingPlanErrorMessage = !growingPlanActive ? String.format("<li>le dispositif '%s (%d)' est inactif</li>", growingPlan.getName(), domainCampaign) : "";
            String domainErrorMessage = !domainActive ? String.format("<li>le domaine '%s (%d)' est inactif</li>", domain.getName(), domainCampaign) : "";

            throw new AgrosystDuplicationException(String.format(
                    errorMessage,
                    toGrowingSystemErrorMessage,
                    growingPlanErrorMessage,
                    domainErrorMessage));
        }

    }

    protected List<String> getTargetedCroppingPlanCodes(Domain domain, Integer targetedCampaign, String targetedDomainCode) {
        List<String> targetedCroppingPlanCodes = domainService.getCroppingPlanCodeForDomainsAndCampaigns(targetedDomainCode, Collections.singleton(targetedCampaign));
        if (targetedCroppingPlanCodes.isEmpty()) {

            Domain relatedDomain = domainService.getDomainForCampaign(domain.getCode(), targetedCampaign);

            if (relatedDomain != null &&
                    relatedDomain.isActive()) {
                throw new AgrosystDuplicationException(String.format(
                        "Aucune culture du sch&eacute;ma de rotation n'est pas pr&eacute;sente sur le domaine '%s (%d)'.</br>" +
                                "La duplication de votre synth&eacute;tis&eacute; est impossible.</br>" +
                                "<strong>Vous pouvez utiliser le copier-coller des cultures depuis une autre campagne du m&ecirc;me domaine " +
                                "pour enregistrer la ou les culture(s) manquantes avant de refaire cette duplication.</strong>",
                        domain.getName(),
                        targetedCampaign));
            } else if (relatedDomain == null) {
                throw new AgrosystDuplicationException(String.format(
                        "Aucun domain '%s' présent pour la campagne '%d'.</br>" +
                                "La duplication de votre synth&eacute;tis&eacute; est impossible.</br>",
                        domain.getName(),
                        targetedCampaign));
            } else if (!relatedDomain.isActive()) {
                throw new AgrosystDuplicationException(String.format(
                        "Aucun domain actif sur le domaine '%s (%d)'.</br>" +
                                "La duplication de votre synth&eacute;tis&eacute; est impossible.</br>",
                        domain.getName(),
                        targetedCampaign));
            }
        }
        return targetedCroppingPlanCodes;
    }

    protected PracticedSystem bindPracticedSystemCommonData(PracticedSystem practicedSystem, PracticedDuplicateCropCyclesContext duplicateCropCyclesContext) {
        Binder<PracticedSystem, PracticedSystem> practicedSystemBinder = BinderFactory.newBinder(PracticedSystem.class);
        PracticedSystem duplicatedPracticedSystem = practicedSystemDao.newInstance();
        practicedSystemBinder.copyExcluding(practicedSystem, duplicatedPracticedSystem,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                PracticedSystem.PROPERTY_VALIDATED,
                PracticedSystem.PROPERTY_VALIDATION_DATE,
                PracticedSystem.PROPERTY_GROWING_SYSTEM);
        duplicatedPracticedSystem.setUpdateDate(context.getCurrentTime());
        duplicatedPracticedSystem.setGrowingSystem(duplicateCropCyclesContext.getToGrowingSystem());
        duplicatedPracticedSystem.setCampaigns(duplicateCropCyclesContext.getTargetedCampaign());
        duplicatedPracticedSystem = practicedSystemDao.create(duplicatedPracticedSystem);
        duplicateCropCyclesContext.setPracticedSystemClone(duplicatedPracticedSystem);
        return duplicatedPracticedSystem;
    }

    protected PracticedDuplicateCropCyclesContext getDuplicateCropCyclesContext(
            PracticedSystem fromPracticedSystem,
            GrowingSystem targetedGrowingSystem) {

        final Domain targetedDomain = targetedGrowingSystem.getGrowingPlan().getDomain();
        Integer targetedCampaign = targetedDomain.getCampaign();
        String targetedDomainCode = targetedDomain.getCode();

        Map<String, List<CroppingPlanSpecies>> fromSpeciesByCropCodes = domainService.getCroppingPlanSpeciesForDomainAndCampaignsByCropCode(targetedDomain.getCode(), getIntCampaigns(fromPracticedSystem.getCampaigns()));
        Map<String, List<CroppingPlanSpecies>> toSpeciesByCropCodes = domainService.getCroppingPlanSpeciesForDomainAndCampaignsByCropCode(targetedDomain.getCode(), Collections.singleton(targetedCampaign));
        List<String> targetedCroppingPlanCodes = getTargetedCroppingPlanCodes(targetedDomain, targetedCampaign, targetedDomainCode);

        Map<String, Map<String, String>> fromSpeciesCodeToSpeciesCodeForCropCode = new HashMap<>();
        Map<String, Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>>> toSpeciesByCropCode = new HashMap<>();

        for (String targetedCroppingPlanCode : targetedCroppingPlanCodes) {
            List<CroppingPlanSpecies> fromCroppingPlanSpecies = fromSpeciesByCropCodes.get(targetedCroppingPlanCode);
            List<CroppingPlanSpecies> toCroppingPlanSpecies = toSpeciesByCropCodes.get(targetedCroppingPlanCode);

            if (CollectionUtils.isNotEmpty(fromCroppingPlanSpecies) && CollectionUtils.isNotEmpty(toCroppingPlanSpecies)) {

                Map<String, CroppingPlanSpecies> fromSpeciesByRefEspeceIds = new HashMap<>();
                for (CroppingPlanSpecies fromSpecies : fromCroppingPlanSpecies) {
                    String refEspeceVarieteId = fromSpecies.getSpecies().getTopiaId();
                    refEspeceVarieteId += fromSpecies.getVariety() == null ? "" : fromSpecies.getVariety().getTopiaId();
                    fromSpeciesByRefEspeceIds.put(refEspeceVarieteId, fromSpecies);
                }

                for (CroppingPlanSpecies toSpecies : toCroppingPlanSpecies) {
                    String refEspeceVarieteId = toSpecies.getSpecies().getTopiaId();
                    refEspeceVarieteId += toSpecies.getVariety() == null ? "" : toSpecies.getVariety().getTopiaId();
                    CroppingPlanSpecies fromSpecies = fromSpeciesByRefEspeceIds.get(refEspeceVarieteId);
                    if (fromSpecies != null) {
                        Map<String, String> fromSpeciesCodeToSpeciesCode = fromSpeciesCodeToSpeciesCodeForCropCode.computeIfAbsent(targetedCroppingPlanCode, k -> new HashMap<>());
                        fromSpeciesCodeToSpeciesCode.put(fromSpecies.getCode(), toSpecies.getCode());

                        Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>> cropToSpeciesByCode = toSpeciesByCropCode.computeIfAbsent(targetedCroppingPlanCode, k -> Pair.of(toSpecies.getCroppingPlanEntry(), new HashMap<>()));
                        final Map<String, CroppingPlanSpecies> cpsByCodes = cropToSpeciesByCode.getRight();
                        cpsByCodes.put(toSpecies.getCode(), toSpecies);
                    }
                }

            }
        }

        List<String> toolsCouplingsCodes = getToolsCouplingsCodesFromDomainAndCampaigns(targetedDomainCode, targetedCampaign);
        // context de duplication pour l'ensemble de la graphe "system synthétisé"
        PracticedDuplicateCropCyclesContext duplicateContext = new PracticedDuplicateCropCyclesContext();

        Map<String, AbstractDomainInputStockUnit> setDomainInputStockUnitByCodes = domainInputStockUnitService.loadDomainInputStockByCodes(targetedDomain);
        duplicateContext.setFromSpeciesCodeToSpeciesCodeForCropIdentifier(fromSpeciesCodeToSpeciesCodeForCropCode);
        duplicateContext.setToSpeciesByCropCode(toSpeciesByCropCode);
        duplicateContext.setToolsCouplingsCode(toolsCouplingsCodes);
        duplicateContext.setPracticedSystem(fromPracticedSystem);
        duplicateContext.setTargetedSystemCropCodes(targetedCroppingPlanCodes);
        duplicateContext.setTargetedCampaign(targetedCampaign);
        duplicateContext.setToDomainName(targetedDomain.getName());
        duplicateContext.setToDomainCode(targetedDomain.getCode());
        duplicateContext.setToGrowingSystem(targetedGrowingSystem);
        duplicateContext.setDomainInputStockUnitByCodes(new HashMap<>(setDomainInputStockUnitByCodes));
        return duplicateContext;
    }

    protected void duplicatePerennialCropCycles(
            PracticedSystem fromPracticedSystem,
            PracticedSystem practicedSystemClone,
            PracticedDuplicateCropCyclesContext duplicateContext) {

        Collection<PracticedPerennialCropCycle> fromPerennialCropCycles = practicedPerennialCropCycleDao.forPracticedSystemEquals(fromPracticedSystem).findAll();
        Binder<PracticedPerennialCropCycle, PracticedPerennialCropCycle> perennialCropCycleBinder = BinderFactory.newBinder(PracticedPerennialCropCycle.class);
        for (PracticedPerennialCropCycle fromPerennialCropCycle : fromPerennialCropCycles) {
            if (duplicateContext.getTargetedSystemCropCodes().contains(fromPerennialCropCycle.getCroppingPlanEntryCode())) {

                PracticedPerennialCropCycle duplicatedPerennialCropCycle = practicedPerennialCropCycleDao.newInstance();
                perennialCropCycleBinder.copyExcluding(fromPerennialCropCycle, duplicatedPerennialCropCycle,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM,
                        PracticedPerennialCropCycle.PROPERTY_CROP_CYCLE_PHASES,
                        PracticedPerennialCropCycle.PROPERTY_PRACTICED_CROP_CYCLE_SPECIES);
                duplicatedPerennialCropCycle.setPracticedSystem(practicedSystemClone);

                practicedPerennialCropCycleDao.create(duplicatedPerennialCropCycle);

                // copy phases
                Collection<PracticedCropCyclePhase> fromPhases = fromPerennialCropCycle.getCropCyclePhases();
                if (fromPhases != null) {
                    Binder<PracticedCropCyclePhase, PracticedCropCyclePhase> phaseBinder = BinderFactory.newBinder(PracticedCropCyclePhase.class);
                    for (PracticedCropCyclePhase fromPhase : fromPhases) {
                        PracticedCropCyclePhase duplicatedPhase = practicedCropCyclePhaseDao.newInstance();
                        phaseBinder.copyExcluding(fromPhase, duplicatedPhase,
                                TopiaEntity.PROPERTY_TOPIA_ID,
                                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                TopiaEntity.PROPERTY_TOPIA_VERSION,
                                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE);
                        duplicatedPerennialCropCycle.addCropCyclePhases(duplicatedPhase);

                        // save phase
                        duplicatedPhase = practicedCropCyclePhaseDao.create(duplicatedPhase);

                        // continue to intervention copy
                        String tagetedCrop = fromPerennialCropCycle.getCroppingPlanEntryCode(); // same as from one
                        duplicateInterventions(
                                tagetedCrop,
                                fromPhase,
                                duplicatedPhase,
                                null,
                                null,
                                duplicateContext);
                    }
                }

                // copy species
                Collection<PracticedCropCycleSpecies> fromPerennialCropCycleSpecies = fromPerennialCropCycle.getPracticedCropCycleSpecies();
                if (
                        fromPerennialCropCycleSpecies != null &&
                                duplicateContext.getToSpeciesByCropCode() != null &&
                                duplicateContext.getToSpeciesByCropCode().get(fromPerennialCropCycle.getCroppingPlanEntryCode()) != null) {

                    Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>> pair = duplicateContext.getToSpeciesByCropCode().get(fromPerennialCropCycle.getCroppingPlanEntryCode());
                    Map<String, CroppingPlanSpecies> speciesByCode = pair.getValue();

                    Binder<PracticedCropCycleSpecies, PracticedCropCycleSpecies> speciesBinder = BinderFactory.newBinder(PracticedCropCycleSpecies.class);
                    for (PracticedCropCycleSpecies fromSpecies : fromPerennialCropCycleSpecies) {

                        if (speciesByCode != null && speciesByCode.containsKey(fromSpecies.getCroppingPlanSpeciesCode())) {
                            PracticedCropCycleSpecies duplicatedSpecies = practicedCropCycleSpeciesDao.newInstance();
                            speciesBinder.copyExcluding(fromSpecies, duplicatedSpecies,
                                    TopiaEntity.PROPERTY_TOPIA_ID,
                                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                    TopiaEntity.PROPERTY_TOPIA_VERSION);
                            duplicatedPerennialCropCycle.addPracticedCropCycleSpecies(duplicatedSpecies);
                        }
                    }
                }

                practicedPerennialCropCycleDao.update(duplicatedPerennialCropCycle);
            } else {
                CroppingPlanEntry crop = croppingPlanEntryDao.forCodeEquals(fromPerennialCropCycle.getCroppingPlanEntryCode()).findAnyOrNull();

                throw new AgrosystDuplicationException(String.format(
                        "La culture p&eacute;renne %s n'est pas pr&eacute;sente sur le domaine %s (%s).</br>" +
                                "La duplication de votre synth&eacute;tis&eacute; est impossible",
                        crop != null ? crop.getName() : "?",
                        duplicateContext.getToDomainName(),
                        duplicateContext.getTargetedCampaign()));
            }
        }
    }

    protected void duplicateSeasonalCycles(
            PracticedSystem fromPracticedSystem,
            PracticedSystem practicedSystemClone,
            PracticedDuplicateCropCyclesContext duplicateContext) {

        Collection<PracticedSeasonalCropCycle> fromSeasonalCropCycles = practicedSeasonalCropCycleDao.forPracticedSystemEquals(fromPracticedSystem).findAll();
        Binder<PracticedSeasonalCropCycle, PracticedSeasonalCropCycle> seasonalCropCycleBinder = BinderFactory.newBinder(PracticedSeasonalCropCycle.class);

        for (PracticedSeasonalCropCycle fromSeasonalCropCycle : fromSeasonalCropCycles) {

            PracticedSeasonalCropCycle duplicatedSeasonalCycle = bindPracticedSeasonalCropCycle(seasonalCropCycleBinder, fromSeasonalCropCycle);
            duplicatedSeasonalCycle.setPracticedSystem(practicedSystemClone);

            Map<PracticedCropCycleNode, PracticedCropCycleNode> fromNodesToNodes = duplicateSeasonalNodes(fromSeasonalCropCycle, duplicatedSeasonalCycle, duplicateContext);

            practicedSeasonalCropCycleDao.create(duplicatedSeasonalCycle);

            duplicateSeasonalConnection(fromSeasonalCropCycle, fromNodesToNodes, duplicateContext);
        }
    }

    protected void duplicateSeasonalConnection(
            PracticedSeasonalCropCycle cycle, Map<PracticedCropCycleNode, PracticedCropCycleNode> nodeCache, PracticedDuplicateCropCyclesContext duplicateContext) {

        Collection<PracticedCropCycleConnection> fromSeasonalConnections = practicedCropCycleConnectionDao.findAllByCropCycle(cycle.getTopiaId());
        Binder<PracticedCropCycleConnection, PracticedCropCycleConnection> seasonalConnectionBinder = BinderFactory.newBinder(PracticedCropCycleConnection.class);
        for (PracticedCropCycleConnection fromSeasonalConnection : fromSeasonalConnections) {

            PracticedCropCycleConnection duplicatedConnection = bindPracticedCropCycleConnection(seasonalConnectionBinder, fromSeasonalConnection);
            duplicatedConnection.setSource(nodeCache.get(fromSeasonalConnection.getSource()));
            duplicatedConnection.setTarget(nodeCache.get(fromSeasonalConnection.getTarget()));
            setDuplicatedConnectionIntermediateCrop(duplicateContext, fromSeasonalConnection, duplicatedConnection);

            duplicatedConnection = practicedCropCycleConnectionDao.create(duplicatedConnection);

            duplicateInterventions(null, null, null, fromSeasonalConnection, duplicatedConnection, duplicateContext);

        }
    }

    protected void setDuplicatedConnectionIntermediateCrop(PracticedDuplicateCropCyclesContext duplicateContext,
                                                           PracticedCropCycleConnection seasonalConnection,
                                                           PracticedCropCycleConnection duplicatedConnection) {
        List<String> croppingPlanCodes = duplicateContext.getTargetedSystemCropCodes();
        if (seasonalConnection.getIntermediateCroppingPlanEntryCode() != null) {
            if (croppingPlanCodes.contains(seasonalConnection.getIntermediateCroppingPlanEntryCode())) {
                duplicatedConnection.setIntermediateCroppingPlanEntryCode(seasonalConnection.getIntermediateCroppingPlanEntryCode());
            } else {

                PracticedCropCycleNode node = seasonalConnection.getTarget();
                int rank = node.getRank() + 1;
                CroppingPlanEntry crop = croppingPlanEntryDao.forCodeEquals(seasonalConnection.getIntermediateCroppingPlanEntryCode()).findAnyOrNull();

                throw new AgrosystDuplicationException(String.format(
                        "La culture interm&eacute;diaire %s (rang cible %d) du sch&eacute;ma de rotation n'est pas pr&eacute;sente sur le domaine %s (%s).</br>" +
                                "La duplication de votre synth&eacute;tis&eacute; est impossible.</br>" +
                                "<strong>Vous pouvez utiliser le copier-coller des cultures depuis une autre campagne du m&ecirc;me domaine " +
                                "pour enregistrer la ou les culture(s) manquantes avant de refaire cette duplication.</strong>",
                        crop != null ? crop.getName() : "?",
                        rank,
                        duplicateContext.getToDomainName(),
                        duplicateContext.getTargetedCampaign()));
            }
        }
    }

    protected PracticedCropCycleConnection bindPracticedCropCycleConnection(Binder<PracticedCropCycleConnection, PracticedCropCycleConnection> seasonalConnectionBinder, PracticedCropCycleConnection seasonalConnection) {
        PracticedCropCycleConnection connectionClone = practicedCropCycleConnectionDao.newInstance();
        seasonalConnectionBinder.copyExcluding(seasonalConnection, connectionClone,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                PracticedCropCycleConnection.PROPERTY_SOURCE,
                PracticedCropCycleConnection.PROPERTY_TARGET,
                PracticedCropCycleConnection.PROPERTY_INTERMEDIATE_CROPPING_PLAN_ENTRY_CODE
        );
        return connectionClone;
    }

    protected Map<PracticedCropCycleNode, PracticedCropCycleNode> duplicateSeasonalNodes(
            PracticedSeasonalCropCycle fromSeasonalCycle, PracticedSeasonalCropCycle toSeasonalCycle,
            PracticedDuplicateCropCyclesContext duplicateContext) {
        Map<PracticedCropCycleNode, PracticedCropCycleNode> fromNodeToNode = new HashMap<>();

        Collection<PracticedCropCycleNode> fromSeasonalNodes = fromSeasonalCycle.getCropCycleNodes();

        for (PracticedCropCycleNode fromSeasonalNode : fromSeasonalNodes) {

            if (duplicateContext.getTargetedSystemCropCodes().contains(fromSeasonalNode.getCroppingPlanEntryCode())) {

                PracticedCropCycleNode duplicatedNode = practicedCropCycleNodeDao.newInstance();
                SEASONAL_NODE_BINDER.copyExcluding(fromSeasonalNode, duplicatedNode,
                        TopiaEntity.PROPERTY_TOPIA_ID,
                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                        TopiaEntity.PROPERTY_TOPIA_VERSION,
                        PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE);

                duplicatedNode.setPracticedSeasonalCropCycle(toSeasonalCycle);
                toSeasonalCycle.addCropCycleNodes(duplicatedNode);

                // add to cache to link connection to cloned instance
                fromNodeToNode.put(fromSeasonalNode, duplicatedNode);
            } else {
                String fromCropCode = fromSeasonalNode.getCroppingPlanEntryCode();
                int rank = fromSeasonalNode.getRank() + 1;
                CroppingPlanEntry crop = croppingPlanEntryDao.forCodeEquals(fromCropCode).findAnyOrNull();

                throw new AgrosystDuplicationException(String.format(
                        "La culture %s (rang %d) du sch&eacute;ma de rotation n'est pas pr&eacute;sente sur le domaine %s (%s).</br>" +
                                "La duplication de votre synth&eacute;tis&eacute; est impossible.<br>" +
                                "<strong>Vous pouvez utiliser le copier-coller des cultures depuis une autre campagne du m&ecirc;me domaine " +
                                "pour enregistrer la ou les culture(s) manquantes avant de refaire cette duplication.</strong>",
                        crop != null ? crop.getName() : "?",
                        rank,
                        duplicateContext.getToDomainName(),
                        duplicateContext.getTargetedCampaign()));
            }
        }
        return fromNodeToNode;
    }

    protected PracticedSeasonalCropCycle bindPracticedSeasonalCropCycle(Binder<PracticedSeasonalCropCycle, PracticedSeasonalCropCycle> seasonalCropCycleBinder, PracticedSeasonalCropCycle cycle) {
        PracticedSeasonalCropCycle duplicatedSeasonalCycle = practicedSeasonalCropCycleDao.newInstance();
        seasonalCropCycleBinder.copyExcluding(cycle, duplicatedSeasonalCycle,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM,
                PracticedSeasonalCropCycle.PROPERTY_CROP_CYCLE_NODES);
        return duplicatedSeasonalCycle;
    }

    public void duplicatePracticedSystemPlot(PracticedSystem practicedSystem, PracticedSystem practicedSystemClone) {
        // clone practiced plots
        Collection<PracticedPlot> practicedPlots = practicedPlotDao.forPracticedSystemContains(practicedSystem).findAll();
        Binder<PracticedPlot, PracticedPlot> practicedPlotBinder = BinderFactory.newBinder(PracticedPlot.class);
        for (PracticedPlot practicedPlot : practicedPlots) {
            PracticedPlot plotClone = practicedPlotDao.newInstance();
            practicedPlotBinder.copyExcluding(practicedPlot, plotClone,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                    PracticedPlot.PROPERTY_PRACTICED_SYSTEM,
                    PracticedPlot.PROPERTY_SOL_HORIZON,
                    PracticedPlot.PROPERTY_PLOT_ZONINGS,
                    PracticedPlot.PROPERTY_ADJACENT_ELEMENTS);
            // TODO kmorin 20221121 check si practicedPlot AGS ou IpmWorks ?
            plotClone.setPracticedSystem(Collections.singleton(practicedSystemClone));

            // clone sol horizon
            if (practicedPlot.getSolHorizon() != null) {
                Binder<SolHorizon, SolHorizon> binderSH = BinderFactory.newBinder(SolHorizon.class);
                for (SolHorizon solHorizon : practicedPlot.getSolHorizon()) {
                    SolHorizon solHorizonClone = solHorizonDao.newInstance();
                    binderSH.copyExcluding(solHorizon, solHorizonClone,
                            TopiaEntity.PROPERTY_TOPIA_ID,
                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                            TopiaEntity.PROPERTY_TOPIA_VERSION);
                    plotClone.addSolHorizon(solHorizonClone);
                }
            }

            // force collection copy instead of collection reference copy
            if (practicedPlot.getPlotZonings() != null) {
                plotClone.setPlotZonings(new ArrayList<>(practicedPlot.getPlotZonings()));
            }
            if (practicedPlot.getAdjacentElements() != null) {
                plotClone.setAdjacentElements(new ArrayList<>(practicedPlot.getAdjacentElements()));
            }

            practicedPlotDao.create(plotClone);
        }
    }

    @Override
    public void inactivatePracticedSystem(List<String> practicedSystemIds, boolean activate) {
        if (practicedSystemIds != null && !practicedSystemIds.isEmpty()) {

            for (String practicedSystemId : practicedSystemIds) {
                PracticedSystem practicedSystem = practicedSystemDao.forTopiaIdEquals(practicedSystemId).findUnique();
                practicedSystem.setActive(activate);
                practicedSystemDao.update(practicedSystem);
            }
            getTransaction().commit();
        }
    }

    /**
     * Duplicate phase or connection interventions.
     *
     * @param phaseTargetedCropCode targeted phase crop code
     * @param phase                 phase to get intervention
     * @param phaseClone            phase to clone intervention to
     * @param connection            connection to get intervention
     * @param connectionClone       connection to clone intervention to
     * @param duplicateContext      duplicate context
     */
    protected void duplicateInterventions(
            String phaseTargetedCropCode,
            PracticedCropCyclePhase phase,
            PracticedCropCyclePhase phaseClone,
            PracticedCropCycleConnection connection,
            PracticedCropCycleConnection connectionClone,
            PracticedDuplicateCropCyclesContext duplicateContext) {

        Preconditions.checkArgument(phase != null ^ connection != null);
        Preconditions.checkArgument(phaseClone != null ^ connectionClone != null);

        Collection<PracticedIntervention> practicedInterventions = getPracticedSystemInterventions(phase, connection);

        Binder<PracticedIntervention, PracticedIntervention> interventionBinder = BinderFactory.newBinder(PracticedIntervention.class);
        for (PracticedIntervention practicedIntervention : practicedInterventions) {

            PracticedIntervention interventionClone = bindPracticedIntervention(interventionBinder, practicedIntervention);

            Boolean intermediateStatusChange = setInterventionIntermediateStatus(phaseClone, connectionClone, interventionClone);
            duplicateContext.setIntermediateStatusChange(intermediateStatusChange);

            addToolsCouplingCodesToIntervention(duplicateContext, practicedIntervention, interventionClone);

            duplicateContext.setTargetedCropCode(getInterventionCropTargetCode(phaseTargetedCropCode, phaseClone, connectionClone, interventionClone));
            addSpeciesStadesToIntervention(duplicateContext, practicedIntervention, interventionClone);

            // save intervention
            interventionClone = practicedInterventionDao.create(interventionClone);

            // duplication des actions liées à l'intervention
            if (duplicateContext.isWithInputUsage()) {
                actionService.duplicatePracticedActionsAndUsage(duplicateContext, practicedIntervention, interventionClone);
            }
        }
    }

    protected Collection<PracticedIntervention> getPracticedSystemInterventions(PracticedCropCyclePhase phase, PracticedCropCycleConnection connection) {
        Collection<PracticedIntervention> practicedInterventions;
        if (phase != null) {
            practicedInterventions = practicedInterventionDao.forPracticedCropCyclePhaseEquals(phase).findAll();
        } else {
            practicedInterventions = practicedInterventionDao.forPracticedCropCycleConnectionEquals(connection).findAll();
        }
        return practicedInterventions;
    }

    protected void addSpeciesStadesToIntervention(
            PracticedDuplicateCropCyclesContext duplicateContext,
            PracticedIntervention practicedIntervention,
            PracticedIntervention interventionClone) {

        Map<String, Map<String, String>> targetedSpeciesByTargtedCrops = duplicateContext.getFromSpeciesCodeToSpeciesCodeForCropIdentifier();
        Map<String, String> fromSpeciesCodeToSpeciesCodes = targetedSpeciesByTargtedCrops == null ? null : targetedSpeciesByTargtedCrops.get(duplicateContext.getTargetedCropCode());

        if (fromSpeciesCodeToSpeciesCodes != null) {

            if (!duplicateContext.getIntermediateStatusChange()) {
                if (practicedIntervention.getSpeciesStades() != null) {
                    Collection<PracticedSpeciesStade> originalSpeciesStades = practicedIntervention.getSpeciesStades();
                    Map<String, PracticedSpeciesStade> originalSpeciesStadesBySpeciesCode = Maps.uniqueIndex(originalSpeciesStades, GET_PRACTICED_SPECIES_STADE_CODE::apply);

                    for (Map.Entry<String, String> fromSpeciesCodeToSpeciesCodeEntry : fromSpeciesCodeToSpeciesCodes.entrySet()) {
                        String fromSpeciesCodeToSpeciesCode = fromSpeciesCodeToSpeciesCodeEntry.getKey();
                        PracticedSpeciesStade originalSpeciesStade = originalSpeciesStadesBySpeciesCode.get(fromSpeciesCodeToSpeciesCode);
                        PracticedSpeciesStade clonedSpeciesStade;
                        if (originalSpeciesStade != null) {
                            clonedSpeciesStade = practicedSpeciesStadeDao.newInstance();
                            PRACTICED_SPECIES_STADE_BINDER.copyExcluding(originalSpeciesStade, clonedSpeciesStade,
                                    TopiaEntity.PROPERTY_TOPIA_ID,
                                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                    TopiaEntity.PROPERTY_TOPIA_VERSION);
                            clonedSpeciesStade.setSpeciesCode(fromSpeciesCodeToSpeciesCodeEntry.getValue());
                            interventionClone.addSpeciesStades(clonedSpeciesStade);
                        }
                    }
                }
            } else {
                for (String croppingPlanSpeciesCode : fromSpeciesCodeToSpeciesCodes.values()) {
                    PracticedSpeciesStade newSpeciesStade = practicedSpeciesStadeDao.newInstance();
                    newSpeciesStade.setSpeciesCode(croppingPlanSpeciesCode);
                    interventionClone.addSpeciesStades(newSpeciesStade);
                }
            }
        }


    }

    protected void addToolsCouplingCodesToIntervention(PracticedDuplicateCropCyclesContext duplicateContext, PracticedIntervention practicedIntervention, PracticedIntervention interventionClone) {
        Collection<String> practicedSystemToolsCouplingsCodes = duplicateContext.getToolsCouplingsCode();
        if (practicedIntervention.getToolsCouplingCodes() != null) {
            List<String> interventionToolsCouplingCodes = new ArrayList<>();
            for (String toolsCouplingCode : practicedIntervention.getToolsCouplingCodes()) {
                if (practicedSystemToolsCouplingsCodes != null && practicedSystemToolsCouplingsCodes.contains(toolsCouplingCode)) {
                    interventionToolsCouplingCodes.add(toolsCouplingCode);
                }
            }
            interventionClone.setToolsCouplingCodes(interventionToolsCouplingCodes);
        }
    }

    protected PracticedIntervention bindPracticedIntervention(Binder<PracticedIntervention, PracticedIntervention> interventionBinder, PracticedIntervention practicedIntervention) {
        PracticedIntervention interventionClone = practicedInterventionDao.newInstance();
        interventionBinder.copyExcluding(practicedIntervention, interventionClone,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION,
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE,
                PracticedIntervention.PROPERTY_SPECIES_STADES,
                PracticedIntervention.PROPERTY_TOOLS_COUPLING_CODES);
        return interventionClone;
    }

    protected Boolean setInterventionIntermediateStatus(PracticedCropCyclePhase phaseClone, PracticedCropCycleConnection connectionClone, PracticedIntervention interventionClone) {
        boolean isChange = false;
        if (phaseClone != null) {
            interventionClone.setPracticedCropCyclePhase(phaseClone);
        } else {
            if (interventionClone.isIntermediateCrop()) {
                if (connectionClone.getIntermediateCroppingPlanEntryCode() != null) {
                    interventionClone.setIntermediateCrop(true);
                } else {
                    interventionClone.setIntermediateCrop(false);
                    isChange = true;
                }
            }
            interventionClone.setPracticedCropCycleConnection(connectionClone);
        }
        return isChange;
    }

    protected String getInterventionCropTargetCode(String phaseSourceCropCode, PracticedCropCyclePhase phaseClone, PracticedCropCycleConnection connectionClone, PracticedIntervention interventionClone) {
        String cropCode;
        if (phaseClone != null) {
            cropCode = phaseSourceCropCode;
        } else {
            if (connectionClone.getIntermediateCroppingPlanEntryCode() != null && interventionClone.isIntermediateCrop()) {
                cropCode = connectionClone.getIntermediateCroppingPlanEntryCode();
            } else {
                cropCode = connectionClone.getTarget().getCroppingPlanEntryCode();
            }
        }
        return cropCode;
    }

    @Override
    public String getDomainCode(String growingSystemId) {
        return domainService.getDomainCodeForGrowingSystem(growingSystemId);
    }

    @Override
    public List<PracticedSystem> getAllValidatedForGivenIds(List<String> practicedSystemIds) {
        return practicedSystemDao.forTopiaIdIn(practicedSystemIds).addEquals(PracticedSystem.PROPERTY_VALIDATED, true).findAll();
    }

    @Override
    public List<PracticedSystem> getAllUnValidatedForGivenIds(List<String> practicedSystemIds) {
        return practicedSystemDao.forTopiaIdIn(practicedSystemIds).addEquals(PracticedSystem.PROPERTY_VALIDATED, false).findAll();
    }

    @Override
    public List<OperationStatus> validate(List<String> practicedSystemIds, boolean validate) {

        List<OperationStatus> result = new ArrayList<>();

        List<PracticedSystem> practicedSystemsToChangeValidationStatus = validate ? getAllUnValidatedForGivenIds(practicedSystemIds) : getAllValidatedForGivenIds(practicedSystemIds);

        for (PracticedSystem practicedSystem : practicedSystemsToChangeValidationStatus) {

            String practicedSystemName = practicedSystem.getName();
            String growingSystemName = practicedSystem.getGrowingSystem().getName();
            String practicedSystemCampaigns = practicedSystem.getCampaigns();

            OperationStatus status = new OperationStatus();
            result.add(status);
            try {
                authorizationService.checkValidatePracticedSystem(practicedSystem.getTopiaId());

                // AThimel 09/12/13 Perform DB update is more powerful
                practicedSystemDao.validatePracticedSystem(practicedSystem.getTopiaId(), context.getCurrentTime(), validate);

                status.success(String.format(
                        "Système synth&eacute;tis&eacute; '%s' du système de culture '%s' %s pour les campagnes '%s'", practicedSystemName, growingSystemName,
                        validate ? "valid&eacute;" : "d&eacute;valid&eacute;", practicedSystemCampaigns));

            } catch (AgrosystAccessDeniedException deniedException) {
                status.failed(String.format("Pour le système synth&eacute;tis&eacute; '%s' du système de culture '%s' et pour les campagnes '%s', l'erreur suivant est survenue: %s",
                        practicedSystemName, practicedSystemCampaigns, growingSystemName,
                        "Droits insuffisants pour modifier l'&eacute;tat de validation du système synth&eacute;tis&eacute;"));
            }
        }

        getTransaction().commit();

        // AThimel 09/12/13 The next line makes sure that cache state is synchronized with database state
        getPersistenceContext().getHibernateSupport().getHibernateSession().clear();

        return result;
    }

    @Override
    public ExportResult exportPracticedSystemsAsXls(Collection<String> practicedSystemIds) {
        return practicedExportXls.exportPracticedSystemsAsXlsStream(practicedSystemIds);
    }

    @Override
    public void exportPracticedSystemsAsXlsAsync(Collection<String> practicedSystemIds) {
        AuthenticatedUser user = getAuthenticatedUser();

        PracticedSystemExportTask exportTask = new PracticedSystemExportTask(user.getTopiaId(), user.getEmail(), practicedSystemIds);
        getBusinessTaskManager().schedule(exportTask);
    }

    @Override
    public List<GrowingSystem> getGrowingSystemsForNewPracticedSystem(GrowingSystemFilter growingSystemFilter, NavigationContext navigationContext) {
        growingSystemFilter.setNavigationContext(navigationContext);
        growingSystemFilter.setActive(Boolean.TRUE);
        growingSystemFilter.setActiveParents(Boolean.TRUE);
        growingSystemFilter.setAllPageSize();
        PaginationResult<GrowingSystem> results = growingSystemService.getFilteredGrowingSystems(growingSystemFilter);
        List<GrowingSystem> elements = results.getElements();
        elements.sort(Comparator.comparing(GrowingSystem::getName)
                .thenComparing(growingSystem -> growingSystem.getGrowingPlan().getDomain().getCampaign()));
        return elements;
    }

    @Override
    public boolean isActivated(PracticedSystem practicedSystem) {
        return practicedSystem.isActive() && practicedSystemDao.isActive(practicedSystem);
    }

    @Override
    public boolean areActivated(Collection<PracticedSystem> practicedSystems) {
        return practicedSystems.stream().allMatch(PracticedSystem::isActive) && practicedSystemDao.areActive(practicedSystems);
    }

    @Override
    public Collection<CattleDto> getAllCampaignsGrowingSystemRelatedCattles(String growingSystemId, Set<Integer> campaigns) {
        Language language = getSecurityContext().getLanguage();
        Domain domain = growingSystemDao.forTopiaIdEquals(growingSystemId).findUnique().getGrowingPlan().getDomain();
        List<LivestockUnit> allLivestocks = livestockUnitDao.forAllDomainCampaigns(domain.getCode(), campaigns, language);

        Set<CattleDto> allCattle = new HashSet<>();
        for (LivestockUnit livestockUnit : allLivestocks) {
            Collection<Cattle> cattles = livestockUnit.getCattles();
            for (Cattle cattle : cattles) {
                CattleDto cattleDto = new CattleDto(cattle, livestockUnit.getRefAnimalType());
                allCattle.add(cattleDto);
            }
        }

        return allCattle;
    }

    @Override
    public Map<String, DomainInputDto> getDomainInputs(String growingSystemId, String campaigns) {
        Domain domain = growingSystemDao.forTopiaIdEquals(growingSystemId).findUnique().getGrowingPlan().getDomain();
        Map<String, DomainInputDto> domainInputStockUnitByCodes = domainInputStockUnitService.loadDomainInputStockDtoByCodes(domain.getTopiaId());
        return domainInputStockUnitByCodes;
    }

    @Override
    public List<Integer> getGrowingSystemCampaignsFromId(String practicedSystemTopiaId, NavigationContext navigationContext) {
        Optional<PracticedSystem> practicedSystemOptional = practicedSystemDao.forTopiaIdEquals(practicedSystemTopiaId).tryFindAny();
        if (practicedSystemOptional.isPresent()) {
            return growingSystemService.getGrowingSystemCampaignsFromId(practicedSystemOptional.get().getGrowingSystem().getTopiaId(), navigationContext);
        }
        return List.of();
    }

    @Override
    public Collection<CattleDto> getAllCampaignsPracticedSystemRelatedCattles(String practicedSystemTopiaId, LinkedHashSet<Integer> campaigns) {
        Optional<PracticedSystem> practicedSystemOptional = StringUtils.isBlank(practicedSystemTopiaId) ? Optional.absent() : practicedSystemDao.forTopiaIdEquals(practicedSystemTopiaId).tryFindAny();
        if (practicedSystemOptional.isPresent()) {
            return getAllCampaignsGrowingSystemRelatedCattles(practicedSystemOptional.get().getGrowingSystem().getTopiaId(), campaigns);
        }
        return List.of();
    }

}
