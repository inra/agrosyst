package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefSeedUnits;
import fr.inra.agrosyst.api.entities.referential.RefSeedUnitsImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefSeedUnitsModel extends AbstractAgrosystModel<RefSeedUnits> implements ExportModel<RefSeedUnits> {

    public RefSeedUnitsModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("code_espece_bota", RefSeedUnits.PROPERTY_CODE_ESPECE_BOTANIQUE);
        newMandatoryColumn("libelle_espece_bota", RefSeedUnits.PROPERTY_LIBELLE_ESPECE_BOTANIQUE);
        newMandatoryColumn("code_qualifiant_aee", RefSeedUnits.PROPERTY_CODE_QUALIFIANT__AEE);
        newMandatoryColumn("libelle_qualifiant_aee", RefSeedUnits.PROPERTY_LIBELLE_QUALIFIANT__AEE);
        newMandatoryColumn("unite_semis", RefSeedUnits.PROPERTY_SEED_PLANT_UNIT, SEED_PLANT_UNIT_VALUE_UNIT_PARSER);
        newMandatoryColumn("source", RefSeedUnits.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefSeedUnits.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }
    
    @Override
    public Iterable<ExportableColumn<RefSeedUnits, Object>> getColumnsForExport() {
        ModelBuilder<RefEspece> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("code_espece_bota", RefSeedUnits.PROPERTY_CODE_ESPECE_BOTANIQUE);
        modelBuilder.newColumnForExport("libelle_espece_bota", RefSeedUnits.PROPERTY_LIBELLE_ESPECE_BOTANIQUE);
        modelBuilder.newColumnForExport("code_qualifiant_aee", RefSeedUnits.PROPERTY_CODE_QUALIFIANT__AEE);
        modelBuilder.newColumnForExport("libelle_qualifiant_aee", RefSeedUnits.PROPERTY_LIBELLE_QUALIFIANT__AEE);
        modelBuilder.newColumnForExport("unite_semis", RefSeedUnits.PROPERTY_SEED_PLANT_UNIT, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("source", RefSeedUnits.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefSeedUnits.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefSeedUnits newEmptyInstance() {
        RefSeedUnits result = new RefSeedUnitsImpl();
        result.setActive(true);
        return result;
    }
}
