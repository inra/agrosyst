package fr.inra.agrosyst.services.async;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.services.async.Task;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class ScheduledTask<T extends Task> {

    protected final T task;

    protected final LocalDateTime scheduledAt;

    protected Optional<LocalDateTime> startedAt = Optional.empty();

    protected Optional<LocalDateTime> finishedAt = Optional.empty();

    protected transient Optional<Thread> runningInThread = java.util.Optional.empty();

    public ScheduledTask(T task, LocalDateTime scheduledAt) {
        Preconditions.checkArgument(task != null);
        Preconditions.checkArgument(scheduledAt != null);
        this.task = task;
        this.scheduledAt = scheduledAt;
    }

    public T getTask() {
        return task;
    }

    public LocalDateTime getScheduledAt() {
        return scheduledAt;
    }

    public Optional<LocalDateTime> getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(LocalDateTime startedAt) {
        this.startedAt = Optional.of(startedAt);
    }

    public Optional<LocalDateTime> getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(LocalDateTime finishedAt) {
        this.finishedAt = Optional.of(finishedAt);
    }

    public void setRunningInThread(Thread thread) {
        this.runningInThread = java.util.Optional.of(thread);
    }
    
    public java.util.Optional<Thread> getRunningInThread() {
        return runningInThread;
    }
    
    public boolean isVisible() {
        return getTask().isVisible();
    }

    @Override
    public String toString() {
        
        final String startedAt = this.startedAt.map(localDateTime -> DateTimeFormatter.ofPattern("dd MMMM yyyy à HH:mm").format(localDateTime))
                .orElse("not started");
        final String endedAt = finishedAt.map(localDateTime -> DateTimeFormatter.ofPattern("dd MMMM yyyy à HH:mm").format(localDateTime))
                .orElse("not ended");
        
        return String.format("Task : '%s', started at %s ended at %s",
                task.getTaskId(),
                startedAt,
                endedAt);
    }
    
}
