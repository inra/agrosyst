package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.referential.CriteriaUnit;
import fr.inra.agrosyst.api.entities.referential.QualityAttributeType;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaImpl;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

public class RefQualityCriteriaModel extends AbstractDestinationAndPriceModel<RefQualityCriteria> implements ExportModel<RefQualityCriteria> {

    protected static final String MM = "mm";
    protected static final String G = "g";
    protected static final String G_L = "g/L";
    protected static final String POURC = "%";
    protected static final String DEG = "°";
    protected static final String KG_HL = "kg/hL";
    protected static final String G_H2SO4_L = "g H2SO4/L";
    protected static final String S_U = "S.U.";
    protected static final String S = "s";
    protected static final String POURC_BRIX = "% Brix";
    protected static final String MICRO_G_KG = "µg/kg";
    protected static final String KG_CATEG_DIAM_SURF = "kg/catégorie de diamètre/surface";
    protected static final String POURC_POID_CAROTTE_COM = "% poids des carottes commercialisables";
    protected static final String T_HA = "t/ha";
    protected static final String KG_CATEG_DECHET_SURFACE = "kg/catégorie de déchet/surface";
    protected static final String POURC_POID_DECHETS = "% poids des déchets";
    protected static final String DUROFEL_0_5 = "Durofel 0,5";
    protected static final String ML_NAOH = "ml NaOH";
    protected static final String G_PLANT = "g/plant";
    protected static final String KG_M2 = "kg/m²";
    protected static final String POURC_FRUIT_MACHES_POURRIS = "% fruits mâchés ou pourris";
    protected static final String DUROFEL_1 = "Durofel 1";
    protected static final String POUR_TOTAL = "% du total";
    protected static final String KG_HA = "kg/ha";
    protected static final String DUROFEL = "Durofel";
    protected static final String MG_100G_MS = "mg/100g MS";
    protected static final String POURC_POUR_CATEG_I = "% pour catégorie i";
    protected static final String MG_KG_MF = "mg/kg MF";
    protected static final String MG_KG_MS = "mg/kg MS";

    public RefQualityCriteriaModel() {
        super(CSV_SEPARATOR);
    }

    public RefQualityCriteriaModel(RefEspeceTopiaDao refEspeceDao) {
        super(CSV_SEPARATOR);
        super.refEspeceDao = refEspeceDao;
        getUpperCodeEspeceBotaniqueToCodeEspeceBotanique();

        newMandatoryColumn("Code_critères_de_qualité", RefQualityCriteria.PROPERTY_CODE);
        newMandatoryColumn("Filière", RefQualityCriteria.PROPERTY_SECTOR, RefDestinationModel.SECTOR_PARSER);
        newMandatoryColumn("code_espece_botanique", RefQualityCriteria.PROPERTY_CODE_ESPECE_BOTANIQUE, CODE_ESPECE_BOTANIQUE_PARSER);
        newMandatoryColumn("code_qualifiant_AEE", RefQualityCriteria.PROPERTY_CODE_QUALIFIANT__AEE, EMPTY_TO_NULL);
        newMandatoryColumn("Espèce", RefQualityCriteria.PROPERTY_ESPECE);
        newMandatoryColumn("Critères de qualité", RefQualityCriteria.PROPERTY_QUALITY_CRITERIA_LABEL);
        newMandatoryColumn("Type de variable", RefQualityCriteria.PROPERTY_QUALITY_ATTRIBUTE_TYPE, QUALITY_ATTRIBUTE_TYPE_VALUE_PARSER);
        newMandatoryColumn("Unité_critère", RefQualityCriteria.PROPERTY_CRITERIA_UNIT, CRITERIA_UNIT_PARSER);
        newMandatoryColumn("Valorisation_viticulture", RefDestination.PROPERTY_WINE_VALORISATION, VALORISATION_PARSER);
        newMandatoryColumn("Source", RefQualityCriteria.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefQualityCriteria.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefQualityCriteria, Object>> getColumnsForExport() {
        ModelBuilder<RefQualityCriteria> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Code_critères_de_qualité", RefQualityCriteria.PROPERTY_CODE);
        modelBuilder.newColumnForExport("Filière", RefQualityCriteria.PROPERTY_SECTOR, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("code_espece_botanique", RefQualityCriteria.PROPERTY_CODE_ESPECE_BOTANIQUE);
        modelBuilder.newColumnForExport("code_qualifiant_AEE", RefQualityCriteria.PROPERTY_CODE_QUALIFIANT__AEE);
        modelBuilder.newColumnForExport("Espèce", RefQualityCriteria.PROPERTY_ESPECE);
        modelBuilder.newColumnForExport("Critères de qualité", RefQualityCriteria.PROPERTY_QUALITY_CRITERIA_LABEL);
        modelBuilder.newColumnForExport("Type de variable", RefQualityCriteria.PROPERTY_QUALITY_ATTRIBUTE_TYPE, QUALITY_ATTRIBUTE_TYPE_VALUE_FORMATTER);
        modelBuilder.newColumnForExport("Unité_critère", RefQualityCriteria.PROPERTY_CRITERIA_UNIT, CRITERIA_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("Valorisation_viticulture", RefDestination.PROPERTY_WINE_VALORISATION, VALORISATION_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefQualityCriteria.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefQualityCriteria.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefQualityCriteria newEmptyInstance() {
        RefQualityCriteria result = new RefQualityCriteriaImpl();
        result.setActive(true);
        return result;
    }

    protected static final ValueParser<QualityAttributeType> QUALITY_ATTRIBUTE_TYPE_VALUE_PARSER = value -> {
        QualityAttributeType result = null;
        if (!Strings.isNullOrEmpty(value)) {
            result = (QualityAttributeType) getGenericEnumParser(QualityAttributeType.class, value);
        }
        return result;
    };

    protected static final ValueFormatter<QualityAttributeType> QUALITY_ATTRIBUTE_TYPE_VALUE_FORMATTER = value -> {
        String result;
        if (value != null) {
            result = value.name().toLowerCase();
        } else {
            result = "";
        }
        return result;
    };

    protected static final ValueParser<CriteriaUnit> CRITERIA_UNIT_PARSER = value -> {
        CriteriaUnit result = null;
        if (!Strings.isNullOrEmpty(value)) {
            String strValue = StringUtils.stripAccents(value);
            strValue = strValue.toUpperCase();
            if (strValue.equalsIgnoreCase(MM)) {
                result = CriteriaUnit.MM;
            } else if (strValue.equalsIgnoreCase(G)) {
                result = CriteriaUnit.G;
            } else if (strValue.equalsIgnoreCase(G_L)) {
                result = CriteriaUnit.G_L;
            } else if (strValue.equalsIgnoreCase(POURC)) {
                result = CriteriaUnit.POURC;
            } else if (strValue.equalsIgnoreCase(DEG)) {
                result = CriteriaUnit.DEG;
            } else if (strValue.equalsIgnoreCase(KG_HL)) {
                result = CriteriaUnit.KG_HL;
            } else if (strValue.equalsIgnoreCase(G_H2SO4_L)) {
                result = CriteriaUnit.G_H2SO4_L;
            } else if (strValue.equalsIgnoreCase(S_U)) {
                result = CriteriaUnit.S_U;
            } else if (strValue.equalsIgnoreCase(S)) {
                result = CriteriaUnit.S;
            } else if (strValue.equalsIgnoreCase(POURC_BRIX)) {
                result = CriteriaUnit.POURC_BRIX;
            } else if (strValue.equalsIgnoreCase(MICRO_G_KG)) {
                result = CriteriaUnit.MICRO_G_KG;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(KG_CATEG_DIAM_SURF))) {
                result = CriteriaUnit.KG_CATEG_DIAM_SURF;
            } else if (strValue.equalsIgnoreCase(POURC_POID_CAROTTE_COM)) {
                result = CriteriaUnit.POURC_POID_CAROTTE_COM;
            } else if (strValue.equalsIgnoreCase(T_HA)) {
                result = CriteriaUnit.T_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(KG_CATEG_DECHET_SURFACE))) {
                result = CriteriaUnit.KG_CATEG_DECHET_SURFACE;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(POURC_POID_DECHETS))) {
                result = CriteriaUnit.POURC_POID_DECHETS;
            } else if (strValue.equalsIgnoreCase(DUROFEL_0_5)) {
                result = CriteriaUnit.DUROFEL_0_5;
            } else if (strValue.equalsIgnoreCase(ML_NAOH)) {
                result = CriteriaUnit.ML_NAOH;
            } else if (strValue.equalsIgnoreCase(G_PLANT)) {
                result = CriteriaUnit.G_PLANT;
            } else if (strValue.equalsIgnoreCase(KG_M2)) {
                result = CriteriaUnit.KG_M2;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(POURC_FRUIT_MACHES_POURRIS))) {
                result = CriteriaUnit.POURC_FRUIT_MACHES_POURRIS;
            } else if (strValue.equalsIgnoreCase(DUROFEL_1)) {
                result = CriteriaUnit.DUROFEL_1;
            } else if (strValue.equalsIgnoreCase(POUR_TOTAL)) {
                result = CriteriaUnit.POUR_TOTAL;
            } else if (strValue.equalsIgnoreCase(KG_HA)) {
                result = CriteriaUnit.KG_HA;
            } else if (strValue.equalsIgnoreCase(DUROFEL)) {
                result = CriteriaUnit.DUROFEL;
            } else if (strValue.equalsIgnoreCase(MG_100G_MS)) {
                result = CriteriaUnit.MG_100G_MS;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(POURC_POUR_CATEG_I))) {
                result = CriteriaUnit.POURC_POUR_CATEG_I;
            } else if (strValue.equalsIgnoreCase(MG_KG_MF)) {
                result = CriteriaUnit.MG_KG_MF;
            } else if (strValue.equalsIgnoreCase(MG_KG_MS)) {
                result = CriteriaUnit.MG_KG_MS;
            } else {
                result = (CriteriaUnit) getGenericEnumParser(CriteriaUnit.class, strValue);
            }
        }
        return result;
    };

    protected static final ValueFormatter<CriteriaUnit> CRITERIA_UNIT_FORMATTER = value -> {
        String result;
        if (value != null) {
            result = switch (value) {
                case MM -> MM;
                case G -> G;
                case G_L -> G_L;
                case POURC -> POURC;
                case G_H2SO4_L -> G_H2SO4_L;
                case S_U -> S_U;
                case KG_HL -> KG_HL;
                case S -> S;
                case DEG -> DEG;
                case POURC_BRIX -> POURC_BRIX;
                case MICRO_G_KG -> MICRO_G_KG;
                case KG_CATEG_DIAM_SURF -> KG_CATEG_DIAM_SURF;
                case POURC_POID_CAROTTE_COM -> POURC_POID_CAROTTE_COM;
                case T_HA -> T_HA;
                case KG_CATEG_DECHET_SURFACE -> KG_CATEG_DECHET_SURFACE;
                case POURC_POID_DECHETS -> POURC_POID_DECHETS;
                case DUROFEL_0_5 -> DUROFEL_0_5;
                case ML_NAOH -> ML_NAOH;
                case G_PLANT -> G_PLANT;
                case KG_M2 -> KG_M2;
                case POURC_FRUIT_MACHES_POURRIS -> POURC_FRUIT_MACHES_POURRIS;
                case DUROFEL_1 -> DUROFEL_1;
                case POUR_TOTAL -> POUR_TOTAL;
                case KG_HA -> KG_HA;
                case DUROFEL -> DUROFEL;
                case MG_100G_MS -> MG_100G_MS;
                case POURC_POUR_CATEG_I -> POURC_POUR_CATEG_I;
                case MG_KG_MF -> MG_KG_MF;
                case MG_KG_MS -> MG_KG_MS;
                default -> value.name().toLowerCase();
            };
        } else {
            result = "";
        }
        return result;
    };
}
