package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/**
 * #12312
 * Identiques à la QSA totale
 * on ne prend en compte que les substances du référentiel "Substances Actives Commission Européenne" dont les valeurs de la colonne L "SA_candidates_substitution" prend la valeur "O"
 */
public class IndicatorSubstancesCandidateToSubstitution extends IndicatorTotalActiveSubstanceAmount {
    public static final String COLUMN_NAME = "qsa_substances_candidates_substitution";
    public static final String COLUMN_NAME_HTS = "qsa_substances_candidates_substitution_hts";

    protected final String[] indicators = new String[]{
            "Indicator.label.totalSubstituteQSA",        // QSA Substances candidates à la substitution
            "Indicator.label.totalSubstituteQSA_hts",    // QSA Substances candidates à la substitution hors traitement de semence
    };

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME_HTS);
        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(
            WriterContext writerContext,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedCropExecutionContext cropContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            PracticedCropCyclePhase phase) {

        if (interventionContext.isFictive()) {
            return newArray(indicators.length, 0.0d);
        }

        // just to increment field counter
        double toolPSCi = getToolPSCi(interventionContext.getIntervention());
        MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode = practicedSystemContext.getAllSubstancesActivesCommissionEuropeenneByAmmCode();

        final Optional<Pair<Double, Double>> totalQsa = computeTotalQsa(
                writerContext,
                toolPSCi,
                interventionContext,
                domainContext.getAllDomainSubstancesByAmm(),
                allSubstancesActivesCommissionEuropeenneByAmmCode);

        return totalQsa.map(doubleDoublePair -> new Double[]{doubleDoublePair.getLeft(), doubleDoublePair.getRight()}).orElse(null);
    }

    @Override
    public Double[] manageIntervention(
            WriterContext writerContext,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveCropExecutionContext cropContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {

        // just to increment field counter
        double toolPSCi = getToolPSCi(interventionContext.getIntervention());

        MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode = domainContext.getAllSubstancesActivesCommissionEuropeenneByAmmCode();
        final Optional<Pair<Double, Double>> totalQsa = computeTotalQsa(
                writerContext,
                toolPSCi,
                interventionContext,
                domainContext.getAllDomainSubstancesByAmm(),
                allSubstancesActivesCommissionEuropeenneByAmmCode);

        return totalQsa.map(doubleDoublePair -> new Double[]{doubleDoublePair.getLeft(), doubleDoublePair.getRight()}).orElse(null);
    }

    private Optional<Pair<Double, Double>> computeTotalQsa(
            WriterContext writerContext,
            double psci,
            PerformanceInterventionContext interventionContext,
            Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> domainSubstancesByAmm,
            MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode) {

        AtomicBoolean computeDoneFlag = new AtomicBoolean(false);

        AtomicReference<Double> result0 = new AtomicReference<>(0.0);

        interventionContext.getOptionalPesticidesSpreadingAction().ifPresent(abstractAction -> {
            final double treatedSurface = abstractAction.getProportionOfTreatedSurface() / 100;
            final double phytoPsci = psci * treatedSurface;

            Collection<? extends AbstractPhytoProductInputUsage> pesticideProductInputUsages = abstractAction.getPesticideProductInputUsages();
            if (pesticideProductInputUsages != null) {
                pesticideProductInputUsages.stream()
                        .filter(u -> u.getDomainPhytoProductInput() != null)
                        .filter(u -> u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                        .forEach(
                                usage -> {

                                    ReferenceDoseDTO referenceDose = getReferenceDoseDTO(usage, interventionContext);

                                    final double inputAmount = this.computeAmountUsedInKgHa(abstractAction, usage, phytoPsci, referenceDose);

                                    final String codeAmm = usage.getDomainPhytoProductInput().getRefInput().getCode_AMM();

                                    List<RefCompositionSubstancesActivesParNumeroAMM> doseSaCandidatesToSubstitution = getQSA_Substance(
                                            domainSubstancesByAmm, allSubstancesActivesCommissionEuropeenneByAmmCode, codeAmm);

                                    double sum = computeAndWriteUsage(
                                            writerContext,
                                            interventionContext,
                                            abstractAction,
                                            usage,
                                            doseSaCandidatesToSubstitution,
                                            inputAmount,
                                            false);
                                    result0.updateAndGet(v -> (v + sum));
                                    pushValuesToContext(writerContext, interventionContext, false, abstractAction, usage, sum);

                                    computeDoneFlag.set(true);
                                }
                        );
            }
        });

        interventionContext.getOptionalBiologicalControlAction().ifPresent(abstractAction -> {
            // Application du psci phyto pour les actions d'application de produit phytosanitaire
            final double treatedSurface = abstractAction.getProportionOfTreatedSurface() / 100;
            final double phytoPsci = psci * treatedSurface;

            // Parmi les produits sans AMM et macro-organismes, certains ont un AMM (qui peut être un vrai AMM
            // ou un numéro de dossier quelconque) que l'on peut retrouver dans refactatraitementsproduit,
            // donc pour ces produits on peut trouver des quantités de substances actives
            Collection<? extends AbstractPhytoProductInputUsage> biologicalProductInputUsages = abstractAction.getBiologicalProductInputUsages();
            if (biologicalProductInputUsages != null) {
                biologicalProductInputUsages.stream()
                        .filter(u -> u.getDomainPhytoProductInput() != null)
                        .filter(u -> u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                        .forEach(
                                usage -> {
                                    ReferenceDoseDTO referenceDose = getReferenceDoseDTO(usage, interventionContext);

                                    final double inputAmount = this.computeAmountUsedInKgHa(abstractAction, usage, phytoPsci, referenceDose);

                                    final String codeAmm = usage.getDomainPhytoProductInput().getRefInput().getCode_AMM();

                                    List<RefCompositionSubstancesActivesParNumeroAMM> doseSaCandidatesToSubstitution = getQSA_Substance(domainSubstancesByAmm, allSubstancesActivesCommissionEuropeenneByAmmCode, codeAmm);

                                    double sum = computeAndWriteUsage(
                                            writerContext,
                                            interventionContext,
                                            abstractAction,
                                            usage,
                                            doseSaCandidatesToSubstitution,
                                            inputAmount,
                                            false);
                                    result0.updateAndGet(v -> (v + sum));
                                    pushValuesToContext(writerContext, interventionContext, false, abstractAction, usage, sum);

                                    computeDoneFlag.set(true);
                                }
                        );
            }
        });

        Double tsaWithoutTS = result0.get();

        interventionContext.getOptionalSeedingActionUsage().ifPresent(abstractAction -> abstractAction.getSeedLotInputUsage()
                .stream()
                .filter(u -> u != null && u.getSeedingSpecies() != null)
                .flatMap(u -> u.getSeedingSpecies().stream())
                .filter(u -> u.getSeedProductInputUsages() != null)
                .flatMap(u -> u.getSeedProductInputUsages().stream())
                .filter(u -> u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                .forEach(
                        usage -> {
                            ReferenceDoseDTO referenceDose = getReferenceDoseDTO(usage, interventionContext);
                            final double inputAmount = this.computeAmountUsedInKgHa(abstractAction, usage, psci, referenceDose);
                            final String codeAmm = usage.getDomainPhytoProductInput().getRefInput().getCode_AMM();

                            List<RefCompositionSubstancesActivesParNumeroAMM> doseSaCandidatesToSubstitution = getQSA_Substance(domainSubstancesByAmm, allSubstancesActivesCommissionEuropeenneByAmmCode, codeAmm);

                            double sum = computeAndWriteUsage(
                                    writerContext,
                                    interventionContext,
                                    abstractAction,
                                    usage,
                                    doseSaCandidatesToSubstitution,
                                    inputAmount,
                                    true);
                            result0.updateAndGet(v -> (v + sum));

                            pushValuesToContext(writerContext, interventionContext, true, abstractAction, usage, sum);

                            computeDoneFlag.set(true);
                        }
                ));

        Double tsaWithTS = result0.get();

        pushValuesToContext(interventionContext, tsaWithTS, tsaWithoutTS);

        return computeDoneFlag.get() ? Optional.of(Pair.of(tsaWithTS, tsaWithoutTS)) : Optional.empty();
    }

    protected void pushValuesToContext(WriterContext writerContext, PerformanceInterventionContext interventionContext, boolean isSeedingTreatment, AbstractAction action, AbstractPhytoProductInputUsage usage, Double tsaWithoutTS) {}

    protected void pushValuesToContext(PerformanceInterventionContext interventionContext, Double tsaWithTS, Double tsaWithoutTS) {}

    protected @NotNull List<RefCompositionSubstancesActivesParNumeroAMM> getQSA_Substance(Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> domainSubstancesByAmm, MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode, String codeAmm) {
        List<String> idSaCandidatesToSubstitution = allSubstancesActivesCommissionEuropeenneByAmmCode.get(codeAmm)
                .stream()
                .filter(RefSubstancesActivesCommissionEuropeenne::isSa_candidates_substitution)
                .toList()
                .stream()
                .map(RefSubstancesActivesCommissionEuropeenne::getId_sa)
                .toList();

        List<RefCompositionSubstancesActivesParNumeroAMM> doseSaCandidatesToSubstitution = domainSubstancesByAmm.getOrDefault(codeAmm, List.of())
                .stream()
                .filter(ref -> idSaCandidatesToSubstitution.contains(ref.getId_sa())).
                toList();
        return doseSaCandidatesToSubstitution;
    }

    @Override
    protected String[] getIndicators() {
        return indicators;
    }

}
