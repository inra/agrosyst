package fr.inra.agrosyst.services.measurement;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.VariableType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.measure.Measure;
import fr.inra.agrosyst.api.entities.measure.MeasureImpl;
import fr.inra.agrosyst.api.entities.measure.Measurement;
import fr.inra.agrosyst.api.entities.measure.MeasurementSession;
import fr.inra.agrosyst.api.entities.measure.MeasurementSessionTopiaDao;
import fr.inra.agrosyst.api.entities.measure.MeasurementTopiaDao;
import fr.inra.agrosyst.api.entities.measure.MeasurementType;
import fr.inra.agrosyst.api.entities.measure.Observation;
import fr.inra.agrosyst.api.entities.measure.ObservationImpl;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActiveTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefAdventiceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefMesure;
import fr.inra.agrosyst.api.entities.referential.RefMesureTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefProtocoleVgObs;
import fr.inra.agrosyst.api.entities.referential.RefProtocoleVgObsTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefStadeNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeNuisibleEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSupportOrganeEDI;
import fr.inra.agrosyst.api.entities.referential.RefSupportOrganeEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefTypeNotationEDI;
import fr.inra.agrosyst.api.entities.referential.RefTypeNotationEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefUnitesQualifiantEDI;
import fr.inra.agrosyst.api.entities.referential.RefUnitesQualifiantEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefValeurQualitativeEDI;
import fr.inra.agrosyst.api.entities.referential.RefValeurQualitativeEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.measurement.MeasurementService;
import fr.inra.agrosyst.api.services.measurement.ProtocoleVgObsFilter;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.export.EntityExporter;
import fr.inra.agrosyst.services.common.export.ExportModelAndRows;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.AdventiceBean;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.AdventiceModel;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.CommonBean;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.GesModel;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.MeasureBean;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.MeasurementBean;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.MeasurementModel;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.MeteoModel;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.NuisibleMaladiesAuxiliairesBean;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.NuisibleMaladiesAuxiliairesModel;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.PlanteModel;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.SolModel;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.SoluteModel;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.StadeBean;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportBeansAndModels.StadeModel;
import fr.inra.agrosyst.services.measurement.export.MeasurementExportTask;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@Setter
public class MeasurementServiceImpl extends AbstractAgrosystService implements MeasurementService {

    private static final Log LOGGER = LogFactory.getLog(MeasurementServiceImpl.class);

    protected AnonymizeService anonymizeService;

    protected MeasurementSessionTopiaDao measurementSessionDao;
    protected ZoneTopiaDao zoneDao;
    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    protected RefMesureTopiaDao refMesureDao;
    protected MeasurementTopiaDao measurementDao;
    protected RefSupportOrganeEDITopiaDao refSupportOrganeEDIDao;
    protected RefActaSubstanceActiveTopiaDao refSubstanceActiveDao;
    protected RefAdventiceTopiaDao refAdventiceDao;
    protected RefStadeEDITopiaDao refStadeEDIDao;
    protected EffectiveSeasonalCropCycleTopiaDao effectiveSeasonalCropCycleDao;
    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleDao;
    protected RefProtocoleVgObsTopiaDao refProtocoleVgObsDao;
    protected RefNuisibleEDITopiaDao refNuisiblesEdiDao;
    protected RefStadeNuisibleEDITopiaDao refStadeNuisibleEDIDao;
    protected RefTypeNotationEDITopiaDao refTypeNotationEDIDao;
    protected RefUnitesQualifiantEDITopiaDao refUnitesQualifiantEDIDao;
    protected RefValeurQualitativeEDITopiaDao refValeurQualitativeEDIDao;

    @Override
    public List<MeasurementSession> getZoneMeasurementSessions(Zone zone) {
        return measurementSessionDao.forZoneEquals(zone).setOrderByArguments(MeasurementSession.PROPERTY_START_DATE).findAll();
    }

    protected void checkPreconditionOnActive(Zone zone) {
        Preconditions.checkArgument(
                zone.isActive() && zone.getPlot().isActive() && zone.getPlot().getDomain().isActive() &&
                        (zone.getPlot().getGrowingSystem() == null || zone.getPlot().getGrowingSystem().isActive()),
                "The zone, plot, growing system or domain for zone with id:" + zone.getTopiaId() + " is actually unactivated");
    }

    @Override
    public void updateMeasurementSessions(Zone zone, Collection<MeasurementSession> inputSessions) {

        checkPreconditionOnActive(zone);
        
        // copy binder
        Binder<MeasurementSession, MeasurementSession> sessionBinder = BinderFactory.newBinder(MeasurementSession.class);

        List<MeasurementSession> currentSessions = measurementSessionDao.forZoneEquals(zone).findAll();
        List<MeasurementSession> toDeleteSessions = Lists.newArrayList(currentSessions);
        Map<String, MeasurementSession> idToSession = Maps.uniqueIndex(currentSessions, TopiaEntities.getTopiaIdFunction());
        for (MeasurementSession inputSession : inputSessions) {

            Preconditions.checkNotNull(inputSession.getStartDate());

            MeasurementSession currentSession;
            if (StringUtils.isEmpty(inputSession.getTopiaId())) {
                currentSession = measurementSessionDao.newInstance();
                currentSession.setZone(zone);
            } else {
                currentSession = idToSession.get(inputSession.getTopiaId());
                toDeleteSessions.remove(currentSession);
            }

            // il faut aider topia pour la composition sur une entité abstraite (inverse = true)
            sessionBinder.copyExcluding(inputSession, currentSession,
                    MeasurementSession.PROPERTY_ZONE,
                    MeasurementSession.PROPERTY_MEASUREMENTS);

            if (currentSession.isPersisted()) {
                currentSession = measurementSessionDao.update(currentSession);
            } else {
                currentSession = measurementSessionDao.create(currentSession);
            }

            Collection<Measurement> currentMeasurements = currentSession.getMeasurements();
            if (currentMeasurements == null) {
                currentMeasurements = new ArrayList<>();
            }
            if (inputSession.getMeasurements() != null) {
                Map<String, Measurement> idToMeasurement = new HashMap<>(Maps.uniqueIndex(currentMeasurements, TopiaEntities.getTopiaIdFunction()));
                for (Measurement inputMeasurement : inputSession.getMeasurements()) {

                    // c'est un cas possible si la deserialisation gson n'a pas pu déterminer
                    // le type de mesure dans le cas ou l'utilisateur n'a pas sélectionné
                    // de type de mesure (c'est normalement un cas non permis)
                    if (inputMeasurement == null) {
                        continue;
                    } else {
                        Preconditions.checkNotNull(inputMeasurement.getMeasurementType());
                    }

                    if (inputMeasurement instanceof MeasureImpl measure) {
                        if (measure.getMeasurementType() != MeasurementType.PLANTE && measure.getMeasurementType() != MeasurementType.STADE_CULTURE) {
                            measure.setCroppingPlanSpecies(null);
                        }
                    }

                    if (inputMeasurement instanceof ObservationImpl observation) {
                        if (observation.getMeasurementType() != MeasurementType.PLANTE && observation.getMeasurementType() != MeasurementType.STADE_CULTURE) {
                            observation.setCroppingPlanSpecies(null);
                        }
                    }

                    Measurement currentMeasurement;
                    if (StringUtils.isEmpty(inputMeasurement.getTopiaId())) {
                        currentSession.addMeasurements(inputMeasurement);
                        currentMeasurement = measurementDao.create(inputMeasurement);
                    } else {
                        currentMeasurement = idToMeasurement.remove(inputMeasurement.getTopiaId());
                    }

                    // dynamic binder for inheritance
                    Binder<Measurement, Measurement> measurementBinder = (Binder<Measurement, Measurement>)BinderFactory.newBinder(currentMeasurement.getClass());
                    measurementBinder.copyExcluding(inputMeasurement, currentMeasurement,
                            Measurement.PROPERTY_MEASUREMENT_SESSION
                    );


                    if (currentMeasurement.isPersisted()) {
                        measurementDao.update(currentMeasurement);
                    } else {
                        measurementDao.create(currentMeasurement);
                    }
                }
                currentMeasurements.removeAll(idToMeasurement.values());
                measurementDao.deleteAll(idToMeasurement.values());
            }
        }
        measurementSessionDao.deleteAll(toDeleteSessions);

        getTransaction().commit();
    }

    @Override
    public Zone getZone(String zoneTopiaId) {
        Zone zone = zoneDao.forTopiaIdEquals(zoneTopiaId).findUnique();
        Zone result = anonymizeService.checkForZoneAnonymization(zone);
        return result;
    }

    @Override
    public Set<CroppingPlanEntry> getZoneCroppingPlanEntries(Zone zone) {
        Set<CroppingPlanEntry> result = Sets.newHashSet();
        List<EffectiveSeasonalCropCycle> cycleS = effectiveSeasonalCropCycleDao.forZoneEquals(zone).findAll();
        List<EffectivePerennialCropCycle> cycleP = effectivePerennialCropCycleDao.forZoneEquals(zone).findAll();
        
        for (EffectiveSeasonalCropCycle cycle : cycleS) {
            if (cycle.getNodes() != null) {
                for (EffectiveCropCycleNode node : cycle.getNodes()) {
                    result.add(node.getCroppingPlanEntry());
                }
            }
        }
        for (EffectivePerennialCropCycle cycle : cycleP) {
            result.add(cycle.getCroppingPlanEntry());
        }
        return result;
    }

    @Override
    public List<VariableType> findAllVariableTypes(MeasurementType measurementType) {
        return refMesureDao.findTypeVariableValues(measurementType);
    }

    @Override
    public List<RefMesure> findAllVariables(MeasurementType measurementType, VariableType variableType) {
        return refMesureDao.findVariables(measurementType, variableType);
    }

    @Override
    public List<RefSupportOrganeEDI> findAllSupportOrganeEDI() {
        List<RefSupportOrganeEDI> result = refSupportOrganeEDIDao.forActiveEquals(true)
                .setOrderByArguments(RefSupportOrganeEDI.PROPERTY_REFERENCE_LABEL).findAll();
        return result;
    }

    @Override
    public List<RefActaSubstanceActive> findDistinctSubstanceActives() {
        List<RefActaSubstanceActive> result = refSubstanceActiveDao.findDistinctSubtanceActive();
        return result;
    }

    @Override
    public List<RefAdventice> findAllAdventices() {
        List<RefAdventice> result = refAdventiceDao.forActiveEquals(true)
                .setOrderByArguments(RefAdventice.PROPERTY_ADVENTICE).findAll();
        return result;
    }

    @Override
    public List<RefStadeEDI> findAllStadeEdi(String cropFamily, String vegetativeProfile) {
        List<RefStadeEDI> result;
        
        if (StringUtils.isNotEmpty(cropFamily)) {
            result = refStadeEDIDao.forFamille_de_cultureEquals(cropFamily).setOrderByArguments(RefStadeEDI.PROPERTY_COLONNE2).findAll();
        } else if (StringUtils.isNumeric(vegetativeProfile)) {
            Integer pv = Integer.valueOf(vegetativeProfile);
            result = refStadeEDIDao.forProfil_vegetatifEquals(pv).setOrderByArguments(RefStadeEDI.PROPERTY_COLONNE2).findAll();
        } else {
            result = Collections.emptyList();
        }

        return result;
    }

    @Override
    public List<String> findAllProtocoleVgObsLabels() {
        List<String> result = refProtocoleVgObsDao.findAllProperties(RefProtocoleVgObs.PROPERTY_PROTOCOLE_LIBELLE, null);
        return result;
    }
    
    @Override
    public List<String> findAllProtocoleVgObsPests(ProtocoleVgObsFilter filter) {
        List<String> result = refProtocoleVgObsDao.findAllProperties(RefProtocoleVgObs.PROPERTY_LIGNE_ORGANISME_VIVANT, filter);
        return result;
    }
    
    @Override
    public List<String> findAllProtocoleVgObsStades(ProtocoleVgObsFilter filter) {
        List<String> result = refProtocoleVgObsDao.findAllProperties(RefProtocoleVgObs.PROPERTY_LIGNE_STADES_DEVELOPPEMENT, filter);
        return result;
    }
    
    @Override
    public List<String> findAllProtocoleVgObsSupports(ProtocoleVgObsFilter filter) {
        List<String> result = refProtocoleVgObsDao.findAllProperties(RefProtocoleVgObs.PROPERTY_LIGNE_SUPPORTS_ORGANES, filter);
        return result;
    }
    
    @Override
    public List<String> findAllProtocoleVgObsObservations(ProtocoleVgObsFilter filter) {
        List<String> result = refProtocoleVgObsDao.findAllProperties(RefProtocoleVgObs.PROPERTY_LIGNE_TYPE_OBSERVATION, filter);
        return result;
    }
    
    @Override
    public List<String> findAllProtocoleVgObsQualitatives(ProtocoleVgObsFilter filter) {
        List<String> result = refProtocoleVgObsDao.findAllProperties(RefProtocoleVgObs.PROPERTY_CLASSE_VALEUR_QUALITATIVE, filter);
        return result;
    }

    @Override
    public List<String> findAllProtocoleVgObsUnits(ProtocoleVgObsFilter filter) {
        List<String> result = refProtocoleVgObsDao.findAllProperties(RefProtocoleVgObs.PROPERTY_RELEVE_UNITE, filter);
        return result;
    }

    @Override
    public List<RefProtocoleVgObs> findAllProtocoleVgObsQualifiers(ProtocoleVgObsFilter filter) {
        List<RefProtocoleVgObs> result = refProtocoleVgObsDao.findAllProperties(null, filter);
        return result;
    }

    @Override
    public List<BioAgressorType> findAllEdiPestTypes() {
        List<BioAgressorType> result = refNuisiblesEdiDao.findAllActiveParam();
        return result;
    }

    @Override
    public List<RefNuisibleEDI> findAllEdiPests(BioAgressorType pestType) {
        List<RefNuisibleEDI> result = refNuisiblesEdiDao.forReference_paramEquals(pestType)
                .setOrderByArguments(RefNuisibleEDI.PROPERTY_REFERENCE_LABEL).findAll();
        return result;
    }

    @Override
    public List<RefStadeNuisibleEDI> findAllEdiPestStades() {
        List<RefStadeNuisibleEDI> result = refStadeNuisibleEDIDao.newQueryBuilder()
                .setOrderByArguments(RefStadeNuisibleEDI.PROPERTY_REFERENCE_LABEL).findAll();
        return result;
    }

    @Override
    public List<String> findAllVgObsUnits() {
        List<String> result = refProtocoleVgObsDao.findAllProperties(RefProtocoleVgObs.PROPERTY_RELEVE_QUALIFIANT_UNITE_MESURE, null);
        return result;
    }

    @Override
    public List<RefTypeNotationEDI> findAllEdiNotations() {
        List<RefTypeNotationEDI> result = refTypeNotationEDIDao.newQueryBuilder()
                .setOrderByArguments(RefTypeNotationEDI.PROPERTY_REFERENCE_LABEL).findAll();
        return result;
    }

    @Override
    public List<RefValeurQualitativeEDI> findAllEdiQualitatives() {
        List<RefValeurQualitativeEDI> result = refValeurQualitativeEDIDao.newQueryBuilder()
                .setOrderByArguments(RefValeurQualitativeEDI.PROPERTY_REFERENCE_LABEL).findAll();
        return result;
    }

    @Override
    public List<RefUnitesQualifiantEDI> findAllEdiQualifiantUnits() {
        List<RefUnitesQualifiantEDI> result = refUnitesQualifiantEDIDao.newQueryBuilder()
                .setOrderByArguments(RefUnitesQualifiantEDI.PROPERTY_REFERENCE_LABEL).findAll();
        return result;
    }

    protected static String computeExportValue(Measurement input) {
        String value = "";
        if (input instanceof Measure measure) {
            if (StringUtils.isNotBlank(measure.getMeasureValue())) {
                value += measure.getMeasureValue() + " ";
            }
            if (StringUtils.isNotBlank(measure.getMeasureUnit())) {
                value += measure.getMeasureUnit() + " ";
            }
        } else if (input instanceof Observation observation) {
            if (observation.getMeasurementType() == MeasurementType.ADVENTICES) {
                if (observation.getAdventiceStage() != null) {
                    value += observation.getAdventiceStage().getColonne2() + " ";
                }
            }
            if (observation.getMeasurementType() == MeasurementType.NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES) {
                value += observation.getQuantitativeValue() != null ? String.valueOf(observation.getQuantitativeValue()) : "";
            }
            if (observation.getMeasurementType() == MeasurementType.STADE_CULTURE) {
                if (observation.getCropNumberObserved() != null) {
                    value += observation.getCropNumberObserved() + " ";
                }
            }

            if (observation.getMeasurementType() == MeasurementType.NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES) {

                if (observation.isProtocolVgObs()) {
                    if (observation.getProtocol() != null
                            && StringUtils.isNotBlank(observation.getProtocol().getReleve_qualifiant_unite_mesure())) {
                        value += observation.getProtocol().getReleve_qualifiant_unite_mesure();
                    }
                } else {
                    if (observation.getCropUnitQualifier() != null
                            && StringUtils.isNotBlank(observation.getCropUnitQualifier().getReference_label())) {
                        value += observation.getCropUnitQualifier().getReference_label();
                    } else if (StringUtils.isNotBlank(observation.getUnitEDI())) {
                        value += observation.getUnitEDI();
                    }
                }
            }
        }

        return value;
    }

    protected static String computeExportVariable(Measurement input) {
        String variable = "";

        if (input != null) {
            if (input instanceof Measure measure) {
                if (measure.getRefMesure() != null) {
                    variable += measure.getRefMesure().getVariable_mesuree();
                }
            } else if (input instanceof Observation observation) {
                if (observation.getMeasurementType() == MeasurementType.ADVENTICES) {

                    if (observation.getMeasuredAdventice() != null
                            && StringUtils.isNotBlank(observation.getMeasuredAdventice().getAdventice())) {
                        variable += observation.getMeasuredAdventice().getAdventice();
                    }
                }
                if (observation.getMeasurementType() == MeasurementType.NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES) {
                    if (observation.getPest() != null && StringUtils.isNotBlank(observation.getPest().getReference_label())) {
                        variable += observation.getPest().getReference_label();
                    } else if (observation.getProtocol() != null
                            && StringUtils.isNotBlank(observation.getProtocol().getProtocole_libelle())) {
                        variable += observation.getProtocol().getProtocole_libelle();
                    }
                }
            }
        }

        return variable;
    }

    /**
     * use RefCompositionSubstancesActivesParNumeroAMM instead of RefActaSubstanceActive
     * @param zoneIds zone ids
     */
    @Deprecated
    @Override
    public ExportResult exportEffectiveMeasurementsAsXls(Collection<String> zoneIds) {
        List<MeasurementBean> measurementBeans = new LinkedList<>();
        List<StadeBean> stadeBeans = new LinkedList<>();
        List<AdventiceBean> adventiceBeans = new LinkedList<>();
        List<NuisibleMaladiesAuxiliairesBean> nuisibleMaladiesAuxiliairesBeans = new LinkedList<>();
        List<MeasureBean> planteBeans = new LinkedList<>();
        List<MeasureBean> solBeans = new LinkedList<>();
        List<MeasureBean> soluteBeans = new LinkedList<>();
        List<MeasureBean> gesBeans = new LinkedList<>();
        List<MeasureBean> meteoBeans = new LinkedList<>();

        try {
            if (CollectionUtils.isNotEmpty(zoneIds)) {
                long start = System.currentTimeMillis();
                Iterable<List<String>> chunks = Iterables.partition(zoneIds, 20);
                for (List<String> chunk : chunks) {

                    zoneDao.clear();

                    Iterable<Zone> zones = zoneDao.forTopiaIdIn(chunk).findAll();
                    for (Zone zone : zones) {

                        Iterable<MeasurementSession> measurementSessions = measurementSessionDao.forZoneEquals(zone).findAllLazy(100);

                        // anonymize plot
                        zone = anonymizeService.checkForZoneAnonymization(zone);

                        for (MeasurementSession measurementSession : measurementSessions) {
                            // Common data for all tabs

                            String growingSystemName = null;
                            String growingPlanName = null;
                            if (zone.getPlot().getGrowingSystem() != null) {
                                growingSystemName = zone.getPlot().getGrowingSystem().getName();
                                growingPlanName = zone.getPlot().getGrowingSystem().getGrowingPlan().getName();
                            }

                            CommonBean baseBean = new CommonBean(
                                    zone.getName(),
                                    zone.getPlot().getName(),
                                    growingSystemName,
                                    growingPlanName,
                                    zone.getPlot().getDomain().getName(),
                                    zone.getPlot().getDomain().getCampaign(),
                                    measurementSession.getStartDate(),
                                    measurementSession.getEndDate()
                            );

                            Collection<Measurement> measurements = measurementSession.getMeasurements();

                            // main tab
                            for (Measurement measurement : measurements) {
                                MeasurementBean mainBean = new MeasurementBean(baseBean);
                                mainBean.setMeasurementType(measurement.getMeasurementType());
                                mainBean.setRepetitionNumber(measurement.getRepetitionNumber());
                                mainBean.setVariable(computeExportVariable(measurement));
                                mainBean.setValue(computeExportValue(measurement));
                                measurementBeans.add(mainBean);
                            }

                            // StadeCulture
                            Set<Measurement> stadeCultureMst = measurements.stream()
                                    .filter(m -> m.getMeasurementType() == MeasurementType.STADE_CULTURE)
                                    .collect(Collectors.toSet());
                            for (Measurement measurement : stadeCultureMst) {
                                StadeBean stadeBean = new StadeBean(baseBean);
                                stadeBean.setMeasurementType(measurement.getMeasurementType());
                                stadeBean.setMeasuring_protocol(measurement.getMeasuringProtocol());
                                stadeBean.setRepetition_number(measurement.getRepetitionNumber());
                                stadeBean.setComment(measurement.getComment());
                                if (measurement instanceof Observation observation) {
                                    Optional<RefEspece> refEspece = Optional.ofNullable(observation.getCroppingPlanSpecies())
                                            .map(CroppingPlanSpecies::getSpecies);
                                    refEspece.ifPresent(espece -> {
                                        stadeBean.setLibelle_espece_botanique(espece.getLibelle_espece_botanique_Translated());
                                        stadeBean.setLibelle_qualifiant__aee(espece.getLibelle_qualifiant_AEE());
                                        stadeBean.setLibelle_type_saisonnier__aee(espece.getLibelle_type_saisonnier_AEE());
                                        stadeBean.setLibelle_destination__aee(espece.getLibelle_destination_AEE());
                                    });
                                    Optional.ofNullable(observation.getCropStageMin())
                                            .map(RefStadeEDI::getColonne2)
                                            .ifPresent(stadeBean::setCrop_stage_min);
                                    Optional.ofNullable(observation.getCropStageAverage())
                                            .map(RefStadeEDI::getColonne2)
                                            .ifPresent(stadeBean::setCrop_stage_average);
                                    Optional.ofNullable(observation.getCropStageMedium())
                                            .map(RefStadeEDI::getColonne2)
                                            .ifPresent(stadeBean::setCrop_stage_medium);
                                    Optional.ofNullable(observation.getCropStageMax())
                                            .map(RefStadeEDI::getColonne2)
                                            .ifPresent(stadeBean::setCrop_stage_max);
                                    stadeBean.setCrop_number_observed(observation.getCropNumberObserved());
                                }
                                stadeBeans.add(stadeBean);
                            }

                            // Adventices
                            Set<Measurement> adventiceMst = measurements.stream()
                                    .filter(m -> m.getMeasurementType() == MeasurementType.ADVENTICES)
                                    .collect(Collectors.toSet());
                            for (Measurement measurement : adventiceMst) {
                                AdventiceBean adventiceBean = new AdventiceBean(baseBean);
                                adventiceBean.setMeasurementType(measurement.getMeasurementType());
                                adventiceBean.setMeasuring_protocol(measurement.getMeasuringProtocol());
                                adventiceBean.setRepetition_number(measurement.getRepetitionNumber());
                                adventiceBean.setComment(measurement.getComment());
                                if (measurement instanceof Observation observation) {
                                    Optional.ofNullable(observation.getMeasuredAdventice())
                                            .map(RefAdventice::getAdventice)
                                            .ifPresent(adventiceBean::setMeasured_adventice);
                                    Optional.ofNullable(observation.getAdventiceStage())
                                            .map(RefStadeEDI::getColonne2)
                                            .ifPresent(adventiceBean::setAdventice_stage);
                                    adventiceBean.setAdventice_min(observation.getAdventiceMin());
                                    adventiceBean.setAdventice_average(observation.getAdventiceAverage());
                                    adventiceBean.setAdventice_max(observation.getAdventiceMax());
                                    adventiceBean.setAdventice_median(observation.getAdventiceMedian());
                                }
                                adventiceBeans.add(adventiceBean);
                            }

                            // NuisibleMaladiesPhysiologiquesAux
                            Set<Measurement> nuisibles = measurements.stream()
                                    .filter(m -> m.getMeasurementType() == MeasurementType.NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES)
                                    .collect(Collectors.toSet());
                            for (Measurement nuisible : nuisibles) {
                                Observation observation = (Observation) nuisible;
                                NuisibleMaladiesAuxiliairesBean nuisibleMaladiesAuxiliairesBean = new NuisibleMaladiesAuxiliairesBean(baseBean);
                                nuisibleMaladiesAuxiliairesBean.setMeasurementType(observation.getMeasurementType());
                                nuisibleMaladiesAuxiliairesBean.setRepetition_number(observation.getRepetitionNumber());
                                nuisibleMaladiesAuxiliairesBean.setComment(observation.getComment());
                                nuisibleMaladiesAuxiliairesBean.setCrop_number_observed(observation.getCropNumberObserved());
                                nuisibleMaladiesAuxiliairesBean.setQuantitative_value(observation.getQuantitativeValue());
                                nuisibleMaladiesAuxiliairesBean.setUnit_other(observation.getUnitOther());
                                nuisibleMaladiesAuxiliairesBean.setOther_qualifier(observation.getOtherQualifier());
                                Optional.ofNullable(observation.getProtocol())
                                        .map(RefProtocoleVgObs::getProtocole_libelle)
                                        .ifPresent(nuisibleMaladiesAuxiliairesBean::setProtocol);
                                if (observation.isProtocolVgObs() && observation.getProtocol() != null) {
                                    RefProtocoleVgObs protocol = observation.getProtocol();
                                    nuisibleMaladiesAuxiliairesBean.setPest(protocol.getLigne_organisme_vivant());
                                    nuisibleMaladiesAuxiliairesBean.setCrop_organism_stage(protocol.getLigne_stades_developpement());
                                    nuisibleMaladiesAuxiliairesBean.setCrop_organ_support(protocol.getLigne_supports_organes());
                                    nuisibleMaladiesAuxiliairesBean.setCrop_notation_type(protocol.getLigne_type_observation());
                                    nuisibleMaladiesAuxiliairesBean.setCrop_qualititive_value(protocol.getClasse_valeur_qualitative());
                                    nuisibleMaladiesAuxiliairesBean.setUnit_edi(protocol.getReleve_unite());
                                    nuisibleMaladiesAuxiliairesBean.setCrop_unit_qualifier(protocol.getReleve_qualifiant_unite_mesure());
                                } else {
                                    Optional.ofNullable(observation.getPest())
                                            .map(RefNuisibleEDI::getReference_label)
                                            .ifPresent(nuisibleMaladiesAuxiliairesBean::setPest);
                                    Optional.ofNullable(observation.getCropOrganismStage())
                                            .map(RefStadeNuisibleEDI::getReference_label)
                                            .ifPresent(nuisibleMaladiesAuxiliairesBean::setCrop_organism_stage);
                                    Optional.ofNullable(observation.getCropOrganSupport())
                                            .map(RefSupportOrganeEDI::getReference_label)
                                            .ifPresent(nuisibleMaladiesAuxiliairesBean::setCrop_organ_support);
                                    Optional.ofNullable(observation.getCropNotationType())
                                            .map(RefTypeNotationEDI::getReference_label)
                                            .ifPresent(nuisibleMaladiesAuxiliairesBean::setCrop_notation_type);
                                    Optional.ofNullable(observation.getCropQualititiveValue())
                                            .map(RefValeurQualitativeEDI::getReference_label)
                                            .ifPresent(nuisibleMaladiesAuxiliairesBean::setCrop_qualititive_value);
                                    nuisibleMaladiesAuxiliairesBean.setUnit_edi(observation.getUnitEDI());
                                    Optional.ofNullable(observation.getCropUnitQualifier())
                                            .map(RefUnitesQualifiantEDI::getReference_label)
                                            .ifPresent(nuisibleMaladiesAuxiliairesBean::setCrop_unit_qualifier);
                                }
                                nuisibleMaladiesAuxiliairesBeans.add(nuisibleMaladiesAuxiliairesBean);
                            }

                            // Plante
                            Set<Measurement> plantes = measurements.stream()
                                    .filter(m -> m.getMeasurementType() == MeasurementType.PLANTE)
                                    .collect(Collectors.toSet());
                            for (Measurement plante : plantes) {
                                MeasureBean planteBean = toMeasureBean(baseBean, plante);
                                if (plante instanceof Measure measure) {
                                    Optional.ofNullable(measure.getCroppingPlanSpecies())
                                            .map(es -> {
                                                RefEspece species = es.getSpecies();
                                                RefVariete variety = es.getVariety();
                                                String name = species.getLibelle_espece_botanique();
                                                if (variety != null) {
                                                    name += " (" + variety.getLabel() + ")";
                                                }
                                                return name;
                                            })
                                            .ifPresent(planteBean::setCropping_plan_species);
                                    Optional.ofNullable(measure.getRefMesure())
                                            .map(RefMesure::getType_variable_mesuree)
                                            .ifPresent(planteBean::setType_variable_mesuree);
                                    planteBean.setEffective_or_area_taken(measure.getEffectiveOrAreaTaken());
                                }
                                planteBeans.add(planteBean);
                            }

                            // Sol
                            Set<Measurement> sols = measurements.stream()
                                    .filter(m -> m.getMeasurementType() == MeasurementType.SOL)
                                    .collect(Collectors.toSet());
                            for (Measurement sol : sols) {
                                MeasureBean solBean = toMeasureBean(baseBean, sol);
                                if (sol instanceof Measure measure) {
                                    solBean.setHorizonType(measure.getHorizonType());
                                    Optional.ofNullable(measure.getRefMesure())
                                            .map(RefMesure::getType_variable_mesuree)
                                            .ifPresent(solBean::setType_variable_mesuree);
                                }
                                solBeans.add(solBean);
                            }

                            // TransferDeSolutes
                            Set<Measurement> solutes = measurements.stream()
                                    .filter(m -> m.getMeasurementType() == MeasurementType.TRANSFERT_DE_SOLUTES)
                                    .collect(Collectors.toSet());
                            for (Measurement solute : solutes) {
                                MeasureBean soluteBean = toMeasureBean(baseBean, solute);
                                if (solute instanceof Measure measure) {
                                    Optional.ofNullable(measure.getActiveSubstance())
                                            .map(RefActaSubstanceActive::getNom_produit)
                                            .ifPresent(soluteBean::setActiveSubstance);
                                    soluteBean.setNitrogenMolecule(measure.getNitrogenMolecule());
                                }
                                soluteBeans.add(soluteBean);
                            }

                            // Ges
                            Set<Measurement> gaesSet = measurements.stream()
                                    .filter(m -> m.getMeasurementType() == MeasurementType.GES)
                                    .collect(Collectors.toSet());
                            for (Measurement gaes : gaesSet) {
                                MeasureBean gesBean = toMeasureBean(baseBean, gaes);
                                gesBeans.add(gesBean);
                            }

                            // Meteo
                            Set<Measurement> meteos = measurements.stream()
                                    .filter(m -> m.getMeasurementType() == MeasurementType.METEO)
                                    .collect(Collectors.toSet());
                            for (Measurement meteo : meteos) {
                                MeasureBean meteoBean = toMeasureBean(baseBean, meteo);
                                meteoBeans.add(meteoBean);
                            }

                        }
                    }
                }

                if (LOGGER.isInfoEnabled()) {
                    long duration = System.currentTimeMillis() - start;
                    int count = zoneIds.size();
                    LOGGER.warn(String.format("%d zones (mesures et observations) exportées en %dms (%dms/zone)", count, duration, duration / count));
                }
            }
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't copy properties", ex);
        }

        ExportModelAndRows<MeasurementBean> mainBeanExportModelAndRows = new ExportModelAndRows<>(new MeasurementModel(), measurementBeans);
        ExportModelAndRows<StadeBean> stadeBeanExportModelAndRows = new ExportModelAndRows<>(new StadeModel(), stadeBeans);
        ExportModelAndRows<AdventiceBean> adventiceBeanExportModelAndRows = new ExportModelAndRows<>(new AdventiceModel(), adventiceBeans);
        ExportModelAndRows<NuisibleMaladiesAuxiliairesBean> nuisibleMaladiesAuxiliairesBeanExportModelAndRows = new ExportModelAndRows<>(new NuisibleMaladiesAuxiliairesModel(), nuisibleMaladiesAuxiliairesBeans);
        ExportModelAndRows<MeasureBean> planteBeanExportModelAndRows = new ExportModelAndRows<>(new PlanteModel(refMesureDao), planteBeans);
        ExportModelAndRows<MeasureBean> solBeanExportModelAndRows = new ExportModelAndRows<>(new SolModel(refMesureDao), solBeans);
        ExportModelAndRows<MeasureBean> soluteBeanExportModelAndRows = new ExportModelAndRows<>(new SoluteModel(refMesureDao, refSubstanceActiveDao), soluteBeans);
        ExportModelAndRows<MeasureBean> gesBeanExportModelAndRows = new ExportModelAndRows<>(new GesModel(refMesureDao), gesBeans);
        ExportModelAndRows<MeasureBean> meteoBeanExportModelAndRows = new ExportModelAndRows<>(new MeteoModel(refMesureDao), meteoBeans);

        // technical export
        EntityExporter exporter = new EntityExporter();
        ExportResult result = exporter.exportAsXlsx(
                "Mesures-et-observations-export",
                mainBeanExportModelAndRows,
                stadeBeanExportModelAndRows,
                adventiceBeanExportModelAndRows,
                nuisibleMaladiesAuxiliairesBeanExportModelAndRows,
                planteBeanExportModelAndRows,
                solBeanExportModelAndRows,
                soluteBeanExportModelAndRows,
                gesBeanExportModelAndRows,
                meteoBeanExportModelAndRows
        );

        return result;
    }

    protected MeasureBean toMeasureBean(CommonBean baseBean, Measurement measurement) {
        MeasureBean gesBean = new MeasureBean(baseBean);

        gesBean.setMeasurementType(measurement.getMeasurementType());
        gesBean.setMeasuringProtocol(measurement.getMeasuringProtocol());
        gesBean.setRepetitionNumber(measurement.getRepetitionNumber());
        gesBean.setComment(measurement.getComment());
        if (measurement instanceof Measure measure) {
            gesBean.setSampling(measure.getSampling());
            Optional.ofNullable(measure.getRefMesure())
                    .map(RefMesure::getVariable_mesuree)
                    .ifPresent(gesBean::setVariable_mesuree);
            gesBean.setMeasure_type(measure.getMeasureType());
            gesBean.setMeasure_value(measure.getMeasureValue());
            gesBean.setMeasure_unit(measure.getMeasureUnit());
        }
        return gesBean;
    }

    @Override
    public void exportEffectiveMeasurementsAsXlsAsync(Collection<String> effectiveCropCycleIds) {
        AuthenticatedUser user = getAuthenticatedUser();

        MeasurementExportTask exportTask = new MeasurementExportTask(user.getTopiaId(), user.getEmail(), effectiveCropCycleIds);
        getBusinessTaskManager().schedule(exportTask);
    }

}
