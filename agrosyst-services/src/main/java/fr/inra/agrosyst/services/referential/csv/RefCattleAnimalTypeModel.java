package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleAnimalTypeImpl;
import fr.inra.agrosyst.api.exceptions.AgrosystImportException;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

import java.util.List;

/**
 * animal_type;livestock_type;livestock_population_unit
 * 
 * @author Kevin Morin
 */
public class RefCattleAnimalTypeModel extends AbstractAgrosystModel<RefCattleAnimalType> implements ExportModel<RefCattleAnimalType> {

    public RefCattleAnimalTypeModel(List<RefAnimalType> existingAnimalTypes) {
        super(CSV_SEPARATOR);

        newMandatoryColumn("animal_type", RefCattleAnimalType.PROPERTY_ANIMAL_TYPE);
        newMandatoryColumn("livestock_type", (refCattleAnimalType, value) -> {
            // we look for any ref animal type with the correct type (no matter the unit)
            RefAnimalType refAnimalType = existingAnimalTypes.stream()
                    .filter(t -> value.equals(t.getAnimalType()))
                    .findFirst()
                    .orElseThrow(() -> new AgrosystImportException(String.format("Le type d'atelier %s est introuvable dans le référentiel.", value)));
            refCattleAnimalType.setRefAnimalType(refAnimalType);
        });
        newMandatoryColumn("livestock_population_unit", (refCattleAnimalType, value) -> {
            // we check if the unit is the one of the previously found ref animal type
            // - if yes, then do nothing
            // - if no, then find the right one with the type and the unit
            RefAnimalType refAnimalType = refCattleAnimalType.getRefAnimalType();
            if (!value.equals(refAnimalType.getAnimalPopulationUnits())) {
                String type = refAnimalType.getAnimalType();
                refAnimalType = existingAnimalTypes.stream()
                        .filter(t -> type.equals(t.getAnimalType()) && value.equals(t.getAnimalPopulationUnits()))
                        .findFirst()
                        .orElseThrow(() -> new AgrosystImportException(String.format("Le type d'atelier %s (%s) est introuvable dans le référentiel.", type, value)));
                refCattleAnimalType.setRefAnimalType(refAnimalType);
            }
        });
        newOptionalColumn(COLUMN_ACTIVE, RefCattleAnimalType.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefCattleAnimalType, Object>> getColumnsForExport() {
        ModelBuilder<RefCattleAnimalType> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("animal_type", RefCattleAnimalType.PROPERTY_ANIMAL_TYPE);
        modelBuilder.newColumnForExport("livestock_type",
                refCattleAnimalType -> refCattleAnimalType.getRefAnimalType().getAnimalType());
        modelBuilder.newColumnForExport("livestock_population_unit",
                refCattleAnimalType -> refCattleAnimalType.getRefAnimalType().getAnimalPopulationUnits());
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefCattleAnimalType.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefCattleAnimalType newEmptyInstance() {
        RefCattleAnimalType refCattleAnimalType = new RefCattleAnimalTypeImpl();
        refCattleAnimalType.setActive(true);
        return refCattleAnimalType;
    }
}
