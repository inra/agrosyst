package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefDestinationImpl;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefDestinationModel extends AbstractDestinationAndPriceModel<RefDestination> implements ExportModel<RefDestination> {

    public RefDestinationModel() {
        super(CSV_SEPARATOR);
    }

    public RefDestinationModel(RefEspeceTopiaDao refEspeceDao) {
        super(CSV_SEPARATOR);
        super.refEspeceDao = refEspeceDao;
        getUpperCodeEspeceBotaniqueToCodeEspeceBotanique();

        newMandatoryColumn("code_espece_botanique", RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE, CODE_ESPECE_BOTANIQUE_PARSER);
        newMandatoryColumn("code_qualifiant_AEE", RefDestination.PROPERTY_CODE_QUALIFIANT__AEE, EMPTY_TO_NULL);
        newMandatoryColumn("code_destination_A", RefDestination.PROPERTY_CODE_DESTINATION__A);
        newMandatoryColumn("Filière", RefDestination.PROPERTY_SECTOR, SECTOR_PARSER);
        newMandatoryColumn("Espèce", RefDestination.PROPERTY_ESPECE);
        newMandatoryColumn("Valorisation_viticulture", RefDestination.PROPERTY_WINE_VALORISATION, VALORISATION_PARSER);
        newMandatoryColumn("Destinations", RefDestination.PROPERTY_DESTINATION);
        newMandatoryColumn("Unité_rendement", RefDestination.PROPERTY_YEALD_UNIT, YEALD_UNIT_PARSER);
        newMandatoryColumn("Source", RefDestination.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefDestination.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefDestination, Object>> getColumnsForExport() {
        ModelBuilder<RefDestination> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("code_espece_botanique", RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE);
        modelBuilder.newColumnForExport("code_destination_A", RefDestination.PROPERTY_CODE_DESTINATION__A);
        modelBuilder.newColumnForExport("code_qualifiant_AEE", RefDestination.PROPERTY_CODE_QUALIFIANT__AEE);
        modelBuilder.newColumnForExport("Filière", RefDestination.PROPERTY_SECTOR, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Espèce", RefDestination.PROPERTY_ESPECE);
        modelBuilder.newColumnForExport("Valorisation_viticulture", RefDestination.PROPERTY_WINE_VALORISATION, VALORISATION_FORMATTER);
        modelBuilder.newColumnForExport("Destinations", RefDestination.PROPERTY_DESTINATION);
        modelBuilder.newColumnForExport("Unité_rendement", RefDestination.PROPERTY_YEALD_UNIT, YEALD_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefDestination.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefDestination.PROPERTY_ACTIVE, T_F_FORMATTER);

        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefDestination newEmptyInstance() {
        RefDestination result = new RefDestinationImpl();
        result.setActive(true);
        return result;
    }
}
