package fr.inra.agrosyst.services.effective;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.services.action.CreateOrUpdateActionsContext;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleConnectionDto;
import fr.inra.agrosyst.api.services.effective.EffectiveInterventionDto;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class CreateOrUpdateEffectiveInterventionContext {
    protected final List<EffectiveInterventionDto> interventionDtos;
    protected final List<EffectiveCropCycleConnectionDto> connectionDtos;
    protected final Pair<String, EffectiveCropCycleNode> nodeIdToNode;
    protected final EffectivePerennialCropCycle perennialCropCycle;
    protected final EffectiveCropCyclePhase phase;
    protected final Domain domain;
    
    protected final Zone zone;
    protected final Map<String, EffectiveIntervention> idsToInterventions;
    protected final Map<String, ToolsCoupling> toolsCouplingByCode;
    
    protected final Collection<AbstractDomainInputStockUnit> domainInputStockUnits;

    protected final CroppingPlanEntry mainCrop;
    protected final CroppingPlanEntry intermediateCrop;

    // for Valorisation validation
    protected final Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
    protected final Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant;

    private final List<CreateOrUpdateActionsContext> createOrUpdateActionsContexts = new ArrayList<>();

    public CreateOrUpdateEffectiveInterventionContext(
            List<EffectiveInterventionDto> interventionDtos,
            List<EffectiveCropCycleConnectionDto> connectionDtos,
            Pair<String, EffectiveCropCycleNode> nodeIdToNode,
            EffectivePerennialCropCycle perennialCropCycle,
            EffectiveCropCyclePhase phase,
            Domain domain,
            Zone zone,
            Map<String, EffectiveIntervention> idsToInterventions,
            Collection<AbstractDomainInputStockUnit> domainInputStockUnits,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, ToolsCoupling> toolsCouplingByCode,
            CroppingPlanEntry mainCrop,
            CroppingPlanEntry intermediateCrop) {
        
        this.interventionDtos = interventionDtos;
        this.connectionDtos = connectionDtos;
        this.nodeIdToNode = nodeIdToNode;
        this.perennialCropCycle = perennialCropCycle;
        this.phase = phase;
        this.domain = domain;
        this.zone = zone;
        this.idsToInterventions = idsToInterventions;
        this.domainInputStockUnits = domainInputStockUnits;
        this.speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
        this.sectorByCodeEspceBotaniqueCodeQualifiant = sectorByCodeEspceBotaniqueCodeQualifiant;
        this.toolsCouplingByCode = toolsCouplingByCode;
        this.mainCrop = mainCrop;
        this.intermediateCrop = intermediateCrop;
    }

    public static CreateOrUpdateEffectiveInterventionContext createPerennialInterventionCreateOrUpdateContext(
            List<EffectiveInterventionDto> interventionDtos,
            EffectivePerennialCropCycle perennialCropCycle,
            EffectiveCropCyclePhase phase,
            Domain domain,
            Zone zone, Map<String, EffectiveIntervention> idsToInterventions,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, ToolsCoupling> toolsCouplingByCode,
            Collection<AbstractDomainInputStockUnit> domainInputStockUnits,
            CroppingPlanEntry mainCrop) {
        
        CreateOrUpdateEffectiveInterventionContext result = new CreateOrUpdateEffectiveInterventionContext(
                interventionDtos,
                null, null,
                perennialCropCycle,
                phase,
                domain,
                zone, idsToInterventions,
                domainInputStockUnits,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspceBotaniqueCodeQualifiant,
                toolsCouplingByCode,
                mainCrop,
                null);
        return result;
    }

    public static CreateOrUpdateEffectiveInterventionContext createSeasonalInterventionCreateOrUpdateContext(
            List<EffectiveInterventionDto> interventionDtos,
            List<EffectiveCropCycleConnectionDto> connectionDtos,
            Pair<String, EffectiveCropCycleNode> nodeIdToNode,
            Domain domain,
            Zone zone,
            Map<String, EffectiveIntervention> idsToInterventions,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, ToolsCoupling> toolsCouplingByCode,
            Collection<AbstractDomainInputStockUnit> domainInputStockUnits,
            CroppingPlanEntry targetedCrop,
            CroppingPlanEntry targetedIntermediateCrop) {

        CreateOrUpdateEffectiveInterventionContext result = new CreateOrUpdateEffectiveInterventionContext(
                interventionDtos,
                connectionDtos,
                nodeIdToNode,
                null,
                null,
                domain,
                zone,
                idsToInterventions,
                domainInputStockUnits,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspceBotaniqueCodeQualifiant,
                toolsCouplingByCode,
                targetedCrop,
                targetedIntermediateCrop);
        return result;
    }

    public List<EffectiveInterventionDto> getInterventionDtos() {
        return interventionDtos;
    }

    public List<EffectiveCropCycleConnectionDto> getConnectionDtos() {
        return connectionDtos;
    }

    public Pair<String, EffectiveCropCycleNode> getNodeIdToNode() {
        return nodeIdToNode;
    }

    public EffectivePerennialCropCycle getPerennialCropCycle() {
        return perennialCropCycle;
    }

    public EffectiveCropCyclePhase getPhase() {
        return phase;
    }

    public Domain getDomain() {
        return domain;
    }

    public Map<String, EffectiveIntervention> getIdsToInterventions() {
        return idsToInterventions;
    }

    public List<CreateOrUpdateActionsContext> getCreateOrUpdateActionsContexts() {
        return createOrUpdateActionsContexts;
    }

    public void addCreateOrUpdateActionsContext(CreateOrUpdateActionsContext createOrUpdateActionsContext) {
        createOrUpdateActionsContexts.add(createOrUpdateActionsContext);
    }

    public Map<String, List<Pair<String, String>>> getSpeciesCodeToCodeEspeceBotaniqueCodeQualifiantAee() {
        return speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
    }

    public Map<String, List<Sector>> getSectorByCodeEspceBotaniqueCodeQualifiant() {
        return sectorByCodeEspceBotaniqueCodeQualifiant;
    }

    public Map<String, ToolsCoupling> getToolsCouplingByCode() {
        return toolsCouplingByCode;
    }
    
    public Zone getZone() {
        return zone;
    }
    
    public Collection<AbstractDomainInputStockUnit> getDomainInputStockUnits() {
        return CollectionUtils.emptyIfNull(domainInputStockUnits);
    }

    public CroppingPlanEntry getMainCrop() {
        return mainCrop;
    }

    public CroppingPlanEntry getIntermediateCrop() {
        return intermediateCrop;
    }
}
