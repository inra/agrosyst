package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.performance.UsageQsaResult;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Classe calculant l'indicateur QSA Cuivre Total.
 * <p>
 * QSA Cuivre total = QSA Cuivre phytosanitaire + QSA Cuivre fertilisation
 */
public class IndicatorCopperTotalProduct extends AbstractIndicator {

    private static final String COLUMN_NAME = "qsa_cuivre_tot";
    private static final String COLUMN_NAME_HTS = "qsa_cuivre_tot_hts";

    protected static final String[] LABELS = {
            "Indicator.label.copperTotalProduct",
            "Indicator.label.copperTotalProduct_hts"
    };

    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INPUT,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    protected boolean isWithSeedingTreatment = true;
    protected boolean isWithoutSeedingTreatment = true;

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME_HTS);
        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {
        if (interventionContext.isFictive()) {
            return newArray(LABELS.length, 0.0);
        }


        return this.computeQuantities(writerContext, interventionContext);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {

        return this.computeQuantities(writerContext, interventionContext);
    }

    private Double[] computeQuantities(WriterContext writerContext, PerformanceInterventionContext interventionContext) {

        IndicatorWriter writer = writerContext.getWriter();
        List<UsageQsaResult> usageFertiCuQuantities = interventionContext.getUsageFertiCuQuantities();
        writeUsageInput(usageFertiCuQuantities, writer, writerContext, 0);

        List<UsageQsaResult> usagePhytoCuQuantities = interventionContext.getUsagePhytoCuQuantities();
        writeUsageInput(usagePhytoCuQuantities, writer, writerContext, 0);

        List<UsageQsaResult> usagePhytoCuQuantitiesHts = interventionContext.getUsagePhytoCuQuantitiesHts();
        writeUsageInput(usagePhytoCuQuantitiesHts, writer, writerContext, 1);

        if (interventionContext.getFertiCuQuantity() != null || interventionContext.getPhytoCuQuantity() != null || interventionContext.getPhytoCuQuantityHts() != null) {
            Double[] quantities = newArray(LABELS.length, 0.0d);

            quantities[0] = getDouble(interventionContext.getFertiCuQuantity()) + getDouble(interventionContext.getPhytoCuQuantity());
            //  pour le ferti, il n'y a pas de version d'indicateur hors traitement de semence puisqu'un traitement de semence ne peut pas être un fertilisant
            quantities[1] = getDouble(interventionContext.getFertiCuQuantity()) + getDouble(interventionContext.getPhytoCuQuantityHts());

            return quantities;
        }

        return null;
    }

    private void writeUsageInput(List<UsageQsaResult> usageFertiCuQuantities, IndicatorWriter writer, WriterContext writerContext, int i) {
        if (isDisplayed(ExportLevel.INPUT, i)) {
            for (UsageQsaResult usageResult : usageFertiCuQuantities) {
                AbstractAction action = usageResult.abstractAction();
                AbstractInputUsage usage = usageResult.usage();
                Double value = usageResult.value();

                Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(usage.getTopiaId());
                String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(
                        usage.getTopiaId(),
                        MissingMessageScope.INPUT);

                writer.writeInputUsage(
                        writerContext.getIts(),
                        writerContext.getIrs(),
                        getIndicatorCategory(),
                        getIndicatorLabel(i),
                        usage,
                        action,
                        writerContext.getEffectiveIntervention(),
                        writerContext.getPracticedIntervention(),
                        writerContext.getCodeAmmBioControle(),
                        writerContext.getAnonymizeDomain(),
                        writerContext.getAnonymizeGrowingSystem(),
                        writerContext.getPlot(),
                        writerContext.getZone(),
                        writerContext.getPracticedSystem(),
                        writerContext.getCroppingPlanEntry(),
                        writerContext.getPracticedPhase(),
                        writerContext.getSolOccupationPercent(),
                        writerContext.getEffectivePhase(),
                        writerContext.getRank(),
                        writerContext.getPreviousPlanEntry(),
                        writerContext.getIntermediateCrop(),
                        this.getClass(),
                        value,
                        reliabilityIndexForInputId,
                        reliabilityCommentForInputId,
                        "",
                        "",
                        "",
                        writerContext.getGroupesCiblesByCode());

            }
        }
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, "Indicator.category.activeSubstances");
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return isRelevant(atLevel) && displayed && (i == 0 && isWithSeedingTreatment || i == 1 && isWithoutSeedingTreatment);
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
        if (displayed) {
            isWithSeedingTreatment = filter.getWithSeedingTreatment();
            isWithoutSeedingTreatment = filter.getWithoutSeedingTreatment();
        }
    }
}
