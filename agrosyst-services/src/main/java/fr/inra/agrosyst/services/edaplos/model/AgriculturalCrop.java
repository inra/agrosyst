package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2017 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * {@code
 * <ram:BotanicalSpeciesCode listAgencyName="FR, AGRO EDI EUROPE" listAgencyID="312">ZCS</ram:BotanicalSpeciesCode>
 * <ram:SupplementaryBotanicalSpeciesCode listAgencyName="FR, AGRO EDI EUROPE" listAgencyID="312"></ram:SupplementaryBotanicalSpeciesCode>
 * <ram:Description>Maïs Fourrage</ram:Description>
 * }
 * </pre>
 */
public class AgriculturalCrop implements AgroEdiObject {

    protected String botanicalSpeciesCode;

    protected String supplementaryBotanicalSpeciesCode;

    protected String description;

    protected String sowingPeriodCode;

    protected List<CropSpeciesVariety> sownCropSpeciesVarietys = new ArrayList<>();

    public String getBotanicalSpeciesCode() {
        return botanicalSpeciesCode;
    }

    public void setBotanicalSpeciesCode(String botanicalSpeciesCode) {
        this.botanicalSpeciesCode = botanicalSpeciesCode;
    }

    public String getSupplementaryBotanicalSpeciesCode() {
        return supplementaryBotanicalSpeciesCode;
    }

    public void setSupplementaryBotanicalSpeciesCode(String supplementaryBotanicalSpeciesCode) {
        this.supplementaryBotanicalSpeciesCode = supplementaryBotanicalSpeciesCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSowingPeriodCode() {
        return sowingPeriodCode;
    }

    public void setSowingPeriodCode(String sowingPeriodCode) {
        this.sowingPeriodCode = sowingPeriodCode;
    }

    public List<CropSpeciesVariety> getSownCropSpeciesVarietys() {
        return sownCropSpeciesVarietys;
    }

    public void setSownCropSpeciesVarietys(List<CropSpeciesVariety> sownCropSpeciesVarietys) {
        this.sownCropSpeciesVarietys = sownCropSpeciesVarietys;
    }

    public void addSownCropSpeciesVariety(CropSpeciesVariety sownCropSpeciesVariety) {
        sownCropSpeciesVarietys.add(sownCropSpeciesVariety);
    }

    @Override
    public String getLocalizedIdentifier() {
        return "culture '" + botanicalSpeciesCode + "'";
    }
}
