package fr.inra.agrosyst.services.common.export;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.inra.agrosyst.api.entities.TypeDEPHY;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ValueFormatter;

import java.util.EnumSet;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class ExportUtils {

    private static final Log LOGGER = LogFactory.getLog(ExportUtils.class);

    protected static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final String NULL_SOURCE_PLACEHOLDER = "N/A";
    public static final String NO_VALUE = "-";

    /**
     * Return a iterable list of String representation of all enum element for {@code enumClass}.
     * 
     * A simplier way is Arrays.asList(XXX.values());
     * But this method could handle real translation later.
     * 
     * @param enumClazz enum class
     * @return list of string
     */
    public static <E extends Enum<E>> Iterable<String> allStringOf(Class<E> enumClazz) {
        // XXX: toString() for now, could be replaced by translation later
        return EnumSet.allOf(enumClazz).stream().map(e -> e != null ? e.name() : null).collect(Collectors.toList());
    }

    public static final ValueFormatter<TypeDEPHY> TYPE_DEPHY_FORMATTER = input -> {
        String result = switch (input) {
            case DEPHY_EXPE -> "DEPHY-EXPE";
            case DEPHY_FERME -> "DEPHY-FERME";
            case NOT_DEPHY -> "Hors DEPHY";
            default -> throw new UnsupportedOperationException("Unexpected TypeDEPHY: " + input);
        };
        // XXX: toString() for now, could be replaced by translation later
        return result;
    };

}
