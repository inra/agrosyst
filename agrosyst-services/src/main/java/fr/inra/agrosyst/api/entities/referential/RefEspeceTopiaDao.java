package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class RefEspeceTopiaDao extends AbstractRefEspeceTopiaDao<RefEspece> {

    /**
     * Search all RefCommuneValues matching user navigation context and custom additional {@code filter}.
     *
     * @param filter custom filter
     * @return matching domains
     *
     */
    public List<RefEspece> findActiveEspeces(Language language, String filter, int maxResults) {
        if (Strings.isNullOrEmpty(filter)) {
            return new ArrayList<>();
        }

        I18nDaoHelper libelleEspecei18n = I18nDaoHelper.withSimpleI18nKey(RefEspece.PROPERTY_LIBELLE_ESPECE_BOTANIQUE, language, TradRefVivant.class, "re");
        I18nDaoHelper libelleQualifiantAEEi18n = I18nDaoHelper.withSimpleI18nKey(RefEspece.PROPERTY_LIBELLE_QUALIFIANT__AEE, language, TradRefVivant.class, "re");
        I18nDaoHelper libelleTypeSaisonnierAEEi18n = I18nDaoHelper.withSimpleI18nKey(RefEspece.PROPERTY_LIBELLE_TYPE_SAISONNIER__AEE, language, TradRefVivant.class, "re");
        I18nDaoHelper libelleDestinationAEEi18n = I18nDaoHelper.withSimpleI18nKey(RefEspece.PROPERTY_LIBELLE_DESTINATION__AEE, language, TradRefVivant.class, "re");

        List<String> transAliases = new ArrayList<>();
        transAliases.add(libelleEspecei18n.getAlias());
        transAliases.add(libelleQualifiantAEEi18n.getAlias());
        transAliases.add(libelleTypeSaisonnierAEEi18n.getAlias());
        transAliases.add(libelleDestinationAEEi18n.getAlias());

        String query = "SELECT re," + String.join(", ", transAliases);
        query += "FROM " + getEntityClass().getName() + " re";
        query += libelleEspecei18n.leftJoinTranslation();
        query += libelleQualifiantAEEi18n.leftJoinTranslation();
        query += libelleTypeSaisonnierAEEi18n.leftJoinTranslation();
        query += libelleDestinationAEEi18n.leftJoinTranslation();
        query += " WHERE re." + RefEspece.PROPERTY_ACTIVE + " = true";
        Map<String, Object> args = Maps.newLinkedHashMap();

        String escapedFilter = StringUtils.stripAccents(filter.trim());
        query += " AND " + DaoUtils.getFieldLikeInsensitive(libelleEspecei18n.coalesceTranslation(), ":escapedFilter");
        args.put("escapedFilter", "%"+escapedFilter+"%");

        query += " ORDER BY " + libelleEspecei18n.coalesceTranslation();

        List<Object[]> resultWithTranslation = find(query, args, 0, maxResults);
        List<RefEspece> result = resultWithTranslation.stream()
                .map(objects -> {
                    RefEspece refEspece = (RefEspece)objects[0];
                    refEspece.setLibelle_espece_botanique_Translated(I18nDaoHelper.tryGetTranslation(objects[1]).orElse(refEspece.getLibelle_espece_botanique()));
                    refEspece.setLibelle_qualifiant_AEE_Translated(I18nDaoHelper.tryGetTranslation(objects[2]).orElse(refEspece.getLibelle_qualifiant_AEE()));
                    refEspece.setLibelle_type_saisonnier_AEE_Translated(I18nDaoHelper.tryGetTranslation(objects[3]).orElse(refEspece.getLibelle_type_saisonnier_AEE()));
                    refEspece.setLibelle_destination_AEE_Translated(I18nDaoHelper.tryGetTranslation(objects[4]).orElse(refEspece.getLibelle_destination_AEE()));
                    return refEspece;
                })
                .collect(Collectors.toList());

        return result;
    }

    public Map<String, String> getUpperCodeEspeceBotaniqueToCodeEspeceBotanique() {
        String query = "SELECT UPPER(re." + RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE+ "), re." + RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE +
                " FROM " + getEntityClass().getName() + " re ";

        List<Object[]> allCodes = findAll(query);
        HashMap<String, String> result = new HashMap<>();

        if (allCodes != null) {
            for (Object[] entry : allCodes) {
                String upperCode = (String) entry[0];
                String code = (String) entry[1];
                result.put(upperCode, code);
            }
        }

        return result;
    }

    /**
     *
     * @return code_espece_botanique + "_" + code_qualifiant_AEE
     */
    public List<String> findAllKeys() {
        List<String> result = new ArrayList<>();

        String query = "SELECT re." + RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE + "," +
                "              re." + RefEspece.PROPERTY_CODE_QUALIFIANT__AEE +
                "       FROM " + getEntityClass().getName() + " re";

        List<Object[]> result0 = findAll(query);

        for (Object[] objects : result0) {
            String rowResult = objects[0] + "_" + objects[1];
            result.add(rowResult);
        }

        return result;
    }

    public List<Pair<RefEspece, RefVariete>> findEspeceAndVariete() {
        Map<String, Object> args = new HashMap<>();

        String query = "SELECT re, rvg " +
                "       FROM " + RefEspece.class.getName() + " re, " + RefVarieteGeves.class.getName() + " rvg , " + RefEspeceToVariete.class.getName() +  " retv " +
                "       WHERE 1 = 1";
        query += DaoUtils.andAttributeLike("retv", RefEspeceToVariete.PROPERTY_REFERENTIEL_SOURCE, args, ReferentialService.VARIETE_GEVES);
        query += "      AND re." + RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE + "= retv." + RefEspeceToVariete.PROPERTY_CODE_ESPECE_EDI;
        query += "      AND CONCAT('', rvg." + RefVarieteGeves.PROPERTY_NUM__ESPECE__BOTANIQUE + ", '') = retv." + RefEspeceToVariete.PROPERTY_CODE_ESPECE_AUTRE_REFERENTIEL;
        query += "      AND re." + RefEspece.PROPERTY_ACTIVE + " = true ";
        query += "      AND re." + RefEspeceToVariete.PROPERTY_ACTIVE + " = true ";
        query += "      AND re." + RefVarieteGeves.PROPERTY_ACTIVE + " = true ";

        List<Object[]> res = findAll(query, args);

        List<Pair<RefEspece, RefVariete>> refEspeceRefVariete = new ArrayList<>();
        Set<String> refEspeceIds = Sets.newHashSet();
        for (Object[] re : res) {
            RefEspece refEspece = (RefEspece) re[0];
            RefVarieteGeves refVarieteGeves = (RefVarieteGeves) re[1];
            refEspeceRefVariete.add(Pair.of(refEspece, refVarieteGeves));
            refEspeceIds.add(refEspece.getTopiaId());
        }

        args.clear();
        query = " SELECT re, rvpg " +
                " FROM " + RefEspece.class.getName() + " re, " + RefVarietePlantGrape.class.getName() + " rvpg , " + RefEspeceToVariete.class.getName() +  " retv " +
                " WHERE 1 = 1";
        query += " AND LOWER (retv." + RefEspeceToVariete.PROPERTY_REFERENTIEL_SOURCE + ") NOT LIKE '" + ReferentialService.VARIETE_GEVES + "'";
        query += " AND re." + RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE + "= retv." + RefEspeceToVariete.PROPERTY_CODE_ESPECE_EDI;

        res = findAll(query, args);

        for (Object[] re : res) {
            RefEspece refEspece = (RefEspece) re[0];
            RefVarietePlantGrape refVarieteGeves = (RefVarietePlantGrape) re[1];
            refEspeceRefVariete.add(Pair.of(refEspece, refVarieteGeves));
            refEspeceIds.add(refEspece.getTopiaId());
        }

        args.clear();
        query = "  FROM " + RefEspece.class.getName() + " re ";
        query += " WHERE 1 = 1 ";
        query += DaoUtils.andAttributeNotIn("re", RefEspece.PROPERTY_TOPIA_ID, args, refEspeceIds);
        query += " AND re." + RefEspece.PROPERTY_ACTIVE + " = true ";

        List<RefEspece> res0 = findAll(query, args);
        for (RefEspece refEspece : res0) {
            refEspeceRefVariete.add(Pair.of(refEspece, null));
        }

        return refEspeceRefVariete;
    }

} //RefEspeceTopiaDao<E extends RefEspece>
