package fr.inra.agrosyst.api.entities;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.support.TopiaSqlQuery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Classe qui a pour but de faciliter l'utilisation des TopiaSqlQuery.
 *
 * @see TopiaSqlQuery
 */
public abstract class SimpleSqlQuery<T> extends TopiaSqlQuery<T> {

    final String sqlQuery;
    final List<String> sqlArgs;

    public SimpleSqlQuery(String sqlQuery, List<String> sqlArgs) {
        this.sqlQuery = sqlQuery;
        this.sqlArgs = sqlArgs;
    }

    @Override
    public Optional<String> getSqlQuery() {
        Optional<String> result = Optional.of(sqlQuery);
        return result;
    }

    @Override
    public Optional<List<?>> getSqlArgs() {
        Optional<List<?>> result = Optional.of(sqlArgs);
        return result;
    }

    @Override
    public PreparedStatement prepareQuery(Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(sqlQuery);
        for (int i = 0; i < sqlArgs.size(); i++) {
            statement.setString(i+1, sqlArgs.get(i));
        }
        return statement;
    }

}
