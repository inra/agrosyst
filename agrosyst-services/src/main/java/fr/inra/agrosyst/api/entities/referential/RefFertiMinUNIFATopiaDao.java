package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.services.referential.MineralProductType;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author David Cossé
 */
public class RefFertiMinUNIFATopiaDao extends AbstractRefFertiMinUNIFATopiaDao<RefFertiMinUNIFA> {

    public static final String ALIAS = "ferti";
    public static final StringBuilder ORDER_BY_BUILDER = new StringBuilder();

    static {
        List<String> properties = List.of(
                RefFertiMinUNIFA.PROPERTY_N, RefFertiMinUNIFA.PROPERTY_P2_O5, RefFertiMinUNIFA.PROPERTY_K2_O,
                RefFertiMinUNIFA.PROPERTY_BORE, RefFertiMinUNIFA.PROPERTY_FER, RefFertiMinUNIFA.PROPERTY_CALCIUM,
                RefFertiMinUNIFA.PROPERTY_MANGANESE, RefFertiMinUNIFA.PROPERTY_MOLYBDENE,
                RefFertiMinUNIFA.PROPERTY_MG_O, RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM,
                RefFertiMinUNIFA.PROPERTY_S_O3, RefFertiMinUNIFA.PROPERTY_CUIVRE, RefFertiMinUNIFA.PROPERTY_ZINC);
        for (String prop : properties) {
            if (ORDER_BY_BUILDER.isEmpty()) {
                ORDER_BY_BUILDER.append(" ORDER BY ");
            } else {
                ORDER_BY_BUILDER.append(", ");
            }
            ORDER_BY_BUILDER.append(ALIAS + ".").append(prop).append(" ASC ");
        }
    }
    public static final String FERTI_ORDER_BY = ORDER_BY_BUILDER.toString();

    /**
     * return all categories and ProductTypes from the actives RefFertiMinUNIFA
     * @return all categories and ProductTypes from the actives RefFertiMinUNIFA
     */
    public List<MineralProductType> findAllActiveFertiMinProductType(Language language) {
        List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(language);

        String hql = "SELECT " + ALIAS + "." + RefFertiMinUNIFA.PROPERTY_CATEG + "," +
                I18nDaoHelper.buildQuerySelectTranslationProperties(i18nDaoHelpers) + ", " +
                ALIAS + "." + RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT + ", " +
                ALIAS + "." + RefFertiMinUNIFA.PROPERTY_FORME + " " +
                newFromClause(ALIAS) + " " +
                I18nDaoHelper.buildQueryJoinTranslations(i18nDaoHelpers) +
                " WHERE " + ALIAS + "." + RefFertiMinUNIFA.PROPERTY_ACTIVE + " IS true" +
                " GROUP BY " + ALIAS + "." + RefFertiMinUNIFA.PROPERTY_CATEG + ", " +
                    I18nDaoHelper.buildQuerySelectTranslationProperties(i18nDaoHelpers) + "," +
                    ALIAS + "." + RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT + ", " +
                    ALIAS + "." + RefFertiMinUNIFA.PROPERTY_FORME +
                " ORDER BY " + I18nDaoHelper.buildQuerySelectTranslationProperties(i18nDaoHelpers) + "," +
                    ALIAS + "." + RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT + ", " +
                    ALIAS + "." + RefFertiMinUNIFA.PROPERTY_FORME;

        Map<String, Object> args = new HashMap<>();
        List<Object[]> mineralProducts = findAll(hql, args);
        List<MineralProductType> result = new ArrayList<>();
        Map<Integer, MineralProductType> typesIndex = new HashMap<>();
        for (Object[] tuple : mineralProducts) {
            Integer categ = (Integer) tuple[0];
            String typeProdTranslated = tuple[1] != null ? (String) tuple[1] : null;
            String typeProd = tuple[3] != null ? (String) tuple[3] : null;
            String shapeTranslated = tuple[2] != null ? (String) tuple[2] :  null;
            String shape = tuple[4] != null ? (String) tuple[4] : null;
            MineralProductType type = typesIndex.get(categ);

            Pair<String, String> shapeWithTranslation = null;
            if (shape != null) {
                shapeWithTranslation = Pair.of(shape, StringUtils.firstNonBlank(shapeTranslated, shape));
            }

            Pair<String, String> typeProdWithTranslation = Pair.of(typeProd, StringUtils.firstNonBlank(typeProdTranslated, typeProd));

            if (type == null) {
                type = new MineralProductType(categ, typeProdWithTranslation, shapeWithTranslation);
                result.add(type);
                typesIndex.put(categ, type);
            } else {
                type.addShape(shapeWithTranslation);
            }
        }
        return result;
    }

    public String findMineralProductTranslatedTypeProduit(Language language, String typeProduit) {
        I18nDaoHelper i18nDaoHelper = I18nDaoHelper.withComplexI18nKey(RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, language, TradRefIntrant.class, ALIAS);
        ArrayList<I18nDaoHelper> helpers = Lists.newArrayList(i18nDaoHelper);

        Map<String, Object> args = DaoUtils.asArgsMap();

        String hql = "SELECT DISTINCT(" + I18nDaoHelper.buildQuerySelectTranslationProperties(helpers) + ") " +
                newFromClause(ALIAS) + " " +
                I18nDaoHelper.buildQueryJoinTranslations(helpers) +
                " WHERE 1=1 " +
                DaoUtils.andAttributeEquals(ALIAS, RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, args, typeProduit);

        String trad = findUniqueOrNull(hql, args);
        return trad;
    }

    public ReferentialTranslationMap fillTranslations(Collection<String> topiaIdList, ReferentialTranslationMap translationMap) {
        List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(translationMap.getLanguage());
        I18nDaoHelper.fillRefEntitiesTranslations(this, ALIAS, i18nDaoHelpers, topiaIdList, translationMap);
        return translationMap;
    }

    protected List<I18nDaoHelper> getI18nDaoHelpers(Language language) {
        return List.of(
            I18nDaoHelper.withComplexI18nKey(RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, language, TradRefIntrant.class, ALIAS),
            I18nDaoHelper.withSimpleI18nKey(RefFertiMinUNIFA.PROPERTY_FORME, language, TradRefIntrant.class, ALIAS)
        );
    }

    public List<String> findNotAgrosystUserRefFertiMinUnifTypeProduitForCateg(Integer categ) {
        String query = "SELECT DISTINCT rfmu." + RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT + " FROM " + RefFertiMinUNIFA.class.getName() + " rfmu ";
        query += " WHERE rfmu." + RefFertiMinUNIFA.PROPERTY_CATEG + " = :categ";
        query += " AND rfmu." + RefFertiMinUNIFA.PROPERTY_SOURCE + " != :agrosyst_user";
        List<String> result = findAll(query , DaoUtils.asArgsMap("categ", categ, "agrosyst_user", UserService.AGROSYST_USER));
        return result;
    }

    public List<RefFertiMinUNIFA> findAllActiveRefFertiMinUnifaByCategAndShape(Integer categ, String fertilizerShape) {
        Map<String, Object> args = new HashMap<>();
        String query = "FROM " + RefFertiMinUNIFA.class.getName() + " " + ALIAS;
        query += " WHERE 1=1 ";
        query += DaoUtils.andAttributeEquals(ALIAS, RefFertiMinUNIFA.PROPERTY_CATEG, args, categ);
        query += DaoUtils.andAttributeEquals(ALIAS, RefFertiMinUNIFA.PROPERTY_FORME, args, fertilizerShape);
        query += DaoUtils.andAttributeEquals(ALIAS, RefFertiMinUNIFA.PROPERTY_ACTIVE, args, true);

        List<RefFertiMinUNIFA> result = findAll(query + FERTI_ORDER_BY, args);

        return result;
    }

}
