package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.SimpleSqlQuery;
import fr.inra.agrosyst.api.services.common.InputPriceFilter;
import fr.inra.agrosyst.api.services.domain.inputStock.InputPriceDto;
import fr.inra.agrosyst.api.services.performance.Scenario;
import fr.inra.agrosyst.api.services.performance.ScenarioFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static fr.inra.agrosyst.api.FormatUtils.parseIdTraitementIdProduit;

public class RefInputPriceTopiaDao extends AbstractRefInputPriceTopiaDao<RefInputPrice> {
    
    static final String GROUP_BY_PART = " GROUP BY rp." + RefInputPrice.PROPERTY_CODE_SCENARIO + ", rp." + RefInputPrice.PROPERTY_SCENARIO;
    
    static final String ORDER_PART = " ORDER BY rp." + RefInputPrice.PROPERTY_SCENARIO;
    
    public PaginationResult<Scenario> findAllScenariosByScenarioCodeForName(ScenarioFilter filter) {
    
        Map<String, Object> args = new LinkedHashMap<>();
        List<Scenario> loadedScenarios = new ArrayList<>();
        Set<String> loadedScenarioCodes = new HashSet<>();
    
        StringBuilder filterQueryPart = getScenarioFilterCommonBodyPart(filter, args);
    
        loadRefInputPriceScenarios(args, filterQueryPart, loadedScenarios, loadedScenarioCodes);
    
        loadRefHarvestingScenarios(args, filterQueryPart, loadedScenarios, loadedScenarioCodes);
    
        loadedScenarios.sort(Scenario.LABEL_SCENARIO_COMPARATOR);
    
        final int size = loadedScenarios.isEmpty() ? DaoUtils.NO_PAGE_LIMIT : loadedScenarios.size();
        PaginationParameter pager = PaginationParameter.of(0, size);

        return PaginationResult.of(loadedScenarios, loadedScenarios.size(), pager);
    }
    
    protected void loadRefInputPriceScenarios(Map<String, Object> args,
                                              StringBuilder filterQueryPart,
                                              Collection<Scenario> loadedScenarios,
                                              Collection<String> loadedScenarioCodes) {
        
        String selectPart = "SELECT DISTINCT rp." + RefInputPrice.PROPERTY_CODE_SCENARIO + ", rp." + RefInputPrice.PROPERTY_SCENARIO;
        
        String fromPart = " FROM " + RefInputPrice.class.getName() + " rp";
        
        List<Object[]> inputPriceResults = findAll(selectPart + fromPart + filterQueryPart.toString() + GROUP_BY_PART + ORDER_PART, args);
        
        transformeQueryResultToScenarios(inputPriceResults, loadedScenarios, loadedScenarioCodes);
    }
    
    protected void loadRefHarvestingScenarios(Map<String, Object> args,
                                              StringBuilder filterQueryPart,
                                              Collection<Scenario> loadedScenarios,
                                              Collection<String> loadedScenarioCodes) {
        String selectPart;
        String fromPart;
        selectPart = "SELECT DISTINCT rp." + RefHarvestingPrice.PROPERTY_CODE_SCENARIO + ", rp." + RefHarvestingPrice.PROPERTY_SCENARIO;
        
        fromPart = " FROM " + RefHarvestingPrice.class.getName() + " rp";
        
        // avoid loading twice same scenario
        if (!loadedScenarioCodes.isEmpty()) {
            filterQueryPart.append(DaoUtils.andAttributeNotIn("rp", RefHarvestingPrice.PROPERTY_CODE_SCENARIO, args, loadedScenarioCodes));
        }
        
        List<Object[]> harvestingPriceResults = findAll(selectPart + fromPart + filterQueryPart.toString() + GROUP_BY_PART + ORDER_PART, args);
        
        transformeQueryResultToScenarios(harvestingPriceResults, loadedScenarios, loadedScenarioCodes);
    }
    
    protected void transformeQueryResultToScenarios(Collection<Object[]> inputPriceResults,
                                                    Collection<Scenario> loadedScenarios,
                                                    Collection<String> loadedScenarioCodes) {
        for (Object[] row : inputPriceResults) {
            final String code = (String) row[0];
            final String label = (String) row[1];
            loadedScenarios.add(new Scenario(code, label));
            loadedScenarioCodes.add(code);
        }
    }
    
    /**
     * Same for RefInputPrice and RefHarvestingPrice
     * @param filter user label part
     * @param args the query args
     * @return common query filter part
     */
    protected StringBuilder getScenarioFilterCommonBodyPart(ScenarioFilter filter, Map<String, Object> args) {
        StringBuilder filterQueryPart = new StringBuilder(" WHERE 1 = 1");
        
        // apply non null filter
        if (filter != null) {
        
            // name
            filterQueryPart.append(DaoUtils.andAttributeLike("rp", RefInputPrice.PROPERTY_SCENARIO, args, filter.getScenarioName()));
        }
        
        filterQueryPart.append(DaoUtils.andAttributeEquals("rp", RefInputPrice.PROPERTY_ACTIVE, args, Boolean.TRUE));
        
        filterQueryPart.append(" AND rp." + RefInputPrice.PROPERTY_CODE_SCENARIO + " IS NOT NULL ");
        
        filterQueryPart.append(" AND rp." + RefInputPrice.PROPERTY_CODE_SCENARIO + " != '' ");
        
        return filterQueryPart;
    }
    
    public Collection<Scenario> findAllScenariosForCodes(Collection<String> scenarioCodes) {
    
        Map<String, Object> args = new LinkedHashMap<>();
        
        List<Scenario> loadedScenarios = new ArrayList<>();
        Set<String> loadedScenarioCodes = new HashSet<>();
    
        StringBuilder filterQueryPart = getScenariosForCodesCommonBodyPart(scenarioCodes, args);
    
        loadRefInputPriceScenarios(args, filterQueryPart, loadedScenarios, loadedScenarioCodes);
    
        loadRefHarvestingScenarios(args, filterQueryPart, loadedScenarios, loadedScenarioCodes);
    
        loadedScenarios.sort(Scenario.LABEL_SCENARIO_COMPARATOR);
        
        return loadedScenarios;
    }
    
    protected StringBuilder getScenariosForCodesCommonBodyPart(Collection<String> scenarioCodes, Map<String, Object> args) {
        StringBuilder filterQueryPart = new StringBuilder(" WHERE 1 = 1");
        
        if (CollectionUtils.isNotEmpty(scenarioCodes)) {

            if (scenarioCodes.size() == 1) {
                String scenarioCode = scenarioCodes.iterator().next();
                filterQueryPart.append(DaoUtils.andAttributeEquals("rp", RefInputPrice.PROPERTY_CODE_SCENARIO, args, scenarioCode));
            } else {
                filterQueryPart.append("AND rp.code_scenario in (:code_scenario0)");
                args.put("code_scenario0", scenarioCodes);
            }

        }
        
        filterQueryPart.append(DaoUtils.andAttributeEquals("rp", RefInputPrice.PROPERTY_ACTIVE, args, Boolean.TRUE));
        
        return filterQueryPart;
    }


    public List<RefPrixFertiMin> findRefPrixFertiMinForCampaigns(int categ, String forme, Set<FertiMinElement> fertiMinElements, Set<Integer> campaignRange) {

        if (CollectionUtils.isEmpty(fertiMinElements) || CollectionUtils.isEmpty(campaignRange)) {
            return new ArrayList<>();
        }

        Map<String, Object> args = new HashMap<>();
        String query = " FROM " + RefPrixFertiMin.class.getName() + " rpfm ";
        query += "WHERE 1 = 1";
        query += DaoUtils.andAttributeEquals("rpfm", RefPrixFertiMin.PROPERTY_CATEG, args, categ);
        query += DaoUtils.andAttributeLike("rpfm", RefPrixFertiMin.PROPERTY_FORME, args, forme);
        if (fertiMinElements.size() > 1) {
            query += DaoUtils.andAttributeIn("rpfm", RefPrixFertiMin.PROPERTY_ELEMENT, args, fertiMinElements);
        } else {
            query += DaoUtils.andAttributeEquals("rpfm", RefPrixFertiMin.PROPERTY_ELEMENT, args, fertiMinElements.iterator().next());
        }
        if (campaignRange.size() > 1) {
            query += DaoUtils.andAttributeIn("rpfm", RefInputPrice.PROPERTY_CAMPAIGN, args, campaignRange);
        } else {
            query += DaoUtils.andAttributeEquals("rpfm", RefInputPrice.PROPERTY_CAMPAIGN, args, campaignRange.iterator().next());
        }
        query += " ORDER BY rpfm." + RefPrixFertiMin.PROPERTY_ELEMENT + ", rpfm." + RefInputPrice.PROPERTY_CAMPAIGN;

        return findAll(query,args);
    }

    protected Integer findLowestOrEqualRefPrixFertiOrgaCampaign(InputPriceFilter filter) {
        Integer campaign = filter.getCampaign();
        String query = "SELECT rip." + RefInputPrice.PROPERTY_CAMPAIGN + " FROM " + RefPrixFertiOrga.class.getName() + " rip " +
                " WHERE rip." + RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT + " = :" + RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT +
                " AND rip." + RefPrixFertiOrga.PROPERTY_ORGANIC + " = :" + RefPrixFertiOrga.PROPERTY_ORGANIC +
                " AND rip." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL" +
                " AND rip." + RefInputPrice.PROPERTY_CAMPAIGN + " <= :" + RefInputPrice.PROPERTY_CAMPAIGN +
                " AND rip." +  RefInputPrice.PROPERTY_ACTIVE + " is TRUE" +
                " ORDER BY rip." + RefInputPrice.PROPERTY_CAMPAIGN + " DESC ";
        Integer refCampaign = findFirstOrNull(query, DaoUtils.asArgsMap(
                RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT, filter.getObjectId(),
                RefPrixFertiOrga.PROPERTY_ORGANIC, filter.isOrganic(),
                RefInputPrice.PROPERTY_CAMPAIGN, campaign
        ));
        return refCampaign == null ? -Integer.MAX_VALUE : refCampaign;
    }

    protected Integer findHighestRefPrixFertileOrgaCampaign(InputPriceFilter filter) {
        Integer campaign = filter.getCampaign();
        String query = "SELECT rip." + RefInputPrice.PROPERTY_CAMPAIGN + " FROM " + RefPrixFertiOrga.class.getName() + " rip " +
                " WHERE rip." + RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT + " = :" + RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT +
                " AND rip." + RefPrixFertiOrga.PROPERTY_ORGANIC + " = :" + RefPrixFertiOrga.PROPERTY_ORGANIC +
                " AND rip." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL" +
                " AND rip." + RefInputPrice.PROPERTY_CAMPAIGN + " > :" + RefInputPrice.PROPERTY_CAMPAIGN +
                " AND rip." +  RefInputPrice.PROPERTY_ACTIVE + " is TRUE" +
                " ORDER BY rip." + RefInputPrice.PROPERTY_CAMPAIGN + " ASC ";
        Integer refCampaign = findFirstOrNull(query, DaoUtils.asArgsMap(
                RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT, filter.getObjectId(),
                RefPrixFertiOrga.PROPERTY_ORGANIC, filter.isOrganic(),
                RefInputPrice.PROPERTY_CAMPAIGN, campaign
        ));
        return refCampaign == null ? Integer.MAX_VALUE : refCampaign;
    }

    public List<RefPrixFertiOrga> findRefPrixFertiOrgaForCampaign(InputPriceFilter filter) {
        Integer campaign = filter.getCampaign();
        Integer closestCampaign;
        Integer lowestOrEqualRefCampaign = findLowestOrEqualRefPrixFertiOrgaCampaign(filter);
        if (!Objects.equals(lowestOrEqualRefCampaign, campaign)) {
            Integer highestRefPrixFortiOrgaCampaign = findHighestRefPrixFertileOrgaCampaign(filter);
            closestCampaign =
                    Math.abs(campaign - lowestOrEqualRefCampaign) <=
                    Math.abs(highestRefPrixFortiOrgaCampaign - campaign)
                            ? lowestOrEqualRefCampaign : highestRefPrixFortiOrgaCampaign;
        } else {
            closestCampaign = lowestOrEqualRefCampaign;
        }

        String q0 = "SELECT DISTINCT refProductPrice";

        q0 += "      FROM " + RefPrixFertiOrga.class.getName() + " refProductPrice";

        q0 += "      WHERE refProductPrice." + RefInputPrice.PROPERTY_ACTIVE + " = true";

        q0 += " AND refProductPrice." + RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT + " = :" + RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT;

        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " = :" + RefInputPrice.PROPERTY_CAMPAIGN ;


        Map<String, Object> args = DaoUtils.asArgsMap(
                RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT, filter.getObjectId(),
                RefInputPrice.PROPERTY_CAMPAIGN, closestCampaign);

        List<RefPrixFertiOrga> result = findAll(q0, args);

        return result;
    }

    protected Optional<RefInputPrice> findLowestOrEqualRefInputPriceForCampaign(Class<? extends RefInputPrice> aClass, Integer campaign) {
        String query = "SELECT rip FROM " + aClass.getName() + " rip " +
                " WHERE rip." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL" +
                " AND rip." + RefInputPrice.PROPERTY_CAMPAIGN + " <= :" + RefInputPrice.PROPERTY_CAMPAIGN +
                " AND rip." +  RefInputPrice.PROPERTY_ACTIVE + " is TRUE " +
                " AND rip." + RefInputPrice.PROPERTY_PRICE + " IS NOT NULL " +
                " ORDER BY rip." + RefInputPrice.PROPERTY_CAMPAIGN + " DESC, rip." + RefInputPrice.PROPERTY_PRICE + " DESC";
        RefInputPrice refInputPrice = findFirstOrNull(query, DaoUtils.asArgsMap(
                RefInputPrice.PROPERTY_CAMPAIGN, campaign
        ));
        return Optional.ofNullable(refInputPrice);
    }
    protected Optional<RefInputPrice> findHighestRefPrixCarbuForCampaign(Class<? extends RefInputPrice> aClass, Integer campaign) {
        String query = "SELECT rip FROM " + aClass.getName() + " rip " +
                " WHERE rip." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL" +
                " AND rip." + RefInputPrice.PROPERTY_CAMPAIGN + " > :" + RefInputPrice.PROPERTY_CAMPAIGN +
                " AND rip." +  RefInputPrice.PROPERTY_ACTIVE + " is TRUE" +
                " AND rip." + RefInputPrice.PROPERTY_PRICE + " IS NOT NULL " +
                " ORDER BY rip." + RefInputPrice.PROPERTY_CAMPAIGN + " ASC, rip." + RefInputPrice.PROPERTY_PRICE + " DESC";
        RefInputPrice refInputPrice = findFirstOrNull(query, DaoUtils.asArgsMap(
                RefInputPrice.PROPERTY_CAMPAIGN, campaign
        ));
        return Optional.ofNullable(refInputPrice);
    }

    public Optional<RefInputPrice> findRefPrixForCampaign(Class<? extends RefInputPrice> aClass, Integer campaign) {
        Optional<RefInputPrice> result;
        Optional<RefInputPrice> optionalLowestRefInputPrice = findLowestOrEqualRefInputPriceForCampaign(aClass, campaign);
         if (optionalLowestRefInputPrice.isEmpty()) {
             final Optional<RefInputPrice> optionalHighestRefPrixCarbuForCampaign = findHighestRefPrixCarbuForCampaign(aClass, campaign);
             result = optionalHighestRefPrixCarbuForCampaign;
         } else {
             final RefInputPrice lowestOrEqualRefPrixForCampaign = optionalLowestRefInputPrice.get();
             if (!Objects.equals(lowestOrEqualRefPrixForCampaign.getCampaign(), campaign)) {
                Optional<RefInputPrice> optionalHighestRefPrixForCampaign = findHighestRefPrixCarbuForCampaign(aClass, campaign);
                if (optionalHighestRefPrixForCampaign.isEmpty()) {
                    result = optionalLowestRefInputPrice;
                } else {
                    RefInputPrice highestRefPrixForCampaign = optionalHighestRefPrixForCampaign.get();
                    RefInputPrice closestCampaignResult =
                            Math.abs(campaign - lowestOrEqualRefPrixForCampaign.getCampaign()) <
                                    Math.abs(highestRefPrixForCampaign.getCampaign() - campaign)
                                    ? lowestOrEqualRefPrixForCampaign : highestRefPrixForCampaign;
                    result = Optional.of(closestCampaignResult);
                }
            } else {
                result = optionalLowestRefInputPrice;
            }
        }

        return result;
    }

    protected Integer findLowestOrEqualRefPrixPhytoCampaign(InputPriceFilter filter) {
        Integer campaign = filter.getCampaign();
        String query = "SELECT rip." + RefInputPrice.PROPERTY_CAMPAIGN + " FROM " + RefPrixPhyto.class.getName() + " rip " +
                " WHERE rip." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL" +
                " AND rip." + RefInputPrice.PROPERTY_CAMPAIGN + " <= :" + RefInputPrice.PROPERTY_CAMPAIGN +
                " AND rip." + RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID + " = :" + RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID +
                " AND rip." +  RefInputPrice.PROPERTY_ACTIVE + " is TRUE" +
                " ORDER BY rip." + RefInputPrice.PROPERTY_CAMPAIGN + " DESC ";
        Integer refCampaign = findFirstOrNull(
                query,
                DaoUtils.asArgsMap(
                        RefInputPrice.PROPERTY_CAMPAIGN, campaign,
                        RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, filter.getObjectId()));
        return refCampaign == null ? -Integer.MAX_VALUE : refCampaign;
    }

    protected Integer findLowestOrEqualRefPrixPhytoCampaign(String idProduit, int idTraitement, Integer campaign, RefCountry refCountry) {
        String query = MessageFormat.format(
                """
                    SELECT refPrixPhyto.{0}
                    FROM {1} refPrixPhyto
                    WHERE refPrixPhyto.{0} IS NOT NULL
                        AND refPrixPhyto.{0} <= :{0}
                        AND refPrixPhyto.{2} is TRUE
                        AND refPrixPhyto.{3} = (
                            SELECT DISTINCT {4}
                            FROM {5} refActaTraitementsProduit
                            WHERE refActaTraitementsProduit.{6} = :{6}
                                AND refActaTraitementsProduit.{7} = :{7}
                                AND refActaTraitementsProduit.{8} IS TRUE
                                AND refActaTraitementsProduit.{9} = :{9}
                        )
                        ORDER BY refPrixPhyto.{0} DESC
                """,
                RefInputPrice.PROPERTY_CAMPAIGN,
                RefPrixPhyto.class.getName(),
                RefInputPrice.PROPERTY_ACTIVE,
                RefPrixPhyto.PROPERTY_CODE__AMM,
                RefActaTraitementsProduit.PROPERTY_CODE__AMM,
                RefActaTraitementsProduit.class.getName(),
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT,
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT,
                RefActaTraitementsProduit.PROPERTY_ACTIVE,
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY
        );
        Integer refCampaign = findFirstOrNull(
                query,
                DaoUtils.asArgsMap(
                        RefInputPrice.PROPERTY_CAMPAIGN, campaign,
                        RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, idProduit,
                        RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, idTraitement,
                        RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, refCountry
                )
        );
        return refCampaign == null ? -Integer.MAX_VALUE : refCampaign;
    }

    protected Integer findHighestRefPrixPhytoCampaign(InputPriceDto filter, Integer campaign) {
        String query = "SELECT rip." + RefInputPrice.PROPERTY_CAMPAIGN + " FROM " + RefPrixPhyto.class.getName() + " rip " +
                " WHERE rip." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL" +
                " AND rip." + RefInputPrice.PROPERTY_CAMPAIGN + " > :" + RefInputPrice.PROPERTY_CAMPAIGN +
                " AND rip." +  RefInputPrice.PROPERTY_ACTIVE + " is TRUE" +
                " AND rip." + RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID + " = :" + RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID +
                " ORDER BY rip." + RefInputPrice.PROPERTY_CAMPAIGN + " ASC ";
        Integer refCampaign = findFirstOrNull(
                query,
                DaoUtils.asArgsMap(
                        RefInputPrice.PROPERTY_CAMPAIGN, campaign,
                        RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, filter.getObjectId()));
        return refCampaign == null ? Integer.MAX_VALUE : refCampaign;
    }

    protected Integer findHighestRefPrixPhytoCampaign(String idProduit, int idTraitement, Integer campaign, RefCountry refCountry) {
        String query = MessageFormat.format(
                """
                    SELECT refPrixPhyto.{0}
                    FROM {1} refPrixPhyto
                    WHERE refPrixPhyto.{0} IS NOT NULL
                        AND refPrixPhyto.{0} > :{0}
                        AND refPrixPhyto.{2} is TRUE
                        AND refPrixPhyto.{3} = (
                            SELECT DISTINCT {4}
                            FROM {5} refActaTraitementsProduit
                            WHERE refActaTraitementsProduit.{6} = :{6}
                                AND refActaTraitementsProduit.{7} = :{7}
                                AND refActaTraitementsProduit.{8} IS TRUE
                                AND refActaTraitementsProduit.{9} = :{9}
                        )
                        ORDER BY refPrixPhyto.{0} DESC
                """,
                RefInputPrice.PROPERTY_CAMPAIGN,
                RefPrixPhyto.class.getName(),
                RefInputPrice.PROPERTY_ACTIVE,
                RefPrixPhyto.PROPERTY_CODE__AMM,
                RefActaTraitementsProduit.PROPERTY_CODE__AMM,
                RefActaTraitementsProduit.class.getName(),
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT,
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT,
                RefActaTraitementsProduit.PROPERTY_ACTIVE,
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY
        );
        Integer refCampaign = findFirstOrNull(
                query,
                DaoUtils.asArgsMap(
                        RefInputPrice.PROPERTY_CAMPAIGN, campaign,
                        RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, idProduit,
                        RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, idTraitement,
                        RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, refCountry
                )
        );
        return refCampaign == null ? Integer.MAX_VALUE : refCampaign;
    }

    public List<RefPrixPhyto> findRefPrixPhytoForCampaign(InputPriceFilter filter, RefCountry refCountry) {
    
        Integer campaign = filter.getCampaign();
        Integer closestCampaign;
        Integer lowestOrEqualRefCampaign = findLowestOrEqualRefPrixPhytoCampaign(filter);
        if (!Objects.equals(lowestOrEqualRefCampaign, campaign)) {
            Integer highestRefPrixForCampaign = findHighestRefPrixPhytoCampaign(filter, campaign);
            closestCampaign =
                    Math.abs(campaign - lowestOrEqualRefCampaign) <=
                            Math.abs(highestRefPrixForCampaign - campaign)
                            ? lowestOrEqualRefCampaign : highestRefPrixForCampaign;
        } else {
            closestCampaign = lowestOrEqualRefCampaign;
        }
        
        String q0 = "SELECT DISTINCT refProductPrice";

        q0 += "      FROM " + RefPrixPhyto.class.getName() + " refProductPrice";
        
        q0 += "      WHERE refProductPrice." + RefInputPrice.PROPERTY_ACTIVE + " = true";
        
        q0 += " AND refProductPrice." + RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID + " = :" + RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID;
        
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " = :" + RefInputPrice.PROPERTY_CAMPAIGN ;
        
        
        Map<String, Object> args = DaoUtils.asArgsMap(
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, filter.getObjectId(),
                RefInputPrice.PROPERTY_CAMPAIGN, closestCampaign);
        
        List<RefPrixPhyto> result = findAll(q0, args);

        if (result.isEmpty() && filter.getObjectId() != null && filter.getObjectId().contains("_")) {
            var pair = parseIdTraitementIdProduit(filter.getObjectId());
            result = electCampaignAndFindRefPrixPhytoWithCodeAMM(pair.getRight(), pair.getLeft(), filter.getCampaign(), refCountry);
        }

        return result;
    }

    private List <RefPrixPhyto> electCampaignAndFindRefPrixPhytoWithCodeAMM(String idProduit, int idTraitement, Integer campaign, RefCountry refCountry) {
        Integer closestCampaign;
        Integer lowestOrEqualRefCampaign = findLowestOrEqualRefPrixPhytoCampaign(idProduit, idTraitement, campaign, refCountry);
        if (!Objects.equals(lowestOrEqualRefCampaign, campaign)) {
            Integer highestRefPrixForCampaign = findHighestRefPrixPhytoCampaign(idProduit, idTraitement, campaign, refCountry);
            closestCampaign =
                    Math.abs(campaign - lowestOrEqualRefCampaign) <=
                            Math.abs(highestRefPrixForCampaign - campaign)
                            ? lowestOrEqualRefCampaign : highestRefPrixForCampaign;
        } else {
            closestCampaign = lowestOrEqualRefCampaign;
        }

        return findRefPrixPhytoWithCodeAMM(idProduit, idTraitement, closestCampaign, refCountry);
    }

    public List<RefPrixPhyto> findRefPrixPhytoWithCodeAMM(String idProduit, int idTraitement, Integer campaign, RefCountry refCountry) {
        String q0 = MessageFormat.format(
                """
                            SELECT DISTINCT refPrixPhyto
                            FROM {0} refPrixPhyto
                            WHERE refPrixPhyto.{1} IS TRUE
                                AND refPrixPhyto.{2} = :{2}
                                AND refPrixPhyto.{3} = (
                                    SELECT DISTINCT {4}
                                    FROM {5} refActaTraitementsProduit
                                    WHERE refActaTraitementsProduit.{6} = :{6}
                                        AND refActaTraitementsProduit.{7} = :{7}
                                        AND refActaTraitementsProduit.{8} IS TRUE
                                        AND refActaTraitementsProduit.{9} = :{9}
                                )
                        """,
                RefPrixPhyto.class.getName(),
                RefInputPrice.PROPERTY_ACTIVE,
                RefInputPrice.PROPERTY_CAMPAIGN,
                RefPrixPhyto.PROPERTY_CODE__AMM,
                RefActaTraitementsProduit.PROPERTY_CODE__AMM,
                RefActaTraitementsProduit.class.getName(),
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT,
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT,
                RefActaTraitementsProduit.PROPERTY_ACTIVE,
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY

        );

        Map<String, Object> args = DaoUtils.asArgsMap(
                RefInputPrice.PROPERTY_CAMPAIGN, campaign,
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, idProduit,
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, idTraitement,
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, refCountry
        );

        List<RefPrixPhyto> result = findAll(q0, args);
        Map<PriceUnit, List<RefPrixPhyto>> pricesByUnit = result.stream()
                .collect(Collectors.groupingBy(RefPrixPhyto::getUnit));

        return pricesByUnit.entrySet().stream()
                .map(entry -> {
                    PriceUnit unit = entry.getKey();
                    List<RefPrixPhyto> prices = entry.getValue().stream().filter(p -> Objects.nonNull(p.getPrice())).toList();
                    if (prices.size() == 2) {
                        return computeAveragePrice(prices, campaign, null, unit, idProduit, idTraitement);
                    } else if (prices.size() > 2) {
                        return computeMedianPrice(prices, campaign, null, unit, idProduit, idTraitement);
                    }
                    return entry.getValue().getFirst();
                }).collect(Collectors.toList());
    }

    public List<RefPrixPhyto> findRefPrixPhytoWithCodeAMM(String idProduit, int idTraitement, String codeScenario, RefCountry refCountry) {
        String q0 = MessageFormat.format(
                """
                            SELECT DISTINCT refPrixPhyto
                            FROM {0} refPrixPhyto
                            WHERE refPrixPhyto.{1} IS TRUE
                                AND refPrixPhyto.{2} = :{2}
                                AND refPrixPhyto.{3} = (
                                    SELECT DISTINCT {4}
                                    FROM {5} refActaTraitementsProduit
                                    WHERE refActaTraitementsProduit.{6} = :{6}
                                        AND refActaTraitementsProduit.{7} = :{7}
                                        AND refActaTraitementsProduit.{8} IS TRUE
                                        AND refActaTraitementsProduit.{9} = :{9}
                                )
                        """,
                RefPrixPhyto.class.getName(),
                RefInputPrice.PROPERTY_ACTIVE,
                RefInputPrice.PROPERTY_CODE_SCENARIO,
                RefPrixPhyto.PROPERTY_CODE__AMM,
                RefActaTraitementsProduit.PROPERTY_CODE__AMM,
                RefActaTraitementsProduit.class.getName(),
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT,
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT,
                RefActaTraitementsProduit.PROPERTY_ACTIVE,
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY
        );

        Map<String, Object> args = DaoUtils.asArgsMap(
                RefInputPrice.PROPERTY_CODE_SCENARIO, codeScenario,
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, idProduit,
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, idTraitement,
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, refCountry
        );

        List<RefPrixPhyto> result = findAll(q0, args);
        Map<PriceUnit, List<RefPrixPhyto>> pricesByUnit = result.stream()
                .collect(Collectors.groupingBy(RefPrixPhyto::getUnit));

        return pricesByUnit.entrySet().stream()
                .map(entry -> {
                    PriceUnit unit = entry.getKey();
                    List<RefPrixPhyto> prices = entry.getValue().stream().filter(p -> Objects.nonNull(p.getPrice())).toList();
                    if (prices.size() == 2) {
                        return computeAveragePrice(prices, null, codeScenario, unit, idProduit, idTraitement);
                    } else if (prices.size() > 2) {
                        return computeMedianPrice(prices, null, codeScenario, unit, idProduit, idTraitement);
                    }
                    return entry.getValue().getFirst();
                }).collect(Collectors.toList());
    }

    private RefPrixPhyto computeAveragePrice(List<RefPrixPhyto> result, Integer campaign, String codeScenario, PriceUnit unit, String idProduit, int idTraitement) {
        var price = result.stream().map(RefPrixPhyto::getPrice).mapToDouble(Double::doubleValue).sum() / result.size();
        return buildRefPrixPhyto(price, campaign, codeScenario, unit, idProduit, idTraitement);
    }

    private RefPrixPhyto buildRefPrixPhyto(Double price, Integer campaign, String codeScenario, PriceUnit unit, String idProduit, int idTraitement) {
        RefPrixPhyto refPrixPhyto = new RefPrixPhytoImpl();
        refPrixPhyto.setPrice(price);
        refPrixPhyto.setCampaign(campaign);
        refPrixPhyto.setCode_scenario(codeScenario);
        refPrixPhyto.setUnit(unit);
        refPrixPhyto.setId_produit(idProduit);
        refPrixPhyto.setId_traitement(idTraitement);
        refPrixPhyto.setPhytoObjectId(idProduit + "_" + idTraitement);
        return refPrixPhyto;
    }

    protected RefPrixPhyto computeMedianPrice(List<RefPrixPhyto> result, Integer campaign, String codeScenario, PriceUnit unit, String idProduit, int idTraitement) {

        List<Double> sortedPriceValues = result.stream()
                .map(RefPrixPhyto::getPrice)
                .filter(Objects::nonNull)
                .sorted(Comparator.comparing(Double::doubleValue))
                .toList();

        if (sortedPriceValues.size() % 2 == 1) {
            Double refPrixPhytoValue = sortedPriceValues.get(sortedPriceValues.size() / 2);
            return buildRefPrixPhyto(refPrixPhytoValue, campaign, codeScenario, unit, idProduit, idTraitement);
        }
        return computeAveragePrice(result.subList(result.size() / 2 - 1, result.size() / 2), campaign, codeScenario, unit, idProduit, idTraitement);
    }

    public List<RefPrixAutre> findRefPrixAutreForCampaign(
            String inputType_c0,
            String caracteristic1,
            String caracteristic2,
            String caracteristic3,
            Integer campaign) {
        
        Integer closestCampaign;
        Integer lowestOrEqualRefCampaign = findLowestOrEqualRefPrixAutreCampaign(inputType_c0, caracteristic1, caracteristic2, caracteristic3, campaign);
        if (!Objects.equals(lowestOrEqualRefCampaign, campaign)) {
            Integer highestRefPrixForCampaign = findHighestRefPrixAutreCampaign(inputType_c0, caracteristic1, caracteristic2, caracteristic3, campaign);
            closestCampaign =
                    Math.abs(campaign - lowestOrEqualRefCampaign) <=
                            Math.abs(highestRefPrixForCampaign - campaign)
                            ? lowestOrEqualRefCampaign : highestRefPrixForCampaign;
        } else {
            closestCampaign = lowestOrEqualRefCampaign;
        }

        Map<String, Object> args = new HashMap<>();
        args.put(RefPrixAutre.PROPERTY_INPUT_TYPE_C0, inputType_c0);
        args.put(RefInputPrice.PROPERTY_CAMPAIGN, closestCampaign);
    
        String q0 = "SELECT DISTINCT refProductPrice";
    
        q0 += "      FROM " + RefPrixAutre.class.getName() + " refProductPrice";
    
        q0 += "      WHERE refProductPrice." + RefInputPrice.PROPERTY_ACTIVE + " = true";

        q0 += " AND refProductPrice." + RefPrixAutre.PROPERTY_INPUT_TYPE_C0 + " = :" + RefPrixAutre.PROPERTY_INPUT_TYPE_C0;
        if (caracteristic1 != null) {
            q0 += " AND refProductPrice." + RefPrixAutre.PROPERTY_CARACTERISTIC1 + " = :" + RefPrixAutre.PROPERTY_CARACTERISTIC1;
            args.put(RefPrixAutre.PROPERTY_CARACTERISTIC1, caracteristic1);
        } else {
            q0 += " AND refProductPrice." + RefPrixAutre.PROPERTY_CARACTERISTIC1 + " IS NULL " ;
        }
        if (caracteristic2 != null) {
            q0 += " AND refProductPrice." + RefPrixAutre.PROPERTY_CARACTERISTIC2 + " = :" + RefPrixAutre.PROPERTY_CARACTERISTIC2;
            args.put(RefPrixAutre.PROPERTY_CARACTERISTIC2, caracteristic2);
        } else {
            q0 += " AND refProductPrice." + RefPrixAutre.PROPERTY_CARACTERISTIC2 + " IS NULL ";
        }
    
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " = :" + RefInputPrice.PROPERTY_CAMPAIGN ;
    
        List<RefPrixAutre> result = findAll(q0, args);
    
        return result;
    }
    
    private Integer findLowestOrEqualRefPrixAutreCampaign(
            String inputType_c0,
            String caracteristic1,
            String caracteristic2,
            String caracteristic3,
            Integer campaign) {

        Map<String, Object> args = new HashMap<>();
        args.put(RefPrixAutre.PROPERTY_INPUT_TYPE_C0, inputType_c0);
        args.put(RefInputPrice.PROPERTY_CAMPAIGN, campaign);

        String q0 = "SELECT DISTINCT refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN;
        
        q0 += "      FROM " + RefPrixAutre.class.getName() + " refProductPrice";
        
        q0 += "      WHERE refProductPrice." + RefInputPrice.PROPERTY_ACTIVE + " IS true";
        
        q0 += " AND refProductPrice." + RefPrixAutre.PROPERTY_INPUT_TYPE_C0 + " = :" + RefPrixAutre.PROPERTY_INPUT_TYPE_C0;
        q0 = addCaracteristicsOtherQueryFilter(caracteristic1, caracteristic2, caracteristic3, args, q0);

        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL";
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " <= :" + RefInputPrice.PROPERTY_CAMPAIGN ;
        q0 += " ORDER BY refProductPrice." + RefPrixAutre.PROPERTY_CAMPAIGN + " DESC ";

        Integer result = findFirstOrNull(q0, args);
        
        return result == null ? -Integer.MAX_VALUE : result;
    }
    
    private Integer findHighestRefPrixAutreCampaign(
            String inputType_c0,
            String caracteristic1,
            String caracteristic2,
            String caracteristic3,
            Integer campaign) {

        Map<String, Object> args = new HashMap<>();
        args.put(RefPrixAutre.PROPERTY_INPUT_TYPE_C0, inputType_c0);
        args.put(RefInputPrice.PROPERTY_CAMPAIGN, campaign);

        String q0 = "SELECT DISTINCT refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN;
    
        q0 += "      FROM " + RefPrixAutre.class.getName() + " refProductPrice";
    
        q0 += "      WHERE refProductPrice." + RefInputPrice.PROPERTY_ACTIVE + " = true";

        q0 += " AND refProductPrice." + RefPrixAutre.PROPERTY_INPUT_TYPE_C0 + " = :" + RefPrixAutre.PROPERTY_INPUT_TYPE_C0;
        q0 = addCaracteristicsOtherQueryFilter(caracteristic1, caracteristic2, caracteristic3, args, q0);

        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL";
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " > :" + RefInputPrice.PROPERTY_CAMPAIGN ;
        q0 += " ORDER BY refProductPrice." + RefPrixAutre.PROPERTY_CAMPAIGN + " ASC ";

        Integer result = findFirstOrNull(q0, args);
    
        return result == null ? Integer.MAX_VALUE : result;
    }

    private static String addCaracteristicsOtherQueryFilter(String caracteristic1, String caracteristic2, String caracteristic3, Map<String, Object> args, String q0) {
        if (caracteristic1 != null) {
            q0 += " AND refProductPrice." + RefPrixAutre.PROPERTY_CARACTERISTIC1 + " = :" + RefPrixAutre.PROPERTY_CARACTERISTIC1;
            args.put(RefPrixAutre.PROPERTY_CARACTERISTIC1, caracteristic1);
        } else {
            q0 += " AND refProductPrice." + RefPrixAutre.PROPERTY_CARACTERISTIC1 + " IS NULL " ;
        }
        if (caracteristic2 != null) {
            q0 += " AND refProductPrice." + RefPrixAutre.PROPERTY_CARACTERISTIC2 + " = :" + RefPrixAutre.PROPERTY_CARACTERISTIC2;
            args.put(RefPrixAutre.PROPERTY_CARACTERISTIC2, caracteristic2);
        } else {
            q0 += " AND refProductPrice." + RefPrixAutre.PROPERTY_CARACTERISTIC2 + " IS NULL ";
        }
        if (caracteristic3 != null) {
            q0 += " AND refProductPrice." + RefPrixAutre.PROPERTY_CARACTERISTIC3 + " = :" + RefPrixAutre.PROPERTY_CARACTERISTIC3;
            args.put(RefPrixAutre.PROPERTY_CARACTERISTIC3, caracteristic3);
        } else {
            q0 += " AND refProductPrice." + RefPrixAutre.PROPERTY_CARACTERISTIC3 + " IS NULL ";
        }
        return q0;
    }

    public String getParamDbName(String objectId, String columnName, Class class_) {
        String query = "SELECT DISTINCT refProductPrice." + columnName;
        query += "      FROM " + class_.getSimpleName() + " refProductPrice";
        query += "      WHERE refProductPrice." + RefInputPrice.PROPERTY_ACTIVE + " is true";
        query += " AND to_simple_i18n_key(refProductPrice." + columnName + ") = ? ";
        query += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL";

        List<String> params0 = new ArrayList<>();
        params0.add(I18nDaoHelper.to_simple_i18n_key(objectId));
        SimpleSqlQuery<List<Object>> query00 = new SimpleSqlQuery<>(query, params0) {
            @Override
            public List<Object> prepareResult(ResultSet resultSet) throws SQLException {
                List<Object> results = new ArrayList<>();
                results.add(resultSet.getString(1));
                return results;
            }
        };
        List<Object> singleResult = this.topiaSqlSupport.findSingleResult(query00);

        String caracteristic1 = "";
        if (CollectionUtils.isNotEmpty(singleResult)) {
            caracteristic1 = (String) singleResult.getFirst();
        }
        return caracteristic1;
    }

    public List<RefPrixPot> findRefPrixPotForCampaign(String caracteristic1, Integer campaign) {
        Integer closestCampaign;
        Integer lowestOrEqualRefCampaign = findLowestOrEqualRefPrixPotCampaign(caracteristic1, campaign);
        if (!Objects.equals(lowestOrEqualRefCampaign, campaign)) {
            Integer highestRefPrixForCampaign = findHighestRefPrixPotCampaign(caracteristic1, campaign);
            closestCampaign =
                    Math.abs(campaign - lowestOrEqualRefCampaign) <=
                            Math.abs(highestRefPrixForCampaign - campaign)
                            ? lowestOrEqualRefCampaign : highestRefPrixForCampaign;
        } else {
            closestCampaign = lowestOrEqualRefCampaign;
        }
    
        String q0 = "SELECT DISTINCT refProductPrice";
    
        q0 += "      FROM " + RefPrixPot.class.getName() + " refProductPrice";
    
        q0 += "      WHERE refProductPrice." + RefInputPrice.PROPERTY_ACTIVE + " = true";
    
        q0 += " AND refProductPrice." + RefPrixPot.PROPERTY_CARACTERISTIC1 + " = :" + RefPrixPot.PROPERTY_CARACTERISTIC1;

        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " = :" + RefInputPrice.PROPERTY_CAMPAIGN ;
    
    
        Map<String, Object> args = DaoUtils.asArgsMap(
                RefPrixPot.PROPERTY_CARACTERISTIC1, caracteristic1,
                RefInputPrice.PROPERTY_CAMPAIGN, closestCampaign);
    
        List<RefPrixPot> result = findAll(q0, args);
        
        return result;
    }
    
    private Integer findHighestRefPrixPotCampaign(String caracteristic1, Integer campaign) {
        String q0 = "SELECT DISTINCT refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN;
        
        q0 += "      FROM " + RefPrixPot.class.getName() + " refProductPrice";
        
        q0 += "      WHERE refProductPrice." + RefInputPrice.PROPERTY_ACTIVE + " = true";
        
        q0 += " AND refProductPrice." + RefPrixPot.PROPERTY_CARACTERISTIC1 + " = :" + RefPrixPot.PROPERTY_CARACTERISTIC1;
        
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL";
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " > :" + RefInputPrice.PROPERTY_CAMPAIGN ;
        q0 += " ORDER BY refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " ASC ";
        
        Map<String, Object> args = DaoUtils.asArgsMap(
                RefPrixPot.PROPERTY_CARACTERISTIC1, caracteristic1,
                RefInputPrice.PROPERTY_CAMPAIGN, campaign);
        
        Integer result = findFirstOrNull(q0, args);
        
        return result == null ? Integer.MAX_VALUE : result;
    }
    
    private Integer findLowestOrEqualRefPrixPotCampaign(String caracteristic1, Integer campaign) {
        String q0 = "SELECT DISTINCT refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN;
        
        q0 += "      FROM " + RefPrixPot.class.getName() + " refProductPrice";
        
        q0 += "      WHERE refProductPrice." + RefInputPrice.PROPERTY_ACTIVE + " = true";
        
        q0 += " AND refProductPrice." + RefPrixPot.PROPERTY_CARACTERISTIC1 + " = :" + RefPrixPot.PROPERTY_CARACTERISTIC1;
        
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL";
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " <= :" + RefInputPrice.PROPERTY_CAMPAIGN ;
        q0 += " ORDER BY refProductPrice." + RefPrixPot.PROPERTY_CAMPAIGN + " DESC ";
        
        Map<String, Object> args = DaoUtils.asArgsMap(
                RefPrixPot.PROPERTY_CARACTERISTIC1, caracteristic1,
                RefInputPrice.PROPERTY_CAMPAIGN, campaign);
        
        Integer result = findFirstOrNull(q0, args);
        
        return result == null ? -Integer.MAX_VALUE : result;
    }
    
    public List<RefPrixSubstrate> findRefPrixSubstratForCampaign(String caracteristic1, String caracteristic2, Integer campaign) {
        
        Integer closestCampaign;
        Integer lowestOrEqualRefCampaign = findLowestOrEqualRefPrixSubstratCampaign(caracteristic1, caracteristic2, campaign);
        if (!Objects.equals(lowestOrEqualRefCampaign, campaign)) {
            Integer highestRefPrixForCampaign = findHighestRefPrixSubstratCampaign(caracteristic1, caracteristic2, campaign);
            closestCampaign =
                    Math.abs(campaign - lowestOrEqualRefCampaign) <=
                            Math.abs(highestRefPrixForCampaign - campaign)
                            ? lowestOrEqualRefCampaign : highestRefPrixForCampaign;
        } else {
            closestCampaign = lowestOrEqualRefCampaign;
        }
    
        String q0 = "SELECT DISTINCT refProductPrice";
    
        q0 += "      FROM " + RefPrixSubstrate.class.getName() + " refProductPrice";
    
        q0 += "      WHERE refProductPrice." + RefInputPrice.PROPERTY_ACTIVE + " = true";
    
        q0 += " AND refProductPrice." + RefPrixSubstrate.PROPERTY_CARACTERISTIC1 + " = :" + RefPrixSubstrate.PROPERTY_CARACTERISTIC1;
        q0 += " AND refProductPrice." + RefPrixSubstrate.PROPERTY_CARACTERISTIC2 + " = :" + RefPrixSubstrate.PROPERTY_CARACTERISTIC2;
    
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " = :" + RefInputPrice.PROPERTY_CAMPAIGN ;
    
    
        Map<String, Object> args = DaoUtils.asArgsMap(
                RefPrixSubstrate.PROPERTY_CARACTERISTIC1, caracteristic1,
                RefPrixSubstrate.PROPERTY_CARACTERISTIC2, caracteristic2,
                RefInputPrice.PROPERTY_CAMPAIGN, closestCampaign);
    
        List<RefPrixSubstrate> result = findAll(q0, args);
    
        return result;
    }
    
    private Integer findLowestOrEqualRefPrixSubstratCampaign(
            String caracteristic1,
            String caracteristic2,
            Integer campaign) {
        String q0 = "SELECT DISTINCT refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN;
        
        q0 += "      FROM " + RefPrixSubstrate.class.getName() + " refProductPrice";
        
        q0 += "      WHERE refProductPrice." + RefInputPrice.PROPERTY_ACTIVE + " = true";
        
        q0 += " AND refProductPrice." + RefPrixSubstrate.PROPERTY_CARACTERISTIC1 + " = :" + RefPrixSubstrate.PROPERTY_CARACTERISTIC1;
        q0 += " AND refProductPrice." + RefPrixSubstrate.PROPERTY_CARACTERISTIC2 + " = :" + RefPrixSubstrate.PROPERTY_CARACTERISTIC2;
        
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL";
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " <= :" + RefInputPrice.PROPERTY_CAMPAIGN ;
        q0 += " ORDER BY refProductPrice." + RefPrixSubstrate.PROPERTY_CAMPAIGN + " DESC ";
        
        Map<String, Object> args = DaoUtils.asArgsMap(
                RefPrixSubstrate.PROPERTY_CARACTERISTIC1, caracteristic1,
                RefPrixSubstrate.PROPERTY_CARACTERISTIC2, caracteristic2,
                RefInputPrice.PROPERTY_CAMPAIGN, campaign);
        
        Integer result = findFirstOrNull(q0, args);
        
        return result == null ? -Integer.MAX_VALUE : result;
    }
    
    private Integer findHighestRefPrixSubstratCampaign(
            String caracteristic1,
            String caracteristic2,
            Integer campaign) {
        String q0 = "SELECT DISTINCT refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN;
    
        q0 += "      FROM " + RefPrixAutre.class.getName() + " refProductPrice";
    
        q0 += "      WHERE refProductPrice." + RefPrixSubstrate.PROPERTY_ACTIVE + " = true";
    
        q0 += " AND refProductPrice." + RefPrixSubstrate.PROPERTY_CARACTERISTIC1 + " = :" + RefPrixSubstrate.PROPERTY_CARACTERISTIC1;
        q0 += " AND refProductPrice." + RefPrixSubstrate.PROPERTY_CARACTERISTIC2 + " = :" + RefPrixSubstrate.PROPERTY_CARACTERISTIC2;
    
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL";
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " > :" + RefInputPrice.PROPERTY_CAMPAIGN ;
        q0 += " ORDER BY refProductPrice." + RefPrixSubstrate.PROPERTY_CAMPAIGN + " ASC ";
    
        Map<String, Object> args = DaoUtils.asArgsMap(
                RefPrixSubstrate.PROPERTY_CARACTERISTIC1, caracteristic1,
                RefPrixSubstrate.PROPERTY_CARACTERISTIC2, caracteristic2,
                RefInputPrice.PROPERTY_CAMPAIGN, campaign);
    
        Integer result = findFirstOrNull(q0, args);
    
        return result == null ? Integer.MAX_VALUE : result;
    }
    
    public List<RefPrixEspece> findRefPrixEspeceForCampaign(InputPriceFilter filter) {
        
        Integer campaign = filter.getCampaign();
        Integer closestCampaign;
        Integer lowestOrEqualRefCampaign = findLowestOrEqualRefPrixEspeceCampaign(filter);
        if (!Objects.equals(lowestOrEqualRefCampaign, campaign)) {
            Integer highestRefPrixForCampaign = findHighestRefPrixEspeceCampaign(filter);
            closestCampaign =
                    Math.abs(campaign - lowestOrEqualRefCampaign) <=
                            Math.abs(highestRefPrixForCampaign - campaign)
                            ? lowestOrEqualRefCampaign : highestRefPrixForCampaign;
        } else {
            closestCampaign = lowestOrEqualRefCampaign;
        }
        
        String q0 = "SELECT DISTINCT refProductPrice";
        
        q0 += "      FROM " + RefPrixEspece.class.getName() + " refProductPrice";
        
        q0 += "      WHERE refProductPrice." + RefInputPrice.PROPERTY_ACTIVE + " = true";
    
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE + " = :" + RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE;
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE + " = :" + RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE;
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_TREATMENT + " = :" + RefPrixEspece.PROPERTY_TREATMENT;
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_ORGANIC + " = :" + RefPrixEspece.PROPERTY_ORGANIC;
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_SEED_TYPE + " = :" + RefPrixEspece.PROPERTY_SEED_TYPE;
        
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " = :" + RefInputPrice.PROPERTY_CAMPAIGN ;
        
        
        Map<String, Object> args = DaoUtils.asArgsMap(
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE , filter.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE , filter.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT ,filter.isIncludedTreatment(),
                RefPrixEspece.PROPERTY_ORGANIC , filter.isOrganic(),
                RefPrixEspece.PROPERTY_SEED_TYPE , filter.getSeedType(),
                RefInputPrice.PROPERTY_CAMPAIGN, closestCampaign);
        
        List<RefPrixEspece> result = findAll(q0, args);
        
        return result;
    }
    
    private Integer findLowestOrEqualRefPrixEspeceCampaign(InputPriceFilter filter) {
        
        String q0 = "SELECT DISTINCT refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN;
        
        q0 += "      FROM " + RefPrixEspece.class.getName() + " refProductPrice";
        
        q0 += "      WHERE refProductPrice." + RefInputPrice.PROPERTY_ACTIVE + " = true";
        
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE + " = :" + RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE;
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE + " = :" + RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE;
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_TREATMENT + " = :" + RefPrixEspece.PROPERTY_TREATMENT;
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_ORGANIC + " = :" + RefPrixEspece.PROPERTY_ORGANIC;
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_SEED_TYPE + " = :" + RefPrixEspece.PROPERTY_SEED_TYPE;
        
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL";
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " <= :" + RefInputPrice.PROPERTY_CAMPAIGN ;
        q0 += " ORDER BY refProductPrice." + RefPrixEspece.PROPERTY_CAMPAIGN + " DESC ";
        
        Map<String, Object> args = DaoUtils.asArgsMap(
            RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE , filter.getCode_espece_botanique(),
            RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE , filter.getCode_qualifiant_AEE(),
            RefPrixEspece.PROPERTY_TREATMENT ,filter.isIncludedTreatment(),
            RefPrixEspece.PROPERTY_ORGANIC , filter.isOrganic(),
            RefPrixEspece.PROPERTY_SEED_TYPE , filter.getSeedType(),
            RefInputPrice.PROPERTY_CAMPAIGN, filter.getCampaign());
        
        Integer result = findFirstOrNull(q0, args);
        
        return result == null ? -Integer.MAX_VALUE : result;
    }
    
    private Integer findHighestRefPrixEspeceCampaign(InputPriceFilter filter) {
        
        String q0 = "SELECT DISTINCT refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN;
        
        q0 += "      FROM " + RefPrixEspece.class.getName() + " refProductPrice";
        
        q0 += "      WHERE refProductPrice." + RefInputPrice.PROPERTY_ACTIVE + " = true";
    
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE + " = :" + RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE;
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE + " = :" + RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE;
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_TREATMENT + " = :" + RefPrixEspece.PROPERTY_TREATMENT;
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_ORGANIC + " = :" + RefPrixEspece.PROPERTY_ORGANIC;
        q0 += " AND refProductPrice." + RefPrixEspece.PROPERTY_SEED_TYPE + " = :" + RefPrixEspece.PROPERTY_SEED_TYPE;
        
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " IS NOT NULL";
        q0 += " AND refProductPrice." + RefInputPrice.PROPERTY_CAMPAIGN + " > :" + RefInputPrice.PROPERTY_CAMPAIGN ;
        q0 += " ORDER BY refProductPrice." + RefPrixEspece.PROPERTY_CAMPAIGN + " ASC ";
    
        Map<String, Object> args = DaoUtils.asArgsMap(
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE , filter.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE , filter.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT ,filter.isIncludedTreatment(),
                RefPrixEspece.PROPERTY_ORGANIC , filter.isOrganic(),
                RefPrixEspece.PROPERTY_SEED_TYPE , filter.getSeedType(),
                RefInputPrice.PROPERTY_CAMPAIGN, filter.getCampaign());
        
        Integer result = findFirstOrNull(q0, args);
        
        return result == null ? Integer.MAX_VALUE : result;
    }
} //RefInputPriceTopiaDao
