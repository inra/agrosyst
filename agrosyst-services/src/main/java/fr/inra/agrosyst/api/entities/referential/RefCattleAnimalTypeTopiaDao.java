package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.services.referential.RefAnimalTypeDto;
import fr.inra.agrosyst.api.services.referential.RefCattleAnimalTypeDto;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RefCattleAnimalTypeTopiaDao extends AbstractRefCattleAnimalTypeTopiaDao<RefCattleAnimalType> {
    public List<RefCattleAnimalTypeDto> findAllActive(Language language) {

        I18nDaoHelper animalTypeI18nDaoHelper =
                I18nDaoHelper.withComplexI18nKey(
                        RefCattleAnimalType.PROPERTY_ANIMAL_TYPE,
                        language,
                        TradRefVivant.class,
                        "rcat"
                );

        String hql = String.join("\n"
                , "select rcat"
                        + ", " + animalTypeI18nDaoHelper.coalesceTranslation()
                , newFromClause("rcat")
                        + animalTypeI18nDaoHelper.leftJoinTranslation()
                , "where rcat.active is true"
        );

        try (Stream<Object[]> stream = stream(hql)) {
            return stream.map(row -> {
                RefCattleAnimalType refCattleAnimalType = (RefCattleAnimalType) row[0];
                RefAnimalType refAnimalType = refCattleAnimalType.getRefAnimalType();
                RefAnimalTypeDto refAnimalTypeDto = new RefAnimalTypeDto(
                        refAnimalType.getTopiaId(),
                        refAnimalType.getAnimalType(),
                        refAnimalType.getAnimalPopulationUnits()
                );
                String animalTypeTranslation = (String) row[1];
                return new RefCattleAnimalTypeDto(
                        refCattleAnimalType.getTopiaId(),
                        animalTypeTranslation,
                        refAnimalTypeDto
                );
            }).collect(Collectors.toList());
        }
    }
}
