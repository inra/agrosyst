package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefAnimalTypeImpl;
import fr.inra.agrosyst.api.entities.referential.TradRefVivant;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public class LivestockUnitTopiaDao extends AbstractLivestockUnitTopiaDao<LivestockUnit> {

    public List<LivestockUnit> forDomain(String domainId, Language language) {
        Map<String, Object> args = Map.of("domainId", domainId);

        I18nDaoHelper animalTypeI18nDaoHelper = I18nDaoHelper.withSimpleI18nKey(
                RefAnimalType.PROPERTY_ANIMAL_TYPE,
                language, TradRefVivant.class,
                "at");

        I18nDaoHelper animalPopUnitsI18nDaoHelper = I18nDaoHelper.withSimpleI18nKey(
                RefAnimalType.PROPERTY_ANIMAL_POPULATION_UNITS,
                language, TradRefVivant.class,
                "at");

        String hql = "SELECT ls " +
                " , " + animalTypeI18nDaoHelper.coalesceTranslation() + " as animalType " +
                " , " + animalPopUnitsI18nDaoHelper.coalesceTranslation() + " as animalPopulationUnits " +
                " FROM " + getEntityClass().getName() + " ls " +
                " JOIN ls." + LivestockUnit.PROPERTY_REF_ANIMAL_TYPE + " at " +
                animalTypeI18nDaoHelper.leftJoinTranslation() +
                animalPopUnitsI18nDaoHelper.leftJoinTranslation() +
                " WHERE ls.domain.topiaId = :domainId ";

        List<LivestockUnit> result = new LinkedList<>();
        try (Stream<Object[]> stream = stream(hql, args)) {
            stream.forEach((Object[] row) -> {
                LivestockUnit livestockUnit = clone((LivestockUnit)row[0]);
                livestockUnit.getRefAnimalType().setAnimalType((String)row[1]);
                livestockUnit.getRefAnimalType().setAnimalPopulationUnits((String)row[2]);
                result.add(livestockUnit);
            });
        }
        return result;
    }

    public List<LivestockUnit> forAllDomainCampaigns(String domainCode, Set<Integer> campaigns, Language language) {
        Map<String, Object> args = Maps.newLinkedHashMap();

        I18nDaoHelper animalTypeI18nDaoHelper = I18nDaoHelper.withSimpleI18nKey(
                RefAnimalType.PROPERTY_ANIMAL_TYPE,
                language, TradRefVivant.class,
                "at");

        I18nDaoHelper animalPopUnitsI18nDaoHelper = I18nDaoHelper.withSimpleI18nKey(
                RefAnimalType.PROPERTY_ANIMAL_POPULATION_UNITS,
                language, TradRefVivant.class,
                "at");

        String query = "SELECT ls " +
                " , " + animalTypeI18nDaoHelper.coalesceTranslation() + " as animalType " +
                " , " + animalPopUnitsI18nDaoHelper.coalesceTranslation() + " as animalPopulationUnits " +
                " FROM " + getEntityClass().getName() + " ls " +
                " INNER JOIN ls." + LivestockUnit.PROPERTY_DOMAIN + " d " +
                " JOIN ls." + LivestockUnit.PROPERTY_REF_ANIMAL_TYPE + " at " +
                animalTypeI18nDaoHelper.leftJoinTranslation() +
                animalPopUnitsI18nDaoHelper.leftJoinTranslation() +
                " WHERE 1 = 1";
        
        // code
        query += DaoUtils.andAttributeEquals("d", Domain.PROPERTY_CODE, args, domainCode);
    
        // campaigns
        query += DaoUtils.andAttributeIn("d", Domain.PROPERTY_CAMPAIGN, args, campaigns);

        query += " ORDER BY ls." + LivestockUnit.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN + " DESC ";

        List<LivestockUnit> result = new LinkedList<>();
        try (Stream<Object[]> stream = stream(query, args)) {
            stream.forEach((Object[] row) -> {
                LivestockUnit livestockUnit = clone((LivestockUnit)row[0]);
                livestockUnit.getRefAnimalType().setAnimalType((String)row[1]);
                livestockUnit.getRefAnimalType().setAnimalPopulationUnits((String)row[2]);
                result.add(livestockUnit);
            });
        }

        return result;
    }


    public LivestockUnit findLivestockUnitForCattleCodeAndDomainId(String cattleCode, String domainId, Language language) {
        I18nDaoHelper animalTypeI18nDaoHelper = I18nDaoHelper.withSimpleI18nKey(
                RefAnimalType.PROPERTY_ANIMAL_TYPE,
                language, TradRefVivant.class,
                "at");

        I18nDaoHelper animalPopUnitsI18nDaoHelper = I18nDaoHelper.withSimpleI18nKey(
                RefAnimalType.PROPERTY_ANIMAL_POPULATION_UNITS,
                language, TradRefVivant.class,
                "at");

        String query = """
            SELECT ls, %s as animalType, %s as animalPopulationUnits
            FROM %s ls
            JOIN ls.refAnimalType at
            %s
            %s
            LEFT JOIN ls.cattles c
            WHERE ls.domain.topiaId = :domainId
            AND c.code = :cattleCode""".formatted(
                animalTypeI18nDaoHelper.coalesceTranslation(),
                animalPopUnitsI18nDaoHelper.coalesceTranslation(),
                getEntityClass().getName(),
                animalTypeI18nDaoHelper.leftJoinTranslation(),
                animalPopUnitsI18nDaoHelper.leftJoinTranslation());

        Object[] row = findAny(query, Map.of("domainId", domainId, "cattleCode", cattleCode));
        LivestockUnit livestockUnit = clone((LivestockUnit)row[0]);
        livestockUnit.getRefAnimalType().setAnimalType((String)row[1]);
        livestockUnit.getRefAnimalType().setAnimalPopulationUnits((String)row[2]);

        return livestockUnit;
    }

    public LivestockUnit findLivestockUnitForCattleCodeAndDomainCode(String cattleCode, String domainCode, Language language) {
        I18nDaoHelper animalTypeI18nDaoHelper = I18nDaoHelper.withSimpleI18nKey(
                RefAnimalType.PROPERTY_ANIMAL_TYPE,
                language, TradRefVivant.class,
                "at");

        I18nDaoHelper animalPopUnitsI18nDaoHelper = I18nDaoHelper.withSimpleI18nKey(
                RefAnimalType.PROPERTY_ANIMAL_POPULATION_UNITS,
                language, TradRefVivant.class,
                "at");


        //
        String query = """
            SELECT ls, %s as animalType, %s as animalPopulationUnits
            FROM %s ls
            JOIN ls.refAnimalType at
            %s
            %s
            LEFT JOIN ls.cattles c
            WHERE ls.domain.code = :domainCode
            AND c.code = :cattleCode""".formatted(
                animalTypeI18nDaoHelper.coalesceTranslation(),
                animalPopUnitsI18nDaoHelper.coalesceTranslation(),
                getEntityClass().getName(),
                animalTypeI18nDaoHelper.leftJoinTranslation(),
                animalPopUnitsI18nDaoHelper.leftJoinTranslation());

        Object[] row = findAny(query, Map.of("domainCode", domainCode, "cattleCode", cattleCode));
        LivestockUnit livestockUnit = clone((LivestockUnit)row[0]);
        livestockUnit.getRefAnimalType().setAnimalType((String)row[1]);
        livestockUnit.getRefAnimalType().setAnimalPopulationUnits((String)row[2]);

        return livestockUnit;
    }

    // Method required to mutate a LivestockUnit instance outside the hibernate session
    protected LivestockUnit clone(LivestockUnit src) {
        LivestockUnit entity = new LivestockUnitImpl();
        entity.setTopiaId(src.getTopiaId());
        entity.setCode(src.getCode());
        entity.setLivestockUnitSize(src.getLivestockUnitSize());
        entity.setPermanentGrasslandArea(src.getPermanentGrasslandArea());
        entity.setTemporaryGrasslandArea(src.getTemporaryGrasslandArea());
        entity.setPermanentMowedGrasslandArea(src.getPermanentMowedGrasslandArea());
        entity.setTemporaryMowedGrasslandArea(src.getTemporaryMowedGrasslandArea());
        entity.setForageCropsArea(src.getForageCropsArea());
        entity.setProducingFoodArea(src.getProducingFoodArea());
        entity.setHoldingStrawArea(src.getHoldingStrawArea());
        entity.setMassSelfSustainingPercent(src.getMassSelfSustainingPercent());
        entity.setComment(src.getComment());
        entity.setAverageStrawForBedding(src.getAverageStrawForBedding());
        entity.setAverageManureProduced(src.getAverageManureProduced());
        entity.setAverageLiquidEffluent(src.getAverageLiquidEffluent());
        entity.setAverageCompostedEffluent(src.getAverageCompostedEffluent());
        entity.setAverageMethanisedEffluent(src.getAverageMethanisedEffluent());
        entity.setDomain(src.getDomain());

        RefAnimalType refAnimalType = new RefAnimalTypeImpl();
        refAnimalType.setTopiaId(src.getRefAnimalType().getTopiaId());
        refAnimalType.setActive(src.getRefAnimalType().isActive());
        refAnimalType.setAnimalType(src.getRefAnimalType().getAnimalType());
        refAnimalType.setAnimalPopulationUnits(src.getRefAnimalType().getAnimalPopulationUnits());
        entity.setRefAnimalType(refAnimalType);

        if (CollectionUtils.isNotEmpty(src.getCattles())) {
            List<Cattle> cattles = src.getCattles().stream().map(this::clone).toList();
            entity.setCattles(cattles);
        } else {
            entity.setCattles(new ArrayList<>());
        }

        return entity;
    }

    protected Cattle clone(Cattle src) {
        Cattle entity = new CattleImpl();
        entity.setTopiaId(src.getTopiaId());
        entity.setCode(src.getCode());
        entity.setNumberOfHeads(src.getNumberOfHeads());
        entity.setAnimalType(src.getAnimalType());
        if (CollectionUtils.isNotEmpty(src.getRations())) {
            List<Ration> rations = src.getRations().stream().map(this::clone).toList();
            entity.setRations(rations);
        } else {
            entity.setRations(new ArrayList<>());
        }

        return entity;
    }

    protected Ration clone(Ration src) {
        Ration entity = new RationImpl();
        entity.setTopiaId(src.getTopiaId());
        entity.setStartingHalfMonth(src.getStartingHalfMonth());
        entity.setEndingHalfMonth(src.getEndingHalfMonth());
        if (CollectionUtils.isNotEmpty(src.getAliments())) {
            List<Aliment> aliments = src.getAliments().stream().map(this::clone).toList();
            entity.setAliments(aliments);
        } else {
            entity.setAliments(new ArrayList<>());
        }

        return entity;
    }

    protected Aliment clone(Aliment src) {
        Aliment entity = new AlimentImpl();
        entity.setTopiaId(src.getTopiaId());
        entity.setQuantity(src.getQuantity());
        entity.setAliment(src.getAliment());

        return entity;
    }

} //LivestockUnitTopiaDao
