package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.utils.DaoUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class RefActaTraitementsProduitsCategTopiaDao extends AbstractRefActaTraitementsProduitsCategTopiaDao<RefActaTraitementsProduitsCateg> {

    public LinkedHashMap<AgrosystInterventionType, List<ProductType>> findAllActiveActaTreatmentProductType() {
        Map<String, Object> args = DaoUtils.asArgsMap();

        String query = "SELECT DISTINCT" +
                " acta_tpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTION + ", acta_tpc." + RefActaTraitementsProduitsCateg.PROPERTY_TYPE_PRODUIT +
                " FROM " + getEntityClass().getName() + " acta_tpc " +
                " WHERE 1 = 1 ";
        query += DaoUtils.andAttributeEquals("acta_tpc", RefActaTraitementsProduitsCateg.PROPERTY_ACTIVE, args, true);
        query += " ORDER BY acta_tpc." + RefActaTraitementsProduitsCateg.PROPERTY_TYPE_PRODUIT + " ASC ";


        List<Object[]> queryResult = findAll(query, args);

        LinkedHashMap<AgrosystInterventionType, List<ProductType>> allActiveProductType = Maps.newLinkedHashMap();
        if (queryResult != null) {
            for (Object[] entry : queryResult) {
                AgrosystInterventionType agrosystInterventionType = (AgrosystInterventionType) entry[0];
                List<ProductType> treatmentsList = allActiveProductType.get(agrosystInterventionType);
                    if (treatmentsList == null) {
                    treatmentsList = new ArrayList<>();
                }
                treatmentsList.add((ProductType) entry[1]);
                allActiveProductType.put(agrosystInterventionType, treatmentsList);
            }
        }

        return allActiveProductType;
    }

} //RefActaTraitementsProduitsCategTopiaDao
