package fr.inra.agrosyst.api.entities.report;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.SortedColumn;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.services.report.ReportGrowingSystemFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.security.SecurityContext;
import fr.inra.agrosyst.services.security.SecurityHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ReportGrowingSystemTopiaDao extends AbstractReportGrowingSystemTopiaDao<ReportGrowingSystem> {

    private static final Log LOGGER = LogFactory.getLog(ReportGrowingSystemTopiaDao.class);

    public static final String REPORT_GROWING_SYSTEM_TO_DOMAIN = ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN;
    public static final String REPORT_GROWING_SYSTEM_TO_CAMPAIGN = REPORT_GROWING_SYSTEM_TO_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN;

    public PaginationResult<ReportGrowingSystem> findFilteredReportGrowingSystems(ReportGrowingSystemFilter filter, SecurityContext securityContext) throws TopiaException {

        StringBuilder query = new StringBuilder("FROM " + getEntityClass().getName() + " RGS");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = Maps.newLinkedHashMap();

        // apply non null filter
        applyFiltersOnQuery(filter, securityContext, query, args);
    
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
    
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("startIndex:" + startIndex + " endIndex:" + endIndex);
        }
    
        String queryString = query.toString();
    
        String filterOrderBy = getFilterOrderBy(filter);
    
        String queryAndOrder = queryString + filterOrderBy;
        List<ReportGrowingSystem> reportGrowingSystems = find(queryAndOrder, args, startIndex, endIndex);
        long totalCount = findUnique("SELECT count(*) " + queryString, args);
    
        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        PaginationResult<ReportGrowingSystem> result = PaginationResult.of(reportGrowingSystems, totalCount, pager);
        return result;
    }
    
    /**
     * @return - user selected ORDER BY or DEFAULT one
     */
    private String getFilterOrderBy(ReportGrowingSystemFilter filter) {
        String result = null;
        
        if (filter != null && filter.getSortedColumn() != null) {
            SortedColumn sortedColumn = filter.getSortedColumn();
            String filterOrderBy = switch (sortedColumn) {
                case REPORT -> " lower (RGS." + ReportGrowingSystem.PROPERTY_NAME + ")";
                case GROWING_SYSTEM -> " lower (RGS." + ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_NAME + ")";
                case GROWING_PLAN ->
                        " lower (RGS." + ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_NAME + ")";
                case DOMAIN -> " lower (RGS." + REPORT_GROWING_SYSTEM_TO_DOMAIN + ")";
                case DEPHY_NUMBER -> " lower (RGS." + ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_DEPHY_NUMBER + ")";
                case CAMPAIGN -> " RGS." + REPORT_GROWING_SYSTEM_TO_CAMPAIGN;
                default -> null;
            };
            if (StringUtils.isNotBlank(filterOrderBy)) {
                filterOrderBy += " " + filter.getSortOrder().name();
                result = " ORDER BY " +
                        filterOrderBy +
                        ", RGS." + TopiaEntity.PROPERTY_TOPIA_ID;
            }
            
        }
        
        if (result == null) {
            // default
            result = " ORDER BY " +
                    "RGS." + REPORT_GROWING_SYSTEM_TO_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN + " DESC" +
                    ", lower (RGS." + ReportGrowingSystem.PROPERTY_NAME + ")" +
                    ", RGS." + ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_NAME +
                    ", RGS." + ReportGrowingSystem.PROPERTY_TOPIA_ID;
        }
        
        return result;
    }
    
    public Set<String> findFilteredReportGrowingSystemIds(ReportGrowingSystemFilter filter, SecurityContext securityContext) throws TopiaException {
        
        StringBuilder query = new StringBuilder("SELECT RGS.topiaId FROM " + getEntityClass().getName() + " RGS");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = Maps.newLinkedHashMap();
        applyFiltersOnQuery(filter, securityContext, query, args);
        
        String queryString = query.toString();
        String queryAndOrder = queryString + " ORDER BY " +
                "RGS." + REPORT_GROWING_SYSTEM_TO_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN + " DESC" +
                ", lower (RGS." + ReportGrowingSystem.PROPERTY_NAME + ")" +
                ", RGS." + ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_NAME +
                ", RGS." + ReportGrowingSystem.PROPERTY_TOPIA_ID;
        return new HashSet<>(findAll(queryAndOrder, args));
    }

    private void applyFiltersOnQuery(ReportGrowingSystemFilter filter, SecurityContext securityContext, StringBuilder query, Map<String, Object> args) {
        // apply non null filter
        if (filter != null) {

            // name
            query.append(DaoUtils.andAttributeLike("RGS", ReportGrowingSystem.PROPERTY_NAME, args, filter.getName()));

            // campaign
            query.append(DaoUtils.andAttributeEquals("RGS", REPORT_GROWING_SYSTEM_TO_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN, args, filter.getCampaign()));

            // growingSystemName
            query.append(DaoUtils.andAttributeLike("RGS", ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_NAME, args, filter.getGrowingSystemName()));

            // growingPlanName
            query.append(DaoUtils.andAttributeLike("RGS", ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_NAME, args, filter.getGrowingPlanName()));

            // domainName
            query.append(DaoUtils.andAttributeLike("RGS", REPORT_GROWING_SYSTEM_TO_DOMAIN + "." + Domain.PROPERTY_NAME, args, filter.getDomainName()));

            // dephy
            query.append(DaoUtils.andAttributeLike("RGS", ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_DEPHY_NUMBER, args, filter.getDephyNumber()));


            // Navigation context
            NavigationContext navigationContext = filter.getNavigationContext();
            if (navigationContext != null) {

                // campaigns
                Set<Integer> ncCampaigns = navigationContext.getCampaigns();
                query.append(DaoUtils.andAttributeInIfNotEmpty("RGS", REPORT_GROWING_SYSTEM_TO_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN, args, ncCampaigns));

                // domains
                Set<String> ncDomains = navigationContext.getDomains();
                query.append(DaoUtils.andAttributeInIfNotEmpty("RGS", REPORT_GROWING_SYSTEM_TO_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID, args, ncDomains));

                // growingPlans
                Set<String> ncGropwingPlans = navigationContext.getGrowingPlans();
                query.append(DaoUtils.andAttributeInIfNotEmpty("RGS", ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_TOPIA_ID, args, ncGropwingPlans));

                // growingSystems && network
                Set<String> ncGropwingSystems = Sets.newHashSet(navigationContext.getGrowingSystems());
                if (navigationContext.getNetworksCount() > 0) {
                    Set<String> growingSystemIds = getProjectionHelper().networksToGrowingSystems(navigationContext.getNetworks());
                    ncGropwingSystems.addAll(growingSystemIds);
                }
                query.append(DaoUtils.andAttributeInIfNotEmpty("RGS", ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID, args, ncGropwingSystems));

            }
        }

        // security
        SecurityHelper.addReportGrowingSystemFilter(query, args, securityContext, "RGS");
    }

    public String findGrowingSystemIdForTopiaIdEquals(String reportGrowingSystemId) {
        String query = " SELECT rgs." + ReportGrowingSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID +
                " FROM " + ReportGrowingSystem.class.getName() + " rgs " +
                " WHERE  rgs." + ReportGrowingSystem.PROPERTY_TOPIA_ID + " = :rgsId";
        String result = findUnique(query, DaoUtils.asArgsMap("rgsId", reportGrowingSystemId));
        return result;
    }

    public <T> List<T> findReportGrowingSystemCollections(String reportGrowingSystemId, String property) {
        String query = "SELECT rgs." + property + " FROM " + getEntityClass().getName() + " rgs ";
        query +=       "WHERE rgs." + ReportGrowingSystem.PROPERTY_TOPIA_ID + " = :id";
        List<T> result = findAll(query, DaoUtils.asArgsMap("id", reportGrowingSystemId));
        return result;
    }

    public <T> List<T> findReportGrowingSystemCollectionsForPreviousCampaign(String reportGrowingSystemId, String property) {
        String query = "SELECT rgs." + property + " FROM " + getEntityClass().getName() + " rgs ";
        query +=       "WHERE rgs." + ReportGrowingSystem.PROPERTY_CODE + " = (";
        query +=       "  SELECT rgs2." + ReportGrowingSystem.PROPERTY_CODE + " FROM " + getEntityClass().getName() + " rgs2";
        query +=       "  WHERE rgs2." + ReportGrowingSystem.PROPERTY_TOPIA_ID + " = :id)";
        query +=       "AND rgs." + REPORT_GROWING_SYSTEM_TO_CAMPAIGN + " = (";
        query +=       "  SELECT rgs2." + REPORT_GROWING_SYSTEM_TO_CAMPAIGN + " FROM " + getEntityClass().getName() + " rgs2";
        query +=       "  WHERE rgs2." + ReportGrowingSystem.PROPERTY_TOPIA_ID + " = :id) - 1";
        Map<String, Object> args = DaoUtils.asArgsMap("id", reportGrowingSystemId);
        List<T> result = findAll(query, args);
        return result;
    }

    public LinkedHashMap<Integer, String> findAllRelatedReports(String code) {
        String query = "SELECT " + REPORT_GROWING_SYSTEM_TO_CAMPAIGN + ", " + ReportGrowingSystem.PROPERTY_TOPIA_ID
                + " FROM " + getEntityClass().getName()
                + " WHERE " + ReportGrowingSystem.PROPERTY_CODE + " = :code"
                + " ORDER BY " + REPORT_GROWING_SYSTEM_TO_CAMPAIGN + " DESC";
        List<Object[]> reports = findAll(query, DaoUtils.asArgsMap("code", code));
        LinkedHashMap<Integer, String> result = DaoUtils.toRelatedMap(reports);
        return result;
    }


} //ReportGrowingSystemTopiaDao
