package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA, CodeLutin
 * Copyright (C) 2020 - 2023 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.services.performance.EquipmentUsageRange;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AbstractRefMaterielTopiaDao<E extends RefMateriel> extends GeneratedRefMaterielTopiaDao<E> {

    /**
     * Recherche dans la liste des RefMaterielAutomoteur les valeurs de materielType1
     *
     * @return materielType1 value
     */
    public List<String> findTypeMateriel1WithoutPetitMaterielValues(boolean filterEdaplos) throws TopiaException {
        return findPropertyValues(
                RefMateriel.PROPERTY_TYPE_MATERIEL1,
                null,
                null,
                null,
                null,
                null,
                filterEdaplos);
    }
    
    /**
     * Recherche dans la liste des RefMateriel les valeurs de materielType[2-4] suivant les filtres
     * typeMateriel[1-3]
     *
     * @param property      property to extract
     * @param typeMateriel1 optionnal type 1 filter
     * @param typeMateriel2 optionnal type 2 filter
     * @param typeMateriel3 optionnal type 3 filter
     * @param petitMateriel not use there
     * @return Le nom des matériels, hors petits matériel
     */
    public List<String> findPropertyValues(
            String property,
            String typeMateriel1,
            String typeMateriel2,
            String typeMateriel3,
            Boolean petitMateriel) throws TopiaException {

        return findPropertyValues(
                property,
                typeMateriel1,
                typeMateriel2,
                typeMateriel3,
                null,
                petitMateriel,
                false);
    }

    public List<String> findPropertyValues(
            String property,
            String typeMateriel1,
            String typeMateriel2,
            String typeMateriel3,
            String typeMateriel4,
            Boolean petitMateriel,
            boolean filterEdaplos) throws TopiaException {


        String querySelectPart = "SELECT distinct( m." + property + " ) " +
                " FROM " + getEntityClass().getName() + " m " +
                " WHERE m." + RefMateriel.PROPERTY_ACTIVE + " IS true";

        Pair<String, Map<String, Object>> queryBody = getQueryBody(
                "m",
                typeMateriel1,
                typeMateriel2,
                typeMateriel3,
                typeMateriel4,
                petitMateriel,
                filterEdaplos);

        String queryBodyPart = queryBody.getKey();
        Map<String, Object> args = queryBody.getValue();

        String query = querySelectPart + queryBodyPart;

        List<String> result = findAll(query, args);

        return result;
    }

    private Pair<String, Map<String, Object>> getQueryBody(
            String alias,
            String typeMateriel1,
            String typeMateriel2,
            String typeMateriel3,
            String typeMateriel4,
            Boolean petitMateriel,
            boolean filterEdaplos) {

        String query = "";

        Map<String, Object> args = new HashMap<>();
        query += DaoUtils.andAttributeEquals(alias, RefMateriel.PROPERTY_TYPE_MATERIEL1, args, typeMateriel1);
        query += DaoUtils.andAttributeEquals(alias, RefMateriel.PROPERTY_TYPE_MATERIEL2, args, typeMateriel2);
        query += DaoUtils.andAttributeEquals(alias, RefMateriel.PROPERTY_TYPE_MATERIEL3, args, typeMateriel3);
        query += DaoUtils.andAttributeEquals(alias, RefMateriel.PROPERTY_TYPE_MATERIEL4, args, typeMateriel4);
        if (RefMaterielOutil.class.equals(getEntityClass())) {
            query += DaoUtils.andAttributeEquals(alias, RefMaterielOutil.PROPERTY_PETIT_MATERIEL, args, BooleanUtils.isTrue(petitMateriel));
        }
        if (filterEdaplos) {
            query += DaoUtils.andNotAttributeLike(alias, RefMateriel.PROPERTY_SOURCE, args, "EDAPLOS");
        }

        return Pair.of(query, args);
    }


    /**
     * Recherche dans la liste des RefMateriel les valeurs de deux colonnes suivant les filtres
     * typeMateriel[1-4]
     *
     * @param property      property key to extract
     * @param property2     property value to extract
     * @param typeMateriel1 optionnal type 1 filter
     * @param typeMateriel2 optionnal type 2 filter
     * @param typeMateriel3 optionnal type 3 filter
     * @param typeMateriel4 optionnal type 4 filter
     * @param petitMateriel optionnal boolean
     * @return the ref identifier
     */
    public Map<String, String[]> findPropertyValuesAsMap(
            String property,
            String property2,
            String typeMateriel1,
            String typeMateriel2,
            String typeMateriel3,
            String typeMateriel4,
            Boolean petitMateriel) throws TopiaException {

        String querySelectPart = "SELECT m." + TopiaEntity.PROPERTY_TOPIA_ID +
                ", m." + property +
                ", m." + property2 +
                " FROM " + getEntityClass().getName() + " m " +
                " WHERE m." + RefMateriel.PROPERTY_ACTIVE + " IS true";

        Pair<String, Map<String, Object>> queryBody = getQueryBody(
                "m",
                typeMateriel1,
                typeMateriel2,
                typeMateriel3,
                typeMateriel4,
                petitMateriel, false);

        String queryBodyPart = queryBody.getKey();
        Map<String, Object> args = queryBody.getValue();

        String query = querySelectPart + queryBodyPart + " ORDER BY m." + property + " ASC ";

        List<Object[]> rows = findAll(query, args);
        Map<String, String[]> result = Maps.newLinkedHashMap();
        for (Object[] row : rows) {
            String topiaId = (String) row[0];
            String[] properties1And2Values = {String.valueOf(row[1]), (String) row[2]};
            result.put(topiaId, properties1And2Values);
        }
        return result;
    }
    
    public Collection<EquipmentUsageRange> findEquipmentsUsages(Collection<RefMateriel> equipments) throws TopiaException {
        String query = "SELECT m0,  m1." + RefMateriel.PROPERTY_UNITE_PAR_AN + ", m1." + RefMateriel.PROPERTY_UNITE +
                " FROM " + RefMateriel.class.getName() + " m0, "
                         + RefMateriel.class.getName() + " m1 " +
                         
                " WHERE m0 IN (:equipments)" +
                " AND   m1." + RefMateriel.PROPERTY_ACTIVE + " = true" +
                " AND   m0." + RefMateriel.PROPERTY_TYPE_MATERIEL1 + " = " + "m1." + RefMateriel.PROPERTY_TYPE_MATERIEL1 +
                " AND   m0." + RefMateriel.PROPERTY_TYPE_MATERIEL2 + " = " + "m1." + RefMateriel.PROPERTY_TYPE_MATERIEL2 +
                " AND   m0." + RefMateriel.PROPERTY_TYPE_MATERIEL3 + " = " + "m1." + RefMateriel.PROPERTY_TYPE_MATERIEL3 +
                " AND   m0." + RefMateriel.PROPERTY_TYPE_MATERIEL4 + " = " + "m1." + RefMateriel.PROPERTY_TYPE_MATERIEL4 +
                " AND   m1." + RefMateriel.PROPERTY_UNITE + " IS NOT NULL" +
                
                " ORDER BY m0, m1." + RefMateriel.PROPERTY_UNITE_PAR_AN;
        
        List<Object[]> result0 = findAll(query, DaoUtils.asArgsMap("equipments", equipments));
        
        Map<RefMateriel, EquipmentUsageRange> intermediateResult = new HashMap<>();
        
        for (Object[] row : result0) {
            RefMateriel materiel = (RefMateriel) row[0];
    
            EquipmentUsageRange equipmentUsageRange = intermediateResult.get(materiel);
            if (equipmentUsageRange == null) {
                equipmentUsageRange = new EquipmentUsageRange(materiel);
                intermediateResult.put(materiel, equipmentUsageRange);
            }
            final double uniteParAn = (double) row[1];
            final String unit = (String) row[2];
            equipmentUsageRange.pushValue(uniteParAn, unit);
        }

        return intermediateResult.values();
    }

    public ReferentialTranslationMap fillTranslations(Collection<String> topiaIdList, ReferentialTranslationMap translationMap) {
        List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(translationMap.getLanguage(), "rm");

        I18nDaoHelper.fillRefEntitiesTranslations(this, "rm", i18nDaoHelpers, topiaIdList, translationMap);

        return translationMap;
    }

    protected List<I18nDaoHelper> getI18nDaoHelpers(Language language, String entityAlias) {
        return Lists.newArrayList(
                I18nDaoHelper.withComplexI18nKey(RefMateriel.PROPERTY_TYPE_MATERIEL1, language, TradRefMateriel.class, entityAlias),
                I18nDaoHelper.withComplexI18nKey(RefMateriel.PROPERTY_TYPE_MATERIEL2, language, TradRefMateriel.class, entityAlias),
                I18nDaoHelper.withComplexI18nKey(RefMateriel.PROPERTY_TYPE_MATERIEL3, language, TradRefMateriel.class, entityAlias),
                I18nDaoHelper.withComplexI18nKey(RefMateriel.PROPERTY_TYPE_MATERIEL4, language, TradRefMateriel.class, entityAlias),
                I18nDaoHelper.withComplexI18nKey(RefMateriel.PROPERTY_UNITE, language, TradRefDivers.class, entityAlias)
        );
    }
} //AbstractRefMaterielTopiaDao<E extends RefMateriel>
