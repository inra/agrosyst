package fr.inra.agrosyst.api.entities.effective;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Zone;

import java.util.HashMap;
import java.util.Map;

public class EffectiveCropCycleNodeTopiaDao extends AbstractEffectiveCropCycleNodeTopiaDao<EffectiveCropCycleNode> {

    /**
     * Recherche pour une zone, le dernier nœud défini pour la campagne précédente.
     * 
     * @param zone zone
     * @return last year node (or {@code null})
     */
    public EffectiveCropCycleNode findLastNodeForPreviousCampaign(Zone zone) {
        String query = "FROM " + getEntityClass().getName() + " N" +
                " WHERE exists (" +
                "  FROM " + EffectiveSeasonalCropCycle.class.getName() + " CC" +
                "   WHERE N in elements(CC.nodes) AND CC.zone.code = :code AND CC.zone.plot.domain.campaign = :campaign" +
                ") ORDER by N.rank DESC";

        Map<String, Object> args = new HashMap<>();
        args.put("code", zone.getCode());
        args.put("campaign", zone.getPlot().getDomain().getCampaign() - 1);
        EffectiveCropCycleNode result = findFirstOrNull(query, args);
        return result;
    }

} //EffectiveCropCycleNodeTopiaDao
