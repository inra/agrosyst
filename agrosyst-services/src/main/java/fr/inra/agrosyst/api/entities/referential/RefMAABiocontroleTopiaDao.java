package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class RefMAABiocontroleTopiaDao extends AbstractRefMAABiocontroleTopiaDao<RefMAABiocontrole> {

    public Collection<String> getActiveRefMaaBioControle(int debutCampagne, int finCampagne) {
        String query = "SELECT r." + RefMAABiocontrole.PROPERTY_CODE_AMM + " FROM " + getEntityClass().getName() + " r" +
                " WHERE r." + RefMAABiocontrole.PROPERTY_BIOCONTROL + " = true" +
                " AND (r." + RefMAABiocontrole.PROPERTY_CODE_AMM + ", r." + RefMAABiocontrole.PROPERTY_CAMPAGNE + ") IN (" +
                    "SELECT r2." + RefMAABiocontrole.PROPERTY_CODE_AMM + ", MAX(r2." + RefMAABiocontrole.PROPERTY_CAMPAGNE + ")" +
                    " FROM " + getEntityClass().getName() + " r2" +
                    " WHERE r2." + RefMAABiocontrole.PROPERTY_ACTIVE + " = true" +
                    " AND r2." + RefMAABiocontrole.PROPERTY_CAMPAGNE + " BETWEEN :debutCampagne AND :finCampagne" +
                    " GROUP BY r2." + RefMAABiocontrole.PROPERTY_CODE_AMM +
                ")";
        Map<String, Object> args = new HashMap<>();
        args.put("debutCampagne", debutCampagne);
        args.put("finCampagne", finCampagne);
        return findAll(query, args);
    }

} //RefMAABiocontroleTopiaDao
