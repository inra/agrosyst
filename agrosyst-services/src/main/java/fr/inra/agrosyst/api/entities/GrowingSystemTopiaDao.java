package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.SortedColumn;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeCategory;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.services.common.GrowingSystemsIndicator;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.network.NetworkFilter;
import fr.inra.agrosyst.api.services.report.ReportFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.security.SecurityContext;
import fr.inra.agrosyst.services.security.SecurityHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class GrowingSystemTopiaDao extends AbstractGrowingSystemTopiaDao<GrowingSystem> {

    public static final String PROPERTY_GROWING_PLAN_DOMAIN = GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN;
    public static final String PROPERTY_GROWING_PLAN_DOMAIN_ID = PROPERTY_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID;
    public static final String PROPERTY_GROWING_PLAN_ID = GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_TOPIA_ID;
    public static final String PROPERTY_GROWING_PLAN_NAME = GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_NAME;
    public static final String PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN = PROPERTY_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN;
    public static final String PROPERTY_GROWING_PLAN_DOMAIN_NAME = PROPERTY_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_NAME;

    public Set<String> getFilteredGrowingSystemIds(GrowingSystemFilter filter, SecurityContext securityContext) throws TopiaException {
        StringBuilder query = new StringBuilder("SELECT gs.topiaId FROM " + getEntityClass().getName() + " gs");
        query.append(" WHERE 1 = 1 ");
        Map<String, Object> args = new LinkedHashMap<>();
        applyFiltersOnQuery(filter, securityContext, query, args);
        return new HashSet<>(findAll(query.toString(), args));
    }

    public PaginationResult<GrowingSystem> getFilteredGrowingSystems(GrowingSystemFilter filter, SecurityContext securityContext) {
        StringBuilder query = new StringBuilder("FROM " + getEntityClass().getName() + " gs");
        query.append(" WHERE 1 = 1 ");
        Map<String, Object> args = new LinkedHashMap<>();

        // apply non null filter
        applyFiltersOnQuery(filter, securityContext, query, args);
    
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
    
        // refs:9570 TopiaQueryException: unable to find page startIndex=-1, endIndex=-3
        startIndex = Math.max(startIndex, 0);
        endIndex = Math.max(endIndex, DaoUtils.NO_PAGE_LIMIT);
    
        String queryString = query.toString();
        String filterOrderBy = getFilterOrderBy(filter);
    
        List<GrowingSystem> growingSystems = find(queryString + filterOrderBy, args, startIndex, endIndex);
        long totalCount = findUnique("SELECT count(*) " + queryString, args);
    
        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        return PaginationResult.of(growingSystems, totalCount, pager);
    }

    protected void applyFiltersOnQuery(GrowingSystemFilter filter, SecurityContext securityContext, StringBuilder query, Map<String, Object> args) {
        // apply non null filter
        if (filter != null) {
            addFilterQuery(filter, query, args);
        }
        SecurityHelper.addGrowingSystemFilter(query, args, securityContext, "gs");
    }

    public List<String> findAllDephyNb(SecurityContext securityContext) {
        StringBuilder query = new StringBuilder("SELECT distinct(gs." + GrowingSystem.PROPERTY_DEPHY_NUMBER + ") FROM " + getEntityClass().getName() + " gs");
        query.append(" WHERE gs."+ GrowingSystem.PROPERTY_DEPHY_NUMBER + " IS NOT NULL ");
        Map<String, Object> args = new LinkedHashMap<>();

        SecurityHelper.addGrowingSystemFilter(query, args, securityContext, "gs");

        return findAll(query.toString(), args);
    }

    protected void addFilterQuery(GrowingSystemFilter filter, StringBuilder query, Map<String, Object> args) {
        // selected ids
        query.append(DaoUtils.andAttributeIn("gs", GrowingSystem.PROPERTY_TOPIA_ID, args, DaoUtils.getRealIds(filter.getSelectedIds(), GrowingSystem.class)));
        // SDC name
        query.append(DaoUtils.andAttributeLike("gs", GrowingSystem.PROPERTY_NAME, args, filter.getGrowingSystemName()));
        // SDC numéro DEPHY
        query.append(DaoUtils.andAttributeLike("gs", GrowingSystem.PROPERTY_DEPHY_NUMBER, args, filter.getDephyNumber()));
        // SDC filière
        query.append(DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_SECTOR, args, filter.getSector()));
        // Domain name
        query.append(DaoUtils.andAttributeLike("gs", PROPERTY_GROWING_PLAN_DOMAIN_NAME, args, filter.getDomainName()));
        // Growing plan name
        query.append(DaoUtils.andAttributeLike("gs", PROPERTY_GROWING_PLAN_NAME, args, filter.getGrowingPlanName()));
        // active
        query.append(DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_ACTIVE, args, filter.getActive()));

        if (filter.getActiveParents() != null && filter.getActiveParents()) {
            query.append(DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_ACTIVE, args, filter.getActive()));
            query.append(DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_ACTIVE, args, filter.getActive()));
        }
        // validated
        query.append(DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_VALIDATED, args, filter.getValidated()));
        // campaign
        query.append(DaoUtils.andAttributeEquals("gs", PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN, args, filter.getCampaign()));
        // network
        if (StringUtils.isNotEmpty(filter.getNetwork())) {
            NetworkTopiaDao networkDAO = topiaDaoSupplier.getDao(Network.class, NetworkTopiaDao.class);
            NetworkFilter networkFilter = new NetworkFilter();
            networkFilter.setNetworkName(filter.getNetwork());
            networkFilter.setAllPageSize();
            PaginationResult<Network> networks = networkDAO.getFilteredNetworks(networkFilter, null);
    
            Set<String> networkIds = new HashSet<>(Collections2.transform(networks.getElements(), Entities.GET_TOPIA_ID::apply));
            Set<String> growingSystemIds = getProjectionHelper().networksToGrowingSystems(networkIds);
            query.append(DaoUtils.andAttributeIn("gs", GrowingSystem.PROPERTY_TOPIA_ID, args, growingSystemIds));
        }
        // Navigation context
        NavigationContext navigationContext = filter.getNavigationContext();
        if (navigationContext != null) {
            addNavigationContextQuery(query, args, navigationContext);
        }
    }
    
    /**
     * do not manage SortedColumn.NETWORK
     *
     * @return - user selected ORDER BY or DEFAULT one
     */
    private String getFilterOrderBy(GrowingSystemFilter filter) {
        String result = null;
        
        if (filter != null && filter.getSortedColumn() != null) {
            SortedColumn sortedColumn = filter.getSortedColumn();
            String filterOrderBy = switch (sortedColumn) {
                case GROWING_SYSTEM -> " lower (gs." + GrowingSystem.PROPERTY_NAME + ")";
                case GROWING_PLAN -> " lower (gs." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_NAME + ")";
                case DOMAIN -> " lower (gs." + PROPERTY_GROWING_PLAN_DOMAIN + ")";
                case CAMPAIGN -> " gs." + PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN;
                case DEPHY_NUMBER -> "COALESCE (" + " gs." + GrowingSystem.PROPERTY_DEPHY_NUMBER + ", '###')";
                default -> null;
            };
            if (StringUtils.isNotBlank(filterOrderBy)) {
                filterOrderBy += " " + filter.getSortOrder().name();
                result = " ORDER BY " +
                        filterOrderBy +
                        ", gs." + TopiaEntity.PROPERTY_TOPIA_ID;
            }
            
        }
        
        if (result == null) {
            // default
            result = " ORDER BY " +
                    "gs." + PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN + " DESC" +
                    ", gs." + PROPERTY_GROWING_PLAN_DOMAIN_NAME +
                    ", gs." + PROPERTY_GROWING_PLAN_NAME +
                    ", lower ( gs." + GrowingSystem.PROPERTY_NAME + " )" +
                    ", gs." + TopiaEntity.PROPERTY_TOPIA_ID;
        }
        
        return result;
    }
    
    protected void addNavigationContextQuery(StringBuilder query, Map<String, Object> args, NavigationContext navigationContext) {
        
        if (navigationContext != null) {
            // campaigns
            query.append(DaoUtils.andAttributeInIfNotEmpty("gs", PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN, args, navigationContext.getCampaigns()));
            
            // networks
            if (navigationContext.getNetworksCount() > 0) {
                Set<String> growingSystemIds = getProjectionHelper().networksToGrowingSystems(navigationContext.getNetworks());
                query.append(DaoUtils.andAttributeIn("gs", GrowingSystem.PROPERTY_TOPIA_ID, args, growingSystemIds));
            }

            // domains
            query.append(DaoUtils.andAttributeInIfNotEmpty("gs", PROPERTY_GROWING_PLAN_DOMAIN_ID, args, DaoUtils.getRealIds(navigationContext.getDomains(), Domain.class)));

            // growingPlans
            query.append(DaoUtils.andAttributeInIfNotEmpty("gs", PROPERTY_GROWING_PLAN_ID, args, DaoUtils.getRealIds(navigationContext.getGrowingPlans(), GrowingPlan.class)));

            // growingSystems
            query.append(DaoUtils.andAttributeInIfNotEmpty("gs", GrowingSystem.PROPERTY_TOPIA_ID, args, DaoUtils.getRealIds(navigationContext.getGrowingSystems(), GrowingSystem.class)));

        }
    }

    /**
     * Find all growingSystems using same growingSystem's duplication code.
     *
     * @return related growingSystem
     */
    public LinkedHashMap<Integer, String> findAllRelatedGrowingSystemsIdByCampaigns(String code) {
        String query = "SELECT " + PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN + ", " + GrowingSystem.PROPERTY_TOPIA_ID
                + " FROM " + getEntityClass().getName()
                + " WHERE " + GrowingSystem.PROPERTY_CODE + " = :code"
                + " ORDER BY " + PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN + " DESC";
        List<Object[]> growingSystems = findAll(query, DaoUtils.asArgsMap("code", code));
        return DaoUtils.toRelatedMap(growingSystems);
    }
    
    public GrowingSystem findLastGrowingSystemForCampaigns(String code, Set<Integer> practicedSystemCampaigns) {
        
        String query = " FROM " + getEntityClass().getName() + " gs "
                + " WHERE gs." + GrowingSystem.PROPERTY_CODE + " = :code"
                + " AND gs." + GrowingSystem.PROPERTY_ACTIVE + " IS TRUE"
                + " AND gs." + PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN + " IN :practicedSystemCampaigns"
                + " ORDER BY gs." + PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN + " DESC";

        return findFirstOrNull(query, DaoUtils.asArgsMap("code", code, "practicedSystemCampaigns", practicedSystemCampaigns));
    }
    
    public List<GrowingSystem> findAllByCodeAndCampaign(String code, Set<Integer> campaigns) {
        Preconditions.checkArgument(campaigns != null && !campaigns.isEmpty());
        List<GrowingSystem> result;
        String query = "FROM " + getEntityClass().getName() + " gs WHERE 1 = 1";
        Map<String, Object> args = new LinkedHashMap<>();
        query += DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_CODE, args, code);
        query += DaoUtils.andAttributeIn("gs", PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN, args, campaigns);
        result = findAll(query+" ORDER BY gs." + PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN + " ASC ", args);
        return result;
    }

    public List<GrowingSystem> findAllActiveWritableByDomain(Domain domain, SecurityContext securityContext) {

        StringBuilder query = new StringBuilder(" FROM " + getEntityClass().getName() + " gs WHERE 1 = 1 ");
        Map<String, Object> args = new LinkedHashMap<>();

        query.append(DaoUtils.andAttributeEquals("gs", PROPERTY_GROWING_PLAN_DOMAIN, args, domain));
        query.append(DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_ACTIVE, args, true));

        SecurityHelper.addWritableGrowingSystemFilter(query, args, securityContext, "gs");

        query.append(" ORDER BY LOWER ( gs." + GrowingSystem.PROPERTY_NAME + " ) ");

        return findAll(query.toString(), args);
    }

    public List<GrowingSystem> findAllAvailableForManagementModeDuplication(List<GrowingSystem> growingSystems, GrowingSystem managementModeGrowingSystem) {

        String query = "FROM " + getEntityClass().getName() + " gs WHERE 1 = 1";
        Map<String, Object> args = new LinkedHashMap<>();

        // Ne sont disponibles que les systèmes de culture qui n'ont pas de mode de gestion portant dessus.
        query += DaoUtils.andAttributeEquals("gs", PROPERTY_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_CODE, args, managementModeGrowingSystem.getGrowingPlan().getDomain().getCode());
        query += DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_ACTIVE, args, true);

        query += (" AND gs NOT IN (" +
                "   SELECT " +
                "   mm." + ManagementMode.PROPERTY_GROWING_SYSTEM +
                "   FROM " + ManagementMode.class.getName() + " mm" +
                " )");

        if (CollectionUtils.isNotEmpty(growingSystems)) {
            query += (" AND gs IN (:growingSystems)");
            args.put("growingSystems", growingSystems);
        }

        return CollectionUtils.isNotEmpty(growingSystems) ? findAll(query, args) : new ArrayList<>();
    }

    public List<GrowingSystem> findAllAvailableFoPracticedSystemDuplication(GrowingSystemFilter growingSystemFilter, GrowingSystem growingSystem) {
        Map<String, Object> args = new LinkedHashMap<>();
        StringBuilder query = new StringBuilder("FROM " + getEntityClass().getName() + " gs");
        query.append(" WHERE 1 = 1");
    
        addFilterQuery(growingSystemFilter, query, args);
        final String domainCode = growingSystem.getGrowingPlan().getDomain().getCode();
        query.append(
                DaoUtils.andAttributeEquals(
                        "gs",
                        PROPERTY_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_CODE,
                        args, domainCode));

        return findAll(query.toString(), args);
    }


    public Set<String> getAllGrowingSystemCodes() {
        String query = "SELECT DISTINCT " + GrowingSystem.PROPERTY_CODE +
                " FROM " + getEntityClass().getName();
        List<String> list = findAll(query, DaoUtils.asArgsMap());
        return new HashSet<>(list);
    }

    public void updateGrowingSystemValidateStatus(String growingSystemId, LocalDateTime now, boolean validate) {

        Map<String, Object> args = DaoUtils.asArgsMap("growingSystemId", growingSystemId, "now", now, "validate", validate);
        topiaJpaSupport.execute("UPDATE " + getEntityClass().getName() + " gs" +
                " SET gs.validated=:validate, gs.validationDate=:now, gs.updateDate=:now" +
                " WHERE gs." + GrowingSystem.PROPERTY_TOPIA_ID + "=:growingSystemId", args);
    }

    public Iterable<String> getAllNetworksUsedByGrowingSystems() {
        String hql = " SELECT DISTINCT gs. " + GrowingSystem.PROPERTY_NETWORKS +
                " FROM " + getEntityClass().getName() + " gs ";
        List<Network> networks = findAll(hql, DaoUtils.asArgsMap());

        return networks.stream().map(Entities.GET_TOPIA_ID).collect(Collectors.toList());
    }

    public List<GrowingSystem> getGrowingSystemsForManagementMode(NavigationContext navigationContext, SecurityContext securityContext) {
        Map<String, Object> args = new LinkedHashMap<>();

        StringBuilder query = new StringBuilder(" FROM " + GrowingSystem.class.getName() + " gs ");
        query.append(" WHERE gs NOT IN")
                .append("(SELECT " + "   mm1." + ManagementMode.PROPERTY_GROWING_SYSTEM + "   FROM ").append(ManagementMode.class.getName()).append(" mm1,").append(ManagementMode.class.getName()).append(" mm2 ")
                .append("   WHERE mm1.").append(ManagementMode.PROPERTY_CATEGORY).append(" = :prevu ")
                .append("   AND mm2.").append(ManagementMode.PROPERTY_CATEGORY).append(" = :constate ")
                .append("   AND mm1.").append(ManagementMode.PROPERTY_GROWING_SYSTEM).append(" = mm2.").append(ManagementMode.PROPERTY_GROWING_SYSTEM).append(" ) ")
                
                .append(" AND gs.").append(GrowingSystem.PROPERTY_ACTIVE).append(" = true ")
                .append(" AND gs.").append(GrowingSystem.PROPERTY_GROWING_PLAN).append(".").append(GrowingPlan.PROPERTY_ACTIVE).append(" = true ")
                .append(" AND gs.").append(GrowingSystem.PROPERTY_GROWING_PLAN).append(".").append(GrowingPlan.PROPERTY_DOMAIN).append(".").append(Domain.PROPERTY_ACTIVE).append(" = true ");

        args.put("prevu", ManagementModeCategory.PLANNED);
        args.put("constate", ManagementModeCategory.OBSERVED);

        addNavigationContextQuery(query, args, navigationContext);

        SecurityHelper.addWritableGrowingSystemFilter(query, args, securityContext, "gs");

        return findAll(query + " ORDER BY gs." + GrowingSystem.PROPERTY_NAME, args);
    }

    protected GrowingSystemsIndicator getIndicator(List<GrowingSystemsIndicator> indicators, Object o) {
        TypeDEPHY expectedDephy = null;
        if (o instanceof TypeDEPHY) {
            expectedDephy = (TypeDEPHY)o;
        }
        Sector expectedSector = null;
        if (o instanceof Sector) {
            expectedSector = (Sector)o;
        }
        for (GrowingSystemsIndicator indicator : indicators) {
            if (Objects.equals(indicator.getSector(), expectedSector)
                    && Objects.equals(indicator.getTypeDephy(), expectedDephy)) {
                return indicator;
            }
        }
        GrowingSystemsIndicator result;
        if (expectedDephy != null) {
            result = new GrowingSystemsIndicator(expectedDephy);
        } else if (expectedSector != null) {
            result = new GrowingSystemsIndicator(expectedSector);
        } else {
            result = new GrowingSystemsIndicator();
        }
        indicators.add(result);
        return result;
    }

    public List<GrowingSystemsIndicator> networksToGrowingSystemIndicators(Set<String> networkIds) {

        List<GrowingSystemsIndicator> result = new ArrayList<>();

        GrowingSystemsIndicator gsIndicator = new GrowingSystemsIndicator();
        
        final Set<String> growingSystemIds = getProjectionHelper().networksToGrowingSystems(networkIds);
        gsIndicator.setCount(growingSystemIds.size());
        
        List<GrowingSystem> growingSystems = this.forTopiaIdIn(growingSystemIds).findAll();
        List<GrowingSystem> activeGrowingSystems = growingSystems.stream().filter(GrowingSystem::isActive).toList();
        List<GrowingSystem> validateGrowingSystems = growingSystems.stream().filter(GrowingSystem::isValidated).toList();
     
        long nbActiveGrowingSystems = activeGrowingSystems.size();
        gsIndicator.setActive(nbActiveGrowingSystems);
    
        long nbValidateGrowingSystems = validateGrowingSystems.size();
        gsIndicator.setValidated(nbValidateGrowingSystems);
        
        result.add(gsIndicator);

        {
            Map<Object, Long> map = growingSystems.stream().collect(Collectors.groupingBy(GrowingSystem::getSector, Collectors.counting()));
            for (Map.Entry<Object, Long> entry : map.entrySet()) {
                Object key = entry.getKey();
                GrowingSystemsIndicator indicator = getIndicator(result, key);
                indicator.setCount(entry.getValue());
            }
        }
        {
            Map<Object, Long> map = activeGrowingSystems.stream().collect(Collectors.groupingBy(GrowingSystem::getSector, Collectors.counting()));
            for (Map.Entry<Object, Long> entry : map.entrySet()) {
                Object key = entry.getKey();
                GrowingSystemsIndicator indicator = getIndicator(result, key);
                indicator.setActive(entry.getValue());
            }
        }
        {
            Map<Object, Long> map = validateGrowingSystems.stream().collect(Collectors.groupingBy(GrowingSystem::getSector, Collectors.counting()));
            for (Map.Entry<Object, Long> entry : map.entrySet()) {
                Object key = entry.getKey();
                GrowingSystemsIndicator indicator = getIndicator(result, key);
                indicator.setValidated(entry.getValue());
            }
        }
        {
            Map<Object, Long> map = growingSystems.stream().map(GrowingSystem::getGrowingPlan).collect(Collectors.groupingBy(GrowingPlan::getType, Collectors.counting()));
            for (Map.Entry<Object, Long> entry : map.entrySet()) {
                Object key = entry.getKey();
                GrowingSystemsIndicator indicator = getIndicator(result, key);
                indicator.setCount(entry.getValue());
            }
        }
        {
            Map<Object, Long> map = activeGrowingSystems.stream().map(GrowingSystem::getGrowingPlan).collect(Collectors.groupingBy(GrowingPlan::getType, Collectors.counting()));
            for (Map.Entry<Object, Long> entry : map.entrySet()) {
                Object key = entry.getKey();
                GrowingSystemsIndicator indicator = getIndicator(result, key);
                indicator.setActive(entry.getValue());
            }
        }
        {
            Map<Object, Long> map = validateGrowingSystems.stream().map(GrowingSystem::getGrowingPlan).collect(Collectors.groupingBy(GrowingPlan::getType, Collectors.counting()));
            for (Map.Entry<Object, Long> entry : map.entrySet()) {
                Object key = entry.getKey();
                GrowingSystemsIndicator indicator = getIndicator(result, key);
                indicator.setValidated(entry.getValue());
            }
        }

        return result;
    }

    public List<Integer> findCampaigns(String growingSystemCode) {

        Map<String, Object> args = DaoUtils.asArgsMap();
        String hql = " SELECT DISTINCT gs." + PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN +
                " FROM " + getEntityClass().getName() + " gs "+
                " WHERE 1=1 ";
        hql += DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_CODE, args, growingSystemCode);

        hql += " ORDER BY gs." + PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN + " ASC ";
        return findAll(hql, args);
    }

    public List<Integer> findCampaignsFromGrowingSystemId(String growingSystemId) {

        Map<String, Object> args = DaoUtils.asArgsMap();
        String hql = " SELECT DISTINCT gs." + PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN +
                " FROM " + getEntityClass().getName() + " gs "+
                " WHERE gs." + GrowingSystem.PROPERTY_CODE + " = (" +
                "SELECT gs1." + GrowingSystem.PROPERTY_CODE + " FROM " + GrowingSystem.class.getName() +
                " gs1 WHERE gs1." + GrowingSystem.PROPERTY_TOPIA_ID + " = :growingSystemId)";
        args.put("growingSystemId", growingSystemId);

        hql += " ORDER BY gs." + PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN + " ASC ";
        return findAll(hql, args);
    }

    public Sector findGrowingSystemSector(String growingSystemId) {
        Map<String, Object> args = DaoUtils.asArgsMap();
        String hql = " SELECT gs." + GrowingSystem.PROPERTY_SECTOR +
                " FROM " + getEntityClass().getName() + " gs "+
                " WHERE 1=1 " +
                DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_TOPIA_ID, args, growingSystemId);
        return findUnique(hql, args);
    }

    public RefTypeAgriculture findGrowingSystemRefTypeAgriculture(String growingSystemId) {
        Map<String, Object> args = DaoUtils.asArgsMap();
        String hql = " SELECT gs." + GrowingSystem.PROPERTY_TYPE_AGRICULTURE +
                " FROM " + getEntityClass().getName() + " gs "+
                " WHERE 1=1 " +
                DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_TOPIA_ID, args, growingSystemId);
        return findUniqueOrNull(hql, args);
    }

    public Stream<GrowingSystem> getFilteredGrowingSystemsForReport(ReportFilter filter, SecurityContext securityContext) {
        Map<String, Object> args = new HashMap<>();

        StringBuilder query = new StringBuilder(newFromClause("gs"));

        query.append(" JOIN FETCH gs." + GrowingSystem.PROPERTY_GROWING_PLAN)
             .append(" WHERE 1 = 1");

        // active
        query.append(DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_ACTIVE, args, true));

        // campaign
        query.append(DaoUtils.andAttributeEquals("gs", PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN, args, filter.getCampaign()));

        // Domain
        if (StringUtils.isNoneEmpty(filter.getDomainId())) {
            query.append(DaoUtils.andAttributeLike("gs", PROPERTY_GROWING_PLAN_DOMAIN_ID, args, filter.getDomainId()));
        }

        // Navigation context
        NavigationContext navigationContext = filter.getNavigationContext();
        if (navigationContext != null) {
            addNavigationContextQuery(query, args, navigationContext);
        }

        SecurityHelper.addGrowingSystemFilter(query, args, securityContext, "gs");

        query.append(" ORDER BY " +
                "lower ( gs." + GrowingSystem.PROPERTY_NAME + " )" +
                ", gs." + PROPERTY_GROWING_PLAN_DOMAIN_CAMPAIGN);

        return stream(query.toString(), args);
    }

    public List<CroppingPlanEntry> findCropForGrowingSystemId(String growingSystemId) {
        GrowingSystem gs = forTopiaIdEquals(growingSystemId).findUnique();
        Domain d = gs.getGrowingPlan().getDomain();
        String query = " SELECT crop";
        query +=       " FROM " + CroppingPlanEntry.class.getName() + " crop ";
        query +=       " WHERE crop." + CroppingPlanEntry.PROPERTY_DOMAIN + " = :d";
        query +=       " ORDER BY crop." + CroppingPlanEntry.PROPERTY_NAME;

        return findAll(query, DaoUtils.asArgsMap("d", d));
    }
}
