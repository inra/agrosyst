package fr.inra.agrosyst.api.entities.performance;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.SortedColumn;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.SimpleSqlQuery;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.performance.PerformanceFilter;
import fr.inra.agrosyst.api.services.performance.PerformanceStatistics;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.performance.dbPersistence.Column;
import fr.inra.agrosyst.services.performance.dbPersistence.TableRowModel;
import fr.inra.agrosyst.services.security.SecurityContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.support.HibernateTopiaJpaSupport;
import org.nuiton.topia.persistence.support.SqlFunction;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.lang.reflect.Type;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PerformanceTopiaDao extends AbstractPerformanceTopiaDao<Performance> {
    
    private static final Log LOGGER = LogFactory.getLog(PerformanceTopiaDao.class);
    
    public static final String DATE_PATTERN = "dd MMMM yyyy à HH:mm";
    public static final String LOWEST_CAMPAIGN = "1979";
    public static final String HIGEST_CAMPAIGN = "2051";

    public static final String LOAD_PERFORMANCE_STATISTICS = """
        SELECT
            perf.topiaid                                      AS performance_id,
            perf.updatedate                                   AS start,
            (SELECT GREATEST(max(perfsynt.date_calcul), max(perfreal.date_calcul))
                FROM synthetise_echelle_synthetise perfsynt
                INNER JOIN realise_echelle_sdc perfreal
                ON perfreal.performance_id = perfsynt.performance_id
                WHERE perfsynt.performance_id = perf.topiaid) AS last,
            perf.nbTasks                                      AS total,
            (SELECT COUNT(perfsynt.id_systeme_synthetise)
                FROM synthetise_echelle_synthetise perfsynt
                WHERE perfsynt.performance_id = perf.topiaid) AS synth_id,
            (SELECT COUNT(perfreal.id_sdc)
                FROM realise_echelle_sdc perfreal
                WHERE perfreal.performance_id = perf.topiaid) AS real_sdc_id,
            perf.computestatus                                AS computestatus
        FROM performance perf
        WHERE perf.exporttype = 'DB'
        %s
        GROUP BY perf.topiaid
        """;
    
    public PaginationResult<Performance> getFilteredPerformances(PerformanceFilter filter, SecurityContext securityContext) throws TopiaException {
        Map<String, Object> args = new LinkedHashMap<>();
        String query = "FROM " + Performance.class.getName() + " P";
        query += " WHERE 1 = 1";
    
        // apply non null filter
        if (filter != null) {
            // practiced
            query += DaoUtils.andAttributeEquals("P", Performance.PROPERTY_PRACTICED, args, filter.isPracticed());
        
            // name
            query += DaoUtils.andAttributeLike("P", Performance.PROPERTY_NAME, args, filter.getPerformanceName());
        
            // domain name
            final String domainName = filter.getDomainName();
            if (StringUtils.isNotEmpty(domainName)) {
                if (NumberUtils.isCreatable(domainName) && NumberUtils.toInt(domainName, -1) != -1) {
                    query += " AND EXISTS (FROM " + Domain.class.getName() + " D" +
                            " WHERE D in elements(P." + Performance.PROPERTY_DOMAINS + ")" +
                            " AND D." + Domain.PROPERTY_CAMPAIGN + " = :domainCampaign" +
                            ")";
                    args.put("domainCampaign", Integer.valueOf(filter.getDomainName()));
                } else {
                    query += " AND EXISTS (FROM " + Domain.class.getName() + " D" +
                            " WHERE D in elements(P." + Performance.PROPERTY_DOMAINS + ")" +
                            " AND " + DaoUtils.getFieldLikeInsensitive("D." + Domain.PROPERTY_NAME, ":domainName") +
                            ")";
                    args.put("domainName", "%" + domainName + "%");
                }
            }

            // growing system name
            if (StringUtils.isNotEmpty(filter.getGrowingSystemName())) {
                query += " AND EXISTS (FROM " + GrowingSystem.class.getName() + " G" +
                        " WHERE G in elements(P." + Performance.PROPERTY_GROWING_SYSTEMS + ")" +
                        " AND " + DaoUtils.getFieldLikeInsensitive("G." + GrowingSystem.PROPERTY_NAME, ":growingSystemName") +
                        ")";
                args.put("growingSystemName", "%" + filter.getGrowingSystemName() + "%");
            }
            
            // plot name
            if (StringUtils.isNotEmpty(filter.getPlotName())) {
                query += " AND EXISTS (FROM " + Plot.class.getName() + " PL" +
                        " WHERE PL in elements(P." + Performance.PROPERTY_PLOTS + ")" +
                        " AND " + DaoUtils.getFieldLikeInsensitive("PL." + Plot.PROPERTY_NAME, ":plotName") +
                        ")";
                args.put("plotName", "%" + filter.getPlotName() + "%");
            }
            
            // zone name
            if (StringUtils.isNotEmpty(filter.getZoneName())) {
                query += " AND EXISTS (FROM " + Zone.class.getName() + " Z" +
                        " WHERE Z in elements(P." + Performance.PROPERTY_ZONES + ")" +
                        " AND " + DaoUtils.getFieldLikeInsensitive("Z." + Zone.PROPERTY_NAME, ":zoneName") +
                        ")";
                args.put("zoneName", "%" + filter.getZoneName() + "%");
            }
        }

        // security (author)
        if (!securityContext.isAdmin()) {
            String userId = securityContext.getUserId();
            query += DaoUtils.andAttributeEquals("P",
                    Performance.PROPERTY_AUTHOR + "." + AgrosystUser.PROPERTY_TOPIA_ID,
                    args,
                    userId);
        }
    
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
    
        String queryString = query;
        final String orderByQuery = getFilterOrderBy(filter);
        String queryAndOrder = queryString + orderByQuery;
    
        List<Performance> performances = find(queryAndOrder, args, startIndex, endIndex);
        long totalCount = findUnique("SELECT count(*) " + queryString, args);
    
        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        return PaginationResult.of(performances, totalCount, pager);
    }
    
    private String getFilterOrderBy(PerformanceFilter filter) {
        
        String result = null;
        
        if (filter != null && filter.getSortedColumn() != null) {
            SortedColumn sortedColumn = filter.getSortedColumn();
            String filterOrderBy;
            if (sortedColumn == SortedColumn.PERFORMANCE) {
                filterOrderBy = " lower (P." + Performance.PROPERTY_NAME + ")";
            } else if (sortedColumn == SortedColumn.DATE) {
                filterOrderBy = " P." + Performance.PROPERTY_UPDATE_DATE;
            } else {
                filterOrderBy = null;
            }
            if (StringUtils.isNotBlank(filterOrderBy)) {
                filterOrderBy += " " + filter.getSortOrder().name();
                result = " ORDER BY " +
                        filterOrderBy +
                        ", P." + TopiaEntity.PROPERTY_TOPIA_ID;
            }
            
        }
        
        if (result == null) {
            // default
            result = " ORDER BY " +
                    " P." + Performance.PROPERTY_UPDATE_DATE + " DESC " +
                    ", P." + TopiaEntity.PROPERTY_TOPIA_ID;
        }
        
        return result;
    }

    public void insertIntoTable(TableRowModel tableModel) {
        
        String query = "INSERT INTO %s (%s) VALUES (%s);";
        List<String> columnNames = new ArrayList<>();
        List<String> valuesParameters = new ArrayList<>();
        
        Collection<Column> columns = tableModel.getColumnsToPersists();
        for (Column column : columns) {
            if (column.getValue().isEmpty()) {
                continue;
            }
    
            Type t = column.geValueType();
            final Object value = column.getValue().get();
            String valueParameter;
            if (String.class.isAssignableFrom((Class<?>) t)) {
                valueParameter = DaoUtils.getStringQueryCompatible(value);
            } else if (((Class<?>) t).isEnum()) {
                valueParameter = DaoUtils.getEnumStringQueryCompatible(value, t);
            } else if (Date.class.isAssignableFrom((Class<?>) t)) {
                valueParameter = "(SELECT CURRENT_TIMESTAMP(3)::timestamp with time zone)";
            } else {
                valueParameter = value.toString();
            }
    
            if (valueParameter == null) {
                continue;
            }
    
            valueParameter = valueParameter.replaceAll("\\r\\n", "<br>");
            valueParameter = valueParameter.replaceAll("\\r", "<br>");
            valueParameter = valueParameter.replaceAll("\\n", "<br>");
            valueParameter = valueParameter.replaceAll("\\+", "<br>");
            
            String dbColumnName = column.getDbColumnName();
            columnNames.add(dbColumnName);
            valuesParameters.add(valueParameter);
        }
        query = String.format(query,
                tableModel.getTableName(),
                String.join(", ", columnNames),
                String.join(", ", valuesParameters));
    
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("process INSERT: " + query);
        }
        try {
            topiaSqlSupport.executeSql(query);
        } catch (Exception e) {
            LOGGER.error(String.format("L'insertion dans la table %s à échouée", tableModel.getTableName()), e);
            LOGGER.error(String.format("La requête en échec est:\n%s", query));
            throw new AgrosystTechnicalException("Échec de persistance en base d'une ligne d'un export de performance en masse.", e);
        }
    }
    
    public Map<String, List<String>> findAllPracticedSystemGrowingSystemIdsByDomainIds() {
        String query = "SELECT d." + TopiaEntity.PROPERTY_TOPIA_ID + ", gs." + TopiaEntity.PROPERTY_TOPIA_ID + " FROM practicedsystem ps " +
                " INNER JOIN growingsystem gs ON gs." + TopiaEntity.PROPERTY_TOPIA_ID + " = ps." + PracticedSystem.PROPERTY_GROWING_SYSTEM +
                " INNER JOIN growingplan gp ON gp." + TopiaEntity.PROPERTY_TOPIA_ID + " = gs." + GrowingSystem.PROPERTY_GROWING_PLAN +
                " INNER JOIN domain d ON d." + TopiaEntity.PROPERTY_TOPIA_ID + " = gp." + GrowingPlan.PROPERTY_DOMAIN +
                " WHERE ps." + PracticedSystem.PROPERTY_ACTIVE + " IS TRUE" +
                " AND gp." + GrowingPlan.PROPERTY_ACTIVE + " IS TRUE" +
                " AND gs." + GrowingSystem.PROPERTY_ACTIVE + " IS TRUE" +
                " AND d." + Domain.PROPERTY_ACTIVE + " IS TRUE" +
                " AND d." + Domain.PROPERTY_CAMPAIGN + " > " + LOWEST_CAMPAIGN +
                " AND d." + Domain.PROPERTY_CAMPAIGN + " < " + HIGEST_CAMPAIGN +
                " GROUP BY d." + TopiaEntity.PROPERTY_TOPIA_ID + ", gs.topiaid ORDER BY COUNT(gs." + TopiaEntity.PROPERTY_TOPIA_ID + ") DESC";

        SqlFunction<ResultSet, Pair<String, String>> transformer = resultSet -> {
            final String domainId = resultSet.getString(1);
            final String growingSystemId = resultSet.getString(2);
            return Pair.of(domainId, growingSystemId);
        };

        List<Pair<String, String>> result0 = topiaSqlSupport.findMultipleResult(query, transformer);
        Map<String, List<String>> result = new HashMap<>();
        for (Pair<String, String> dgs : result0) {
            String domainId = dgs.getLeft();
            List<String> domainGss = result.computeIfAbsent(domainId, k -> new ArrayList<>());
            domainGss.add(dgs.getRight());
        }
        
        return result;
    }
    
    public Map<String, List<String>> findAllEffectiveZoneIdsByDomainIds() {
        String query = "SELECT d." + TopiaEntity.PROPERTY_TOPIA_ID + ", zone." + TopiaEntity.PROPERTY_TOPIA_ID + " FROM Zone zone " +
                " INNER JOIN plot plot ON plot." + TopiaEntity.PROPERTY_TOPIA_ID + " = zone." + Zone.PROPERTY_PLOT +
                " INNER JOIN domain d ON d." + TopiaEntity.PROPERTY_TOPIA_ID + " = plot." + Plot.PROPERTY_DOMAIN +
                " WHERE zone." + Zone.PROPERTY_ACTIVE + " IS TRUE" +
                " AND plot." + Plot.PROPERTY_ACTIVE + " IS TRUE" +
                " AND d." + Domain.PROPERTY_ACTIVE + " IS TRUE" +
                " AND d." + Domain.PROPERTY_CAMPAIGN + " > " + LOWEST_CAMPAIGN +
                " AND d." + Domain.PROPERTY_CAMPAIGN + " < " + HIGEST_CAMPAIGN +
                " GROUP BY d." + TopiaEntity.PROPERTY_TOPIA_ID + ", zone.topiaid ORDER BY COUNT(zone." + TopiaEntity.PROPERTY_TOPIA_ID + ") DESC";
    
        SqlFunction<ResultSet, Pair<String, String>> transformer = resultSet -> {
            final String domainId = resultSet.getString(1);
            final String zoneId = resultSet.getString(2);
            return Pair.of(domainId, zoneId);
        };
    
        List<Pair<String, String>> result0 = topiaSqlSupport.findMultipleResult(query, transformer);
        Map<String, List<String>> result = new HashMap<>();
        for (Pair<String, String> domainZone : result0) {
            String domainId = domainZone.getLeft();
            List<String> domainZoneIds = result.computeIfAbsent(domainId, k -> new ArrayList<>());
            domainZoneIds.add(domainZone.getRight());
        }
    
        return result;
    }
    
    public List<PerformanceStatistics> loadDbPerformancesStatistics(String performanceId) {

        String sql = String.format(LOAD_PERFORMANCE_STATISTICS, StringUtils.isBlank(performanceId) ? "" : "AND perf.topiaid = ?");
        List<String> params = new LinkedList<>();
        if (StringUtils.isNotBlank(performanceId)) {
            params.add(performanceId);
        }

        SimpleSqlQuery<PerformanceStatistics> query0 = new SimpleSqlQuery<>(sql, params) {
            @Override
            public PerformanceStatistics prepareResult(ResultSet resultSet) throws SQLException {
                PerformanceStatistics result = new PerformanceStatistics();
                result.setPerformanceId(resultSet.getString("performance_id"));
                final Timestamp startTimestamp = resultSet.getTimestamp("start");
                LocalDateTime startAt = new java.sql.Timestamp(startTimestamp.getTime()).toLocalDateTime();
                result.setStartAt(DateTimeFormatter.ofPattern(DATE_PATTERN).format(startAt));
                Timestamp endTimestamp = resultSet.getTimestamp("last");
                endTimestamp = endTimestamp == null ? startTimestamp : endTimestamp;//no task currently ended
                LocalDateTime endAt = new java.sql.Timestamp(endTimestamp.getTime()).toLocalDateTime();
                result.setEndAtTime(endAt);
                result.setEndAt(DateTimeFormatter.ofPattern("dd MMMM yyyy à HH:mm").format(endAt));
                result.setNbTotalTask(resultSet.getLong("total"));
                result.setNbPracticedPracticed(resultSet.getLong("synth_id"));
                result.setNbEffectiveSdc(resultSet.getLong("real_sdc_id"));
                result.setPerformanceState(resultSet.getString("computestatus"));
                return result;
            }
        };
    
        return topiaSqlSupport.findMultipleResult(query0);
    }
    
    public void changePerformanceStatusGeneratingToFailedAtApplicationStart() {
        TopiaHibernateSupport hibernateSupport = ((HibernateTopiaJpaSupport) topiaJpaSupport).getHibernateSupport();
        hibernateSupport.getHibernateSession()
                .createNativeQuery("UPDATE " + getEntityClass().getSimpleName() + " perf" +
                " SET " + Performance.PROPERTY_COMPUTE_STATUS + " = '" + PerformanceState.FAILED.name() + "'" +
                " WHERE perf." + Performance.PROPERTY_COMPUTE_STATUS + " = '" + PerformanceState.GENERATING.name() + "'", Performance.class)
                .executeUpdate();
    }

    public void changePerformanceStatus(String performanceId, PerformanceState status) {
        TopiaHibernateSupport hibernateSupport = ((HibernateTopiaJpaSupport) topiaJpaSupport).getHibernateSupport();
        hibernateSupport.getHibernateSession()
                .createNativeQuery("UPDATE " + getEntityClass().getSimpleName() + " perf" +
                " SET " + Performance.PROPERTY_COMPUTE_STATUS + " = '" + status.name() + "'" +
                " WHERE perf." + Performance.PROPERTY_TOPIA_ID + " = '" + performanceId + "'", Performance.class)
                .executeUpdate();
    }
    
    public void deleteDbPerformanceData(String performanceId) {
    
        topiaSqlSupport.executeSql("DELETE FROM echelle_intrant perf" +
                " WHERE perf.performance_id = '" + performanceId + "'");
    
        topiaSqlSupport.executeSql("DELETE FROM realise_echelle_culture perf" +
                " WHERE perf.performance_id = '" + performanceId + "'");
        
        topiaSqlSupport.executeSql("DELETE FROM realise_echelle_intervention perf" +
                " WHERE perf.performance_id = '" + performanceId + "'");
        
        topiaSqlSupport.executeSql("DELETE FROM realise_echelle_itk perf" +
                " WHERE perf.performance_id = '" + performanceId + "'");
        
        topiaSqlSupport.executeSql("DELETE FROM realise_echelle_parcelle perf" +
                " WHERE perf.performance_id = '" + performanceId + "'");
        
        topiaSqlSupport.executeSql("DELETE FROM realise_echelle_sdc perf" +
                " WHERE perf.performance_id = '" + performanceId + "'");
        
        topiaSqlSupport.executeSql("DELETE FROM realise_echelle_zone perf" +
                " WHERE perf.performance_id = '" + performanceId + "'");
    
        topiaSqlSupport.executeSql("DELETE FROM synthetise_echelle_culture perf" +
                " WHERE perf.performance_id = '" + performanceId + "'");
        
        topiaSqlSupport.executeSql("DELETE FROM synthetise_echelle_intervention perf" +
                " WHERE perf.performance_id = '" + performanceId + "'");
        
        topiaSqlSupport.executeSql("DELETE FROM synthetise_echelle_synthetise perf" +
                " WHERE perf.performance_id = '" + performanceId + "'");
        
    }
    
} //PerformanceTopiaDao
