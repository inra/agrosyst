package fr.inra.agrosyst.api.entities.practiced;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.SortedColumn;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.security.SecurityContext;
import fr.inra.agrosyst.services.security.SecurityHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PracticedPlotTopiaDao extends AbstractPracticedPlotTopiaDao<PracticedPlot> {

    protected static final String PROPERTY_GROWING_SYSTEM_ID = PracticedSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID;
    protected static final String PROPERTY_GROWING_SYSTEM_NAME = PracticedSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_NAME;

    protected static final String PROPERTY_GROWING_SYSTEM_GROWING_PLAN = PracticedSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN;
    protected static final String PROPERTY_GROWING_SYSTEM_GROWING_PLAN_ID = PROPERTY_GROWING_SYSTEM_GROWING_PLAN + "." + GrowingPlan.PROPERTY_TOPIA_ID;
    protected static final String PROPERTY_GROWING_SYSTEM_GROWING_PLAN_NAME = PROPERTY_GROWING_SYSTEM_GROWING_PLAN + "." + GrowingPlan.PROPERTY_NAME;

    protected static final String PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN = PROPERTY_GROWING_SYSTEM_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN;
    protected static final String PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_ID = PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID;
    protected static final String PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_NAME = PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_NAME;
    protected static final String PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_CAMPAIGN = PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN;

    public PaginationResult<PracticedPlot> getFilteredPracticedPlots(PracticedPlotFilter filter, SecurityContext securityContext) {
        StringBuilder query = new StringBuilder("FROM " + getEntityClass().getName() + " pp, " + PracticedSystem.class.getName() + " ps ");
        query.append(" LEFT JOIN pp." + PracticedPlot.PROPERTY_PRACTICED_SYSTEM + " as pp_ps ");
        query.append(" WHERE ps." + PracticedSystem.PROPERTY_TOPIA_ID + " = pp_ps" + "." + TopiaEntity.PROPERTY_TOPIA_ID);

        Map<String, Object> args = new LinkedHashMap<>();

        // apply non null filter
        if (filter != null) {
            // PracticedPlot name
            query.append(DaoUtils.andAttributeLike("pp", PracticedPlot.PROPERTY_NAME, args, filter.getPracticedPlotName()));
            // PracticedSystem name
            query.append(DaoUtils.andAttributeLike("ps", PracticedSystem.PROPERTY_NAME, args, filter.getPracticedSystemName()));
            // Growing System
            query.append(DaoUtils.andAttributeLike("ps", PROPERTY_GROWING_SYSTEM_NAME, args, filter.getGrowingSystemName()));
            // PracticedSystem campaigns
            if (StringUtils.isNotBlank(filter.getPracticedSystemCampaign())) {
                Set<Integer> intcampaigns = CommonService.GET_CAMPAIGNS_SET.apply(filter.getPracticedSystemCampaign());
                for (Integer intcampaign : intcampaigns) {
                    query.append(DaoUtils.andAttributeLike("ps", PracticedSystem.PROPERTY_CAMPAIGNS, args, intcampaign.toString()));
                }
            }
            // Domain name
            query.append(DaoUtils.andAttributeLike("ps", PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_NAME, args, filter.getDomainName()));
            // GrowingPlan name
            query.append(DaoUtils.andAttributeLike("ps", PROPERTY_GROWING_SYSTEM_GROWING_PLAN_NAME, args, filter.getGrowingPlanName()));
            // Navigation context
            NavigationContext navigationContext = filter.getNavigationContext();
            if (navigationContext != null) {
                // campaigns
                if (CollectionUtils.isNotEmpty(navigationContext.getCampaigns())) {
                    for (Integer campaign : navigationContext.getCampaigns()) {
                        query.append(DaoUtils.andAttributeLike("ps", PracticedSystem.PROPERTY_CAMPAIGNS, args, campaign.toString()));
                    }
                }
                query.append(DaoUtils.andAttributeInIfNotEmpty("ps", PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_CAMPAIGN, args, navigationContext.getCampaigns()));
                // networks
                if (navigationContext.getNetworksCount() > 0) {
                    Set<String> growingSystemIds = getProjectionHelper().networksToGrowingSystems(navigationContext.getNetworks());
                    query.append(DaoUtils.andAttributeIn("ps", PROPERTY_GROWING_SYSTEM_ID, args, growingSystemIds));
                }
                // domains
                query.append(DaoUtils.andAttributeInIfNotEmpty("ps", PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_ID, args, DaoUtils.getRealIds(navigationContext.getDomains(), Domain.class)));
                // growingPlans
                query.append(DaoUtils.andAttributeInIfNotEmpty("ps", PROPERTY_GROWING_SYSTEM_GROWING_PLAN_ID, args, DaoUtils.getRealIds(navigationContext.getGrowingPlans(), GrowingPlan.class)));
                // growingSystems
                query.append(DaoUtils.andAttributeInIfNotEmpty("ps", PROPERTY_GROWING_SYSTEM_ID, args, DaoUtils.getRealIds(navigationContext.getGrowingSystems(), GrowingSystem.class)));
            }
        }
    
        SecurityHelper.addPracticedPlotFilter(query, args, securityContext, "ps");
    
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
    
        String orderByClause = getFilterOrderBy(filter);
    
        List<PracticedPlot> practicedPlots = find("SELECT pp " + query + orderByClause, args, startIndex, endIndex);
        long totalCount = findUnique("SELECT count(pp.topiaId) " + query, args);
    
        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        return PaginationResult.of(practicedPlots, totalCount, pager);
    }
    
    private String getFilterOrderBy(PracticedPlotFilter filter) {
        String result = null;
        
        if (filter != null && filter.getSortedColumn() != null) {
            SortedColumn sortedColumn = filter.getSortedColumn();
            String filterOrderBy = switch (sortedColumn) {
                case PLOT -> " lower (pp." + PracticedPlot.PROPERTY_NAME + ")";
                case PRACTICED_SYSTEM -> " lower (ps." + PracticedSystem.PROPERTY_NAME + ")";
                case GROWING_SYSTEM -> " lower (ps." + PROPERTY_GROWING_SYSTEM_NAME + ")";
                case GROWING_PLAN -> " lower (ps." + PROPERTY_GROWING_SYSTEM_GROWING_PLAN_NAME + ")";
                case DOMAIN -> " lower (ps." + PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_NAME + ")";
                case CAMPAIGN -> " ps." + PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_CAMPAIGN;
                default -> null;
            };
            if (StringUtils.isNotBlank(filterOrderBy)) {
                filterOrderBy += " " + filter.getSortOrder().name();
                result = " ORDER BY " +
                        filterOrderBy +
                        ", pp." + TopiaEntity.PROPERTY_TOPIA_ID;
            }
        }
        if (result == null) {
            // default
            result = " ORDER BY " +
                    "ps." + PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_CAMPAIGN + " DESC" +
                    ", ps." + PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_NAME +
                    ", ps." + PROPERTY_GROWING_SYSTEM_GROWING_PLAN_NAME +
                    ", ps." + PROPERTY_GROWING_SYSTEM_NAME +
                    ", ps." + PracticedSystem.PROPERTY_NAME +
                    ", lower (pp." + PracticedPlot.PROPERTY_NAME + ")" +
                    ", pp." + TopiaEntity.PROPERTY_TOPIA_ID;
        }
        
        return result;
    }

} //PracticedPlotTopiaDao
