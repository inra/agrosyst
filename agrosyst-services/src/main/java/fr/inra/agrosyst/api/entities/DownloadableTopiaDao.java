package fr.inra.agrosyst.api.entities;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class DownloadableTopiaDao extends AbstractDownloadableTopiaDao<Downloadable> {

    private static final Log log = LogFactory.getLog(DownloadableTopiaDao.class);

    public void purgeDownloadable() {
        String query =
                String.format(" FROM %s d", Downloadable.class.getName()) +
                " WHERE " +
                " d." + Downloadable.PROPERTY_EXPIRES_ON + " < :now ";
        List<Downloadable> list = findAll(query, DaoUtils.asArgsMap("now", LocalDateTime.now()));
        if (log.isTraceEnabled() && !list.isEmpty()) {
            String displayable = list.stream()
                    .map(d -> String.format("[id=%s,taskId=%s]", d.getTopiaId(), d.getTaskId()))
                    .collect(Collectors.joining(";"));
            log.trace(list.size() + " Downloadable à supprimer : " + displayable);
        }
        deleteAll(list);
    }

}
