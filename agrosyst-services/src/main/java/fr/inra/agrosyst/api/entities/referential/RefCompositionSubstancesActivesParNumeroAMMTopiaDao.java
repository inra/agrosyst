package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import org.nuiton.topia.persistence.support.SqlFunction;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RefCompositionSubstancesActivesParNumeroAMMTopiaDao extends AbstractRefCompositionSubstancesActivesParNumeroAMMTopiaDao {

    /**
     * Récupère la liste des codes AMM dans RefCompositionSubstancesActivesParNumeroAMM qui ne se trouvent pas
     * dans la table RefActaTraitementsProduit
     */
    public Set<String> findNonExistingCodeAmm() {
        final String query = """
                    SELECT DISTINCT(r.%s) FROM %s r
                    WHERE NOT EXISTS (
                        SELECT 1 FROM %s r2 WHERE r2.%s = r.%s
                    )
                """.formatted(
                RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NUMERO__AMM,
                getEntityClass().getName(),
                RefActaTraitementsProduit.class.getName(),
                RefActaTraitementsProduit.PROPERTY_CODE__AMM,
                RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NUMERO__AMM
        );

        return new HashSet<>(this.findAll(query));
    }

    public List<RefCompositionSubstancesActivesParNumeroAMM> findAllRefCompositionSubstancesActivesParNumeroAMMForDomain(Domain domain) {
        final String query =
                "FROM " + getEntityClass().getName() + " r " +
                        "WHERE r." + RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_ACTIVE + " IS TRUE " +
                        "  AND EXISTS (" +
                        "    SELECT 1 FROM " + DomainPhytoProductInput.class.getName() + " d " +
                        "    WHERE d." + DomainPhytoProductInput.PROPERTY_DOMAIN + " = :domain " +
                        "    AND d." + DomainPhytoProductInput.PROPERTY_REF_INPUT + "." + RefActaTraitementsProduit.PROPERTY_CODE__AMM + " = r." + RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NUMERO__AMM +
                        "  )";

        final List<RefCompositionSubstancesActivesParNumeroAMM> results = this.findAll(query, Map.of(
                "domain", domain
        ));

        return results;
    }

    public List<String> findActiveSubstanceNamesForInterventionType(AgrosystInterventionType interventionType, boolean ipmworks) {
        String selectVariantName = ipmworks ? RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_VARIANT_SA : RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NOM_SA;
        String sql = "select distinct(rasa." + selectVariantName + ")" +
                " from " + RefActaTraitementsProduit.class.getSimpleName() + " ratp" +
                " inner join " + RefActaTraitementsProduitsCateg.class.getSimpleName() + " ratpc on ratp." + RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT + "  = ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ID_TRAITEMENT +
                " inner join " + RefCompositionSubstancesActivesParNumeroAMM.class.getSimpleName() + " rasa on rasa." + RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NUMERO__AMM + " = ratp." + RefActaTraitementsProduit.PROPERTY_CODE__AMM +
                " where ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTION + " = '%s'" +
                " and rasa." + RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NOM_SA + " != ''" +
                " and rasa." + RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_ACTIVE + " is true" +
                " order by rasa." + selectVariantName;

        String result = String.format(sql, interventionType.name());

        SqlFunction<ResultSet, String> transformer = resultSet -> resultSet.getString(1);

        List<String> substanceActiveNames = topiaSqlSupport.findMultipleResult(result, transformer);
        return substanceActiveNames;
    }

    public Collection<String> findDistinctSubtanceActiveNamesForProducts(Language language, Set<String> code_AMMs) {

        I18nDaoHelper i18nDaoHelper = I18nDaoHelper.withComplexI18nKey(RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NOM_SA,
                language, TradRefIntrant.class, "SA");

        String selectedField = i18nDaoHelper.coalesceTranslation();
        String query = "SELECT distinct( " + selectedField + " ) " +
                newFromClause("SA") +
                i18nDaoHelper.leftJoinTranslation() +
                " WHERE SA." + RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_ACTIVE + " = true" +
                " AND SA." + RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NUMERO__AMM + " IN (:code_AMMs)" +
                " ORDER BY " + selectedField;

        return findAll(query, Map.of("code_AMMs", code_AMMs));
    }
}
