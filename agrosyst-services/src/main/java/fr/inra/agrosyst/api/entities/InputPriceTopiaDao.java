package fr.inra.agrosyst.api.entities;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefInputPriceImpl;
import fr.inra.agrosyst.api.services.common.InputPriceFilter;
import fr.inra.agrosyst.api.services.common.ProductPriceSummary;
import fr.inra.agrosyst.api.services.domain.inputStock.InputPriceDto;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.DoubleAdder;
import java.util.stream.Collectors;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class InputPriceTopiaDao extends AbstractInputPriceTopiaDao<InputPrice> {
    
    public ProductPriceSummary loadUserPriceIndicationFromDomain(InputPriceFilter filter,
                                                                 String excludeDomainId) {
        Integer campaign = filter.getCampaign();
        
        Map<String, Object> args = new HashMap<>();
        
        // projection for price median
        String queryProjectionAll = "SELECT p." + InputPrice.PROPERTY_PRICE_UNIT + ", p." + InputPrice.PROPERTY_PRICE;
        
        // Query filter shared between all possible queries
        String queryFilter = getUserPriceIndicationCommonQueryPart(filter, args);
        
        if (StringUtils.isNotBlank(excludeDomainId)) {
            queryFilter += " AND p." + InputPrice.PROPERTY_DOMAIN + "." + TopiaEntity.PROPERTY_TOPIA_ID + " != :excludeId";
            args.put("excludeId", excludeDomainId);
        }
        
        String orderedResult = " ORDER BY p." + InputPrice.PROPERTY_PRICE_UNIT + ", p." + InputPrice.PROPERTY_PRICE + " ASC ";
        
        String currentCampaignsPriceQuery = queryFilter + DaoUtils.andAttributeEquals(
                "p",
                InputPrice.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN,
                args,
                campaign);

        // [0]: price [1]: priceUnit
        List<Object[]> allCampaignsPrices = findAll(queryProjectionAll + currentCampaignsPriceQuery + orderedResult, args);

        // refs #9696 try convert user prices to the targeted price unit
        List<Double> allPricesAtTargetedUnit = getPricesToTargetedUnit(allCampaignsPrices, filter.getPriceUnit());

        ProductPriceSummary currentCampaignsPricesSummary = computeProductPriceSummary(allPricesAtTargetedUnit);
        
        return currentCampaignsPricesSummary;
    }

    protected String getUserPriceIndicationCommonQueryPart(InputPriceDto filter, Map<String, Object> args) {
        
        // Query filter shared between all possible queries
        String className;
        className = getClassNameFromPriceCategory(filter);
        
        StringBuilder queryFilter = new StringBuilder(" FROM " + className + " p " +
                " WHERE p." + InputPrice.PROPERTY_PRICE + " IS NOT NULL " +
                " AND p." + InputPrice.PROPERTY_PRICE + " > 0 " +
                " AND p." + InputPrice.PROPERTY_PRICE_UNIT + " IS NOT NULL ");
        if (InputPriceCategory.SEEDING_INPUT.equals(filter.getCategory())) {
                queryFilter.append(DaoUtils.andAttributeEquals(
                        "p",
                        SeedPrice.PROPERTY_SEED_TYPE,
                        args,
                        filter.getSeedType()));
                
                queryFilter.append(DaoUtils.andAttributeEquals(
                        "p",
                        SeedPrice.PROPERTY_BIOLOGICAL_SEED_INOCULATION,
                        args,
                        filter.isBiologicalSeedInoculation()));
                
                queryFilter.append(DaoUtils.andAttributeEquals(
                        "p",
                        SeedPrice.PROPERTY_CHEMICAL_TREATMENT,
                        args,
                        filter.isChemicalTreatment()));
                queryFilter.append(DaoUtils.andAttributeEquals(
                        "p",
                        SeedPrice.PROPERTY_INCLUDED_TREATMENT,
                        args,
                        filter.isIncludedTreatment()));
            
        }
        
        queryFilter.append(DaoUtils.andAttributeEquals(
                "p",
                DomainPhytoProductInput.PROPERTY_PRODUCT_TYPE,
                args,
                filter.getPhytoProductType()));
        
        // price category
        queryFilter.append(DaoUtils.andAttributeEquals("p", InputPrice.PROPERTY_CATEGORY, args, filter.getCategory()));
        
        if (filter.getCategory().equals(InputPriceCategory.MINERAL_INPUT)) {
            String objectId = filter.getObjectId();
            String[] researchPart = objectId.split(" ");
            String categ = researchPart[0];
            String forme = researchPart[1];
            
            List<String> notEquals = new ArrayList<>();
            if (!researchPart[2].equals("n=0.000000")) {
                notEquals.add("%n=0.000000%");
            }
            if (!researchPart[3].equals("p2O5=0.000000")) {
                notEquals.add("%p2O5=0.000000%");
            }
            if (!researchPart[4].equals("k2O=0.000000")) {
                notEquals.add("%k2O=0.000000%");
            }
            if (!researchPart[5].equals("bore=0.000000")) {
                notEquals.add("%bore=0.000000%");
            }
            if (!researchPart[6].equals("calcium=0.000000")) {
                notEquals.add("%calcium=0.000000%");
            }
            if (!researchPart[7].equals("fer=0.000000")) {
                notEquals.add("%fer=0.000000%");
            }
            if (!researchPart[8].equals("manganese=0.000000")) {
                notEquals.add("%manganese=0.000000%");
            }
            if (!researchPart[9].equals("molybdene=0.000000")) {
                notEquals.add("%molybdene=0.000000%");
            }
            if (!researchPart[10].equals("mgO=0.000000")) {
                notEquals.add("%mgO=0.000000%");
            }
            if (!researchPart[11].equals("oxyde_de_sodium=0.000000")) {
                notEquals.add("%oxyde_de_sodium=0.000000%");
            }
            if (!researchPart[12].equals("sO3=0.000000")) {
                notEquals.add("%sO3=0.000000%");
            }
            if (!researchPart[13].equals("cuivre=0.000000")) {
                notEquals.add("%cuivre=0.000000%");
            }
            if (!researchPart[14].equals("zinc=0.000000")) {
                notEquals.add("%zinc=0.000000%");
            }
            
            String n = researchPart[2].equals("n=0.000000") ? "n=0.000000" : "n=%.%";
            String p2o5 = researchPart[3].equals("p2O5=0.000000") ? "p2O5=0.000000" : "p2O5=%.%";
            String k2O = researchPart[4].equals("k2O=0.000000") ? "k2O=0.000000" : "k2O=%.%";
            String bore = researchPart[5].equals("bore=0.000000") ? "bore=0.000000" : "bore=%.%";
            String calcium = researchPart[6].equals("calcium=0.000000") ? "calcium=0.000000" : "calcium=%.%";
            String fer = researchPart[7].equals("fer=0.000000") ? "fer=0.000000" : "fer=%.%";
            String manganese = researchPart[8].equals("manganese=0.000000") ? "manganese=0.000000" : "manganese=%.%";
            String molybdene = researchPart[9].equals("molybdene=0.000000") ? "molybdene=0.000000" : "molybdene=%.%";
            String mgO = researchPart[10].equals("mgO=0.000000") ? "mgO=0.000000" : "mgO=%.%";
            String oxide_de_sodium = researchPart[11].equals("oxyde_de_sodium=0.000000") ? "oxyde_de_sodium=0.000000" : "oxyde_de_sodium=%.%";
            String sO3 = researchPart[12].equals("sO3=0.000000") ? "sO3=0.000000" : "sO3=%.%";
            String cuivre = researchPart[13].equals("cuivre=0.000000") ? "cuivre=0.000000" : "cuivre=%.%";
            String zinc = researchPart[14].equals("zinc=0.000000") ? "zinc=0.000000" : "zinc=%.%";
            
            // categ=%d forme%s
            List<String> resarchObjectIdParts = Arrays.asList(
                    categ,
                    forme,
                    n,
                    p2o5,
                    k2O,
                    bore,
                    calcium,
                    fer,
                    manganese,
                    molybdene,
                    mgO,
                    oxide_de_sodium,
                    sO3,
                    cuivre,
                    zinc
            );
            
            String researchedObjectId = StringUtils.join(resarchObjectIdParts, " ");
            
            queryFilter.append(DaoUtils.andAttributeLike("p", InputPrice.PROPERTY_OBJECT_ID, args, researchedObjectId));
            
            for (String notEqual : notEquals) {
                queryFilter.append(" AND p." + InputPrice.PROPERTY_OBJECT_ID + " NOT LIKE '").append(notEqual).append("' ");
            }
            
        } else {
            // price objectId
            queryFilter.append(DaoUtils.andAttributeEquals("p", InputPrice.PROPERTY_OBJECT_ID, args, filter.getObjectId()));
        }
        
        return queryFilter.toString();
    }
    
    private String getClassNameFromPriceCategory(InputPriceDto filter) {
        return switch (filter.getCategory()) {
            case HARVESTING_ACTION -> HarvestingPrice.class.getName();
            case SEEDING_INPUT, SEEDING_PLAN_COMPAGNE_INPUT -> SeedPrice.class.getName();
            default -> InputPrice.class.getName();
        };
    }
    
    protected ProductPriceSummary computeProductPriceSummary(List<Double> allPricesAtTargetedUnit) {
        
        ProductPriceSummary pricesSummary;
        
        if (CollectionUtils.isNotEmpty(allPricesAtTargetedUnit)) {
            
            double min = allPricesAtTargetedUnit.getFirst();
            
            double max = allPricesAtTargetedUnit.get(allPricesAtTargetedUnit.size() - 1);
            
            DoubleAdder a = new DoubleAdder();
            allPricesAtTargetedUnit.forEach(a::add);
            double sum = a.doubleValue();
            
            double average = sum / allPricesAtTargetedUnit.size();
            
            Double median = DaoUtils.median(allPricesAtTargetedUnit);
            
            final long countedPrice = allPricesAtTargetedUnit.size();
            
            pricesSummary = new ProductPriceSummary(
                    min, max, median, average, countedPrice, null, Collections.emptyList());
            
        } else {
            pricesSummary = new ProductPriceSummary(
                    null, null, null, null, 0L, null, Collections.emptyList());
        }
        
        return pricesSummary;
    }
    
    protected List<Double> getPricesToTargetedUnit(List<Object[]> pricesByPriceUnitsQueryResult, PriceUnit targetedUnit) {
        
        List<Double> allPrices = new ArrayList<>();

        for (Object[] priceUnitAndPrice : pricesByPriceUnitsQueryResult) {
            PriceUnit pu = (PriceUnit) priceUnitAndPrice[0];
            Double price = (Double) priceUnitAndPrice[1];
            if (targetedUnit == null) {
                targetedUnit = pu;
            }
            if (pu.equals(targetedUnit) && price != null) {
                allPrices.add(price);
            } else {
                Double rate = RefInputPriceImpl.GET_PRICE_UNIT_TO_OTHER_PRICE_UNIT_CONVERSION_RATE.apply(Pair.of(pu, targetedUnit));
                if (rate != null && rate > 0 && price != null && price > 0) {
                    allPrices.add(price * rate);
                }
            }
        }
        
        allPrices = allPrices.stream().sorted().collect(Collectors.toList());
        
        return allPrices;
    }
    
    public List<InputPrice> forObjectIdLike(String objectIdPart) {
        String query = " FROM " + getEntityClass().getName() + " ip AND ip." + InputPrice.PROPERTY_OBJECT_ID + " LIKE :objectid ";
        return findAll(query, DaoUtils.asArgsMap("objectid", objectIdPart));
    }
}
