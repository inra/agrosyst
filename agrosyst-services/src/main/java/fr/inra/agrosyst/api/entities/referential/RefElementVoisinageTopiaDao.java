package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;

import java.util.Comparator;
import java.util.List;

public class RefElementVoisinageTopiaDao extends AbstractRefElementVoisinageTopiaDao<RefElementVoisinage> {

    public List<RefElementVoisinage> getAllActiveElementsVoisinage(Language language) {
        I18nDaoHelper nomi18n = I18nDaoHelper.withSimpleI18nKey(
                RefElementVoisinage.PROPERTY_IAE_NOM,
                language,
                TradRefDivers.class,
                "rev"
        );
        String query = "SELECT rev," + nomi18n.getAlias();
        query += newFromClause("rev ");
        query += nomi18n.leftJoinTranslation();
        query += " WHERE rev." + RefElementVoisinage.PROPERTY_ACTIVE + " = true";
        query += " ORDER BY " + nomi18n.coalesceTranslation();

        List<Object[]> resultWithTranslation = findAll(query);
        return resultWithTranslation.stream()
                .map(objects -> {
                    RefElementVoisinage refElementVoisinage = (RefElementVoisinage) objects[0];
                    refElementVoisinage.setIae_nom_Translated(I18nDaoHelper.tryGetTranslation(objects[1]).orElse(refElementVoisinage.getIae_nom()));
                    return refElementVoisinage;
                })
                .sorted(Comparator.comparing(RefElementVoisinage::getIae_nom_Translated))
                .toList();
    }
} //RefElementVoisinageTopiaDao
