package fr.inra.agrosyst.api.entities.migration;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class V3_0_11_1__12164_fix_up_speciestoarea extends BaseJavaMigration {

    private static final Log LOG = LogFactory.getLog(V3_0_11_1__12164_fix_up_speciestoarea.class);

    String GET_MISSING_BIOLOGICAL_USAGE_TARGET = """
            SELECT topiaid, speciestoarea, to_replace FROM _12164_speciestoarea_fix
            """;

    String UPDATE_DOMAIN_SPECIESTOAREA = """
            UPDATE domain set speciestoarea = (replace(speciestoarea, ? , ?)) WHERE topiaid = ?
            """;
    @Override
    public void migrate(Context context) throws Exception {
        Connection connection = context.getConnection();
        List<DomainSpeciesAreaFix> domainSpeciesAreaFixes = new ArrayList<>();
        try (Statement loadFixStatement = connection.createStatement()) {
            ResultSet resultSet = loadFixStatement.executeQuery(GET_MISSING_BIOLOGICAL_USAGE_TARGET);
            while (resultSet.next()) {
                DomainSpeciesAreaFix fix = new DomainSpeciesAreaFix(
                        resultSet.getString("topiaid"),
                        resultSet.getString("speciestoarea"),
                        resultSet.getString("to_replace")
                );
                domainSpeciesAreaFixes.add(fix);
            }
        } catch (SQLException throwables) {
            LOG.error(throwables);
            throw new AgrosystTechnicalException("FAILED to load _12164_speciestoarea_fix ", throwables);
        }

        try (PreparedStatement prepareStatement = connection.prepareStatement(UPDATE_DOMAIN_SPECIESTOAREA)) {
            for (DomainSpeciesAreaFix domainSpeciesAreaFix : domainSpeciesAreaFixes) {
                LOG.info(String.format("domain '%s', remplace: '%s' par '%s' de '%s", domainSpeciesAreaFix.topiaid, domainSpeciesAreaFix.to_replace, domainSpeciesAreaFix.to_replace.replace(",","."), domainSpeciesAreaFix.speciestoarea));
                prepareStatement.setString(1, domainSpeciesAreaFix.to_replace);
                prepareStatement.setString(2, domainSpeciesAreaFix.to_replace.replace(",","."));
                prepareStatement.setString(3, domainSpeciesAreaFix.topiaid);
                prepareStatement.addBatch();
            }
            prepareStatement.executeBatch();
        } catch (SQLException throwables) {
            LOG.error(throwables);
            throw new AgrosystTechnicalException("FAILED to update domain speciestoarea", throwables);
        }
    }

    protected record DomainSpeciesAreaFix(
            String topiaid,
            String speciestoarea,
            String to_replace
    ){}
}
