package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class NetworkManagerTopiaDao extends AbstractNetworkManagerTopiaDao<NetworkManager> {

    public List<NetworkManager> getNameFilteredNetworkManager(String name) {
        String query = "FROM " + getEntityClass().getName() + " nm";
        query += " WHERE 1 = 0";
        Map<String, Object> args = Maps.newLinkedHashMap();
        if (!StringUtils.isBlank(name)){

            // Prénom
            query += DaoUtils.orAttributeLike("nm", NetworkManager.PROPERTY_AGROSYST_USER + '.' + AgrosystUser.PROPERTY_FIRST_NAME, args, name);

            // Nom
            query += DaoUtils.orAttributeLike("nm", NetworkManager.PROPERTY_AGROSYST_USER + '.' + AgrosystUser.PROPERTY_LAST_NAME, args, name);
        }
        List<NetworkManager> result = findAll(query, args);
        return result;
    }

} //NetworkManagerTopiaDao<E extends NetworkManager>
