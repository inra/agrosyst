package fr.inra.agrosyst.api.entities.effective;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collections;
import java.util.List;

public class EffectiveCropCycleConnectionTopiaDao extends AbstractEffectiveCropCycleConnectionTopiaDao<EffectiveCropCycleConnection> {

    /**
     * Retourne les connexions qui appartiennent au cycle.
     * Via une requete, car la navigabilité ne permet pas la navigation.
     * 
     * @param cycle le cycle qui contient les connexions
     * @return all cycle's connections
     */
    public List<EffectiveCropCycleConnection> findAllByEffectiveSeasonalCropCycle(EffectiveSeasonalCropCycle cycle) {
        List<EffectiveCropCycleConnection> result;
        // prevent query bug if nodes collection is empty
        if (CollectionUtils.isEmpty(cycle.getNodes())) {
            result = Collections.emptyList();
        } else {
            String query = "FROM " + getEntityClass().getName() + " C" +
                " WHERE (C." + EffectiveCropCycleConnection.PROPERTY_SOURCE + " IN (:cycleNodes) OR C." + EffectiveCropCycleConnection.PROPERTY_SOURCE + " IS NULL)" +
                " AND C." + EffectiveCropCycleConnection.PROPERTY_TARGET + " IN (:cycleNodes)";

            result = findAll(query, DaoUtils.asArgsMap("cycleNodes", cycle.getNodes()));

        }
        return result;
    }
}
