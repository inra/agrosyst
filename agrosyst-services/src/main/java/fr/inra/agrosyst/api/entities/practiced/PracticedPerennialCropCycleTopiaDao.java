package fr.inra.agrosyst.api.entities.practiced;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.utils.DaoUtils;

public class PracticedPerennialCropCycleTopiaDao extends AbstractPracticedPerennialCropCycleTopiaDao<PracticedPerennialCropCycle> {

    public PracticedPerennialCropCycle findPerennialCropCycleForIntervention(PracticedIntervention practicedIntervention) {
        String query = "FROM " + PracticedPerennialCropCycle.class.getName() + " C" +
                " WHERE :practicedCropCyclePhase in elements (C." + PracticedPerennialCropCycle.PROPERTY_CROP_CYCLE_PHASES + ")";

        return findUnique(query, DaoUtils.asArgsMap("practicedCropCyclePhase", practicedIntervention.getPracticedCropCyclePhase()));
    }
} //PracticedPerennialCropCycleTopiaDao
