package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.services.action.HarvestingActionValorisationDto;
import fr.inra.agrosyst.api.services.common.GlobalRefHarvestingPriceFilter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RefHarvestingPriceTopiaDao extends AbstractRefHarvestingPriceTopiaDao<RefHarvestingPrice> {

    protected static final String REF_HARVESTING_PRICE_BASE_QUERY =
            " FROM " + RefHarvestingPrice.class.getName() + " rhp " +
                    " WHERE rhp." + RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A + " = :destination0 " +
                    " %s " +
                    " AND rhp." + RefHarvestingPrice.PROPERTY_ORGANIC + " = :isOrganic " +
                    " AND rhp." + RefHarvestingPrice.PROPERTY_ACTIVE + " = :active " +
                    " %s ";

    
    private String getSpeciesSubQuery(Set<Pair<String, String>> valorisationCodeEspeceBotaniqueCodeQualifiantAee,
                                      Map<String, Object> args) {
        StringBuilder speciesSubQuery = new StringBuilder();
        
        if (CollectionUtils.isNotEmpty(valorisationCodeEspeceBotaniqueCodeQualifiantAee)) {
            // A) to return references prices that match same code espece botanique/code qualifiant
            String speciesSubQuery0 =
                    " %1$s (" +                                                                                     // AND/OR
                            "      %2$s." + RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE + " = :ceb_%3$d_%4$d AND " +  // alias + numArg + count
                            "      %2$s." + RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE + "  = :cq_%3$d_%4$d" +         // alias + numArg + count
                            "    )";

            // B) to return references prices that match same code espece and that are valid for all code qualifiant
            String speciesSubQuery1 =
                    " OR (" +
                    "      %1$s." + RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE+ " = :ceb_%2$d_%3$d AND " +  // alias + numArg + count
                    "      (%1$s." + RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE+ " IS NULL OR " +            // alias
                    "      %1$s." + RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE+ " = '') " +            // alias
                    "    )";

            // C) to return references prices that don't matter the species
            String speciesSubQuery2 =
                    " OR ((%1$s."+ RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE + " IS NULL OR " +
                    "      %1$s." + RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE + " = '') AND " +
                    "     (%1$s." + RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE + " IS NULL OR " +
                    "      %1$s." + RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE + " = '')" +
                    "    )";

            Iterator<Pair<String,String>> iterator = valorisationCodeEspeceBotaniqueCodeQualifiantAee.iterator();
            int count = 0;
            speciesSubQuery.append(" AND (");
            while (iterator.hasNext()) {
                Pair<String, String> cebcq = iterator.next();
                String operator = count == 0 ? "" : "OR";
//              A)
                speciesSubQuery.append(String.format(
                        speciesSubQuery0,
                        operator,
                        "rhp",
                        0,
                        count));
    
                args.put(String.format("ceb_%d_%d", 0, count), cebcq.getLeft());
                args.put(String.format("cq_%d_%d", 0, count), cebcq.getRight());
//              B)
                count++;
    
                speciesSubQuery.append(String.format(
                        speciesSubQuery1,
                        "rhp", 0, count));
    
                args.put(String.format("ceb_%d_%d", 0, count), cebcq.getLeft());
    
                count++;
            }
//          // C)
            speciesSubQuery.append(String.format(speciesSubQuery2, "rhp"));
    
            speciesSubQuery.append(")");
    
        }
    
        return speciesSubQuery.toString();
    }
    
    public List<RefHarvestingPrice> findRefHarvestingPricesForValorisation(
            HarvestingActionValorisation valorisation,
            Set<Pair<String, String>> allValorisationCodeEspeceBotaniqueCodeQualifiantAee) {
        
        Map<String, Object> args = new LinkedHashMap<>();
        
        String speciesSubQuery = getSpeciesSubQuery(allValorisationCodeEspeceBotaniqueCodeQualifiantAee, args);
        
        String query = String.format(
                REF_HARVESTING_PRICE_BASE_QUERY,
                " AND rhp." + RefHarvestingPrice.PROPERTY_CAMPAIGN + " <= :campaigns0 ",
                speciesSubQuery);
        
        args.put("destination0", valorisation.getDestination().getCode_destination_A());
        args.put("campaigns0", valorisation.getEndingMarketingPeriodCampaign());
        args.put("isOrganic", valorisation.isIsOrganicCrop());
        args.put("active", true);
        
        List<RefHarvestingPrice> harvestingPricesForValorisation = findAll(query, args);
        
        return harvestingPricesForValorisation;
    }

    public List<RefHarvestingPrice> findRefHarvestingPricesForValorisation(
            HarvestingActionValorisationDto valorisationDto,
            Set<Pair<String, String>> allValorisationCodeEspeceBotaniqueCodeQualifiantAee) {

        Map<String, Object> args = new LinkedHashMap<>();

        String speciesSubQuery = getSpeciesSubQuery(allValorisationCodeEspeceBotaniqueCodeQualifiantAee, args);

        String query = "SELECT rhp FROM " + RefHarvestingPrice.class.getName() + " rhp " +
                " , " + RefDestination.class.getName() + " rd " +
                " WHERE rhp." + RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A + " = rd." + RefDestination.PROPERTY_CODE_DESTINATION__A +
                " AND rd." + RefDestination.PROPERTY_TOPIA_ID + " = :destination0" +
                " AND rhp." + RefHarvestingPrice.PROPERTY_CAMPAIGN + " <= :campaigns0 " +
                " AND rhp." + RefHarvestingPrice.PROPERTY_ORGANIC + " = :isOrganic " +
                " AND rhp." + RefHarvestingPrice.PROPERTY_ACTIVE + " = :active " +
                speciesSubQuery;


        args.put("destination0", valorisationDto.getDestinationId());
        args.put("campaigns0", valorisationDto.getEndingMarketingPeriodCampaign());
        args.put("isOrganic", valorisationDto.isOrganicCrop());
        args.put("active", true);

        List<RefHarvestingPrice> harvestingPricesForValorisation = findAll(query, args);

        return harvestingPricesForValorisation;
    }

    protected Map<String, List<RefHarvestingPrice>> indexByPracticedSystemId(List<Object[]> refHarvestingPricesByPracticedSystemsRows) {
        Map<String, List<RefHarvestingPrice>> result = new HashMap<>();
        for (Object[] row : refHarvestingPricesByPracticedSystemsRows) {
            String practicedSystemId = (String) row[0];
            RefHarvestingPrice rap = (RefHarvestingPrice) row[1];
            List<RefHarvestingPrice> practicedSystemPrices = result.computeIfAbsent(practicedSystemId, k -> new ArrayList<>());
            practicedSystemPrices.add(rap);
        }
        return result;
    }
    
    public Map<String, List<RefHarvestingPrice>> getAllScenarioPricesForGrowingSystemByPracticedSystemIds(
            GrowingSystem gs,
            Set<String> scenarioCodes) {

        String query = " SELECT ps." + PracticedSystem.PROPERTY_TOPIA_ID + ", rsp "
                + String.format(" FROM %s p, %s cps, %s rsp ", HarvestingPrice.class.getName(), CroppingPlanSpecies.class.getName(), getEntityClass().getName())
                + " INNER JOIN p." + HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION + " AS val "
                + " INNER JOIN val." + HarvestingActionValorisation.PROPERTY_DESTINATION + " AS valDestination_ "
                + " INNER JOIN p." + HarvestingPrice.PROPERTY_PRACTICED_SYSTEM + " AS ps "
                + " INNER JOIN cps." + CroppingPlanSpecies.PROPERTY_SPECIES + " AS refEspece"
                + " WHERE ps." + PracticedSystem.PROPERTY_GROWING_SYSTEM  + " = :gs"
                + " AND rsp." + RefHarvestingPrice.PROPERTY_ACTIVE + " IS TRUE "
                + " AND rsp." + RefHarvestingPrice.PROPERTY_CODE_SCENARIO + " IN :scenarioCodes ";

        Map<String, Object> args = new HashMap<>();
        args.put("gs", gs);
        args.put("scenarioCodes", scenarioCodes);

        query += getRefHarvestingPricesCommonQueryPart();

        List<Object[]> rows = findAll(query, args);
        Map<String, List<RefHarvestingPrice>> result = indexByPracticedSystemId(rows);

        return result;
    }
    
    public List<RefHarvestingPrice> getAllRefPricesForDomainAndScenarios(
            Domain domain,
            Set<String> scenarioCodes) {
    
        String query = "SELECT DISTINCT rsp "
                + String.format(" FROM %s p, %s cps, %s rsp ", HarvestingPrice.class.getName(), CroppingPlanSpecies.class.getName(), getEntityClass().getName())
                + " INNER JOIN p." + HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION + " AS val "
                + " INNER JOIN val." + HarvestingActionValorisation.PROPERTY_DESTINATION + " AS valDestination_ "
                + " INNER JOIN cps." + CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY + " AS cpe "
                + " INNER JOIN cps." + CroppingPlanSpecies.PROPERTY_SPECIES + " AS refEspece "
                + " WHERE p." + HarvestingPrice.PROPERTY_DOMAIN + " = cpe." + CroppingPlanEntry.PROPERTY_DOMAIN
                + " AND p." + HarvestingPrice.PROPERTY_DOMAIN + " = :dom"
                + " AND rsp." + RefHarvestingPrice.PROPERTY_ACTIVE + " IS TRUE "
                + " AND rsp." + RefHarvestingPrice.PROPERTY_CODE_SCENARIO + " IN :scenarioCodes ";
    
        Map<String, Object> args = new HashMap<>();
        args.put("dom", domain);
        args.put("scenarioCodes", scenarioCodes);
    
        query += getRefHarvestingPricesCommonQueryPart();
    
        List<RefHarvestingPrice> result = findAll(query, args);
        return result;
    }

    protected String getRefHarvestingPricesCommonQueryPart() {
        String subQuery =
                "   AND   cps." + CroppingPlanSpecies.PROPERTY_CODE + " = val." + HarvestingActionValorisation.PROPERTY_SPECIES_CODE
                + " AND   rsp." + RefHarvestingPrice.PROPERTY_ORGANIC + " = val." + HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP
                + " AND   rsp." + RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A + " = valDestination_." + RefDestination.PROPERTY_CODE_DESTINATION__A
                + " AND   (" +
                "       ( rsp." + RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE + " = refEspece." + RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE
                + "       AND   rsp." + RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE + " = refEspece." + RefEspece.PROPERTY_CODE_QUALIFIANT__AEE
                + "     ) OR ("
                + "       rsp." + RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE + " = refEspece." + RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE
                + "       AND  rsp." + RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE + " = '' "
                + "     ) OR ("
                + "       rsp." + RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE + " = ''"
                + "       AND   rsp." + RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE + " = '' "
                + "     )"
                + " )"
                + " AND   ("
                + "         (rsp." + RefHarvestingPrice.PROPERTY_MARKETING_PERIOD + " = val." + HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD
                + "           AND rsp." + RefHarvestingPrice.PROPERTY_MARKETING_PERIOD_DECADE + " >= val." + HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE
                + "                AND rsp." + RefHarvestingPrice.PROPERTY_MARKETING_PERIOD_DECADE + " <= val." + HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE
                + "            OR rsp." + RefHarvestingPrice.PROPERTY_MARKETING_PERIOD_DECADE + " IS NULL "
                + "         )"
                + "         OR"
                + "         (rsp." + RefHarvestingPrice.PROPERTY_MARKETING_PERIOD + " > val." + HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD
                + "          AND rsp." + RefHarvestingPrice.PROPERTY_MARKETING_PERIOD + " < val." + HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD
                + "         )"
                + "         OR"
                + "         (rsp." + RefHarvestingPrice.PROPERTY_MARKETING_PERIOD + " = val." + HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD
                + "           AND rsp." + RefHarvestingPrice.PROPERTY_MARKETING_PERIOD_DECADE + " <= val." + HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE
                + "            OR rsp." + RefHarvestingPrice.PROPERTY_MARKETING_PERIOD_DECADE + " IS NULL"
                + "         )"
                + "         OR"
                + "         (rsp." + RefHarvestingPrice.PROPERTY_MARKETING_PERIOD + " = -1"// ALL
                + "            AND rsp." + RefHarvestingPrice.PROPERTY_MARKETING_PERIOD_DECADE + " >= val." + HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE
                + "                AND rsp." + RefHarvestingPrice.PROPERTY_MARKETING_PERIOD_DECADE + " <= val." + HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE
                + "            OR rsp." + RefHarvestingPrice.PROPERTY_MARKETING_PERIOD_DECADE + " IS NULL"
                + "         )"
                + "       )";
        return subQuery;
    }

    public List<RefHarvestingPrice> loadRefPriceForCodeEspeceBotaniquesAndGivenCampaigns(
            GlobalRefHarvestingPriceFilter filter) {

        Map<String, Object> args = Maps.newLinkedHashMap();

        String query =
                "SELECT hp " +
                        " FROM " + RefHarvestingPrice.class.getName() + " hp " +
                        " WHERE hp." + RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE + " IN (:codeEspeceBotaniques)" +
                        " AND hp." + RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A + " IN (:codeDestinations) " +
                        " AND hp." + RefHarvestingPrice.PROPERTY_CAMPAIGN + " >= :beginMarketingPeriodCampaign " +
                        " AND hp." + RefHarvestingPrice.PROPERTY_CAMPAIGN + " <= :endingMarketingPeriodCampaign " +
                        " AND hp." + RefHarvestingPrice.PROPERTY_ACTIVE + " IS TRUE " +
                        " AND hp." + RefHarvestingPrice.PROPERTY_PRICE + " > 0 "
                ;
        args.put("codeEspeceBotaniques", filter.code_espece_botaniques());
        args.put("codeDestinations", filter.code_destination_As());
        args.put("beginMarketingPeriodCampaign", filter.beginMarketingPeriodCampaign());
        args.put("endingMarketingPeriodCampaign", filter.endingMarketingPeriodCampaign());

        List<RefHarvestingPrice> result = findAll(query, args);
        return result;
    }
}
