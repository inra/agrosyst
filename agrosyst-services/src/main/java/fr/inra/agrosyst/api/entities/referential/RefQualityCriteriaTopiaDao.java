package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import fr.inra.agrosyst.services.referential.ReferentialServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RefQualityCriteriaTopiaDao extends AbstractRefQualityCriteriaTopiaDao<RefQualityCriteria> {

    public List<RefQualityCriteria> getRefQualityCriteria(Language language, Map<Pair<String, String>, List<Sector>>sectorsByCodeEspeceBotaniqueCodeQualifiant, Set<WineValorisation> wineValorisations) {

        List<RefQualityCriteria> result = new ArrayList<>();

        if ((sectorsByCodeEspeceBotaniqueCodeQualifiant != null &&
                !sectorsByCodeEspeceBotaniqueCodeQualifiant.isEmpty()) ||
                CollectionUtils.isNotEmpty(wineValorisations)) {

            List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(language, "qc");

            Map<String, Object> args = Maps.newLinkedHashMap();
            String selectQueryString = String.format("SELECT qc, %s FROM %s qc %s WHERE 1 = 1",
                    I18nDaoHelper.buildQuerySelectTranslationProperties(i18nDaoHelpers),
                    RefQualityCriteria.class.getName(),
                    I18nDaoHelper.buildQueryJoinTranslations(i18nDaoHelpers));
            StringBuilder query = new StringBuilder(selectQueryString);

            int i = 0;
            for (Map.Entry<Pair<String, String>, List<Sector>> sectorsByCodeEspeceBotaniqueCodeQualifant : sectorsByCodeEspeceBotaniqueCodeQualifiant.entrySet()) {

                Pair<String, String> codeEspeceBotaniqueCodeQualifant = sectorsByCodeEspeceBotaniqueCodeQualifant.getKey();
                List<Sector> sectors = sectorsByCodeEspeceBotaniqueCodeQualifant.getValue();
                String codeEspeceBotaniques = codeEspeceBotaniqueCodeQualifant.getLeft();
                String codeEspeceQualifant = codeEspeceBotaniqueCodeQualifant.getRight();

                if (ReferentialServiceImpl.WINE.equals(codeEspeceBotaniques) &&  CollectionUtils.isEmpty(wineValorisations)) {
                    continue;
                }

                String operand = i == 0 ? " AND (" : " OR (";

                // filter on Quality Criteria that match same Sectors
                if (sectors != null) {
                    query.append(String.format("%s qc." + RefQualityCriteria.PROPERTY_SECTOR + " IN (:sector_%d)", operand, i));
                    args.put( "sector_" + i, Sets.newHashSet(sectors));
                } else {
                    query.append(String.format("%s 1=1", operand));
                }

                query.append(" AND (");

                if (ReferentialServiceImpl.WINE.equals(codeEspeceBotaniques)) {
                    // Filter on Quality Criteria that match same codeEspeceBotanique
                    query.append(" qc." + RefQualityCriteria.PROPERTY_CODE_ESPECE_BOTANIQUE + " = :codeEspeceBotaniques_").append(i);
                    args.put("codeEspeceBotaniques_" + i, codeEspeceBotaniques);

                    query.append(" AND (");
                    query.append(String.format(" qc." + RefQualityCriteria.PROPERTY_WINE_VALORISATION + " IN (:wineValorisations_%d) ", i));
                    args.put("wineValorisations_" + i, wineValorisations);

                    query.append(" OR (qc." + RefQualityCriteria.PROPERTY_WINE_VALORISATION + " IS NULL )");
                    query.append(" )");

                } else {

                    // Filter on Quality Criteria that match same codeEspeceBotanique
                    query.append(" qc." + RefQualityCriteria.PROPERTY_CODE_ESPECE_BOTANIQUE + " = :codeEspeceBotaniques_").append(i);
                    args.put("codeEspeceBotaniques_" + i, codeEspeceBotaniques);

                    if (StringUtils.isNotBlank(codeEspeceQualifant)) {

                        // include Quality Criteria with same codeQualifantAEE as the given one
                        query.append(" AND (qc." + RefQualityCriteria.PROPERTY_CODE_QUALIFIANT__AEE + " IN (:refCodequalifiantAEE_").append(i).append(")");
                        args.put("refCodequalifiantAEE_" + i, codeEspeceQualifant);

                        // this is to include Quality Criteria that does not have any codeQualifantAEE
                        query.append(" OR qc." + RefQualityCriteria.PROPERTY_CODE_QUALIFIANT__AEE + " IS NULL )");
                    } else {

                        // include all Quality Criteria that does not target a precise codeQualifantAEE
                        query.append(" AND qc." + RefQualityCriteria.PROPERTY_CODE_QUALIFIANT__AEE + " IS NULL ");
                    }
                }

                // Include Quality Criteria that match all species for the same sector
                if (CollectionUtils.isNotEmpty(sectors)) {
                    query.append(" OR (qc." + RefQualityCriteria.PROPERTY_CODE_ESPECE_BOTANIQUE + " IS NULL ");
                    query.append(" AND qc." + RefQualityCriteria.PROPERTY_CODE_QUALIFIANT__AEE + " IS NULL )");
                }

                query.append("))");

                i++;
            }

            query.append(DaoUtils.andAttributeEquals("qc", RefQualityCriteria.PROPERTY_ACTIVE , args, true));

            query.append(" ORDER BY ").append(i18nDaoHelpers.getFirst().coalesceTranslation());

            // cas ou il n'y a qu'une seule espèce, que celle ci est une espèce de type vigne et que aucune valorisation vigne n'a été sélectionnée
            if (args.size() != 1) {
                List<Object[]> resultWithTranslation = findAll(query.toString(), args);
                for (Object[] objects : resultWithTranslation) {
                    RefQualityCriteria qc = (RefQualityCriteria)objects[0];
                    qc.setQualityCriteriaLabel_Translated(I18nDaoHelper.tryGetTranslation(objects[1]).orElse(qc.getQualityCriteriaLabel()));
                    result.add(qc);
                }
            }
        }

        return result;
    }

    public List<String> findAllRefQualityCriteriaCodes() {
        List<String> result;
        String query = "SELECT DISTINCT(rqc." + RefQualityCriteria.PROPERTY_CODE + ") FROM " + RefQualityCriteria.class.getName() + " rqc";
        result = findAll(query);
        return result;
    }

    protected List<I18nDaoHelper> getI18nDaoHelpers(Language language, String entityAlias) {
        return Lists.newArrayList(
            I18nDaoHelper.withSimpleI18nKey(RefQualityCriteria.PROPERTY_QUALITY_CRITERIA_LABEL, language, TradRefDivers.class, entityAlias)
        );
    }
}
