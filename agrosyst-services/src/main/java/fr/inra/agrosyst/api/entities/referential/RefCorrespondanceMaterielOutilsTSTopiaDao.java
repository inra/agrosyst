package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.utils.DaoUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RefCorrespondanceMaterielOutilsTSTopiaDao extends AbstractRefCorrespondanceMaterielOutilsTSTopiaDao<RefCorrespondanceMaterielOutilsTS> {

    public Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> findRefCorrespondanceMaterielOutilsTSForTools(Set<RefMateriel> materiels) {
        String query = " SELECT DISTINCT rm, rc FROM " + this.getEntityClass().getName() + " rc, " + RefMaterielOutil.class.getName() + " rm ";
        query +=       " WHERE rm." + RefMaterielOutil.PROPERTY_TYPE_MATERIEL1 + " = rc." + RefCorrespondanceMaterielOutilsTS.PROPERTY_TYPE_MATERIEL_1;
        query +=       "    AND   rm." + RefMaterielOutil.PROPERTY_ACTIVE + " IS TRUE ";
        query +=       "    AND   rc." + RefCorrespondanceMaterielOutilsTS.PROPERTY_ACTIVE + " IS TRUE";
        query +=       "    AND   rm IN (:materiels)";
        List<Object[]> result0 = findAll(query, DaoUtils.asArgsMap("materiels", materiels));
        return getRefMaterielRefCorrespondanceMaterielOutilsTSMap(result0);
    }

    protected Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> getRefMaterielRefCorrespondanceMaterielOutilsTSMap(List<Object[]> result0) {
        Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> result = new HashMap<>();
        for (Object[] row : result0) {
            RefMateriel equipment = (RefMateriel) row[0];
            RefCorrespondanceMaterielOutilsTS correspondance = (RefCorrespondanceMaterielOutilsTS) row[1];
            result.put(equipment, correspondance);
        }
        return result;
    }

} // RefCorrespondanceMaterielOutilsTSTopiaDao
