package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.common.CacheAware;
import fr.inra.agrosyst.services.common.CacheService;
import fr.inra.agrosyst.services.common.ProjectionHelper;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.internal.AbstractTopiaDao;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;

import java.util.HashMap;
import java.util.function.Supplier;

public abstract class AbstractAgrosystTopiaDao<E extends TopiaEntity> extends AbstractTopiaDao<E> implements CacheAware {

    private ProjectionHelper projectionHelper;
    protected Supplier<CacheService> cacheServiceSupplier;

    public Iterable<String> findPropertyDistinctValues(String property) {
        String query = "SELECT DISTINCT " + property + " " + newFromClause() + " ORDER BY " + property;
        Iterable<String> result = findAll(query, new HashMap<>());
        return result;
    }

    public ProjectionHelper getProjectionHelper() {
        if (projectionHelper == null) {
            AgrosystTopiaDaoSupplier supplier = (AgrosystTopiaDaoSupplier) topiaDaoSupplier;
            projectionHelper = new ProjectionHelper(supplier, topiaJpaSupport, getCacheService());
        }
        return projectionHelper;
    }

    public CacheService getCacheService() {
        return cacheServiceSupplier.get();
    }

    @Override
    public void setCacheServiceSupplier(Supplier<CacheService> cacheServiceSupplier) {
        this.cacheServiceSupplier = cacheServiceSupplier;
    }

    public void setEntityTopiaId(E entity) {
        String topiaId = topiaIdFactory.newTopiaId(getEntityClass(), entity);
        entity.setTopiaId(topiaId);
    }

    /**
     * Créé une nouvelle instance du helper. Attention l'instance est liée à la transaction en cours, ne pas l'exposer
     * au dehors du DAO
     */
    protected ComputedCacheHelper newComputedCacheHelper() {
        ComputedCacheHelper result = new ComputedCacheHelper(this.topiaJpaSupport, this.topiaSqlSupport);
        return result;
    }

    /**
     * Permet de déclencher un nettoyage de la session Hibernate
     */
    public void clear() {
        topiaHibernateSupport.getHibernateSession().clear();
    }

    public TopiaJpaSupport getTopiaJpaSupport() {
        return topiaJpaSupport;
    }

    public void save(E entity) {
        if (entity.isPersisted()) {
            update(entity);
        } else {
            create(entity);
        }
    }
}
