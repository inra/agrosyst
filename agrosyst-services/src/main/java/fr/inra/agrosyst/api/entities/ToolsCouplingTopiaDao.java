package fr.inra.agrosyst.api.entities;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.utils.DaoUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class ToolsCouplingTopiaDao extends AbstractToolsCouplingTopiaDao<ToolsCoupling> {
    
    public List<ToolsCoupling> findToolsCouplingsForAllCampaignsDomain(Domain domain) {
        
        String query = "FROM " + ToolsCoupling.class.getName() + " tc ";
        query += " WHERE tc." + ToolsCoupling.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CODE + " = :domainCode";
        query += " AND tc." + ToolsCoupling.PROPERTY_DOMAIN + "." + Domain.PROPERTY_ACTIVE + " = " + Boolean.TRUE;
        
        final List<ToolsCoupling> toolsCouplings = findAll(query, DaoUtils.asArgsMap("domainCode", domain.getCode()));
        return toolsCouplings;
    }

    public Map<String, String> findAllToolsCouplingNamesByCode() {

        String query = String.format(" SELECT tc.%s, tc.%s FROM %s tc", ToolsCoupling.PROPERTY_CODE, ToolsCoupling.PROPERTY_TOOLS_COUPLING_NAME, ToolsCoupling.class.getName());

        Map<String, String> result = new HashMap<>();
        final List<Object[]> toolsCouplings = findAll(query);
        for (Object[] codeAndNAme : toolsCouplings) {
            result.put((String)codeAndNAme[0], (String)codeAndNAme[1]);
        }
        return result;
    }

}
