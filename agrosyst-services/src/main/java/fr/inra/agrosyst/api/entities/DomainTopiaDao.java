package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.SortedColumn;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.entities.security.UserRole;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainSummaryDto;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.security.SecurityContext;
import fr.inra.agrosyst.services.security.SecurityHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.support.HibernateTopiaJpaSupport;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

/**
 * Custom dao methods on domains.
 *
 * @author Eric Chatellier
 */
public class DomainTopiaDao extends AbstractDomainTopiaDao<Domain> {

    private static final String PROPERTY_LOCATION_DEPARTEMENT = Domain.PROPERTY_LOCATION + "." + RefLocation.PROPERTY_DEPARTEMENT;
    static final String PRACTICED_NODE_SEASONAL_CYCLE_PRACTICED_SYSTEM_CAMPAIGNS = PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE + "." + PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM + "." + PracticedSystem.PROPERTY_CAMPAIGNS;

    /**
     * Search domain matching user context filter and custom additional {@code filter}.
     *
     * @param filter custom filter
     * @return matching domains
     */
    public Set<String> getFilteredDomainIds(DomainFilter filter, SecurityContext securityContext) throws TopiaException {

        StringBuilder query = new StringBuilder("SELECT D.topiaId FROM " + getEntityClass().getName() + " D");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = new LinkedHashMap<>();
        applyFiltersOnQuery(filter, securityContext, query, args);
        return new HashSet<>(findAll(query.toString(), args));
    }

    /**
     * Search domain matching user context filter and custom additional {@code filter}.
     *
     * @param filter custom filter
     * @return matching domains
     */
    public PaginationResult<Domain> getFilteredDomains(DomainFilter filter, SecurityContext securityContext) throws TopiaException {

        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;

        StringBuilder query = new StringBuilder("FROM " + getEntityClass().getName() + " D");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = new LinkedHashMap<>();

        // apply non null filter
        applyFiltersOnQuery(filter, securityContext, query, args);

        // refs:9570 TopiaQueryException: unable to find page startIndex=-1, endIndex=-3
        startIndex = Math.max(startIndex, 0);
        endIndex = Math.max(endIndex, DaoUtils.NO_PAGE_LIMIT);

        String filterOrderBy = getFilterOrderBy(filter);

        String queryString = query.toString();
        List<Domain> domains = find(queryString + filterOrderBy, args, startIndex, endIndex);

        long totalCount = findUnique("SELECT count(*) " + queryString, args);

        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        return PaginationResult.of(domains, totalCount, pager);
    }

    /**
     * Search domain matching user context filter and custom additional {@code filter}.
     *
     * @param filter custom filter
     * @return matching domains
     */
    public PaginationResult<DomainSummaryDto> getFilteredDomainSummaries(DomainFilter filter, SecurityContext securityContext) throws TopiaException {
    
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;

        StringBuilder query = new StringBuilder("FROM ").append(getEntityClass().getName()).append(" D");
        //query.append(" LEFT JOIN ").append(Ground.class.getName()).append(" G ON D.topiaId = G.domain");
        //query.append(" LEFT JOIN ").append(Equipment.class.getName()).append(" E ON D.topiaId = E.domain");
        //query.append(" LEFT JOIN ").append(Price.class.getName()).append(" P ON D.topiaId = P.domain");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = new LinkedHashMap<>();
    
        // apply non null filter
        applyFiltersOnQuery(filter, securityContext, query, args);
    
        // refs:9570 TopiaQueryException: unable to find page startIndex=-1, endIndex=-3
        startIndex = Math.max(startIndex, 0);
        endIndex = Math.max(endIndex, DaoUtils.NO_PAGE_LIMIT);
    
        String filterOrderBy = getFilterOrderBy(filter);
    
        String queryString = query.toString();
        List<Domain> queryResult = find(queryString + filterOrderBy, args, startIndex, endIndex);
        List<DomainSummaryDto> domains = queryResult.stream()
                .map(d -> new DomainSummaryDto(d.getTopiaId(), d.getName(), d.getCampaign()))
                .collect(Collectors.toList());

        List<String> practicedPlotNbByDomain = queryResult.stream()
                .filter(Domain::isPracticedPlotNotEmpty)
                .map(Domain::getTopiaId)
                .toList();
        setDataForDomain(practicedPlotNbByDomain, domains, DomainSummaryDto::setHasValidFieldProperties);
        Map<String, Object> domainIdsArgs = Map.of("domains", queryResult);
        String externalTableRefQuery = "SELECT E.domain.topiaId FROM %s E WHERE E.domain IN :domains";
        List<String> equipmentNbByDomain = findAll(String.format(externalTableRefQuery, Equipment.class.getName()), domainIdsArgs);
        setDataForDomain(equipmentNbByDomain, domains, DomainSummaryDto::setHasValidEquipments);
        List<String> cropNbByDomain = findAll(String.format(externalTableRefQuery, CroppingPlanEntry.class.getName()), domainIdsArgs);
        setDataForDomain(cropNbByDomain, domains, DomainSummaryDto::setHasValidCrops);
        List<String> inputNbByDomain = findAll(String.format(externalTableRefQuery, AbstractDomainInputStockUnit.class.getName()), domainIdsArgs);
        setDataForDomain(inputNbByDomain, domains, DomainSummaryDto::setHasValidIputs);
        List<String> priceNbByDomain = findAll(String.format(externalTableRefQuery, InputPrice.class.getName()), domainIdsArgs);
        priceNbByDomain.addAll(findAll(String.format(externalTableRefQuery, HarvestingPrice.class.getName()), domainIdsArgs));
        setDataForDomain(priceNbByDomain, domains, DomainSummaryDto::setHasValidPrices);

        String practicedSystemsQuery = "SELECT ps.growingSystem.growingPlan.domain.topiaId, ps.topiaId, ps.name" +
                " FROM " + PracticedSystem.class.getName() + " ps" +
                " WHERE ps.growingSystem.growingPlan.domain IN :domains";
        List<Object[]> practicedSystems = findAll(practicedSystemsQuery, domainIdsArgs);
        MultiValuedMap<String, DomainSummaryDto.CroppingSystem> practicedSystemsByDomain = new HashSetValuedHashMap<>();

        List<Object> practicedSystemIds = practicedSystems.stream().map(o -> o[1]).toList();
        Map<String, Object> practicedSystemIdsArgs = Map.of("practicedSystemIds", practicedSystemIds);
        String perennialInterventionsQuery = "SELECT pi.practicedCropCyclePhase.practicedPerennialCropCycle.practicedSystem.topiaId" +
                //" pi.practicedCropCycleConnection.target.practicedSeasonalCropCycle.practicedSystem.topiaId" +
                " FROM " + PracticedIntervention.class.getName() + " pi" +
                " WHERE pi.practicedCropCyclePhase.practicedPerennialCropCycle.practicedSystem.topiaId IN :practicedSystemIds";
               // " OR pi.practicedCropCycleConnection.source.practicedSeasonalCropCycle.practicedSystem.topiaId IN :practicedSystemIds";
        String seasonalInterventionsQuery = "SELECT pi.practicedCropCycleConnection.target.practicedSeasonalCropCycle.practicedSystem.topiaId" +
                " FROM " + PracticedIntervention.class.getName() + " pi" +
                " WHERE pi.practicedCropCycleConnection.source.practicedSeasonalCropCycle.practicedSystem.topiaId IN :practicedSystemIds";
        Collection<String> practicedSystemsWithInterventions = new HashSet<>();
        practicedSystemsWithInterventions.addAll(findAll(perennialInterventionsQuery, practicedSystemIdsArgs));
        practicedSystemsWithInterventions.addAll(findAll(seasonalInterventionsQuery, practicedSystemIdsArgs));

        for (Object[] practicedSystem : practicedSystems) {
            String practicedSystemId = practicedSystem[1].toString();
            boolean interventionsForPracticedSystem = practicedSystemsWithInterventions.contains(practicedSystemId);
            DomainSummaryDto.CroppingSystem croppingSystem =
                    new DomainSummaryDto.CroppingSystem(practicedSystemId, practicedSystem[2].toString(), interventionsForPracticedSystem);
            practicedSystemsByDomain.put(practicedSystem[0].toString(), croppingSystem);
        }
        domains.forEach(domain -> {
            List<DomainSummaryDto.CroppingSystem> croppingSystemsAndManagements =
                    practicedSystemsByDomain.get(domain.getTopiaId()).stream()
                            .sorted(Comparator.comparing(DomainSummaryDto.CroppingSystem::practicedSystemName))
                            .toList();
            domain.setCroppingSystemsAndManagements(croppingSystemsAndManagements);
        });

        long totalCount = findUnique("SELECT count(*) " + queryString, args);
    
        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        return PaginationResult.of(domains, totalCount, pager);
    }

    protected void setDataForDomain(List<String> data,
                                    List<DomainSummaryDto> domains,
                                    BiConsumer<DomainSummaryDto, Boolean> setter) {

        domains.forEach(domain -> setter.accept(domain, data.contains(domain.getTopiaId())));
    }
    
    /**
     * do not manage SortedColumn.RESPONSIBLE
     *
     * @return - user selected ORDER BY or DEFAULT one
     */
    private String getFilterOrderBy(DomainFilter filter) {
        String result = null;
        
        if (filter != null && filter.getSortedColumn() != null) {
            SortedColumn sortedColumn = filter.getSortedColumn();
            String filterOrderBy = switch (sortedColumn) {
                case DOMAIN -> " lower (D." + Domain.PROPERTY_NAME + ")";
                case CAMPAIGN -> " D." + Domain.PROPERTY_CAMPAIGN;
                case MAIN_CONTACT -> " lower (D." + Domain.PROPERTY_MAIN_CONTACT + ")";
                case DEPARTEMENT -> PROPERTY_LOCATION_DEPARTEMENT;
                case SIRET -> "COALESCE (" + " D." + Domain.PROPERTY_SIRET + ", '###')";
                default -> null;
            };
            if (StringUtils.isNotBlank(filterOrderBy)) {
                filterOrderBy += " " + filter.getSortOrder().name();
                result = " ORDER BY " +
                        filterOrderBy +
                        ", D." + Domain.PROPERTY_TOPIA_ID;
            }
    
        }
        
        if (result == null) {
            // default
            result = " ORDER BY D." + Domain.PROPERTY_CAMPAIGN + " DESC" +
                    ", lower (D." + Domain.PROPERTY_NAME + ")";
        }
        
        return result;
    }
    
    protected void applyFiltersOnQuery(DomainFilter filter, SecurityContext securityContext, StringBuilder query, Map<String, Object> args) {
        // apply non null filter
        if (filter != null) {
            // selected ids
            query.append(DaoUtils.andAttributeIn("D", Domain.PROPERTY_TOPIA_ID, args, DaoUtils.getRealIds(filter.getSelectedIds(), Domain.class)));
            // name
            query.append(DaoUtils.andAttributeLike("D", Domain.PROPERTY_NAME, args, filter.getDomainName()));
            // active
            query.append(DaoUtils.andAttributeEquals("D", Domain.PROPERTY_ACTIVE, args, filter.getActive()));
            // type
            query.append(DaoUtils.andAttributeEquals("D", Domain.PROPERTY_TYPE, args, filter.getType()));
            // main contact
            query.append(DaoUtils.andAttributeLike("D", Domain.PROPERTY_MAIN_CONTACT, args, filter.getMainContact()));
            // departement
            query.append(DaoUtils.andAttributeLike("D", PROPERTY_LOCATION_DEPARTEMENT, args, filter.getDepartement()));
            // campaign
            query.append(DaoUtils.andAttributeEquals("D", Domain.PROPERTY_CAMPAIGN, args, filter.getCampaign()));
            // siret
            if (StringUtils.isNotBlank(filter.getSiret())) {
                query.append(DaoUtils.andAttributeLike("D", Domain.PROPERTY_SIRET, args, filter.getSiret()));
            }
            if (SortedColumn.SIRET.equals(filter.getSortedColumn())) {
                query.append(" AND D." + Domain.PROPERTY_SIRET + " IS NOT NULL ");
            }
            // responsable
            if (StringUtils.isNotBlank(filter.getResponsable())) {
        
                String[] researchParts = filter.getResponsable().split(" ");
                for (String researchPart : researchParts) {
                    StringBuilder userFilterSubQuery = new StringBuilder(" AND D." + Domain.PROPERTY_CODE + " IN ");
                    userFilterSubQuery.append("(SELECT ur." + UserRole.PROPERTY_DOMAIN_CODE + " FROM ").append(UserRole.class.getName()).append(" ur ");
                    userFilterSubQuery.append(" WHERE 1 = 1");
                    userFilterSubQuery.append(DaoUtils.andAttributeLike("ur", UserRole.PROPERTY_AGROSYST_USER + "." + AgrosystUser.PROPERTY_FIRST_NAME, args, researchPart));
                    userFilterSubQuery.append(DaoUtils.orAttributeLike("ur", UserRole.PROPERTY_AGROSYST_USER + "." + AgrosystUser.PROPERTY_LAST_NAME, args, researchPart));
                    userFilterSubQuery.append(")");
                    query.append(userFilterSubQuery);
                }
            }
            // Navigation context
            NavigationContext navigationContext = filter.getNavigationContext();
            addDomainNavigationContextFilter(query, args, "D", navigationContext);
        }
        SecurityHelper.addDomainFilter(query, args, securityContext, "D");
    }

    public Set<String> networksToDomains(Set<String> networksIds) {
        return getProjectionHelper().networksToDomains(networksIds);
    }

    /**
     * Find all domains using same domain's duplication code.
     *
     * @return related domain (ordered by campaign desc)
     */
    public LinkedHashMap<Integer, String> findAllRelatedDomains(String code, boolean filterOnActive) {
        String query = "SELECT " + Domain.PROPERTY_CAMPAIGN + ", " + Domain.PROPERTY_TOPIA_ID
                + " FROM " + getEntityClass().getName()
                + " WHERE " + Domain.PROPERTY_CODE + " = :code";
                if (filterOnActive) {
                    query += " AND " + Domain.PROPERTY_ACTIVE + " IS TRUE";
                }
        query += " ORDER BY " + Domain.PROPERTY_CAMPAIGN + " DESC";

        List<Object[]> domains = findAll(query, DaoUtils.asArgsMap("code", code));
        return DaoUtils.toRelatedMap(domains);
    }

    /**
     * Retourne le nombre de domaine dont le nom est egal à celui demandé (sans tenir compte
     * de la case.
     *
     * @param domainName domain name
     * @return domain count
     */
    public long countDomainWithName(String domainName) {
        String query = "SELECT count(*) FROM " + getEntityClass().getName()
                + " WHERE lower(" + Domain.PROPERTY_NAME + ") = lower(:" + Domain.PROPERTY_NAME + ")";
        Number result = findUnique(query, DaoUtils.asArgsMap(Domain.PROPERTY_NAME, domainName));
        return result.longValue();
    }

    public List<Integer> getAllCampaigns() {
        String query = "SELECT DISTINCT " + Domain.PROPERTY_CAMPAIGN +
                " FROM " + getEntityClass().getName() +
                " ORDER BY " + Domain.PROPERTY_CAMPAIGN + " ASC";
        return findAll(query, DaoUtils.asArgsMap());
    }

    public Set<String> getAllDomainCodes() {
        String query = "SELECT DISTINCT " + Domain.PROPERTY_CODE +
                " FROM " + getEntityClass().getName();
        List<String> list = findAll(query, DaoUtils.asArgsMap());
        return new HashSet<>(list);
    }

    public void validateDomain(String domainId, LocalDateTime now) {

        Map<String, Object> args = DaoUtils.asArgsMap("domainId", domainId, "now", now);
        topiaJpaSupport.execute("UPDATE " + getEntityClass().getName() + " d" +
                " SET d.validated=true, d.validationDate=:now, d.updateDate=:now" +
                " WHERE d." + Domain.PROPERTY_TOPIA_ID + "=:domainId", args);

        Domain domain = forTopiaIdEquals(domainId).findUnique();

        {
            args = DaoUtils.asArgsMap("domain", domain);
            topiaJpaSupport.execute("UPDATE " + CroppingPlanEntry.class.getName() + " cpe" +
                    " SET cpe." + CroppingPlanEntry.PROPERTY_VALIDATED + "=true" +
                    " WHERE cpe." + CroppingPlanEntry.PROPERTY_TOPIA_ID + " IN (" +
                    "   SELECT sub." + CroppingPlanEntry.PROPERTY_TOPIA_ID + " FROM " + CroppingPlanEntry.class.getName() + " sub" +
                    "   WHERE sub." + CroppingPlanEntry.PROPERTY_DOMAIN + "=:domain" +
                    " )", args);
        }

        {
            args = DaoUtils.asArgsMap("domain", domain);
            topiaJpaSupport.execute("UPDATE " + CroppingPlanSpecies.class.getName() + " cps" +
                    " SET cps." + CroppingPlanSpecies.PROPERTY_VALIDATED + "=true" +
                    " WHERE cps." + CroppingPlanSpecies.PROPERTY_TOPIA_ID + " IN (" +
                    "   SELECT sub." + CroppingPlanSpecies.PROPERTY_TOPIA_ID + " FROM " + CroppingPlanSpecies.class.getName() + " sub" +
                    "   WHERE sub." + CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY + "." + CroppingPlanEntry.PROPERTY_DOMAIN + "=:domain" +
                    " )", args);
        }

        {
            args = DaoUtils.asArgsMap("domain", domain);
            topiaJpaSupport.execute("UPDATE " + Ground.class.getName() + " s" +
                    " SET s." + Ground.PROPERTY_VALIDATED + "=true" +
                    " WHERE s." + Ground.PROPERTY_TOPIA_ID + " IN ( " +
                    "   SELECT sub." + Ground.PROPERTY_TOPIA_ID + " FROM " + Ground.class.getName() + " sub" +
                    "   WHERE sub." + Ground.PROPERTY_DOMAIN + "=:domain" +
                    " )", args);
        }

        {
            args = DaoUtils.asArgsMap("domain", domain);
            topiaJpaSupport.execute("UPDATE " + GeoPoint.class.getName() + " gd" +
                    " SET gd." + GeoPoint.PROPERTY_VALIDATED + "=true" +
                    " WHERE gd." + GeoPoint.PROPERTY_TOPIA_ID + " IN ( " +
                    "   SELECT sub." + GeoPoint.PROPERTY_TOPIA_ID + " FROM " + GeoPoint.class.getName() + " sub" +
                    "   WHERE sub." + GeoPoint.PROPERTY_DOMAIN + "=:domain" +
                    " )", args);
        }

        {
            Collection<WeatherStation> weatherStations = domain.getWeatherStations();
            if (CollectionUtils.isNotEmpty(weatherStations)) {
                args = DaoUtils.asArgsMap("weatherStationIds", weatherStations.stream().map(TopiaEntity::getTopiaId).collect(Collectors.toSet()));
                topiaJpaSupport.execute("UPDATE " + WeatherStation.class.getName() + " ws" +
                        " SET ws." + WeatherStation.PROPERTY_VALIDATED + "=true" +
                        " WHERE ws." + WeatherStation.PROPERTY_TOPIA_ID + " IN ( :weatherStationIds )", args);
            }
        }

        {
            args = DaoUtils.asArgsMap("domain", domain);
            topiaJpaSupport.execute("UPDATE " + Equipment.class.getName() + " m" +
                    " SET m." + Equipment.PROPERTY_VALIDATED + "=true" +
                    " WHERE m." + Equipment.PROPERTY_TOPIA_ID + " IN ( " +
                    "   SELECT sub." + Equipment.PROPERTY_TOPIA_ID + " FROM " + Equipment.class.getName() + " sub" +
                    "   WHERE sub." + Equipment.PROPERTY_DOMAIN + "=:domain" +
                    " )", args);
        }

        {
            args = DaoUtils.asArgsMap("domain", domain);
            topiaJpaSupport.execute("UPDATE " + ToolsCoupling.class.getName() + " tc" +
                    " SET tc." + ToolsCoupling.PROPERTY_VALIDATED + "=true" +
                    " WHERE tc." + ToolsCoupling.PROPERTY_TOPIA_ID + " IN ( " +
                    "   SELECT sub." + ToolsCoupling.PROPERTY_TOPIA_ID + " FROM " + ToolsCoupling.class.getName() + " sub" +
                    "   WHERE sub." + ToolsCoupling.PROPERTY_DOMAIN + "=:domain" +
                    " )", args);
        }

    }

    public String getDomainCodeForGrowingSystem(String gsTopiaId) {
        String query = "SELECT gs." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." +Domain.PROPERTY_CODE +
                " FROM " + GrowingSystem.class.getName() + " gs " +
                " WHERE gs." + GrowingSystem.PROPERTY_TOPIA_ID + " = :gsTopiaId" ;

        return findUnique(query, DaoUtils.asArgsMap("gsTopiaId", gsTopiaId));
    }

    public String getDomainIdForGrowingSystem(String gsTopiaId) {
        String query = "SELECT gs." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." +Domain.PROPERTY_TOPIA_ID +
                " FROM " + GrowingSystem.class.getName() + " gs " +
                " WHERE gs." + GrowingSystem.PROPERTY_TOPIA_ID + " = :gsTopiaId" ;

        return findUnique(query, DaoUtils.asArgsMap("gsTopiaId", gsTopiaId));
    }

    /*
     * WARNING AThimel 18/02/2014 : Do not use this method to get writable domains, this is only usable on DecisionRule creation !
     * See https://forge.codelutin.com/issues/4439#note-6
     */
    public List<Domain> getActiveWritableDomainsForDecisionRuleCreation(SecurityContext securityContext, DomainFilter filter) {
        StringBuilder query = new StringBuilder("FROM " + getEntityClass().getName() + " D");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = new LinkedHashMap<>();

        // active
        final String domainAlias = "D";
        
        query.append(DaoUtils.andAttributeEquals(domainAlias, Domain.PROPERTY_ACTIVE, args, true));

        SecurityHelper.addWritableDomainFilterForDecisionRuleCreation(query, args, securityContext, domainAlias);
    
        // Navigation context
        NavigationContext navigationContext = filter.getNavigationContext();
    
        addDomainNavigationContextFilter(query, args, domainAlias, navigationContext);
    
        String queryString = query.toString();
        String queryAndOrder = queryString + " ORDER BY " +
                "lower (D." + Domain.PROPERTY_NAME + ")";
        List<Domain> domains = findAll(queryAndOrder, args);

        // Filter to retain only one domain per code
        // TODO AThimel 18/02/14 Find a better way
        final Set<String> presentCodes = new HashSet<>();
        Iterables.removeIf(domains, input -> {
            // If the code is already present in Set, 'added' will be false
            boolean added = presentCodes.add(input.getCode());
            return !added;
        });

        return domains;
    }
    
    protected void addDomainNavigationContextFilter(StringBuilder query,
                                                    Map<String, Object> args,
                                                    String domainAlias,
                                                    NavigationContext navigationContext) {
        if (navigationContext != null) {
            
            // campaigns
            Set<Integer> ncCampaigns = navigationContext.getCampaigns();
            query.append(DaoUtils.andAttributeInIfNotEmpty(domainAlias, Domain.PROPERTY_CAMPAIGN, args, ncCampaigns));
            
            // networks
            if (navigationContext.getNetworksCount() > 0) {
                Set<String> domainIds = networksToDomains(navigationContext.getNetworks());
                query.append(DaoUtils.andAttributeIn(domainAlias, Domain.PROPERTY_TOPIA_ID, args, DaoUtils.getRealIds(domainIds, Domain.class)));
            }
            
            // domains
            Set<String> domainIds = new HashSet<>(DaoUtils.getRealIds(navigationContext.getDomains(), Domain.class));
            if (!navigationContext.getGrowingSystems().isEmpty()) {
                String hql = "SELECT" +
                        " gs." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID +
                        " FROM " + GrowingSystem.class.getName() + " gs" +
                        " WHERE gs." + GrowingSystem.PROPERTY_TOPIA_ID + " IN :gsIds";
                List<String> domainsIds = findAll(hql, DaoUtils.asArgsMap("gsIds", navigationContext.getGrowingSystems()));
                domainIds.addAll(domainsIds);
            }
            if (!navigationContext.getGrowingPlans().isEmpty()) {
                String hql = "SELECT" +
                        " gp." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID +
                        " FROM " + GrowingPlan.class.getName() + " gp" +
                        " WHERE gp." + GrowingPlan.PROPERTY_TOPIA_ID + " IN :gpIds";
                List<String> domainsIds = findAll(hql, DaoUtils.asArgsMap("gpIds", navigationContext.getGrowingPlans()));
                domainIds.addAll(domainsIds);
            }
            
            query.append(DaoUtils.andAttributeInIfNotEmpty(domainAlias, Domain.PROPERTY_TOPIA_ID, args, domainIds));
        }
    }
    
    /*
     * WARNING echatellier 29/09/2017 : Do not use this method to get writable domains, this is only usable on DecisionRule list !
     * See https://forge.codelutin.com/issues/9402
     */
    public Domain getAnyReadableDomainsForDecisionRuleList(String domainCode, SecurityContext securityContext) {
        StringBuilder query = new StringBuilder("FROM " + getEntityClass().getName() + " D");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = new LinkedHashMap<>();

        // code
        query.append(DaoUtils.andAttributeEquals("D", Domain.PROPERTY_CODE, args, domainCode));

        SecurityHelper.addReadValidatedDomainFilterForDecisionRuleList(query, args, securityContext, "D");

        String queryString = query.toString();
        String queryAndOrder = queryString + " ORDER BY " +
                "lower (D." + Domain.PROPERTY_NAME + ")";

        return findAnyOrNull(queryAndOrder, args);
    }

    public List<String> findDomainIdsForGrowingSystemIdAndCampaigns(String gsTopiaId, Set<Integer> campaigns, boolean includeCropsFromInactiveDomains){
        Preconditions.checkArgument(campaigns != null && !campaigns.isEmpty());
        Map<String, Object> args = new LinkedHashMap<>();

        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT D." + Domain.PROPERTY_TOPIA_ID + " FROM " + getEntityClass().getName() + " D, " + GrowingSystem.class.getName() + " gs ");
        query.append(" INNER JOIN gs." + GrowingSystem.PROPERTY_GROWING_PLAN + " gp ");
        query.append(" INNER JOIN gp." + GrowingPlan.PROPERTY_DOMAIN + " D0 ");
        query.append(" WHERE gs." + GrowingSystem.PROPERTY_TOPIA_ID + " = :gsTopiaId");
        args.put("gsTopiaId", gsTopiaId.contains(GrowingSystem.class.getName()) ? gsTopiaId : GrowingSystem.class.getName() + "_" + gsTopiaId);

        // active
        if (!includeCropsFromInactiveDomains) {
            query.append(DaoUtils.andAttributeEquals("D", Domain.PROPERTY_ACTIVE, args, true));
        }

        query.append(" AND D." + Domain.PROPERTY_CODE + " = D0." + Domain.PROPERTY_CODE);

        // campaigns
        query.append(DaoUtils.andAttributeIn("D", Domain.PROPERTY_CAMPAIGN, args, campaigns));

        return findAll(query.toString(), args);
    }

    public List<String> findDomainIdsForDomainIdAndCampaigns(String domainId, Set<Integer> campaigns, boolean includeCropsFromInactiveDomains){
        Preconditions.checkArgument(campaigns != null && !campaigns.isEmpty());
        Map<String, Object> args = new LinkedHashMap<>();

        String query = "SELECT DISTINCT D0." + Domain.PROPERTY_TOPIA_ID +
                " FROM " + getEntityClass().getName() + " D" +
                " LEFT JOIN " + getEntityClass().getName() + " D0" +
                " ON D." + Domain.PROPERTY_CODE + " = D0." + Domain.PROPERTY_CODE +
                " WHERE D0." + Domain.PROPERTY_TOPIA_ID + " = :domainId";
        args.put("domainId", domainId.contains(Domain.class.getName()) ? domainId : Domain.class.getName() + "_" + domainId);

        // active
        if (!includeCropsFromInactiveDomains) {
            query += DaoUtils.andAttributeEquals("D", Domain.PROPERTY_ACTIVE, args, true);
        }

        // campaigns
        query += DaoUtils.andAttributeIn("D", Domain.PROPERTY_CAMPAIGN, args, campaigns);

        return findAll(query, args);
    }

    public List<String> findCroppingPlanCodeForDomainsAndCampaigns(String code, Set<Integer> campaigns){
        Preconditions.checkArgument(campaigns != null && !campaigns.isEmpty());
        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT cp." + CroppingPlanEntry.PROPERTY_CODE + " FROM " + CroppingPlanEntry.class.getName() + " cp ");
        query.append(" WHERE 1 = 1");

        Map<String, Object> args = new LinkedHashMap<>();
        // domains
        query.append(DaoUtils.andAttributeEquals("cp", CroppingPlanEntry.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CODE, args, code));
        query.append(DaoUtils.andAttributeEquals("cp", CroppingPlanEntry.PROPERTY_DOMAIN + "." + Domain.PROPERTY_ACTIVE, args, true));

        // campaigns
        query.append(DaoUtils.andAttributeIn("cp", CroppingPlanEntry.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN, args, campaigns));

        return findAll(query.toString(), args);
    }

    public Map<String, List<CroppingPlanSpecies>> findCroppingPlanSpeciesForDomainAndCampaignsByCropCode(String domainCode, Set<Integer> campaigns){

        StringBuilder query = new StringBuilder("SELECT DISTINCT cpe."+ CroppingPlanEntry.PROPERTY_CODE + ", cps");
        query.append(" FROM " + CroppingPlanSpecies.class.getName() + " cps ");
        query.append(" INNER JOIN cps." + CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY + " cpe ");
        query.append(" INNER JOIN cpe." + CroppingPlanEntry.PROPERTY_DOMAIN + " domain ");
        query.append(" WHERE 1=1 ");

        Map<String, Object> args = new LinkedHashMap<>();
        // domains
        query.append(DaoUtils.andAttributeEquals("domain", Domain.PROPERTY_CODE , args, domainCode));
        query.append(DaoUtils.andAttributeEquals("domain", Domain.PROPERTY_ACTIVE, args, true));
        query.append(DaoUtils.andAttributeIn("domain", Domain.PROPERTY_CAMPAIGN, args, campaigns));

        List<Object[]> result_ = findAll(query.toString(), args);

        Map<String, List<CroppingPlanSpecies>> result = new HashMap<>();
        if (result_ != null) {
            for (Object[] entry : result_) {
                String cropCode = (String) entry[0];
                CroppingPlanSpecies species = (CroppingPlanSpecies) entry[1];
                List<CroppingPlanSpecies> cropSpecies = result.computeIfAbsent(cropCode, k -> new ArrayList<>());
                cropSpecies.add(species);
            }
        }
        return result;
    }

    public List<CroppingPlanEntry> findCroppingPlanEntriesForDomainAndCampaignsByCropCode(String domainCode, Set<Integer> campaigns){

        Map<String, Object> args = new LinkedHashMap<>();
        args.put("domainCode", domainCode);
        args.put("campaigns", campaigns);

        String query = " SELECT cpe FROM " + CroppingPlanEntry.class.getName() + " cpe " +
                " INNER JOIN cpe." + CroppingPlanEntry.PROPERTY_DOMAIN + " domain " +
                " WHERE domain." + Domain.PROPERTY_CODE + " = :domainCode" +
                " AND domain." + Domain.PROPERTY_ACTIVE + " IS TRUE" +
                " AND domain." + Domain.PROPERTY_CAMPAIGN + " IN :campaigns";

        return findAll(query, args);
    }

    public List<String> findToolsCouplingCodeForDomainsAndCampaigns(String code, Set<Integer> campaigns){
        Preconditions.checkArgument(campaigns != null && !campaigns.isEmpty());
        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT tc." + ToolsCoupling.PROPERTY_CODE + " FROM " + ToolsCoupling.class.getName() + " tc ");
        query.append(" WHERE 1 = 1");

        Map<String, Object> args = new LinkedHashMap<>();
        // domains
        query.append(DaoUtils.andAttributeEquals("tc", ToolsCoupling.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CODE, args, code));
        query.append(DaoUtils.andAttributeEquals("tc", ToolsCoupling.PROPERTY_DOMAIN + "." + Domain.PROPERTY_ACTIVE, args, true));

        // campaigns
        query.append(DaoUtils.andAttributeIn("tc", ToolsCoupling.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN, args, campaigns));

        return findAll(query.toString(), args);
    }

    public List<ToolsCoupling> findToolsCouplingsForDomainCodeAndCampaigns(String domainCode, Set<Integer> campaigns){
        Preconditions.checkArgument(campaigns != null && !campaigns.isEmpty());
        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT tc FROM " + ToolsCoupling.class.getName() + " tc ");
        query.append(" WHERE 1 = 1 ");

        Map<String, Object> args = new LinkedHashMap<>();
        // domains
        query.append(DaoUtils.andAttributeEquals("tc", ToolsCoupling.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CODE, args, domainCode));
        query.append(DaoUtils.andAttributeEquals("tc", ToolsCoupling.PROPERTY_DOMAIN + "." + Domain.PROPERTY_ACTIVE, args, true));

        // campaigns
        query.append(DaoUtils.andAttributeIn("tc", ToolsCoupling.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN, args, campaigns));

        return findAll(query.toString(), args);
    }

    public List<Equipment> findEquipmentsForDomainCodeAndCampaigns(String domainCode, Set<Integer> campaigns){
        Preconditions.checkArgument(campaigns != null && !campaigns.isEmpty());
        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT e FROM " + Equipment.class.getName() + " e ");
        query.append(" WHERE 1 = 1 ");

        Map<String, Object> args = new LinkedHashMap<>();
        // domains
        query.append(DaoUtils.andAttributeEquals("e", Equipment.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CODE, args, domainCode));
        query.append(DaoUtils.andAttributeEquals("e", Equipment.PROPERTY_DOMAIN + "." + Domain.PROPERTY_ACTIVE, args, true));

        // campaigns
        query.append(DaoUtils.andAttributeIn("e", Equipment.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN, args, campaigns));

        return findAll(query.toString(), args);
    }

    protected Set<String> computeToolsCouplingUsed(Set<String> toolsCouplingIds, String subQuery) {

        Map<String, Object> args = new LinkedHashMap<>();
        Preconditions.checkArgument(!toolsCouplingIds.isEmpty());

        String query =
                " SELECT tc." + ToolsCoupling.PROPERTY_TOPIA_ID + " ,";

        query += "(" + subQuery + ")";

        query +=" FROM " + ToolsCoupling.class.getName() + " tc  WHERE 1 = 1 " ;

        query += DaoUtils.andAttributeIn("tc", ToolsCoupling.PROPERTY_TOPIA_ID, args, toolsCouplingIds);
    
        Set<String> result = getDomainInputUsedIds(query, args);
    
        return result;
    }

    public Set<String> getTCUsedForEffectiveInterventions(Set<String> toolsCouplingIds) {
        String query =
            "SELECT COUNT(*) FROM " + EffectiveIntervention.class.getName() + " ei " +
            " INNER JOIN ei." + EffectiveIntervention.PROPERTY_TOOL_COUPLINGS + " tc0 " +
            " WHERE tc0 = tc ";
        return computeToolsCouplingUsed(toolsCouplingIds, query);
    }

    public Set<String> getTCUsedForPracticedInterventionNodesAndConnections(Set<String> toolsCouplingIds, String campaign) {
        String nativeQuery = """
                select
                  tci1_0.topiaId
                from
                    toolsCoupling tci1_0
                where
                    1=1
                and tci1_0.topiaId in (%s)
                and (
                  select count(pii1_0.topiaId)
                  from
                      practicedIntervention pii1_0
                  join
                      practicedCropCycleConnection pccc1_0
                          on pccc1_0.topiaId=pii1_0.practicedCropCycleConnection
                  join
                      practicedCropCycleNode t1_0
                          on t1_0.topiaId=pccc1_0.target
                  join
                      (practicedSeasonalCropCycle pscc1_0
                  join
                      practicedCropCycle pscc1_1
                          on pscc1_0.topiaId=pscc1_1.topiaId)
                      on pscc1_0.topiaId=t1_0.practicedSeasonalCropCycle
                  join
                      practicedSystem ps1_0
                          on ps1_0.topiaId=pscc1_1.practicedSystem
                  join
                      practicedintervention_toolscouplingcodes tcc1_0 on tcc1_0.owner = pii1_0.topiaId and tci1_0.code = tcc1_0.toolsCouplingCodes
                  where ps1_0.campaigns like '%s' escape '') > 0;
                """;

        Set<String> queryToolsCouplingIds = new HashSet<>();
        toolsCouplingIds.forEach(tc -> queryToolsCouplingIds.add("'" + tc + "'"));
        String queryAllToolsCouplingIds = String.join(",", queryToolsCouplingIds);

        TopiaHibernateSupport hibernateSupport = ((HibernateTopiaJpaSupport) topiaJpaSupport).getHibernateSupport();

        List<String> listIds = hibernateSupport.getHibernateSession()
                .createNativeQuery(String.format(nativeQuery, queryAllToolsCouplingIds, campaign), String.class)
                .list();

        return new HashSet<>(listIds);
    }

    public Set<String> getTCUsedForPracticedInterventionPhases(Set<String> toolsCouplingIds, String campaign) {
        String nativeQuery = """
                select
                  tci1_0.topiaId
                from
                    toolsCoupling tci1_0
                where
                    1=1
                and tci1_0.topiaId in (%s)
                and (
                  select count(pii1_0.topiaId)
                  from
                      practicedIntervention pii1_0
                  join
                      practicedCropCyclePhase pccp1_0
                          on pccp1_0.topiaId=pii1_0.practicedCropCyclePhase
                  join
                      (practicedPerennialCropCycle ppcc1_0
                          join
                          practicedCropCycle  ppcc1_1  on ppcc1_0.topiaId=ppcc1_1.topiaId) on ppcc1_0.topiaId = pccp1_0.practicedPerennialCropCycle
                  join
                      practicedSystem ps1_0
                          on ps1_0.topiaId=ppcc1_1.practicedSystem
                  join
                      practicedintervention_toolscouplingcodes tcc1_0 on tcc1_0.owner = pii1_0.topiaId and tci1_0.code = tcc1_0.toolsCouplingCodes
                  where ps1_0.campaigns like '%s' escape '') > 0
                """;

        Set<String> queryToolsCouplingIds = new HashSet<>();
        toolsCouplingIds.forEach(tc -> queryToolsCouplingIds.add("'" + tc + "'"));
        String queryAllToolsCouplingIds = String.join(",", queryToolsCouplingIds);

        TopiaHibernateSupport hibernateSupport = ((HibernateTopiaJpaSupport) topiaJpaSupport).getHibernateSupport();

        List<String> listIds = hibernateSupport.getHibernateSession()
                .createNativeQuery(String.format(nativeQuery, queryAllToolsCouplingIds, campaign), String.class)
                .list();

        return new HashSet<>(listIds);
    }

    public Integer findCampaignForDomainId(String domainId_) {
        String query = " SELECT d." + Domain.PROPERTY_CAMPAIGN + " FROM " + Domain.class.getName() + " d WHERE d." + Domain.PROPERTY_TOPIA_ID + " = :domainid";
        String domainId = domainId_.contains(Domain.class.getName()) ? domainId_ : Domain.class.getName() + domainId_;
        Integer campaign = findUnique(
                query,
                DaoUtils.asArgsMap("domainid", domainId));
        return campaign;
    }
    
    public List<Integer> findCampaignForDomain(String domainCode) {
        String query = " SELECT d." + Domain.PROPERTY_CAMPAIGN +
                " FROM " + Domain.class.getName() + " d " +
                " WHERE d." + Domain.PROPERTY_CODE + " = :domaincode" +
                " AND d." + Domain.PROPERTY_ACTIVE + " IS TRUE " +
                " ORDER BY " + Domain.PROPERTY_CAMPAIGN + " ASC";
        return findAll(query, DaoUtils.asArgsMap("domaincode", domainCode));
    }
    
    protected Set<String> getDomainInputUsedIds(String query, Map<String, Object> domainInputIds) {
        List<Object[]> iIds = findAll(query, domainInputIds);
        Set<String> result = new HashSet<>();
        for (Object[] iuIdTuple : iIds) {
            String inputId = (String) iuIdTuple[0];
            Long count = (Long) iuIdTuple[1];
            if (count != null && count > 0L) {
                result.add(inputId);
            }
        }
        return result;
    }
} //DomainTopiaDao<E extends Domain>
