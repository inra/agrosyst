package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import org.nuiton.topia.persistence.support.SqlFunction;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * The referential "RefActaSubstanceActive" is replaced by RefCompositionSubstancesActivesParNumeroAMM
 */
@Deprecated
public class RefActaSubstanceActiveTopiaDao extends AbstractRefActaSubstanceActiveTopiaDao<RefActaSubstanceActive> {

    @Deprecated
    public List<RefActaSubstanceActive> findDistinctSubtanceActive() {
        String query = newFromClause("SA") +
                " WHERE SA." + RefActaSubstanceActive.PROPERTY_ACTIVE + " = true" +
                " AND not exists( " + newFromClause("other") +
                "  WHERE upper(SA." + RefActaSubstanceActive.PROPERTY_NOM_COMMUN_SA + ")" +
                "   = upper(other." + RefActaSubstanceActive.PROPERTY_NOM_COMMUN_SA + ")" +
                "  AND SA != other )" +
                " ORDER BY SA." + RefActaSubstanceActive.PROPERTY_NOM_COMMUN_SA;
        return findAll(query);
    }

    @Deprecated
    public List<String> findDistinctSubtanceActiveNamesForProducts(Language language, Collection<String> productIds) {
        I18nDaoHelper i18nDaoHelper = I18nDaoHelper.withComplexI18nKey(RefActaSubstanceActive.PROPERTY_NOM_COMMUN_SA,
                language, TradRefIntrant.class, "SA");

        String selectedField = i18nDaoHelper.coalesceTranslation();
        String query = "SELECT distinct( " + selectedField + " ) " +
                newFromClause("SA") +
                i18nDaoHelper.leftJoinTranslation() +
                " WHERE SA." + RefActaSubstanceActive.PROPERTY_ACTIVE + " = true" +
                " AND SA." + RefActaSubstanceActive.PROPERTY_ID_PRODUIT + " IN (:productIds)" +
                " ORDER BY " + selectedField;

        return findAll(query, Map.of("productIds", productIds));

    }

    @Deprecated
    public List<RefActaSubstanceActive> getAmmForProductNameEquals(String productName) {
        String query = newFromClause() +
                " WHERE " + RefActaSubstanceActive.PROPERTY_ACTIVE + " = true" +
                " AND upper(" + RefActaSubstanceActive.PROPERTY_NOM_PRODUIT + ") = upper(:productName)";
        Map<String, Object> args = Map.of("productName", productName);
        return findAll(query, args);
    }

    @Deprecated
    public List<RefActaSubstanceActive> getAmmForProductNameLike(String productName) {
        String query = newFromClause() +
                " WHERE " + RefActaSubstanceActive.PROPERTY_ACTIVE + " = true" +
                " AND upper(" + RefActaSubstanceActive.PROPERTY_NOM_PRODUIT + ") like :productNameLike";
        Map<String, Object> args = Map.of("productNameLike", "%" + productName.toUpperCase().replace("'", "''") + "%");
        return findAll(query, args);
    }

    @Deprecated
    public List<String> findActiveSubstanceNamesForInterventionType(AgrosystInterventionType interventionType) {
        String sql = "select distinct(rasa." + RefActaSubstanceActive.PROPERTY_NOM_COMMUN_SA + ")" +
                " from " + RefActaTraitementsProduit.class.getSimpleName() + " ratp" +
                " inner join " + RefActaTraitementsProduitsCateg.class.getSimpleName() + " ratpc on ratp." + RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT + "  = ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ID_TRAITEMENT +
                " inner join " + RefActaSubstanceActive.class.getSimpleName() + " rasa on rasa." + RefActaSubstanceActive.PROPERTY_ID_PRODUIT + " = ratp." + RefActaTraitementsProduit.PROPERTY_ID_PRODUIT +
                " where ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTION + " = '%s'" +
                " and rasa." + RefActaSubstanceActive.PROPERTY_NOM_COMMUN_SA + " != ''" +
                " and rasa." + RefActaSubstanceActive.PROPERTY_ACTIVE + " is true" +
                " order by rasa." + RefActaSubstanceActive.PROPERTY_NOM_COMMUN_SA;

        String result = String.format(sql, interventionType.name());

        SqlFunction<ResultSet, String> transformer = resultSet -> resultSet.getString(1);

        List<String> substanceActiveNames = topiaSqlSupport.findMultipleResult(result, transformer);
        return substanceActiveNames;
    }

} //RefActaSubstanceActiveTopiaDao
