package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class RefFertiOrgaTopiaDao extends AbstractRefFertiOrgaTopiaDao<RefFertiOrga> {

    private static final String ALIAS = "orga";

    /**
     * return all categories and ProductTypes from the actives RefFertiMinUNIFA
     *
     * @return all categories and ProductTypes from the actives RefFertiMinUNIFA
     */
    public List<RefFertiOrga> findAll(Language language, boolean addInactives) {
        List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(language);
        String hql = "SELECT " + ALIAS + ", " + I18nDaoHelper.buildQueryTradObjectAlias(i18nDaoHelpers) +
                   newFromClause(ALIAS)
                   + I18nDaoHelper.buildQueryJoinTranslations(i18nDaoHelpers)
                   + "%s"
                   + " ORDER BY " + I18nDaoHelper.buildQuerySelectTranslationProperties(i18nDaoHelpers) + " ASC";

        String filterOnActivePart = " WHERE " + ALIAS + "." + RefFertiOrga.PROPERTY_ACTIVE + " IS true ";
        if (addInactives) {
            filterOnActivePart = "";
        }
        List<RefFertiOrga> result = new LinkedList<>();
        try (Stream<Object[]> stream = stream(String.format(hql, filterOnActivePart))) {
            stream.forEach((Object[] row) -> {
                RefFertiOrga refFertiOrga = (RefFertiOrga) row[0];
                refFertiOrga.setLibelle_Translated(I18nDaoHelper.tryGetTranslation(row[1]).orElse(refFertiOrga.getLibelle()));
                result.add(refFertiOrga);
            });
        }
        return result;
    }

    public ReferentialTranslationMap fillTranslations(Collection<String> topiaIdList, ReferentialTranslationMap translationMap) {
        List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(translationMap.getLanguage());
        I18nDaoHelper.fillRefEntitiesTranslations(this, ALIAS, i18nDaoHelpers, topiaIdList, translationMap);
        return translationMap;
    }

    protected List<I18nDaoHelper> getI18nDaoHelpers(Language language) {
        return List.of(
                I18nDaoHelper.withSimpleI18nKey(RefFertiOrga.PROPERTY_LIBELLE, language, TradRefIntrant.class, ALIAS)
        );
    }

    public List<String> finfAllKeysForActive() {
        String query = "SELECT orga." + RefFertiOrga.PROPERTY_IDTYPEEFFLUENT + " ";
        query += "      FROM " + getEntityClass().getName() + " orga ";
        query += "      WHERE orga." + RefFertiOrga.PROPERTY_ACTIVE + " = true ";
        Map<String, Object> args = DaoUtils.asArgsMap();
        List<String> result = findAll(query, args);
        return result;
    }

} //RefFertiOrgaTopiaDao
