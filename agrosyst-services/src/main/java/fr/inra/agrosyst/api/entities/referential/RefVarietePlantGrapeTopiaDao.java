package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RefVarietePlantGrapeTopiaDao extends AbstractRefVarietePlantGrapeTopiaDao<RefVarietePlantGrape> {

    public List<RefVarietePlantGrape> findAllVarietes(boolean isZMO, String filter, int maxResults) {
        List<RefVarietePlantGrape> result = new ArrayList<>();
        
        // La requete doit retourner tout le référentiel seulement si la liste d'id n'est pas vide
        // dans ce cas, il contient même seulement "ZMO"
        if (isZMO) {

            String query = "FROM " + RefVarietePlantGrape.class.getName() + " vpg ";
            query += " WHERE 1 = 1 ";
            Map<String, Object> args = new HashMap<>();

            // denomination
            query += DaoUtils.andAttributeLike("vpg", RefVarietePlantGrape.PROPERTY_VARIETE, args, filter);

            // active
            query += DaoUtils.andAttributeEquals("vpg", RefVarietePlantGrape.PROPERTY_ACTIVE, args, true);

            query += " ORDER BY vpg." + RefVarietePlantGrape.PROPERTY_VARIETE;

            List<RefVarietePlantGrape> entities = find(query, args, 0, maxResults - 1);
            if (entities != null) {
                result.addAll(entities);
            }
        }
        return result;
    }

    public List<RefVarietePlantGrape> findGraftSupport(String filter, String utilisation, int maxResults, String codeGnis) {
        String query = "FROM " + RefVarietePlantGrape.class.getName() + " vpg ";
        query += " WHERE 1 = 1 ";
        
        Map<String, Object> args = new HashMap<>();
        
        query += DaoUtils.andAttributeEquals("vpg", RefVarietePlantGrape.PROPERTY_UTILISATION, args, utilisation);
        if (StringUtils.isNotBlank(filter)) {
            query += DaoUtils.andAttributeLike("vpg", RefVarietePlantGrape.PROPERTY_VARIETE, args, filter);
        }
        
        if (StringUtils.isNotBlank(codeGnis)) {
            query += DaoUtils.andAttributeLike("vpg", RefVarietePlantGrape.PROPERTY_CODE_GNIS, args, codeGnis);
        }

        // active
        query += DaoUtils.andAttributeEquals("vpg", RefVarietePlantGrape.PROPERTY_ACTIVE, args, true);

        query += " ORDER BY vpg." + RefVarietePlantGrape.PROPERTY_VARIETE;
    
        maxResults = maxResults == DaoUtils.NO_PAGE_LIMIT ? DaoUtils.NO_PAGE_LIMIT : maxResults - 1;

        return find(query, args, 0, maxResults);
    }

    public RefVarietePlantGrape findAnyOrNullForCodeGnisEqualsIgnoreSpace(String codeGnis) {
        String query = "FROM " + RefVarietePlantGrape.class.getName() + " vpg WHERE 1 = 1 ";
        Map<String, Object> args = new HashMap<>();
        query += DaoUtils.andReplaceAttributeEquals("vpg", RefVarietePlantGrape.PROPERTY_CODE_GNIS, " ", "", args, codeGnis);
        return findAnyOrNull(query, args);
    }

} //RefVarietePlantGrapeTopiaDao<E extends RefVarietePlantGrape>
