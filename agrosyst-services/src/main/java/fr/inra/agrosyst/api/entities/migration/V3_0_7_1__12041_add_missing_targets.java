package fr.inra.agrosyst.api.entities.migration;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PhytoProductTarget;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class V3_0_7_1__12041_add_missing_targets extends BaseJavaMigration {

    /**
     * ROLLBACK
     * drop table _12041_add_missing_targets;
     * drop table _12041_script_date;
     *
      */

    private static final Log LOG = LogFactory.getLog(V3_0_7_1__12041_add_missing_targets.class);

    String scriptDate = "CREATE TABLE _12041_script_date AS (SELECT CURRENT_TIMESTAMP(3)::timestamp without time zone AS topiacreatedate)";

    String q0 = """
            CREATE TABLE _12041_add_missing_targets AS (
                select *
                from (
                  select ppt.topiaid as phytoproducttarget_id,
                  ppt.category,
                  ppt.codegroupeciblemaa,
                  ppt.target,
                  ppt.phytoproductinput,
                  ai.inputtype,
                  concat (ai.biologicalcontrolaction, ai.pesticidesspreadingaction, aa.practicedintervention, aa.effectiveintervention) as rootrelation,
                  ai.phytoproduct as refinput,
                  coalesce(ai.phytoproductunit, 'KG_HA') as usageunit,
                  concat(aa.practicedintervention, aa.effectiveintervention) as intervention,
                  ai.qtmin,
                  ai.qtavg,
                  ai.qtmed,
                  ai.qtmax
                  from _12041_phytoproducttarget_2_72 ppt
                  inner join abstractinput ai on ppt.phytoproductinput = ai.topiaid
                  left join abstractaction aa on aa.topiaid = ai.seedingaction
                  where ai.phytoproduct is not null
                  and ppt.target is not null
                  ) ppt272
                where not exists (
                  select 1
                  from (
                    select
                    ppt.category,
                    ppt.codegroupeciblemaa,
                    ppt.target,
                    concat(bpiu.biologicalcontrolaction, ppiu.pesticidesspreadingaction, aa.practicedintervention, aa.effectiveintervention) as rootrelation,
                    dpp.refinput as refinput,
                    dpp.usageunit as usageunit
                    from phytoproducttarget ppt
                    inner join abstractphytoproductinputusage appiu on appiu.topiaid = ppt.abstractphytoproductinputusage
                    inner join domainphytoproductinput dpp on dpp.topiaid = appiu.domainphytoproductinput
                    left join biologicalproductinputusage bpiu on bpiu.topiaid = ppt.abstractphytoproductinputusage
                    left join pesticideproductinputusage ppiu on ppiu.topiaid = ppt.abstractphytoproductinputusage
                    left join seedproductinputusage spiu on spiu.topiaid = ppt.abstractphytoproductinputusage
                    left join seedspeciesinputusage ssiu on ssiu.topiaid = spiu.seedspeciesinputusage
                    left join seedlotinputusage sliu on sliu.topiaid = ssiu.seedlotinputusage
                    left join abstractaction aa on aa.topiaid = sliu.seedingactionusage
                    ) ppt
                  where coalesce (ppt.codegroupeciblemaa, '') = coalesce (ppt272.codegroupeciblemaa, '')
                  and coalesce (ppt.category, '') = coalesce (ppt272 .category, '')
                  and ppt.target = ppt272.target
                  and ppt.rootrelation = ppt272.rootrelation
                  and ppt.refinput = ppt272.refinput
                  and coalesce(ppt.usageunit, 'KG_HA') = coalesce(ppt272.usageunit, 'KG_HA')
                )
            )
            """;

    String GET_ALL_MISSING_TARGETS = """
            SELECT
                  ppt.phytoproducttarget_id,
                  ppt.category,
                  ppt.codegroupeciblemaa,
                  ppt.target,
                  ppt.phytoproductinput,
                  ppt.inputtype,
                  ppt.rootrelation,
                  ppt.refinput,
                  ppt.usageunit,
                  ppt.intervention,
                  ppt.qtmin,
                  ppt.qtavg,
                  ppt.qtmed,
                  ppt.qtmax
            from _12041_add_missing_targets ppt
            inner join abstractinput ai on ppt.phytoproductinput = ai.topiaid
            inner join domainphytoproductinput dppi on dppi.refinput = ai.phytoproduct and coalesce (ai.phytoproductunit, 'KG_HA') = dppi.usageunit
            inner join abstractphytoproductinputusage appiu on appiu.domainphytoproductinput = dppi.topiaid
            inner join abstractinputusage aiu on aiu.topiaid = appiu.topiaid
            """;


    String INSERT_NEW_PHYTO_TARGETS = """
            INSERT INTO phytoproducttarget(topiaid, topiaversion, topiacreatedate, category, codegroupeciblemaa, target, abstractphytoproductinputusage, phytoproductinput)
            VALUES (
              ?,
              0,
              (SELECT topiacreatedate FROM _12041_script_date),
              ?,
              ?,
              ?,
              ?,
              ?
            )
            """;

    String UPDATE_PHYTO_TARGETS = """
            UPDATE phytoproducttarget SET abstractphytoproductinputusage = ? where topiaid = ?
            """;

    String GET_MISSING_BIOLOGICAL_USAGE_TARGET = """
            select distinct
                     ppt.phytoproducttarget_id,
                     ai.topiaid as ai_topiaid,
                     ai.topiadiscriminator,
                     ai.biologicalcontrolaction,
                     ai.pesticidesspreadingaction,
                     ai.seedingaction,
                     ai.phytoproduct,
                     ai.phytoproductunit ipui,
                     dppi.usageunit dpu,
                     ai.qtavg,
                     ai.qtmin,
                     ai.qtmed,
                     ai.qtmax,
                     aiu.qtavg as aiu_qtavg,
                     appiu.topiaid as usageId,
                     aiu.productname,
                     ai.inputtype,
                     appiu.domainphytoproductinput,
                     null as seedlotinputusage,
                     null as seedspeciesinputusage_id,
                     (select count(*) from _12041_phytoproducttarget_2_72 ppt where ppt.phytoproductinput = ai.topiaid) as nbtarget,
                     (select count(distinct ai0) from abstractinput ai0
                         where ai0.biologicalcontrolaction = bpiu.biologicalcontrolaction
                         and ai0.qtavg = ai.qtavg
                         and ai0.phytoproduct = ai.phytoproduct
                         and ai0.phytoproductunit = ai.phytoproductunit) as nb_duplicated_input
            from _12041_add_missing_targets ppt
            inner join abstractinput ai on ppt.phytoproductinput = ai.topiaid
            inner join domainphytoproductinput dppi on dppi.refinput = ai.phytoproduct and coalesce (ai.phytoproductunit, 'KG_HA') = dppi.usageunit
            inner join abstractphytoproductinputusage appiu on appiu.domainphytoproductinput = dppi.topiaid
            inner join biologicalproductinputusage bpiu on bpiu.topiaid = appiu.topiaid and bpiu.biologicalcontrolaction = ai.biologicalcontrolaction
            inner join abstractinputusage aiu on aiu.topiaid = appiu.topiaid
            """;

    //nb_duplicated_input
    String GET_MISSING_PESTICIDE_PRODUCT_USAGE_TARGET = """
            select distinct
                     ppt.phytoproducttarget_id,
                     ai.topiaid ai_topiaid,
                     ai.topiadiscriminator,
                     ai.biologicalcontrolaction,
                     ai.pesticidesspreadingaction,
                     ai.seedingaction,
                     ai.phytoproduct,
                     ai.phytoproductunit ipui,
                     dppi.usageunit dpu,
                     ai.qtavg,
                     ai.qtmin,
                     ai.qtmed,
                     ai.qtmax,
                     aiu.qtavg as aiu_qtavg,
                     appiu.topiaid as usageId,
                     aiu.productname,
                     ai.inputtype,
                     appiu.domainphytoproductinput,
                     null as seedlotinputusage,
                     null as seedspeciesinputusage_id,
                     (select count(*) from _12041_phytoproducttarget_2_72 ppt where ppt.phytoproductinput = ai.topiaid) as nbtarget,
                     (select count(distinct ai0) from abstractinput ai0
                         where ai0.pesticidesspreadingaction = ppiu.pesticidesspreadingaction
                         and ai0.qtavg = ai.qtavg
                         and ai0.phytoproduct = ai.phytoproduct
                         and ai0.phytoproductunit = ai.phytoproductunit) as nb_duplicated_input
            from _12041_add_missing_targets ppt
            inner join abstractinput ai on ppt.phytoproductinput = ai.topiaid
            inner join domainphytoproductinput dppi on dppi.refinput = ai.phytoproduct and coalesce (ai.phytoproductunit, 'KG_HA') = dppi.usageunit
            inner join abstractphytoproductinputusage appiu on appiu.domainphytoproductinput = dppi.topiaid
            inner join pesticideproductinputusage ppiu on ppiu.topiaid = appiu.topiaid and ppiu.pesticidesspreadingaction = ai.pesticidesspreadingaction
            inner join abstractinputusage aiu on aiu.topiaid = appiu.topiaid
            
            """;

    String GET_MISSING_SEEDING_PRODUCT_USAGE_TARGET = """
            select ppt.phytoproducttarget_id,
                   null as ai_topiaid,
                   null as topiadiscriminator,
                   null as biologicalcontrolaction,
                   null as pesticidesspreadingaction,
                   sliu.seedingactionusage as seedingaction,
                   ppt.refinput as phytoproduct,
                   ppt.usageunit ipui,
                   dppi.usageunit as dpu,
                   ppt.qtavg,
                   ppt.qtmin,
                   ppt.qtmed, ppt.qtmax,
                   null as aiu_qtavg,
                   spiu.topiaid as usageId,
                   null as productname,
                   ppt.inputtype,
                   dppi.topiaid as domainphytoproductinput,
                   sliu.topiaid as seedlotinputusage,
                   ssiu.topiaid as seedspeciesinputusage_id,
                   (select count(*) from _12041_phytoproducttarget_2_72 ppt inner join abstractinput ai on ppt.phytoproductinput = ai.topiaid and ai.seedingaction = sau.topiaid) as nbtarget,
                   (select count(distinct ai0) from abstractinput ai0
                       inner join abstractinput ai on ai.topiaid = ppt.phytoproductinput and ai.seedingaction = sau.topiaid
                       where ai0.seedingaction = sau.topiaid
                       and ai0.qtavg = ai.qtavg
                       and ai0.phytoproduct = ai.phytoproduct
                       and ai0.phytoproductunit = ai.phytoproductunit) as nb_duplicated_input
            from _12041_add_missing_targets ppt
            inner join abstractaction sau on (sau.practicedintervention = ppt.rootrelation or sau.effectiveintervention = ppt.rootrelation)
            inner join seedlotinputusage sliu on sliu.seedingactionusage = sau.topiaid
            inner join seedspeciesinputusage ssiu on ssiu.seedlotinputusage = sliu.topiaid
            inner join seedproductinputusage spiu on spiu.seedspeciesinputusage = ssiu.topiaid
            inner join abstractphytoproductinputusage appiu on appiu.topiaid = spiu.topiaid
            inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
            where dppi.refinput = ppt.refinput
            """;

    //                                  1                                       2         3       4     5       6       7        8
    String insertNewAPIU =  """
        INSERT INTO abstractinputusage (topiaid, topiaversion, topiacreatedate, productname, qtmin, qtavg, qtmed, qtmax, inputtype, code) values (
            ?,
            0,
            (select topiacreatedate from _12041_script_date),
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?
          )
        """;

    String insertNewPPIU =  """
        INSERT INTO abstractphytoproductinputusage (topiaid, domainphytoproductinput) values (
            ?,
            ?
          )
        """;

    String insertNewBPIU =  """
        INSERT INTO biologicalproductinputusage (topiaid, biologicalcontrolaction) values (
            ?,
            ?
          )
        """;

    String insertNewPestPIU =  """
        INSERT INTO pesticideproductinputusage (topiaid, pesticidesspreadingaction) values (
            ?,
            ?
          )
        """;

    String insertNewSPIU =  """
        INSERT INTO seedproductinputusage (topiaid, seedspeciesinputusage) values (
            ?,
            ?
          )
        """;
    @Override
    public void migrate(Context context) throws Exception {
        Connection connection = context.getConnection();

        try(Statement statement = connection.createStatement()) {
            statement.execute(scriptDate);
        }

        int nbPhytoproducttarget;
        try(Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) FROM phytoproducttarget");
            resultSet.next();
            nbPhytoproducttarget = resultSet.getInt(1);
        }
        LOG.info(nbPhytoproducttarget + " FOUND phytoproducttarget");

        Map<String, MissingPytoProductTarget> missingPytoProductTargetById = new HashMap<>();

        LOG.info("EXECUTE CREATE TABLE _12041_add_missing_targets ");
        try (Statement createMissingTargetsTable = connection.createStatement()) {
            createMissingTargetsTable.execute(q0);
        }

        LOG.info("EXECUTE GET_MISSING_PHYTO_TARGETS");
        try (Statement getMissingPhytoProductTargetStatement = connection.createStatement()) {
            ResultSet resultSet = getMissingPhytoProductTargetStatement.executeQuery(GET_ALL_MISSING_TARGETS);
            while (resultSet.next()) {
                InputType inputType = InputType.valueOf(resultSet.getString("inputtype"));
                String rootRelation = resultSet.getString("rootrelation");
                String seedingIntervention = null;
                String biologicalControlAction = null;
                String pesticidesSpreadingAction = null;
                if (InputType.SEMIS == inputType) {
                    seedingIntervention = rootRelation;
                } else if (InputType.LUTTE_BIOLOGIQUE == inputType) {
                    biologicalControlAction = rootRelation;
                } else if (InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES == inputType) {
                    pesticidesSpreadingAction = rootRelation;
                }

                MissingPytoProductTarget missingPytoProductTarget = new MissingPytoProductTarget();
                missingPytoProductTarget.setTopiaid(resultSet.getString("phytoproducttarget_id"));
                missingPytoProductTarget.setCategory(resultSet.getString("category"));
                missingPytoProductTarget.setCodegroupeciblemaa(resultSet.getString("codegroupeciblemaa"));
                missingPytoProductTarget.setTarget(resultSet.getString("target"));
                missingPytoProductTarget.setPhytoproductinput(resultSet.getString("phytoproductinput"));
                missingPytoProductTarget.setInputtype(resultSet.getString("inputtype"));
                missingPytoProductTarget.setSeedingIntervention(seedingIntervention);
                missingPytoProductTarget.setBiologicalcontrolaction(biologicalControlAction);
                missingPytoProductTarget.setPesticidesspreadingaction(pesticidesSpreadingAction);

                Double inputQtavg = (Double) resultSet.getObject("qtavg");
                missingPytoProductTarget.setQtavg(inputQtavg != null ? inputQtavg : 0);
                missingPytoProductTarget.setQtmin((Double) resultSet.getObject("qtmin"));
                missingPytoProductTarget.setQtmed((Double) resultSet.getObject("qtmed"));
                missingPytoProductTarget.setQtmax((Double) resultSet.getObject("qtmax"));
                missingPytoProductTargetById.put(missingPytoProductTarget.getTopiaid(), missingPytoProductTarget);
            }
        } catch (SQLException throwable) {
            LOG.error(throwable);
            throw new AgrosystTechnicalException("FAILED to get missing targets ", throwable);
        }

        try (Statement createIndexOn_12041_add_missing_targets = connection.createStatement()) {
            createIndexOn_12041_add_missing_targets.addBatch("CREATE INDEX if not exists _12041_phytoproducttarget_2_72_input_id on _12041_phytoproducttarget_2_72(phytoproductinput)");
            createIndexOn_12041_add_missing_targets.addBatch("CREATE INDEX if not exists _12041_add_missing_targets_rootrelation on _12041_add_missing_targets(rootrelation)");
            createIndexOn_12041_add_missing_targets.addBatch("CREATE INDEX if not exists _12041_add_missing_targets_idx ON  _12041_add_missing_targets(phytoproductinput)");
            createIndexOn_12041_add_missing_targets.executeBatch();
        }

        LOG.info(missingPytoProductTargetById.size() + " missing targets found");
        LOG.info("EXECUTE GET_MISSING_BIOLOGICAL_USAGE_TARGET");
        List<DetailedMissingPytoProductTarget> missingBiologicalProductTarget = new ArrayList<>();
        try (Statement loadMissingPhytoProductTargetTableStatement = connection.createStatement()) {
            ResultSet resultSet = loadMissingPhytoProductTargetTableStatement.executeQuery(GET_MISSING_BIOLOGICAL_USAGE_TARGET);
            bindToDetailedMissingPytoProductTarget(missingBiologicalProductTarget, resultSet, missingPytoProductTargetById);
        } catch (SQLException throwables) {
            LOG.error(throwables);
            throw new AgrosystTechnicalException("FAILED to load MISSING_BIOLOGICAL_USAGE_TARGET ", throwables);
        }

        LOG.info("EXECUTE GET_MISSING_PESTICIDE_PRODUCT_USAGE_TARGET");
        List<DetailedMissingPytoProductTarget> missingPesticideProductTarget = new ArrayList<>();
        try (Statement loadIntoMissingPhytoProductTargetTableStatement = connection.createStatement()) {
            ResultSet resultSet = loadIntoMissingPhytoProductTargetTableStatement.executeQuery(GET_MISSING_PESTICIDE_PRODUCT_USAGE_TARGET);
            bindToDetailedMissingPytoProductTarget(missingPesticideProductTarget, resultSet, missingPytoProductTargetById);
        } catch (SQLException throwables) {
            LOG.error(throwables);
            throw new AgrosystTechnicalException("FAILED to load MISSING_PESTICIDE_PRODUCT_USAGE_TARGET ", throwables);
        }

        LOG.info("EXECUTE GET_MISSING_SEEDING_PRODUCT_USAGE_TARGET");
        List<DetailedMissingPytoProductTarget> missingSeedingProductTarget = new ArrayList<>();
        try (Statement loadMissingPhytoProductTargetTableStatement = connection.createStatement()) {
            ResultSet resultSet = loadMissingPhytoProductTargetTableStatement.executeQuery(GET_MISSING_SEEDING_PRODUCT_USAGE_TARGET);
            bindToDetailedMissingPytoProductTarget(missingSeedingProductTarget, resultSet, missingPytoProductTargetById);
        } catch (SQLException throwables) {
            LOG.error(throwables);
            throw new AgrosystTechnicalException("FAILED to load MISSING_SEEDING_PRODUCT_USAGE_TARGET ", throwables);
        }

        // on ajoute les cibles manquantes aux usages déjà existants
        updateTargetToExistingUsage(
                connection,
                missingBiologicalProductTarget,
                missingPesticideProductTarget,
                missingSeedingProductTarget);

        // on ajoute les cibles manquantes sur de nouveaux usages pour garder la relation 1 pour 1 entre la cible et l'intrant
        insertMissingTargetsOnNewUsages(
                connection,
                missingBiologicalProductTarget,
                missingPesticideProductTarget,
                missingSeedingProductTarget
        );

        LOG.info("IT REMAIN " + missingBiologicalProductTarget.size() + " missing BiologicalProductTarget");

        LOG.info("IT REMAIN " + missingPesticideProductTarget.size() + " missing PesticideProductTarget");

        LOG.info("IT REMAIN " + missingSeedingProductTarget.size() + " missing SeedingProductTarget");

        int nbPhytoproducttarget1;
        try(Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) FROM phytoproducttarget");
            resultSet.next();
            nbPhytoproducttarget1 = resultSet.getInt(1);
        }
        LOG.info(nbPhytoproducttarget1 + " FOUND phytoproducttarget");
        LOG.info((nbPhytoproducttarget1 - nbPhytoproducttarget) + " phytoproducttarget added");
    }

    private void insertMissingTargetsOnNewUsages(
            Connection connection,
            List<DetailedMissingPytoProductTarget> missingBiologicalProductTarget,
            List<DetailedMissingPytoProductTarget> missingPesticideProductTarget,
            List<DetailedMissingPytoProductTarget> missingSeedingProductTarget) {

        List<DetailedMissingPytoProductTarget> missingBiologicalProductTargetsOnNewUsage = missingBiologicalProductTarget.stream()
                .filter(mppt -> mppt.nbDuplicatedInput > 1 && StringUtils.isNotBlank(mppt.iu_topiaid))
                .toList();
        missingBiologicalProductTarget.removeAll(missingBiologicalProductTargetsOnNewUsage);
        LOG.info("Mise à jour des cibles pour pointer vers de nouveaux usages d'intrants");
        LOG.info(missingBiologicalProductTargetsOnNewUsage.size() + " cibles mise à jours référençant un intrant de lutte biologique");
        insertTargetsOnNewUsages(
                connection,
                insertNewBPIU,
                missingBiologicalProductTargetsOnNewUsage,
                BiologicalProductInputUsage.class.getName());

        List<DetailedMissingPytoProductTarget> missingPesticideProductTargetOnNewUsage = missingPesticideProductTarget.stream()
                .filter(mppt -> mppt.nbDuplicatedInput > 1 && StringUtils.isNotBlank(mppt.iu_topiaid))
                .toList();
        missingPesticideProductTarget.removeAll(missingPesticideProductTargetOnNewUsage);

        LOG.info("Mise à jour des cibles pour pointer vers de nouveaux usages d'intrants");
        LOG.info(missingPesticideProductTargetOnNewUsage.size() + " cibles mise à jours référençant un intrant traitement chimique");
        insertTargetsOnNewUsages(
                connection,
                insertNewPestPIU,
                missingPesticideProductTargetOnNewUsage,
                PesticideProductInputUsage.class.getName());


        List<DetailedMissingPytoProductTarget> missingSeedingProductTargetOnNewUsage = missingSeedingProductTarget.stream()
                .filter(mppt -> mppt.nbDuplicatedInput > 1 && StringUtils.isNotBlank(mppt.iu_topiaid))
                .toList();
        LOG.info("Mise à jour des cibles pour pointer vers de nouveaux usages d'intrants");
        LOG.info(missingSeedingProductTargetOnNewUsage.size() + " cibles mise à jours référençant un intrant traitement de semence");

        missingSeedingProductTarget.removeAll(missingSeedingProductTargetOnNewUsage);
        insertTargetsOnNewUsages(
                connection,
                insertNewSPIU,
                missingSeedingProductTargetOnNewUsage,
                SeedProductInputUsage.class.getName());
    }

    private void insertTargetsOnNewUsages(Connection connection, String insertNewBPIU, List<DetailedMissingPytoProductTarget> missingTargetsOnNewUsage, String Name) {
        try (PreparedStatement insertIntoMissingPhytoProductTargetTableStatement = connection.prepareStatement(INSERT_NEW_PHYTO_TARGETS)) {
            try (PreparedStatement updateMissingPhytoProductTargetTableStatement = connection.prepareStatement(UPDATE_PHYTO_TARGETS)) {
                try (PreparedStatement insertNewAPIUStatement = connection.prepareStatement(insertNewAPIU)) {
                    try (PreparedStatement insertNewPPIUStatement = connection.prepareStatement(insertNewPPIU)) {
                        try (PreparedStatement insertNewBPIUStatement = connection.prepareStatement(insertNewBPIU)) {
                            insertNewInputUsages(
                                    missingTargetsOnNewUsage,
                                    insertNewAPIUStatement,
                                    insertNewPPIUStatement,
                                    insertNewBPIUStatement,
                                    updateMissingPhytoProductTargetTableStatement,
                                    insertIntoMissingPhytoProductTargetTableStatement,
                                    Name);
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOG.error(e);
            throw new AgrosystTechnicalException("FAILED create usage and targets ", e);
        }
    }

    private void updateTargetToExistingUsage(
            Connection connection,
            List<DetailedMissingPytoProductTarget> missingBiologicalProductTarget,
            List<DetailedMissingPytoProductTarget> missingPesticideProductTarget,
            List<DetailedMissingPytoProductTarget> missingSeedingProductTarget) {

        List<DetailedMissingPytoProductTarget> missingUniqueBiologicalProductTargetOnExistingUsage = missingBiologicalProductTarget.stream()
                .filter(mppt -> mppt.nbDuplicatedInput == 1 && StringUtils.isNotBlank(mppt.iu_topiaid))
                .toList();
        missingBiologicalProductTarget.removeAll(missingUniqueBiologicalProductTargetOnExistingUsage);

        List<DetailedMissingPytoProductTarget> missingUniquePesticideTargetOnExistingUsage = missingPesticideProductTarget.stream()
                .filter(mppt -> mppt.nbDuplicatedInput == 1 && StringUtils.isNotBlank(mppt.iu_topiaid))
                .toList();
        missingPesticideProductTarget.removeAll(missingUniquePesticideTargetOnExistingUsage);

        List<DetailedMissingPytoProductTarget> missingUniqueSeedingTargetOnExistingUsage = missingSeedingProductTarget.stream()
                .filter(mppt -> mppt.nbDuplicatedInput == 1 && StringUtils.isNotBlank(mppt.iu_topiaid))
                .toList();
        missingSeedingProductTarget.removeAll(missingUniqueSeedingTargetOnExistingUsage);

        //    1                                       2             3              4                 5                        6
        // topiaid, topiaversion, topiacreatedate, category, codegroupeciblemaa, target, abstractphytoproductinputusage, phytoproductinput)
        try (PreparedStatement updatePhytoProductTargetTableStatement = connection.prepareStatement(UPDATE_PHYTO_TARGETS)) {
            LOG.info("Ajout des cibles aux usages existants");
            LOG.info(missingUniqueBiologicalProductTargetOnExistingUsage.size() + " cibles mise à jours référençant un intrant de lutte biologique");
            for (DetailedMissingPytoProductTarget missingTarget : missingUniqueBiologicalProductTargetOnExistingUsage) {
                updatePhytoProductTarget(updatePhytoProductTargetTableStatement, missingTarget, null);
            }
            LOG.info("Ajout des cibles aux usages existants");
            LOG.info(missingUniquePesticideTargetOnExistingUsage.size() + " cibles mise à jours référençant un intrant de traitement chimique");
            for (DetailedMissingPytoProductTarget missingTarget : missingUniquePesticideTargetOnExistingUsage) {
                updatePhytoProductTarget(updatePhytoProductTargetTableStatement, missingTarget, null);
            }
            LOG.info("Ajout des cibles aux usages existants");
            LOG.info(missingUniqueSeedingTargetOnExistingUsage.size() + " cibles mise à jours référençant un intrant de traitement de semences");
            for (DetailedMissingPytoProductTarget missingTarget : missingUniqueSeedingTargetOnExistingUsage) {
                updatePhytoProductTarget(updatePhytoProductTargetTableStatement, missingTarget, null);
            }
            updatePhytoProductTargetTableStatement.executeBatch();
        } catch (Exception e) {
            LOG.error(e);
            throw new AgrosystTechnicalException("FAILED create targets ", e);
        }
    }

    private void insertNewInputUsages(
            List<DetailedMissingPytoProductTarget> missingTargetsOnNewUsage,
            PreparedStatement insertNewAPIUStatement,
            PreparedStatement insertNewPPIUStatement,
            PreparedStatement insertNewBPIUStatement,
            PreparedStatement updateMissingPhytoProductTargetTableStatement,
            PreparedStatement insertIntoMissingPhytoProductTargetTableStatement,
            String usageTopiaIdPrefix) throws SQLException {

        //      1          -              -             2         3       4     5       6       7        8
        // topiaid, topiaversion, topiacreatedate, productname, qtmin, qtavg, qtmed, qtmax, inputtype, code
        Set<String> targetIdUsed = new HashSet<>();
        for (DetailedMissingPytoProductTarget missingTarget : missingTargetsOnNewUsage) {

            String newInputUsageId = usageTopiaIdPrefix + "_" + UUID.randomUUID();
            insertNewAPIUStatement.setString(1, newInputUsageId);
            insertNewAPIUStatement.setString(2, missingTarget.getProductname());
            setSqlDoubleOrNull(3, missingTarget.getAi_qtmin(), insertNewAPIUStatement);
            insertNewAPIUStatement.setDouble(4, missingTarget.getAi_qtavg());
            setSqlDoubleOrNull(5, missingTarget.getAi_qtmed(), insertNewAPIUStatement);
            setSqlDoubleOrNull(6, missingTarget.getAi_qtmax(), insertNewAPIUStatement);
            insertNewAPIUStatement.setString(7, missingTarget.getInputtype());
            insertNewAPIUStatement.setString(8, UUID.randomUUID().toString());
            insertNewAPIUStatement.addBatch();

            insertNewPPIUStatement.setString(1, newInputUsageId);
            insertNewPPIUStatement.setString(2, missingTarget.domainphytoproductinput);
            insertNewPPIUStatement.addBatch();

            insertNewBPIUStatement.setString(1, newInputUsageId);
            insertNewBPIUStatement.setString(2, StringUtils.firstNonBlank(missingTarget.ai_biologicalcontrolaction, missingTarget.ai_pesticidesspreadingaction, missingTarget.seedspeciesinputusage_id));
            insertNewBPIUStatement.addBatch();

            if (targetIdUsed.contains(missingTarget.topiaid)) {
                insertMissingPhytoProductTarget(insertIntoMissingPhytoProductTargetTableStatement, missingTarget, newInputUsageId);
            } else {
                targetIdUsed.add(missingTarget.topiaid);
                updatePhytoProductTarget(updateMissingPhytoProductTargetTableStatement, missingTarget, newInputUsageId);
            }

        }
        insertNewAPIUStatement.executeBatch();
        insertNewPPIUStatement.executeBatch();
        insertNewBPIUStatement.executeBatch();
        updateMissingPhytoProductTargetTableStatement.executeBatch();
        insertIntoMissingPhytoProductTargetTableStatement.executeBatch();

    }

    protected void setSqlDoubleOrNull(int parameterIndex, Double value, PreparedStatement statement) throws SQLException {
        if (value != null) {
            statement.setDouble(parameterIndex, value);
        } else {
            statement.setNull(parameterIndex, Types.DOUBLE);
        }
    }

    private static void insertMissingPhytoProductTarget(PreparedStatement insertIntoMissingPhytoProductTargetTableStatement, DetailedMissingPytoProductTarget detailedMissingPytoProductTarget, String newInputUsageId) throws SQLException {
        String newTargetId = PhytoProductTarget.class.getName() + "_" + UUID.randomUUID();
        insertIntoMissingPhytoProductTargetTableStatement.setString(1, newTargetId);
        insertIntoMissingPhytoProductTargetTableStatement.setString(2, detailedMissingPytoProductTarget.category);
        insertIntoMissingPhytoProductTargetTableStatement.setString(3, detailedMissingPytoProductTarget.codegroupeciblemaa);
        insertIntoMissingPhytoProductTargetTableStatement.setString(4, detailedMissingPytoProductTarget.target);
        insertIntoMissingPhytoProductTargetTableStatement.setString(5, StringUtils.firstNonBlank(newInputUsageId, detailedMissingPytoProductTarget.iu_topiaid));
        insertIntoMissingPhytoProductTargetTableStatement.setString(6, detailedMissingPytoProductTarget.phytoproductinput);
    }

    private static void updatePhytoProductTarget(PreparedStatement updatePhytoProductTargetTableStatement, DetailedMissingPytoProductTarget detailedMissingPytoProductTarget, String newInputUsageId) throws SQLException {
        updatePhytoProductTargetTableStatement.setString(1, StringUtils.firstNonBlank(newInputUsageId, detailedMissingPytoProductTarget.iu_topiaid));
        updatePhytoProductTargetTableStatement.setString(2, detailedMissingPytoProductTarget.topiaid);
        updatePhytoProductTargetTableStatement.addBatch();
    }

    private void bindToDetailedMissingPytoProductTarget(
            List<DetailedMissingPytoProductTarget> missingPhytoProductTarget,
            ResultSet resultSet,
            Map<String, MissingPytoProductTarget> missingPytoProductTargetById) throws SQLException {

        while (resultSet.next()) {
//       1                       2                 3                     4                              5                        6               7                               8                   9        10        11        12        13                 14                       15            16          17                        18             19                 20
// ppt.topiaid, ai.topiaid ai_topiaid, ai.topiadiscriminator, ai.biologicalcontrolaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.phytoproduct, ai.phytoproductunit ipui, dppi.usageunit dpu, ai.qtavg, ai.qtmin, ai.qtmed, ai.qtmax, aiu.qtavg aiu_qtavg, appiu.topiaid as usageId, aiu.productname, ai.inputtype, domainphytoproductinput, seedlotinputusage, nbtarget
            String targetId = resultSet.getString("phytoproducttarget_id");
            MissingPytoProductTarget missingPytoProductTarget = missingPytoProductTargetById.get(targetId);
            if (missingPytoProductTarget == null) continue;

            Double inputQtavg = (Double) resultSet.getObject("qtavg");
            DetailedMissingPytoProductTarget detailedMissingPytoProductTarget = new DetailedMissingPytoProductTarget(
                    targetId,
                    missingPytoProductTarget.phytoproductinput,
                    missingPytoProductTarget.category,
                    missingPytoProductTarget.codegroupeciblemaa,
                    missingPytoProductTarget.target,
                    resultSet.getString("usageId"),
                    resultSet.getString("biologicalcontrolaction"),
                    resultSet.getString("pesticidesspreadingaction"),
                    resultSet.getString("seedingaction"),
                    resultSet.getString("phytoproduct"),
                    resultSet.getString("ipui"),
                    resultSet.getString("dpu"),
                    inputQtavg != null ? inputQtavg : 0,
                    (Double) resultSet.getObject("qtmin"),
                    (Double) resultSet.getObject("qtmed"),
                    (Double) resultSet.getObject("qtmax"),
                    resultSet.getDouble("aiu_qtavg"),
                    resultSet.getString("usageId"),
                    resultSet.getString("productname"),
                    resultSet.getString("inputtype"),
                    resultSet.getString("domainphytoproductinput"),
                    resultSet.getString("seedlotinputusage"),
                    resultSet.getString("seedspeciesinputusage_id"),
                    resultSet.getInt("nbtarget"),
                    resultSet.getInt("nb_duplicated_input")
            );
            missingPhytoProductTarget.add(detailedMissingPytoProductTarget);
        }
    }

    @Data
    @Getter
    @Setter
    public static class MissingPytoProductTarget {
        String topiaid;
        String phytoproductinput;
        String category;
        String codegroupeciblemaa;
        String target;
        String inputtype;
        String seedingIntervention;
        String biologicalcontrolaction;
        String pesticidesspreadingaction;
        double qtavg;
        Double qtmin;
        Double qtmed;
        Double qtmax;
    }

    @Data
    @AllArgsConstructor
    protected static class DetailedMissingPytoProductTarget {
        String topiaid;
        String phytoproductinput;
        String category;
        String codegroupeciblemaa;
        String target;
        String abstractphytoproductinputusage;
        String ai_biologicalcontrolaction;
        String ai_pesticidesspreadingaction;
        String ai_seedingaction;
        String ai_phytoproduct;
        String ai_phytoproductunit;
        String iu_phytoproductunit;
        double ai_qtavg;
        Double ai_qtmin;
        Double ai_qtmed;
        Double ai_qtmax;
        Double iu_qtavg;
        String iu_topiaid;
        String productname;
        String inputtype;
        String domainphytoproductinput;
        String seedlotinputusage;
        String seedspeciesinputusage_id;
        int nbtarget;
        int nbDuplicatedInput;
    }

}

