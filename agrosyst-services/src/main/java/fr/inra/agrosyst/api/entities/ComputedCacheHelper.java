package fr.inra.agrosyst.api.entities;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>Le but de ce helper est de fournir un moyen de stocker en base des listes d'identifiants afin de les inclure
 * ensuite dans d'autres requêtes. C'est souvent utile pour alléger la requête finale : on pré-calcule et on stocke une
 * liste d'identifiants puis on fait une jointure sur la table où sont stockés ces identifiants.</p>
 *
 * <p>Les listes d'identifiants sont stockées dans la table computed_cache. Cette table est constituée de 2 colonnes :
 * <ul>
 * <li> cachekey : représente l'identifiant de la liste ;</li>
 * <li> ref : représente 1 des entrées de la liste.</li>
 * </ul>
 * Chaque clé (cachekey) correspond donc à un groupe d'identifiants différent
 * </p>
 *
 * Par exemple, soit la liste d'identifiants {@code [A, B, C, D]} qu'on veut stocker. Le ComputedCacheHelper va générer
 * une clé (exemple : {@code cachekey=123}) et stocker en base les tuples suivants :
 * <pre>
 * cachekey | ref
 * ---------
 * 123 | A
 * 123 | B
 * 123 | C
 * 123 | D
 * </pre>
 * </p>
 *
 * <p>
 * Pour requêter on peut donc ensuite faire des requêtes du genre :
 * <pre>
 * FROM toto where id in (select ref from ComputedCache where cachekey='123')
 * </pre>
 * Le requêtage final est prévu pour du HQL mais il est tout à fait possible de le faire en SQL aussi.
 * </p>
 *
 * <p>La classe gère elle-même la purge de la table, il n'est donc pas nécessaire de s'en soucier.</p>
 *
 * <p>Pour des raisons de performances, les requêtes sont majoritairement faites en SQL.</p>
 *
 * @since 2.56
 * @author Arnaud Thimel (Code Lutin)
 * @see ComputedCache
 */
public class ComputedCacheHelper {

    protected static final Log log = LogFactory.getLog(ComputedCacheHelper.class);

    protected static final String ENTITY_NAME = ComputedCache.class.getName();
    protected static final String TABLE_NAME = "COMPUTED_CACHE";

    /**
     * Liste des identifiants à purger (car remplacés ou purge au démarrage)
     */
    protected static final Set<String> KEYS_TO_DROP = Collections.newSetFromMap(new ConcurrentHashMap<>());

    /**
     * Permet de faire le lien entre l'expiration dans le cache et la suppression en base
     */
    protected static final RemovalListener<String, String> REMOVAL_LISTENER = notification -> {
        String key = notification.getValue();

        if (log.isTraceEnabled()) {
            log.trace(String.format("Eviction of key %s because of: %s", notification.getCause(), key));
        }

        KEYS_TO_DROP.add(key);
    };

    /**
     * Une gross optimisation vient du fait qu'on conserve une trace in-memory des identifiants stockés pour ne pas les
     * restocker. C'est l'objet de ce cache : la clé du cache est la liste des identifiants (ref) et la valeur est la
     * clé du groupe (key) tel que stocké en base
     */
    protected static final Cache<String, String> COMPUTED_CACHE_IDS_TO_KEYS = CacheBuilder.newBuilder()
            .removalListener(REMOVAL_LISTENER)
            .expireAfterAccess(2, TimeUnit.HOURS)
            .build();

    protected final TopiaJpaSupport jpaSupport;
    protected final TopiaSqlSupport sqlSupport;

    public ComputedCacheHelper(TopiaJpaSupport jpaSupport, TopiaSqlSupport sqlSupport) {
        Preconditions.checkArgument(jpaSupport != null);
        Preconditions.checkArgument(sqlSupport != null);
        this.jpaSupport = jpaSupport;
        this.sqlSupport = sqlSupport;
    }

    @FunctionalInterface
    public interface SqlFunction<T, R> {
        R apply(T t) throws SQLException;
    }

    /**
     * Charge la liste des clés (key) depuis la base
     */
    protected List<String> listKeys() {
        String sql = " SELECT DISTINCT cachekey FROM " + TABLE_NAME;
        List<String> result = sqlSupport.findMultipleResult(sql, set -> set.getString(1));
        return result;
    }

    /**
     * Supprime le groupe d'identifiants en base donc la clé est {@code key}
     */
    protected void dropIdsForKey(String key) {

        if (log.isDebugEnabled()) {
            log.debug(String.format("Will drop ids for key: %s", key));
        }

        String sql = " DELETE FROM " + TABLE_NAME + " WHERE cachekey = ? ";
        sqlSupport.doSqlWork(connection -> {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, key);
            statement.execute();
        });
    }

    /**
     * Cherche dans la base s'il y a des groupes d'identifiants à supprimer et les supprime le cas échéant
     */
    protected void checkDroppableKeys() {

        if (COMPUTED_CACHE_IDS_TO_KEYS.size() == 0) {

            List<String> databaseKeys = listKeys();

            if (log.isDebugEnabled()) {
                log.debug(String.format("Will mark this keys to drop: %s", databaseKeys));
            }

            KEYS_TO_DROP.addAll(databaseKeys);
        }

        // On limite à 10 suppressions par appel
        if (log.isDebugEnabled() && !KEYS_TO_DROP.isEmpty()) {
            log.debug(String.format("%d keys to drop", KEYS_TO_DROP.size()));
        }

        for (String key : Iterables.limit(KEYS_TO_DROP, 10)) {
            dropIdsForKey(key);
            KEYS_TO_DROP.remove(key);
        }
    }

    /**
     * Vérifie la cohérence d'une clé et de la liste d'identifiants fourni.

     * Attention, la validité est approximative car elle se base sur le nombre d'éléments uniquement
     */
    protected boolean isValidKey(String key, Set<String> ids) {
        int expected = ids.size();
        long found = countByKey(key);
        boolean result = found == expected; // XXX AThimel 25/09/2020 rule may be improved
        return result;
    }

    /**
     * Compte le nombre d'identifiants du groupe dont la clé est {@code key}
     */
    protected long countByKey(String key) {
        String sql = " SELECT count(*) FROM " + TABLE_NAME + " WHERE cachekey = ? ";
        long count = sqlSupport.findSingleResult(
                connection -> {
                    PreparedStatement statement = connection.prepareStatement(sql);
                    statement.setString(1, key);
                    return statement;
                },
                set -> set.getLong(1));
        return count;
    }

    protected String writeIds(Set<String> ids) {

        final String key = UUID.randomUUID().toString();

        writeIdsAndKey(ids, key);

        return key;
    }

    /**
     * Procède à l'écriture des identifiants en base. Pour éviter les problèmes, les clés sont insérées par paquets
     *
     * @param ids la liste des identifiants
     * @param key la clé de groupe
     */
    private void writeIdsAndKey(Set<String> ids, String key) {

        long begin = System.currentTimeMillis();

        /*
         * Requête Postgres :
         * INSERT INTO computed_cache (key, ref) values
         * ('AZERTY', '111'),
         * ('AZERTY', '222'),
         * ('AZERTY', '333')
         */

        String header = " INSERT INTO " + TABLE_NAME + " (cachekey, ref) VALUES ";
        Function<String, String> function = input -> String.format("('%s', '%s')", key, input);

        for (List<String> subList : Iterables.partition(ids, 5000)) {
            String sql = header + subList.stream().map(function).collect(Collectors.joining(","));
            sqlSupport.executeSql(sql);
        }

        if (log.isDebugEnabled()) {
            long end = System.currentTimeMillis();
            log.debug(String.format("New key written in %dms matching %d ids: %s", end - begin, ids.size(), key));
        }

    }

    /**
     * Retrouve ou génère la clé de groupe à partir de la liste des identifiants
     */
    protected String getComputedCacheKey(Set<String> ids) {

        // À chaque appel on vérifie s'il est nécessaire de faire du ménage
        // NB : À faire AVANT d'ajouter une clé au cache
        checkDroppableKeys();

        Preconditions.checkArgument(ids != null && !ids.isEmpty());
        // On recopie dans une liste pour pouvoir trier et générer un ID
        List<String> idsAsList = Lists.newArrayList(ids);
        Collections.sort(idsAsList);
        String idsAsCacheKey = String.join("|", idsAsList);
        String key = COMPUTED_CACHE_IDS_TO_KEYS.getIfPresent(idsAsCacheKey);
        if (StringUtils.isNotBlank(key)) {
            if (!isValidKey(key, ids)) {
                dropIdsForKey(key);
                key = writeIds(ids);
                COMPUTED_CACHE_IDS_TO_KEYS.put(idsAsCacheKey, key);
            }
        } else {
            key = writeIds(ids);
            COMPUTED_CACHE_IDS_TO_KEYS.put(idsAsCacheKey, key);
        }

        return key;
    }

    /**
     * Méthode à utiliser pour le requêtage quand on veut faire un {@code t.id IN ([liste])}.

     * IMPORTANT : Pour maximiser l'efficacité de ce cache, il faut faire un commit après l'appel à cette méthode, sans
     * quoi les identifiants stockés en base seront rollbackés.
     *
     * @param aliasAndAttribute l'alias et attribut dont on peut tester le IN : {@code t.id}
     * @param args les arguments de la requête
     * @param ids la liste des identifiants : {@code [liste]}
     * @return la sous-requête à inclure dans la requête finale
     */
    public String andIdsIn(String aliasAndAttribute, Map<String, Object> args, Set<String> ids) {
        String result = andIdsInOrNotIn(aliasAndAttribute, args, ids, true);
        return result;
    }

    /**
     * Méthode à utiliser pour le requêtage quand on veut faire un {@code t.id NOT IN ([liste])}
     *
     * IMPORTANT : Pour maximiser l'efficacité de ce cache, il faut faire un commit après l'appel à cette méthode, sans
     * quoi les identifiants stockés en base seront rollbackés.
     *
     * @param aliasAndAttribute l'alias et attribut dont on peut tester le NOT IN : {@code t.id}
     * @param args les arguments de la requête
     * @param ids la liste des identifiants : {@code [liste]}
     * @return la sous-requête à inclure dans la requête finale
     */
    public String andIdsNotIn(String aliasAndAttribute, Map<String, Object> args, Set<String> ids) {
        String result = andIdsInOrNotIn(aliasAndAttribute, args, ids, false);
        return result;
    }

    /**
     * Génère la requête HQL en fonction du besoin. Cette méthode va s'adapter à la liste des identifiants
     * (potentiellement ne pas faire un {@code IN} mais plutôt un {@code ==})
     */
    protected String andIdsInOrNotIn(String aliasAndAttribute, Map<String, Object> args, Set<String> ids, boolean in) {

        final String result;

        if (CollectionUtils.isEmpty(ids)) {
            if (in) {
                result = " AND 1 = 0 "; // On fait en sorte que ça ne renvoie aucun résultat
            } else {
                result=""; // On fait en sorte que ça renvoie tous les résultats
            }
            return result;
        } else if (ids.size() == 1) {
            String id = Iterables.getOnlyElement(ids);
            String idAttributeName = DaoUtils.addQueryAttribute(args, "ref", id);
            if (in) {
                result = String.format(" AND %s = :%s ", aliasAndAttribute, idAttributeName);
            }
            else {
                result = String.format(" AND %s != :%s ", aliasAndAttribute, idAttributeName);
            }
        } else {
            String key = getComputedCacheKey(ids);
            String keyAttributeName = DaoUtils.addQueryAttribute(args, "key", key);
            result = String.format(
                    in ?
                            " AND EXISTS ( SELECT 1 FROM %s WHERE cachekey=:%s AND ref = %s ) " :
                            " AND NOT EXISTS ( SELECT 1 FROM %s WHERE cachekey=:%s AND ref = %s ) "
                    ,
                    ENTITY_NAME,
                    keyAttributeName,
                    aliasAndAttribute);
        }

        return result;
    }

}
