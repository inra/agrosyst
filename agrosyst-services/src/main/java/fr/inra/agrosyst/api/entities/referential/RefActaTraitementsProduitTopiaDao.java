package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.services.domain.inputStock.search.PhytoProductInputFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.nuiton.topia.persistence.support.SqlFunction;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author David Cossé
 */
public class RefActaTraitementsProduitTopiaDao extends AbstractRefActaTraitementsProduitTopiaDao<RefActaTraitementsProduit> {


    public static final String ALIAS = "ratp";

    public Map<String, String> findAllTreatmentCodesAndNames() {
        Map<String, String> treatmentCodesAndNames = new LinkedHashMap<>();
        String hql = "SELECT DISTINCT" +
                " atp." + RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT + "," + " atp." + RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT +
                " FROM " + getEntityClass().getName() + " atp" +
                " where atp." + RefActaTraitementsProduit.PROPERTY_ACTIVE + " = true" +
                " GROUP BY atp." + RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT + "," + " atp." + RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT +
                " ORDER BY atp." + RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT;

        Map<String, Object> map = new HashMap<>();
        List<Object[]> allByQuery = findAll(hql, map);
        for (Object[] treatment : allByQuery) {
            String key = (String) treatment[0];
            String value = (String) treatment[1];
            treatmentCodesAndNames.put(key, value);
        }
        return treatmentCodesAndNames;
    }

    public List<RefActaTraitementsProduit> findAllActiveTreatmentTypesForProductType(AgrosystInterventionType agrosystInterventionType, ProductType productType) {
        Map<String, Object> args = DaoUtils.asArgsMap(
                "productType", productType,
                "acta_tpc_active", true,
                "acta_tp_active", true,
                "agrosystInterventionType", agrosystInterventionType);

        String query = " FROM " + getEntityClass().getName() + " acta_tp " +
                " WHERE EXISTS (" +
                "   SELECT 1 FROM " + RefActaTraitementsProduitsCateg.class.getName() + " acta_tpc " +
                "   WHERE acta_tp." + RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT + " = acta_tpc." + RefActaTraitementsProduitsCateg.PROPERTY_ID_TRAITEMENT +
                "   AND acta_tpc." + RefActaTraitementsProduitsCateg.PROPERTY_TYPE_PRODUIT + " = :productType " +
                "   AND acta_tpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTIVE + " = :acta_tpc_active" +
                "   AND acta_tpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTION + " = :agrosystInterventionType" +
                "  )" +
                " AND acta_tp." + RefActaTraitementsProduit.PROPERTY_ACTIVE + " = :acta_tp_active " +
                " ORDER BY acta_tp." + RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT;

        List<RefActaTraitementsProduit> result = findAll(query, args);

        return result;
    }

    public List<String> finfAllKeysForActive() {
        String query = "SELECT DISTINCT CONCAT (acta_tp." + RefActaTraitementsProduit.PROPERTY_ID_PRODUIT + ", '_', acta_tp." + RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT + ")";
        query += "      FROM " + getEntityClass().getName() + " acta_tp ";
        query += "      WHERE acta_tp." + RefActaTraitementsProduit.PROPERTY_ACTIVE + " = true ";
        Map<String, Object> args = DaoUtils.asArgsMap();
        List<String> result = findAll(query, args);
        return result;
    }

    public List<Pair<String, String>> findResulPhytoProductNameAndAmm(
            Language language,
            AgrosystInterventionType agrosystInterventionType) {

        String sql = "select distinct coalesce(tri.traduction, ratp." + RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT + "), ratp." + RefActaTraitementsProduit.PROPERTY_CODE__AMM;
        sql += " from " + RefActaTraitementsProduit.class.getSimpleName() + " ratp ";
        sql += " inner join " + RefActaTraitementsProduitsCateg.class.getSimpleName() + " ratpc on ratp." + RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT + " = ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ID_TRAITEMENT;
        sql += " left join trad_ref_intrant tri on (to_simple_i18n_key(ratp." + RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT + ") = tri.tradkey and tri.lang = '%s')";
        sql += " where ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTION + " = '%s'";
        sql += " and ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTIVE + " is true";
        sql += " and ratp." + RefActaTraitementsProduit.PROPERTY_ACTIVE + " is true";
        sql += " order by coalesce(tri.traduction, ratp." + RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT + ")";

        String sqlQuery = String.format(sql, language.getTrigram(), agrosystInterventionType.name());

        SqlFunction<ResultSet, Pair<String, String>> transformer = resultSet -> {
            String nomProduit = resultSet.getString(1);
            String codeAmm = resultSet.getString(2);
            return Pair.of(nomProduit, codeAmm);
        };

        List<Pair<String, String>> nomProduitCodeAmm = topiaSqlSupport.findMultipleResult(sqlQuery, transformer);

        return nomProduitCodeAmm;
    }

    private static @NotNull String getFromQueryForLanguage(Language language) {
        return  String.format(" left join trad_ref_intrant tri on (to_simple_i18n_key(ratp." + RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT + ") = tri.tradkey and tri.lang = '%s')", language.getTrigram());
    }

    private static @NotNull String getConditionalFromQueryForLanguage(Language language, String nomProduit, String nomProduitTerm) {
        String fromLanguageProductName = "";
        if (StringUtils.isNoneEmpty(nomProduit) || StringUtils.isNoneEmpty(nomProduitTerm)) {
            fromLanguageProductName = getFromQueryForLanguage(language);
        }
        return fromLanguageProductName;
    }

    //ICI
    public List<DomainPhytoProductInputSearchResult> findDomainPhytoProductInputSearchResults(
            Language language,
            PhytoProductInputFilter filter) {

        String sql = "select distinct " +
                "coalesce(tri.traduction, ratp." + RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT + ")" +
                ", ratp." + RefActaTraitementsProduit.PROPERTY_CODE__AMM +
                ", ratp." + RefActaTraitementsProduit.PROPERTY_ID_PRODUIT +
                ", ratpc."+ RefActaTraitementsProduitsCateg.PROPERTY_TYPE_PRODUIT +
                " %s "; // selectQueryForActiveSubstance
        sql += " from " + RefActaTraitementsProduit.class.getSimpleName() + " ratp ";
        sql += " inner join " + RefActaTraitementsProduitsCateg.class.getSimpleName() + " ratpc on ratp." + RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT + " = ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ID_TRAITEMENT;
        sql += " %s "; // fromQueryForActiveSubstance
        sql += " %s "; // fromLanguageProductName
        sql += " where ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTION + " = '%s'"; // action
        sql += " and ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTIVE + " is true";
        sql += " and ratp." + RefActaTraitementsProduit.PROPERTY_ACTIVE + " is true";
        sql += " %s "; // queryBodyForNomProduit
        sql += " %s "; // queryBodyForActiveSubstance
        sql += " %s "; // queryBodyForTypeProduit
        sql += " %s "; // queryBodyForAmmCode
        sql += " %s "; // queryBodyForCountryFilter

        final String fromLanguageProductName = getFromQueryForLanguage(language);

        final String nomProduit = filter.getNomProduit();
        final String nomProduitTerm = filter.getNomProduitTerm();
        final String queryBodyForNomProduit = getQueryBodyForNomProduit(nomProduit, nomProduitTerm);

        final String ammCode = filter.getCodeAMM();
        final String queryBodyForAmmCode = getQueryBodyForAmmCode(ammCode);


        final String activeSubstances = filter.getActiveSubstance();
        final String activeSubstancesTerm = filter.getActiveSubstancesTerm();
        final String selectQueryForActiveSubstance = getSelectQueryForActiveSubstance(activeSubstancesTerm, activeSubstances, filter.isIpmworks());
        final String fromQueryForActiveSubstance = getFromSubstanceActiveQueryForActiveSubstance(activeSubstancesTerm, activeSubstances);
        final String queryBodyForActiveSubstance = getQueryBodyForActiveSubstance(activeSubstancesTerm, activeSubstances, ammCode, filter.isIpmworks());

        final String queryBodyForTypeProduit = getQueryBodyForTypeProduit(filter);

        final String queryBodyForCountryFilter = getQueryBodyForCountryFilter(filter);

        final String sqlQuery = String.format(
                sql,
                selectQueryForActiveSubstance,
                fromQueryForActiveSubstance,
                fromLanguageProductName,
                filter.getAction().name(),
                queryBodyForNomProduit,
                queryBodyForActiveSubstance,
                queryBodyForTypeProduit,
                queryBodyForAmmCode,
                queryBodyForCountryFilter);

        SqlFunction<ResultSet, DomainPhytoProductInputSearchResult> transformer = resultSet -> {
            String nomProduit_ = resultSet.getString(1);
            String codeAmm = resultSet.getString(2);
            String idProduit = resultSet.getString(3);
            ProductType productType = ProductType.valueOf(resultSet.getString(4));
            String activeSubstanceNomCommun = "";
            if (StringUtils.isNoneEmpty(activeSubstancesTerm) || StringUtils.isNoneEmpty(activeSubstances)) {
                activeSubstanceNomCommun = resultSet.getString(5);
            }
            DomainPhytoProductInputSearchResult res = new DomainPhytoProductInputSearchResult(nomProduit_, codeAmm, idProduit, productType, activeSubstanceNomCommun);
            return res;
        };

        List<DomainPhytoProductInputSearchResult> results = topiaSqlSupport.findMultipleResult(sqlQuery, transformer);

        return results;
    }

    public List<PhytoProductUnit> findRefActaDosageSpcPhytoProductUnitsForProducts(Language language, PhytoProductInputFilter filter) {

        String sql = "select distinct (radspc." + RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE + ")";
        sql += " from " + RefActaDosageSPC.class.getSimpleName() + " radspc ";
        sql += " inner join " + RefActaTraitementsProduit.class.getSimpleName() + " ratp on ratp." + RefActaTraitementsProduit.PROPERTY_ID_PRODUIT + " = radspc." + RefActaDosageSPC.PROPERTY_ID_PRODUIT;
        sql += " inner join " + RefActaTraitementsProduitsCateg.class.getSimpleName() + " ratpc on ratp." + RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT + " = ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ID_TRAITEMENT;
        sql += " %s "; // fromQueryForActiveSubstance
        sql += " %s "; // fromLanguageProductName
        sql += " where ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTION + " = '%s'"; // action
        sql += " and ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTIVE + " is true";
        sql += " and ratp." + RefActaTraitementsProduit.PROPERTY_ACTIVE + " is true";
        sql += " and radspc." + RefActaDosageSPC.PROPERTY_ACTIVE + " is true";
        sql += " and radspc." + RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE + " is not null";

        sql += " %s "; // queryBodyForNomProduit
        sql += " %s "; // queryBodyForActiveSubstance
        sql += " %s "; // type_produitFilterPart
        sql += " %s "; // ammCodeFilterPart
        sql += " %s "; // countryFilterPart

        final String nomProduit = filter.getNomProduit();
        final String nomProduitTerm = filter.getNomProduitTerm();

        final String fromLanguageProductName = getConditionalFromQueryForLanguage(language, nomProduit, nomProduitTerm);

        final String queryBodyForNomProduit = getQueryBodyForNomProduit(nomProduit, nomProduitTerm);

        final String ammCode = filter.getCodeAMM();
        String ammCodeFilterPart = getQueryBodyForAmmCode(ammCode);

        final String activeSubstances = filter.getActiveSubstance();
        final String activeSubstancesTerm = filter.getActiveSubstancesTerm();
        final String fromQueryForActiveSubstance = getFromSubstanceActiveQueryForActiveSubstance(activeSubstancesTerm, activeSubstances);
        final String queryBodyForActiveSubstance = getQueryBodyForActiveSubstance(activeSubstancesTerm, activeSubstances, ammCode, filter.isIpmworks());

        String type_produitFilterPart = getQueryBodyForTypeProduit(filter);

        String countryFilterPart = getQueryBodyForCountryFilter(filter);

        String sqlQuery = String.format(
                sql,
                fromLanguageProductName,
                fromQueryForActiveSubstance,
                filter.getAction().name(),
                queryBodyForNomProduit,
                queryBodyForActiveSubstance,
                type_produitFilterPart,
                ammCodeFilterPart,
                countryFilterPart);

        SqlFunction<ResultSet, PhytoProductUnit> transformer = resultSet -> {
            PhytoProductUnit dosageUnit = PhytoProductUnit.valueOf(resultSet.getString(1));
            return dosageUnit;
        };

        List<PhytoProductUnit> doseUnits = topiaSqlSupport.findMultipleResult(sqlQuery, transformer);

        return doseUnits;
    }

    public List<PhytoProductUnit> findMAADosesPhytoProductUnitsForProducts(Language language, PhytoProductInputFilter filter) {

        String sql = "select distinct (" + RefMAADosesRefParGroupeCible.PROPERTY_UNIT_DOSE_REF_MAA + ")";
        sql += " from " + RefMAADosesRefParGroupeCible.class.getSimpleName() + " radspc ";
        sql += " inner join " + RefActaTraitementsProduit.class.getSimpleName() + " ratp on ratp." + RefActaTraitementsProduit.PROPERTY_CODE__AMM + " = radspc." + RefMAADosesRefParGroupeCible.PROPERTY_CODE_AMM;
        sql += " inner join " + RefActaTraitementsProduitsCateg.class.getSimpleName() + " ratpc on ratp." + RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT + " = ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ID_TRAITEMENT;
        sql += " %s "; // fromQueryForActiveSubstance
        sql += " %s "; // fromLanguageProductName
        sql += " where ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTION + " = '%s'"; // action
        sql += " and ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTIVE + " is true";
        sql += " and ratp." + RefActaTraitementsProduit.PROPERTY_ACTIVE + " is true";
        sql += " and ratp." + RefActaTraitementsProduit.PROPERTY_CODE__AMM + " is not null";
        sql += " and radspc." + RefMAADosesRefParGroupeCible.PROPERTY_ACTIVE + " is true";
        sql += " and radspc." + RefMAADosesRefParGroupeCible.PROPERTY_UNIT_DOSE_REF_MAA + " is not null";

        sql += " and ratp." + RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA + " = radspc." + RefMAADosesRefParGroupeCible.PROPERTY_CODE_TRAITEMENT_MAA;
        sql += " %s "; // queryBodyForNomProduit
        sql += " %s "; // queryBodyForActiveSubstance
        sql += " %s "; // queryBodyForTypeProduit
        sql += " %s "; // ammCodeFilterPart
        sql += " %s "; // queryBodyForCountryFilter

        final String nomProduit = filter.getNomProduit();
        final String nomProduitTerm = filter.getNomProduitTerm();

        final String fromLanguageProductName = getFromLanguageProductName(language, nomProduit, nomProduitTerm);

        final String queryBodyForNomProduit = getQueryBodyForNomProduit(nomProduit, nomProduitTerm);

        final String ammCode = filter.getCodeAMM();
        final String ammCodeFilterPart = getQueryBodyForAmmCode(ammCode);

        final String activeSubstances = filter.getActiveSubstance();
        final String activeSubstancesTerm = filter.getActiveSubstancesTerm();
        final String fromQueryForActiveSubstance = getFromSubstanceActiveQueryForActiveSubstance(activeSubstancesTerm, activeSubstances);
        final String queryBodyForActiveSubstance = getQueryBodyForActiveSubstance(activeSubstancesTerm, activeSubstances, ammCode, filter.isIpmworks());
        final String queryBodyForTypeProduit = getQueryBodyForTypeProduit(filter);

        final String queryBodyForCountryFilter = getQueryBodyForCountryFilter(filter);

        final String sqlQuery = String.format(
                sql,
                fromQueryForActiveSubstance,
                fromLanguageProductName,
                filter.getAction().name(),
                queryBodyForNomProduit,
                queryBodyForActiveSubstance,
                queryBodyForTypeProduit,
                ammCodeFilterPart,
                queryBodyForCountryFilter);

        SqlFunction<ResultSet, PhytoProductUnit> transformer = resultSet -> {
            PhytoProductUnit dosageUnit = PhytoProductUnit.valueOf(resultSet.getString(1));
            return dosageUnit;
        };

        List<PhytoProductUnit> doseUnits = topiaSqlSupport.findMultipleResult(sqlQuery, transformer);

        return doseUnits;
    }

    public List<DomainPhytoProductInputDaoSearchResult> findAllByPhytoProductInputFilter(
            PhytoProductInputFilter filter) {
    
        Map<String, Object> args = new LinkedHashMap<>();
    
        AgrosystInterventionType action = filter.getAction();// must not be null
        
        String ammCode = filter.getCodeAMM();
        ProductType type_produit = filter.getTypeProduit();
        Boolean notEqualsTypeProduit = filter.getNotEqualsTypeProduit();
        String nom_produitTerm = filter.getNomProduitTerm();
        final String nom_produit = filter.getNomProduit();
        final String activeSubstancesTerm = filter.getActiveSubstancesTerm();
        final String activeSubstances = filter.getActiveSubstance();
        final String countryTopiaId = filter.getCountryTopiaId();
        final boolean isWithCodeAMM = filter.getIsWithAmmCode() != null && filter.getIsWithAmmCode();

        String selectPart = " SELECT acta_tpc, acta_tp %s ";
        String fromPart = "  FROM " + RefActaTraitementsProduitsCateg.class.getName() + " acta_tpc," + getEntityClass().getName() + " acta_tp %s";
        
        String commonFilterPart = " WHERE acta_tp." + RefActaTraitementsProduit.PROPERTY_ACTIVE + " IS TRUE " +
                " AND acta_tpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTIVE + " IS TRUE ";
        commonFilterPart += DaoUtils.andAttributeEquals("acta_tpc", RefActaTraitementsProduitsCateg.PROPERTY_ACTION, args, action);
        commonFilterPart += " AND acta_tp." + RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT + " = acta_tpc." + RefActaTraitementsProduitsCateg.PROPERTY_ID_TRAITEMENT;

        String activeSubstancesFilterPart = "";
        String type_produitFilterPart = "";
        String nom_produitFilterPart = "";
        String ammCodeFilterPart = "";
    
        if (StringUtils.isNoneEmpty(activeSubstancesTerm) || StringUtils.isNoneEmpty(activeSubstances)) {
    
            selectPart = String.format(selectPart,", acta_sa ");
            fromPart = String.format(fromPart, ", " + RefActaSubstanceActive.class.getName() + " acta_sa ");
    
            activeSubstancesFilterPart = " AND acta_sa." + RefActaSubstanceActive.PROPERTY_ACTIVE + " IS TRUE " +
                    " AND acta_sa." + RefActaSubstanceActive.PROPERTY_ID_PRODUIT + " = acta_tp." + RefActaTraitementsProduit.PROPERTY_ID_PRODUIT;

            if (StringUtils.isNoneEmpty(activeSubstancesTerm)) {
                activeSubstancesFilterPart += DaoUtils.andAttributeLike("acta_sa", RefActaSubstanceActive.PROPERTY_NOM_COMMUN_SA, args, activeSubstancesTerm);
            } else {
                activeSubstancesFilterPart += DaoUtils.andAttributeEquals("acta_sa", RefActaSubstanceActive.PROPERTY_NOM_COMMUN_SA, args, activeSubstances);
            }
    
            if (StringUtils.isNotEmpty(ammCode)) {
                activeSubstancesFilterPart += DaoUtils.andAttributeEquals("acta_sa", RefActaSubstanceActive.PROPERTY_CODE__AMM, args, ammCode);
            }
            
        } else {
            selectPart = String.format(selectPart,"");
            fromPart = String.format(fromPart, "");
        }

        // refs #11317
        // Si TSC cochée: Ajouter un bouton ‘+ traitement de semences chimique’. En cliquant dessus, les informations suivantes sont demandées:
        // RefActaTraitementsProduitsCateg.action = 'LUTTE_BIOLOGIQUE'; 'SEMIS'; 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
        //
        //    Type de produit:
        //
        //        Liste issue du référentiel RefActaTraitementsProduitsCateg, champ type_produit si RefActaTraitementsProduitsCateg.action = SEMIS et type_produit différent de SEEDINGS_OR_PLANTS_BIOLOGICAL_INNOCULATION
        //        Les libellés correspondent à la traduction présente dans la liste d’autorité ProductType
        //        Liste identique à ‘Type de produit’ à la saisie actuelle d’un intrant de traitement de semence dans les ITK si case ‘Traitement chimique des plants’ est cochée
        //    Nom du produit:
        //        Liste issue du référentiel RefActaTraitementsProduits, champ nom_produit
        //        Obligatoire: pour lien avec référence existante
        //    Ajouter champ Nom d’usage du produit (Champ de saisie libre, pré-rempli selon choix dans menu Nom du produit?)
        //
        //Si IB cochée: Ajouter un bouton ‘+ inoculation biologique’. En cliquant dessus, les informations suivantes sont demandées:
        //
        //    Type de produit:
        //        Liste issue du référentiel RefActaTraitementsProduitsCateg, champ type_produit si RefActaTraitementsProduitsCateg.action = SEMIS et type_produit = SEEDINGS_OR_PLANTS_BIOLOGICAL_INNOCULATION
        //        Les libellés correspondent à la traduction présente dans la liste d’autorité ProductType
        //        Liste identique à ‘Type de produit’ à la saisie actuelle d’un intrant de traitement de semence dans les ITK si case ‘Inoculation biologique des plants’ est cochée.
        //    Nom du produit:
        //        Liste issue du référentiel RefActaTraitementsProduits, champ nom_produit
        //        Obligatoire: pour lien avec référence existante
        //    Ajouter champ Nom d’usage du produit (Champ de saisie libre, pré-rempli selon choix dans menu Nom du produit?)
        if (type_produit != null) {
            if (notEqualsTypeProduit != null &&  notEqualsTypeProduit) {
                type_produitFilterPart = DaoUtils.andAttributeNotEquals("acta_tpc", RefActaTraitementsProduitsCateg.PROPERTY_TYPE_PRODUIT, args, type_produit);
            } else {
                type_produitFilterPart = DaoUtils.andAttributeEquals("acta_tpc", RefActaTraitementsProduitsCateg.PROPERTY_TYPE_PRODUIT, args, type_produit);
            }
        }
    
        if (StringUtils.isNotEmpty(nom_produitTerm)) {
            nom_produitFilterPart = DaoUtils.andAttributeLike("acta_tp", RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT, args, nom_produitTerm);
        }
        
        if (StringUtils.isNotEmpty(nom_produit)) {
            nom_produitFilterPart = DaoUtils.andAttributeEquals("acta_tp", RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT, args, nom_produit);
        }
        
        if (StringUtils.isNotEmpty(ammCode)) {
            ammCodeFilterPart = DaoUtils.andAttributeEquals("acta_tp", RefActaTraitementsProduit.PROPERTY_CODE__AMM, args, ammCode);
        }

        if (isWithCodeAMM) {
            commonFilterPart += " AND COALESCE (" + " acta_tp." + RefActaTraitementsProduit.PROPERTY_CODE__AMM + ", '') != '' " ;
        }

        if (StringUtils.isNotEmpty(countryTopiaId)) {
            commonFilterPart += DaoUtils.andAttributeEquals("acta_tp", RefActaTraitementsProduit.PROPERTY_REF_COUNTRY + "." + RefCountry.PROPERTY_TOPIA_ID, args, countryTopiaId);
        }
    
        String query = selectPart + fromPart + commonFilterPart + activeSubstancesFilterPart + type_produitFilterPart + nom_produitFilterPart + ammCodeFilterPart;
        
        List<Object[]> rows = findAll(query, args);
        
        List<DomainPhytoProductInputDaoSearchResult> result = new ArrayList<>();
        for (Object[] row : rows) {
            RefActaTraitementsProduitsCateg refActaTraitementsProduitsCateg = (RefActaTraitementsProduitsCateg)row[0];
            RefActaTraitementsProduit refActaTraitementsProduit = (RefActaTraitementsProduit)row[1];
            RefActaSubstanceActive refActaSubstanceActive = null;
            if (StringUtils.isNoneEmpty(activeSubstancesTerm) || StringUtils.isNoneEmpty(activeSubstances)) {
                refActaSubstanceActive = (RefActaSubstanceActive) row[2];
            }
            result.add(new DomainPhytoProductInputDaoSearchResult(refActaTraitementsProduitsCateg, refActaTraitementsProduit, refActaSubstanceActive));
        }
        return result;
    }

    /**
     * Produit sans AMM = Lutte biologique. Le nom a été changé dans les libellés pour "Produits sans AMM", mais c'est
     * bien la même chose que Lutte biologique. Il n'y a pas besoin de vérifier qu'il n'y a pas de code AMM, seulement
     * qu'on récupère les produits dont l'action est LUTTE_BIOLOGIQUE.
     * Cette requête doit récupérer spécifiquement les produits de lutte biologique.
     */
    public List<DomainPhytoProductInputDaoSearchResult> findAllBiologicalControlInputs(
            Language language,
            PhytoProductInputFilter filter) {

        Map<String, Object> args = new LinkedHashMap<>();

        AgrosystInterventionType action = AgrosystInterventionType.LUTTE_BIOLOGIQUE;

        ProductType type_produit = filter.getTypeProduit();
        Boolean notEqualsTypeProduit = filter.getNotEqualsTypeProduit();
        String nom_produitTerm = filter.getNomProduitTerm();
        final String nom_produit = filter.getNomProduit();
        final String countryTopiaId = filter.getCountryTopiaId();

        String selectPart = " SELECT acta_tpc, acta_tp, tri ";
        String fromPart = "  FROM " + RefActaTraitementsProduitsCateg.class.getName() + " acta_tpc," + getEntityClass().getName() + " acta_tp ";

        String commonFilterPart = " WHERE acta_tp." + RefActaTraitementsProduit.PROPERTY_ACTIVE + " IS TRUE " +
                " AND acta_tpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTIVE + " IS TRUE ";
        commonFilterPart += DaoUtils.andAttributeEquals("acta_tpc", RefActaTraitementsProduitsCateg.PROPERTY_ACTION, args, action);
        commonFilterPart += " AND acta_tp." + RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT + " = acta_tpc." + RefActaTraitementsProduitsCateg.PROPERTY_ID_TRAITEMENT;

        // load all translations as well if there is the product name given because of al product are not translated, so we can use the product name instead of translated name
        String transActaTpFromPart = " LEFT JOIN  "+ TradRefIntrant.class.getName() + " tri ON " +
                " to_simple_i18n_key(acta_tp."+ RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT +") = tri. " + TradRefIntrant.PROPERTY_TRADKEY +
                " AND tri." + TradRefIntrant.PROPERTY_LANG + " = '"+ language.getTrigram() + "' ";

        fromPart += transActaTpFromPart;

        String type_produitFilterPart = "";
        if (type_produit != null) {
            if (notEqualsTypeProduit != null &&  notEqualsTypeProduit) {
                type_produitFilterPart = DaoUtils.andAttributeNotEquals("acta_tpc", RefActaTraitementsProduitsCateg.PROPERTY_TYPE_PRODUIT, args, type_produit);
            } else {
                type_produitFilterPart = DaoUtils.andAttributeEquals("acta_tpc", RefActaTraitementsProduitsCateg.PROPERTY_TYPE_PRODUIT, args, type_produit);
            }
        }

        if (StringUtils.isNotEmpty(countryTopiaId)) {
            commonFilterPart += DaoUtils.andAttributeEquals("acta_tp", RefActaTraitementsProduit.PROPERTY_REF_COUNTRY + "." + RefCountry.PROPERTY_TOPIA_ID, args, countryTopiaId);
        }

        String query = selectPart + fromPart + commonFilterPart + type_produitFilterPart;

        List<Object[]> rows = findAll(query, args);

        List<DomainPhytoProductInputDaoSearchResult> result = new ArrayList<>();
        for (Object[] row : rows) {
            RefActaTraitementsProduitsCateg refActaTraitementsProduitsCateg = (RefActaTraitementsProduitsCateg)row[0];
            RefActaTraitementsProduit refActaTraitementsProduit = (RefActaTraitementsProduit)row[1];

            Object atpNomProduit = row.length > 2 ? row[2] : null;
            I18nDaoHelper.tryGetTranslation(atpNomProduit).ifPresent(refActaTraitementsProduit::setNom_produit);

            if(StringUtils.isNotEmpty(nom_produitTerm)) {
                if (refActaTraitementsProduit.getNom_produit().toLowerCase().contains(nom_produitTerm.toLowerCase())) {
                    result.add(new DomainPhytoProductInputDaoSearchResult(refActaTraitementsProduitsCateg, refActaTraitementsProduit, null));
                }
            } else if (StringUtils.isNotEmpty(nom_produit)) {
                if (refActaTraitementsProduit.getNom_produit().equalsIgnoreCase(nom_produit)) {
                    result.add(new DomainPhytoProductInputDaoSearchResult(refActaTraitementsProduitsCateg, refActaTraitementsProduit, null));
                    break;
                }
            } else {
                result.add(new DomainPhytoProductInputDaoSearchResult(refActaTraitementsProduitsCateg, refActaTraitementsProduit, null));
            }
        }
        return result;
    }

    public List<RefActaTraitementsProduit> findAllForTreatmentIds(Collection<Integer> traitementIds, Language language) {
        List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(language);

        Map<String, Object> args = new HashMap<>();
        String query = "SELECT " + ALIAS + ", " + I18nDaoHelper.buildQueryTradObjectAlias(i18nDaoHelpers) +
                newFromClause(ALIAS) +
                I18nDaoHelper.buildQueryJoinTranslations(i18nDaoHelpers) +
                " WHERE " + ALIAS + "." + RefActaTraitementsProduit.PROPERTY_ACTIVE + " =  true " +
                DaoUtils.andAttributeIn(ALIAS, RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, args, traitementIds) +
                " ORDER BY " + ALIAS + "." + RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT;

        List<Object[]> resultWithTranslation = findAll(query, args);
        return resultWithTranslation.stream()
                .map(objects -> {
                    RefActaTraitementsProduit refActaTraitementsProduit = (RefActaTraitementsProduit) objects[0];
                    I18nDaoHelper.tryGetTranslation(objects[1]).ifPresent(refActaTraitementsProduit::setEtat_usage);
                    return refActaTraitementsProduit;
                })
                .toList();
    }

    public ReferentialTranslationMap fillTranslations(Collection<String> topiaIdList, ReferentialTranslationMap translationMap) {
        List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(translationMap.getLanguage());
        I18nDaoHelper.fillRefEntitiesTranslations(this, ALIAS, i18nDaoHelpers, topiaIdList, translationMap);
        return translationMap;
    }

    protected List<I18nDaoHelper> getI18nDaoHelpers(Language language) {
        return List.of(
                I18nDaoHelper.withSimpleI18nKey(RefActaTraitementsProduit.PROPERTY_ETAT_USAGE, language, TradRefIntrant.class, ALIAS)
        );
    }

    protected static @NotNull String getQueryBodyForNomProduit(String nomProduit, String nomProduitTerm) {
        String nomProduitFilterQuery = "";
        if (StringUtils.isNoneEmpty(nomProduit) || StringUtils.isNoneEmpty(nomProduitTerm)) {
            String comparator = StringUtils.isNoneEmpty(nomProduit) ? "=" : "like";
            String nameSearch = StringUtils.isNoneEmpty(nomProduit) ? nomProduit : "%" + nomProduitTerm + "%";
            nomProduitFilterQuery = String.format(
                    "and coalesce(" +
                            "TRANSLATE(LOWER(tri.traduction),'áàâãäåāăąèééêëēĕėęěìíîïìĩīĭḩóôõöōŏőùúûüũūŭůäàáâãåæçćĉčöòóôõøüùúûßéèêëýñîìíïş','aaaaaaaaaeeeeeeeeeeiiiiiiiihooooooouuuuuuuuaaaaaaeccccoooooouuuuseeeeyniiiis')," +
                            "TRANSLATE(LOWER(ratp.%s),'áàâãäåāăąèééêëēĕėęěìíîïìĩīĭḩóôõöōŏőùúûüũūŭůäàáâãåæçćĉčöòóôõøüùúûßéèêëýñîìíïş','aaaaaaaaaeeeeeeeeeeiiiiiiiihooooooouuuuuuuuaaaaaaeccccoooooouuuuseeeeyniiiis')" +
                            ") %s TRANSLATE(LOWER('%s'),'áàâãäåāăąèééêëēĕėęěìíîïìĩīĭḩóôõöōŏőùúûüũūŭůäàáâãåæçćĉčöòóôõøüùúûßéèêëýñîìíïş','aaaaaaaaaeeeeeeeeeeiiiiiiiihooooooouuuuuuuuaaaaaaeccccoooooouuuuseeeeyniiiis')",
                    RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT, comparator, nameSearch.replace("'", "''")
            );
        }
        return nomProduitFilterQuery;
    }

    protected static @NotNull String getQueryBodyForAmmCode(String ammCode) {
        String ammCodeFilterPart = "";
        if (StringUtils.isNotEmpty(ammCode)) {
            ammCodeFilterPart += String.format(" and ratp.%s = '%s'", RefActaTraitementsProduit.PROPERTY_CODE__AMM, ammCode);
        }
        return ammCodeFilterPart;
    }

    protected static @NotNull String getSelectQueryForActiveSubstance(String activeSubstancesTerm, String activeSubstances, boolean ipmworks) {
        String selectActiveSubstancePart = "";
        if (StringUtils.isNoneEmpty(activeSubstancesTerm) || StringUtils.isNoneEmpty(activeSubstances)) {
            String selectVariantName = ipmworks ? RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_VARIANT_SA : RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NOM_SA;
            selectActiveSubstancePart = ", rasa." + selectVariantName;
        }
        return selectActiveSubstancePart;
    }

    private static @NotNull String getFromSubstanceActiveQueryForActiveSubstance(String activeSubstancesTerm, String activeSubstances) {
        String fromActiveSubstancePart = "";
        if (StringUtils.isNoneEmpty(activeSubstancesTerm) || StringUtils.isNoneEmpty(activeSubstances)) {
            fromActiveSubstancePart = " inner join " + RefCompositionSubstancesActivesParNumeroAMM.class.getSimpleName() +
                    " rasa on (rasa." + RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NUMERO__AMM + " = ratp."+ RefActaTraitementsProduit.PROPERTY_CODE__AMM +
                    " and ratp." + RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_ACTIVE + " is true)";
        }
        return fromActiveSubstancePart;
    }

    protected static @NotNull String getQueryBodyForActiveSubstance(String activeSubstancesTerm, String activeSubstances, String ammCode, boolean ipmworks) {
        String andActiveSubstancePart = "";
        if (StringUtils.isNoneEmpty(activeSubstancesTerm) || StringUtils.isNoneEmpty(activeSubstances)) {

            if (StringUtils.isNoneEmpty(activeSubstancesTerm)) {
                String electVariantName = ipmworks ? RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_VARIANT_SA : RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NOM_SA;
                String activeSubstancesTermLike = "'%" + activeSubstancesTerm.replace("'", "''") + "%'";
                andActiveSubstancePart = String.format(" and TRANSLATE(LOWER( rasa.%s),'áàâãäåāăąèééêëēĕėęěìíîïìĩīĭḩóôõöōŏőùúûüũūŭůäàáâãåæçćĉčöòóôõøüùúûßéèêëýñîìíïş','aaaaaaaaaeeeeeeeeeeiiiiiiiihooooooouuuuuuuuaaaaaaeccccoooooouuuuseeeeyniiiis') like TRANSLATE(LOWER(%s),'áàâãäåāăąèééêëēĕėęěìíîïìĩīĭḩóôõöōŏőùúûüũūŭůäàáâãåæçćĉčöòóôõøüùúûßéèêëýñîìíïş','aaaaaaaaaeeeeeeeeeeiiiiiiiihooooooouuuuuuuuaaaaaaeccccoooooouuuuseeeeyniiiis')",
                        electVariantName, activeSubstancesTermLike);
            } else {
                andActiveSubstancePart = String.format(" and rasa.%s = '%s'", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NOM_SA, activeSubstances);
            }

            if (StringUtils.isNotEmpty(ammCode)) {
                andActiveSubstancePart += String.format(" and rasa.%s = '%s'", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NUMERO__AMM, ammCode);
            }

            andActiveSubstancePart +=  String.format(" and rasa.%s is true ", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_ACTIVE);
        }
        return andActiveSubstancePart;
    }

    protected static @NotNull String getFromLanguageProductName(Language language, String nomProduit, String nomProduitTerm) {
        String fromLanguageProductName = "";
        if (StringUtils.isNoneEmpty(nomProduit) || StringUtils.isNoneEmpty(nomProduitTerm)) {
            fromLanguageProductName += String.format(" left join trad_ref_intrant tri on (to_simple_i18n_key(ratp.%s) = tri.tradkey and tri.lang = '%s')", RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT, language.getTrigram());
        }
        return fromLanguageProductName;
    }

    protected static @NotNull String getQueryBodyForCountryFilter(PhytoProductInputFilter filter) {
        String countryFilterPart = "";
        final String countryTopiaId = filter.getCountryTopiaId();
        if (StringUtils.isNotEmpty(countryTopiaId)) {
            countryFilterPart += String.format(" and ratp.%s = '%s'", RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, countryTopiaId);
        }
        return countryFilterPart;
    }

    protected static @NotNull String getQueryBodyForTypeProduit(PhytoProductInputFilter filter) {
        String type_produitFilterPart = "";
        Boolean notEqualsTypeProduit = filter.getNotEqualsTypeProduit();
        final ProductType type_produit = filter.getTypeProduit();
        if (type_produit != null) {
            String compare = (notEqualsTypeProduit != null &&  notEqualsTypeProduit) ? " != " : " = ";
            type_produitFilterPart += String.format(" and ratpc.%s %s '%s'", RefActaTraitementsProduitsCateg.PROPERTY_TYPE_PRODUIT, compare, type_produit);
        }
        return type_produitFilterPart;
    }

}
