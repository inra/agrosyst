package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class RefClonePlantGrapeTopiaDao extends AbstractRefClonePlantGrapeTopiaDao<RefClonePlantGrape> {

    public List<RefClonePlantGrape> findGraftClones(String filter, int codeVar, int maxResults) {
        List<RefClonePlantGrape> result = null;
        if (filter != null) {
            String query = "FROM " + RefClonePlantGrape.class.getName() + " CPG";
            query += " WHERE CPG." + RefClonePlantGrape.PROPERTY_ACTIVE + " = true";

            Map<String, Object> args = Maps.newLinkedHashMap();

            query += " AND CAST(CPG." + RefClonePlantGrape.PROPERTY_CODE_VAR + " as text) LIKE :codeVar";
            args.put("codeVar", String.valueOf(codeVar));

            if (StringUtils.isNotBlank(filter)){
                query += " AND CAST(CPG." + RefClonePlantGrape.PROPERTY_CODE_CLONE + " as text)  LIKE CONCAT('%',:filter,'%') ";
                args.put("filter", filter.replace("'", "''"));
            }

            query += " ORDER BY CPG." + RefClonePlantGrape.PROPERTY_CODE_VAR + ", CPG." + RefClonePlantGrape.PROPERTY_CODE_CLONE;

            result = find(query, args, 0, maxResults - 1);
        }
        return result;
    }

} //RefClonePlantGrapeTopiaDao<E extends RefClonePlantGrape>
