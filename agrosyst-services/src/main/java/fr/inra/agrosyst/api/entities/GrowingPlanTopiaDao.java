package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.SortedColumn;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.entities.security.UserRole;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.security.SecurityContext;
import fr.inra.agrosyst.services.security.SecurityHelper;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Custom dao methods on growingPlan.
 *
 * @author Eric Chatellier
 */
public class GrowingPlanTopiaDao extends AbstractGrowingPlanTopiaDao<GrowingPlan> {

    protected static final String PROPERTY_DOMAIN_NAME = GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_NAME;
    protected static final String PROPERTY_DOMAIN_CAMPAIGN = GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN;
    protected static final String PROPERTY_DOMAIN_ID = GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID;

    public Set<String> getFilteredGrowingPlanIds(GrowingPlanFilter filter, SecurityContext securityContext) {
        StringBuilder query = new StringBuilder("SELECT gp.topiaId FROM " + getEntityClass().getName() + " gp");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = new LinkedHashMap<>();
        applyFiltersOnQuery(filter, securityContext, query, args);
        return new HashSet<>(findAll(query.toString(), args));
    }

    /**
     * Search growingPlan matching user navigation context and custom additional {@code filter}.
     *
     * @param filter          custom filter
     * @return matching domains
     */
    public PaginationResult<GrowingPlan> getFilteredGrowingPlans(GrowingPlanFilter filter, SecurityContext securityContext) throws TopiaException {
        StringBuilder query = new StringBuilder("FROM " + GrowingPlan.class.getName() + " gp ");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = new LinkedHashMap<>();
    
        applyFiltersOnQuery(filter, securityContext, query, args);
    
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
    
        String filterOrderBy = getFilterOrderBy(filter);
    
        String queryString = query.toString();
        List<GrowingPlan> growingPlans = find(queryString + filterOrderBy, args, startIndex, endIndex);
        long totalCount = findUnique("SELECT count(*) " + queryString, args);
    
        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        return PaginationResult.of(growingPlans, totalCount, pager);
    }

    private void applyFiltersOnQuery(GrowingPlanFilter filter, SecurityContext securityContext, StringBuilder query, Map<String, Object> args) {
        // apply non null filter
        if (filter != null) {
            // selected entities
            query.append(DaoUtils.andAttributeIn("gp", TopiaEntity.PROPERTY_TOPIA_ID, args, DaoUtils.getRealIds(filter.getSelectedIds(), GrowingPlan.class)));
            // growing plan name
            query.append(DaoUtils.andAttributeLike("gp", GrowingPlan.PROPERTY_NAME, args, filter.getGrowingPlanName()));
            // active
            query.append(DaoUtils.andAttributeEquals("gp", GrowingPlan.PROPERTY_ACTIVE, args, filter.getActive()));

            if (filter.getActiveParents() != null && filter.getActiveParents()) {
                query.append(DaoUtils.andAttributeEquals("gp", GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_ACTIVE, args, filter.getActive()));
            }
            // type
            query.append(DaoUtils.andAttributeEquals("gp", GrowingPlan.PROPERTY_TYPE, args, filter.getTypeDephy()));
            // domain name
            query.append(DaoUtils.andAttributeLike("gp", PROPERTY_DOMAIN_NAME, args, filter.getDomainName()));
            // campaigns
            query.append(DaoUtils.andAttributeEquals("gp", PROPERTY_DOMAIN_CAMPAIGN, args, filter.getCampaign()));
            // responsable
            if (StringUtils.isNotBlank(filter.getMainContact())) {
                String[] researchParts = filter.getMainContact().split(" ");
                for (String researchPart : researchParts) {
                    String userFilterSubQuery = " AND gp." + GrowingPlan.PROPERTY_CODE + " IN " +
                            "(SELECT ur." + UserRole.PROPERTY_GROWING_PLAN_CODE + " FROM " + UserRole.class.getName() + " ur " +
                            " WHERE 1 = 1" +
                            DaoUtils.andAttributeLike("ur", UserRole.PROPERTY_AGROSYST_USER + "." + AgrosystUser.PROPERTY_FIRST_NAME, args, researchPart) +
                            DaoUtils.orAttributeLike("ur", UserRole.PROPERTY_AGROSYST_USER + "." + AgrosystUser.PROPERTY_LAST_NAME, args, researchPart) +
                            ")";
                    query.append(userFilterSubQuery);
                }
            }

            // Navigation context
            NavigationContext navigationContext = filter.getNavigationContext();
            if (navigationContext != null) {

                // campaigns
                Set<Integer> ncCampaigns = navigationContext.getCampaigns();
                query.append(DaoUtils.andAttributeInIfNotEmpty("gp", PROPERTY_DOMAIN_CAMPAIGN, args, ncCampaigns));

                // networks
                if (navigationContext.getNetworksCount() > 0) {
                    Set<String> growingPlanIds = networksToGrowingPlans(navigationContext.getNetworks());
                    query.append(DaoUtils.andAttributeIn("gp", GrowingPlan.PROPERTY_TOPIA_ID, args, growingPlanIds));
                }

                // domains
                Set<String> ncDomainIds = new HashSet<>(DaoUtils.getRealIds(navigationContext.getDomains(), Domain.class));
                query.append(DaoUtils.andAttributeInIfNotEmpty("gp", PROPERTY_DOMAIN_ID, args, ncDomainIds));

                // growingPlans
                Set<String> growingPlanIds = new HashSet<>(DaoUtils.getRealIds(navigationContext.getGrowingPlans(), GrowingPlan.class));
                if (!navigationContext.getGrowingSystems().isEmpty()) {
                    String hql = "SELECT" +
                            " gs." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_TOPIA_ID +
                            " FROM " + GrowingSystem.class.getName() + " gs" +
                            " WHERE gs." + GrowingSystem.PROPERTY_TOPIA_ID + " IN :gsIds";
                    List<String> growingPlansIds = findAll(hql, DaoUtils.asArgsMap("gsIds", DaoUtils.getRealIds(navigationContext.getGrowingSystems(), GrowingSystem.class)));
                    growingPlanIds.addAll(growingPlansIds);
                }
                query.append(DaoUtils.andAttributeInIfNotEmpty("gp", GrowingPlan.PROPERTY_TOPIA_ID, args, growingPlanIds));
    
            }
        }
    
        SecurityHelper.addGrowingPlanFilter(query, args, securityContext, "gp");
    }
    
    /**
     * do not manage SortedColumn.RESPONSIBLE
     *
     * @return - user selected ORDER BY or DEFAULT one
     */
    private String getFilterOrderBy(GrowingPlanFilter filter) {
        String result = null;
    
        if (filter != null && filter.getSortedColumn() != null) {
            SortedColumn sortedColumn = filter.getSortedColumn();
        
            String filterOrderBy = switch (sortedColumn) {
                case GROWING_PLAN -> " lower (gp." + GrowingPlan.PROPERTY_NAME + ")";
                case DOMAIN -> " lower (gp." + PROPERTY_DOMAIN_NAME + ")";
                case CAMPAIGN -> " gp." + PROPERTY_DOMAIN_CAMPAIGN;
                default -> null;
            };
            if (StringUtils.isNotBlank(filterOrderBy)) {
                filterOrderBy += " " + filter.getSortOrder().name();
                result = " ORDER BY " +
                        filterOrderBy +
                        ", gp." + TopiaEntity.PROPERTY_TOPIA_ID;
            }
            
        }
        
        if (result == null) {
            // default
            result = " ORDER BY " +
                    " gp." + PROPERTY_DOMAIN_CAMPAIGN + " DESC" +
                    ", gp." + PROPERTY_DOMAIN_NAME +
                    ", lower (gp." + GrowingPlan.PROPERTY_NAME + ")" +
                    ", gp." + TopiaEntity.PROPERTY_TOPIA_ID;
        }
        
        return result;
    }
    
    protected Set<String> networksToGrowingPlans(Set<String> networksIds) {
        return getProjectionHelper().networksToGrowingPlans(networksIds);
    }
    
    public LinkedHashSet<String> domainsToGrowingPlansCode(Set<String> domainCodes) {
        Preconditions.checkArgument(domainCodes != null && !domainCodes.isEmpty());
        
        String domainCodeProperty = GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CODE;

        String query = String.format("SELECT DISTINCT gp.%s FROM %s gp", GrowingPlan.PROPERTY_CODE, getEntityClass().getName());
        query += " WHERE 1 = 1";
        Map<String, Object> args = new LinkedHashMap<>();

        // domains
        query += DaoUtils.andAttributeIn("gp", domainCodeProperty, args, domainCodes);

        List<String> growingPlanCodes = findAll(query, args);

        return new LinkedHashSet<>(growingPlanCodes);
    }

    /**
     * Find all growingPlans using same growingPlan's duplication code.
     *
     * @return related growingPlan
     */
    public LinkedHashMap<Integer, String> findAllRelatedGrowingPlans(String code) {

        String query = "SELECT " + PROPERTY_DOMAIN_CAMPAIGN + ", " + GrowingPlan.PROPERTY_TOPIA_ID
                + " FROM " + getEntityClass().getName()
                + " WHERE " + GrowingPlan.PROPERTY_CODE + " = :code"
                + " ORDER BY " + PROPERTY_DOMAIN_CAMPAIGN + " DESC";

        List<Object[]> growingPlans = findAll(query, DaoUtils.asArgsMap("code", code));
        return DaoUtils.toRelatedMap(growingPlans);
    }

    public Set<String> getAllGrowingPlanCodes() {
        String query = "SELECT DISTINCT " + GrowingPlan.PROPERTY_CODE +
                " FROM " + getEntityClass().getName();
        List<String> list = findAll(query, DaoUtils.asArgsMap());
        return new HashSet<>(list);
    }

    public void validateGrowingPlan(String growingPlanId, LocalDateTime now) {
        Map<String, Object> args = DaoUtils.asArgsMap("growingPlanId", growingPlanId, "now", now);
        topiaJpaSupport.execute("UPDATE " + getEntityClass().getName() + " gp" +
                " SET gp.validated=true, gp.validationDate=:now, gp.updateDate=:now" +
                " WHERE gp." + GrowingPlan.PROPERTY_TOPIA_ID + "=:growingPlanId", args);

        // TODO AThimel 10/12/13 Validate all underlying entities

    }

} //GrowingPlanTopiaDao<E extends GrowingPlan>
