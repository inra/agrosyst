package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RefCountryTopiaDao extends AbstractRefCountryTopiaDao<RefCountry> {

    public Map<String, String> getCountryNamesByTopiaId(Language language) {
        I18nDaoHelper nomi18n = I18nDaoHelper.withSimpleI18nKey(RefCountry.PROPERTY_FRENCH_NAME, language, TradRefDivers.class, "rc");

        String query = "SELECT rc," + nomi18n.getAlias();
        query += " FROM " + getEntityClass().getName() + " rc ";
        query += nomi18n.leftJoinTranslation();
        query += " WHERE rc." + RefCountry.PROPERTY_ACTIVE + " = true";
        query += " ORDER BY " + nomi18n.coalesceTranslation();

        List<Object[]> resultWithTranslation = findAll(query);
        Map<String, String> result = resultWithTranslation.stream().collect(Collectors.toMap(
                objects -> ((RefCountry) objects[0]).getTopiaId(),
                objects -> I18nDaoHelper.tryGetTranslation(objects[1]).orElse(((RefCountry) objects[0]).getFrenchName()),
                (u, v) -> u,
                LinkedHashMap::new));

        return result;
    }

} //RefCountryTopiaDao
