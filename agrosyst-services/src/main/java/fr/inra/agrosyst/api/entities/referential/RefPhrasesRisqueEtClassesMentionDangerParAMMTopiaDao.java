package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RefPhrasesRisqueEtClassesMentionDangerParAMMTopiaDao extends AbstractRefPhrasesRisqueEtClassesMentionDangerParAMMTopiaDao<RefPhrasesRisqueEtClassesMentionDangerParAMM> {

    /**
     * Récupère la liste des codes AMM dans RefPhrasesRisqueEtClassesMentionDangerParAMM qui ne se trouvent pas
     * dans la table RefActaTraitementsProduit
     */
    public Set<String> findNonExistingCodeAmm() {
        final String query = """
            SELECT DISTINCT(r.%s) FROM %s r
            WHERE NOT EXISTS (
                SELECT 1 FROM %s r2 WHERE r2.%s = r.%s
            )
        """.formatted(
                RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_CODE__AMM,
                getEntityClass().getName(),
                RefActaTraitementsProduit.class.getName(),
                RefActaTraitementsProduit.PROPERTY_CODE__AMM,
                RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_CODE__AMM
        );

        return new HashSet<>(this.findAll(query));
    }

    public MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> findRefPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM(Domain domain) {

        MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> result = new HashSetValuedHashMap<>();
        final String query =
                "FROM " + getEntityClass().getName() + " r " +
                        " INNER JOIN " + RefActaTraitementsProduit.class.getName() + " ratp on ratp." + RefActaTraitementsProduit.PROPERTY_CODE__AMM + " = r." + RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_CODE__AMM +
                        " INNER JOIN " + DomainPhytoProductInput.class.getName() + " dppi on dppi." + DomainPhytoProductInput.PROPERTY_REF_INPUT + " = ratp " +
                        " WHERE dppi." + DomainPhytoProductInput.PROPERTY_DOMAIN + " = :domain " +
                        " AND r." + RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_ACTIVE + " IS TRUE ";

        List<RefPhrasesRisqueEtClassesMentionDangerParAMM> refPhrasesRisqueEtClassesMentionDangerParAMMS = this.findAll(query, DaoUtils.asArgsMap("domain", domain));
        refPhrasesRisqueEtClassesMentionDangerParAMMS.forEach(
                r -> result.put(r.getCode_AMM(), r)
        );
        return result;
    }
} //RefPhrasesRisqueEtClassesMentionDangerParAMMTopiaDao
