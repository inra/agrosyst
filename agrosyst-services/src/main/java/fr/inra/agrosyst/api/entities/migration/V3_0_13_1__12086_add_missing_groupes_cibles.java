package fr.inra.agrosyst.api.entities.migration;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.PhytoProductTarget;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class V3_0_13_1__12086_add_missing_groupes_cibles extends BaseJavaMigration {

    /**
     * ROLLBACK
     * drop table _12086_add_missing_targets;
     * drop table _12086_script_date;
     *
      */

    private static final Log LOG = LogFactory.getLog(V3_0_13_1__12086_add_missing_groupes_cibles.class);

    String SCRIPT_DATE = "CREATE TABLE _12086_script_date AS (SELECT CURRENT_TIMESTAMP(3)::timestamp without time zone AS topiacreatedate)";

    String CREATE_MISSING_TARGETS_TABLE = """
            create table _12086_missing_targets as (
                select
                  distinct ppt_272.topiaid ppt_272_topiaid,
                  ppt_272.topiaversion ppt_272_topiaversion,
                  ppt_272.category ppt_272_category,
                  ppt_272.codegroupeciblemaa ppt_272_codegroupeciblemaa,
                  aiu.topiaid aiu_topiaid,
                  ppt_272.phytoproductinput ppt_272_phytoproductinput
                from _12041_phytoproducttarget_2_72 ppt_272
                inner join _12084_input_to_usage input_to_usage on input_to_usage.id_input = ppt_272.phytoproductinput
                inner join abstractphytoproductinputusage aiu on aiu.topiaid = input_to_usage.id_usage
                where not exists (
                  select 1 from phytoproducttarget ppt_30
                  where ppt_30.topiaid = ppt_272.topiaid
                )
                and not exists  (
                  select 1 from phytoproducttarget ppt_30
                  where ppt_30.abstractphytoproductinputusage = input_to_usage.id_usage
                  and ppt_272.category = ppt_30.category
                  and ppt_272.codegroupeciblemaa = ppt_30.codegroupeciblemaa
                  and ppt_272.target = ppt_30.target
                )
            )
            """;

    String GET_MISSING_TARGETS = """
            SELECT distinct ppt_272_topiaid, ppt_272_topiaversion, ppt_272_category, ppt_272_codegroupeciblemaa, aiu_topiaid, ppt_272_phytoproductinput
            FROM _12086_missing_targets
            """;

    String INSERT_MISSING_TARGET = """
            INSERT INTO phytoproducttarget(topiaid, topiaversion, topiacreatedate, category, codegroupeciblemaa, abstractphytoproductinputusage, phytoproductinput)
            VALUES (
              ?,
              0,
              (SELECT topiacreatedate FROM _12086_script_date),
              ?,
              ?,
              ?,
              ?
            )
            """;

    @Override
    public void migrate(Context context) throws Exception {
        Connection connection = context.getConnection();

        try (Statement getMissingTargets = connection.createStatement()) {
            getMissingTargets.execute(SCRIPT_DATE);
        }

        try (Statement getMissingTargets = connection.createStatement()) {
            getMissingTargets.execute(CREATE_MISSING_TARGETS_TABLE);
        }

        List<MissingTarget> missingTargets = new ArrayList<>();
        try (Statement getMissingTargets = connection.createStatement()) {
            ResultSet resultSet = getMissingTargets.executeQuery(GET_MISSING_TARGETS);
            while (resultSet.next()) {
                MissingTarget mt = new MissingTarget(
                        resultSet.getString(1),
                        resultSet.getInt(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6)
                );
                missingTargets.add(mt);
            }
        }

        LOG.info(missingTargets.size() + " cibles manquantes trouvée voire _12086_missing_targets");

        Set<String> duplicateSeedingTargetIds = new HashSet<>();
        try (PreparedStatement insertIntoMissingPhytoProductTargetTableStatement = connection.prepareStatement(INSERT_MISSING_TARGET)) {
            for (MissingTarget missingTarget : missingTargets) {
                String targetId = duplicateSeedingTargetIds.contains(missingTarget.getPpt_272_topiaid()) ? PhytoProductTarget.class.getName() + "_" + UUID.randomUUID() : missingTarget.getPpt_272_topiaid();
                insertMissingPhytoProductTarget(insertIntoMissingPhytoProductTargetTableStatement, targetId, missingTarget);
                duplicateSeedingTargetIds.add(targetId);
            }
        }

        LOG.info(duplicateSeedingTargetIds.size() + " cibles manquantes créées");
    }

    private static void insertMissingPhytoProductTarget(
            PreparedStatement insertIntoMissingPhytoProductTargetTableStatement,
            String targetId,
            MissingTarget missingTarget) throws SQLException {
        insertIntoMissingPhytoProductTargetTableStatement.setString(1, targetId);
        insertIntoMissingPhytoProductTargetTableStatement.setString(2, missingTarget.ppt_272_category);
        insertIntoMissingPhytoProductTargetTableStatement.setString(3, missingTarget.ppt_272_codegroupeciblemaa);
        insertIntoMissingPhytoProductTargetTableStatement.setString(4, missingTarget.aiu_topiaid);
        insertIntoMissingPhytoProductTargetTableStatement.setString(5, missingTarget.ppt_272_phytoproductinput);
        insertIntoMissingPhytoProductTargetTableStatement.execute();
    }

    @Data
    @AllArgsConstructor
    protected static class MissingTarget {
        String ppt_272_topiaid;
        int ppt_272_topiaversion;
        String ppt_272_category;
        String ppt_272_codegroupeciblemaa;
        String aiu_topiaid;
        String ppt_272_phytoproductinput;
    }

}

