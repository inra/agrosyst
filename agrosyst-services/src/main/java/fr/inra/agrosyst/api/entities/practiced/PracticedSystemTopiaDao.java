package fr.inra.agrosyst.api.entities.practiced;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.SortedColumn;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.security.SecurityContext;
import fr.inra.agrosyst.services.security.SecurityHelper;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Practiced system dao custom methods.
 * 
 * @author Eric Chatellier
 */
public class PracticedSystemTopiaDao extends AbstractPracticedSystemTopiaDao<PracticedSystem> {

    protected static final String PROPERTY_GROWING_SYSTEM_ID = PracticedSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID;
    protected static final String PROPERTY_GROWING_SYSTEM_NAME = PracticedSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_NAME;

    protected static final String PROPERTY_GROWING_SYSTEM_GROWING_PLAN = PracticedSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN;
    protected static final String PROPERTY_GROWING_SYSTEM_GROWING_PLAN_ID = PROPERTY_GROWING_SYSTEM_GROWING_PLAN + "." + GrowingPlan.PROPERTY_TOPIA_ID;
    protected static final String PROPERTY_GROWING_SYSTEM_GROWING_PLAN_NAME = PROPERTY_GROWING_SYSTEM_GROWING_PLAN + "." + GrowingPlan.PROPERTY_NAME;

    protected static final String PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN = PROPERTY_GROWING_SYSTEM_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN;
    protected static final String PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_ID = PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID;
    protected static final String PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_NAME = PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_NAME;
    protected static final String PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_CAMPAIGN = PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN;
    
    public PaginationResult<PracticedSystem> getFilteredPracticedSystems(PracticedSystemFilter filter, SecurityContext securityContext) {
        StringBuilder query = new StringBuilder(" FROM " + getEntityClass().getName() + " ps ");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = new LinkedHashMap<>();
        applyFiltersOnQuery(filter, securityContext, query, args);
        
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
        
        String orderByClause = getFilterOrderBy(filter);
        List<PracticedSystem> practicedSystems = find(query + orderByClause, args, startIndex, endIndex);
        long totalCount = findUnique("SELECT COUNT(*) " + query, args);
        
        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        return PaginationResult.of(practicedSystems, totalCount, pager);
    }
    
    private String getFilterOrderBy(PracticedSystemFilter filter) {
        String result = null;
        
        if (filter != null && filter.getSortedColumn() != null) {
            SortedColumn sortedColumn = filter.getSortedColumn();
            String filterOrderBy = switch (sortedColumn) {
                case PRACTICED_SYSTEM -> " lower (ps." + PracticedSystem.PROPERTY_NAME + ")";
                case GROWING_SYSTEM -> " lower (ps." + PROPERTY_GROWING_SYSTEM_NAME + ")";
                case GROWING_PLAN -> " lower (ps." + PROPERTY_GROWING_SYSTEM_GROWING_PLAN_NAME + ")";
                case DOMAIN -> " lower (ps." + PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_NAME + ")";
                case CAMPAIGN -> " CAST(SUBSTRING(" +
                        "ps." + PracticedSystem.PROPERTY_CAMPAIGNS +
                        ", LENGTH(ps." + PracticedSystem.PROPERTY_CAMPAIGNS + ") - 3" +
                        ", LENGTH(ps." + PracticedSystem.PROPERTY_CAMPAIGNS + ")" +
                        ") as integer) ";
                default -> null;
            };
            if (StringUtils.isNotBlank(filterOrderBy)) {
                filterOrderBy += " " + filter.getSortOrder().name();
                result = " ORDER BY " +
                        filterOrderBy +
                        ", ps." + TopiaEntity.PROPERTY_TOPIA_ID;
            }
            
        }
        
        if (result == null) {
            // default
            result = " ORDER BY" +
                    " CAST(SUBSTRING(" +
                    "ps." + PracticedSystem.PROPERTY_CAMPAIGNS +
                    ", LENGTH(ps." + PracticedSystem.PROPERTY_CAMPAIGNS + ") - 3" +
                    ", LENGTH(ps." + PracticedSystem.PROPERTY_CAMPAIGNS + ")" +
                    ") as integer) DESC" +
                    ", ps." + PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_NAME +
                    ", ps." + PROPERTY_GROWING_SYSTEM_GROWING_PLAN_NAME +
                    ", ps." + PROPERTY_GROWING_SYSTEM_NAME +
                    ", ps." + PracticedSystem.PROPERTY_NAME +
                    ", ps." + PracticedSystem.PROPERTY_TOPIA_ID;
        }
        
        return result;
    }
    
    public Set<String> getFilteredPracticedSystemIds(PracticedSystemFilter filter, SecurityContext securityContext) {
        StringBuilder query = new StringBuilder("SELECT ps.topiaId FROM " + getEntityClass().getName() + " ps ");
        query .append(" WHERE 1 = 1");
        Map<String, Object> args = new HashMap<>();
        applyFiltersOnQuery(filter, securityContext, query, args);
        return new HashSet<>(findAll(query.toString(), args));
    }

    private void applyFiltersOnQuery(PracticedSystemFilter filter, SecurityContext securityContext, StringBuilder query, Map<String, Object> args) {
        // apply non null filter
        if (filter != null) {
            // PracticedSystem name
            query.append(DaoUtils.andAttributeLike("ps", PracticedSystem.PROPERTY_NAME, args, filter.getPracticedSystemName()));
            // Growing System
            query.append(DaoUtils.andAttributeLike("ps", PROPERTY_GROWING_SYSTEM_NAME, args, filter.getGrowingSystemName()));
            // PracticedSystem campaigns
            query.append(DaoUtils.andAttributeLike("ps", PracticedSystem.PROPERTY_CAMPAIGNS, args, filter.getPracticedSystemCampaign()));
            // PracticeSystem active
            query.append(DaoUtils.andAttributeEquals("ps", PracticedSystem.PROPERTY_ACTIVE, args, filter.getActive()));
            // PracticeSystem validated
            query.append(DaoUtils.andAttributeEquals("ps", PracticedSystem.PROPERTY_VALIDATED, args, filter.getValidated()));
            // Domain name
            query.append(DaoUtils.andAttributeLike("ps", PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_NAME, args, filter.getDomainName()));
            // GrowingPlan name
            query.append(DaoUtils.andAttributeLike("ps", PROPERTY_GROWING_SYSTEM_GROWING_PLAN_NAME, args, filter.getGrowingPlanName()));
            // Navigation context
            NavigationContext navigationContext = filter.getNavigationContext();
            if (navigationContext != null) {
                // campaigns
                // FIXME query must compare PracticedSystem campaing and not domain campaigns
                query.append(DaoUtils.andAttributeInIfNotEmpty("ps", PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_CAMPAIGN, args, navigationContext.getCampaigns()));
                // networks
                if (navigationContext.getNetworksCount() > 0) {
                    Set<String> growingSystemIds = getProjectionHelper().networksToGrowingSystems(navigationContext.getNetworks());
                    query.append(DaoUtils.andAttributeIn("ps", PROPERTY_GROWING_SYSTEM_ID, args, growingSystemIds));
                }
                // domains
                query.append(DaoUtils.andAttributeInIfNotEmpty("ps", PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_ID, args, DaoUtils.getRealIds(navigationContext.getDomains(), Domain.class)));
                // growingPlans
                query.append(DaoUtils.andAttributeInIfNotEmpty("ps", PROPERTY_GROWING_SYSTEM_GROWING_PLAN_ID, args, DaoUtils.getRealIds(navigationContext.getGrowingPlans(), GrowingPlan.class)));
                // growingSystems
                // FIXME query must compare code and not id
                query.append(DaoUtils.andAttributeInIfNotEmpty("ps", PROPERTY_GROWING_SYSTEM_ID, args, DaoUtils.getRealIds(navigationContext.getGrowingSystems(), GrowingSystem.class)));
            }
        }
        SecurityHelper.addPracticedSystemFilter(query, args, securityContext, "ps");
    }

    public List<PracticedSystem> getPracticedSystemsWithoutPlot(NavigationContext navigationContext, SecurityContext securityContext) {

        StringBuilder query = new StringBuilder(" FROM " + getEntityClass().getName() + " ps ");
        query.append(" WHERE ps not in (SELECT ps2 FROM " + PracticedPlot.class.getName() + " pp LEFT JOIN pp." + PracticedPlot.PROPERTY_PRACTICED_SYSTEM + " as ps2) ");
        Map<String, Object> args = new LinkedHashMap<>();

        if (navigationContext != null) {

            // campaigns
            // FIXME query must compare PracticedSystem campaing and not domain campaigns
            query.append(DaoUtils.andAttributeInIfNotEmpty("ps", PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_CAMPAIGN, args, navigationContext.getCampaigns()));

            // networks
            if (navigationContext.getNetworksCount() > 0) {
                Set<String> growingSystemIds = getProjectionHelper().networksToGrowingSystems(navigationContext.getNetworks());
                query.append(DaoUtils.andAttributeIn("ps", PROPERTY_GROWING_SYSTEM_ID, args, growingSystemIds));
            }

            // domains
            query.append(DaoUtils.andAttributeInIfNotEmpty("ps", PROPERTY_GROWING_SYSTEM_GROWING_PLAN_DOMAIN_ID, args, DaoUtils.getRealIds(navigationContext.getDomains(), Domain.class)));

            // growingPlans
            query.append(DaoUtils.andAttributeInIfNotEmpty("ps", PROPERTY_GROWING_SYSTEM_GROWING_PLAN_ID, args, DaoUtils.getRealIds(navigationContext.getGrowingPlans(), GrowingPlan.class)));

            // growingSystems
            // FIXME query must compare code and not id
            query.append(DaoUtils.andAttributeInIfNotEmpty("ps", PROPERTY_GROWING_SYSTEM_ID, args, DaoUtils.getRealIds(navigationContext.getGrowingSystems(), GrowingSystem.class)));
        }

        query.append(DaoUtils.andAttributeEquals("ps", PracticedSystem.PROPERTY_ACTIVE, args, Boolean.TRUE));

        query.append(" AND 0 < (SELECT COUNT(*) ")
                .append(" FROM ").append(GrowingSystem.class.getName()).append(" gs0 ")
                .append(" WHERE gs0.").append(GrowingSystem.PROPERTY_CODE).append(" = ps.").append(PracticedSystem.PROPERTY_GROWING_SYSTEM).append(".").append(GrowingSystem.PROPERTY_CODE)
                .append(" AND gs0.").append(GrowingSystem.PROPERTY_ACTIVE).append(" = true")
                .append(" AND gs0.").append(GrowingSystem.PROPERTY_GROWING_PLAN).append(".").append(GrowingPlan.PROPERTY_ACTIVE).append(" = true ")
                .append(" AND gs0.").append(GrowingSystem.PROPERTY_GROWING_PLAN).append(".").append(GrowingPlan.PROPERTY_DOMAIN).append(".").append(Domain.PROPERTY_ACTIVE).append(" = true")
                .append(" AND ps.").append(PracticedSystem.PROPERTY_CAMPAIGNS).append(" LIKE CONCAT('%', gs0.").append(GrowingSystem.PROPERTY_GROWING_PLAN).append(".").append(GrowingPlan.PROPERTY_DOMAIN).append(".").append(Domain.PROPERTY_CAMPAIGN).append(", '%'))");

        SecurityHelper.addPracticedSystemFilter(query, args, securityContext, "ps");

        return findAll(query + " ORDER BY lower (ps." + PracticedSystem.PROPERTY_NAME +")", args);
    }

    public void validatePracticedSystem(String practicedSystemId, LocalDateTime now, boolean validate) {
        Map<String, Object> args = DaoUtils.asArgsMap("practicedSystemId", practicedSystemId, "now", now, "validate", validate);
        topiaJpaSupport.execute("UPDATE " + getEntityClass().getName() + " ps" +
                " SET ps.validated=:validate, ps.validationDate=:now, ps.updateDate=:now" +
                " WHERE ps." + PracticedSystem.PROPERTY_TOPIA_ID + "=:practicedSystemId", args);

        // TODO AThimel 10/12/13 Validate all underlying entities
    }

    public boolean isActive(PracticedSystem practicedSystem) {
        Map<String, Object> args = new HashMap<>();

        String query = "SELECT count(*) FROM " + GrowingSystem.class.getName() + " gs ";

        query += " WHERE 1=1";

        query += DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_CODE, args, practicedSystem.getGrowingSystem().getCode());

        query += DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_ACTIVE, args, Boolean.TRUE);

        query += DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_GROWING_PLAN + "." +
                GrowingPlan.PROPERTY_ACTIVE, args, Boolean.TRUE);

        query += DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_GROWING_PLAN + "." +
                GrowingPlan.PROPERTY_DOMAIN + "." +
                Domain.PROPERTY_ACTIVE, args, Boolean.TRUE);


        LinkedHashSet<Integer> campaigns = CommonService.GET_CAMPAIGNS_SET.apply(practicedSystem.getCampaigns());

        query += "AND (" ;
        int i = 0;
        List<String> subCampaignQuerys = new ArrayList<>(campaigns.size());
        for (Integer campaign : campaigns) {
            String queryParam = "growingPlan_domain_campaign_" + i++;
            subCampaignQuerys.add(" :" + queryParam +" = gs.growingPlan.domain.campaign ");
            args.put(queryParam, campaign);
        }
        query += String.join(" OR ", subCampaignQuerys);
        query += ")";

        long activeGrowingSystemCount = findUnique(query, args);

        return activeGrowingSystemCount > 0;
    }

    public boolean areActive(Collection<PracticedSystem> practicedSystems) {
        Map<String, Object> args = new HashMap<>();

        String query = "SELECT count(*) FROM " + GrowingSystem.class.getName() + " gs ";

        query += " WHERE 1=1";

        Set<String> codes = practicedSystems.stream()
                .map(PracticedSystem::getGrowingSystem)
                .map(GrowingSystem::getCode)
                .collect(Collectors.toSet());
        query += DaoUtils.andAttributeIn("gs", GrowingSystem.PROPERTY_CODE, args, codes);

        query += DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_ACTIVE, args, Boolean.TRUE);

        query += DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_GROWING_PLAN + "." +
                GrowingPlan.PROPERTY_ACTIVE, args, Boolean.TRUE);

        query += DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_GROWING_PLAN + "." +
                GrowingPlan.PROPERTY_DOMAIN + "." +
                Domain.PROPERTY_ACTIVE, args, Boolean.TRUE);


        Set<Integer> campaigns = practicedSystems.stream()
                .map(practicedSystem -> CommonService.GET_CAMPAIGNS_SET.apply(practicedSystem.getCampaigns()))
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        query += "AND (" ;
        int i = 0;
        List<String> subCampaignQuerys = new ArrayList<>(campaigns.size());
        for (Integer campaign : campaigns) {
            String queryParam = "growingPlan_domain_campaign_" + i++;
            subCampaignQuerys.add(" :" + queryParam +" = gs.growingPlan.domain.campaign ");
            args.put(queryParam, campaign);
        }
        query += String.join(" OR ", subCampaignQuerys);
        query += ")";

        long activeGrowingSystemCount = findUnique(query, args);

        return activeGrowingSystemCount > 0;
    }

    public List<PracticedSystem> loadAllPracticedSystemsForGrowingSystemsAnsCampaign(Set<GrowingSystem> growingSystems, int campaign) {
        String practicedSystemQuery = "FROM " + PracticedSystem.class.getName() + " ps " +
                " WHERE ps." + PracticedSystem.PROPERTY_GROWING_SYSTEM + " IN :growingSystems" +
                " AND ps." + PracticedSystem.PROPERTY_CAMPAIGNS + " LIKE :campaigns";

        return findAll(practicedSystemQuery,
                DaoUtils.asArgsMap("growingSystems", growingSystems, "campaigns", "%" + campaign + "%"));
    }

} //PracticedSystemTopiaDao<E extends PracticedSystem>
