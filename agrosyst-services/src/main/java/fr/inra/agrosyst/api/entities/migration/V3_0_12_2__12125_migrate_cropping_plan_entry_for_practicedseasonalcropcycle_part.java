package fr.inra.agrosyst.api.entities.migration;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

public class V3_0_12_2__12125_migrate_cropping_plan_entry_for_practicedseasonalcropcycle_part extends BaseJavaMigration {

    private static final Log LOGGER = LogFactory.getLog(V3_0_12_2__12125_migrate_cropping_plan_entry_for_practicedseasonalcropcycle_part.class);

    public void migrate(Context context) throws Exception {
        var nodesWithoutValidEntry = """
                select
                    pccn.topiaid,
                    pcc.topiaid,
                    ps.topiaid,
                    ps.campaigns,
                    d.topiaid,
                    d.campaign,
                    pccn.croppingplanentrycode
                from
                    practicedcropcyclenode pccn
                    join practicedcropcycle pcc on pccn.practicedseasonalcropcycle = pcc.topiaid
                    join practicedsystem ps on pcc.practicedsystem = ps.topiaid
                    join growingsystem gs on ps.growingsystem = gs.topiaid
                    join growingplan gp on gs.growingplan = gp.topiaid
                    join "domain" d on gp."domain" = d.topiaid
                    left join croppingplanentry cpe on cpe."domain" = d.topiaid and pccn.croppingplanentrycode = cpe.code
                where
                    cpe is null;""";
        try (Statement selectNodes = context.getConnection().createStatement()) {
            try (ResultSet nodes = selectNodes.executeQuery(nodesWithoutValidEntry)) {
                while (nodes.next()) {
                    var nodeId = nodes.getString(1);
                    var cycleId = nodes.getString(2);
                    var systemId = nodes.getString(3);
                    var campaigns = Arrays.asList(nodes.getString(4).split(", "));
                    Collections.reverse(campaigns);
                    var oDomain = nodes.getString(5);
                    var oCampaignDomain = nodes.getString(6);
                    var entryCode = nodes.getString(7);
                    LOGGER.info(String.format("Start working on node %s%n\tof cycle %s%n\tof system %s%n\twith code%s%n", nodeId, cycleId, systemId, entryCode));
                    for (var campaign : campaigns) {
                        if (!campaign.equals(oCampaignDomain)) {
                            var entryCorresponding = """
                                    select cpe.*
                                    from croppingplanentry cpe join "domain" d on cpe."domain" = d.topiaid
                                    where cpe.code = '%s' and d.campaign = %s;""";
                            try (Statement selectEntry = context.getConnection().createStatement()) {
                                try (ResultSet entry = selectEntry.executeQuery(String.format(entryCorresponding, entryCode, campaign))) {
                                    if (entry.next()) {
                                        if (!entryExist(context, entryCode, oDomain)) {
                                            var entryId = migrateEntry(context, entry, oDomain, nodeId);
                                            migrateSpecies(context, entryCode, campaign, entryId, nodeId);
                                            break;
                                        } else {
                                            LOGGER.info(String.format("Entry already exist for campaign %s and code %s for node %s%n", campaign, entryCode, nodeId));
                                        }
                                    } else {
                                        LOGGER.info(String.format("No entry found for campaign %s and code %s for node %s%n", campaign, entryCode, nodeId));
                                    }
                                }
                            }
                        } else {
                            LOGGER.info(String.format("Evicted campaign for node %s because of original campaign equality%n", nodeId));
                        }
                    }
                    LOGGER.info(String.format("Stop working on node %s of cycle %s of system %s%n%n", nodeId, cycleId, systemId));
                }
            }
        }
    }

    private boolean entryExist(Context context, String entryCode, String oDomain) throws Exception {
        var exist = """
               select cpe.*
               from croppingplanentry cpe
               where cpe.code = '%s' and cpe."domain" = '%s';""";
        try (Statement existEntry = context.getConnection().createStatement()) {
            try (ResultSet entry = existEntry.executeQuery(String.format(exist, entryCode, oDomain))) {
                return entry.next();
            }
        }
    }

    private String migrateEntry(Context context, ResultSet entry, String oDomain, String nodeId) throws Exception {
        var entryId = "fr.inra.agrosyst.api.entities.CroppingPlanEntry_" + UUID.randomUUID();
        var insert = """
                insert into croppingplanentry (topiaid, topiaversion, code, domain, topiacreatedate, name, sellingprice, validated, type,
                yealdaverage, averageift, yealdunit, estimatingiftrules, iftseedstype, dosetype, biocontrolift, temporarymeadow, pasturedmeadow, mowedmeadow, mixspecies, mixvariety)
                values (?, 0, ?, ?, (select topiacreatedate from _12125_script_date), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);""";
        try (PreparedStatement insertEntry = context.getConnection().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS)) {
            insertEntry.setString(1, entryId);
            insertEntry.setString(2, entry.getString(3));
            insertEntry.setString(3, oDomain);
            insertEntry.setString(4, entry.getString(6));
            insertEntry.setDouble(5, entry.getDouble(7));
            insertEntry.setBoolean(6, entry.getBoolean(8));
            insertEntry.setString(7, entry.getString(9));
            insertEntry.setDouble(8, entry.getDouble(10));
            insertEntry.setDouble(9, entry.getDouble(11));
            insertEntry.setString(10, entry.getString(12));
            insertEntry.setString(11, entry.getString(13));
            insertEntry.setString(12, entry.getString(14));
            insertEntry.setString(13, entry.getString(15));
            insertEntry.setDouble(14, entry.getDouble(16));
            insertEntry.setBoolean(15, entry.getBoolean(17));
            insertEntry.setBoolean(16, entry.getBoolean(18));
            insertEntry.setBoolean(17, entry.getBoolean(19));
            insertEntry.setBoolean(18, entry.getBoolean(20));
            insertEntry.setBoolean(19, entry.getBoolean(21));
            insertEntry.executeUpdate();
        }
        LOGGER.info(String.format("One entry found and migrated for node %s%n", nodeId));
        return entryId;
    }

    private void migrateSpecies(Context context, String entryCode, String campaign, String croppingPlanEntryId, String nodeId) throws Exception {
        var speciesCorresponding = """
                select cps.*
                from croppingplanspecies cps join croppingplanentry cpe on cps.croppingplanentry = cpe.topiaid join "domain" d on cpe."domain" = d.topiaid
                where cpe.code = '%s' and d.campaign = %s;""";

        try (Statement selectSpecies = context.getConnection().createStatement()) {
            try (ResultSet species = selectSpecies.executeQuery(String.format(speciesCorresponding, entryCode, campaign))) {
                var quantity = 0;
                while (species.next()) {
                    var insert = """
                            insert into croppingplanspecies (topiaid, topiaversion, topiacreatedate, code, validated, variety, croppingplanentry,
                                species, croppingplanentry_idx, speciesarea, edaplosunknownvariety, destination, compagne)
                            values (CONCAT('fr.inra.agrosyst.api.entities.CroppingPlanSpecies_', uuid_in(md5(random()::text || random()::text)::cstring)),
                                0, (select topiacreatedate from _12125_script_date), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);""";
                    try (PreparedStatement insertSpecies = context.getConnection().prepareStatement(insert)) {
                        insertSpecies.setString(1, species.getString(4));
                        insertSpecies.setBoolean(2, species.getBoolean(5));
                        insertSpecies.setString(3, species.getString(6));
                        insertSpecies.setString(4, croppingPlanEntryId);
                        insertSpecies.setString(5, species.getString(8));
                        insertSpecies.setInt(6, species.getInt(9));
                        insertSpecies.setInt(7, species.getInt(10));
                        insertSpecies.setString(8, species.getString(11));
                        insertSpecies.setString(9, species.getString(12));
                        insertSpecies.setString(10, species.getString(13));
                        insertSpecies.executeUpdate();
                    }
                    quantity++;
                }
                LOGGER.info(String.format("%d species found and migrated for node %s%n", quantity, nodeId));
            }
        }
    }
}
