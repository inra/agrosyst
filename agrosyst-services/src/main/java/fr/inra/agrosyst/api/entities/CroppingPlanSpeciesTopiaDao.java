package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleSpecies;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.measure.Measure;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleSpecies;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.report.ArboCropAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropPestMaster;
import fr.inra.agrosyst.api.entities.report.CropPestMaster;
import fr.inra.agrosyst.api.entities.report.FoodMaster;
import fr.inra.agrosyst.api.entities.report.VerseMaster;
import fr.inra.agrosyst.api.entities.report.YieldLoss;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author David Cossé
 */
public class CroppingPlanSpeciesTopiaDao extends AbstractCroppingPlanSpeciesTopiaDao<CroppingPlanSpecies> {

    private static final String FROM_NODE_TO_DOMAIN_QUERY = PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE + "." +
            PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM + "." +
            PracticedSystem.PROPERTY_GROWING_SYSTEM + "." +
            GrowingSystem.PROPERTY_GROWING_PLAN + "." +
            GrowingPlan.PROPERTY_DOMAIN;

    protected static final String FROM_SPECIES_CODES_TO_CODE_ESPECE_BOTANIQUE_CODE_QUALIFIANT_AEE_QUERY =
            "SELECT DISTINCT " +
                    "       cps." + CroppingPlanSpecies.PROPERTY_CODE +
                    "     , ref_espece." + RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE +
                    "     , ref_espece." + RefEspece.PROPERTY_CODE_QUALIFIANT__AEE +
                    "       FROM " + CroppingPlanSpecies.class.getName() + " cps " +
                    "       INNER JOIN cps." + CroppingPlanSpecies.PROPERTY_SPECIES + " ref_espece" +
                    "       WHERE cps." + CroppingPlanSpecies.PROPERTY_CODE + " IN :speciesCodes ";

    public List<CroppingPlanSpecies> getCroppingPlanSpeciesForCodeAndDomainId (Set<String> speciesCodes, String domainId) {
        Map<String, Object> args = Maps.newLinkedHashMap();
        StringBuilder query = new StringBuilder("FROM " + CroppingPlanSpecies.class.getName() + " cps ");
        query.append("  WHERE 1 = 1 ");
        query.append(DaoUtils.andAttributeIn("cps", CroppingPlanSpecies.PROPERTY_CODE, args, speciesCodes));
        query.append(DaoUtils.andAttributeEquals("cps", CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY + "." + CroppingPlanEntry.PROPERTY_DOMAIN + '.' + Domain.PROPERTY_TOPIA_ID, args, domainId));
        List<CroppingPlanSpecies> result = findAll(query.toString(), args);

        return result;
    }
    
    public List<CroppingPlanSpecies> getCroppingPlanSpeciesForCodeAndCampaigns(Set<String> speciesCodes, Set<Integer> campaigns) {
        Map<String, Object> args = Maps.newLinkedHashMap();
        StringBuilder query = new StringBuilder("FROM " + CroppingPlanSpecies.class.getName() + " cps ");
        query.append("  WHERE 1 = 1 ");
        query.append(DaoUtils.andAttributeIn("cps", CroppingPlanSpecies.PROPERTY_CODE, args, speciesCodes));
        query.append(DaoUtils.andAttributeIn("cps", CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY + "." + CroppingPlanEntry.PROPERTY_DOMAIN + '.' + Domain.PROPERTY_CAMPAIGN, args, campaigns));
        List<CroppingPlanSpecies> result = findAll(query.toString(), args);
        return result;
    }

    protected Set<String> queryBody(String subQuery, Map<String, Object> args) {

        StringBuilder query = new StringBuilder("SELECT cps."+ CroppingPlanSpecies.PROPERTY_TOPIA_ID +", ");

        query.append("(" + subQuery + ")");

        query.append(" FROM " + CroppingPlanSpecies.class.getName() + " cps  WHERE 1 = 1 ");
        query.append(" AND cps." + CroppingPlanSpecies.PROPERTY_TOPIA_ID + " IN (:cpeIds)");

        Set<String> result = Sets.newHashSet();
        List<Object[]> cpss = findAll(query.toString(), args);
        for (Object[] cpsTuple : cpss) {
            String cpsId = (String) cpsTuple[0];
            Long count = (Long)cpsTuple[1];
            if (count != null && count > 0L) {
                result.add(cpsId);
            }
        }

        return result;

    }

    public Set<String> getCPSpeciesUsedForMeasures(Set<String> ids) {
        Map<String, Object> args = ImmutableMap.of("cpeIds", ids);

        // count nb species into Measure
        String query =
            "SELECT COUNT(*) FROM " + Measure.class.getName() + " m " +
            "  WHERE m." + Measure.PROPERTY_CROPPING_PLAN_SPECIES + " = cps ";

        Set<String> result = queryBody(query, args);
        return result;
    }

    public Set<String> getCPSpeciesUsedForLot(Set<String> ids) {
        Map<String, Object> args = ImmutableMap.of("cpeIds", ids);

        // count nb species use into EffectiveCropCycleSpecies
        String query =
                "SELECT COUNT(*) FROM " + DomainSeedSpeciesInput.class.getName() + " dssi " +
                        "   WHERE dssi."+ DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED + " = cps ";

        Set<String> result = queryBody(query, args);
        return result;
    }

    public Set<String> getCPSpeciesUsedForEffectiveCropCycleSpecies(Set<String> ids) {
        Map<String, Object> args = ImmutableMap.of("cpeIds", ids);

        // count nb species use into EffectiveCropCycleSpecies
        String query =
            "SELECT COUNT(*) FROM " + EffectiveCropCycleSpecies.class.getName() + " eccs " +
            "   WHERE eccs."+ EffectiveCropCycleSpecies.PROPERTY_CROPPING_PLAN_SPECIES + " = cps ";

        Set<String> result = queryBody(query, args);
        return result;
    }

    public Set<String> getCPSpeciesUsedForEffectiveSpeciesStades(Set<String> ids) {
        Map<String, Object> args = ImmutableMap.of("cpeIds", ids);

        // count nb species used into EffectiveSpeciesStade
        String query =
            "SELECT COUNT(*) FROM " + EffectiveSpeciesStade.class.getName() + " ess " +
            "   WHERE ess."+ EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES + " = cps ";

        Set<String>result = queryBody(query, args);
        return result;
    }

    public Set<String> getCPSpeciesUsedForPracticedCropCycleSpecies(String domainCode, String stCampaign, Set<String> croppingPlanSpeciesIds) {
        Map<String, Object> args = ImmutableMap.of("domainCode" , domainCode, "campaign" , stCampaign, "cpeIds", croppingPlanSpeciesIds);
         // count nb species used for practicedPerennialCropCycle into PracticedCropCycleSpecies
        String query =
            "SELECT COUNT(*) FROM " + PracticedCropCycleSpecies.class.getName() + " pccs " +
            "   INNER JOIN pccs." +  PracticedCropCycleSpecies.PROPERTY_CYCLE + "." + PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM  + " ps" +
            "   WHERE pccs." + PracticedCropCycleSpecies.PROPERTY_CROPPING_PLAN_SPECIES_CODE + " = cps."+ CroppingPlanSpecies.PROPERTY_CODE +
            "   AND ps." + PracticedSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CODE + "=" + ":domainCode" +
            "   AND ps." + PracticedSystem.PROPERTY_CAMPAIGNS + " LIKE :campaign ";

        Set<String> result = queryBody(query, args);
        return result;
    }

    public List<PracticedCropCycleNode> getPracticedCropCycleNodeForCampaignAndDomainCode(String domainCode, String campaign) {

        String query = "FROM " + PracticedCropCycleNode.class.getName() + " pccn " +
            "  WHERE " +
            "  pccn." + DomainTopiaDao.PRACTICED_NODE_SEASONAL_CYCLE_PRACTICED_SYSTEM_CAMPAIGNS + " LIKE :campaign " +
            "  AND pccn." + FROM_NODE_TO_DOMAIN_QUERY + "." + Domain.PROPERTY_CODE + " = :domainCode";

        Map<String, Object> args = Maps.newLinkedHashMap();
        args.put("campaign", campaign);
        args.put("domainCode", domainCode);

        List<PracticedCropCycleNode> result = findAll(query, args);
        return result;
    }

    public Set<String> getCPSpeciesUsedForPracticedPerennialCropCycleStades(String domainCode, String stCampaign, Set<String> croppingPlanSpeciesIds) {
        Map<String, Object> args = ImmutableMap.of("domainCode" , domainCode, "campaign" , stCampaign, "cpeIds", croppingPlanSpeciesIds);
        // count nb species used for practicedPerennialCropCycle into stades (found from Phases)
        String query = "SELECT COUNT(ppcc) FROM " + PracticedIntervention.class.getName() + " pi " +
                "  INNER JOIN pi." + PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE + " phase " +
                "  INNER JOIN phase." + PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE + " ppcc " +
                "  INNER JOIN ppcc." + PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM + " ps " +
                "  INNER JOIN pi." + PracticedIntervention.PROPERTY_SPECIES_STADES + " sst " +
                "  WHERE sst." + PracticedSpeciesStade.PROPERTY_SPECIES_CODE  + " = cps." + CroppingPlanSpecies.PROPERTY_CODE +
                "  AND ps." + PracticedSystem.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CODE + "=" + ":domainCode" +
                "  AND ps." + PracticedSystem.PROPERTY_CAMPAIGNS + " LIKE :campaign ";

        Set<String> result = queryBody(query, args);
        return result;
    }

    public List<PracticedIntervention> practicedInterventionsForSources(List<PracticedCropCycleNode> nodes) {
        StringBuilder query = new StringBuilder("FROM " + PracticedIntervention.class.getName() + " pi");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = Maps.newLinkedHashMap();

        String piSource = PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION + "."+ PracticedCropCycleConnection.PROPERTY_SOURCE;
        Set<PracticedCropCycleNode> nodesSet = Sets.newHashSet(nodes);
        query.append(DaoUtils.andAttributeInIfNotEmpty("pi", piSource, args, nodesSet));

        List<PracticedIntervention> result = findAll(query.toString(), args);
        return result;
    }

    public List<PracticedIntervention> practicedInterventionsForTargets(List<PracticedCropCycleNode> nodes) {
        StringBuilder query = new StringBuilder("FROM " + PracticedIntervention.class.getName() + " pi");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = Maps.newLinkedHashMap();

        String piSource = PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION + "." + PracticedCropCycleConnection.PROPERTY_TARGET;
        Set<PracticedCropCycleNode> nodesSet = Sets.newHashSet(nodes);
        query.append(DaoUtils.andAttributeInIfNotEmpty("pi", piSource, args, nodesSet));

        List<PracticedIntervention> result = findAll(query.toString(), args);
        return result;
    }

    /**
     * get a map of practicedSpeciesStade and it's related Species TopiaId.
     * @param croppingPlanSpeciesIds species Id
     * @return a map of speciesStade and it's related Species TopiaId.
     */
    public Map<PracticedSpeciesStade, String> getSpeciesPracticedSpeciesStades(
            Iterable<String> croppingPlanSpeciesIds) {

        Set<String> cpeSpeciesIds = Sets.newHashSet(croppingPlanSpeciesIds);
        Map<String, Object> args = Maps.newLinkedHashMap();
        String query =
                "SELECT DISTINCT pss, cps."+ CroppingPlanSpecies.PROPERTY_TOPIA_ID +
                " FROM " + PracticedSpeciesStade.class.getName() + " pss, "
                         + CroppingPlanSpecies.class.getName() + " cps " +
                " WHERE pss." + PracticedSpeciesStade.PROPERTY_SPECIES_CODE + " = cps." + CroppingPlanSpecies.PROPERTY_CODE;

        query += DaoUtils.andAttributeIn("cps", CroppingPlanSpecies.PROPERTY_TOPIA_ID, args, cpeSpeciesIds);

        Map<PracticedSpeciesStade, String> result = new HashMap<>();
        List<Object[]> queryResults = findAll(query, args);
        for (Object[] res : queryResults) {
            PracticedSpeciesStade speciesStade = (PracticedSpeciesStade) res[0];
            String speciesId = (String)res[1];
            result.put(speciesStade, speciesId);
        }
        return result;
    }

    public List<Pair<String, String>> loadCodeEspeceBotaniquesCodeQualifiantAEEForDomainId(Set<String> domainIds) {
        Map<String, Object> args = Maps.newLinkedHashMap();

        Preconditions.checkNotNull(domainIds);

        String query =
                "SELECT DISTINCT " +
                        " re." + RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE + " ," +
                        " re." + RefEspece.PROPERTY_CODE_QUALIFIANT__AEE +
                        " FROM " + CroppingPlanSpecies.class.getName() + " cps " +
                        " INNER JOIN cps." + CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY + " cpe " +
                        " INNER JOIN cpe." + CroppingPlanEntry.PROPERTY_DOMAIN + " d " +
                        " INNER JOIN cps." + CroppingPlanSpecies.PROPERTY_SPECIES + " re " +
                        " WHERE d." + Domain.PROPERTY_TOPIA_ID + " IN (:domainIds)" +
                        " AND d." + Domain.PROPERTY_ACTIVE + " = TRUE " +
                        " AND re." + RefEspece.PROPERTY_ACTIVE + " = TRUE ";
        args.put("domainIds", domainIds);

        List<Object[]> result0 = findAll(query, args);
        List<Pair<String, String>> result = Lists.newArrayListWithExpectedSize(result0.size());
        for (Object[] pair : result0) {
            String codeEspeceBotanique = Strings.emptyToNull((String) pair[0]);
            String codeQualifiantAEE = Strings.emptyToNull((String) pair[1]);
            result.add(Pair.of(codeEspeceBotanique, codeQualifiantAEE));
        }
        return result;
    }

    public List<Object[]> loadSpeciesCodeCodeEspeceBotaniquesCodeQualifiantAEEForDomainId(Set<String> domainIds) {
        Map<String, Object> args = Maps.newLinkedHashMap();

        Preconditions.checkNotNull(domainIds);

        String query =
                "SELECT DISTINCT " +
                        " cps." + CroppingPlanSpecies.PROPERTY_CODE + "," +
                        " re." + RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE + " ," +
                        " re." + RefEspece.PROPERTY_CODE_QUALIFIANT__AEE +
                        " FROM " + CroppingPlanSpecies.class.getName() + " cps " +
                        " INNER JOIN cps." + CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY + " cpe " +
                        " INNER JOIN cpe." + CroppingPlanEntry.PROPERTY_DOMAIN + " d " +
                        " INNER JOIN cps." + CroppingPlanSpecies.PROPERTY_SPECIES + " re " +
                        " WHERE d." + Domain.PROPERTY_TOPIA_ID + " IN (:domainIds)" +
                        " AND d." + Domain.PROPERTY_ACTIVE + " = TRUE " +
                        " AND re." + RefEspece.PROPERTY_ACTIVE + " = TRUE ";
        args.put("domainIds", domainIds);

        List<Object[]> result = findAll(query, args);
        return result;
    }

    public List<Pair<String, String>> loadCodeEspeceBotaniqueCodeQualifiantAEEForSpeciesCode(Set<String> speciesCodes) {
        Map<String, Object> args = Maps.newLinkedHashMap();
        String query =
                "SELECT DISTINCT " +
                        " cps." + CroppingPlanSpecies.PROPERTY_SPECIES + "." + RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE + " , " +
                        " cps." + CroppingPlanSpecies.PROPERTY_SPECIES + "." + RefEspece.PROPERTY_CODE_QUALIFIANT__AEE +
                        " FROM " + CroppingPlanSpecies.class.getName() + " cps " +
                        " WHERE 1=1 " +
                        DaoUtils.andAttributeIn("cps", CroppingPlanSpecies.PROPERTY_CODE , args, speciesCodes) +
                        DaoUtils.andAttributeEquals("cps", CroppingPlanSpecies.PROPERTY_SPECIES + "." + RefEspece.PROPERTY_ACTIVE , args, true);

        List<Object[]> result0 = findAll(query, args);
        List<Pair<String, String>> result =Lists.newArrayListWithExpectedSize(result0.size());
        for (Object[] pair : result0) {
            String codeEspeceBotanique = Strings.emptyToNull((String) pair[0]);
            String codeQualifiantAEE = Strings.emptyToNull((String) pair[1]);
            result.add(Pair.of(codeEspeceBotanique, codeQualifiantAEE));
        }
        return result;
    }



    public Set<String> getCPSUsedForCropPestMaster(Set<String> ids) {
        Map<String, Object> args = ImmutableMap.of("cpeIds", ids);

        String query = "SELECT COUNT(*) FROM " + CropPestMaster.class.getName() + " cpm " +
                "        INNER JOIN cpm." + CropPestMaster.PROPERTY_SPECIES + " cps0 " +
                "        WHERE cps0 = cps";
        Set<String>result = queryBody(query, args);

        return result;
    }

    public Set<String> getCPSUsedForVerseMaster(Set<String> ids) {
        Map<String, Object> args = ImmutableMap.of("cpeIds", ids);

        String query = "SELECT COUNT(*) FROM " + VerseMaster.class.getName() + " vm " +
                "        INNER JOIN vm." + VerseMaster.PROPERTY_SPECIES + " cps0 " +
                "        WHERE cps0 = cps";
        Set<String>result = queryBody(query, args);

        return result;
    }

    public Set<String> getCPSUsedForFoodMaster(Set<String> ids) {
        Map<String, Object> args = ImmutableMap.of("cpeIds", ids);

        String query = "SELECT COUNT(*) FROM " + FoodMaster.class.getName() + " fm " +
                "        INNER JOIN fm." + FoodMaster.PROPERTY_SPECIES + " cps0 " +
                "        WHERE cps0 = cps";
        Set<String>result = queryBody(query, args);

        return result;
    }

    public Set<String> getCPSUsedForYieldLoss(Set<String> ids) {
        Map<String, Object> args = ImmutableMap.of("cpeIds", ids);

        String query = "SELECT COUNT(*) FROM " + YieldLoss.class.getName() + " yl " +
                "        INNER JOIN yl." + YieldLoss.PROPERTY_SPECIES + " cps0 " +
                "        WHERE cps0 = cps";
        Set<String>result = queryBody(query, args);

        return result;
    }

    public Set<String> getCPSUsedForArboCropAdventiceMaster(Set<String> ids) {
        Map<String, Object> args = ImmutableMap.of("cpeIds", ids);

        String query = "SELECT COUNT(*) FROM " + ArboCropAdventiceMaster.class.getName() + " acam " +
                "        INNER JOIN acam." + ArboCropAdventiceMaster.PROPERTY_SPECIES + " cps0 " +
                "        WHERE cps0 = cps";
        Set<String>result = queryBody(query, args);

        return result;
    }

    public Set<String> getCPSUsedForArboCropPestMaster(Set<String> ids) {
        Map<String, Object> args = ImmutableMap.of("cpeIds", ids);

        String query = "SELECT COUNT(*) FROM " + ArboCropPestMaster.class.getName() + " acpm " +
                "        INNER JOIN acpm." + ArboCropPestMaster.PROPERTY_SPECIES + " cps0 " +
                "        WHERE cps0 = cps";
        Set<String>result = queryBody(query, args);

        return result;
    }


    public Map<String, Set<Pair<String, String>>> loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(Set<String> speciesCodes) {
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = new HashMap<>();

        Map<String, Object> args = new HashMap<>();
        args.put("speciesCodes", speciesCodes);
        List<Object[]> result0 = findAll(FROM_SPECIES_CODES_TO_CODE_ESPECE_BOTANIQUE_CODE_QUALIFIANT_AEE_QUERY, args);
        for (Object[] objects : result0) {
            String speciesCode = (String) objects[0];
            String codeEspeceBotanique = (String) objects[1];
            String codeQualifiantAee = (String) objects[2];
            Set<Pair<String, String>> speciesCodeEspBotCodeQuali = codeEspBotCodeQualiBySpeciesCode.computeIfAbsent(speciesCode, k -> new HashSet<>());
            speciesCodeEspBotCodeQuali.add(Pair.of(codeEspeceBotanique, codeQualifiantAee));
        }

        return codeEspBotCodeQualiBySpeciesCode;
    }
}
