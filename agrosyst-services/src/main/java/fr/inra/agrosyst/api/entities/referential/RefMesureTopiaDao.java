package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.VariableType;
import fr.inra.agrosyst.api.entities.measure.MeasurementType;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.nuiton.topia.persistence.TopiaException;

import java.util.List;
import java.util.Map;

public class RefMesureTopiaDao extends AbstractRefMesureTopiaDao<RefMesure> {

    public List<VariableType> findTypeVariableValues(MeasurementType measurementType) throws TopiaException {
        String query = "SELECT distinct(" + RefMesure.PROPERTY_TYPE_VARIABLE_MESUREE + ")" +
                " FROM " + getEntityClass().getName() +
                " WHERE " + RefMesure.PROPERTY_ACTIVE + " = true" +
                " AND " + RefMesure.PROPERTY_CATEGORIE_DE_MESURE +  " = :measurementType" +
                " ORDER BY " + RefMesure.PROPERTY_TYPE_VARIABLE_MESUREE;

        Map<String,Object> args = DaoUtils.asArgsMap("measurementType", measurementType);
        List<VariableType> result = findAll(query, args);
        return result;
    }
    
    public List<RefMesure> findVariables(MeasurementType measurementType, VariableType variableType) throws TopiaException {
        String query = " FROM " + getEntityClass().getName() +
                " WHERE " + RefMesure.PROPERTY_ACTIVE + " = true" +
                " AND " + RefMesure.PROPERTY_CATEGORIE_DE_MESURE + " = :measurementType";
        
        // TODO check if topia have to handle = null without NPE
        if (variableType != null) {
            query += " AND " + RefMesure.PROPERTY_TYPE_VARIABLE_MESUREE + " = :variableType";
        }
        
        query += " ORDER BY " + RefMesure.PROPERTY_TYPE_VARIABLE_MESUREE;

        List<RefMesure> result;
        if (variableType != null) {
            result = findAll(query, DaoUtils.asArgsMap("measurementType", measurementType, "variableType", variableType));
        } else {
            result = findAll(query, DaoUtils.asArgsMap("measurementType", measurementType));
        }
        return result;
    }
}
