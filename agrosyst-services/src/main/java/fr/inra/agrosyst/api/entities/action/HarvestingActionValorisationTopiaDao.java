package fr.inra.agrosyst.api.entities.action;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2016 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefDestination;

/**
 * Created by davidcosse on 06/07/16.
 */
public class HarvestingActionValorisationTopiaDao extends AbstractHarvestingActionValorisationTopiaDao<HarvestingActionValorisation> {

    public static final String NEW_HARVESTING_VALORISATION = "NEW-HARVESTING-VALORISATION-";

    public record UniqueKeyValorisation(
            HarvestingAction action,
            String speciesCode,
            RefDestination destination
    ){
        @Override
        public String toString() {
            return "action:"+ action.getTopiaId() + ";speciesCode:"+ speciesCode + ";destination" + destination.getTopiaId();
        }
    }
}
