package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.MaterielType;
import fr.inra.agrosyst.api.utils.DaoUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RefAgsAmortissementTopiaDao extends AbstractRefAgsAmortissementTopiaDao<RefAgsAmortissement> {
    
    public Map<RefMateriel, RefAgsAmortissement> findRefAgsAmortissementForTractors(Set<RefMateriel> materiels) {
        String query = " SELECT DISTINCT rm, ra FROM " + this.getEntityClass().getName() + " ra, " + RefMaterielTraction.class.getName() + " rm ";
        query +=       " WHERE rm." + RefMaterielTraction.PROPERTY_DONNEES_AMORTISSEMENT1 + " = ra." + RefAgsAmortissement.PROPERTY_DONNEES_AMORTISSEMENT1;
        query += " AND   rm." + RefMaterielTraction.PROPERTY_DONNEES_AMORTISSEMENT2 + " = ra." + RefAgsAmortissement.PROPERTY_DONNEES_AMORTISSEMENT2;
        query += " AND   rm." + RefMaterielTraction.PROPERTY_ACTIVE + " IS TRUE ";
        query += " AND   ra." + RefAgsAmortissement.PROPERTY_TYPE__MATERIEL + " = :typeMat";
        query +=       " AND   ra." + RefAgsAmortissement.PROPERTY_ACTIVE + " IS TRUE";
        query +=       " AND   rm IN (:materiels)";
        
        List<Object[]> result0 = findAll(query, DaoUtils.asArgsMap("materiels", materiels, "typeMat", MaterielType.TRACTEUR));
    
        Map<RefMateriel, RefAgsAmortissement> result = getRefMaterielRefAgsAmortissementMap(result0);
    
        return result;
    }
    
    public Map<RefMateriel, RefAgsAmortissement> findRefAgsAmortissementForAutomotors(Set<RefMateriel> materiels) {
        String query = " SELECT DISTINCT rm, ra FROM " + this.getEntityClass().getName() + " ra, " + RefMaterielAutomoteur.class.getName() + " rm ";
        query +=       " WHERE rm." + RefMaterielAutomoteur.PROPERTY_DONNEES_AMORTISSEMENT1 + " = ra." + RefAgsAmortissement.PROPERTY_DONNEES_AMORTISSEMENT1;
        query += " AND   rm." + RefMaterielAutomoteur.PROPERTY_DONNEES_AMORTISSEMENT2 + " = ra." + RefAgsAmortissement.PROPERTY_DONNEES_AMORTISSEMENT2;
        query += " AND   rm." + RefMaterielAutomoteur.PROPERTY_ACTIVE + " IS TRUE ";
        query += " AND   ra." + RefAgsAmortissement.PROPERTY_TYPE__MATERIEL + " = :typeMat";
        query +=       " AND   ra." + RefAgsAmortissement.PROPERTY_ACTIVE + " IS TRUE";
        query +=       " AND   rm IN (:materiels)";
        
        List<Object[]> result0 = findAll(query, DaoUtils.asArgsMap("materiels", materiels, "typeMat", MaterielType.AUTOMOTEUR));
    
        Map<RefMateriel, RefAgsAmortissement> result = getRefMaterielRefAgsAmortissementMap(result0);
    
        return result;
    }
    
    public Map<RefMateriel, RefAgsAmortissement> findRefAgsAmortissementForTools(Set<RefMateriel> materiels) {
        String query = " SELECT DISTINCT rm, ra FROM " + this.getEntityClass().getName() + " ra, " + RefMaterielOutil.class.getName() + " rm ";
        query +=       " WHERE rm." + RefMaterielOutil.PROPERTY_DONNEES_AMORTISSEMENT1 + " = ra." + RefAgsAmortissement.PROPERTY_DONNEES_AMORTISSEMENT1;
        query += " AND   rm." + RefMaterielOutil.PROPERTY_DONNEES_AMORTISSEMENT2 + " = ra." + RefAgsAmortissement.PROPERTY_DONNEES_AMORTISSEMENT2;
        query += " AND   rm." + RefMaterielOutil.PROPERTY_ACTIVE + " IS TRUE ";
        query += " AND   ra." + RefAgsAmortissement.PROPERTY_TYPE__MATERIEL + " = :typeMat";
        query +=       " AND   ra." + RefAgsAmortissement.PROPERTY_ACTIVE + " IS TRUE";
        query +=       " AND   rm IN (:materiels)";
        
        List<Object[]> result0 = findAll(query, DaoUtils.asArgsMap("materiels", materiels, "typeMat", MaterielType.OUTIL));
    
        Map<RefMateriel, RefAgsAmortissement> result = getRefMaterielRefAgsAmortissementMap(result0);
    
        return result;
    }
    
    protected Map<RefMateriel, RefAgsAmortissement> getRefMaterielRefAgsAmortissementMap(List<Object[]> result0) {
        Map<RefMateriel, RefAgsAmortissement> result = new HashMap<>();
        for (Object[] row : result0) {
            RefMateriel equipment = (RefMateriel) row[0];
            RefAgsAmortissement deprecationRate = (RefAgsAmortissement) row[1];
            result.put(equipment, deprecationRate);
        }
        return result;
    }
} //RefAgsAmortissementTopiaDao
