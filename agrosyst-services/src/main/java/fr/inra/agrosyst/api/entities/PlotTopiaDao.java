package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.edaplos.PlotFilter;
import fr.inra.agrosyst.services.security.SecurityContext;
import fr.inra.agrosyst.services.security.SecurityHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * @author cosse
 */
public class PlotTopiaDao extends AbstractPlotTopiaDao<Plot> {

    protected static final String PROPERTY_DOMAIN_ID = Plot.PROPERTY_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID;
    protected static final String PROPERTY_DOMAIN_CAMPAIGN = Plot.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN;

    public List<Plot> findAllByDomainId(String domainId) {
        List<Plot> result = forProperties(PROPERTY_DOMAIN_ID, domainId).findAll();
        return result;
    }
    
    public List<Plot> findAllOrderByNameForDomain(Domain domain) {
        StringBuilder query = new StringBuilder(" FROM " + Plot.class.getName() + " p ");
        query.append(" WHERE p." + Plot.PROPERTY_DOMAIN + " = :domain ");

        Map<String, Object> args = DaoUtils.asArgsMap("domain", domain);

        query.append(" ORDER BY p." + Plot.PROPERTY_NAME);


        List<Plot> result = findAll(query.toString(), args);
        return result;
    }

    public List<Plot> findAllFreePlotInDomain(String domainId) {
        String query = "FROM " + Plot.class.getName() + " P";
        query += " WHERE " + PROPERTY_DOMAIN_ID + " = :domainId";
        query += " AND " + Plot.PROPERTY_GROWING_SYSTEM + " is null";
        List<Plot> result = findAll(query, DaoUtils.asArgsMap("domainId", domainId));
        return result;
    }

    /**
     * Find all plot using same plot's duplication code.
     *
     * @return related plots
     */
    public LinkedHashMap<Integer, String> findAllRelatedPlotIdsByCampaigns(String code) {
        String query = "SELECT " + PROPERTY_DOMAIN_CAMPAIGN + ", " + Plot.PROPERTY_TOPIA_ID
                + " FROM " + getEntityClass().getName()
                + " WHERE " + Plot.PROPERTY_CODE + " = :code"
                + " ORDER BY " + PROPERTY_DOMAIN_CAMPAIGN + " DESC";
        List<Object[]> plots = findAll(query, DaoUtils.asArgsMap("code", code));
        LinkedHashMap<Integer, String> result = DaoUtils.toRelatedMap(plots);
        return result;
    }

    /**
     * Get domain'plot total area.
     * 
     * @param domainId domain id
     * @return total area
     */
    public double getDomainPlotTotalArea(String domainId) {
        String query = "SELECT sum(" + Plot.PROPERTY_AREA + ")"
                + " FROM " + Plot.class.getName()
                + " WHERE " + PROPERTY_DOMAIN_ID + " = :domainId"
                + " AND " + Plot.PROPERTY_ACTIVE + " = true";
        Double totalArea = findUniqueOrNull(query, DaoUtils.asArgsMap("domainId", domainId));
        totalArea = totalArea == null ? 0d : totalArea;
        return totalArea;
    }

    public Long countAllGrowingSystemsForPlots(Set<String> plotTopiaIds) {
        Preconditions.checkArgument(plotTopiaIds != null && !plotTopiaIds.isEmpty());
        StringBuilder query = new StringBuilder(
                "SELECT count(DISTINCT p." + Plot.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID + ") FROM " + Plot.class.getName() + " p ");
        query.append(" WHERE 1 = 1");

        Map<String, Object> args = Maps.newLinkedHashMap();

        query.append(DaoUtils.andAttributeIn("p", Plot.PROPERTY_TOPIA_ID, args, DaoUtils.getRealIds(plotTopiaIds, Plot.class)));

        long result = findUnique(query.toString(), args);

        return result;
    }

    public List<Plot> findAllForFilter(PlotFilter filter) {
        
        Map<String, Object> args = DaoUtils.asArgsMap();
    
        String query = " FROM " + Plot.class.getName() + " p ";
        
        query +=" WHERE 1 = 1";
        
        query += DaoUtils.andAttributeEquals("p", Plot.PROPERTY_DOMAIN, args, filter.getDomain());
        
        query += DaoUtils.andAttributeEquals("p", Plot.PROPERTY_E_DAPLOS_ISSUER_ID, args, null);

        if (filter.getArea() != null) {
            query += DaoUtils.andAttributeEquals("p", Plot.PROPERTY_AREA, args, filter.getArea());
        }
    
        if (filter.getActive() != null) {
            query += DaoUtils.andAttributeEquals("p", Plot.PROPERTY_ACTIVE, args, filter.getActive());
        }
    
        List<Plot> result = findAll(query, args);
    
        return result;
    }
    
    public PaginationResult<Plot> findWithDomainAndSdcFilter(SecurityContext securityContext,
                                                             PaginationParameter page,
                                                             Collection<String> selectedIds,
                                                             Collection<String> domainIds,
                                                             Collection<String> growingSystemIds,
                                                             Boolean active) {
        
        Map<String, Object> args = new LinkedHashMap<>();
        
        String query = " FROM " + Plot.class.getName() + " p ";
        query += " WHERE 1 = 1 ";
        
        if (CollectionUtils.isNotEmpty(selectedIds)) {
            query += DaoUtils.andAttributeIn("p", Plot.PROPERTY_TOPIA_ID, args, DaoUtils.getRealIds(selectedIds, Plot.class));
        }
        if (CollectionUtils.isNotEmpty(domainIds)) {
            query += DaoUtils.andAttributeIn("p", Plot.PROPERTY_DOMAIN + "." + TopiaEntity.PROPERTY_TOPIA_ID, args, DaoUtils.getRealIds(domainIds, Domain.class));
        }
        if (CollectionUtils.isNotEmpty(growingSystemIds)) {
            query += DaoUtils.andAttributeIn("p", Plot.PROPERTY_GROWING_SYSTEM + "." + TopiaEntity.PROPERTY_TOPIA_ID, args, DaoUtils.getRealIds(growingSystemIds, GrowingSystem.class));
        }
        if (active != null) {
            query += DaoUtils.andAttributeEquals("p", Plot.PROPERTY_ACTIVE, args, active);
        }
        
        Optional<Set<String>> grantedPlotIds = this.toGrantedPlotIds(securityContext);
        String plotId = "p." + Plot.PROPERTY_TOPIA_ID;
        if (grantedPlotIds.isPresent()) {
            query += newComputedCacheHelper().andIdsIn(plotId, args, grantedPlotIds.get());
        }
        
        long totalCount = findUnique("SELECT count(*) " + query, args);
        
        String findQuery = query +
                " ORDER BY p." + Plot.PROPERTY_NAME + ", " +
                " p." + TopiaEntity.PROPERTY_TOPIA_ID;
        List<Plot> plots = find(findQuery, args, page);

        // build result bean
        PaginationResult<Plot> result = PaginationResult.of(plots, totalCount, page);
        return result;
    }

    public void updateStatus(List<String> plotTopiaIds, boolean isToActivated) {
        Map<String, Object> args = DaoUtils.asArgsMap("isToActivated", isToActivated, "plotIds", plotTopiaIds);
        
        String whereQuery;
        whereQuery = " WHERE p." + Plot.PROPERTY_TOPIA_ID + " IN :plotIds";

        
        topiaJpaSupport.execute("UPDATE " + getEntityClass().getName() + " p" +
                " SET p." + Plot.PROPERTY_ACTIVE + "= :isToActivated " + whereQuery, args);
    }

    /**
     * Calcule la liste des identifiants de Plot sur laquelle filtrer. La liste retournée contient les identifiants de
     * Plot que l'utilisateur a le droit de voir
     *
     * @param securityContext le contexte de sécurité nécessaire pour le calcul de droits.
     * @return Optional.empty s'il ne faut pas filtrer ou bien la liste exhaustive des Plot que l'utilisateur a le droit
     * de voir.
     */
    public Optional<Set<String>> toGrantedPlotIds(SecurityContext securityContext) {
        Optional<Set<String>> result = Optional.empty();
        {
            Map<String, Object> args = new HashMap<>();
            Optional<String> query = SecurityHelper.createPlotFilterQueryForDomain(args, securityContext);
            if (query.isPresent()) {
                List<String> ids = topiaJpaSupport.findAll(query.get(), args);
                Set<String> idsSet = new HashSet<>(ids);
                result = Optional.of(idsSet);
            }
        }
        {
            Map<String, Object> args = new HashMap<>();
            Optional<String> query = SecurityHelper.createPlotFilterQueryForGrowingSystem(args, securityContext);
            if (query.isPresent()) {
                List<String> ids = topiaJpaSupport.findAll(query.get(), args);
                Set<String> idsSet = new HashSet<>(ids);
                if (result.isPresent()) {
                    result.get().addAll(idsSet);
                } else {
                    result = Optional.of(idsSet);
                }
            }
        }
        return result;
    }
    
    public List<Plot> findAllActiveGrantedByDomainId(SecurityContext securityContext, String domainId, Set<String> growingSystemIds) {
        ImmutableSet<String> domainIdsSet = ImmutableSet.of(domainId);
        PaginationResult<Plot> paginationResult = findWithDomainAndSdcFilter(
                securityContext,
                PaginationParameter.ALL,
                null,
                domainIdsSet,
                growingSystemIds,
                null
        );
        return paginationResult.getElements();
    }

}
