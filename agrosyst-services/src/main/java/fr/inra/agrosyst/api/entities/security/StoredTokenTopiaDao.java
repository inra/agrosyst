package fr.inra.agrosyst.api.entities.security;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.utils.DaoUtils;
import org.nuiton.topia.persistence.TopiaEntity;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StoredTokenTopiaDao extends AbstractStoredTokenTopiaDao<StoredToken> {

    public long getConnectedUserCount(LocalDateTime currentDate) {
        LocalDateTime oneDayAgo = currentDate.minusDays(1);

        String hql =
                " SELECT st."+ StoredToken.PROPERTY_USER_ID + ", MAX(st." + StoredToken.PROPERTY_TOPIA_CREATE_DATE + ") " +
                " FROM " + getEntityClass().getName() + " st " +
                " GROUP BY st." + StoredToken.PROPERTY_USER_ID;
        List<Object[]> rows = findAll(hql);

        Set<String> uniqueUserIds = new HashSet<>();
        for (Object[] row : rows) {
            String userId = (String)row[0];
            LocalDateTime date = convertToLocalDate((Date)row[1]);

            // Déconnexion auto des utilisateurs connectés depuis plus de 24h
            if (date.isBefore(oneDayAgo)) {
                List<StoredToken> all = forUserIdEquals(userId).findAll();
                deleteAll(all);
            } else {
                uniqueUserIds.add(userId);
            }
        }
        int result = uniqueUserIds.size();
        return result;
    }

    protected LocalDateTime convertToLocalDate(Date date) {
        LocalDateTime ld = new Timestamp(date.getTime()).toLocalDateTime();
        return ld;
    }

    public void removeAllTokens() {
        topiaJpaSupport.execute("DELETE FROM " + StoredToken.class.getName() + " st WHERE st." + TopiaEntity.PROPERTY_TOPIA_ID + " IS NOT NULL", DaoUtils.asArgsMap());
    }

} //StoredTokenTopiaDao
