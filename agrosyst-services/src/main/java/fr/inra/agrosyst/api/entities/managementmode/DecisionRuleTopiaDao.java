package fr.inra.agrosyst.api.entities.managementmode;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.SortedColumn;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.services.managementmode.DecisionRuleFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.security.SecurityContext;
import fr.inra.agrosyst.services.security.SecurityHelper;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DecisionRuleTopiaDao extends AbstractDecisionRuleTopiaDao<DecisionRule> {

    public PaginationResult<DecisionRule> getFilteredDecisionRules(DecisionRuleFilter filter, SecurityContext securityContext) throws TopiaException {
        StringBuilder query = new StringBuilder("FROM " + DecisionRule.class.getName() + " dr");

        if (filter != null && StringUtils.isNotBlank(filter.getDomainName())) {
            query.append(" JOIN ").append(Domain.class.getName()).append(" d ON d.code = dr.domainCode");
            query.append(" WHERE d.campaign = (SELECT max(campaign) FROM ").append(Domain.class.getName()).append(" d2 WHERE d2.code = d.code) ");
        } else {
            query.append(" WHERE 1 = 1 ");
        }
        Map<String, Object> args = new HashMap<>();
    
        // FIXME echatellier 20140218 : this is a huge hack
        applyFiltersOnQuery(filter, securityContext, query, args);
    
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
    
        String filterOrderBy = getFilterOrderBy(filter);
    
        List<DecisionRule> decisionRules = find("SELECT dr " + query + filterOrderBy, args, startIndex, endIndex);
        long totalCount = findUnique("SELECT count(DISTINCT dr) " + query, args);
    
        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        return PaginationResult.of(decisionRules, totalCount, pager);
    }

    public Set<String> getFilteredDecisionRuleIds(DecisionRuleFilter filter, SecurityContext securityContext) throws TopiaException {
        StringBuilder query = new StringBuilder("SELECT dr.topiaId FROM " + DecisionRule.class.getName() + " dr ");
        query.append(" WHERE 1 = 1 ");
        Map<String, Object> args = new HashMap<>();
        applyFiltersOnQuery(filter, securityContext, query, args);
        return new HashSet<>(findAll(query.toString(), args));
    }

    private void applyFiltersOnQuery(DecisionRuleFilter filter, SecurityContext securityContext, StringBuilder query, Map<String, Object> args) {
        // FIXME echatellier 20140218 : this is a huge hack
        query.append(DaoUtils.andAttributeEquals("dr", DecisionRule.PROPERTY_VERSION_NUMBER, args, 1));

        // apply non null filter
        if (filter != null) {
            query.append(DaoUtils.andAttributeEquals("dr", DecisionRule.PROPERTY_ACTIVE, args, filter.getActive()));
            // decision rule name
            query.append(DaoUtils.andAttributeLike("dr", DecisionRule.PROPERTY_NAME, args, filter.getDecisionRuleName()));
            // intervention type
            query.append(DaoUtils.andAttributeEquals("dr", DecisionRule.PROPERTY_INTERVENTION_TYPE, args, filter.getInterventionType()));
            // domain name
            if (StringUtils.isNotBlank(filter.getDomainName())) {
                query.append(DaoUtils.andAttributeLike("d", Domain.PROPERTY_NAME, args, filter.getDomainName()));
            }

            // Navigation context
            NavigationContext navigationContext = filter.getNavigationContext();
            if (navigationContext != null) {
                if (navigationContext.getCampaignsCount() == 0 &&
                        navigationContext.getNetworksCount() == 0 &&
                        navigationContext.getDomainsCount() == 0 &&
                        navigationContext.getGrowingPlansCount() == 0 &&
                        navigationContext.getGrowingSystemsCount() == 0) {

                    // we load all domain's code
                    Set<String> domainCodes = getProjectionHelper().domainsToDomainCodes(null, null);
                    query.append(DaoUtils.andAttributeIn("dr", DecisionRule.PROPERTY_DOMAIN_CODE, args, domainCodes));

                } else {
                    // campaigns
                    if (navigationContext.getCampaignsCount() > 0 &&
                            navigationContext.getNetworksCount() == 0 &&
                            navigationContext.getDomainsCount() == 0 &&
                            navigationContext.getGrowingPlansCount() == 0 &&
                            navigationContext.getGrowingSystemsCount() == 0) {
                        Set<String> domainCodes = getProjectionHelper().campaignsToDomainCodes(navigationContext.getCampaigns(), true);
                        query.append(DaoUtils.andAttributeIn("dr", DecisionRule.PROPERTY_DOMAIN_CODE, args, domainCodes));
                    }
                    // networks
                    if (navigationContext.getNetworksCount() > 0) {
                        Set<String> domainCodes = getProjectionHelper().networksToDomainsCode(navigationContext.getNetworks(), true);
                        query.append(DaoUtils.andAttributeIn("dr", DecisionRule.PROPERTY_DOMAIN_CODE, args, domainCodes));
                    }
                    // domains
                    if (navigationContext.getDomainsCount() > 0) {
                        Set<String> domainCodes = getProjectionHelper().domainsToDomainCodes(navigationContext.getDomains(), null);
                        query.append(DaoUtils.andAttributeIn("dr", DecisionRule.PROPERTY_DOMAIN_CODE, args, domainCodes));
                    }
                    // growingPlan
                    if (navigationContext.getGrowingPlansCount() > 0) {
                        Set<String> domainCodes = getProjectionHelper().growingPlansToDomainCodes(navigationContext.getGrowingPlans(), true);
                        query.append(DaoUtils.andAttributeIn("dr", DecisionRule.PROPERTY_DOMAIN_CODE, args, domainCodes));
                    }
                    // growingSystem
                    if (navigationContext.getGrowingSystemsCount() > 0) {
                        Set<String> domainCodes = getProjectionHelper().growingSystemsToDomainCodes(navigationContext.getGrowingSystems(), true);
                        query.append(DaoUtils.andAttributeIn("dr", DecisionRule.PROPERTY_DOMAIN_CODE, args, domainCodes));
                    }
                }
            } else {
                // we load all domain's code
                Set<String> domainCodes = getProjectionHelper().domainsToDomainCodes(null, null);
                query.append(DaoUtils.andAttributeIn("dr", DecisionRule.PROPERTY_DOMAIN_CODE, args, domainCodes));
            }
        }
    
        SecurityHelper.addDecisionRuleFilter(query, args, securityContext, "dr");
    }
    
    /**
     * @return - user selected ORDER BY or DEFAULT one
     */
    private String getFilterOrderBy(DecisionRuleFilter filter) {
        String result = null;
        
        if (filter != null && filter.getSortedColumn() != null) {
            SortedColumn sortedColumn = filter.getSortedColumn();
            String filterOrderBy = switch (sortedColumn) {
                case DECISION_RULE -> " lower (dr." + DecisionRule.PROPERTY_NAME + ")";
                case DOMAIN -> StringUtils.isNotBlank(filter.getDomainName()) ?" lower (d." + Domain.PROPERTY_NAME + ")" : " ";
                default -> null;
            };
            if (StringUtils.isNotBlank(filterOrderBy)) {
                filterOrderBy += " " + filter.getSortOrder().name();
                result = " ORDER BY " +
                        filterOrderBy +
                        ", dr." + TopiaEntity.PROPERTY_TOPIA_ID;
            }
            
        }
        
        if (result == null) {
            // default
            result = " ORDER BY dr." + DecisionRule.PROPERTY_INTERVENTION_TYPE +
                    ", lower (dr." + DecisionRule.PROPERTY_NAME + ")" +
                    ", dr." + DecisionRule.PROPERTY_TOPIA_ID;
        }
        
        return result;
    }
    
    public List<DecisionRule> findAllRelatedDecisionRules(String code) {
        String query = " FROM " + getEntityClass().getName()
                + " WHERE " + DecisionRule.PROPERTY_CODE + " = :code"
                + " ORDER BY " + DecisionRule.PROPERTY_VERSION_NUMBER + " ASC";
        return findAll(query, DaoUtils.asArgsMap("code", code));
    }
    
    public DecisionRule getLastRuleVersion(String code) {
        String query = "FROM " + DecisionRule.class.getName() + " dr";
        query += " WHERE 1 = 1";
        query += " AND dr." + DecisionRule.PROPERTY_CODE + " = :code ";

        String hql = query + " ORDER BY dr." + DecisionRule.PROPERTY_VERSION_NUMBER + " DESC";
        Map<String, Object> args = DaoUtils.asArgsMap("code", code);

        return findFirstOrNull(hql, args);
    }
}
