package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.entities.referential.TradRefDestination;
import fr.inra.agrosyst.api.entities.referential.TradRefDivers;
import fr.inra.agrosyst.api.entities.referential.TradRefIntervention;
import fr.inra.agrosyst.api.entities.referential.TradRefIntrant;
import fr.inra.agrosyst.api.entities.referential.TradRefMateriel;
import fr.inra.agrosyst.api.entities.referential.TradRefSol;
import fr.inra.agrosyst.api.entities.referential.TradRefVivant;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.AgrosystServiceConfig;
import fr.inra.agrosyst.services.DefaultServiceContext;
import fr.inra.agrosyst.services.ServiceContext;
import fr.inra.agrosyst.services.async.BusinessTasksManager;
import fr.inra.agrosyst.services.common.CacheDiscriminator;
import fr.inra.agrosyst.services.common.CacheService;
import fr.inra.agrosyst.services.cron.CheckSynchroHelperHealthJob;
import fr.inra.agrosyst.services.cron.CronHelper;
import fr.inra.agrosyst.services.cron.PurgeComputedUserPermissionsJob;
import fr.inra.agrosyst.services.cron.PurgeDownloadablesJob;
import fr.inra.agrosyst.services.internal.InstancesSynchroHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaConfiguration;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.apache.logging.log4j.core.config.Configurator.initialize;

/**
 * Agrosyst application context.
 *
 * @author Eric Chatellier
 */
public class AgrosystTopiaApplicationContext extends AbstractAgrosystTopiaApplicationContext {

    protected static final AtomicInteger nbTry = new AtomicInteger(0);

    private static final Log LOGGER = LogFactory.getLog(AgrosystTopiaApplicationContext.class);

    protected AgrosystServiceConfig config;

    protected InstancesSynchroHelper synchroHelper;
    protected CronHelper cronHelper;
    protected BusinessTasksManager taskManager;

    public AgrosystTopiaApplicationContext(AgrosystServiceConfig config, TopiaConfiguration topiaConfiguration) {
        super(topiaConfiguration);
        this.config = config;
        init0();// call the super.init() not yet executed
    }

    @Override
    public List<Class<?>> getPersistenceClasses() {
        List<Class<?>> result = new LinkedList<>(super.getPersistenceClasses());
        result.add(ComputedCache.class);
        result.add(TradRefVivant.class);
        result.add(TradRefSol.class);
        result.add(TradRefMateriel.class);
        result.add(TradRefIntervention.class);
        result.add(TradRefDestination.class);
        result.add(TradRefDivers.class);
        result.add(TradRefIntrant.class);
        return result;
    }

    @Override
    protected void init() {
        // Do nothing now as context is not setup and logs are not enable
    }

    private void init0() {

        applicationInit();

        startTaskManager();

        boolean done = false;
        while (!done && nbTry.get() <= 6) {
            try {
                if (LOGGER.isInfoEnabled()) {
                    LOGGER.info("Agrosyst is started, running migration will be done if some.");
                }

                super.init();

                afterMigrate();

                done = true;
            } catch (Exception e) {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("Error during TopiaApplicationContext initialisation", e);
                }
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("Failed to initialize application context ! Next try n°" + (nbTry.get() + 1) + " in 30s");
                }
                nbTry.incrementAndGet();
                try {
                    TimeUnit.SECONDS.sleep(30);
                } catch (InterruptedException e1) {
                    // nothing to do
                    LOGGER.error(e1);
                }
            }
        }
        if (!done) {
            throw new AgrosystTechnicalException("Failed to initialize application context !");
        }

        startSynchroWatchers();

        startCronScheduler();
    }

    private void afterMigrate() {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Changing performances status to failed");
        }
        final AgrosystTopiaPersistenceContext agrosystTopiaPersistenceContext = super.newPersistenceContext();
        agrosystTopiaPersistenceContext.getPerformanceDao().changePerformanceStatusGeneratingToFailedAtApplicationStart();
        agrosystTopiaPersistenceContext.commit();
        agrosystTopiaPersistenceContext.close();
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Migration done !");
        }
    }

    /**
     * Init application.
     *
     * <ul>
     * <li>Creation application schema</li>
     * <li>Update log4j configuration</li>
     * </ul>
     */
    public void applicationInit() {

        // replace application logging to custom file
        String logFileLocation = config.getLogFileLocation();
        if (StringUtils.isNotBlank(logFileLocation)) {

            File log4jConfigurationFile = new File(logFileLocation);
            String log4jConfigurationFileAbsolutePath = log4jConfigurationFile.getAbsolutePath();

            if (log4jConfigurationFile.exists()) {

                if (LOGGER.isInfoEnabled()) {
                    LOGGER.info("will use logging configuration " + log4jConfigurationFileAbsolutePath);
                }

                initialize(null, log4jConfigurationFileAbsolutePath);

            } else {
                if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn("there is no file " + log4jConfigurationFileAbsolutePath + ". Default logging configuration will be used.");
                }
            }
        }
    }

    public void setConfig(AgrosystServiceConfig config) {
        this.config = config;
    }

    public AgrosystServiceConfig getConfig() {
        return config;
    }

    public ServiceContext newServiceContext() {
        DefaultServiceContext result = new DefaultServiceContext(this.config, this::newPersistenceContext, this.synchroHelper, this.taskManager);
        return result;
    }

    public ServiceContext newServiceContext(AuthenticatedUser authenticatedUser) {
        Preconditions.checkArgument(authenticatedUser != null, "Ça n'a pas de sens d'appeler cette méthode sans token d'authentification");
        DefaultServiceContext result = new DefaultServiceContext(this.config, this::newPersistenceContext, this.synchroHelper, this.taskManager, authenticatedUser);
        return result;
    }

    protected void startTaskManager() {
        this.taskManager = new BusinessTasksManager(config, this::newServiceContext);
    }

    public BusinessTasksManager getTaskManager() {
        return taskManager;
    }

    protected void shutdownTaskManager() {
        // TODO AThimel 09/02/2021 Implement
        LOGGER.info("Task is shutting down");
    }

    protected void startSynchroWatchers() {
        synchroHelper = new InstancesSynchroHelper(config);
        synchroHelper.watchConfigReload(this.config::reload);
        synchroHelper.watchClearCache(this::clearLocalCaches);
        synchroHelper.watchClearSingleCache(this::clearLocalSingleCache);
        synchroHelper.watchPing();
    }

    protected void shutdownSynchroWatchers() {
        try {
            if (synchroHelper != null) {
                synchroHelper.shutdown();
            }
        } catch (Exception eee) {
            LOGGER.error("Unable to start SynchroWatchers", eee);
        }
    }

    protected void clearLocalCaches() {
        try (ServiceContext serviceContext = newServiceContext()) {
            CacheService cacheService = serviceContext.newService(CacheService.class);
            cacheService.clear();
        } catch (Exception eee) {
            LOGGER.error("Unable to clearLocalCaches", eee);
        }
    }

    protected void clearLocalSingleCache(String discriminatorAsString) {
        try (ServiceContext serviceContext = newServiceContext()) {
            CacheService cacheService = serviceContext.newService(CacheService.class);
            CacheDiscriminator cacheDiscriminator = new CacheDiscriminator(discriminatorAsString);
            cacheService.clearSingle(cacheDiscriminator);
        } catch (Exception eee) {
            LOGGER.error("Unable to clearLocalSingleCache", eee);
        }
    }

    protected void startCronScheduler() {
        if (config.isCronJobsEnabled()) {
            cronHelper = new CronHelper(this);

            cronHelper.scheduleJob(60, PurgeDownloadablesJob.class);
            cronHelper.scheduleJob(42, PurgeComputedUserPermissionsJob.class);
            if (config.getJmsUrl().isPresent()) {
                cronHelper.scheduleJob(1, CheckSynchroHelperHealthJob.class);
            }
        } else {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("cronScheduler is not enabled");
            }
        }
    }

    protected void shutdownCronScheduler() {
        if (config.isCronJobsEnabled()) {
            cronHelper.shutdown();
        }
    }

    @Override
    public void close() {
        shutdownCronScheduler();
        shutdownSynchroWatchers();
        shutdownTaskManager();
        super.close();
    }

} //AgrosystTopiaApplicationContext
