package fr.inra.agrosyst.api.entities.migration;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.services.referential.csv.RefOtherInputModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.nuiton.csv.Import;

import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class V3_0_0_14__11710_add_unit_to_ref_other_input extends BaseJavaMigration {

    private static final Log LOG = LogFactory.getLog(V3_0_0_14__11710_add_unit_to_ref_other_input.class);
    //                                   1          2         3              4             5                6             7             8       9       10      11           12           13
    public static final String INSERT_INTO_REF_OTHER_INPUT_QUERY = """
            INSERT INTO refotherinput (topiaid,topiaversion,inputtype_c0,caracteristic2,caracteristic3,topiacreatedate,caracteristic1,lifetime,active,source,reference_id,reference_code, unit)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            """;

    public static final String UPDATE_REF_OTHER_INPUT_SECTOR_QUERY = "UPDATE refotherinput SET unit = ? WHERE reference_id = ?";

    public static final String INSERT_INTO_REF_OTHER_INPUT_SECTOR_QUERY = """
            INSERT INTO refotherinput_sector (owner, sector)
            VALUES (?, ?)
            """;
    private static  Set<String> loadSeedPrices(
            Connection connection) {

        Set<String> refOtherInputIds = new HashSet<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet refotherinputRsIds = statement.executeQuery("SELECT reference_id from refotherinput");
            while (refotherinputRsIds.next()) {
                refOtherInputIds.add(refotherinputRsIds.getString(1));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return refOtherInputIds;
    }

    @Override
    public void migrate(Context context) {
        Connection connection = context.getConnection();

        Set<String> refOtherInputIds = loadSeedPrices(connection);
        LOG.info(" Ajout des données de référentiel pour les intrants autre 'RefOtherInput'");
        try (PreparedStatement refOtherInputSectorStatement = connection.prepareStatement(INSERT_INTO_REF_OTHER_INPUT_SECTOR_QUERY)) {
            try (PreparedStatement refOtherInputStatement = connection.prepareStatement(INSERT_INTO_REF_OTHER_INPUT_QUERY)) {
                try (PreparedStatement updateOtherInputStatement = connection.prepareStatement(UPDATE_REF_OTHER_INPUT_SECTOR_QUERY)) {

                    try (Import<RefOtherInput> refOtherInputs = Import.newImport(new RefOtherInputModel(true),
                            new InputStreamReader(
                                    Objects.requireNonNull(getClass().getResourceAsStream("/db/migration/V3_0_0_14__11710_add_unit_to_ref_other_input.csv")),
                                    StandardCharsets.UTF_8
                            ))) {
                        for (RefOtherInput input : refOtherInputs) {
                            if (input.getUnit() != null) {
                                if (refOtherInputIds.contains(input.getReference_id())) {

                                    updateOtherInputStatement.setString(1, input.getUnit().name());
                                    updateOtherInputStatement.setString(2, input.getReference_id());

                                    updateOtherInputStatement.addBatch();
                                    LOG.info("UPDATE " + input.getReference_id());

                                } else {

                                    String refOtherInputId = RefOtherInput.class.getName() + "_" + UUID.randomUUID();
                                    refOtherInputStatement.setString(1, refOtherInputId);
                                    refOtherInputStatement.setInt(2, 0);
                                    refOtherInputStatement.setString(3, input.getInputType_c0());
                                    refOtherInputStatement.setString(4, input.getCaracteristic2());
                                    refOtherInputStatement.setString(5, input.getCaracteristic3());
                                    refOtherInputStatement.setDate(6, new java.sql.Date(new java.util.Date().getTime()));
                                    refOtherInputStatement.setString(7, input.getCaracteristic1());
                                    refOtherInputStatement.setDouble(8, input.getLifetime());
                                    refOtherInputStatement.setBoolean(9, true);
                                    refOtherInputStatement.setString(10, input.getSource());
                                    refOtherInputStatement.setString(11, input.getReference_id());
                                    refOtherInputStatement.setString(12, input.getReference_code());
                                    refOtherInputStatement.setString(13, input.getUnit().name());

                                    refOtherInputStatement.addBatch();
                                    LOG.info("INSERT " + input.getReference_id());

                                    if (CollectionUtils.isNotEmpty(input.getSector())) {
                                        for (Sector sector : input.getSector()) {
                                            refOtherInputSectorStatement.setString(1, refOtherInputId);
                                            refOtherInputSectorStatement.setString(2, sector.name());

                                            refOtherInputSectorStatement.addBatch();
                                            LOG.info("ADD SECTOR TO INPUT " + input.getSector() + ", " + input.getReference_id());
                                        }
                                    }
                                }
                            }
                        }
                        updateOtherInputStatement.executeBatch();
                        refOtherInputStatement.executeBatch();
                        refOtherInputSectorStatement.executeBatch();
                    }
                }

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
