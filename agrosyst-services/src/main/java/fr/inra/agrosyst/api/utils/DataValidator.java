package fr.inra.agrosyst.api.utils;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2016 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

/**
 * Util class that aims to validate some data format.
 *
 * @author ymartel (martel@codelutin.com)
 */
public class DataValidator {
    
    public static final String SIRET_REGEX = "[0-9]{3}[\\s]?[0-9]{3}[\\s]?[0-9]{3}[\\s]?[0-9]{5}";

    /**
     * Check that given SIRET has good format
     * @return true if SIRET is valid.
     */
    public static boolean isSiretValid(String siret) {
        return StringUtils.isBlank(siret) || StringUtils.isNotBlank(siret) && siret.matches(SIRET_REGEX) && luhnChecksum(siret);
    }
    
    /**
     * Check that given SIRET has good format
     * @return true if SIRET is valid.
     */
    public static boolean isEdaplosSiretValid(String siret) {
        return StringUtils.isNotBlank(siret) && siret.matches(SIRET_REGEX);
    }
    
    /**
     * Verifie la validite d'un numero en suivant l'algorithme Luhn tel que d'ecrit
     * dans <a href="http://fr.wikipedia.org/wiki/Luhn">wikipedia</a>
     *
     * Algo:
     * en fonction de la position du numero dans la sequence,
     * on multiplie par 1 (pour les impaires) ou par 2 pour les paires
     * (1 etant le numero le plus a droite)
     * On fait la somme de tous les chiffres qui resulte de ces multiplications
     * (si un resultat etait 14, on ne fait pas +14 mais +1+4)
     *
     * Si le résultat de cette somme donne un reste de 0 une fois divisé par 10
     * le numero est valide.
     *
     * @param value une chaine composer que de chiffre
     * @return vrai si on a reussi a valider le numero
     */
    public static boolean luhnChecksum(String value) {
    
        value = StringUtils.deleteWhitespace(value);
        
        char[] tab = value.toCharArray();
        int sum = 0;
        
        // on multiple alternativement par 1 ou par 2 les chiffres
        // sachant que le chiffre le plus à droite est multiplié par 1
        
        // l'offset permet de savoir si le premier chiffre( le plus a gauche) doit etre
        // multiplier par 1 ( offset = 0)
        // multiplier par 2 (offset = 1)
        int offset = (tab.length + 1) % 2;
        
        
        for (int i = 0; i < tab.length; i++) {
            
            // recuperation de la valeur
            int n = getDigit(tab[i]);
            
            // multiplication par 1 ou 2 en fonction de l'index et de l'offset
            n *= (i + offset) % 2 + 1;
            
            // si une fois multiplie il est superieur a 9, il faut additionner
            // toutes ces constituante, mais comme il ne peut pas etre superieur
            // a 18, cela revient a retrancher 9
            if (n > 9) {
                n -= 9;
            }
            
            // on peut directement faire la somme
            sum += n;
        }
        
        // 3eme phase on verifie que c'est bien un multiple de 10
        boolean result = sum % 10 == 0;
        
        return result;
    }
    
    /**
     * Converti un char en un entier '0' =&gt; 0 et '9' =&gt; 9, et 'A' =&gt; 1 a 'Z' =&gt; 36,
     * les autres caractere sont aussi convertis pour que
     * A|B|C|D|E|F|G|H|I|J
     * K|L|M|N|O|P|Q|R|S|T            z
     * U|V|W|X|Y|Z| | | |
     * -+-+-+-+-+-+-+-+-+
     * 1|2|3|4|5|6|7|8|9|0.
     * Pour les autres c'est un indedermine
     *
     * @param c le caractere qui doit etre converti
     * @return le chiffre
     */
    public static int getDigit(char c) {
        int result;
        if (c >= '0' && c <= '9') {
            result = c - '0';
        } else if (c >= 'A' && c <= 'Z') {
            result = (c - 'A' + 1) % 10;
        } else {
            result = (c - 'a' + 1) % 10;
        }
        return result;
    }
}
