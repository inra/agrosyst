package fr.inra.agrosyst.api.entities.practiced;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.utils.DaoUtils;

import java.util.List;
import java.util.Set;

public class PracticedCropCycleConnectionTopiaDao extends AbstractPracticedCropCycleConnectionTopiaDao<PracticedCropCycleConnection> {

    protected static final String PROPERTY_CYCLE_ID = PracticedCropCycleConnection.PROPERTY_TARGET + "." + PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE + "." + PracticedSeasonalCropCycle.PROPERTY_TOPIA_ID;

    public List<PracticedCropCycleConnection> findAllByCropCycle(String cycleId) {
        String query = "FROM " + getEntityClass().getName() + " ccnc";
        query += " WHERE " + PROPERTY_CYCLE_ID + " = :cycleId";
        List<PracticedCropCycleConnection> result = findAll(query, DaoUtils.asArgsMap("cycleId", cycleId));
        return result;
    }

    public List<PracticedCropCycleConnection> findAllByCropCycles(Set<String> cycleIds) {
        String query = "FROM " + getEntityClass().getName() + " ccnc";
        query += " WHERE " + PROPERTY_CYCLE_ID + " IN (:cycleIds)";
        List<PracticedCropCycleConnection> result = findAll(query, DaoUtils.asArgsMap("cycleIds", cycleIds));
        return result;
    }

} //PracticedCropCycleNodeConnectionDAOImpl<E extends CropCycleNodeConnection>
