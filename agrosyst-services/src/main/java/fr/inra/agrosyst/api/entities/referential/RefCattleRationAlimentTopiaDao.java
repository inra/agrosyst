package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.services.referential.RefCattleRationAlimentDto;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RefCattleRationAlimentTopiaDao extends AbstractRefCattleRationAlimentTopiaDao<RefCattleRationAliment> {
    public List<RefCattleRationAlimentDto> findAllActive(Language language) {
        I18nDaoHelper alimentTypeI18nDaoHelper =
                I18nDaoHelper.withSimpleI18nKey(
                        RefCattleRationAliment.PROPERTY_ALIMENT_TYPE,
                        language,
                        TradRefVivant.class,
                        "rcra"
                );
        I18nDaoHelper alimentUnitI18nDaoHelper =
                I18nDaoHelper.withSimpleI18nKey(
                        RefCattleRationAliment.PROPERTY_ALIMENT_UNIT,
                        language,
                        TradRefDivers.class,
                        "rcra"
                );

        String hql = String.join("\n"
                , "select rcra"
                        + ", " + alimentTypeI18nDaoHelper.coalesceTranslation()
                        + ", " + alimentUnitI18nDaoHelper.coalesceTranslation()
                , newFromClause("rcra")
                        + alimentTypeI18nDaoHelper.leftJoinTranslation()
                        + alimentUnitI18nDaoHelper.leftJoinTranslation()
                , "where rcra.active is true"
        );

        try (Stream<Object[]> stream = stream(hql)) {
            return stream.map(row -> {
                RefCattleRationAliment refCattleRationAliment = (RefCattleRationAliment) row[0];
                String alimentTypeTranslation = (String) row[1];
                String alimentUnitTranslation = (String) row[2];
                return new RefCattleRationAlimentDto(
                        refCattleRationAliment.getTopiaId(),
                        alimentTypeTranslation,
                        alimentUnitTranslation
                );
            }).collect(Collectors.toList());
        }
    }
}
