package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2025 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.utils.DaoUtils;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RefActaGroupeCulturesTopiaDao extends AbstractRefActaGroupeCulturesTopiaDao<RefActaGroupeCultures> {


    public @NotNull List<Integer> findGroupCultureActaSpeciesIds(int idCulture, int actaDosageSpcCroppingZonesGroupId) {
        Map<String, Object> args = new HashMap<>();
        String hql = "SELECT DISTINCT agc."+ RefActaGroupeCultures.PROPERTY_ID_GROUPE_CULTURE + " FROM " + getEntityClass().getName() + " agc WHERE 1 = 1 ";
                hql += DaoUtils.andAttributeEquals("agc", RefActaGroupeCultures.PROPERTY_ACTIVE, args, true);
                hql += DaoUtils.andAttributeEquals("agc", RefActaGroupeCultures.PROPERTY_ID_CULTURE, args, idCulture);
                hql += DaoUtils.andAttributeNotEquals("agc", RefActaGroupeCultures.PROPERTY_ID_GROUPE_CULTURE, args, actaDosageSpcCroppingZonesGroupId);

        List<Integer> res = findAll(hql, args);

        return res;
    }

} //RefActaGroupeCulturesTopiaDao
