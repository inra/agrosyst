package fr.inra.agrosyst.api.entities.migration;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class V3_0_16_1__12212__fix_migrate_qtavg_for_seedingproductinput extends BaseJavaMigration {

    @Override
    public void migrate(Context context) throws Exception {
        final String idsInputsToProcess = """
            select distinct(deprecatedid) from abstractinputusage a
            inner join seedproductinputusage spiu on a.topiaid = spiu.topiaid
            where deprecatedid is not null and deprecatedqtavg is not null and deprecatedqtavg > 0
            and qtavg = deprecatedqtavg;
        """;

        final String abstractInputUsagesByDeprecatedId = """
            select topiaid, deprecatedqtavg from abstractinputusage where deprecatedid = ?;
        """;

        final String updateAbstractInputUsageQtAvgById = """
            update abstractinputusage
            set qtavg = ?
            where topiaid = ?;
        """;

        ResultSet resultSelectedIds;
        try (Statement selectIds = context.getConnection().createStatement()) {
            resultSelectedIds = selectIds.executeQuery(idsInputsToProcess);

            while (resultSelectedIds.next()) {
                final List<AbstractInputUsageToUpdate> inputUsagesToUpdate = new LinkedList<>();

                try (PreparedStatement preparedStatement = context.getConnection().prepareStatement(abstractInputUsagesByDeprecatedId)) {
                    preparedStatement.setString(1, resultSelectedIds.getString(1));

                    try (ResultSet resultSetAbstractInputUsages = preparedStatement.executeQuery()) {
                        while (resultSetAbstractInputUsages.next()) {
                            final String topiaId = resultSetAbstractInputUsages.getString(1);
                            final Double deprecatedQtAvg = resultSetAbstractInputUsages.getDouble(2);

                            inputUsagesToUpdate.add(new AbstractInputUsageToUpdate(topiaId, deprecatedQtAvg));
                        }
                    }

                    final int nbEspeces = inputUsagesToUpdate.size();
                    if (nbEspeces == 1) continue;

                    // Pour que la somme des qtavg soit bien égale à la quantité initiale, on calcule la nouvelle
                    // qtavg pour tous les éléments sauf le dernier de la liste, qui quant à lui prendra pour valeur
                    // la qtavg initiale moins la somme des qtavg calculées.
                    double sum = 0.0;
                    for (int i = 0; i < inputUsagesToUpdate.size() - 1; i++) {
                        final AbstractInputUsageToUpdate inputUsageToUpdate = inputUsagesToUpdate.get(i);

                        // qtavg doit être arrondie à 3 chiffres après la virgule
                        final double newQtAvg = inputUsageToUpdate.deprecatedQtAvg / nbEspeces;
                        inputUsageToUpdate.qtAvg = new BigDecimal(newQtAvg).setScale(3, RoundingMode.HALF_UP).doubleValue();

                        sum += inputUsageToUpdate.qtAvg;
                    }
                    final AbstractInputUsageToUpdate lastInputUsageToUpdate = inputUsagesToUpdate.get(inputUsagesToUpdate.size() - 1);
                    lastInputUsageToUpdate.qtAvg = lastInputUsageToUpdate.deprecatedQtAvg - sum;

                    for (AbstractInputUsageToUpdate inputUsageToUpdate : inputUsagesToUpdate) {
                        try (final PreparedStatement update = context.getConnection().prepareStatement(updateAbstractInputUsageQtAvgById)) {
                            update.setDouble(1, inputUsageToUpdate.qtAvg);
                            update.setString(2, inputUsageToUpdate.topiaId);

                            update.executeUpdate();
                        }
                    }
                }
            }
        }

    }

    private static class AbstractInputUsageToUpdate {

        private final String topiaId;
        private final Double deprecatedQtAvg;
        private Double qtAvg;

        public AbstractInputUsageToUpdate(String topiaId, Double deprecatedQtAvg) {
            this.topiaId = topiaId;
            this.deprecatedQtAvg = deprecatedQtAvg;
            this.qtAvg = 0.0;
        }

    }
}
