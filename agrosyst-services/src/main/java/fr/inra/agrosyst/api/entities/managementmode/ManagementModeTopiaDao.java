package fr.inra.agrosyst.api.entities.managementmode;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.SortedColumn;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeDto;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.security.SecurityContext;
import fr.inra.agrosyst.services.security.SecurityHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ManagementModeTopiaDao extends AbstractManagementModeTopiaDao<ManagementMode> {

    private static final Log LOGGER = LogFactory.getLog(ManagementModeTopiaDao.class);

    public static final String SELECT_GS_MM_ID_MM_TYPE = "SELECT " +
            "mm." + ManagementMode.PROPERTY_GROWING_SYSTEM + ", " +
            "mm." + ManagementMode.PROPERTY_TOPIA_ID + ", " +
            "mm." + ManagementMode.PROPERTY_CATEGORY + " FROM " + ManagementMode.class.getName() + " mm ";

    protected static final String PROPERTY_GROWING_SYSTEM_NAME = ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_NAME;
    protected static final String PROPERTY_DOMAIN_CAMPAIGN = ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN;
    protected static final String PROPERTY_DOMAIN_ID = ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID;
    protected static final String PROPERTY_DOMAIN_NAME = ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_NAME;
    protected static final String PROPERTY_GROWING_PLAN_ID = ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_TOPIA_ID;
    protected static final String PROPERTY_GROWING_PLAN_NAME = ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_NAME;
    public static final String PROPERTY_GROWING_SYSTEM_ID = ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID;
    
    public Pair<Map<GrowingSystem, ManagementModeDto>, PaginationResult<ManagementModeDto>> getFilteredManagementModeDtos(
            ManagementModeFilter filter, SecurityContext securityContext) throws TopiaException {
        
        long t0 = System.currentTimeMillis();
        String growingSystemManagementModeQuery0 = "FROM " + GrowingSystem.class.getName() + " gs ";
        growingSystemManagementModeQuery0 += " WHERE gs IN (";
        growingSystemManagementModeQuery0 += " SELECT mm." + ManagementMode.PROPERTY_GROWING_SYSTEM + " FROM " + ManagementMode.class.getName() + " mm ";
        growingSystemManagementModeQuery0 += " WHERE 1 = 1 ";
        
        Map<String, Object> args = new HashMap<>();
        
        StringBuilder growingSystemManagementModeQuery = new StringBuilder(growingSystemManagementModeQuery0);
        applyFiltersOnQuery(growingSystemManagementModeQuery, args, filter, securityContext);
        growingSystemManagementModeQuery.append(")");
        
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
        
        String filterOrderBy = getFilterGrowingSystemOrderBy(filter);
        
        List<GrowingSystem> growingSystems = find(growingSystemManagementModeQuery + filterOrderBy, args, startIndex, endIndex);
        long totalCount = findUnique("SELECT count(*) " + growingSystemManagementModeQuery, args);
        PaginationParameter pager = PaginationParameter.of(page, count);
        PaginationResult<ManagementModeDto> pageResult = PaginationResult.of(Collections.emptyList(), totalCount, pager);
        
        args = new HashMap<>();
        StringBuilder query = new StringBuilder(SELECT_GS_MM_ID_MM_TYPE);
        
        query.append(" WHERE 1 = 1 ");
        
        applyFiltersOnQuery(query, args, filter, securityContext);
        
        Set<GrowingSystem> growingSystemSet = new HashSet<>(growingSystems);
        query.append(DaoUtils.andAttributeIn("mm", ManagementMode.PROPERTY_GROWING_SYSTEM, args, growingSystemSet));
        
        String mmFilterOrderBy = getFilterManagementModeOderBy(filter);
        query.append(mmFilterOrderBy);
        
        List<Object[]> results = findAll(query.toString(), args);
        
        Map<GrowingSystem, ManagementModeDto> gsManagmentModeDtos = getGrowingSystemManagementModeDtoMap(results);
        
        Pair<Map<GrowingSystem, ManagementModeDto>, PaginationResult<ManagementModeDto>> result = Pair.of(gsManagmentModeDtos, pageResult);
        long t1 = System.currentTimeMillis();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Chargement de %d MG en %d ms", count, (t1 - t0)));
        }
        
        return result;
    }

    protected Map<GrowingSystem, ManagementModeDto> getGrowingSystemManagementModeDtoMap(List<Object[]> results) {
        Map<GrowingSystem, ManagementModeDto> gsManagmentModeDtos = new LinkedHashMap<>();

        if (results != null) {
            for (Object[] result: results) {

                GrowingSystem gs = (GrowingSystem) result[0];
                ManagementModeDto managementModeDto = gsManagmentModeDtos.computeIfAbsent(gs, k -> new ManagementModeDto());
                String mmId = (String) result[1];
                ManagementModeCategory category = (ManagementModeCategory) result[2];
                if (category == ManagementModeCategory.PLANNED) {
                    managementModeDto.setPlannedManagementModeId(mmId);
                } else {
                    managementModeDto.setObservedManagementModeId(mmId);
                }
            }
        }
        return gsManagmentModeDtos;
    }

    public Set<String> getFilteredManagementModeIds(ManagementModeFilter filter, SecurityContext securityContext) {
        StringBuilder growingSystemManagementModeQuery = new StringBuilder("SELECT gs.topiaId  FROM " + GrowingSystem.class.getName() + " gs ");
        growingSystemManagementModeQuery.append(" WHERE gs IN (");
        growingSystemManagementModeQuery.append(" SELECT mm." + ManagementMode.PROPERTY_GROWING_SYSTEM +
                " FROM ").append(ManagementMode.class.getName()).append(" mm ");
        growingSystemManagementModeQuery.append(" WHERE 1 = 1 ");
    
        Map<String, Object> args = new HashMap<>();
        applyFiltersOnQuery(growingSystemManagementModeQuery, args, filter, securityContext);
        growingSystemManagementModeQuery.append(")");
    
        return new HashSet<>(findAll(growingSystemManagementModeQuery.toString(), args));
    }
    
    protected void applyFiltersOnQuery(StringBuilder query, Map<String, Object> args, ManagementModeFilter filter, SecurityContext securityContext) {
        if (filter != null) {
            
            // decision rule name
            query.append(DaoUtils.andAttributeEquals("mm", ManagementMode.PROPERTY_VERSION_NUMBER, args, filter.getVersion()));
            // growing system name
            query.append(DaoUtils.andAttributeLike("mm", PROPERTY_GROWING_SYSTEM_NAME, args, filter.getGrowingSystemName()));
            // campaign
            query.append(DaoUtils.andAttributeEquals("mm", PROPERTY_DOMAIN_CAMPAIGN, args, filter.getCampaign()));
            // category
            query.append(DaoUtils.andAttributeEquals("mm", ManagementMode.PROPERTY_CATEGORY, args, filter.getManagementModeCategory()));
            // growing plan
            query.append(DaoUtils.andAttributeLike("mm", PROPERTY_GROWING_PLAN_NAME, args, filter.getGrowingPlanName()));
            // domain
            query.append(DaoUtils.andAttributeLike("mm", PROPERTY_DOMAIN_NAME, args, filter.getDomainName()));

            // Navigation context
            NavigationContext navigationContext = filter.getNavigationContext();
            if (navigationContext != null) {
                // campaigns
                query.append(DaoUtils.andAttributeInIfNotEmpty("mm", PROPERTY_DOMAIN_CAMPAIGN, args, navigationContext.getCampaigns()));

                // networks
                if (navigationContext.getNetworksCount() > 0) {
                    Set<String> growingSystemIds = networksToGrowingSystems(navigationContext.getNetworks());
                    query.append(DaoUtils.andAttributeInIfNotEmpty("mm", ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID, args, growingSystemIds));
                }
                // domains
                query.append(DaoUtils.andAttributeInIfNotEmpty("mm", PROPERTY_DOMAIN_ID, args, navigationContext.getDomains()));
                // growingPlans
                query.append(DaoUtils.andAttributeInIfNotEmpty("mm", PROPERTY_GROWING_PLAN_ID, args, navigationContext.getGrowingPlans()));
                // growingSystems
                query.append(DaoUtils.andAttributeInIfNotEmpty("mm",
                        ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID, args, navigationContext.getGrowingSystems()));
            }
        }
        SecurityHelper.addManagementModeFilter(query, args, securityContext, "mm");
    }
    
    /**
     * @return - user selected ORDER BY or DEFAULT one
     */
    private String getFilterGrowingSystemOrderBy(ManagementModeFilter filter) {
        String result = null;
        
        if (filter != null && filter.getSortedColumn() != null) {
            SortedColumn sortedColumn = filter.getSortedColumn();
            String filterOrderBy = switch (sortedColumn) {
                case GROWING_SYSTEM -> " lower (gs." + GrowingSystem.PROPERTY_NAME + ")";
                case GROWING_PLAN -> " lower (gs." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_NAME + ")";
                case DOMAIN -> " lower (gs." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_NAME + ")";
                case CAMPAIGN -> " gs." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN;
                default -> null;
            };
            if (StringUtils.isNotBlank(filterOrderBy)) {
                filterOrderBy += " " + filter.getSortOrder().name();
                result = " ORDER BY " +
                        filterOrderBy +
                        ", gs." + TopiaEntity.PROPERTY_TOPIA_ID;
            }
            
        }
        
        if (result == null) {
            // default
            result = " ORDER BY " +
                    " gs." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN + " DESC" +
                    ", lower (gs." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_NAME + ")" +
                    ", lower (gs." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_NAME + ")" +
                    ", lower (gs." + GrowingSystem.PROPERTY_NAME + ")" +
                    ", gs." + TopiaEntity.PROPERTY_TOPIA_ID;
        }
        
        return result;
    }
    
    /**
     * @return - user selected ORDER BY or DEFAULT one
     */
    private String getFilterManagementModeOderBy(ManagementModeFilter filter) {
        String result = null;
        
        if (filter != null && filter.getSortedColumn() != null) {
            SortedColumn sortedColumn = filter.getSortedColumn();
            String filterOrderBy = switch (sortedColumn) {
                case GROWING_SYSTEM -> " lower (mm." + PROPERTY_GROWING_SYSTEM_NAME + ")";
                case GROWING_PLAN -> " lower (mm." + PROPERTY_GROWING_PLAN_NAME + ")";
                case DOMAIN -> " lower (mm." + PROPERTY_DOMAIN_NAME + ")";
                case CAMPAIGN -> " mm." + PROPERTY_DOMAIN_CAMPAIGN;
                default -> null;
            };
            if (StringUtils.isNotBlank(filterOrderBy)) {
                filterOrderBy += " " + filter.getSortOrder().name();
                result = " ORDER BY " +
                        filterOrderBy +
                        ", mm." + TopiaEntity.PROPERTY_TOPIA_ID;
            }
            
        }
        
        if (result == null) {
            // default
            result = " ORDER BY " +
                    "mm." + PROPERTY_DOMAIN_CAMPAIGN + " DESC" +
                    ", lower (mm." + PROPERTY_DOMAIN_NAME + ")" +
                    ", lower (mm." + PROPERTY_GROWING_PLAN_NAME + ")" +
                    ", lower (mm." + PROPERTY_GROWING_SYSTEM_NAME + ")";
        }
        
        return result;
    }
    
    public Pair<Map<GrowingSystem, ManagementModeDto>, PaginationResult<ManagementModeDto>> loadActiveReadableManagementModesForGrowingSystemIds(
            List<String> growingSystemIds, SecurityContext securityContext) {
        Map<String, Object> args = new HashMap<>();
        String query = SELECT_GS_MM_ID_MM_TYPE;
        query += ", " + GrowingSystem.class.getName() + " AS gs ";
        query += " WHERE gs." + GrowingSystem.PROPERTY_TOPIA_ID + " = mm." + ManagementMode.PROPERTY_GROWING_SYSTEM + "." + GrowingSystem.PROPERTY_TOPIA_ID;
        query += DaoUtils.andAttributeIn("gs", GrowingSystem.PROPERTY_TOPIA_ID, args, new HashSet<>(growingSystemIds));
        StringBuilder query0 = new StringBuilder(query);
        SecurityHelper.addWritableGrowingSystemFilter(query0, args, securityContext, "gs");
        List<Object[]> results = findAll(query0.toString(), args);
        
        Map<GrowingSystem, ManagementModeDto> gsManagmentModeDtos = getGrowingSystemManagementModeDtoMap(results);
        PaginationParameter pager = PaginationParameter.of(0, gsManagmentModeDtos.size());
        PaginationResult<ManagementModeDto> pageResult = PaginationResult.of(Collections.emptyList(), gsManagmentModeDtos.size(), pager);
        
        return Pair.of(gsManagmentModeDtos, pageResult);
    }

    protected Set<String> networksToGrowingSystems(Set<String> networksIds) {
        return getProjectionHelper().networksToGrowingSystems(networksIds);
    }

    public List<ManagementModeCategory> findManagementModeCategories(String growingSystemId) {
        List<ManagementModeCategory> managementModeCategories = new ArrayList<>();
        String query = "SELECT DISTINCT " +
                "mm." + ManagementMode.PROPERTY_CATEGORY + " FROM " + ManagementMode.class.getName() + " mm";
        query += " WHERE 1 = 1";
        Map<String, Object> args = new HashMap<>();
        query += DaoUtils.andAttributeLike("mm", PROPERTY_GROWING_SYSTEM_ID, args, growingSystemId);
        List<ManagementModeCategory> results = findAll(query + " ORDER BY mm." + ManagementMode.PROPERTY_CATEGORY, args);

        if (results != null) {
            managementModeCategories.addAll(results);
        }

        return managementModeCategories;
    }

    public List<CroppingPlanEntry> getCroppingPlanEntryWithDomainCode(String domainsCode) {
        String query = " FROM " + CroppingPlanEntry.class.getName() + " cpe " +
                " WHERE cpe." + CroppingPlanEntry.PROPERTY_DOMAIN + " IN (" +
                " SELECT d FROM " + Domain.class.getName() + " d " +
                " WHERE d." + Domain.PROPERTY_CODE +" = :domainCode )";
        Map<String, Object> args = new HashMap<>();
        args.put("domainCode", domainsCode);
        List<CroppingPlanEntry> allCroppingPlanEntries = findAll(query + " ORDER BY cpe." + CroppingPlanEntry.PROPERTY_CODE, args);
        Map<String, CroppingPlanEntry> uniqueCroppingPlanEntryCodes = new HashMap<>();
        for(CroppingPlanEntry croppingPlanEntry : allCroppingPlanEntries) {
            uniqueCroppingPlanEntryCodes.putIfAbsent(croppingPlanEntry.getCode(), croppingPlanEntry);
        }
        return new ArrayList<>(uniqueCroppingPlanEntryCodes.values());
    }

    public boolean isDecisionRuleActive(DecisionRule decisionRule) {

        String query = "SELECT COUNT(*) FROM " + decisionRule.getClass().getName() + " dr, " + Domain.class.getName() + " d ";
        query += " WHERE dr." + DecisionRule.PROPERTY_DOMAIN_CODE + " = d." + Domain.PROPERTY_CODE;
        query += " AND dr = :dr";
        query += " AND dr." + DecisionRule.PROPERTY_ACTIVE + " IS TRUE ";
        query += " AND d." + Domain.PROPERTY_ACTIVE + " IS TRUE";

        long nbActive = findUnique(query, DaoUtils.asArgsMap("dr", decisionRule));

        return nbActive > 0;
    }

}
