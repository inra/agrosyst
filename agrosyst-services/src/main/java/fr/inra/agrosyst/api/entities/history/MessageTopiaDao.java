package fr.inra.agrosyst.api.entities.history;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.history.MessageFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

/**
 * Created by davidcosse on 14/10/14.
 */
public class MessageTopiaDao extends AbstractMessageTopiaDao<Message> {

    public PaginationResult<Message> getAllMessageByPage(MessageFilter filter) {
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;

        String query = "FROM " + getEntityClass().getName() + " M WHERE 1 = 1 ";
        String orderedQuery = query + " ORDER BY M." + Message.PROPERTY_MESSAGE_DATE + " DESC" +
                ", M." + TopiaEntity.PROPERTY_TOPIA_ID;

        List<Message> messages = find(orderedQuery, new HashMap<>(), startIndex, endIndex);
        long totalCount = findUnique("SELECT count(*) " + query, new HashMap<>());

        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        PaginationResult<Message> result = PaginationResult.of(messages, totalCount, pager);
        return result;
    }

    public List<Message> geAllMessages() {
        StringBuilder query = new StringBuilder("FROM " + getEntityClass().getName() + " M");
        query.append(" WHERE 1 = 1");
        query.append(" ORDER BY M." + Message.PROPERTY_MESSAGE_DATE + " DESC");

        List<Message> result = findAll(query.toString(), new HashMap<>());
        return result;
    }

    public List<Message> getMessageFromDate(LocalDateTime fromDate) {

        StringBuilder query = new StringBuilder("FROM " + getEntityClass().getName() + " M");
        query.append(" WHERE 1 = 1");

        query.append(" AND M." + Message.PROPERTY_MESSAGE_DATE + " > :fromDate");
        query.append(" ORDER BY M." + Message.PROPERTY_MESSAGE_DATE + " DESC");

        List<Message> result = findAll(query.toString(), DaoUtils.asArgsMap("fromDate", fromDate));
        return result;
    }

}
