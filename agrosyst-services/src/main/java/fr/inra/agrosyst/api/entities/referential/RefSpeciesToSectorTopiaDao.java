package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.Sector;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by davidcosse on 25/05/16.
 */
public class RefSpeciesToSectorTopiaDao extends AbstractRefSpeciesToSectorTopiaDao<RefSpeciesToSector>  {

    public static final String SEPARATOR = "_";

    /**
     * To get full list of sector you need to include values for (codeEspeceBotanique + "_" + codeQualifiantAee) and (codeEspeceBotanique + "_" + null)
     */
    public Map<String, List<Sector>> loadSectorsByCodeEspeceBotanique_CodeQualifiant(List<Pair<String, String>> codeEspeceBotaniquesCodeQualifiantAEE) {
        List<Object[]> queryResults = findSectorsByCodeEspeceBotaniqueCodeQualifant(codeEspeceBotaniquesCodeQualifiantAEE);
        Map<String, List<Sector>> result = getSectorsByCodeEspeceBotanique_CodeQualifiantAee(queryResults);
        return result;
    }

    /**
     * To get full list of sector you need to include values for (codeEspeceBotanique + codeQualifiantAee) and (codeEspeceBotanique + null)
     */
    public Map<Pair<String, String>, List<Sector>> loadSectorsByCodeEspeceBotaniqueCodeQualifiant(List<Pair<String, String>> codeEspeceBotaniquesCodeQualifiantAEE) {
        List<Object[]> queryResults = findSectorsByCodeEspeceBotaniqueCodeQualifant(codeEspeceBotaniquesCodeQualifiantAEE);
        Map<Pair<String, String>, List<Sector>> result = getSectorsByCodeEspeceBotaniqueCodeQualifiantAee(queryResults);
        return result;
    }

    private List<Object[]> findSectorsByCodeEspeceBotaniqueCodeQualifant(List<Pair<String, String>> codeEspeceBotaniquesCodeQualifiantAEE) {
        Map<String, Object> args = Maps.newLinkedHashMap();

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT   rsts." + RefSpeciesToSector.PROPERTY_CODE_ESPECE_BOTANIQUE +  ", " +
                        " rsts." + RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE + ", " +
                        " rsts." + RefSpeciesToSector.PROPERTY_SECTOR +
                        " FROM " + RefSpeciesToSector.class.getName() + " rsts " +
                        " WHERE 1 = 1 ");

        for (int i = 0; i < codeEspeceBotaniquesCodeQualifiantAEE.size(); i++) {
            Pair<String, String> codeEspeceBotaniqueCodeQualifiantAee = codeEspeceBotaniquesCodeQualifiantAEE.get(i);
            String codeEspeceBotanique = codeEspeceBotaniqueCodeQualifiantAee.getLeft();
            String codeQualifiantAee = Strings.emptyToNull(codeEspeceBotaniqueCodeQualifiantAee.getRight());
            String operand = i == 0 ? "AND" : "OR";
            String codeQualifiantAeeQuery = "";
            if (codeQualifiantAee != null) {
                codeQualifiantAeeQuery = String.format(" rsts." + RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE + " = :%s OR", "codeQualifiantAee_"+i);
                args.put("codeQualifiantAee_"+i, codeQualifiantAee);
            }

            sb.append(String.format(
                    " %s (rsts." + RefSpeciesToSector.PROPERTY_CODE_ESPECE_BOTANIQUE + " = :%s" +
                    "     AND (" +
                    "       %s" +
                    "       rsts." + RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE + " IS NULL" +
                    "     )" +
                    "    )", operand, "codeEspeceBotanique_"+i, codeQualifiantAeeQuery));

            args.put("codeEspeceBotanique_"+i, codeEspeceBotanique);
        }

        return findAll(sb.toString(), args);
    }

    protected static Map<Pair<String, String>, List<Sector>> getSectorsByCodeEspeceBotaniqueCodeQualifiantAee(List<Object[]> input) {
        Map<Pair<String, String>, List<Sector>> sectorsByCodeEspeceBotanique = new HashMap<>();
        if (input != null) {
            for (Object[] entry : input) {
                String codeEspeceBotanique = (String) entry[0];
                String codeQualifiantAee = (String) entry[1];
                Sector sector = (Sector) entry[2];
                Pair<String, String> pebcq = Pair.of(codeEspeceBotanique, Strings.emptyToNull(codeQualifiantAee));
                List<Sector> sectors = sectorsByCodeEspeceBotanique.computeIfAbsent(pebcq, k -> Lists.newArrayList());
                sectors.add(sector);
            }
        }
        return sectorsByCodeEspeceBotanique;
    }

    protected static Map<String, List<Sector>> getSectorsByCodeEspeceBotanique_CodeQualifiantAee(List<Object[]> input) {
        Map<String, List<Sector>> sectorsByCodeEspeceBotanique = new HashMap<>();
        if (input != null) {
            for (Object[] entry : input) {
                String codeEspeceBotanique = (String) entry[0];
                String codeQualifiantAee = (String) entry[1];
                Sector sector = (Sector) entry[2];
                String pebcq = codeEspeceBotanique + SEPARATOR + Strings.emptyToNull(codeQualifiantAee);
                List<Sector> sectors = sectorsByCodeEspeceBotanique.computeIfAbsent(pebcq, k -> Lists.newArrayList());
                sectors.add(sector);
            }
        }
        return sectorsByCodeEspeceBotanique;
    }

    public RefSpeciesToSector findByCodeEspeceBotaniqueCodeQualifant(String codeEspeceBotanique, String codeQualifiantAEE) {
        Map<String, Object> args = Maps.newLinkedHashMap();

        StringBuilder sb = new StringBuilder();
        sb.append(" FROM " + RefSpeciesToSector.class.getName() + " rsts " +
                " WHERE rsts." + RefSpeciesToSector.PROPERTY_CODE_ESPECE_BOTANIQUE + " = :codeEspeceBotanique");
        args.put("codeEspeceBotanique", codeEspeceBotanique);
        sb.append(" AND (");
        sb.append("  rsts." + RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE + " = :codeQualifiantAEE");
        args.put("codeQualifiantAEE", codeQualifiantAEE);
        sb.append("  OR rsts." + RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE + " is null");
        sb.append(")");

        return findAnyOrNull(sb.toString(), args);
    }
}
