package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.services.referential.RefAnimalTypeDto;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RefAnimalTypeTopiaDao extends AbstractRefAnimalTypeTopiaDao<RefAnimalType> {

    public List<RefAnimalTypeDto> findAllActive(Language language) {

        I18nDaoHelper animalTypeI18nDaoHelper =
                I18nDaoHelper.withSimpleI18nKey(
                        RefAnimalType.PROPERTY_ANIMAL_TYPE,
                        language,
                        TradRefVivant.class,
                        "rat"
                );

        I18nDaoHelper animalPopulationUnitI18nDaoHelper =
                I18nDaoHelper.withSimpleI18nKey(
                        RefAnimalType.PROPERTY_ANIMAL_POPULATION_UNITS,
                        language,
                        TradRefVivant.class,
                        "rat"
                );

        String hql = String.join("\n"
                , "select rat"
                        + ", " + animalTypeI18nDaoHelper.coalesceTranslation()
                        + ", " + animalPopulationUnitI18nDaoHelper.coalesceTranslation()
                , newFromClause("rat")
                  + animalTypeI18nDaoHelper.leftJoinTranslation()
                  + animalPopulationUnitI18nDaoHelper.leftJoinTranslation()
                , "where rat.active is true"
        );

        try (Stream<Object[]> stream = stream(hql)) {
            return stream.map(row -> {
                RefAnimalType refAnimalType = (RefAnimalType) row[0];
                String animalTypeTranslation = (String) row[1];
                String animalPopulationUnitTranslation = (String) row[2];
                return new RefAnimalTypeDto(
                        refAnimalType.getTopiaId(),
                        animalTypeTranslation,
                        animalPopulationUnitTranslation
                );
            }).collect(Collectors.toList());
        }
    }
}
