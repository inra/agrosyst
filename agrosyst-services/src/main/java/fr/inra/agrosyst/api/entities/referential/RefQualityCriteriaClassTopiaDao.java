package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.utils.DaoUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by davidcosse on 31/01/17.
 */
public class RefQualityCriteriaClassTopiaDao extends AbstractRefQualityCriteriaClassTopiaDao<RefQualityCriteriaClass> {

    protected static final java.util.function.Function<RefQualityCriteria, String> GET_REF_QUALITY_CRITERIA_CODE = RefQualityCriteria::getCode;

    public List<RefQualityCriteriaClass> findAllForRefQualityCriteria(List<RefQualityCriteria> qualityCriteries){
        Map<String, Object> args = Maps.newLinkedHashMap();
        List<RefQualityCriteriaClass> result;
        if (qualityCriteries != null){
            Set<String> qualityCriteriesCodes = Maps.uniqueIndex(qualityCriteries, GET_REF_QUALITY_CRITERIA_CODE::apply).keySet();
            String query = " FROM " + RefQualityCriteriaClass.class.getName() + " rqcc " + " WHERE 1 = 1" +
                    DaoUtils.andAttributeIn("rqcc", RefQualityCriteriaClass.PROPERTY_REF_QUALITY_CRITERIA_CODE, args, qualityCriteriesCodes) +
                    DaoUtils.andAttributeEquals("rqcc", RefQualityCriteriaClass.PROPERTY_ACTIVE, args, true);
            result = findAll(query, args);
        } else {
            result = new ArrayList<>();
        }

        return result;
    }
}
