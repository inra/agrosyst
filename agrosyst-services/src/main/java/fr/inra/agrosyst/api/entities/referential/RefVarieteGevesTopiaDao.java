package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RefVarieteGevesTopiaDao extends AbstractRefVarieteGevesTopiaDao<RefVarieteGeves> {

    public List<RefVarieteGeves> findAllActiveVarietes(Set<Integer> varietesGevesNumEspeceBotaniques, String filter, int maxResults) {
        List<RefVarieteGeves> result = new ArrayList<>();
        if (varietesGevesNumEspeceBotaniques != null && !varietesGevesNumEspeceBotaniques.isEmpty()) {

            String query = "FROM " + RefVarieteGeves.class.getName() + " vg";
            query += " WHERE vg." + RefVarieteGeves.PROPERTY_ACTIVE + " = true";
            Map<String, Object> args = new HashMap<>();

            // num espece
            query += DaoUtils.andAttributeIn("vg", RefVarieteGeves.PROPERTY_NUM__ESPECE__BOTANIQUE, args, varietesGevesNumEspeceBotaniques);

            // denomination
            query += DaoUtils.andAttributeLike("vg", RefVarieteGeves.PROPERTY_DENOMINATION, args, filter);

            query += " ORDER BY vg." + RefVarieteGeves.PROPERTY_DENOMINATION;

            List<RefVarieteGeves> entities = find(query, args, 0, maxResults - 1);
            if (entities != null) {
                result.addAll(entities);
            }
        }
        return result;
    }

    public List<RefVarieteGeves> findActiveGraftSupport(String filter, int codeSection, int maxResults, String code_gnis) {
        String query = "FROM " + RefVarieteGeves.class.getName() + " vg";
        query += " WHERE vg." + RefVarieteGeves.PROPERTY_ACTIVE + " = true";

        Map<String, Object> args = new HashMap<>();

        query += DaoUtils.andAttributeEquals("vg", RefVarieteGeves.PROPERTY_CODE__SECTION, args, codeSection);
        query += DaoUtils.andAttributeLike("vg", RefVarieteGeves.PROPERTY_DENOMINATION, args, filter);
        
        if (StringUtils.isNotBlank(code_gnis)) {
            query += DaoUtils.andAttributeLike("vg", RefVarieteGeves.PROPERTY_CODE_GNIS_ESPECE, args, code_gnis);
        }

        query += " ORDER BY vg." + RefVarieteGeves.PROPERTY_DENOMINATION;
    
        maxResults = maxResults == DaoUtils.NO_PAGE_LIMIT ? DaoUtils.NO_PAGE_LIMIT : maxResults - 1;

        return find(query, args, 0, maxResults);
    }
    
    public List<RefVarieteGeves> findForNumEspeceBotaniqueAndDescriptionEquals(int numEspeceBotanique, String varietyCode) {
        
        String query = "FROM " + RefVarieteGeves.class.getName() + " vg";
        query += " WHERE vg." + RefVarieteGeves.PROPERTY_ACTIVE + " = true";
        Map<String, Object> args = new HashMap<>();
    
        // num espece
        query += DaoUtils.andAttributeEquals("vg", RefVarieteGeves.PROPERTY_NUM__ESPECE__BOTANIQUE, args, numEspeceBotanique);

        try {
            if (NumberUtils.isParsable(varietyCode)) {
                int numDossier = Integer.parseInt(varietyCode);
                query += DaoUtils.andAttributeNotEquals("vg", RefVarieteGeves.PROPERTY_NUM__DOSSIER, args, numDossier);
            }
        } catch (NumberFormatException e) {
            // The variety code is just not an int
        }

        return findAll(query, args);
    }


    public List<RefVarieteGeves> findAllForCodeGnisVarieteEqualsIgnoreSpace(String codeGnis) {
        String query = "FROM " + RefVarieteGeves.class.getName() + " vg WHERE 1 = 1 ";
        Map<String, Object> args = new HashMap<>();
        query += DaoUtils.andReplaceAttributeEquals("vg", RefVarieteGeves.PROPERTY_CODE_GNIS_VARIETE, " ", "", args, codeGnis);
        return findAll(query, args);
    }

} //RefVarieteGevesTopiaDao<E extends RefVarieteGeves>
