package fr.inra.agrosyst.api.entities.security;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class UserRoleTopiaDao extends AbstractUserRoleTopiaDao<UserRole> {

    protected static final String ROLE_USER_ID = UserRole.PROPERTY_AGROSYST_USER + "." + AgrosystUser.PROPERTY_TOPIA_ID;

    public UserRole findRole(String userId, RoleType roleType) {
        return forProperties(ROLE_USER_ID, userId,
                UserRole.PROPERTY_TYPE, roleType).findAnyOrNull();
    }

    public UserRole findNetworkResponsibleRole(String userId) {
        return forProperties(ROLE_USER_ID, userId)
                .addIn(UserRole.PROPERTY_TYPE, Arrays.asList(RoleType.NETWORK_RESPONSIBLE, RoleType.NETWORK_SUPERVISOR))
                .findAnyOrNull();
    }

    public List<UserRole> findAllForUserId(String userId) {
        return forProperties(ROLE_USER_ID, userId).findAll();
    }

    public ImmutableSet<RoleType> findUserRoleTypes(String userId) {
        List<UserRole> roles = findAllForUserId(userId);
        ImmutableSet<RoleType> roleTypes = roles.stream()
                .map(UserRole::getType)
                .collect(ImmutableSet.toImmutableSet());
        return roleTypes;
    }

    public List<AgrosystUser> findAllRoleUsers(RoleType roleType, String propCode, String code) {
        String hql = " SELECT ur." + UserRole.PROPERTY_AGROSYST_USER +
                " FROM " + UserRole.class.getName() + " ur " +
                " WHERE 1=1 ";
        Map<String, Object> args = DaoUtils.asArgsMap();
        hql += DaoUtils.andAttributeEquals("ur", UserRole.PROPERTY_TYPE, args, roleType);
        hql += DaoUtils.andAttributeEquals("ur", propCode, args, code);
        String query = hql + " ORDER BY " +
                "ur." + UserRole.PROPERTY_AGROSYST_USER + "." + AgrosystUser.PROPERTY_FIRST_NAME + " ASC, " +
                "ur." + UserRole.PROPERTY_AGROSYST_USER + "." + AgrosystUser.PROPERTY_LAST_NAME + " ASC ";
        return findAll(query, args);
    }

    public void deleteAllUserRolesForDomains(Collection<String> domainCodes,
                                             Collection<String> growingPlanCodes,
                                             Collection<String> growingSystemCodes,
                                             Collection<String> growingSystemIds) {
        List<String> conditions = new ArrayList<>();
        Map<String, Object> args = DaoUtils.asArgsMap();
        addDeleteAllUserRoleCondition(conditions, args, UserRole.PROPERTY_DOMAIN_CODE, domainCodes, "domainCodes");
        addDeleteAllUserRoleCondition(conditions, args, UserRole.PROPERTY_GROWING_PLAN_CODE, growingPlanCodes, "growingPlanCodes");
        addDeleteAllUserRoleCondition(conditions, args, UserRole.PROPERTY_GROWING_SYSTEM_CODE, growingSystemCodes, "growingSystemCodes");
        addDeleteAllUserRoleCondition(conditions, args, UserRole.PROPERTY_GROWING_SYSTEM_ID, growingSystemIds, "growingSystemIds");

        if (!conditions.isEmpty()) {
            String query = "DELETE FROM " + getEntityClass().getName() + " WHERE " + String.join(" OR ", conditions);
            topiaJpaSupport.execute(query, args);
        }
        
    }

    private void addDeleteAllUserRoleCondition(List<String> conditions, Map<String, Object> args,
                                               String property, Collection<String> objects, String objectArg) {
        if (CollectionUtils.isNotEmpty(objects)) {
            conditions.add(property + " IN (:" + objectArg + ")");
            args.put(objectArg, objects);
        }
    }

} //UserRoleTopiaDao<E extends UserRole>
