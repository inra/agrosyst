package fr.inra.agrosyst.api.entities.referential;

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.SimpleSqlQuery;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.topia.persistence.TopiaException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class AbstractRefMaterielOutilTopiaDao<E extends RefMaterielOutil> extends GeneratedRefMaterielOutilTopiaDao<E> {

    /**
     * Recherche dans la liste des RefMaterielOutils les valeurs de materielType1 des petits outils
     *
     * @return materielType1 value
     */
    public List<String> findTypeMateriel1PetitMaterielValues() throws TopiaException {
        List<String> result = findPropertyValues(RefMateriel.PROPERTY_TYPE_MATERIEL1, null, null, null, true);
        return result;
    }
    
    /**
     * Recherche dans la liste des RefMaterielOutil les valeurs de materielType[2-4] suivant les filtres
     * typeMateriel[1-3]
     *
     * @param property      property to extract
     * @param typeMateriel1 optionnal type 1 filter
     * @param typeMateriel2 optionnal type 2 filter
     * @param typeMateriel3 optionnal type 3 filter
     * @param petitMateriel not use there
     * @return Le nom des petits matériels
     */
    public List<String> findPropertyValues(
            String property,
            String typeMateriel1,
            String typeMateriel2,
            String typeMateriel3,
            boolean petitMateriel) throws TopiaException {
        
        String query = "SELECT distinct( m." + property + " ) " +
                " FROM " + getEntityClass().getName() + " m " +
                " WHERE m." + RefMateriel.PROPERTY_ACTIVE + " = true";
        
        Map<String, Object> args = new HashMap<>();
        query += DaoUtils.andAttributeEquals("m", RefMateriel.PROPERTY_TYPE_MATERIEL1, args, typeMateriel1);
        
        query += DaoUtils.andAttributeEquals("m", RefMateriel.PROPERTY_TYPE_MATERIEL2, args, typeMateriel2);
        
        query += DaoUtils.andAttributeEquals("m", RefMateriel.PROPERTY_TYPE_MATERIEL3, args, typeMateriel3);
        
        query += DaoUtils.andAttributeEquals("m", RefMaterielOutil.PROPERTY_PETIT_MATERIEL, args, petitMateriel);
        query += " ORDER BY m." + property + " ASC ";
        
        List<String> result = findAll(query, args);
        return result;
    }

    public List<Pair<String, String>> getMaterlielTraduction(String refTrad, List<String> libelles, Language language) {

        List<Pair<String, String>> result = new ArrayList<>();
        for (String libelle : libelles) {

            String query = """
             SELECT
               trm.traduction   as trad_request,
               trmen.traduction as trad_ent,
               trmfr.traduction as trad_fr
             from %s trmfr
             left join %s trm on trmfr.tradkey = trm.tradkey
             left join %s trmen on trmfr.tradkey = trmen.tradkey
             where trmfr.lang = '%s'
             and trmen.lang = '%s'
             and trm.lang = ?
             and to_complex_i18n_key(?) = trmfr.tradkey
                """;
            query = String.format(query,
                    refTrad,
                    refTrad,
                    refTrad,
                    Language.FRENCH.getTrigram(),
                    Language.ENGLISH.getTrigram());

            List<String> params = new ArrayList<>();
            params.add(language.getTrigram());
            params.add(libelle);
            SimpleSqlQuery<List<String>> simpleSqlQuery = new SimpleSqlQuery<>(query, params) {
                @Override
                public List<String> prepareResult(ResultSet resultSet) throws SQLException {
                    List<String> results = new ArrayList<>();
                    results.add(resultSet.getString(1));
                    results.add(resultSet.getString(2));
                    results.add(resultSet.getString(3));
                    return results;
                }
            };
            List<String> singleResult = this.topiaSqlSupport.findSingleResult(simpleSqlQuery);

            String tradfr = "";
            String traden = "";
            String tradrequest = "";
            if (CollectionUtils.isNotEmpty(singleResult)) {
                tradrequest = singleResult.getFirst();
                traden = singleResult.get(1);
                tradfr = singleResult.get(2);
            }
            result.add(Pair.of(libelle, StringUtils.firstNonBlank(tradrequest, traden, tradfr, libelle)));

        }
        return result;
    }
}
