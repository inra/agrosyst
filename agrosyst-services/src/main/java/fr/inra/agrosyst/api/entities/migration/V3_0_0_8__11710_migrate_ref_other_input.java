package fr.inra.agrosyst.api.entities.migration;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefOtherInputImpl;
import fr.inra.agrosyst.api.entities.referential.RefPrixPot;
import fr.inra.agrosyst.services.referential.csv.AbstractAgrosystModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.Import;
import org.nuiton.csv.ModelBuilder;

import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;
import java.util.UUID;

public class V3_0_0_8__11710_migrate_ref_other_input extends BaseJavaMigration {

    private static final Log LOG = LogFactory.getLog(V3_0_0_8__11710_migrate_ref_other_input.class);

    //                                    1          2            3              4              5                6              7             8        9        10      11              12
    public static final String INSERT_INTO_REF_OTHER_INPUT_QUERY = """
            INSERT INTO refotherinput (topiaid, topiaversion, inputtype_c0, caracteristic2, caracteristic3, topiacreatedate, caracteristic1, lifetime, active, source, reference_id, reference_code)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            """;

    public static final String INSERT_INTO_REF_OTHER_INPUT_SECTOR_QUERY = """
            INSERT INTO refotherinput_sector (owner, sector)
            VALUES (?, ?)
            """;

    public class RefOtherInputModel extends AbstractAgrosystModel<RefOtherInput> implements ExportModel<RefOtherInput> {

        public RefOtherInputModel() {
            super(CSV_SEPARATOR);
            newMandatoryColumn("reference_id", RefOtherInput.PROPERTY_REFERENCE_ID);
            newMandatoryColumn("reference_code", RefOtherInput.PROPERTY_REFERENCE_CODE);
            newMandatoryColumn("Type_Intrants", RefOtherInput.PROPERTY_INPUT_TYPE_C0);
            newMandatoryColumn("Caracteristique_1", RefOtherInput.PROPERTY_CARACTERISTIC1);
            newMandatoryColumn("Caracteristique_2", RefOtherInput.PROPERTY_CARACTERISTIC2);
            newMandatoryColumn("Caracteristique_3", RefOtherInput.PROPERTY_CARACTERISTIC3);
            newMandatoryColumn("Duree de vie", RefOtherInput.PROPERTY_LIFETIME, DOUBLE_PARSER);
            newMandatoryColumn("filieres", RefOtherInput.PROPERTY_SECTOR, AGROSYST_SECTORS_PARSER);
            newMandatoryColumn("source", RefOtherInput.PROPERTY_SOURCE);
            newOptionalColumn(COLUMN_ACTIVE, RefOtherInput.PROPERTY_ACTIVE, ACTIVE_PARSER);
        }

        @Override
        public Iterable<ExportableColumn<RefOtherInput, Object>> getColumnsForExport() {
            ModelBuilder<RefPrixPot> modelBuilder = new ModelBuilder<>();
            modelBuilder.newColumnForExport("reference_id", RefOtherInput.PROPERTY_REFERENCE_ID);
            modelBuilder.newColumnForExport("reference_code", RefOtherInput.PROPERTY_REFERENCE_CODE);
            modelBuilder.newColumnForExport("Type_Intrants", RefOtherInput.PROPERTY_INPUT_TYPE_C0);
            modelBuilder.newColumnForExport("Caracteristique_1", RefOtherInput.PROPERTY_CARACTERISTIC1);
            modelBuilder.newColumnForExport("Caracteristique_2", RefOtherInput.PROPERTY_CARACTERISTIC2);
            modelBuilder.newColumnForExport("Caracteristique_3", RefOtherInput.PROPERTY_CARACTERISTIC3);
            modelBuilder.newColumnForExport("Duree de vie", RefOtherInput.PROPERTY_LIFETIME, DOUBLE_FORMATTER);
            modelBuilder.newColumnForExport("filieres", RefOtherInput.PROPERTY_SECTOR, AGROSYST_SECTORS_FORMATTER);
            modelBuilder.newColumnForExport("source", RefOtherInput.PROPERTY_SOURCE);
            modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefPrixPot.PROPERTY_ACTIVE, T_F_FORMATTER);
            return (Iterable) modelBuilder.getColumnsForExport();
        }

        @Override
        public RefOtherInput newEmptyInstance() {
            RefOtherInput result = new RefOtherInputImpl();
            result.setActive(true);
            return result;
        }

    }

    @Override
    public void migrate(Context context) {
        Connection connection = context.getConnection();

        LOG.info(" Ajout des données de référentiel pour les intrants autre 'RefOtherInput'");
        try (PreparedStatement refOtherInputSectorStatement = connection.prepareStatement(INSERT_INTO_REF_OTHER_INPUT_SECTOR_QUERY)) {
            try (PreparedStatement refOtherInputStatement = connection.prepareStatement(INSERT_INTO_REF_OTHER_INPUT_QUERY)) {

                try (Import<RefOtherInput> refOtherInputs = Import.newImport(new RefOtherInputModel(),
                        new InputStreamReader(
                                Objects.requireNonNull(getClass().getResourceAsStream("/db/migration/V3_0_0_8__11710_migrate_ref_other_input.csv")),
                                StandardCharsets.UTF_8
                        ))) {

                    for (RefOtherInput input : refOtherInputs) {
                        String refOtherInputId = RefOtherInput.class.getName() + "_" + UUID.randomUUID();
                        refOtherInputStatement.setString(1, refOtherInputId);
                        refOtherInputStatement.setInt(2, 0);
                        refOtherInputStatement.setString(3, input.getInputType_c0());
                        refOtherInputStatement.setString(4, input.getCaracteristic2());
                        refOtherInputStatement.setString(5, input.getCaracteristic3());
                        refOtherInputStatement.setDate(6, new java.sql.Date(new java.util.Date().getTime()));
                        refOtherInputStatement.setString(7, input.getCaracteristic1());
                        refOtherInputStatement.setDouble(8, input.getLifetime());
                        refOtherInputStatement.setBoolean(9, true);
                        refOtherInputStatement.setString(10, input.getSource());
                        refOtherInputStatement.setString(11, input.getReference_id());
                        refOtherInputStatement.setString(12, input.getReference_code());

                        refOtherInputStatement.addBatch();

                        if (CollectionUtils.isNotEmpty(input.getSector())) {
                            for (Sector sector : input.getSector()) {
                                refOtherInputSectorStatement.setString(1, refOtherInputId);
                                refOtherInputSectorStatement.setString(2, sector.name());

                                refOtherInputSectorStatement.addBatch();
                            }
                        }
                    }
                    refOtherInputStatement.executeBatch();
                    refOtherInputSectorStatement.executeBatch();
                }

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
