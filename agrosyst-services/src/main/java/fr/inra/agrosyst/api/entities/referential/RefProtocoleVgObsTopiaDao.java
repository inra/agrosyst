package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.measurement.ProtocoleVgObsFilter;
import org.nuiton.topia.persistence.TopiaException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RefProtocoleVgObsTopiaDao extends AbstractRefProtocoleVgObsTopiaDao<RefProtocoleVgObs> {

    protected String filterToQuery(ProtocoleVgObsFilter filter, Map<String, Object> args) {
        String query = "";
        if (filter.getLabel() != null) {
            query += " AND " + RefProtocoleVgObs.PROPERTY_PROTOCOLE_LIBELLE + " = :" + RefProtocoleVgObs.PROPERTY_PROTOCOLE_LIBELLE;
            args.put(RefProtocoleVgObs.PROPERTY_PROTOCOLE_LIBELLE, filter.getLabel());
        }
        if (filter.getPest() != null) {
            query += " AND " + RefProtocoleVgObs.PROPERTY_LIGNE_ORGANISME_VIVANT + " = :" + RefProtocoleVgObs.PROPERTY_LIGNE_ORGANISME_VIVANT;
            args.put(RefProtocoleVgObs.PROPERTY_LIGNE_ORGANISME_VIVANT, filter.getPest());
        }
        if (filter.getStade() != null) {
            query += " AND " + RefProtocoleVgObs.PROPERTY_LIGNE_STADES_DEVELOPPEMENT + " = :" + RefProtocoleVgObs.PROPERTY_LIGNE_STADES_DEVELOPPEMENT;
            args.put(RefProtocoleVgObs.PROPERTY_LIGNE_STADES_DEVELOPPEMENT, filter.getStade());
        }
        if (filter.getSupport() != null) {
            query += " AND " + RefProtocoleVgObs.PROPERTY_LIGNE_SUPPORTS_ORGANES + " = :" + RefProtocoleVgObs.PROPERTY_LIGNE_SUPPORTS_ORGANES;
            args.put(RefProtocoleVgObs.PROPERTY_LIGNE_SUPPORTS_ORGANES, filter.getSupport());
        }
        if (filter.getObservation() != null) {
            query += " AND " + RefProtocoleVgObs.PROPERTY_LIGNE_TYPE_OBSERVATION + " = :" + RefProtocoleVgObs.PROPERTY_LIGNE_TYPE_OBSERVATION;
            args.put(RefProtocoleVgObs.PROPERTY_LIGNE_TYPE_OBSERVATION, filter.getObservation());
        }
        if (filter.getQualitative() != null) {
            query += " AND " + RefProtocoleVgObs.PROPERTY_CLASSE_VALEUR_QUALITATIVE + " = :" + RefProtocoleVgObs.PROPERTY_CLASSE_VALEUR_QUALITATIVE;
            args.put(RefProtocoleVgObs.PROPERTY_CLASSE_VALEUR_QUALITATIVE, filter.getQualitative());
        }
        if (filter.getUnit() != null) {
            query += " AND " + RefProtocoleVgObs.PROPERTY_RELEVE_UNITE + " = :" + RefProtocoleVgObs.PROPERTY_RELEVE_UNITE;
            args.put(RefProtocoleVgObs.PROPERTY_RELEVE_UNITE, filter.getUnit());
        }
        return query;
    }

    public <T> List<T> findAllProperties(String property, ProtocoleVgObsFilter filter) throws TopiaException {
        String query = "";

        // if no property, return E (all entity)
        if (property != null) {
            query += "SELECT distinct(" + property + ")";
        }

        query += " FROM " + getEntityClass().getName() +
                " WHERE " + RefProtocoleVgObs.PROPERTY_ACTIVE + " = true";

        Map<String, Object> args = new HashMap<>();
        if (filter != null) {
            query += filterToQuery(filter, args);
        }

        if (property != null) {
            query += " ORDER BY " + property;
        }

        List<T> result = findAll(query, args);
        return result;
    }
}
