package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;

import java.util.List;
import java.util.Map;

public class RefSubstancesActivesCommissionEuropeenneTopiaDao extends AbstractRefSubstancesActivesCommissionEuropeenneTopiaDao<RefSubstancesActivesCommissionEuropeenne> {

    public MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> findSubstancesActivesCommissionEuropeenneByAmmCodeForDomain(Domain domain) {
        final String query =
                " SELECT r2." + RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NUMERO__AMM + ", r  FROM " + getEntityClass().getName() + " r " +
                        "INNER JOIN " + RefCompositionSubstancesActivesParNumeroAMM.class.getName() + " r2 ON r2." + RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_ID_SA + " = r." + RefSubstancesActivesCommissionEuropeenne.PROPERTY_ID_SA + " " +
                        "WHERE EXISTS (" +
                        "    SELECT 1 FROM " + DomainPhytoProductInput.class.getName() + " d " +
                        "    WHERE d." + DomainPhytoProductInput.PROPERTY_DOMAIN + " = :domain " +
                        "    AND d." + DomainPhytoProductInput.PROPERTY_REF_INPUT + "." + RefActaTraitementsProduit.PROPERTY_CODE__AMM + " = r2." + RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NUMERO__AMM +
                        "  )";

        MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> refSubstancesActivesCommissionEuropeenneByAmmCode = new HashSetValuedHashMap<>();

        List<Object[]> results = this.findAll(query, Map.of("domain", domain));
        for (Object[] result : results) {
            String ammCode = (String) result[0];
            RefSubstancesActivesCommissionEuropeenne refValue = (RefSubstancesActivesCommissionEuropeenne) result[1];
            refSubstancesActivesCommissionEuropeenneByAmmCode.put(ammCode, refValue);
        }

        return refSubstancesActivesCommissionEuropeenneByAmmCode;
    }

} //RefSubstancesActivesCommissionEuropeenneTopiaDao
