package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.SortedColumn;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.measure.MeasurementSession;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.services.effective.EffectiveZoneFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.security.SecurityContext;
import fr.inra.agrosyst.services.security.SecurityHelper;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class ZoneTopiaDao extends AbstractZoneTopiaDao<Zone> {

    /**
     * query from plot LEFT OUTER JOIN pl
     */
    protected static final String PROPERTY_DOMAIN = Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_DOMAIN;
    protected static final String PROPERTY_DOMAIN_CAMPAIGN = PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN;
    protected static final String PROPERTY_GROWING_SYSTEM = Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_GROWING_SYSTEM;

    public PaginationResult<Zone> getFilteredZones(EffectiveZoneFilter filter, SecurityContext securityContext) throws TopiaException {

        // used of OUTER JOIN to be able to use ORDER BY on growing system that can be null
        StringBuilder query = new StringBuilder(
                // setup Select because of
                "FROM " + Zone.class.getName() +
                        " z LEFT OUTER JOIN z." + Zone.PROPERTY_PLOT + " AS pl " +
                        " LEFT OUTER JOIN pl." + Plot.PROPERTY_GROWING_SYSTEM + " gs " +
                        " LEFT OUTER JOIN pl." + Plot.PROPERTY_DOMAIN + " domain " +
                        " LEFT OUTER JOIN gs." + GrowingSystem.PROPERTY_GROWING_PLAN + " gp "
        );
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = new LinkedHashMap<>();
        applyFiltersOnQuery(filter, securityContext, query, args);
    
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
    
        String filterOrderBy = getFilterOrderBy(filter);
        // add SELECT to the query otherwise a List of object [] is returned
        List<Zone> zones = find("SELECT z " + query +
                        filterOrderBy,
                args, startIndex, endIndex);
    
        long totalCount = findUnique("SELECT count(*) " + query, args);
    
        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        return PaginationResult.of(zones, totalCount, pager);
    }
    
    /**
     * do not manage SortedColumn.CROP
     *
     * @return - user selected ORDER BY or DEFAULT one
     */
    private String getFilterOrderBy(EffectiveZoneFilter filter) {
        String result = null;
        
        if (filter != null && filter.getSortedColumn() != null) {
            SortedColumn sortedColumn = filter.getSortedColumn();
            String filterOrderBy = switch (sortedColumn) {
                case ZONE -> " lower (z." + Zone.PROPERTY_NAME + ")";
                case GROWING_SYSTEM -> " lower (gs." + GrowingSystem.PROPERTY_NAME + ")";
                case GROWING_PLAN -> " lower (gp." + GrowingPlan.PROPERTY_NAME + ")";
                case PLOT -> " lower (pl." + Plot.PROPERTY_NAME + ")";
                case DOMAIN -> " lower (domain." + Domain.PROPERTY_NAME + ")";
                case CAMPAIGN -> " domain." + Domain.PROPERTY_CAMPAIGN;
                default -> null;
            };
            if (StringUtils.isNotBlank(filterOrderBy)) {
                filterOrderBy += " " + filter.getSortOrder().name();
                result = " ORDER BY " +
                        filterOrderBy +
                        ", z." + TopiaEntity.PROPERTY_TOPIA_ID;
            }
            
        }
        
        if (result == null) {
            // default
            result = " ORDER BY domain." + Domain.PROPERTY_CAMPAIGN + " DESC, " +
                    " lower(domain." + Domain.PROPERTY_NAME + ")," +
                    " lower(gp." + GrowingPlan.PROPERTY_NAME + ") NULLS LAST," +
                    " lower(gs." + GrowingSystem.PROPERTY_NAME + ") NULLS LAST," +
                    " lower(pl." + Plot.PROPERTY_NAME + ")," +
                    " lower(z." + Zone.PROPERTY_NAME + ")," +
                    " z." + TopiaEntity.PROPERTY_TOPIA_ID;
        }
        
        return result;
    }
    
    public Set<String> getFilteredZoneIds(EffectiveZoneFilter filter, SecurityContext securityContext) throws TopiaException {
        // used of OUTER JOIN to be able to use ORDER BY on growing system that can be null
        StringBuilder query = new StringBuilder(
                // setup Select because of
                "SELECT z.topiaId FROM " + Zone.class.getName() +
                        " z LEFT OUTER JOIN z." + Zone.PROPERTY_PLOT + " AS pl " +
                        " LEFT OUTER JOIN pl." + Plot.PROPERTY_GROWING_SYSTEM + " gs " +
                        " LEFT OUTER JOIN pl." + Plot.PROPERTY_DOMAIN + " domain " +
                        " LEFT OUTER JOIN gs." + GrowingSystem.PROPERTY_GROWING_PLAN + " gp "
        );
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = new LinkedHashMap<>();
        applyFiltersOnQuery(filter, securityContext, query, args);

        return new HashSet<>(findAll(query +
                        " ORDER BY domain." + Domain.PROPERTY_CAMPAIGN + " DESC, " +
                        " lower(domain." + Domain.PROPERTY_NAME + ")," +
                        " lower(gp." + GrowingPlan.PROPERTY_NAME +") NULLS LAST," +
                        " lower(gs." + GrowingSystem.PROPERTY_NAME + ") NULLS LAST," +
                        " lower(pl." + Plot.PROPERTY_NAME + ")," +
                        " lower(z." + Zone.PROPERTY_NAME + ")," +
                        " z." + TopiaEntity.PROPERTY_TOPIA_ID,
                args));
    }

    private void applyFiltersOnQuery(EffectiveZoneFilter filter, SecurityContext securityContext, StringBuilder query, Map<String, Object> args) {
        ComputedCacheHelper computedCacheHelper = newComputedCacheHelper();
        
        // apply non null filter
        if (filter != null) {
            // selected entities
            query.append(DaoUtils.andAttributeIn("z", TopiaEntity.PROPERTY_TOPIA_ID, args, DaoUtils.getRealIds(filter.getSelectedIds(), Zone.class)));
            // zone name
            query.append(DaoUtils.andAttributeLike("z", Zone.PROPERTY_NAME, args, filter.getZoneName()));
            // zone active
            if (filter.getZoneState() != null) {
                query.append(DaoUtils.andAttributeEquals("z", Zone.PROPERTY_ACTIVE, args, filter.getZoneState()));
            }
            // plot name
            query.append(DaoUtils.andAttributeLike("pl", Plot.PROPERTY_NAME, args, filter.getPlotName()));
            // active plot
            if (filter.getPlotState() != null) {
                query.append(DaoUtils.andAttributeEquals("pl", Plot.PROPERTY_ACTIVE, args, filter.getPlotState()));
            }
            // domain name
            query.append(DaoUtils.andAttributeLike("domain", Domain.PROPERTY_NAME, args, filter.getDomainName()));
            // growing plan name
            query.append(DaoUtils.andAttributeLike("gp", GrowingPlan.PROPERTY_NAME, args, filter.getGrowingPlanName()));
            // growing system name
            query.append(DaoUtils.andAttributeLike("gs", GrowingSystem.PROPERTY_NAME, args, filter.getGrowingSystemName()));
            // campaign
            query.append(DaoUtils.andAttributeEquals("domain", Domain.PROPERTY_CAMPAIGN, args, filter.getCampaign()));

            // cropping plan info
            if (StringUtils.isNotBlank(filter.getCroppingPlanInfo())) {
                String croppingPlanInfo = filter.getCroppingPlanInfo();
                EffectivePerennialCropCycleTopiaDao perenialDao = topiaDaoSupplier.getDao(EffectivePerennialCropCycle.class, EffectivePerennialCropCycleTopiaDao.class);
                EffectiveSeasonalCropCycleTopiaDao seasonalDao = topiaDaoSupplier.getDao(EffectiveSeasonalCropCycle.class, EffectiveSeasonalCropCycleTopiaDao.class);

                Set<String> zoneIds = new HashSet<>(perenialDao.findPerennialCropCycleZoneForCroppingPlanEntryName(croppingPlanInfo));
                zoneIds.addAll(seasonalDao.findSeasonalCropCycleZoneForCroppingPlanEntryName(croppingPlanInfo));

                if (!zoneIds.isEmpty()) {
                    query.append(computedCacheHelper.andIdsIn("z." + Zone.PROPERTY_TOPIA_ID, args, zoneIds));
                }
            }

            // Navigation context
            NavigationContext navigationContext = filter.getNavigationContext();
            if (navigationContext != null) {
                // campaigns
                query.append(DaoUtils.andAttributeInIfNotEmpty("domain", Domain.PROPERTY_CAMPAIGN, args, navigationContext.getCampaigns()));
                // networks
                if (navigationContext.getNetworksCount() > 0) {
                    Set<String> domainIds = networksToDomains(navigationContext.getNetworks());
                    query.append(DaoUtils.andAttributeIn("domain", Domain.PROPERTY_TOPIA_ID, args, domainIds));
                }
                // domains
                Set<String> domainIds = new HashSet<>(DaoUtils.getRealIds(navigationContext.getDomains(), Domain.class));
                if (!navigationContext.getGrowingSystems().isEmpty()) {
                    String hql = "SELECT" +
                            " gs." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID +
                            " FROM " + GrowingSystem.class.getName() + " gs" +
                            " WHERE gs." + GrowingSystem.PROPERTY_TOPIA_ID + " IN :gsIds";
                    List<String> domainsIds = findAll(hql, DaoUtils.asArgsMap("gsIds", navigationContext.getGrowingSystems()));
                    domainIds.addAll(domainsIds);
                }
                if (!navigationContext.getGrowingPlans().isEmpty()) {
                    String hql = "SELECT" +
                            " gp." + GrowingPlan.PROPERTY_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID +
                            " FROM " + GrowingPlan.class.getName() + " gp" +
                            " WHERE gp." + GrowingPlan.PROPERTY_TOPIA_ID + " IN :gpIds";
                    List<String> domainsIds = findAll(hql, DaoUtils.asArgsMap("gpIds", navigationContext.getGrowingPlans()));
                    domainIds.addAll(domainsIds);
                }
                query.append(DaoUtils.andAttributeInIfNotEmpty("domain", Domain.PROPERTY_TOPIA_ID, args, domainIds));
            }
        }

        PlotTopiaDao plotDao = topiaDaoSupplier.getDao(Plot.class, PlotTopiaDao.class);
        Optional<Set<String>> grantedPlotIds = plotDao.toGrantedPlotIds(securityContext);
        String zonePlotId = "z." + Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_TOPIA_ID;
        grantedPlotIds.map(strings -> computedCacheHelper.andIdsIn(zonePlotId, args, strings))
                .ifPresent(query::append);

    }

    protected Set<String> networksToDomains(Set<String> networksIds) {
        return getProjectionHelper().networksToDomains(networksIds);
    }

    /**
     * Find all zone using same zone's duplication code.
     *
     * @return related zones
     */
    public LinkedHashMap<Integer, String> findAllRelatedZones(String code) {
        String query = "SELECT " + PROPERTY_DOMAIN_CAMPAIGN + ", " + Zone.PROPERTY_TOPIA_ID
                + " FROM " + Zone.class.getName()
                + " WHERE " + Zone.PROPERTY_CODE + " = :code"
                + " ORDER BY " + PROPERTY_DOMAIN_CAMPAIGN + " DESC";
        List<Object[]> zones = findAll(query, DaoUtils.asArgsMap("code", code));
        return DaoUtils.toRelatedMap(zones);
    }

    public List<Zone> findZonesWithoutCycle(String zoneId, String domainId) {
        String query = "FROM " + Zone.class.getName() + " z "
                     + "WHERE z." + PROPERTY_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID + " =:domainId "
                     + "AND z." + Zone.PROPERTY_ACTIVE + " = true "
                     + "AND z." + Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_ACTIVE + " = true "
                     + "AND z." + Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_DOMAIN + "." + Domain.PROPERTY_ACTIVE + " = true "
                     + "AND z NOT IN "
                     + "(SELECT epcc." + EffectivePerennialCropCycle.PROPERTY_ZONE + " FROM " + EffectivePerennialCropCycle.class.getName() + " epcc "
                     + " WHERE epcc." + EffectivePerennialCropCycle.PROPERTY_ZONE + "." + PROPERTY_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID + " =:domainId "
                     + ")"
                     + "AND z NOT IN "
                     + "(SELECT escc." + EffectiveSeasonalCropCycle.PROPERTY_ZONE + " FROM " + EffectiveSeasonalCropCycle.class.getName() + " escc "
                     + " WHERE escc." + EffectiveSeasonalCropCycle.PROPERTY_ZONE + "." + PROPERTY_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID + " =:domainId)"
                     + " AND z." + Zone.PROPERTY_TOPIA_ID + " != :zoneId";
        Map<String, Object> args = new LinkedHashMap<>();
        args.put("domainId", domainId);
        args.put("zoneId", zoneId);

        return findAll(query, args);
    }

    public Set<String> forQuery(Collection<String> zonesIds, String subQuery) {

        Map<String, Object> args = new LinkedHashMap<>();
        Set<String> zIds = new HashSet<>(zonesIds);

        Preconditions.checkArgument(!zIds.isEmpty());

        String query = " SELECT z." + Zone.PROPERTY_TOPIA_ID +", ";

        query += "(" + subQuery + ")";

        query += " FROM " + Zone.class.getName() + " z  WHERE 1 = 1 " ;

        query += DaoUtils.andAttributeIn("z", Zone.PROPERTY_TOPIA_ID, args, DaoUtils.getRealIds(zIds, Zone.class));

        Set<String> result = new HashSet<>();
        List<Object[]> zonesTuples = findAll(query, args);
        for (Object[] zoneTuple : zonesTuples) {
            String zoneId = (String) zoneTuple[0];
            Long count = (Long)zoneTuple[1];
            if (count != null && count > 0L) {
                result.add(zoneId);
            }
        }

        return result;
    }

    public Set<String> getZonesUsedForPerformances(Set<String> zonesIds) {
        String query =
            "SELECT COUNT(*) FROM " + Performance.class.getName() + " p " +
            " WHERE z IN ELEMENTS (p." + Performance.PROPERTY_ZONES + ")";
        return forQuery(zonesIds, query);
    }

    public Set<String> getZonesUsedForMesurementSessions(Set<String> zonesIds) {
        String query =
            "SELECT COUNT(*) FROM " + MeasurementSession.class.getName() + " ms " +
            " WHERE ms." + MeasurementSession.PROPERTY_ZONE + " = z";
        return forQuery(zonesIds, query);
    }

    public Set<String> getZonesUsedForEffectivePerennialCropCycles(Set<String> zonesIds) {
        String query =
            "SELECT COUNT(*) FROM " + EffectivePerennialCropCycle.class.getName() + " epcc " +
            " WHERE epcc." + EffectivePerennialCropCycle.PROPERTY_ZONE + "= z";
        return forQuery(zonesIds, query);
    }

    public Set<String> getZonesUsedForEffectiveSeasonalCropCycles(Set<String> zonesIds) {
        String query =
            "SELECT COUNT(*) FROM " + EffectiveSeasonalCropCycle.class.getName() + " escc " +
            " WHERE escc." + EffectiveSeasonalCropCycle.PROPERTY_ZONE + " = z";
        return forQuery(zonesIds, query);
    }

    public List<Zone> findZonesFromDomainCode(String domainCode, SecurityContext securityContext) {
        Map<String, Object> args = new LinkedHashMap<>();

        StringBuilder query = new StringBuilder("SELECT z FROM " + Zone.class.getName() + " z ");
        query.append(" INNER JOIN z." + Zone.PROPERTY_PLOT + " p ");
        query.append(" INNER JOIN p." + Plot.PROPERTY_DOMAIN + " d ");
        query.append(" LEFT JOIN p." + Plot.PROPERTY_GROWING_SYSTEM + " g ");

        query.append(" WHERE d." + Domain.PROPERTY_CODE + " = :domainCode ");
        args.put("domainCode", domainCode);
        query.append(" AND d." + Domain.PROPERTY_ACTIVE + " IS TRUE");
        query.append(" AND p." + Plot.PROPERTY_ACTIVE + " IS TRUE ");
        query.append(" AND z." + Zone.PROPERTY_ACTIVE + " IS TRUE ");
        query.append(" AND (g." + GrowingSystem.PROPERTY_ACTIVE + " IS TRUE  OR g IS NULL)");

        SecurityHelper.addZoneFilter(query, args, securityContext, "z");
        return findAll(query + " ORDER BY lower(z." + Zone.PROPERTY_NAME + "), lower (p." + Plot.PROPERTY_NAME + "), lower(d." + Domain.PROPERTY_NAME + "), d." + Domain.PROPERTY_CAMPAIGN, args);
    }

    public String findDomainIdForZoneId(String zoneId) {
        return findDomainPropertyForZoneId(zoneId, Domain.PROPERTY_TOPIA_ID);
    }

    public int findDomainCampaignForZoneId(String zoneId) {
        return findDomainPropertyForZoneId(zoneId, Domain.PROPERTY_CAMPAIGN);
    }

    protected <O> O findDomainPropertyForZoneId(String zoneId, String property) {
        String query =
                " SELECT z." + Zone.PROPERTY_PLOT + "."+ Plot.PROPERTY_DOMAIN + "." + property +
                        " FROM " + Zone.class.getName() + " z " +
                        " WHERE z." + Zone.PROPERTY_TOPIA_ID + " = :zoneId";
        Map<String, Object> args = new HashMap<>();
        args.put("zoneId", zoneId);
        return findUnique(query, args);
    }

}
