package fr.inra.agrosyst.api.utils;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.ArrayUtil;
import org.nuiton.util.beans.Binder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class TopiaUtils {

    /**
     * Update current collection (database) with new given collection of entities.
     * Allways return a new collection.
     *
     * @param binder binder to use
     * @param origCollection collection of element in database
     * @param currentCollection current collection to save
     * @param <T> type
     * @return new collection to persist
     */
    public static <T extends TopiaEntity> Collection<T> updateCollection(Binder<T, T> binder,
                                                                         Collection<T> origCollection,
                                                                         Collection<T> currentCollection,
                                                                         String... exclusions) {
        origCollection = origCollection == null ? new ArrayList<>() : origCollection;
        currentCollection = CollectionUtils.emptyIfNull(currentCollection);

        Map<String, T> elementsByIds = origCollection.stream().collect(Collectors.toMap(TopiaEntity::getTopiaId, Function.identity()));
        List<T> newCollection = new ArrayList<>(origCollection);
        for (T newElement : currentCollection) {
            T origElement = elementsByIds.get(newElement.getTopiaId());
            if (origElement == null) {
                newCollection.add(newElement);
            } else {
                String[] topiaAttributes = { TopiaEntity.PROPERTY_TOPIA_ID, TopiaEntity.PROPERTY_TOPIA_CREATE_DATE, TopiaEntity.PROPERTY_TOPIA_VERSION };
                binder.copyExcluding(newElement, origElement, ArrayUtil.concatElems(topiaAttributes, exclusions));
                elementsByIds.remove(origElement.getTopiaId());
            }
        }
        newCollection.removeAll(elementsByIds.values());

        return newCollection;
    }

    /**
     * Remove topia related field for a collection.
     *
     * @param collection collection
     * @param <T> type
     */
    public static <T extends TopiaEntity> void cleanTopiaId(Collection<T> collection) {
        collection.forEach(TopiaUtils::cleanTopiaId);
    }

    /**
     * Remove topia related field for a element.
     *
     * @param element element
     * @param <T> type
     */
    @SafeVarargs
    public static <T extends TopiaEntity, O extends TopiaEntity> void cleanTopiaId(T element, Supplier<Collection<O>>... collections) {
        element.setTopiaId(null);
        element.setTopiaCreateDate(null);
        element.setTopiaVersion(0);
        Arrays.stream(collections)
                .map(Supplier::get)
                .filter(Objects::nonNull)
                .forEach(TopiaUtils::cleanTopiaId);
    }
}
