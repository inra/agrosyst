package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import fr.inra.agrosyst.services.referential.ReferentialServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by davidcosse on 16/02/16.
 */
public class RefDestinationTopiaDao extends AbstractRefDestinationTopiaDao<RefDestination> {

    public List<String> getAllDestinationLibelle() {
        List<String> result;
        String query = "SELECT DISTINCT(dest." + RefDestination.PROPERTY_DESTINATION + ") FROM " + RefDestination.class.getName() + " dest";
        result = findAll(query);
        return result;
    }

    public List<RefDestination> getDestinations(
            Language language,
            Map<Pair<String, String>, List<Sector>> sectorsByCodeEspeceBotaniqueCodeQualifiant,
            Collection<WineValorisation> wineValorisations) {

        List<RefDestination> result = new ArrayList<>();

        if ((sectorsByCodeEspeceBotaniqueCodeQualifiant != null &&
                !sectorsByCodeEspeceBotaniqueCodeQualifiant.isEmpty())) {

            List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(language, "dest");

            Map<String, Object> args = Maps.newLinkedHashMap();
            String selectQueryString = String.format("SELECT dest, %s FROM %s dest %s WHERE 1 = 1 ",
                    I18nDaoHelper.buildQuerySelectTranslationProperties(i18nDaoHelpers),
                    getEntityClass().getName(),
                    I18nDaoHelper.buildQueryJoinTranslations(i18nDaoHelpers));
            StringBuilder query = new StringBuilder(selectQueryString);

            int i = 0;
            for (Map.Entry<Pair<String, String>, List<Sector>> sectorsByCodeEspeceBotaniqueCodeQualifant : sectorsByCodeEspeceBotaniqueCodeQualifiant.entrySet()) {

                Pair<String, String> codeEspeceBotaniqueCodeQualifant = sectorsByCodeEspeceBotaniqueCodeQualifant.getKey();
                List<Sector> sectors = sectorsByCodeEspeceBotaniqueCodeQualifant.getValue();
                String codeEspeceBotaniques = codeEspeceBotaniqueCodeQualifant.getLeft();
                String codeEspeceQualifant = codeEspeceBotaniqueCodeQualifant.getRight();

                boolean isWineSpecies = ReferentialServiceImpl.WINE.equals(codeEspeceBotaniques);
                if (isWineSpecies && CollectionUtils.isEmpty(wineValorisations)) {
                    continue;
                }

                String operand = i == 0 ? " AND (" : " OR (";

                // filter on Destination that match same Sectors
                if (CollectionUtils.isNotEmpty(sectors)) {
                    query.append(String.format("%s dest." + RefDestination.PROPERTY_SECTOR + " IN (:sector_%d)", operand, i));
                    args.put("sector_" + i, Sets.newHashSet(sectors));
                } else {
                    query.append(String.format("%s 1=1", operand));
                }

                query.append(" AND (");

                if (isWineSpecies) {

                    // Filter on Quality Criteria that match same codeEspeceBotanique
                    query.append(" dest." + RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE + " = :codeEspeceBotaniques_").append(i);
                    args.put("codeEspeceBotaniques_" + i, codeEspeceBotaniques);

                    query.append(" AND (");
                    query.append(String.format(" dest." + RefDestination.PROPERTY_WINE_VALORISATION + " IN (:wineValorisations_%d) ", i));
                    args.put("wineValorisations_" + i, wineValorisations);

                    query.append(" OR (dest." + RefDestination.PROPERTY_WINE_VALORISATION + " IS NULL )");
                    query.append(" )");

                } else {

                    // Filter on Destination that match same codeEspeceBotanique
                    query.append(" dest." + RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE + " = :codeEspeceBotaniques_").append(i);
                    args.put("codeEspeceBotaniques_" + i, codeEspeceBotaniques);

                    if (StringUtils.isNotBlank(codeEspeceQualifant)) {

                        // include destination with same codeQualifantAEE as the given one
                        query.append(" AND (dest." + RefDestination.PROPERTY_CODE_QUALIFIANT__AEE + " IN (:refCodequalifiantAEE_").append(i).append(")");
                        args.put("refCodequalifiantAEE_" + i, codeEspeceQualifant);

                        // this is to include Destination that does not have any codeQualifantAEE
                        query.append(" OR dest." + RefDestination.PROPERTY_CODE_QUALIFIANT__AEE + " IS NULL )");
                    } else {

                        // include all Destination that does not target a precise codeQualifantAEE
                        query.append(" AND dest." + RefDestination.PROPERTY_CODE_QUALIFIANT__AEE + " IS NULL ");
                    }
                }

                // Include destination that match all species for the same sector
                if (CollectionUtils.isNotEmpty(sectors)) {
                    query.append(" OR (dest." + RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE + " IS NULL ");
                    query.append(" AND dest." + RefDestination.PROPERTY_CODE_QUALIFIANT__AEE + " IS NULL )");
                }

                query.append("))");

                i++;
            }

            query.append(DaoUtils.andAttributeEquals("dest", RefDestination.PROPERTY_ACTIVE, args, true));

            query.append(" ORDER BY dest." + RefDestination.PROPERTY_DESTINATION);

            // cas ou il n'y a qu'une seule espèce, que celle ci est une espèce de type vigne et que aucune valorisation vigne n'a été sélectionnée
            if (args.size() != 1) {
                List<Object[]> resultWithTranslation = findAll(query.toString(), args);
                for (Object[] objects : resultWithTranslation) {
                    RefDestination dest = (RefDestination) objects[0];
                    dest.setDestination_Translated(I18nDaoHelper.tryGetTranslation(objects[1]).orElse(dest.getDestination()));
                    result.add(dest);
                }
            }
        }

        return result;
    }

    public ReferentialTranslationMap fillTranslations(Collection<String> topiaIdList, ReferentialTranslationMap translationMap) {
        List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(translationMap.getLanguage(), "dest");
        I18nDaoHelper.fillRefEntitiesTranslations(this, "dest", i18nDaoHelpers, topiaIdList, translationMap);
        return translationMap;
    }

    protected List<I18nDaoHelper> getI18nDaoHelpers(Language language, String entityAlias) {
        return Lists.newArrayList(
                I18nDaoHelper.withComplexI18nKey(RefDestination.PROPERTY_DESTINATION, language, TradRefDestination.class, entityAlias)
        );
    }
}
