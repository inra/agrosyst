package fr.inra.agrosyst.api.utils;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class containing utilities methods for agrosyst DAO package.
 *
 * @author Eric Chatellier
 * @author Arnaud Thimel (Code Lutin)
 */
public class DaoUtils {
    
    private static final Log LOGGER = LogFactory.getLog(DaoUtils.class);
    
    public static final Integer NO_PAGE_LIMIT = -1;
    
    protected static final String IN =
            " TRANSLATE(LOWER( %s ),"
                    + "'áàâãäåāăąèééêëēĕėęěìíîïìĩīĭḩóôõöōŏőùúûüũūŭůäàáâãåæçćĉčöòóôõøüùúûßéèêëýñîìíïş',"
                    + "'aaaaaaaaaeeeeeeeeeeiiiiiiiihooooooouuuuuuuuaaaaaaeccccoooooouuuuseeeeyniiiis')"
                    + " IN ( %s ) ";
    
    protected static final String LIKE =
            " TRANSLATE(LOWER( %s ),"
            + "'áàâãäåāăąèééêëēĕėęěìíîïìĩīĭḩóôõöōŏőùúûüũūŭůäàáâãåæçćĉčöòóôõøüùúûßéèêëýñîìíïş',"
            + "'aaaaaaaaaeeeeeeeeeeiiiiiiiihooooooouuuuuuuuaaaaaaeccccoooooouuuuseeeeyniiiis')"
            + " %s like LOWER( %s ) ";

    /**
     * Generate sql like operator case and accent insensitive.
     *
     * @param field1 entity field to search into
     * @param field2 value field (must be accent escaped)
     * @return sql string
     */
    public static String getFieldLikeInsensitive(String field1, String field2) {
        return getFieldLikeInsensitive(field1, field2, false);
    }

    public static String getFieldLikeInsensitive(String field1, String field2, boolean not) {
        return String.format(LIKE, field1.replace("'", "''"), not ? "NOT" : "", field2.replace("'", "''"));
    }

    public static String getFieldInInsensitive(String field1, String field2) {
        return String.format(IN, field1.replace("'", "''"), field2.replace("'", "''"));
    }

    public static String addQueryAttribute(Map<String, Object> args, String entityAttributeName, Object value) {
        String baseAttributeName = entityAttributeName.replaceAll("[.]", "_");

        int index = 0;
        String queryAttributeName;
        do {
            queryAttributeName = baseAttributeName + index;
            index++;
        } while (args.containsKey(queryAttributeName));

        args.put(queryAttributeName, value);
        return queryAttributeName;
    }

    public static String getQueryForAttributeLike(String entityAlias,
                                                  String entityAttributeName,
                                                  Map<String, Object> args,
                                                  String likeValue,
                                                  String operator) {
        return getQueryForAttributeLike(entityAlias, entityAttributeName, args, likeValue, operator, false);
    }

    public static String getQueryForAttributeLike(String entityAlias,
                                                  String entityAttributeName,
                                                  Map<String, Object> args,
                                                  String likeValue,
                                                  String operator,
                                                  boolean not) {
        // TODO AThimel 12/07/13 Refactor : peut-être qu'il n'est pas nécessaire d'utiliser la méthode "getFieldLikeInsensitive"
        String alias = StringUtils.isBlank(entityAlias) ? "" : entityAlias + ".";
        String queryAttributeName = addQueryAttribute(args, entityAttributeName, StringUtils.stripAccents(likeValue));

        return String.format(" %s %s ", operator, DaoUtils.getFieldLikeInsensitive(alias + entityAttributeName, ":" + queryAttributeName, not));
    }

    public static String andAttributeLike(String entityAlias, String entityAttributeName, Map<String, Object> args, String value) {
        String result = "";
        if (StringUtils.isNotBlank(value)) {
            result = getQueryForAttributeLike(entityAlias, entityAttributeName, args, "%" + value + "%", "AND");
        }
        return result;
    }

    public static String andNotAttributeLike(String entityAlias, String entityAttributeName, Map<String, Object> args, String value) {
        String result = "";
        if (StringUtils.isNotBlank(value)) {
            result = getQueryForAttributeLike(entityAlias, entityAttributeName, args, "%" + value + "%", "AND", true);
        }
        return result;
    }

    public static String orAttributeLike(String entityAlias, String entityAttributeName, Map<String, Object> args, String value) {
        String result = "";
        if (StringUtils.isNotBlank(value)) {
            result = getQueryForAttributeLike(entityAlias, entityAttributeName, args, "%" + value + "%", "OR");
        }
        return result;
    }
    
    public static String andAttributeStartLike(String entityAlias, String entityAttributeName, Map<String, Object> args, String value) {
        String result = "";
        if (StringUtils.isNotBlank(value)) {
            result = getQueryForAttributeLike(entityAlias, entityAttributeName, args, value + "%", "AND");
        }
        return result;
    }

    public static String orAttributeStartLike(String entityAlias, String entityAttributeName, Map<String, Object> args, String value) {
        String result = "";
        if (StringUtils.isNotBlank(value)) {
            result = getQueryForAttributeLike(entityAlias, entityAttributeName, args, value + "%", "OR");
        }
        return result;
    }
    
    public static String orAttributeEquals(String entityAlias, String entityAttributeName, Map<String, Object> args, Object value) {
        return getQueryForAttributeEquals(entityAlias, entityAttributeName, args, value, "OR");
    }
    
    protected static String getQueryForAttributeEquals(String entityAlias,
                                                       String entityAttributeName,
                                                       Map<String, Object> args,
                                                       Object value,
                                                       String operator) {
        String result = "";
        
        if (value != null) {
            String alias = StringUtils.isBlank(entityAlias) ? "" : entityAlias + ".";
            String queryAttributeName = addQueryAttribute(args, entityAttributeName, value);
            result += String.format(" %s %s = :%s ", operator, alias + entityAttributeName, queryAttributeName.replace("'", "''"));
        }
        
        return result;
    }
    
    protected static String getQueryForAttributeNotEquals(String entityAlias,
                                                          String entityAttributeName,
                                                          Map<String, Object> args,
                                                          Object value,
                                                          String operator) {
        String result = "";
        
        if (value != null) {
            String alias = StringUtils.isBlank(entityAlias) ? "" : entityAlias + ".";
            String queryAttributeName = addQueryAttribute(args, entityAttributeName, value);
            result += String.format(" %s %s != :%s ", operator, alias + entityAttributeName, queryAttributeName.replace("'", "''"));
        }
        
        return result;
    }
    
    public static String andReplaceAttributeEquals(String entityAlias,
                                                   String entityAttributeName,
                                                   String sourceChars,
                                                   String targetChars,
                                                   Map<String, Object> args,
                                                   Object value) {
        String result = "";
        
        if (value != null) {
            String alias = StringUtils.isBlank(entityAlias) ? "" : entityAlias + ".";
            String queryAttributeName = addQueryAttribute(args, entityAttributeName, value);
            result += String.format(" %s replace(%s, '%s', '%s') = :%s ",
                    "AND", alias + entityAttributeName, sourceChars, targetChars, queryAttributeName.replace("'", "''"));
        }
        
        return result;
    }

    public static String andAttributeEquals(String entityAlias, String entityAttributeName, Map<String, Object> args, Object value) {
        return getQueryForAttributeEquals(entityAlias, entityAttributeName, args, value, "AND");
    }

    public static String andAttributeNotEquals(String entityAlias, String entityAttributeName, Map<String, Object> args, Object value) {
        return getQueryForAttributeNotEquals(entityAlias, entityAttributeName, args, value, "AND");
    }

    protected static String getQueryForAttributeIn(String entityAlias, String entityAttributeName, Map<String, Object> args, Collection<?> values, String operator) {
        String result = "";

        if (values != null) {
            if (values.isEmpty()) {
                // Requesting IN with empty list could never return any result. Motivated by issue http://forge.codelutin.com/issues/4748
                result += String.format(" %s 1 = 0 ", operator);
            } else if (values.size() == 1) {
                // Small optimization : use equals when IN is not necessary
                return getQueryForAttributeEquals(entityAlias, entityAttributeName, args, values.iterator().next(), operator);
            } else {
                String alias = StringUtils.isBlank(entityAlias) ? "" : entityAlias + ".";
                String queryAttributeName = addQueryAttribute(args, entityAttributeName, values);
                result += String.format(
                        " %s %s in ( :%s ) ",
                        operator,
                        alias + entityAttributeName,
                        queryAttributeName);
            }
        }

        return result;
    }

    public static String andAttributeInIfNotEmpty(String entityAlias, String entityAttributeName, Map<String, Object> args, Collection<?> values) {
        if (values != null && !values.isEmpty()) {
            return getQueryForAttributeIn(entityAlias, entityAttributeName, args, values, "AND");
        } else {
            return "";
        }
    }
    
    public static String andAttributeIn(String entityAlias, String entityAttributeName, Map<String, Object> args, Collection<?> values) {
        return getQueryForAttributeIn(entityAlias, entityAttributeName, args, values, "AND");
    }
    
    protected static String getQueryForAttributeNotIn(String entityAlias,
                                                      String entityAttributeName,
                                                      Map<String, Object> args,
                                                      Collection<?> values,
                                                      String operator) {
        String result = null;
        
        if (values != null) {
            if (values.isEmpty()) {
                // Requesting NOT IN with empty list will always return results
                result = String.format(" %s 1 = 1 ", operator);
            } else {
                String alias = StringUtils.isBlank(entityAlias) ? "" : entityAlias + ".";
                String queryAttributeName = addQueryAttribute(args, entityAttributeName, values);
                result = String.format(
                        " %s %s not in ( :%s ) ",
                        operator,
                        alias + entityAttributeName,
                        queryAttributeName);
            }
        }
        
        return Strings.nullToEmpty(result);
    }
    
    public static String andAttributeInElements(String entityAlias, String entityAttributeName, Map<String, Object> args, Object value) {
        return getQueryForAttributeInElements(entityAlias, entityAttributeName, args, value, "AND");
    }
    
    public static String orAttributeInElements(String entityAlias,
                                               String entityAttributeName,
                                               Map<String, Object> args,
                                               Object value) {
        
        final String result = getQueryForAttributeInElements(entityAlias,
                entityAttributeName,
                args,
                value,
                "OR");
        return result;
    }
    
    protected static String getQueryForAttributeInElements(String entityAlias,
                                                           String entityAttributeName,
                                                           Map<String, Object> args,
                                                           Object value,
                                                           String operator) {
        String result = "";
        
        if (value != null) {
            String alias = StringUtils.isBlank(entityAlias) ? "" : entityAlias + ".";
            String queryAttributeName = addQueryAttribute(args, entityAttributeName, value);
            result += String.format(
                    " %s :%s in elements ( %s ) ",
                    operator,
                    queryAttributeName,
                    alias + entityAttributeName);
        }
        
        return result;
    }

    public static String andAttributeNotIn(String entityAlias, String entityAttributeName, Map<String, Object> args, Collection<?> values) {
        return getQueryForAttributeNotIn(entityAlias, entityAttributeName, args, values, "AND");
    }

    public static Map<String, Object> asArgsMap() {
        return new LinkedHashMap<>();
    }

    public static Map<String, Object> asArgsMap(String key1, Object value1) {
        Map<String, Object> result = asArgsMap();
        result.put(key1, value1);
        return result;
    }

    public static Map<String, Object> asArgsMap(String key1, Object value1, String key2, Object value2) {
        Map<String, Object> result = asArgsMap(key1, value1);
        result.put(key2, value2);
        return result;
    }

    public static Map<String, Object> asArgsMap(String key1, Object value1, String key2, Object value2, String key3, Object value3) {
        Map<String, Object> result = asArgsMap(key1, value1, key2, value2);
        result.put(key3, value3);
        return result;
    }

    public static Map<String, Object> asArgsMap(String key1, Object value1, String key2, Object value2, String key3, Object value3,String key4, Object value4) {
        Map<String, Object> result = asArgsMap(key1, value1, key2, value2,key3, value3);
        result.put(key4, value4);
        return result;
    }
    
    public static Map<String, Object> asArgsMap(String key1, Object value1, String key2, Object value2, String key3, Object value3,String key4, Object value4, String key5, Object value5) {
        Map<String, Object> result = asArgsMap(key1, value1, key2, value2,key3, value3, key4, value4);
        result.put(key5, value5);
        return result;
    }
    
    public static Map<String, Object> asArgsMap(String key1, Object value1, String key2, Object value2, String key3, Object value3,String key4, Object value4, String key5, Object value5, String key6, Object value6) {
        Map<String, Object> result = asArgsMap(key1, value1, key2, value2,key3, value3, key4, value4, key5, value5);
        result.put(key6, value6);
        return result;
    }
    
    public static Object[] toArgsArray(Map<String, Object> args) {
        Object[] result = new Object[args.size() * 2];
        int index = 0;
        for (Map.Entry<String, Object> entry : args.entrySet()) {
            result[index++] = entry.getKey();
            result[index++] = entry.getValue();
        }
        return result;
    }

    public static LinkedHashMap<Integer, String> toRelatedMap(Collection<Object[]> input) {
        LinkedHashMap<Integer, String> result = new LinkedHashMap<>();
        if (input != null) {
            for (Object[] entry : input) {
                Integer campaign = (Integer) entry[0];
                String topiaId = (String) entry[1];
                result.put(campaign, topiaId);
            }
        }
        return result;
    }

    public static Double median(List<Double> list) {
        Double result = null;
        if (CollectionUtils.isNotEmpty(list)) {
            list.sort(Comparator.naturalOrder());
            int middle = list.size() / 2;
            if (list.size() % 2 == 1) {
                result = list.get(middle);
            } else {
                Double valInf = list.get(middle - 1);
                Double valSup = list.get(middle);
                if (valInf != null && valSup != null) {
                    result = (valInf + valSup) / 2.0;
                }
            }
        }
        return result;
    }
    
    public static String getStringQueryCompatible(Object value) {
        String result = null;
        String stValue = (String) value;
        if (value != null && StringUtils.isNotBlank(stValue) && ((String) value).compareToIgnoreCase("null") != 0) {
            result = "'" + ((String) value).replaceAll("'", "''") + "'";
        }
        return result;
    }
    
    public static String getEnumStringQueryCompatible(Object value, Type type) {
        String result = null;
        if (Enum.class.isAssignableFrom(value.getClass())) {
            result = "'" + ((Enum<?>) value).name() + "'";
        } else if (String.class.isAssignableFrom(value.getClass())) {
            try {
                Class clazz = com.google.common.reflect.TypeToken.of(type).getRawType();
                String name = (String) value;
                name = name.replace("." + clazz.getSimpleName(), "");
                result = "'" + Enum.valueOf(clazz, name).name() + "'";
            } catch (Exception e) {
                LOGGER.error(String.format("Valeur d'enum %s non convertie %s", type.getTypeName(), value));
            }
        }
        return result;
    }

    public static <T> Collection<String> getRealIds(Collection<String> selectedIds, Class<T> clazz) {
        if (selectedIds == null || selectedIds.isEmpty()) {
            return selectedIds;
        }

        String sampleId = selectedIds.iterator().next();
        boolean truncatedId = !sampleId.contains(clazz.getName());

        if (truncatedId) {
            String prefix = clazz.getName() ;
            return selectedIds.stream()
                    .map(id -> prefix + id)
                    .collect(Collectors.toList());
        }

        return selectedIds;
    }

    public static <T> Collection<String> getShortenIds(Collection<String> selectedIds, Class<T> clazz) {
        if (selectedIds == null || selectedIds.isEmpty()) {
            return selectedIds;
        }

        String sampleId = selectedIds.iterator().next();
        boolean truncatedId = sampleId.contains(clazz.getName());

        if (truncatedId) {
            String prefix = clazz.getName() ;
            return selectedIds.stream()
                    .map(id -> StringUtils.remove(id, prefix))
                    .collect(Collectors.toList());
        }

        return selectedIds;
    }
    
}
