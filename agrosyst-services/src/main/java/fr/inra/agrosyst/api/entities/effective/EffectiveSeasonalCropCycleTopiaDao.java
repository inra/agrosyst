package fr.inra.agrosyst.api.entities.effective;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.utils.DaoUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EffectiveSeasonalCropCycleTopiaDao extends AbstractEffectiveSeasonalCropCycleTopiaDao<EffectiveSeasonalCropCycle> {

    public List<String> findSeasonalCropCycleZoneForCroppingPlanEntryName(String croppingPlanEntryName) {
        Map<String, Object> args = new HashMap<>();
        String query = "SELECT zone." + Zone.PROPERTY_TOPIA_ID +
                " FROM " + EffectiveSeasonalCropCycle.class.getName() + " escc" +
                " LEFT JOIN escc." + EffectiveSeasonalCropCycle.PROPERTY_NODES + " node" +
                " LEFT JOIN node." + EffectiveCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY + " cpe" +
                " LEFT JOIN escc." + EffectiveSeasonalCropCycle.PROPERTY_ZONE + " zone" +
                " WHERE " + DaoUtils.getQueryForAttributeLike("cpe", CroppingPlanEntry.PROPERTY_NAME, args, "%" + croppingPlanEntryName + "%", "");
        return findAll(query, args);
    }
}
