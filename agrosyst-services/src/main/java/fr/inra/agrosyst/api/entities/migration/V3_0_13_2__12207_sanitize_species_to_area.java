package fr.inra.agrosyst.api.entities.migration;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.commons.gson.AgrosystGsonSupplier;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class V3_0_13_2__12207_sanitize_species_to_area extends BaseJavaMigration {

    private final Gson gson = new AgrosystGsonSupplier().get();

    private static final Log LOGGER = LogFactory.getLog(V3_0_13_2__12207_sanitize_species_to_area.class);

    String BACKUP_SPECIES_AREA = """
            CREATE TABLE _3_0_13_2__12207_sanitize_species_to_area AS (SELECT topiaid, speciestoarea from domain where speciestoarea is not null and speciestoarea != '');
            """;

    String LOAD_DOMAIN_SPECIES_AREA = """
            SELECT topiaid, speciestoarea from domain where speciestoarea is not null and speciestoarea != '';
            """;

    String CREATE_TABLE_NEW_SPECIES_AREA = """
            CREATE TABLE _3_0_13_2__12207_new_species_to_area (topiaid varchar(255), speciestoarea text);
            """;
    String INSERT_NEW_SPECIES_AREA = """
            INSERT INTO _3_0_13_2__12207_new_species_to_area (topiaid, speciestoarea) VALUES (?, ?);
            """;

    String UPDATE_SPECIES_AREA = """
            update domain d set speciestoarea = nspa.speciestoarea
            from (
              select nspa0.topiaid, nspa0.speciestoarea
              from _3_0_13_2__12207_new_species_to_area nspa0
            ) as nspa
            where nspa.topiaid  = d.topiaid;
            """;

    @Override
    public void migrate(Context context) throws Exception {
        Connection connection = context.getConnection();

        Map<String, String> domainSpeciesAreas = new HashMap<>();

        try (Statement backupDomainSpeciesArea = connection.createStatement()) {
            backupDomainSpeciesArea.execute(BACKUP_SPECIES_AREA);
        }

        try (Statement connectionStatement = connection.createStatement()) {
            connectionStatement.execute(CREATE_TABLE_NEW_SPECIES_AREA);
        }

        try (Statement getDomainSpeciesArea = connection.createStatement()) {
            ResultSet resultSet = getDomainSpeciesArea.executeQuery(LOAD_DOMAIN_SPECIES_AREA);
            while (resultSet.next()) {
                String domainId = resultSet.getString(1);
                String speciesArea = resultSet.getString(2);
                String sanitizedDomainSpeciesToAreaJson = sanitizedDomainSpeciesToAreaJson(speciesArea, domainId);
                if (!speciesArea.contentEquals(sanitizedDomainSpeciesToAreaJson)) {
                    domainSpeciesAreas.put(domainId, sanitizedDomainSpeciesToAreaJson);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }

        try (PreparedStatement insertSpeciesAreaStatement = connection.prepareStatement(INSERT_NEW_SPECIES_AREA)) {
            for (Map.Entry<String, String> domainIdToSpeciesArea : domainSpeciesAreas.entrySet()) {
                String domainId = domainIdToSpeciesArea.getKey();
                insertSpeciesAreaStatement.setString(1, domainId);
                String area = Strings.emptyToNull(domainIdToSpeciesArea.getValue());
                if (area != null) {
                    LOGGER.info(String.format("Update domain %s with speciestoarea = %s", domainId, area));
                    insertSpeciesAreaStatement.setString(2, area);
                } else {
                    insertSpeciesAreaStatement.setNull(2, Types.VARCHAR);
                }
                insertSpeciesAreaStatement.executeUpdate();
            }
        }

        try (Statement updateSpeciesAreaStatement = connection.createStatement()) {
            updateSpeciesAreaStatement.executeUpdate(UPDATE_SPECIES_AREA);
        }


    }

    private Map<String, Double> safelyConvertDomainSpeciesToAreaJson(String json, String domainId) {
        Map<String, Double> speciesToArea = new LinkedHashMap<>();
        if (StringUtils.isNotBlank(json)) {
            try {
                Type type = new TypeToken<Map<String, Double>>() {}.getType();
                speciesToArea = gson.fromJson(json, type);
            } catch (Exception e) {
                String fromJson = json;
                json = json.replace("{", "");
                json = json.replace("}", "");
                String[] parts = json.split(",\"");
                for (String part : parts) {
                    String[] aSpeciesPart = part.split(":");
                    if (aSpeciesPart.length == 2) {
                        String speciesIdentifier = aSpeciesPart[0];
                        String sanitizedStId = speciesIdentifier.replace("\"", "");
                        String stSpeciesArea = aSpeciesPart[1];
                        String sanitizedStArea = stSpeciesArea.replace(",", ".");
                        try {
                            double area = Double.parseDouble(sanitizedStArea);
                            speciesToArea.put(sanitizedStId, area);
                            LOGGER.info(String.format("For domain '%s', json %s convert identifier from %s to %s area from %s to %f ", domainId, fromJson, speciesIdentifier, sanitizedStId, stSpeciesArea, area));
                        } catch (NumberFormatException ex) {
                            LOGGER.error(String.format("Domain '%s', area %s from %s could not be convert to double ", domainId, sanitizedStArea, stSpeciesArea));
                        }
                    } else {
                        LOGGER.error(String.format("Domain '%s', could not get part from %s ", domainId, fromJson));
                    }
                }
            }

        }
        return speciesToArea;
    }

    private String sanitizedDomainSpeciesToAreaJson(String json, String domainId) {
        Map<String, Double> speciesToArea = safelyConvertDomainSpeciesToAreaJson(json, domainId);
        if (!speciesToArea.isEmpty()) {
            Type type = new TypeToken<Map<String, Double>>() {}.getType();
            return gson.toJson(speciesToArea, type);
        }
        return "";
    }

}

