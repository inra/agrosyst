package fr.inra.agrosyst.api.entities.security;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.utils.DaoUtils;

import java.util.List;

public class HashedValueTopiaDao extends AbstractHashedValueTopiaDao<HashedValue> {

    public boolean checkValue(String clear, String hashed) {
        String query = " FROM " + getEntityClass().getName() + " hv " +
                " WHERE hv." + HashedValue.PROPERTY_HASHED + " = :hashed ";

        boolean created = false;
        List<HashedValue> values = findAll(query, DaoUtils.asArgsMap("hashed", hashed));
        if (values == null || values.isEmpty()) {
            create(
                    HashedValue.PROPERTY_RAW, clear,
                    HashedValue.PROPERTY_HASHED, hashed);
            created = true;
        }
        return created;
    }

} //HashedValueTopiaDao
