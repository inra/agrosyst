package fr.inra.agrosyst.api.entities.practiced;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.List;
import java.util.Map;

public class PracticedCropCycleTopiaDao extends AbstractPracticedCropCycleTopiaDao<PracticedCropCycle> {

    protected static final String PRACTICED_SYSTEM_SDC = PracticedCropCycle.PROPERTY_PRACTICED_SYSTEM + "." + PracticedSystem.PROPERTY_GROWING_SYSTEM;
    protected static final String SDC_ID = PRACTICED_SYSTEM_SDC + "." + GrowingSystem.PROPERTY_TOPIA_ID;
    protected static final String SDC_NAME = PRACTICED_SYSTEM_SDC + "." + GrowingSystem.PROPERTY_NAME;
    protected static final String PRACTICED_SYSTEM_SDC_GROWING_PLAN = PRACTICED_SYSTEM_SDC + "." + GrowingSystem.PROPERTY_GROWING_PLAN;
    protected static final String GROWING_PLAN_ID = PRACTICED_SYSTEM_SDC_GROWING_PLAN + "." + GrowingPlan.PROPERTY_TOPIA_ID;
    protected static final String PRACTICED_SYSTEM_SDC_GROWING_PLAN_DOMAIN = PRACTICED_SYSTEM_SDC_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN;
    protected static final String DOMAIN_ID = PRACTICED_SYSTEM_SDC_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID;
    protected static final String DOMAIN_CAMPAIGN = PRACTICED_SYSTEM_SDC_GROWING_PLAN_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN;
    protected static final String PRACTICED_SYSTEM_NAME = PracticedCropCycle.PROPERTY_PRACTICED_SYSTEM + "." + PracticedSystem.PROPERTY_NAME;
    protected static final String PRACTICED_SYSTEM_CAMPAIGNS = PracticedCropCycle.PROPERTY_PRACTICED_SYSTEM + "." + PracticedSystem.PROPERTY_CAMPAIGNS;

    public PaginationResult<PracticedCropCycle> getFilteredCycles(PracticedCropCycleFilter filter) {

        String className = getEntityClass().getName();
        if (filter != null && !Strings.isNullOrEmpty(filter.getPracticedCropCycleType())) {
            className = filter.getPracticedCropCycleType();
        }

        String query = "FROM " + className + " pcc";
        query += " WHERE 1 = 1";
        Map<String, Object> args = Maps.newLinkedHashMap();

        // apply non null filter
        if (filter != null) {

            // PracticedSystem name
            query += DaoUtils.andAttributeLike("pcc", PRACTICED_SYSTEM_NAME, args, filter.getPracticedSystemName());

            // Growing System
            query += DaoUtils.andAttributeLike("pcc", SDC_NAME, args, filter.getGrowingSystemName());

            // PracticedSystem campaigns
            query += DaoUtils.andAttributeLike("pcc", PRACTICED_SYSTEM_CAMPAIGNS, args, filter.getPracticedSystemCampaign());

            // Navigation context
            NavigationContext navigationContext = filter.getNavigationContext();
            if (navigationContext != null) {

                // campaigns
                query += DaoUtils.andAttributeInIfNotEmpty("pcc", DOMAIN_CAMPAIGN, args, navigationContext.getCampaigns());

                // domains
                query += DaoUtils.andAttributeInIfNotEmpty("pcc", DOMAIN_ID, args, DaoUtils.getRealIds(navigationContext.getDomains(), Domain.class));

                // growingPlans
                query += DaoUtils.andAttributeInIfNotEmpty("pcc", GROWING_PLAN_ID, args, DaoUtils.getRealIds(navigationContext.getGrowingPlans(), GrowingPlan.class));

                // growingSystems
                query += DaoUtils.andAttributeInIfNotEmpty("pcc", SDC_ID, args, DaoUtils.getRealIds(navigationContext.getGrowingSystems(), GrowingSystem.class));
            }
        }

        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
        List<PracticedCropCycle> cycles = find(query + " ORDER BY pcc." + PracticedCropCycle.PROPERTY_TOPIA_CREATE_DATE + " DESC, pcc." + PracticedCropCycle.PROPERTY_TOPIA_ID, args, startIndex, endIndex);
        long totalCount = findUnique("SELECT count(*) " + query, args);

        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        PaginationResult<PracticedCropCycle> result = PaginationResult.of(cycles, totalCount, pager);
        return result;
    }

} //PracticedCropCycleTopiaDao<E extends PracticedCropCycle>
