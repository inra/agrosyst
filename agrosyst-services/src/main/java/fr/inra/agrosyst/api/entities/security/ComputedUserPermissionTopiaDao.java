package fr.inra.agrosyst.api.entities.security;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.security.SecurityHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ComputedUserPermissionTopiaDao extends AbstractComputedUserPermissionTopiaDao<ComputedUserPermission> {

    public long getUsersPermissionCount(String userId) {
        String hql = "SELECT COUNT(cup." + ComputedUserPermission.PROPERTY_TOPIA_ID + ")" +
                " FROM " + getEntityClass().getName() + " cup" +
                " WHERE cup." + ComputedUserPermission.PROPERTY_USER_ID + " = :userId";
        return count(hql, DaoUtils.asArgsMap("userId", userId));
    }

    public long getUsersDirtyCount(String userId) {
        String hql = "SELECT COUNT(cup." + ComputedUserPermission.PROPERTY_TOPIA_ID + ")" +
                " FROM " + getEntityClass().getName() + " cup" +
                " WHERE cup." + ComputedUserPermission.PROPERTY_USER_ID + " = :userId" +
                " AND cup." + ComputedUserPermission.PROPERTY_DIRTY + " = :dirty";
        return count(hql, DaoUtils.asArgsMap("userId", userId, "dirty", true));
    }

    public int getMaxAction(String userId, String object, PermissionObjectType type) {

        String hql = "SELECT MAX(cup." + ComputedUserPermission.PROPERTY_ACTION+ ")" +
                " FROM " + getEntityClass().getName() + " cup" +
                " WHERE cup." + ComputedUserPermission.PROPERTY_USER_ID + " = :userId" +
                " AND cup." + ComputedUserPermission.PROPERTY_OBJECT + " = :obj";
        Map<String,Object> argsMap = DaoUtils.asArgsMap("userId", userId, "obj", object);

        hql += DaoUtils.andAttributeEquals("cup", ComputedUserPermission.PROPERTY_TYPE, argsMap, type);

        Integer result = findAnyOrNull(hql, argsMap);
        return MoreObjects.firstNonNull(result, 0);
    }

    protected ComputedUserPermission newPermission(String userId, PermissionObjectType type, String object, int action) {
        ComputedUserPermission result = newInstance();
        result.setUserId(userId);
        result.setType(type);
        result.setObject(object);
        result.setAction(action);
        result.setDirty(false);
        return result;
    }

    protected ComputedUserPermission newReadValidatedPermission(String userId, PermissionObjectType type, String object) {
        return newPermission(userId, type, object, SecurityHelper.PERMISSION_READ_VALIDATED);
    }

    protected ComputedUserPermission newReadPermission(String userId, PermissionObjectType type, String object) {
        return newPermission(userId, type, object, SecurityHelper.PERMISSION_READ_RAW);
    }

    protected ComputedUserPermission newWritePermission(String userId, PermissionObjectType type, String object) {
        return newPermission(userId, type, object, SecurityHelper.PERMISSION_WRITE);
    }

    protected ComputedUserPermission newAdminPermission(String userId, PermissionObjectType type, String object) {
        return newPermission(userId, type, object, SecurityHelper.PERMISSION_ADMIN);
    }

    public ComputedUserPermission newDomainAdminPermission(String userId, String domainCode) {
        return newAdminPermission(userId, PermissionObjectType.DOMAIN_CODE, domainCode);
    }

    public ComputedUserPermission newDomainWritePermission(String userId, String domainCode) {
        return newWritePermission(userId, PermissionObjectType.DOMAIN_CODE, domainCode);
    }

    public ComputedUserPermission newDomainReadPermission(String userId, String domainCode) {
        return newReadPermission(userId, PermissionObjectType.DOMAIN_CODE, domainCode);
    }

    public ComputedUserPermission newDomainReadValidatedPermission(String userId, String domainCode) {
        return newReadValidatedPermission(userId, PermissionObjectType.DOMAIN_CODE, domainCode);
    }

    public ComputedUserPermission newSpecificDomainReadPermission(String userId, String domainId) {
        return newReadPermission(userId, PermissionObjectType.DOMAIN_ID, domainId);
    }

    public ComputedUserPermission newSpecificDomainReadValidatedPermission(String userId, String domainId) {
        return newReadValidatedPermission(userId, PermissionObjectType.DOMAIN_ID, domainId);
    }

    public ComputedUserPermission newGrowingPlanAdminPermission(String userId, String growingPlanCode) {
        return newAdminPermission(userId, PermissionObjectType.GROWING_PLAN_CODE, growingPlanCode);
    }

    public ComputedUserPermission newGrowingPlanWritePermission(String userId, String growingPlanCode) {
        return newWritePermission(userId, PermissionObjectType.GROWING_PLAN_CODE, growingPlanCode);
    }

    public ComputedUserPermission newGrowingPlanReadPermission(String userId, String growingPlanCode) {
        return newReadPermission(userId, PermissionObjectType.GROWING_PLAN_CODE, growingPlanCode);
    }

    public ComputedUserPermission newGrowingPlanReadValidatedPermission(String userId, String growingPlanCode) {
        return newReadValidatedPermission(userId, PermissionObjectType.GROWING_PLAN_CODE, growingPlanCode);
    }

    public ComputedUserPermission newSpecificGrowingPlanReadPermission(String userId, String growingPlanId) {
        return newReadPermission(userId, PermissionObjectType.GROWING_PLAN_ID, growingPlanId);
    }

    public ComputedUserPermission newSpecificGrowingPlanReadValidatedPermission(String userId, String growingPlanId) {
        return newReadValidatedPermission(userId, PermissionObjectType.GROWING_PLAN_ID, growingPlanId);
    }

    public ComputedUserPermission newNetworkAdminPermission(String userId, String networkId) {
        return newAdminPermission(userId, PermissionObjectType.NETWORK_ID, networkId);
    }

    public ComputedUserPermission newGrowingSystemWritePermission(String userId, String growingSystemCode) {
        return newWritePermission(userId, PermissionObjectType.GROWING_SYSTEM_CODE, growingSystemCode);
    }

    public ComputedUserPermission newGrowingSystemReadValidatedPermission(String userId, String growingSystemCode) {
        return newReadValidatedPermission(userId, PermissionObjectType.GROWING_SYSTEM_CODE, growingSystemCode);
    }

    public ComputedUserPermission newGrowingSystemReadPermission(String userId, String growingSystemCode) {
        return newReadPermission(userId, PermissionObjectType.GROWING_SYSTEM_CODE, growingSystemCode);
    }

    public ComputedUserPermission newGrowingSystemAdminPermission(String userId, String growingSystemCode) {
        return newAdminPermission(userId, PermissionObjectType.GROWING_SYSTEM_CODE, growingSystemCode);
    }

    public ComputedUserPermission newSpecificGrowingSystemReadPermission(String userId, String growingSystemId) {
        return newReadPermission(userId, PermissionObjectType.GROWING_SYSTEM_ID, growingSystemId);
    }

    public ComputedUserPermission newSpecificGrowingSystemReadValidatedPermission(String userId, String growingSystemId) {
        return newReadValidatedPermission(userId, PermissionObjectType.GROWING_SYSTEM_ID, growingSystemId);
    }

    public ComputedUserPermission newReportRegionalReadPermission(String userId, String reportRegionalId) {
        return newReadPermission(userId, PermissionObjectType.REPORT_REGIONAL_ID, reportRegionalId);
    }

    public ComputedUserPermission newReportRegionalAdminPermission(String userId, String growingSystemCode) {
        return newAdminPermission(userId, PermissionObjectType.REPORT_REGIONAL_ID, growingSystemCode);
    }

    public void markAsDirtyComputedUserPermissionsForDomains(Collection<String> domainCodes,
                                                             Collection<String> domainIds,
                                                             Collection<String> growingPlanCodes,
                                                             Collection<String> growingPlanIds,
                                                             Collection<String> growingSystemCodes,
                                                             Collection<String> growingSystemIds) {

        List<String> conditions = new ArrayList<>();
        Map<String, Object> args = DaoUtils.asArgsMap();
        addDeleteAllComputedUserPermissionCondition(conditions, args, domainCodes, PermissionObjectType.DOMAIN_CODE, "domainCodes");
        addDeleteAllComputedUserPermissionCondition(conditions, args, domainIds, PermissionObjectType.DOMAIN_ID, "domainIds");
        addDeleteAllComputedUserPermissionCondition(conditions, args, growingPlanCodes, PermissionObjectType.GROWING_PLAN_CODE, "growingPlanCodes");
        addDeleteAllComputedUserPermissionCondition(conditions, args, growingPlanIds, PermissionObjectType.GROWING_PLAN_ID, "growingPlanIds");
        addDeleteAllComputedUserPermissionCondition(conditions, args, growingSystemCodes, PermissionObjectType.GROWING_SYSTEM_CODE, "growingSystemCodes");
        addDeleteAllComputedUserPermissionCondition(conditions, args, growingSystemIds, PermissionObjectType.GROWING_SYSTEM_ID, "growingSystemIds");

        if (!conditions.isEmpty()) {
            String query =
                    "UPDATE " + getEntityClass().getName() + " e " +
                    " SET e." + ComputedUserPermission.PROPERTY_DIRTY + " = true " +
                    " WHERE " + String.join(" OR ", conditions);
            topiaJpaSupport.execute(query, args);
        }
    }
    
    private void addDeleteAllComputedUserPermissionCondition(List<String> conditions, Map<String, Object> args,
                                                             Collection<String> objects,
                                                             PermissionObjectType type, String objectArg) {
        if (CollectionUtils.isNotEmpty(objects)) {
            conditions.add("e." + ComputedUserPermission.PROPERTY_TYPE + " = :" + objectArg + "Type AND "
                    + "e." + ComputedUserPermission.PROPERTY_OBJECT + " IN (:" + objectArg + ")");
            args.put(objectArg, objects);
            args.put(objectArg + "Type", type);
        }
    }
    
    public void deleteAllComputedUserPermissions() {
        String query = "DELETE FROM " + getEntityClass().getName() + " e  WHERE " + TopiaEntity.PROPERTY_TOPIA_ID + " IS NOT NULL";
        topiaJpaSupport.execute(query, DaoUtils.asArgsMap());
    }

    public Set<String> findAllUsersWithObsolete(Date obsoleteDate, boolean dirty) {
        String query = String.format(" SELECT cup.%s ", ComputedUserPermission.PROPERTY_USER_ID);
        query += String.format(" FROM %s cup ", getEntityClass().getName());
        query += String.format(" WHERE cup.%s < :obsoleteDate ", ComputedUserPermission.PROPERTY_TOPIA_CREATE_DATE);
        Map<String, Object> args = DaoUtils.asArgsMap("obsoleteDate", obsoleteDate);
        query += DaoUtils.andAttributeEquals("cup", ComputedUserPermission.PROPERTY_DIRTY, args, dirty);
        List<String> userIds = findAll(query, args);
        Set<String> result = ImmutableSet.copyOf(userIds);
        return result;
    }

    public void markAsDirtyForUserIds(Set<String> nonDirtyOnlyUsers) {
        Preconditions.checkArgument(!nonDirtyOnlyUsers.isEmpty());
        String query = String.format(" UPDATE %s ", getEntityClass().getName());
        query += String.format(" SET %s = true ", ComputedUserPermission.PROPERTY_DIRTY);
        query += " WHERE 1=1 ";
        Map<String, Object> args = DaoUtils.asArgsMap();
        query += DaoUtils.andAttributeIn("", ComputedUserPermission.PROPERTY_USER_ID, args, nonDirtyOnlyUsers);
        topiaJpaSupport.execute(query, args);
    }

    public void deleteByUserIds(Set<String> dirtyOnlyUsers) {
        Preconditions.checkArgument(!dirtyOnlyUsers.isEmpty());
        String query = String.format(" DELETE FROM %s ", getEntityClass().getName());
        query += " WHERE 1=1 ";
        Map<String, Object> args = DaoUtils.asArgsMap();
        query += DaoUtils.andAttributeIn("", ComputedUserPermission.PROPERTY_USER_ID, args, dirtyOnlyUsers);
        topiaJpaSupport.execute(query, args);
    }
    
    public void markAsDirtyForAllUsers() {
        String query = String.format(" UPDATE %s ", getEntityClass().getName());
        query += String.format(" SET %s = true ", ComputedUserPermission.PROPERTY_DIRTY);
        topiaJpaSupport.execute(query, DaoUtils.asArgsMap());
    }

} //ComputedUserPermissionTopiaDao<E extends ComputedUserPermission>
