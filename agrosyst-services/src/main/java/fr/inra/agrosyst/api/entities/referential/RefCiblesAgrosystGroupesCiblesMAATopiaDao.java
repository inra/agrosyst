package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;

import java.util.LinkedList;
import java.util.List;

public class RefCiblesAgrosystGroupesCiblesMAATopiaDao extends AbstractRefCiblesAgrosystGroupesCiblesMAATopiaDao<RefCiblesAgrosystGroupesCiblesMAA> {

    public List<RefCiblesAgrosystGroupesCiblesMAA> findAllActive(Language language) {
        I18nDaoHelper cibleEdiRefLabelI18nDaoHelper =
                I18nDaoHelper.withSimpleI18nKey(
                        RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_LABEL,
                        language,
                        TradRefVivant.class,
                        "rcagcmaa"
                );

        I18nDaoHelper groupeCibleMaaI18nDaoHelper =
                I18nDaoHelper.withSimpleI18nKey(
                        RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_GROUPE_CIBLE_MAA,
                        language,
                        TradRefVivant.class,
                        "rcagcmaa"
                );

        String hql = String.join("\n"
                , "select rcagcmaa"
                    + ", " + cibleEdiRefLabelI18nDaoHelper.coalesceTranslation()
                    + ", " + groupeCibleMaaI18nDaoHelper.coalesceTranslation()
                , newFromClause("rcagcmaa")
                    + cibleEdiRefLabelI18nDaoHelper.leftJoinTranslation()
                    + groupeCibleMaaI18nDaoHelper.leftJoinTranslation()
                , " where rcagcmaa." + RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_ACTIVE + " is true"
        );

        List<RefCiblesAgrosystGroupesCiblesMAA> result = new LinkedList<>();
        this.<Object[]>findAll(hql).forEach(row -> {
            RefCiblesAgrosystGroupesCiblesMAA refCiblesAgrosystGroupesCiblesMAA = (RefCiblesAgrosystGroupesCiblesMAA) row[0];
            String cible_edi_ref_label = (String) row[1];
            String groupe_cible_maa = (String) row[2];
            refCiblesAgrosystGroupesCiblesMAA.setCible_edi_ref_label_Translated(cible_edi_ref_label);
            refCiblesAgrosystGroupesCiblesMAA.setGroupe_cible_maa_Translated(groupe_cible_maa);
            result.add(refCiblesAgrosystGroupesCiblesMAA);
        });

        return result;
    }
}
