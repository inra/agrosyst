package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnection;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.managementmode.Strategy;
import fr.inra.agrosyst.api.entities.measure.MeasurementSession;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.report.ArboCropAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropPestMaster;
import fr.inra.agrosyst.api.entities.report.CropPestMaster;
import fr.inra.agrosyst.api.entities.report.FoodMaster;
import fr.inra.agrosyst.api.entities.report.VerseMaster;
import fr.inra.agrosyst.api.entities.report.YieldLoss;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class CroppingPlanEntryTopiaDao extends AbstractCroppingPlanEntryTopiaDao<CroppingPlanEntry> {

    private static final Log log = LogFactory.getLog(CroppingPlanEntryTopiaDao.class);
    protected static final String PRACTICED_NODE_SOURCE_SEASONAL_CYCLE_PRACTICED_SYSTEM_CAMPAIGNS = PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE + "." + PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM + "." + PracticedSystem.PROPERTY_CAMPAIGNS;
    protected static final String PRACTICED_CONNECTION_SOURCE_SEASONAL_CYCLE_PRACTICED_SYSTEM_CAMPAIGNS = PracticedCropCycleConnection.PROPERTY_SOURCE + "." + PRACTICED_NODE_SOURCE_SEASONAL_CYCLE_PRACTICED_SYSTEM_CAMPAIGNS;
    protected static final String PRACTICED_CONNECTION_TARGET_SEASONAL_CYCLE_PRACTICED_SYSTEM_CAMPAIGNS = PracticedCropCycleConnection.PROPERTY_TARGET + "." + PRACTICED_NODE_SOURCE_SEASONAL_CYCLE_PRACTICED_SYSTEM_CAMPAIGNS;
    protected static final String CROPPING_PLAN_ENTRY_DOMAIN_CAMPAIGN = CroppingPlanEntry.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN;
    public static final String CROPPING_PLAN_ENTRY_DOMAIN_ID = CroppingPlanEntry.PROPERTY_DOMAIN + "." + Domain.PROPERTY_TOPIA_ID;

    public List<CroppingPlanEntry> findEntriesFromCode(String croppingPlanEntryCode, Set<Integer> campaigns) {

        String query = "FROM " + getEntityClass().getName() + " e ";
        Map<String, Object> args = Maps.newLinkedHashMap();

        // code
        query += " WHERE e." + CroppingPlanEntry.PROPERTY_CODE + " = :code";
        args.put("code", croppingPlanEntryCode);

        // campaigns
        query += DaoUtils.andAttributeIn("e", CROPPING_PLAN_ENTRY_DOMAIN_CAMPAIGN, args, campaigns);

        return findAll(query + " ORDER BY e." + CROPPING_PLAN_ENTRY_DOMAIN_CAMPAIGN + " DESC", args);

    }

    public String findFirstEntryNameFromCode(String croppingPlanEntryCode, Set<Integer> campaigns) {

        String query = "FROM " + getEntityClass().getName() + " e ";
        Map<String, Object> args = Maps.newLinkedHashMap();

        // code
        query += " WHERE e." + CroppingPlanEntry.PROPERTY_CODE + " = :code";
        args.put("code", croppingPlanEntryCode);

        // campaigns
        query += DaoUtils.andAttributeIn("e", CROPPING_PLAN_ENTRY_DOMAIN_CAMPAIGN, args, campaigns);

        CroppingPlanEntry first = findFirstOrNull(query + " ORDER BY e." + CROPPING_PLAN_ENTRY_DOMAIN_CAMPAIGN + " DESC", args);

        String result;
        if (first == null) {
            log.error(String.format("Crops with code %s could not be found for campains %s", croppingPlanEntryCode, campaigns));
            CroppingPlanEntry aCrop = findACropFromCode(croppingPlanEntryCode);
            if (aCrop != null) {
                List<Integer> campaignsForCrop = getCampaignsForCrop(aCrop.getCode());
                String cropCampaigns = campaignsForCrop.size() == 1 ? "la campagne " + campaignsForCrop.iterator().next() : "les campagnes " + StringUtils.join(", ", campaignsForCrop);

                result = String.format("%s /!\\ Cette culture n'est pas présente pour les campagne %s mais existe sur %s", aCrop.getName(), campaigns, cropCampaigns);
            } else {
                result = "?";
            }

        } else {
            result = first.getName();
        }
        return result;
    }

    protected CroppingPlanEntry findACropFromCode(String croppingPlanEntryCode) {

        String query = "FROM " + getEntityClass().getName() + " e ";
        Map<String, Object> args = Maps.newLinkedHashMap();

        // code
        query += " WHERE e." + CroppingPlanEntry.PROPERTY_CODE + " = :code";
        args.put("code", croppingPlanEntryCode);

        CroppingPlanEntry croppingPlanEntry = findAnyOrNull(query + " ORDER BY e." + CROPPING_PLAN_ENTRY_DOMAIN_CAMPAIGN + " DESC", args);

        return croppingPlanEntry;
    }

    protected List<Integer> getCampaignsForCrop (String croppingPlanEntryCode) {

        String query = "SELECT d." + Domain.PROPERTY_CAMPAIGN + " FROM " + getEntityClass().getName() + " e ";
        query += " INNER JOIN e." + CroppingPlanEntry.PROPERTY_DOMAIN + " d ";
        Map<String, Object> args = Maps.newLinkedHashMap();

        // code
        query += " WHERE e." + CroppingPlanEntry.PROPERTY_CODE + " = :code";
        args.put("code", croppingPlanEntryCode);

        List<Integer> cropCampaigns = findAll(query + " ORDER BY d." + Domain.PROPERTY_CAMPAIGN, args);

        return cropCampaigns;
    }

    protected Set<String> queryBody(Iterable<String> croppingPlanIds, String campaign, String subQuery) {

        Map<String, Object> args = Maps.newLinkedHashMap();
        Set<String> cpeIds = Sets.newHashSet(croppingPlanIds);

        if (campaign != null) {
            args.put("campaign" , campaign);
        }

        String query = " SELECT cpe." + CroppingPlanEntry.PROPERTY_TOPIA_ID + ", ";

        query += "(" + subQuery + ")";

        query +=" FROM " + CroppingPlanEntry.class.getName() + " cpe WHERE 1 = 1 " ;
        query += DaoUtils.andAttributeIn("cpe", CroppingPlanEntry.PROPERTY_TOPIA_ID, args, cpeIds);

        Set<String> result = Sets.newHashSet();
        List<Object[]> cpes = findAll(query, args);
        for (Object[] cpeTuple : cpes) {
            String cpeId = (String) cpeTuple[0];
            Long count = (Long)cpeTuple[1];
            if (count != null && count > 0L) {
                result.add(cpeId);
            }
        }

        return result;
    }

    public Set<String> getCPEUsedForStrategies(Set<String> cpeIds) {
        String query = "SELECT COUNT(*) FROM " + Strategy.class.getName() + " s " +
                "        INNER JOIN s." + Strategy.PROPERTY_CROPS + " cpe0 " +
                "        WHERE cpe0 = cpe";
        return queryBody(cpeIds, null, query);//crops_strategy
    }

    public Set<String> getCPEUsedForMeasurementSessions(Set<String> cpeIds) {
        String query = "SELECT COUNT(*) FROM "+ MeasurementSession.class.getName() + " ms " +
        "        WHERE ms." + MeasurementSession.PROPERTY_CROPPING_PLAN_ENTRY + " = cpe ";
        return queryBody(cpeIds, null, query);
    }

    public Set<String> getCPEUsedForEffectiveCropCycleNode(Set<String> cpeIds) {
        String query = "SELECT COUNT(*) FROM " + EffectiveCropCycleNode.class.getName() + " eccn " +
        "        WHERE eccn." + EffectiveCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY + " = cpe ";
        return queryBody(cpeIds, null, query);
    }

    public Set<String> getCPEUsedForEffectiveCropCycleConnectionsIntermediate(Set<String> cpeIds) {
        String query = "SELECT COUNT(*) FROM " + EffectiveCropCycleConnection.class.getName() + " eccc " +
        "        WHERE eccc." + EffectiveCropCycleConnection.PROPERTY_INTERMEDIATE_CROPPING_PLAN_ENTRY + " = cpe ";
        return queryBody(cpeIds, null, query);
    }

    public Set<String> getCPEUsedForPracticedCropCycleConnections(Set<String> cpeIds, String campaign) {
        String query = "SELECT COUNT(*) FROM " + PracticedCropCycleConnection.class.getName() + " pccc " +
        "        WHERE pccc." + PracticedCropCycleConnection.PROPERTY_INTERMEDIATE_CROPPING_PLAN_ENTRY_CODE + " = cpe." + CroppingPlanEntry.PROPERTY_CODE +
        "        AND (" +
                "        (pccc." + PRACTICED_CONNECTION_SOURCE_SEASONAL_CYCLE_PRACTICED_SYSTEM_CAMPAIGNS + " LIKE :campaign" +
                "     ) " +
        "             OR " +
        "                (pccc." + PRACTICED_CONNECTION_TARGET_SEASONAL_CYCLE_PRACTICED_SYSTEM_CAMPAIGNS + " LIKE :campaign) " +
                "     ) ";
        return queryBody(cpeIds, campaign, query);
    }

    public Set<String> getCPEUsedForPracticedCropCycleNodes(Set<String> cpeIds, String campaign) {
        String query = "SELECT COUNT(*) FROM " + PracticedCropCycleNode.class.getName() + " pccn " +
        "        WHERE pccn." + PRACTICED_NODE_SOURCE_SEASONAL_CYCLE_PRACTICED_SYSTEM_CAMPAIGNS + " LIKE :campaign " +
        "        AND pccn." + PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE + " = cpe." + CroppingPlanEntry.PROPERTY_CODE;
        return queryBody(cpeIds, campaign, query);
    }

    public Set<String> getCPEUsedForLot(Set<String> cpeIds) {
        String query = "SELECT COUNT(*) FROM " + DomainSeedLotInput.class.getName() + " dsl " +
                "        WHERE dsl."+ DomainSeedLotInput.PROPERTY_CROP_SEED + " = cpe ";
        return queryBody(cpeIds, null, query);
    }

    public Set<String> getCPEUsedForEffectivePerenialCropCycles(Set<String> cpeIds) {
        String query = "SELECT COUNT(*) FROM " + EffectivePerennialCropCycle.class.getName() + " epcc " +
        "        WHERE epcc."+ EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY + " = cpe ";
        return queryBody(cpeIds, null, query);
    }

    public Set<String> getCPEUsedPracticedPerennialCropCycles(Set<String> cpeIds, String campaign) {
        String query = "SELECT COUNT(*) FROM " + PracticedPerennialCropCycle.class.getName() + " ppcc " +
        "        WHERE ppcc." + PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE + " = cpe." + CroppingPlanEntry.PROPERTY_CODE +
        "        AND ppcc." + PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM + "." + PracticedSystem.PROPERTY_CAMPAIGNS + " LIKE :campaign ";
        return queryBody(cpeIds, campaign, query);
    }
    
    public String findCropNameForId(String cropId) {
    
        Map<String, Object> args = Maps.newLinkedHashMap();
    
        String query = "SELECT cp." + CroppingPlanEntry.PROPERTY_NAME + " FROM " + CroppingPlanEntry.class.getName() + " cp " + " WHERE 1 = 1" +
                DaoUtils.andAttributeEquals("cp", CroppingPlanEntry.PROPERTY_TOPIA_ID, args, cropId);
        return findUnique(query, args);
    }

    public List<String> findCropsNamesByIds(Set<String> cropIds) {
        
        List<String> result;

        if(cropIds.size() == 1) {
            result = new ArrayList<>();
            result.add(findCropNameForId(cropIds.iterator().next()));
        } else {
            result = findCropsNamesForCropOrId(cropIds);
        }
        return result;
    }
    
    
    public String findCropNameForCode(String cropCode) {
    
        Map<String, Object> args = Maps.newLinkedHashMap();
    
        String query = "SELECT cp." + CroppingPlanEntry.PROPERTY_NAME +
                " FROM " + CroppingPlanEntry.class.getName() + " cp " +
                " WHERE 1 = 1" +
                DaoUtils.andAttributeEquals("cp", CroppingPlanEntry.PROPERTY_CODE, args, cropCode);
        return findAny(query, args);
    }
    
    protected List<String> findCropsNamesForCropOrId(Set<String> cropCodes) {
        List<String> result;
        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT cp." + CroppingPlanEntry.PROPERTY_NAME + " FROM " + CroppingPlanEntry.class.getName() + " cp ");
        query.append(" WHERE 1 = 1");
        
        Map<String, Object> args = Maps.newLinkedHashMap();
        
        query.append(DaoUtils.andAttributeIn("cp", org.nuiton.topia.persistence.TopiaEntity.PROPERTY_TOPIA_ID, args, cropCodes));
        
        result = findAll(query.toString(), args);
        return result;
    }
    
    public List<String> findCropsCodes(Set<String> cropIds) {
        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT cp." + CroppingPlanEntry.PROPERTY_CODE + " FROM " + CroppingPlanEntry.class.getName() + " cp ");
        query.append(" WHERE 1 = 1");

        Map<String, Object> args = Maps.newLinkedHashMap();
        // domains
        query.append(DaoUtils.andAttributeIn("cp", CroppingPlanEntry.PROPERTY_TOPIA_ID, args, cropIds));
        return findAll(query.toString(), args);
    }

    public List<String> findCropsIdsForGrowingSystemId(String growingSystemId) {
        Map<String, Object> args = Maps.newLinkedHashMap();
    
        String query = "SELECT cp." + CroppingPlanEntry.PROPERTY_TOPIA_ID + " FROM " + CroppingPlanEntry.class.getName() + " cp, " + GrowingSystem.class.getName() + " gs " +
                " WHERE 1 = 1" +
                DaoUtils.andAttributeEquals("gs", GrowingSystem.PROPERTY_TOPIA_ID, args, growingSystemId) +
                " AND cp." + CroppingPlanEntry.PROPERTY_DOMAIN + " = gs." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN;
        return findAll(query, args);
    }

    public Set<String> getCPEUsedForCropPestMaster(Set<String> cpeIds) {
        String query = "SELECT COUNT(*) FROM " + CropPestMaster.class.getName() + " cpm " +
                "        INNER JOIN cpm." + CropPestMaster.PROPERTY_CROPS + " cpe0 " +
                "        WHERE cpe0 = cpe";
        return queryBody(cpeIds, null, query);//croppestmaster_crops
    }

    public Set<String> getCPEUsedForVerseMaster(Set<String> cpeIds) {
        String query = "SELECT COUNT(*) FROM " + VerseMaster.class.getName() + " vm " +
                "        INNER JOIN vm." + VerseMaster.PROPERTY_CROPS + " cpe0 " +
                "        WHERE cpe0 = cpe";
        return queryBody(cpeIds, null, query);
    }

    public Set<String> getCPEUsedForFoodMaster(Set<String> cpeIds) {
        String query = "SELECT COUNT(*) FROM " + FoodMaster.class.getName() + " fm " +
                "        INNER JOIN fm." + FoodMaster.PROPERTY_CROPS + " cpe0 " +
                "        WHERE cpe0 = cpe";//crops_foodmaster
        return queryBody(cpeIds, null, query);
    }

    public Set<String> getCPEUsedForYieldLoss(Set<String> cpeIds) {
        String query = "SELECT COUNT(*) FROM " + YieldLoss.class.getName() + " yl " +
                "        INNER JOIN yl." + YieldLoss.PROPERTY_CROPS + " cpe0 " +
                "        WHERE cpe0 = cpe";
        return queryBody(cpeIds, null, query);
    }

    public Set<String> getCPEUsedForArboCropAdventiceMaster(Set<String> cpeIds) {
        String query = "SELECT COUNT(*) FROM " + ArboCropAdventiceMaster.class.getName() + " aam " +
                "        INNER JOIN aam." + ArboCropAdventiceMaster.PROPERTY_CROPS + " cpe0 " +
                "        WHERE cpe0 = cpe";
        return queryBody(cpeIds, null, query);
    }

    public Set<String> getCPEUsedForArboCropPestMaster(Set<String> cpeIds) {
        String query = "SELECT COUNT(*) FROM " + ArboCropPestMaster.class.getName() + " acpm " +
                "        INNER JOIN acpm." + ArboCropPestMaster.PROPERTY_CROPS + " cpe0 " +
                "        WHERE cpe0 = cpe";
        return queryBody(cpeIds, null, query);
    }

    public List<CroppingPlanEntry> findCroppingPlanEntriesForAllCampaignsDomain(Domain domain) {
        String query = "FROM " + CroppingPlanEntry.class.getName() + " cpe ";
        query += " WHERE cpe." + CroppingPlanEntry.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CODE + " = :domainCode";
        query += " AND cpe." + CroppingPlanEntry.PROPERTY_DOMAIN + "." + Domain.PROPERTY_ACTIVE + " = " + Boolean.TRUE;

        return findAll(query, DaoUtils.asArgsMap("domainCode", domain.getCode()));
    }

    public List<CroppingPlanEntry> findCroppingPlanEntriesForAllCampaignsDomains(Collection<Domain> domains) {
        String query = "FROM " + CroppingPlanEntry.class.getName() + " cpe ";
        query += " WHERE cpe." + CroppingPlanEntry.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CODE + " IN (:domainCode)";
        query += " AND cpe." + CroppingPlanEntry.PROPERTY_DOMAIN + "." + Domain.PROPERTY_ACTIVE + " = " + Boolean.TRUE;

        Set<String> domainCodes = domains.stream().map(Domain::getCode).collect(Collectors.toSet());
        return findAll(query, DaoUtils.asArgsMap("domainCode", domainCodes));
    }

    /**
     * Charge la liste complète des noms d'espèce par code. Utilisé pour optimiser les exports
     */
    public Map<String, String> findAllCropsNamesByCodes() {
        String query = " SELECT cpe." + CroppingPlanEntry.PROPERTY_CODE + "," +
                " cpe." + CroppingPlanEntry.PROPERTY_NAME +
                " FROM " + CroppingPlanEntry.class.getName() + " cpe ";
        List<Object[]> rows = findAll(query);

        Map<String, String> result = new HashMap<>();

        for (Object[] row : rows) {
            String code = (String)row[0];
            String name = (String)row[1];
            result.put(code, name);
        }

        return result;
    }

}
