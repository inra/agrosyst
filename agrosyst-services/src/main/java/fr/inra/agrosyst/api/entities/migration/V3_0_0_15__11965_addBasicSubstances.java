package fr.inra.agrosyst.api.entities.migration;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPC;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitImpl;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRootImpl;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.referential.csv.InternationalizationReferentialModel;
import fr.inra.agrosyst.services.referential.csv.RefActaDosageSPCModel;
import fr.inra.agrosyst.services.referential.csv.RefActaTraitementsProduitsCategModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.nuiton.csv.Import;

import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;

public class V3_0_0_15__11965_addBasicSubstances extends BaseJavaMigration {
    
    private static final Log LOG = LogFactory.getLog(V3_0_0_15__11965_addBasicSubstances.class);
    private final Function<String, String> ROOT_LOWER_CASE = input -> StringUtils.toRootLowerCase(StringUtils.trimToEmpty(StringUtils.stripAccents(input)));

    @Override
    public void migrate(Context context) {

        Date scriptDate = new Date(new java.util.Date().getTime());
        String refActaTraitementsProduitTopiaIdPrefix = RefActaTraitementsProduit.class.getName() + "_";
        String refActaTraitementsProduitsCategTopiaIdPrefix = RefActaTraitementsProduitsCateg.class.getName() + "_";
        String refActaDosageSPCTopiaIdPrefix = RefActaDosageSPC.class.getName() + "_";
        int topiaVersion = 0;
        String source = "AGROSYST";

        Connection connection = context.getConnection();
        // topiaid;topiaversion;topiacreatedate;id_produit;id_traitement;nom_produit;code_traitement;nom_traitement;nodu;code_amm;code_traitement_maa;nom_traitement_maa;etat_usage;refcountry;source;active;
        String insertRefActaTraitementsProduitQuery = "INSERT INTO refactatraitementsproduit (" +
                RefActaTraitementsProduit.PROPERTY_TOPIA_ID + ", " +
                RefActaTraitementsProduit.PROPERTY_TOPIA_VERSION + ", " +
                RefActaTraitementsProduit.PROPERTY_TOPIA_CREATE_DATE + ", " +
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT + ", " +
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT + ", " +
                RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT + ", " +
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT + ", " +
                RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT + ", " +
                RefActaTraitementsProduit.PROPERTY_NODU + ", " +
                RefActaTraitementsProduit.PROPERTY_CODE__AMM + ", " +
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA + ", " +
                RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT_MAA + ", " +
                RefActaTraitementsProduit.PROPERTY_ETAT_USAGE + ", " +
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY + ", " +
                RefActaTraitementsProduit.PROPERTY_SOURCE + ", " +
                RefActaTraitementsProduit.PROPERTY_ACTIVE +
                ") values (" +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "? " +
                ")";

        //     1        2            3                4                5               6               7              8                    9               10     11     12     13      14      15      16          17       18      19
        // topiaid; topiaversion; topiacreatedate; id_traitement; code_traitement; nom_traitement; type_produit; ift_chimique_total; ift_chimique_tot_hts; ift_h; ift_f; ift_i; ift_ts; ift_a; ift_hh; ift_moy_bio; source; active; action
        String insertRefActaTraitementsProduitsCategQuery = "INSERT INTO refactatraitementsproduitscateg (" +
                RefActaTraitementsProduitsCateg.PROPERTY_TOPIA_ID + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_TOPIA_VERSION + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_TOPIA_CREATE_DATE + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_ID_TRAITEMENT + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_CODE_TRAITEMENT + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_NOM_TRAITEMENT + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_TYPE_PRODUIT + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_IFT_CHIMIQUE_TOTAL + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_IFT_CHIMIQUE_TOT_HTS + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_IFT_H + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_IFT_F + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_IFT_I + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_IFT_TS + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_IFT_A + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_IFT_HH + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_IFT_MOY_BIO + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_SOURCE + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_ACTIVE + ", " +
                RefActaTraitementsProduitsCateg.PROPERTY_ACTION +
                ") values (" +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "? " +
                ")";

        //     1         2            3                4              5            6               7              8                      9              10             11              12           13      14           15           16
        // topiaid; topiaversion; topiacreatedate; id_produit; id_traitement; id_culture; dosage_spc_valeur; dosage_spc_commentaire; nom_produit; code_traitement; nom_culture; remarque_culture; source; active; dosage_spc_unite; code_amm
        String insertRefActaDosageSPCQuery = "INSERT INTO refactadosagespc (" +
                RefActaDosageSPC.PROPERTY_TOPIA_ID + ", " +
                RefActaDosageSPC.PROPERTY_TOPIA_VERSION + ", " +
                RefActaDosageSPC.PROPERTY_TOPIA_CREATE_DATE + ", " +
                RefActaDosageSPC.PROPERTY_ID_PRODUIT + ", " +
                RefActaDosageSPC.PROPERTY_ID_TRAITEMENT + ", " +
                RefActaDosageSPC.PROPERTY_ID_CULTURE + ", " +
                RefActaDosageSPC.PROPERTY_DOSAGE_SPC_VALEUR + ", " +
                RefActaDosageSPC.PROPERTY_DOSAGE_SPC_COMMENTAIRE + ", " +
                RefActaDosageSPC.PROPERTY_NOM_PRODUIT + ", " +
                RefActaDosageSPC.PROPERTY_CODE_TRAITEMENT + ", " +
                RefActaDosageSPC.PROPERTY_NOM_CULTURE + ", " +
                RefActaDosageSPC.PROPERTY_REMARQUE_CULTURE + ", " +
                RefActaDosageSPC.PROPERTY_SOURCE + ", " +
                RefActaDosageSPC.PROPERTY_ACTIVE + ", " +
                RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE + ", " +
                RefActaDosageSPC.PROPERTY_CODE_AMM +
                ") values (" +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?" +
                ")";

        //     1         2            3                4          5         6     7       8      9       10
        // topiaid; topiaversion; topiacreatedate; idproduit; nomproduit; amm; active; permis; ammue; refcountry
        String insertRefActaProduitRootQuery = "INSERT INTO refactaproduitroot (" +
                RefActaProduitRoot.PROPERTY_TOPIA_ID + ", " +
                RefActaProduitRoot.PROPERTY_TOPIA_VERSION + ", " +
                RefActaProduitRoot.PROPERTY_TOPIA_CREATE_DATE + ", " +
                RefActaProduitRoot.PROPERTY_ID_PRODUIT + ", " +
                RefActaProduitRoot.PROPERTY_NOM_PRODUIT + ", " +
                RefActaProduitRoot.PROPERTY_AMM + ", " +
                RefActaProduitRoot.PROPERTY_ACTIVE + ", " +
                RefActaProduitRoot.PROPERTY_PERMIS + ", " +
                RefActaProduitRoot.PROPERTY_AMM_UE + ", " +
                RefActaProduitRoot.PROPERTY_REF_COUNTRY +
                ") values (" +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?, " +
                "?" +
                ")";

        String frenhContryTopiaId;
        try (Statement getFrenchCountryIdStatement = connection.createStatement()) {
            ResultSet resultSet = getFrenchCountryIdStatement.executeQuery("SELECT topiaid FROM refcountry WHERE trigram = 'fra'");
            resultSet.next();//1 résulta
            frenhContryTopiaId = resultSet.getString(1);
        } catch (SQLException e) {
            throw new AgrosystTechnicalException("Échec de récupération de topiaId de la france", e);
        }

        LOG.info("Inport RefActaTraitementsProduitsCateg CSV");

        Map<String, RefActaTraitementsProduitsCateg> refActaTraitementsProduitsCategByNomTraitement = new HashMap<>();
        try (Import<RefActaTraitementsProduitsCateg> refActaTraitementsProduitsCategs = Import.newImport(new RefActaTraitementsProduitsCategModel(),
                new InputStreamReader(
                        Objects.requireNonNull(getClass().getResourceAsStream("/db/migration/V3_0_0_15__11965_addBasicSubstancesRefActaTraitementsProduitsCateg.csv")),
                        StandardCharsets.UTF_8
                ))) {
            for (RefActaTraitementsProduitsCateg refActaTraitementsProduitsCategModel : refActaTraitementsProduitsCategs) {
                String nomTraitement = refActaTraitementsProduitsCategModel.getNom_traitement();
                refActaTraitementsProduitsCategByNomTraitement.put(ROOT_LOWER_CASE.apply(nomTraitement), refActaTraitementsProduitsCategModel);
            }

        } catch (Exception e) {
            throw new AgrosystTechnicalException("Échec de récupération des RefActaTraitementsProduitsCateg", e);
        }

        try (PreparedStatement insertRefActaTraitementProduitsCategsStatement = connection.prepareStatement(insertRefActaTraitementsProduitsCategQuery)) {
            try (PreparedStatement insertRefActaTraitementsProduitStatement = connection.prepareStatement(insertRefActaTraitementsProduitQuery)) {

                int startIdTraitementNumber = 1000;

                LOG.info("Inport RefActaTraitementsProduit CSV");

                try (Import<RefActaTraitementsProduit> temporaryRefActaTraitementsProduits = Import.newImport(new RefActaTraitementsProduitModel(),
                        new InputStreamReader(
                                Objects.requireNonNull(getClass().getResourceAsStream("/db/migration/V3_0_0_15__11965_addBasicSubstancesRefActaTraitementsProduit.csv")),
                                StandardCharsets.UTF_8
                        ))) {
                    int refActaTraitementsProduitLine = 2;
                    for (RefActaTraitementsProduit refActaTraitementsProduitModel : temporaryRefActaTraitementsProduits) {

                        final String refActaTopiaId = refActaTraitementsProduitTopiaIdPrefix + UUID.randomUUID();
                        String id_produit = refActaTraitementsProduitModel.getId_produit();
                        int id_traitement = startIdTraitementNumber++;
                        String code_traitement = String.valueOf(id_traitement);
                        String nomProduit = refActaTraitementsProduitModel.getNom_produit();
                        String nomTraitement = refActaTraitementsProduitModel.getNom_traitement();

                        int line = refActaTraitementsProduitLine++;
                        LOG.info(String.format("Ajout de refActaTraitementsProduit %s %d ligne: %d", id_produit, id_traitement, line));

//                         1        2           3                4           5            6           7             8             9    10        11                  12                 13        14        15      16
//                      topiaid;topiaversion;topiacreatedate;id_produit;id_traitement;nom_produit;code_traitement;nom_traitement;nodu;code_amm;code_traitement_maa;nom_traitement_maa;etat_usage;refcountry;source;active;
                        insertRefActaTraitementsProduitStatement.setString(1, refActaTopiaId);
                        insertRefActaTraitementsProduitStatement.setInt(2, 0);
                        insertRefActaTraitementsProduitStatement.setDate(3, scriptDate);
                        insertRefActaTraitementsProduitStatement.setString(4, id_produit);
                        insertRefActaTraitementsProduitStatement.setInt(5, id_traitement);
                        insertRefActaTraitementsProduitStatement.setString(6, nomProduit);
                        insertRefActaTraitementsProduitStatement.setString(7, code_traitement);
                        insertRefActaTraitementsProduitStatement.setString(8, nomTraitement);
                        insertRefActaTraitementsProduitStatement.setBoolean(9, refActaTraitementsProduitModel.isNodu());
                        insertRefActaTraitementsProduitStatement.setString(10, refActaTraitementsProduitModel.getCode_AMM());
                        insertRefActaTraitementsProduitStatement.setString(11, code_traitement);
                        insertRefActaTraitementsProduitStatement.setString(12, refActaTraitementsProduitModel.getNom_traitement_maa());
                        insertRefActaTraitementsProduitStatement.setString(13, refActaTraitementsProduitModel.getEtat_usage());
                        insertRefActaTraitementsProduitStatement.setString(14, frenhContryTopiaId);
                        insertRefActaTraitementsProduitStatement.setString(15, source);
                        insertRefActaTraitementsProduitStatement.setBoolean(16, true);

                        insertRefActaTraitementsProduitStatement.addBatch();

                        RefActaTraitementsProduitsCateg refActaTraitementsProduitsCateg = refActaTraitementsProduitsCategByNomTraitement.get(ROOT_LOWER_CASE.apply(nomTraitement));
                        if (refActaTraitementsProduitsCateg != null) {
                            //     1        2            3                4                5               6               7              8                    9               10     11     12     13      14      15      16          17       18      19
                            // topiaid; topiaversion; topiacreatedate; id_traitement; code_traitement; nom_traitement; type_produit; ift_chimique_total; ift_chimique_tot_hts; ift_h; ift_f; ift_i; ift_ts; ift_a; ift_hh; ift_moy_bio; source; active; action
                            final String refActaCategTopiaId = refActaTraitementsProduitsCategTopiaIdPrefix + UUID.randomUUID();
                            insertRefActaTraitementProduitsCategsStatement.setString(1, refActaCategTopiaId);
                            insertRefActaTraitementProduitsCategsStatement.setInt(2, 0);
                            insertRefActaTraitementProduitsCategsStatement.setDate(3, scriptDate);
                            insertRefActaTraitementProduitsCategsStatement.setInt(4, id_traitement);
                            insertRefActaTraitementProduitsCategsStatement.setString(5, code_traitement);
                            insertRefActaTraitementProduitsCategsStatement.setString(6, refActaTraitementsProduitsCateg.getNom_traitement());
                            insertRefActaTraitementProduitsCategsStatement.setString(7, refActaTraitementsProduitsCateg.getType_produit().name());
                            insertRefActaTraitementProduitsCategsStatement.setBoolean(8, refActaTraitementsProduitsCateg.isIft_chimique_total());
                            insertRefActaTraitementProduitsCategsStatement.setBoolean(9, refActaTraitementsProduitsCateg.isIft_chimique_tot_hts());
                            insertRefActaTraitementProduitsCategsStatement.setBoolean(10, refActaTraitementsProduitsCateg.isIft_h());
                            insertRefActaTraitementProduitsCategsStatement.setBoolean(11, refActaTraitementsProduitsCateg.isIft_f());
                            insertRefActaTraitementProduitsCategsStatement.setBoolean(12, refActaTraitementsProduitsCateg.isIft_i());
                            insertRefActaTraitementProduitsCategsStatement.setBoolean(13, refActaTraitementsProduitsCateg.isIft_ts());
                            insertRefActaTraitementProduitsCategsStatement.setBoolean(14, refActaTraitementsProduitsCateg.isIft_a());
                            insertRefActaTraitementProduitsCategsStatement.setBoolean(15, refActaTraitementsProduitsCateg.isIft_hh());
                            insertRefActaTraitementProduitsCategsStatement.setBoolean(16, refActaTraitementsProduitsCateg.isIft_moy_bio());
                            insertRefActaTraitementProduitsCategsStatement.setString(17, source);
                            insertRefActaTraitementProduitsCategsStatement.setBoolean(18, true);
                            insertRefActaTraitementProduitsCategsStatement.setString(19, refActaTraitementsProduitsCateg.getAction().name());

                            LOG.info(String.format("Ajout de RefActaTraitementProduitsCategs %s %d %s ligne: %d", id_produit, id_traitement, nomTraitement, line));
                            insertRefActaTraitementProduitsCategsStatement.addBatch();
                        }
                    }
                    insertRefActaTraitementsProduitStatement.executeBatch();
                    insertRefActaTraitementProduitsCategsStatement.executeBatch();
                }

            } catch (Exception e) {
                //LOG.error("Exception à l'enregistrement du traitement", e);
                throw new AgrosystTechnicalException("Exception à l'enregistrement du traitement", e);
            }
        } catch (Exception e) {
            //LOG.error("Exception à l'entregistrement de la catégorie de traitement", e);
            throw new AgrosystTechnicalException("Exception à l'enregistrement de la catégorie de traitement", e);
        }

        //id_produit, id_traitement, id_culture, COALESCE(dosage_spc_valeur::numeric, '-1.0'::numeric), dosage_spc_commentaire
        Map<ActaDosageSpcKey, String> keyToRefActaDosageSPCTopiaId = new HashMap<>();
        try (Statement getRefActaDosageSpcKey = connection.createStatement()) {
            ResultSet resultSet = getRefActaDosageSpcKey.executeQuery("SELECT id_produit, id_traitement, id_culture, COALESCE(dosage_spc_valeur::numeric, '-1.0'::numeric), dosage_spc_commentaire, topiaid FROM refactadosagespc");
            while (resultSet.next()) {
                ActaDosageSpcKey actaDosageSpcKey = new ActaDosageSpcKey(
                        resultSet.getString(1),
                        resultSet.getInt(2),
                        resultSet.getInt(3),
                        resultSet.getDouble(4),
                        resultSet.getString(5)
                );
                keyToRefActaDosageSPCTopiaId.put(actaDosageSpcKey, resultSet.getString(6));
            }
        } catch (SQLException e) {
            //LOG.error("Exception à la récupération des clefs RefActaDosageSpc", e);
            throw new AgrosystTechnicalException("Exception à la récupération des clefs RefActaDosageSpc", e);
        }

        try (Import<RefActaDosageSPC> refActaDosageSpcExportRow = Import.newImport(new RefActaDosageSPCModel(),
                new InputStreamReader(
                        Objects.requireNonNull(getClass().getResourceAsStream("/db/migration/V3_0_0_15__11965_addRefDosageSPC.csv")),
                        StandardCharsets.UTF_8
                ))) {
            try (Statement updateRefActaDosageSpcStatement = connection.createStatement()) {
                try (PreparedStatement insertRefActaDosageSpcStatement = connection.prepareStatement(insertRefActaDosageSPCQuery)) {
                    int refActaDosageSpcRootExportRowNumber = 2;

                    for (RefActaDosageSPC refActaDosageSpc : refActaDosageSpcExportRow) {

                        ActaDosageSpcKey key = new ActaDosageSpcKey(
                                refActaDosageSpc.getId_produit(),
                                refActaDosageSpc.getId_traitement(),
                                refActaDosageSpc.getId_culture(),
                                refActaDosageSpc.getDosage_spc_valeur(),
                                refActaDosageSpc.getDosage_spc_commentaire()
                        );
                        String existingRefActaDosageSpcTopiaId = keyToRefActaDosageSPCTopiaId.get(key);
                        if (existingRefActaDosageSpcTopiaId == null) {
                            //refActaDosageSpcExportRow
                            //     1         2            3                4              5            6               7              8                      9              10             11              12           13      14           15           16
                            // topiaid; topiaversion; topiacreatedate; id_produit; id_traitement; id_culture; dosage_spc_valeur; dosage_spc_commentaire; nom_produit; code_traitement; nom_culture; remarque_culture; source; active; dosage_spc_unite; code_amm
                            final String refActaDosageSpcTopiaId = refActaDosageSPCTopiaIdPrefix + UUID.randomUUID();
                            insertRefActaDosageSpcStatement.setString(1, refActaDosageSpcTopiaId);
                            insertRefActaDosageSpcStatement.setInt(2, 0);
                            insertRefActaDosageSpcStatement.setDate(3, scriptDate);
                            insertRefActaDosageSpcStatement.setString(4, refActaDosageSpc.getId_produit());
                            insertRefActaDosageSpcStatement.setInt(5, refActaDosageSpc.getId_traitement());
                            insertRefActaDosageSpcStatement.setInt(6, refActaDosageSpc.getId_culture());
                            insertRefActaDosageSpcStatement.setDouble(7, refActaDosageSpc.getDosage_spc_valeur());
                            insertRefActaDosageSpcStatement.setString(8, refActaDosageSpc.getDosage_spc_commentaire());
                            insertRefActaDosageSpcStatement.setString(9, refActaDosageSpc.getNom_produit());
                            insertRefActaDosageSpcStatement.setString(10, refActaDosageSpc.getCode_traitement());
                            insertRefActaDosageSpcStatement.setString(11, refActaDosageSpc.getNom_culture());
                            insertRefActaDosageSpcStatement.setString(12, refActaDosageSpc.getRemarque_culture());
                            insertRefActaDosageSpcStatement.setString(13, refActaDosageSpc.getSource());
                            insertRefActaDosageSpcStatement.setBoolean(14, refActaDosageSpc.isActive());
                            insertRefActaDosageSpcStatement.setString(15, refActaDosageSpc.getDosage_spc_unite().name());
                            insertRefActaDosageSpcStatement.setString(16, refActaDosageSpc.getCode_amm());

                            insertRefActaDosageSpcStatement.addBatch();
                        } else {
                            LOG.info("UPDATE " + key + " " + existingRefActaDosageSpcTopiaId);
                            //id_produit, id_traitement, id_culture, COALESCE(dosage_spc_valeur::numeric, '-1.0'::numeric), dosage_spc_commentaire
                            // topiaversion; nom_produit; code_traitement; nom_culture; remarque_culture; source; active; dosage_spc_unite; code_amm
                            updateRefActaDosageSpcStatement.executeUpdate(String.format("UPDATE refactadosagespc SET topiaversion = topiaversion +1 WHERE topiaid = '%s' ", existingRefActaDosageSpcTopiaId));
                            updateRefActaDosageSpcStatement.executeUpdate(String.format("UPDATE refactadosagespc SET nom_produit = '%s' WHERE topiaid = '%s' ", refActaDosageSpc.getNom_produit(), existingRefActaDosageSpcTopiaId));
                            updateRefActaDosageSpcStatement.executeUpdate(String.format("UPDATE refactadosagespc SET code_traitement = '%s' WHERE topiaid = '%s' ", refActaDosageSpc.getCode_traitement(), existingRefActaDosageSpcTopiaId));
                            updateRefActaDosageSpcStatement.executeUpdate(String.format("UPDATE refactadosagespc SET nom_culture = '%s' WHERE topiaid = '%s' ", refActaDosageSpc.getNom_culture(), existingRefActaDosageSpcTopiaId));
                            updateRefActaDosageSpcStatement.executeUpdate(String.format("UPDATE refactadosagespc SET remarque_culture = '%s' WHERE topiaid = '%s' ", refActaDosageSpc.getRemarque_culture(), existingRefActaDosageSpcTopiaId));
                            updateRefActaDosageSpcStatement.executeUpdate(String.format("UPDATE refactadosagespc SET source = '%s' WHERE topiaid = '%s' ", refActaDosageSpc.getSource(), existingRefActaDosageSpcTopiaId));
                            updateRefActaDosageSpcStatement.executeUpdate(String.format("UPDATE refactadosagespc SET active = '%b' WHERE topiaid = '%s' ", refActaDosageSpc.isActive(), existingRefActaDosageSpcTopiaId));
                            updateRefActaDosageSpcStatement.executeUpdate(String.format("UPDATE refactadosagespc SET dosage_spc_unite = '%s' WHERE topiaid = '%s' ", refActaDosageSpc.getDosage_spc_unite().name(), existingRefActaDosageSpcTopiaId));
                            updateRefActaDosageSpcStatement.executeUpdate(String.format("UPDATE refactadosagespc SET code_amm = '%s' WHERE topiaid = '%s' ", refActaDosageSpc.getCode_amm(), existingRefActaDosageSpcTopiaId));
                        }
                    }
                    insertRefActaDosageSpcStatement.executeBatch();
                } catch (SQLException e) {
                    //LOG.error("Exception à l'enregistrement de refActaDosageSpc", e);
                    throw new AgrosystTechnicalException("Exception à l'enregistrement de refActaDosageSpc", e);
                }
            } catch (SQLException e) {
                //LOG.error("Exception à l'enregistrement de refActaDosageSpc", e);
                throw new AgrosystTechnicalException("Exception à l'enregistrement de refActaDosageSpc", e);
            }
        }

        LOG.info("Importe le fichier V3_0_0_15__11965_import_02_05_RefActaTraitementsProd.csv");
        try (PreparedStatement insertRefActaTraitementsProduitStatement = connection.prepareStatement(insertRefActaTraitementsProduitQuery)) {
            try (Import<RefActaTraitementsProduit> refActaTraitementsProduitMigs = Import.newImport(new RefActaTraitementsProduitModel(),
                    new InputStreamReader(
                            Objects.requireNonNull(getClass().getResourceAsStream("/db/migration/V3_0_0_15__11965_import_02_05_RefActaTraitementsProd.csv")),
                            StandardCharsets.UTF_8
                    ))) {
                int refActaTraitementsProduitLine = 2;

                for (RefActaTraitementsProduit refActaTraitementsProduitModel : refActaTraitementsProduitMigs) {

                    final String refActaTopiaId = refActaTraitementsProduitTopiaIdPrefix + UUID.randomUUID();
                    String id_produit = refActaTraitementsProduitModel.getId_produit();
                    int id_traitement = refActaTraitementsProduitModel.getId_traitement();
                    String code_traitement = String.valueOf(id_traitement);
                    String nomProduit = refActaTraitementsProduitModel.getNom_produit();
                    String nomTraitement = refActaTraitementsProduitModel.getNom_traitement();

                    int line = refActaTraitementsProduitLine++;
                    LOG.info(String.format("Ajout de refActaTraitementsProduit %s %d ligne: %d", id_produit, id_traitement, line));

                    //                         1        2           3                4           5            6           7             8             9    10        11                  12                 13        14        15      16
                    //                      topiaid;topiaversion;topiacreatedate;id_produit;id_traitement;nom_produit;code_traitement;nom_traitement;nodu;code_amm;code_traitement_maa;nom_traitement_maa;etat_usage;refcountry;source;active;
                    insertRefActaTraitementsProduitStatement.setString(1, refActaTopiaId);
                    insertRefActaTraitementsProduitStatement.setInt(2, 0);
                    insertRefActaTraitementsProduitStatement.setDate(3, scriptDate);
                    insertRefActaTraitementsProduitStatement.setString(4, id_produit);
                    insertRefActaTraitementsProduitStatement.setInt(5, id_traitement);
                    insertRefActaTraitementsProduitStatement.setString(6, nomProduit);
                    insertRefActaTraitementsProduitStatement.setString(7, code_traitement);
                    insertRefActaTraitementsProduitStatement.setString(8, nomTraitement);
                    insertRefActaTraitementsProduitStatement.setBoolean(9, refActaTraitementsProduitModel.isNodu());
                    insertRefActaTraitementsProduitStatement.setString(10, refActaTraitementsProduitModel.getCode_AMM());
                    insertRefActaTraitementsProduitStatement.setString(11, refActaTraitementsProduitModel.getCode_traitement_maa());
                    insertRefActaTraitementsProduitStatement.setString(12, refActaTraitementsProduitModel.getNom_traitement_maa());
                    insertRefActaTraitementsProduitStatement.setString(13, refActaTraitementsProduitModel.getEtat_usage());
                    insertRefActaTraitementsProduitStatement.setString(14, frenhContryTopiaId);
                    insertRefActaTraitementsProduitStatement.setString(15, source);
                    insertRefActaTraitementsProduitStatement.setBoolean(16, true);

                    insertRefActaTraitementsProduitStatement.addBatch();

                }
            }
            insertRefActaTraitementsProduitStatement.executeBatch();
        } catch (SQLException e) {
            //LOG.error("Exception à l'enregistrement des RefActaTraitementsProduit", e);
            throw new AgrosystTechnicalException("Exception à l'enregistrement des RefActaTraitementsProduit", e);
        }


        LOG.info("Importe le fichier V3_0_0_15_11965_import_02_05_RefProduitRoot.csv");
        try (PreparedStatement insertRefActaProduitRootStatement = connection.prepareStatement(insertRefActaProduitRootQuery)) {
            try (Import<RefActaProduitRoot> refActaProduitRootLine = Import.newImport(new RefActaProduitRootModel(),
                    new InputStreamReader(
                            Objects.requireNonNull(getClass().getResourceAsStream("/db/migration/V3_0_0_15_11965_import_02_05_RefProduitRoot.csv")),
                            StandardCharsets.UTF_8
                    ))) {
                int refActaTraitementsProduitLine = 2;

                String refActaProduitRootTopiaIdPrefix = RefActaProduitRoot.class.getName() + "_";
                for (RefActaProduitRoot refActaProduitRoot : refActaProduitRootLine) {

                    final String refActaTopiaId = refActaProduitRootTopiaIdPrefix + UUID.randomUUID();


                    int line = refActaTraitementsProduitLine++;
                    LOG.info(String.format("Ajout de refactaproduitroot %s %s ligne: %d", refActaProduitRoot.getIdProduit(), refActaProduitRoot.getNomProduit(), line));

                    //     1         2            3                4          5         6     7       8      9       10
                    // topiaid; topiaversion; topiacreatedate; idproduit; nomproduit; amm; active; permis; ammue; refcountry
                    insertRefActaProduitRootStatement.setString(1, refActaTopiaId);
                    insertRefActaProduitRootStatement.setInt(2, 0);
                    insertRefActaProduitRootStatement.setDate(3, scriptDate);
                    insertRefActaProduitRootStatement.setString(4, refActaProduitRoot.getIdProduit());
                    insertRefActaProduitRootStatement.setString(5, refActaProduitRoot.getNomProduit());
                    insertRefActaProduitRootStatement.setString(6, refActaProduitRoot.getAmm());
                    insertRefActaProduitRootStatement.setBoolean(7, refActaProduitRoot.isActive());
                    insertRefActaProduitRootStatement.setString(8, refActaProduitRoot.getPermis());
                    insertRefActaProduitRootStatement.setString(9, refActaProduitRoot.getAmmUE());
                    insertRefActaProduitRootStatement.setString(10, frenhContryTopiaId);

                    insertRefActaProduitRootStatement.addBatch();

                }
            }
            insertRefActaProduitRootStatement.executeBatch();
        } catch (SQLException e) {
            //LOG.error("Exception à l'enregistrement des RefActaProduitRoot", e);
            throw new AgrosystTechnicalException("Exception à l'enregistrement des RefActaProduitRoot", e);
        }

        try {
            connection.commit();
        } catch (SQLException e) {
            //LOG.error("Exception au commit", e);
            throw new AgrosystTechnicalException("Exception au commit", e);
        }

    }

    record ActaDosageSpcKey(
        String id_produit,
        Integer id_traitement,
        Integer id_culture,
        Double dosage_spc_valeur,
        String dosage_spc_commentaire) {}


    public static class RefActaTraitementsProduitModel extends InternationalizationReferentialModel<RefActaTraitementsProduit> {

        // for Export
        public RefActaTraitementsProduitModel() {
            super(CSV_SEPARATOR);
            newIgnoredColumn("pays");
            newMandatoryColumn("id_produit", RefActaTraitementsProduit.PROPERTY_ID_PRODUIT);
            newMandatoryColumn("nom_produit", RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT);
            newOptionalColumn("id_traitement", RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, INT_PARSER);
            newOptionalColumn("code_traitement", RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT);
            newMandatoryColumn("nom_traitement", RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT);
            newMandatoryColumn("NODU 2013", RefActaTraitementsProduit.PROPERTY_NODU, CommonService.BOOLEAN_PARSER);
            newOptionalColumn("etat_usage", RefActaTraitementsProduit.PROPERTY_ETAT_USAGE);
            newMandatoryColumn("Source", RefActaTraitementsProduit.PROPERTY_SOURCE);
            newOptionalColumn("code_AMM", RefActaTraitementsProduit.PROPERTY_CODE__AMM);
            newOptionalColumn("code_traitement_maa", RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA);
            newOptionalColumn("nom_traitement_maa", RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT_MAA);
            newOptionalColumn(COLUMN_ACTIVE, RefActaTraitementsProduit.PROPERTY_ACTIVE, ACTIVE_PARSER);
            newIgnoredColumn("Status");
        }

        @Override
        public RefActaTraitementsProduit newEmptyInstance() {
            RefActaTraitementsProduit refActaTraitementsProduit = new RefActaTraitementsProduitImpl();
            refActaTraitementsProduit.setActive(true);
            return refActaTraitementsProduit;
        }
    }

    public static class RefActaProduitRootModel extends InternationalizationReferentialModel<RefActaProduitRoot> {

        // for import
        public RefActaProduitRootModel() {
            super(CSV_SEPARATOR);

            newIgnoredColumn("pays");
            newMandatoryColumn("idProduit", RefActaProduitRoot.PROPERTY_ID_PRODUIT, EMPTY_TO_NULL);
            newMandatoryColumn("nomProduit", RefActaProduitRoot.PROPERTY_NOM_PRODUIT);
            newMandatoryColumn("amm", RefActaProduitRoot.PROPERTY_AMM, EMPTY_TO_NULL);
            newMandatoryColumn("ammUE", RefActaProduitRoot.PROPERTY_AMM_UE, EMPTY_TO_NULL);
            newMandatoryColumn("permis", RefActaProduitRoot.PROPERTY_PERMIS, EMPTY_TO_NULL);
            newOptionalColumn(COLUMN_ACTIVE, RefActaProduitRoot.PROPERTY_ACTIVE, ACTIVE_PARSER);
            newIgnoredColumn("status");
        }

        @Override
        public RefActaProduitRoot newEmptyInstance() {
            RefActaProduitRoot refActaProduitRoot = new RefActaProduitRootImpl();
            refActaProduitRoot.setActive(true);
            return refActaProduitRoot;
        }
    }
}
