package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.services.referential.RefPotDto;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.List;

public class RefPotTopiaDao extends AbstractRefPotTopiaDao<RefPot> {

    private static final String ALIAS = "p";

    public List<RefPotDto> findAllActivePots(Language language) {
        List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(language);

        String query = "SELECT " + ALIAS + ", " + I18nDaoHelper.buildQueryTradObjectAlias(i18nDaoHelpers) +
                newFromClause("p") +
                I18nDaoHelper.buildQueryJoinTranslations(i18nDaoHelpers) +
                " WHERE " + ALIAS + "." + RefPot.PROPERTY_ACTIVE + " = true";

        List<Object[]> resultWithTranslation = findAll(query);
        Binder<RefPot, RefPotDto> binder = BinderFactory.newBinder(RefPot.class, RefPotDto.class);
        return resultWithTranslation.stream()
                .map(objects -> {
                    RefPotDto refPot = new RefPotDto();
                    binder.copy((RefPot) objects[0], refPot);
                    refPot.setCaracteristic1Translated(I18nDaoHelper.tryGetTranslation(objects[1]).orElse(refPot.getCaracteristic1()));
                    return refPot;
                })
                .toList();
    }

    public ReferentialTranslationMap fillTranslations(Collection<String> topiaIdList, ReferentialTranslationMap translationMap) {
        List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(translationMap.getLanguage());
        I18nDaoHelper.fillRefEntitiesTranslations(this, ALIAS, i18nDaoHelpers, topiaIdList, translationMap);
        return translationMap;
    }

    protected List<I18nDaoHelper> getI18nDaoHelpers(Language language) {
        return List.of(
                I18nDaoHelper.withSimpleI18nKey(RefPot.PROPERTY_CARACTERISTIC1, language, TradRefIntrant.class, ALIAS)
        );
    }

} //RefPotTopiaDao
