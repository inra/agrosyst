package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class RefOrientationEDITopiaDao extends AbstractRefOrientationEDITopiaDao<RefOrientationEDI> {

    public List<RefOrientationEDI> findAllOrientationEdi(Language language) {

        I18nDaoHelper libelleI18nDaoHelper =
                I18nDaoHelper.withSimpleI18nKey(
                        RefOrientationEDI.PROPERTY_REFERENCE_LABEL,
                        language,
                        TradRefDivers.class,
                        "roe"
                );

        String hql = "SELECT roe, " + libelleI18nDaoHelper.getAlias()
                + newFromClause("roe")
                + libelleI18nDaoHelper.leftJoinTranslation()
                + "WHERE roe." + RefOrientationEDI.PROPERTY_ACTIVE + " = true";

        List<Object[]> resultWithTranslation = findAll(hql);
        List<RefOrientationEDI> result = resultWithTranslation.stream()
                .map(objects -> {
                    RefOrientationEDI refOrientationEDI = (RefOrientationEDI) objects[0];
                    I18nDaoHelper.tryGetTranslation(objects[1]).ifPresent(refOrientationEDI::setReference_label);
                    return refOrientationEDI;
                })
                .sorted(Comparator.comparing(RefOrientationEDI::getReference_label))
                .collect(Collectors.toList());

        return result;
    }

} //RefOrientationEDITopiaDao
