package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Splitter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class RefLocationTopiaDao extends AbstractRefLocationTopiaDao<RefLocation> {

    public static final Function<String, String> NORMALIZE_COMMUNE_FOR_SEARCH = input -> {
        String  result = null;
        if (input != null) {
            result = input.toLowerCase().trim();
            Iterable<String> parts = Splitter.onPattern("[\\s-_,.]").trimResults().omitEmptyStrings().split(result); // C'est surtout le trimResults et omitEmptyStrings qui sont utiles ici
            result = String.join("-", parts);
            if (result.startsWith("la-") || result.startsWith("le-") || result.startsWith("les-") || result.startsWith("los-")) {
                result = result.replaceFirst("-", " ");
            }
            result = result.replaceAll("-d-", "-d'");
        }
        return result;
    };

    /**
     * Search all RefCommuneValues matching user custom additional {@code filter}.
     *
     * @param filter custom filter
     * @return matching domains
     * @throws TopiaException invalid query exception
     */
    public List<RefLocation> findActiveLocations(String filter, String refCountryId, int maxResults) {
        String query = "FROM " + getEntityClass().getName() + " rl";
        query += " WHERE 1 = 1";
        Map<String, Object> args = new HashMap<>();

        query += DaoUtils.andAttributeEquals("rl", RefLocation.PROPERTY_REF_COUNTRY + "." + TopiaEntity.PROPERTY_TOPIA_ID, args, refCountryId);

        // apply non null filter
        if (filter != null) {
            String escapedFilter = NORMALIZE_COMMUNE_FOR_SEARCH.apply(filter);
            if (StringUtils.isNotBlank(escapedFilter)) {
                query += " AND ( 1 = 0 ";
                // Post code
                query += DaoUtils.orAttributeStartLike("rl", RefLocation.PROPERTY_CODE_POSTAL, args, escapedFilter);

                // Town name
                query += DaoUtils.orAttributeLike("rl", RefLocation.PROPERTY_COMMUNE, args, escapedFilter);

                query += " ) ";
            }
        }

        // active
        query += DaoUtils.andAttributeEquals("rl", RefLocation.PROPERTY_ACTIVE, args, true);

        query += " ORDER BY rl." + RefLocation.PROPERTY_COMMUNE;

        List<RefLocation> result = find(query, args, 0, maxResults);

        return result;
    }

}
