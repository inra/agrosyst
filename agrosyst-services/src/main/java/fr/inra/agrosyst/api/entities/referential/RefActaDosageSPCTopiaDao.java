package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.referential.ReferentialServiceImpl;
import org.apache.commons.lang3.ObjectUtils;
import org.nuiton.topia.persistence.support.SqlFunction;

import java.sql.ResultSet;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class RefActaDosageSPCTopiaDao extends AbstractRefActaDosageSPCTopiaDao<RefActaDosageSPC> {
    
    public Optional<RefActaDosageSPC> findMinimalValueBySpeciesIds(String phytoProductId, int idTraitement, Set<Integer> actaSpeciesIdCultures) {
        Map<String, Object> args = new HashMap<>();
        String hql = " FROM " + getEntityClass().getName() + " d " +
                " WHERE 1 = 1 ";
        hql += DaoUtils.andAttributeEquals("d", RefActaDosageSPC.PROPERTY_ACTIVE, args, true);
        hql += DaoUtils.andAttributeEquals("d", RefActaDosageSPC.PROPERTY_ID_PRODUIT, args, phytoProductId);
        hql += DaoUtils.andAttributeEquals("d", RefActaDosageSPC.PROPERTY_ID_TRAITEMENT, args, idTraitement);
        hql += DaoUtils.andAttributeIn("d", RefActaDosageSPC.PROPERTY_ID_CULTURE, args, actaSpeciesIdCultures);
        hql += " AND d." + RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE + " IS NOT NULL ";
        hql += " AND d." + RefActaDosageSPC.PROPERTY_DOSAGE_SPC_VALEUR + " IS NOT NULL ";
        String query = hql + " ORDER BY d." + RefActaDosageSPC.PROPERTY_DOSAGE_SPC_VALEUR + " ASC ";
        
        RefActaDosageSPC result = findFirstOrNull(query, args);
        return Optional.ofNullable(result);
    }

    public Optional<RefActaDosageSPC> findMinimalValueByGroupId(String phytoProductId, int idTraitement, int actaDosageSpcCroppingZonesGroupId) {
        Map<String, Object> args = new HashMap<>();
        String hql = " FROM " + getEntityClass().getName() + " d " +
                " WHERE 1 = 1 ";
        hql += DaoUtils.andAttributeEquals("d", RefActaDosageSPC.PROPERTY_ACTIVE, args, true);
        hql += DaoUtils.andAttributeEquals("d", RefActaDosageSPC.PROPERTY_ID_PRODUIT, args, phytoProductId);
        hql += DaoUtils.andAttributeEquals("d", RefActaDosageSPC.PROPERTY_ID_TRAITEMENT, args, idTraitement);
        hql += DaoUtils.andAttributeEquals("d", RefActaDosageSPC.PROPERTY_ID_CULTURE, args, actaDosageSpcCroppingZonesGroupId);
        hql += " AND d." + RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE + " IS NOT NULL ";
        String query = hql + " ORDER BY d." + RefActaDosageSPC.PROPERTY_DOSAGE_SPC_VALEUR + " ASC ";

        List<RefActaDosageSPC> results = findAll(query, args);

        Optional<RefActaDosageSPC> actaRefDoseForSpecies = results.stream()
                .filter(refActaDosageSPC -> refActaDosageSPC.getDosage_spc_valeur() != null)
                .findFirst();

        if (actaRefDoseForSpecies.isEmpty()) {
            actaRefDoseForSpecies = results.stream()
                    .findFirst();
        }

        return actaRefDoseForSpecies;
    }

    public Optional<RefActaDosageSPC> findMinimalValueByAMM(String code_AMM, int idTraitement, Set<Integer> actaSpeciesIdCultures) {

        //- Si on ne trouve pas id_produit | id_traitement | (id_culture + id_culture des groupes culture) dans ACTA_Dosage_SPC_Complet,
        //alors rechercher dans ACTA_Dosage_SPC_Complet selon le triplet code_amm | id_traitement |(id_culture+id_culture des groupes culture), et retenir comme Dose_Ref la plus petite des valeurs trouvées
        Map<String, Object> args = new HashMap<>();
        String hql = " FROM " + getEntityClass().getName() + " d " +
                " WHERE 1 = 1 ";
        hql += DaoUtils.andAttributeEquals("d", RefActaDosageSPC.PROPERTY_ACTIVE, args, true);
        hql += DaoUtils.andAttributeEquals("d", RefActaDosageSPC.PROPERTY_CODE_AMM, args, code_AMM);
        hql += DaoUtils.andAttributeEquals("d", RefActaDosageSPC.PROPERTY_ID_TRAITEMENT, args, idTraitement);
        hql += DaoUtils.andAttributeIn("d", RefActaDosageSPC.PROPERTY_ID_CULTURE, args, actaSpeciesIdCultures);
        hql += " AND d." + RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE + " IS NOT NULL ";

        List<RefActaDosageSPC> res = findAll(hql, args);

        // Ça a du sens d'avoir une dose sans unité pour l'INRAE, ça signifie que le produit existe mais n'a pas de dose de référence.
        Optional<RefActaDosageSPC> actaRefDoseForSpecies = res.stream()
                .min(Comparator.<RefActaDosageSPC>comparingDouble(
                                dose -> ReferentialServiceImpl.REFERENCE_DOSE_UNIT_ORDER.indexOf(dose.getDosage_spc_unite()))
                        .thenComparingDouble(
                                rad -> ObjectUtils.firstNonNull(rad.getDosage_spc_valeur(), Double.MAX_VALUE)));

        return actaRefDoseForSpecies;
    }

    public List<PhytoProductUnit> findPhytoProductUnitsForProduct(RefActaTraitementsProduit refActaTraitementsProduit) {
        final Map<String, Object> args = new HashMap<>();
        final String hqlQuery = getHqlQueryForUnits(refActaTraitementsProduit, args, false);
        final List<PhytoProductUnit> results = findAll(hqlQuery, args);

        return results.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public List<PhytoProductUnit> findPhytoProductUnitsForCodeAmm(RefActaTraitementsProduit refActaTraitementsProduit) {
        final Map<String, Object> args = new HashMap<>();
        final String hqlQuery = getHqlQueryForUnits(refActaTraitementsProduit, args, true);
        final List<PhytoProductUnit> results = findAll(hqlQuery, args);

        return results.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private String getHqlQueryForUnits(RefActaTraitementsProduit refActaTraitementsProduit, Map<String, Object> args, boolean byCodeAmm) {
        String hql = "SELECT DISTINCT(" + RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE + ") FROM " + getEntityClass().getName() + " d " +
                " WHERE 1 = 1 ";

        hql += DaoUtils.andAttributeEquals("d", RefActaDosageSPC.PROPERTY_ACTIVE, args, true);
        if (!byCodeAmm) {
            hql += DaoUtils.andAttributeEquals("d", RefActaDosageSPC.PROPERTY_ID_PRODUIT, args, refActaTraitementsProduit.getId_produit());
        } else {
            hql += DaoUtils.andAttributeEquals("d", RefActaDosageSPC.PROPERTY_CODE_AMM, args, refActaTraitementsProduit.getCode_AMM());
        }
        hql += DaoUtils.andAttributeEquals("d", RefActaDosageSPC.PROPERTY_ID_TRAITEMENT, args, refActaTraitementsProduit.getId_traitement());
        hql += " ORDER BY d." + RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE;

        return hql;
    }

    public List<PhytoProductUnit> findRefActaDosageSpcDosageUnits(boolean byCodeAmm, AgrosystInterventionType action) {
        String sql = "select distinct(radspc." + RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE + ")";
        sql += " from " + RefActaTraitementsProduit.class.getSimpleName() + " ratp";
        sql += " inner join " + RefActaDosageSPC.class.getSimpleName() + " radspc on %s and radspc." + RefActaDosageSPC.PROPERTY_ID_TRAITEMENT + " = ratp." + RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT;
        sql += " inner join " + RefActaTraitementsProduitsCateg.class.getSimpleName() + " ratpc on ratp." + RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT + " = ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ID_TRAITEMENT;
        sql += " where ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTION + " = '%s'";
        sql += " and ratpc." + RefActaTraitementsProduitsCateg.PROPERTY_ACTIVE + " is true";
        sql += " and radspc." + RefActaDosageSPC.PROPERTY_ACTIVE + " is true";
        sql += " and radspc." + RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE + " is not null";
        sql += " and ratp." + RefActaTraitementsProduit.PROPERTY_ACTIVE + " is true";
        sql += " order by " + RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE;

        String filterPart;
        if (byCodeAmm) {
            filterPart = "radspc."+ RefActaDosageSPC.PROPERTY_CODE_AMM +" = ratp." + RefActaTraitementsProduit.PROPERTY_CODE__AMM;
        } else {
            filterPart = "radspc."+ RefActaDosageSPC.PROPERTY_ID_PRODUIT +" = ratp." + RefActaTraitementsProduit.PROPERTY_ID_PRODUIT;
        }

        String result = String.format(sql, filterPart, action.name());

        SqlFunction<ResultSet, PhytoProductUnit> transformer = resultSet -> PhytoProductUnit.valueOf(resultSet.getString(1));

        List<PhytoProductUnit> doseUnits = topiaSqlSupport.findMultipleResult(result, transformer);

        return doseUnits;
    }

} //RefActaDosageSPCTopiaDao
