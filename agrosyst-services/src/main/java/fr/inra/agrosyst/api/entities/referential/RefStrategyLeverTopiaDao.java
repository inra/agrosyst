package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.managementmode.StrategyType;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RefStrategyLeverTopiaDao extends AbstractRefStrategyLeverTopiaDao<RefStrategyLever> {

    final Map<String, StrategyType> strategyTypeByTrads = new HashMap<>();


    public RefStrategyLeverTopiaDao() {
        fillUpStrategyTypeByTrad();
    }


    public void fillUpStrategyTypeByTrad() {
        strategyTypeByTrads.put("Autre", StrategyType.AUTRE);
        strategyTypeByTrads.put("Contrôle cultural (action sur inoculum ou population)", StrategyType.ACTION_INOCULUM);
        strategyTypeByTrads.put("Contrôle cultural (évitement)", StrategyType.EVITEMENT);
        strategyTypeByTrads.put("Contrôle cultural (atténuation)", StrategyType.ATTENUATION);
        strategyTypeByTrads.put("Lutte physique", StrategyType.LUTTE_PHYSIQUE);
        strategyTypeByTrads.put("Lutte biologique", StrategyType.LUTTE_BIOLOGIQUE);
        strategyTypeByTrads.put("Lutte chimique", StrategyType.LUTTE_CHIMIQUE);
        strategyTypeByTrads.put("Contrôle génétique", StrategyType.GENETIC_CONTROL);
        strategyTypeByTrads.put("Biocontrôle", StrategyType.BIOCONTROL);
        strategyTypeByTrads.put("Limitation de l'inoculum", StrategyType.LIMITATION_INOCULUM);
        strategyTypeByTrads.put("Solarisation - biodésinfection", StrategyType.SOLARISATION_BIODESINFECTION);
        strategyTypeByTrads.put("Atténuation de la pression bioagresseur", StrategyType.ATTENUATION_PRESSION_BIOAGRESSEUR);
        strategyTypeByTrads.put("Produits liste biocontrôle - PNPP - biostimulant", StrategyType.PRODUITS_BIOCONTROLE_PNPP_BIOSTIMULANT);
        strategyTypeByTrads.put("Évitement du bioagresseur", StrategyType.EVITEMENT_BIOAGRESSEUR);
        strategyTypeByTrads.put("Génétique", StrategyType.GENETIQUE);
        strategyTypeByTrads.put("Lâchers d'auxiliaires (macro-organismes)", StrategyType.LACHERS_AUXILIAIRES);
        strategyTypeByTrads.put("Usage optimisé de PPP hors biocontrôle", StrategyType.USAGE_OPTIMISE_PPP_HORS_BIOCONTROLE);
        strategyTypeByTrads.put("Barrière physique (voile, filet, mulch, paillages)", StrategyType.BARRIERE_PHYSIQUE);
        strategyTypeByTrads.put("Mécanique", StrategyType.MECANIQUE);
        strategyTypeByTrads.put("Plantes de services", StrategyType.PLANTES_SERVICES);
        strategyTypeByTrads.put("Gestion de la fertilisation et de l'irrigation", StrategyType.GESTION_FERTILISATION_IRRIGATION);
        strategyTypeByTrads.put("Éléments de pilotage", StrategyType.ELEMENTS_PILOTAGE);
        strategyTypeByTrads.put("Régulation naturelle", StrategyType.REGULATION_NATURELLE);
        strategyTypeByTrads.put("Piégeage de masse", StrategyType.PIEGEAGE_MASSE);
        strategyTypeByTrads.put("Rotation", StrategyType.ROTATION);
        strategyTypeByTrads.put("Thermique", StrategyType.THERMIQUE);
        strategyTypeByTrads.put("Manuel", StrategyType.MANUEL);
        strategyTypeByTrads.put("Produits de biocontrôle: substances naturelles, micro-organismes", StrategyType.PRODUITS_BIOCONTROLE_SUBSTANCES_NATURELLES_MICRO_ORGANISMES);
        strategyTypeByTrads.put("Lutte biologique macro-organismes", StrategyType.LUTTE_BIOLOGIQUE_MACRO_ORGANISMES);
        strategyTypeByTrads.put("Utilisation d'OAD", StrategyType.UTILISATION_OAD);
        strategyTypeByTrads.put("Utilisation de variété résistantes", StrategyType.UTILISATION_VARIETE_RESISTANTES);
        strategyTypeByTrads.put("Travail du sol", StrategyType.TRAVAIL_SOL);
        strategyTypeByTrads.put("Produits de: substances naturelles, micro-organismes", StrategyType.PRODUITS_SUBSTANCES_NATURELLES_MICRO_ORGANISMES);
    }

    protected void appendStrategyTypeQueryPart(String keyPart, StringBuilder query, Map<String, Object> args) {
        Set<String> keys = strategyTypeByTrads.keySet();
        List<String> matchingStrategyTypeLibelle = keys.stream().filter(k -> normalizeKey(k).contains(normalizeKey(keyPart))).toList();
        List<StrategyType> matchingStrategyTypes = new ArrayList<>();
        matchingStrategyTypeLibelle.forEach(elem -> matchingStrategyTypes.add(strategyTypeByTrads.get(elem)));
        if (!matchingStrategyTypes.isEmpty()) {
            for (StrategyType matchingStrategyType : matchingStrategyTypes) {
                query.append(DaoUtils.orAttributeEquals("RSL", RefStrategyLever.PROPERTY_STRATEGY_TYPE, args, matchingStrategyType));
            }
        }
    }

    public List<RefStrategyLever> findRefStrategyLevers(
            String growingSystemTopiaId,
            SectionType sectionType,
            StrategyType strategyType){

        StringBuilder query = new StringBuilder("SELECT RSL FROM " + getEntityClass().getName() + " RSL, " + GrowingSystem.class.getName()+ " GS ");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = Maps.newLinkedHashMap();

        query.append(DaoUtils.andAttributeEquals("GS", GrowingSystem.PROPERTY_TOPIA_ID, args, growingSystemTopiaId));

        query.append(" AND RSL." + RefStrategyLever.PROPERTY_SECTOR + " = GS." + GrowingSystem.PROPERTY_SECTOR);

        query.append(DaoUtils.andAttributeEquals("RSL", RefStrategyLever.PROPERTY_SECTION_TYPE, args, sectionType));

        query.append(DaoUtils.andAttributeEquals("RSL", RefStrategyLever.PROPERTY_STRATEGY_TYPE, args, strategyType));

        query.append(DaoUtils.andAttributeEquals("RSL", RefStrategyLever.PROPERTY_ACTIVE, args, true));

        query.append(" ORDER BY RSL." + RefStrategyLever.PROPERTY_LEVER);

        List<RefStrategyLever> result = findAll(query.toString(), args);
        return result;
    }
    
    public List<RefStrategyLever> findRefStrategyLevers(
            Sector sector,
            SectionType sectionType,
            StrategyType strategyType,
            String term
    ){
        Map<String, Object> args = Maps.newLinkedHashMap();

        StringBuilder query = new StringBuilder("SELECT DISTINCT RSL FROM " + getEntityClass().getName() + " RSL ");
        
        query.append(" WHERE 1 = 1");
        
        query.append(DaoUtils.andAttributeEquals("RSL", RefStrategyLever.PROPERTY_ACTIVE, args, true));
        
        query.append(DaoUtils.andAttributeEquals("RSL", RefStrategyLever.PROPERTY_SECTOR, args, sector));
    
        if (sectionType != null) query.append(DaoUtils.andAttributeEquals("RSL", RefStrategyLever.PROPERTY_SECTION_TYPE, args, sectionType));
    
        if (strategyType != null) query.append(DaoUtils.andAttributeEquals("RSL", RefStrategyLever.PROPERTY_STRATEGY_TYPE, args, strategyType));
        
        if (StringUtils.isNotEmpty(term)) {
            if (strategyType == null &&
                    (!Sector.GRANDES_CULTURES.equals(sector) &&
                            !Sector.VITICULTURE.equals(sector)  &&
                            !Sector.ARBORICULTURE.equals(sector)  &&
                            !Sector.POLYCULTURE_ELEVAGE.equals(sector))) {

                query.append("AND ( TRANSLATE(LOWER( RSL." + RefStrategyLever.PROPERTY_LEVER + "),'áàâãäåāăąèééêëēĕėęěìíîïìĩīĭḩóôõöōŏőùúûüũūŭůäàáâãåæçćĉčöòóôõøüùúûßéèêëýñîìíïş','aaaaaaaaaeeeeeeeeeeiiiiiiiihooooooouuuuuuuuaaaaaaeccccoooooouuuuseeeeyniiiis')  like LOWER('%" + StringUtils.stripAccents(term).replace("'", "''") + "%')");
                appendStrategyTypeQueryPart(term, query, args);
                query.append(")");
            } else {
                query.append(DaoUtils.andAttributeLike("RSL", RefStrategyLever.PROPERTY_LEVER, args, term));
            }
        }
        query.append(" ORDER BY RSL." + RefStrategyLever.PROPERTY_LEVER);

        List<RefStrategyLever> result = findAll(query.toString(), args);
        return result;
    }

    protected static String normalizeKey(String value) {
        String strValue = "";
        if (!Strings.isNullOrEmpty(value)) {
            strValue = StringUtils.stripAccents(value);
            strValue = strValue.replaceAll("\\W", "_"); // non word chars
            strValue = strValue.toUpperCase();
        }
        return strValue;
    }
}
