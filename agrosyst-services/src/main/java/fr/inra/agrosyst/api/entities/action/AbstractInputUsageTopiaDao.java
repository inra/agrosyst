package fr.inra.agrosyst.api.entities.action;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class AbstractInputUsageTopiaDao extends AbstractAbstractInputUsageTopiaDao<AbstractInputUsage> {
    private static final Log log = LogFactory.getLog(AbstractInputUsageTopiaDao.class);
    public static final String DOMAIN_INPUT_IDS_PARAM = "domainInputIds";
    
    protected Set<String> queryBody(String subQuery, String propertyDomainPhytoProductInput, Map<String, Object> args) {
        Class<? extends AbstractDomainInputStockUnit> class_ = getFromDomainInputConcretClass(propertyDomainPhytoProductInput);
        String query = "SELECT di." + AbstractDomainInputStockUnit.PROPERTY_TOPIA_ID + ", " + "(" + subQuery + ")" +
                " FROM " + class_.getName() + " di  WHERE 1 = 1 " +
                " AND di." + AbstractDomainInputStockUnit.PROPERTY_TOPIA_ID + " IN (:" + DOMAIN_INPUT_IDS_PARAM + ")";

        if (log.isDebugEnabled()) {
            log.debug("Execute query: " + query);
        }
        Set<String> result = Sets.newHashSet();
        List<Object[]> allTuples = findAll(query, args);
        for (Object[] aTuple : allTuples) {
            String domainInputId = (String) aTuple[0];
            Long count = (Long)aTuple[1];
            if (count != null && count > 0L) {
                result.add(domainInputId);
            }
        }
        
        return result;
        
    }

    @NotNull
    private static Class<? extends AbstractDomainInputStockUnit> getFromDomainInputConcretClass(String propertyDomainPhytoProductInput) {
        Class<? extends AbstractDomainInputStockUnit> class_;
        if (AbstractPhytoProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT.contentEquals(propertyDomainPhytoProductInput)){
            class_ = DomainPhytoProductInput.class;
        } else if (MineralProductInputUsage.PROPERTY_DOMAIN_MINERAL_PRODUCT_INPUT.contentEquals(propertyDomainPhytoProductInput)) {
            class_ = DomainMineralProductInput.class;
        } else if (SubstrateInputUsage.PROPERTY_DOMAIN_SUBSTRATE_INPUT.contentEquals(propertyDomainPhytoProductInput)) {
            class_ = DomainSubstrateInput.class;
        } else if (PotInputUsage.PROPERTY_DOMAIN_POT_INPUT.contentEquals(propertyDomainPhytoProductInput)) {
            class_ = DomainPotInput.class;
        } else if (OrganicProductInputUsage.PROPERTY_DOMAIN_ORGANIC_PRODUCT_INPUT.contentEquals(propertyDomainPhytoProductInput)) {
            class_ = DomainOrganicProductInput.class;
        } else if (OtherProductInputUsage.PROPERTY_DOMAIN_OTHER_INPUT.contentEquals(propertyDomainPhytoProductInput)) {
            class_ = DomainOtherInput.class;
        } else if (SeedLotInputUsage.PROPERTY_DOMAIN_SEED_LOT_INPUT.contentEquals(propertyDomainPhytoProductInput)) {
            class_ = DomainSeedLotInput.class;
        } else if (SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT.contentEquals(propertyDomainPhytoProductInput)) {
            class_ = DomainSeedSpeciesInput.class;
        } else if (IrrigationInputUsage.PROPERTY_DOMAIN_IRRIGATION_INPUT.contentEquals(propertyDomainPhytoProductInput)){
            class_ = DomainIrrigationInput.class;
        } else {
            throw new AgrosystTechnicalException("Unsupported usage class for " + propertyDomainPhytoProductInput);
        }
        return class_;
    }

    public Set<String> getDomainPhytoProductInputUsedForBiologicalProductInputUsage(Set<String> domainInputIds) {
        return getDomainPhytoProductInputUsed(BiologicalProductInputUsage.class, domainInputIds);
    }

    /**
     * Consider phyto as not used if it's quantity Average is not set
     */
    public Set<String> getDomainPhytoProductInputUsedForSeedProductInputUsage(Set<String> domainInputIds) {
        Map<String, Object> args = ImmutableMap.of(DOMAIN_INPUT_IDS_PARAM, domainInputIds);

        String query = "SELECT COUNT(*) FROM " + SeedProductInputUsage.class.getName() + " inputUsage " +
                "        INNER JOIN inputUsage." + AbstractPhytoProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT + " dpi " +
                "        WHERE dpi = di" +
                "        AND inputUsage." + AbstractInputUsage.PROPERTY_QT_AVG + " IS NOT NULL";
        Set<String>result = queryBody(query, AbstractPhytoProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, args);

        return result;
    }
    
    public Set<String> getDomainPhytoProductInputUsedForPesticideProductInputUsage(Set<String> domainInputIds) {
        return getDomainPhytoProductInputUsed(PesticideProductInputUsage.class, domainInputIds);
    }
    
    public Set<String> getDomainPhytoProductInputUsed(Class<? extends AbstractPhytoProductInputUsage> clazz, Set<String> domainInputIds) {
        Map<String, Object> args = ImmutableMap.of(DOMAIN_INPUT_IDS_PARAM, domainInputIds);
        
        String query = "SELECT COUNT(*) FROM " + clazz.getName() + " inputUsage " +
                "        INNER JOIN inputUsage." + AbstractPhytoProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT + " dpi " +
                "        WHERE dpi = di";
        Set<String>result = queryBody(query, AbstractPhytoProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, args);
        
        return result;
    }
    
    public Set<String> getDomainMineralProductInputUsed(Set<String> domainInputIds) {
        Map<String, Object> args = ImmutableMap.of(DOMAIN_INPUT_IDS_PARAM, domainInputIds);
        
        String query = "SELECT COUNT(*) FROM " + MineralProductInputUsage.class.getName() + " inputUsage " +
                "        INNER JOIN inputUsage." + MineralProductInputUsage.PROPERTY_DOMAIN_MINERAL_PRODUCT_INPUT + " dpi " +
                "        WHERE dpi = di";
        Set<String>result = queryBody(query, MineralProductInputUsage.PROPERTY_DOMAIN_MINERAL_PRODUCT_INPUT, args);
        
        return result;
    }
    
    public Set<String> getDomainSubstrateInputUsed(Set<String> domainInputIds) {
        Map<String, Object> args = ImmutableMap.of(DOMAIN_INPUT_IDS_PARAM, domainInputIds);
        
        String query = "SELECT COUNT(*) FROM " + SubstrateInputUsage.class.getName() + " inputUsage " +
                "        INNER JOIN inputUsage." + SubstrateInputUsage.PROPERTY_DOMAIN_SUBSTRATE_INPUT + " dpi " +
                "        WHERE dpi = di";
        Set<String>result = queryBody(query, SubstrateInputUsage.PROPERTY_DOMAIN_SUBSTRATE_INPUT, args);
        
        return result;
    }
    
    public Set<String> getDomainPotInputUsed(Set<String> domainInputIds) {
        Map<String, Object> args = ImmutableMap.of(DOMAIN_INPUT_IDS_PARAM, domainInputIds);
        
        String query = "SELECT COUNT(*) FROM " + PotInputUsage.class.getName() + " inputUsage " +
                "        INNER JOIN inputUsage." + PotInputUsage.PROPERTY_DOMAIN_POT_INPUT + " dpi " +
                "        WHERE dpi = di";
        Set<String>result = queryBody(query, PotInputUsage.PROPERTY_DOMAIN_POT_INPUT, args);
        
        return result;
    }
    
    public Set<String> getDomainOtherInputUsed(Set<String> domainInputIds) {
        Map<String, Object> args = ImmutableMap.of(DOMAIN_INPUT_IDS_PARAM, domainInputIds);
        
        String query = "SELECT COUNT(*) FROM " + OtherProductInputUsage.class.getName() + " inputUsage " +
                "        INNER JOIN inputUsage." + OtherProductInputUsage.PROPERTY_DOMAIN_OTHER_INPUT + " dpi " +
                "        WHERE dpi = di";
        Set<String>result = queryBody(query, OtherProductInputUsage.PROPERTY_DOMAIN_OTHER_INPUT, args);
        
        return result;
    }
    
    public Set<String> getOrganicProductInputUsed(Set<String> domainInputIds) {
        Map<String, Object> args = ImmutableMap.of(DOMAIN_INPUT_IDS_PARAM, domainInputIds);
    
        String query = "SELECT COUNT(*) FROM " + OrganicProductInputUsage.class.getName() + " inputUsage " +
                "        INNER JOIN inputUsage." + OrganicProductInputUsage.PROPERTY_DOMAIN_ORGANIC_PRODUCT_INPUT + " dpi " +
                "        WHERE dpi = di";
        Set<String>result = queryBody(query, OrganicProductInputUsage.PROPERTY_DOMAIN_ORGANIC_PRODUCT_INPUT, args);
        return result;
    }
    
    public Set<String> getDomainSeedLotInputUsed(Set<String> domainInputIds) {
        Map<String, Object> args = ImmutableMap.of(DOMAIN_INPUT_IDS_PARAM, domainInputIds);
    
        String query = "SELECT COUNT(*) FROM " + SeedLotInputUsage.class.getName() + " inputUsage " +
                "        INNER JOIN inputUsage." + SeedLotInputUsage.PROPERTY_DOMAIN_SEED_LOT_INPUT + " dpi " +
                "        WHERE dpi = di";
        Set<String>result = queryBody(query, SeedLotInputUsage.PROPERTY_DOMAIN_SEED_LOT_INPUT, args);
        return result;
    }
    
    public Set<String> getDomainSeedSpeciesInputUsed(Set<String> domainInputIds) {
        Map<String, Object> args = ImmutableMap.of(DOMAIN_INPUT_IDS_PARAM, domainInputIds);
    
        String query = "SELECT COUNT(*) FROM " + SeedSpeciesInputUsage.class.getName() + " inputUsage " +
                "        INNER JOIN inputUsage." + SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT + " dpi " +
                "        WHERE dpi = di";
        Set<String>result = queryBody(query, SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, args);
        return result;
    }
    
    public Set<String> getDomainIrrigationInputUsed(Set<String> domainInputIds) {
        Map<String, Object> args = ImmutableMap.of(DOMAIN_INPUT_IDS_PARAM, domainInputIds);
        
        String query = "SELECT COUNT(*) FROM " + IrrigationInputUsage.class.getName() + " inputUsage " +
                "        INNER JOIN inputUsage." + IrrigationInputUsage.PROPERTY_DOMAIN_IRRIGATION_INPUT + " dpi " +
                "        WHERE dpi = di";
        Set<String>result = queryBody(query, IrrigationInputUsage.PROPERTY_DOMAIN_IRRIGATION_INPUT, args);
        return result;
    }
}
