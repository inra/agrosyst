package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HarvestingPriceTopiaDao extends AbstractHarvestingPriceTopiaDao<HarvestingPrice> {
    
    public List<HarvestingPrice> findPrices(String domainTopiaId, String practicedSystemId) {
        StringBuilder query = new StringBuilder("FROM " + HarvestingPrice.class.getName() + " p ");
        query.append(" WHERE 1 = 1");

        Map<String, Object> args = new HashMap<>();

        if (!Strings.isNullOrEmpty(domainTopiaId)) {

            query.append(DaoUtils.andAttributeEquals("p", HarvestingPrice.PROPERTY_DOMAIN +
                    "." + Domain.PROPERTY_TOPIA_ID, args, domainTopiaId));


        } else if (!Strings.isNullOrEmpty(practicedSystemId)) {
            query.append(DaoUtils.andAttributeEquals("p", HarvestingPrice.PROPERTY_PRACTICED_SYSTEM +
                    "." + PracticedSystem.PROPERTY_TOPIA_ID, args, practicedSystemId));
        }

        return findAll(query.toString(), args);
    }

    public MultiValuedMap<String, HarvestingPrice> loadUserPriceForCodeEspeceBotaniquesAndGivenCampaigns(Set<String> allCodeEspeceBotaniques, Domain domain, Integer campaign) {
        Map<String, Object> args = Maps.newLinkedHashMap();

        String query =
                "SELECT DISTINCT re." + RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE + ", hp " +
                        " FROM " + HarvestingPrice.class.getName() + " hp, " +  CroppingPlanSpecies.class.getName() + " cps " +
                        " INNER JOIN hp." + HarvestingPrice.PROPERTY_DOMAIN + " pdom " +
                        " INNER JOIN hp." + HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION + " hav " +
                        " INNER JOIN cps." + CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY + " cpe " +
                        " INNER JOIN cpe." + CroppingPlanEntry.PROPERTY_DOMAIN + " d " +
                        " INNER JOIN cps." + CroppingPlanSpecies.PROPERTY_SPECIES + " re " +
                        " WHERE d." + Domain.PROPERTY_CAMPAIGN + " = :campaign" +
                        " AND pdom = d " +
                        " AND pdom != :domain0 " +
                        " AND d." + Domain.PROPERTY_ACTIVE + " = TRUE " +
                        " AND hp." + HarvestingPrice.PROPERTY_PRICE + " IS NOT NULL " +
                        " AND cps." + CroppingPlanSpecies.PROPERTY_CODE + " = hav." + HarvestingActionValorisation.PROPERTY_SPECIES_CODE +
                        " AND re." + RefEspece.PROPERTY_ACTIVE + " = TRUE " +
                        " AND re." + RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE + " IN (:codeEspeceBotaniques)"
                ;
        args.put("campaign", campaign);
        args.put("domain0", domain);
        args.put("codeEspeceBotaniques", allCodeEspeceBotaniques);

        List<Object[]> result0 = findAll(query, args);
        MultiValuedMap<String, HarvestingPrice> result = new HashSetValuedHashMap<>();
        for (Object[] r : result0) {
            String codeEspeceBotanique = Strings.emptyToNull((String) r[0]);
            HarvestingPrice harvestingPrice = (HarvestingPrice) r[1];
            result.put(codeEspeceBotanique, harvestingPrice);
        }
        return result;
    }

} //PriceTopiaDao
