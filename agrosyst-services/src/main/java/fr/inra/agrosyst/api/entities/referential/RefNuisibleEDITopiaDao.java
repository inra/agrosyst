package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class RefNuisibleEDITopiaDao extends AbstractRefNuisibleEDITopiaDao<RefNuisibleEDI> {

    /**
     * Recherche des types de nuisibles (colonne reference_param) du référentiel
     * NuisiblesEDI, triés par reference_param.
     * 
     * @return types de nuisibles
     */
    public List<BioAgressorType> findAllActiveParam() {
        String query = "SELECT distinct " + RefNuisibleEDI.PROPERTY_REFERENCE_PARAM +
                " FROM " + getEntityClass().getName() +
                " WHERE active IS true" +
                " ORDER BY " + RefNuisibleEDI.PROPERTY_REFERENCE_PARAM;

        return findAll(query, DaoUtils.asArgsMap());
    }

    public List<RefNuisibleEDI> findActiveRefNuisibleEDIForBioAgressorType(Language language,
                                                                           Collection<BioAgressorType> bioAgressorTypes,
                                                                           Collection<Sector> sectors) {

        I18nDaoHelper referenceLabelI18nDaoHelper =
                I18nDaoHelper.withSimpleI18nKey(
                        RefNuisibleEDI.PROPERTY_REFERENCE_LABEL,
                        language,
                        TradRefVivant.class,
                        "rn"
                );

        StringBuilder query = new StringBuilder()
                .append(" SELECT rn, " + referenceLabelI18nDaoHelper.coalesceTranslation() + " as libelle")
                .append(" FROM " + getEntityClass().getName() + " rn")
                .append(" LEFT JOIN FETCH rn." + RefNuisibleEDI.PROPERTY_SECTORS)
                .append(referenceLabelI18nDaoHelper.leftJoinTranslation())
                .append(" WHERE rn." + RefNuisibleEDI.PROPERTY_ACTIVE + " = true");
        Map<String, Object> args = new HashMap<>();

        if (CollectionUtils.isNotEmpty(bioAgressorTypes)) {
            if (bioAgressorTypes.size() == 1) {
                query.append(DaoUtils.andAttributeEquals("rn", RefNuisibleEDI.PROPERTY_REFERENCE_PARAM, args, bioAgressorTypes.iterator().next()));
            } else {
                query.append(" AND ( 1 = 0");
                for (BioAgressorType bioAgressorType : bioAgressorTypes) {
                    query.append(DaoUtils.orAttributeEquals("rn", RefNuisibleEDI.PROPERTY_REFERENCE_PARAM, args, bioAgressorType));
                }
                query.append(" )");
            }
        }

        if (CollectionUtils.isNotEmpty(sectors)) {
            query.append(" AND ( 1 = 0");
            for (Sector sector : sectors) {
                query.append(DaoUtils.orAttributeInElements("rn", RefNuisibleEDI.PROPERTY_SECTORS, args, sector));
            }
            query.append(" )");
        }

        query.append(" ORDER BY rn." + RefNuisibleEDI.PROPERTY_MAIN + " DESC, libelle ASC");

        List<RefNuisibleEDI> result = new LinkedList<>();
        try (Stream<Object[]> stream = stream(query.toString(), args)) {
            stream.forEach(row -> {
                RefNuisibleEDI refNuisibleEDI = (RefNuisibleEDI) row[0];
                String libelle = (String) row[1];
                refNuisibleEDI.setReference_label(libelle);
                result.add(refNuisibleEDI);
            });
        }

        return result;
    }
}
