package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class RefInterventionAgrosystTravailEDITopiaDao extends AbstractRefInterventionAgrosystTravailEDITopiaDao<RefInterventionAgrosystTravailEDI> {

    public List<RefInterventionAgrosystTravailEDI> findAllActive(Language language, AgrosystInterventionType actionType) {
        I18nDaoHelper labeli18n = I18nDaoHelper.withSimpleI18nKey(RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_LABEL, language, TradRefIntervention.class, "a");

        Map<String,Object> args = DaoUtils.asArgsMap("active", true);

        String query = "SELECT a, " + labeli18n.getAlias() +
                " FROM " + getEntityClass().getName() + " a " +
                labeli18n.leftJoinTranslation() +
                " WHERE a." + RefInterventionAgrosystTravailEDI.PROPERTY_ACTIVE + " = :active";

        query += DaoUtils.andAttributeEquals("a", RefInterventionAgrosystTravailEDI.PROPERTY_INTERVENTION_AGROSYST, args, actionType);

        String hql = query +
                " ORDER BY " + labeli18n.coalesceTranslation() + " ASC ";

        List<Object[]> resultWithTranslation = findAll(hql, args);
        List<RefInterventionAgrosystTravailEDI> result = resultWithTranslation.stream()
                .map(objects -> {
                    RefInterventionAgrosystTravailEDI refInterventionTravail = (RefInterventionAgrosystTravailEDI) objects[0];
                    refInterventionTravail.setReference_label_Translated(I18nDaoHelper.tryGetTranslation(objects[1]).orElse(refInterventionTravail.getReference_label()));
                    return refInterventionTravail;
                })
                .toList();
        return result;
    }

    public ReferentialTranslationMap fillTranslations(Collection<String> topiaIdList, ReferentialTranslationMap translationMap) {
        List<I18nDaoHelper> i18nDaoHelpers = Lists.newArrayList(I18nDaoHelper.withSimpleI18nKey(RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_LABEL, Language.fromTrigram(translationMap.getLanguage().getTrigram()), TradRefIntervention.class, "a"));

        I18nDaoHelper.fillRefEntitiesTranslations(this, "a", i18nDaoHelpers, topiaIdList, translationMap);

        return translationMap;
    }

} //RefInterventionAgrosystTravailEDITopiaDao
