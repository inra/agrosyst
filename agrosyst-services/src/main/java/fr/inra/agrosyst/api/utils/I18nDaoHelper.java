package fr.inra.agrosyst.api.utils;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AbstractAgrosystTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntityTranslation;
import fr.inra.agrosyst.api.entities.referential.ReferentialI18nEntry;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import org.nuiton.topia.persistence.TopiaEntity;

import java.text.Normalizer;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

public class I18nDaoHelper {

    public enum I18NKeyGenerationType {
        TO_SIMPLE_I18N_KEY("to_simple_i18n_key"),
        TO_COMPLEX_I18N_KEY("to_complex_i18n_key");

        I18NKeyGenerationType(String methodName) {
            this.methodName = methodName;
        }

        private final String methodName;

        public String getMethodName() {
            return methodName;
        }
    }

    protected final String propertyToTranslate;
    protected final Language lang;
    protected final Class<? extends ReferentialI18nEntry> i18nEntryClass;
    protected final String aliasAndProperty;
    protected final String i18nEntryAlias;
    protected final I18NKeyGenerationType keyGenerationType;

    public static String to_simple_i18n_key(String value) {
        String normalized = Normalizer.normalize(Strings.nullToEmpty(value), Normalizer.Form.NFD);
        String replaced = normalized
                .replaceAll("[\\p{InCombiningDiacriticalMarks}]", "")
                .replaceAll("[^A-Za-z0-9]", "_")
                .replaceAll("_+", "_")
                .replaceAll("_$", "");
        String result = replaced.toUpperCase();
        return result;
    }

    public static String to_complex_i18n_key(String value) {
        String sanitizedValue = Strings.nullToEmpty(value).replaceAll("µ", "MICRO")
                .replaceAll(">", "GT")
                .replaceAll("<", "LT")
                .replaceAll("=", "EQ")
                .replaceAll("%", "PCT")
                .replaceAll("°", "DEG")
                .replaceAll("²", "2");
        String result = to_simple_i18n_key(sanitizedValue);
        return result;
    }

    public static I18nDaoHelper withSimpleI18nKey(
            String propertyToTranslate,
            Language lang,
            Class<? extends ReferentialI18nEntry> i18nEntryClass,
            String entityAlias) {
        return new I18nDaoHelper(propertyToTranslate, lang, i18nEntryClass, entityAlias, I18NKeyGenerationType.TO_SIMPLE_I18N_KEY);
    }

    public static I18nDaoHelper withComplexI18nKey(
            String propertyToTranslate,
            Language lang,
            Class<? extends ReferentialI18nEntry> i18nEntryClass,
            String entityAlias) {
        return new I18nDaoHelper(propertyToTranslate, lang, i18nEntryClass, entityAlias, I18NKeyGenerationType.TO_COMPLEX_I18N_KEY);
    }

    public I18nDaoHelper(
            String propertyToTranslate,
            Language lang,
            Class<? extends ReferentialI18nEntry> i18nEntryClass,
            String entityAlias,
            I18NKeyGenerationType keyGenerationType) {
        this.propertyToTranslate = propertyToTranslate;
        this.lang = lang;
        this.i18nEntryClass = i18nEntryClass;
        this.aliasAndProperty = entityAlias + "." + propertyToTranslate;
        this.i18nEntryAlias = "trans_" + propertyToTranslate;
        this.keyGenerationType = keyGenerationType;
    }

    public String getPropertyToTranslate() {
        return this.propertyToTranslate;
    }

    public String traductionProperty() {
        return String.format("%s.traduction", i18nEntryAlias);
    }

    public String getAlias() {
        return this.i18nEntryAlias + " ";
    }

    public String leftJoinTranslation() {
        String result = String.format(
                " LEFT JOIN %s %s ON %s(%s) = %s.%s AND %s.%s = '%s' ",
                i18nEntryClass.getName(),
                i18nEntryAlias,
                keyGenerationType.getMethodName(),
                aliasAndProperty,
                i18nEntryAlias,
                ReferentialI18nEntry.PROPERTY_TRADKEY,
                i18nEntryAlias,
                ReferentialI18nEntry.PROPERTY_LANG,
                lang.getTrigram());
        return result;
    }

    public String coalesceTranslation() {
        String coalesceCall = String.format(
                "COALESCE(%s.%s, %s)",
                i18nEntryAlias,
                ReferentialI18nEntry.PROPERTY_TRADUCTION,
                aliasAndProperty);
        return coalesceCall;
    }

    public static Optional<String> tryGetTranslation(Object object) {
        if (object instanceof ReferentialI18nEntry entry) {
            return Optional.ofNullable(entry.getTraduction());
        } else if (object instanceof String t) {
            return Optional.of(t);
        } else if (object == null) {
            return Optional.empty();
        } else {
            throw new IllegalArgumentException("ne sait pas comment déterminer une traduction à partir de " + object);
        }
    }

    public static String buildQuerySelectTranslationProperties(List<I18nDaoHelper> helpers) {
        String result = helpers.stream()
                .map(I18nDaoHelper::traductionProperty)
                .collect(Collectors.joining(","));
        return result;
    }

    public static String buildQueryJoinTranslations(List<I18nDaoHelper> helpers) {
        String result = helpers.stream()
                .map(I18nDaoHelper::leftJoinTranslation)
                .collect(Collectors.joining(" "));
        return result;
    }

    public static String buildQueryTradObjectAlias(List<I18nDaoHelper> helpers) {
        String result = helpers.stream()
                .map(I18nDaoHelper::getAlias)
                .collect(Collectors.joining(", "));
        return result + " ";
    }

    public static void positionalForEachHelper(List<I18nDaoHelper> helpers, BiConsumer<I18nDaoHelper, Integer> consumer) {
        for (int i = 0; i < helpers.size(); i++) {
            consumer.accept(helpers.get(i), i);
        }
    }

    public static <T extends TopiaEntity> ReferentialTranslationMap fillRefEntitiesTranslations(
            AbstractAgrosystTopiaDao<T> dao,
            String entityAlias,
            List<I18nDaoHelper> helpers,
            Collection<String> topiaIdList,
            ReferentialTranslationMap translationMap) {

        Map<String, Object> args = new HashMap<>();
        String aliasWithTopiaId = entityAlias + "." + TopiaEntity.PROPERTY_TOPIA_ID;
        String query = String.format(
                        " SELECT %s, %s FROM %s %s %s " +
                        " WHERE %s IN (:topiaIdList) ",
                aliasWithTopiaId,
                buildQuerySelectTranslationProperties(helpers),
                dao.getEntityClass().getName(),
                entityAlias,
                buildQueryJoinTranslations(helpers),
                aliasWithTopiaId);
        args.put("topiaIdList", topiaIdList);

        List<Object[]> resultWithTranslation = dao.getTopiaJpaSupport().findAll(query, args);
        resultWithTranslation.forEach(r -> {
            String topiaId = (String) r[0];
            ReferentialEntityTranslation entityTranslation = new ReferentialEntityTranslation(topiaId);
            positionalForEachHelper(helpers, (helper, i) ->
                entityTranslation.setPropertyTranslationFromObject(helper.getPropertyToTranslate(), r[i+1])
            );
            translationMap.setEntityTranslation(topiaId, entityTranslation);
        });

        return translationMap;
    }
}
