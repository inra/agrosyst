package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import fr.inra.agrosyst.services.common.AgrosystI18nService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Custom doa methods related to {@link RefSolArvalis}.
 * 
 * @author Eric Chatellier
 */
public class RefSolArvalisTopiaDao extends AbstractRefSolArvalisTopiaDao<RefSolArvalis> {

    /**
     * Return all sol_region attributes value ordered alphabetically.
     * 
     * @return all sol_region
     */
    public Map<Integer, String> getAllSolArvalisRegions() {
        String query = "SELECT distinct " + RefSolArvalis.PROPERTY_SOL_REGION_CODE + "," + RefSolArvalis.PROPERTY_SOL_REGION + 
                " FROM " + getEntityClass().getName() +
                " WHERE " + RefSolArvalis.PROPERTY_ACTIVE + " = :active " +
                " ORDER BY " + RefSolArvalis.PROPERTY_SOL_REGION;

        Map<String, Object> args = DaoUtils.asArgsMap("active", true);
        List<Object[]> list = findAll(query, args);

        Map<Integer, String> result = Maps.newLinkedHashMap();
        for (Object[] entry : list) {
            result.put((Integer)entry[0], (String)entry[1]);
        }
        return result;
    }

    /**
     * Look for RefSolArvalis in a specific region.
     * 
     * @param regionCode region codes
     * @return RefSolArvalis for requested region
     */
    public List<RefSolArvalis> findAllForRegion(Language language, int regionCode) {
        List<I18nDaoHelper> i18nDaoHelpers = AgrosystI18nService.getI18nRefSolArvalisDaoHelpers(language, "sa");
        I18nDaoHelper nomSolI18n = i18nDaoHelpers.getFirst();

        Map<String, Object> args = DaoUtils.asArgsMap("argCode", regionCode, "argActive", true);
        String hql = String.format("SELECT sa, %s FROM %s sa %s" +
                " WHERE sa.%s = :argCode AND sa.%s = :argActive " +
                " ORDER BY %s ASC ",
                I18nDaoHelper.buildQuerySelectTranslationProperties(i18nDaoHelpers),
                getEntityClass().getName(),
                I18nDaoHelper.buildQueryJoinTranslations(i18nDaoHelpers),
                RefSolArvalis.PROPERTY_SOL_REGION_CODE,
                RefSolArvalis.PROPERTY_ACTIVE,
                nomSolI18n.coalesceTranslation());

        List<Object[]> resultWithTranslation = findAll(hql, args);
        List<RefSolArvalis> result = new ArrayList<>(resultWithTranslation.size());
        for (Object[] objects : resultWithTranslation) {
            RefSolArvalis sol = (RefSolArvalis)objects[0];
            sol.setSol_nom_Translated(I18nDaoHelper.tryGetTranslation(objects[1]).orElse(sol.getSol_nom()));
            sol.setSol_texture_Translated(I18nDaoHelper.tryGetTranslation(objects[2]).orElse(sol.getSol_texture()));
            sol.setSol_calcaire_Translated(I18nDaoHelper.tryGetTranslation(objects[3]).orElse(sol.getSol_calcaire()));
            sol.setSol_profondeur_Translated(I18nDaoHelper.tryGetTranslation(objects[4]).orElse(sol.getSol_profondeur()));
            sol.setSol_hydromorphie_Translated(I18nDaoHelper.tryGetTranslation(objects[5]).orElse(sol.getSol_hydromorphie()));
            sol.setSol_pierrosite_Translated(I18nDaoHelper.tryGetTranslation(objects[6]).orElse(sol.getSol_pierrosite()));
            result.add(sol);//sol_nom_Translated
        }
        return result;
    }
} //RefSolArvalisTopiaDao<E extends RefSolArvalis>
