package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.utils.DaoUtils;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class RefMAADosesRefParGroupeCibleTopiaDao extends AbstractRefMAADosesRefParGroupeCibleTopiaDao<RefMAADosesRefParGroupeCible> {

    public List<RefMAADosesRefParGroupeCible> findByCodeAmmCodeCulturesAndTreatmentCode(String ammCode, Set<String> codeCultureMaas, String maaTreatmentCode) {

        String query = "SELECT dose FROM " + getEntityClass().getName() + " dose " +
                " WHERE dose." + RefMAADosesRefParGroupeCible.PROPERTY_CODE_AMM + " = :ammCode " +
                " AND dose." + RefMAADosesRefParGroupeCible.PROPERTY_CODE_CULTURE_MAA + " IN :codeCultureMaas" +
                " AND dose." + RefMAADosesRefParGroupeCible.PROPERTY_CODE_TRAITEMENT_MAA + " = :maaTreatmentCode " +
                " AND dose." + RefMAADosesRefParGroupeCible.PROPERTY_ACTIVE + " IS true";

        Map<String, Object> args = ImmutableMap.of("ammCode", ammCode, "codeCultureMaas", codeCultureMaas, "maaTreatmentCode", maaTreatmentCode);
        return findAll(query, args);
    }

    public Optional<LocalDate> findLastDateCorrectionMaa(int campaign) {
        String query = "SELECT max(dose." + RefMAADosesRefParGroupeCible.PROPERTY_DATE_CORRECTION_MAA + ") " +
                "FROM " + getEntityClass().getName() + " dose " +
                "WHERE dose." + RefMAADosesRefParGroupeCible.PROPERTY_CAMPAGNE + " = :campaign " +
                "AND dose." + RefMAADosesRefParGroupeCible.PROPERTY_ACTIVE + " = true";

        Map<String, Object> args = ImmutableMap.of("campaign", campaign);
        LocalDate lastDateCorrectionMaa = findAnyOrNull(query, args);
        return Optional.ofNullable(lastDateCorrectionMaa);
    }

    /**
     * La table contient plus d'1 million de lignes, donc on ne charge que les éléments dont on a besoin plutôt que de
     * charger toutes les entités
     *
     * @return une Map dont la clé est une concaténation de {@link RefMAADosesRefParGroupeCible#PROPERTY_CODE_AMM} et
     *         {@link RefMAADosesRefParGroupeCible#PROPERTY_CODE_TRAITEMENT_MAA} et la valeur est
     *         {@link RefMAADosesRefParGroupeCible#PROPERTY_GROUPE_CIBLE_MAA}
     */
    public Map<String, String> findAllActiveGroupesCiblesMaaByCodeAmmAndCodeTraitementMaa() {

        String query = String.format(
                " SELECT d.%s, d.%s, d.%s " +
                " FROM %s  d " +
                " WHERE d.%s = true ",
                RefMAADosesRefParGroupeCible.PROPERTY_CODE_AMM,
                RefMAADosesRefParGroupeCible.PROPERTY_CODE_TRAITEMENT_MAA,
                RefMAADosesRefParGroupeCible.PROPERTY_GROUPE_CIBLE_MAA,
                getEntityClass().getName(),
                RefMAADosesRefParGroupeCible.PROPERTY_ACTIVE);

        List<Object[]> rows = findAll(query);
        Map<String, String> groupesCiblesMaaByCodeAmmAndCodeTraitementMaa = new HashMap<>();
        rows.forEach(row -> {
            String codeAmm = (String) row[0];
            String codeTraitementMaa = (String) row[1];
            String groupeCibleMaa = (String)row[2];
            groupesCiblesMaaByCodeAmmAndCodeTraitementMaa.put(codeAmm + codeTraitementMaa, groupeCibleMaa);
        });
        return groupesCiblesMaaByCodeAmmAndCodeTraitementMaa;
    }

    public List<PhytoProductUnit> findPhytoProductUnitsForProduct(RefActaTraitementsProduit refActaTraitementsProduit) {
        Map<String, Object> args = new HashMap<>();
        String hql = "SELECT DISTINCT(" + RefMAADosesRefParGroupeCible.PROPERTY_UNIT_DOSE_REF_MAA + ") FROM " + getEntityClass().getName() + " d " +
                " WHERE 1 = 1 ";

        hql += DaoUtils.andAttributeEquals("d", RefMAADosesRefParGroupeCible.PROPERTY_CODE_AMM, args, refActaTraitementsProduit.getCode_AMM());
        hql += " ORDER BY d." + RefMAADosesRefParGroupeCible.PROPERTY_UNIT_DOSE_REF_MAA;

        List<PhytoProductUnit> results = findAll(hql, args);

        return results.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

}
