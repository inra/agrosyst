package fr.inra.agrosyst.api.entities.migration;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CompagneType;
import fr.inra.agrosyst.api.entities.CroppingEntryType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryAbstract;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesImpl;
import fr.inra.agrosyst.api.entities.DomainFuelInputImpl;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.DoseType;
import fr.inra.agrosyst.api.entities.EstimatingIftRules;
import fr.inra.agrosyst.api.entities.FuelUnit;
import fr.inra.agrosyst.api.entities.IftSeedsType;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputPriceImpl;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.IrrigationUnit;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.OtherProductInputUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PotInputUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.SeedPriceImpl;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.UnitType;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.FertiOrgaUnit;
import fr.inra.agrosyst.api.entities.action.IrrigationInputUsage;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PhytoProductTarget;
import fr.inra.agrosyst.api.entities.action.PotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsageImpl;
import fr.inra.agrosyst.api.entities.action.SubstrateInputUsage;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFAImpl;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrgaImpl;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefPotImpl;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefSubstrateImpl;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import freemarker.template.utility.StringUtil;
import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

public class V3_0_0_9__11484_input_storage extends BaseJavaMigration {

    /**
     REVERT SCRIPT
     DELETE FROM phytoproducttarget WHERE abstractphytoproductinputusage IS NOT null;
     DELETE FROM pesticideproductinputusage WHERE topiaid IS NOT null;
     DELETE FROM biologicalproductinputusage WHERE topiaid IS NOT null;
     DELETE FROM seedproductinputusage WHERE topiaid IS NOT null;
     DELETE FROM abstractphytoproductinputusage WHERE topiaid IS NOT null;
     DELETE FROM domainphytoproductinput WHERE topiaid IS NOT null;
     DELETE FROM mineralproductinputusage WHERE topiaid IS NOT null;
     UPDATE abstractaction SET irrigationinputusage = null;
     DELETE FROM irrigationinputusage WHERE topiaid IS NOT null;
     DELETE FROM seedspeciesinputusage WHERE topiaid IS NOT null;
     DELETE FROM domainseedspeciesinput WHERE topiaid IS NOT null;
     DELETE FROM organicproductinputusage WHERE topiaid IS NOT null;
     DELETE FROM potinputusage WHERE topiaid IS NOT null;
     DELETE FROM seedlotinputusage WHERE topiaid IS NOT null;
     DELETE FROM substrateinputusage WHERE topiaid IS NOT null;
     DELETE FROM domainseedlotinput WHERE topiaid IS NOT null;
     DELETE FROM potinputusage WHERE topiaid IS NOT null;
     DELETE FROM abstractinputusage WHERE topiaid IS NOT null;
     DELETE FROM domainorganicproductinput WHERE topiaid IS NOT null;
     DELETE FROM domainmineralproductinput WHERE topiaid IS NOT null;
     DELETE FROM domainirrigationinput WHERE topiaid IS NOT null;
     DELETE FROM domainotherinput WHERE topiaid IS NOT null;
     DELETE FROM domainpotinput WHERE topiaid IS NOT null;
     DELETE FROM substrateinputusage WHERE topiaid IS NOT null;
     DELETE FROM domainsubstrateinput WHERE topiaid IS NOT null;
     DELETE FROM domainfuelinput WHERE topiaid IS NOT null;
     DELETE FROM abstractdomaininputstockunit WHERE topiaid IS NOT null;
     ALTER TABLE domainotherinput drop column comment;
     DELETE FROM schema_version WHERE script = 'fr.inra.agrosyst.api.entities.migration.V3_0_0_9__11484_input_storage';
     DROP TABLE tmp_migrate_to_domain_input_stock_table;
     DROP TABLE tmp_validpracticedseedingactionspecies;
     DROP TABLE tmp_missing_practiced_crop_and_species;
     DROP TABLE tmp_new_croppingplanentry;
     DROP TABLE new_croppingplanspecies;
     DROP TABLE tmp_seedingaction;
     UPDATE abstractaction SET topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.SeedingActionUsageImpl' WHERE topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.SeedingActionImpl';
     ALTER TABLE abstractinput ADD CONSTRAINT fk_5q9sjdjohh4x44ri8co9gt1vh FOREIGN KEY (mineralfertilizersspreadingaction) REFERENCES abstractaction(topiaid)");
     ALTER TABLE abstractinput ADD CONSTRAINT fk_7o49kfk9gj8j5w8ngdcohfc30 FOREIGN KEY (maintenancepruningvinesaction) REFERENCES abstractaction(topiaid)");
     ALTER TABLE abstractinput ADD CONSTRAINT fk_8hrwd40n863dsqij0otnd1wbv FOREIGN KEY (pesticidesspreadingaction) REFERENCES abstractaction(topiaid)");
     ALTER TABLE abstractinput ADD CONSTRAINT fk_9bx0734vwkb2pffutk93e3eoc FOREIGN KEY (mineralproduct) REFERENCES abstractaction(topiaid)");
     ALTER TABLE abstractinput ADD CONSTRAINT fk_b9hmxoperi24errro6e8co4bw FOREIGN KEY (biologicalcontrolaction) REFERENCES abstractaction(topiaid)");
     ALTER TABLE abstractinput ADD CONSTRAINT fk_dq90e4dhy88qk1c4rgebgrlgf FOREIGN KEY (phytoproduct) REFERENCES abstractaction(topiaid)");
     ALTER TABLE abstractinput ADD CONSTRAINT fk_h0sslpvjvpx19k9by0hvksnk8 FOREIGN KEY (otheraction) REFERENCES abstractaction(topiaid)");
     ALTER TABLE abstractinput ADD CONSTRAINT fk_ikuu59fr9lvg3itocgn36rwhj FOREIGN KEY (harvestingaction) REFERENCES abstractaction(topiaid)");
     ALTER TABLE abstractinput ADD CONSTRAINT fk_j18hh0y2nyxq6v7ssn99itvs3 FOREIGN KEY (organicfertilizersspreadingaction) REFERENCES abstractaction(topiaid)");
     ALTER TABLE abstractinput ADD CONSTRAINT fk_ljyxhp2ilx9raveem4ogy17le FOREIGN KEY (seedingaction) REFERENCES abstractaction(topiaid)");
     ALTER TABLE abstractinput ADD CONSTRAINT fk_nxs91yj4ld8uceumlksu9eb55 FOREIGN KEY (organicproduct) REFERENCES abstractaction(topiaid)");
     ALTER TABLE abstractinput ADD CONSTRAINT fk_shjuknig3tfx2gudw41uxeqcj FOREIGN KEY (irrigationaction) REFERENCES abstractaction(topiaid)");
     ALTER TABLE abstractinput ADD CONSTRAINT fkyxesef16ubmgsbmdh3ebecf8 FOREIGN KEY (substrate) REFERENCES refsubstrate(topiaid);
     ALTER TABLE abstractinput ADD CONSTRAINT fkyxesef16ubmgsbmdh3ebecf9 FOREIGN KEY (pot) REFERENCES refpot(topiaid);
     ALTER TABLE seedingactionspecies ADD CONSTRAINT fk_jvpevjwtaqhnfsva02bututmc FOREIGN KEY (seedingaction) REFERENCES abstractaction(topiaid);
     ALTER TABLE phytoproducttarget ADD CONSTRAINT fk_7o49kfk9gj8j5w8ngdcohfc31 FOREIGN KEY (phytoproductinput) REFERENCES abstractinput(topiaid);
     */

    public static final String FIND_SEEDING_CROP_PRICES_FROM_ID_QUERY = """
            SELECT topiaid, objectid, priceunit, domain, practicedsystem, type, topiacreatedate, category,
                   displayname, price, sourceunit, sourceunitlabel, harvestingactionvalorisation, topiadiscriminator,
                   includedtreatment, chemicaltreatment, biologicaltreatment, producttype, seedtype
            FROM price
            WHERE category = 'SEEDING_ACTION_CATEGORIE'
            AND price > 0
             """;

    public static final String PRACTICED_SYSTEM_ID_TO_DOMAIN_CODE_DOMAIN_ID_QUERY = """
           SELECT ps.topiaid, d.code, d.topiaid
           FROM price p
           INNER JOIN practicedsystem ps ON p.practicedsystem = ps.topiaid
           INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
           INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
           INNER JOIN domain d ON d.topiaid = gp.domain
           WHERE category = 'SEEDING_ACTION_CATEGORIE' AND price > 0
             """;

    public static final String FIND_PRICES_QUERY = """
            SELECT topiaid, objectid, priceunit, domain, practicedsystem, type, topiacreatedate, category,
                   displayname, price, sourceunit, sourceunitlabel, harvestingactionvalorisation, topiadiscriminator,
                   includedtreatment, chemicaltreatment, biologicaltreatment, producttype, seedtype
            FROM price
            WHERE objectid IN (%s)
            %s
            AND category = ?
            AND price > 0
            ORDER BY price DESC, topiacreatedate DESC
             """;

    public static final String FIND_EFFECTIVE_NO_OBJECT_IDS_PRICES_QUERY = """
            SELECT p.topiaid, p.objectid, p.priceunit, d.topiaid AS domain, d.code AS domaincode, d.campaign AS domaincampaign, p.practicedsystem, p.type, p.topiacreatedate, p.category,
                   p.displayname, p.price, p.sourceunit, p.sourceunitlabel, p.harvestingactionvalorisation, p.topiadiscriminator,
                   p.includedtreatment, p.chemicaltreatment, p.biologicaltreatment, p.producttype, p.seedtype
            FROM price p
            INNER JOIN domain d ON p.domain = d.topiaid
            WHERE category = %s
            AND price > 0
            ORDER BY price DESC, topiacreatedate DESC
             """;

    public static final String FIND_PRACTICED_NO_OBJECT_IDS_PRICES_QUERY = """
            SELECT p.topiaid, p.objectid, p.priceunit, d.topiaid AS domain, d.code AS domaincode, d.campaign AS domaincampaign, p.practicedsystem, p.type, p.topiacreatedate, p.category,
                   p.displayname, p.price, p.sourceunit, p.sourceunitlabel, p.harvestingactionvalorisation, p.topiadiscriminator,
                   p.includedtreatment, p.chemicaltreatment, p.biologicaltreatment, p.producttype, p.seedtype
            FROM price p
            INNER JOIN practicedsystem ps ON p.practicedsystem = ps.topiaid
            INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
            INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
            INNER JOIN domain d ON gp.domain = d.topiaid
            WHERE category = %s
            AND price > 0
            ORDER BY price DESC, topiacreatedate DESC
             """;
    public static final String FIND_EFFECTIVE_FUEL_PRICES_QUERY = String.format(FIND_EFFECTIVE_NO_OBJECT_IDS_PRICES_QUERY, "'NONE_CATEGORIE'");

    public static final String FIND_PRACTICED_FUEL_PRICES_QUERY = String.format(FIND_PRACTICED_NO_OBJECT_IDS_PRICES_QUERY, "'NONE_CATEGORIE'");

    public static final String FIND_EFFECTIVE_IRRIGATION_PRICES_QUERY = String.format(FIND_EFFECTIVE_NO_OBJECT_IDS_PRICES_QUERY, "'IRRIGATION_ACTION_CATEGORIE'");

    public static final String FIND_PRACTICED_IRRIGATION_PRICES_QUERY = String.format(FIND_PRACTICED_NO_OBJECT_IDS_PRICES_QUERY, "'IRRIGATION_ACTION_CATEGORIE'");

    public static final String FIND_DOMAIN_PRICES_QUERY = String.format(FIND_PRICES_QUERY, "%s", "AND domain IN (%s)");
    public static final String FIND_PRACTICED_PRICES_QUERY = String.format(FIND_PRICES_QUERY, "%s", "AND practicedsystem IN (%s)");

    //                                                   1        2               3           4       5        6          7        8         9
    public static final String INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY = """
            INSERT INTO abstractdomaininputstockunit (topiaid, topiaversion, topiacreatedate, key, inputname, inputtype, domain, inputprice, code)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
            """;
    public static final String INSERT_INTO_DOMAIN_MINERAL_PRODUCT_INPUT_QUERY = """
            INSERT INTO domainmineralproductinput (topiaid, phytoeffect, refinput, usageunit)
            VALUES (?, ?, ?, ?)
            """;

    public static final String INSERT_INTO_DOMAIN_FUEL_INPUT_QUERY = """
            INSERT INTO domainfuelinput (topiaid, usageunit)
            VALUES (?, ?)
            """;

    public static final String INSERT_INTO_DOMAIN_IRRIGATION_INPUT_QUERY = """
            INSERT INTO domainirrigationinput (topiaid, usageunit)
            VALUES (?, ?)
            """;
    //                                          1                                        2         3           4     5       6     7      8       9
    public static final String INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY = """
            INSERT INTO abstractinputusage (topiaid, topiaversion, topiacreatedate, comment, productname, qtmin, qtavg, qtmed, qtmax, inputtype, code)
            VALUES (?, 0, (SELECT CURRENT_TIMESTAMP(3)::timestamp without time zone), ?, ?, ?, ?, ?, ?, ?, ?)
            """;


    public static final String INSERT_INTO_IRRIG_INPUT_USAGE_QUERY = """
            INSERT INTO irrigationinputusage (topiaid, domainirrigationinput)
            VALUES (?, ?)
            """;

    public static final String UPDATE_IRRIG_ACTION_QUERY = """
            UPDATE abstractaction SET irrigationinputusage = ? WHERE topiaid = ?

            """;
    public static final String INSERT_INTO_DOMAIN_MINERAL_PRODUCT_INPUT_USAGE_QUERY = """
            INSERT INTO mineralproductinputusage (topiaid, domainmineralproductinput, mineralfertilizersspreadingaction)
            VALUES (?, ?, ?)
            """;

    public static final String INSERT_INTO_DOMAIN_PHYTO_PRODUCT_INPUT_QUERY = """
            INSERT INTO domainphytoproductinput (topiaid, refinput, usageunit, producttype, domainseedspeciesinput)
            VALUES (?, ?, ?, ?, ?)
            """;

    public static final String INSERT_INTO_DOMAIN_ORGANIC_PRODUCT_INPUT_QUERY = """
            INSERT INTO domainorganicproductinput (topiaid, refinput, usageunit, n, p2o5, k2o, cao, mgo, s, organic)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            """;

    public static final String INSERT_INTO_DOMAIN_OTHER_INPUT_QUERY = """
            INSERT INTO domainotherinput (topiaid, refinput, usageunit, comment)
            VALUES (?, ?, ?, ?)
            """;

    //                                             1         2                     3                     4                   5                     6                                   7                            8                           9                 10                        11
    public static final String INSERT_INTO_DOMAIN_OTHER_PRODUCT_INPUT_USAGE_QUERY = """
            INSERT INTO otherproductinputusage (topiaid, domainotherinput, biologicalcontrolaction, harvestingaction, irrigationaction, maintenancepruningvinesaction, mineralfertilizersspreadingaction, organicfertilizersspreadingaction, otheraction, pesticidesspreadingaction, seedingaction)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            """;
    public static final String INSERT_INTO_DOMAIN_POT_INPUT_QUERY = """
            INSERT INTO domainpotinput (topiaid, refinput, usageunit)
            VALUES (?, ?, ?)
            """;

    public static final String INSERT_INTO_DOMAIN_SUBSTRATE_INPUT_QUERY = """
            INSERT INTO domainsubstrateinput (topiaid, refinput, usageunit)
            VALUES (?, ?, ?)
            """;

    public static final String INSERT_INTO_ABSTRACT_PHYTO_PRODUCT_INPUT_USAGE_QUERY = """
            INSERT INTO abstractphytoproductinputusage (topiaid, domainphytoproductinput)
            VALUES (?, ?)
            """;

    public static final String INSERT_INTO_PESTICIDE_PRODUCT_INPUT_USAGE_QUERY = """
            INSERT INTO pesticideproductinputusage (topiaid, pesticidesspreadingaction)
            VALUES (?, ?)
            """;

    public static final String INSERT_PHYTO_TARGETS = """
            INSERT INTO phytoproducttarget(topiaid, topiaversion, topiacreatedate, category, codegroupeciblemaa, target, abstractphytoproductinputusage)
            VALUES (?, 0, ?,
              (SELECT category FROM phytoproducttarget WHERE phytoproductinput = ? and target = ? limit 1),
              (SELECT codegroupeciblemaa FROM phytoproducttarget WHERE phytoproductinput = ? and target = ? limit 1),
              ?,
              ?
            )
            """;

    public static final String SELECT_PHYTO_TARGETS = """
            SELECT phytoproductinput, target FROM phytoproducttarget WHERE phytoproductinput IN (%s)
            """;

    public static final String INSERT_INTO_BIOLOGICAL_PRODUCT_INPUT_USAGE_QUERY = """
            INSERT INTO biologicalproductinputusage (topiaid, biologicalcontrolaction)
            VALUES (?, ?)
            """;
    public static final String INSERT_INTO_ORGANIC_PRODUCT_INPUT_USAGE_QUERY = """
            INSERT INTO organicproductinputusage (topiaid, domainorganicproductinput, organicfertilizersspreadingaction)
            VALUES (?, ?, ?)
            """;

    public static final String INSERT_INTO_POT_INPUT_USAGE_QUERY = """
            INSERT INTO potinputusage (topiaid, domainpotinput, otheraction)
            VALUES (?, ?, ?)
            """;

    //                                          1        2       3         4         5
    public static final String INSERT_INTO_DOMAIN_SEED_LOT_INPUT_QUERY = """
            INSERT INTO domainseedlotinput (topiaid, organic, cropseed, seedprice, usageunit)
            VALUES (?, ?, ?, ?, ?)
            """;

    //                                            1           2                        3                 4         5          6         7            8
    public static final String INSERT_INTO_DOMAIN_SEED_SPECIES_INPUT_QUERY = """
            INSERT INTO domainseedspeciesinput (topiaid, biologicalseedinoculation, chemicaltreatment, organic, speciesseed, seedtype, seedprice, domainseedlotinput)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?)
            """;

    //                                            1      2            3                 4
    public static final String INSERT_INTO_SEED_LOT_INPUT_USAGE_QUERY = """
            INSERT INTO seedlotinputusage (topiaid, deepness, domainseedlotinput, seedingactionusage)
            VALUES (?, ?, ?, ?)
            """;

    public static final String INSERT_INTO_SEED_PRODUCT_INPUT_USAGE = """
           INSERT INTO seedproductinputusage (topiaid, seedspeciesinputusage)
            VALUES (?, ?)
            """;

    public static final String INSERT_INTO_SEED_SPECIES_INPUT_USAGE_QUERY = """
            INSERT INTO seedspeciesinputusage (topiaid, deepness, domainseedspeciesinput, seedlotinputusage)
            VALUES (?, ?, ?, ?)
            """;

    public static final String INSERT_INTO_SUBSTRATE_INPUT_USAGE_QUERY = """
            INSERT INTO substrateinputusage (topiaid, domainsubstrateinput, otheraction)
            VALUES (?, ?, ?)
            """;
    //                            1            2               3                4              5         6           7        8         9          10       11         12
    public static final String INSERT_INPUT_PRICE_QUERY = """
        INSERT INTO inputprice (topiaid, topiaversion, topiacreatedate, topiadiscriminator, objectid, sourceunit, category, domain,  displayname, price, priceunit, phytoproducttype)
        VALUES (?,?,?,?,?,?,?,?,?,?,?,?)
            """;

    //                              1         2              3                 4              5          6           7        8         9          10      11               12                    13                     14            15       16
    public static final String INSERT_SEEDING_INPUT_PRICE_QUERY = """
        INSERT INTO inputprice (topiaid, topiaversion, topiacreatedate, topiadiscriminator, objectid, sourceunit, category, domain,  displayname, price, priceunit, biologicalseedinoculation, chemicaltreatment, includedtreatment, organic, seedtype)
        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
        """;

    public static final String GET_SEEING_ACTION_SPECIES_SEEDING_ACTION_QUERY = """
        SELECT actionId, mainaction, comment, toolscouplingcode, practicedintervention, effectiveintervention, yealdunit, seedtype, yealdtarget,
                isIntermediatecrop,
                interventionCropId,
                interventionCropCode,
                interventionIntermediateCropId,
                interventionIntermediateCropCode,
                domainid, domaincode, domaincampaign,
                psCampaigns,
                practicedsystem,
                gsCode,
                seedingActionSpeciesCropId,
                seedingActionSpeciesId,
                seedingActionSpeciesQuantity,
                seedingActionSpeciesCPS_Code,
                seedingActionSpeciesCPS_Id,
                seedingActionSpeciesDeepness,
                seedingActionSpeciesTreatment,
                seedingActionSpeciesBiologicalSeedInoculation,
                usageUnit,
                type_agriculture_id,
                crop_name,
                intermediate_crop_name,
                libelle_espece_botanique,
                libelle_qualifiant_aee,
                libelle_type_saisonnier_aee,
                libelle_destination_aee,
                libelle_variete
        FROM tmp_migrate_to_domain_input_stock_table
    """;
    public static final String CREATE_SEEING_ACTION_SPECIES_SEEDING_ACTION_TABLE_QUERY = """
        CREATE TABLE tmp_migrate_to_domain_input_stock_table AS (
            SELECT aa.topiaid AS actionId, aa.mainaction, aa.comment, aa.toolscouplingcode, aa.practicedintervention, aa.effectiveintervention, aa.yealdunit, aa.seedtype, aa.yealdtarget,
                false AS isIntermediatecrop,
                epcc.croppingplanentry AS interventionCropId,
                cpe.code AS interventionCropCode,
                null AS interventionIntermediateCropId,
                null AS interventionIntermediateCropCode,
                d.topiaid AS domainid, d.code AS domaincode, d.campaign AS domaincampaign,
                null as psCampaigns,
                null as practicedsystem,
                null as gsCode,
                null as seedingActionSpeciesCropId,
                null AS seedingActionSpeciesId,
                0.0 AS seedingActionSpeciesQuantity,
                null AS seedingActionSpeciesCPS_Code,
                null AS seedingActionSpeciesCPS_Id,
                0.0 AS seedingActionSpeciesDeepness,
                false AS seedingActionSpeciesTreatment,
                false AS seedingActionSpeciesBiologicalSeedInoculation,
                'KG_PAR_HA' AS usageUnit,
                (SELECT rta.reference_id
                  FROM reftypeagriculture rta
                  INNER JOIN growingsystem gs ON gs.typeagriculture = rta.topiaid
                  INNER JOIN plot p ON p.growingsystem = gs.topiaid
                  INNER JOIN zone z ON z.plot = p.topiaid AND epcc.zone = z.topiaid) AS type_agriculture_id,
                cpe.name AS crop_name,
                null AS intermediate_crop_name,
                null AS libelle_espece_botanique,
                null AS libelle_qualifiant_aee,
                null AS libelle_type_saisonnier_aee,
                null AS libelle_destination_aee,
                null AS libelle_variete
            FROM tmp_seedingaction aa
            INNER JOIN effectiveintervention ei ON aa.effectiveintervention= ei.topiaid
            INNER JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
            INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid
            INNER JOIN croppingplanentry cpe ON cpe.topiaid = epcc.croppingplanentry
            INNER JOIN domain d ON cpe.domain = d.topiaid
            WHERE NOT EXISTS (
              SELECT sas.seedingaction FROM seedingactionspecies sas WHERE sas.seedingaction = aa.topiaid)
                   
            UNION
                   
            SELECT aa.topiaid AS actionId, aa.mainaction, aa.comment, aa.toolscouplingcode, aa.practicedintervention, aa.effectiveintervention, aa.yealdunit, aa.seedtype, aa.yealdtarget,
                ei.intermediatecrop AS isIntermediatecrop,
                eccn.croppingplanentry AS interventionCropId,
                cpe.code AS interventionCropCode,
                (SELECT eccc.intermediatecroppingplanentry FROM effectivecropcycleconnection eccc WHERE eccc.target = eccn.topiaid) AS interventionIntermediateCropId,
                (SELECT icpe.code FROM croppingplanentry icpe INNER JOIN effectivecropcycleconnection eccc ON eccc.intermediatecroppingplanentry = icpe.topiaid AND eccc.target = eccn.topiaid) AS interventionIntermediateCropCode,
                d.topiaid AS domainid, d.code AS domaincode, d.campaign AS domaincampaign,
                null as psCampaigns,
                null as practicedsystem,
                null as gsCode,
                null as seedingActionSpeciesCropId,
                null AS seedingActionSpeciesId,
                0.0 AS seedingActionSpeciesQuantity,
                null AS seedingActionSpeciesCPS_Code,
                null AS seedingActionSpeciesCPS_Id,
                0.0 AS seedingActionSpeciesDeepness,
                false AS seedingActionSpeciesTreatment,
                false AS seedingActionSpeciesBiologicalSeedInoculation,
                'KG_PAR_HA' AS usageUnit,
                (SELECT rta.reference_id
                  FROM reftypeagriculture rta
                  INNER JOIN growingsystem gs ON gs.typeagriculture = rta.topiaid
                  INNER JOIN plot p ON p.growingsystem = gs.topiaid
                  INNER JOIN zone z ON z.plot = p.topiaid
                  INNER JOIN effectiveseasonalcropcycle escc
                    ON escc.zone = z.topiaid
                    AND escc.topiaid = eccn.effectiveseasonalcropcycle) AS type_agriculture_id,
                cpe.name AS crop_name,
                (SELECT icpe.name FROM croppingplanentry icpe
                                  INNER JOIN effectivecropcycleconnection eccc ON  icpe.topiaid = eccc.intermediatecroppingplanentry
                                  AND eccc.target = eccn.topiaid) AS intermediate_crop_name,
                null AS libelle_espece_botanique,
                null AS libelle_qualifiant_aee,
                null AS libelle_type_saisonnier_aee,
                null AS libelle_destination_aee,
                null AS libelle_variete
            FROM tmp_seedingaction aa
            INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
            INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
            INNER JOIN croppingplanentry cpe ON cpe.topiaid = eccn.croppingplanentry
            INNER JOIN domain d ON cpe.domain = d.topiaid
            WHERE NOT EXISTS (
                SELECT sas.seedingaction FROM seedingactionspecies sas WHERE sas.seedingaction = aa.topiaid)

            UNION
                   
            SELECT aa.topiaid AS actionId, aa.mainaction, aa.comment, aa.toolscouplingcode, aa.practicedintervention, aa.effectiveintervention, aa.yealdunit, aa.seedtype, aa.yealdtarget,
                false AS isIntermediatecrop,
                epcc.croppingplanentry AS interventionCropId,
                cyclecpe.code AS interventionCropCode,
                null AS interventionIntermediateCropId,
                null AS interventionIntermediateCropCode,
                d.topiaid AS domainid, d.code AS domaincode, d.campaign AS domaincampaign,
                null as psCampaigns,
                null as practicedsystem,
                null as gsCode,
                sascpe.topiaid AS seedingActionSpeciesCropId,
                sas.topiaid AS seedingActionSpeciesId,
                sas.quantity AS seedingActionSpeciesQuantity,
                sas.speciescode AS seedingActionSpeciesCPS_Code,
                (SELECT sas_cps.topiaid FROM croppingplanspecies sas_cps INNER JOIN croppingplanentry sas_cpe ON sas_cps.croppingplanentry = sas_cpe.topiaid WHERE sas_cps.code = sas.speciescode AND sas_cpe.domain = d.topiaid limit 1) AS seedingActionSpeciesCPS_Id,
                sas.deepness AS seedingActionSpeciesDeepness,
                sas.treatment AS seedingActionSpeciesTreatment,
                sas.biologicalseedinoculation AS seedingActionSpeciesBiologicalSeedInoculation,
                sas.seedplantunit AS usageUnit,
                (SELECT rta.reference_id
                  FROM reftypeagriculture rta
                  INNER JOIN growingsystem gs ON gs.typeagriculture = rta.topiaid
                  INNER JOIN plot p ON p.growingsystem = gs.topiaid
                  INNER JOIN zone z ON z.plot = p.topiaid AND epcc.zone = z.topiaid) AS type_agriculture_id,
                cyclecpe.name AS crop_name,
                null AS intermediate_crop_name,
                (SELECT re.libelle_espece_botanique FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_espece_botanique,
                (SELECT re.libelle_qualifiant_aee FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_qualifiant_aee,
                (SELECT re.libelle_type_saisonnier_aee FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_type_saisonnier_aee,
                (SELECT re.libelle_destination_aee FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_destination_aee,
                (SELECT CONCAT(
                 (SELECT rvpg.variete FROM refvarieteplantgrape rvpg INNER JOIN croppingplanspecies cps ON rvpg.topiaid = cps.variety WHERE cps.code = sas.speciescode limit 1),
                 (SELECT rvg.denomination FROM refvarietegeves rvg INNER JOIN croppingplanspecies cps ON rvg.topiaid =  cps.variety WHERE cps.code = sas.speciescode limit 1)
                )) AS libelle_variete
            FROM tmp_seedingaction aa
            INNER JOIN seedingactionspecies sas ON sas.seedingaction = aa.topiaid
            INNER JOIN effectiveintervention ei ON aa.effectiveintervention = ei.topiaid
            INNER JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
            INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid
            INNER JOIN croppingplanentry cyclecpe ON cyclecpe.topiaid = epcc.croppingplanentry
            INNER JOIN domain d ON cyclecpe.domain = d.topiaid
            INNER JOIN croppingplanspecies sascps ON sascps.code = sas.speciescode
            INNER JOIN croppingplanentry sascpe ON  (sascpe.domain=d.topiaid AND sascps.croppingplanentry = sascpe.topiaid)

            UNION
            
            SELECT aa.topiaid AS actionId, aa.mainaction, aa.comment, aa.toolscouplingcode, aa.practicedintervention, aa.effectiveintervention, aa.yealdunit, aa.seedtype, aa.yealdtarget,
                ei.intermediatecrop AS isIntermediatecrop,
                eccn.croppingplanentry AS interventionCropId,
                cyclecpe.code AS interventionCropCode,
                (SELECT eccc.intermediatecroppingplanentry FROM effectivecropcycleconnection eccc WHERE eccc.target = eccn.topiaid) AS interventionIntermediateCropId,
                (SELECT icpe.code FROM croppingplanentry icpe INNER JOIN effectivecropcycleconnection eccc ON eccc.intermediatecroppingplanentry = icpe.topiaid AND eccc.target = eccn.topiaid) AS interventionIntermediateCropCode,
                d.topiaid AS domainid, d.code AS domaincode, d.campaign AS domaincampaign,
                null as psCampaigns,
                null as practicedsystem,
                null as gsCode,
                sascpe.topiaid AS seedingActionSpeciesCropId,
                sas.topiaid AS seedingActionSpeciesId,
                sas.quantity AS seedingActionSpeciesQuantity,
                sas.speciescode AS seedingActionSpeciesCPS_Code,
                (SELECT sas_cps.topiaid FROM croppingplanspecies sas_cps INNER JOIN croppingplanentry sas_cpe ON sas_cps.croppingplanentry = sas_cpe.topiaid WHERE sas_cps.code = sas.speciescode AND sas_cpe.domain = d.topiaid limit 1) AS seedingActionSpeciesCPS_Id,
                sas.deepness AS seedingActionSpeciesDeepness,
                sas.treatment AS seedingActionSpeciesTreatment,
                sas.biologicalseedinoculation AS seedingActionSpeciesBiologicalSeedInoculation,
                sas.seedplantunit AS usageUnit,
                (SELECT rta.reference_id
                  FROM reftypeagriculture rta
                  INNER JOIN growingsystem gs ON gs.typeagriculture = rta.topiaid
                  INNER JOIN plot p ON p.growingsystem = gs.topiaid
                  INNER JOIN zone z ON z.plot = p.topiaid
                  INNER JOIN effectiveseasonalcropcycle escc
                    ON escc.zone = z.topiaid
                    AND escc.topiaid = eccn.effectiveseasonalcropcycle) AS type_agriculture_id,
                cyclecpe.name AS crop_name,
                (SELECT icpe.name FROM croppingplanentry icpe INNER JOIN effectivecropcycleconnection eccc ON  icpe.topiaid = eccc.intermediatecroppingplanentry AND eccc.target = eccn.topiaid) AS intermediate_crop_name,
                (SELECT re.libelle_espece_botanique FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_espece_botanique,
                (SELECT re.libelle_qualifiant_aee FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_qualifiant_aee,
                (SELECT re.libelle_type_saisonnier_aee FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_type_saisonnier_aee,
                (SELECT re.libelle_destination_aee FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_destination_aee,
                (SELECT CONCAT(
                 (SELECT rvpg.variete FROM refvarieteplantgrape rvpg INNER JOIN croppingplanspecies cps ON rvpg.topiaid = cps.variety WHERE cps.code = sas.speciescode limit 1),
                 (SELECT rvg.denomination FROM refvarietegeves rvg INNER JOIN croppingplanspecies cps ON rvg.topiaid =  cps.variety WHERE cps.code = sas.speciescode limit 1)
                )) AS libelle_variete
            FROM tmp_seedingaction aa
            INNER JOIN seedingactionspecies sas ON sas.seedingaction = aa.topiaid
            INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
            INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
            INNER JOIN croppingplanentry cyclecpe ON cyclecpe.topiaid = eccn.croppingplanentry
            INNER JOIN domain d ON cyclecpe.domain = d.topiaid
            INNER JOIN croppingplanspecies sascps ON sascps.code = sas.speciescode
            INNER JOIN croppingplanentry sascpe ON (sascpe.domain=d.topiaid AND sascps.croppingplanentry = sascpe.topiaid)
            
            UNION
            
            SELECT aa.topiaid AS actionId, aa.mainaction, aa.comment, aa.toolscouplingcode, aa.practicedintervention, aa.effectiveintervention, aa.yealdunit, aa.seedtype, aa.yealdtarget,
                false AS isIntermediatecrop,
                (SELECT cpe.topiaid FROM croppingplanentry cpe WHERE cpe.code = ppcc.croppingplanentrycode AND cpe.domain = d.topiaid) AS interventionCropId,
                ppcc.croppingplanentrycode AS interventionCropCode,
                null AS interventionIntermediateCropId,
                null AS interventionIntermediateCropCode,
                d.topiaid AS domainid, d.code AS domaincode, d.campaign AS domaincampaign,
                ps.campaigns AS psCampaigns,
                ps.topiaid as practicedsystem,
                gs.code AS gsCode,
                null as seedingActionSpeciesCropId,
                null AS seedingActionSpeciesId,
                0.0 AS seedingActionSpeciesQuantity,
                null AS seedingActionSpeciesCPS_Code,
                null AS seedingActionSpeciesCPS_Id,
                null AS seedingActionSpeciesDeepness,
                false AS seedingActionSpeciesTreatment,
                false AS seedingActionSpeciesBiologicalSeedInoculation,
                'KG_PAR_HA' AS usageUnit,
                (select rta.reference_id FROM reftypeagriculture rta INNER JOIN growingsystem gs0 ON gs0.typeagriculture = rta.topiaid AND gs0.topiaid = gs.topiaid) AS type_agriculture_id,
                (SELECT cpe.name FROM croppingplanentry cpe WHERE cpe.code = ppcc.croppingplanentrycode AND cpe.domain = d.topiaid) AS crop_name,
                null AS intermediate_crop_name,
                null AS libelle_espece_botanique,
                null AS libelle_qualifiant_aee,
                null AS libelle_type_saisonnier_aee,
                null AS libelle_destination_aee,
                null AS libelle_variete
            FROM tmp_seedingaction aa
            INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
            INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
            INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
            INNER JOIN practicedperennialcropcycle ppcc ON  ppcc.topiaid = pcc.topiaid
            INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
            INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
            INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
            INNER JOIN domain d ON gp.domain = d.topiaid
            WHERE NOT EXISTS (
              SELECT sas.seedingaction FROM seedingactionspecies sas WHERE sas.seedingaction = aa.topiaid)
                   
            UNION
                   
            SELECT aa.topiaid AS actionId, aa.mainaction, aa.comment, aa.toolscouplingcode, aa.practicedintervention, aa.effectiveintervention, aa.yealdunit, aa.seedtype, aa.yealdtarget,
                pi.intermediatecrop AS isIntermediatecrop,
                (SELECT cpe.topiaid FROM croppingplanentry cpe WHERE cpe.code = pccn.croppingplanentrycode AND cpe.domain = d.topiaid) AS interventionCropId,
                pccn.croppingplanentrycode AS interventionCropCode,
                (SELECT cpe.topiaid FROM croppingplanentry cpe WHERE cpe.code = pccc.intermediatecroppingplanentrycode AND cpe.domain = d.topiaid) AS interventionIntermediateCropId,
                pccc.intermediatecroppingplanentrycode AS interventionIntermediateCropCode,
                d.topiaid AS domainid, d.code AS domaincode, d.campaign AS domaincampaign,
                ps.campaigns AS psCampaigns,
                ps.topiaid as practicedsystem,
                gs.code AS gsCode,
                null as seedingActionSpeciesCropId,
                null AS seedingActionSpeciesId,
                0.0 AS seedingActionSpeciesQuantity,
                null AS seedingActionSpeciesCPS_Code,
                null AS seedingActionSpeciesCPS_Id,
                0.0 AS seedingActionSpeciesDeepness,
                false AS seedingActionSpeciesTreatment,
                false AS seedingActionSpeciesBiologicalSeedInoculation,
                'KG_PAR_HA' AS usageUnit,
                (select rta.reference_id FROM reftypeagriculture rta INNER JOIN growingsystem gs0 ON gs0.typeagriculture = rta.topiaid AND gs0.topiaid = gs.topiaid) AS type_agriculture_id,
                (SELECT cpe.name FROM croppingplanentry cpe WHERE cpe.code = pccn.croppingplanentrycode AND cpe.domain = d.topiaid) AS crop_name,
                (SELECT cpe.name FROM croppingplanentry cpe WHERE cpe.code = pccc.intermediatecroppingplanentrycode AND cpe.domain = d.topiaid) AS intermediate_crop_name,
                null AS libelle_espece_botanique,
                null AS libelle_qualifiant_aee,
                null AS libelle_type_saisonnier_aee,
                null AS libelle_destination_aee,
                null AS libelle_variete
            FROM tmp_seedingaction aa
            INNER JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
            INNER JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
            INNER JOIN practicedcropcyclenode pccn ON pccc.target = pccn.topiaid
            INNER JOIN practicedseasonalcropcycle pscc ON pccn.practicedseasonalcropcycle = pscc.topiaid
            INNER JOIN practicedcropcycle pcc ON pscc.topiaid = pcc.topiaid
            INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
            INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
            INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
            INNER JOIN domain d ON gp.domain = d.topiaid
            WHERE NOT EXISTS (
              SELECT sas.seedingaction FROM seedingactionspecies sas WHERE sas.seedingaction = aa.topiaid)
                   
            UNION
                   
            SELECT aa.topiaid AS actionId, aa.mainaction, aa.comment, aa.toolscouplingcode, aa.practicedintervention, aa.effectiveintervention, aa.yealdunit, aa.seedtype, aa.yealdtarget,
                false AS isIntermediatecrop,
                (SELECT cpe.topiaid FROM croppingplanentry cpe WHERE cpe.code = ppcc.croppingplanentrycode AND cpe.domain = d.topiaid) AS interventionCropId,
                ppcc.croppingplanentrycode AS interventionCropCode,
                null AS interventionIntermediateCropId,
                null AS interventionIntermediateCropCode,
                d.topiaid AS domainid, d.code AS domaincode, d.campaign AS domaincampaign,
                ps.campaigns AS psCampaigns,
                ps.topiaid as practicedsystem,
                gs.code AS gsCode,
                sascpe.topiaid AS seedingActionSpeciesCropId,
                sas.topiaid AS seedingActionSpeciesId,
                sas.quantity AS seedingActionSpeciesQuantity,
                sas.speciescode AS seedingActionSpeciesCPS_Code,
                (SELECT sas_cps.topiaid FROM croppingplanspecies sas_cps INNER JOIN croppingplanentry sas_cpe ON sas_cps.croppingplanentry = sas_cpe.topiaid WHERE sas_cps.code = sas.speciescode AND sas_cpe.domain = d.topiaid limit 1) AS seedingActionSpeciesCPS_Id,
                sas.deepness AS seedingActionSpeciesDeepness,
                sas.treatment AS seedingActionSpeciesTreatment,
                sas.biologicalseedinoculation AS seedingActionSpeciesBiologicalSeedInoculation,
                sas.seedplantunit AS usageUnit,
                (select rta.reference_id FROM reftypeagriculture rta INNER JOIN growingsystem gs0 ON gs0.typeagriculture = rta.topiaid AND gs0.topiaid = gs.topiaid) AS type_agriculture_id,
                (SELECT cpe.name FROM croppingplanentry cpe WHERE cpe.code = ppcc.croppingplanentrycode AND cpe.domain = d.topiaid) AS crop_name,
                null AS intermediate_crop_name,
                (SELECT re.libelle_espece_botanique FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_espece_botanique,
                (SELECT re.libelle_qualifiant_aee FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_qualifiant_aee,
                (SELECT re.libelle_type_saisonnier_aee FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_type_saisonnier_aee,
                (SELECT re.libelle_destination_aee FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_destination_aee,
                (SELECT CONCAT(
                 (SELECT rvpg.variete FROM refvarieteplantgrape rvpg INNER JOIN croppingplanspecies cps ON rvpg.topiaid = cps.variety WHERE cps.code = sas.speciescode limit 1),
                 (SELECT rvg.denomination FROM refvarietegeves rvg INNER JOIN croppingplanspecies cps ON rvg.topiaid =  cps.variety WHERE cps.code = sas.speciescode limit 1)
                )) AS libelle_variete
            FROM tmp_seedingaction aa
            INNER JOIN seedingactionspecies sas ON sas.seedingaction = aa.topiaid
            INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
            INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
            INNER JOIN practicedperennialcropcycle ppcc ON ppcc.topiaid = pccp.practicedperennialcropcycle
            INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
            INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
            INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
            INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
            INNER JOIN domain d ON gp.domain = d.topiaid
            INNER JOIN croppingplanspecies sascps ON sascps.code = sas.speciescode
            INNER JOIN croppingplanentry sascpe ON  (sascpe.domain=d.topiaid AND sascps.croppingplanentry = sascpe.topiaid)

            UNION
                   
            SELECT aa.topiaid AS actionId, aa.mainaction, aa.comment, aa.toolscouplingcode, aa.practicedintervention, aa.effectiveintervention, aa.yealdunit, aa.seedtype, aa.yealdtarget,
                pi.intermediatecrop AS isIntermediatecrop,
                (SELECT cpe.topiaid FROM croppingplanentry cpe WHERE cpe.code = pccn.croppingplanentrycode AND cpe.domain = d.topiaid) AS interventionCropId,
                pccn.croppingplanentrycode AS interventionCropCode,
                (SELECT cpe.topiaid FROM croppingplanentry cpe WHERE cpe.code = pccc.intermediatecroppingplanentrycode AND cpe.domain = d.topiaid) AS interventionIntermediateCropId,
                pccc.intermediatecroppingplanentrycode AS interventionIntermediateCropCode,
                d.topiaid AS domainid, d.code AS domaincode, d.campaign AS domaincampaign,
                ps.campaigns AS psCampaigns,
                ps.topiaid as practicedsystem,
                gs.code AS gsCode,
                sascpe.topiaid AS seedingActionSpeciesCropId,
                sas.topiaid AS seedingActionSpeciesId,
                sas.quantity AS seedingActionSpeciesQuantity,
                sas.speciescode AS seedingActionSpeciesCPS_Code,
                (SELECT sas_cps.topiaid FROM croppingplanspecies sas_cps INNER JOIN croppingplanentry sas_cpe ON sas_cps.croppingplanentry = sas_cpe.topiaid WHERE sas_cps.code = sas.speciescode AND sas_cpe.domain = d.topiaid limit 1) AS seedingActionSpeciesCPS_Id,
                sas.deepness AS seedingActionSpeciesDeepness,
                sas.treatment AS seedingActionSpeciesTreatment,
                sas.biologicalseedinoculation AS seedingActionSpeciesBiologicalSeedInoculation,
                sas.seedplantunit AS usageUnit,
                (SELECT rta.reference_id FROM reftypeagriculture rta INNER JOIN growingsystem gs0 ON gs0.typeagriculture = rta.topiaid AND gs0.topiaid = gs.topiaid) AS type_agriculture_id,
                (SELECT cpe.name FROM croppingplanentry cpe WHERE cpe.code = pccn.croppingplanentrycode AND cpe.domain = d.topiaid) AS crop_name,
                (SELECT cpe.name FROM croppingplanentry cpe WHERE cpe.code = pccc.intermediatecroppingplanentrycode AND cpe.domain = d.topiaid) AS intermediate_crop_name,
                (SELECT re.libelle_espece_botanique FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_espece_botanique,
                (SELECT re.libelle_qualifiant_aee FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_qualifiant_aee,
                (SELECT re.libelle_type_saisonnier_aee FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_type_saisonnier_aee,
                (SELECT re.libelle_destination_aee FROM refespece re INNER JOIN croppingplanspecies cps ON re.topiaid = cps.species WHERE cps.code = sas.speciescode limit 1) AS libelle_destination_aee,
                (SELECT CONCAT(
                 (SELECT rvpg.variete FROM refvarieteplantgrape rvpg INNER JOIN croppingplanspecies cps ON rvpg.topiaid = cps.variety WHERE cps.code = sas.speciescode limit 1),
                 (SELECT rvg.denomination FROM refvarietegeves rvg INNER JOIN croppingplanspecies cps ON rvg.topiaid =  cps.variety WHERE cps.code = sas.speciescode limit 1)
                )) AS libelle_variete
            FROM tmp_seedingaction aa
            INNER JOIN seedingactionspecies sas ON sas.seedingaction = aa.topiaid
            INNER JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
            INNER JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
            INNER JOIN practicedcropcyclenode pccn ON pccc.target = pccn.topiaid
            INNER JOIN practicedseasonalcropcycle pscc ON pccn.practicedseasonalcropcycle = pscc.topiaid
            INNER JOIN practicedcropcycle pcc ON pscc.topiaid = pcc.topiaid
            INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
            INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
            INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
            INNER JOIN domain d ON gp.domain = d.topiaid
            INNER JOIN croppingplanspecies sascps ON sascps.code = sas.speciescode
            INNER JOIN croppingplanentry sascpe ON  (sascpe.domain=d.topiaid AND sascps.croppingplanentry = sascpe.topiaid)
        )
            """;


    public static final String GET_IRRIG_ACTION_SPECIES_SEEDING_ACTION_QUERY = """
                    SELECT aa.topiaid AS actionId,
                           d.topiaid AS domainid,
                           d.code AS domaincode,
                           d.campaign AS domaincampaign,
                           aa.waterquantitymin as waterquantitymin,
                           aa.waterquantitymax as waterquantitymax,
                           aa.waterquantityaverage as waterquantityaverage,
                           aa.waterquantitymedian as waterquantitymedian
                    FROM abstractaction aa
                    INNER JOIN effectiveintervention ei ON aa.effectiveintervention= ei.topiaid
                    INNER JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
                    INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid
                    INNER JOIN croppingplanentry cpe ON cpe.topiaid = epcc.croppingplanentry
                    INNER JOIN domain d ON cpe.domain = d.topiaid
                    WHERE aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.IrrigationActionImpl'
                           
                    UNION
                           
                    SELECT aa.topiaid AS actionId,
                           d.topiaid AS domainid,
                           d.code AS domaincode,
                           d.campaign AS domaincampaign,
                           aa.waterquantitymin as waterquantitymin,
                           aa.waterquantitymax as waterquantitymax,
                           aa.waterquantityaverage as waterquantityaverage,
                           aa.waterquantitymedian as waterquantitymedian
                    FROM abstractaction aa
                    INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
                    INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
                    INNER JOIN croppingplanentry cpe ON cpe.topiaid = eccn.croppingplanentry
                    INNER JOIN domain d ON cpe.domain = d.topiaid
                    WHERE aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.IrrigationActionImpl'

                    UNION
                    
                    SELECT aa.topiaid AS actionId,
                           d.topiaid AS domainid,
                           d.code AS domaincode,
                           d.campaign AS domaincampaign,
                           aa.waterquantitymin as waterquantitymin,
                           aa.waterquantitymax as waterquantitymax,
                           aa.waterquantityaverage as waterquantityaverage,
                           aa.waterquantitymedian as waterquantitymedian
                    FROM abstractaction aa
                    INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
                    INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
                    INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
                    INNER JOIN practicedperennialcropcycle ppcc ON  ppcc.topiaid = pcc.topiaid
                    INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
                    INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
                    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
                    INNER JOIN domain d ON gp.domain = d.topiaid
                    WHERE aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.IrrigationActionImpl'
                    
                    UNION
                           
                    SELECT aa.topiaid AS actionId,
                           d.topiaid AS domainid,
                           d.code AS domaincode,
                           d.campaign AS domaincampaign,
                           aa.waterquantitymin as waterquantitymin,
                           aa.waterquantitymax as waterquantitymax,
                           aa.waterquantityaverage as waterquantityaverage,
                           aa.waterquantitymedian as waterquantitymedian
                    FROM abstractaction aa
                    INNER JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
                    INNER JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
                    INNER JOIN practicedcropcyclenode pccn ON pccc.target = pccn.topiaid
                    INNER JOIN practicedseasonalcropcycle pscc ON pccn.practicedseasonalcropcycle = pscc.topiaid
                    INNER JOIN practicedcropcycle pcc ON pscc.topiaid = pcc.topiaid
                    INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
                    INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
                    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
                    INNER JOIN domain d ON gp.domain = d.topiaid
                    WHERE aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.IrrigationActionImpl'
            """;

    protected final Log LOG = LogFactory.getLog(V3_0_0_9__11484_input_storage.class);
    public static final String DEFAULT_FALLBACK_TOPIA_ID = "_0000000-0000-0000-0000-000000000000";
    public static final String DEFAULT_REF_ACTA_TRAITEMENTS_PRODUIT_ID = RefActaTraitementsProduit.class.getName() + DEFAULT_FALLBACK_TOPIA_ID;

    public static final String DEFAULT_REF_OTHER_INPUT_ID = RefOtherInput.class.getName() + DEFAULT_FALLBACK_TOPIA_ID;

    public static final String DEFAULT_REF_POT_ID = RefPot.class.getName() + DEFAULT_FALLBACK_TOPIA_ID;

    public static final String DEFAULT_REF_SUBSTRAT_ID = RefSubstrate.class.getName() + DEFAULT_FALLBACK_TOPIA_ID;

    public static final String DEFAULT_REF_FERTI_ORGA_ID = RefFertiOrga.class.getName() + DEFAULT_FALLBACK_TOPIA_ID;
    public static final String INNER_JOIN_ABSTRACTACTION_AA_ON_AA = "INNER JOIN abstractaction aa ON aa.topiaid = ";
    public static final int PERSIST_INPUT_CHUNK_SIZE = 50;
    public static final int CHUNCK_SIZE = 500;

    java.sql.Date scriptDate = new java.sql.Date(new Date().getTime());

    private void createDefaultRefActaTreatmentsProducts(Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM RefActaTraitementsProduit WHERE topiaid = ?")){
            statement.setString(1, DEFAULT_REF_ACTA_TRAITEMENTS_PRODUIT_ID);
            statement.execute();
        }
        //                                                1        2                 3            4           5                6               7               8      9
        String insertFakeRefActaTraitementsProduit = """
                INSERT INTO RefActaTraitementsProduit (topiaid, topiaversion, topiacreatedate, id_produit, id_traitement, nom_traitement, code_traitement_maa, nodu, active) VALUES
                (?, ?, ?, ?, ?, ?, ?, ?, ?)
                """;
        try (PreparedStatement statement = connection.prepareStatement(insertFakeRefActaTraitementsProduit)){
            statement.setString(1, DEFAULT_REF_ACTA_TRAITEMENTS_PRODUIT_ID);
            statement.setLong(2, 0L);
            statement.setDate(3, scriptDate);
            statement.setString(4,"NO_PRODUCT");
            statement.setInt(5, 99999);
            statement.setString(6,"NO_PRODUCT");
            statement.setString(7,"NO_PRODUCT");
            statement.setBoolean(8, false);
            statement.setBoolean(9, false);
            statement.execute();
        }
    }

    private void createDefaultRefOtherInput(Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM refotherinput_sector WHERE owner = ?")) {
            statement.setString(1, DEFAULT_REF_OTHER_INPUT_ID);
            statement.execute();
        }

        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM refotherinput WHERE topiaid = ?")) {
            statement.setString(1, DEFAULT_REF_OTHER_INPUT_ID);
            statement.execute();
        }

        String insertFakeRefOtherInoutSector = """
            INSERT INTO refotherinput_sector (owner, sector)
            VALUES (?, ?)
                """;

        //                                1          2            3              4              5                6              7             8        9        10      11              12

        String insertFakeRefOtherInput = """
            INSERT INTO refotherinput (topiaid, topiaversion, inputtype_c0, caracteristic2, caracteristic3, topiacreatedate, caracteristic1, lifetime, active, source, reference_id, reference_code)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            """;

        try (PreparedStatement refOtherInputStatement = connection.prepareStatement(insertFakeRefOtherInput)) {
            try (PreparedStatement insertIntoRefOterInputSectorStatement = connection.prepareStatement(insertFakeRefOtherInoutSector)) {

                refOtherInputStatement.setString(1, DEFAULT_REF_OTHER_INPUT_ID);
                refOtherInputStatement.setInt(2, 0);
                refOtherInputStatement.setString(3, "-");
                refOtherInputStatement.setString(4, "-");
                refOtherInputStatement.setString(5, "-");
                refOtherInputStatement.setDate(6, scriptDate);
                refOtherInputStatement.setString(7, "AUTRE");
                refOtherInputStatement.setDouble(8,  0);
                refOtherInputStatement.setBoolean(9, false);
                refOtherInputStatement.setString(10, "CL");
                refOtherInputStatement.setString(11, "999999999");
                refOtherInputStatement.setString(12, "999999999");
                refOtherInputStatement.execute();

                for (Sector sector : Sector.values()) {

                    insertIntoRefOterInputSectorStatement.setString(1, DEFAULT_REF_OTHER_INPUT_ID);
                    insertIntoRefOterInputSectorStatement.setString(2, sector.name());
                    insertIntoRefOterInputSectorStatement.execute();

                }
            }
        }
    }

    private void createDefaultRefFertiOrga(Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM RefFertiOrga WHERE topiaid = ?")){
            statement.setString(1, DEFAULT_REF_FERTI_ORGA_ID);
            statement.execute();
        }
        //                                    1        2                 3            4             5       6
        String insertFakeRefActaTraitementsProduit = """
                INSERT INTO reffertiorga (topiaid, topiaversion, topiacreatedate, idtypeeffluent, libelle, active) VALUES
                (?, ?, ?, ?, ?, ?)
                """;
        try (PreparedStatement statement = connection.prepareStatement(insertFakeRefActaTraitementsProduit)){
            statement.setString(1, DEFAULT_REF_FERTI_ORGA_ID);
            statement.setLong(2, 0L);
            statement.setDate(3, scriptDate);
            statement.setString(4,"NO_PRODUCT");
            statement.setString(5, "NO_PRODUCT");
            statement.setBoolean(6, false);
            statement.execute();
        }
    }

    private void createDefaultRefSubstrate(Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM refsubstrate WHERE topiaid = ?")){
            statement.setString(1, DEFAULT_REF_SUBSTRAT_ID);
            statement.execute();
        }
        //                                    1        2                 3            4             5               6          7
        String insertFakeRefActaTraitementsProduit = """
                INSERT INTO refsubstrate (topiaid, topiaversion, topiacreatedate, caracteristic1, caracteristic2, unittype, active) VALUES
                (?, ?, ?, ?, ?, ?, ?)
                """;
        try (PreparedStatement statement = connection.prepareStatement(insertFakeRefActaTraitementsProduit)){
            statement.setString(1, DEFAULT_REF_SUBSTRAT_ID);
            statement.setLong(2, 0L);
            statement.setDate(3, scriptDate);
            statement.setString(4,"NO_PRODUCT");
            statement.setString(5, "NO_PRODUCT");
            statement.setString(6, "VOLUME");
            statement.setBoolean(7, false);
            statement.execute();
        }
    }
    private void createDefaultRefPot(Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM refpot WHERE topiaid = ?")){
            statement.setString(1, DEFAULT_REF_POT_ID);
            statement.execute();
        }
        //                                    1        2                 3            4        5      6
        String insertFakeRefActaTraitementsProduit = """
                INSERT INTO refpot (topiaid, topiaversion, topiacreatedate, caracteristic1, volume, active) VALUES
                (?, ?, ?, ?, ?, ?)
                """;
        try (PreparedStatement statement = connection.prepareStatement(insertFakeRefActaTraitementsProduit)){
            statement.setString(1, DEFAULT_REF_POT_ID);
            statement.setLong(2, 0L);
            statement.setDate(3, scriptDate);
            statement.setString(4,"NO_PRODUCT");
            statement.setDouble(5, 0.0);
            statement.setBoolean(6, false);
            statement.execute();
        }
    }

    private void addOtherInputCommentColumn(Connection connection) throws SQLException {

        try (Statement statement = connection.createStatement()){
            statement.execute("ALTER TABLE domainotherinput add column comment text");
        }

    }
    private void createActionIndexes(Connection connection) throws SQLException {

        try (Statement statement = connection.createStatement()){
            statement.execute("CREATE INDEX if not exists abstractaction_practicedintervention ON abstractaction(practicedintervention)");
            statement.execute("CREATE INDEX if not exists abstractaction_effectiveintervention ON abstractaction(effectiveintervention)");
        }

    }

    private void createTemporaryTargetInputIndexe(Connection connection) throws SQLException {

        try (Statement statement = connection.createStatement()){
            statement.execute("CREATE INDEX if not exists phytoproducttargetphytoproductinputidx ON phytoproducttarget(phytoproductinput);");
        }

    }

    private void dropTemporaryTargetInputIndexe(Connection connection) throws SQLException {

        try (Statement statement = connection.createStatement()){
            statement.execute("DROP INDEX phytoproducttargetphytoproductinputidx ;");
        }

    }

    private int getNbInputToMigrate(Connection connection) throws SQLException {
        String countInputEntries = "SELECT COUNT(*) AS nbinputs FROM abstractinput";
        final int nbInputs;
        try (Statement statement = connection.createStatement();
             ResultSet inputsRs = statement.executeQuery(countInputEntries)) {
            inputsRs.next();
            nbInputs = inputsRs.getInt("nbinputs");

        }
        return nbInputs;
    }

    private List<String> loadAgrosystDomainIds(Connection connection) throws SQLException {
        List<String> domainIds = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery("SELECT topiaid FROM domain")) {

            while (rs.next()) {
                domainIds.add(rs.getString("topiaid"));
            }

        }
        return domainIds;
    }

    protected String getActionJoinOnPart(InputType inputType) {
        String actionJoinOnPart = "";
        switch (inputType) {
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> actionJoinOnPart = INNER_JOIN_ABSTRACTACTION_AA_ON_AA + " ai.mineralfertilizersspreadingaction ";
            case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES -> actionJoinOnPart = INNER_JOIN_ABSTRACTACTION_AA_ON_AA + " ai.pesticidesspreadingaction ";
            case AUTRE -> actionJoinOnPart = """
                   INNER JOIN abstractaction aa ON (
                                   ai.seedingaction = aa.topiaid
                                   OR ai.biologicalcontrolaction = aa.topiaid
                                   OR ai.organicfertilizersspreadingaction = aa.topiaid
                                   OR ai.mineralfertilizersspreadingaction = aa.topiaid
                                   OR ai.pesticidesspreadingaction = aa.topiaid
                                   OR ai.harvestingaction = aa.topiaid
                                   OR ai.irrigationaction = aa.topiaid
                                   OR ai.maintenancepruningvinesaction = aa.topiaid
                                   OR ai.otheraction = aa.topiaid)
                   """;
            case CARBURANT, IRRIGATION, PLAN_COMPAGNE, TRAITEMENT_SEMENCE -> actionJoinOnPart = "";
            case EPANDAGES_ORGANIQUES -> actionJoinOnPart = INNER_JOIN_ABSTRACTACTION_AA_ON_AA + " ai.organicfertilizersspreadingaction ";
            case LUTTE_BIOLOGIQUE -> actionJoinOnPart = INNER_JOIN_ABSTRACTACTION_AA_ON_AA + " ai.biologicalcontrolaction ";
            case POT, SUBSTRAT -> actionJoinOnPart = INNER_JOIN_ABSTRACTACTION_AA_ON_AA + " ai.otheraction ";
            case SEMIS -> actionJoinOnPart = INNER_JOIN_ABSTRACTACTION_AA_ON_AA + " ai.seedingaction ";
        }
        return actionJoinOnPart;
    }

    protected String getDomainInputCode(Map<String, String> productCodes, String inputKeyCode) {
        String inputCode = productCodes.get(inputKeyCode);
        if (inputCode == null) {
            inputCode = UUID.randomUUID().toString();
            productCodes.put(inputKeyCode, inputCode);
        }
        return inputCode;
    }

    protected String getInputPriceIdIfExists(
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            AbstractInputRow inputRow,
            String objectId) {

        InputPrice abstractInputPrice = priceByDomainIdObjectId.get(inputRow);
        String priceTopiaId = abstractInputPrice != null ? abstractInputPrice.getTopiaId() : null;
        return priceTopiaId;
    }

    protected InputPrice getInputPriceIfExists(
            Map<Pair<String, String>, List<InputPrice>> priceByDomainIdObjectId,
            AbstractInputRow inputRow,
            String objectId) {

        InputPrice abstractInputPrice = null;
        List<InputPrice> abstractInputPrices = priceByDomainIdObjectId.get(Pair.of(inputRow.domainId(), objectId));
        if (CollectionUtils.isNotEmpty(abstractInputPrices)) {
            // dans un premier temps on cherche un prix avec la même unité source que celle de l'intrant, ensuite si l'on ne trouve pas on prend le premier trouvé, à ce stade il ne doit pas y avoir de prix null
            final String productUnit = StringUtils.firstNonBlank(inputRow.otherproductinputunit, inputRow.mineralproductunit, inputRow.organicproductunit, inputRow.potinputunit, inputRow.substrateinputunit, inputRow.phytoproductunit, inputRow.mineralproductunit);
            abstractInputPrice = abstractInputPrices.stream().filter(p -> p.sourceUnit != null && p.sourceUnit.equalsIgnoreCase(productUnit)).findFirst().orElse(abstractInputPrices.stream().findAny().orElse(null));
            if (abstractInputPrice == null) {
                LOG.error("Price not found for key:" + (inputRow.domainId() + objectId));
            } else {
                abstractInputPrices.remove(abstractInputPrice);// ne pas utiliser 2 fois un même prix
            }
        }
        return abstractInputPrice;
    }

    protected boolean ifInputNotExistsOnDomain(String domainInputId) {
        return StringUtils.isEmpty(domainInputId);
    }

    @Override
    public void migrate(Context context) throws SQLException {

        Connection connection = context.getConnection();

        addOtherInputCommentColumn(connection);
        createActionIndexes(connection);
        createTemporaryTargetInputIndexe(connection);
        createDefaultRefActaTreatmentsProducts(connection);
        createDefaultRefFertiOrga(connection);
        createDefaultRefSubstrate(connection);
        createDefaultRefPot(connection);
        createDefaultRefOtherInput(connection);

        final int nbInputs = getNbInputToMigrate(connection);
        LOG.info(nbInputs + " records into abstractinput");

        List<String> domainIds = loadAgrosystDomainIds(connection);
        LOG.info(domainIds.size() + " domains found");
        final int nbDomainTotal = domainIds.size();

        String effectivePerennialInputsQuery = """
               SELECT d.topiaid AS domainid, d.code AS domaincode, d.campaign AS domaincampaign, ai.topiaid AS inputid, ai.topiadiscriminator AS inputdiscriminator, aa.topiaid AS actionid, aa.topiadiscriminator AS actiondiscriminator, ai.topiaversion, ai.topiacreatedate, null as practicedsystem, ai.qtmin, ai.qtavg, ai.qtmed, ai.qtmax, ai.productname, ai.inputtype, ai.producttype, ai.phytoproduct, ai.phytoproductunit, ai.oldproductqtunit, ai.otheraction, ai.n, ai.p2o5, ai.k2o, ai.organicproduct, ai.organicfertilizersspreadingaction, ai.organicproductunit, ai.mineralproduct, ai.mineralproductunit, ai.mineralfertilizersspreadingaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction, ai.phytoeffect, ai.otherproductinputunit, ai.substrate, ai.substrateinputunit, ai.pot, ai.potinputunit
               FROM abstractinput ai
               %s
               INNER JOIN effectiveintervention ei ON aa.effectiveintervention= ei.topiaid
               INNER JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
               INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid
               INNER JOIN croppingplanentry cpe ON cpe.topiaid = epcc.croppingplanentry
               INNER JOIN domain d ON cpe.domain = d.topiaid
               WHERE d.topiaid IN (%s)
               AND ai.inputtype = ?
               """;

        String effectiveSeasonalInputsQuery = """
               SELECT d.topiaid AS domainid, d.code AS domaincode, d.campaign AS domaincampaign, ai.topiaid AS inputid, ai.topiadiscriminator AS inputdiscriminator, aa.topiaid AS actionid, aa.topiadiscriminator AS actiondiscriminator, ai.topiaversion, ai.topiacreatedate, null as practicedsystem, ai.qtmin, ai.qtavg, ai.qtmed, ai.qtmax, ai.productname, ai.inputtype, ai.producttype, ai.phytoproduct, ai.phytoproductunit, ai.oldproductqtunit, ai.otheraction, ai.n, ai.p2o5, ai.k2o, ai.organicproduct, ai.organicfertilizersspreadingaction, ai.organicproductunit, ai.mineralproduct, ai.mineralproductunit, ai.mineralfertilizersspreadingaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction, ai.phytoeffect, ai.otherproductinputunit, ai.substrate, ai.substrateinputunit, ai.pot, ai.potinputunit
               FROM abstractinput ai
               %s
               INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
               INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
               INNER JOIN croppingplanentry cpe ON cpe.topiaid = eccn.croppingplanentry
               INNER JOIN domain d ON cpe.domain = d.topiaid
               WHERE d.topiaid IN (%s)
               AND ai.inputtype = ?
       """;

        String practicedPerennialInputsQuery = """
               SELECT d.topiaid AS domainid, d.code AS domaincode, d.campaign AS domaincampaign, ai.topiaid AS inputid, ai.topiadiscriminator AS inputdiscriminator, aa.topiaid AS actionid, aa.topiadiscriminator AS actiondiscriminator, ai.topiaversion, ai.topiacreatedate, ps.topiaid as practicedsystem, ai.qtmin, ai.qtavg, ai.qtmed, ai.qtmax, ai.productname, ai.inputtype, ai.producttype, ai.phytoproduct, ai.phytoproductunit, ai.oldproductqtunit, ai.otheraction, ai.n, ai.p2o5, ai.k2o, ai.organicproduct, ai.organicfertilizersspreadingaction, ai.organicproductunit, ai.mineralproduct, ai.mineralproductunit, ai.mineralfertilizersspreadingaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction, ai.phytoeffect, ai.otherproductinputunit, ai.substrate, ai.substrateinputunit, ai.pot, ai.potinputunit
               FROM abstractinput ai
               %s
               INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
               INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
               INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
               INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
               INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
               INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
               INNER JOIN domain d ON gp.domain = d.topiaid
               WHERE d.topiaid IN (%s)
               AND ai.inputtype = ?
       """;

        String practicedSeasonalInputsQuery = """
               SELECT d.topiaid AS domainid, d.code AS domaincode, d.campaign AS domaincampaign, d.campaign AS domaincampaign, ai.topiaid AS inputid, ai.topiadiscriminator AS inputdiscriminator, aa.topiaid AS actionid, aa.topiadiscriminator AS actiondiscriminator, ai.topiaversion, ai.topiacreatedate, ps.topiaid as practicedsystem, ai.qtmin, ai.qtavg, ai.qtmed, ai.qtmax, ai.productname, ai.inputtype, ai.producttype, ai.phytoproduct, ai.phytoproductunit, ai.oldproductqtunit, ai.otheraction, ai.n, ai.p2o5, ai.k2o, ai.organicproduct, ai.organicfertilizersspreadingaction, ai.organicproductunit, ai.mineralproduct, ai.mineralproductunit, ai.mineralfertilizersspreadingaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction, ai.phytoeffect, ai.otherproductinputunit, ai.substrate, ai.substrateinputunit, ai.pot, ai.potinputunit
               FROM abstractinput ai
               %s
               INNER JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
               INNER JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
               INNER JOIN practicedcropcyclenode pccn ON pccc.target = pccn.topiaid
               INNER JOIN practicedseasonalcropcycle pscc ON pccn.practicedseasonalcropcycle = pscc.topiaid
               INNER JOIN practicedcropcycle pcc ON pscc.topiaid = pcc.topiaid
               INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
               INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
               INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
               INNER JOIN domain d ON gp.domain = d.topiaid
               WHERE d.topiaid IN (%s)
               AND ai.inputtype = ?
       """;

        Map<String, String> inputQueries = Maps.newHashMap();
        inputQueries.put("Effective Perennial Inputs", effectivePerennialInputsQuery);
        inputQueries.put("Effective Seasonal Inputs", effectiveSeasonalInputsQuery);
        inputQueries.put("Practiced Perennial Inputs", practicedPerennialInputsQuery);
        inputQueries.put("Practiced Seasonal Inputs", practicedSeasonalInputsQuery);

        AtomicInteger currentNbInput = new AtomicInteger();
        AtomicInteger nbDomainPartDone = new AtomicInteger();
        AtomicInteger nbDomainDone = new AtomicInteger();


        // Prepare database removing invalid values and adding necessaries missing crop and species
        java.sql.Date now = new java.sql.Date(new Date().getTime());
        {
            String deleteSeedingActionSpeciesSpeciesNotExists = """
                       DELETE FROM seedingactionspecies sas where not exists (select 1 from croppingplanspecies cps WHERE cps.code = sas.speciescode)""";

            //  On vérifie que les "seedingactionspecies" portent bien sur des espèces existantes et correspondant à la bonne culture de l'ITK sont valides
            String createValidPracticedSeedingActionSpeciesTable = """
                   CREATE TABLE tmp_validpracticedseedingactionspecies AS(
                       SELECT sas.topiaid FROM seedingactionspecies sas
                       INNER JOIN abstractaction aa ON sas.seedingaction = aa.topiaid
                       INNER JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
                       INNER JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
                       INNER JOIN practicedcropcyclenode pccn ON pccc.target = pccn.topiaid
                       INNER JOIN croppingplanentry cpe ON cpe.code = pccn.croppingplanentrycode
                       INNER JOIN croppingplanspecies cps ON cps.croppingplanentry = cpe.topiaid
                       WHERE pi.intermediatecrop IS false
                       AND sas.speciescode = cps.code
                       UNION
                       SELECT sas.topiaid FROM seedingactionspecies sas
                       INNER JOIN abstractaction aa ON sas.seedingaction = aa.topiaid
                       INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
                       INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
                       INNER JOIN practicedperennialcropcycle ppcc ON ppcc.topiaid = pccp.practicedperennialcropcycle
                       INNER JOIN croppingplanentry cpe ON cpe.code = ppcc.croppingplanentrycode
                       INNER JOIN croppingplanspecies cps ON cps.croppingplanentry = cpe.topiaid
                       WHERE pi.intermediatecrop IS false
                       AND sas.speciescode = cps.code
                       UNION
                       SELECT sas.topiaid FROM seedingactionspecies sas
                       INNER JOIN abstractaction aa ON sas.seedingaction = aa.topiaid
                       INNER JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
                       INNER JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
                       INNER JOIN croppingplanentry cpe ON cpe.code = pccc.intermediatecroppingplanentrycode
                       INNER JOIN croppingplanspecies cps ON cps.croppingplanentry = cpe.topiaid
                       WHERE pi.intermediatecrop IS true
                       AND sas.speciescode = cps.code
                       UNION
                       SELECT sas.topiaid FROM abstractaction aa
                       INNER JOIN seedingactionspecies sas ON sas.seedingaction = aa.topiaid
                       INNER JOIN effectiveintervention ei ON aa.effectiveintervention = ei.topiaid
                       INNER JOIN effectivespeciesstade ess ON ess.effectiveintervention = ei.topiaid
                       INNER JOIN croppingplanspecies cps ON ess.croppingplanspecies = cps.topiaid
                             AND cps.code = sas.speciescode
                       )""";

            String createValidPracticedSeedingActionSpeciesIndex = """
                       CREATE INDEX vsas_id_idx on tmp_validpracticedseedingactionspecies(topiaid)""";


            String deleteBadSeedingActionSpecies = """
                       DELETE FROM seedingactionspecies sas where not exists (select 1 from tmp_validpracticedseedingactionspecies vsas where vsas.topiaid = sas.topiaid)""";

            String deleteBadPracticedSpeciesStades = """
                       DELETE FROM practicedspeciesstade pss where not exists (select 1 from croppingplanspecies cps WHERE cps.code = pss.speciescode)""";

            String createMissingPracticedCropAndSpeciesTable = """
                       CREATE TABLE tmp_missing_practiced_crop_and_species AS
                           SELECT res.croppingplanentrycode, res.cpe_count, res.species_code, res.cps_count, res.domain_id, res.domain_code from (
                               SELECT
                                 ppcc.croppingplanentrycode AS croppingplanentrycode,
                                 sas.speciescode as species_code,
                                 (select count(*) FROM croppingplanentry cpe WHERE cpe.domain = d.topiaid and cpe.code = ppcc.croppingplanentrycode) AS cpe_count,
                                 (select count(*) FROM croppingplanspecies cps INNER JOIN croppingplanentry cpe ON (cpe.topiaid = cps.croppingplanentry AND cpe.code = ppcc.croppingplanentrycode AND cpe.domain = d.topiaid) WHERE cps.code = sas.speciescode) AS cps_count,
                                 d.topiaid as domain_id,
                                 d.code as domain_code
                               FROM abstractaction aa
                               INNER JOIN seedingactionspecies sas ON sas.seedingaction = aa.topiaid
                               INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
                               INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
                               INNER JOIN practicedperennialcropcycle ppcc ON ppcc.topiaid = pccp.practicedperennialcropcycle
                               INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
                               INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
                               INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
                               INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
                               INNER JOIN domain d ON gp.domain = d.topiaid
                               
                               UNION ALL
                               
                               SELECT
                                 pccc.intermediatecroppingplanentrycode AS croppingplanentrycode,
                                 sas.speciescode AS species_code,
                                 (select count(*) FROM croppingplanentry cpe WHERE cpe.domain = d.topiaid and cpe.code = pccc.intermediatecroppingplanentrycode) AS cpe_count,
                                 (select count(*) FROM croppingplanspecies cps INNER JOIN croppingplanentry cpe ON (cpe.topiaid = cps.croppingplanentry AND cpe.code = pccc.intermediatecroppingplanentrycode AND cpe.domain = d.topiaid) WHERE cps.code = sas.speciescode) AS cps_count,
                                 d.topiaid as domain_id,
                                 d.code as domain_code
                               FROM abstractaction aa
                               INNER JOIN seedingactionspecies sas ON sas.seedingaction = aa.topiaid
                               INNER JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
                               INNER JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
                               INNER JOIN practicedcropcyclenode pccn ON pccc.target = pccn.topiaid
                               INNER JOIN practicedseasonalcropcycle pscc ON pccn.practicedseasonalcropcycle = pscc.topiaid
                               INNER JOIN practicedcropcycle pcc ON pscc.topiaid = pcc.topiaid
                               INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
                               INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
                               INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
                               INNER JOIN domain d ON gp.domain = d.topiaid
                               WHERE pi.intermediatecrop IS TRUE
                               
                               UNION ALL
                               
                               SELECT
                                 pccn.croppingplanentrycode AS croppingplanentrycode,
                                 sas.speciescode AS species_code,
                                 (select count(*) FROM croppingplanentry cpe WHERE cpe.domain = d.topiaid and cpe.code = pccn.croppingplanentrycode) AS cpe_count,
                                 (select count(*) FROM croppingplanspecies cps INNER JOIN croppingplanentry cpe ON (cpe.topiaid = cps.croppingplanentry AND cpe.code = pccn.croppingplanentrycode AND cpe.domain = d.topiaid) WHERE cps.code = sas.speciescode) AS cps_count,
                                 d.topiaid as domain_id,
                                 d.code as domain_code
                               FROM abstractaction aa
                               INNER JOIN seedingactionspecies sas ON sas.seedingaction = aa.topiaid
                               INNER JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
                               INNER JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
                               INNER JOIN practicedcropcyclenode pccn ON pccc.target = pccn.topiaid
                               INNER JOIN practicedseasonalcropcycle pscc ON pccn.practicedseasonalcropcycle = pscc.topiaid
                               INNER JOIN practicedcropcycle pcc ON pscc.topiaid = pcc.topiaid
                               INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
                               INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
                               INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
                               INNER JOIN domain d ON gp.domain = d.topiaid
                               WHERE pi.intermediatecrop IS false
                               ) AS res
                           WHERE res.cpe_count = 0 OR res.cps_count = 0
                           GROUP BY res.croppingplanentrycode, res.species_code, res.domain_id, res.domain_code, res.cpe_count, res.cps_count""";

            String retainExistingCropFromMissingPracticedCropAndSpeciesTable = """
                       DELETE FROM tmp_missing_practiced_crop_and_species mcpe WHERE not exists (select 1 from croppingplanentry cpe WHERE mcpe.croppingplanentrycode = cpe.code)""";

            String retainSpeciesCropFromMissingPracticedCropAndSpeciesTable = """
                       DELETE FROM tmp_missing_practiced_crop_and_species mcps WHERE not exists (select 1 from croppingplanspecies cps WHERE mcps.species_code = cps.code)""";

            String fixMixSpeciesCroppingPlanEntryStatusRows = """
                       UPDATE croppingplanentry SET mixspecies = false WHERE type = 'INTERMEDIATE'""";

            String fixMixVarietyCroppingPlanEntryStatusRows = """
                       UPDATE croppingplanentry SET mixvariety = false WHERE type = 'INTERMEDIATE'""";

            String backupSeedingActionRowsQuery = """
                   CREATE TABLE tmp_seedingaction AS (SELECT * FROM abstractaction WHERE topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.SeedingActionImpl')
                   """;

            String createTopiaIdIndexOnSeedingActionQuery = """
                   CREATE INDEX seedingactionIdx ON tmp_seedingaction (topiaid)
                   """;
            String createPracticedInterventionIndexOnSeedingActionQuery = """
                   CREATE INDEX seedingactionPracticedInterventionIdx ON tmp_seedingaction (practicedintervention)
                   """;

            String createEffectiveInterventionIndexOnSeedingActionQuery = """
                   CREATE INDEX seedingactionEffectiveInterventionIdx ON tmp_seedingaction (effectiveintervention)
                   """;

            String selectCropQuery = """
                    SELECT topiaid,topiaversion,code,domain,topiacreatedate,name,sellingprice,validated,type,yealdaverage,averageift,yealdunit,estimatingiftrules,iftseedstype,dosetype,biocontrolift,temporarymeadow,pasturedmeadow,mowedmeadow,mixspecies,mixvariety FROM croppingplanentry where code IN (%s)""";

            String selectCpsQuery = "SELECT topiaid,topiaversion,topiacreatedate,code,validated,variety,croppingplanentry,species,croppingplanentry_idx,speciesarea,edaplosunknownvariety,compagne,destination FROM croppingplanspecies WHERE code in (%s)";

            LOG.info("Add Missing crop to domain");
            try (Statement statement = connection.createStatement()) {
                statement.addBatch(deleteSeedingActionSpeciesSpeciesNotExists);
                statement.addBatch(createValidPracticedSeedingActionSpeciesTable);
                statement.addBatch(createValidPracticedSeedingActionSpeciesIndex);
                statement.addBatch(deleteBadSeedingActionSpecies);
                statement.addBatch(deleteBadPracticedSpeciesStades);
                statement.addBatch(createMissingPracticedCropAndSpeciesTable);
                statement.addBatch(retainExistingCropFromMissingPracticedCropAndSpeciesTable);
                statement.addBatch(retainSpeciesCropFromMissingPracticedCropAndSpeciesTable);

                statement.executeBatch();
                ResultSet resultSet = statement.executeQuery("select croppingplanentrycode, species_code, cpe_count, cps_count, domain_id, domain_code from tmp_missing_practiced_crop_and_species");
                Set<Pair<String, String>> missingCpeCodeByDomainIds = new HashSet<>();
                Map<Pair<String, String>, List<String>> missingCpsByDomainIdCpeCodes = new HashMap<>();

                List<TmpMissingPracticedCropAndSpecies> allMissingPracticedCropAndSpecies = new ArrayList<>();
                while (resultSet.next()) {
                    String croppingPlanEntryCode = resultSet.getString("croppingplanentrycode");
                    String speciesCode = resultSet.getString("species_code");
                    String domainId = resultSet.getString("domain_id");
                    int cpeCount = resultSet.getInt("cpe_count");
                    int cpsCount = resultSet.getInt("cps_count");
                    TmpMissingPracticedCropAndSpecies tmpMissingPracticedCropAndSpecies = new TmpMissingPracticedCropAndSpecies(
                            croppingPlanEntryCode,
                            speciesCode,
                            cpeCount,
                            cpsCount,
                            domainId,
                            resultSet.getString("domain_code")
                    );
                    allMissingPracticedCropAndSpecies.add(tmpMissingPracticedCropAndSpecies);
                    Pair<String, String> domainIdCropCode = Pair.of(domainId, croppingPlanEntryCode);

                    if (cpeCount == 0) {
                        missingCpeCodeByDomainIds.add(domainIdCropCode);
                    }
                    if (cpsCount == 0) {
                        //missingCpsCodeByCropCodes.add(Pair.of(croppingPlanEntryCode, speciesCode));
                        List<String> speciesCodes = missingCpsByDomainIdCpeCodes.computeIfAbsent(domainIdCropCode, k -> new ArrayList<>());
                        speciesCodes.add(speciesCode);
                    }

                }
                Set<String> missingCropCodes = missingCpeCodeByDomainIds.stream().map(Pair::getRight).collect(Collectors.toSet());


                Map<String, NvCroppingPlanEntry> missingCropsByCropCodes = loadMissingCropsAndRelativeSpeciesByCropCodes(selectCropQuery, statement, missingCropCodes);

                // ajout des cultures manquantes
                Map<TmpMissingPracticedCropAndSpecies, NvCroppingPlanEntry> newCropsForDomains = new HashMap<>();
                Binder<NvCroppingPlanEntry, NvCroppingPlanEntry> nvCroppingPlanEntryBinder = BinderFactory.newBinder(NvCroppingPlanEntry.class);
                Binder<NvCroppingPlanSpecies, NvCroppingPlanSpecies> nvCroppingPlanSpeciesBinder = BinderFactory.newBinder(NvCroppingPlanSpecies.class);
                Map<Pair<String, String>, NvCroppingPlanEntry> newCropByDomainIdCropCodes = new HashMap<>();
                for (TmpMissingPracticedCropAndSpecies tmpMissingPracticedCropRow : allMissingPracticedCropAndSpecies.stream().filter(m -> (m.cpeCount()) == 0).toList()) {
                    String croppingPlanEntryCode = tmpMissingPracticedCropRow.croppingPlanEntryCode;
                    String domainId = tmpMissingPracticedCropRow.domainId;

                    NvCroppingPlanEntry cropModel = missingCropsByCropCodes.get(croppingPlanEntryCode);
                    if (cropModel != null) {
                        NvCroppingPlanEntry nvCpe;
                        NvCroppingPlanEntry nvCroppingPlanEntry = newCropByDomainIdCropCodes.get(Pair.of(domainId, croppingPlanEntryCode));
                        if (nvCroppingPlanEntry != null) {
                            nvCpe = nvCroppingPlanEntry;
                        } else {
                            nvCpe = new NvCroppingPlanEntry(domainId);
                            nvCroppingPlanEntryBinder.copyExcluding(cropModel, nvCpe,
                                    "croppingPlanSpecies",
                                    "domainId",
                                    CroppingPlanEntry.PROPERTY_TOPIA_ID,
                                    CroppingPlanEntry.PROPERTY_DOMAIN);
                            newCropByDomainIdCropCodes.put(Pair.of(domainId, croppingPlanEntryCode), nvCpe);
                            // copie des espèces
                            List<NvCroppingPlanSpecies> nvCroppingPlanSpeciesModels = cropModel.getNvCroppingPlanSpecies();
                            for (NvCroppingPlanSpecies croppingPlanSpeciesModel : nvCroppingPlanSpeciesModels) {
                                NvCroppingPlanSpecies nvCps = new NvCroppingPlanSpecies();
                                nvCroppingPlanSpeciesBinder.copyExcluding(
                                        croppingPlanSpeciesModel,
                                        nvCps,
                                        "cropId");
                                nvCpe.addCroppingPlanSpecies(nvCps);
                            }
                        }
                        newCropsForDomains.put(tmpMissingPracticedCropRow, nvCpe);
                    }
                }

                // Chargement des espèces manquantes
                Map<String, Set<NvCroppingPlanSpecies>> speciesToAddToExistingCrop = new HashMap<>();
                Map<String, NvCroppingPlanSpecies> croppingPlanSpeciesModelByCode = new HashMap<>();
                // "SELECT topiaid,topiaversion,topiacreatedate,code,validated,variety,croppingplanentry,species,croppingplanentry_idx,speciesarea,edaplosunknownvariety,compagne,destination FROM croppingplanspecies WHERE code in (%s)";
                Set<String> missingSpeciesCodes = missingCpsByDomainIdCpeCodes.values().stream().flatMap(Collection::stream).collect(Collectors.toSet());
                String loadMissingSpeciesQuery = String.format(selectCpsQuery, formatInValues(StringUtils.join(missingSpeciesCodes, "','")));// missing species from existing crop for campaign
                ResultSet cpsRS = statement.executeQuery(loadMissingSpeciesQuery);
                while (cpsRS.next()) {
                    NvCroppingPlanSpecies croppingPlanSpecies = new NvCroppingPlanSpecies();
                    croppingPlanSpecies.setTopiaId(cpsRS.getString("topiaid"));
                    croppingPlanSpecies.setTopiaVersion(0L);
                    croppingPlanSpecies.setCode(cpsRS.getString("code"));
                    croppingPlanSpecies.setValidated(cpsRS.getBoolean("validated"));
                    croppingPlanSpecies.setVarietyId(cpsRS.getString("variety"));
                    croppingPlanSpecies.setSpeciesId(cpsRS.getString("species"));
                    croppingPlanSpecies.setCroppingplanentry_idx(cpsRS.getDouble("croppingplanentry_idx"));
                    croppingPlanSpecies.setSpeciesArea(cpsRS.getInt("speciesarea"));
                    croppingPlanSpecies.setEdaplosUnknownVariety(cpsRS.getString("edaplosunknownvariety"));
                    String compagneName = cpsRS.getString("compagne");
                    if (compagneName != null) {
                        croppingPlanSpecies.setCompagne(CompagneType.valueOf(compagneName));
                    }
                    croppingPlanSpecies.setDestinationId(cpsRS.getString("destination"));

                    croppingPlanSpeciesModelByCode.put(croppingPlanSpecies.getCode(), croppingPlanSpecies);
                }

                // requête permettant de faire la correspondance entre les nouvelles espèces et la culture via son code et le domain
                List<TmpMissingPracticedCropAndSpecies> missingSpeciesWithExistingCropRow = allMissingPracticedCropAndSpecies.stream().filter(m -> (m.cpeCount()) > 0).toList();
                Set<String> existingCropCodes = missingSpeciesWithExistingCropRow.stream().map(TmpMissingPracticedCropAndSpecies::croppingPlanEntryCode).filter(Objects::nonNull).collect(Collectors.toSet());
                ResultSet resultSet1 = statement.executeQuery("SELECT topiaid, code, domain from croppingplanentry WHERE code IN (" + formatInValues(StringUtils.join(existingCropCodes, "','")) + ")");
                Map<Pair<String,String>, String> cropIdByDomainIdCropCode = new HashMap<>();
                while (resultSet1.next()) {
                    String cropTopiaid = resultSet1.getString("topiaid");
                    String code = resultSet1.getString("code");
                    String domain = resultSet1.getString("domain");
                    cropIdByDomainIdCropCode.put(Pair.of(domain, code), cropTopiaid);
                }

                // affectation des id cultures aux nouvelles espèces crées
                for (TmpMissingPracticedCropAndSpecies missingSpeciesRow : allMissingPracticedCropAndSpecies.stream().filter(row -> (row.cpsCount()) == 0).toList()) {
                    String domainId = missingSpeciesRow.domainId;
                    String croppingPlanEntryCode = missingSpeciesRow.croppingPlanEntryCode;
                    List<String> cpeSpeciesCodes = missingCpsByDomainIdCpeCodes.get(Pair.of(domainId, croppingPlanEntryCode));
                    for (String missingSpeciesCode : cpeSpeciesCodes) {

                        // try find a species with same code to create a new one for the current crop
                        NvCroppingPlanSpecies croppingPlanSpeciesModel = croppingPlanSpeciesModelByCode.get(missingSpeciesCode);
                        if (croppingPlanSpeciesModel != null) {

                            NvCroppingPlanEntry newCrop = newCropsForDomains.get(missingSpeciesRow);

                            NvCroppingPlanSpecies nvCps = new NvCroppingPlanSpecies();
                            nvCroppingPlanSpeciesBinder.copyExcluding(croppingPlanSpeciesModel, nvCps, "cropId");

                            if (newCrop != null) {
                                // in case the species is to add to new crop
                                if (newCrop.getNotOrderedNvCroppingPlanSpecies().stream().map(NvCroppingPlanSpecies::getCode).noneMatch(c -> c.equals(nvCps.getCode()))) {
                                    newCrop.addCroppingPlanSpecies(nvCps);// crop id it setup inside
                                }
                            } else {
                                // in case crop exists
                                String cropId = cropIdByDomainIdCropCode.get(Pair.of(domainId, croppingPlanEntryCode));
                                if (cropId != null) {
                                    Set<NvCroppingPlanSpecies> nvCroppingPlanSpecies = speciesToAddToExistingCrop.computeIfAbsent(cropId, k -> new HashSet<>());
                                    nvCps.setCropId(cropId);
                                    nvCroppingPlanSpecies.add(nvCps);
                                } else {
                                    LOG.error("species can't be added to existing crop, crop not found for domain: " + domainId + " crop code: " + croppingPlanEntryCode);
                                }

                            }
                        } else {
                            LOG.error("No species found for code " + missingSpeciesCode + " for domain " + domainId + " and crop code:" + croppingPlanEntryCode);
                        }
                    }
                }

                // corrige l'index des espèces
                if (!speciesToAddToExistingCrop.isEmpty()) {
                    Map<String, Integer> maxIndexForCropIds = new HashMap<>();
                    Set<String> cropIds = speciesToAddToExistingCrop.keySet();
                    ResultSet resultSet2 = statement.executeQuery("select cps.croppingplanentry, max(cps.croppingplanentry_idx) as maxidx from croppingplanspecies cps group by cps.croppingplanentry");
                    while (resultSet2.next()) {
                        maxIndexForCropIds.put(resultSet2.getString(1), resultSet2.getInt(2));
                    }
                    for (Map.Entry<String, Set<NvCroppingPlanSpecies>> speciesToAddByCropIds : speciesToAddToExistingCrop.entrySet()) {
                        Integer lastIndex = maxIndexForCropIds.get(speciesToAddByCropIds.getKey());
                        if (lastIndex == null) {// crop did not have species
                            lastIndex = 0;
                        }
                        Set<NvCroppingPlanSpecies> speciesToAddToCrop = speciesToAddByCropIds.getValue();
                        for (NvCroppingPlanSpecies croppingPlanSpecies : speciesToAddToCrop) {
                            croppingPlanSpecies.setCroppingplanentry_idx(lastIndex++);
                        }
                    }
                }

                // save new species
                try (PreparedStatement insertSpeciesStatement = connection.prepareStatement(
                        //           1,         2,            3,          4,      5,       6,         7,            8,           9,                 10,           11,              12,        13
                        "INSERT INTO croppingplanspecies " +
                                " (topiaid,topiaversion,topiacreatedate,code,validated,variety,croppingplanentry,species,croppingplanentry_idx,speciesarea,edaplosunknownvariety,compagne,destination)" +
                                " values (?,?,?,?,?,?,?,?,?,?,?,?,?)")) {

                    // save new crop
                    try (PreparedStatement insertCropStatement = connection.prepareStatement(
                            //           1,           2       ,3,   4,         5,         6 ,      7 ,        8,       9,       10,        11,        12,        13,               14,        15,        16,             17,           18,          19,          20,        21
                            "INSERT INTO croppingplanentry " +
                                    "(topiaid,topiaversion,code,domain,topiacreatedate,name,sellingprice,validated,type,yealdaverage,averageift,yealdunit,estimatingiftrules,iftseedstype,dosetype,biocontrolift,temporarymeadow,pasturedmeadow,mowedmeadow,mixspecies,mixvariety) " +
                                    " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")) {

                        Set<Pair<String, String>> cropCodeByDomain = new HashSet<>();
                        Set<Pair<String, String>> speciesCodeByCropId = new HashSet<>();
                        for (NvCroppingPlanEntry newCropsForDomain : newCropsForDomains.values()) {
                            String domainId = newCropsForDomain.getDomainId();
                            String code = newCropsForDomain.getCode();

                            Pair<String, String> domainIdCropCode = Pair.of(domainId, code);
                            if (!cropCodeByDomain.contains(domainIdCropCode)) {
                                cropCodeByDomain.add(domainIdCropCode);

                                String newCropTopiaId = newCropsForDomain.getNewTopiaId();
                                insertCropStatement.setString(1, newCropTopiaId);
                                insertCropStatement.setLong(2, 0L);
                                insertCropStatement.setString(3, code);
                                insertCropStatement.setString(4, domainId);
                                insertCropStatement.setDate(5, scriptDate);
                                insertCropStatement.setString(6, newCropsForDomain.getName());
                                insertCropStatement.setNull(7, Types.DOUBLE);
                                insertCropStatement.setBoolean(8, false);
                                insertCropStatement.setString(9, newCropsForDomain.getType().name());
                                setSqlTypeOrNull(10, newCropsForDomain.getYealdAverage(), Types.DOUBLE, insertCropStatement);
                                setSqlTypeOrNull(11, newCropsForDomain.getAverageIFT(), Types.DOUBLE, insertCropStatement);
                                setSqlTypeOrNull(12, newCropsForDomain.getYealdUnit(), Types.VARCHAR, insertCropStatement);
                                setSqlTypeOrNull(13, newCropsForDomain.getEstimatingIftRules(), Types.VARCHAR, insertCropStatement);
                                setSqlTypeOrNull(14, newCropsForDomain.getIftSeedsType(), Types.VARCHAR, insertCropStatement);
                                setSqlTypeOrNull(15, newCropsForDomain.getDoseType(), Types.VARCHAR, insertCropStatement);
                                setSqlTypeOrNull(16, newCropsForDomain.getBiocontrolIFT(), Types.DOUBLE, insertCropStatement);
                                setSqlTypeOrNull(17, newCropsForDomain.getTemporaryMeadow(), Types.DOUBLE, insertCropStatement);
                                insertCropStatement.setBoolean(18, newCropsForDomain.isPasturedMeadow());
                                insertCropStatement.setBoolean(19, newCropsForDomain.isMowedMeadow());
                                insertCropStatement.setBoolean(20, newCropsForDomain.isMixSpecies());
                                insertCropStatement.setBoolean(21, newCropsForDomain.isMixVariety());
                                insertCropStatement.addBatch();

                                List<NvCroppingPlanSpecies> nvCroppingPlanSpecies = newCropsForDomain.getNvCroppingPlanSpecies();
                                for (NvCroppingPlanSpecies croppingPlanSpecies : nvCroppingPlanSpecies) {
                                    String cpsCode = croppingPlanSpecies.getCode();
                                    insertSpeciesStatement.setString(1, croppingPlanSpecies.getNewTopiaId());
                                    insertSpeciesStatement.setLong(2, 0L);
                                    insertSpeciesStatement.setDate(3, scriptDate);
                                    insertSpeciesStatement.setString(4, cpsCode);
                                    insertSpeciesStatement.setBoolean(5, false);
                                    setSqlTypeOrNull(6, croppingPlanSpecies.getVarietyId(), Types.VARCHAR, insertSpeciesStatement);
                                    insertSpeciesStatement.setString(7, newCropTopiaId);
                                    insertSpeciesStatement.setString(8, croppingPlanSpecies.getSpeciesId());
                                    insertSpeciesStatement.setDouble(9, croppingPlanSpecies.getCroppingplanentry_idx());
                                    insertSpeciesStatement.setInt(10, croppingPlanSpecies.getSpeciesArea());
                                    setSqlTypeOrNull(11, croppingPlanSpecies.getEdaplosUnknownVariety(), Types.VARCHAR, insertSpeciesStatement);
                                    setSqlTypeOrNull(12, croppingPlanSpecies.getCompagne(), Types.VARCHAR, insertSpeciesStatement);
                                    setSqlTypeOrNull(13, croppingPlanSpecies.getDestinationId(), Types.VARCHAR, insertSpeciesStatement);
                                    insertSpeciesStatement.addBatch();

                                }
                            }
                        }

                        Set<NvCroppingPlanSpecies> newSpeciesToAddToExistingCrops = speciesToAddToExistingCrop.values().stream().flatMap(Collection::stream).collect(Collectors.toSet());
                        for (NvCroppingPlanSpecies croppingPlanSpecies : newSpeciesToAddToExistingCrops) {
                            String cropId = croppingPlanSpecies.getCropId();

                            insertSpeciesStatement.setString(1, croppingPlanSpecies.getNewTopiaId());
                            insertSpeciesStatement.setLong(2, 0L);
                            insertSpeciesStatement.setDate(3, scriptDate);
                            insertSpeciesStatement.setString(4, croppingPlanSpecies.getCode());
                            insertSpeciesStatement.setBoolean(5, false);
                            setSqlTypeOrNull(6, croppingPlanSpecies.getVarietyId(), Types.VARCHAR, insertSpeciesStatement);
                            insertSpeciesStatement.setString(7, cropId);
                            insertSpeciesStatement.setString(8, croppingPlanSpecies.getSpeciesId());
                            insertSpeciesStatement.setDouble(9, croppingPlanSpecies.getCroppingplanentry_idx());
                            insertSpeciesStatement.setInt(10, croppingPlanSpecies.getSpeciesArea());
                            setSqlTypeOrNull(11, croppingPlanSpecies.getEdaplosUnknownVariety(), Types.VARCHAR, insertSpeciesStatement);
                            setSqlTypeOrNull(12, croppingPlanSpecies.getCompagne(), Types.VARCHAR, insertSpeciesStatement);
                            setSqlTypeOrNull(13, croppingPlanSpecies.getDestinationId(), Types.VARCHAR, insertSpeciesStatement);
                            insertSpeciesStatement.addBatch();
                        }
                        insertCropStatement.executeBatch();
                        insertSpeciesStatement.executeBatch();

                        LOG.info(newCropsForDomains.values().size() + " Cultures Auto ajoutées");
                        LOG.info(newSpeciesToAddToExistingCrops.size() + " Espèces Auto ajoutées");
                    }
                }

                statement.addBatch(fixMixSpeciesCroppingPlanEntryStatusRows);
                statement.addBatch(fixMixVarietyCroppingPlanEntryStatusRows);
                statement.addBatch(backupSeedingActionRowsQuery);
                statement.addBatch(createTopiaIdIndexOnSeedingActionQuery);
                statement.addBatch(createPracticedInterventionIndexOnSeedingActionQuery);
                statement.addBatch(createEffectiveInterventionIndexOnSeedingActionQuery);
                statement.executeBatch();
                LOG.info("Remove contraints on tables abstractinput, seedingactionspecies, phytoproducttarget");
                statement.execute("ALTER TABLE abstractinput DROP CONSTRAINT IF EXISTS fk_5q9sjdjohh4x44ri8co9gt1vh"); // mineralfertilizersspreadingaction
                statement.execute("ALTER TABLE abstractinput DROP CONSTRAINT IF EXISTS fk_7o49kfk9gj8j5w8ngdcohfc30"); // maintenancepruningvinesaction
                statement.execute("ALTER TABLE abstractinput DROP CONSTRAINT IF EXISTS fk_8hrwd40n863dsqij0otnd1wbv"); // pesticidesspreadingaction
                statement.execute("ALTER TABLE abstractinput DROP CONSTRAINT IF EXISTS fk_9bx0734vwkb2pffutk93e3eoc"); // mineralproduct
                statement.execute("ALTER TABLE abstractinput DROP CONSTRAINT IF EXISTS fk_b9hmxoperi24errro6e8co4bw"); // biologicalcontrolaction
                statement.execute("ALTER TABLE abstractinput DROP CONSTRAINT IF EXISTS fk_dq90e4dhy88qk1c4rgebgrlgf"); // phytoproduct
                statement.execute("ALTER TABLE abstractinput DROP CONSTRAINT IF EXISTS fk_h0sslpvjvpx19k9by0hvksnk8"); // otheraction
                statement.execute("ALTER TABLE abstractinput DROP CONSTRAINT IF EXISTS fk_ikuu59fr9lvg3itocgn36rwhj"); // harvestingaction
                statement.execute("ALTER TABLE abstractinput DROP CONSTRAINT IF EXISTS fk_j18hh0y2nyxq6v7ssn99itvs3"); // organicfertilizersspreadingaction
                statement.execute("ALTER TABLE abstractinput DROP CONSTRAINT IF EXISTS fk_ljyxhp2ilx9raveem4ogy17le"); // seedingaction
                statement.execute("ALTER TABLE abstractinput DROP CONSTRAINT IF EXISTS fk_nxs91yj4ld8uceumlksu9eb55"); // organicproduct
                statement.execute("ALTER TABLE abstractinput DROP CONSTRAINT IF EXISTS fk_shjuknig3tfx2gudw41uxeqcj"); // irrigationaction
                statement.execute("ALTER TABLE abstractinput DROP CONSTRAINT IF EXISTS fkyxesef16ubmgsbmdh3ebecf8");   // substrate
                statement.execute("ALTER TABLE abstractinput DROP CONSTRAINT IF EXISTS fkyxesef16ubmgsbmdh3ebecf9");   // pot
                statement.execute("ALTER TABLE seedingactionspecies DROP CONSTRAINT IF EXISTS fk_jvpevjwtaqhnfsva02bututmc"); // seedingactionspecies
                statement.execute("ALTER TABLE phytoproducttarget DROP CONSTRAINT IF EXISTS fk_7o49kfk9gj8j5w8ngdcohfc31"); // phytoproducttarget
                statement.execute("DROP INDEX IF EXISTS phytoproducttarget_product_taget_idx;"); // phytoproducttarget
            }
        }

        Map<String, List<DbDomainSeedSpeciesInput>> speciesInputForProductByActionId = new HashMap<>();
        Map<String, List<DbSeedSpeciesInputUsage>> speciesUsageForProductByActionId = new HashMap<>();

        persistSeeLotAndInputs(
                connection,
                speciesInputForProductByActionId,
                speciesUsageForProductByActionId);


        // MIGRATE INPUTS
        Iterable<List<String>> domainIdParts = Iterables.partition(domainIds, CHUNCK_SIZE);
        for (List<String> domainIdsPart : domainIdParts) {

            int nbDomainPartDoneNb = nbDomainPartDone.addAndGet(1);
            int nbDomainDoneNb = nbDomainDone.addAndGet(domainIdsPart.size());
            int domainDonePercent = (nbDomainDoneNb * 100)/nbDomainTotal;

            for (InputType inputType : InputType.values()) {

                if (InputType.CARBURANT.equals(inputType) ||
                        InputType.IRRIGATION.equals(inputType) ||
                        InputType.PLAN_COMPAGNE.equals(inputType) ||
                        InputType.TRAITEMENT_SEMENCE.equals(inputType)) {
                    // done later
                    continue;
                }

                LOG.info(String.format("for input type %s: start load Inputs, domain: %d percent",inputType, domainDonePercent));

                Collection<AbstractInputRow> domainAbstractInputRows = getInputRowForGivenDomainsAndType(
                        connection,
                        nbInputs,
                        inputQueries,
                        currentNbInput,
                        domainIdsPart,
                        inputType);

                LOG.info(String.format("for input type %s: end load Inputs, domain: %d percent",inputType, domainDonePercent));

                migrateInputs(
                        domainAbstractInputRows,
                        connection,
                        inputType,
                        speciesInputForProductByActionId,
                        speciesUsageForProductByActionId,
                        domainDonePercent
                );

                LOG.info(String.format("for input type %s: %d/%d, NB domain done %d/%d %d percent",
                        inputType,
                        nbDomainPartDoneNb,
                        ((int)Math.ceil(nbDomainTotal / (double) CHUNCK_SIZE)),
                        nbDomainDoneNb,
                        nbDomainTotal,
                        domainDonePercent));
            }


        }

        // PERSIST FUEL PRICES AND INPUTS
        {
            Map<Pair<String, String>, InputPrice> priceByDomainId = persistFuelInputPrices(
                    connection
            );

            persistFuelInputs(
                    connection,
                    priceByDomainId,
                    FuelUnit.L_HA.name(),
                    InputType.CARBURANT.name()
            );
        }

        // PERSIST IRRIGATION PRICES AND INPUTS
        {

            // Pair.of(domainId, campaign) -> AbstractInputPrice
            Map<Pair<String, String>, InputPrice> priceByDomainIdObjectId = persistIrrigationInputPrices(connection);

            // domainCode -> domainId
            MultiValuedMap<String, String> domainCodeToDomainIds =  new HashSetValuedHashMap<>();

            Map<String, String> domainIdToPriceId = new HashMap<>();
            priceByDomainIdObjectId.values().forEach(aip -> {
                domainCodeToDomainIds.put(aip.getDomainCode(), aip.getDomainId());
                domainIdToPriceId.put(aip.getDomainId(), aip.getTopiaId());
            });

            // domainCode -> [IrrigationActionRow]
            MultiValuedMap<String, IrrigationActionRow> irrigationActionRowByDomainCodes = executeGetIrrigActionQueries(connection);
            irrigationActionRowByDomainCodes.values().forEach(iar -> domainCodeToDomainIds.put(iar.getDomainCode(), iar.getDomainId()));

            Map<String, DomainIrrigationRow> domainIrrigationRowsByDomainIds = new HashMap<>();
            //Set<DomainIrrigationRow> domainIrrigationRows = new HashSet<>();
            for (String domainCode : domainCodeToDomainIds.keys()) {
                Set<String> domainIdsForCode = Sets.newHashSet(domainCodeToDomainIds.get(domainCode));
                String irrigDomainCode = UUID.randomUUID().toString();
                for (String domainId : domainIdsForCode) {
                    if (domainIrrigationRowsByDomainIds.get(domainId) == null) {
                        DomainIrrigationRow domainIrrigationRow = new DomainIrrigationRow(
                                DomainIrrigationInput.class.getName() + "_" + UUID.randomUUID(),
                                IrrigationUnit.MM.name(),
                                "Eau",
                                InputType.IRRIGATION,
                                domainId,
                                domainIdToPriceId.get(domainId),
                                irrigDomainCode
                        );
                        domainIrrigationRowsByDomainIds.put(domainId, domainIrrigationRow);
                    }
                }

            }

            String unitName = IrrigationUnit.MM.name();
            AtomicInteger nbInputMigrated = new AtomicInteger();
            try (PreparedStatement persistDomainInputStatement = connection.prepareStatement(INSERT_INTO_DOMAIN_IRRIGATION_INPUT_QUERY)) {
                try (PreparedStatement persistAbstractDomainInputStatement = connection.prepareStatement(INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY)) {
                    int nbInputToCreate = priceByDomainIdObjectId.size();
                    for (DomainIrrigationRow priceRow : domainIrrigationRowsByDomainIds.values()) {
                        //                                              1        2               3           4       5        6          7        8         9
                        // INSERT INTO abstractdomaininputstockunit (topiaid, topiaversion, topiacreatedate, key, inputname, inputtype, domain, inputprice, code)
                        persistAbstractDomainInputStatement.setString(1, priceRow.topiaId);
                        persistAbstractDomainInputStatement.setLong(2, 0L);
                        persistAbstractDomainInputStatement.setDate(3, scriptDate);
                        persistAbstractDomainInputStatement.setString(4, unitName);
                        persistAbstractDomainInputStatement.setString(5, "Eau");
                        persistAbstractDomainInputStatement.setString(6, InputType.IRRIGATION.name());
                        persistAbstractDomainInputStatement.setString(7, priceRow.domainId);
                        persistAbstractDomainInputStatement.setString(8, priceRow.priceTopiaId);
                        persistAbstractDomainInputStatement.setString(9, priceRow.inputCode);
                        persistAbstractDomainInputStatement.addBatch();

                        //                                 1         2
                        // INSERT INTO domainfuelinput (topiaid, usageunit)
                        persistDomainInputStatement.setString(1, priceRow.topiaId);
                        persistDomainInputStatement.setString(2, unitName);
                        persistDomainInputStatement.addBatch();

                        nbInputMigrated.addAndGet(1);
                        LOG.info("For current part processed " + (nbInputMigrated.get()*100)/nbInputToCreate + "%  Migrate " + InputType.IRRIGATION.name());

                    }
                    LOG.info("For current part processed " + InputType.IRRIGATION.name() + " execute " + INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY +  " BATCH");
                    persistAbstractDomainInputStatement.executeBatch();

                }
                LOG.info("For current part processed " + InputType.IRRIGATION.name() + " execute " + INSERT_INTO_DOMAIN_IRRIGATION_INPUT_QUERY + " BATCH");
                persistDomainInputStatement.executeBatch();
                LOG.info("For current part processed " + InputType.IRRIGATION.name() + " END BATCH");

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

            try(PreparedStatement updateIrrigActionStatement = connection.prepareStatement(UPDATE_IRRIG_ACTION_QUERY)) {
                try(PreparedStatement insertIntoIrrigInputUsageStatement = connection.prepareStatement(INSERT_INTO_IRRIG_INPUT_USAGE_QUERY)) {
                    try (PreparedStatement persistAbstractInputUsageStatement = connection.prepareStatement(INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY)) {
                        Collection<IrrigationActionRow> irrigationActionRows = irrigationActionRowByDomainCodes.values();
                        for (IrrigationActionRow iar : irrigationActionRows) {
                            String domainId = iar.getDomainId();
                            DomainIrrigationRow domainIrrigRow = domainIrrigationRowsByDomainIds.get(domainId);

                            String irrigationInputUsageTopiaId = IrrigationInputUsage.class.getName() + "_" + UUID.randomUUID();

                            //                                    1                                       2         3           4     5       6     7        8       9
                            // INSERT INTO abstractinputusage (topiaid, topiaversion, topiacreatedate, comment, productname, qtmin, qtavg, qtmed, qtmax, inputtype, code)
                            persistAbstractInputUsageStatement.setString(1, irrigationInputUsageTopiaId);
                            persistAbstractInputUsageStatement.setString(2, "Eau");
                            persistAbstractInputUsageStatement.setString(3, "Eau");
                            Double qtmin = iar.getWaterquantitymin();
                            setSqlDoubleOrNull(4, qtmin, persistAbstractInputUsageStatement);
                            persistAbstractInputUsageStatement.setDouble(5, iar.getWaterquantityaverage());
                            Double qtmed = iar.getWaterquantitymedian();
                            setSqlDoubleOrNull(6, qtmed, persistAbstractInputUsageStatement);
                            Double qtmax = iar.getWaterquantitymax();
                            setSqlDoubleOrNull(7, qtmax, persistAbstractInputUsageStatement);
                            persistAbstractInputUsageStatement.setString(8, InputType.IRRIGATION.name());
                            persistAbstractInputUsageStatement.setString(9, UUID.randomUUID().toString());
                            persistAbstractInputUsageStatement.addBatch();

                            // INSERT INTO irrigationinputusage (topiaid, domainirrigationinput)
                            insertIntoIrrigInputUsageStatement.setString(1, irrigationInputUsageTopiaId);
                            insertIntoIrrigInputUsageStatement.setString(2, domainIrrigRow.topiaId);
                            insertIntoIrrigInputUsageStatement.addBatch();

                            // UPDATE abstractaction SET irrigationinputusage = ? WHERE topiaid = ?
                            updateIrrigActionStatement.setString(1, irrigationInputUsageTopiaId);
                            updateIrrigActionStatement.setString(2, iar.actionId);
                            updateIrrigActionStatement.addBatch();
                        }

                        LOG.info("For current part processed " + InputType.IRRIGATION.name() + " execute " + INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY + " BATCH");
                        persistAbstractInputUsageStatement.executeBatch();

                        LOG.info("For current part processed " + InputType.IRRIGATION.name() + " execute " + INSERT_INTO_IRRIG_INPUT_USAGE_QUERY + " BATCH");
                        insertIntoIrrigInputUsageStatement.executeBatch();

                        LOG.info("For current part processed " + InputType.IRRIGATION.name() + " execute " + UPDATE_IRRIG_ACTION_QUERY + " BATCH");
                        updateIrrigActionStatement.executeBatch();
                        LOG.info("For current part processed " + InputType.IRRIGATION.name() + " END BATCH");

                    }
                }
            }
        }

        dropTemporaryTargetInputIndexe(connection);

        LOG.info("END OF SCRIPT");
    }

    private Map<String, NvCroppingPlanEntry> loadMissingCropsAndRelativeSpeciesByCropCodes(String selectCropQuery, Statement statement, Set<String> missingCropCodes) throws SQLException {
        Map<String, NvCroppingPlanEntry> missingCropsByCropIds = new HashMap<>();
        Map<String, NvCroppingPlanEntry> missingCropsByCropCodes = new HashMap<>();
        ResultSet missingCropForDomainResultSet = statement.executeQuery(String.format(selectCropQuery, formatInValues(StringUtils.join(missingCropCodes, "','"))));
        while (missingCropForDomainResultSet.next()) {

            String tmpPreviousDomainId = missingCropForDomainResultSet.getString("domain");
            NvCroppingPlanEntry missingCroppingPlanEntryForCampaign = new NvCroppingPlanEntry(tmpPreviousDomainId);

            String topiaid = missingCropForDomainResultSet.getString("topiaid");
            String code = missingCropForDomainResultSet.getString("code");

            missingCroppingPlanEntryForCampaign.setTopiaId(topiaid);
            missingCroppingPlanEntryForCampaign.setTopiaVersion(0L);
            missingCroppingPlanEntryForCampaign.setCode(code);
            missingCroppingPlanEntryForCampaign.setDomainId(tmpPreviousDomainId);
            missingCroppingPlanEntryForCampaign.setTopiaCreateDate(scriptDate);
            missingCroppingPlanEntryForCampaign.setName(missingCropForDomainResultSet.getString("name") + " Ajouté par AGS");
            missingCroppingPlanEntryForCampaign.setSellingPrice(missingCropForDomainResultSet.getDouble("sellingprice"));
            missingCroppingPlanEntryForCampaign.setValidated(false);
            missingCroppingPlanEntryForCampaign.setType(CroppingEntryType.valueOf(missingCropForDomainResultSet.getString("type")));// non null
            missingCroppingPlanEntryForCampaign.setYealdAverage(missingCropForDomainResultSet.getDouble("yealdaverage"));
            missingCroppingPlanEntryForCampaign.setAverageIFT(missingCropForDomainResultSet.getDouble("averageift"));
            String yealdunitName = missingCropForDomainResultSet.getString("yealdunit");
            if (yealdunitName != null) {
                YealdUnit yealdunit = YealdUnit.valueOf(yealdunitName);
                missingCroppingPlanEntryForCampaign.setYealdUnit(yealdunit);
            }
            String estimatingiftrulesName = missingCropForDomainResultSet.getString("estimatingiftrules");
            if (estimatingiftrulesName != null) {
                missingCroppingPlanEntryForCampaign.setEstimatingIftRules(EstimatingIftRules.valueOf(estimatingiftrulesName));
            }
            String iftseedstypeName = missingCropForDomainResultSet.getString("iftseedstype");
            if (iftseedstypeName != null) {
                missingCroppingPlanEntryForCampaign.setIftSeedsType(IftSeedsType.valueOf(iftseedstypeName));
            }
            String dosetypeName = missingCropForDomainResultSet.getString("dosetype");
            if (dosetypeName != null) {
                missingCroppingPlanEntryForCampaign.setDoseType(DoseType.valueOf(dosetypeName));
            }
            missingCroppingPlanEntryForCampaign.setBiocontrolIFT(missingCropForDomainResultSet.getDouble("biocontrolift"));
            missingCroppingPlanEntryForCampaign.setTemporaryMeadow(missingCropForDomainResultSet.getBoolean("temporarymeadow"));
            missingCroppingPlanEntryForCampaign.setPasturedMeadow(missingCropForDomainResultSet.getBoolean("pasturedmeadow"));
            missingCroppingPlanEntryForCampaign.setMowedMeadow(missingCropForDomainResultSet.getBoolean("mowedmeadow"));
            missingCroppingPlanEntryForCampaign.setMixSpecies(missingCropForDomainResultSet.getBoolean("mixspecies"));
            missingCroppingPlanEntryForCampaign.setMixVariety(missingCropForDomainResultSet.getBoolean("mixvariety"));

            missingCropsByCropIds.put(topiaid, missingCroppingPlanEntryForCampaign);
            missingCropsByCropCodes.put(code, missingCroppingPlanEntryForCampaign);
        }

        Set<NvCroppingPlanEntry> missingCrops = new HashSet<>(missingCropsByCropIds.values());
        Set<String> missingCropIds = missingCrops.stream().map(NvCroppingPlanEntry::getTopiaId).collect(Collectors.toSet());
        String selectMissingCropCpsQuery = "SELECT topiaid,topiaversion,topiacreatedate,code,validated,variety,croppingplanentry,species,croppingplanentry_idx,speciesarea,edaplosunknownvariety,compagne,destination FROM croppingplanspecies WHERE croppingplanentry in (%s)";
        String loadMissingCropSpeciesQuery = String.format(selectMissingCropCpsQuery, formatInValues(StringUtils.join(missingCropIds, "','")));
        ResultSet cpeCpsRS = statement.executeQuery(loadMissingCropSpeciesQuery);
        while (cpeCpsRS.next()) {

            String croppingplanentry = cpeCpsRS.getString("croppingplanentry");
            String speciesCode = cpeCpsRS.getString("code");
            String speciesId = cpeCpsRS.getString("species");
            String varietyId = cpeCpsRS.getString("variety");

            NvCroppingPlanSpecies croppingPlanSpecies = new NvCroppingPlanSpecies();
            croppingPlanSpecies.setTopiaId(cpeCpsRS.getString("topiaid"));
            croppingPlanSpecies.setTopiaVersion(0L);
            croppingPlanSpecies.setCode(speciesCode);
            croppingPlanSpecies.setValidated(cpeCpsRS.getBoolean("validated"));
            croppingPlanSpecies.setVarietyId(varietyId);
            croppingPlanSpecies.setCropId(croppingplanentry);
            croppingPlanSpecies.setSpeciesId(speciesId);
            croppingPlanSpecies.setCroppingplanentry_idx(cpeCpsRS.getDouble("croppingplanentry_idx"));
            croppingPlanSpecies.setSpeciesArea(cpeCpsRS.getInt("speciesarea"));
            croppingPlanSpecies.setEdaplosUnknownVariety(cpeCpsRS.getString("edaplosunknownvariety"));
            String compagneName = cpeCpsRS.getString("compagne");
            if (compagneName != null) {
                croppingPlanSpecies.setCompagne(CompagneType.valueOf(compagneName));
            }
            croppingPlanSpecies.setDestinationId(cpeCpsRS.getString("destination"));

            NvCroppingPlanEntry missingCroppingPlanEntry = missingCropsByCropIds.get(croppingplanentry);
            if (missingCroppingPlanEntry != null) {
                missingCroppingPlanEntry.addCroppingPlanSpecies(croppingPlanSpecies);
            } else {
                LOG.error("Cropping plan witj id " + croppingplanentry + " was not found");
            }
        }

        return missingCropsByCropCodes;
    }

    /**
     * Migration des données (ce point est une proposition, à rediscuter si besoin):
     *     • on crée plusieurs lots de semences pour une culture dès que:
     *         ◦ La liste des espèces est différente ;
     *         ◦ L’unité de la quantité semée est différente (comme l’unité d’application d’un lot de semences est valable pour l’ensemble du lot, ce qui simplifie les calculs économiques).
     *         ◦ Un lot de semences en AB et en conventionnel existent (cas qui sera a priori rare: il faut qu’une même culture soit utilisée dans 2 SDC différents, un en bio et un en conventionnel).
     *         ◦ le type de semence est différent (cette variable sera différente pour l’ensemble des espèces du lot de semences, comme elle est déclarée à la culture dans les données historiques) ;
     *         ◦ Pour une des espèces, la valeur ‘Traitement chimique des semences’ ou ‘Inoculation biologique des semences’ est différente.
     *     • À l’inverse, on ne crée pas de lots différents si:
     * Il y a un traitement de semences déclaré différent => on liste tous les traitements de semences existants dans un même lot de semences.
     */
    private void persistSeeLotAndInputs(
            Connection connection,
            Map<String, List<DbDomainSeedSpeciesInput>> domainSpeciesInputForProductByActionId,
            Map<String, List<DbSeedSpeciesInputUsage>> speciesUsageForProductByActionId) throws SQLException {

        LOG.info("LOAD seeding actions and seeding action species");
        List<SeedingActionAndSpeciesRow> seedingActionAndSpeciesRows = executeGetSeedingInputQueries(connection);
        LOG.info("Nb loaded " + seedingActionAndSpeciesRows.size());

        Set<String> cpsIds = seedingActionAndSpeciesRows.stream().map(SeedingActionAndSpeciesRow::getSeedingActionSpeciesCPSId)
                .filter(StringUtils::isNotBlank).collect(Collectors.toSet());

        Map<String, List<SeedingActionAndSpeciesRow>> seedingActionAndSpeciesRowByActionIds = new HashMap<>();
        for (SeedingActionAndSpeciesRow seedingActionAndSpeciesRow : seedingActionAndSpeciesRows) {
            String actionId = seedingActionAndSpeciesRow.getActionId();
            List<SeedingActionAndSpeciesRow> seedingActionAndSpeciesRowsForAction = seedingActionAndSpeciesRowByActionIds.computeIfAbsent(actionId, k -> new ArrayList<>());
            seedingActionAndSpeciesRowsForAction.add(seedingActionAndSpeciesRow);
        }

        String cpsIdsQueryParts = formatInValues(StringUtils.join(cpsIds, "','"));

        LOG.info("LOAD SpeciesInputCpsPartKey");
        Map<String, CpsDto> cpsDtoByIds = new HashMap<>();
        try (Statement statement = connection.createStatement()) {
            String getCpsKeyQuery = """
                        SELECT cps.topiaid, cps.code, re.code_espece_botanique, re.code_qualifiant_aee, re.code_destination_aee, CONCAT(COALESCE(rvpg.variete, ''), COALESCE(rvg.denomination, ''))
                        FROM croppingplanspecies cps
                        INNER JOIN refespece re ON cps.species = re.topiaid
                        LEFT JOIN refvarieteplantgrape rvpg ON rvpg.topiaid = cps.variety
                        LEFT JOIN refvarietegeves rvg ON rvg.topiaid = cps.variety
                        WHERE cps.topiaid IN (%s)""";

            ResultSet rs = statement.executeQuery(String.format(getCpsKeyQuery, cpsIdsQueryParts));
            while (rs.next()) {
                String topiaId = rs.getString(1);
                String code = rs.getString(2);
                String codeEspeceBotanique = rs.getString(3);
                String codeQualifiantAee = rs.getString(4);
                String codeDestinationAee = rs.getString(5);
                String variety = rs.getString(6);
                CpsDto speciesKey = new CpsDto(topiaId, code, codeEspeceBotanique, codeQualifiantAee, codeDestinationAee, variety);
                cpsDtoByIds.put(topiaId, speciesKey);
            }

        }

        LOG.info(String.format("%d, NB speciesstade and seeding action loaded => wil start to create DomainSeedLotInput and DomainSeedSpeciesInput", seedingActionAndSpeciesRows.size()));

        Pair<Collection<DbDomainSeedLotInput>, Collection<DBSeedingActionUsage>> dbDomainSeedLotInputsWithUsages = buildSeedLotsPersistStructure(
                seedingActionAndSpeciesRowByActionIds,
                domainSpeciesInputForProductByActionId,
                cpsDtoByIds
        );

        Collection<DbDomainSeedLotInput> domainSeedLotInputs = dbDomainSeedLotInputsWithUsages.getLeft();

        MultiValuedMap<SeedPriceKey, SeedPrice> seedPricesByKeys = loadAllSeedPricePrices(connection);

        Binder<SeedPrice, SeedPrice> seedPriceBinder = BinderFactory.newBinder(SeedPrice.class);

        for (DbDomainSeedLotInput domainSeedLotInput : domainSeedLotInputs) {
            String domainId = domainSeedLotInput.getDomain();
            String domainCode = domainSeedLotInput.getDomainCode();

            Collection<DbDomainSeedSpeciesInput> domainSeedSpeciesInputs = domainSeedLotInput.getDomainSeedSpeciesInputs().values();

            // cherche correspondance prix réalisé
            SeedPriceKey key = getSeedPriceKey(domainSeedSpeciesInputs, domainSeedLotInput.cropSeedId, domainId);
            Collection<SeedPrice> seedPrices = seedPricesByKeys.get(key);

            // cherche correspondance prix synthétisé
            if (CollectionUtils.isEmpty(seedPrices)) {
                key = getSeedPriceKey(domainSeedSpeciesInputs, domainSeedLotInput.cropSeedCode, domainCode);
                seedPrices = seedPricesByKeys.get(key);
            }

            // cherche correspondance prix global à la culture sur le réalisé
            if (CollectionUtils.isEmpty(seedPrices)) {
                key = getSeedPriceKey(null, domainSeedLotInput.cropSeedId, domainId);
                seedPrices = seedPricesByKeys.get(key);
            }

            // cherche correspondance prix global à la culture sur le synthétisé
            if (CollectionUtils.isEmpty(seedPrices)) {
                key = getSeedPriceKey(null, domainSeedLotInput.cropSeedCode, domainCode);
                seedPrices = seedPricesByKeys.get(key);
            }
            if (CollectionUtils.isNotEmpty(seedPrices)) {

                // refs #11316 #39 si le prix de la culture
                if (CollectionUtils.size(domainSeedSpeciesInputs) == 1) {
                    DbDomainSeedSpeciesInput dbDomainSeedSpeciesInput = domainSeedSpeciesInputs.stream().findAny().orElse(null);
                    if (dbDomainSeedSpeciesInput != null) {
                        CpsDto cpsDto = cpsDtoByIds.get(dbDomainSeedSpeciesInput.getSpeciesSeedId());
                        if (cpsDto != null) {
                            String objectId = GET_SPECIES_SEED_OBJECT_ID.apply(cpsDto);
                            SeedPrice seedPriceModel = seedPrices.iterator().next();
                            SeedPrice seedPrice = new SeedPrice();
                            seedPriceBinder.copyExcluding(seedPriceModel, seedPrice, "topiaId");
                            seedPrice.setObjectId(objectId);
                            seedPrice.setDomainId(domainId);
                            seedPrice.setDisplayName(dbDomainSeedSpeciesInput.inputName);
                            seedPrice.setCategory(InputPriceCategory.SEEDING_INPUT);
                            seedPrice.setOrganic(domainSeedLotInput.isOrganic());
                            dbDomainSeedSpeciesInput.setSeedPrice(seedPrice);
                        } else {
                            LOG.error("Species not found for id: '" + dbDomainSeedSpeciesInput.getSpeciesSeedId() + "'");
                        }
                    }

                } else {
                    SeedPrice seedPriceModel = seedPrices.iterator().next();
                    SeedPrice seedPrice = new SeedPrice();
                    seedPriceBinder.copyExcluding(seedPriceModel, seedPrice, "topiaId");
                    seedPrice.setDomainId(domainId);
                    seedPrice.setObjectId(domainSeedLotInput.getCropSeedId() + " - " + domainSeedLotInput.isOrganic());// nouveau format
                    seedPrice.setCategory(InputPriceCategory.SEEDING_INPUT);
                    seedPrice.setOrganic(domainSeedLotInput.isOrganic());
                    domainSeedLotInput.setSeedPrice(seedPrice);
                }

            }

        }

        Set<String> pretestedDomainInputLotOrSpeciesIds = persistDomainSeedLots(
                connection,
                dbDomainSeedLotInputsWithUsages);

        persistSeedingUsages(
                connection,
                speciesUsageForProductByActionId,
                dbDomainSeedLotInputsWithUsages,
                pretestedDomainInputLotOrSpeciesIds);

        LOG.info("Update abstractaction to change topiadiscriminator: SeedingActionImpl -> SeedingActionUsageImpl ");

        // transformation des seedingaction en SeedingActionUsage
        try(Statement statement = connection.createStatement()) {
            statement.execute("UPDATE abstractaction SET topiadiscriminator = '" + SeedingActionUsageImpl.class.getName() +"' WHERE topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.SeedingActionImpl'");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        LOG.info("Update SeedingActionUsage -> add depness");
        // calcul d'une valeur moyenne de profondeur de semis par action
        String getSeedingActionUsageAverageDeepness = """
                UPDATE abstractaction aa SET deepness = (SELECT avg(sas.deepness) as avg FROM seedingactionspecies sas WHERE sas.seedingaction = aa.topiaid GROUP BY sas.seedingaction)
                WHERE exists (SELECT 1 FROM seedingactionspecies sas0 where sas0.seedingaction = aa.topiaid)
                AND aa.topiadiscriminator = '%s'""";
        try(Statement statement = connection.createStatement()) {
            statement.execute(String.format(getSeedingActionUsageAverageDeepness, SeedingActionUsageImpl.class.getName()));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

        Function<CpsDto, String> GET_SPECIES_SEED_OBJECT_ID = (cps) -> String.format(
                "%s - %s - %s - %s",
                cps.code_espece_botanique,
                cps.code_qualifiant_aee,
                cps.code_destination_aee,
                cps.variety != null ? cps.variety : "none"
        );

    protected SeedPriceKey getSeedPriceKey(Collection<DbDomainSeedSpeciesInput> domainSeedSpeciesInputs, String cropIdentifier, String domainIdentifier) {
        SeedPriceKey key;
        if (CollectionUtils.isEmpty(domainSeedSpeciesInputs)) {
            key = new SeedPriceKey(
                    domainIdentifier,
                    cropIdentifier,
                    SeedType.SEMENCES_CERTIFIEES,
                    false,
                    false);
        } else {
            key = new SeedPriceKey(
                    domainIdentifier,
                    cropIdentifier,
                    domainSeedSpeciesInputs.stream().map(DbDomainSeedSpeciesInput::getSeedType).findAny().orElse(SeedType.SEMENCES_CERTIFIEES),
                    domainSeedSpeciesInputs.stream().anyMatch(dssi -> Boolean.TRUE.equals(dssi.chemicalTreatment)),
                    domainSeedSpeciesInputs.stream().anyMatch(dssi -> Boolean.TRUE.equals(dssi.biologicalSeedInoculation))
            );
        }
        return key;
    }

    protected Set<String> persistDomainSeedLots(
            Connection connection,
            Pair<Collection<DbDomainSeedLotInput>, Collection<DBSeedingActionUsage>> dbDomainSeedLotInputsWithUsages) {

        Set<String> persistedDomainInputLotOrSpeciesIds = new HashSet<>();

        try (PreparedStatement insertIntoSeedLotInputStatement = connection.prepareStatement(INSERT_INTO_DOMAIN_SEED_LOT_INPUT_QUERY)) {
            try (PreparedStatement insertIntoDomainInputStockStatement = connection.prepareStatement(INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY)) {
                try(PreparedStatement insertIntoSeedingInputPriceStatement = connection.prepareStatement(INSERT_SEEDING_INPUT_PRICE_QUERY)) {
                    Collection<DbDomainSeedLotInput> dbDomainSeedLotInputs = dbDomainSeedLotInputsWithUsages.getLeft();
                    for (DbDomainSeedLotInput domainSeedLotInput : dbDomainSeedLotInputs) {

                        SeedPrice seedPrice = domainSeedLotInput.getSeedPrice();

                        String seedPriceId = null;
                        if (seedPrice != null) {
                            if (!Objects.equals(seedPrice.getDomainId(), domainSeedLotInput.getDomain())) {
                                LOG.warn("prix de semis: THE DOMAIN should be the same, domain Id:" + domainSeedLotInput.getDomain() + " seed price domain id:" + seedPrice.getDomainId());
                            }
                            seedPriceId = seedPrice.getTopiaId();
                            persistSeedPrices(seedPrice, insertIntoSeedingInputPriceStatement);
                        }

                        String domainSeedLotInputTopiaId = domainSeedLotInput.getTopiaId();
                        LOG.debug("Persist DbDomainSeedLotInput:" + domainSeedLotInputTopiaId);
                        persistAbstractDomainInput(
                                insertIntoDomainInputStockStatement,
                                domainSeedLotInputTopiaId,
                                domainSeedLotInput.getKey(),
                                domainSeedLotInput.getInputName(),
                                domainSeedLotInput.getInputName(),
                                null,
                                InputType.SEMIS,
                                null,
                                domainSeedLotInput.getCode(),
                                domainSeedLotInput.getDomain());

                        //    1        2       3         4         5
                        // topiaid, organic, cropseed, seedprice, usageunit
                        insertIntoSeedLotInputStatement.setString(1, domainSeedLotInputTopiaId);
                        insertIntoSeedLotInputStatement.setBoolean(2, domainSeedLotInput.isOrganic());
                        insertIntoSeedLotInputStatement.setString(3, domainSeedLotInput.cropSeedId);
                        insertIntoSeedLotInputStatement.setString(4, seedPriceId);
                        insertIntoSeedLotInputStatement.setString(5, domainSeedLotInput.usageUnit.name());
                        insertIntoSeedLotInputStatement.addBatch();
                        persistedDomainInputLotOrSpeciesIds.add(domainSeedLotInputTopiaId);

                    }
                    LOG.info("For current part processed " + InputType.SEMIS.name() + " execute " + INSERT_INTO_DOMAIN_SEED_LOT_INPUT_QUERY +  " BATCH");
                    insertIntoSeedingInputPriceStatement.executeBatch();
                    LOG.info("For current part processed " + InputType.SEMIS.name() + " execute " + INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY +  " BATCH");
                    insertIntoDomainInputStockStatement.executeBatch();
                    LOG.info("For current part processed " + InputType.SEMIS.name() + " execute " + INSERT_INTO_DOMAIN_SEED_LOT_INPUT_QUERY +  " BATCH");
                    insertIntoSeedLotInputStatement.executeBatch();
                    LOG.info("For current part processed " + InputType.SEMIS.name() + " END BATCH");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try (PreparedStatement insertIntoDomainSeedSpeciesInput = connection.prepareStatement(INSERT_INTO_DOMAIN_SEED_SPECIES_INPUT_QUERY)) {
            try (PreparedStatement insertIntoDomainInputStockStatement = connection.prepareStatement(INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY)) {
                try(PreparedStatement insertIntoSeedingInputPriceStatement = connection.prepareStatement(INSERT_SEEDING_INPUT_PRICE_QUERY)) {
                    Collection<DbDomainSeedLotInput> dbDomainSeedLotInputs = dbDomainSeedLotInputsWithUsages.getLeft();
                    for (DbDomainSeedLotInput domainSeedLotInput : dbDomainSeedLotInputs) {

                        String domainSeedLotInputTopiaId = domainSeedLotInput.getTopiaId();

                        for (DbDomainSeedSpeciesInput domainSeedSpeciesInput : domainSeedLotInput.getDomainSeedSpeciesInputs().values()) {
                            String domainSeedSpeciesInputTopiaId = domainSeedSpeciesInput.getTopiaId();
                            LOG.debug("Persist DbDomainSeedSpeciesInput:" + domainSeedSpeciesInputTopiaId + " on " + domainSeedSpeciesInput.getDomainSeedLotInputId());

                            SeedPrice seedPrice = domainSeedSpeciesInput.getSeedPrice();

                            String seedPriceId = null;
                            if (seedPrice != null) {
                                if (!Objects.equals(seedPrice.getDomainId(), domainSeedLotInput.getDomain())) {
                                    LOG.warn("prix de semis: THE DOMAIN should be the same, domain Id:" + domainSeedLotInput.getDomain() + " seed price domain id:" + seedPrice.getDomainId());
                                }
                                seedPriceId = seedPrice.getTopiaId();
                                persistSeedPrices(seedPrice, insertIntoSeedingInputPriceStatement);
                            }

                            persistAbstractDomainInput(
                                    insertIntoDomainInputStockStatement,
                                    domainSeedSpeciesInputTopiaId,
                                    domainSeedSpeciesInput.getKey(),
                                    domainSeedSpeciesInput.getInputName(),
                                    domainSeedSpeciesInput.getInputName(),
                                    null,
                                    InputType.SEMIS,
                                    null,
                                    domainSeedSpeciesInput.getCode(),
                                    domainSeedSpeciesInput.getDomain());

                            //                                       1           2                        3                 4         5          6         7            8
                            // INSERT INTO domainseedspeciesinput (topiaid, biologicalseedinoculation, chemicaltreatment, organic, speciesseed, seedtype, seedprice, domainseedlotinput)
                            insertIntoDomainSeedSpeciesInput.setString(1, domainSeedSpeciesInputTopiaId);
                            insertIntoDomainSeedSpeciesInput.setBoolean(2, domainSeedSpeciesInput.biologicalSeedInoculation);
                            insertIntoDomainSeedSpeciesInput.setBoolean(3, domainSeedSpeciesInput.chemicalTreatment);
                            insertIntoDomainSeedSpeciesInput.setBoolean(4, domainSeedSpeciesInput.organic);
                            insertIntoDomainSeedSpeciesInput.setString(5, domainSeedSpeciesInput.speciesSeedId);
                            setSqlSeedType(6, domainSeedSpeciesInput.seedType, insertIntoDomainSeedSpeciesInput);
                            insertIntoDomainSeedSpeciesInput.setString(7, seedPriceId);
                            insertIntoDomainSeedSpeciesInput.setString(8, domainSeedLotInputTopiaId);

                            insertIntoDomainSeedSpeciesInput.addBatch();
                            persistedDomainInputLotOrSpeciesIds.add(domainSeedSpeciesInputTopiaId);

                        }
                    }
                    insertIntoSeedingInputPriceStatement.executeBatch();
                    LOG.info("For current part processed " + InputType.SEMIS.name() + " execute " + INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY + " BATCH");
                    insertIntoDomainInputStockStatement.executeBatch();
                }
            }
            LOG.info("For current part processed " + InputType.SEMIS.name() + " execute " + INSERT_INTO_DOMAIN_SEED_SPECIES_INPUT_QUERY + " BATCH");
            insertIntoDomainSeedSpeciesInput.executeBatch();
            LOG.info("For current part processed " + InputType.SEMIS.name() + " END BATCH");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        LOG.info("Execute persistance of persistDomainAbstractInputStatement, insertIntoSeedLotInputStatement, insertIntoDomainSeedSpeciesInput, insertIntoSeedingInputPriceStatement");


        return persistedDomainInputLotOrSpeciesIds;
    }

    protected void persistSeedingUsages(
            Connection connection,
            Map<String, List<DbSeedSpeciesInputUsage>> speciesUsageForProductByActionId,
            Pair<Collection<DbDomainSeedLotInput>, Collection<DBSeedingActionUsage>> dbDomainSeedLotInputsWithUsages,
            Set<String> pretestedDomainInputLotOrSpeciesIds) {
        //    1       2               3                   4
        // topiaid, deepness, domainseedspeciesinput, seedlotinputusage
        try (PreparedStatement seedSpeciesInputUsageStatement = connection.prepareStatement(INSERT_INTO_SEED_SPECIES_INPUT_USAGE_QUERY)) {

            //     1      2            3                 4
            // topiaid, deepness, domainseedlotinput, seedingactionusage
            try (PreparedStatement seedLotInputUsageStatement = connection.prepareStatement(INSERT_INTO_SEED_LOT_INPUT_USAGE_QUERY)) {
                //     1        2         3           4     5       6     7      8       9
                // topiaid, comment, productname, qtmin, qtavg, qtmed, qtmax, inputtype, code
                try (PreparedStatement insertIntoAbstractInputUsageStatement = connection.prepareStatement(INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY)) {

                    for (DBSeedingActionUsage dbSeedingActionUsage : dbDomainSeedLotInputsWithUsages.getRight()) {
                        Collection<DbSeedLotInputUsage> dbSeedLotInputUsages = dbSeedingActionUsage.seedLotInputUsageByDomainSeedLotInputId.values();
                        for (DbSeedLotInputUsage dbSeedLotInputUsage : dbSeedLotInputUsages) {

                            String domainSeedLotInputId = dbSeedLotInputUsage.getDomainseedlotinput();
                            if (!pretestedDomainInputLotOrSpeciesIds.contains(domainSeedLotInputId)) {
                                LOG.error("DOMAIN SEED LOT WITH ID " + domainSeedLotInputId + " IS NOT PERSISTED");
                                continue;
                            }
                            Collection<DbSeedSpeciesInputUsage> dbSeedSpeciesInputUsages = dbSeedLotInputUsage.getDbSeedSpeciesInputUsageByDomainSeedSpeciesInput().values();
                            Double lotQtAvg = dbSeedSpeciesInputUsages.stream().filter(dbsliu ->  Objects.nonNull(dbsliu.getQtAvg())).mapToDouble(DbSeedSpeciesInputUsage::getQtAvg).sum();// la dose appliquée correspond à la quantité de semence

                            String dbSeedLotInputUsageTopiaId = dbSeedLotInputUsage.getTopiaId();
                            insertIntoAbstractInputUsageStatement.setString(1, dbSeedLotInputUsageTopiaId);
                            insertIntoAbstractInputUsageStatement.setString(2, dbSeedLotInputUsage.getProductName());
                            insertIntoAbstractInputUsageStatement.setString(3, StringUtils.firstNonBlank(dbSeedLotInputUsage.getProductName(), "UNKNOWN"));

                            Double qtMin = dbSeedLotInputUsage.getQtMin();
                            setSqlDoubleOrNull(4, qtMin, insertIntoAbstractInputUsageStatement);
                            setSqlDoubleOrNull(5, lotQtAvg, insertIntoAbstractInputUsageStatement);
                            setSqlDoubleOrNull(6, dbSeedLotInputUsage.getQtMed(), insertIntoAbstractInputUsageStatement);
                            setSqlDoubleOrNull(7, dbSeedLotInputUsage.getQtMax(), insertIntoAbstractInputUsageStatement);
                            insertIntoAbstractInputUsageStatement.setString(8, InputType.SEMIS.name());
                            insertIntoAbstractInputUsageStatement.setString(9, dbSeedLotInputUsage.getCode());
                            insertIntoAbstractInputUsageStatement.addBatch();

                            seedLotInputUsageStatement.setString(1, dbSeedLotInputUsageTopiaId);
                            setSqlDoubleOrNull(2, dbSeedLotInputUsage.getDeepness(), seedLotInputUsageStatement);
                            seedLotInputUsageStatement.setString(3, domainSeedLotInputId);
                            seedLotInputUsageStatement.setString(4, dbSeedLotInputUsage.getSeedingactionusage());
                            seedLotInputUsageStatement.addBatch();
                            LOG.debug("Persist DbSeedLotInputUsage:" + dbSeedLotInputUsageTopiaId + " on " + domainSeedLotInputId);

                            for (DbSeedSpeciesInputUsage dbSeedSpeciesInputUsage : dbSeedSpeciesInputUsages) {

                                String domainSeedSpeciesInputId = dbSeedSpeciesInputUsage.getDomainSeedSpeciesInput();
                                String seedSpeciesInputUsageId = dbSeedSpeciesInputUsage.getTopiaId();

                                if (!pretestedDomainInputLotOrSpeciesIds.contains(domainSeedSpeciesInputId)) {
                                    LOG.error("DOMAIN SEED SPECIES WITH ID " + domainSeedSpeciesInputId + " IS NOT PERSISTED");
                                    continue;
                                }

                                insertIntoAbstractInputUsageStatement.setString(1, seedSpeciesInputUsageId);
                                insertIntoAbstractInputUsageStatement.setString(2, dbSeedSpeciesInputUsage.getProductName());
                                insertIntoAbstractInputUsageStatement.setString(3, StringUtils.firstNonBlank(dbSeedSpeciesInputUsage.getProductName(), "UNKNOWN"));
                                setSqlDoubleOrNull(4, dbSeedSpeciesInputUsage.getQtMin(), insertIntoAbstractInputUsageStatement);
                                setSqlDoubleOrNull(5, dbSeedSpeciesInputUsage.getQtAvg(), insertIntoAbstractInputUsageStatement);
                                setSqlDoubleOrNull(6, dbSeedSpeciesInputUsage.getQtMed(), insertIntoAbstractInputUsageStatement);
                                setSqlDoubleOrNull(7, dbSeedSpeciesInputUsage.getQtMax(), insertIntoAbstractInputUsageStatement);
                                insertIntoAbstractInputUsageStatement.setString(8, InputType.SEMIS.name());
                                insertIntoAbstractInputUsageStatement.setString(9, dbSeedSpeciesInputUsage.getCode());
                                insertIntoAbstractInputUsageStatement.addBatch();

                                //    1       2               3                   4
                                // topiaid, deepness, domainseedspeciesinput, seedlotinputusage
                                seedSpeciesInputUsageStatement.setString(1, seedSpeciesInputUsageId);
                                setSqlDoubleOrNull(2, dbSeedSpeciesInputUsage.getDeepness(), seedSpeciesInputUsageStatement);
                                seedSpeciesInputUsageStatement.setString(3, domainSeedSpeciesInputId);
                                seedSpeciesInputUsageStatement.setString(4, dbSeedLotInputUsageTopiaId);
                                seedSpeciesInputUsageStatement.addBatch();

                                LOG.debug("Persist DbSeedSpeciesInputUsage:" + seedSpeciesInputUsageId + " on " + domainSeedSpeciesInputId);

                                List<DbSeedSpeciesInputUsage> dbDomainSeedSpeciesInputs = speciesUsageForProductByActionId.computeIfAbsent(dbSeedSpeciesInputUsage.getSeedingActionId(), k -> new ArrayList<>());
                                dbDomainSeedSpeciesInputs.add(dbSeedSpeciesInputUsage);
                            }
                        }
                    }
                    LOG.info("For current part processed " + InputType.SEMIS.name() + " execute " + INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY + " BATCH");
                    insertIntoAbstractInputUsageStatement.executeBatch();
                }
                LOG.info("For current part processed " + InputType.SEMIS.name() + " execute " + INSERT_INTO_SEED_LOT_INPUT_USAGE_QUERY + " BATCH");
                seedLotInputUsageStatement.executeBatch();
            }
            LOG.info("For current part processed " + InputType.SEMIS.name() + " execute " + INSERT_INTO_SEED_SPECIES_INPUT_USAGE_QUERY + " BATCH");
            seedSpeciesInputUsageStatement.executeBatch();
            LOG.info("For current part processed " + InputType.SEMIS.name() + " END BATCH");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    record SeedPriceKey (

            String domainIdentifier,
            String cropIdentifier,

            SeedType seedType,
            boolean treatment,
            boolean biologicaltreatment
    ){}

    protected MultiValuedMap<SeedPriceKey, SeedPrice> loadAllSeedPricePrices(
            Connection connection) {
        Collection<SeedPrice> seedPrices = loadSeedPrices(connection);

        MultiValuedMap<SeedPriceKey, SeedPrice> result = new HashSetValuedHashMap<>();
        for (SeedPrice seedPrice : seedPrices) {
            String domainIdentifier = StringUtils.isNotBlank(seedPrice.getPracticedSystemId()) ? seedPrice.getDomainCode() : seedPrice.getDomainId();
            SeedPriceKey key = new SeedPriceKey(
                    domainIdentifier,
                    seedPrice.getObjectId(),
                    seedPrice.getSeedType(),
                    seedPrice.chemicalTreatment,
                    seedPrice.biologicalSeedInoculation);
            result.put(key, seedPrice);
        }

        return result;
    }

    protected MultiValuedMap<SeedPriceKey, SeedPrice> loadAllProductSeedPricePrices(
            Connection connection) {
        Collection<SeedPrice> seedPrices = loadSeedPrices(connection);

        MultiValuedMap<SeedPriceKey, SeedPrice> result = new HashSetValuedHashMap<>();
        for (SeedPrice seedPrice : seedPrices) {
            String domainIdentifier = StringUtils.isNotBlank(seedPrice.getPracticedSystemId()) ? seedPrice.getDomainCode() : seedPrice.getDomainId();
            SeedPriceKey key = new SeedPriceKey(
                    domainIdentifier,
                    seedPrice.getObjectId(),
                    seedPrice.getSeedType(),
                    seedPrice.chemicalTreatment,
                    seedPrice.biologicalSeedInoculation);
            result.put(key, seedPrice);
        }

        return result;
    }

    protected void setSqlDoubleOrNull(int parameterIndex, Double value, PreparedStatement statement) throws SQLException {
        if (value != null) {
            statement.setDouble(parameterIndex, value);
        } else {
            statement.setNull(parameterIndex, Types.DOUBLE);
        }
    }
    protected void setSqlTypeOrNull(int parameterIndex, Object value, int sqlType, PreparedStatement statement) throws SQLException {
        if (value != null) {
            statementSetNonNullTypedValue(parameterIndex, value, statement);
        } else {
            statementSetNullTypedValue(parameterIndex, sqlType, statement);
        }
    }

    protected void statementSetNullTypedValue(int parameterIndex, int sqlType, PreparedStatement statement) throws SQLException {
        if (sqlType == Types.DOUBLE) {
            statement.setNull(parameterIndex, Types.DOUBLE);
        } else if (sqlType == Types.INTEGER) {
            statement.setNull(parameterIndex, Types.INTEGER);
        } else if (sqlType == Types.VARCHAR) {
            statement.setNull(parameterIndex, Types.VARCHAR);
        } else if (sqlType == Types.BOOLEAN) {
            statement.setNull(parameterIndex, Types.BOOLEAN);
        }
    }

    protected void statementSetNonNullTypedValue(int parameterIndex, Object value, PreparedStatement statement) throws SQLException {
        if (value instanceof Double v) {
            statement.setDouble(parameterIndex, v);
        } else if (value instanceof Integer v) {
            statement.setInt(parameterIndex, v);
        } else if (value instanceof String v) {
            statement.setString(parameterIndex, v);
        } else if (value instanceof Boolean v) {
            statement.setBoolean(parameterIndex, v);
        } else if (value instanceof Enum v) {
            statement.setString(parameterIndex, v.name());
        } else {
            LOG.error("OBJECT NOT SETTED");
        }
    }

    protected void setSqlSeedType(int parameterIndex, SeedType seedType, PreparedStatement insertIntoDomainSeedSpeciesInput) throws SQLException {
        // default
        insertIntoDomainSeedSpeciesInput.setString(parameterIndex, Objects.requireNonNullElse(seedType, SeedType.SEMENCES_CERTIFIEES).name());
    }

    protected Pair<Collection<DbDomainSeedLotInput>, Collection<DBSeedingActionUsage>> buildSeedLotsPersistStructure(
            Map<String, List<SeedingActionAndSpeciesRow>> seedingActionAndSpeciesRowsByActionId,
            Map<String, List<DbDomainSeedSpeciesInput>> speciesInputForProductByActionId,
            Map<String, CpsDto> cpsDtoByIds) {

        LOG.info("buildSeedLotsPersistStructure");

        Collection<DbDomainSeedLotInput> dbDomainSeedLotInputs = new HashSet<>();
        Collection<DBSeedingActionUsage> dbSeedingActionUsages = new HashSet<>();

        Map<DbDomainSeedLotInput, DbDomainSeedLotInput> domainSeedLotInputMap = new HashMap<>();
        Map<String, DBSeedingActionUsage> dbSeedingActionUsageByActionId = new HashMap<>();

        Map<String, List<DbDomainSeedLotInput>> domainSeedLotInputByDomainCode = new HashMap<>();

        for (Map.Entry<String, List<SeedingActionAndSpeciesRow>> seedingActionAndSpeciesRowsForActionId : seedingActionAndSpeciesRowsByActionId.entrySet()) {
            String actionId = seedingActionAndSpeciesRowsForActionId.getKey();
            List<SeedingActionAndSpeciesRow> seedingActionAndSpeciesRows0 = seedingActionAndSpeciesRowsForActionId.getValue();
            // 1 create the domainSeedLotInputModel, create lot as much as ther are differents units
            // 2 add all species
            // 3 look if same existing domainSeedLotModel exists

            // group by unitUsage
            Map<SeedPlantUnit, List<SeedingActionAndSpeciesRow>> seedingActionAndSpeciesRowByUsageUnit = new HashMap<>();
            for (SeedingActionAndSpeciesRow seedingActionAndSpeciesRow : seedingActionAndSpeciesRows0) {
                SeedPlantUnit usageUnit = seedingActionAndSpeciesRow.getSeedingActionSpeciesUsageUnit();
                List<SeedingActionAndSpeciesRow> seedingActionAndSpeciesForUsageUnit = seedingActionAndSpeciesRowByUsageUnit.computeIfAbsent(usageUnit, k -> new ArrayList<>());
                seedingActionAndSpeciesForUsageUnit.add(seedingActionAndSpeciesRow);
            }

            for (Map.Entry<SeedPlantUnit, List<SeedingActionAndSpeciesRow>> seedingActionAndSpeciesRowForSeedPlantUnit : seedingActionAndSpeciesRowByUsageUnit.entrySet()) {
                List<SeedingActionAndSpeciesRow> seedingActionAndSpeciesRows = seedingActionAndSpeciesRowForSeedPlantUnit.getValue();

                SeedingActionAndSpeciesRow useToCreateSeedLotModel = seedingActionAndSpeciesRows.get(0);
                DbDomainSeedLotInput seedLotModel = createSeedLotPesistStructure(useToCreateSeedLotModel);

                if (seedLotModel == null) {
                    LOG.error("LOT IS NULL " + useToCreateSeedLotModel);
                    continue;
                }

                for (SeedingActionAndSpeciesRow seedingActionAndSpeciesRow : seedingActionAndSpeciesRows) {
                    DbDomainSeedSpeciesInput domainSeedSpeciesInput = createSeedSpecies(
                            seedLotModel,
                            cpsDtoByIds,
                            seedingActionAndSpeciesRow
                    );

                    if (domainSeedSpeciesInput != null) {
                        seedLotModel.addDbDomainSeedSpeciesInput(domainSeedSpeciesInput);
                    }
                }

                String lotSpeciesName = seedLotModel.domainSeedSpeciesInputs.values().stream()
                        .map(DbDomainSeedSpeciesInput::getSpeciesSimpleName)
                        .collect(Collectors.joining(", "));

                // same domain seed lot
                DbDomainSeedLotInput currentDbDomainSeedLotInput = domainSeedLotInputMap.get(seedLotModel);
                if (currentDbDomainSeedLotInput == null) {
                    // same domain seed lot not found the model current become the current
                    currentDbDomainSeedLotInput = seedLotModel;

                    currentDbDomainSeedLotInput.setInputName(currentDbDomainSeedLotInput.getInputName() + " " + lotSpeciesName);

                    domainSeedLotInputMap.put(seedLotModel, currentDbDomainSeedLotInput);

                    // we look if the same lot on another campaign exists
                    // if so we set to the lot and to the species the relevant code
                    String domainCode = useToCreateSeedLotModel.getDomainCode();
                    List<DbDomainSeedLotInput> otherDomainLots = domainSeedLotInputByDomainCode.computeIfAbsent(domainCode, k -> new ArrayList<>());

                    DbDomainSeedLotInput finalCurrentDbDomainSeedLotInput = currentDbDomainSeedLotInput;
                    Optional<DbDomainSeedLotInput> otherCampaignDomainSeedLot = otherDomainLots.stream().filter(dsli -> dsli.otherCampaignEquals(finalCurrentDbDomainSeedLotInput)).findAny();
                    if (otherCampaignDomainSeedLot.isPresent()) {
                        DbDomainSeedLotInput otherCampaignDomainSeedLotInput = otherCampaignDomainSeedLot.get();
                        currentDbDomainSeedLotInput.setCode(otherCampaignDomainSeedLotInput.getCode());
                        Collection<DbDomainSeedSpeciesInput> currentDomainSeedSpeciesInputs = currentDbDomainSeedLotInput.getDomainSeedSpeciesInputs().values();
                        Map<String, DbDomainSeedSpeciesInput> otherCampaignDomainSeedSpeciesInputBySpeciesKeys = otherCampaignDomainSeedLotInput.getDomainSeedSpeciesInputs();
                        for (DbDomainSeedSpeciesInput currentDomainSeedSpeciesInput : currentDomainSeedSpeciesInputs) {
                            DbDomainSeedSpeciesInput otherCampaignDomainSeedSpeciesInput = otherCampaignDomainSeedSpeciesInputBySpeciesKeys.get(currentDomainSeedSpeciesInput.getKey());
                            if (otherCampaignDomainSeedSpeciesInput != null) {// should be true
                                currentDomainSeedSpeciesInput.setCode(otherCampaignDomainSeedSpeciesInput.getCode());
                            }
                        }

                    } else {
                        otherDomainLots.add(currentDbDomainSeedLotInput);
                    }

                    if (!dbDomainSeedLotInputs.contains(currentDbDomainSeedLotInput)) {
                        dbDomainSeedLotInputs.add(currentDbDomainSeedLotInput);
                    } else {
                        LOG.error("Contient déjà " + currentDbDomainSeedLotInput);
                    }

                }

                Collection<DbDomainSeedSpeciesInput> domainSeedSpeciesInputs = currentDbDomainSeedLotInput.getDomainSeedSpeciesInputs().values();
                if (CollectionUtils.isNotEmpty(domainSeedSpeciesInputs)) {
                    List<DbDomainSeedSpeciesInput> dbDomainSeedSpeciesInputs = speciesInputForProductByActionId.computeIfAbsent(actionId, k -> new ArrayList<>());
                    dbDomainSeedSpeciesInputs.addAll(domainSeedSpeciesInputs);
                }

                // we add usages
                Map<String, DbDomainSeedSpeciesInput> domainSeedSpeciesInputByKeys = currentDbDomainSeedLotInput.getDomainSeedSpeciesInputs();
                for (SeedingActionAndSpeciesRow seedingActionAndSpeciesRow : seedingActionAndSpeciesRows) {

                    DBSeedingActionUsage dbSeedingActionUsage = createOrGetDbSeedingActionUsage(
                            dbSeedingActionUsageByActionId,
                            seedingActionAndSpeciesRow.getActionId());

                    dbSeedingActionUsages.add(dbSeedingActionUsage);

                    DbSeedLotInputUsage dbSeedLotInputUsage = createOrGetSeedLotInputUsage(
                            seedingActionAndSpeciesRow,
                            dbSeedingActionUsage,
                            currentDbDomainSeedLotInput
                    );

                    if (StringUtils.isNotBlank(seedingActionAndSpeciesRow.getSeedingActionSpeciesId())) {

                        String vLabel = seedingActionAndSpeciesRow.getLibelle_variete() != null ? seedingActionAndSpeciesRow.getLibelle_variete() : "";
                        CpsDto cpsDto = cpsDtoByIds.get(seedingActionAndSpeciesRow.getSeedingActionSpeciesCPSId());
                        String domainSeedSpeciesInputKey = DomainInputStockUnitService.getLotSpeciesInputKey(
                                cpsDto.code_espece_botanique,
                                cpsDto.code_qualifiant_aee,
                                Boolean.TRUE.equals(seedingActionAndSpeciesRow.getSeedingActionSpeciesBiologicalSeedInoculation()),
                                Boolean.TRUE.equals(seedingActionAndSpeciesRow.getSeedingActionSpeciesTreatment()),
                                currentDbDomainSeedLotInput.isOrganic(),
                                vLabel,
                                seedingActionAndSpeciesRow.getSeedingActionSpeciesUsageUnit(),
                                seedingActionAndSpeciesRow.getSeedType());

                        DbDomainSeedSpeciesInput domainSeedSpeciesInput = domainSeedSpeciesInputByKeys.get(domainSeedSpeciesInputKey);
                        if (domainSeedSpeciesInput == null) {
                            LOG.error("DbDomainSeedSpeciesInput should not be null");
                            throw new AgrosystTechnicalException("key not found for domain " + seedingActionAndSpeciesRow.getDomainId() + ": " + domainSeedSpeciesInputKey);
                        }

                        DbSeedSpeciesInputUsage dbSeedSpeciesInputUsage = buildSeedingActionUsagePersistStructure(
                                dbSeedingActionUsage,
                                seedingActionAndSpeciesRow,
                                domainSeedSpeciesInput
                        );

                        if (dbSeedSpeciesInputUsage != null) {
                            dbSeedLotInputUsage.addDbSeedSpeciesInputUsage(domainSeedSpeciesInput.getTopiaId(), dbSeedSpeciesInputUsage);
                        }
                    }
                }
            }
        }

        return Pair.of(dbDomainSeedLotInputs, dbSeedingActionUsages);
    }

    protected DbSeedLotInputUsage createOrGetSeedLotInputUsage(
            SeedingActionAndSpeciesRow seedingActionAndSpeciesRow,
            DBSeedingActionUsage dbSeedingActionUsage,
            DbDomainSeedLotInput currentDbDomainSeedLotInput) {
        String domainSeedLotInputTopiaId = currentDbDomainSeedLotInput.getTopiaId();
        DbSeedLotInputUsage dbSeedLotInputUsage = dbSeedingActionUsage.getDbSeedLotInputUsageForSeedLotInputId(domainSeedLotInputTopiaId);
        if (dbSeedLotInputUsage == null) {
            dbSeedLotInputUsage = new DbSeedLotInputUsage();
            dbSeedLotInputUsage.setSeedingactionusage(dbSeedingActionUsage.topiaId);
            dbSeedLotInputUsage.setDomainseedlotinput(domainSeedLotInputTopiaId);
            dbSeedLotInputUsage.setDeepness(seedingActionAndSpeciesRow.getSeedingActionSpeciesDeepness());
            dbSeedLotInputUsage.setProductName(currentDbDomainSeedLotInput.getInputName());
            dbSeedingActionUsage.addtDbSeedLotInputUsageForSeedLotInputId(domainSeedLotInputTopiaId, dbSeedLotInputUsage);

            LOG.debug("DbSeedLotInputUsage build: " + dbSeedLotInputUsage.getTopiaId() + " on " + dbSeedLotInputUsage.getDomainseedlotinput() + " action " + dbSeedLotInputUsage.getSeedingactionusage());
        }
        return dbSeedLotInputUsage;
    }

    protected DBSeedingActionUsage createOrGetDbSeedingActionUsage(
            Map<String, DBSeedingActionUsage> dbSeedingActionUsageByActionId,
            String seedingActionUsageId) {

        DBSeedingActionUsage dbSeedingActionUsage = dbSeedingActionUsageByActionId.get(seedingActionUsageId);
        if (dbSeedingActionUsage == null) {
            dbSeedingActionUsage = new DBSeedingActionUsage(seedingActionUsageId);
            dbSeedingActionUsageByActionId.put(seedingActionUsageId, dbSeedingActionUsage);
        }
        return dbSeedingActionUsage;
    }

    protected DbSeedSpeciesInputUsage buildSeedingActionUsagePersistStructure(
            DBSeedingActionUsage dbSeedingActionUsage,
            SeedingActionAndSpeciesRow seedingActionAndSpeciesRow,
            DbDomainSeedSpeciesInput domainSeedSpeciesInput) {

        DbSeedSpeciesInputUsage dbSeedSpeciesInputUsage = null;

        if (domainSeedSpeciesInput != null) {
            String seedingActionUsageId = dbSeedingActionUsage.topiaId;

            dbSeedSpeciesInputUsage = new DbSeedSpeciesInputUsage();
            dbSeedSpeciesInputUsage.setProductName(domainSeedSpeciesInput.getInputName());
            dbSeedSpeciesInputUsage.setDomainSeedSpeciesInput(domainSeedSpeciesInput.getTopiaId());
            dbSeedSpeciesInputUsage.setDeepness(seedingActionAndSpeciesRow.getSeedingActionSpeciesDeepness());
            dbSeedSpeciesInputUsage.setQtAvg(seedingActionAndSpeciesRow.getSeedingActionSpeciesQuantity());

            // to match with phyto
            dbSeedSpeciesInputUsage.setSeedingActionId(seedingActionUsageId);
            dbSeedSpeciesInputUsage.setSpeciesSeed(seedingActionAndSpeciesRow.getSeedingActionSpeciesCPSId());
            dbSeedSpeciesInputUsage.setBiologicalSeedInoculation(domainSeedSpeciesInput.isBiologicalSeedInoculation());
            dbSeedSpeciesInputUsage.setChemicalTreatment(domainSeedSpeciesInput.isChemicalTreatment());

            LOG.debug("DbSeedSpeciesInputUsage build: " + dbSeedSpeciesInputUsage.getTopiaId() + " on " + dbSeedSpeciesInputUsage.getDomainSeedSpeciesInput());
        } else {
            LOG.debug("NO SPECIES FOR " + seedingActionAndSpeciesRow);
        }

        return dbSeedSpeciesInputUsage;
    }

    protected String getSeedLotKey(SeedingActionAndSpeciesRow seedingActionAndSpeciesRow, boolean isOrganic, String cropCode) {
        return cropCode + DomainInputStockUnitService.KEY_SEPARATOR + isOrganic + DomainInputStockUnitService.KEY_SEPARATOR + seedingActionAndSpeciesRow.seedingActionSpeciesUsageUnit;
    }

    protected DbDomainSeedLotInput createSeedLotPesistStructure(
            SeedingActionAndSpeciesRow seedingActionAndSpeciesRow) {

        boolean isOrganic = 19948 == seedingActionAndSpeciesRow.getType_agriculture_id();// from growingSystem
        DbDomainSeedLotInput domainSeedLotInput = new DbDomainSeedLotInput();
        domainSeedLotInput.setDomain(seedingActionAndSpeciesRow.getDomainId());
        domainSeedLotInput.setDomainCode(seedingActionAndSpeciesRow.getDomainCode());
        domainSeedLotInput.setOrganic(isOrganic);
        domainSeedLotInput.setUsageUnit(seedingActionAndSpeciesRow.getSeedingActionSpeciesUsageUnit());

        String cropCode;
        if (!seedingActionAndSpeciesRow.isIntermediatecrop() && seedingActionAndSpeciesRow.getInterventionCropId() != null) {
            cropCode = seedingActionAndSpeciesRow.getInterventionCropCode();
            domainSeedLotInput.setKey(getSeedLotKey(seedingActionAndSpeciesRow, isOrganic, cropCode));
            domainSeedLotInput.setInputName(seedingActionAndSpeciesRow.getCrop_name());
            domainSeedLotInput.setCropSeedId(seedingActionAndSpeciesRow.getInterventionCropId());
            domainSeedLotInput.setCropSeedCode(seedingActionAndSpeciesRow.getInterventionCropCode());

        } else if (seedingActionAndSpeciesRow.isIntermediatecrop() && seedingActionAndSpeciesRow.getInterventionIntermediateCropId() != null) {
            cropCode = seedingActionAndSpeciesRow.getInterventionIntermediateCropCode();
            domainSeedLotInput.setKey(getSeedLotKey(seedingActionAndSpeciesRow, isOrganic, cropCode));
            domainSeedLotInput.setInputName(seedingActionAndSpeciesRow.getIntermediate_crop_name());
            domainSeedLotInput.setCropSeedId(seedingActionAndSpeciesRow.getInterventionIntermediateCropId());
            domainSeedLotInput.setCropSeedCode(seedingActionAndSpeciesRow.getInterventionIntermediateCropCode());
        } else {
            LOG.error("Seeding Crop not found:" + seedingActionAndSpeciesRow);
            return null;
        }

        return domainSeedLotInput;
    }

    protected DbDomainSeedSpeciesInput createSeedSpecies(
            DbDomainSeedLotInput domainSeedLotInput,
            Map<String, CpsDto> cpsDtoByIds,
            SeedingActionAndSpeciesRow seedingActionAndSpeciesRow) {

        DbDomainSeedSpeciesInput domainSeedSpeciesInput = null;

        if (StringUtils.isNotBlank(seedingActionAndSpeciesRow.getSeedingActionSpeciesId())) {
            boolean isOrganic = domainSeedLotInput.isOrganic();

            String domainSeedLotInputTopiaId = domainSeedLotInput.getTopiaId();

            domainSeedSpeciesInput = new DbDomainSeedSpeciesInput();
            domainSeedSpeciesInput.setBiologicalSeedInoculation(Boolean.TRUE.equals(seedingActionAndSpeciesRow.seedingActionSpeciesBiologicalSeedInoculation));
            domainSeedSpeciesInput.setChemicalTreatment(Boolean.TRUE.equals(seedingActionAndSpeciesRow.seedingActionSpeciesTreatment));
            domainSeedSpeciesInput.setOrganic(isOrganic);
            domainSeedSpeciesInput.setSpeciesSeedId(seedingActionAndSpeciesRow.getSeedingActionSpeciesCPSId());
            domainSeedSpeciesInput.setSpeciesSeedCode(seedingActionAndSpeciesRow.getSeedingActionSpeciesCPSCode());
            SeedType seedType = seedingActionAndSpeciesRow.getSeedType();
            domainSeedSpeciesInput.setSeedType(seedType);
            domainSeedSpeciesInput.setDomainSeedLotInputId(domainSeedLotInputTopiaId);

            String vLabel = seedingActionAndSpeciesRow.getLibelle_variete() != null ? seedingActionAndSpeciesRow.getLibelle_variete() : "";
            CpsDto cpsDto = cpsDtoByIds.get(seedingActionAndSpeciesRow.getSeedingActionSpeciesCPSId());
            String domainSeedSpeciesInputKey = DomainInputStockUnitService.getLotSpeciesInputKey(
                    cpsDto.code_espece_botanique,
                    cpsDto.code_qualifiant_aee,
                    domainSeedSpeciesInput.biologicalSeedInoculation,
                    domainSeedSpeciesInput.chemicalTreatment,
                    isOrganic,
                    vLabel,
                    domainSeedLotInput.usageUnit,// 1 usage unit for the lot
                    domainSeedSpeciesInput.seedType
            );

            domainSeedSpeciesInput.setKey(domainSeedSpeciesInputKey);

            String libelleEspeceBotanique = seedingActionAndSpeciesRow.getLibelle_espece_botanique();

            String speciesName = computeDetails(seedingActionAndSpeciesRow);
            String enumTranslation = getSeetTypeTraduction(seedType);
            String speciesInputName = speciesName.isEmpty() ? libelleEspeceBotanique + " " +  enumTranslation : libelleEspeceBotanique + " " + speciesName + " " +  enumTranslation;

            List<String> suffixes = new ArrayList<>();

            if (domainSeedSpeciesInput.biologicalSeedInoculation) {
                suffixes.add("traitement biologique");
            }
            if (domainSeedSpeciesInput.chemicalTreatment) {
                suffixes.add("traitement chimique");
            }
            if (!domainSeedSpeciesInput.biologicalSeedInoculation && !domainSeedSpeciesInput.chemicalTreatment) {
                suffixes.add("non traité");
            }

            speciesInputName += (", " + String.join(", ", suffixes));


            final String varietyName = StringUtils.isNoneBlank(seedingActionAndSpeciesRow.getLibelle_variete()) ? " (" + seedingActionAndSpeciesRow.getLibelle_variete() + ")" : "";
            String simpleName = libelleEspeceBotanique + varietyName;

            domainSeedSpeciesInput.setInputName(speciesInputName);
            domainSeedSpeciesInput.setSpeciesSimpleName(simpleName);
            domainSeedSpeciesInput.setDomain(domainSeedLotInput.getDomain());
            domainSeedSpeciesInput.setDomainCode(domainSeedLotInput.getDomainCode());
        }

        return domainSeedSpeciesInput;
    }

    protected static String computeDetails(SeedingActionAndSpeciesRow speciesRow) {
        List<String> details = new ArrayList<>();
        details.add(StringUtils.trimToNull(speciesRow.getLibelle_qualifiant_aee()));
        details.add(StringUtils.trimToNull(speciesRow.getLibelle_type_saisonnier_aee()));
        details.add(StringUtils.trimToNull(speciesRow.getLibelle_destination_aee()));
        details.add(StringUtils.trimToNull(speciesRow.getLibelle_variete()));
        String speciesName = StringUtils.trimToNull(details.stream().filter(Objects::nonNull).collect(Collectors.joining(", ")));
        speciesName = speciesName != null ? "(" + speciesName + ")" : "";
        return speciesName;
    }

    protected static String getSeetTypeTraduction(SeedType seedType) {
        String translation;
        switch (seedType) {
            case SEMENCES_CERTIFIEES -> translation = "Semences ou plants certifiés";
            case SEMENCES_DE_FERME -> translation = "Semences de ferme";
            case SEMENCES_DE_FERME_ET_CERTIFIEES -> translation = "Mélange de semences certifiées et de ferme";
            default -> translation = "";
        }
        return translation;
    }

    private Collection<AbstractInputRow> getInputRowForGivenDomainsAndType(
            Connection connection,
            int nbInputs,
            Map<String, String> inputQueries,
            AtomicInteger currentNbInput,
            List<String> domainIdsPart,
            InputType inputType) {

        String domainIdQueryPart = formatInValues(StringUtils.join(domainIdsPart, "','"));
        Collection<AbstractInputRow> domainAbstractInputRows = new HashSet<>();

        for (Map.Entry<String, String> getInputRowQuery : inputQueries.entrySet()) {

            final Set<AbstractInputRow> resultsForQuery = executesGetInputsQuery(
                    connection,
                    getInputRowQuery,
                    inputType,
                    domainIdQueryPart,
                    nbInputs,
                    currentNbInput);

            domainAbstractInputRows.addAll(resultsForQuery);
        }
        return domainAbstractInputRows;
    }


    private Set<AbstractInputRow> executesGetInputsQuery(
            Connection connection,
            Map.Entry<String, String> getInputRowQuery,
            InputType inputType,
            String domainIdQueryPart,
            int nbInputs,
            AtomicInteger currentNbInput) {

        Set<AbstractInputRow> abstractInputRows = new HashSet<>();

        if (InputType.CARBURANT.equals(inputType) ||
                InputType.IRRIGATION.equals(inputType) ||
                InputType.PLAN_COMPAGNE.equals(inputType) ||
                InputType.TRAITEMENT_SEMENCE.equals(inputType)) {
            return abstractInputRows;
        }

        LOG.info("Récupération des intrants:" + getInputRowQuery.getKey() + " de type " + inputType.name());

        final String getInputRowQueryValue = getInputRowQuery.getValue();
        String getInputRowQueryForGivenDomains = String.format(getInputRowQueryValue, getActionJoinOnPart(inputType), domainIdQueryPart);
        try (PreparedStatement statement = connection.prepareStatement(getInputRowQueryForGivenDomains)) {

            statement.setString(1, inputType.name());
            ResultSet inputsRs = statement.executeQuery();

            while (inputsRs.next()) {
                AbstractInputRow abstractInputRow = new AbstractInputRow(
                        inputsRs.getString("domainid"),
                        inputsRs.getString("domaincode"),
                        inputsRs.getInt("domaincampaign"),
                        inputsRs.getString("inputid"),
                        inputsRs.getString("inputdiscriminator"),
                        inputsRs.getString("actionid"),
                        inputsRs.getString("actiondiscriminator"),
                        inputsRs.getLong("topiaversion"),
                        inputsRs.getDate("topiacreatedate"),
                        inputsRs.getString("practicedsystem"),
                        (Double) inputsRs.getObject("qtmin"),
                        (Double) inputsRs.getObject("qtavg"),
                        (Double) inputsRs.getObject("qtmed"),
                        (Double) inputsRs.getObject("qtmax"),
                        StringUtil.emptyToNull(inputsRs.getString("productname")),
                        inputsRs.getString("inputtype"),
                        inputsRs.getString("producttype"),
                        inputsRs.getString("phytoproduct"),
                        inputsRs.getString("phytoproductunit"),
                        inputsRs.getString("oldproductqtunit"),
                        inputsRs.getString("otheraction"),
                        (Double) inputsRs.getObject("n"),
                        (Double) inputsRs.getObject("p2o5"),
                        (Double) inputsRs.getObject("k2o"),
                        inputsRs.getString("organicproduct"),
                        inputsRs.getString("organicfertilizersspreadingaction"),
                        inputsRs.getString("organicproductunit"),
                        inputsRs.getString("mineralproduct"),
                        inputsRs.getString("mineralproductunit"),
                        inputsRs.getString("mineralfertilizersspreadingaction"),
                        inputsRs.getString("pesticidesspreadingaction"),
                        inputsRs.getString("seedingaction"),
                        inputsRs.getString("biologicalcontrolaction"),
                        inputsRs.getString("harvestingaction"),
                        inputsRs.getString("irrigationaction"),
                        inputsRs.getString("maintenancepruningvinesaction"),
                        inputsRs.getBoolean("phytoeffect"),
                        inputsRs.getString("otherproductinputunit"),
                        inputsRs.getString("substrate"),
                        inputsRs.getString("substrateinputunit"),
                        inputsRs.getString("pot"),
                        inputsRs.getString("potinputunit")
                );
                LOG.info("Nb input loaded:" + currentNbInput.addAndGet(1) + "/" + nbInputs);
                abstractInputRows.add(abstractInputRow);
            }


        } catch (Exception e) {
            LOG.error("Exception lors de la récupération des intrants:" + getInputRowQuery.getKey(), e);
            throw new AgrosystTechnicalException("Exception lors de la récupération des intrants:" + getInputRowQuery.getKey(), e);
        }
        return abstractInputRows;
    }

    public static String formatInValues(String requestElement) {
        return "'" + requestElement + "'";
    }

    private void migrateInputs(
            Collection<AbstractInputRow> inputRows,
            Connection connection,
            InputType inputType,
            Map<String, List<DbDomainSeedSpeciesInput>> speciesInputForProductByActionId,
            Map<String, List<DbSeedSpeciesInputUsage>> speciesUsageForProductByActionId,
            int domainDonePercent) {

        LOG.info(String.format("migrate %d of type: %s domain: %d perc", inputRows.size(), inputType, domainDonePercent));

        final Set<AbstractInputRow> practicedInputRows = inputRows.stream().filter(ir -> StringUtils.isNoneBlank(ir.practicedSystemId())).collect(Collectors.toSet());
        final Set<AbstractInputRow> effectiveInputRows = inputRows.stream().filter(ir -> StringUtils.isBlank(ir.practicedSystemId())).collect(Collectors.toSet());

        switch (inputType) {
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {

                Map<String, RefFertiMinUNIFA> refFertiMinUNIFAByIds = loadRefFertiMinByIds(connection, inputRows);

                Map<InputPrice, InputPrice> uniquePrices = new HashMap<>();
                Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId = persistMineralInputPrices(
                        connection,
                        practicedInputRows,
                        FIND_PRACTICED_PRICES_QUERY,
                        refFertiMinUNIFAByIds,
                        true,
                        uniquePrices
                );
                priceByDomainIdObjectId.putAll(persistMineralInputPrices(
                        connection,
                        effectiveInputRows,
                        FIND_DOMAIN_PRICES_QUERY,
                        refFertiMinUNIFAByIds,
                        false,
                        uniquePrices
                ));

                persistInputPrices(
                        connection,
                        InputPriceCategory.MINERAL_INPUT,
                        uniquePrices.values());

                persistMineralInputs(
                        connection,
                        refFertiMinUNIFAByIds,
                        priceByDomainIdObjectId,
                        inputRows,
                        domainDonePercent
                );

            }
            case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES -> {

                Map<String, RefActaTraitementsProduitRow> refPhytoInputByIds = loadRefActaTraitementsProduitByIds(connection, inputRows);

                Map<InputPrice, InputPrice> uniquePrices = new HashMap<>();

                Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId = persistPhytoInputPrices(
                        connection,
                        practicedInputRows,
                        FIND_PRACTICED_PRICES_QUERY,
                        refPhytoInputByIds,
                        true,
                        PriceCategory.PHYTO_TRAITMENT_INPUT_CATEGORIE,
                        InputPriceCategory.PHYTO_TRAITMENT_INPUT,
                        uniquePrices
                );
                priceByDomainIdObjectId.putAll(persistPhytoInputPrices(
                        connection,
                        effectiveInputRows,
                        FIND_DOMAIN_PRICES_QUERY,
                        refPhytoInputByIds,
                        false,
                        PriceCategory.PHYTO_TRAITMENT_INPUT_CATEGORIE,
                        InputPriceCategory.PHYTO_TRAITMENT_INPUT,
                        uniquePrices
                ));

                persistInputPrices(
                        connection,
                        InputPriceCategory.PHYTO_TRAITMENT_INPUT,
                        uniquePrices.values());

                persistPhytoInputs(
                        connection,
                        refPhytoInputByIds,
                        priceByDomainIdObjectId,
                        inputRows,
                        inputType,
                        domainDonePercent);
            }
            case AUTRE -> {

                Map<InputPrice, InputPrice> uniquePrices = new HashMap<>();

                Set<String> objectIds = inputRows.stream()
                        .map(AbstractInputRow::productname)
                        .filter(Objects::nonNull)
                        .map(pn -> StringUtils.stripAccents(pn).trim()
                                .replaceAll("\\s+", "_")
                                .replaceAll("'", "''")
                                .toLowerCase())
                        .collect(Collectors.toSet());

                Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId = persistOtherInputPrices(
                        connection,
                        practicedInputRows,
                        FIND_PRACTICED_PRICES_QUERY,
                        true,
                        objectIds,
                        uniquePrices
                );
                priceByDomainIdObjectId.putAll(persistOtherInputPrices(
                        connection,
                        effectiveInputRows,
                        FIND_DOMAIN_PRICES_QUERY,
                        false,
                        objectIds,
                        uniquePrices
                ));

                persistInputPrices(
                        connection,
                        InputPriceCategory.OTHER_INPUT,
                        uniquePrices.values());


                persisteDomainOtherInputsAndUsages(
                        inputRows,
                        connection,
                        inputType,
                        priceByDomainIdObjectId,
                        domainDonePercent);
            }
            case CARBURANT, IRRIGATION, PLAN_COMPAGNE, TRAITEMENT_SEMENCE -> {
                // THERE IS NONE
            }
            case EPANDAGES_ORGANIQUES -> {
                Map<String, RefFertiOrga> refInputByIds = loadRefFertiOrgaByIds(connection, inputRows);

                Map<InputPrice, InputPrice> uniquePrices = new HashMap<>();

                Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId = persistOrganicInputPrices(
                        connection,
                        practicedInputRows,
                        FIND_PRACTICED_PRICES_QUERY,
                        refInputByIds,
                        true,
                        uniquePrices
                );
                priceByDomainIdObjectId.putAll(persistOrganicInputPrices(
                        connection,
                        effectiveInputRows,
                        FIND_DOMAIN_PRICES_QUERY,
                        refInputByIds,
                        false,
                        uniquePrices
                ));

                persistInputPrices(
                        connection,
                        InputPriceCategory.ORGANIQUES_INPUT,
                        uniquePrices.values());

                persistDomainOrganicProductinputs(
                        connection,
                        refInputByIds,
                        priceByDomainIdObjectId,
                        inputRows,
                        inputType,
                        domainDonePercent);
            }
            case LUTTE_BIOLOGIQUE -> {
                Map<String, RefActaTraitementsProduitRow> refPhytoInputByIds = loadRefActaTraitementsProduitByIds(connection, inputRows);
                Map<InputPrice, InputPrice> uniquePrices = new HashMap<>();
                Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId = persistPhytoInputPrices(
                        connection,
                        practicedInputRows,
                        FIND_PRACTICED_PRICES_QUERY,
                        refPhytoInputByIds,
                        true,
                        PriceCategory.BIOLOGICAL_CONTROL_INPUT_CATEGORIE,
                        InputPriceCategory.BIOLOGICAL_CONTROL_INPUT,
                        uniquePrices);
                priceByDomainIdObjectId.putAll(persistPhytoInputPrices(
                        connection,
                        effectiveInputRows,
                        FIND_DOMAIN_PRICES_QUERY,
                        refPhytoInputByIds,
                        false,
                        PriceCategory.BIOLOGICAL_CONTROL_INPUT_CATEGORIE,
                        InputPriceCategory.BIOLOGICAL_CONTROL_INPUT,
                        uniquePrices));

                persistInputPrices(
                        connection,
                        InputPriceCategory.BIOLOGICAL_CONTROL_INPUT,
                        uniquePrices.values());

                persistBiologicalProductInputs(
                        connection,
                        refPhytoInputByIds,
                        priceByDomainIdObjectId,
                        inputRows,
                        inputType,
                        domainDonePercent);

            }
            case POT -> {

                Map<InputPrice, InputPrice> uniquePrices = new HashMap<>();

                Map<String, RefPot> refPotByIds = loadRefPotByIds(connection, inputRows);

                Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId = persistPotInputPrices(
                        connection,
                        practicedInputRows,
                        FIND_PRACTICED_PRICES_QUERY,
                        refPotByIds,
                        true,
                        uniquePrices
                );
                priceByDomainIdObjectId.putAll(persistPotInputPrices(
                        connection,
                        effectiveInputRows,
                        FIND_DOMAIN_PRICES_QUERY,
                        refPotByIds,
                        false,
                        uniquePrices
                ));

                persistInputPrices(
                        connection,
                        InputPriceCategory.POT_INPUT,
                        uniquePrices.values());

                persistPotInputs(
                        connection,
                        refPotByIds,
                        priceByDomainIdObjectId,
                        inputRows,
                        inputType,
                        domainDonePercent);
            }
            case SEMIS -> {

                Map<String, RefActaTraitementsProduitRow> refPhytoInputByIds = loadRefActaTraitementsProduitByIds(connection, inputRows);

                Map<InputPrice, InputPrice> uniquePrices = new HashMap<>();
                Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId = persistPhytoInputPrices(
                        connection,
                        practicedInputRows,
                        FIND_PRACTICED_PRICES_QUERY,
                        refPhytoInputByIds,
                        true,
                        PriceCategory.SEEDING_TREATMENT_INPUT_CATEGORIE,
                        InputPriceCategory.SEEDING_TREATMENT_INPUT,
                        uniquePrices);
                priceByDomainIdObjectId.putAll(persistPhytoInputPrices(
                        connection,
                        effectiveInputRows,
                        FIND_DOMAIN_PRICES_QUERY,
                        refPhytoInputByIds,
                        false,
                        PriceCategory.SEEDING_TREATMENT_INPUT_CATEGORIE,
                        InputPriceCategory.SEEDING_TREATMENT_INPUT,
                        uniquePrices));

                persistInputPrices(
                        connection,
                        InputPriceCategory.SEEDING_TREATMENT_INPUT,
                        uniquePrices.values());

                persistSeedingTreatmentInputs(
                        connection,
                        refPhytoInputByIds,
                        priceByDomainIdObjectId,
                        inputRows,
                        speciesInputForProductByActionId,
                        speciesUsageForProductByActionId,
                        domainDonePercent);

            }
            case SUBSTRAT -> {

                Map<InputPrice, InputPrice> uniquePrices = new HashMap<>();

                Map<String, RefSubstrate> refSubstrateByIds = loadRefSubstratByIds(connection, inputRows);

                Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId = persistSubstrateInputPrices(
                        connection,
                        practicedInputRows,
                        FIND_PRACTICED_PRICES_QUERY,
                        refSubstrateByIds,
                        true,
                        uniquePrices
                );
                priceByDomainIdObjectId.putAll(persistSubstrateInputPrices(
                        connection,
                        effectiveInputRows,
                        FIND_DOMAIN_PRICES_QUERY,
                        refSubstrateByIds,
                        false,
                        uniquePrices
                ));

                persistInputPrices(
                        connection,
                        InputPriceCategory.SUBSTRATE_INPUT,
                        uniquePrices.values());

                persistSubstrateInputs(
                        connection,
                        refSubstrateByIds,
                        priceByDomainIdObjectId,
                        inputRows,
                        inputType,
                        domainDonePercent);
            }
        }

    }

    protected void persisteDomainOtherInputsAndUsages(
            Collection<AbstractInputRow> inputRows,
            Connection connection,
            InputType inputType,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            int domainDonePercent) {

        Map<String, String> inputProductCodes = new HashMap<>();
        Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId = new HashMap<>();

        try (PreparedStatement persistProductInputUsageStatement = connection.prepareStatement(INSERT_INTO_DOMAIN_OTHER_PRODUCT_INPUT_USAGE_QUERY)) {
            try (PreparedStatement persistAbstractInputUsageStatement = connection.prepareStatement(INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY)) {
                try (PreparedStatement persistDomainProductInputStatement = connection.prepareStatement(INSERT_INTO_DOMAIN_OTHER_INPUT_QUERY)) {
                    try (PreparedStatement persistDomainAbstractInputStatement = connection.prepareStatement(INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY)) {

                        Map<String, Set<OtherProductComment>> otherProductCommentByInputIds = new HashMap<>();
                        for (AbstractInputRow inputRow : inputRows) {

                            final OtherProductInputUnit productUnit = StringUtils.isBlank(inputRow.otherproductinputunit()) ? OtherProductInputUnit.KG_HA : OtherProductInputUnit.valueOf(inputRow.otherproductinputunit());
                            String objectId = StringUtils.stripAccents(StringUtils.trimToEmpty(inputRow.productname))
                                    .replaceAll("\\s+", "_")
                                    .replaceAll("'", "''")
                                    .toLowerCase();
                            String inputKey = objectId + DomainInputStockUnitService.KEY_SEPARATOR + productUnit.name();
                            Pair<String, String> domainIdAndInputKey = Pair.of(inputRow.domainId(), inputKey);
                            String domainInputId = domainIdInputKeyToDomainInputId.get(domainIdAndInputKey);

                            String otherProductUnit = inputRow.otherproductinputunit != null ? t(OtherProductInputUnit.class.getName() + "." + inputRow.otherproductinputunit) : "";
                            OtherProductComment otherProductComment = new OtherProductComment(
                                    inputRow.qtavg, otherProductUnit
                            );

                            if (ifInputNotExistsOnDomain(domainInputId)) {

                                String domainInputCodeKey = inputKey + "_" + inputRow.domainCode();
                                String inputCode = getDomainInputCode(inputProductCodes, domainInputCodeKey);

                                String priceTopiaId = null;
                                InputPrice inputPrice = priceByDomainIdObjectId.get(inputRow);

                                if (inputPrice != null) {
                                    priceTopiaId = inputPrice.getTopiaId();
                                }

                                domainInputId = DomainOtherInput.class.getName() + "_" + UUID.randomUUID();
                                domainIdInputKeyToDomainInputId.put(domainIdAndInputKey, domainInputId);

                                persistAbstractDomainInput(
                                        persistDomainAbstractInputStatement,
                                        domainInputId,
                                        inputKey,
                                        StringUtils.trimToEmpty(inputRow.productname()),
                                        inputRow.productname(),
                                        "",
                                        InputType.AUTRE,
                                        priceTopiaId,
                                        inputCode,
                                        inputRow.domainId());

                                String commentParts = "";
                                if (inputRow.qtavg != null || StringUtils.isNotBlank(inputRow.otherproductinputunit)) {
                                    commentParts += inputRow.qtavg != null ? String.valueOf(inputRow.qtavg) : "";
                                    commentParts += StringUtils.isNotBlank(inputRow.otherproductinputunit) ? otherProductUnit : "";
                                }

                                Set<OtherProductComment> comments = new HashSet<>();
                                comments.add(otherProductComment);
                                otherProductCommentByInputIds.put(domainInputId, comments);
                                // INSERT INTO domainotherinput (topiaid, refinput, usageunit, comment)
                                persistDomainProductInputStatement.setString(1, domainInputId);
                                persistDomainProductInputStatement.setString(2, DEFAULT_REF_OTHER_INPUT_ID);
                                persistDomainProductInputStatement.setString(3, productUnit.name());
                                persistDomainProductInputStatement.setString(4, commentParts);
                                persistDomainProductInputStatement.addBatch();

                            } else {
                                Set<OtherProductComment> comments = otherProductCommentByInputIds.get(domainInputId);
                                comments.add(otherProductComment);
                            }

                            String inputUsageId = OtherProductInputUsage.class.getName() + "_" + UUID.randomUUID();
                            persistAbstractInputUsage(
                                    persistAbstractInputUsageStatement,
                                    inputRow,
                                    inputUsageId,
                                    inputType,
                                    "Autre");
//                                                                            1         2                     3                     4                   5                     6                                   7                            8                           9                 10                        11
//                                     INSERT INTO otherproductinputusage (topiaid, domainotherinput, biologicalcontrolaction, harvestingaction, irrigationaction, maintenancepruningvinesaction, mineralfertilizersspreadingaction, organicfertilizersspreadingaction, otheraction, pesticidesspreadingaction, seedingaction)
                            persistProductInputUsageStatement.setString(1, inputUsageId);
                            persistProductInputUsageStatement.setString(2, domainInputId);
                            persistProductInputUsageStatement.setString(3, inputRow.biologicalcontrolaction);
                            persistProductInputUsageStatement.setString(4, inputRow.harvestingaction);
                            persistProductInputUsageStatement.setString(5, inputRow.irrigationaction);
                            persistProductInputUsageStatement.setString(6, inputRow.maintenancepruningvinesaction);
                            persistProductInputUsageStatement.setString(7, inputRow.mineralfertilizersspreadingaction);
                            persistProductInputUsageStatement.setString(8, inputRow.organicfertilizersspreadingaction);
                            persistProductInputUsageStatement.setString(9, inputRow.otheraction);
                            persistProductInputUsageStatement.setString(10, inputRow.pesticidesspreadingaction);
                            persistProductInputUsageStatement.setString(11, inputRow.seedingaction);
                            persistProductInputUsageStatement.addBatch();

                        }
                        LOG.info("For current part processed " + InputType.AUTRE.name() + " execute " + INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                        persistDomainAbstractInputStatement.executeBatch();
                        LOG.info("For current part processed " + InputType.AUTRE.name() + " execute " + INSERT_INTO_DOMAIN_OTHER_INPUT_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                        persistDomainProductInputStatement.executeBatch();
                        LOG.info("For current part processed " + InputType.AUTRE.name() + " execute " + INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                        persistAbstractInputUsageStatement.executeBatch();
                        LOG.info("For current part processed " + InputType.AUTRE.name() + " execute " + INSERT_INTO_DOMAIN_OTHER_PRODUCT_INPUT_USAGE_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                        persistProductInputUsageStatement.executeBatch();
                        LOG.info("For current part processed " + InputType.AUTRE.name() + " END BATCH, domain part " + domainDonePercent + "%");

                        try (PreparedStatement updateComment = connection.prepareStatement("UPDATE domainotherinput SET comment = ? WHERE topiaid = ?")) {
                            for (Map.Entry<String, Set<OtherProductComment>> otherProductCommentByInputForInputId : otherProductCommentByInputIds.entrySet()) {
                                String inputId = otherProductCommentByInputForInputId.getKey();
                                Set<OtherProductComment> comments = otherProductCommentByInputForInputId.getValue();
                                if (comments.size() > 1) {
                                    List<String> allCommentParts = new ArrayList<>();
                                    for (OtherProductComment comment : comments) {
                                        String commentParts = "";
                                        if (comment.qtavg != null || StringUtils.isNotBlank(comment.otherproductinputunit)) {
                                            commentParts += comment.qtavg != null ? String.valueOf(comment.qtavg) : "";
                                            commentParts += StringUtils.isNotBlank(comment.otherproductinputunit) ? comment.otherproductinputunit : "";
                                            allCommentParts.add(commentParts);
                                        }
                                    }
                                    String allComments = String.join(", ", allCommentParts);

                                    updateComment.setString(1, allComments);
                                    updateComment.setString(2, inputId);
                                    updateComment.addBatch();

                                }
                            }
                            LOG.info("For current part processed " + InputType.AUTRE.name() + " execute  UPDATE domainotherinput SET comment = ? WHERE topiaid = ? BATCH, domain part " + domainDonePercent + "%");
                            updateComment.executeBatch();
                            LOG.info("For current part processed " + InputType.AUTRE.name() + " END BATCH, domain part " + domainDonePercent + "%");
                        }

                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    record CpsDto(
            String topiaid,
            String code,
            String code_espece_botanique,
            String code_qualifiant_aee,

            String code_destination_aee,
            String variety
    ){}


    private MultiValuedMap<String, IrrigationActionRow> executeGetIrrigActionQueries(Connection connection) {

        MultiValuedMap<String, IrrigationActionRow> result = new HashSetValuedHashMap<>();
        ResultSet rs;
        try (Statement statement = connection.createStatement()) {
            rs = statement.executeQuery(GET_IRRIG_ACTION_SPECIES_SEEDING_ACTION_QUERY);
            while (rs.next()) {
                IrrigationActionRow irrigationActionRow = new IrrigationActionRow();
                irrigationActionRow.setActionId(rs.getString("actionId"));
                irrigationActionRow.setDomainId(rs.getString("domainid"));
                irrigationActionRow.setDomainCode(rs.getString("domaincode"));
                irrigationActionRow.setDomainCampaign(rs.getInt("domaincampaign"));
                irrigationActionRow.setWaterquantitymin((Double) rs.getObject("waterquantitymin"));
                irrigationActionRow.setWaterquantitymax((Double) rs.getObject("waterquantitymax"));
                irrigationActionRow.setWaterquantityaverage(rs.getDouble("waterquantityaverage"));
                irrigationActionRow.setWaterquantitymedian((Double) rs.getObject("waterquantitymedian"));
                result.put(irrigationActionRow.getDomainCode(), irrigationActionRow);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private List<SeedingActionAndSpeciesRow> executeGetSeedingInputQueries(Connection connection) {

        List<SeedingActionAndSpeciesRow> result = new ArrayList<>();
        ResultSet rs;
        try (Statement statement = connection.createStatement()) {
            statement.execute(CREATE_SEEING_ACTION_SPECIES_SEEDING_ACTION_TABLE_QUERY);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try (Statement statement = connection.createStatement()) {
            rs = statement.executeQuery(GET_SEEING_ACTION_SPECIES_SEEDING_ACTION_QUERY);
            while (rs.next()) {
                SeedingActionAndSpeciesRow seedingActionAndSpeciesRow = new SeedingActionAndSpeciesRow();
                seedingActionAndSpeciesRow.setActionId(rs.getString("actionId"));
                seedingActionAndSpeciesRow.setMainAction(rs.getString("mainaction"));
                seedingActionAndSpeciesRow.setComment(rs.getString("comment"));
                seedingActionAndSpeciesRow.setToolsCouplingCode(rs.getString("toolscouplingcode"));
                seedingActionAndSpeciesRow.setPracticedIntervention(rs.getString("practicedintervention"));
                seedingActionAndSpeciesRow.setEffectiveIntervention(rs.getString("effectiveintervention"));
                String yealdUnitName = rs.getString("yealdunit");
                if (StringUtils.isNotBlank(yealdUnitName)) {
                    seedingActionAndSpeciesRow.setYealdUnit(YealdUnit.valueOf(yealdUnitName));
                }

                String seedTypeName = rs.getString("seedtype");
                if (StringUtils.isNotBlank(seedTypeName)) {
                    seedingActionAndSpeciesRow.setSeedType(SeedType.valueOf(seedTypeName));
                } else {
                    seedingActionAndSpeciesRow.setSeedType(SeedType.SEMENCES_CERTIFIEES);
                }

                seedingActionAndSpeciesRow.setYealdTarget((Double) rs.getObject("yealdtarget"));
                seedingActionAndSpeciesRow.setIntermediatecrop(rs.getBoolean("isIntermediatecrop"));
                seedingActionAndSpeciesRow.setInterventionCropId(rs.getString("interventionCropId"));
                seedingActionAndSpeciesRow.setInterventionCropCode(rs.getString("interventionCropCode"));
                seedingActionAndSpeciesRow.setInterventionIntermediateCropId(rs.getString("interventionIntermediateCropId"));
                seedingActionAndSpeciesRow.setInterventionIntermediateCropCode(rs.getString("interventionIntermediateCropCode"));
                seedingActionAndSpeciesRow.setDomainId(rs.getString("domainid"));
                seedingActionAndSpeciesRow.setDomainCode(rs.getString("domaincode"));
                seedingActionAndSpeciesRow.setDomainCampaign(rs.getInt("domaincampaign"));
                seedingActionAndSpeciesRow.setPsCampaigns(rs.getString("psCampaigns"));
                seedingActionAndSpeciesRow.setPracticedSystemId(rs.getString("practicedsystem"));
                seedingActionAndSpeciesRow.setGsCode(rs.getString("gsCode"));

                seedingActionAndSpeciesRow.setSeedingActionSpeciesCropId(rs.getString("seedingActionSpeciesCropId"));
                seedingActionAndSpeciesRow.setSeedingActionSpeciesId(rs.getString("seedingActionSpeciesId"));
                seedingActionAndSpeciesRow.setSeedingActionSpeciesQuantity((Double) rs.getObject("seedingActionSpeciesQuantity"));
                seedingActionAndSpeciesRow.setSeedingActionSpeciesCPSCode(rs.getString("seedingActionSpeciesCPS_Code"));
                seedingActionAndSpeciesRow.setSeedingActionSpeciesCPSId(rs.getString("seedingActionSpeciesCPS_Id"));
                seedingActionAndSpeciesRow.setSeedingActionSpeciesDeepness((Double) rs.getObject("seedingActionSpeciesDeepness"));
                seedingActionAndSpeciesRow.setSeedingActionSpeciesTreatment(rs.getBoolean("seedingActionSpeciesTreatment"));
                seedingActionAndSpeciesRow.setSeedingActionSpeciesBiologicalSeedInoculation(rs.getBoolean("seedingActionSpeciesBiologicalSeedInoculation"));
                String usageUnitName = rs.getString("usageUnit");
                if (usageUnitName != null) {
                    seedingActionAndSpeciesRow.setSeedingActionSpeciesUsageUnit(SeedPlantUnit.valueOf(usageUnitName));
                }
                seedingActionAndSpeciesRow.setType_agriculture_id(rs.getInt("type_agriculture_id"));
                seedingActionAndSpeciesRow.setCrop_name(rs.getString("crop_name"));
                seedingActionAndSpeciesRow.setIntermediate_crop_name(rs.getString("intermediate_crop_name"));
                seedingActionAndSpeciesRow.setLibelle_espece_botanique(rs.getString("libelle_espece_botanique"));
                seedingActionAndSpeciesRow.setLibelle_qualifiant_aee(rs.getString("libelle_qualifiant_aee"));
                seedingActionAndSpeciesRow.setLibelle_type_saisonnier_aee(rs.getString("libelle_type_saisonnier_aee"));
                seedingActionAndSpeciesRow.setLibelle_destination_aee(rs.getString("libelle_destination_aee"));
                seedingActionAndSpeciesRow.setLibelle_variete((rs.getString("libelle_variete")));

                result.add(seedingActionAndSpeciesRow);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private void persistDomainOrganicProductinputs(
            Connection connection,
            Map<String, RefFertiOrga> refInputByIds,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Collection<AbstractInputRow> inputRows,
            InputType inputType, int domainDonePercent) {
        Map<String, String> productCodes = new HashMap<>();

        Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId = new HashMap<>();

        AtomicInteger nbInputMigrated = new AtomicInteger();
        try (PreparedStatement persistProductInputUsageStatement = connection.prepareStatement(INSERT_INTO_ORGANIC_PRODUCT_INPUT_USAGE_QUERY)) {
            try (PreparedStatement persistAbstractInputUsageStatement = connection.prepareStatement(INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY)) {
                try (PreparedStatement persistDomainProductInputStatement = connection.prepareStatement(INSERT_INTO_DOMAIN_ORGANIC_PRODUCT_INPUT_QUERY)) {
                    try (PreparedStatement persistDomainAbstractInputStatement = connection.prepareStatement(INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY)) {
                        Iterable<List<AbstractInputRow>> inputRowParts = Iterables.partition(inputRows, PERSIST_INPUT_CHUNK_SIZE);
                        for (List<AbstractInputRow> inputRowPart : inputRowParts) {
                            for (AbstractInputRow inputRow : inputRowPart) {

                                persistOrganicRow(
                                        refInputByIds,
                                        productCodes,
                                        priceByDomainIdObjectId,
                                        domainIdInputKeyToDomainInputId,

                                        persistProductInputUsageStatement,
                                        persistAbstractInputUsageStatement,
                                        persistDomainProductInputStatement,
                                        persistDomainAbstractInputStatement,
                                        inputRow,
                                        inputType);
                                nbInputMigrated.addAndGet(1);
                            }
                            LOG.info("For current part processed " + (nbInputMigrated.get()*100)/inputRows.size() + "%  Migrate " + inputType + ", domain part " + domainDonePercent + "%");
                        }
                        LOG.info("For current part processed " + inputType + " execute " + INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                        persistDomainAbstractInputStatement.executeBatch();
                    }
                    LOG.info("For current part processed " + inputType + " execute " + INSERT_INTO_DOMAIN_ORGANIC_PRODUCT_INPUT_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                    persistDomainProductInputStatement.executeBatch();
                }
                LOG.info("For current part processed " + inputType + " execute " + INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                persistAbstractInputUsageStatement.executeBatch();
            }
            LOG.info("For current part processed " + inputType + " execute " + INSERT_INTO_ORGANIC_PRODUCT_INPUT_USAGE_QUERY + " BATCH, domain part " + domainDonePercent + "%");
            persistProductInputUsageStatement.executeBatch();
            LOG.info("For current part processed " + inputType + " END BATCH, domain part " + domainDonePercent + "%");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void persistOrganicRow(
            Map<String, RefFertiOrga> refInputByIds,
            Map<String, String> inputProductCodes,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId,

            PreparedStatement persistProductInputUsageStatement,
            PreparedStatement persistAbstractInputUsageStatement,
            PreparedStatement persistDomainProductInputStatement,
            PreparedStatement persistDomainAbstractInputStatement,
            AbstractInputRow inputRow,
            InputType inputType) throws SQLException {

        final String refProductId = StringUtils.firstNonBlank(inputRow.organicproduct(), DEFAULT_REF_FERTI_ORGA_ID);
        RefFertiOrga refInput = refInputByIds.get(refProductId);

        final OrganicProductUnit productUnit = StringUtils.isBlank(inputRow.organicproductunit()) ? OrganicProductUnit.KG_HA : OrganicProductUnit.valueOf(inputRow.organicproductunit());

        // create a key for the input to find same inputs on same domain but other campaigns
        // if found the input will have the same code otherwise a new code will be generated
        String inputKey = DomainInputStockUnitService.getOrganicProductInputKey(
                refInput.getIdtypeeffluent(),
                productUnit,
                inputRow.n,
                inputRow.p2o5,
                inputRow.k2o,
                refInput.getTeneur_Ferti_Orga_CaO(),
                refInput.getTeneur_Ferti_Orga_MgO(),
                refInput.getTeneur_Ferti_Orga_S(),
                false);

        Pair<String, String> domainIdAndInputKey = Pair.of(inputRow.domainId(), inputKey);
        String domainInputId = persistDomainOrganicProductInput(
                persistDomainAbstractInputStatement,
                persistDomainProductInputStatement,
                inputProductCodes,
                priceByDomainIdObjectId,
                domainIdInputKeyToDomainInputId,
                inputRow,
                refInput,
                inputKey,
                domainIdAndInputKey);

        String inputUsageId = OrganicProductInputUsage.class.getName() + "_" + UUID.randomUUID();

        persistAbstractInputUsage(
                persistAbstractInputUsageStatement,
                inputRow,
                inputUsageId,
                inputType,
                refInput.getLibelle());

        persistOrganicInputUsage(
                persistProductInputUsageStatement,
                domainInputId,
                inputUsageId,
                inputRow.actionid);
    }

    protected void persistPotRow(
            Map<String, RefPot> refInputByIds,
            Map<String, String> inputProductCodes,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId,

            PreparedStatement persistProductInputUsageStatement,
            PreparedStatement persistAbstractInputUsageStatement,
            PreparedStatement persistDomainProductInputStatement,
            PreparedStatement persistDomainAbstractInputStatement,
            AbstractInputRow inputRow,
            InputType inputType) throws SQLException {

        final String refProductId = StringUtils.firstNonBlank(inputRow.pot(), DEFAULT_REF_POT_ID);
        RefPot refInput = refInputByIds.get(refProductId);

        final PotInputUnit productUnit = StringUtils.isBlank(inputRow.potinputunit()) ?
                PotInputUnit.POTS_M2 : PotInputUnit.valueOf(inputRow.potinputunit());

        // create a key for the input to find same inputs on same domain but other campaigns
        // if found the input will have the same code otherwise a new code will be generated
        String inputKey = DomainInputStockUnitService.getPotInputKey(
                refInput,
                productUnit);

        Pair<String, String> domainIdAndInputKey = Pair.of(inputRow.domainId(), inputKey);
        String domainInputId = persistDomainPotInput(
                persistDomainAbstractInputStatement,
                persistDomainProductInputStatement,
                inputProductCodes,
                priceByDomainIdObjectId,
                domainIdInputKeyToDomainInputId,
                inputRow,
                refInput,
                inputKey,
                domainIdAndInputKey,
                productUnit);

        String inputUsageId = PotInputUsage.class.getName() + "_" + UUID.randomUUID();

        persistAbstractInputUsage(
                persistAbstractInputUsageStatement,
                inputRow,
                inputUsageId,
                inputType,
                refInput.getCaracteristic1());

        persistPotInputUsage(
                persistProductInputUsageStatement,
                domainInputId,
                inputUsageId,
                inputRow.actionid);
    }

    protected void persistSubstrateRow(
            Map<String, RefSubstrate> refInputByIds,
            Map<String, String> inputProductCodes,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId,

            PreparedStatement persistProductInputUsageStatement,
            PreparedStatement persistAbstractInputUsageStatement,
            PreparedStatement persistDomainProductInputStatement,
            PreparedStatement persistDomainAbstractInputStatement,
            AbstractInputRow inputRow,
            InputType inputType) throws SQLException {

        final String refProductId = StringUtils.firstNonBlank(inputRow.substrate(), DEFAULT_REF_SUBSTRAT_ID);
        RefSubstrate refInput = refInputByIds.get(refProductId);

        final SubstrateInputUnit productUnit = StringUtils.isBlank(inputRow.substrateinputunit()) ?
                SubstrateInputUnit.KG_HA : SubstrateInputUnit.valueOf(inputRow.substrateinputunit());

        // create a key for the input to find same inputs on same domain but other campaigns
        // if found the input will have the same code otherwise a new code will be generated
        String inputKey = DomainInputStockUnitService.getSubstratInputKey(
                refInput,
                productUnit);

        Pair<String, String> domainIdAndInputKey = Pair.of(inputRow.domainId(), inputKey);
        String domainInputId = persistDomainSubstrateInput(
                persistDomainAbstractInputStatement,
                persistDomainProductInputStatement,
                inputProductCodes,
                priceByDomainIdObjectId,
                domainIdInputKeyToDomainInputId,
                inputRow,
                refInput,
                inputKey,
                domainIdAndInputKey,
                productUnit);

        String inputUsageId = SubstrateInputUsage.class.getName() + "_" + UUID.randomUUID();

        persistAbstractInputUsage(
                persistAbstractInputUsageStatement,
                inputRow,
                inputUsageId,
                inputType,
                refInput.getCaracteristic1());

        persistSubstrateInputUsage(
                persistProductInputUsageStatement,
                domainInputId,
                inputUsageId,
                inputRow.actionid);
    }

    private String persistDomainOrganicProductInput(
            PreparedStatement persistDomainAbstractInputStatement,
            PreparedStatement persistDomainProductInputStatement,
            Map<String, String> inputProductCodes,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId,
            AbstractInputRow inputRow,
            RefFertiOrga refInput,
            String inputKey,
            Pair<String, String> domainIdAndInputKey) throws SQLException {

        String domainInputId = domainIdInputKeyToDomainInputId.get(domainIdAndInputKey);
        if (ifInputNotExistsOnDomain(domainInputId)) {

            String domainInputCodeKey = inputKey + "_" + inputRow.domainCode();
            String inputCode = getDomainInputCode(inputProductCodes, domainInputCodeKey);

            String objectId = InputPriceService.GET_REF_FERTI_ORGA_OBJECT_ID.apply(refInput);

            String priceTopiaId = null;
            InputPrice inputPrice = priceByDomainIdObjectId.get(inputRow);

            if (inputPrice != null) {
                priceTopiaId = inputPrice.getTopiaId();
            }

            domainInputId = DomainOrganicProductInput.class.getName() + "_" + UUID.randomUUID();
            domainIdInputKeyToDomainInputId.put(domainIdAndInputKey, domainInputId);

            persistAbstractDomainInput(
                    persistDomainAbstractInputStatement, domainInputId,
                    inputKey,
                    StringUtils.firstNonBlank(inputRow.productname(), refInput.getLibelle()),
                    inputRow.productname(), objectId,
                    InputType.EPANDAGES_ORGANIQUES,
                    priceTopiaId,
                    inputCode, inputRow.domainId());

            final OrganicProductUnit productUnit = StringUtils.isBlank(inputRow.organicproductunit()) ? OrganicProductUnit.KG_HA : OrganicProductUnit.valueOf(inputRow.organicproductunit());
            final String refProductId = StringUtils.firstNonBlank(inputRow.organicproduct(), DEFAULT_REF_FERTI_ORGA_ID);
            //                                          1        2         3        4   5     6    7   8    9   10
            // INSERT INTO domainorganicproductinput (topiaid, refinput, usageunit, n, p2o5, k2o, cao, mgo, s, organic)
            persistDomainProductInputStatement.setString(1, domainInputId);
            persistDomainProductInputStatement.setString(2, refProductId);
            persistDomainProductInputStatement.setString(3, productUnit.name());
            Double n = inputRow.n;
            setSqlDoubleOrNull(4, n, persistDomainProductInputStatement);
            Double p2o5 = inputRow.p2o5;
            setSqlDoubleOrNull(5, p2o5, persistDomainProductInputStatement);
            Double k2o = inputRow.k2o;
            setSqlDoubleOrNull(6, k2o, persistDomainProductInputStatement);
            persistDomainProductInputStatement.setNull(7, Types.DOUBLE);
            persistDomainProductInputStatement.setNull(8, Types.DOUBLE);
            persistDomainProductInputStatement.setNull(9, Types.DOUBLE);
            persistDomainProductInputStatement.setBoolean(10, false);
            persistDomainProductInputStatement.addBatch();

        }
        return domainInputId;
    }

    protected String persistDomainPotInput(
            PreparedStatement persistDomainAbstractInputStatement,
            PreparedStatement persistDomainProductInputStatement,
            Map<String, String> inputProductCodes,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId,
            AbstractInputRow inputRow,
            RefPot refInput,
            String inputKey,
            Pair<String, String> domainIdAndInputKey,
            PotInputUnit productUnit) throws SQLException {

        String domainInputId = domainIdInputKeyToDomainInputId.get(domainIdAndInputKey);
        if (ifInputNotExistsOnDomain(domainInputId)) {

            String domainInputCodeKey = inputKey + "_" + inputRow.domainCode();
            String inputCode = getDomainInputCode(inputProductCodes, domainInputCodeKey);

            String objectId = InputPriceService.GET_REF_POT_OBJECT_ID.apply(refInput);

            String priceTopiaId = getInputPriceIdIfExists(priceByDomainIdObjectId, inputRow, objectId);

            domainInputId = DomainPotInput.class.getName() + "_" + UUID.randomUUID();
            domainIdInputKeyToDomainInputId.put(domainIdAndInputKey, domainInputId);

            persistAbstractDomainInput(
                    persistDomainAbstractInputStatement, domainInputId,
                    inputKey,
                    StringUtils.firstNonBlank(inputRow.productname(), refInput.getCaracteristic1()),
                    inputRow.productname(), objectId,
                    InputType.POT,
                    priceTopiaId,
                    inputCode, inputRow.domainId());

            final String refProductId = StringUtils.firstNonBlank(inputRow.pot(), DEFAULT_REF_POT_ID);
            //                               1        2         3
            // INSERT INTO domainpotinput (topiaid, refinput, usageunit)
            persistDomainProductInputStatement.setString(1, domainInputId);
            persistDomainProductInputStatement.setString(2, refProductId);
            persistDomainProductInputStatement.setString(3, productUnit.name());
            persistDomainProductInputStatement.addBatch();

        }
        return domainInputId;
    }

    protected String persistDomainSubstrateInput(
            PreparedStatement persistDomainAbstractInputStatement,
            PreparedStatement persistDomainProductInputStatement,
            Map<String, String> inputProductCodes,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId,
            AbstractInputRow inputRow,
            RefSubstrate refInput,
            String inputKey,
            Pair<String, String> domainIdAndInputKey,
            SubstrateInputUnit productUnit) throws SQLException {

        String domainInputId = domainIdInputKeyToDomainInputId.get(domainIdAndInputKey);
        if (ifInputNotExistsOnDomain(domainInputId)) {

            String domainInputCodeKey = inputKey + "_" + inputRow.domainCode();
            String inputCode = getDomainInputCode(inputProductCodes, domainInputCodeKey);

            String objectId = InputPriceService.GET_REF_SUBSTRAT_OBJECT_ID.apply(refInput);

            String priceTopiaId = getInputPriceIdIfExists(priceByDomainIdObjectId, inputRow, objectId);

            domainInputId = DomainSubstrateInput.class.getName() + "_" + UUID.randomUUID();
            domainIdInputKeyToDomainInputId.put(domainIdAndInputKey, domainInputId);

            persistAbstractDomainInput(
                    persistDomainAbstractInputStatement, domainInputId,
                    inputKey,
                    StringUtils.firstNonBlank(inputRow.productname(), refInput.getCaracteristic1()),
                    inputRow.productname(), objectId,
                    InputType.SUBSTRAT,
                    priceTopiaId,
                    inputCode, inputRow.domainId());

            final String refProductId = StringUtils.firstNonBlank(inputRow.substrate(), DEFAULT_REF_SUBSTRAT_ID);
            //                               1        2         3
            // INSERT INTO domainpotinput (topiaid, refinput, usageunit)
            persistDomainProductInputStatement.setString(1, domainInputId);
            persistDomainProductInputStatement.setString(2, refProductId);
            persistDomainProductInputStatement.setString(3, productUnit.name());
            persistDomainProductInputStatement.addBatch();

        }
        return domainInputId;
    }

    protected Map<Pair<String, String>, InputPrice> persistIrrigationInputPrices(
            Connection connection) {

        // Pair.of(domainId, objectId) -> AbstractInputPrice
        Map<Pair<String, String>, InputPrice> inputPriceByDomainIdObjectId = new HashMap<>();

        try (Statement findPriceStatement = connection.createStatement()) {
            ResultSet rs = findPriceStatement.executeQuery(V3_0_0_9__11484_input_storage.FIND_EFFECTIVE_IRRIGATION_PRICES_QUERY);
            bindPriceResultSetToInputPrice(inputPriceByDomainIdObjectId, rs, InputPriceService.GET_IRRIGATION_OBJECT_ID, InputPriceCategory.IRRIGATION_INPUT, InputType.IRRIGATION);
        } catch (SQLException e) {
            LOG.error(V3_0_0_9__11484_input_storage.FIND_EFFECTIVE_IRRIGATION_PRICES_QUERY);
            throw new RuntimeException(e);
        }

        try (Statement findPriceStatement = connection.createStatement()) {
            ResultSet rs = findPriceStatement.executeQuery(V3_0_0_9__11484_input_storage.FIND_PRACTICED_IRRIGATION_PRICES_QUERY);
            bindPriceResultSetToInputPrice(inputPriceByDomainIdObjectId, rs, InputPriceService.GET_IRRIGATION_OBJECT_ID, InputPriceCategory.IRRIGATION_INPUT, InputType.IRRIGATION);
        } catch (SQLException e) {
            LOG.error(V3_0_0_9__11484_input_storage.FIND_PRACTICED_IRRIGATION_PRICES_QUERY);
            throw new RuntimeException(e);
        }

        persistInputPrices(
                connection,
                InputPriceCategory.IRRIGATION_INPUT,
                inputPriceByDomainIdObjectId.values());

        return inputPriceByDomainIdObjectId;
    }

    private Map<Pair<String, String>, InputPrice> persistFuelInputPrices(
            Connection connection) {

        Map<Pair<String, String>, InputPrice> inputPriceByDomainIdObjectId = new HashMap<>();

        try (Statement findPriceStatement = connection.createStatement()) {
            ResultSet rs = findPriceStatement.executeQuery(FIND_EFFECTIVE_FUEL_PRICES_QUERY);
            bindPriceResultSetToInputPrice(inputPriceByDomainIdObjectId, rs, InputPriceService.GET_FUEL_OBJECT_ID, InputPriceCategory.FUEL_INPUT, InputType.CARBURANT);
        } catch (SQLException e) {
            LOG.error(FIND_EFFECTIVE_FUEL_PRICES_QUERY);
            throw new RuntimeException(e);
        }

        try (Statement findPriceStatement = connection.createStatement()) {
            ResultSet rs = findPriceStatement.executeQuery(FIND_PRACTICED_FUEL_PRICES_QUERY);
            bindPriceResultSetToInputPrice(inputPriceByDomainIdObjectId, rs, InputPriceService.GET_FUEL_OBJECT_ID, InputPriceCategory.FUEL_INPUT, InputType.CARBURANT);
        } catch (SQLException e) {
            LOG.error(FIND_PRACTICED_FUEL_PRICES_QUERY);
            throw new RuntimeException(e);
        }

        persistInputPrices(
                connection,
                InputPriceCategory.FUEL_INPUT,
                inputPriceByDomainIdObjectId.values());

        return inputPriceByDomainIdObjectId;
    }

    protected void bindPriceResultSetToInputPrice(
            Map<Pair<String, String>, InputPrice> inputPriceByDomainIdObjectId,
            ResultSet rs,
            Function<Integer, String> getObjectId,
            InputPriceCategory priceCategory,
            InputType inputType) throws SQLException {

        while (rs.next()) {
            String domainId = rs.getString("domain");
            Integer campaign = rs.getInt("domaincampaign");
            final String objectId = getObjectId.apply(campaign);
            InputPrice inputPrice = new InputPrice();
            inputPrice.setDeprecatedTopiaId(rs.getString("topiaid"));
            inputPrice.setTopiaVersion(0);
            inputPrice.setTopiaDiscriminator(InputPriceImpl.class.getName());
            inputPrice.setObjectId(objectId);
            inputPrice.setPriceUnit(PriceUnit.valueOf(rs.getString("priceunit")));
            inputPrice.setDomainId(domainId);
            inputPrice.setDomainCode(rs.getString("domaincode"));
            inputPrice.setDisplayName(inputType.name());
            inputPrice.setPrice(rs.getDouble("price"));
            inputPrice.setSourceUnit(FuelUnit.L_HA.name());
            inputPrice.setCategory(priceCategory);
            inputPrice.setPhytoProductType(null);
            final Pair<String, String> key = Pair.of(domainId, objectId);
            inputPriceByDomainIdObjectId.putIfAbsent(key, inputPrice);
        }
    }

    private void persistFuelInputs(
            Connection connection,
            Map<Pair<String, String>, InputPrice> priceByDomainIdObjectId,
            String unitName,
            String inputName) {

        Map<String, String> productCodes = new HashMap<>();
        Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId = new HashMap<>();

        AtomicInteger nbInputMigrated = new AtomicInteger();
        try (PreparedStatement persistDomainInputStatement = connection.prepareStatement(V3_0_0_9__11484_input_storage.INSERT_INTO_DOMAIN_FUEL_INPUT_QUERY)) {
            try (PreparedStatement persistAbstractDomainInputStatement = connection.prepareStatement(INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY)) {
                int nbInputToCreate = priceByDomainIdObjectId.size();
                for (Map.Entry<Pair<String, String>, InputPrice> priceRowByDomainIdObjectId : priceByDomainIdObjectId.entrySet()) {
                    InputPrice priceRow = priceRowByDomainIdObjectId.getValue();

                    Pair<String, String> domainIdAndInputKey = Pair.of(priceRow.getDomainId(), unitName);
                    String domainInputId = domainIdInputKeyToDomainInputId.get(domainIdAndInputKey);
                    if (ifInputNotExistsOnDomain(domainInputId)) {

                        String inputKeyCode = unitName + "_" + priceRow.getDomainCode();
                        String inputCode = getDomainInputCode(productCodes, inputKeyCode);

                        String priceTopiaId = priceRow.getTopiaId();

                        domainInputId = DomainFuelInputImpl.class.getName() + "_" + UUID.randomUUID();
                        domainIdInputKeyToDomainInputId.put(domainIdAndInputKey, domainInputId);

                        //                                              1        2               3           4       5        6          7        8         9
                        // INSERT INTO abstractdomaininputstockunit (topiaid, topiaversion, topiacreatedate, key, inputname, inputtype, domain, inputprice, code)

                        persistAbstractDomainInputStatement.setString(1, domainInputId);
                        persistAbstractDomainInputStatement.setLong(2, 0L);
                        persistAbstractDomainInputStatement.setDate(3, scriptDate);
                        persistAbstractDomainInputStatement.setString(4, unitName);
                        persistAbstractDomainInputStatement.setString(5, inputName);
                        persistAbstractDomainInputStatement.setString(6, InputType.CARBURANT.name());
                        persistAbstractDomainInputStatement.setString(7, priceRow.getDomainId());
                        persistAbstractDomainInputStatement.setString(8, priceTopiaId);
                        persistAbstractDomainInputStatement.setString(9, inputCode);
                        persistAbstractDomainInputStatement.addBatch();

                        //                                 1         2
                        // INSERT INTO domainfuelinput (topiaid, usageunit)
                        persistDomainInputStatement.setString(1, domainInputId);
                        persistDomainInputStatement.setString(2, unitName);
                        persistDomainInputStatement.addBatch();
                    }

                    nbInputMigrated.addAndGet(1);

                    LOG.info("For current part processed " + (nbInputMigrated.get()*100)/nbInputToCreate + "%  Migrate " + InputType.CARBURANT.name());
                }
                LOG.info("For current part processed " + InputType.CARBURANT.name() + " execute " + INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY + " BATCH");
                persistAbstractDomainInputStatement.executeBatch();
            }
            LOG.info("For current part processed " + InputType.CARBURANT.name() + " execute " + INSERT_INTO_DOMAIN_FUEL_INPUT_QUERY + " BATCH");
            persistDomainInputStatement.executeBatch();
            LOG.info("For current part processed " + InputType.CARBURANT.name() + " END BATCH");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Map<AbstractInputRow, InputPrice> persistOrganicInputPrices(
            Connection connection,
            Set<AbstractInputRow> inputRows,
            String findPricesQuery,
            Map<String, RefFertiOrga> refInputByIds,
            boolean isPracticed,
            Map<InputPrice, InputPrice> uniquePrices) {

        Set<String> objectIds = new HashSet<>();
        Set<String> practicedSystemOrDomainIds = new HashSet<>();
        Map<String, String> practicedIdToDomainId = new HashMap<>();

        Map<AbstractInputRow, InputPrice> inputPriceByDomainIdObjectId_ = new HashMap<>();

        for (AbstractInputRow inputRow : inputRows) {

            final String refProductId = StringUtils.firstNonBlank(inputRow.organicproduct(), DEFAULT_REF_FERTI_ORGA_ID);
            RefFertiOrga refInput = refInputByIds.get(refProductId);
            objectIds.add(InputPriceService.GET_REF_FERTI_ORGA_OBJECT_ID.apply(refInput));

            if (isPracticed) {
                practicedSystemOrDomainIds.add(inputRow.practicedSystemId());
                practicedIdToDomainId.put(inputRow.practicedSystemId(), inputRow.domainId());
            } else {
                practicedSystemOrDomainIds.add(inputRow.domainId());
            }

        }

        Map<Pair<String, String>, List<InputPrice>> inputPriceByDomainIdObjectId = bindToInputPrices(
                connection,
                findPricesQuery,
                isPracticed,
                PriceCategory.ORGANIQUES_INPUT_CATEGORIE,
                InputPriceCategory.ORGANIQUES_INPUT,
                objectIds,
                practicedSystemOrDomainIds,
                practicedIdToDomainId,
                PhytoProductUnit.KG_HA.name(),
                uniquePrices
        );

        for (AbstractInputRow inputRow : inputRows) {
            final String refProductId = StringUtils.firstNonBlank(inputRow.organicproduct(), DEFAULT_REF_FERTI_ORGA_ID);
            RefFertiOrga refInput = refInputByIds.get(refProductId);
            String objectId = InputPriceService.GET_REF_FERTI_ORGA_OBJECT_ID.apply(refInput);

            InputPrice inputPrice = getInputPriceIfExists(inputPriceByDomainIdObjectId, inputRow, objectId);
            if (inputPrice != null) {
                inputPriceByDomainIdObjectId_.put(inputRow, inputPrice);
            }
        }

        return inputPriceByDomainIdObjectId_;
    }

    protected final Function<RefActaTraitementsProduitRow, String> GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID = input -> java.lang.String.format(
            "%s_%d",
            input.getId_produit(),
            input.getId_traitement()
    );

    protected Map<AbstractInputRow, InputPrice> persistPhytoInputPrices(
            Connection connection,
            Set<AbstractInputRow> inputRows,
            String findPricesQuery,
            Map<String, RefActaTraitementsProduitRow> refInputByIds,
            boolean isPracticed,
            PriceCategory priceCategory,
            InputPriceCategory inputPriceCategory,
            Map<InputPrice, InputPrice> uniquePrices) {

        LOG.info(String.format("migrate %s Phyto prices", isPracticed ? "practiced" : "effective"));

        Map<AbstractInputRow, InputPrice> inputPriceByDomainIdObjectId_ = new HashMap<>();

        Set<String> objectIds = new HashSet<>();
        Set<String> practicedSystemOrDomainIds = new HashSet<>();
        Map<String, String> practicedIdToDomainId = new HashMap<>();

        for (AbstractInputRow inputRow : inputRows) {

            final String refProductId = StringUtils.firstNonBlank(inputRow.phytoproduct(), DEFAULT_REF_ACTA_TRAITEMENTS_PRODUIT_ID);
            RefActaTraitementsProduitRow refInput = refInputByIds.get(refProductId);
            objectIds.add(GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(refInput));

            if (isPracticed) {
                practicedSystemOrDomainIds.add(inputRow.practicedSystemId());
                practicedIdToDomainId.put(inputRow.practicedSystemId(), inputRow.domainId());
            } else {
                practicedSystemOrDomainIds.add(inputRow.domainId());
            }

        }

        Map<Pair<String, String>, List<InputPrice>> inputPriceByDomainIdObjectId = bindToInputPrices(
                connection,
                findPricesQuery,
                isPracticed,
                priceCategory,
                inputPriceCategory,
                objectIds,
                practicedSystemOrDomainIds,
                practicedIdToDomainId,
                PhytoProductUnit.KG_HA.name(),
                uniquePrices);

        for (AbstractInputRow inputRow : inputRows) {
            final String refProductId = StringUtils.firstNonBlank(inputRow.phytoproduct(), DEFAULT_REF_ACTA_TRAITEMENTS_PRODUIT_ID);
            RefActaTraitementsProduitRow refInput = refInputByIds.get(refProductId);
            String objectId = GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(refInput);

            InputPrice inputPrice = getInputPriceIfExists(inputPriceByDomainIdObjectId, inputRow, objectId);
            if (inputPrice != null) {
                inputPriceByDomainIdObjectId_.put(inputRow, inputPrice);
            }
        }

        return inputPriceByDomainIdObjectId_;
    }

    protected Map<AbstractInputRow, InputPrice> persistPotInputPrices(
            Connection connection,
            Set<AbstractInputRow> inputRows,
            String findPricesQuery,
            Map<String, RefPot> refInputByIds,
            boolean isPracticed, Map<InputPrice, InputPrice> uniquePrices) {

        LOG.info(String.format("migrate %s Pot prices", isPracticed ? "practiced" : "effective"));

        Map<AbstractInputRow, InputPrice> inputPriceByDomainIdObjectId_ = new HashMap<>();

        Set<String> objectIds = new HashSet<>();
        Set<String> practicedSystemOrDomainIds = new HashSet<>();
        Map<String, String> practicedIdToDomainId = new HashMap<>();

        for (AbstractInputRow inputRow : inputRows) {

            final String refProductId = StringUtils.firstNonBlank(inputRow.pot(), DEFAULT_REF_POT_ID);
            RefPot refInput = refInputByIds.get(refProductId);
            objectIds.add(InputPriceService.GET_REF_POT_OBJECT_ID.apply(refInput));

            if (isPracticed) {
                practicedSystemOrDomainIds.add(inputRow.practicedSystemId());
                practicedIdToDomainId.put(inputRow.practicedSystemId(), inputRow.domainId());
            } else {
                practicedSystemOrDomainIds.add(inputRow.domainId());
            }

        }

        Map<Pair<String, String>, List<InputPrice>> inputPriceByDomainIdObjectId = bindToInputPrices(
                connection,
                findPricesQuery,
                isPracticed,
                PriceCategory.POT_INPUT_CATEGORIE,
                InputPriceCategory.POT_INPUT,
                objectIds,
                practicedSystemOrDomainIds,
                practicedIdToDomainId,
                PotInputUnit.POTS_M2.name(),
                uniquePrices);

        for (AbstractInputRow inputRow : inputRows) {
            final String refProductId = StringUtils.firstNonBlank(inputRow.pot(), DEFAULT_REF_POT_ID);
            RefPot refInput = refInputByIds.get(refProductId);
            String objectId =InputPriceService.GET_REF_POT_OBJECT_ID.apply(refInput);

            InputPrice inputPrice = getInputPriceIfExists(inputPriceByDomainIdObjectId, inputRow, objectId);
            if (inputPrice != null) {
                inputPriceByDomainIdObjectId_.put(inputRow, inputPrice);
            }
        }

        return inputPriceByDomainIdObjectId_;
    }

    protected Map<AbstractInputRow, InputPrice> persistSubstrateInputPrices(
            Connection connection,
            Set<AbstractInputRow> inputRows,
            String findPricesQuery,
            Map<String, RefSubstrate> refInputByIds,
            boolean isPracticed, Map<InputPrice, InputPrice> uniquePrices) {

        LOG.info(String.format("migrate %s Substrate prices", isPracticed ? "practiced" : "effective"));

        Map<AbstractInputRow, InputPrice> inputPriceByDomainIdObjectId_ = new HashMap<>();

        Set<String> objectIds = new HashSet<>();
        Set<String> practicedSystemOrDomainIds = new HashSet<>();
        Map<String, String> practicedIdToDomainId = new HashMap<>();

        for (AbstractInputRow inputRow : inputRows) {

            final String refProductId = StringUtils.firstNonBlank(inputRow.substrate(), DEFAULT_REF_SUBSTRAT_ID);
            RefSubstrate refInput = refInputByIds.get(refProductId);
            objectIds.add(InputPriceService.GET_REF_SUBSTRAT_OBJECT_ID.apply(refInput));

            if (isPracticed) {
                practicedSystemOrDomainIds.add(inputRow.practicedSystemId());
                practicedIdToDomainId.put(inputRow.practicedSystemId(), inputRow.domainId());
            } else {
                practicedSystemOrDomainIds.add(inputRow.domainId());
            }

        }

        Map<Pair<String, String>, List<InputPrice>> inputPriceByDomainIdObjectId = bindToInputPrices(
                connection,
                findPricesQuery,
                isPracticed,
                PriceCategory.SUBSTRATE_INPUT_CATEGORIE,
                InputPriceCategory.SUBSTRATE_INPUT,
                objectIds,
                practicedSystemOrDomainIds,
                practicedIdToDomainId,
                SubstrateInputUnit.KG_HA.name(),
                uniquePrices);

        for (AbstractInputRow inputRow : inputRows) {
            final String refProductId = StringUtils.firstNonBlank(inputRow.substrate(), DEFAULT_REF_SUBSTRAT_ID);
            RefSubstrate refInput = refInputByIds.get(refProductId);
            String objectId = InputPriceService.GET_REF_SUBSTRAT_OBJECT_ID.apply(refInput);

            InputPrice inputPrice = getInputPriceIfExists(inputPriceByDomainIdObjectId, inputRow, objectId);
            if (inputPrice != null) {
                inputPriceByDomainIdObjectId_.put(inputRow, inputPrice);
            }
        }

        return inputPriceByDomainIdObjectId_;
    }

    protected void persistMineralInputs(
            Connection connection,
            Map<String, RefFertiMinUNIFA> refFertiMinUNIFAByIds,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Collection<AbstractInputRow> inputRows,
            int domainDonePercent) {

        Map<String, String> productCodes = new HashMap<>();

        Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId = new HashMap<>();

        AtomicInteger nbInputMigrated = new AtomicInteger();
        try (PreparedStatement persistMineralUsageStatement = connection.prepareStatement(INSERT_INTO_DOMAIN_MINERAL_PRODUCT_INPUT_USAGE_QUERY)) {
            try (PreparedStatement persistAbstractInputUsageStatement = connection.prepareStatement(INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY)) {
                try (PreparedStatement persistDomainMineralInputStatement = connection.prepareStatement(INSERT_INTO_DOMAIN_MINERAL_PRODUCT_INPUT_QUERY)) {
                    try (PreparedStatement persistDomainInputStatement = connection.prepareStatement(INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY)) {
                        Iterable<List<AbstractInputRow>> inputRowParts = Iterables.partition(inputRows, PERSIST_INPUT_CHUNK_SIZE);
                        for (List<AbstractInputRow> inputRowPart : inputRowParts) {
                            for (AbstractInputRow inputRow : inputRowPart) {

                                persistMineralRow(
                                        refFertiMinUNIFAByIds,
                                        productCodes,
                                        priceByDomainIdObjectId,
                                        domainIdInputKeyToDomainInputId,
                                        persistMineralUsageStatement,
                                        persistAbstractInputUsageStatement,
                                        persistDomainMineralInputStatement,
                                        persistDomainInputStatement,
                                        inputRow
                                );

                                nbInputMigrated.addAndGet(1);
                            }
                            LOG.info("For current part processed " + (nbInputMigrated.get()*100)/inputRows.size() + "%  Migrate " + InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.name() + " domainPart " + domainDonePercent + "%");
                        }
                        LOG.info("For current part processed " + InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.name() + " execute " + INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY + " BATCH");
                        persistDomainInputStatement.executeBatch();
                    }
                    LOG.info("For current part processed " + InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.name() + " execute " + INSERT_INTO_DOMAIN_MINERAL_PRODUCT_INPUT_QUERY + " BATCH");
                    persistDomainMineralInputStatement.executeBatch();
                }
                LOG.info("For current part processed " + InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.name() + " execute " + INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY + " BATCH");
                persistAbstractInputUsageStatement.executeBatch();
            }
            LOG.info("For current part processed " + InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.name() + " execute " + INSERT_INTO_DOMAIN_MINERAL_PRODUCT_INPUT_USAGE_QUERY + " BATCH");
            persistMineralUsageStatement.executeBatch();
            LOG.info("For current part processed " + InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.name() + " execute END BATCH");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected void persistMineralRow(
            Map<String, RefFertiMinUNIFA> refFertiMinUNIFAByIds,
            Map<String, String> productCodes,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId,

            PreparedStatement persistMineralUsageStatement,
            PreparedStatement persistAbstractInputUsageStatement,
            PreparedStatement persistDomainMineralInputStatement,
            PreparedStatement persistDomainInputStatement,
            AbstractInputRow inputRow
    ) throws SQLException {

        RefFertiMinUNIFA refFertiMinUNIFA = refFertiMinUNIFAByIds.get(inputRow.mineralproduct());
        Boolean phytoEffect = inputRow.phytoeffect();
        final MineralProductUnit mineralProductUnit = StringUtils.isBlank(inputRow.mineralproductunit()) ? MineralProductUnit.KG_HA : MineralProductUnit.valueOf(inputRow.mineralproductunit());

        // create a key for the input to find same inputs on same domain but other campaigns
        // if found the input will have the same code otherwise a new code will be generated
        String mineralInputKey = DomainInputStockUnitService.getMineralInputKey(refFertiMinUNIFA, phytoEffect, mineralProductUnit);

        Pair<String, String> domainIdAndInputKey = Pair.of(inputRow.domainId(), mineralInputKey);
        String domainInputId = persistDomainMineralProductInput(
                persistDomainInputStatement,
                persistDomainMineralInputStatement,
                productCodes,
                priceByDomainIdObjectId,
                domainIdInputKeyToDomainInputId,
                inputRow,
                refFertiMinUNIFA,
                mineralInputKey,
                domainIdAndInputKey);

        String inputUsageId = MineralProductInputUsage.class.getName() + "_" + UUID.randomUUID();
        String productName = StringUtils.firstNonBlank(refFertiMinUNIFA.getType_produit(), InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.toString());

        persistAbstractInputUsage(
                persistAbstractInputUsageStatement,
                inputRow,
                inputUsageId,
                InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                productName//ref product name
        );

        persistMineralInputUsage(
                persistMineralUsageStatement,
                inputRow,
                domainInputId,
                inputUsageId);
    }

    protected void persistMineralInputUsage(
            PreparedStatement persistMineralUsageStatement,
            AbstractInputRow inputRow,
            String domainInputId,
            String mineralInputUsageId) throws SQLException {

        //                                            1               2                           3
        // INSERT INTO mineralproductinputusage (topiaid, domainmineralproductinput, mineralfertilizersspreadingaction)
        persistMineralUsageStatement.setString(1, mineralInputUsageId);
        persistMineralUsageStatement.setString(2, domainInputId);
        persistMineralUsageStatement.setString(3, inputRow.actionid());
        persistMineralUsageStatement.addBatch();
    }

    protected void persistPhytoInputs(
            Connection connection,
            Map<String, RefActaTraitementsProduitRow> refInputByIds,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Collection<AbstractInputRow> inputRows,
            InputType inputType,
            int domainDonePercent) {

        Map<String, String> productCodes = new HashMap<>();

        Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId = new HashMap<>();

        AtomicInteger nbInputMigrated = new AtomicInteger();
        try (PreparedStatement persistPesticideProductInputUsageStatement = connection.prepareStatement(INSERT_INTO_PESTICIDE_PRODUCT_INPUT_USAGE_QUERY)) {
            persistAbstractPhytoInputAndUsages(
                    connection,
                    inputRows,
                    refInputByIds,
                    productCodes,
                    priceByDomainIdObjectId,
                    domainIdInputKeyToDomainInputId,
                    inputType,
                    PesticideProductInputUsage.class.getName(),
                    persistPesticideProductInputUsageStatement,
                    nbInputMigrated,
                    domainDonePercent);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void persistSeedingTreatmentInputs(
            Connection connection,
            Map<String, RefActaTraitementsProduitRow> refInputByIds,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Collection<AbstractInputRow> inputRows,
            Map<String, List<DbDomainSeedSpeciesInput>> speciesInputForProductByActionId,
            Map<String, List<DbSeedSpeciesInputUsage>> speciesUsageForProductByActionId,
            int domainDonePercent) {

        Map<String, String> productCodes = new HashMap<>();

        Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId = new HashMap<>();

        AtomicInteger nbInputMigrated = new AtomicInteger();
        try (PreparedStatement persistPesticideProductInputUsageStatement = connection.prepareStatement(INSERT_INTO_SEED_PRODUCT_INPUT_USAGE)) {
            try(PreparedStatement insertIntoPhytoTargetPreparedStatement = connection.prepareStatement(INSERT_PHYTO_TARGETS)) {
                try (PreparedStatement abstractPhytoProductInputUsageStatement = connection.prepareStatement(INSERT_INTO_ABSTRACT_PHYTO_PRODUCT_INPUT_USAGE_QUERY)) {
                    try (PreparedStatement abstractInputUsageStatement = connection.prepareStatement(INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY)) {
                        try (PreparedStatement domainPhytoProductInputStatement = connection.prepareStatement(INSERT_INTO_DOMAIN_PHYTO_PRODUCT_INPUT_QUERY)) {
                            try (PreparedStatement abstractDomainInputStockUnitStatement = connection.prepareStatement(INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY)) {
                                Iterable<List<AbstractInputRow>> inputRowParts = Iterables.partition(inputRows, PERSIST_INPUT_CHUNK_SIZE);
                                for (List<AbstractInputRow> inputRowPart : inputRowParts) {
                                    MultiValuedMap<String, String> targetsForInputs = new HashSetValuedHashMap<>();
                                    Set<String> inputIds = inputRowPart.stream().map(AbstractInputRow::inputId).collect(Collectors.toSet());
                                    String inputIdsPart = formatInValues(StringUtils.join(inputIds, "','"));
                                    try(Statement selectPhytoTargetStatement = connection.createStatement()) {
                                        ResultSet rs = selectPhytoTargetStatement.executeQuery(String.format(SELECT_PHYTO_TARGETS, inputIdsPart));
                                        while (rs.next()) {
                                            String phytoProductInput = rs.getString("phytoproductinput");
                                            String target = rs.getString("target");
                                            targetsForInputs.put(phytoProductInput, target);
                                        }
                                    }

                                    for (AbstractInputRow inputRow : inputRowPart) {

                                        final PhytoProductUnit productUnit = StringUtils.isBlank(inputRow.phytoproductunit()) ? PhytoProductUnit.KG_HA : PhytoProductUnit.valueOf(inputRow.phytoproductunit());
                                        final String refProductId = StringUtils.firstNonBlank(inputRow.phytoproduct(), DEFAULT_REF_ACTA_TRAITEMENTS_PRODUIT_ID);
                                        final String productType = StringUtils.firstNonBlank(inputRow.producttype(), ProductType.OTHER.name());

                                        RefActaTraitementsProduitRow refActaTraitementsProduit = refInputByIds.get(refProductId);

                                        AgrosystInterventionType action = refActaTraitementsProduit.getAction();
                                        ProductType typeProduit = refActaTraitementsProduit.type_produit;
                                        // si tc : ASSOCIATIONS, FUNGICIDAL, INSECTICIDAL
                                        // si lb : SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION
                                        // si les deux ASSOCIATIONS, FUNGICIDAL, INSECTICIDAL, SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION

                                        String seedingActionId = inputRow.seedingaction;
                                        boolean isChemicalTreatment = AgrosystInterventionType.SEMIS.equals(action) && (ProductType.ASSOCIATIONS.equals(typeProduit) || ProductType.FUNGICIDAL.equals(typeProduit) || ProductType.INSECTICIDAL.equals(typeProduit));
                                        boolean isBiologicalSeedingInoculation = AgrosystInterventionType.SEMIS.equals(action) && ProductType.SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION.equals(typeProduit);

                                        Collection<DbDomainSeedSpeciesInput> seedingActionDbDomainSeedSpeciesInputs = CollectionUtils.emptyIfNull(speciesInputForProductByActionId.get(seedingActionId));
                                        Collection<DbSeedSpeciesInputUsage> seedingActionDbSeedSpeciesInputUsages = CollectionUtils.emptyIfNull(speciesUsageForProductByActionId.get(seedingActionId));

                                        List<DbDomainSeedSpeciesInput> availableInputsForProduct = new ArrayList<>();
                                        if (isChemicalTreatment) {
                                            availableInputsForProduct.addAll(seedingActionDbDomainSeedSpeciesInputs.stream().filter(DbDomainSeedSpeciesInput::isChemicalTreatment).toList());
                                        }
                                        if (isBiologicalSeedingInoculation) {
                                            availableInputsForProduct.addAll(seedingActionDbDomainSeedSpeciesInputs.stream().filter(DbDomainSeedSpeciesInput::isBiologicalSeedInoculation).toList());
                                        }
                                        Map<String, DbDomainSeedSpeciesInput> availableInputsForProductByIds = new HashMap<>();
                                        // they can be duplicate one, do not use Identity
                                        availableInputsForProduct.forEach(ai -> availableInputsForProductByIds.put(ai.getTopiaId(), ai));

                                        Set<String> speciesInputIds = availableInputsForProductByIds.keySet();

                                        List<DbSeedSpeciesInputUsage> dbSeedSpeciesInputUsages = seedingActionDbSeedSpeciesInputUsages.stream().filter(su -> speciesInputIds.contains(su.getDomainSeedSpeciesInput())).toList();

                                        boolean usageFound = false;
                                        for (DbSeedSpeciesInputUsage dbSeedSpeciesInputUsage : dbSeedSpeciesInputUsages) {

                                            String speciesSeed = dbSeedSpeciesInputUsage.getSpeciesSeed();
                                            // create a key for the input to find same inputs on same domain but other campaigns
                                            // if found the input will have the same code otherwise a new code will be generated
                                            String inputKey = getSeedingPhytoInputKey(speciesSeed, refActaTraitementsProduit, productUnit);

                                            Pair<String, String> domainIdAndInputKey = Pair.of(inputRow.domainId(), inputKey);

                                            String domainInputId = addPhytoProductToDomainSpecies(
                                                    productCodes,
                                                    priceByDomainIdObjectId,
                                                    domainIdInputKeyToDomainInputId,
                                                    domainPhytoProductInputStatement,
                                                    abstractDomainInputStockUnitStatement,
                                                    inputRow,
                                                    productUnit,
                                                    refProductId,
                                                    productType,
                                                    refActaTraitementsProduit,
                                                    seedingActionId,
                                                    availableInputsForProductByIds,
                                                    dbSeedSpeciesInputUsage,
                                                    inputKey,
                                                    domainIdAndInputKey);

                                            if (domainInputId == null) continue;

                                            usageFound = true;

                                            String seedProductInputUsageId = PesticideProductInputUsage.class.getName() + "_" + UUID.randomUUID();
                                            String fromTargetInputId = inputRow.inputId;
//
                                            Collection<String> targets = targetsForInputs.get(fromTargetInputId);
                                            for (String target : targets) {
                                                //    1                         2           3 and 4     5 and 6            7              8
                                                // topiaid, topiaversion, topiacreatedate, category, codegroupeciblemaa, target, abstractphytoproductinputusage
                                                insertIntoPhytoTargetPreparedStatement.setString(1, PhytoProductTarget.class.getName() + "_" + UUID.randomUUID());
                                                insertIntoPhytoTargetPreparedStatement.setDate(2,  scriptDate);
                                                insertIntoPhytoTargetPreparedStatement.setString(3, fromTargetInputId);//phytoproductinput:(SELECT category FROM phytoproducttarget WHERE phytoproductinput = ? and target = ?)
                                                insertIntoPhytoTargetPreparedStatement.setString(4, target);//target: (SELECT category FROM phytoproducttarget WHERE phytoproductinput = ? and target = ?)
                                                insertIntoPhytoTargetPreparedStatement.setString(5, fromTargetInputId);//phytoproductinput: (SELECT codegroupeciblemaa FROM phytoproducttarget WHERE phytoproductinput = ? and target = ?)
                                                insertIntoPhytoTargetPreparedStatement.setString(6, target);//target: (SELECT codegroupeciblemaa FROM phytoproducttarget WHERE phytoproductinput = ? and target = ?)
                                                insertIntoPhytoTargetPreparedStatement.setString(7, target);// target
                                                insertIntoPhytoTargetPreparedStatement.setString(8, seedProductInputUsageId);
                                                insertIntoPhytoTargetPreparedStatement.addBatch();
                                            }


                                            persistAbstractInputUsage(
                                                    abstractInputUsageStatement,
                                                    inputRow,
                                                    seedProductInputUsageId,
                                                    InputType.TRAITEMENT_SEMENCE,
                                                    refActaTraitementsProduit.getNom_produit());

                                            persistAbstractPhytoInputUsage(
                                                    abstractPhytoProductInputUsageStatement,
                                                    seedProductInputUsageId,
                                                    domainInputId);

                                            // INSERT INTO seedproductinputusage  (topiaid, seedspeciesinputusage)
                                            persistPesticideProductInputUsageStatement.setString(1, seedProductInputUsageId);
                                            persistPesticideProductInputUsageStatement.setString(2, dbSeedSpeciesInputUsage.getTopiaId());
                                            persistPesticideProductInputUsageStatement.addBatch();

                                            nbInputMigrated.addAndGet(1);

                                        }

                                        if (!usageFound) {
                                            LOG.warn("No usage found for phyto " + inputRow);
                                        }
                                    }


                                    LOG.info("For current part processed " + (nbInputMigrated.get() * 100) / inputRows.size() + "%  Migrate " + InputType.TRAITEMENT_SEMENCE + ", domain part " + domainDonePercent + "%");
                                }
                                LOG.info("For current part processed " + InputType.TRAITEMENT_SEMENCE.name() + " execute " + INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                                abstractDomainInputStockUnitStatement.executeBatch();
                                LOG.info("For current part processed " + InputType.TRAITEMENT_SEMENCE.name() + " execute " + INSERT_INTO_DOMAIN_PHYTO_PRODUCT_INPUT_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                                domainPhytoProductInputStatement.executeBatch();
                                LOG.info("For current part processed " + InputType.TRAITEMENT_SEMENCE.name() + " execute " + INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                                abstractInputUsageStatement.executeBatch();
                                LOG.info("For current part processed " + InputType.TRAITEMENT_SEMENCE.name() + " execute " + INSERT_INTO_ABSTRACT_PHYTO_PRODUCT_INPUT_USAGE_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                                abstractPhytoProductInputUsageStatement.executeBatch();
                                LOG.info("For current part processed " + InputType.TRAITEMENT_SEMENCE.name() + " execute " + INSERT_INTO_SEED_PRODUCT_INPUT_USAGE + " BATCH, domain part " + domainDonePercent + "%");
                                persistPesticideProductInputUsageStatement.executeBatch();
                                LOG.info("For current part processed " + InputType.TRAITEMENT_SEMENCE.name() + " execute " + INSERT_PHYTO_TARGETS + " BATCH, domain part " + domainDonePercent + "%");
                                insertIntoPhytoTargetPreparedStatement.executeBatch();
                                LOG.info("For current part processed " + InputType.TRAITEMENT_SEMENCE.name() + " END BATCH, domain part " + domainDonePercent + "%");
                            }
                        }
                    }
                }

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected void persistBiologicalProductInputs(
            Connection connection,
            Map<String, RefActaTraitementsProduitRow> refInputByIds,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Collection<AbstractInputRow> inputRows,
            InputType inputType,
            int domainDonePercent) {

        Map<String, String> productCodes = new HashMap<>();

        Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId = new HashMap<>();

        AtomicInteger nbInputMigrated = new AtomicInteger();
        try (PreparedStatement persistBiologicalProductInputUsageStatement = connection.prepareStatement(INSERT_INTO_BIOLOGICAL_PRODUCT_INPUT_USAGE_QUERY)) {
            persistAbstractPhytoInputAndUsages(
                    connection,
                    inputRows,
                    refInputByIds,
                    productCodes,
                    priceByDomainIdObjectId,
                    domainIdInputKeyToDomainInputId,
                    inputType,
                    BiologicalProductInputUsage.class.getName(),
                    persistBiologicalProductInputUsageStatement,
                    nbInputMigrated,
                    domainDonePercent);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected void persistPotInputs(
            Connection connection,
            Map<String, RefPot> refInputByIds,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Collection<AbstractInputRow> inputRows,
            InputType inputType,
            int domainDonePercent) {

        Map<String, String> productCodes = new HashMap<>();

        Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId = new HashMap<>();

        AtomicInteger nbInputMigrated = new AtomicInteger();
        try (PreparedStatement persistProductInputUsageStatement = connection.prepareStatement(INSERT_INTO_POT_INPUT_USAGE_QUERY)) {
            try (PreparedStatement persistAbstractInputUsageStatement = connection.prepareStatement(INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY)) {
                try (PreparedStatement persistDomainProductInputStatement = connection.prepareStatement(INSERT_INTO_DOMAIN_POT_INPUT_QUERY)) {
                    try (PreparedStatement persistDomainAbstractInputStatement = connection.prepareStatement(INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY)) {
                        Iterable<List<AbstractInputRow>> inputRowParts = Iterables.partition(inputRows, PERSIST_INPUT_CHUNK_SIZE);
                        for (List<AbstractInputRow> inputRowPart : inputRowParts) {
                            for (AbstractInputRow inputRow : inputRowPart) {

                                persistPotRow(
                                        refInputByIds,
                                        productCodes,
                                        priceByDomainIdObjectId,
                                        domainIdInputKeyToDomainInputId,

                                        persistProductInputUsageStatement,
                                        persistAbstractInputUsageStatement,
                                        persistDomainProductInputStatement,
                                        persistDomainAbstractInputStatement,
                                        inputRow,
                                        inputType);
                                nbInputMigrated.addAndGet(1);
                            }
                            LOG.info("For current part processed " + (nbInputMigrated.get()*100)/inputRows.size() + "%  Migrate " + inputType + ", domain part " + domainDonePercent + "%");
                        }
                        LOG.info("For current part processed " + inputType.name() + " execute " + INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                        persistDomainAbstractInputStatement.executeBatch();
                    }
                    LOG.info("For current part processed " + inputType.name() + " execute " + INSERT_INTO_DOMAIN_POT_INPUT_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                    persistDomainProductInputStatement.executeBatch();
                }
                LOG.info("For current part processed " + inputType.name() + " execute " + INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                persistAbstractInputUsageStatement.executeBatch();
            }
            LOG.info("For current part processed " + inputType.name() + " execute " + INSERT_INTO_POT_INPUT_USAGE_QUERY + " BATCH, domain part " + domainDonePercent + "%");
            persistProductInputUsageStatement.executeBatch();
            LOG.info("For current part processed " + inputType.name() + " END BATCH, domain part " + domainDonePercent + "%");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected void persistSubstrateInputs(
            Connection connection,
            Map<String, RefSubstrate> refInputByIds,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Collection<AbstractInputRow> inputRows,
            InputType inputType,
            int domainDonePercent) {

        Map<String, String> productCodes = new HashMap<>();

        Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId = new HashMap<>();

        AtomicInteger nbInputMigrated = new AtomicInteger();
        try (PreparedStatement persistProductInputUsageStatement = connection.prepareStatement(INSERT_INTO_SUBSTRATE_INPUT_USAGE_QUERY)) {
            try (PreparedStatement persistAbstractInputUsageStatement = connection.prepareStatement(INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY)) {
                try (PreparedStatement persistDomainProductInputStatement = connection.prepareStatement(INSERT_INTO_DOMAIN_SUBSTRATE_INPUT_QUERY)) {
                    try (PreparedStatement persistDomainAbstractInputStatement = connection.prepareStatement(INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY)) {
                        Iterable<List<AbstractInputRow>> inputRowParts = Iterables.partition(inputRows, PERSIST_INPUT_CHUNK_SIZE);
                        for (List<AbstractInputRow> inputRowPart : inputRowParts) {
                            for (AbstractInputRow inputRow : inputRowPart) {

                                persistSubstrateRow(
                                        refInputByIds,
                                        productCodes,
                                        priceByDomainIdObjectId,
                                        domainIdInputKeyToDomainInputId,

                                        persistProductInputUsageStatement,
                                        persistAbstractInputUsageStatement,
                                        persistDomainProductInputStatement,
                                        persistDomainAbstractInputStatement,
                                        inputRow,
                                        inputType);
                                nbInputMigrated.addAndGet(1);
                            }
                            LOG.info("For current part processed " + (nbInputMigrated.get()*100)/inputRows.size() + "%  Migrate " + inputType + ", domain part " + domainDonePercent + "%");
                        }
                        LOG.info("For current part processed " + inputType.name() + " execute " + INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                        persistDomainAbstractInputStatement.executeBatch();
                    }
                    LOG.info("For current part processed " + inputType.name() + " execute " + INSERT_INTO_DOMAIN_SUBSTRATE_INPUT_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                    persistDomainProductInputStatement.executeBatch();
                }
                LOG.info("For current part processed " + inputType.name() + " execute " + INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                persistAbstractInputUsageStatement.executeBatch();
            }
            LOG.info("For current part processed " + inputType.name() + " execute " + INSERT_INTO_SUBSTRATE_INPUT_USAGE_QUERY + " BATCH, domain part " + domainDonePercent + "%");
            persistProductInputUsageStatement.executeBatch();
            LOG.info("For current part processed " + inputType.name() + " END BATCH, domain part " + domainDonePercent + "%");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private String addPhytoProductToDomainSpecies(
            Map<String, String> productCodes,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId,
            PreparedStatement domainPhytoProductInputStatement,
            PreparedStatement abstractDomainInputStockUnitStatement,
            AbstractInputRow inputRow,
            PhytoProductUnit productUnit,
            String refProductId,
            String productType,
            RefActaTraitementsProduitRow refActaTraitementsProduit,
            String key,
            Map<String, DbDomainSeedSpeciesInput> domainSpeciesForProductByIds,
            DbSeedSpeciesInputUsage dbSeedSpeciesInputUsage,
            String inputKey,
            Pair<String, String> domainIdAndInputKey) throws SQLException {

        String domainInputId = domainIdInputKeyToDomainInputId.get(domainIdAndInputKey);
        if (ifInputNotExistsOnDomain(domainInputId)) {

            String domainInputCodeKey = inputKey + "_" + inputRow.domainCode();
            String inputCode = getDomainInputCode(productCodes, domainInputCodeKey);

            DbDomainSeedSpeciesInput dbDomainSeedSpeciesInput = domainSpeciesForProductByIds.get(dbSeedSpeciesInputUsage.getDomainSeedSpeciesInput());

            if (dbDomainSeedSpeciesInput == null) {
                LOG.warn("Requires DomainSeedSpeciesInput with id " + dbSeedSpeciesInputUsage.getDomainSeedSpeciesInput() + " is not found for key:" + key + " with usage as " + dbSeedSpeciesInputUsage);
                return null;
            }

            String domainSeedSpeciesInputId = dbDomainSeedSpeciesInput.getTopiaId();

            String objectId = GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(refActaTraitementsProduit);

            String priceTopiaId = getInputPriceIdIfExists(priceByDomainIdObjectId, inputRow, objectId);

            domainInputId = DomainPhytoProductInput.class.getName() + "_" + UUID.randomUUID();
            domainIdInputKeyToDomainInputId.put(domainIdAndInputKey, domainInputId);

            //                                              1        2               3           4       5        6          7        8         9
            // INSERT INTO abstractdomaininputstockunit (topiaid, topiaversion, topiacreatedate, key, inputname, inputtype, domain, inputprice, code)

            abstractDomainInputStockUnitStatement.setString(1, domainInputId);
            abstractDomainInputStockUnitStatement.setLong(2, 0L);
            abstractDomainInputStockUnitStatement.setDate(3, scriptDate);
            abstractDomainInputStockUnitStatement.setString(4, inputKey);
            abstractDomainInputStockUnitStatement.setString(5, StringUtils.firstNonBlank(refActaTraitementsProduit.getNom_produit(), refActaTraitementsProduit.getNom_traitement(), objectId, "UNKNOWN"));
            abstractDomainInputStockUnitStatement.setString(6, InputType.TRAITEMENT_SEMENCE.toString());
            abstractDomainInputStockUnitStatement.setString(7, inputRow.domainId);
            abstractDomainInputStockUnitStatement.setString(8, priceTopiaId);
            abstractDomainInputStockUnitStatement.setString(9, inputCode);
            abstractDomainInputStockUnitStatement.addBatch();

            //                                        1          2         3           4             5
            // INSERT INTO domainphytoproductinput (topiaid, refinput, usageunit, producttype, domainseedspeciesinput)
            domainPhytoProductInputStatement.setString(1, domainInputId);
            domainPhytoProductInputStatement.setString(2, refProductId);
            domainPhytoProductInputStatement.setString(3, productUnit.name());
            domainPhytoProductInputStatement.setString(4, productType);
            domainPhytoProductInputStatement.setString(5, domainSeedSpeciesInputId);
            domainPhytoProductInputStatement.addBatch();

        }
        return domainInputId;
    }

    private void persistAbstractPhytoInputAndUsages(
            Connection connection,
            Collection<AbstractInputRow> inputRows,
            Map<String, RefActaTraitementsProduitRow> refInputByIds,
            Map<String, String> productCodes,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId,
            InputType inputType,
            String usageEntityClasseName,
            PreparedStatement persistProductInputUsageStatement,
            AtomicInteger nbInputMigrated,
            int domainDonePercent) throws SQLException {

        try(PreparedStatement insertIntoPhytoTargetPreparedStatement = connection.prepareStatement(INSERT_PHYTO_TARGETS)) {
            try (PreparedStatement persistAbstractPhytoInputUsageStatement = connection.prepareStatement(INSERT_INTO_ABSTRACT_PHYTO_PRODUCT_INPUT_USAGE_QUERY)) {
                try (PreparedStatement persistAbstractInputUsageStatement = connection.prepareStatement(INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY)) {
                    try (PreparedStatement persistDomainPhytoProductInputStatement = connection.prepareStatement(INSERT_INTO_DOMAIN_PHYTO_PRODUCT_INPUT_QUERY)) {
                        try (PreparedStatement persistDomainInputStatement = connection.prepareStatement(INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY)) {
                            Iterable<List<AbstractInputRow>> inputRowParts = Iterables.partition(inputRows, PERSIST_INPUT_CHUNK_SIZE);
                            for (List<AbstractInputRow> inputRowPart : inputRowParts) {
                                for (AbstractInputRow inputRow : inputRowPart) {
                                    MultiValuedMap<String, String> targetsForInputs = new HashSetValuedHashMap<>();
                                    Set<String> inputIds = inputRowPart.stream().map(AbstractInputRow::inputId).collect(Collectors.toSet());
                                    String inputIdsPart = formatInValues(StringUtils.join(inputIds, "','"));
                                    try(Statement selectPhytoTargetStatement = connection.createStatement()) {
                                        ResultSet rs = selectPhytoTargetStatement.executeQuery(String.format(SELECT_PHYTO_TARGETS, inputIdsPart));
                                        while (rs.next()) {
                                            String phytoProductInput = rs.getString("phytoproductinput");
                                            String target = rs.getString("target");
                                            targetsForInputs.put(phytoProductInput, target);
                                        }
                                    }

                                    final String refProductId = StringUtils.firstNonBlank(inputRow.phytoproduct(), DEFAULT_REF_ACTA_TRAITEMENTS_PRODUIT_ID);
                                    RefActaTraitementsProduitRow refActaTraitementsProduit = refInputByIds.get(refProductId);

                                    String domainInputId = persistPhytoRow(
                                            refInputByIds,
                                            productCodes,
                                            priceByDomainIdObjectId,
                                            domainIdInputKeyToDomainInputId,

                                            inputType,
                                            persistDomainPhytoProductInputStatement,
                                            persistDomainInputStatement,
                                            inputRow
                                    );

                                    String inputId = inputRow.inputId;
                                    String inputOriginalRandomPart = inputId.split("_")[1];
                                    String inputUsageId = usageEntityClasseName + "_" + inputOriginalRandomPart;

//                                    updatePhytoTargetPreparedStatement.setString(1, inputUsageId);
//                                    updatePhytoTargetPreparedStatement.setString(2, inputId);
//                                    updatePhytoTargetPreparedStatement.addBatch();

                                    String fromTargetInputId = inputRow.inputId;
                                    Collection<String> targets = targetsForInputs.get(fromTargetInputId);
                                    for (String target : targets) {
                                        //    1                         2           3 and 4     5 and 6            7              8
                                        // topiaid, topiaversion, topiacreatedate, category, codegroupeciblemaa, target, abstractphytoproductinputusage
                                        insertIntoPhytoTargetPreparedStatement.setString(1, PhytoProductTarget.class.getName() + "_" + UUID.randomUUID());
                                        insertIntoPhytoTargetPreparedStatement.setDate(2,  scriptDate);
                                        insertIntoPhytoTargetPreparedStatement.setString(3, fromTargetInputId);//phytoproductinput:(SELECT category FROM phytoproducttarget WHERE phytoproductinput = ? and target = ?)
                                        insertIntoPhytoTargetPreparedStatement.setString(4, target);//target: (SELECT category FROM phytoproducttarget WHERE phytoproductinput = ? and target = ?)
                                        insertIntoPhytoTargetPreparedStatement.setString(5, fromTargetInputId);//phytoproductinput: (SELECT codegroupeciblemaa FROM phytoproducttarget WHERE phytoproductinput = ? and target = ?)
                                        insertIntoPhytoTargetPreparedStatement.setString(6, target);//target: (SELECT codegroupeciblemaa FROM phytoproducttarget WHERE phytoproductinput = ? and target = ?)
                                        insertIntoPhytoTargetPreparedStatement.setString(7, target);// target
                                        insertIntoPhytoTargetPreparedStatement.setString(8, inputUsageId);
                                        insertIntoPhytoTargetPreparedStatement.addBatch();
                                    }

                                    persistAbstractInputUsage(
                                            persistAbstractInputUsageStatement,
                                            inputRow,
                                            inputUsageId,
                                            inputType,
                                            refActaTraitementsProduit.getNom_produit());

                                    persistAbstractPhytoInputUsage(
                                            persistAbstractPhytoInputUsageStatement,
                                            inputUsageId,
                                            domainInputId);

                                    persistPhytoInputUsage(
                                            persistProductInputUsageStatement,
                                            inputUsageId,
                                            inputRow.actionid());

                                    nbInputMigrated.addAndGet(1);

                                }
                                LOG.info("For current part processed " + (nbInputMigrated.get() * 100) / inputRows.size() + "%  Migrate " + inputType + " , domain part " + domainDonePercent + "%");
                            }
                            LOG.info("For current part processed " + inputType.name() + " execute " + INSERT_ABSTRACT_DOMAIN_INPUT_STOCK_UNIT_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                            persistDomainInputStatement.executeBatch();
                        }
                        LOG.info("For current part processed " + inputType.name() + " execute " + INSERT_INTO_DOMAIN_PHYTO_PRODUCT_INPUT_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                        persistDomainPhytoProductInputStatement.executeBatch();
                    }
                    LOG.info("For current part processed " + inputType.name() + " execute " + INSERT_INTO_ABSTRACT_INPUT_USAGE_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                    persistAbstractInputUsageStatement.executeBatch();
                }
                LOG.info("For current part processed " + inputType.name() + " execute " + INSERT_INTO_ABSTRACT_PHYTO_PRODUCT_INPUT_USAGE_QUERY + " BATCH, domain part " + domainDonePercent + "%");
                persistAbstractPhytoInputUsageStatement.executeBatch();
            }
            LOG.info("For current part processed " + inputType.name() + " execute persistProductInputUsageStatement BATCH, domain part " + domainDonePercent + "%");
            persistProductInputUsageStatement.executeBatch();
            LOG.info("For current part processed " + inputType.name() + " execute " + INSERT_PHYTO_TARGETS + " BATCH, domain part " + domainDonePercent + "%");
            insertIntoPhytoTargetPreparedStatement.executeBatch();
        }
    }

    static String getPhytoInputKey(RefActaTraitementsProduitRow refInput, PhytoProductUnit usageUnit) {
        String result = refInput.getId_produit() + ";" + refInput.getId_traitement() + ";" + usageUnit;
        return result;
    }

    static String getSeedingPhytoInputKey(String domainSpeciesId, RefActaTraitementsProduitRow refInput, PhytoProductUnit usageUnit) {
        String result = domainSpeciesId + ";" + refInput.getId_produit() + ";" + refInput.getId_traitement() + ";" + usageUnit;
        return result;
    }

    protected String persistPhytoRow(
            Map<String, RefActaTraitementsProduitRow> refPhytoByIds,
            Map<String, String> inputProductCodes,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId,

            InputType inputType,
            PreparedStatement persistDomainProductInputStatement,
            PreparedStatement persistDomainAbstractInputStatement,
            AbstractInputRow inputRow) throws SQLException {

        final String refProductId = StringUtils.firstNonBlank(inputRow.phytoproduct(), DEFAULT_REF_ACTA_TRAITEMENTS_PRODUIT_ID);
        RefActaTraitementsProduitRow refActaTraitementsProduit = refPhytoByIds.get(refProductId);

        final PhytoProductUnit productUnit = StringUtils.isBlank(inputRow.phytoproductunit()) ? PhytoProductUnit.KG_HA : PhytoProductUnit.valueOf(inputRow.phytoproductunit());

        // create a key for the input to find same inputs on same domain but other campaigns
        // if found the input will have the same code otherwise a new code will be generated
        String inputKey = getPhytoInputKey(refActaTraitementsProduit, productUnit);

        Pair<String, String> domainIdAndInputKey = Pair.of(inputRow.domainId(), inputKey);
        String domainInputId = persistDomainPhytoProductInput(
                persistDomainAbstractInputStatement,
                persistDomainProductInputStatement,
                inputProductCodes,
                priceByDomainIdObjectId,
                domainIdInputKeyToDomainInputId,
                inputRow,
                inputType,
                refActaTraitementsProduit,
                inputKey,
                domainIdAndInputKey);

        return domainInputId;
    }

    protected void persistAbstractPhytoInputUsage(
            PreparedStatement persistAbstractPhytoInputUsageStatement,
            String inputUsageId,
            String domainInputId) throws SQLException {

        // INSERT INTO abstractphytoproductinputusage (topiaid, domainphytoproductinput)
        persistAbstractPhytoInputUsageStatement.setString(1, inputUsageId);
        persistAbstractPhytoInputUsageStatement.setString(2, domainInputId);
        persistAbstractPhytoInputUsageStatement.addBatch();
    }

    protected void persistPhytoInputUsage(
            PreparedStatement persistPesticideProductInputUsageStatement,
            String inputUsageId,
            String actionid) throws SQLException {

        //                                            1               2
        // INSERT INTO biologicalproductinputusage (topiaid, biologicalcontrolaction)
        // INSERT INTO pesticideproductinputusage  (topiaid, pesticidesspreadingaction)
        persistPesticideProductInputUsageStatement.setString(1, inputUsageId);
        persistPesticideProductInputUsageStatement.setString(2, actionid);
        persistPesticideProductInputUsageStatement.addBatch();
    }

    protected void persistOrganicInputUsage(
            PreparedStatement persistPesticideProductInputUsageStatement,
            String domainInputId,
            String inputUsageId,
            String actionid) throws SQLException {

        //                                            1               2                             3
        // INSERT INTO domainorganicproductinput (topiaid, domainorganicproductinput, organicfertilizersspreadingaction)
        persistPesticideProductInputUsageStatement.setString(1, inputUsageId);
        persistPesticideProductInputUsageStatement.setString(2, domainInputId);
        persistPesticideProductInputUsageStatement.setString(3, actionid);
        persistPesticideProductInputUsageStatement.addBatch();
    }

    protected void persistPotInputUsage(
            PreparedStatement persistProductInputUsageStatement,
            String domainInputId,
            String inputUsageId,
            String actionid) throws SQLException {

        //                               1             2           3
        //  INSERT INTO potinputusage (topiaid, domainpotinput, otheraction)
        persistProductInputUsageStatement.setString(1, inputUsageId);
        persistProductInputUsageStatement.setString(2, domainInputId);
        persistProductInputUsageStatement.setString(3, actionid);
        persistProductInputUsageStatement.addBatch();
    }

    protected void persistSubstrateInputUsage(
            PreparedStatement persistProductInputUsageStatement,
            String domainInputId,
            String inputUsageId,
            String actionid) throws SQLException {

        //                               1             2           3
        //  INSERT INTO domainsubstrateinput (topiaid, domainpotinput, otheraction)
        persistProductInputUsageStatement.setString(1, inputUsageId);
        persistProductInputUsageStatement.setString(2, domainInputId);
        persistProductInputUsageStatement.setString(3, actionid);
        persistProductInputUsageStatement.addBatch();
    }

    protected void persistAbstractInputUsage(
            PreparedStatement persistAbstractInputUsageStatement,
            AbstractInputRow inputRow,
            String inputUsageId,
            InputType inputType,
            String refProductName) throws SQLException {

        //                                    1                                       2         3           4     5       6     7        8       9
        // INSERT INTO abstractinputusage (topiaid, topiaversion, topiacreatedate, comment, productname, qtmin, qtavg, qtmed, qtmax, inputtype, code)
        persistAbstractInputUsageStatement.setString(1, inputUsageId);
        persistAbstractInputUsageStatement.setString(2, inputRow.productname());
        persistAbstractInputUsageStatement.setString(3, StringUtils.firstNonBlank(inputRow.productname(), refProductName, "UNKNOWN"));
        setSqlDoubleOrNull(4, inputRow.qtmin(), persistAbstractInputUsageStatement);
        setSqlDoubleOrNull(5, inputRow.qtavg(), persistAbstractInputUsageStatement);
        setSqlDoubleOrNull(6, inputRow.qtmed(), persistAbstractInputUsageStatement);
        setSqlDoubleOrNull(7, inputRow.qtmax(), persistAbstractInputUsageStatement);
        persistAbstractInputUsageStatement.setString(8, inputType.name());
        persistAbstractInputUsageStatement.setString(9, UUID.randomUUID().toString());
        persistAbstractInputUsageStatement.addBatch();

    }

    protected String persistDomainMineralProductInput(
            PreparedStatement persistDomainInputStatement,
            PreparedStatement persistDomainMineralProductInputStatement,
            Map<String, String> inputProductCodes,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId,
            AbstractInputRow inputRow,
            RefFertiMinUNIFA refFertiMinUNIFA,
            String inputKey,
            Pair<String, String> domainIdAndInputKey) throws SQLException {

        String domainInputId = domainIdInputKeyToDomainInputId.get(domainIdAndInputKey);
        if (ifInputNotExistsOnDomain(domainInputId)) {

            String inputKeyCode = inputKey + "_" + inputRow.domainCode();
            String inputCode = getDomainInputCode(inputProductCodes, inputKeyCode);

            String objectId = InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(refFertiMinUNIFA);
            String productName = StringUtils.firstNonBlank(refFertiMinUNIFA.getType_produit(), InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.toString());

            String priceTopiaId = getInputPriceIdIfExists(priceByDomainIdObjectId, inputRow, objectId);

            domainInputId = DomainMineralProductInput.class.getName() + "_" + UUID.randomUUID();
            domainIdInputKeyToDomainInputId.put(domainIdAndInputKey, domainInputId);

            persistAbstractDomainInput(
                    persistDomainInputStatement, domainInputId,
                    inputKey,
                    productName,
                    inputRow.productname(), objectId,
                    InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                    priceTopiaId,
                    inputCode, inputRow.domainId());

            Boolean phytoEffect = inputRow.phytoeffect();
            final MineralProductUnit mineralProductUnit = StringUtils.isBlank(inputRow.mineralproductunit()) ? MineralProductUnit.KG_HA : MineralProductUnit.valueOf(inputRow.mineralproductunit());

            //                                           1         2            3        4
            // INSERT INTO domainmineralproductinput (topiaid, phytoeffect, refinput, usageunit)
            persistDomainMineralProductInputStatement.setString(1, domainInputId);
            persistDomainMineralProductInputStatement.setBoolean(2, phytoEffect);
            persistDomainMineralProductInputStatement.setString(3, refFertiMinUNIFA.getTopiaId());
            persistDomainMineralProductInputStatement.setString(4, mineralProductUnit.toString());
            persistDomainMineralProductInputStatement.addBatch();

        }
        return domainInputId;
    }

    protected String persistDomainPhytoProductInput(
            PreparedStatement persistDomainAbstractInputStatement,
            PreparedStatement persistDomainProductInputStatement,
            Map<String, String> inputProductCodes,
            Map<AbstractInputRow, InputPrice> priceByDomainIdObjectId,
            Map<Pair<String, String>, String> domainIdInputKeyToDomainInputId,
            AbstractInputRow inputRow,
            InputType inputType,
            RefActaTraitementsProduitRow refInput,
            String inputKey,
            Pair<String, String> domainIdAndInputKey) throws SQLException {

        String domainInputId = domainIdInputKeyToDomainInputId.get(domainIdAndInputKey);
        if (ifInputNotExistsOnDomain(domainInputId)) {

            String domainInputCodeKey = inputKey + "_" + inputRow.domainCode();
            String inputCode = getDomainInputCode(inputProductCodes, domainInputCodeKey);

            String objectId = GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(refInput);

            String priceTopiaId = getInputPriceIdIfExists(priceByDomainIdObjectId, inputRow, objectId);

            domainInputId = DomainPhytoProductInput.class.getName() + "_" + UUID.randomUUID();
            domainIdInputKeyToDomainInputId.put(domainIdAndInputKey, domainInputId);

            persistAbstractDomainInput(
                    persistDomainAbstractInputStatement,
                    domainInputId,
                    inputKey,
                    StringUtils.firstNonBlank(refInput.getNom_produit(), refInput.getNom_traitement()),
                    inputRow.productname(), objectId,
                    inputType,
                    priceTopiaId,
                    inputCode,
                    inputRow.domainId());

            final PhytoProductUnit productUnit = StringUtils.isBlank(inputRow.phytoproductunit()) ? PhytoProductUnit.KG_HA : PhytoProductUnit.valueOf(inputRow.phytoproductunit());
            final String refProductId = StringUtils.firstNonBlank(inputRow.phytoproduct(), DEFAULT_REF_ACTA_TRAITEMENTS_PRODUIT_ID);
            final String productType = StringUtils.firstNonBlank(inputRow.producttype(), ProductType.OTHER.name());
            //                                        1          2         3           4             5
            // INSERT INTO domainphytoproductinput (topiaid, refinput, usageunit, producttype, domainseedspeciesinput)
            persistDomainProductInputStatement.setString(1, domainInputId);
            persistDomainProductInputStatement.setString(2, refProductId);
            persistDomainProductInputStatement.setString(3, productUnit.name());
            persistDomainProductInputStatement.setString(4, productType);
            persistDomainProductInputStatement.setNull(5, Types.VARCHAR);
            persistDomainProductInputStatement.addBatch();

        }
        return domainInputId;
    }

    private void persistAbstractDomainInput(
            PreparedStatement persistAbstractDomainInputStatement,
            String domainInputId,
            String inputKey,
            String refProductName,
            String userProductName,
            String objectId,
            InputType inputType,
            String priceTopiaId,
            String inputCode,
            String domainId) throws SQLException {

        //                                              1        2               3           4       5        6          7        8         9
        // INSERT INTO abstractdomaininputstockunit (topiaid, topiaversion, topiacreatedate, key, inputname, inputtype, domain, inputprice, code)

        persistAbstractDomainInputStatement.setString(1, domainInputId);
        persistAbstractDomainInputStatement.setLong(2, 0L);
        persistAbstractDomainInputStatement.setDate(3, scriptDate);
        persistAbstractDomainInputStatement.setString(4, inputKey);
        persistAbstractDomainInputStatement.setString(5, StringUtils.firstNonBlank(userProductName, refProductName, objectId, "UNKNOWN"));
        persistAbstractDomainInputStatement.setString(6, inputType.name());
        persistAbstractDomainInputStatement.setString(7, domainId);
        persistAbstractDomainInputStatement.setString(8, priceTopiaId);
        persistAbstractDomainInputStatement.setString(9, inputCode);
        persistAbstractDomainInputStatement.addBatch();
    }

    protected Map<AbstractInputRow, InputPrice> persistMineralInputPrices(
            Connection connection,
            Set<AbstractInputRow> inputRows,
            String findPricesQuery,
            Map<String, RefFertiMinUNIFA> refFertiMinUNIFAByIds,
            boolean isPracticed, Map<InputPrice, InputPrice> uniquePrices) {

        LOG.info(String.format("migrate %s MineralInputPrice prices", isPracticed ? "practiced" : "effective"));

        Map<AbstractInputRow, InputPrice> inputPriceByDomainIdObjectId_ = new HashMap<>();

        Set<String> objectIds = new HashSet<>();
        Set<String> practicedSystemOrDomainIds = new HashSet<>();
        Map<String, String> practicedIdToDomainId = new HashMap<>();

        for (AbstractInputRow inputRow : inputRows) {

            RefFertiMinUNIFA refFertiMinUNIFA = refFertiMinUNIFAByIds.get(inputRow.mineralproduct());
            objectIds.add(InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(refFertiMinUNIFA));

            if (isPracticed) {
                practicedSystemOrDomainIds.add(inputRow.practicedSystemId());
                practicedIdToDomainId.put(inputRow.practicedSystemId(), inputRow.domainId());
            } else {
                practicedSystemOrDomainIds.add(inputRow.domainId());
            }

        }

        Map<Pair<String, String>, List<InputPrice>> inputPriceByDomainIdObjectId = bindToInputPrices(
                connection,
                findPricesQuery,
                isPracticed,
                PriceCategory.MINERAL_INPUT_CATEGORIE,
                InputPriceCategory.MINERAL_INPUT,
                objectIds,
                practicedSystemOrDomainIds,
                practicedIdToDomainId,
                PhytoProductUnit.KG_HA.name(),
                uniquePrices);

        for (AbstractInputRow inputRow : inputRows) {
            RefFertiMinUNIFA refFertiMinUNIFA = refFertiMinUNIFAByIds.get(inputRow.mineralproduct());
            String objectId = InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(refFertiMinUNIFA);

            InputPrice inputPrice = getInputPriceIfExists(inputPriceByDomainIdObjectId, inputRow, objectId);
            if (inputPrice != null) {
                inputPriceByDomainIdObjectId_.put(inputRow, inputPrice);
            }
        }

        return inputPriceByDomainIdObjectId_;
    }

    protected Map<AbstractInputRow, InputPrice> persistOtherInputPrices(
            Connection connection,
            Set<AbstractInputRow> inputRows,
            String findPricesQuery,
            boolean isPracticed,
            Set<String> ObjectIds,
            Map<InputPrice, InputPrice> uniquePrices) {

        LOG.info(String.format("migrate %s Other prices", isPracticed ? "practiced" : "effective"));

        Map<AbstractInputRow, InputPrice> inputPriceByDomainIdObjectId_ = new HashMap<>();

        Set<String> practicedSystemOrDomainIds = new HashSet<>();
        Map<String, String> practicedIdToDomainId = new HashMap<>();

        for (AbstractInputRow inputRow : inputRows) {

            if (isPracticed) {
                practicedSystemOrDomainIds.add(inputRow.practicedSystemId());
                practicedIdToDomainId.put(inputRow.practicedSystemId(), inputRow.domainId());
            } else {
                practicedSystemOrDomainIds.add(inputRow.domainId());
            }

        }

        Map<Pair<String, String>, List<InputPrice>> inputPricesByDomainIdObjectId = bindToInputPrices(
                connection,
                findPricesQuery,
                isPracticed,
                PriceCategory.OTHER_INPUT_CATEGORIE,
                InputPriceCategory.OTHER_INPUT,
                ObjectIds,
                practicedSystemOrDomainIds,
                practicedIdToDomainId,
                PhytoProductUnit.KG_HA.name(),
                uniquePrices);

        for (AbstractInputRow inputRow : inputRows) {
            String objectId = StringUtils.stripAccents(StringUtils.trimToEmpty(inputRow.productname))
                    .replaceAll("\\s+", "_")
                    .replaceAll("'", "''")
                    .toLowerCase();

            InputPrice inputPrice = getInputPriceIfExists(inputPricesByDomainIdObjectId, inputRow, objectId);
            if (inputPrice != null) {
                inputPriceByDomainIdObjectId_.put(inputRow, inputPrice);
            }
        }

        return inputPriceByDomainIdObjectId_;
    }

    private void persistInputPrices(
            Connection connection,
            InputPriceCategory inputPriceCategory,
            Collection<InputPrice> inputPrices) {

        if (CollectionUtils.isNotEmpty(inputPrices)) {
            final int inputPriceSize = inputPrices.size();
            LOG.debug("DO PERSIST " + inputPriceSize + " AbstractInputPrice FOR CATEGORY " + inputPriceCategory);
            try (PreparedStatement statement = connection.prepareStatement(INSERT_INPUT_PRICE_QUERY)) {
                final List<InputPrice> abstractInputPriceWithoutIds = inputPrices.stream().filter(ip -> StringUtils.isBlank(ip.getTopiaId())).toList();
                if (CollectionUtils.isNotEmpty(abstractInputPriceWithoutIds)) {
                    LOG.error(abstractInputPriceWithoutIds.size() + " INPUT PRICES WITHOUT ID FOUND");
                }
                int bulkIndex = 0;
                for (InputPrice inputPrice : inputPrices) {
                    if (inputPrice != null) {
                        statement.setString(1, inputPrice.getTopiaId());
                        statement.setLong(2, 0L);
                        statement.setDate(3, scriptDate);
                        statement.setString(4, inputPrice.getTopiaDiscriminator());
                        statement.setString(5, inputPrice.getObjectId());
                        statement.setString(6, inputPrice.getSourceUnit());
                        statement.setString(7, inputPrice.getCategory().name());
                        statement.setString(8, inputPrice.getDomainId());
                        statement.setString(9, inputPrice.getDisplayName());
                        statement.setDouble(10, inputPrice.getPrice());
                        statement.setString(11, inputPrice.getPriceUnit().toString());
                        ProductType phytoProductType = inputPrice.getPhytoProductType();
                        if (phytoProductType != null) {
                            statement.setString(12, phytoProductType.toString());
                        } else {
                            statement.setNull(12, Types.VARCHAR);
                        }
                        statement.addBatch();
                        bulkIndex++;
                        if (bulkIndex % 1000 == 0 || bulkIndex == inputPriceSize) {
                            LOG.info("For current part processed " + inputPriceCategory.name() + " execute " + INSERT_INPUT_PRICE_QUERY + " BATCH");
                            statement.executeBatch();
                            LOG.info("For current part processed " + inputPriceCategory.name() + " END BATCH");
                        }
                    }
                }
                LOG.info("For current part processed AbstractInputPrice FOR CATEGORY " + inputPriceCategory);
            } catch (SQLException e) {
                LOG.error(INSERT_INPUT_PRICE_QUERY);
                throw new RuntimeException(e);
            }
        }
    }

    private void persistSeedPrices(
            SeedPrice seedPrice,
            PreparedStatement statement) throws SQLException {
        //                              1         2              3                 4              5          6           7        8         9          10      11               12                    13                     14            15       16
        //   INSERT INTO inputprice (topiaid, topiaversion, topiacreatedate, topiadiscriminator, objectid, sourceunit, category, domain,  displayname, price, priceunit, biologicalseedinoculation, chemicaltreatment, includedtreatment, organic, seedtype)
        if (seedPrice != null) {
            statement.setString(1, seedPrice.getTopiaId());
            statement.setLong(2, 0L);
            statement.setDate(3, scriptDate);
            statement.setString(4, seedPrice.getTopiaDiscriminator());
            statement.setString(5, seedPrice.getObjectId());
            statement.setNull(6,  Types.VARCHAR);
            statement.setString(7, InputPriceCategory.SEEDING_INPUT.name());
            statement.setString(8, seedPrice.getDomainId());
            statement.setString(9, seedPrice.getDisplayName());
            statement.setDouble(10, seedPrice.getPrice());
            statement.setString(11, seedPrice.getPriceUnit().toString());
            statement.setBoolean(12, seedPrice.biologicalSeedInoculation);
            statement.setBoolean(13, seedPrice.chemicalTreatment);
            statement.setBoolean(14, seedPrice.includedTreatment);
            statement.setBoolean(15, seedPrice.organic);
            setSqlSeedType(16, seedPrice.getSeedType(), statement);
            statement.addBatch();
        }
    }

    protected Map<Pair<String, String>, List<InputPrice>> bindToInputPrices(
            Connection connection,
            String findPricesQuery,
            boolean isPracticed,
            PriceCategory priceCategory,
            InputPriceCategory inputPriceCategory,
            Set<String> objectIds,
            Set<String> practicedSystemOrDomainIds,
            Map<String, String> practicedIdToDomainId,
            String defaultSourceUnit,
            Map<InputPrice, InputPrice> uniquePrices) {

        Map<Pair<String, String>, List<InputPrice>> inputPriceByDomainIdObjectId = new HashMap<>();

        Iterable<List<String>> practicedSystemOrDomainIdChunks = Iterables.partition(practicedSystemOrDomainIds, 100);

        for (List<String> practicedSystemOrDomainIdChunk : practicedSystemOrDomainIdChunks) {
            String practicedSystemOrDomainIdQueryParts = formatInValues(StringUtils.join(practicedSystemOrDomainIdChunk, "','"));

            Iterable<List<String>> objectIdChunks = Iterables.partition(objectIds, 100);
            for (List<String> objectIdChunk : objectIdChunks) {

                String objectIdQueryParts = formatInValues(StringUtils.join(objectIdChunk, "','"));
                final String loadPriceQuery = String.format(findPricesQuery, objectIdQueryParts, practicedSystemOrDomainIdQueryParts);

                inputPriceByDomainIdObjectId = loadPrices(
                        connection,
                        isPracticed,
                        priceCategory,
                        inputPriceCategory,
                        practicedIdToDomainId,
                        defaultSourceUnit,
                        loadPriceQuery,
                        uniquePrices);
            }

        }
        return inputPriceByDomainIdObjectId;
    }

    protected Map<Pair<String, String>, List<InputPrice>> loadPrices(
            Connection connection,
            boolean isPracticed,
            PriceCategory priceCategory,
            InputPriceCategory inputPriceCategory,
            Map<String, String> practicedIdToDomainId,
            String defaultSourceUnit,
            String loadPriceQuery,
            Map<InputPrice, InputPrice> uniquePrices) {

        Map<Pair<String, String>, List<InputPrice>> inputPriceByDomainIdObjectId = new HashMap<>();

        try (PreparedStatement findPriceStatement = connection.prepareStatement(loadPriceQuery)) {
            findPriceStatement.setString(1, priceCategory.name());
            ResultSet rs = findPriceStatement.executeQuery();
            while (rs.next()) {
                final String domainId;

                if (isPracticed) {
                    final String practicedSystemId = rs.getString("practicedsystem");
                    domainId = practicedIdToDomainId.get(practicedSystemId);
                } else {
                    domainId = rs.getString("domain");
                }

                final String sourceUnit = StringUtils.firstNonBlank(rs.getString("sourceunit"), defaultSourceUnit);

                final ProductType phytoProductType1 = ProductType.OTHER;
                final String objectid = rs.getString("objectid");
                InputPrice inputPrice = new InputPrice();
                inputPrice.setDeprecatedTopiaId(rs.getString("topiaid"));
                inputPrice.setTopiaVersion(0);
                inputPrice.setTopiaDiscriminator(InputPriceImpl.class.getName());
                inputPrice.setObjectId(objectid);
                inputPrice.setPriceUnit(PriceUnit.valueOf(rs.getString("priceunit")));
                inputPrice.setDomainId(domainId);
                inputPrice.setPracticedSystemId(rs.getString("practicedsystem"));
                inputPrice.setDisplayName(rs.getString("displayname"));
                inputPrice.setPrice(rs.getDouble("price"));
                inputPrice.setSourceUnit(sourceUnit);
                inputPrice.setCategory(inputPriceCategory);
                inputPrice.setPhytoProductType(phytoProductType1);

                if (StringUtils.isBlank(domainId)) {
                    LOG.warn("Price domainId could not be found for price " + inputPrice);
                    continue;
                }

                final Pair<String, String> key = Pair.of(domainId, objectid);
                if (uniquePrices.get(inputPrice) == null) {
                    uniquePrices.put(inputPrice, inputPrice);
                    List<InputPrice> abstractInputPrices = inputPriceByDomainIdObjectId.computeIfAbsent(key, k -> new ArrayList<>());
                    abstractInputPrices.add(inputPrice);
                }

            }
        } catch (SQLException e) {
            LOG.error(loadPriceQuery);
            throw new RuntimeException(e);
        }
        return inputPriceByDomainIdObjectId;
    }

    protected List<SeedPrice> loadSeedPrices(
            Connection connection) {

        List<SeedPrice> seedPrices = new ArrayList<>();

        try (Statement findPriceStatement = connection.createStatement()) {
            ResultSet rs = findPriceStatement.executeQuery(V3_0_0_9__11484_input_storage.FIND_SEEDING_CROP_PRICES_FROM_ID_QUERY);
            while (rs.next()) {

                final ProductType phytoProductType1 = ProductType.OTHER;
                final String objectid = rs.getString("objectid");
                SeedPrice seedPrice = new SeedPrice();
                seedPrice.setDeprecatedTopiaId(rs.getString("topiaid"));
                seedPrice.setTopiaVersion(0);
                seedPrice.setTopiaDiscriminator(SeedPriceImpl.class.getName());
                seedPrice.setObjectId(objectid);
                seedPrice.setPriceUnit(PriceUnit.valueOf(rs.getString("priceunit")));
                seedPrice.setDomainId(rs.getString("domain"));
                seedPrice.setPracticedSystemId(rs.getString("practicedsystem"));
                seedPrice.setDisplayName(rs.getString("displayname"));
                seedPrice.setPrice(rs.getDouble("price"));
                seedPrice.setCategory(InputPriceCategory.SEEDING_INPUT);
                seedPrice.setPhytoProductType(phytoProductType1);

                seedPrice.setBiologicalSeedInoculation(rs.getBoolean("biologicaltreatment"));
                seedPrice.setChemicalTreatment(rs.getBoolean("chemicaltreatment"));
                seedPrice.setIncludedTreatment(rs.getBoolean("includedtreatment"));
                seedPrice.setOrganic(false);
                String seedtype = rs.getString("seedtype");
                if (seedtype != null) {
                    seedPrice.setSeedType(SeedType.valueOf(seedtype));
                }
                seedPrices.add(seedPrice);


            }
        } catch (SQLException e) {
            LOG.error(FIND_SEEDING_CROP_PRICES_FROM_ID_QUERY);
            throw new RuntimeException(e);
        }

        setDomainIdForPracticedSystemPrice(connection, seedPrices);

        return seedPrices;
    }

    private void setDomainIdForPracticedSystemPrice(Connection connection, List<SeedPrice> seedPrices) {
        MultiValuedMap<String, SeedPrice> practicedSystemSeedPrices = new HashSetValuedHashMap<>();

        seedPrices.stream().filter(asp -> asp.getPracticedSystemId() != null).forEach(sp -> practicedSystemSeedPrices.put(sp.getPracticedSystemId(), sp));

        Set<String> practicedSystemIds = seedPrices.stream().map(SeedPrice::getPracticedSystemId).filter(Objects::nonNull).collect(Collectors.toSet());
        try (Statement findDomainCodeAndIdFormPracticedSystem = connection.createStatement()) {
            ResultSet resultSet = findDomainCodeAndIdFormPracticedSystem.executeQuery(V3_0_0_9__11484_input_storage.PRACTICED_SYSTEM_ID_TO_DOMAIN_CODE_DOMAIN_ID_QUERY);
            while (resultSet.next()) {
                String practicedSystemId = resultSet.getString(1);
                String domainCode = resultSet.getString(2);
                String domainId = resultSet.getString(3);
                Collection<SeedPrice> abstractSeedPrices = practicedSystemSeedPrices.get(practicedSystemId);
                if (CollectionUtils.isNotEmpty(abstractSeedPrices)) {
                    for (SeedPrice abstractSeedPrice : abstractSeedPrices) {
                        abstractSeedPrice.setDomainId(domainId);
                        abstractSeedPrice.setDomainCode(domainCode);
                    }
                }
            }
        } catch (SQLException e) {
            LOG.error(FIND_SEEDING_CROP_PRICES_FROM_ID_QUERY);
            throw new RuntimeException(e);
        }
    }

    @Data
    protected static class RefActaTraitementsProduitRow {

        protected String topiaId;

        protected String id_produit;

        protected String nom_produit;

        protected int id_traitement;

        protected String nom_traitement;

        protected String code_traitement_maa;

        protected ProductType type_produit;

        protected AgrosystInterventionType action;

    }

    protected Map<String, RefActaTraitementsProduitRow> loadRefActaTraitementsProduitByIds(Connection connection, Collection<AbstractInputRow> inputRows) {
        Set<String> refInputIds = inputRows.stream().map(AbstractInputRow::phytoproduct).collect(Collectors.toSet());
        String refInputIdQueryParts = formatInValues(StringUtils.join(refInputIds, "','"));
        String getRefInputQueryRow =
                String.format(
                        "SELECT ratp.topiaid, ratp.id_produit, ratp.id_traitement, ratp.nom_produit, ratp.nom_traitement, ratpc.type_produit, ratpc.action " +
                                " FROM refactatraitementsproduit ratp INNER JOIN refactatraitementsproduitscateg ratpc ON  ratpc.id_traitement = ratp.id_traitement WHERE ratp.topiaid IN (%s)",
                        refInputIdQueryParts);

        LOG.info("Récupération des intrants phyto");
        Map<String, RefActaTraitementsProduitRow> refINPUTByIds = new HashMap<>();

        RefActaTraitementsProduitRow noProductRefActaTraitementsProduit = new RefActaTraitementsProduitRow();
        noProductRefActaTraitementsProduit.setTopiaId(DEFAULT_REF_ACTA_TRAITEMENTS_PRODUIT_ID);
        noProductRefActaTraitementsProduit.setId_produit("NO_PRODUCT");
        noProductRefActaTraitementsProduit.setId_traitement(99999);
        noProductRefActaTraitementsProduit.setNom_traitement("NO_PRODUCT");
        noProductRefActaTraitementsProduit.setCode_traitement_maa("NO_PRODUCT");

        refINPUTByIds.put(DEFAULT_REF_ACTA_TRAITEMENTS_PRODUIT_ID, noProductRefActaTraitementsProduit);

        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(getRefInputQueryRow)) {

            while (rs.next()) {
                RefActaTraitementsProduitRow refInput = new RefActaTraitementsProduitRow();
                refInput.setTopiaId(rs.getString("topiaid"));
                refInput.setId_produit(rs.getString("id_produit"));
                refInput.setId_traitement(rs.getInt("id_traitement"));
                refInput.setNom_traitement(rs.getString("nom_traitement"));
                refInput.setNom_produit(rs.getString("nom_produit"));
                refInput.setType_produit(ProductType.valueOf(rs.getString("type_produit")));
                refInput.setAction(AgrosystInterventionType.valueOf(rs.getString("action")));
                refINPUTByIds.put(refInput.getTopiaId(), refInput);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return refINPUTByIds;
    }

    protected Map<String, RefPot> loadRefPotByIds(Connection connection, Collection<AbstractInputRow> inputRows) {
        Set<String> refInputIds = inputRows.stream().map(AbstractInputRow::pot).collect(Collectors.toSet());
        String refInputIdQueryParts = formatInValues(StringUtils.join(refInputIds, "','"));
        String getRefInputQueryRow =
                String.format(
                        "SELECT topiaid, caracteristic1, volume " +
                                " FROM refpot WHERE topiaid IN (%s)",
                        refInputIdQueryParts);

        LOG.info("Récupération des intrants pots");
        Map<String, RefPot> refINPUTByIds = new HashMap<>();

        RefPot noRefPot = new RefPotImpl();
        noRefPot.setTopiaId(DEFAULT_REF_POT_ID);
        noRefPot.setCaracteristic1("NO_PRODUCT");
        noRefPot.setVolume(99999);

        refINPUTByIds.put(DEFAULT_REF_POT_ID, noRefPot);

        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(getRefInputQueryRow)) {

            while (rs.next()) {
                RefPot refInput = new RefPotImpl();
                refInput.setTopiaId(rs.getString("topiaid"));
                refInput.setCaracteristic1(rs.getString("caracteristic1"));
                refInput.setVolume(rs.getDouble("volume"));
                refINPUTByIds.put(refInput.getTopiaId(), refInput);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return refINPUTByIds;
    }

    protected Map<String, RefSubstrate> loadRefSubstratByIds(Connection connection, Collection<AbstractInputRow> inputRows) {
        Set<String> refInputIds = inputRows.stream().map(AbstractInputRow::substrate).collect(Collectors.toSet());
        String refInputIdQueryParts = formatInValues(StringUtils.join(refInputIds, "','"));
        String getRefInputQueryRow =
                String.format(
                        "SELECT topiaid, caracteristic1, caracteristic2, unittype " +
                                " FROM refsubstrate WHERE topiaid IN (%s)",
                        refInputIdQueryParts);

        LOG.info("Récupération des intrants substrat");
        Map<String, RefSubstrate> refINPUTByIds = new HashMap<>();

        RefSubstrate noRefRefSubstrate = new RefSubstrateImpl();
        noRefRefSubstrate.setTopiaId(DEFAULT_REF_SUBSTRAT_ID);
        noRefRefSubstrate.setCaracteristic1("NO_PRODUCT");
        noRefRefSubstrate.setCaracteristic2("NO_PRODUCT");
        noRefRefSubstrate.setUnitType(UnitType.VOLUME);

        refINPUTByIds.put(DEFAULT_REF_POT_ID, noRefRefSubstrate);

        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(getRefInputQueryRow)) {

            while (rs.next()) {
                RefSubstrate refInput = new RefSubstrateImpl();
                refInput.setTopiaId(rs.getString("topiaid"));
                refInput.setCaracteristic1(rs.getString("caracteristic1"));
                refInput.setCaracteristic2(rs.getString("caracteristic2"));
                refInput.setUnitType(UnitType.valueOf(rs.getString("unittype")));
                refINPUTByIds.put(refInput.getTopiaId(), refInput);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return refINPUTByIds;
    }

    protected Map<String, RefFertiOrga> loadRefFertiOrgaByIds(Connection connection, Collection<AbstractInputRow> inputRows) {
        Set<String> refInputIds = inputRows.stream().map(ir -> ir.organicproduct).collect(Collectors.toSet());
        String refInputIdQueryParts = formatInValues(StringUtils.join(refInputIds, "','"));
        String getRefInputQueryRow =
                String.format(
                        "SELECT topiaid, idtypeeffluent, libelle " +
                                " FROM reffertiorga WHERE topiaid IN (%s)",
                        refInputIdQueryParts);

        LOG.info("Récupération des intrants de fertilisation organique");
        Map<String, RefFertiOrga> refINPUTByIds = new HashMap<>();

        RefFertiOrga noProductRefFertiOrga = new RefFertiOrgaImpl();
        noProductRefFertiOrga.setTopiaId(DEFAULT_REF_FERTI_ORGA_ID);
        noProductRefFertiOrga.setIdtypeeffluent("NO_PRODUCT");
        noProductRefFertiOrga.setLibelle("NO_PRODUCT");
        noProductRefFertiOrga.setUnite_teneur_ferti_orga(FertiOrgaUnit.KG_T);
        noProductRefFertiOrga.setActive(false);

        refINPUTByIds.put(DEFAULT_REF_FERTI_ORGA_ID, noProductRefFertiOrga);

        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(getRefInputQueryRow)) {

            while (rs.next()) {
                RefFertiOrga refInput = new RefFertiOrgaImpl();
                refInput.setTopiaId(rs.getString("topiaid"));
                refInput.setIdtypeeffluent(rs.getString("idtypeeffluent"));
                refInput.setLibelle(rs.getString("libelle"));
                refINPUTByIds.put(refInput.getTopiaId(), refInput);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return refINPUTByIds;
    }

    protected Map<String, RefFertiMinUNIFA> loadRefFertiMinByIds(Connection connection, Collection<AbstractInputRow> inputRows) {
        Set<String> refFertiMinIds = inputRows.stream().map(ir -> ir.mineralproduct).collect(Collectors.toSet());
        String refFertiMinIdQueryParts = formatInValues(StringUtils.join(refFertiMinIds, "','"));
        String getRefFertiMinQueryRow =
                String.format(
                        "SELECT topiaid, categ, forme, n, p2o5, k2o, bore, calcium, fer, manganese, molybdene, mgo, oxyde_de_sodium, so3, cuivre, zinc " +
                                " FROM reffertiminunifa WHERE topiaid IN (%s)",
                        refFertiMinIdQueryParts);

        LOG.info("Récupération des intrants de fertilisation minérales:");
        Map<String, RefFertiMinUNIFA> refFertiMinUNIFAByIds = new HashMap<>();

        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(getRefFertiMinQueryRow)) {

            while (rs.next()) {
                RefFertiMinUNIFAImpl refFertiMinUNIFA = new RefFertiMinUNIFAImpl();
                refFertiMinUNIFA.setTopiaId(rs.getString("topiaid"));
                refFertiMinUNIFA.setCateg(rs.getInt("categ"));
                refFertiMinUNIFA.setForme(rs.getString("forme"));
                refFertiMinUNIFA.setN(rs.getDouble("n"));
                refFertiMinUNIFA.setP2O5(rs.getDouble("p2o5"));
                refFertiMinUNIFA.setK2O(rs.getDouble("k2o"));
                refFertiMinUNIFA.setBore(rs.getDouble("bore"));
                refFertiMinUNIFA.setCalcium(rs.getDouble("calcium"));
                refFertiMinUNIFA.setFer(rs.getDouble("fer"));
                refFertiMinUNIFA.setManganese(rs.getDouble("manganese"));
                refFertiMinUNIFA.setMolybdene(rs.getDouble("molybdene"));
                refFertiMinUNIFA.setMgO(rs.getDouble("mgo"));
                refFertiMinUNIFA.setOxyde_de_sodium(rs.getDouble("oxyde_de_sodium"));
                refFertiMinUNIFA.setsO3(rs.getDouble("so3"));
                refFertiMinUNIFA.setCuivre(rs.getDouble("cuivre"));
                refFertiMinUNIFA.setZinc(rs.getDouble("zinc"));
                refFertiMinUNIFAByIds.put(refFertiMinUNIFA.getTopiaId(), refFertiMinUNIFA);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return refFertiMinUNIFAByIds;
    }

    private record AbstractInputRow(
            String domainId, String domainCode, Integer domainCampaign, String inputId,
            String inputdiscriminator, String actionid, String actiondiscriminator,
            long topiaversion, Date topiacreatedate, String practicedSystemId, Double qtmin,
            Double qtavg, Double qtmed, Double qtmax, String productname, String inputtype,
            String producttype, String phytoproduct, String phytoproductunit,
            String oldproductqtunit, String otheraction, Double n, Double p2o5, Double k2o,
            String organicproduct, String organicfertilizersspreadingaction,
            String organicproductunit, String mineralproduct, String mineralproductunit,
            String mineralfertilizersspreadingaction, String pesticidesspreadingaction,
            String seedingaction, String biologicalcontrolaction, String harvestingaction,
            String irrigationaction, String maintenancepruningvinesaction, Boolean phytoeffect,
            String otherproductinputunit, String substrate, String substrateinputunit,
            String pot, String potinputunit) {

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            AbstractInputRow that = (AbstractInputRow) o;
            return domainId.equals(that.domainId) && inputdiscriminator.equals(that.inputdiscriminator) && Objects.equals(qtmin, that.qtmin) && Objects.equals(qtavg, that.qtavg) && Objects.equals(qtmed, that.qtmed) && Objects.equals(qtmax, that.qtmax) && Objects.equals(productname, that.productname) && Objects.equals(inputtype, that.inputtype) && Objects.equals(producttype, that.producttype) && Objects.equals(phytoproduct, that.phytoproduct) && Objects.equals(phytoproductunit, that.phytoproductunit) && Objects.equals(oldproductqtunit, that.oldproductqtunit) && Objects.equals(otheraction, that.otheraction) && Objects.equals(n, that.n) && Objects.equals(p2o5, that.p2o5) && Objects.equals(k2o, that.k2o) && Objects.equals(organicproduct, that.organicproduct) && Objects.equals(organicfertilizersspreadingaction, that.organicfertilizersspreadingaction) && Objects.equals(organicproductunit, that.organicproductunit) && Objects.equals(mineralproduct, that.mineralproduct) && Objects.equals(mineralproductunit, that.mineralproductunit) && Objects.equals(mineralfertilizersspreadingaction, that.mineralfertilizersspreadingaction) && Objects.equals(pesticidesspreadingaction, that.pesticidesspreadingaction) && Objects.equals(seedingaction, that.seedingaction) && Objects.equals(biologicalcontrolaction, that.biologicalcontrolaction) && Objects.equals(harvestingaction, that.harvestingaction) && Objects.equals(irrigationaction, that.irrigationaction) && Objects.equals(maintenancepruningvinesaction, that.maintenancepruningvinesaction) && Objects.equals(phytoeffect, that.phytoeffect) && Objects.equals(otherproductinputunit, that.otherproductinputunit) && Objects.equals(substrate, that.substrate) && Objects.equals(substrateinputunit, that.substrateinputunit) && Objects.equals(pot, that.pot) && Objects.equals(potinputunit, that.potinputunit);
        }

        @Override
        public int hashCode() {
            return Objects.hash(domainId, inputdiscriminator, qtmin, qtavg, qtmed, qtmax, productname, inputtype, producttype, phytoproduct, phytoproductunit, oldproductqtunit, otheraction, n, p2o5, k2o, organicproduct, organicfertilizersspreadingaction, organicproductunit, mineralproduct, mineralproductunit, mineralfertilizersspreadingaction, pesticidesspreadingaction, seedingaction, biologicalcontrolaction, harvestingaction, irrigationaction, maintenancepruningvinesaction, phytoeffect, otherproductinputunit, substrate, substrateinputunit, pot, potinputunit);
        }

        @Override
        public String toString() {
            return "AbstractInputRow{" +
                    "domainId='" + domainId + '\'' +
                    ", domainCode" + domainCode + '\'' +
                    ", domainCampaign" + domainCampaign + '\'' +
                    ", inputid='" + inputId + '\'' +
                    ", inputdiscriminator='" + inputdiscriminator + '\'' +
                    ", topiaversion=" + topiaversion +
                    ", topiacreatedate=" + topiacreatedate +
                    ", qtmin=" + qtmin +
                    ", qtavg=" + qtavg +
                    ", qtmed=" + qtmed +
                    ", qtmax=" + qtmax +
                    ", productname='" + productname + '\'' +
                    ", inputtype='" + inputtype + '\'' +
                    ", producttype='" + producttype + '\'' +
                    ", phytoproduct='" + phytoproduct + '\'' +
                    ", phytoproductunit='" + phytoproductunit + '\'' +
                    ", oldproductqtunit='" + oldproductqtunit + '\'' +
                    ", otheraction='" + otheraction + '\'' +
                    ", n=" + n +
                    ", p2o5=" + p2o5 +
                    ", k2o=" + k2o +
                    ", organicproduct='" + organicproduct + '\'' +
                    ", organicfertilizersspreadingaction='" + organicfertilizersspreadingaction + '\'' +
                    ", organicproductunit='" + organicproductunit + '\'' +
                    ", mineralproduct='" + mineralproduct + '\'' +
                    ", mineralproductunit='" + mineralproductunit + '\'' +
                    ", mineralfertilizersspreadingaction='" + mineralfertilizersspreadingaction + '\'' +
                    ", pesticidesspreadingaction='" + pesticidesspreadingaction + '\'' +
                    ", seedingaction='" + seedingaction + '\'' +
                    ", biologicalcontrolaction='" + biologicalcontrolaction + '\'' +
                    ", harvestingaction='" + harvestingaction + '\'' +
                    ", irrigationaction='" + irrigationaction + '\'' +
                    ", maintenancepruningvinesaction='" + maintenancepruningvinesaction + '\'' +
                    ", phytoeffect=" + phytoeffect +
                    ", otherproductinputunit='" + otherproductinputunit + '\'' +
                    ", substrate='" + substrate + '\'' +
                    ", substrateinputunit='" + substrateinputunit + '\'' +
                    ", pot='" + pot + '\'' +
                    ", potinputunit='" + potinputunit + '\'' +
                    '}';
        }
    }

    @Data
    protected static class SeedPrice extends InputPrice {
        /**
         * Nom de l'attribut en BD : biologicalSeedInoculation
         */
        protected boolean biologicalSeedInoculation;

        /**
         * Nom de l'attribut en BD : chemicalTreatment
         */
        protected boolean chemicalTreatment;

        /**
         * Nom de l'attribut en BD : includedTreatment
         */
        protected boolean includedTreatment;

        /**
         * Nom de l'attribut en BD : organic
         */
        protected boolean organic;

        /**
         * Nom de l'attribut en BD : seedType
         */
        protected SeedType seedType;

        public SeedPrice() {
            topiaId = fr.inra.agrosyst.api.entities.SeedPrice.class.getName() + "_" + UUID.randomUUID();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SeedPrice that = (SeedPrice) o;
            return Objects.equals(
                    topiaDiscriminator, that.topiaDiscriminator) &&
                    Objects.equals(objectId, that.objectId) &&
                    Objects.equals(displayName, that.displayName) &&
                    Objects.equals(sourceUnit, that.sourceUnit) &&
                    priceUnit == that.priceUnit &&
                    category == that.category &&
                    Objects.equals(domainId, that.domainId) &&
                    phytoProductType == that.phytoProductType &&
                    biologicalSeedInoculation == that.biologicalSeedInoculation &&
                    chemicalTreatment == that.chemicalTreatment &&
                    includedTreatment == that.includedTreatment &&
                    organic == that.organic &&
                    seedType == that.seedType;
        }

        @Override
        public int hashCode() {
            return Objects.hash(topiaDiscriminator, objectId, displayName, sourceUnit, priceUnit, category, domainId, phytoProductType, biologicalSeedInoculation, chemicalTreatment, includedTreatment, organic, seedType);
        }
    }

    @Data
    protected static class InputPrice {

        protected String deprecatedTopiaId;

        protected String topiaId;

        protected long topiaVersion = 0;

        protected String topiaDiscriminator;

        /**
         * Nom de l'attribut en BD : objectId
         */
        protected String objectId;

        /**
         * Nom de l'attribut en BD : displayName
         */
        protected String displayName;

        /**
         * Nom de l'attribut en BD : price
         */
        protected Double price;

        /**
         * Nom de l'attribut en BD : sourceUnit
         */
        protected String sourceUnit;

        /**
         * Nom de l'attribut en BD : priceUnit
         */
        protected PriceUnit priceUnit;

        /**
         * Nom de l'attribut en BD : category
         */
        protected InputPriceCategory category;

        /**
         * Nom de l'attribut en BD : domain
         */
        protected String domainId;

        protected String practicedSystemId;

        protected String domainCode;

        protected Integer domaincampaign;

        /**
         * Nom de l'attribut en BD : phytoProductType
         */
        protected ProductType phytoProductType;

        public InputPrice() {
            topiaId = fr.inra.agrosyst.api.entities.InputPrice.class.getName() + "_" + UUID.randomUUID();
        }

        @Override
        public boolean equals(Object o) {
            // do not include topiaId and practicedSystemId
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            InputPrice that = (InputPrice) o;
            return Objects.equals(topiaDiscriminator, that.topiaDiscriminator) &&
                    Objects.equals(objectId, that.objectId) &&
                    Objects.equals(displayName, that.displayName) &&
                    Objects.equals(sourceUnit, that.sourceUnit) &&
                    priceUnit == that.priceUnit &&
                    category == that.category &&
                    Objects.equals(domainId, that.domainId) &&
                    phytoProductType == that.phytoProductType;
        }

        @Override
        public int hashCode() {
            return Objects.hash(topiaDiscriminator, objectId, displayName, sourceUnit, priceUnit, category, domainId, phytoProductType);
        }
    }

    @Data
    protected static class IrrigationActionRow {
        String actionId;
        String domainId;
        String domainCode;
        int domainCampaign;
        Double waterquantitymin;
        Double waterquantitymax;
        double waterquantityaverage;
        Double waterquantitymedian;
    }

    @Data
    protected static class SeedingActionAndSpeciesRow {
        String actionId;
        String mainAction;
        String comment;
        String toolsCouplingCode;
        String practicedIntervention;
        String effectiveIntervention;
        YealdUnit yealdUnit;
        SeedType seedType;
        Double yealdTarget;

        boolean isIntermediatecrop;

        String interventionCropId;
        String interventionCropCode;
        String interventionIntermediateCropId;
        String interventionIntermediateCropCode;

        String domainId;
        String domainCode;
        int domainCampaign;

        String seedingActionSpeciesCropId;

        String psCampaigns;
        String practicedSystemId;
        String gsCode;

        String seedingActionSpeciesId;

        Double seedingActionSpeciesQuantity;

        String seedingActionSpeciesCPSCode;

        String seedingActionSpeciesCPSId;

        Double seedingActionSpeciesDeepness;

        Boolean seedingActionSpeciesTreatment;

        Boolean seedingActionSpeciesBiologicalSeedInoculation;

        SeedPlantUnit seedingActionSpeciesUsageUnit;

        Integer type_agriculture_id;

        String crop_name;

        String intermediate_crop_name;

        String libelle_espece_botanique;
        
        String libelle_qualifiant_aee;
        
        String libelle_type_saisonnier_aee;
        
        String libelle_destination_aee;

        String libelle_variete;

        String priceId;

        @Override
        public String toString() {
            return "SeedingActionAndSpeciesRow{" +
                    "actionId='" + actionId + '\'' +
                    ", mainAction='" + mainAction + '\'' +
                    ", comment='" + comment + '\'' +
                    ", toolsCouplingCode='" + toolsCouplingCode + '\'' +
                    ", practicedIntervention='" + practicedIntervention + '\'' +
                    ", effectiveIntervention='" + effectiveIntervention + '\'' +
                    ", yealdUnit=" + yealdUnit +
                    ", seedType=" + seedType +
                    ", yealdTarget=" + yealdTarget +
                    ", isIntermediatecrop=" + isIntermediatecrop +
                    ", interventionCropId='" + interventionCropId + '\'' +
                    ", interventionCropCode='" + interventionCropCode + '\'' +
                    ", interventionIntermediateCropId='" + interventionIntermediateCropId + '\'' +
                    ", interventionIntermediateCropCode='" + interventionIntermediateCropCode + '\'' +
                    ", domainId='" + domainId + '\'' +
                    ", domainCode='" + domainCode + '\'' +
                    ", domainCampaign=" + domainCampaign +
                    ", seedingActionSpeciesCropId='" + seedingActionSpeciesCropId + '\'' +
                    ", psCampaigns='" + psCampaigns + '\'' +
                    ", practicedSystemId='" + practicedSystemId +'\'' +
                    ", gsCode='" + gsCode + '\'' +
                    ", seedingActionSpeciesId='" + seedingActionSpeciesId + '\'' +
                    ", seedingActionSpeciesQuantity=" + seedingActionSpeciesQuantity +
                    ", seedingActionSpeciesCPSCode='" + seedingActionSpeciesCPSCode + '\'' +
                    ", seedingActionSpeciesCPSId='" + seedingActionSpeciesCPSId + '\'' +
                    ", seedingActionSpeciesDeepness=" + seedingActionSpeciesDeepness +
                    ", seedingActionSpeciesTreatment=" + seedingActionSpeciesTreatment +
                    ", seedingActionSpeciesBiologicalSeedInoculation=" + seedingActionSpeciesBiologicalSeedInoculation +
                    ", seedingActionSpeciesUsageUnit=" + seedingActionSpeciesUsageUnit +
                    ", type_agriculture_id=" + type_agriculture_id +
                    ", crop_name='" + crop_name + '\'' +
                    ", intermediate_crop_name='" + intermediate_crop_name + '\'' +
                    ", libelle_espece_botanique='" + libelle_espece_botanique + '\'' +
                    ", libelle_qualifiant_aee='" + libelle_espece_botanique + '\'' +
                    ", libelle_type_saisonnier_aee='" + libelle_type_saisonnier_aee + '\'' +
                    ", libelle_destination_aee='" + libelle_destination_aee + '\'' +
                    ", libelle_variete='" + libelle_variete + '\'' +
                    ", priceId='" + priceId + '\'' +
                    '}';
        }
    }

    @Data
    protected class DbAbstractDomainInputStockUnit {
        String topiaId;

        int topiaversion = 0;

        java.sql.Date topiacreatedate = scriptDate;

        String key;

        String inputName;

        InputType inputtype = InputType.SEMIS;

        String domain;

        String domainCode;

        String inputprice = null;

        String code;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DbAbstractDomainInputStockUnit that = (DbAbstractDomainInputStockUnit) o;
            return key.equals(that.key) && inputName.equals(that.inputName) && domain.equals(that.domain) && domainCode.equals(that.domainCode);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key, inputName, domain, domainCode);
        }

        @Override
        public String toString() {
            return "DbAbstractDomainInputStockUnit{" +
                    "topiaId='" + topiaId + '\'' +
                    ", topiaversion=" + topiaversion +
                    ", topiacreatedate=" + topiacreatedate +
                    ", key='" + key + '\'' +
                    ", inputName='" + inputName + '\'' +
                    ", inputtype=" + inputtype +
                    ", domain='" + domain + '\'' +
                    ", domainCode='" + domainCode + '\'' +
                    ", inputprice='" + inputprice + '\'' +
                    ", code='" + code + '\'' +
                    '}';
        }
    }

    @Data
    protected class DbDomainSeedLotInput extends DbAbstractDomainInputStockUnit {
        // key: crop.getCode() + KEY_SEPARATOR + organic + KEY_SEPARATOR + usageUnit
        Map<String, DbDomainSeedSpeciesInput> domainSeedSpeciesInputs = new HashMap<>();

        boolean organic;
        String cropSeedId;

        String cropSeedCode;

        String seedPriceId;
        SeedPlantUnit usageUnit;

        SeedPrice seedPrice;

        public DbDomainSeedLotInput(){
            topiaId = DomainSeedLotInput.class.getName() + "_" + UUID.randomUUID();
            code = UUID.randomUUID().toString();
        }

        public boolean otherCampaignEquals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DbDomainSeedLotInput that = (DbDomainSeedLotInput) o;
            return organic == that.organic &&
                    domainCode.equals(that.domainCode) &&
                    key.equals(that.key) &&
                    domainSeedSpeciesInputs.keySet().equals(that.domainSeedSpeciesInputs.keySet()) &&
                    cropSeedId.equals(that.cropSeedId) &&
                    usageUnit.equals(that.usageUnit);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DbDomainSeedLotInput that = (DbDomainSeedLotInput) o;
            return organic == that.organic &&
                    domain.equals(that.domain) &&
                    key.equals(that.key) &&
                    domainSeedSpeciesInputs.keySet().equals(that.domainSeedSpeciesInputs.keySet()) &&
                    cropSeedId.equals(that.cropSeedId) &&
                    usageUnit.equals(that.usageUnit);
        }

        @Override
        public int hashCode() {
            return Objects.hash(organic, domain, key, domainSeedSpeciesInputs.keySet(), cropSeedId, usageUnit);
        }

        public void addDbDomainSeedSpeciesInput(DbDomainSeedSpeciesInput domainSeedSpeciesInput) {
            domainSeedSpeciesInputs.put(domainSeedSpeciesInput.getKey(), domainSeedSpeciesInput);
        }

        @Override
        public String toString() {
            return "DbDomainSeedLotInput{" +
                    "domainSeedSpeciesInputs=" + domainSeedSpeciesInputs +
                    ", organic=" + organic +
                    ", cropSeedId='" + cropSeedId + '\'' +
                    ", cropSeedCode='" + cropSeedCode + '\'' +
                    ", seedPriceId='" + seedPriceId + '\'' +
                    ", usageUnit=" + usageUnit +
                    ", seedPrice=" + seedPrice +
                    ", topiaId='" + topiaId + '\'' +
                    ", topiaversion=" + topiaversion +
                    ", topiacreatedate=" + topiacreatedate +
                    ", key='" + key + '\'' +
                    ", inputName='" + inputName + '\'' +
                    ", inputtype=" + inputtype +
                    ", domain='" + domain + '\'' +
                    ", domainCode='" + domainCode + '\'' +
                    ", inputprice='" + inputprice + '\'' +
                    ", code='" + code + '\'' +
                    '}';
        }
    }

    @Data
    protected class DbDomainSeedSpeciesInput extends DbAbstractDomainInputStockUnit {

        // key: code_espece_botanique + KEY_SEPARATOR + code_qualifiant_aee + KEY_SEPARATOR + biologicalSeedInoculation + KEY_SEPARATOR + chemicalTreatment + KEY_SEPARATOR + organic + KEY_SEPARATOR + SeedPlantUnit usageUnit
        boolean biologicalSeedInoculation;
        boolean chemicalTreatment;
        boolean organic;

        String speciesSeedId;

        String speciesSeedCode;
        SeedType seedType;
        //String seedPriceId;

        String domainSeedLotInputId;

        String speciesSimpleName;

        SeedPrice seedPrice;

        public DbDomainSeedSpeciesInput() {
            topiaId = DomainSeedSpeciesInput.class.getName() + "_" + UUID.randomUUID();
            code = UUID.randomUUID().toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DbDomainSeedSpeciesInput that = (DbDomainSeedSpeciesInput) o;
            return Objects.equals(domainSeedLotInputId, that.domainSeedLotInputId) &&
                    biologicalSeedInoculation == that.biologicalSeedInoculation &&
                    chemicalTreatment == that.chemicalTreatment &&
                    organic == that.organic &&
                    domain.equals(that.domain) &&
                    speciesSeedId.equals(that.speciesSeedId) &&
                    seedType.equals(that.seedType);
        }

        @Override
        public int hashCode() {
            return Objects.hash(domainSeedLotInputId, biologicalSeedInoculation, chemicalTreatment, organic, domain, speciesSeedId, seedType);
        }

        @Override
        public String toString() {
            return "DbDomainSeedSpeciesInput{" +
                    "biologicalSeedInoculation=" + biologicalSeedInoculation +
                    ", chemicalTreatment=" + chemicalTreatment +
                    ", organic=" + organic +
                    ", speciesSeedId='" + speciesSeedId + '\'' +
                    ", speciesSeedCode='" + speciesSeedCode + '\'' +
                    ", seedType=" + seedType +
                    ", domainSeedLotInputId='" + domainSeedLotInputId + '\'' +
                    ", topiaId='" + topiaId + '\'' +
                    ", topiaversion=" + topiaversion +
                    ", topiacreatedate=" + topiacreatedate +
                    ", key='" + key + '\'' +
                    ", inputName='" + inputName + '\'' +
                    ", inputName='" + speciesSimpleName + '\'' +
                    ", inputtype=" + inputtype +
                    ", domain='" + domain + '\'' +
                    ", inputprice='" + inputprice + '\'' +
                    ", code='" + code + '\'' +
                    '}';
        }
    }

    protected class DBSeedingActionUsage {
        String topiaId;

        Map<String, DbSeedLotInputUsage> seedLotInputUsageByDomainSeedLotInputId = new HashMap<>();

        private DBSeedingActionUsage(String actionId) {
            this.topiaId = actionId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DBSeedingActionUsage that = (DBSeedingActionUsage) o;
            return topiaId.equals(that.topiaId);
        }

        public DbSeedLotInputUsage getDbSeedLotInputUsageForSeedLotInputId(String domainSeedLotInputTopiaId) {
            return seedLotInputUsageByDomainSeedLotInputId.get(domainSeedLotInputTopiaId);
        }

        public void addtDbSeedLotInputUsageForSeedLotInputId(String domainSeedLotInputTopiaId, DbSeedLotInputUsage dbSeedLotInputUsage) {
            seedLotInputUsageByDomainSeedLotInputId.put(domainSeedLotInputTopiaId, dbSeedLotInputUsage);
        }



        @Override
        public int hashCode() {
            return Objects.hash(topiaId);
        }
    }

    @Data
    private class DbAbstrActinputUsage {
        String topiaId;
        int topiaVersion = 0;
        java.sql.Date topiaCreateDate = scriptDate;
        String comment;
        String productName;
        Double qtMin;
        Double qtAvg;
        Double qtMed;
        Double qtMax;
        InputType inputType;

        String code = UUID.randomUUID().toString();
    }

    @Data
    protected class DbSeedSpeciesInputUsage extends DbAbstrActinputUsage {

        public DbSeedSpeciesInputUsage() {
            topiaId = SeedSpeciesInputUsage.class.getName() + "_" + UUID.randomUUID();
            inputType = InputType.SEMIS;
        }

        Double deepness;
        String domainSeedSpeciesInput;

        // to match with phyto :
        String seedingActionId;

        String speciesSeed;

        boolean biologicalSeedInoculation;
        boolean chemicalTreatment;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DbSeedSpeciesInputUsage that = (DbSeedSpeciesInputUsage) o;
            return domainSeedSpeciesInput.equals(that.domainSeedSpeciesInput);
        }

        @Override
        public int hashCode() {
            return Objects.hash(domainSeedSpeciesInput);
        }

        @Override
        public String toString() {
            return "DbSeedSpeciesInputUsage{" +
                    ", deepness=" + deepness +
                    ", domainSeedSpeciesInput='" + domainSeedSpeciesInput + '\'' +
                    ", seedingActionId='" + seedingActionId + '\'' +
                    ", speciesSeed='" + speciesSeed + '\'' +
                    ", biologicalSeedInoculation=" + biologicalSeedInoculation +
                    ", chemicalTreatment=" + chemicalTreatment +
                    '}';
        }
    }

    @Data
    protected class DbSeedLotInputUsage extends DbAbstrActinputUsage {

        Map<String, DbSeedSpeciesInputUsage> dbSeedSpeciesInputUsageByDomainSeedSpeciesInput = new HashMap<>();

        String topiaId = SeedLotInputUsage.class.getName() + "_" + UUID.randomUUID();

        Double deepness;

        String domainseedlotinput;

        String seedingactionusage;

        public void addDbSeedSpeciesInputUsage(String domainSeedSpeciesInputId, DbSeedSpeciesInputUsage usage) {
            dbSeedSpeciesInputUsageByDomainSeedSpeciesInput.put(domainSeedSpeciesInputId, usage);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DbSeedLotInputUsage that = (DbSeedLotInputUsage) o;
            return domainseedlotinput.equals(that.domainseedlotinput) && seedingactionusage.equals(that.seedingactionusage);
        }

        @Override
        public int hashCode() {
            return Objects.hash(domainseedlotinput, seedingactionusage);
        }
    }

    record DomainIrrigationRow(
            String topiaId,
            String key,//unitName
            String inputname,
            InputType inputType,
            String domainId,
            String priceTopiaId,
            String inputCode


    ){
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DomainIrrigationRow that = (DomainIrrigationRow) o;
            return key.equals(that.key) && inputType == that.inputType && domainId.equals(that.domainId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key, inputType, domainId);
        }
    }

    record OtherProductComment(
            Double qtavg,
            String otherproductinputunit
    ) {}

    record TmpMissingPracticedCropAndSpecies (
            String croppingPlanEntryCode,
            String speciesCode,
            int cpeCount,
            int cpsCount,
            String domainId,

            String domainCode
    ) {}

    public static class NvCroppingPlanEntry extends CroppingPlanEntryAbstract {
        String domainId;

        final String newTopiaId = CroppingPlanEntry.class.getName() + "_" + UUID.randomUUID();

        Set<NvCroppingPlanSpecies> croppingPlanSpecies = new HashSet<>();

        public String getDomainId() {
            return domainId;
        }

        public void setDomainId(String domainId) {
            this.domainId = domainId;
        }

        public NvCroppingPlanEntry(String domainId) {
            this.domainId = domainId;
        }

        protected void addCroppingPlanSpecies(NvCroppingPlanSpecies cps) {
            if (cps != null) {
                cps.setCropId(newTopiaId);
                croppingPlanSpecies.add(cps);
            }
        }

        public String getNewTopiaId() {
            return newTopiaId;
        }

        protected List<NvCroppingPlanSpecies> getNotOrderedNvCroppingPlanSpecies() {
            List<NvCroppingPlanSpecies> result = new ArrayList<>(croppingPlanSpecies);
            return result;
        }

        protected List<NvCroppingPlanSpecies> getNvCroppingPlanSpecies() {
            List<NvCroppingPlanSpecies> result = new ArrayList<>(croppingPlanSpecies);
            result.sort(CPS_COMPARATOR);
            // fix index
            for (int index = 0; index < result.size(); index++) {
                NvCroppingPlanSpecies cps = result.get(index);
                cps.setCroppingplanentry_idx(index);
            }
            return result;
        }

        public static final Comparator<NvCroppingPlanSpecies> CPS_COMPARATOR = (o1, o2) -> {
            Double o1idx = o1.getCroppingplanentry_idx();
            Double o2idx = o2.getCroppingplanentry_idx();
            int result = o1idx.compareTo(o2idx);
            return result;
        };

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof NvCroppingPlanEntry that)) return false;

            return Objects.equals(domainId, that.domainId) &&
                    Objects.equals(code, that.code);
        }

        @Override
        public int hashCode() {
            return Objects.hash(domainId, code);
        }
    }

    public static class NvCroppingPlanSpecies extends CroppingPlanSpeciesImpl {

        final String newTopiaId = CroppingPlanSpecies.class.getName() + "_" + UUID.randomUUID();

        String cropId;

        String varietyId;

        String speciesId;

        String destinationId;

        double croppingplanentry_idx;

        public String getNewTopiaId() {
            return newTopiaId;
        }

        public String getCropId() {
            return cropId;
        }

        public void setCropId(String cropId) {
            this.cropId = cropId;
        }

        public String getVarietyId() {
            return varietyId;
        }

        public void setVarietyId(String varietyId) {
            this.varietyId = varietyId;
        }

        public String getSpeciesId() {
            return speciesId;
        }

        public void setSpeciesId(String speciesId) {
            this.speciesId = speciesId;
        }

        public String getDestinationId() {
            return destinationId;
        }

        public void setDestinationId(String destinationId) {
            this.destinationId = destinationId;
        }

        public double getCroppingplanentry_idx() {
            return croppingplanentry_idx;
        }

        public void setCroppingplanentry_idx(double croppingplanentry_idx) {
            this.croppingplanentry_idx = croppingplanentry_idx;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof NvCroppingPlanSpecies that)) return false;

            return Objects.equals(cropId, that.cropId) &&
                    Objects.equals(code, that.code);
        }

        @Override
        public int hashCode() {
            return Objects.hash(cropId, code);
        }
    }

    public enum PriceCategory {
        BIOLOGICAL_CONTROL_INPUT_CATEGORIE,
        HARVESTING_ACTION_PRICE_CATEGORIE,
        IRRIGATION_ACTION_CATEGORIE,
        MINERAL_INPUT_CATEGORIE,
        NONE_CATEGORIE,
        ORGANIQUES_INPUT_CATEGORIE,
        OTHER_INPUT_CATEGORIE,
        PHYTO_TRAITMENT_INPUT_CATEGORIE,
        POT_INPUT_CATEGORIE,
        SEEDING_ACTION_CATEGORIE,
        SEEDING_TREATMENT_INPUT_CATEGORIE,
        SUBSTRATE_INPUT_CATEGORIE
    }

}

