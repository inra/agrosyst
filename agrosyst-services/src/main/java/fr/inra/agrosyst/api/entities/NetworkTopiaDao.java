package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.SortedColumn;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.services.network.NetworkFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author cosse
 *
 */
public class NetworkTopiaDao extends AbstractNetworkTopiaDao<Network> {
    
    protected static final String PROPERTY_CAMPAIGN = Network.PROPERTY_NAME;


    public PaginationResult<Network> getFilteredNetworks(NetworkFilter filter, List<NetworkManager> managers) {
        StringBuilder query = new StringBuilder("FROM " + getEntityClass().getName() + " n ");
        query.append(" WHERE 1 = 1");
    
        Map<String, Object> args = Maps.newLinkedHashMap();
    
        // apply non null filter
        applyFiltersOnQuery(filter, managers, query, args);
    
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
    
        final String orderByQuery = getFilterOrderBy(filter);
    
        List<Network> networks = find(query + orderByQuery, args, startIndex, endIndex);
        long totalCount = findUnique("SELECT count(*) " + query, args);
    
        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        PaginationResult<Network> result = PaginationResult.of(networks, totalCount, pager);
        return result;
    }
    
    private String getFilterOrderBy(NetworkFilter filter) {
        String result = null;
        
        if (filter != null && filter.getSortedColumn() != null) {
            SortedColumn sortedColumn = filter.getSortedColumn();
            String filterOrderBy;
            if (sortedColumn == SortedColumn.NETWORK) {
                filterOrderBy = " lower (n." + Network.PROPERTY_NAME + ")";
            } else {
                filterOrderBy = null;
            }
            if (StringUtils.isNotBlank(filterOrderBy)) {
                filterOrderBy += " " + filter.getSortOrder().name();
                result = " ORDER BY " +
                        filterOrderBy +
                        ", n." + TopiaEntity.PROPERTY_TOPIA_ID;
            }
            
        }
        
        if (result == null) {
            // default
            result = " ORDER BY lower (n." + Network.PROPERTY_NAME + ")," +
                    " n." + TopiaEntity.PROPERTY_TOPIA_ID;
        }
        
        return result;
    }
    
    public Set<String> getFilteredNetworkIds(NetworkFilter filter, List<NetworkManager> managers) {
        StringBuilder query = new StringBuilder("SELECT n.topiaId FROM " + getEntityClass().getName() + " n ");
        query.append(" WHERE 1 = 1");
        
        Map<String, Object> args = Maps.newLinkedHashMap();
        applyFiltersOnQuery(filter, managers, query, args);
        return new HashSet<>(findAll(query.toString(), args));
    }

    private void applyFiltersOnQuery(NetworkFilter filter, List<NetworkManager> managers, StringBuilder query, Map<String, Object> args) {
        // apply non null filter
        if (filter != null) {
            // selected entities
            if (CollectionUtils.isNotEmpty(filter.getSelectedIds())) {
                Collection<String> ids = DaoUtils.getRealIds(filter.getSelectedIds(), Network.class);
                query.append(DaoUtils.andAttributeIn("n", TopiaEntity.PROPERTY_TOPIA_ID, args, ids));
            }
            // network name
            query.append(DaoUtils.andAttributeLike("n", Network.PROPERTY_NAME, args, filter.getNetworkName()));
            // active
            query.append(DaoUtils.andAttributeEquals("n", Network.PROPERTY_ACTIVE, args, filter.getActive()));
            // network managers
            if (managers != null) { // TODO AThimel 08/08/13 Find a better solution
                query.append(" AND ( 1=0 ");
                int index = 0;
                for (NetworkManager manager : managers) {
                    String key = "manager" + index++;
                    query.append(String.format(" OR :%s in elements ( %s )", key, "n." + Network.PROPERTY_MANAGERS));
                    args.put(key, manager);
                }
                query.append(" ) ");
            }
        }
    }

    protected Set<String> buildFullParentSet(Set<String> networkIds) {
        Set<String> result = Sets.newHashSet();
        if (networkIds != null) {
            for (String networkId : DaoUtils.getRealIds(networkIds, Network.class)) {
                buildParentSet(result, networkId);
            }
        }
        return result;
    }

    protected void buildParentSet(Set<String> result, String networkId) {
        if (!Strings.isNullOrEmpty(networkId) && !result.contains(networkId)) {
            Network network = forTopiaIdEquals(networkId).findUnique();
            result.add(networkId);
            if (network.getParents() != null) {
                for (Network parent : network.getParents()) {
                    buildParentSet(result, parent.getTopiaId());
                }
            }
        }
    }

    public LinkedHashMap<String, String> getNameFilteredNetworks(String name, Integer size, Set<String> exclusions,
                                                                 String selfNetworkId, boolean onNetworkEdition,
                                                                 Set<String> onlyNetworkIds) {
        String query = "SELECT n."+ Network.PROPERTY_TOPIA_ID +", n." + Network.PROPERTY_NAME
                + " FROM " + getEntityClass().getName() + " n"
                + " WHERE n." + Network.PROPERTY_ACTIVE + " = true ";

        Map<String, Object> args = Maps.newLinkedHashMap();
        query += DaoUtils.andAttributeLike("n", Network.PROPERTY_NAME, args, name);

        // Exclude parents
        Set<String> fullExclusionSet = buildFullParentSet(exclusions);

        // Exclude self and children networks
        if (!Strings.isNullOrEmpty(selfNetworkId)) {
            String networkid;
            if (!selfNetworkId.contains(Network.class.getName())) {
                networkid = Network.class.getName()  + selfNetworkId;
            } else {
                networkid = selfNetworkId;
            }
            fullExclusionSet.add(selfNetworkId);
            // Add all its children to avoid loops
            Set<Network> children = loadNetworksWithDescendantHierarchy(Sets.newHashSet(networkid));
            Iterables.addAll(fullExclusionSet, children.stream().map(Entities.GET_TOPIA_ID).collect(Collectors.toList()));
        }

        if (onNetworkEdition) {
            // Ignore all networks having some SdCs has children
            GrowingSystemTopiaDao growingSystemDAO = topiaDaoSupplier.getDao(GrowingSystem.class, GrowingSystemTopiaDao.class);
            Iterable<String> usedByGrowingSystemsNetworks = growingSystemDAO.getAllNetworksUsedByGrowingSystems();
            Iterables.addAll(fullExclusionSet, usedByGrowingSystemsNetworks);
        } else {
            // Ignore all networks having some networks has children
            Iterable<String> usedByNetworks = getAllNetworksUsedAsParents();
            Iterables.addAll(fullExclusionSet, usedByNetworks);
        }

        query += DaoUtils.andAttributeNotIn("n", Network.PROPERTY_TOPIA_ID, args, fullExclusionSet);

        if (onlyNetworkIds != null) {
            Set<String> ids = new HashSet<>(DaoUtils.getRealIds(onlyNetworkIds, Network.class));
            Set<Network> onlySubNetworks = loadNetworksWithDescendantHierarchy(ids);
            Set<String> onlySubNetworkIds = onlySubNetworks.stream().map(Entities.GET_TOPIA_ID).collect(Collectors.toSet());
            query += DaoUtils.andAttributeIn("n", Network.PROPERTY_TOPIA_ID, args, onlySubNetworkIds);
        }

        int nbResult = size != null ? size : 10;
        String order = " ORDER BY lower (n." + Network.PROPERTY_NAME + ")";
        List<Object[]> networks = find(query + order, args, 0, nbResult);
        LinkedHashMap<String, String> result = Maps.newLinkedHashMap();
        for (Object[] network : networks) {
            String topiaId = (String) network[0];
            String networkName = (String) network[1];
            result.put(topiaId, networkName);
        }
        return result;
    }

    public Set<Network> loadNetworksWithDescendantHierarchy(Set<String> networkIds) {
        Set<Network> result;
        if (networkIds != null && !networkIds.isEmpty()) {
            Collection<String> ids = DaoUtils.getRealIds(networkIds, Network.class);
            result = Sets.newHashSet(forTopiaIdIn(ids).findAll());
            Set<Network> children = findNetworkDescendantHierarchyExcluding(result, new HashSet<>());
            result.addAll(children);
        } else {
            result = new HashSet<>();
        }

        return result;
    }

    protected Set<Network> findNetworkDescendantHierarchyExcluding(Collection<Network> networks, Set<Network> networksToExclude) {
        Set<Network> children = Sets.newHashSet();
        if (networks != null && !networks.isEmpty()) {
            Set<Network> networksExcluded = Sets.newHashSet(networksToExclude);
            Map<String, Object> args = Maps.newLinkedHashMap();

            networksExcluded.addAll(networks);

            String query = " FROM " + getEntityClass().getName() + " n " +
                           " WHERE 1=1" +
                           " AND n NOT IN (:networksExcluded)";
            args.put("networksExcluded", networksExcluded);

            List<Network> res0 = findAll(query, args);

            Set<Network> networkList = new HashSet<>();
            for (Network parent : networks) {
                networkList.addAll(res0.stream().filter(network -> network.containsParents(parent)).collect(Collectors.toSet()));
            }

            if (!networkList.isEmpty()) {
                children.addAll(networkList);
                children.addAll(findNetworkDescendantHierarchyExcluding(networkList, networksExcluded));
            }
        }

        return children;
    }

    public Iterable<String> getAllNetworksUsedAsParents() {
        String hql = " SELECT DISTINCT n." + Network.PROPERTY_PARENTS +
                " FROM " + getEntityClass().getName() + " n " ;
        List<Network> networks = findAll(hql, DaoUtils.asArgsMap());

        Iterable<String> result = networks.stream().map(Entities.GET_TOPIA_ID).collect(Collectors.toList());
        return result;
    }

    public Iterable<Network> findAllByNameWithoutCase(String name) {
        String hql = newFromClause() + " WHERE UPPER(" + Network.PROPERTY_NAME + ") = :name";
        List<Network> networks = findAll(hql, DaoUtils.asArgsMap("name", name.toUpperCase()));
        return networks;
    }

    public List<Network> findAllNetworksForUserManager(String userId) {
        String hql = "SELECT DISTINCT n " + newFromClause("n")
                + " INNER JOIN n." + Network.PROPERTY_MANAGERS + " as m"
                + " WHERE m." + NetworkManager.PROPERTY_AGROSYST_USER + "." + AgrosystUser.PROPERTY_TOPIA_ID + " = :userId"
                + " AND (m." + NetworkManager.PROPERTY_FROM_DATE + " IS NULL OR m." + NetworkManager.PROPERTY_FROM_DATE + " <= current_date())"
                + " AND (m." + NetworkManager.PROPERTY_TO_DATE + " IS NULL OR m." + NetworkManager.PROPERTY_TO_DATE + " >= current_date())"
                + " AND m." + NetworkManager.PROPERTY_ACTIVE + " IS true";
        return findAll(hql, DaoUtils.asArgsMap("userId", userId));
    }
}
