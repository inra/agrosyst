package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.common.CommonService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

public class RefFeedbackRouterTopiaDao extends AbstractRefFeedbackRouterTopiaDao<RefFeedbackRouter> {
    
    public List<String> findDestinataires(FeedbackCategory category, TypeDEPHY typeDEPHY, List<Sector> sectors) {
        
        Map<String, Object> args = new HashMap<>();
        String query = "SELECT rr." + RefFeedbackRouter.PROPERTY_MAILS + " FROM " + getEntityClass().getName() + " rr WHERE 1=1";
        query += DaoUtils.andAttributeEquals("rr", RefFeedbackRouter.PROPERTY_ACTIVE, args, true);
        query += DaoUtils.andAttributeEquals("rr", RefFeedbackRouter.PROPERTY_FEEDBACK_CATEGORY, args, category);
        query += DaoUtils.andAttributeEquals("rr", RefFeedbackRouter.PROPERTY_TYPE_DEPHY, args, typeDEPHY);
        query += DaoUtils.andAttributeIn("rr", RefFeedbackRouter.PROPERTY_SECTOR, args, new HashSet<>(sectors));

        List<String> result0 = findAll(query, args);
        LinkedHashSet<String> result1 = new LinkedHashSet<>();
        for (String aList : result0) {
            result1.addAll(CommonService.EXTRACT_EMAILS.apply(aList));
        }
        
        return new ArrayList<>(result1);
    }

    public List<String> findCci(FeedbackCategory category, TypeDEPHY typeDEPHY) {
        Map<String, Object> args = new HashMap<>();
        String query = "SELECT rr." + RefFeedbackRouter.PROPERTY_CCI + " FROM " + getEntityClass().getName() + " rr WHERE 1=1 ";
        query += DaoUtils.andAttributeEquals("rr", RefFeedbackRouter.PROPERTY_ACTIVE, args, true);
        query += DaoUtils.andAttributeEquals("rr", RefFeedbackRouter.PROPERTY_FEEDBACK_CATEGORY, args, category);
        query += DaoUtils.andAttributeEquals("rr", RefFeedbackRouter.PROPERTY_TYPE_DEPHY, args, typeDEPHY);

        List<String> queryResult = findAll(query, args);
        LinkedHashSet<String> ccis = new LinkedHashSet<>();
        for (String aList : queryResult) {
            ccis.addAll(CommonService.EXTRACT_EMAILS.apply(aList));
        }

        return new ArrayList<>(ccis);
    }

} //RefFeedbackRouterTopiaDao
