package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.utils.DaoUtils;

import java.util.List;

public class RefSeedUnitsTopiaDao extends AbstractRefSeedUnitsTopiaDao<RefSeedUnits> {

    public List<SeedPlantUnit> findAllforRefEspeceIn(List<RefEspece> species) {
        String query = "SELECT DISTINCT rsu." + RefSeedUnits.PROPERTY_SEED_PLANT_UNIT + " FROM " + getEntityClass().getName() + " rsu" +
                " INNER JOIN " + RefEspece.class.getName() + " re ON re." + RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE + " = rsu." + RefSeedUnits.PROPERTY_CODE_ESPECE_BOTANIQUE +
                " WHERE re." + RefEspece.PROPERTY_CODE_QUALIFIANT__AEE + " = rsu." + RefSeedUnits.PROPERTY_CODE_QUALIFIANT__AEE +
                " AND rsu." + RefSeedUnits.PROPERTY_ACTIVE + " IS TRUE" +
                " AND re IN :refEspece";

        List<SeedPlantUnit> seedPlantUnits = findAll(query, DaoUtils.asArgsMap("refEspece", species));
        return seedPlantUnits;
    }
} //RefSeedUnitsTopiaDao
