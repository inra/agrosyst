package fr.inra.agrosyst.api.entities.security;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.services.users.UserFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.List;
import java.util.Map;

public class AgrosystUserTopiaDao extends AbstractAgrosystUserTopiaDao<AgrosystUser> {

    public PaginationResult<AgrosystUser> getFilteredUsers(UserFilter filter) {
        String query = "FROM " + getEntityClass().getName() + " u ";
        query += " WHERE 1 = 1";
        Map<String, Object> args = Maps.newLinkedHashMap();

        // apply non null filter
        if (filter != null) {

            // first name
            query += DaoUtils.andAttributeLike("u", AgrosystUser.PROPERTY_FIRST_NAME, args, filter.getFirstName());

            // last name
            query += DaoUtils.andAttributeLike("u", AgrosystUser.PROPERTY_LAST_NAME, args, filter.getLastName());

            // email
            query += DaoUtils.andAttributeLike("u", AgrosystUser.PROPERTY_EMAIL, args, filter.getEmail());

            // organisation
            query += DaoUtils.andAttributeLike("u", AgrosystUser.PROPERTY_ORGANISATION, args, filter.getOrganisation());

            if (filter.getRoleType() != null) {
                String subQuery = " SELECT DISTINCT ur." + UserRole.PROPERTY_AGROSYST_USER + " FROM " + UserRole.class.getName() + " ur ";
                subQuery += " WHERE 1=1 ";
                subQuery += DaoUtils.andAttributeEquals("ur", UserRole.PROPERTY_TYPE, args, filter.getRoleType());

                query += " AND u IN ( " + subQuery + " ) ";
            }

            // active
            query += DaoUtils.andAttributeEquals("u", AgrosystUser.PROPERTY_ACTIVE, args, filter.getActive());

        }

        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
        List<AgrosystUser> users = find(query + " ORDER BY lower (u." + AgrosystUser.PROPERTY_LAST_NAME + "), u." + AgrosystUser.PROPERTY_FIRST_NAME +
                ", u." + TopiaEntity.PROPERTY_TOPIA_ID, args, startIndex, endIndex);
        long totalCount = findUnique("SELECT count(*) " + query, args);

        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        PaginationResult<AgrosystUser> result = PaginationResult.of(users, totalCount, pager);
        return result;
    }
    
    public List<AgrosystUser> getNameFilteredActiveUsers(String name, Integer size) {
        String query = "FROM " + getEntityClass().getName() + " U";
        query += " WHERE 1 = 0";
        Map<String, Object> args = Maps.newLinkedHashMap();
        if (!StringUtils.isBlank(name)){

            // Prénom
            query += DaoUtils.orAttributeLike("U", AgrosystUser.PROPERTY_FIRST_NAME, args, name);

            // Nom
            query += DaoUtils.orAttributeLike("U", AgrosystUser.PROPERTY_LAST_NAME, args, name);
            
            // active
            query += DaoUtils.andAttributeEquals("U", AgrosystUser.PROPERTY_ACTIVE, args, true);
        }
        int nbResult = size != null ? size : 10;
        List<AgrosystUser> users = find(query + " ORDER BY lower (U." + AgrosystUser.PROPERTY_LAST_NAME + "), U." + AgrosystUser.PROPERTY_FIRST_NAME, args, 0, nbResult);
        return users;
    }
    
    public List<String> getActiveUserEmails() {
        String query = "SELECT lower(U." + AgrosystUser.PROPERTY_EMAIL + ") FROM " + getEntityClass().getName() + " U";
        query += " WHERE 1 = 1";
        Map<String, Object> args = Maps.newLinkedHashMap();
        query += DaoUtils.andAttributeEquals("U", AgrosystUser.PROPERTY_ACTIVE, args, true);
        List<String> userEmails = findAll(query, args);
        return userEmails;
    }

} //AgrosystUserTopiaDao<E extends User>
