package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RefTypeAgricultureTopiaDao extends AbstractRefTypeAgricultureTopiaDao<RefTypeAgriculture> {

    public static final String ALIAS = "rta";

    public List<RefTypeAgriculture> findAllTypeAgricultures(Language language) {
        return findTypeAgricultures(language, null);
    }

    public List<RefTypeAgriculture> findTypeAgricultures(Language language, Collection<Integer> typeConduiteIds) {
        List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(language);

        Map<String, Object> args = new HashMap<>();
        String hql = "SELECT " + ALIAS + ", " + I18nDaoHelper.buildQueryTradObjectAlias(i18nDaoHelpers) +
                newFromClause(ALIAS)
                + I18nDaoHelper.buildQueryJoinTranslations(i18nDaoHelpers)
                + " WHERE " + ALIAS + "." + RefTypeAgriculture.PROPERTY_ACTIVE + " = true";
        if (CollectionUtils.isNotEmpty(typeConduiteIds)) {
            hql += " AND " + ALIAS + "." + RefTypeAgriculture.PROPERTY_REFERENCE_ID + " IN (:typeConduiteIds)";
            args.put("typeConduiteIds", typeConduiteIds);
        }

        List<Object[]> resultWithTranslation = findAll(hql, args);
        return resultWithTranslation.stream()
                .map(objects -> {
                    RefTypeAgriculture refTypeAgriculture = (RefTypeAgriculture) objects[0];
                    I18nDaoHelper.tryGetTranslation(objects[1]).ifPresent(refTypeAgriculture::setReference_label_Translated);
                    return refTypeAgriculture;
                })
                .sorted(Comparator.comparing(RefTypeAgriculture::getReference_label_Translated))
                .collect(Collectors.toList());
    }

    public ReferentialTranslationMap fillTranslations(Collection<String> topiaIdList, ReferentialTranslationMap translationMap) {
        List<I18nDaoHelper> i18nDaoHelpers = getI18nDaoHelpers(translationMap.getLanguage());
        I18nDaoHelper.fillRefEntitiesTranslations(this, ALIAS, i18nDaoHelpers, topiaIdList, translationMap);
        return translationMap;
    }

    protected List<I18nDaoHelper> getI18nDaoHelpers(Language language) {
        return List.of(
                I18nDaoHelper.withSimpleI18nKey(RefTypeAgriculture.PROPERTY_REFERENCE_LABEL,
                        language,
                        TradRefDivers.class,
                        ALIAS
                )
        );
    }

} //RefTypeAgricultureTopiaDao
