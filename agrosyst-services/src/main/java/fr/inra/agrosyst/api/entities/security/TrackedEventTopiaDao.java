package fr.inra.agrosyst.api.entities.security;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.services.security.TrackedEventFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.List;
import java.util.Map;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class TrackedEventTopiaDao extends AbstractTrackedEventTopiaDao<TrackedEvent>{
    
    public PaginationResult<TrackedEvent> getFilteredTrackedEvents(TrackedEventFilter filter) throws TopiaException {
    
        Map<String, Object> args = Maps.newLinkedHashMap();
        
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
        
        // refs:9570 TopiaQueryException: unable to find page startIndex=-1, endIndex=-3
        startIndex = Math.max(startIndex, 0);
        endIndex = Math.max(endIndex, DaoUtils.NO_PAGE_LIMIT);
        
        String queryString = "FROM " + getEntityClass().getName() + " TE" + " WHERE 1 = 1"
                // refs:9570 TopiaQueryException: unable to find page startIndex=-1, endIndex=-3
                ;
        String queryAndOrder = queryString + " ORDER BY TE." + TrackedEvent.PROPERTY_DATE + " DESC";
        List<TrackedEvent> trackedEvents = find(queryAndOrder, args, startIndex, endIndex);
        long totalCount = findUnique("SELECT count(*) " + queryString, args);
        
        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        return PaginationResult.of(trackedEvents, totalCount, pager);
    }
}
