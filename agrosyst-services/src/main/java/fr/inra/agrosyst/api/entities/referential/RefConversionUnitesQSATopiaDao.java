package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.PhytoProductUnit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RefConversionUnitesQSATopiaDao extends AbstractRefConversionUnitesQSATopiaDao<RefConversionUnitesQSA> {

    public Map<String, Double> findAllConversionsToKgHa() {
        String query = """
            SELECT %s, %s
            FROM %s r
            WHERE r.%s = :uniteKgHa
            AND r.%s IS TRUE
        """.formatted(
                RefConversionUnitesQSA.PROPERTY_UNITE_SA,
                RefConversionUnitesQSA.PROPERTY_QSA_KG_HA,
                RefConversionUnitesQSA.class.getName(),
                RefConversionUnitesQSA.PROPERTY_UNITE_SAISIE,
                RefConversionUnitesQSA.PROPERTY_ACTIVE
        );

        final List<Object[]> unitsAndCoefs = findAll(query, Map.of("uniteKgHa", PhytoProductUnit.KG_HA));

        Map<String, Double> conversionRatesByUnit = new HashMap<>();
        unitsAndCoefs.forEach(unitAndCoef -> conversionRatesByUnit.put((String) unitAndCoef[0], (Double) unitAndCoef[1]));

        return conversionRatesByUnit;
    }

}
