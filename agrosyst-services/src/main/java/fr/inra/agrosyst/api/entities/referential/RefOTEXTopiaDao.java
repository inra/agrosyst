package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author cosse
 *
 */
public class RefOTEXTopiaDao extends AbstractRefOTEXTopiaDao<RefOTEX> {

    public Map<Integer, String> findAllActiveCodeOtex70ByOtex18code(Integer otex18code, Language language) {

        I18nDaoHelper libelleI18nDaoHelper =
                I18nDaoHelper.withComplexI18nKey(
                        RefOTEX.PROPERTY_LIBELLE__OTEX_70_POSTES,
                        language,
                        TradRefDivers.class,
                        "ro"
                );

        String hql = String.join("\n"
                , "SELECT"
                    + " ro." + RefOTEX.PROPERTY_CODE__OTEX_70_POSTES + ", "
                    + libelleI18nDaoHelper.coalesceTranslation() + " as libelle"
                , newFromClause("ro")
                    + libelleI18nDaoHelper.leftJoinTranslation()
                , " where ro." + RefOTEX.PROPERTY_ACTIVE + " = true"
        );

        Map<String, Object> args = Maps.newLinkedHashMap();

        hql += DaoUtils.andAttributeEquals("ro", RefOTEX.PROPERTY_CODE__OTEX_18_POSTES, args, otex18code);

        hql += " ORDER BY libelle";
        
        Map<Integer, String> otexByCodes = this.<Object[]>findAll(hql, args).stream()
                .collect(Collectors.toMap(row -> (Integer) row[0], row -> row[0] + " : " + row[1]));
        return otexByCodes;
    }
    
    public Map<Integer, String> findAllActiveOtex18Code(Language language) {

        I18nDaoHelper libelleI18nDaoHelper =
                I18nDaoHelper.withSimpleI18nKey(
                        RefOTEX.PROPERTY_LIBELLE__OTEX_18_POSTES,
                        language,
                        TradRefDivers.class,
                        "ro"
                );

        String hql = String.join("\n"
                , "SELECT"
                    + " ro." + RefOTEX.PROPERTY_CODE__OTEX_18_POSTES + ", "
                    + libelleI18nDaoHelper.coalesceTranslation() + " as libelle"
                , newFromClause("ro")
                    + libelleI18nDaoHelper.leftJoinTranslation()
                , "WHERE ro." + RefOTEX.PROPERTY_ACTIVE + " = true"
                , "GROUP BY ro." + RefOTEX.PROPERTY_CODE__OTEX_18_POSTES  + ", libelle"
                , "ORDER BY libelle"
        );

        Map<Integer, String> otex18ByCodes = this.<Object[]>findAll(hql).stream()
                .collect(Collectors.toMap(row -> (Integer) row[0], row -> row[0] + " : " + row[1]));
        return otex18ByCodes;
    }
}
