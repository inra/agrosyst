package fr.inra.agrosyst.api.entities.action;

import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.utils.DaoUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.support.SqlFunction;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * @author david cosse@codelutin.com
 */
public class HarvestingActionTopiaDao extends AbstractHarvestingActionTopiaDao<HarvestingAction> {

    public Collection<HarvestingAction> findAllHarvestingActionForGrowingSystem(GrowingSystem growingSystem) {

        if (growingSystem == null) {
            return new ArrayList<>();
        }

        Collection<HarvestingAction> result;
        StringBuilder query = new StringBuilder("SELECT ha FROM " + getEntityClass().getName() + " ha ");
        query.append(" INNER JOIN ha." + HarvestingAction.PROPERTY_PRACTICED_INTERVENTION + " pi ");
        query.append(" INNER JOIN pi." + PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE + " phase ");
        query.append(" INNER JOIN phase." + PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE + " cycle ");
        query.append(" INNER JOIN cycle." + PracticedCropCycle.PROPERTY_PRACTICED_SYSTEM + " ps ");
        query.append(" WHERE ps." + PracticedSystem.PROPERTY_GROWING_SYSTEM + " = :growingSystem ");

        result = findAll(query.toString(), DaoUtils.asArgsMap("growingSystem", growingSystem));

        query = new StringBuilder("SELECT ha FROM " + getEntityClass().getName() + " ha ");
        query.append(" INNER JOIN ha." + HarvestingAction.PROPERTY_PRACTICED_INTERVENTION + " pi ");
        query.append(" INNER JOIN pi." + PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION + " conn ");
        query.append(" INNER JOIN conn." + PracticedCropCycleConnection.PROPERTY_TARGET + " target ");
        query.append(" INNER JOIN target." + PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE + " cycle ");
        query.append(" INNER JOIN cycle." + PracticedCropCycle.PROPERTY_PRACTICED_SYSTEM + " ps ");
        query.append(" WHERE ps." + PracticedSystem.PROPERTY_GROWING_SYSTEM + " = :growingSystem ");

        result.addAll(findAll(query.toString(), DaoUtils.asArgsMap("growingSystem", growingSystem)));

        query = new StringBuilder("SELECT escc." + EffectiveSeasonalCropCycle.PROPERTY_NODES);
        query.append(" FROM " + EffectiveSeasonalCropCycle.class.getName() + " escc");
        query.append(" INNER JOIN escc." + EffectiveSeasonalCropCycle.PROPERTY_ZONE + " zone ");
        query.append(" INNER JOIN zone." + Zone.PROPERTY_PLOT + " plot ");
        query.append(" WHERE plot." + Plot.PROPERTY_GROWING_SYSTEM + " = :growingSystem ");

        List<EffectiveCropCycleNode> effectiveCropCycleNodes = findAll(query.toString(), DaoUtils.asArgsMap("growingSystem", growingSystem));

        if (CollectionUtils.isNotEmpty(effectiveCropCycleNodes)) {
            Map<String, Object> args = DaoUtils.asArgsMap();
            query = new StringBuilder("SELECT ha FROM " + getEntityClass().getName() + " ha ");
            query.append(" INNER JOIN ha." + HarvestingAction.PROPERTY_EFFECTIVE_INTERVENTION + " ei ");
            query.append(" WHERE 1 = 1");
            query.append(DaoUtils.andAttributeIn("ei", EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_NODE, args, effectiveCropCycleNodes));

            result.addAll(findAll(query.toString(), args));
        }

        query = new StringBuilder("SELECT epcc." + EffectivePerennialCropCycle.PROPERTY_PHASE);
        query.append(" FROM " + EffectivePerennialCropCycle.class.getName() + " epcc");
        query.append(" INNER JOIN epcc." + EffectivePerennialCropCycle.PROPERTY_ZONE + " zone ");
        query.append(" INNER JOIN zone." + Zone.PROPERTY_PLOT + " plot ");
        query.append(" WHERE plot." + Plot.PROPERTY_GROWING_SYSTEM + " = :growingSystem ");

        List<EffectiveCropCyclePhase> effectiveCropCyclePhases = findAll(query.toString(), DaoUtils.asArgsMap("growingSystem", growingSystem));

        if (CollectionUtils.isNotEmpty(effectiveCropCyclePhases)) {
            Map<String, Object> args = DaoUtils.asArgsMap();
            query = new StringBuilder("SELECT ha FROM " + getEntityClass().getName() + " ha ");
            query.append(" INNER JOIN ha." + HarvestingAction.PROPERTY_EFFECTIVE_INTERVENTION + " ei ");
            query.append(" WHERE 1 = 1");
            query.append(DaoUtils.andAttributeIn("ei", EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, args, effectiveCropCyclePhases));

            result.addAll(findAll(query.toString(), args));
        }

        return result;
    }


    public static String formatInValues(String requestElement) {
        return "'" + requestElement + "'";
    }

    public Map<HarvestingActionValorisation, String> findValorisationToActionId(Collection<HarvestingActionValorisation> valorisations) {
        Map<String, HarvestingActionValorisation> valorisiationByIds = valorisations.stream().collect(Collectors.toMap((TopiaEntity::getTopiaId), Function.identity()));
        Set<String> valorisationIds = valorisations.stream().map(TopiaEntity::getTopiaId).collect(Collectors.toSet());
        String valorisationIdsQueryParts = formatInValues(StringUtils.join(valorisationIds, "','"));

        String query = """
            SELECT hav.topiaid, hav.harvestingaction FROM harvestingactionvalorisation hav
            WHERE hav.topiaid IN (%s)
            """;

        query = String.format(query, valorisationIdsQueryParts);

        Map<HarvestingActionValorisation, String> res = new HashMap<>();

        SqlFunction<ResultSet, Pair<String, String>> transformer = resultSet -> {
            final String valorisationId = resultSet.getString(1);
            final String actionId = resultSet.getString(2);
            res.put(valorisiationByIds.get(valorisationId), actionId);
            return Pair.of(valorisationId, actionId);
        };

        topiaSqlSupport.findMultipleResult(query, transformer);

        return res;
    }
} //HarvestingActionTopiaDao
