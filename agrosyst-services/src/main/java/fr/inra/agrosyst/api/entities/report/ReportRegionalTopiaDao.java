package fr.inra.agrosyst.api.entities.report;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.SortedColumn;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.NetworkTopiaDao;
import fr.inra.agrosyst.api.services.network.NetworkFilter;
import fr.inra.agrosyst.api.services.report.ReportFilter;
import fr.inra.agrosyst.api.services.report.ReportRegionalFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.security.SecurityContext;
import fr.inra.agrosyst.services.security.SecurityHelper;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ReportRegionalTopiaDao extends AbstractReportRegionalTopiaDao<ReportRegional> {

    public static void GET_REPORT_NETWORK_LIST_SUB_QUERY(StringBuilder query, Map<String, Object> args, Set<Network> networks) {
        int index = 0;
        StringBuilder networkQueryPart = new StringBuilder();
        String surround = " AND (%s)";
        for (Network network : networks) {
            String key = "network" + index;
            networkQueryPart.append(String.format(" %s :%s IN ELEMENTS ( R.%s ) ", index == 0 ? "" : "OR",  key, ReportRegional.PROPERTY_NETWORKS));
            args.put(key, network);
            index++;
        }
        query.append(String.format(surround, networkQueryPart));
    }

    public PaginationResult<ReportRegional> findFilteredReportRegional(ReportRegionalFilter filter, SecurityContext securityContext) {

        StringBuilder query = new StringBuilder("FROM " + getEntityClass().getName() + " R");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = Maps.newLinkedHashMap();
    
        // apply non null filter
        applyFiltersOnQuery(filter, securityContext, query, args);
    
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
        int startIndex = page * count;
        int endIndex = page * count + count - 1;
    
        String queryString = query.toString();
    
        String filterOrderBy = getFilterOrderBy(filter);
    
        String queryAndOrder = queryString + filterOrderBy;
        List<ReportRegional> reportRegionals = find(queryAndOrder, args, startIndex, endIndex);
        long totalCount = findUnique("SELECT count(*) " + queryString, args);
    
        // build result bean
        PaginationParameter pager = PaginationParameter.of(page, count);
        PaginationResult<ReportRegional> result = PaginationResult.of(reportRegionals, totalCount, pager);
        return result;
    }
    
    /**
     * @return - user selected ORDER BY or DEFAULT one
     */
    private String getFilterOrderBy(ReportRegionalFilter filter) {
        String result = null;
        
        if (filter != null && filter.getSortedColumn() != null) {
            SortedColumn sortedColumn = filter.getSortedColumn();
            String filterOrderBy = switch (sortedColumn) {
                case REPORT -> " lower (R." + ReportRegional.PROPERTY_NAME + ")";
                case CAMPAIGN -> " R." + ReportRegional.PROPERTY_CAMPAIGN;
                default -> null;
            };
            if (StringUtils.isNotBlank(filterOrderBy)) {
                filterOrderBy += " " + filter.getSortOrder().name();
                result = " ORDER BY " +
                        filterOrderBy +
                        ", R." + TopiaEntity.PROPERTY_TOPIA_ID;
            }
            
        }
        
        if (result == null) {
            // default
            result = " ORDER BY " +
                    "R." + ReportRegional.PROPERTY_CAMPAIGN + " DESC" +
                    ", lower (R." + ReportRegional.PROPERTY_NAME + ")" +
                    ", R." + ReportRegional.PROPERTY_TOPIA_ID;
        }
        
        return result;
    }
    
    public Set<String> findFilteredReportRegionalIds(ReportRegionalFilter filter, SecurityContext securityContext) {
        
        StringBuilder query = new StringBuilder("SELECT R.topiaId FROM " + getEntityClass().getName() + " R");
        query.append(" WHERE 1 = 1");
        Map<String, Object> args = Maps.newLinkedHashMap();
        applyFiltersOnQuery(filter, securityContext, query, args);
        
        String queryString = query.toString();
        String orderByString = getFilterOrderBy(filter);
        String queryAndOrder = queryString + orderByString;
       return new HashSet<>(findAll(queryAndOrder, args));
    }

    private void applyFiltersOnQuery(ReportRegionalFilter filter, SecurityContext securityContext, StringBuilder query, Map<String, Object> args) {
        // apply non null filter
        if (filter != null) {

            // name
            query.append(DaoUtils.andAttributeLike("R", ReportRegional.PROPERTY_NAME, args, filter.getName()));

            // campaign
            query.append(DaoUtils.andAttributeEquals("R", ReportRegional.PROPERTY_CAMPAIGN, args, filter.getCampaign()));

            // sector
            query.append(DaoUtils.andAttributeInElements("R", ReportRegional.PROPERTY_SECTORS, args, filter.getSector()));

            // network

            // Navigation context
            Set<Network> networks = Sets.newHashSet();
            NavigationContext navigationContext = filter.getNavigationContext();
            if (navigationContext != null) {
                // networks
                if (navigationContext.getNetworksCount() > 0) {
                    NetworkTopiaDao networkDAO = topiaDaoSupplier.getDao(Network.class, NetworkTopiaDao.class);
                    networks.addAll(networkDAO.loadNetworksWithDescendantHierarchy(navigationContext.getNetworks()));
                }
            }

            if (StringUtils.isNotEmpty(filter.getNetwork())) {
                NetworkTopiaDao networkDAO = topiaDaoSupplier.getDao(Network.class, NetworkTopiaDao.class);
                NetworkFilter networkFilter = new NetworkFilter();
                networkFilter.setNetworkName(filter.getNetwork());
                networkFilter.setAllPageSize();
                networks.addAll(networkDAO.getFilteredNetworks(networkFilter, null).getElements());
            }

            if (!networks.isEmpty()) {
                GET_REPORT_NETWORK_LIST_SUB_QUERY(query, args, networks);
            }

        }

        SecurityHelper.addReportRegionalFilter(query, args, securityContext, "R");
    }

    public Map<String, String> getFilteredReportRegionalForReportGrowingSystem(ReportFilter filter, SecurityContext securityContext) {

        Map<String, String> result = new LinkedHashMap<>();

        if (filter == null || (StringUtils.isEmpty(filter.getGrowingSystemId()) && StringUtils.isEmpty(filter.getDomainId()))) {
            return result;
        }

        Map<String, Object> args = Maps.newLinkedHashMap();


        StringBuilder query = new StringBuilder("SELECT r." + ReportRegional.PROPERTY_NAME + ", r." + ReportRegional.PROPERTY_TOPIA_ID);
        query.append(" FROM " + getEntityClass().getName() + " r," + GrowingSystem.class.getName() + " gs, "+ Network.class.getName() + " nw ");
        query.append(" INNER JOIN r." + ReportRegional.PROPERTY_SECTORS + " r_sector ");

        // networks
        if (StringUtils.isNotEmpty(filter.getGrowingSystemId())) {
            query.append(" WHERE gs." + GrowingSystem.PROPERTY_TOPIA_ID + " = :gsId ");
            args.put("gsId", filter.getGrowingSystemId());

        } else if (StringUtils.isNoneEmpty(filter.getDomainId())) {
            query.append(" INNER JOIN gs." + GrowingSystem.PROPERTY_GROWING_PLAN + "." + GrowingPlan.PROPERTY_DOMAIN + " domain ");

            query.append(" WHERE domain." + Domain.PROPERTY_TOPIA_ID + " = :domainId ");
            args.put("domainId", filter.getDomainId());

        }

        NavigationContext navigationContext = filter.getNavigationContext();
        Set<String> networkIds = navigationContext == null ? null : navigationContext.getNetworks();
        if (networkIds != null && !networkIds.isEmpty()) {
            query.append(" AND nw." + Network.PROPERTY_TOPIA_ID + "  IN (:networkIds)");
            args.put("networkIds", networkIds);
        }

        // campaign
        query.append(DaoUtils.andAttributeEquals("r", ReportRegional.PROPERTY_CAMPAIGN, args, filter.getCampaign()));

        // sector
        query.append(" AND r_sector IN :sectors ");
        args.put("sectors", filter.getSectors());

        query.append(" AND nw member of r." + ReportRegional.PROPERTY_NETWORKS);
        query.append(" AND nw member of gs." + GrowingSystem.PROPERTY_NETWORKS);

        // security
        SecurityHelper.addGrowingSystemFilter(query, args, securityContext, "gs");

        String queryString = query.toString();

        String queryAndOrder = queryString + " ORDER BY " +
                "r." + ReportRegional.PROPERTY_CAMPAIGN + " DESC" +
                ", lower (r." + ReportRegional.PROPERTY_NAME + ")";
        List<Object[]> res0 = findAll(queryAndOrder, args);
        for (Object[] res : res0) {
            String rRName = (String) res[0];
            String rRId = (String) res[1];
            result.put(rRId, rRName);
        }

        return result;
    }

    public LinkedHashMap<Integer, String> findAllRelatedReports(String code) {
        String query = "SELECT " + ReportRegional.PROPERTY_CAMPAIGN + ", " + ReportRegional.PROPERTY_TOPIA_ID
                + " FROM " + getEntityClass().getName()
                + " WHERE " + ReportRegional.PROPERTY_CODE + " = :code"
                + " ORDER BY " + ReportRegional.PROPERTY_CAMPAIGN + " DESC";
        List<Object[]> reports = findAll(query, DaoUtils.asArgsMap("code", code));
        LinkedHashMap<Integer, String> result = DaoUtils.toRelatedMap(reports);
        return result;
    }

} //ReportRegionalTopiaDao
