package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2016 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.utils.I18nDaoHelper;

import java.util.LinkedList;
import java.util.List;

public class RefAdventiceTopiaDao extends AbstractRefAdventiceTopiaDao<RefAdventice> {

    public List<RefAdventice> findActiveRefAdventiceForBioAgressorType(Language language) {

        I18nDaoHelper adventiceI18nDaoHelper =
                I18nDaoHelper.withSimpleI18nKey(
                        RefAdventice.PROPERTY_ADVENTICE,
                        language,
                        TradRefVivant.class,
                        "ra"
                );

        I18nDaoHelper famille_de_cultureI18nDaoHelper =
                I18nDaoHelper.withSimpleI18nKey(
                        RefAdventice.PROPERTY_FAMILLE_DE_CULTURE,
                        language,
                        TradRefVivant.class,
                        "ra"
                );

        String hql = String.join("\n"
                , "SELECT ra"
                    + ", " + adventiceI18nDaoHelper.coalesceTranslation() + " as adventice"
                    + ", " + famille_de_cultureI18nDaoHelper.coalesceTranslation()
                , "FROM " + getEntityClass().getName() + " ra"
                    + adventiceI18nDaoHelper.leftJoinTranslation()
                    + famille_de_cultureI18nDaoHelper.leftJoinTranslation()
                , "WHERE ra." + RefAdventice.PROPERTY_ACTIVE + " = true"
                , "ORDER BY ra." + RefAdventice.PROPERTY_MAIN + " DESC, adventice ASC"
        );

        List<RefAdventice> result = new LinkedList<>();
        this.<Object[]>findAll(hql).forEach(row -> {
            RefAdventice refAdventice = (RefAdventice) row[0];
            String adventice = (String) row[1];
            String famille_de_culture = (String) row[1];
            refAdventice.setAdventice(adventice);
            refAdventice.setFamille_de_culture(famille_de_culture);
            result.add(refAdventice);
        });
        return result;
    }
}
