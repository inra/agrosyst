package fr.inra.agrosyst.api.entities.effective;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.utils.DaoUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EffectivePerennialCropCycleTopiaDao extends AbstractEffectivePerennialCropCycleTopiaDao<EffectivePerennialCropCycle> {

    /**
     * Recherche le cycle qui contient l'intervention.
     * 
     * @param effectiveIntervention intervention
     * @return
     */
    public EffectivePerennialCropCycle findPerennialCropCycleForIntervention(EffectiveIntervention effectiveIntervention) {
        String query = "FROM " + EffectivePerennialCropCycle.class.getName() + " C" +
                " WHERE :effectiveCropCyclePhase = C." + EffectivePerennialCropCycle.PROPERTY_PHASE;

        return findUnique(query, DaoUtils.asArgsMap("effectiveCropCyclePhase", effectiveIntervention.getEffectiveCropCyclePhase()));
    }

    public List<String> findPerennialCropCycleZoneForCroppingPlanEntryName(String croppingPlanEntryName) {
        Map<String, Object> args = new HashMap<>();
        String query = "SELECT zone." + Zone.PROPERTY_TOPIA_ID +
                " FROM " + EffectivePerennialCropCycle.class.getName() + " epcc" +
                " LEFT JOIN epcc." + EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY + " cpe" +
                " LEFT JOIN epcc." + EffectivePerennialCropCycle.PROPERTY_ZONE + " zone" +
                " WHERE " + DaoUtils.getQueryForAttributeLike("cpe", CroppingPlanEntry.PROPERTY_NAME, args, "%" + croppingPlanEntryName + "%", "");
        return findAll(query, args);
    }

} //EffectivePerennialCropCycleTopiaDao
