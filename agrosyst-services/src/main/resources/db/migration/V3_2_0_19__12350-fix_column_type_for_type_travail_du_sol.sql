---
-- %%Ignore-License
---

ALTER TABLE realise_echelle_zone ALTER COLUMN type_de_travail_du_sol TYPE text;
ALTER TABLE realise_echelle_sdc ALTER COLUMN type_de_travail_du_sol TYPE text;
ALTER TABLE realise_echelle_parcelle ALTER COLUMN type_de_travail_du_sol TYPE text;
ALTER TABLE realise_echelle_itk ALTER COLUMN type_de_travail_du_sol TYPE text;
ALTER TABLE realise_echelle_culture ALTER COLUMN type_de_travail_du_sol TYPE text;
ALTER TABLE realise_echelle_intervention ALTER COLUMN type_de_travail_du_sol TYPE text;
ALTER TABLE synthetise_echelle_culture ALTER COLUMN type_de_travail_du_sol TYPE text;
ALTER TABLE synthetise_echelle_intervention ALTER COLUMN type_de_travail_du_sol TYPE text;
ALTER TABLE synthetise_echelle_synthetise ALTER COLUMN type_de_travail_du_sol TYPE text;