---
-- %%Ignore-License
---

update public."phytoproducttarget" set category = (select r.reference_param from public.refnuisibleedi r where r.topiaid = target) where category is null and target like 'fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI_%';

update public."phytoproducttarget" set category = 'ADVENTICE' where category is null and target like 'fr.inra.agrosyst.api.entities.referential.RefAdventice_%';
