---
-- %%Ignore-License
---

-- Supprime les doublons d'intrants introduits par un bug dans la copie des systèmes synthétisés.

CREATE TABLE _12186_inputs_to_remove AS (
    select a.*
    from abstractdomaininputstockunit a
        join abstractdomaininputstockunit a2 on a.code = a2.code and a.domain = a2.domain and a.topiaid != a2.topiaid
        left join abstractphytoproductinputusage appiu on a.topiaid = appiu.domainphytoproductinput
    where
        appiu.domainphytoproductinput is null and
        (
            a.inputtype = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES' or
            a.inputtype = 'TRAITEMENT_SEMENCE' or
            a.inputtype = 'LUTTE_BIOLOGIQUE'
        )
);

delete from inputprice where topiaid in (select r.inputprice from _12186_inputs_to_remove r);

delete from domainphytoproductinput where topiaid in (select topiaid from _12186_inputs_to_remove);
delete from abstractdomaininputstockunit where topiaid in (select topiaid from _12186_inputs_to_remove);
