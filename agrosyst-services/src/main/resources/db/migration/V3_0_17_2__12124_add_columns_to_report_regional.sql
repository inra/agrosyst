---
-- %%Ignore-License
---

ALTER TABLE reportregional ADD COLUMN tropicalrainfalljanuarymarch float8 NULL;
ALTER TABLE reportregional ADD COLUMN tropicalrainfallapriljune float8 NULL;
ALTER TABLE reportregional ADD COLUMN tropicalrainfalljulyseptember float8 NULL;
ALTER TABLE reportregional ADD COLUMN tropicalrainfalloctoberdecember float8 NULL;

ALTER TABLE reportregional ADD COLUMN tropicalrainydaysjanuarymarch int4 NULL;
ALTER TABLE reportregional ADD COLUMN tropicalrainydaysapriljune int4 NULL;
ALTER TABLE reportregional ADD COLUMN tropicalrainydaysjulyseptember int4 NULL;
ALTER TABLE reportregional ADD COLUMN tropicalrainydaysoctoberdecember int4 NULL;
