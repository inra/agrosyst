---
-- %%Ignore-License
---

-- rollback
-- UPDATE abstractinputusage aiu
--   SET comment = pre_res.comment
--   FROM
--   (
--     SELECT usage_id, comment
--     FROM _1200_add_2_72_groupe_cible_info_to_seedlot
--   ) AS pre_res
--   WHERE pre_res.usage_id = aiu.topiaid;

CREATE TABLE _1200_add_2_72_groupe_cible_info_to_seedlot AS (
  SELECT DISTINCT
  aiu.topiaid AS usage_id,
  aiu.comment,
  ai.producttype,
  aiu.productname AS usage_name,
  (
  SELECT case when rgc.groupe_cible_maa IS NOT NULL 
    THEN (
      concat(' - ', (SELECT CASE WHEN producttype IS NOT NULL THEN producttype ELSE '' end), ' - ', ppt.category, ' - ', rgc.groupe_cible_maa, ' - ', concat(ra.adventice, rn.reference_label))
    )
    ELSE
      concat(' - ', (SELECT CASE WHEN producttype IS NOT NULL THEN producttype ELSE '' end), ' - ', ppt.category, ' - ', ra.adventice, rn.reference_label)
    END
  ) group_cible_part
  FROM abstractinput ai
  INNER JOIN abstractaction aa ON aa.topiaid = ai.seedingaction
  INNER JOIN seedlotinputusage slu ON slu.seedingactionusage = aa.topiaid
  INNER JOIN abstractinputusage aiu ON aiu.topiaid = slu.topiaid
  INNER JOIN phytoproducttarget ppt ON ppt.phytoproductinput = ai.topiaid
  LEFT JOIN refnuisibleedi rn on rn.topiaid = ppt.target
  LEFT JOIN refadventice ra on ra.topiaid = ppt.target
  LEFT JOIN refciblesagrosystgroupesciblesmaa rgc on rgc.code_groupe_cible_maa = ppt.codegroupeciblemaa AND rgc.reference_param = ppt.category
  AND ai.phytoproduct IS NULL
  AND ai.inputtype = 'SEMIS'
);

CREATE TABLE _1200_add_2_72_groupe_cible_info_to_seedlot_unique AS (
SELECT DISTINCT
  usage_id,
  comment,
  usage_name,
  (SELECT string_agg(group_cible_part, CHR(10)) from _1200_add_2_72_groupe_cible_info_to_seedlot tmp1 where tmp0.usage_id =  tmp1.usage_id) as group_cible_part
  FROM _1200_add_2_72_groupe_cible_info_to_seedlot tmp0
);

UPDATE abstractinputusage aiu
SET comment = pre_res.new_comment
FROM
(
  SELECT usage_id, (
    concat(
      (SELECT CASE WHEN comment IS NOT NULL THEN concat(comment, CHR(10)) ELSE '' END),
      'Cibles du traitement non migrées', CHR(160), ': ', CHR(10), group_cible_part)) AS new_comment
  FROM _1200_add_2_72_groupe_cible_info_to_seedlot_unique
) AS pre_res
WHERE pre_res.usage_id = aiu.topiaid;
