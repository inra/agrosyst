---
-- %%Ignore-License
---

CREATE TABLE _12183_other_input_usage_linked_to_several_actions AS (
    select *
    from otherproductinputusage o
    where
        (biologicalcontrolaction is not null and harvestingaction is not null) or
        (biologicalcontrolaction is not null and irrigationaction  is not null) or
        (biologicalcontrolaction is not null and maintenancepruningvinesaction  is not null) or
        (biologicalcontrolaction is not null and mineralfertilizersspreadingaction  is not null) or
        (biologicalcontrolaction is not null and organicfertilizersspreadingaction  is not null) or
        (biologicalcontrolaction is not null and otheraction  is not null) or
        (biologicalcontrolaction is not null and pesticidesspreadingaction  is not null) or
        (biologicalcontrolaction is not null and seedingaction  is not null) or
        (harvestingaction is not null and irrigationaction  is not null) or
        (harvestingaction is not null and maintenancepruningvinesaction  is not null) or
        (harvestingaction is not null and mineralfertilizersspreadingaction  is not null) or
        (harvestingaction is not null and organicfertilizersspreadingaction  is not null) or
        (harvestingaction is not null and otheraction  is not null) or
        (harvestingaction is not null and pesticidesspreadingaction  is not null) or
        (harvestingaction is not null and seedingaction  is not null) or
        (irrigationaction is not null and maintenancepruningvinesaction is not null) or
        (irrigationaction is not null and mineralfertilizersspreadingaction is not null) or
        (irrigationaction is not null and organicfertilizersspreadingaction is not null) or
        (irrigationaction is not null and otheraction is not null) or
        (irrigationaction is not null and pesticidesspreadingaction is not null) or
        (irrigationaction is not null and seedingaction is not null) or
        (maintenancepruningvinesaction is not null and mineralfertilizersspreadingaction is not null) or
        (maintenancepruningvinesaction is not null and organicfertilizersspreadingaction is not null) or
        (maintenancepruningvinesaction is not null and otheraction is not null) or
        (maintenancepruningvinesaction is not null and pesticidesspreadingaction is not null) or
        (maintenancepruningvinesaction is not null and seedingaction is not null) or
        (mineralfertilizersspreadingaction is not null and organicfertilizersspreadingaction is not null) or
        (mineralfertilizersspreadingaction is not null and otheraction is not null) or
        (mineralfertilizersspreadingaction is not null and pesticidesspreadingaction is not null) or
        (mineralfertilizersspreadingaction is not null and seedingaction is not null) or
        (organicfertilizersspreadingaction is not null and otheraction is not null) or
        (organicfertilizersspreadingaction is not null and pesticidesspreadingaction is not null) or
        (organicfertilizersspreadingaction is not null and seedingaction is not null) or
        (otheraction is not null and pesticidesspreadingaction is not null) or
        (otheraction is not null and seedingaction is not null) or
        (pesticidesspreadingaction is not null and seedingaction is not null)
);

update otherproductinputusage set harvestingaction = null, irrigationaction = null, maintenancepruningvinesaction = null, mineralfertilizersspreadingaction = null,
organicfertilizersspreadingaction = null, otheraction = null, pesticidesspreadingaction = null, seedingaction = null
where topiaid in (select topiaid from _12183_other_input_usage_linked_to_several_actions where biologicalcontrolaction is not null);

update otherproductinputusage set irrigationaction = null, maintenancepruningvinesaction = null, mineralfertilizersspreadingaction = null,
organicfertilizersspreadingaction = null, otheraction = null, pesticidesspreadingaction = null, seedingaction = null
where topiaid in (select topiaid from _12183_other_input_usage_linked_to_several_actions where harvestingaction is not null);

update otherproductinputusage set maintenancepruningvinesaction = null, mineralfertilizersspreadingaction = null,
organicfertilizersspreadingaction = null, otheraction = null, pesticidesspreadingaction = null, seedingaction = null
where topiaid in (select topiaid from _12183_other_input_usage_linked_to_several_actions where irrigationaction is not null);

update otherproductinputusage set mineralfertilizersspreadingaction = null, organicfertilizersspreadingaction = null, otheraction = null, pesticidesspreadingaction = null, seedingaction = null
where topiaid in (select topiaid from _12183_other_input_usage_linked_to_several_actions where maintenancepruningvinesaction is not null);

update otherproductinputusage set organicfertilizersspreadingaction = null, otheraction = null, pesticidesspreadingaction = null, seedingaction = null
where topiaid in (select topiaid from _12183_other_input_usage_linked_to_several_actions where mineralfertilizersspreadingaction is not null);

update otherproductinputusage set otheraction = null, pesticidesspreadingaction = null, seedingaction = null
where topiaid in (select topiaid from _12183_other_input_usage_linked_to_several_actions where organicfertilizersspreadingaction is not null);

update otherproductinputusage set pesticidesspreadingaction = null, seedingaction = null
where topiaid in (select topiaid from _12183_other_input_usage_linked_to_several_actions where otheraction is not null);

update otherproductinputusage set seedingaction = null
where topiaid in (select topiaid from _12183_other_input_usage_linked_to_several_actions where pesticidesspreadingaction is not null);