---
-- %%Ignore-License
---

ALTER TABLE reffeedbackrouter DROP CONSTRAINT uks1pwd2ki79krts5vb9v2jkg5h;
ALTER TABLE reffeedbackrouter ADD CONSTRAINT uk_reffeedbackrouter UNIQUE (feedbackcategory, sector, typedephy);
