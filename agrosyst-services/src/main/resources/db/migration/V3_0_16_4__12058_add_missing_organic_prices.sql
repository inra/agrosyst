---
-- %%Ignore-License
---

CREATE TABLE _12058_organic_script_date AS (SELECT CURRENT_TIMESTAMP(3)::timestamp without time zone AS topiacreatedate);
CREATE TEMPORARY TABLE li_create_date AS (
  SELECT topiacreatedate
  FROM domainorganicproductinput dopi
  INNER JOIN abstractdomaininputstockunit adis on adis.topiaid = dopi.topiaid
  ORDER BY topiacreatedate limit 1);

CREATE TABLE _12058_effective_organic_missing_prices as
  SELECT concat('fr.inra.agrosyst.api.entities.InputPrice_', split_part(adis."topiaid", '_', 2)) AS topiaid,
         concat('fr.inra.agrosyst.api.entities.InputPrice_', uuid_in(md5(random()::text || random()::text)::cstring)) AS unique_topiaid,
         adis.topiaid AS input_id,
         p.topiaid AS price_id,
         p.topiaversion AS topiaversion,
         'fr.inra.agrosyst.api.entities.InputPriceImpl' AS topiadiscriminator,
         p.objectid AS objectid,
         p.sourceunit AS sourceunit,
         'ORGANIQUES_INPUT' AS category,
         adis."domain" AS domain,
         (SELECT topiacreatedate FROM  _12058_organic_script_date) AS topiacreatedate,
         p.displayname AS displayname,
         p.price AS price,
         p.priceunit AS priceunit
  FROM domainorganicproductinput opi
  INNER JOIN abstractdomaininputstockunit adis on opi.topiaid = adis.topiaid
  INNER JOIN reffertiorga rfo on rfo.topiaid = opi.refinput
  INNER JOIN price p on p.objectid = rfo.idtypeeffluent AND p."domain" = adis."domain"
  WHERE adis.inputtype = 'EPANDAGES_ORGANIQUES'
  AND adis.inputprice is null
  AND p.price is not null
  AND p.category = 'ORGANIQUES_INPUT_CATEGORIE'
  AND p.price != 0
  AND p.priceunit is not null
  AND adis.topiacreatedate = (SELECT topiacreatedate FROM li_create_date);

CREATE INDEX _12058_effective_organic_missing_prices_Idx ON _12058_effective_organic_missing_prices (input_id);

DELETE FROM _12058_effective_organic_missing_prices
  WHERE unique_topiaid IN
      (SELECT unique_topiaid
       FROM
          (SELECT unique_topiaid,
           ROW_NUMBER() OVER( PARTITION BY topiaid
          ORDER BY coalesce (sourceunit, 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ'), price) AS row_num
          FROM _12058_effective_organic_missing_prices ) t
          WHERE t.row_num > 1);

INSERT INTO inputprice(topiaid, topiaversion, topiadiscriminator, objectid, sourceunit, category, domain, topiacreatedate, displayname, price, priceunit)
  SELECT topiaid,
         topiaversion,
         topiadiscriminator,
         objectid,
         sourceunit,
         category,
         domain,
         topiacreatedate,
         displayname,
         price,
         priceunit
  FROM _12058_effective_organic_missing_prices;

UPDATE abstractdomaininputstockunit adis set inputprice = orphan_input_price.price_id
FROM (
  SELECT eomp.input_id AS topiaid, eomp.topiaid AS price_id
  FROM _12058_effective_organic_missing_prices eomp
) AS orphan_input_price
WHERE adis.topiaid  = orphan_input_price.topiaid;

CREATE TABLE _12058_practiced_organic_missing_prices as
  SELECT concat('fr.inra.agrosyst.api.entities.InputPrice_', split_part(adis."topiaid", '_', 2)) AS topiaid,
         concat('fr.inra.agrosyst.api.entities.InputPrice_', uuid_in(md5(random()::text || random()::text)::cstring)) AS unique_topiaid,
         adis.topiaid AS input_id,
         p.topiaid AS price_id,
         p.topiaversion AS topiaversion,
         'fr.inra.agrosyst.api.entities.InputPriceImpl' AS topiadiscriminator,
         p.objectid AS objectid,
         p.sourceunit AS sourceunit,
         'ORGANIQUES_INPUT' AS category,
         adis."domain" AS domain,
         (SELECT topiacreatedate FROM  _12058_organic_script_date) AS topiacreatedate,
         p.displayname AS displayname,
         p.price AS price,
         p.priceunit AS priceunit
  FROM domainorganicproductinput opi
  INNER JOIN abstractdomaininputstockunit adis on opi.topiaid = adis.topiaid
  INNER JOIN reffertiorga rfo on rfo.topiaid = opi.refinput
  INNER JOIN growingplan gp on gp.domain = adis.domain
  INNER JOIN growingsystem gs on gs.growingplan = gp.topiaid
  INNER JOIN practicedsystem ps on ps.growingsystem = gs.topiaid
  INNER JOIN price p on p.objectid = rfo.idtypeeffluent AND p.practicedsystem = ps.topiaid AND p.domain is null
  WHERE adis.inputtype = 'EPANDAGES_ORGANIQUES'
  AND adis.inputprice is null
  AND p.price is not null
  AND p.category = 'ORGANIQUES_INPUT_CATEGORIE'
  AND p.price != 0
  AND p.priceunit is not null
  AND adis.topiacreatedate = (SELECT topiacreatedate FROM li_create_date);

CREATE INDEX _12058_practiced_organic_missing_prices_Idx ON _12058_practiced_organic_missing_prices (input_id);

DELETE FROM _12058_practiced_organic_missing_prices
  WHERE unique_topiaid IN
      (SELECT unique_topiaid
       FROM
          (SELECT unique_topiaid,
           ROW_NUMBER() OVER( PARTITION BY topiaid
          ORDER BY coalesce (sourceunit, 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ'), price) AS row_num
          FROM _12058_practiced_organic_missing_prices ) t
          WHERE t.row_num > 1);

INSERT INTO inputprice(topiaid, topiaversion, topiadiscriminator, objectid, sourceunit, category, domain, topiacreatedate, displayname, price, priceunit)
  SELECT topiaid,
         topiaversion,
         topiadiscriminator,
         objectid,
         sourceunit,
         category,
         domain,
         topiacreatedate,
         displayname,
         price,
         priceunit
  FROM _12058_practiced_organic_missing_prices;

UPDATE abstractdomaininputstockunit adis set inputprice = orphan_input_price.price_id
FROM (
  SELECT eomp.input_id AS topiaid, eomp.topiaid AS price_id
  FROM _12058_practiced_organic_missing_prices eomp
) AS orphan_input_price
WHERE adis.topiaid  = orphan_input_price.topiaid;
