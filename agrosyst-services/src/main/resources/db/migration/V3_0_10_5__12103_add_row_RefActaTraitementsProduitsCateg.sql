---
-- %%Ignore-License
---

insert into refactatraitementsproduitscateg(topiaid, topiaversion , topiacreatedate , id_traitement, code_traitement, nom_traitement, "action", type_produit, ift_chimique_total, ift_chimique_tot_hts, ift_h, ift_f, ift_i, ift_ts, ift_a, ift_hh, ift_moy_bio, "source", active)
values ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg_de85cfeb-6358-7673-3d52-08200436593f', 0, now(),
	165,'M6','Moyens biologiques - Abiotique - Matières Fertilisantes et Supports de Culture','LUTTE_BIOLOGIQUE','FERTILIZING_MATERIALS_AND_GROWING_MEDIA',false,false,false,false,false,false,false,false,true,'EPHY 2023',true);