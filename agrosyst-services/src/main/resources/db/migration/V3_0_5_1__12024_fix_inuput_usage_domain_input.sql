---
-- %%Ignore-License
---

CREATE TEMPORARY TABLE V3_0_5_1__12024_fix_inuput_usage_domain_input AS (SELECT CURRENT_TIMESTAMP(3)::timestamp without time zone AS topiacreatedate);

CREATE TEMPORARY TABLE seedspeciesinputusage_to_domain AS (
        SELECT
        ssiu.topiaid as species_usage_id,
        sliu.topiaid as bad_lot_usage_Id,
        dssi.speciesseed bad_input_species_id,
        dssi.topiaid as bad_domainspecies_input,
        (select cps.topiaid from croppingplanspecies cps inner join croppingplanentry cpe on cps.croppingplanentry = cpe.topiaid where cps.code = scps.code and cpe."domain" = gp.domain) as valid_species_id,
        (select vdssi.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = gp.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code and vadis.code = adis.code) as valid_same_code_domainspecies_input,
        (select vdssi.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = gp.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code limit 1) as valid_other_code_domainspecies_input,
        dsli.topiaid as bad_dsli,
        (select vdsli.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = gp.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code and vadis.code = adis.code) as valid_same_code_dsli,
        (select vdsli.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = gp.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code limit 1) as valid_other_code_dsli,
        adis.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM seedspeciesinputusage ssiu
        INNER JOIN abstractinputusage aiu ON aiu.topiaid = ssiu.topiaid
        inner join seedlotinputusage sliu on sliu.topiaid = ssiu.seedlotinputusage
        inner join domainseedspeciesinput dssi on dssi.topiaid = ssiu.domainseedspeciesinput
        inner join domainseedlotinput dsli on dsli.topiaid = dssi.domainseedlotinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dsli.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = sliu.seedingactionusage
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        left join croppingplanspecies scps on scps.topiaid = dssi.speciesseed
        where gp.domain != adis.domain

UNION ALL
    (   SELECT
        ssiu.topiaid as species_usage_id,
        sliu.topiaid as bad_lot_usage_Id,
        dssi.speciesseed bad_input_species_id,
        dssi.topiaid as bad_domainspecies_input,
        (select cps.topiaid from croppingplanspecies cps inner join croppingplanentry cpe on cps.croppingplanentry = cpe.topiaid where cps.code = scps.code and cpe."domain" = gp.domain) as valid_species_id,
        (select vdssi.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = gp.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code and vadis.code = adis.code) as valid_same_code_domainspecies_input,
        (select vdssi.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = gp.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code limit 1) as valid_other_code_domainspecies_input,
        dsli.topiaid as bad_dsli,
        (select vdsli.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = gp.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code and vadis.code = adis.code) as valid_same_code_dsli,
        (select vdsli.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = gp.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code limit 1) as valid_other_code_dsli,
        adis.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM seedspeciesinputusage ssiu
        INNER JOIN abstractinputusage aiu ON aiu.topiaid = ssiu.topiaid
        inner join seedlotinputusage sliu on sliu.topiaid = ssiu.seedlotinputusage
        inner join domainseedspeciesinput dssi on dssi.topiaid = ssiu.domainseedspeciesinput
        inner join domainseedlotinput dsli on dsli.topiaid = dssi.domainseedlotinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dsli.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = sliu.seedingactionusage
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        left join croppingplanspecies scps on scps.topiaid = dssi.speciesseed
        where gp.domain != adis.domain
    )

UNION ALL
    (
        SELECT
        ssiu.topiaid as species_usage_id,
        sliu.topiaid as bad_lot_usage_Id,
        dssi.speciesseed bad_input_species_id,
        dssi.topiaid as bad_domainspecies_input,
        (select cps.topiaid from croppingplanspecies cps inner join croppingplanentry cpe on cps.croppingplanentry = cpe.topiaid where cps.code = scps.code and cpe."domain" = plot.domain) as valid_species_id,
        (select vdssi.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = plot.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code and vadis.code = adis.code) as valid_same_code_domainspecies_input,
        (select vdssi.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = plot.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code limit 1) as valid_other_code_domainspecies_input,
        dsli.topiaid as bad_dsli,
        (select vdsli.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = plot.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code and vadis.code = adis.code) as valid_same_code_dsli,
        (select vdsli.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = plot.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code limit 1) as valid_other_code_dsli,
        adis.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM seedspeciesinputusage ssiu
        INNER JOIN abstractinputusage aiu ON aiu.topiaid = ssiu.topiaid
        inner join seedlotinputusage sliu on sliu.topiaid = ssiu.seedlotinputusage
        inner join domainseedspeciesinput dssi on dssi.topiaid = ssiu.domainseedspeciesinput
        inner join domainseedlotinput dsli on dsli.topiaid = dssi.domainseedlotinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dsli.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = sliu.seedingactionusage
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        left join croppingplanspecies scps on scps.topiaid = dssi.speciesseed
        where plot.domain != adis.domain
    )
UNION ALL

    (
        SELECT
        ssiu.topiaid as species_usage_id,
        sliu.topiaid as bad_lot_usage_Id,
        dssi.speciesseed bad_input_species_id,
        dssi.topiaid as bad_domainspecies_input,
        (select cps.topiaid from croppingplanspecies cps inner join croppingplanentry cpe on cps.croppingplanentry = cpe.topiaid where cps.code = scps.code and cpe."domain" = plot.domain) as valid_species_id,
        (select vdssi.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = plot.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code and vadis.code = adis.code) as valid_same_code_domainspecies_input,
        (select vdssi.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = plot.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code limit 1) as valid_other_code_domainspecies_input,
        dsli.topiaid as bad_dsli,
        (select vdsli.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = plot.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code and vadis.code = adis.code) as valid_same_code_dsli,
        (select vdsli.topiaid from domainseedspeciesinput vdssi inner join domainseedlotinput vdsli on vdsli.topiaid = vdssi.domainseedlotinput inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = plot.domain inner join croppingplanspecies vcps on vcps.topiaid = vdssi.speciesseed where vcps.code = scps.code limit 1) as valid_other_code_dsli,
        adis.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM seedspeciesinputusage ssiu
        INNER JOIN abstractinputusage aiu ON aiu.topiaid = ssiu.topiaid
        inner join seedlotinputusage sliu on sliu.topiaid = ssiu.seedlotinputusage
        inner join domainseedspeciesinput dssi on dssi.topiaid = ssiu.domainseedspeciesinput
        inner join domainseedlotinput dsli on dsli.topiaid = dssi.domainseedlotinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dsli.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = sliu.seedingactionusage
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        left join croppingplanspecies scps on scps.topiaid = dssi.speciesseed
        where plot.domain != adis.domain
    )
);

update seedspeciesinputusage ssiu set domainseedspeciesinput = (select CASE WHEN t.valid_same_code_domainspecies_input IS NOT NULL THEN t.valid_same_code_domainspecies_input ELSE t.valid_other_code_domainspecies_input END from seedspeciesinputusage_to_domain t where t.species_usage_id = ssiu.topiaid)
where exists (select 1 from seedspeciesinputusage_to_domain t where t.species_usage_id = ssiu.topiaid);

update seedlotinputusage sliu set domainseedlotinput = (select CASE WHEN t.valid_same_code_dsli IS NOT NULL THEN t.valid_same_code_dsli ELSE t.valid_other_code_dsli END from seedspeciesinputusage_to_domain t where t.bad_lot_usage_Id = sliu.topiaid limit 1)
where exists (select 1 from seedspeciesinputusage_to_domain t where t.bad_lot_usage_Id = sliu.topiaid);


CREATE TEMPORARY TABLE seedlotinputusage_to_domain AS (
        SELECT
        sliu.topiaid as bad_lot_usage_Id,
        dsli.topiaid as bad_dsli,
        (select vdsli.topiaid from domainseedlotinput vdsli inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = gp.domain inner join croppingplanentry vcpe on vcpe.topiaid = vdsli.cropseed where vcpe.code = cpe.code and vadis.code = adis.code) as valid_same_code_dsli,
        (select vdsli.topiaid from domainseedlotinput vdsli inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = gp.domain inner join croppingplanentry vcpe on vcpe.topiaid = vdsli.cropseed where vcpe.code = cpe.code limit 1) as valid_other_code_dsli,
        adis.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM seedlotinputusage sliu
        inner join domainseedlotinput dsli on dsli.topiaid = sliu.domainseedlotinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dsli.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = sliu.seedingactionusage
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        left join croppingplanentry cpe on cpe.topiaid = dsli.cropseed
        where gp.domain != adis.domain

UNION ALL
    (   SELECT
        sliu.topiaid as bad_lot_usage_Id,
        dsli.topiaid as bad_dsli,
        (select vdsli.topiaid from domainseedlotinput vdsli inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = gp.domain inner join croppingplanentry vcpe on vcpe.topiaid = vdsli.cropseed where vcpe.code = cpe.code and vadis.code = adis.code) as valid_same_code_dsli,
        (select vdsli.topiaid from domainseedlotinput vdsli inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = gp.domain inner join croppingplanentry vcpe on vcpe.topiaid = vdsli.cropseed where vcpe.code = cpe.code limit 1) as valid_other_code_dsli,
        adis.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM seedlotinputusage sliu
        inner join domainseedlotinput dsli on dsli.topiaid = sliu.domainseedlotinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dsli.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = sliu.seedingactionusage
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        left join croppingplanentry cpe on cpe.topiaid = dsli.cropseed
        where gp.domain != adis.domain
    )

UNION ALL
    (
        SELECT
        sliu.topiaid as bad_lot_usage_Id,
        dsli.topiaid as bad_dsli,
        (select vdsli.topiaid from domainseedlotinput vdsli inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = plot.domain inner join croppingplanentry vcpe on vcpe.topiaid = vdsli.cropseed where vcpe.code = cpe.code and vadis.code = adis.code) as valid_same_code_dsli,
        (select vdsli.topiaid from domainseedlotinput vdsli inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = plot.domain inner join croppingplanentry vcpe on vcpe.topiaid = vdsli.cropseed where vcpe.code = cpe.code limit 1) as valid_other_code_dsli,
        adis.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM seedlotinputusage sliu
        inner join domainseedlotinput dsli on dsli.topiaid = sliu.domainseedlotinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dsli.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = sliu.seedingactionusage
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        left join croppingplanentry cpe on cpe.topiaid = dsli.cropseed
        where plot.domain != adis.domain
    )
UNION ALL

    (
        SELECT
        sliu.topiaid as bad_lot_usage_Id,
        dsli.topiaid as bad_dsli,
        (select vdsli.topiaid from domainseedlotinput vdsli inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = plot.domain inner join croppingplanentry vcpe on vcpe.topiaid = vdsli.cropseed where vcpe.code = cpe.code and vadis.code = adis.code) as valid_same_code_dsli,
        (select vdsli.topiaid from domainseedlotinput vdsli inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdsli.topiaid and vadis."domain" = plot.domain inner join croppingplanentry vcpe on vcpe.topiaid = vdsli.cropseed where vcpe.code = cpe.code limit 1) as valid_other_code_dsli,
        adis.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM seedlotinputusage sliu
        inner join domainseedlotinput dsli on dsli.topiaid = sliu.domainseedlotinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dsli.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = sliu.seedingactionusage
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        left join croppingplanentry cpe on cpe.topiaid = dsli.cropseed
        where plot.domain != adis.domain
    )
);

update seedlotinputusage sliu set domainseedlotinput = (select CASE WHEN t.valid_same_code_dsli IS NOT NULL THEN t.valid_same_code_dsli ELSE t.valid_other_code_dsli END from seedlotinputusage_to_domain t where t.bad_lot_usage_Id = sliu.topiaid limit 1)
where exists (select 1 from seedlotinputusage_to_domain t where t.bad_lot_usage_Id = sliu.topiaid);


-- biologicalproductinputusage_to_domain
CREATE TEMPORARY TABLE biologicalproductinputusage_to_domain AS (
        SELECT
        appiu.topiaid as bad_usage_Id,
        dppi.topiaid as bad_di_Id,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = gp.domain where vdppi.refinput = dppi.refinput and vadis.code = adis.code) as valid_same_code_di,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = gp.domain where vdppi.refinput = dppi.refinput limit 1) as valid_other_code_di,
        adis.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM abstractphytoproductinputusage appiu
        inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dppi.topiaid
        inner join biologicalproductinputusage piu on piu.topiaid = appiu.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = piu.biologicalcontrolaction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adis.domain

UNION ALL
    (   SELECT
        appiu.topiaid as bad_usage_Id,
        dppi.topiaid as bad_di_Id,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = gp.domain where vdppi.refinput = dppi.refinput and vadis.code = adis.code) as valid_same_code_di,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = gp.domain where vdppi.refinput = dppi.refinput limit 1) as valid_other_code_di,
        adis.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM abstractphytoproductinputusage appiu
        inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dppi.topiaid
        inner join biologicalproductinputusage piu on piu.topiaid = appiu.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = piu.biologicalcontrolaction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adis.domain
    )

UNION ALL
    (
        SELECT
        appiu.topiaid as bad_usage_Id,
        dppi.topiaid as bad_di_Id,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = plot.domain where vdppi.refinput = dppi.refinput and vadis.code = adis.code) as valid_same_code_di,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = plot.domain where vdppi.refinput = dppi.refinput limit 1) as valid_other_code_di,
        adis.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM abstractphytoproductinputusage appiu
        inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dppi.topiaid
        inner join biologicalproductinputusage piu on piu.topiaid = appiu.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = piu.biologicalcontrolaction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adis.domain
    )
UNION ALL

    (
        SELECT
        appiu.topiaid as bad_usage_Id,
        dppi.topiaid as bad_di_Id,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = plot.domain where vdppi.refinput = dppi.refinput and vadis.code = adis.code) as valid_same_code_di,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = plot.domain where vdppi.refinput = dppi.refinput limit 1) as valid_other_code_di,
        adis.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM abstractphytoproductinputusage appiu
        inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dppi.topiaid
        inner join biologicalproductinputusage piu on piu.topiaid = appiu.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = piu.biologicalcontrolaction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adis.domain
    )
);

update abstractphytoproductinputusage sliu set domainphytoproductinput = (select CASE WHEN t.valid_same_code_di IS NOT NULL THEN t.valid_same_code_di ELSE t.valid_other_code_di END from biologicalproductinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid limit 1)
where exists (select 1 from biologicalproductinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid);

-- seedproductinputusage_to_domain
CREATE TEMPORARY TABLE seedproductinputusage_to_domain AS (
        SELECT
        appiu.topiaid as bad_usage_Id,
        dppi.topiaid as bad_di_Id,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = gp.domain where vdppi.refinput = dppi.refinput and vadis.code = adis.code) as valid_same_code_di,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = gp.domain where vdppi.refinput = dppi.refinput limit 1) as valid_other_code_di,
        adis.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM abstractphytoproductinputusage appiu
        inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dppi.topiaid
        inner join seedproductinputusage piu on piu.topiaid = appiu.topiaid
        inner join seedspeciesinputusage ssiu on ssiu.topiaid = piu.seedspeciesinputusage
        INNER JOIN abstractinputusage aiu ON aiu.topiaid = ssiu.topiaid
        inner join seedlotinputusage sliu on sliu.topiaid = ssiu.seedlotinputusage
        INNER JOIN abstractaction aa ON aa.topiaid = sliu.seedingactionusage
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adis.domain

UNION ALL
    (
        SELECT
        appiu.topiaid as bad_usage_Id,
        dppi.topiaid as bad_di_Id,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = gp.domain where vdppi.refinput = dppi.refinput and vadis.code = adis.code) as valid_same_code_di,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = gp.domain where vdppi.refinput = dppi.refinput limit 1) as valid_other_code_di,
        adis.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM abstractphytoproductinputusage appiu
        inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dppi.topiaid
        inner join seedproductinputusage piu on piu.topiaid = appiu.topiaid
        inner join seedspeciesinputusage ssiu on ssiu.topiaid = piu.seedspeciesinputusage
        INNER JOIN abstractinputusage aiu ON aiu.topiaid = ssiu.topiaid
        inner join seedlotinputusage sliu on sliu.topiaid = ssiu.seedlotinputusage
        INNER JOIN abstractaction aa ON aa.topiaid = sliu.seedingactionusage
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adis.domain
    )

UNION ALL
    (
        SELECT
        appiu.topiaid as bad_usage_Id,
        dppi.topiaid as bad_di_Id,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = plot.domain where vdppi.refinput = dppi.refinput and vadis.code = adis.code) as valid_same_code_di,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = plot.domain where vdppi.refinput = dppi.refinput limit 1) as valid_other_code_di,
        adis.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM abstractphytoproductinputusage appiu
        inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dppi.topiaid
        inner join seedproductinputusage piu on piu.topiaid = appiu.topiaid
        inner join seedspeciesinputusage ssiu on ssiu.topiaid = piu.seedspeciesinputusage
        INNER JOIN abstractinputusage aiu ON aiu.topiaid = ssiu.topiaid
        inner join seedlotinputusage sliu on sliu.topiaid = ssiu.seedlotinputusage
        INNER JOIN abstractaction aa ON aa.topiaid = sliu.seedingactionusage
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adis.domain
    )
UNION ALL

    (
        SELECT
        appiu.topiaid as bad_usage_Id,
        dppi.topiaid as bad_di_Id,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = plot.domain where vdppi.refinput = dppi.refinput and vadis.code = adis.code) as valid_same_code_di,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = plot.domain where vdppi.refinput = dppi.refinput limit 1) as valid_other_code_di,
        adis.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM abstractphytoproductinputusage appiu
        inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dppi.topiaid
        inner join seedproductinputusage piu on piu.topiaid = appiu.topiaid
        inner join seedspeciesinputusage ssiu on ssiu.topiaid = piu.seedspeciesinputusage
        INNER JOIN abstractinputusage aiu ON aiu.topiaid = ssiu.topiaid
        inner join seedlotinputusage sliu on sliu.topiaid = ssiu.seedlotinputusage
        INNER JOIN abstractaction aa ON aa.topiaid = sliu.seedingactionusage
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adis.domain
    )
);

update abstractphytoproductinputusage sliu set domainphytoproductinput = (select CASE WHEN t.valid_same_code_di IS NOT NULL THEN t.valid_same_code_di ELSE t.valid_other_code_di END from seedproductinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid limit 1)
where exists (select 1 from seedproductinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid);

-- pesticideproductinputusage_to_domain
CREATE TEMPORARY TABLE pesticideproductinputusage_to_domain AS (
        SELECT
        appiu.topiaid as bad_usage_Id,
        dppi.topiaid as bad_di_Id,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = gp.domain where vdppi.refinput = dppi.refinput and vadis.code = adis.code) as valid_same_code_di,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = gp.domain where vdppi.refinput = dppi.refinput limit 1) as valid_other_code_di,
        adis.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM abstractphytoproductinputusage appiu
        inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dppi.topiaid
        inner join pesticideproductinputusage piu on piu.topiaid = appiu.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = piu.pesticidesspreadingaction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adis.domain

UNION ALL
    (   SELECT
        appiu.topiaid as bad_usage_Id,
        dppi.topiaid as bad_di_Id,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = gp.domain where vdppi.refinput = dppi.refinput and vadis.code = adis.code) as valid_same_code_di,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = gp.domain where vdppi.refinput = dppi.refinput limit 1) as valid_other_code_di,
        adis.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM abstractphytoproductinputusage appiu
        inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dppi.topiaid
        inner join pesticideproductinputusage piu on piu.topiaid = appiu.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = piu.pesticidesspreadingaction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adis.domain
    )

UNION ALL
    (
        SELECT
        appiu.topiaid as bad_usage_Id,
        dppi.topiaid as bad_di_Id,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = plot.domain where vdppi.refinput = dppi.refinput and vadis.code = adis.code) as valid_same_code_di,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = plot.domain where vdppi.refinput = dppi.refinput limit 1) as valid_other_code_di,
        adis.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM abstractphytoproductinputusage appiu
        inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dppi.topiaid
        inner join pesticideproductinputusage piu on piu.topiaid = appiu.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = piu.pesticidesspreadingaction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adis.domain
    )
UNION ALL

    (
        SELECT
        appiu.topiaid as bad_usage_Id,
        dppi.topiaid as bad_di_Id,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = plot.domain where vdppi.refinput = dppi.refinput and vadis.code = adis.code) as valid_same_code_di,
        (select vdppi.topiaid from domainphytoproductinput vdppi inner join abstractdomaininputstockunit vadis on vadis.topiaid = vdppi.topiaid and vadis."domain" = plot.domain where vdppi.refinput = dppi.refinput limit 1) as valid_other_code_di,
        adis.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM abstractphytoproductinputusage appiu
        inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
        inner join abstractdomaininputstockunit adis on adis.topiaid = dppi.topiaid
        inner join pesticideproductinputusage piu on piu.topiaid = appiu.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = piu.pesticidesspreadingaction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adis.domain
    )
);

update abstractphytoproductinputusage sliu set domainphytoproductinput = (select CASE WHEN t.valid_same_code_di IS NOT NULL THEN t.valid_same_code_di ELSE t.valid_other_code_di END from pesticideproductinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid limit 1)
where exists (select 1 from pesticideproductinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid);


-- OtherProductInputUsage_to_domain
CREATE TEMPORARY TABLE otherproductinputusage_to_domain AS (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainotherinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainotherinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM otherproductinputusage iu
        inner join domainotherinput di on di.topiaid = iu.domainotherinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.otheraction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adi.domain

UNION ALL
    (   SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainotherinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainotherinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM otherproductinputusage iu
        inner join domainotherinput di on di.topiaid = iu.domainotherinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.otheraction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adi.domain
    )

UNION ALL
    (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainotherinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainotherinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM otherproductinputusage iu
        inner join domainotherinput di on di.topiaid = iu.domainotherinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.otheraction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adi.domain
    )
UNION ALL

    (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainotherinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainotherinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM otherproductinputusage iu
        inner join domainotherinput di on di.topiaid = iu.domainotherinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.otheraction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adi.domain
    )
);

update otherproductinputusage sliu set domainotherinput = (select CASE WHEN t.valid_same_code_di IS NOT NULL THEN t.valid_same_code_di ELSE t.valid_other_code_di END from otherproductinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid limit 1)
where exists (select 1 from otherproductinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid);


-- organicproductinputusage_to_domain
CREATE TEMPORARY TABLE organicproductinputusage_to_domain AS (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainorganicproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainorganicproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM organicproductinputusage iu
        inner join domainorganicproductinput di on di.topiaid = iu.domainorganicproductinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.organicfertilizersspreadingaction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adi.domain

UNION ALL
    (   SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainorganicproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainorganicproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM organicproductinputusage iu
        inner join domainorganicproductinput di on di.topiaid = iu.domainorganicproductinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.organicfertilizersspreadingaction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adi.domain
    )

UNION ALL
    (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainorganicproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainorganicproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM organicproductinputusage iu
        inner join domainorganicproductinput di on di.topiaid = iu.domainorganicproductinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.organicfertilizersspreadingaction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adi.domain
    )
UNION ALL

    (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainorganicproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainorganicproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM organicproductinputusage iu
        inner join domainorganicproductinput di on di.topiaid = iu.domainorganicproductinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.organicfertilizersspreadingaction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adi.domain
    )
);

update organicproductinputusage sliu set domainorganicproductinput = (select CASE WHEN t.valid_same_code_di IS NOT NULL THEN t.valid_same_code_di ELSE t.valid_other_code_di END from organicproductinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid limit 1)
where exists (select 1 from organicproductinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid);

-- substrateinputusage_to_domain
CREATE TEMPORARY TABLE substrateinputusage_to_domain AS (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainsubstrateinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainsubstrateinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM substrateinputusage iu
        inner join domainsubstrateinput di on di.topiaid = iu.domainsubstrateinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.otheraction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adi.domain

UNION ALL
    (   SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainsubstrateinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainsubstrateinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM substrateinputusage iu
        inner join domainsubstrateinput di on di.topiaid = iu.domainsubstrateinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.otheraction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adi.domain
    )

UNION ALL
    (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainsubstrateinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainsubstrateinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM substrateinputusage iu
        inner join domainsubstrateinput di on di.topiaid = iu.domainsubstrateinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.otheraction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adi.domain
    )
UNION ALL

    (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainsubstrateinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainsubstrateinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM substrateinputusage iu
        inner join domainsubstrateinput di on di.topiaid = iu.domainsubstrateinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.otheraction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adi.domain
    )
);

update substrateinputusage sliu set domainsubstrateinput = (select CASE WHEN t.valid_same_code_di IS NOT NULL THEN t.valid_same_code_di ELSE t.valid_other_code_di END from substrateinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid limit 1)
where exists (select 1 from substrateinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid);

-- potinputusage_to_domain
CREATE TEMPORARY TABLE potinputusage_to_domain AS (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainpotinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainpotinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM potinputusage iu
        inner join domainpotinput di on di.topiaid = iu.domainpotinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.otheraction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adi.domain

UNION ALL
    (   SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainpotinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainpotinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM potinputusage iu
        inner join domainpotinput di on di.topiaid = iu.domainpotinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.otheraction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adi.domain
    )

UNION ALL
    (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainpotinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainpotinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM potinputusage iu
        inner join domainpotinput di on di.topiaid = iu.domainpotinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.otheraction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adi.domain
    )
UNION ALL

    (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainpotinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainpotinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM potinputusage iu
        inner join domainpotinput di on di.topiaid = iu.domainpotinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.otheraction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adi.domain
    )
);

update potinputusage sliu set domainpotinput = (select CASE WHEN t.valid_same_code_di IS NOT NULL THEN t.valid_same_code_di ELSE t.valid_other_code_di END from potinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid limit 1)
where exists (select 1 from potinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid);


-- mineralproductinputusage_to_domain
CREATE TEMPORARY TABLE mineralproductinputusage_to_domain AS (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainmineralproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainmineralproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM mineralproductinputusage iu
        inner join domainmineralproductinput di on di.topiaid = iu.domainmineralproductinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.mineralfertilizersspreadingaction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adi.domain

UNION ALL
    (   SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainmineralproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainmineralproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = gp.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        gp.domain as valid_domain_id
        FROM mineralproductinputusage iu
        inner join domainmineralproductinput di on di.topiaid = iu.domainmineralproductinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.mineralfertilizersspreadingaction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where gp.domain != adi.domain
    )

UNION ALL
    (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainmineralproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainmineralproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM mineralproductinputusage iu
        inner join domainmineralproductinput di on di.topiaid = iu.domainmineralproductinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.mineralfertilizersspreadingaction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adi.domain
    )
UNION ALL

    (
        SELECT
        iu.topiaid as bad_usage_Id,
        di.topiaid as bad_di_Id,
        (select v_di.topiaid from domainmineralproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput and vadi.code = adi.code) as valid_same_code_di,
        (select v_di.topiaid from domainmineralproductinput v_di inner join abstractdomaininputstockunit vadi on vadi.topiaid = v_di.topiaid and vadi."domain" = plot.domain where v_di.refinput = di.refinput limit 1) as valid_other_code_di,
        adi.domain as bad_domain_id,
        plot.domain as valid_domain_id
        FROM mineralproductinputusage iu
        inner join domainmineralproductinput di on di.topiaid = iu.domainmineralproductinput
        inner join abstractdomaininputstockunit adi on adi.topiaid = di.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = iu.mineralfertilizersspreadingaction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where plot.domain != adi.domain
    )
);

update mineralproductinputusage sliu set domainmineralproductinput = (select CASE WHEN t.valid_same_code_di IS NOT NULL THEN t.valid_same_code_di ELSE t.valid_other_code_di END from mineralproductinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid limit 1)
where exists (select 1 from mineralproductinputusage_to_domain t where t.bad_usage_Id = sliu.topiaid);