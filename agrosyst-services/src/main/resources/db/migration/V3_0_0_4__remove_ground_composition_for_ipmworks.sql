---
-- %%Ignore-License
--
--

ALTER TABLE Ground drop constraint FKgg39vviivs09adlxusciegg7g;
ALTER TABLE Ground DROP COLUMN composition;

DROP TABLE GroundComposition;

DELETE FROM Ground WHERE refsolarvalis IS NULL;
ALTER TABLE Ground ALTER refsolarvalis SET NOT NULL;
