---
-- %%Ignore-License
---

-- l'information comme quoi les semences du lot inclues un traitement enrobé ou séparé de la graine
-- est maintenant portée sur le lot
-- à noté qu'il y a actuellement une anomalie dans les données persistées, car avant le #12748 si l'utilisateur
-- sélectionné 'Traitement inclus dans la semence' (cas sélectionné par défaut dans le cas ou un traitement est déclaré sur une espèce)
-- l'information n'était pas sauvegardée et l'information passait à non après sauvegarde du domaine)

alter table domainseedlotinput add column seedCoatedTreatment boolean;

-- par défaut on considère que les semences on un traitement enrobé 'seedCoatedTreatment = true'
-- on définit que les semences ont un traitement séparé lorsque sur le lot il existe au moins une espèce déclarée
-- 1) avec un traitement chimique ou biologique
-- 2) que l'utilisateur a renseigné un prix par espèce et sélectionné non à 'Traitement inclus dans la semence'
-- 3) ou que l'utilisateur à renseigné un prix de traitement

-- requête qui déclare que le traitement n'est pas enrobé à la graine
-- on sait que l'information est correctement sauvegardé si l'utilisateur a renseigné un prix pour le lot
update domainseedlotinput dsli set seedCoatedTreatment = false
  where (dsli.seedprice is not null and dsli.seedprice in (
    select lip.topiaid from inputprice lip
    inner join domainseedlotinput dsli_wp on dsli_wp.seedprice = lip.topiaid
    where dsli_wp.topiaid = dsli.topiaid
    and lip.includedtreatment is false
  ))
  -- on sait que l'information est correctement sauvegardé si l'utilisateur a renseigné un prix par espèce
  or (dsli.seedprice is null and dsli.topiaid in (
    select dss.domainseedlotinput from domainseedspeciesinput dss
    left join domainphytoproductinput dppi on dppi.domainseedspeciesinput = dss.topiaid
    left join inputprice sp on sp.topiaid = dss.seedprice
    left join abstractdomaininputstockunit phyto_adisu on phyto_adisu.topiaid = dppi.topiaid
    where (dss.chemicaltreatment is true or dss.biologicalseedinoculation is true)
    and (
      phyto_adisu.inputprice is not null
      or (sp.topiaid is not null and sp.includedtreatment is false))
  ));
-- dans les autres cas on applique le comportement par défaut dans le cas ou il n'y a un traitement de déclaré
-- on déclare que le traitement est enrobé à la graine
update domainseedlotinput dsli set seedCoatedTreatment = true
  where dsli.seedCoatedTreatment is null
  and dsli.topiaid in (
    select dss.domainseedlotinput from domainseedspeciesinput dss
    where (dss.chemicaltreatment is true or dss.biologicalseedinoculation is true)
);

update domainseedlotinput dsli set seedCoatedTreatment = false
  where dsli.seedCoatedTreatment is null;