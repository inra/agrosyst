---
-- %%Ignore-License
---

CREATE TABLE _12208_delete_wrong_crop_domainseedspeciesinput AS (
  select dssi.*
  from (
    select res.domai_id, res.di_topiaid, res.cpe_id
    from (
      select distinct adi."domain" domai_id, adi.topiaid di_topiaid, cpe.topiaid cpe_id, cps.croppingplanentry cps_cpe_id
      from domainseedlotinput dsli
      inner join abstractdomaininputstockunit adi on adi.topiaid = dsli.topiaid
      inner join domainseedspeciesinput dssi on dssi.domainseedlotinput = dsli.topiaid
      inner join croppingplanentry cpe on dsli.cropseed = cpe.topiaid
      inner join croppingplanspecies cps on cps.topiaid = dssi.speciesseed
      inner join croppingplanentry cps_cpe on cps_cpe.topiaid = cps.croppingplanentry
    ) as res
    group by res.domai_id, res.di_topiaid, res.cpe_id
    having count(*) > 1
  ) as tmp_res
  inner join domainseedlotinput dsli on dsli.topiaid = tmp_res.di_topiaid
  inner join abstractdomaininputstockunit adi on adi.topiaid = tmp_res.di_topiaid
  inner join domainseedspeciesinput dssi on dssi.domainseedlotinput = dsli.topiaid
  inner join croppingplanentry cpe on dsli.cropseed = cpe.topiaid
  inner join croppingplanspecies cps on cps.topiaid = dssi.speciesseed
  where cps.croppingplanentry != tmp_res.cpe_id
  group by tmp_res.domai_id ,tmp_res.di_topiaid, tmp_res.cpe_id, cps.croppingplanentry, dssi.topiaid
  order by tmp_res.domai_id, tmp_res.di_topiaid, tmp_res.cpe_id, cps.croppingplanentry
);

select * from _12208_delete_wrong_crop_domainseedspeciesinput;

delete from seedspeciesinputusage ssu where ssu.domainseedspeciesinput in (select topiaid from _12208_delete_wrong_crop_domainseedspeciesinput);
delete from inputprice where topiaid in (select seedprice from domainseedspeciesinput dssi where dssi.topiaid in (select topiaid from _12208_delete_wrong_crop_domainseedspeciesinput));
delete from domainseedspeciesinput where topiaid in (select topiaid from _12208_delete_wrong_crop_domainseedspeciesinput);
delete from abstractdomaininputstockunit where topiaid in (select topiaid from _12208_delete_wrong_crop_domainseedspeciesinput);