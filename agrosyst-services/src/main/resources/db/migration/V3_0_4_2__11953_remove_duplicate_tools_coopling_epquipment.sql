---
-- %%Ignore-License
---

DELETE FROM equipments_toolscoupling etc0 WHERE etc0.ctid IN (
SELECT etc.ctid FROM equipments_toolscoupling etc
  WHERE toolscoupling IN
      (SELECT toolscoupling
       FROM
          (SELECT toolscoupling,
           ROW_NUMBER() OVER( PARTITION BY toolscoupling, equipments
          ORDER BY toolscoupling, equipments) AS row_num
          FROM equipments_toolscoupling ) t
          WHERE t.row_num > 1));
