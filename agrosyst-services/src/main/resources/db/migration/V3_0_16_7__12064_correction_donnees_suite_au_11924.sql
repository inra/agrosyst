---
-- %%Ignore-License
---

-- Prérequis à ce script : avoir exécuté sur sa base le script SQL en pièce-jointe du ticket 12064 sur le Redmine.

-- Nettoyage des données
delete from _12064_harvestingactionvalorisation_manquants havm
where exists (
  select 1 from harvestingactionvalorisation hav
  where hav.topiaid = havm.topiaid
);

delete from _12064_harvestingactionvalorisation_manquants ham
where exists (
	select 1 from harvestingactionvalorisation h
	 where ham.harvestingaction = h.harvestingaction
	   and ham.topiaid != h.topiaid
	   and ham.destination = h.destination
	   and h.topiacreatedate > '2023-06-27 18:00'
	   -- date approximative de la MEP de la 3.0.4, qui contenait le ticket 11924 qui a supprimé
	   -- par accident les lignes que l'on cherche à ré-importer.
);

delete from _12064_qualitycriteria_manquants qm
where not exists (
	select 1 from harvestingactionvalorisation h
	where h.topiaid = qm.harvestingactionvalorisation
);

-- Import des lignes supprimées dans le 11924 dans les tables, afin de pouvoir les supprimer par la suite.
insert into harvestingactionvalorisation
select * from _12064_harvestingactionvalorisation_manquants ;

insert into qualitycriteria
select * from _12064_qualitycriteria_manquants;

drop table if exists _12064_valid_duplicate_valorisations;
drop table if exists _12064_non_valid_duplicate_valorisations;
drop table if exists _12064_valid_duplicate_valorisation_to_remove;
drop table if exists _12064_removed_qualitycriteria;
drop table if exists _12064_removed_price;

-- Par rapport au script de migration du ticket 11924 :
--   - la jointure entre effectivespeciesstade et croppingplanspecies est corrigée ;
--   - le reste est identique.
--
-- Conséquences :
--   - il y aura plus de lignes dans cette table
--   - il y aura moins de lignes dans _11924_non_valid_duplicate_valorisations. Et donc, la suppression qu'il y a
--     en fin de script sera plus précise. Car autrement, on avait un certain nombre de lignes qui étaient considérées
--     comme étant non valides alors qu'elles l'étaient bien, donc on les supprimait alors qu'il ne le fallait pas.
create table _12064_valid_duplicate_valorisations as (
  (select hav.topiaid, hav.harvestingaction, hav.speciescode, hav.destination, hav.yealdunit,hav.harvestingaction_idx, hav.topiaversion, hav.topiacreatedate, hav.salespercent, hav.selfconsumedpersent, hav.novalorisationpercent, hav.yealdmin, hav.yealdmax, hav.yealdaverage, hav.yealdmedian, hav.beginmarketingperiod, hav.beginmarketingperioddecade, hav.beginmarketingperiodcampaign, hav.endingmarketingperiod, hav.endingmarketingperioddecade, hav.endingmarketingperiodcampaign, hav.isorganiccrop, hp.price, hp.priceunit
	  from harvestingactionvalorisation hav
	  inner join harvestingprice hp on hp.harvestingactionvalorisation = hav.topiaId
	  inner join _11924_all_duplicate_valorisations_context dvi on dvi.harvestingaction = hav.harvestingaction
	  inner join abstractaction aa on hav.harvestingaction = aa.topiaid
	  inner join effectiveintervention ei on ei.topiaid = aa.effectiveintervention
	  inner join effectivespeciesstade ess on ess.effectiveintervention = ei.topiaid
	  inner join croppingplanspecies cps on ess.croppingplanspecies = cps.topiaid
	  where hav.speciescode = cps.code
	  order by hav.harvestingaction, speciescode, destination, hp.price desc, hav.topiacreatedate desc)
  union
    (select hav.topiaid, hav.harvestingaction, hav.speciescode, hav.destination,hav.yealdunit, hav.harvestingaction_idx, hav.topiaversion, hav.topiacreatedate, hav.salespercent, hav.selfconsumedpersent, hav.novalorisationpercent, hav.yealdmin, hav.yealdmax, hav.yealdaverage, hav.yealdmedian, hav.beginmarketingperiod, hav.beginmarketingperioddecade, hav.beginmarketingperiodcampaign, hav.endingmarketingperiod, hav.endingmarketingperioddecade, hav.endingmarketingperiodcampaign, hav.isorganiccrop, hp.price, hp.priceunit
      from harvestingactionvalorisation hav
      inner join harvestingprice hp on hp.harvestingactionvalorisation = hav.topiaId
	  inner join _11924_all_duplicate_valorisations_context dvi on dvi.harvestingaction = hav.harvestingaction
	  inner join abstractaction aa on hav.harvestingaction = aa.topiaid
	  inner join practicedintervention pi on pi.topiaid = aa.practicedintervention
	  inner join practicedspeciesstade pss on pss.practicedintervention = pi.topiaid
	  where hav.speciescode = pss.speciescode
	  order by hav.harvestingaction, speciescode, destination, hp.price desc, hav.topiacreatedate desc)
);

create table _12064_non_valid_duplicate_valorisations as (
    select hav.topiaid, hav.harvestingaction, hav.speciescode, hav.destination,hav.yealdunit, hav.harvestingaction_idx, hav.topiaversion, hav.topiacreatedate, hav.salespercent, hav.selfconsumedpersent, hav.novalorisationpercent, hav.yealdmin, hav.yealdmax, hav.yealdaverage, hav.yealdmedian, hav.beginmarketingperiod, hav.beginmarketingperioddecade, hav.beginmarketingperiodcampaign, hav.endingmarketingperiod, hav.endingmarketingperioddecade, hav.endingmarketingperiodcampaign, hav.isorganiccrop, hp.price, hp.priceunit
      from harvestingactionvalorisation hav
      inner join harvestingprice hp on hp.harvestingactionvalorisation = hav.topiaId
	  inner join _11924_all_duplicate_valorisations_context dvi on dvi.harvestingaction = hav.harvestingaction
	  where not exists (
	    select 1 from _12064_valid_duplicate_valorisations vdv
	    where vdv.topiaid = hav.topiaid
	  )
);

-- parmi les doublons, on garde s'il existe le rendement > 0, un prix ou le plus récent en priorité
create table _12064_valid_duplicate_valorisation_to_remove as
SELECT * FROM _12064_valid_duplicate_valorisations
WHERE topiaid IN (
  SELECT topiaid FROM (
    SELECT topiaid,
     ROW_NUMBER() OVER( PARTITION BY harvestingaction, speciescode, destination
     ORDER BY yealdaverage desc, price desc, topiaversion, topiacreatedate desc) AS row_num
     FROM _12064_valid_duplicate_valorisations) t
  WHERE t.row_num > 1
);

-- On conserve une copie des critères de qualité à supprimer
create table _12064_removed_qualitycriteria as (
  select * from qualitycriteria q
  where exists ( select 1 from _12064_valid_duplicate_valorisation_to_remove v where v.topiaId = q.harvestingactionvalorisation )
  UNION
  select * from qualitycriteria q
  where exists ( select 1 from _12064_non_valid_duplicate_valorisations v where v.topiaId = q.harvestingactionvalorisation )
);

-- On conserve une copie des prix à supprimer
create table _12064_removed_price as (
  select * from harvestingprice hp
  where exists ( select 1 from _12064_valid_duplicate_valorisation_to_remove v where v.topiaId = hp.harvestingactionvalorisation )
  UNION
  select * from harvestingprice hp
  where exists ( select 1 from _12064_non_valid_duplicate_valorisations v where v.topiaId = hp.harvestingactionvalorisation )
);

delete from qualitycriteria qc where exists (select 1 from _12064_removed_qualitycriteria rqq where rqq.topiaId = qc.topiaId);
delete from harvestingprice hp where exists (select 1 from _12064_removed_price rp where rp.topiaId = hp.topiaId);

delete from harvestingactionvalorisation hav where exists (select 1 from _12064_valid_duplicate_valorisation_to_remove v where v.topiaid = hav.topiaid);
delete from harvestingactionvalorisation hav where exists (select 1 from _12064_non_valid_duplicate_valorisations v where v.topiaid = hav.topiaid);
