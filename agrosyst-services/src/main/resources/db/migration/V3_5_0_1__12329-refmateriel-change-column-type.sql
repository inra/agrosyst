---
-- %%Ignore-License
---
ALTER TABLE refmateriel
ALTER COLUMN donneestauxdechargemoteur TYPE float8
USING REPLACE(NULLIF(donneestauxdechargemoteur, ''), ',', '.')::float8;
-- change les virgules en points pour que PG puisse parser les flottants, et force à NULL pour le cas des chaînes vides.