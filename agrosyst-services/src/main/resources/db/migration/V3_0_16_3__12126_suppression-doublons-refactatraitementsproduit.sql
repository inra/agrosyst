---
-- %%Ignore-License
---

update domainphytoproductinput
set refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_95dace15-bb4d-41b9-ab8d-b6c913050e0d'
where refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_7de426ce-09ab-4a77-a65f-74fb02cffe40';

update domainphytoproductinput
set refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_bb40c48e-6dc9-4ae4-85d9-22e851f5ca60'
where refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_85922ec2-da4e-4adb-956f-128d069154ce';

delete from refactatraitementsproduit
where topiaid in (
	'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_7de426ce-09ab-4a77-a65f-74fb02cffe40',
	'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_85922ec2-da4e-4adb-956f-128d069154ce'
)
and active = false;
