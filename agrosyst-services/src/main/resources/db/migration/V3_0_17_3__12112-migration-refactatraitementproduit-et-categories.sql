---
-- %%Ignore-License
---

--
--        .
--       / \
--      / ! \
--     /_____\
--
-- PRÉREQUIS : passer le script 12112-script-donnees-temporaires.sql qui est en PJ du ticket 12112.
--

insert into refactatraitementsproduitscateg (
	topiaid, topiaversion, topiacreatedate, id_traitement, code_traitement, nom_traitement,
	action, type_produit, ift_chimique_total, ift_chimique_tot_hts, ift_h, ift_f, ift_i, ift_ts, ift_a, ift_hh, ift_moy_bio, source, active
)
values
(
	CONCAT('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg_', uuid_in(md5(random()::text || now()::text)::cstring)),
	0, now(), 162, 'M2', 'Moyens biologiques - Biotique - Substances de base', 'LUTTE_BIOLOGIQUE', 'BASIC_SUBSTANCES',
	false, false, false, false, false, false, false, false, true, 'Agrosyst 2023', true
),
(
  CONCAT('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg_', uuid_in(md5(random()::text || now()::text)::cstring)),
	0, now(), 164, 'M4', 'Moyens biologiques - Abiotique - Extraits de plante', 'LUTTE_BIOLOGIQUE', 'PLANT_EXTRACT',
	false, false, false, false, false, false, false, false, true, 'Agrosyst 2023', true
)
;

delete from refactatraitementsproduitscateg rc
where nom_traitement ilike '%Substances de base%' and code_traitement != 'M2';

delete from refactatraitementsproduitscateg rc
where nom_traitement ilike '%Extraits de plante' and code_traitement != 'M4';

update refactatraitementsproduit r
set id_produit          = tmp.id_produit,
    id_traitement       = tmp.id_traitement,
    code_traitement     = tmp.code_traitement,
    nom_traitement      = tmp.nom_traitement,
    "source"            = tmp."source",
    code_amm            = tmp.code_amm,
    code_traitement_maa = tmp.code_traitement_maa,
    nom_traitement_maa  = tmp.nom_traitement_maa
from "_12112_tmp_maj_refactatraitementsproduit" tmp
where tmp.topiaid = r.topiaid
;

