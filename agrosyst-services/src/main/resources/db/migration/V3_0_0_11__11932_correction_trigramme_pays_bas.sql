---
-- %%Ignore-License
---

update trad_ref_destination set lang = 'nld' where lang = 'dut';
update trad_ref_divers set lang = 'nld' where lang = 'dut';
update trad_ref_intervention set lang = 'nld' where lang = 'dut';
update trad_ref_intrant set lang = 'nld' where lang = 'dut';
update trad_ref_materiel set lang = 'nld' where lang = 'dut';
update trad_ref_sol set lang = 'nld' where lang = 'dut';
update trad_ref_vivant set lang = 'nld' where lang = 'dut';
