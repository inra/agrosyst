---
-- %%Ignore-License
---
alter table reflocation add column altitude_moy integer null;
alter table reflocation add column aire_attr text null;
alter table reflocation add column arrondissement_code text null;
alter table reflocation add column bassin_vie text null;
alter table reflocation add column intercommunalite_code text null;
alter table reflocation add column unite_urbaine text null;
alter table reflocation add column zone_emploi text null;
alter table reflocation add column bassin_viticole text null;
alter table reflocation add column ancienne_region text null;
