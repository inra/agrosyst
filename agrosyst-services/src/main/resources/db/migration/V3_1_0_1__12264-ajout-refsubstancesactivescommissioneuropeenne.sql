---
-- %%Ignore-License
---

CREATE TABLE refsubstancesactivescommissioneuropeenne (
  topiaid varchar(255) NOT NULL,
  topiaversion int8 NOT NULL,
  topiacreatedate timestamp NULL,
  id_sa text not null,
  libelle_sa_eu text not null,
  statut_regl_1107_2009 text not null,
  libelle_sa_fr text,
  libelle_sa_variant_fr text,
  numero_cas text,
  autorisationFR text,
  coef_ponderation float8,
  campagne int4,
  date_approbation_eu date,
  date_expiration_eu date,
  sa_candidates_substitution bool,
  substances_de_base bool,
  sa_faible_risque bool,
  pays text,
  source text not null,
  active bool not null,

  CONSTRAINT refsubstancesactivescommissioneuropeenne_pkey PRIMARY KEY (topiaid),
  CONSTRAINT uk_refsubstancesactivescommissioneuropeenne UNIQUE (id_sa)
);
