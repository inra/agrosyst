---
-- %%Ignore-License
---

-- Corrige erreurs d'arrondis introduites dans le script V3_0_12_4__12079_migrate_qtavg_for_seedingproductinput
-- au niveau du calcul de la valeur "complémentaire" : l'arrondi à 3 chiffres a été oublié à ce moment-là.

update abstractinputusage
set qtavg = round(cast(qtavg as numeric), 3)
where deprecatedid is not null and deprecatedqtavg is not null and deprecatedqtavg > 0
;
