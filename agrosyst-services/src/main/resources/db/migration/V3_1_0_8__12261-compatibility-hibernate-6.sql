---
-- %%Ignore-License
---

--ALTER TABLE abstractDomainInputStockUnit RENAME COLUMN "inputkey" TO key;
--ALTER TABLE trad_ref_destination RENAME COLUMN "tradkey" TO key;
--ALTER TABLE trad_ref_divers RENAME COLUMN "tradkey" TO key;
--ALTER TABLE trad_ref_intervention RENAME COLUMN "tradkey" TO key;
--ALTER TABLE trad_ref_intrant RENAME COLUMN "tradkey" TO key;
--ALTER TABLE trad_ref_materiel RENAME COLUMN "tradkey" TO key;
--ALTER TABLE trad_ref_sol RENAME COLUMN "tradkey" TO key;
--ALTER TABLE trad_ref_vivant RENAME COLUMN "tradkey" TO key;
--ALTER TABLE computed_cache RENAME COLUMN "cache_key" TO key;


-- inside model
ALTER TABLE abstractDomainInputStockUnit RENAME COLUMN "key" TO inputkey;
-- outside model
ALTER TABLE trad_ref_destination RENAME COLUMN "key" TO tradkey;
ALTER TABLE trad_ref_divers RENAME COLUMN "key" TO tradkey;
ALTER TABLE trad_ref_intervention RENAME COLUMN "key" TO tradkey;
ALTER TABLE trad_ref_intrant RENAME COLUMN "key" TO tradkey;
ALTER TABLE trad_ref_materiel RENAME COLUMN "key" TO tradkey;
ALTER TABLE trad_ref_sol RENAME COLUMN "key" TO tradkey;
ALTER TABLE trad_ref_vivant RENAME COLUMN "key" TO tradkey;
ALTER TABLE computed_cache RENAME COLUMN "key" TO cache_key;
