---
-- %%Ignore-License
---

--
-- Restauration de tous les doublons connus, afin de pouvoir ensuite les traiter correctement, et donc récupérer
-- toutes les valorisations manquantes.
--

select distinct(vdv.topiaid) into _12327_non_valid_duplications_to_restore from "_11924_non_valid_duplicate_valorisations" vdv
where not exists (
	select 1 from harvestingactionvalorisation h
	where h.topiaid = vdv.topiaid
)
;

select distinct(topiaid) into _12327_valid_duplications_to_restore from "_11924_valid_duplicate_valorisations" vdv
where not exists (
	select 1 from harvestingactionvalorisation h
	where h.topiaid = vdv.topiaid
)
;


CREATE OR REPLACE FUNCTION restore_duplicate_non_valid_valorisations() RETURNS void
LANGUAGE plpgsql AS $$
declare topiaid_to_restore varchar(255);
begin
  for topiaid_to_restore in
		select r.topiaid
		from _12327_non_valid_duplications_to_restore r
		where not exists (select 1 from harvestingactionvalorisation h where h.topiaid=r.topiaid)

	loop
		insert into harvestingactionvalorisation(topiaid, harvestingaction, speciescode, destination, yealdunit, harvestingaction_idx, topiaversion, topiacreatedate, salespercent, selfconsumedpersent, novalorisationpercent, yealdmin, yealdmax, yealdaverage, yealdmedian, beginmarketingperiod, beginmarketingperioddecade, beginmarketingperiodcampaign, endingmarketingperiod, endingmarketingperioddecade, endingmarketingperiodcampaign, isorganiccrop) (
			select topiaid, harvestingaction, speciescode, destination, yealdunit, harvestingaction_idx, topiaversion, topiacreatedate, salespercent, selfconsumedpersent, novalorisationpercent, yealdmin, yealdmax, yealdaverage, yealdmedian, beginmarketingperiod, beginmarketingperioddecade, beginmarketingperiodcampaign, endingmarketingperiod, endingmarketingperioddecade, endingmarketingperiodcampaign, isorganiccrop
			from _11924_non_valid_duplicate_valorisations nvdv
			where nvdv.topiaid = topiaid_to_restore
			and exists (select 1 from abstractaction a where a.topiaid=nvdv.harvestingaction)
			limit 1
		);
	end loop;
END
$$;

CREATE OR REPLACE FUNCTION restore_duplicate_valid_valorisations() RETURNS void
LANGUAGE plpgsql AS $$
declare topiaid_to_restore varchar(255);
begin
  for topiaid_to_restore in
		select r.topiaid
		from _12327_valid_duplications_to_restore r
		where not exists (select 1 from harvestingactionvalorisation h where h.topiaid=r.topiaid)
	loop
		insert into harvestingactionvalorisation(topiaid, harvestingaction, speciescode, destination, yealdunit, harvestingaction_idx, topiaversion, topiacreatedate, salespercent, selfconsumedpersent, novalorisationpercent, yealdmin, yealdmax, yealdaverage, yealdmedian, beginmarketingperiod, beginmarketingperioddecade, beginmarketingperiodcampaign, endingmarketingperiod, endingmarketingperioddecade, endingmarketingperiodcampaign, isorganiccrop) (
			select topiaid, harvestingaction, speciescode, destination, yealdunit, harvestingaction_idx, topiaversion, topiacreatedate, salespercent, selfconsumedpersent, novalorisationpercent, yealdmin, yealdmax, yealdaverage, yealdmedian, beginmarketingperiod, beginmarketingperioddecade, beginmarketingperiodcampaign, endingmarketingperiod, endingmarketingperioddecade, endingmarketingperiodcampaign, isorganiccrop
			from _11924_valid_duplicate_valorisations nvdv
			where nvdv.topiaid = topiaid_to_restore
			and exists (select 1 from abstractaction a where a.topiaid=nvdv.harvestingaction)
			limit 1
		);
	end loop;
END
$$;

select restore_duplicate_non_valid_valorisations();
select restore_duplicate_valid_valorisations();

drop function restore_duplicate_non_valid_valorisations;
drop function restore_duplicate_valid_valorisations;

insert into qualitycriteria (select * from _11924_removed_qualitycriteria);
insert into harvestingprice (select * from _11924_removed_price rp where exists (select 1 from harvestingactionvalorisation h where h.topiaid = rp.harvestingactionvalorisation));



--
-- Reconstitution des tables du ticket 12064. On les reconstitue à partir des "bonnes données", c'est-à-dire tous les
-- doublons que l'on cherche à supprimer, pour pouvoir ensuite justement les supprimer et obtenir finalement toutes
-- les valorisations manquantes, qui avaient été supprimées par erreur dans le ticket 11924.
--

drop table if exists _12064_valid_duplicate_valorisations;
drop table if exists _12064_non_valid_duplicate_valorisations;
drop table if exists _12064_valid_duplicate_valorisation_to_remove;
drop table if exists _12064_removed_qualitycriteria;
drop table if exists _12064_removed_price;

-- Par rapport au script de migration du ticket 11924 :
--   - la jointure entre effectivespeciesstade et croppingplanspecies est corrigée ;
--   - le reste est identique.
--
-- Conséquences :
--   - il y aura plus de lignes dans cette table
--   - il y aura moins de lignes dans _11924_non_valid_duplicate_valorisations. Et donc, la suppression qu'il y a
--     en fin de script sera plus précise. Car autrement, on avait un certain nombre de lignes qui étaient considérées
--     comme étant non valides alors qu'elles l'étaient bien, donc on les supprimait alors qu'il ne le fallait pas.
create table _12064_valid_duplicate_valorisations as (
  (select hav.topiaid, hav.harvestingaction, hav.speciescode, hav.destination, hav.yealdunit,hav.harvestingaction_idx, hav.topiaversion, hav.topiacreatedate, hav.salespercent, hav.selfconsumedpersent, hav.novalorisationpercent, hav.yealdmin, hav.yealdmax, hav.yealdaverage, hav.yealdmedian, hav.beginmarketingperiod, hav.beginmarketingperioddecade, hav.beginmarketingperiodcampaign, hav.endingmarketingperiod, hav.endingmarketingperioddecade, hav.endingmarketingperiodcampaign, hav.isorganiccrop, hp.price, hp.priceunit
	  from harvestingactionvalorisation hav
	  inner join harvestingprice hp on hp.harvestingactionvalorisation = hav.topiaId
	  inner join _11924_all_duplicate_valorisations_context dvi on dvi.harvestingaction = hav.harvestingaction
	  inner join abstractaction aa on hav.harvestingaction = aa.topiaid
	  inner join effectiveintervention ei on ei.topiaid = aa.effectiveintervention
	  inner join effectivespeciesstade ess on ess.effectiveintervention = ei.topiaid
	  inner join croppingplanspecies cps on ess.croppingplanspecies = cps.topiaid
	  where hav.speciescode = cps.code
	  order by hav.harvestingaction, speciescode, destination, hp.price desc, hav.topiacreatedate desc)
  union
    (select hav.topiaid, hav.harvestingaction, hav.speciescode, hav.destination,hav.yealdunit, hav.harvestingaction_idx, hav.topiaversion, hav.topiacreatedate, hav.salespercent, hav.selfconsumedpersent, hav.novalorisationpercent, hav.yealdmin, hav.yealdmax, hav.yealdaverage, hav.yealdmedian, hav.beginmarketingperiod, hav.beginmarketingperioddecade, hav.beginmarketingperiodcampaign, hav.endingmarketingperiod, hav.endingmarketingperioddecade, hav.endingmarketingperiodcampaign, hav.isorganiccrop, hp.price, hp.priceunit
      from harvestingactionvalorisation hav
      inner join harvestingprice hp on hp.harvestingactionvalorisation = hav.topiaId
	  inner join _11924_all_duplicate_valorisations_context dvi on dvi.harvestingaction = hav.harvestingaction
	  inner join abstractaction aa on hav.harvestingaction = aa.topiaid
	  inner join practicedintervention pi on pi.topiaid = aa.practicedintervention
	  inner join practicedspeciesstade pss on pss.practicedintervention = pi.topiaid
	  where hav.speciescode = pss.speciescode
	  order by hav.harvestingaction, speciescode, destination, hp.price desc, hav.topiacreatedate desc)
);

create table _12064_non_valid_duplicate_valorisations as (
    select hav.topiaid, hav.harvestingaction, hav.speciescode, hav.destination,hav.yealdunit, hav.harvestingaction_idx, hav.topiaversion, hav.topiacreatedate, hav.salespercent, hav.selfconsumedpersent, hav.novalorisationpercent, hav.yealdmin, hav.yealdmax, hav.yealdaverage, hav.yealdmedian, hav.beginmarketingperiod, hav.beginmarketingperioddecade, hav.beginmarketingperiodcampaign, hav.endingmarketingperiod, hav.endingmarketingperioddecade, hav.endingmarketingperiodcampaign, hav.isorganiccrop, hp.price, hp.priceunit
      from harvestingactionvalorisation hav
      inner join harvestingprice hp on hp.harvestingactionvalorisation = hav.topiaId
	  inner join _11924_all_duplicate_valorisations_context dvi on dvi.harvestingaction = hav.harvestingaction
	  where not exists (
	    select 1 from _12064_valid_duplicate_valorisations vdv
	    where vdv.topiaid = hav.topiaid
	  )
);

-- parmi les doublons, on garde s'il existe le rendement > 0, un prix ou le plus récent en priorité
create table _12064_valid_duplicate_valorisation_to_remove as
SELECT * FROM _12064_valid_duplicate_valorisations
WHERE topiaid IN (
  SELECT topiaid FROM (
    SELECT topiaid,
     ROW_NUMBER() OVER( PARTITION BY harvestingaction, speciescode, destination
     ORDER BY yealdaverage desc, price desc, topiaversion, topiacreatedate desc) AS row_num
     FROM _12064_valid_duplicate_valorisations) t
  WHERE t.row_num > 1
);

-- On conserve une copie des critères de qualité à supprimer
create table _12064_removed_qualitycriteria as (
  select * from qualitycriteria q
  where exists ( select 1 from _12064_valid_duplicate_valorisation_to_remove v where v.topiaId = q.harvestingactionvalorisation )
  UNION
  select * from qualitycriteria q
  where exists ( select 1 from _12064_non_valid_duplicate_valorisations v where v.topiaId = q.harvestingactionvalorisation )
);

-- On conserve une copie des prix à supprimer
create table _12064_removed_price as (
  select * from harvestingprice hp
  where exists ( select 1 from _12064_valid_duplicate_valorisation_to_remove v where v.topiaId = hp.harvestingactionvalorisation )
  UNION
  select * from harvestingprice hp
  where exists ( select 1 from _12064_non_valid_duplicate_valorisations v where v.topiaId = hp.harvestingactionvalorisation )
);

delete from qualitycriteria qc where exists (select 1 from _12064_removed_qualitycriteria rqq where rqq.topiaId = qc.topiaId);
delete from harvestingprice hp where exists (select 1 from _12064_removed_price rp where rp.topiaId = hp.topiaId);

delete from harvestingactionvalorisation hav where exists (select 1 from _12064_valid_duplicate_valorisation_to_remove v where v.topiaid = hav.topiaid);
delete from harvestingactionvalorisation hav where exists (select 1 from _12064_non_valid_duplicate_valorisations v where v.topiaid = hav.topiaid);
