---
-- %%Ignore-License
---
insert into trad_ref_vivant values ('fra', 'fr.inra.agrosyst.api.entities.CompagneType.COUVERT_PERMANENT', 'Couvert permanent');
insert into trad_ref_vivant values ('eng', 'fr.inra.agrosyst.api.entities.CompagneType.COUVERT_PERMANENT', 'Continuously covered');
