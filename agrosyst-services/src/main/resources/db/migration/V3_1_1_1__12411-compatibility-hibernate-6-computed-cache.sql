---
-- %%Ignore-License
---

--ALTER TABLE computed_cache RENAME COLUMN "cachekey" TO cache_key;

ALTER TABLE computed_cache RENAME COLUMN "cache_key" TO "cachekey";
