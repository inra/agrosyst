---
-- %%Ignore-License
---

CREATE TEMPORARY TABLE V3_0_4_0__11992_add_missing_valorisation_prices_script_date AS (SELECT CURRENT_TIMESTAMP(3)::timestamp without time zone AS topiacreatedate);

CREATE TEMPORARY TABLE missingpricevalorisation AS (select topiaId
  FROM harvestingactionvalorisation hav
  WHERE NOT EXISTS (SELECT 1 FROM harvestingprice hp WHERE hp.harvestingactionvalorisation = hav.topiaid));

CREATE index IF NOT EXISTS missingpricevalorisation_idx ON missingpricevalorisation(topiaId);

CREATE TEMPORARY TABLE harvesting_price_domain AS (
        SELECT
        hav.topiaid AS harvestingactionvalorisation, gp.domain AS domain, null as zone, ps.topiaid AS practicedsystem
        , CONCAT('fr.inra.agrosyst.api.entities.HarvestingPrice_', uuid_in(md5(CONCAT(hav.topiaid::text, replace(json_build_array(aa.topiaid, hav.speciescode, hav.topiaid)::text,', ',','), now()::text))::cstring))  AS topiaid
        , 0 AS topiaversion
        , replace(json_build_array(re.code_espece_botanique, re.code_qualifiant_aee, re.code_type_saisonnier_aee, re.code_destination_aee, CASE WHEN cps.variety IS NOT NULL THEN (CONCAT((SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety))) ELSE '' END, rd.code_destination_a)::text,', ',',') AS objectid
        , (SELECT topiacreatedate FROM V3_0_4_0__11992_add_missing_valorisation_prices_script_date) AS topiacreatedate
        , hav.yealdunit AS sourceunit
        , CONCAT(
            re.libelle_espece_botanique,
            CASE WHEN re.libelle_qualifiant_AEE IS NOT NULL AND re.libelle_qualifiant_AEE != '' THEN CONCAT(' ', re.libelle_qualifiant_AEE) ELSE NULL END,
            CASE WHEN cps.variety IS NOT NULL THEN (CONCAT(' (', (SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety),')')) ELSE NULL END
          ) AS displayname
        FROM missingpricevalorisation mpv
        INNER JOIN harvestingactionvalorisation hav ON hav.topiaid = mpv.topiaId
        INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        INNER JOIN croppingplanentry cpe on cpe.domain =  gp.domain
        INNER JOIN croppingplanspecies cps ON cps.croppingplanentry = cpe.topiaId AND cps.code = hav.speciescode
        INNER JOIN refespece re ON re.topiaid = cps.species
        INNER JOIN refdestination rd ON hav.destination = rd.topiaid

UNION ALL
    (
        SELECT
        hav.topiaid AS harvestingactionvalorisation, gp.domain AS domain, null as zone, ps.topiaid AS practicedsystem
        , CONCAT('fr.inra.agrosyst.api.entities.HarvestingPrice_', uuid_in(md5(CONCAT(hav.topiaid::text, replace(json_build_array(aa.topiaid, hav.speciescode, hav.topiaid)::text,', ',','), now()::text))::cstring))  AS topiaid
        , 0 AS topiaversion
        , replace(json_build_array(re.code_espece_botanique, re.code_qualifiant_aee, re.code_type_saisonnier_aee, re.code_destination_aee, CASE WHEN cps.variety IS NOT NULL THEN (CONCAT((SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety))) ELSE '' END, rd.code_destination_a)::text,', ',',') AS objectid
        , (SELECT topiacreatedate FROM V3_0_4_0__11992_add_missing_valorisation_prices_script_date) AS topiacreatedate
        , hav.yealdunit AS sourceunit
        , CONCAT(
            re.libelle_espece_botanique,
            CASE WHEN re.libelle_qualifiant_AEE IS NOT NULL AND re.libelle_qualifiant_AEE != '' THEN CONCAT(' ', re.libelle_qualifiant_AEE) ELSE NULL END,
            CASE WHEN cps.variety IS NOT NULL THEN (CONCAT(' (', (SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety),')')) ELSE NULL END
          )  AS displayname
        FROM missingpricevalorisation mpv
        INNER JOIN harvestingactionvalorisation hav ON hav.topiaid = mpv.topiaId
        INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        INNER JOIN croppingplanentry cpe on cpe.domain =  gp.domain
        INNER JOIN croppingplanspecies cps ON cps.croppingplanentry = cpe.topiaId AND cps.code = hav.speciescode
        INNER JOIN refespece re ON re.topiaid = cps.species
        INNER JOIN refdestination rd ON hav.destination = rd.topiaid
    )

UNION ALL
    (
        SELECT
        hav.topiaid AS harvestingactionvalorisation, plot.domain AS domain, zone.topiaId as zone, null AS practicedsystem
        , CONCAT('fr.inra.agrosyst.api.entities.HarvestingPrice_', uuid_in(md5(CONCAT(hav.topiaid::text, replace(json_build_array(aa.topiaid, hav.speciescode, hav.topiaid)::text,', ',','), now()::text))::cstring))  AS topiaid
        , 0 AS topiaversion
        , replace(json_build_array(re.code_espece_botanique, re.code_qualifiant_aee, re.code_type_saisonnier_aee, re.code_destination_aee, CASE WHEN cps.variety IS NOT NULL THEN (CONCAT((SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety))) ELSE '' END, rd.code_destination_a)::text,', ',',') AS objectid
        , (SELECT topiacreatedate FROM V3_0_4_0__11992_add_missing_valorisation_prices_script_date) AS topiacreatedate
        , hav.yealdunit AS sourceunit
        , CONCAT(
            re.libelle_espece_botanique,
            CASE WHEN re.libelle_qualifiant_AEE IS NOT NULL AND re.libelle_qualifiant_AEE != '' THEN CONCAT(' ', re.libelle_qualifiant_AEE) ELSE NULL END,
            CASE WHEN cps.variety IS NOT NULL THEN (CONCAT(' (', (SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety),')')) ELSE NULL END
          )  AS displayname
        FROM missingpricevalorisation mpv
        INNER JOIN harvestingactionvalorisation hav ON hav.topiaid = mpv.topiaId
        INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        INNER JOIN croppingplanentry cpe on cpe.domain =  plot.domain
        INNER JOIN croppingplanspecies cps ON cps.croppingplanentry = cpe.topiaId AND cps.code = hav.speciescode
        INNER JOIN refespece re ON re.topiaid = cps.species
        INNER JOIN refdestination rd ON hav.destination = rd.topiaid
    )
UNION ALL

    (
        SELECT
        hav.topiaid AS harvestingactionvalorisation, plot.domain AS domain, zone.topiaId as zone, null AS practicedsystem
        , CONCAT('fr.inra.agrosyst.api.entities.HarvestingPrice_', uuid_in(md5(CONCAT(hav.topiaid::text, replace(json_build_array(aa.topiaid, hav.speciescode, hav.topiaid)::text,', ',','), now()::text))::cstring))  AS topiaid
        , 0 AS topiaversion
        , replace(json_build_array(re.code_espece_botanique, re.code_qualifiant_aee, re.code_type_saisonnier_aee, re.code_destination_aee, CASE WHEN cps.variety IS NOT NULL THEN (CONCAT((SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety))) ELSE '' END, rd.code_destination_a)::text,', ',',') AS objectid
        , (SELECT topiacreatedate FROM V3_0_4_0__11992_add_missing_valorisation_prices_script_date) AS topiacreatedate
        , hav.yealdunit AS sourceunit
        , CONCAT(
            re.libelle_espece_botanique,
            CASE WHEN re.libelle_qualifiant_AEE IS NOT NULL AND re.libelle_qualifiant_AEE != '' THEN CONCAT(' ', re.libelle_qualifiant_AEE) ELSE NULL END,
            CASE WHEN cps.variety IS NOT NULL THEN (CONCAT(' (', (SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety),')')) ELSE NULL END
          )  AS displayname
        FROM missingpricevalorisation mpv
        INNER JOIN harvestingactionvalorisation hav ON hav.topiaid = mpv.topiaId
        INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        INNER JOIN croppingplanentry cpe on cpe.domain =  plot.domain
        INNER JOIN croppingplanspecies cps ON cps.croppingplanentry = cpe.topiaId AND cps.code = hav.speciescode
        INNER JOIN refespece re ON re.topiaid = cps.species
        INNER JOIN refdestination rd ON hav.destination = rd.topiaid
    )
);

INSERT into harvesting_price_domain (harvestingactionvalorisation, domain, zone, practicedsystem, topiaid, topiaversion, objectid, topiacreatedate, sourceunit, displayname) (
        SELECT
        hav.topiaid AS harvestingactionvalorisation, gp.domain AS domain, null as zone, ps.topiaid AS practicedsystem
        , CONCAT('fr.inra.agrosyst.api.entities.HarvestingPrice_', uuid_in(md5(CONCAT(hav.topiaid::text, replace(json_build_array(aa.topiaid, hav.speciescode, hav.topiaid)::text,', ',','), now()::text))::cstring))  AS topiaid
        , 0  AS sourceunit
        , (SELECT replace(json_build_array(re.code_espece_botanique, re.code_qualifiant_aee, re.code_type_saisonnier_aee, re.code_destination_aee, CASE WHEN cps.variety IS NOT NULL THEN (CONCAT((SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety))) ELSE '' END, rd.code_destination_a)::text,', ',',') FROM  croppingplanspecies cps INNER JOIN refespece re ON re.topiaid = cps.species INNER JOIN refdestination rd ON hav.destination = rd.topiaid WHERE cps.code = hav.speciescode  LIMIT 1) AS objectid
        , (SELECT topiacreatedate FROM V3_0_4_0__11992_add_missing_valorisation_prices_script_date) AS topiacreatedate
        , hav.yealdunit AS sourceunit
        , (SELECT CONCAT(
            re.libelle_espece_botanique,
            CASE WHEN re.libelle_qualifiant_AEE IS NOT NULL AND re.libelle_qualifiant_AEE != '' THEN CONCAT(' ', re.libelle_qualifiant_AEE) ELSE NULL END,
            CASE WHEN cps.variety IS NOT NULL THEN (CONCAT(' (', (SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety),')')) ELSE NULL END
          )  FROM  croppingplanspecies cps INNER JOIN refespece re ON re.topiaid = cps.species INNER JOIN refdestination rd ON hav.destination = rd.topiaid WHERE cps.code = hav.speciescode  LIMIT 1) AS displayname
        FROM missingpricevalorisation mpv
        INNER JOIN harvestingactionvalorisation hav ON hav.topiaid = mpv.topiaId
        INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        WHERE NOT EXISTS (select 1 from harvesting_price_domain hpd WHERE hpd.harvestingactionvalorisation = mpv.topiaid)

UNION ALL
    (
        SELECT
        hav.topiaid AS harvestingactionvalorisation, gp.domain AS domain, null as zone, ps.topiaid AS practicedsystem
        , CONCAT('fr.inra.agrosyst.api.entities.HarvestingPrice_', uuid_in(md5(CONCAT(hav.topiaid::text, replace(json_build_array(aa.topiaid, hav.speciescode, hav.topiaid)::text,', ',','), now()::text))::cstring))  AS topiaid
        , 0  AS sourceunit
        , (SELECT replace(json_build_array(re.code_espece_botanique, re.code_qualifiant_aee, re.code_type_saisonnier_aee, re.code_destination_aee, CASE WHEN cps.variety IS NOT NULL THEN (CONCAT((SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety))) ELSE '' END, rd.code_destination_a)::text,', ',',') FROM  croppingplanspecies cps INNER JOIN refespece re ON re.topiaid = cps.species INNER JOIN refdestination rd ON hav.destination = rd.topiaid WHERE cps.code = hav.speciescode  LIMIT 1) AS objectid
        , (SELECT topiacreatedate FROM V3_0_4_0__11992_add_missing_valorisation_prices_script_date) AS topiacreatedate
        , hav.yealdunit AS sourceunit
        , (SELECT CONCAT(
            re.libelle_espece_botanique,
            CASE WHEN re.libelle_qualifiant_AEE IS NOT NULL AND re.libelle_qualifiant_AEE != '' THEN CONCAT(' ', re.libelle_qualifiant_AEE) ELSE NULL END,
            CASE WHEN cps.variety IS NOT NULL THEN (CONCAT(' (', (SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety),')')) ELSE NULL END
          )  FROM  croppingplanspecies cps INNER JOIN refespece re ON re.topiaid = cps.species INNER JOIN refdestination rd ON hav.destination = rd.topiaid WHERE cps.code = hav.speciescode  LIMIT 1) AS displayname
        FROM missingpricevalorisation mpv
        INNER JOIN harvestingactionvalorisation hav ON hav.topiaid = mpv.topiaId
        INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        WHERE NOT EXISTS (select 1 from harvesting_price_domain hpd WHERE hpd.harvestingactionvalorisation = mpv.topiaid)
    )

);

INSERT into harvestingprice (harvestingactionvalorisation, domain, zone, practicedsystem, topiaid, topiaversion, objectid, topiacreatedate, sourceunit, displayname) (
        SELECT harvestingactionvalorisation, domain, zone, practicedsystem, topiaid, topiaversion, objectid, topiacreatedate, sourceunit, displayname FROM harvesting_price_domain
);
