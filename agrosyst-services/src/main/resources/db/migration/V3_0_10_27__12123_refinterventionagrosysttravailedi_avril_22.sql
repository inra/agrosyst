---
-- %%Ignore-License
---

CREATE TABLE _12123_refinterventionagrosysttravailedi_avril_22 as (SELECT * FROM refinterventionagrosysttravailedi) WITH NO DATA;
CREATE INDEX IF NOT EXISTS _12123_refinterventionagrosysttravailedi_avril_22_idx ON _12123_refinterventionagrosysttravailedi_avril_22(topiaid);

INSERT INTO _12123_refinterventionagrosysttravailedi_avril_22 (topiaid,topiaversion,reference_code,topiacreatedate,reference_label,"source",active,intervention_agrosyst) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_d8979560-9562-48dc-b2ed-db06dd17f489',0,'SEM','2014-01-31 00:43:13.276','Fertilisation minérale','AgroEDI 2013',true,'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_ae8128a5-1c49-4d35-a6bf-19f7d164ba2c',0,'SEK','2014-01-31 00:43:13.279','Epandage organique','AgroEDI 2013',true,'EPANDAGES_ORGANIQUES'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_ef253286-2540-4eb5-8f41-c462ac7934b1',0,'SEO','2014-01-31 00:43:13.279','Irrigation','AgroEDI 2013',true,'IRRIGATION'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_ed8dd0cd-b462-4549-b9e3-d97347ad11ba',0,'SEA','2014-01-31 00:43:13.279','Arrachage','AgroEDI 2013',true,'RECOLTE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_b77312ee-01be-4c6a-91eb-55755b337df5',0,'SEJ','2014-01-31 00:43:13.279','Ensilage','AgroEDI 2013',true,'RECOLTE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_38164526-9a5f-4b9a-a81f-b0fa94a8972d',0,'SEQ','2014-01-31 00:43:13.279','Moisson','AgroEDI 2013',true,'RECOLTE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_6667667b-05ef-4f8b-9179-1f9fe0a4fd97',0,'SER','2014-01-31 00:43:13.279','Récolte','AgroEDI 2013',true,'RECOLTE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_70533ea4-3597-4b67-940c-d40f65576a3a',0,'W19','2014-01-31 00:43:13.279','Vendange','AgroEDI 2013',true,'RECOLTE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_591194fc-0349-4ef8-953b-3e123efb6f66',0,'SFE','2014-01-31 00:43:13.279','Pressage','AgroEDI 2013',true,'RECOLTE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_fc50ea32-a20d-4655-b596-23da9b127a59',0,'SET','2014-01-31 00:43:13.279','Semis classique','AgroEDI 2013',true,'SEMIS');
INSERT INTO _12123_refinterventionagrosysttravailedi_avril_22 (topiaid,topiaversion,reference_code,topiacreatedate,reference_label,"source",active,intervention_agrosyst) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_7b313056-05ea-47b6-8511-9a027c62209b',0,'ZKE','2014-01-31 00:43:13.279','Semis de précision','AgroEDI 2013',true,'SEMIS'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_730f8ae0-0a50-471d-98dd-526e61fe5158',0,'SEU','2014-01-31 00:43:13.279','Semis direct','AgroEDI 2013',true,'SEMIS'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_dbe35202-a88b-4be5-9ac3-443eb5f574cf',0,'SEW','2014-01-31 00:43:13.279','Plantation','AgroEDI 2013',true,'SEMIS'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_f78be613-9415-45d1-a05b-bb37318bf46a',0,'V92','2014-01-31 00:43:13.279','Complantation/recépage/marcotage','AgroEDI 2013',true,'SEMIS'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_61ae05c5-1c44-4b0a-9904-7c74d6b94dd2',0,'V87','2014-01-31 00:43:13.279','Surgreffage','AgroEDI 2013',true,'SEMIS'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_38b94e00-2121-41da-8411-5b7cd3d9fc06',0,'V96','2014-01-31 00:43:13.28','Travail de surface (binage, hersage, griffage, ...)','AgroEDI 2013',true,'TRAVAIL_DU_SOL'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_ae0c022e-b400-4cbb-9e18-11e99bda6048',0,'SFG','2014-01-31 00:43:13.28','Billonnage','AgroEDI 2013',true,'TRAVAIL_DU_SOL'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_412dd059-b819-496d-b0b5-6054b9ffaeae',0,'SEB','2014-01-31 00:43:13.28','Binage','AgroEDI 2013',true,'TRAVAIL_DU_SOL'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_dd48d8eb-df0c-4263-b820-e81ea9f159b6',0,'SED','2014-01-31 00:43:13.28','Buttage','AgroEDI 2013',true,'TRAVAIL_DU_SOL'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_97aa99f5-ef44-4388-ac5c-34ce2f5036bf',0,'V94','2014-01-31 00:43:13.28','Cavaillonage','AgroEDI 2013',true,'TRAVAIL_DU_SOL');
INSERT INTO _12123_refinterventionagrosysttravailedi_avril_22 (topiaid,topiaversion,reference_code,topiacreatedate,reference_label,"source",active,intervention_agrosyst) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_32ea0a58-addf-4cb1-b244-a3f49ac12a78',0,'V93','2014-01-31 00:43:13.28','Décavaillonage','AgroEDI 2013',true,'TRAVAIL_DU_SOL'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_c83a41c6-8384-4cdf-8bb9-6551be0b0682',0,'SEE','2014-01-31 00:43:13.28','Déchaumage','AgroEDI 2013',true,'TRAVAIL_DU_SOL'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_b67dfd92-1f17-464d-8cdc-81ca4f4cee61',0,'SEF','2014-01-31 00:43:13.28','Décompactage','AgroEDI 2013',true,'TRAVAIL_DU_SOL'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_85c08ee5-003a-414f-a2b2-e8a30199f00e',0,'SEN','2014-01-31 00:43:13.28','Hersage','AgroEDI 2013',true,'TRAVAIL_DU_SOL'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_e5d81f08-b97b-4704-b50f-f8ae3d467ce8',0,'SEP','2014-01-31 00:43:13.28','Labour','AgroEDI 2013',true,'TRAVAIL_DU_SOL'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_f9962102-1e2f-4226-8229-25abcdc1daa1',0,'SEV','2014-01-31 00:43:13.28','Sous solage','AgroEDI 2013',true,'TRAVAIL_DU_SOL'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_9fc8f0ad-0a5d-4019-89c6-7496fbbaf85f',0,'W03','2014-01-31 00:43:13.28','Broyage des bois de taille','AgroEDI 2013',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_d1b694d4-b162-4c33-8bf1-3d812cf05a16',0,'W10','2014-01-31 00:43:13.28','Ebourgeonnage','AgroEDI 2013',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_d579c19f-b879-4994-a1d9-9f03774d3f5d',0,'W12','2014-01-31 00:43:13.28','Eclaircissage','AgroEDI 2013',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_c88798fd-e200-460e-8ca7-429503fba50d',0,'W11','2014-01-31 00:43:13.28','Effeuillage','AgroEDI 2013',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER');
INSERT INTO _12123_refinterventionagrosysttravailedi_avril_22 (topiaid,topiaversion,reference_code,topiacreatedate,reference_label,"source",active,intervention_agrosyst) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_9accf356-c766-4f5c-9031-26ede3e4b6b8',0,'W08','2014-01-31 00:43:13.28','Egourmandage (=epamprage)','AgroEDI 2013',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_8730474d-58d7-421e-ac84-dac31784b5dd',0,'W05','2014-01-31 00:43:13.28','Liage/Attachage/Pliage','AgroEDI 2013',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_1b26fed6-02fd-40ec-90b4-a914a7535a39',0,'V97','2014-01-31 00:43:13.28','Palissage (mise en place et entretien)','AgroEDI 2013',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_57307fa1-809e-4f21-9662-758c78001e2d',0,'W06','2014-01-31 00:43:13.28','Pliage (à la taille)','AgroEDI 2013',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_ed5dd1d7-6360-4703-8cb7-ead38e9326de',0,'W02','2014-01-31 00:43:13.28','Pré -taille','AgroEDI 2013',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_8b6adfad-c534-40bf-a9ef-bf5b72d3dccd',0,'W07','2014-01-31 00:43:13.28','Relevage, accolage','AgroEDI 2013',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_6dd9462d-c2a0-484a-8792-bf2f6a54ed56',0,'W09','2014-01-31 00:43:13.28','Rognage / Ecimage','AgroEDI 2013',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_9636294f-9025-4542-8443-0ccd3d00c101',0,'W01','2014-01-31 00:43:13.28','Taille (badigeonnage compris)','AgroEDI 2013',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_e18654e3-7fc6-4005-ac87-8ab48de50bdb',0,'W14','2014-01-31 00:43:13.281','Taille en vert','AgroEDI 2013',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_e7987a56-8ad1-46d0-9b80-9c805e1e1dcc',0,'W13','2014-01-31 00:43:13.281','Vendange en vert (grapillonnage, ...)','AgroEDI 2013',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER');
INSERT INTO _12123_refinterventionagrosysttravailedi_avril_22 (topiaid,topiaversion,reference_code,topiacreatedate,reference_label,"source",active,intervention_agrosyst) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_667a3b77-bc52-4cff-9c54-3956596a386e',0,'V85','2014-01-31 00:43:13.281','Aménagement foncier','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_bfd3642c-c323-4364-8a8e-ad727c923a48',0,'SEH','2014-01-31 00:43:13.281','Andainage','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_932eae90-b50b-473c-9827-7fd313c03505',0,'ZTC','2014-01-31 00:43:13.281','Autre type de travail','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_121788f3-cb12-4d34-bdae-1353b982ab67',0,'SFI','2014-01-31 00:43:13.281','Bâchage','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_16ea2fa3-7456-4af9-af67-a26156974202',0,'SEC','2014-01-31 00:43:13.281','Broyage cailloux','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_00686f82-1468-45bd-a733-6f9bfd4ea1c7',0,'SFK','2014-01-31 00:43:13.281','Broyage des résidus','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_e1c9a926-c97f-4ed4-8114-046f3322a4ef',0,'V99','2014-01-31 00:43:13.281','Brulage enherbement (estivage, ..)','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_eeba1f0d-6a98-4ff0-9d28-856ffc85b211',0,'SFC','2014-01-31 00:43:13.281','Castration','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_5b4b6029-e828-4b93-a40a-94306c67cabb',0,'W20','2014-01-31 00:43:13.281','Contrôle maturité','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_2a1f5f40-d3bb-4852-b911-cb916d00408c',0,'SEG','2014-01-31 00:43:13.281','Défanage','AgroEDI 2013',true,'AUTRE');
INSERT INTO _12123_refinterventionagrosysttravailedi_avril_22 (topiaid,topiaversion,reference_code,topiacreatedate,reference_label,"source",active,intervention_agrosyst) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_4a8a2fa3-b0fc-4ca3-aa10-8598fbe42197',0,'V86','2014-01-31 00:43:13.281','Désinfection','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_782059d8-ac9e-4ea7-8ce5-13e037f5f5e3',0,'ZTA','2014-01-31 00:43:13.281','Destruction des mâles (maïs)','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_39d8873b-c214-4218-bd18-c76689710f0d',0,'V89','2014-01-31 00:43:13.281','Dévitalisation','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_bd1f39af-afc1-476f-96f0-0f14fbe9460d',0,'SFA','2014-01-31 00:43:13.281','Epuration','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_3d274077-53a8-4f27-a7a0-cc6f4a1a5829',0,'SEL','2014-01-31 00:43:13.281','Fauchage','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_3b502d1a-4081-43da-8bce-76b78d8bdc25',0,'ZTB','2014-01-31 00:43:13.281','Fermeture du silo','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_6488414a-6f2b-40ad-87d7-c8f3cdc10243',0,'V90','2014-01-31 00:43:13.281','Installation de protection','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_bc3dcc76-561a-4a68-9b94-428380fe7064',0,'W18','2014-01-31 00:43:13.281','Protection climatique','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_986abcd4-0621-4f05-94a6-3addbb6fac30',0,'V98','2014-01-31 00:43:13.281','Tonte','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_c7f47f48-db22-4e88-a456-bc236dca886a',0,'SFD','2014-01-31 00:43:13.281','Triage au champ','AgroEDI 2013',true,'AUTRE');
INSERT INTO _12123_refinterventionagrosysttravailedi_avril_22 (topiaid,topiaversion,reference_code,topiacreatedate,reference_label,"source",active,intervention_agrosyst) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_bd3d2205-27d8-49a4-b57c-89a0c59e4f66',0,'SFB','2014-01-31 00:43:13.281','Paillage','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_baafc371-204e-4c2c-9e2f-f7b23c5f5fbe',0,'V88','2014-01-31 00:43:13.281','Piquetage/Marquants','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_2aabf037-e70a-473c-a54b-915cfd6af2e9',0,'W15','2014-01-31 00:43:13.281','Rajout de terre','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_7ff406a4-10f4-4bf7-a395-af15b6240c03',0,'SES','2014-01-31 00:43:13.281','Roulage','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_7e64df21-c10b-4b1a-9140-267844cce7c5',0,'SFH','2014-01-31 00:43:13.281','Solarisation','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_5539614d-5962-410b-86ef-9a50e2d59b72',0,'SFJ','2014-01-31 00:43:13.281','Tamisage et/ou Epierrage','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_942e5edd-6fe1-446e-a0a4-10fbe6cdf3ef',0,'W04','2014-01-31 00:43:13.281','Tirage et/ou Brûlage des bois','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_214df347-1a35-4fe7-911d-c4298458a1fb',0,'W22','2014-01-31 00:43:13.281','Autres observations','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_11f21ea6-c267-4ebc-b851-33fd19a2d881',0,'SEI','2014-01-31 00:43:13.281','Enfouissement','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_748185e2-9e65-4d43-894a-9d81439e6597',0,'V91','2014-01-31 00:43:13.281','Observation carences','AgroEDI 2013',true,'AUTRE');
INSERT INTO _12123_refinterventionagrosysttravailedi_avril_22 (topiaid,topiaversion,reference_code,topiacreatedate,reference_label,"source",active,intervention_agrosyst) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_260a2d25-0d5f-4b45-b14c-d411c147d433',0,'W21','2014-01-31 00:43:13.281','Observation ravageurs et maladies','AgroEDI 2013',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_68cabecf-363c-4c31-a2d0-afbf1d611bd1',0,'W23','2014-11-12 17:32:52.493','Fanage','AgroEDI année ?',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_e295fe47-5271-4b81-93dd-8843112654ec',0,'W26','2014-11-12 17:32:52.505','Brûlage des résidus','AgroEDI 2014',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_6682b3ca-e298-4cef-853e-63f1ea03b81d',0,'W24','2014-01-31 00:43:13.281','Transport','AgroEDI 2014',true,'TRANSPORT'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_d2e017db-4db4-4c6b-b75d-9a30e5565878',0,'W25','2014-01-31 00:43:13.279','Pâturage','AgroEDI 2014',true,'RECOLTE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_7cc503e1-eea9-4f2e-88d9-543b3ef2551c',0,'AAT00','2016-11-30 14:45:39.678','Bouturage','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_77c72a79-538a-48c5-8afe-ce20e2b02981',0,'V95','2016-11-30 14:45:39.685','Désherbage','AgroEDI 2013',true,'TRAVAIL_DU_SOL'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_21ecc4a9-b142-49d1-925e-39bfded00276',0,'AAT01','2016-11-30 14:45:39.685','Distançage','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_324fc18b-81d6-4fff-ad2d-55af9459889a',0,'AAT02','2016-11-30 14:45:39.685','Eboutonnage','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_a1dea302-6cb6-48b9-95f8-efad41d469f5',0,'AAT03','2016-11-30 14:45:39.685','Effleurage','AgroEDI 2016',true,'AUTRE');
INSERT INTO _12123_refinterventionagrosysttravailedi_avril_22 (topiaid,topiaversion,reference_code,topiacreatedate,reference_label,"source",active,intervention_agrosyst) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_f0389c09-7c9d-4b18-b815-2ba226d9ade7',0,'AAT04','2016-11-30 14:45:39.685','Empotage','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_bbed1d71-15e6-4302-869a-0a9685f2512f',0,'W27','2016-11-30 14:45:39.685','Engaignage','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_f590b50a-5e8d-4a04-8fb5-5ac42f9cafc4',0,'AAT05','2016-11-30 14:45:39.685','Entretien de nichoirs / gîtes','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_0b850b79-c817-4f03-addf-8a1ad71e119f',0,'AAT06','2016-11-30 14:45:39.685','Entretien des aménagements paysagers','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_06bf04ec-9120-42a6-9c5c-0e9433f2651f',0,'AAT07','2016-11-30 14:45:39.685','Fauche des drageons','AgroEDI 2016',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_bf9773f3-ac9f-4bca-a960-3002575032ad',0,'AAT08','2016-11-30 14:45:39.686','Gestion d''animaux pour la gestion des bioagresseurs','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_ed30215f-797c-4d7b-be45-ff69ca9ca068',0,'AAT09','2016-11-30 14:45:39.686','Greffage','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_826ce648-5d31-49a3-9c34-33a68223c034',0,'W28','2016-11-30 14:45:39.686','Haubanage','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_203b3a9d-cb2f-40a1-8f72-028e5cd175c5',0,'AAT10','2016-11-30 14:45:39.686','Manipulation de protection anti-grêle','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_b0a99e79-cb83-462e-ad33-22dd536131b8',0,'AAT11','2016-11-30 14:45:39.686','Manipulation de protection anti-insecte mono-parcelle','AgroEDI 2016',true,'AUTRE');
INSERT INTO _12123_refinterventionagrosysttravailedi_avril_22 (topiaid,topiaversion,reference_code,topiacreatedate,reference_label,"source",active,intervention_agrosyst) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_ddee82d9-a0e5-4c60-94fc-c2d75dbf3c4c',0,'AAT12','2016-11-30 14:45:39.686','Manipulation de protection anti-insecte mono-rang','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_0673b4b1-5eca-4bc0-8288-329f9ae15b0b',0,'AAT13','2016-11-30 14:45:39.687','Manipulation de protection anti-pluie','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_d0a261b7-5581-461d-9a24-4974b4be129e',0,'AAT14','2016-11-30 14:45:39.687','Nettoyage de plantes','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_800eb449-574e-47e2-b2f3-73a8552087ae',0,'AAT15','2016-11-30 14:45:39.688','Pincement','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_cd43292d-55fc-4118-b187-7fa15c26f6aa',0,'AAT16','2016-11-30 14:45:39.688','Préparation d''extraits végétaux','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_bd0bdf1d-7502-405e-b46c-2bb163708fbe',0,'AAT17','2016-11-30 14:45:39.688','Relevé de piègeage massif','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_7008bf37-fe45-402d-8032-089419bda10e',0,'AAT18','2016-11-30 14:45:39.688','Relevé de pièges campagnols','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_847c3792-fc1f-49c4-a0d1-61b2608168f7',0,'AAT19','2016-11-30 14:45:39.688','Relevé de pièges monitoring','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_002127b7-3367-4474-ac53-f77cd1c66b3f',0,'AAT21','2016-11-30 14:45:39.688','Repiquage','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_1c29728c-f050-4110-abaa-50783624801b',0,'AAT22','2016-11-30 14:45:39.688','Suppression des organes contaminés','AgroEDI 2016',true,'AUTRE');
INSERT INTO _12123_refinterventionagrosysttravailedi_avril_22 (topiaid,topiaversion,reference_code,topiacreatedate,reference_label,"source",active,intervention_agrosyst) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_2ef879e6-3245-4cc9-a718-20755c12cfd7',0,'AAT23','2016-11-30 14:45:39.688','Surfaçage','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_ce47d3b8-2226-49d6-95fd-27665411c89e',0,'AAT24','2016-11-30 14:45:39.688','Taille des racines','AgroEDI 2016',true,'ENTRETIEN_TAILLE_VIGNE_ET_VERGER'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_297e2a53-7c1d-4ad4-baa3-660be22746de',0,'AAT25','2016-11-30 14:45:39.688','Tuteurage','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_b6018a24-731c-426c-a600-da503a253117',0,'AAT26','2016-11-30 14:45:39.688','Utilisation d''OAD','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_d392fa95-05ea-41f9-9758-c621e8434194',0,'AAT20','2016-11-30 14:45:39.688','Rempotage','AgroEDI 2016',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_5398b3ac-8871-4806-9c36-66e92d9c2cba',0,'SEX','2014-01-31 00:43:13.28','Application de produits avec AMM','AgroEDI 2013',true,'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_b7caa7f6-ea6c-421b-9658-5a559c655d22',0,'W16','2014-01-31 00:43:13.28','Application de produits sans AMM','AgroEDI 2013',true,'LUTTE_BIOLOGIQUE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_c44b598a-c2b8-4612-b3fa-4616eb1d82ea',0,'AAT27','2021-02-22 13:53:20.007','Substrat - Mise en place     ','AgroEDI 2020',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_4424c165-57f0-42c5-8b69-95dfc487b4da',0,'AAT28','2021-02-22 13:53:20.008','Substrat - Retrait ','AgroEDI 2020',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_a270674e-2240-42be-a26d-68a5adefa992',0,'AAT29','2021-02-22 13:53:20.008','Substrat - Fumigation ','AgroEDI 2020',true,'AUTRE');
INSERT INTO _12123_refinterventionagrosysttravailedi_avril_22 (topiaid,topiaversion,reference_code,topiacreatedate,reference_label,"source",active,intervention_agrosyst) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_10262adb-395b-4027-b5c4-89528bcf513f',0,'AAT30','2021-02-22 13:53:20.008','Substrat - Chaulage  ','AgroEDI 2020',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_4258b898-d350-47dd-90fe-da468c3ff214',0,'AAT31','2021-02-22 13:53:20.008','Substrat - Broyage ','AgroEDI 2020',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_843772c0-621e-4eac-9827-c11ace3033bc',0,'AAT32','2021-02-22 13:53:20.008','Substrat – Calibrage','AgroEDI 2020',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_cac1f87a-a6d1-4752-b13a-ae9fd10184da',0,'AAT33','2021-02-22 13:53:20.008','Substrat – Compostage','AgroEDI 2020',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_9022f827-54d6-4790-893d-0b881d7595a5',0,'AAT34','2021-02-22 13:53:20.008','Substrat – Trempage  ','AgroEDI 2020',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_0bf46471-01db-4deb-b231-5384c67fbcc6',0,'AAT35','2021-02-22 13:53:20.008','Substrat - Fertilisation','AgroEDI 2020',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_56633caf-cccd-4078-bde9-93ff6ab5ab29',0,'AAT37','2021-03-11 17:05:42.758','Réalisation de tuteur','AgroEDI 2021',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_04f39a57-8b47-47db-966f-96659b28dfdd',0,'AAT38','2021-03-11 17:05:42.758','Comptage','AgroEDI 2021',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_348d98fc-a62b-4c0d-a124-70d9c6450922',0,'AAT39','2021-03-11 17:05:42.758','Destruction plantes de services','AgroEDI 2021',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_1d0cfad8-a79c-448c-b7de-01f8a3f47a47',0,'AAT40','2021-03-11 17:05:42.758','Epaillage','AgroEDI 2021',true,'AUTRE');
INSERT INTO _12123_refinterventionagrosysttravailedi_avril_22 (topiaid,topiaversion,reference_code,topiacreatedate,reference_label,"source",active,intervention_agrosyst) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_04e9c4f1-d7b8-4177-b480-4c7a7b7a25a5',0,'AAT41','2021-03-11 17:05:42.758','Cyclonnage','AgroEDI 2021',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_89dee4a1-dac1-4e7d-a691-d4a180b51e4f',0,'AAT42','2021-03-11 17:05:42.758','Gougeage','AgroEDI 2021',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_48f08cb3-a773-4708-ba14-174d226484fe',0,'AAT43','2021-03-11 17:05:42.758','Sarclage','AgroEDI 2021',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_963ad686-f47d-4832-b557-11ecab6b42f5',0,'AAT44','2021-03-11 17:05:42.758','Oeilletonnage','AgroEDI 2021',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_603b5a0a-d4fb-4fdf-bef4-b80944cbba61',0,'AAT45','2021-03-11 17:05:42.758','Prélèvement de rejets','AgroEDI 2021',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_958bb30d-ca58-4b0d-a771-3b933c945734',0,'AAT46','2021-03-11 17:05:42.758','Trouaison','AgroEDI 2021',true,'AUTRE'),
	 ('fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI_a248d1be-ee56-41e5-9f84-60dc100b1af6',0,'AAT47','2021-03-11 17:05:42.758','Chargement','AgroEDI 2021',true,'AUTRE');
