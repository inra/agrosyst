---
-- %%Ignore-License
---

ALTER TABLE synthetise_echelle_intervention ADD COLUMN consommation_eau float8 NULL;
ALTER TABLE synthetise_echelle_intervention ADD COLUMN consommation_eau_taux_de_completion float8 NULL;
ALTER TABLE synthetise_echelle_intervention ADD COLUMN consommation_eau_detail_champs_non_renseig text NULL;

ALTER TABLE synthetise_echelle_culture ADD COLUMN consommation_eau float8 NULL;
ALTER TABLE synthetise_echelle_culture ADD COLUMN consommation_eau_taux_de_completion float8 NULL;
ALTER TABLE synthetise_echelle_culture ADD COLUMN consommation_eau_detail_champs_non_renseig text NULL;

ALTER TABLE synthetise_echelle_synthetise ADD COLUMN consommation_eau float8 NULL;
ALTER TABLE synthetise_echelle_synthetise ADD COLUMN consommation_eau_taux_de_completion float8 NULL;
ALTER TABLE synthetise_echelle_synthetise ADD COLUMN consommation_eau_detail_champs_non_renseig text NULL;

ALTER TABLE realise_echelle_intervention ADD COLUMN consommation_eau float8 NULL;
ALTER TABLE realise_echelle_intervention ADD COLUMN consommation_eau_taux_de_completion float8 NULL;
ALTER TABLE realise_echelle_intervention ADD COLUMN consommation_eau_detail_champs_non_renseig text NULL;

ALTER TABLE realise_echelle_culture ADD COLUMN consommation_eau float8 NULL;
ALTER TABLE realise_echelle_culture ADD COLUMN consommation_eau_taux_de_completion float8 NULL;
ALTER TABLE realise_echelle_culture ADD COLUMN consommation_eau_detail_champs_non_renseig text NULL;

ALTER TABLE realise_echelle_itk ADD COLUMN consommation_eau float8 NULL;
ALTER TABLE realise_echelle_itk ADD COLUMN consommation_eau_taux_de_completion float8 NULL;
ALTER TABLE realise_echelle_itk ADD COLUMN consommation_eau_detail_champs_non_renseig text NULL;

ALTER TABLE realise_echelle_parcelle ADD COLUMN consommation_eau float8 NULL;
ALTER TABLE realise_echelle_parcelle ADD COLUMN consommation_eau_taux_de_completion float8 NULL;
ALTER TABLE realise_echelle_parcelle ADD COLUMN consommation_eau_detail_champs_non_renseig text NULL;

ALTER TABLE realise_echelle_sdc ADD COLUMN consommation_eau float8 NULL;
ALTER TABLE realise_echelle_sdc ADD COLUMN consommation_eau_taux_de_completion float8 NULL;
ALTER TABLE realise_echelle_sdc ADD COLUMN consommation_eau_detail_champs_non_renseig text NULL;

ALTER TABLE realise_echelle_zone ADD COLUMN consommation_eau float8 NULL;
ALTER TABLE realise_echelle_zone ADD COLUMN consommation_eau_taux_de_completion float8 NULL;
ALTER TABLE realise_echelle_zone ADD COLUMN consommation_eau_detail_champs_non_renseig text NULL;
