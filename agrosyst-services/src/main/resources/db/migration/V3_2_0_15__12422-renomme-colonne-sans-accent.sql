---
-- %%Ignore-License
---

alter table echelle_intrant rename column qsa_smétolachlore_hts to qsa_smetolachlore_hts;
alter table realise_echelle_zone rename column qsa_smétolachlore_hts to qsa_smetolachlore_hts;
alter table realise_echelle_sdc rename column qsa_smétolachlore_hts to qsa_smetolachlore_hts;
alter table realise_echelle_parcelle rename column qsa_smétolachlore_hts to qsa_smetolachlore_hts;
alter table realise_echelle_itk rename column qsa_smétolachlore_hts to qsa_smetolachlore_hts;
alter table realise_echelle_culture rename column qsa_smétolachlore_hts to qsa_smetolachlore_hts;
alter table realise_echelle_intervention rename column qsa_smétolachlore_hts to qsa_smetolachlore_hts;
alter table synthetise_echelle_synthetise rename column qsa_smétolachlore_hts to qsa_smetolachlore_hts;
alter table synthetise_echelle_culture rename column qsa_smétolachlore_hts to qsa_smetolachlore_hts;
alter table synthetise_echelle_intervention rename column qsa_smétolachlore_hts to qsa_smetolachlore_hts;
