---
-- %%Ignore-License
---

CREATE TABLE refconversionunitesqsa (
  topiaid VARCHAR(255) NOT NULL,
  topiaversion INT8 NOT NULL,
  topiacreatedate TIMESTAMP NULL,
  unite_sa TEXT NOT NULL,
  unite_saisie TEXT NOT NULL,
  qsa_kg_ha float8 NOT NULL,
  remarque TEXT NOT NULL,
  active BOOL NOT NULL,

  CONSTRAINT refconversionunitesqsa_pkey PRIMARY KEY (topiaid),
  CONSTRAINT uk_refconversionunitesqsa UNIQUE (unite_sa, unite_saisie)
);
