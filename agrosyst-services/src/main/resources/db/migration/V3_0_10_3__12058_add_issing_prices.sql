---
-- %%Ignore-License
---

CREATE TABLE _12058_script_date AS (SELECT CURRENT_TIMESTAMP(3)::timestamp without time zone AS topiacreatedate);

create table _12058_effective_phyto_missing_prices as
  SELECT concat('fr.inra.agrosyst.api.entities.InputPrice_', split_part(adis."topiaid", '_', 2)) as topiaid,
         concat('fr.inra.agrosyst.api.entities.InputPrice_', uuid_in(md5(random()::text || random()::text)::cstring)) as unique_topiaid,
         adis.topiaid as input_id,
         p.topiaid as price_id,
         p.topiaversion as topiaversion,
         'fr.inra.agrosyst.api.entities.InputPriceImpl' as topiadiscriminator,
         p.objectid as objectid,
         p.sourceunit as sourceunit,
         'PHYTO_TRAITMENT_INPUT' as category,
         adis."domain" as domain,
         (SELECT topiacreatedate from  _12058_script_date) as topiacreatedate,
         p.displayname as displayname,
         p.price as price,
         p.priceunit as priceunit,
         dppi.producttype as phytoproducttype
  FROM abstractdomaininputstockunit adis
  inner join domainphytoproductinput dppi on dppi.topiaid = adis.topiaid
  inner join refotherinput ri on ri.topiacreatedate = adis.topiacreatedate and ri.topiaid = 'fr.inra.agrosyst.api.entities.referential.RefOtherInput_0000000-0000-0000-0000-000000000000'
  inner join price p on p."domain" = adis."domain" and p.objectid = concat(split_part(adis."key", ';', 1),'_', split_part(adis."key", ';', 2))
  where adis.inputtype = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
  and adis.inputprice is null
  and p.price is not null
  and p.category = 'PHYTO_TRAITMENT_INPUT_CATEGORIE'
  and p.price != 0
  and p.priceunit is not null;

CREATE INDEX _12058_effective_phyto_missing_prices_Idx ON _12058_effective_phyto_missing_prices (input_id);

delete FROM _12058_effective_phyto_missing_prices
  WHERE unique_topiaid IN
      (SELECT unique_topiaid
       FROM
          (SELECT unique_topiaid,
           ROW_NUMBER() OVER( PARTITION BY topiaid
          ORDER BY coalesce (sourceunit, 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ'), price) AS row_num
          FROM _12058_effective_phyto_missing_prices ) t
          WHERE t.row_num > 1);

insert into inputprice(topiaid, topiaversion, topiadiscriminator, objectid, sourceunit, category, domain, topiacreatedate, displayname, price, priceunit, phytoproducttype)
  SELECT topiaid,
         topiaversion,
         topiadiscriminator,
         objectid,
         sourceunit,
         category,
         domain,
         topiacreatedate,
         displayname,
         price,
         priceunit,
         phytoproducttype
  FROM _12058_effective_phyto_missing_prices;

update abstractdomaininputstockunit adis set inputprice =
  (
    select p.topiaid
    from _12058_effective_phyto_missing_prices p
    where p.input_id = adis.topiaid
  )
  where exists
  (select 1 from _12058_effective_phyto_missing_prices mp where mp.input_id = adis.topiaid)
  and adis.inputtype = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES';

create table _12058_practiced_phyto_missing_prices as
  SELECT concat('fr.inra.agrosyst.api.entities.InputPrice_', split_part(adis."topiaid", '_', 2)) as topiaid,
         concat('fr.inra.agrosyst.api.entities.InputPrice_', uuid_in(md5(random()::text || random()::text)::cstring)) as unique_topiaid,
         adis.topiaid as input_id,
         p.topiaid as price_id,
         p.topiaversion as topiaversion,
         'fr.inra.agrosyst.api.entities.InputPriceImpl' as topiadiscriminator,
         p.objectid as objectid,
         p.sourceunit as sourceunit,
         'PHYTO_TRAITMENT_INPUT' as category,
         adis."domain" as domain,
         (SELECT topiacreatedate from  _12058_script_date) as topiacreatedate,
         p.displayname as displayname,
         p.price as price,
         p.priceunit as priceunit,
         dppi.producttype as phytoproducttype
  FROM abstractdomaininputstockunit adis
  inner join domainphytoproductinput dppi on dppi.topiaid = adis.topiaid
  inner join refotherinput ri on ri.topiacreatedate = adis.topiacreatedate and ri.topiaid = 'fr.inra.agrosyst.api.entities.referential.RefOtherInput_0000000-0000-0000-0000-000000000000'
  inner join growingplan gp on gp.domain = adis.domain
  inner join growingsystem gs on gs.growingplan = gp.topiaid
  inner join practicedsystem ps on ps.growingsystem = gs.topiaid
  inner join price p on p."practicedsystem" = ps."topiaid" and p.objectid = concat(split_part(adis."key", ';', 1),'_', split_part(adis."key", ';', 2))
  where adis.inputtype = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
  and adis.inputprice is null
  and p.price is not null
  and p.category = 'PHYTO_TRAITMENT_INPUT_CATEGORIE'
  and p.price != 0
  and p.priceunit is not null;

CREATE INDEX _12058_practiced_phyto_missing_prices_Idx ON _12058_practiced_phyto_missing_prices (input_id);

delete FROM _12058_practiced_phyto_missing_prices
  WHERE unique_topiaid IN
      (SELECT unique_topiaid
       FROM
          (SELECT unique_topiaid,
           ROW_NUMBER() OVER( PARTITION BY topiaid
          ORDER BY coalesce (sourceunit, 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ'), price) AS row_num
          FROM _12058_practiced_phyto_missing_prices ) t
          WHERE t.row_num > 1);

insert into inputprice(topiaid, topiaversion, topiadiscriminator, objectid, sourceunit, category, domain, topiacreatedate, displayname, price, priceunit, phytoproducttype)
  SELECT topiaid,
         topiaversion,
         topiadiscriminator,
         objectid,
         sourceunit,
         category,
         domain,
         topiacreatedate,
         displayname,
         price,
         priceunit,
         phytoproducttype
  FROM _12058_practiced_phyto_missing_prices;

update abstractdomaininputstockunit adis set inputprice =
  (
    select p.topiaid
    from _12058_practiced_phyto_missing_prices p
    where p.input_id = adis.topiaid
  )
  where exists
  (select 1 from _12058_practiced_phyto_missing_prices mp where mp.input_id = adis.topiaid)
  and adis.inputtype = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES';


create table _12058_effective_biological_missing_prices as
  SELECT concat('fr.inra.agrosyst.api.entities.InputPrice_', split_part(adis."topiaid", '_', 2)) as topiaid,
         concat('fr.inra.agrosyst.api.entities.InputPrice_', uuid_in(md5(random()::text || random()::text)::cstring)) as unique_topiaid,
         adis.topiaid as input_id,
         p.topiaid as price_id,
         p.topiaversion as topiaversion,
         'fr.inra.agrosyst.api.entities.InputPriceImpl' as topiadiscriminator,
         p.objectid as objectid,
         p.sourceunit as sourceunit,
         'BIOLOGICAL_CONTROL_INPUT' as category,
         adis."domain" as domain,
         (SELECT topiacreatedate from  _12058_script_date) as topiacreatedate,
         p.displayname as displayname,
         p.price as price,
         p.priceunit as priceunit,
         dppi.producttype as phytoproducttype
  FROM abstractdomaininputstockunit adis
  inner join domainphytoproductinput dppi on dppi.topiaid = adis.topiaid
  inner join refotherinput ri on ri.topiacreatedate = adis.topiacreatedate and ri.topiaid = 'fr.inra.agrosyst.api.entities.referential.RefOtherInput_0000000-0000-0000-0000-000000000000'
  inner join price p on p."domain"  = adis."domain" and p.objectid = concat(split_part(adis."key", ';', 1),'_', split_part(adis."key", ';', 2))
  where adis.inputtype = 'LUTTE_BIOLOGIQUE'
  and adis.inputprice is null
  and p.price is not null
  and p.category = 'BIOLOGICAL_CONTROL_INPUT_CATEGORIE'
  and p.price != 0
  and p.priceunit is not null;

CREATE INDEX _12058_effective_biological_missing_prices_Idx ON _12058_effective_biological_missing_prices (input_id);

delete FROM _12058_effective_biological_missing_prices
  WHERE unique_topiaid IN
      (SELECT unique_topiaid
       FROM
          (SELECT unique_topiaid,
           ROW_NUMBER() OVER( PARTITION BY topiaid
          ORDER BY coalesce (sourceunit, 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ'), price) AS row_num
          FROM _12058_effective_biological_missing_prices ) t
          WHERE t.row_num > 1);

insert into inputprice(topiaid, topiaversion, topiadiscriminator, objectid, sourceunit, category, domain, topiacreatedate, displayname, price, priceunit, phytoproducttype)
  SELECT topiaid,
         topiaversion,
         topiadiscriminator,
         objectid,
         sourceunit,
         category,
         domain,
         topiacreatedate,
         displayname,
         price,
         priceunit,
         phytoproducttype
  FROM _12058_effective_biological_missing_prices;

update abstractdomaininputstockunit adis set inputprice =
  (
    select p.topiaid
    from _12058_effective_biological_missing_prices p
    where p.input_id = adis.topiaid
  )
  where exists
  (select 1 from _12058_effective_biological_missing_prices mp where mp.input_id = adis.topiaid)
  and adis.inputtype = 'LUTTE_BIOLOGIQUE';

create table _12058_practiced_biological_missing_prices as
  SELECT concat('fr.inra.agrosyst.api.entities.InputPrice_', split_part(adis."topiaid", '_', 2)) as topiaid,
         concat('fr.inra.agrosyst.api.entities.InputPrice_', uuid_in(md5(random()::text || random()::text)::cstring)) as unique_topiaid,
         adis.topiaid as input_id,
         p.topiaid as price_id,
         p.topiaversion as topiaversion,
         'fr.inra.agrosyst.api.entities.InputPriceImpl' as topiadiscriminator,
         p.objectid as objectid,
         p.sourceunit as sourceunit,
         'BIOLOGICAL_CONTROL_INPUT' as category,
         adis."domain" as domain,
         (SELECT topiacreatedate from  _12058_script_date) as topiacreatedate,
         p.displayname as displayname,
         p.price as price,
         p.priceunit as priceunit,
         dppi.producttype as phytoproducttype
  FROM abstractdomaininputstockunit adis
  inner join domainphytoproductinput dppi on dppi.topiaid = adis.topiaid
  inner join refotherinput ri on ri.topiacreatedate = adis.topiacreatedate and ri.topiaid = 'fr.inra.agrosyst.api.entities.referential.RefOtherInput_0000000-0000-0000-0000-000000000000'
  inner join growingplan gp on gp.domain = adis.domain
  inner join growingsystem gs on gs.growingplan = gp.topiaid
  inner join practicedsystem ps on ps.growingsystem = gs.topiaid
  inner join price p on p."practicedsystem" = ps."topiaid" and p.objectid = concat(split_part(adis."key", ';', 1),'_', split_part(adis."key", ';', 2))
  where adis.inputtype = 'LUTTE_BIOLOGIQUE'
  and adis.inputprice is null
  and p.price is not null
  and p.category = 'BIOLOGICAL_CONTROL_INPUT_CATEGORIE'
  and p.price != 0
  and p.priceunit is not null;

CREATE INDEX _12058_practiced_biological_missing_prices_Idx ON _12058_practiced_biological_missing_prices (input_id);

delete FROM _12058_practiced_biological_missing_prices
  WHERE unique_topiaid IN
      (SELECT unique_topiaid
       FROM
          (SELECT unique_topiaid,
           ROW_NUMBER() OVER( PARTITION BY topiaid
          ORDER BY coalesce (sourceunit, 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ'), price) AS row_num
          FROM _12058_practiced_biological_missing_prices ) t
          WHERE t.row_num > 1);

insert into inputprice(topiaid, topiaversion, topiadiscriminator, objectid, sourceunit, category, domain, topiacreatedate, displayname, price, priceunit, phytoproducttype)
  SELECT topiaid,
         topiaversion,
         topiadiscriminator,
         objectid,
         sourceunit,
         category,
         domain,
         topiacreatedate,
         displayname,
         price,
         priceunit,
         phytoproducttype
  FROM _12058_practiced_biological_missing_prices;

update abstractdomaininputstockunit adis set inputprice =
  (
    select p.topiaid
    from _12058_practiced_biological_missing_prices p
    where p.input_id = adis.topiaid
  )
  where exists
  (select 1 from _12058_practiced_biological_missing_prices mp where mp.input_id = adis.topiaid)
  and adis.inputtype = 'LUTTE_BIOLOGIQUE';

create table _12058_effective_seeding_treatment_missing_prices as
  SELECT concat('fr.inra.agrosyst.api.entities.InputPrice_', split_part(adis."topiaid", '_', 2)) as topiaid,
         concat('fr.inra.agrosyst.api.entities.InputPrice_', uuid_in(md5(random()::text || random()::text)::cstring)) as unique_topiaid,
         adis.topiaid as input_id,
         p.topiaid as price_id,
         p.topiaversion as topiaversion,
         'fr.inra.agrosyst.api.entities.InputPriceImpl' as topiadiscriminator,
         p.objectid as objectid,
         p.sourceunit as sourceunit,
         'SEEDING_TREATMENT_INPUT' as category,
         adis."domain" as domain,
         (SELECT topiacreatedate from  _12058_script_date) as topiacreatedate,
         p.displayname as displayname,
         p.price as price,
         p.priceunit as priceunit,
         dppi.producttype as phytoproducttype
  FROM abstractdomaininputstockunit adis
  inner join domainphytoproductinput dppi on dppi.topiaid = adis.topiaid
  inner join refotherinput ri on ri.topiacreatedate = adis.topiacreatedate and ri.topiaid = 'fr.inra.agrosyst.api.entities.referential.RefOtherInput_0000000-0000-0000-0000-000000000000'
  inner join price p on p."domain"  = adis."domain" and p.objectid = concat(split_part(adis."key", ';', 1),'_', split_part(adis."key", ';', 2))
  where adis.inputtype = 'TRAITEMENT_SEMENCE'
  and adis.inputprice is null
  and p.price is not null
  and p.category = 'SEEDING_TREATMENT_INPUT_CATEGORIE'
  and p.price != 0
  and p.priceunit is not null;

CREATE INDEX _12058_effective_seeding_treatment_missing_prices_Idx ON _12058_effective_seeding_treatment_missing_prices (input_id);

delete FROM _12058_effective_seeding_treatment_missing_prices
  WHERE unique_topiaid IN
      (SELECT unique_topiaid
       FROM
          (SELECT unique_topiaid,
           ROW_NUMBER() OVER( PARTITION BY topiaid
          ORDER BY coalesce (sourceunit, 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ'), price) AS row_num
          FROM _12058_effective_seeding_treatment_missing_prices ) t
          WHERE t.row_num > 1);

insert into inputprice(topiaid, topiaversion, topiadiscriminator, objectid, sourceunit, category, domain, topiacreatedate, displayname, price, priceunit, phytoproducttype)
  SELECT topiaid,
         topiaversion,
         topiadiscriminator,
         objectid,
         sourceunit,
         category,
         domain,
         topiacreatedate,
         displayname,
         price,
         priceunit,
         phytoproducttype
  FROM _12058_effective_seeding_treatment_missing_prices;

create table _12058_practiced_seeding_treatment_missing_prices as
  SELECT concat('fr.inra.agrosyst.api.entities.InputPrice_', split_part(adis."topiaid", '_', 2)) as topiaid,
         concat('fr.inra.agrosyst.api.entities.InputPrice_', uuid_in(md5(random()::text || random()::text)::cstring)) as unique_topiaid,
         adis.topiaid as input_id,
         p.topiaid as price_id,
         p.topiaversion as topiaversion,
         'fr.inra.agrosyst.api.entities.InputPriceImpl' as topiadiscriminator,
         p.objectid as objectid,
         p.sourceunit as sourceunit,
         'SEEDING_TREATMENT_INPUT' as category,
         adis."domain" as domain,
         (SELECT topiacreatedate from  _12058_script_date) as topiacreatedate,
         p.displayname as displayname,
         p.price as price,
         p.priceunit as priceunit,
         dppi.producttype as phytoproducttype
  FROM abstractdomaininputstockunit adis
  inner join domainphytoproductinput dppi on dppi.topiaid = adis.topiaid
  inner join refotherinput ri on ri.topiacreatedate = adis.topiacreatedate and ri.topiaid = 'fr.inra.agrosyst.api.entities.referential.RefOtherInput_0000000-0000-0000-0000-000000000000'
  inner join growingplan gp on gp.domain = adis.domain
  inner join growingsystem gs on gs.growingplan = gp.topiaid
  inner join practicedsystem ps on ps.growingsystem = gs.topiaid
  inner join price p on p."practicedsystem" = ps."topiaid" and p.objectid = concat(split_part(adis."key", ';', 1),'_', split_part(adis."key", ';', 2))
  where adis.inputtype = 'TRAITEMENT_SEMENCE'
  and adis.inputprice is null
  and p.price is not null
  and p.category = 'SEEDING_TREATMENT_INPUT_CATEGORIE'
  and p.price != 0
  and p.priceunit is not null;

CREATE INDEX _12058_practiced_seeding_treatment_missing_prices_Idx ON _12058_practiced_seeding_treatment_missing_prices (input_id);

delete FROM _12058_practiced_seeding_treatment_missing_prices
  WHERE unique_topiaid IN
      (SELECT unique_topiaid
       FROM
          (SELECT unique_topiaid,
           ROW_NUMBER() OVER( PARTITION BY topiaid
          ORDER BY coalesce (sourceunit, 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ'), price) AS row_num
          FROM _12058_practiced_seeding_treatment_missing_prices ) t
          WHERE t.row_num > 1);

insert into inputprice(topiaid, topiaversion, topiadiscriminator, objectid, sourceunit, category, domain, topiacreatedate, displayname, price, priceunit, phytoproducttype)
  SELECT topiaid,
         topiaversion,
         topiadiscriminator,
         objectid,
         sourceunit,
         category,
         domain,
         topiacreatedate,
         displayname,
         price,
         priceunit,
         phytoproducttype
  FROM _12058_practiced_seeding_treatment_missing_prices;



