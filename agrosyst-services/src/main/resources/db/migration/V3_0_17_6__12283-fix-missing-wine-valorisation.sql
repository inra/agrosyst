---
-- %%Ignore-License
---

INSERT INTO harvestingaction_winevalorisations (winevalorisations, owner)
SELECT distinct rd.winevalorisation winevalorisations, aa.topiaid
FROM abstractaction aa
INNER JOIN harvestingactionvalorisation hav on hav.harvestingaction = aa.topiaid
INNER JOIN refdestination rd ON rd.topiaid = hav.destination
INNER JOIN croppingplanspecies cps on cps.code = hav.speciescode
INNER JOIN refespece re ON re.topiaid = cps.species
WHERE aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.HarvestingActionImpl'
AND re.code_espece_botanique ILIKE '%ZMO%'
AND aa.topiaid not in (select owner from public.harvestingaction_winevalorisations);