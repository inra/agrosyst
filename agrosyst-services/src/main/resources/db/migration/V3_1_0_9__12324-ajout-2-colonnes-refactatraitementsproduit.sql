---
-- %%Ignore-License
---

ALTER TABLE refactatraitementsproduit ADD COLUMN date_retrait_produit DATE;
ALTER TABLE refactatraitementsproduit ADD COLUMN date_autorisation_produit DATE;
