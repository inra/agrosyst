---
-- %%Ignore-License
---

create table refcorrespondancematerieloutilsts (
  topiaId varchar(255) not null,
  topiaVersion bigint not null,
  topiaCreateDate timestamp not null,
  type_materiel_1 text not null,
  typetravailsol text,
  desherbage_mecanique bool,
  source text,
  active bool not null,
  primary key (topiaId)
);

ALTER TABLE refcorrespondancematerieloutilsts
ADD CONSTRAINT uk_refcorrespondancematerieloutilsts
UNIQUE (type_materiel_1);
