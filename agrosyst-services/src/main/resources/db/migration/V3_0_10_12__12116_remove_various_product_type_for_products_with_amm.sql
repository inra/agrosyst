---
-- %%Ignore-License
---

--- Migrate product categ

update refactatraitementsproduitscateg set type_produit = 'RODENTICIDES_AND_REPELLENTS' where id_traitement in (118, 119, 120, 122);
update refactatraitementsproduitscateg set type_produit = 'VIRUCIDES' where id_traitement = 135;
update refactatraitementsproduitscateg set type_produit = 'WOUND_PROTECTION' where id_traitement in (136, 138);
delete from refactatraitementsproduitscateg where id_traitement in (114, 132, 137, 999903);

-- CAS 1

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 137 and
    r.id_produit in ('2819', '2820', '2821', '2822', '3207', '3212', '4579', '4580', '4611', '4612', '5692', '8452', '8453', 'maaf_9800245', 'maaf_9800245_A', 'maaf_9800245_B', 'maaf_9800245_C') and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 123 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 123 and
    r.id_produit in ('2819', '2820', '2821', '2822', '3207', '3212', '4579', '4580', '4611', '4612', '5692', '8452', '8453', 'maaf_9800245', 'maaf_9800245_A', 'maaf_9800245_B', 'maaf_9800245_C');

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 999903 and
    r.id_produit in ('maaf_2020245', 'maaf_2030133') and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 999907 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 999907 and
    r.id_produit in ('maaf_2020245', 'maaf_2030133');

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 999903 and
    r.id_produit in ('4249') and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 167 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 167 and
    r.id_produit in ('4249');

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 999903 and
    r.id_produit in ('8334', '8333', 'maaf_2150155') and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 142 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 142 and
    r.id_produit in ('8334', '8333', 'maaf_2150155');

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 114 and
    r.id_produit in ('4564', '4565') and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 140 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 140 and
    r.id_produit in ('4564', '4565');

update domainphytoproductinput d
set producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 120 and
    r.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 120;

update domainphytoproductinput d
set producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 118 and
    r.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 118;

--- CAS 2

insert into refactatraitementsproduit (topiaid, topiaversion, id_produit, id_traitement, topiacreatedate, nom_produit, code_traitement, nom_traitement, nodu, source, active, code_amm, code_traitement_maa, nom_traitement_maa, etat_usage, refcountry) values
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_12003cce-b680-41c9-9f43-ef05a57edbc7', 0, 'a_AAADF', 155, now(), 'BRIFUR 5G', 'I4', 'Insecticides - Traitement du sol', false, 'Agrosyst 2013', true, '8800139', 'S4', 'Insecticides acaricides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_1dc7efe8-e2cb-45e4-93b0-ec5b08e02a98', 0, 'a_AAAFJ', 121, now(), 'FLUXOL', 'D5', 'Divers - Molluscicides', false, 'Agrosyst 2013', true, '9700076', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_da75867b-0b2e-4a99-9167-0482bff20048', 0, 'a_AAAQR', 118, now(), 'RATICIDE PIRATE', 'D2', 'Divers - Rodenticides', false, 'IMPORTS_SYSTERRE_2014', true, '9000066', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_1a77a5a4-64b8-45e1-bad3-de0eee0d440a', 0, 'a_AAAWJ', 118, now(), 'BROMACAL 10', 'D2', 'Divers - Rodenticides', false, 'MAA2015', true, '2000440', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_c9589c23-e352-4384-93a3-c143a60b96bf', 0, '2935', 118, now(), 'ARMICARB', 'D2', 'Divers - Rodenticides', true, 'MAA2022', true, '2110059', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_3dd91fdb-694c-4ecb-a18a-0eba83cd6b9c', 0, 'maaf_2110059', 123, now(), 'PONDUCARB', 'D7', 'Divers - Substances de croissance', true, 'MAA2022', true, '2110059', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_1f1c4aba-4228-4868-875e-fcc0c490bd5f', 0, 'maaf_2000067', 144, now(), 'MENNO FLORADES', 'F5', 'Fongicides - Traitement des locaux et matériels de stockage, matériels de laiterie', false, 'MesParcelles 2020 amm 2013', true, '2000067', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_51e4d5a8-21df-4ade-a6ef-edc6bd9cfe4d', 0, 'maaf_2010237', 153, now(), 'EURO-APPRO C 1', 'I2', 'Insecticides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2010237', 'S4', 'Insecticides acaricides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_fd63bb13-5210-4169-9f0a-c197f1a5813b', 0, 'maaf_2010110', 123, now(), 'TEMPO+', 'D7', 'Divers - Substances de croissance', false, 'MesParcelles 2020 amm 2013', true, '2010110', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_b96ca5f8-507a-4124-b9d8-cc56ff56db94', 0, 'maaf_2020141', 140, now(), 'CIRCUS+', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2020141', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_ca1bc658-46c0-432d-83a4-9ac51bff3e4f', 0, 'maaf_2020390', 140, now(), 'SALSA', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2020390', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_62155b64-3a59-4670-a47d-07b9890f62de', 0, 'maaf_2030080', 121, now(), 'KOUGAR', 'D5', 'Divers - Molluscicides', false, 'MesParcelles 2020 amm 2013', true, '2030080', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_5d8f96f4-278d-4691-832d-68822b31b6ef', 0, 'maaf_2030378', 153, now(), 'FIGARI 25 WP', 'I2', 'Insecticides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2030378', 'S4', 'Insecticides acaricides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_df4ca019-d7f2-481d-b47b-8ddd5e94cb2f', 0, 'maaf_2030445', 140, now(), 'BORABORA', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2030445', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_8e52fb48-9949-4386-be1b-04401ed75ab2', 0, 'maaf_2050032', 140, now(), 'CARAIBES', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2050032', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_51fb48e7-038c-43f7-8aa3-a39470a4dea6', 0, 'maaf_2050065', 123, now(), 'TRIPHON', 'D7', 'Divers - Substances de croissance', false, 'MesParcelles 2020 amm 2013', true, '2050065', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_44541ccd-e057-4431-80a8-790d9d3ad4e9', 0, 'maaf_2050064', 153, now(), 'DUNE', 'I2', 'Insecticides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2050064', 'S4', 'Insecticides acaricides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_9a38acb7-296b-4eec-8600-e88074ca39c5', 0, 'maaf_2040366', 140, now(), 'FOLTANA', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2040366', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_8b8fb499-23cd-4dbd-a8c9-13f5007be02a', 0, 'maaf_2040357', 140, now(), 'SABOT', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2040357', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_7629ebe3-fe08-4cd4-978a-f997ebceb55b', 0, 'maaf_2050117', 140, now(), 'MANDOLINE', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2050117', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_ddeb9da7-ddbe-4be4-b197-0137aa449ee2', 0, 'maaf_2050087', 140, now(), 'ZIRKON', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2050087', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_816e3826-d266-42e2-a618-2eca82d0fe19', 0, 'maaf_2050124', 140, now(), 'NICOSIA', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2050124', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_edeae00c-80cb-435d-8d15-d9139eaeac39', 0, 'maaf_2050122', 140, now(), 'PONTIF', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2050122', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_e7dbf499-09d7-4e23-ac89-7c0072fea4fa', 0, 'maaf_2060008', 153, now(), 'CASCAD''OC', 'I2', 'Insecticides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2060008', 'S4', 'Insecticides acaricides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_58a78426-b973-4512-b591-90004ce031c0', 0, 'maaf_2060087', 140, now(), 'GRAPETEC', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2060087', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_41ae0047-797d-48bc-a118-fd78f8fc2b98', 0, 'maaf_2060009', 153, now(), 'AVALANCHE', 'I2', 'Insecticides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2060009', 'S4', 'Insecticides acaricides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_bf852801-2b0c-4558-bff7-827152987117', 0, 'maaf_2060089', 140, now(), 'CARIBO', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2060089', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_d23eeb90-4c28-4fee-89cf-ef677ba9d0c1', 0, 'maaf_2070154', 140, now(), 'MAITRE', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2070154', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_fe541a16-56b1-49af-8dc7-a0fea97b6ae7', 0, 'maaf_2060124', 140, now(), 'MAINVERTE', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2060124', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_6c3f73f3-9809-4840-9a98-dbf50a6aac3f', 0, 'maaf_2060097', 140, now(), 'QUANTA', 'F1', 'Fongicides - Traitement des parties aériennes', false, 'MesParcelles 2020 amm 2013', true, '2060097', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_81c500fd-8d7e-4497-a33c-4db279e0cf6e', 0, 'maaf_2070070', 121, now(), 'SLAKPAST', 'D5', 'Divers - Molluscicides', false, 'MesParcelles 2020 amm 2013', true, '2070070', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_4208cbaa-b82f-4f86-963d-156c6ab55466', 0, 'maaf_8800674', 131, now(), 'NEORAM BLEU', 'D8bf', 'Divers - Divers - Adjuvants pour marquage des zones traitées', false, 'MesParcelles 2020 amm 2013', true, '8800674', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_86d7d450-7c08-4c43-a71a-474a1f6c8cb5', 0, 'maaf_9000572', 136, now(), 'SICACID', 'D8g', 'Divers - Protection des plaies de taille', false, 'MesParcelles 2020 amm 2013', true, '9000572', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_10bdf65b-60c7-4a40-a372-71193b6338ba', 0, 'maaf_9100415', 123, now(), 'FOLISTOP L', 'D7', 'Divers - Substances de croissance', false, 'MesParcelles 2020 amm 2013', true, '9100415', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_418ce381-d5b6-4551-a75b-88c14c452810', 0, 'maaf_9700275', 130, now(), 'ELECO AF', 'D8be', 'Divers - Divers - Adjuvants actifs', false, 'MesParcelles 2020 amm 2013', true, '9700275', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_4e4a69a2-1ae5-4ab3-bc67-e00bb0b6d558', 0, 'maaf_9900431', 144, now(), 'JAPUR', 'F5', 'Fongicides - Traitement des locaux et matériels de stockage, matériels de laiterie', false, 'MesParcelles 2020 amm 2013', true, '9900431', 'S5', 'Fongicides bactericides', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_51e153ca-7c2a-45f0-9ef2-74b50fa6e2ae', 0, '5502', 136, now(), 'GLU ARBORICOLE PELTON 2', 'D8g', 'Divers - Protection des plaies de taille', false, 'MAA2022', true, '8700111', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_8279c2c5-f8fa-4f88-bd75-f3340be4cf19', 0, 'maaf_9900399', 136, now(), 'GLU BASF HJ', 'D8g', 'Divers - Protection des plaies de taille', false, 'MAA2022', true, '9900399', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_0b7c0875-8e35-4035-90cf-78e911e0102d', 0, 'maaf_9200011', 136, now(), 'GLU CLAUSE', 'D8g', 'Divers - Protection des plaies de taille', false, 'MAA2022', true, '9200011', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_54c822ab-6f8f-409c-8510-644859c748d4', 0, 'maaf_7400634', 136, now(), 'GLU LHOMME LEFORT', 'D8g', 'Divers - Protection des plaies de taille', false, 'MAA2022', true, '7400634', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_3bbcb3f9-2d4a-47ea-b038-efb990f0bd91', 0, 'maaf_9500296', 136, now(), 'GLU NAVARRE', 'D8g', 'Divers - Protection des plaies de taille', false, 'MAA2022', true, '9500296', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_9c56ebe0-d61b-485a-86fb-fc1aae73d1d9', 0, 'maaf_8700149', 136, now(), 'GLU V VILMORIN', 'D8g', 'Divers - Protection des plaies de taille', false, 'MAA2022', true, '8700149', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_95830e8c-58e6-49ce-899d-b4146df48cb6', 0, 'maaf_9100178', 136, now(), 'KB GLU', 'D8g', 'Divers - Protection des plaies de taille', false, 'MAA2022', true, '9100178', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_b7a514dc-0942-4d0c-a790-d7a5eca1e1b9', 0, '5323', 136, now(), 'RAMPASTOP', 'D8g', 'Divers - Protection des plaies de taille', false, 'MAA2022', true, '9200532', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_f102d71a-9f9a-4f22-aedc-7e6576f056b3', 0, 'maaf_8900633', 136, now(), 'UMUPRO JARDIN GLU', 'D8g', 'Divers - Protection des plaies de taille', false, 'MAA2022', true, '8900633', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_2498776c-f060-473e-ba58-053767a6d811', 0, 'maaf_2110059_A', 123, now(), 'APC-09CD', 'D7', 'Divers - Substances de croissance', true, 'MAA2022', true, '2110059', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_2ee144c8-1c25-40a5-9839-254a46c8d493', 0, 'maaf_9800245_D', 123, now(), 'SULFORIX LS', 'D7', 'Divers - Substances de croissance', true, 'MAA2022', true, '9800245', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_08e405da-2067-4290-a486-2da10184c2fd', 0, '2902', 124, now(), 'ANTIMOUSSE BOUILLIES', 'D8a', 'Divers - Huiles adjuvantes', false, 'ACTA 2017', true, '8500205', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_826d15bc-bfa4-4405-a6dc-f3b1cfdd96ac', 0, '2905', 124, now(), 'ANTIMOUSSANT SOVILO', 'D8a', 'Divers - Huiles adjuvantes', false, 'ACTA 2015', true, '8700472', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_4fe7cb9c-7ddc-49de-9aa6-d358c42f9830', 0, '2904', 124, now(), 'CASS''MOUSSE', 'D8a', 'Divers - Huiles adjuvantes', false, 'ACTA 2017', true, '2100013', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_b80a324d-9e74-44c3-a536-f7c54263a3b8', 0, '2903', 124, now(), 'DECCO ANTIMOUSSE', 'D8a', 'Divers - Huiles adjuvantes', false, 'ACTA 2017', true, '8100315', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_9be82aee-26ea-4dfb-8976-5ea664e40b7f', 0, '2906', 124, now(), 'TECHNOMOUSSE', 'D8a', 'Divers - Huiles adjuvantes', false, 'ACTA 2017', true, '8500205', 'S6', 'Autres', 'Retiré', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1'),
    ('fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_932428c5-4d4f-4c33-bd6c-936590aba6d3', 0, '7870', 124, now(), 'BULTAK', 'D8a', 'Divers - Huiles adjuvantes', false, 'ACTA 2017', true, '2100013', 'S6', 'Autres', 'Autorisé', 'fr.inra.agrosyst.api.entities.referential.RefCountry_454036f3-ecf2-472c-9158-b24e60bb24d1');

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 999903 and
    r.id_produit in ('a_AAAQR', 'a_AAAWJ', '2935') and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 118 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 118 and
    r.id_produit in ('a_AAAQR', 'a_AAAWJ', '2935');

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 999903 and
    r.id_produit in ('a_AAAFJ', 'maaf_2030080', 'maaf_2070070') and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 121 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 121 and
    r.id_produit in ('a_AAAFJ', 'maaf_2030080', 'maaf_2070070');

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement in (999903, 137) and
    r.id_produit in ('maaf_2110059', 'maaf_2010110', 'maaf_2050065', 'maaf_9100415', 'maaf_2110059_A', 'maaf_9800245_D') and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 123 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 123 and
    r.id_produit in ('maaf_2110059', 'maaf_2010110', 'maaf_2050065', 'maaf_9100415', 'maaf_2110059_A', 'maaf_9800245_D');

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 132 and
    r.id_produit in ('2902', '2905', '2904', '2903', '2906', '7870') and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 124 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 124 and
    r.id_produit in ('2902', '2905', '2904', '2903', '2906', '7870');

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 999903 and
    r.id_produit = 'maaf_9700275' and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 130 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 130 and
    r.id_produit = 'maaf_9700275';

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 999903 and
    r.id_produit = 'maaf_8800674' and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 131 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 131 and
    r.id_produit = 'maaf_8800674';

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 999903 and
    r.id_produit in ('maaf_9000572', '5502', 'maaf_9900399', 'maaf_9200011', 'maaf_7400634', 'maaf_9500296', 'maaf_8700149', 'maaf_9100178', '5323', 'maaf_8900633') and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 136 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 136 and
    r.id_produit in ('maaf_9000572', '5502', 'maaf_9900399', 'maaf_9200011', 'maaf_7400634', 'maaf_9500296', 'maaf_8700149', 'maaf_9100178', '5323', 'maaf_8900633');

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 999903 and
    r.id_produit in ('maaf_2020141', 'maaf_2020390', 'maaf_2030445', 'maaf_2050032', 'maaf_2040366', 'maaf_2040357', 'maaf_2050117', 'maaf_2050087', 'maaf_2050124', 'maaf_2050122', 'maaf_2060087', 'maaf_2060089', 'maaf_2070154', 'maaf_2060124', 'maaf_2060097') and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 140 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 140 and
    r.id_produit in ('maaf_2020141', 'maaf_2020390', 'maaf_2030445', 'maaf_2050032', 'maaf_2040366', 'maaf_2040357', 'maaf_2050117', 'maaf_2050087', 'maaf_2050124', 'maaf_2050122', 'maaf_2060087', 'maaf_2060089', 'maaf_2070154', 'maaf_2060124', 'maaf_2060097');

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 999903 and
    r.id_produit in ('maaf_2000067', 'maaf_9900431') and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 144 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 144 and
    r.id_produit in ('maaf_2000067', 'maaf_9900431');

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 999903 and
    r.id_produit in ('maaf_2010237', 'maaf_2030378', 'maaf_2050064', 'maaf_2060008', 'maaf_2060009') and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 153 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 153 and
    r.id_produit in ('maaf_2010237', 'maaf_2030378', 'maaf_2050064', 'maaf_2060008', 'maaf_2060009');

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 999903 and
    r.id_produit = 'a_AAADF' and
    r.id_produit = r2.id_produit and
    r2.id_traitement = 155 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 155 and
    r.id_produit = 'a_AAADF';

--- CAS 3

update domainphytoproductinput d
set producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 119 and
    r.id_traitement = c.id_traitement;

update domainphytoproductinput d
set producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 122 and
    r.id_traitement = c.id_traitement;

update domainphytoproductinput d
set producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 138 and
    r.id_traitement = c.id_traitement;

update domainphytoproductinput d
set producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 136 and
    r.id_traitement = c.id_traitement;

update domainphytoproductinput d
set producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 135 and
    r.id_traitement = c.id_traitement;
