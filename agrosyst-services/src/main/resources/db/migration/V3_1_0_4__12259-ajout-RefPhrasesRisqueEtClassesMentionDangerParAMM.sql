---
-- %%Ignore-License
---

CREATE TABLE refphrasesrisqueetclassesmentiondangerparamm (
  topiaid VARCHAR(255) NOT NULL,
  topiaversion INT8 NOT NULL,
  topiacreatedate TIMESTAMP NULL,
  code_AMM TEXT NOT NULL,
  typeproduit TEXT NOT NULL,
  typeinfo TEXT NOT NULL,
  code_info TEXT NOT NULL,
  libelle_info TEXT NOT NULL,
  libelle_court_info TEXT,
  danger_environnement BOOL NOT NULL,
  toxique_utilisateur BOOL NOT NULL,
  cmr BOOL NOT NULL,
  source TEXT,
  active BOOL NOT NULL,

  CONSTRAINT refphrasesrisqueetclassesmentiondangerparamm_pkey PRIMARY KEY (topiaid),
  CONSTRAINT uk_refphrasesrisqueetclassesmentiondangerparamm UNIQUE (code_amm, code_info)
);

