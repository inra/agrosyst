---
-- %%Ignore-License
---

--- Deactivate COMPLICE product

update refactatraitementsproduit set active = false where id_produit = '4236' and id_traitement = 171;

--- Migrate extrait de reine des près

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
    r.id_traitement = 999905 and
    r.id_produit = 'a_AAATJ' and
    r2.id_produit = '' and
    r2.id_traitement = 1144 and
    r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
    a.topiaid = i.topiaid and
    i.refinput = r.topiaid and
    r.id_traitement = 1144 and
    r.id_produit = '';

delete from refactatraitementsproduit where id_produit = 'a_AAATJ' and id_traitement = 999905;

--- Migrate savon noir

update domainphytoproductinput d
set refinput = r2.topiaid, producttype = c.type_produit
from refactatraitementsproduit r, refactatraitementsproduit r2, refactatraitementsproduitscateg c
where d.refinput = r.topiaid and
  r.id_traitement = 999905 and
  r.id_produit = 'a_AAARE' and
  r2.id_produit = 'a_AAAZF' and
  r2.id_traitement = 153 and
  r2.id_traitement = c.id_traitement;

update inputprice p
set objectid = concat(r.id_produit, '_', r.id_traitement)
from abstractdomaininputstockunit a, domainphytoproductinput i, refactatraitementsproduit r
where p.topiaid = a.inputprice and
  a.topiaid = i.topiaid and
  i.refinput = r.topiaid and
  r.id_traitement = 153 and
  r.id_produit = 'a_AAAZF';

delete from refactatraitementsproduit where id_produit = 'a_AAARE' and id_traitement = 999905;

--- Deactivate some products

update refactatraitementsproduit set active = false where id_produit in ('a_AAATK', 'a_AAAYW', 'a_AAASB', 'a_AAAYV') and id_traitement = 999905;
