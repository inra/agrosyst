---
-- %%Ignore-License
---

CREATE TABLE refseedunits (
  topiaid varchar(255) NOT NULL,
  topiaversion int8 NOT NULL,
  topiacreatedate timestamp,
  code_espece_botanique text not null,
  libelle_espece_botanique text,
  code_qualifiant_AEE text,
  libelle_qualifiant_AEE text,
  seedPlantUnit character varying(255) not null,
  source text not null,
  active bool not null,

  CONSTRAINT refseedunits_pkey PRIMARY KEY (topiaid),
  CONSTRAINT uk_refseedunits UNIQUE (code_espece_botanique, code_qualifiant_AEE,seedPlantUnit)
);

comment on column refseedunits.code_espece_botanique is 'Code correspondant à une espèce botanique';
comment on column refseedunits.libelle_espece_botanique is 'Nom vernaculaire de l’espèce botanique';
comment on column refseedunits.code_qualifiant_AEE is 'Code correspondant au qualifiant de ladite espèce';
comment on column refseedunits.libelle_qualifiant_AEE is 'Nom vernaculaire du qualifiant';
comment on column refseedunits.seedPlantUnit is 'Unité de semi qui a été fixée pour ladite espèce';
comment on column refseedunits.source is 'La source de l’information';
comment on column refseedunits.active is 'Est ce que la ligne est active ou non ?';

CREATE INDEX refseedunits_idx ON refseedunits(code_espece_botanique, code_qualifiant_AEE);