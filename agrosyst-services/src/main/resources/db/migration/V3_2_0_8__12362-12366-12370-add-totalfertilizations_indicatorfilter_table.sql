---
-- %%Ignore-License
---

CREATE TABLE public.indicatorfilter_totalfertilizations (
	"owner" varchar(255) NULL,
	totalfertilizations varchar(255) NULL
);

-- Ajout de contraintes pour les nouvelles tables indicatorfilter_*
alter table if exists indicatorfilter_totalfertilizations add constraint FKjngakbhcjfjhdoq8qdu2dcp55 foreign key (OWNER) references indicatorFilter;
