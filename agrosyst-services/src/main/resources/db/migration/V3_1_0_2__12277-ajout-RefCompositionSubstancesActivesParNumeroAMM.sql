---
-- %%Ignore-License
---

CREATE TABLE refcompositionsubstancesactivesparnumeroamm (
  topiaid VARCHAR(255) NOT NULL,
  topiaversion INT8 NOT NULL,
  topiacreatedate TIMESTAMP NULL,
  numero_AMM TEXT NOT NULL,
  id_sa TEXT NOT NULL,
  nom_sa TEXT NOT NULL,
  variant_sa TEXT,
  dose_sa FLOAT8 NOT NULL,
  unite_sa TEXT NOT NULL,
  source TEXT NOT NULL,
  active BOOL NOT NULL,

  CONSTRAINT refcompositionsubstancesactivesparnumeroamm_pkey PRIMARY KEY (topiaid),
  CONSTRAINT uk_refcompositionsubstancesactivesparnumeroamm UNIQUE (numero_AMM, id_sa)
);

