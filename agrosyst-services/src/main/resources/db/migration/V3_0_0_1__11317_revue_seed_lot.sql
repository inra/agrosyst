---
-- %%Ignore-License
--
--

DROP INDEX idx_domainphytoproductinput_domainseedlotinput;
DROP INDEX idx_seedproductinputusage_seedlotinputusage;

ALTER table domainPhytoProductInput DROP CONSTRAINT FKsncrytaeud5femufvc3il41vk;
ALTER table seedProductInputUsage DROP CONSTRAINT FKmyroadmvi85j1vsvwjg6umm7d;

ALTER table domainSeedLotInput DROP COLUMN biologicalSeedInoculation;
ALTER table domainSeedLotInput DROP COLUMN chemicalTreatment;
ALTER table domainSeedLotInput DROP COLUMN mix;
ALTER table domainSeedLotInput DROP COLUMN seedtype;

ALTER table domainPhytoProductInput DROP COLUMN domainSeedLotInput;
ALTER table seedProductInputUsage DROP COLUMN seedLotInputUsage;

ALTER TABLE domainseedspeciesinput DROP column topiaversion;
ALTER TABLE domainseedspeciesinput DROP column topiacreatedate;


