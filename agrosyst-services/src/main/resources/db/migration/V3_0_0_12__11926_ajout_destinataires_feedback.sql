---
-- %%Ignore-License
---

INSERT INTO reffeedbackrouter (topiaid,topiaversion,topiacreatedate,sector,typedephy,feedbackcategory,mails,active) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter_0f0ccb2d-b357-426d-924e-3cb166cb84e6',0,'2023-03-23 13:19:19.703','POLYCULTURE_ELEVAGE','DEPHY_FERME','ETAT_LIEUX_SAISIES','cellule.reference.dephy@idele.fr',true),
	 ('fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter_5f236e92-38ce-4514-9ae5-b5354f954999',0,'2023-03-23 13:19:19.718','HORTICULTURE','DEPHY_FERME','ETAT_LIEUX_SAISIES','cellule.reference.dephy@idele.fr',true),
	 ('fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter_aa348c3a-56ba-40a0-896b-2444614bffb5',0,'2023-03-23 13:19:19.719','CULTURES_TROPICALES','DEPHY_FERME','ETAT_LIEUX_SAISIES','cellule.reference.dephy@idele.fr',true),
	 ('fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter_19c6fb06-afaf-4ac6-b023-2df5fd8725c6',0,'2023-03-23 13:19:19.719','ARBORICULTURE','DEPHY_FERME','ETAT_LIEUX_SAISIES','cellule.reference.dephy@idele.fr',true),
	 ('fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter_0544e7d3-3661-4a63-91f6-66bc83619500',0,'2023-03-23 13:19:19.719','VITICULTURE','DEPHY_FERME','ETAT_LIEUX_SAISIES','cellule.reference.dephy@idele.fr',true),
	 ('fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter_2cb918ae-a181-4cec-8b76-c496061cc0e0',0,'2023-03-23 13:19:19.720','MARAICHAGE','DEPHY_FERME','ETAT_LIEUX_SAISIES','cellule.reference.dephy@idele.fr',true),
	 ('fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter_1cdfa31b-95fb-4f3f-88be-3f1676ffdc88',0,'2023-03-23 13:19:19.720','GRANDES_CULTURES','DEPHY_FERME','ETAT_LIEUX_SAISIES','cellule.reference.dephy@idele.fr',true);

INSERT INTO reffeedbackrouter (topiaid,topiaversion,topiacreatedate,sector,typedephy,feedbackcategory,mails,active) VALUES
	 ('fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter_8d8af088-9251-47f6-a7c7-e9c5b42f3e59',0,'2023-03-23 13:24:00.144','POLYCULTURE_ELEVAGE','DEPHY_FERME','LOCAL_INTRANTS','cellule.reference.dephy@idele.fr, sm-agrosystsupport@inrae.fr',true),
	 ('fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter_869c75ab-5ebc-4283-9a6e-a630fabc89c5',0,'2023-03-23 13:24:00.144','HORTICULTURE','DEPHY_FERME','LOCAL_INTRANTS','cellule.reference.dephy@idele.fr, sm-agrosystsupport@inrae.fr',true),
	 ('fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter_c2229b85-dd14-4b74-bd05-309ba6eec863',0,'2023-03-23 13:24:00.145','CULTURES_TROPICALES','DEPHY_FERME','LOCAL_INTRANTS','cellule.reference.dephy@idele.fr, sm-agrosystsupport@inrae.fr',true),
	 ('fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter_1fd13069-30a5-4fdd-8da5-e39bcac41532',0,'2023-03-23 13:24:00.145','ARBORICULTURE','DEPHY_FERME','LOCAL_INTRANTS','cellule.reference.dephy@idele.fr, sm-agrosystsupport@inrae.fr',true),
	 ('fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter_1480427e-a47c-4db5-8c5a-dcf7cd49bba6',0,'2023-03-23 13:24:00.146','VITICULTURE','DEPHY_FERME','LOCAL_INTRANTS','cellule.reference.dephy@idele.fr, sm-agrosystsupport@inrae.fr',true),
	 ('fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter_37e7cc70-4ae9-4609-8e76-ab06aec8f540',0,'2023-03-23 13:24:00.146','MARAICHAGE','DEPHY_FERME','LOCAL_INTRANTS','cellule.reference.dephy@idele.fr, sm-agrosystsupport@inrae.fr',true),
	 ('fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter_edcd3ce3-b621-46c3-a760-6fed82a43653',0,'2023-03-23 13:24:00.146','GRANDES_CULTURES','DEPHY_FERME','LOCAL_INTRANTS','cellule.reference.dephy@idele.fr, sm-agrosystsupport@inrae.fr',true);

