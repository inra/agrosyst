---
-- %%Ignore-License
---

ALTER TABLE realise_echelle_zone ALTER COLUMN utilisation_desherbage_mecanique TYPE boolean USING CASE WHEN utilisation_desherbage_mecanique=1 THEN TRUE ELSE null END;

alter table realise_echelle_sdc ALTER COLUMN utilisation_desherbage_mecanique TYPE boolean USING CASE WHEN utilisation_desherbage_mecanique=1 THEN TRUE ELSE null END;

alter table realise_echelle_parcelle ALTER COLUMN utilisation_desherbage_mecanique TYPE boolean USING CASE WHEN utilisation_desherbage_mecanique=1 THEN TRUE ELSE null END;

alter table realise_echelle_itk ALTER COLUMN utilisation_desherbage_mecanique TYPE boolean USING CASE WHEN utilisation_desherbage_mecanique=1 THEN TRUE ELSE null END;

alter table realise_echelle_culture ALTER COLUMN utilisation_desherbage_mecanique TYPE boolean USING CASE WHEN utilisation_desherbage_mecanique=1 THEN TRUE ELSE null END;

alter table realise_echelle_intervention ALTER COLUMN utilisation_desherbage_mecanique TYPE boolean USING CASE WHEN utilisation_desherbage_mecanique=1 THEN TRUE ELSE null END;

alter table synthetise_echelle_synthetise ALTER COLUMN utilisation_desherbage_mecanique TYPE boolean USING CASE WHEN utilisation_desherbage_mecanique=1 THEN TRUE ELSE null END;

alter table synthetise_echelle_culture ALTER COLUMN utilisation_desherbage_mecanique TYPE boolean USING CASE WHEN utilisation_desherbage_mecanique=1 THEN TRUE ELSE null END;

alter table synthetise_echelle_intervention ALTER COLUMN utilisation_desherbage_mecanique TYPE boolean USING CASE WHEN utilisation_desherbage_mecanique=1 THEN TRUE ELSE null END;
