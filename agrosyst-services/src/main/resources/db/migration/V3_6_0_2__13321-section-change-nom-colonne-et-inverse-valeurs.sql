---
-- %%Ignore-License
---
alter table "section"
rename column sdcNonConcerneBioAgresseur
to sdcConcerneBioAgresseur;

update "section"
set sdcConcerneBioAgresseur = not sdcConcerneBioAgresseur
where sdcConcerneBioAgresseur is not null;

update "section"
set sdcConcerneBioAgresseur = true
where sectiontype in ('ADVENTICES', 'MALADIES', 'RAVAGEURS')
and sdcConcerneBioAgresseur is null;
