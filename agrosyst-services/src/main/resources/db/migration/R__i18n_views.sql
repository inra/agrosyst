---
-- %%Ignore-License
---

drop view if exists liste_libelles_ref;
drop view if exists liste_libelles_ref_vivant;
drop view if exists liste_libelles_ref_sol;
drop view if exists liste_libelles_ref_intrant;
drop view if exists liste_libelles_ref_materiel;
drop view if exists liste_libelles_ref_intervention;
drop view if exists liste_libelles_ref_divers;
drop view if exists liste_libelles_ref_destination;

create view liste_libelles_ref_destination as (
select distinct on (k) k, v, col, ref from (

select
to_complex_i18n_key(destination) as k,
destination as v, 'destination' as col, 'refdestination' as ref
from refdestination

union

select
to_complex_i18n_key(marketingdestination) as k,
marketingdestination as v, 'marketingdestination' as col, 'refmarketingdestination' as ref
from refmarketingdestination

union

select
to_simple_i18n_key(destination) as k,
destination as v, 'destination' as col, 'refharvestingprice' as ref
from refharvestingprice

union

select
to_simple_i18n_key(libelle_destination_aee) as k, libelle_destination_aee as v, 'libelle_destination_aee' as col, 'refespece' as ref
from refespece

order by k asc
) as t
where v <> '');


create view liste_libelles_ref_divers as (
select distinct on (k) k, v, col, ref from (

select
to_simple_i18n_key(libelle_type_saisonnier_aee) as k,
libelle_type_saisonnier_aee as v, 'libelle_type_saisonnier_aee' as col, 'refespece' as ref
from refespece

union

select
    to_simple_i18n_key(libelle_otex_18_postes) as k,
    libelle_otex_18_postes as v,
    'libelle_otex_18_postes' as col,
    'refotex' as ref
from refotex

union

select
    to_complex_i18n_key(libelle_otex_70_postes) as k,
    libelle_otex_70_postes as v,
    'libelle_otex_70_postes' as col,
    'refotex' as ref
from refotex

union

select
    to_simple_i18n_key(animaltype) as k,
    animaltype as v,
    'animaltype' as col,
    'refanimaltype' as ref
from refanimaltype

union

select
    to_simple_i18n_key(animalpopulationunits) as k,
    animalpopulationunits as v,
    'animalpopulationunits' as col,
    'refanimaltype' as ref
from refanimaltype

union

select
    to_simple_i18n_key(alimentunit) as k,
    alimentunit as v,
    'alimentunit' as col,
    'refcattlerationaliment' as ref
from refcattlerationaliment

union

select
to_simple_i18n_key(iae_nom) as k,
iae_nom as v, 'iae_nom' as col, 'refelementvoisinage' as ref
from refelementvoisinage

union

select
to_simple_i18n_key(unite_surface) as k,
unite_surface as v, 'unite_surface' as col, 'refelementvoisinage' as ref
from refelementvoisinage

union

select
to_simple_i18n_key(libelle_carburant) as k,
libelle_carburant as v, 'libelle_carburant' as col, 'refgescarburant' as ref
from refgescarburant

union

select
to_simple_i18n_key(unite) as k,
unite as v, 'unite' as col, 'refgescarburant' as ref
from refgescarburant

union

select
to_simple_i18n_key(traitement_maa) as k,
traitement_maa as v, 'traitement_maa' as col, 'refgroupecibletraitement' as ref
from refgroupecibletraitement

union

select
to_simple_i18n_key(groupe_cible_maa) as k,
groupe_cible_maa as v, 'groupe_cible_maa' as col, 'refgroupecibletraitement' as ref
from refgroupecibletraitement

union

select
to_simple_i18n_key(nom_traitement) as k,
nom_traitement as v, 'nom_traitement' as col, 'refgroupecibletraitement' as ref
from refgroupecibletraitement

union

select
to_complex_i18n_key(variable_mesuree) as k,
variable_mesuree as v, 'variable_mesuree' as col, 'refmesure' as ref
from refmesure

union

select
to_complex_i18n_key(unite) as k,
unite as v, 'unite' as col, 'refmesure' as ref
from refmesure

union

select
to_simple_i18n_key(valeur_attendue) as k,
valeur_attendue as v, 'valeur_attendue' as col, 'refmesure' as ref
from refmesure

union

select
to_simple_i18n_key(reference_label) as k,
reference_label as v, 'reference_label' as col, 'reforientationedi' as ref
from reforientationedi

union

select
to_simple_i18n_key(libelle_engagement_parcelle) as k,
libelle_engagement_parcelle as v, 'libelle_engagement_parcelle' as col, 'refparcellezonageedi' as ref
from refparcellezonageedi

union

select
to_simple_i18n_key(protocole_libelle) as k,
protocole_libelle as v, 'protocole_libelle' as col, 'refprotocolevgobs' as ref
from refprotocolevgobs

union

select
to_simple_i18n_key(protocole_statut) as k,
protocole_statut as v, 'protocole_statut' as col, 'refprotocolevgobs' as ref
from refprotocolevgobs

union

select
to_simple_i18n_key(ligne_debut) as k,
ligne_debut as v, 'ligne_debut' as col, 'refprotocolevgobs' as ref
from refprotocolevgobs

union

select
to_simple_i18n_key(ligne_fin) as k,
ligne_fin as v, 'ligne_fin' as col, 'refprotocolevgobs' as ref
from refprotocolevgobs

union

select
to_simple_i18n_key(ligne_frequence) as k,
ligne_frequence as v, 'ligne_frequence' as col, 'refprotocolevgobs' as ref
from refprotocolevgobs

union

select
to_simple_i18n_key(ligne_type_echant) as k,
ligne_type_echant as v, 'ligne_type_echant' as col, 'refprotocolevgobs' as ref
from refprotocolevgobs

union

select
to_simple_i18n_key(ligne_type_observation) as k,
ligne_type_observation as v, 'ligne_type_observation' as col, 'refprotocolevgobs' as ref
from refprotocolevgobs

union

select
to_simple_i18n_key(releve_type_releve) as k,
releve_type_releve as v, 'releve_type_releve' as col, 'refprotocolevgobs' as ref
from refprotocolevgobs

union

select
to_simple_i18n_key(qualitycriterialabel) as k,
qualitycriterialabel as v, 'qualitycriterialabel' as col, 'refqualitycriteria' as ref
from refqualitycriteria

union

select
to_simple_i18n_key(applicationperiod) as k,
applicationperiod as v, 'applicationperiod' as col, 'refrcesocasegroundwater' as ref
from refrcesocasegroundwater

union

select
to_complex_i18n_key(stade_producteur) as k,
stade_producteur as v, 'stade_producteur' as col, 'refstadeedi' as ref
from refstadeedi

union

select
to_complex_i18n_key(colonne2) as k,
colonne2 as v, 'colonne2' as col, 'refstadeedi' as ref
from refstadeedi

union

select
to_simple_i18n_key(nom_trait) as k,
nom_trait as v, 'nom_trait' as col, 'reftraitsdc' as ref
from reftraitsdc

union

select
to_simple_i18n_key(reference_label) as k,
reference_label as v, 'reference_label' as col, 'reftypeagriculture' as ref
from reftypeagriculture

union

select
to_simple_i18n_key(reference_label) as k,
reference_label as v, 'reference_label' as col, 'reftypenotationedi' as ref
from reftypenotationedi

union

select
to_simple_i18n_key(reference_label) as k,
reference_label as v, 'reference_label' as col, 'refuniteedi' as ref
from refuniteedi

union

select
to_complex_i18n_key(reference_label) as k,
reference_label as v, 'reference_label' as col, 'refunitesqualifiantedi' as ref
from refunitesqualifiantedi

union

select
to_complex_i18n_key(reference_label) as k,
reference_label as v, 'reference_label' as col, 'refvaleurqualitativeedi' as ref
from refvaleurqualitativeedi

order by k asc
) as t
where v <> '');


create view liste_libelles_ref_intervention as (
select distinct on (k) k, v, col, ref from (

select
to_simple_i18n_key(reference_label) as k,
reference_label as v, 'reference_label' as col, 'refinterventionagrosysttravailedi' as ref
from refinterventionagrosysttravailedi

union

select
to_simple_i18n_key(inputedilabel)  as k,
inputedilabel as v, 'inputedilabel' as col, 'refinterventiontypeiteminputedi' as ref
from refinterventiontypeiteminputedi

order by k asc
) as t
where v <> '');
create view liste_libelles_ref_materiel as (
select distinct on (k) k, v, col, ref from (

select
to_complex_i18n_key(typemateriel1) as k, typemateriel1 as v, 'typemateriel1' as col, 'refmateriel' as ref
from refmateriel

union

select
to_complex_i18n_key(typemateriel2) as k, typemateriel2 as v, 'typemateriel2' as col, 'refmateriel' as ref
from refmateriel

union

select
to_complex_i18n_key(typemateriel3) as k, typemateriel3 as v, 'typemateriel3' as col, 'refmateriel' as ref
from refmateriel

union

select
to_complex_i18n_key(typeMateriel4) as k, typeMateriel4 as v, 'typeMateriel4' as col, 'refmateriel' as ref
from refmateriel

union

select
to_complex_i18n_key(libelle_type_materiel) as k, libelle_type_materiel as v, 'libelle_type_materiel' as col, 'refnrjgesoutil' as ref
from refnrjgesoutil

order by k asc
) as t
where v <> '');


create view liste_libelles_ref_intrant as (
select distinct on (k) k, v, col, ref from (

select
to_simple_i18n_key(forme) as k,
forme as v, 'forme' as col, 'reffertiminunifa' as ref
from reffertiminunifa

union

select to_complex_i18n_key(type_produit) as k, type_produit as v, 'type_produit' as col, 'reffertiminunifa' as ref from reffertiminunifa

union

select
to_simple_i18n_key(libelle) as k,
libelle as v, 'libelle' as col, 'reffertiorga' as ref
from reffertiorga

union

select
to_complex_i18n_key(type_engrais) as k,
type_engrais as v, 'type_engrais' as col, 'refgesengrais' as ref
from refgesengrais

union

select
to_simple_i18n_key(intitule) as k,
intitule as v, 'intitule' as col, 'refgesphyto' as ref
from refgesphyto

union

select
to_complex_i18n_key(type_engrais) as k,
type_engrais as v, 'type_engrais' as col, 'refnrjengrais' as ref
from refnrjengrais

union

select
to_simple_i18n_key(nom_sa) as k,
nom_sa as v, 'nom_sa' as col, 'refphytosubstanceactiveiphy' as ref
from refphytosubstanceactiveiphy

union

select
to_simple_i18n_key(nom_sa_acta) as k,
nom_sa_acta as v, 'nom_sa_acta' as col, 'refsaactaiphy' as ref
from refsaactaiphy

union

select
to_simple_i18n_key(nom_sa_iphy) as k,
nom_sa_iphy as v, 'nom_sa_iphy' as col, 'refsaactaiphy' as ref
from refsaactaiphy

union

select
to_simple_i18n_key(caracteristic1) as k,
caracteristic1 as v, 'caracteristic1' as col, 'refsubstrate' as ref
from refsubstrate

union

select
to_simple_i18n_key(caracteristic2) as k, caracteristic2 as v, 'caracteristic2' as col, 'refsubstrate' as ref
from refsubstrate

union

select
to_simple_i18n_key(caracteristic1) as k,
caracteristic1 as v, 'caracteristic1' as col, 'refpot' as ref
from refpot

union

select
to_simple_i18n_key(inputType_c0) as k,
inputType_c0 as v, 'inputType_c0' as col, 'refotherinput' as ref
from refotherinput

union

select
to_simple_i18n_key(caracteristic1) as k,
caracteristic1 as v, 'caracteristic1' as col, 'refotherinput' as ref
from refotherinput

union

select
to_simple_i18n_key(caracteristic2) as k,
caracteristic2 as v, 'caracteristic2' as col, 'refotherinput' as ref
from refotherinput

union

select
to_simple_i18n_key(caracteristic3) as k,
caracteristic3 as v, 'caracteristic3' as col, 'refotherinput' as ref
from refotherinput

order by k asc
) as t
where v <> '');
create view liste_libelles_ref_sol as (
select distinct on (k) k, v, col, ref from (

select
to_simple_i18n_key(surface_texture) as k,
surface_texture as v, 'surface_texture' as col, 'refrcesocasegroundwater' as ref
from refrcesocasegroundwater

union

select
to_simple_i18n_key(soil_texture) as k,
soil_texture as v, 'soil_texture' as col, 'refrcesurunoffpotrulesparc' as ref
from refrcesurunoffpotrulesparc

union

select
to_simple_i18n_key(sol_nom) as k, sol_nom as v, 'sol_nom' as col, 'refsolarvalis' as ref
from refsolarvalis

union

select
to_complex_i18n_key(sol_calcaire) as k, sol_calcaire as v, 'sol_calcaire' as col, 'refsolarvalis' as ref
from refsolarvalis

union

select
to_complex_i18n_key(sol_hydromorphie) as k, sol_hydromorphie as v, 'sol_hydromorphie' as col, 'refsolarvalis' as ref
from refsolarvalis

union

select
to_complex_i18n_key(sol_pierrosite) as k, sol_pierrosite as v, 'sol_pierrosite' as col, 'refsolarvalis' as ref
from refsolarvalis

union

select
to_complex_i18n_key(sol_profondeur) as k, sol_profondeur as v, 'sol_profondeur' as col, 'refsolarvalis' as ref
from refsolarvalis

union

select
to_complex_i18n_key(sol_texture) as k, sol_texture as v, 'sol_texture' as col, 'refsolarvalis' as ref
from refsolarvalis

union

select
to_simple_i18n_key(classe_de_profondeur_indigo) as k,
classe_de_profondeur_indigo as v, 'classe_de_profondeur_indigo' as col, 'refsolcaracteristiqueindigo' as ref
from refsolcaracteristiqueindigo

union

select
to_simple_i18n_key(classe_indigo) as k,
classe_indigo as v, 'classe_indigo' as col, 'refsolcaracteristiqueindigo' as ref
from refsolcaracteristiqueindigo

union

select
to_simple_i18n_key(classe_de_profondeur_indigo) as k,
classe_de_profondeur_indigo as v, 'classe_de_profondeur_indigo' as col, 'refsolprofondeurindigo' as ref
from refsolprofondeurindigo

union

select
to_simple_i18n_key(libelle_classe) as k,
libelle_classe as v, 'libelle_classe' as col, 'refsolprofondeurindigo' as ref
from refsolprofondeurindigo

union

select
to_simple_i18n_key(classes_texturales_gepaa) as k,
classes_texturales_gepaa as v, 'classes_texturales_gepaa' as col, 'refsoltexturegeppa' as ref
from refsoltexturegeppa

union

select
to_simple_i18n_key(classe_indigo) as k,
classe_indigo as v, 'classe_indigo' as col, 'refsoltexturegeppa' as ref
from refsoltexturegeppa

union

select
to_simple_i18n_key(texture_iphy) as k,
texture_iphy as v, 'texture_iphy' as col, 'refsoltexturegeppa' as ref
from refsoltexturegeppa

order by k asc
) as t
where v <> '');


create view liste_libelles_ref_vivant as (
select distinct on (k) k, v, col, ref from (

select
to_simple_i18n_key(adventice) as k, adventice as v, 'adventice' as col, 'refadventice' as ref
from refadventice

union

select
to_simple_i18n_key(famille_de_culture) as k, famille_de_culture as v, 'famille_de_culture' as col, 'refadventice' as ref
from refadventice

union

select
to_simple_i18n_key(reference_label) as k, reference_label as v, 'reference_label' as col, 'refnuisibleedi' as ref
from refnuisibleedi

union

select
to_simple_i18n_key(libelle_categorie_de_cultures) as k, libelle_categorie_de_cultures as v, 'libelle_categorie_de_cultures' as col, 'refespece' as ref
from refespece

union

select
to_simple_i18n_key(libelle_espece_botanique) as k, libelle_espece_botanique as v, 'libelle_espece_botanique' as col, 'refespece' as ref
from refespece

union

select
to_simple_i18n_key(libelle_qualifiant_aee) as k, libelle_qualifiant_aee as v, 'libelle_qualifiant_aee' as col, 'refespece' as ref
from refespece

union

select
to_simple_i18n_key(libelle_cipan_aee) as k, libelle_cipan_aee as v, 'libelle_cipan_aee' as col, 'refespece' as ref
from refespece

union

select
to_simple_i18n_key(libelle_destination_bbch) as k, libelle_destination_bbch as v, 'libelle_destination_bbch' as col, 'refespece' as ref
from refespece

union

select
to_simple_i18n_key(genre) as k, genre as v, 'genre' as col, 'refespece' as ref
from refespece

union

select
to_simple_i18n_key(espece) as k, espece as v, 'espece' as col, 'refespece' as ref
from refespece

union

select
to_simple_i18n_key(nom_gnis) as k, nom_gnis as v, 'nom_gnis' as col, 'refespece' as ref
from refespece

union

select
to_simple_i18n_key(nom_latin_gnis) as k, nom_latin_gnis as v, 'nom_latin_gnis' as col, 'refespece' as ref
from refespece

union

select
to_simple_i18n_key(nom_culture_acta) as k, nom_culture_acta as v, 'nom_culture_acta' as col, 'refespece' as ref
from refespece

union

select
to_simple_i18n_key(nom_groupe) as k, nom_groupe as v, 'nom_groupe' as col, 'refvarietegeves' as ref
from refvarietegeves

union

select
to_simple_i18n_key(nom_section) as k, nom_section as v, 'nom_section' as col, 'refvarietegeves' as ref
from refvarietegeves

union

select
to_simple_i18n_key(nom_botanique) as k, nom_botanique as v, 'nom_botanique' as col, 'refvarietegeves' as ref
from refvarietegeves

union

select
to_simple_i18n_key(nom_francais) as k, nom_francais as v, 'nom_francais' as col, 'refvarietegeves' as ref
from refvarietegeves

union

select
to_simple_i18n_key(libelle_type_varietal) as k, libelle_type_varietal as v, 'libelle_type_varietal' as col, 'refvarietegeves' as ref
from refvarietegeves

union

select
to_simple_i18n_key(libelle_ploidie) as k, libelle_ploidie as v, 'libelle_ploidie' as col, 'refvarietegeves' as ref
from refvarietegeves

union

select
to_simple_i18n_key(variete) as k, variete as v, 'variete' as col, 'refvarieteplantgrape' as ref
from refvarieteplantgrape

union

select
to_complex_i18n_key(animaltype) as k, animaltype as v, 'animaltype' as col, 'refcattleanimaltype' as ref
from refcattleanimaltype

union

select
to_simple_i18n_key(alimenttype) as k, alimenttype as v, 'alimenttype' as col, 'refcattlerationaliment' as ref
from refcattlerationaliment

union

select
to_simple_i18n_key(cible_edi_ref_label) as k, cible_edi_ref_label as v, 'cible_edi_ref_label' as col, 'refciblesagrosystgroupesciblesmaa' as ref
from refciblesagrosystgroupesciblesmaa

union

select
to_simple_i18n_key(groupe_cible_maa) as k, groupe_cible_maa as v, 'groupe_cible_maa' as col, 'refciblesagrosystgroupesciblesmaa' as ref
from refciblesagrosystgroupesciblesmaa

union

select
to_simple_i18n_key(libelle_espece_botanique) as k, libelle_espece_botanique as v, 'libelle_espece_botanique' as col, 'refcultureedigroupecouvsol' as ref
from refcultureedigroupecouvsol

union

select
to_simple_i18n_key(libelle_qualifiant_aee) as k, libelle_qualifiant_aee as v, 'libelle_qualifiant_aee' as col, 'refcultureedigroupecouvsol' as ref
from refcultureedigroupecouvsol

union

select
to_simple_i18n_key(culture_maa) as k, culture_maa as v, 'culture_maa' as col, 'refculturemaa' as ref
from refculturemaa

union

select
to_simple_i18n_key(ligne_organisme_vivant) as k, ligne_organisme_vivant as v, 'ligne_organisme_vivant' as col, 'refprotocolevgobs' as ref
from refprotocolevgobs

union

select
to_complex_i18n_key(classe) as k, classe as v, 'classe' as col, 'refqualitycriteriaclass' as ref
from refqualitycriteriaclass

union

select
to_simple_i18n_key(famille_de_culture) as k, famille_de_culture as v, 'famille_de_culture' as col, 'refstadeedi' as ref
from refstadeedi

union

select
to_complex_i18n_key(reference_label) as k, reference_label as v, 'reference_label' as col, 'refstadenuisibleedi' as ref
from refstadenuisibleedi

union

select
to_simple_i18n_key(reference_label) as k, reference_label as v, 'reference_label' as col, 'refsupportorganeedi' as ref
from refsupportorganeedi

order by k asc
) as t
where v <> '');

create view liste_libelles_ref as (
select 'destination' as i18nEntryClass, * from liste_libelles_ref_destination
union all
select 'divers' as i18nEntryClass, * from liste_libelles_ref_divers
union all
select 'intervention' as i18nEntryClass, * from liste_libelles_ref_intervention
union all
select 'materiel' as i18nEntryClass, * from liste_libelles_ref_materiel
union all
select 'intrant' as i18nEntryClass, * from liste_libelles_ref_intrant
union all
select 'sol' as i18nEntryClass, * from liste_libelles_ref_sol
union all
select 'vivant' as i18nEntryClass, * from liste_libelles_ref_vivant
);
