---
-- %%Ignore-License
---

ALTER TABLE public.doeindicators_indicatorfilter ADD CONSTRAINT fk_doeindicators_indicatorfilter_indicatorfilter_topiaid FOREIGN KEY (owner) REFERENCES indicatorfilter(topiaid);
ALTER TABLE public.indicatorfilter_mineralfertilizations ADD CONSTRAINT fk_indicatorfilter_mineralfertilizations_indicatorfilter_topiaid FOREIGN KEY (owner) REFERENCES indicatorfilter(topiaid);
ALTER TABLE public.indicatorfilter_organicfertilizations ADD CONSTRAINT fk_indicatorfilter_organicfertilizations_indicatorfilter_topiaid FOREIGN KEY (owner) REFERENCES indicatorfilter(topiaid);
ALTER TABLE public.activesubstances_indicatorfilter ADD CONSTRAINT fk_activesubstances_indicatorfilter_indicatorfilter_topiaid FOREIGN KEY (owner) REFERENCES indicatorfilter(topiaid);
ALTER TABLE public.indicatorfilter_organicproducts ADD CONSTRAINT fk_indicatorfilter_organicproducts_indicatorfilter_topiaid FOREIGN KEY (owner) REFERENCES indicatorfilter(topiaid);