---
-- %%Ignore-License
---

alter table echelle_intrant add column deprecatedqtavg double precision;
alter table echelle_intrant add column deprecatedunit text;
alter table echelle_intrant add column deprecatedid text;
