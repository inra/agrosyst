---
-- %%Ignore-License
---

alter table price drop constraint if exists fk_dq93uhomdsui2leh8dce7eoo6;
alter table price drop constraint if exists fk_i7803ttqfaffbxy31n9yp3m4k;
alter table price drop constraint if exists fk_tha0hbckimueah926ud688th1;
alter table harvestingactionvalorisation drop constraint if exists fki10m8mh9utr4g23yy64wuhajv;
alter table harvestingactionvalorisation drop constraint if exists FKhkwcsx5yf2cnqjifmebtgtucl;

alter table harvestingactionvalorisation drop column if exists price;

DELETE FROM harvestingactionvalorisation hav WHERE NOT EXISTS (SELECT 1 FROM abstractaction aa WHERE aa.topiaid = hav.harvestingaction);
DELETE FROM price p WHERE p.category = 'HARVESTING_ACTION_PRICE_CATEGORIE' AND NOT EXISTS (SELECT 1 FROM harvestingactionvalorisation hav WHERE hav.topiaid = p.harvestingactionvalorisation);

CREATE TABLE harvestingprice AS SELECT topiaid,topiaversion,objectid,sourceunit,harvestingactionvalorisation,domain,topiacreatedate,displayname,price,practicedsystem,null as zone,priceunit
FROM price where category = 'HARVESTING_ACTION_PRICE_CATEGORIE';

CREATE INDEX IF NOT EXISTS harvestingPrice_harvestingactionvalorisation_idx ON harvestingprice (harvestingactionvalorisation);

CREATE TEMPORARY TABLE harvestingpricedomain AS (
    (
        SELECT
        hp.topiaid AS topiaid, gp.domain AS domain, null as zone
        FROM harvestingactionvalorisation hav
        INNER JOIN harvestingprice hp ON hp.harvestingactionvalorisation = hav.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
    )

UNION ALL
    (
        SELECT
        hp.topiaid AS topiaid, gp.domain AS domain, null as zone
        FROM harvestingactionvalorisation hav
        INNER JOIN harvestingprice hp ON hp.harvestingactionvalorisation = hav.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
    )

UNION ALL
    (
        SELECT
        hp.topiaid AS topiaid, plot.domain AS domain, zone.topiaId as zone
        FROM harvestingactionvalorisation hav
        INNER JOIN harvestingprice hp ON hp.harvestingactionvalorisation = hav.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
    )
UNION ALL

    (
        SELECT
        hp.topiaid AS topiaid, plot.domain AS domain, zone.topiaId as zone
        FROM harvestingactionvalorisation hav
        INNER JOIN harvestingprice hp ON hp.harvestingactionvalorisation = hav.topiaid
        INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
    )
);

CREATE INDEX ON harvestingPriceDomain (topiaid);
CREATE INDEX harvestingPrice_topiaid_idx ON harvestingprice (topiaid);

UPDATE harvestingPrice hp SET domain = (
    SELECT
      pricetodomain.domain
    FROM harvestingPriceDomain pricetodomain
    WHERE pricetodomain.topiaid = hp.topiaid)
WHERE hp.domain IS null AND hp.zone IS null;

UPDATE harvestingPrice hp SET zone = (
    SELECT
      pricetodomain.zone
    FROM harvestingPriceDomain pricetodomain
    WHERE pricetodomain.topiaid = hp.topiaid)
WHERE hp.zone IS null;

UPDATE harvestingprice hp0 SET objectid = (
    SELECT
      replace(json_build_array(re.libelle_espece_botanique, re.code_qualifiant_aee, re.code_type_saisonnier_aee, re.code_destination_aee, CASE WHEN cps.variety IS NOT NULL THEN (CONCAT((SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety))) ELSE '' END, rd.code_destination_a, hav.isorganiccrop)::text,', ',',') AS objectid
    FROM harvestingactionvalorisation hav
    INNER JOIN harvestingprice hp1 ON hp1.harvestingactionvalorisation = hav.topiaid
    INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
    INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
    INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
    INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
    INNER JOIN zone zone ON zone.topiaid = epcc.zone
    INNER JOIN plot plot ON zone.plot = plot.topiaid
    INNER JOIN croppingplanspecies cps ON cps.code = hav.speciescode
    INNER JOIN refespece re ON re.topiaid = cps.species
    INNER JOIN refdestination rd ON hav.destination = rd.topiaid
    INNER JOIN effectivespeciesstade ess ON ess.effectiveintervention = ei.topiaid
    WHERE ess.croppingplanspecies = cps.topiaid
    AND hp0.topiaid = hp1.topiaid)
    WHERE hp0.topiaid IN (
    SELECT
        hp2.topiaid
        FROM harvestingactionvalorisation hav1
        INNER JOIN harvestingprice hp2 ON hp2.harvestingactionvalorisation = hav1.topiaid
        INNER JOIN abstractaction aa1 ON aa1.topiaid = hav1.harvestingaction
        INNER JOIN effectiveintervention ei1 ON ei1.topiaid = aa1.effectiveintervention
        INNER JOIN effectivecropcyclephase phase1 ON phase1.topiaid = ei1.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc1 ON epcc1.phase = phase1.topiaid
        INNER JOIN zone zone1 ON zone1.topiaid = epcc1.zone
        INNER JOIN plot plot1 ON zone1.plot = plot1.topiaid
        INNER JOIN croppingplanspecies cps1 ON cps1.code = hav1.speciescode
        INNER JOIN refespece re1 ON re1.topiaid = cps1.species
        INNER JOIN refdestination rd1 ON hav1.destination = rd1.topiaid
        INNER JOIN effectivespeciesstade ess1 ON ess1.effectiveintervention = ei1.topiaid
        WHERE ess1.croppingplanspecies = cps1.topiaid
        AND hp0.topiaid = hp2.topiaid
    );

UPDATE harvestingprice hp0 SET objectid = (
   SELECT
     replace(json_build_array(re.libelle_espece_botanique, re.code_qualifiant_aee, re.code_type_saisonnier_aee, re.code_destination_aee, CASE WHEN cps.variety IS NOT NULL THEN (CONCAT((SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety))) ELSE '' END, rd.code_destination_a, hav.isorganiccrop)::text,', ',',') AS objectid
  FROM harvestingactionvalorisation hav
  INNER JOIN harvestingprice hp1 ON hp1.harvestingactionvalorisation = hav.topiaid
  INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN croppingplanspecies cps ON cps.code = hav.speciescode
  INNER JOIN refespece re ON re.topiaid = cps.species
  INNER JOIN refdestination rd ON hav.destination = rd.topiaid
  INNER JOIN effectivespeciesstade ess ON ess.effectiveintervention = ei.topiaid
  WHERE ess.croppingplanspecies = cps.topiaid
  AND hp0.topiaid = hp1.topiaid)
  WHERE hp0.topiaid IN (
      SELECT
        hp2.topiaid
        FROM harvestingactionvalorisation hav1
        INNER JOIN harvestingprice hp2 ON hp2.harvestingactionvalorisation = hav1.topiaid
        INNER JOIN abstractaction aa1 ON aa1.topiaid = hav1.harvestingaction
        INNER JOIN effectiveintervention ei1 ON ei1.topiaid = aa1.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn1 ON eccn1.topiaid = ei1.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc1 ON escc1.topiaid = eccn1.effectiveseasonalcropcycle
        INNER JOIN zone zone1 ON zone1.topiaid = escc1.zone
        INNER JOIN plot plot1 ON zone1.plot = plot1.topiaid
        INNER JOIN croppingplanspecies cps1 ON cps1.code = hav1.speciescode
        INNER JOIN refespece re1 ON re1.topiaid = cps1.species
        INNER JOIN refdestination rd1 ON hav1.destination = rd1.topiaid
        INNER JOIN effectivespeciesstade ess1 ON ess1.effectiveintervention = ei1.topiaid
        WHERE ess1.croppingplanspecies = cps1.topiaid
        AND hp0.topiaid = hp2.topiaid
  );

UPDATE harvestingprice hp0 SET objectid = (
  SELECT
    replace(json_build_array(re.libelle_espece_botanique, re.code_qualifiant_aee, re.code_type_saisonnier_aee, re.code_destination_aee, CASE WHEN cps.variety IS NOT NULL THEN (CONCAT((SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety))) ELSE '' END, rd.code_destination_a, hav.isorganiccrop)::text,', ',',')
  FROM harvestingactionvalorisation hav
  INNER JOIN harvestingprice hp1 ON hp1.harvestingactionvalorisation = hav.topiaid
  INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
  INNER JOIN croppingplanspecies cps ON cps.code = hav.speciescode
  INNER JOIN refespece re ON re.topiaid = cps.species
  INNER JOIN refdestination rd ON hav.destination = rd.topiaid
  INNER JOIN croppingplanentry cpe ON cps.croppingplanentry = cpe.topiaid
  INNER join domain d ON cpe.domain = d.topiaid
  INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  WHERE hp0.topiaid = hp1.topiaid limit 1)
  WHERE hp0.topiaid IN (
    SELECT
    hp2.topiaid
    FROM harvestingactionvalorisation hav1
    INNER JOIN harvestingprice hp2 ON hp2.harvestingactionvalorisation = hav1.topiaid
    INNER JOIN abstractaction aa1 ON aa1.topiaid = hav1.harvestingaction
    INNER JOIN practicedintervention pi1 ON pi1.topiaid = aa1.practicedintervention
    INNER JOIN practicedcropcyclephase pccp1 ON pccp1.topiaid = pi1.practicedcropcyclephase
    INNER JOIN practicedcropcycle pcc1 ON pcc1.topiaid = pccp1.practicedperennialcropcycle
    INNER JOIN croppingplanspecies cps1 ON cps1.code = hav1.speciescode
    INNER JOIN refespece re1 ON re1.topiaid = cps1.species
    INNER JOIN refdestination rd1 ON hav1.destination = rd1.topiaid
    INNER JOIN croppingplanentry cpe1 ON cps1.croppingplanentry = cpe1.topiaid
    INNER join domain d1 ON cpe1.domain = d1.topiaid
    INNER JOIN practicedsystem ps1 ON pcc1.practicedsystem = ps1.topiaid
    WHERE hp0.topiaid = hp2.topiaid
  );

UPDATE harvestingprice hp0 SET objectid = (
  SELECT
    replace(json_build_array(re.libelle_espece_botanique, re.code_qualifiant_aee, re.code_type_saisonnier_aee, re.code_destination_aee, CASE WHEN cps.variety IS NOT NULL THEN (CONCAT((SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety))) ELSE '' END, rd.code_destination_a, hav.isorganiccrop)::text,', ',',')
  FROM harvestingactionvalorisation hav
  INNER JOIN harvestingprice hp1 ON hp1.harvestingactionvalorisation = hav.topiaid
  INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
  INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
  INNER JOIN croppingplanspecies cps ON cps.code = hav.speciescode
  INNER JOIN croppingplanentry cpe ON cps.croppingplanentry = cpe.topiaid
  INNER join domain d ON cpe.domain = d.topiaid
  INNER JOIN refespece re ON re.topiaid = cps.species
  INNER JOIN refdestination rd ON hav.destination = rd.topiaid
  INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  INNER JOIN growingplan gp ON (gs.growingplan = gp.topiaid AND gp.domain = d.topiaid)
  WHERE hp0.topiaid = hp1.topiaid)
  WHERE hp0.topiaid IN (
  SELECT
      hp2.topiaid
    FROM harvestingactionvalorisation hav1
    INNER JOIN harvestingprice hp2 ON hp2.harvestingactionvalorisation = hav1.topiaid
    INNER JOIN abstractaction aa1 ON aa1.topiaid = hav1.harvestingaction
    INNER JOIN practicedintervention pi1 ON pi1.topiaid = aa1.practicedintervention
    INNER JOIN practicedcropcycleconnection pccc1 ON pccc1.topiaid = pi1.practicedcropcycleconnection
    INNER JOIN practicedcropcyclenode pccn1 ON pccn1.topiaid = pccc1.target
    INNER JOIN practicedcropcycle pcc1 ON pcc1.topiaid = pccn1.practicedseasonalcropcycle
    INNER JOIN croppingplanspecies cps1 ON cps1.code = hav1.speciescode
    INNER JOIN croppingplanentry cpe1 ON cps1.croppingplanentry = cpe1.topiaid
    INNER join domain d1 ON cpe1.domain = d1.topiaid
    INNER JOIN refespece re1 ON re1.topiaid = cps1.species
    INNER JOIN refdestination rd1 ON hav1.destination = rd1.topiaid
    INNER JOIN practicedsystem ps1 ON pcc1.practicedsystem = ps1.topiaid
    INNER JOIN growingsystem gs1 ON ps1.growingsystem = gs1.topiaid
    INNER JOIN growingplan gp1 ON (gs1.growingplan = gp1.topiaid AND gp1.domain = d1.topiaid)
    WHERE hp0.topiaid = hp2.topiaid
  );

UPDATE harvestingprice set topiaid = 'fr.inra.agrosyst.api.entities.Harvestingprice_' || split_part(topiaid, '_', 2);

-- Ajout prix de Récolte manquants sur cultures perenne du réalisé
INSERT INTO harvestingprice
  SELECT
    CONCAT('fr.inra.agrosyst.api.entities.HarvestingPrice_', uuid_in(md5(CONCAT(hav.topiaid::text, replace(json_build_array(aa.topiaid, hav.speciescode, hav.topiaid)::text,', ',','), now()::text))::cstring))  AS topiaid
    , 0                                                                                                                                AS topiaversion
    , replace(json_build_array(re.libelle_espece_botanique, re.code_qualifiant_aee, re.code_type_saisonnier_aee, re.code_destination_aee, CASE WHEN cps.variety IS NOT NULL THEN (CONCAT((SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety))) ELSE '' END, rd.code_destination_a, hav.isorganiccrop)::text,', ',',')                                               AS objectid
    , null                                                                                                                             AS sourceunit
    , hav.topiaid                                                                                                                      AS harvestingactionvalorisation
    , plot.domain                                                                                                                      AS domain
    , (SELECT CURRENT_TIMESTAMP(3)::timestamp without time zone)                                                                       AS topiacreatedate
    , CONCAT(
        re.libelle_espece_botanique,
        CASE WHEN re.libelle_qualifiant_AEE IS NOT NULL AND re.libelle_qualifiant_AEE != '' THEN CONCAT(' ', re.libelle_qualifiant_AEE) ELSE NULL END,
        CASE WHEN cps.variety IS NOT NULL THEN (CONCAT(' (', (SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety),')')) ELSE NULL END
      )                                                                                                                                AS displayname
    , null                                                                                                                             AS price
    , null                                                                                                                             AS practicedsystem
    , zone.topiaid                                                                                                                     AS zone
    , 'EURO_HA'                                                                                                                        AS priceunit
  FROM harvestingactionvalorisation hav
  INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN croppingplanspecies cps ON cps.code = hav.speciescode
  INNER JOIN refespece re ON re.topiaid = cps.species
  INNER JOIN refdestination rd ON hav.destination = rd.topiaid
  INNER JOIN effectivespeciesstade ess ON ess.effectiveintervention = ei.topiaid
  WHERE ess.croppingplanspecies = cps.topiaid
  AND NOT EXISTS (SELECT 1 FROM harvestingprice hp WHERE hp.harvestingactionvalorisation = hav.topiaid);

-- Ajout prix de Récolte manquants sur cultures saisonnière du réalisé
INSERT INTO harvestingprice
  SELECT
    CONCAT('fr.inra.agrosyst.api.entities.HarvestingPrice_', uuid_in(md5(CONCAT(hav.topiaid::text, replace(json_build_array(aa.topiaid, hav.speciescode, hav.topiaid)::text,', ',','), now()::text))::cstring))  AS topiaid
    , 0                                                                                                                                AS topiaversion
    , replace(json_build_array(re.libelle_espece_botanique, re.code_qualifiant_aee, re.code_type_saisonnier_aee, re.code_destination_aee, CASE WHEN cps.variety IS NOT NULL THEN (CONCAT((SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety))) ELSE '' END, rd.code_destination_a, hav.isorganiccrop)::text,', ',',')                                               AS objectid
    , null                                                                                                                             AS sourceunit
    , hav.topiaid                                                                                                                      AS harvestingactionvalorisation
    , plot.domain                                                                                                                      AS domain
    , (SELECT CURRENT_TIMESTAMP(3)::timestamp without time zone)                                                                       AS topiacreatedate
    , CONCAT(
        re.libelle_espece_botanique,
        CASE WHEN re.libelle_qualifiant_AEE IS NOT NULL AND re.libelle_qualifiant_AEE != '' THEN CONCAT(' ', re.libelle_qualifiant_AEE) ELSE NULL END,
        CASE WHEN cps.variety IS NOT NULL THEN (CONCAT(' (', (SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety),')')) ELSE NULL END
      )                                                                                                                                AS displayname
    , null                                                                                                                             AS price
    , null                                                                                                                             AS practicedsystem
    , zone.topiaid                                                                                                                     AS zone
    , 'EURO_HA'                                                                                                                        AS priceunit
  FROM harvestingactionvalorisation hav
  INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN croppingplanspecies cps ON cps.code = hav.speciescode
  INNER JOIN refespece re ON re.topiaid = cps.species
  INNER JOIN refdestination rd ON hav.destination = rd.topiaid
  INNER JOIN effectivespeciesstade ess ON ess.effectiveintervention = ei.topiaid
  WHERE ess.croppingplanspecies = cps.topiaid
  AND NOT EXISTS (SELECT 1 FROM harvestingprice hp WHERE hp.harvestingactionvalorisation = hav.topiaid);

-- Ajout prix de Récolte manquants sur cultures perenne du synthétisé
INSERT INTO harvestingprice
  SELECT
    CONCAT('fr.inra.agrosyst.api.entities.HarvestingPrice_', uuid_in(md5(CONCAT(hav.topiaid::text, replace(json_build_array(aa.topiaid, hav.speciescode, hav.topiaid)::text,', ',','), now()::text))::cstring))  AS topiaid
    , 0                                                                                                                                AS topiaversion
    , replace(json_build_array(re.libelle_espece_botanique, re.code_qualifiant_aee, re.code_type_saisonnier_aee, re.code_destination_aee, CASE WHEN cps.variety IS NOT NULL THEN (CONCAT((SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety))) ELSE '' END, rd.code_destination_a, hav.isorganiccrop)::text,', ',',')                                               AS objectid
    , null                                                                                                                             AS sourceunit
    , hav.topiaid                                                                                                                      AS harvestingactionvalorisation
    , d.topiaid                                                                                                                        AS domain
    , (SELECT CURRENT_TIMESTAMP(3)::timestamp without time zone)                                                                       AS topiacreatedate
    , CONCAT(
        re.libelle_espece_botanique,
        CASE WHEN re.libelle_qualifiant_AEE IS NOT NULL AND re.libelle_qualifiant_AEE != '' THEN CONCAT(' ', re.libelle_qualifiant_AEE) ELSE NULL END,
        CASE WHEN cps.variety IS NOT NULL THEN (CONCAT(' (', (SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety),')')) ELSE NULL END
      )                                                                                                                                AS displayname
    , null                                                                                                                             AS price
    , pcc.practicedsystem                                                                                                              AS practicedsystem
    , null                                                                                                                             AS zone
    , 'EURO_HA'                                                                                                                        AS priceunit
  FROM harvestingactionvalorisation hav
  INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
  INNER JOIN croppingplanspecies cps ON cps.code = hav.speciescode
  INNER JOIN refespece re ON re.topiaid = cps.species
  INNER JOIN refdestination rd ON hav.destination = rd.topiaid
  INNER JOIN croppingplanentry cpe ON cps.croppingplanentry = cpe.topiaid
  INNER join domain d ON cpe.domain = d.topiaid
  INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  WHERE NOT EXISTS (SELECT 1 FROM harvestingprice hp WHERE hp.harvestingactionvalorisation = hav.topiaid);

-- Ajout prix de Récolte manquants sur cultures saisonnière du synthétisé
INSERT INTO harvestingprice
  SELECT
    CONCAT('fr.inra.agrosyst.api.entities.HarvestingPrice_', uuid_in(md5(CONCAT(hav.topiaid::text, replace(json_build_array(aa.topiaid, hav.speciescode, hav.topiaid)::text,', ',','), now()::text))::cstring))  AS topiaid
    , 0                                                                                                                                AS topiaversion
    , replace(json_build_array(re.libelle_espece_botanique, re.code_qualifiant_aee, re.code_type_saisonnier_aee, re.code_destination_aee, CASE WHEN cps.variety IS NOT NULL THEN (CONCAT((SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety))) ELSE '' END, rd.code_destination_a, hav.isorganiccrop)::text,', ',',')                                               AS objectid
    , null                                                                                                                             AS sourceunit
    , hav.topiaid                                                                                                                      AS harvestingactionvalorisation
    , d.topiaid                                                                                                                        AS domain
    , (SELECT CURRENT_TIMESTAMP(3)::timestamp without time zone)                                                                       AS topiacreatedate
    , CONCAT(
        re.libelle_espece_botanique,
        CASE WHEN re.libelle_qualifiant_AEE IS NOT NULL AND re.libelle_qualifiant_AEE != '' THEN CONCAT(' ', re.libelle_qualifiant_AEE) ELSE NULL END,
        CASE WHEN cps.variety IS NOT NULL THEN (CONCAT(' (', (SELECT rvg.denomination FROM refvarietegeves rvg WHERE rvg.topiaid = cps.variety), (SELECT rvpg.variete FROM refvarieteplantgrape rvpg WHERE rvpg.topiaid = cps.variety),')')) ELSE NULL END
      )                                                                                                                                AS displayname
    , null                                                                                                                             AS price
    , pcc.practicedsystem                                                                                                              AS practicedsystem
    , null                                                                                                                             AS zone
    , 'EURO_HA'                                                                                                                        AS priceunit
  FROM harvestingactionvalorisation hav
  INNER JOIN abstractaction aa ON aa.topiaid = hav.harvestingaction
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
  INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
  INNER JOIN croppingplanspecies cps ON cps.code = hav.speciescode
  INNER JOIN croppingplanentry cpe ON cps.croppingplanentry = cpe.topiaid
  INNER join domain d ON cpe.domain = d.topiaid
  INNER JOIN refespece re ON re.topiaid = cps.species
  INNER JOIN refdestination rd ON hav.destination = rd.topiaid
  INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  INNER JOIN growingplan gp ON (gs.growingplan = gp.topiaid AND gp.domain = d.topiaid)
  WHERE NOT EXISTS (SELECT 1 FROM harvestingprice hp WHERE hp.harvestingactionvalorisation = hav.topiaid);

CREATE INDEX IF NOT EXISTS harvestingPrice_practicedsystem_idx ON harvestingprice(practicedsystem);
CREATE INDEX IF NOT EXISTS harvestingPrice_domain_idx ON harvestingprice(domain);
CREATE INDEX IF NOT EXISTS harvestingPrice_zone_idx ON harvestingprice(zone);

-- suppression des doublons, plusieurs prix pour une valorisation
DELETE FROM harvestingprice
  WHERE topiaid IN
      (SELECT topiaid
       FROM
          (SELECT topiaid,
           ROW_NUMBER() OVER( PARTITION BY harvestingactionvalorisation
          ORDER BY  price ) AS row_num
          FROM harvestingprice ) t
          WHERE t.row_num > 1 );

alter table harvestingprice add constraint uK_55dnctmd6pb6ho2nujrm7r4gx unique (harvestingactionvalorisation);
alter table harvestingprice add constraint FKl0kt7shhwea4c8raulg69sx2q foreign key (harvestingactionvalorisation) references harvestingactionvalorisation;
alter table harvestingprice add constraint FKqmytmwvqvcpqk7k6y6uct7k5l foreign key (domain) references domain;
alter table harvestingprice add constraint FKsyyfcx9jvwcy1ptp5j314xv62 foreign key (practicedsystem) references practicedsystem;
alter table harvestingprice add constraint FKlc4c9ctw7xgppoee4sv7ab3q7 foreign key (zone) references zone;
