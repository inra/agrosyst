---
-- %%Ignore-License
--
--

alter table refotherinput drop constraint IF EXISTS uk2k47d62th9dyunm1crpgbosfm;
alter table refotherinput add column reference_id text NOT NULL;
alter table refotherinput add column reference_code text;
alter table refotherinput add constraint refotherinput_undique UNIQUE (reference_id);

drop index if exists refprixautre_undique_idx;
alter table refprixautre add column reference_id text NOT NULL;
alter table refprixautre add column reference_code text;
alter table refprixautre add constraint refprixautre_undique UNIQUE (reference_id);
