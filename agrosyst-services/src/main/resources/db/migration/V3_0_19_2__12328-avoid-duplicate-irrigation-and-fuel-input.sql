---
-- %%Ignore-License
---

-- IRRIGATION

-- crée la table des doublons, on priorise les intrant avec un prix
create table _12328_domainirrigationinputtomigrate as (
SELECT * FROM (
	SELECT d.name, d.campaign, adi.domain as domain, adi.topiaid as id, adi.code, null as unique_id, ROW_NUMBER () OVER (PARTITION BY CONCAT (adi.domain) ORDER BY adi.topiacreatedate, adi.inputprice nulls last)
	FROM domainirrigationinput di
	inner join abstractdomaininputstockunit adi on di.topiaid = adi.topiaid
	inner join domain d on d.topiaid = adi.domain
	) as s
WHERE ROW_NUMBER > 1);

-- on référence un unique intrant
update _12328_domainirrigationinputtomigrate ditm set unique_id = unique_input_id.unique_id
from (
  select dii.topiaid unique_id, adi."domain" as dom
  from domainirrigationinput dii
  inner join abstractdomaininputstockunit adi on adi.topiaid = dii.topiaid
  order by adi.topiacreatedate, adi.topiaid
) as unique_input_id
where ditm.domain  = unique_input_id.dom;

-- on migre les usages sur un même intrant
update irrigationinputusage iiu set domainirrigationinput = r0.unique_id
from(
  select id, unique_id from _12328_domainirrigationinputtomigrate diitu
) r0
where iiu.domainirrigationinput = r0.id;

-- suppréssion de l'intrant d'irrigation de domainirrigationinput
delete from domainirrigationinput dii
where dii.topiaid in (select ditm.id from _12328_domainirrigationinputtomigrate ditm)
and dii.topiaid not in (select domainirrigationinput from irrigationinputusage);

-- suppréssion du prix de l'intrant d'irrigation (il n'y en avait pas lors des tests)
delete from inputprice ip where ip.topiaid in
(select dii.inputprice from abstractdomaininputstockunit dii
where dii.topiaid in (select ditm.id from _12328_domainirrigationinputtomigrate ditm)
and dii.topiaid not in (select domainirrigationinput from irrigationinputusage)
);

-- suppréssion de l'intrant d'irrigation de abstractdomaininputstockunit
delete from abstractdomaininputstockunit dii
where dii.topiaid in (select ditm.id from _12328_domainirrigationinputtomigrate ditm)
and dii.topiaid not in (select domainirrigationinput from irrigationinputusage);

-- CARBURANT

-- crée la table des doublons, on priorise les intrant avec un prix
CREATE TABLE _12328_domainfuelinputtomigrate AS (
  SELECT * FROM
    (
    SELECT
      d.name, d.campaign, adi.domain AS DOMAIN, adi.topiaid AS id, adi.code,
      ROW_NUMBER () OVER (PARTITION BY CONCAT (adi.domain)
    ORDER BY adi.topiacreatedate, adi.inputprice NULLS LAST)
    FROM domainfuelinput di
    INNER JOIN abstractdomaininputstockunit adi ON di.topiaid = adi.topiaid
    INNER JOIN DOMAIN d ON d.topiaid = adi.domain
    ) AS s
WHERE
	ROW_NUMBER > 1);

-- suppréssion de l'intrant carburant de domainfuelinput
DELETE FROM domainfuelinput dii
WHERE dii.topiaid IN (
	SELECT ditm.id
	FROM _12328_domainfuelinputtomigrate ditm);

-- suppréssion du prix de l'intrant carburant (il n'y en avait pas lors des tests)
DELETE FROM inputprice ip
WHERE ip.topiaid IN
(
	SELECT dii.inputprice
	FROM abstractdomaininputstockunit dii
	WHERE dii.topiaid IN (
		SELECT ditm.id
		FROM _12328_domainfuelinputtomigrate ditm));

-- suppréssion de l'intrant carburant de abstractdomaininputstockunit
DELETE
FROM abstractdomaininputstockunit dii
WHERE dii.topiaid IN (
	SELECT ditm.id
	FROM _12328_domainfuelinputtomigrate ditm);