---
-- %%Ignore-License
---

ALTER TABLE synthetise_echelle_intervention ADD COLUMN nombre_UTH_necessaires float8 NULL;

ALTER TABLE synthetise_echelle_culture ADD COLUMN nombre_UTH_necessaires float8 NULL;

ALTER TABLE synthetise_echelle_synthetise ADD COLUMN nombre_UTH_necessaires float8 NULL;

ALTER TABLE realise_echelle_intervention ADD COLUMN nombre_UTH_necessaires float8 NULL;

ALTER TABLE realise_echelle_culture ADD COLUMN nombre_UTH_necessaires float8 NULL;

ALTER TABLE realise_echelle_itk ADD COLUMN nombre_UTH_necessaires float8 NULL;

ALTER TABLE realise_echelle_parcelle ADD COLUMN nombre_UTH_necessaires float8 NULL;

ALTER TABLE realise_echelle_sdc ADD COLUMN nombre_UTH_necessaires float8 NULL;

ALTER TABLE realise_echelle_zone ADD COLUMN nombre_UTH_necessaires float8 NULL;
