---
-- %%Ignore-License
---

-- add missing relation between biological product input and usage
insert into _12084_input_to_usage (id_usage, id_input)
with _12084_tmp_input_to_usage as (
    select
    ai.topiaid as ai_topiaid,
    ai.inputtype as ai_inputtype,
    (select concat (aa0.effectiveintervention, aa0.practicedintervention)
            from abstractaction aa0
            where concat(ai.pesticidesspreadingaction, ai.biologicalcontrolaction) = aa0.topiaid) as ai_intervention,
    NULLIF(ai.phytoproduct, '') as ai_phytoproduct,
    coalesce (NULLIF(ai.phytoproductunit, ''), 'KG_HA') as ai_unit,
    coalesce (ai.qtavg, 0) as ai_qtavg,
    NULLIF(ai.productname, '') as ai_productname,
    concat(ai.pesticidesspreadingaction, ai.biologicalcontrolaction) as ai_action,
    (select array_to_string (
      array
       (select p.target
        from phytoproducttarget p
        where p.phytoproductinput = ai.topiaid
        order by target), ', ')) as ai_target
    FROM abstractinput ai
    WHERE ai.inputtype IN ('LUTTE_BIOLOGIQUE', 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES')
  )
  select
    aiu.topiaid as id_usage,
    ai.ai_topiaid as id_input
    FROM biologicalproductinputusage piu
    inner join abstractphytoproductinputusage appiu on piu.topiaid = appiu.topiaid
    inner join abstractinputusage aiu on aiu.topiaid = appiu.topiaid
    inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
    inner join abstractdomaininputstockunit adis on adis.topiaid = dppi.topiaid
    INNER join _12084_tmp_input_to_usage ai on ai.ai_action = piu.biologicalcontrolaction
    where ai.ai_inputtype IN ('LUTTE_BIOLOGIQUE', 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES')
    and ai.ai_phytoproduct = dppi.refinput
    and ai.ai_unit = dppi.usageunit
    and ai.ai_qtavg = coalesce (aiu.qtavg, 0)
    and coalesce (ai.ai_productname, aiu.productname) = aiu.productname
    and ai.ai_target =
        (
          array_to_string (
            array
             (select p.target from phytoproducttarget p where p.abstractphytoproductinputusage = aiu.topiaid order by target), ', ')
        )
;

-- remove duplicated entry
with _12084_duplicate_usage_for_input as (
  select id_usage, count(id_usage) from (
    select distinct
    id_usage,
    id_input
    FROM _12084_input_to_usage
  ) t group by id_usage having count(id_usage)>1)
delete from _12084_input_to_usage itu
where itu.id_usage in (
  select itu.id_usage from _12084_duplicate_usage_for_input dui
  inner join _12084_input_to_usage itu on itu.id_usage = dui.id_usage
  where split_part(dui.id_usage, '_', 2) = split_part(itu.id_input, '_', 2)
)
and split_part(itu.id_usage, '_', 2) != split_part(itu.id_input, '_', 2);

-- add to abstractinputusage deprecatedid
update abstractinputusage aiu set deprecatedid = (select au.id_input from _12084_input_to_usage au where aiu.topiaid = au.id_usage order by aiu.topiaid limit 1 )
where exists (select 1 from _12084_input_to_usage au where aiu.topiaid = au.id_usage)
and aiu.deprecatedid is null;

-- add to abstractinputusage deprecatedqtavg
update abstractinputusage aiu set deprecatedqtavg = (
  select ai.qtavg
  from abstractinput ai where ai.topiaid = aiu.deprecatedid)
where aiu.deprecatedid is not null
and aiu.deprecatedqtavg is null;

-- add to abstractinputusage deprecatedunit
update abstractinputusage aiu set deprecatedunit = (
  select NULLIF(concat(ai.phytoproductunit, ai.organicproductunit, ai.mineralproductunit, ai.otherproductinputunit, ai.substrateinputunit, ai.potinputunit), '')
  from abstractinput ai where ai.topiaid = aiu.deprecatedid)
where aiu.deprecatedid is not null
and aiu.deprecatedunit is null;

