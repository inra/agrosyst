---
-- %%Ignore-License
---

-- UPDATE abstractdomaininputstockunit adis0
--  SET code = old_code
--  FROM (
--     SELECT input_id,
--            old_code,
--            nv_code
--     FROM _12291_abstractdomaininputstockunit_code_backup) as tmp_res
--   WHERE tmp_res.input_id = adis0.topiaid;
--
-- drop table abstractdomaininputstockunit_code_backup;

CREATE TABLE _12291_abstractdomaininputstockunit_code_backup AS (
  select t.domain,
         t.input_id,
         t.code as old_code,
         uuid_in(md5(CONCAT(t.input_id::text, t.code, now()::text))::cstring) nv_code
  FROM
    (SELECT adis."domain",
            adis.topiaid as input_id,
            adis.code as code,
            ROW_NUMBER() OVER( PARTITION BY adis."domain", adis.code) AS row_num
     FROM abstractdomaininputstockunit adis
     ) t
  WHERE t.row_num > 1
);

UPDATE abstractdomaininputstockunit adis0
SET code = nv_code
FROM (
  SELECT input_id,
         old_code,
         nv_code
  FROM _12291_abstractdomaininputstockunit_code_backup) as tmp_res
WHERE tmp_res.input_id = adis0.topiaid;