---
-- %%Ignore-License
---

UPDATE abstractdomaininputstockunit adi0 SET code = uuid_in(md5(random()::text || random()::text)::cstring)
WHERE adi0.topiaid IN (
  SELECT s.id FROM (
    SELECT adi.topiaID as id, ROW_NUMBER () OVER (PARTITION BY CONCAT (adi.domain, adi.code)
    ORDER BY adi.topiacreatedate DESC)
    FROM abstractdomaininputstockunit adi ) AS s WHERE ROW_NUMBER > 1);