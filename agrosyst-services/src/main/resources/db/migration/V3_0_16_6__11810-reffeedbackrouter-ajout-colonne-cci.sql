---
-- %%Ignore-License
---

alter table reffeedbackrouter add column cci text;

update reffeedbackrouter
set cci = 'SM-AgrosystSupport@inrae.fr'
where
	typedephy in ('DEPHY_EXPE', 'DEPHY_FERME') and
	feedbackcategory in ('CONSIGNES_SAISIES', 'FONCTIONNEMENT_AGROSYST', 'EVOLUTION')
;
