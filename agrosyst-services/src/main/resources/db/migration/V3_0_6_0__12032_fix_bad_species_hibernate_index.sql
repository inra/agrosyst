---
-- %%Ignore-License
---

UPDATE croppingplanspecies cps0 SET croppingplanentry_idx = (SELECT count(*) FROM croppingplanspecies cps1 WHERE cps1.croppingplanentry = cps0.croppingplanentry) - 1
WHERE cps0.topiaid IN (
    SELECT topiaid
        FROM
            (SELECT topiaid,
            ROW_NUMBER() OVER( PARTITION BY croppingplanentry, croppingplanentry_idx) AS row_num
            FROM croppingplanspecies ) t
            WHERE t.row_num > 1
    );