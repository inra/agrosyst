---
-- %%Ignore-License
---

insert into trad_ref_vivant values ('fra', 'fr.inra.agrosyst.api.entities.CompagneType.PLANTE_COMPAGNE', 'Plante compagne') ON CONFLICT (lang,key) DO NOTHING;
insert into trad_ref_vivant values ('fra', 'fr.inra.agrosyst.api.entities.CompagneType.COUVERT_INTER_RANG', 'Couvert inter-rang') ON CONFLICT (lang,key) DO NOTHING;
insert into trad_ref_vivant values ('fra', 'fr.inra.agrosyst.api.entities.CompagneType.COUVERT_SUR_RANG', 'Couvert sur le rang') ON CONFLICT (lang,key) DO NOTHING;

insert into trad_ref_vivant values ('eng', 'fr.inra.agrosyst.api.entities.CompagneType.PLANTE_COMPAGNE', 'Companion plant') ON CONFLICT (lang,key) DO NOTHING;
insert into trad_ref_vivant values ('eng', 'fr.inra.agrosyst.api.entities.CompagneType.COUVERT_INTER_RANG', 'Inter-row cover') ON CONFLICT (lang,key) DO NOTHING;
insert into trad_ref_vivant values ('eng', 'fr.inra.agrosyst.api.entities.CompagneType.COUVERT_SUR_RANG', 'Covered on the row') ON CONFLICT (lang,key) DO NOTHING;
