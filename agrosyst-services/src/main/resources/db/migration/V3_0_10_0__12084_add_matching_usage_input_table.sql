---
-- %%Ignore-License
---

CREATE index IF NOT EXISTS _12084_abstractinput_pesticidesspreadingaction_idx ON abstractinput(pesticidesspreadingaction);
CREATE index IF NOT EXISTS _12084_abstractinput_biologicalcontrolaction_idx ON abstractinput(biologicalcontrolaction);
CREATE index IF NOT EXISTS _12084_phytoproducttarget_phytoproductinput_idx ON phytoproducttarget(phytoproductinput);
CREATE index IF NOT EXISTS _12084_abstractinput_harvestingaction_idx ON abstractinput(harvestingaction);
CREATE index IF NOT EXISTS _12084_abstractinput_irrigationaction_idx ON abstractinput(irrigationaction);
CREATE index IF NOT EXISTS _12084_abstractinput_maintenancepruningvinesaction_idx ON abstractinput(maintenancepruningvinesaction);
CREATE index IF NOT EXISTS _12084_abstractinput_mineralfertilizersspreadingaction_idx ON abstractinput(mineralfertilizersspreadingaction);
CREATE index IF NOT EXISTS _12084_abstractinput_organicfertilizersspreadingaction_idx ON abstractinput(organicfertilizersspreadingaction);
CREATE index IF NOT EXISTS _12084_abstractinput_otheraction_idx ON abstractinput(otheraction);
CREATE index IF NOT EXISTS _12084_abstractinput_pesticidesspreadingaction_idx ON abstractinput(pesticidesspreadingaction);
CREATE index IF NOT EXISTS _12084_abstractinput_seedingaction_idx ON abstractinput(seedingaction);

create table _12084_abstract_input_backup as (select * from abstractinput);

delete from abstractinput where topiaid in (
  SELECT topiaid
     FROM
        (SELECT topiaid,
                ROW_NUMBER() OVER( PARTITION BY
                  topiadiscriminator,
                  coalesce (qtmin,0),
                  coalesce (qtavg,0),
                  coalesce (qtmed,0),
                  coalesce (qtmax,0),
                  NULLIF(concat (productname), ''),
                  inputtype,
                  producttype,
                  NULLIF(concat (ai.phytoproduct), ''),
                  coalesce (NULLIF(concat (ai.phytoproductunit), ''), 'KG_HA'),
                  coalesce (NULLIF(concat (ai.oldproductqtunit), ''), 'KG_HA'),
                  NULLIF(concat (ai.otheraction), ''),
                  coalesce (ai.n,0),
                  coalesce (ai.p2o5,0),
                  coalesce (ai.k2o,0),
                  NULLIF(concat (ai.organicproduct), ''),
                  NULLIF(concat (ai.organicfertilizersspreadingaction), ''),
                  coalesce (NULLIF(concat (ai.organicproductunit), ''), 'KG_HA'),
                  NULLIF(concat (ai.mineralproduct), ''),
                  coalesce (NULLIF(concat (ai.mineralproductunit), ''), 'KG_HA'),
                  NULLIF(concat (ai.mineralfertilizersspreadingaction), ''),
                  NULLIF(concat (ai.pesticidesspreadingaction), ''),
                  NULLIF(concat (ai.seedingaction), ''),
                  NULLIF(concat (ai.biologicalcontrolaction), ''),
                  NULLIF(concat (ai.harvestingaction), ''),
                  NULLIF(concat (ai.irrigationaction), ''),
                  NULLIF(concat (ai.maintenancepruningvinesaction), ''),
                  phytoeffect,
                  coalesce (NULLIF(concat (ai.otherproductinputunit), ''), 'KG_HA'),
                  NULLIF(concat (ai.substrate), ''),
                  coalesce (NULLIF(concat (ai.substrateinputunit), ''), 'KG_HA'),
                  NULLIF(concat (ai.pot), ''),
                  coalesce (NULLIF(concat (ai.potinputunit), ''), 'KG_HA'),
                  (select array_to_string (
                      array
                       (select p.target from phytoproducttarget p where p.phytoproductinput = ai.topiaid order by target), ', '))
          ) AS row_num
        FROM abstractinput ai
        order by ai.topiaversion DESC) t
        WHERE t.row_num > 1);

create temporary table _12084_tmp_input_to_usage as (
  select
    ai.topiaid as ai_topiaid,
    ai.inputtype as ai_inputtype,
    (select concat (aa0.effectiveintervention, aa0.practicedintervention)
            from abstractaction aa0
            where concat(ai.pesticidesspreadingaction, ai.biologicalcontrolaction) = aa0.topiaid) as ai_intervention,
    NULLIF(ai.phytoproduct, '') as ai_phytoproduct,
    coalesce (NULLIF(ai.phytoproductunit, ''), 'KG_HA') as ai_unit,
    coalesce (ai.qtavg, 0) as ai_qtavg,
    NULLIF(ai.productname, '') as ai_productname,
    concat(ai.pesticidesspreadingaction, ai.biologicalcontrolaction) as ai_action,
    (select array_to_string (
      array
       (select p.target
        from phytoproducttarget p
        where p.phytoproductinput = ai.topiaid
        order by target), ', ')) as ai_target
    FROM abstractinput ai
    WHERE ai.inputtype IN ('LUTTE_BIOLOGIQUE', 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES')
);

CREATE index IF NOT EXISTS _12084_tmp_input_to_usage_abstractinput_ai_topiaid_idx ON _12084_tmp_input_to_usage(ai_topiaid);
CREATE index IF NOT EXISTS _12084_tmp_input_to_usage_abstractinput_ai_action_idx ON _12084_tmp_input_to_usage(ai_action);
CREATE index IF NOT EXISTS _12084_tmp_input_to_usage_target_input_idx ON _12084_tmp_input_to_usage(ai_target);
CREATE index IF NOT EXISTS _12084_tmp_input_to_usage_intervention_phytoproductinput_idx ON _12084_tmp_input_to_usage(ai_intervention);

create table _12084_input_to_usage as (
  select
    aiu.topiaid as id_usage,
    ai.ai_topiaid as id_input
    FROM pesticideproductinputusage piu
    inner join abstractphytoproductinputusage appiu on piu.topiaid = appiu.topiaid
    inner join abstractinputusage aiu on aiu.topiaid = appiu.topiaid
    inner join domainphytoproductinput dppi on dppi.topiaid = appiu.domainphytoproductinput
    inner join abstractdomaininputstockunit adis on adis.topiaid = dppi.topiaid
    INNER join _12084_tmp_input_to_usage ai on ai.ai_action = piu.pesticidesspreadingaction
    where ai.ai_inputtype IN ('LUTTE_BIOLOGIQUE', 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES')
    and ai.ai_phytoproduct = dppi.refinput
    and ai.ai_unit = dppi.usageunit
    and ai.ai_qtavg = coalesce (aiu.qtavg, 0)
    and coalesce (ai.ai_productname, aiu.productname) = aiu.productname
    and ai.ai_target =
        (
          array_to_string (
            array
             (select p.target from phytoproducttarget p where p.abstractphytoproductinputusage = aiu.topiaid order by target), ', ')
        )
  union (
    select distinct
    aiu.topiaid as id_usage,
    ai.topiaid as id_input
    FROM abstractphytoproductinputusage appiu
    inner join seedproductinputusage spiu on spiu.topiaid = appiu.topiaid
    inner join abstractinputusage aiu on spiu.topiaid = aiu.topiaid
    inner join seedspeciesinputusage ssiu on ssiu.topiaid = spiu.seedspeciesinputusage
    inner join domainseedspeciesinput dssi on ssiu.domainseedspeciesinput = dssi.topiaid and (dssi.chemicaltreatment or dssi.biologicalseedinoculation)
    inner join seedlotinputusage sliu on sliu.topiaid = ssiu.seedlotinputusage
    inner join abstractaction sau on sau.topiaid = sliu.seedingactionusage
    inner join domainphytoproductinput dppi ON dppi.topiaid = appiu.domainphytoproductinput
    inner join croppingplanspecies ucps on ucps.topiaid = dssi.speciesseed
    inner join abstractaction iaa on (iaa.practicedintervention = sau.practicedintervention or iaa.effectiveintervention = sau.effectiveintervention)
    inner join seedingactionspecies sas on sas.seedingaction = iaa.topiaid
    INNER join abstractinput ai on ai.seedingaction = iaa.topiaid
    where ai.inputtype = 'SEMIS'
    and coalesce (NULLIF(ai.phytoproduct, ''), 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_0000000-0000-0000-0000-000000000000') = dppi.refinput
    and dppi.usageunit = coalesce (ai.phytoproductunit, 'KG_HA')
    and coalesce (aiu.qtavg, 0) = coalesce (ai.qtavg, 0)
    and coalesce (NULLIF(ai.productname, ''), aiu.productname) = aiu.productname
    and (sas.treatment or sas.biologicalseedinoculation)
    and sas.speciescode = ucps.code
    and (select array_to_string (
        array
         (select p.target from phytoproducttarget p where p.phytoproductinput = ai.topiaid order by target), ', ')) =
        (
          array_to_string (
            array
             (select p.target from phytoproducttarget p where p.abstractphytoproductinputusage = aiu.topiaid order by target), ', ')
        )
  )
  union (
    select
    aiu.topiaid as id_usage,
    ai.topiaid as id_input
    FROM mineralproductinputusage piu
    inner join abstractinputusage aiu on aiu.topiaid = piu.topiaid
    inner join domainmineralproductinput dpi on dpi.topiaid = piu.domainmineralproductinput
    INNER join abstractinput ai on ai.mineralfertilizersspreadingaction = piu.mineralfertilizersspreadingaction
    where ai.inputtype = ('APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX')
    and ai.mineralproduct = dpi.refinput
    and coalesce (NULLIF(concat (ai.mineralproductunit), ''), 'KG_HA') = dpi.usageunit
    and coalesce (ai.qtavg,0) = coalesce (aiu.qtavg, 0)
    and coalesce (NULLIF(ai.productname, ''), aiu.productname) = aiu.productname
    and ai.phytoeffect = dpi.phytoeffect
  )
  union (
    select
    aiu.topiaid as id_usage,
    ai.topiaid as id_input
    FROM organicproductinputusage piu
    inner join abstractinputusage aiu on aiu.topiaid = piu.topiaid
    inner join domainorganicproductinput dpi on dpi.topiaid = piu.domainorganicproductinput
    INNER join abstractinput ai on ai.organicfertilizersspreadingaction  = piu.organicfertilizersspreadingaction
    where ai.inputtype = 'EPANDAGES_ORGANIQUES'
    and coalesce (NULLIF(concat (ai.organicproduct), ''),'fr.inra.agrosyst.api.entities.referential.RefFertiOrga_0000000-0000-0000-0000-000000000000') = dpi.refinput
    and coalesce (NULLIF(concat (ai.organicproductunit), ''), 'KG_HA') = dpi.usageunit
    and coalesce (ai.qtavg,0) = coalesce (aiu.qtavg, 0)
    and coalesce (ai.n,0) = coalesce (dpi.n, 0)
    and coalesce (ai.p2o5,0) = coalesce (dpi.p2o5, 0)
    and coalesce (ai.k2o,0) = coalesce (dpi.k2o, 0)
    and coalesce (NULLIF(ai.productname, ''), aiu.productname) = aiu.productname
  )
  union(
    select
    aiu.topiaid as id_usage,
    ai.topiaid as id_input
    FROM potinputusage piu
    inner join abstractinputusage aiu on aiu.topiaid = piu.topiaid
    inner join domainpotinput dpi on dpi.topiaid = piu.domainpotinput
    INNER join abstractinput ai on ai.otheraction = piu.otheraction
    where ai.inputtype = 'POT'
    and coalesce (NULLIF(concat (ai.pot), ''), 'fr.inra.agrosyst.api.entities.referential.RefPot_0000000-0000-0000-0000-000000000000') = dpi.refinput
    and coalesce (NULLIF(concat (ai.potinputunit), ''), 'KG_HA') = dpi.usageunit
    and coalesce (ai.qtavg,0) = coalesce (aiu.qtavg, 0)
    and coalesce (NULLIF(ai.productname, ''), aiu.productname) = aiu.productname
   )
   union (
    select
     aiu.topiaid as id_usage,
     ai.topiaid as id_input
     FROM substrateinputusage piu
     inner join abstractinputusage aiu on aiu.topiaid = piu.topiaid
     inner join domainsubstrateinput dpi on dpi.topiaid = piu.domainsubstrateinput
     INNER join abstractinput ai on ai.otheraction = piu.otheraction
     where ai.inputtype = 'SUBSTRAT'
     and coalesce (NULLIF(concat (ai.substrate), ''), 'fr.inra.agrosyst.api.entities.referential.RefSubstrate_0000000-0000-0000-0000-000000000000') = dpi.refinput
     and coalesce (NULLIF(concat (ai.substrateinputunit), ''), 'KG_HA') = dpi.usageunit
     and coalesce (ai.qtavg,0) = coalesce (aiu.qtavg, 0)
     and coalesce (NULLIF(ai.productname, ''), aiu.productname) = aiu.productname
   )
   union (
    select
     aiu.topiaid as id_usage,
     ai.topiaid as id_input
     FROM otherproductinputusage piu
     inner join abstractinputusage aiu on aiu.topiaid = piu.topiaid
     inner join domainotherinput dpi on dpi.topiaid = piu.domainotherinput
     INNER join abstractinput ai on concat(piu.biologicalcontrolaction , piu.harvestingaction, piu.irrigationaction, piu.maintenancepruningvinesaction, piu.mineralfertilizersspreadingaction, piu.organicfertilizersspreadingaction, piu.otheraction, piu.pesticidesspreadingaction, piu.seedingaction) =
            concat(ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction, ai.mineralfertilizersspreadingaction, ai.organicfertilizersspreadingaction, ai.otheraction, ai.pesticidesspreadingaction, ai.seedingaction)
     where ai.inputtype = 'AUTRE'
     and coalesce (NULLIF(concat (ai.otherproductinputunit), ''), 'KG_HA') = dpi.usageunit
     and coalesce (ai.qtavg,0) = coalesce (aiu.qtavg, 0)
     and coalesce (NULLIF(ai.productname, ''), aiu.productname) = aiu.productname
   )
 )
;

create index _12084_input_to_usage_id_usage on _12084_input_to_usage(id_usage);
create index _12084_input_to_usage_id_input on _12084_input_to_usage(id_input);

create temporary table _12084_duplicate_usage_for_input as (
  select id_usage, count(id_usage) from (
    select distinct
    id_usage,
    id_input
    FROM _12084_input_to_usage
  ) t group by id_usage having count(id_usage)>1);

delete from _12084_input_to_usage itu
where itu.id_usage in (
  select itu.id_usage from _12084_duplicate_usage_for_input dui
  inner join _12084_input_to_usage itu on itu.id_usage = dui.id_usage
  where split_part(dui.id_usage, '_', 2) = split_part(itu.id_input, '_', 2)
)
and split_part(itu.id_usage, '_', 2) != split_part(itu.id_input, '_', 2);


