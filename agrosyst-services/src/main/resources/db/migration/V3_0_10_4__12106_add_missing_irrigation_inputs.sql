---
-- %%Ignore-License
---

CREATE TABLE _12106_script_date AS (SELECT CURRENT_TIMESTAMP(3)::timestamp without time zone AS topiacreatedate);

create table _12106_missing_irrigation as (
SELECT
        null as input_topiaid,
        0 as topiaversion,
        (SELECT topiacreatedate from  _12106_script_date) as topiacreatedate,
        'MM' as key,
        'Eau' as inputname,
        'IRRIGATION' as inputtype,
        d.topiaid as domain,
        null as inputprice,
        d.code as code,
        'MM'as usageunit,
        aa.waterquantitymin as qtmin,
        aa.waterquantitymax as qtmax,
        aa.waterquantityaverage as qtavg,
        aa.waterquantitymedian as qtmed,
        aa.topiaid as action,
        concat('fr.inra.agrosyst.api.entities.action.IrrigationInputUsage_', split_part(aa.topiaid, '_', 2)) as usage_id
        FROM abstractaction aa
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        inner join domain d on gp.domain = d.topiaid
        where aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.IrrigationActionImpl'
        and aa.irrigationinputusage is null
UNION ALL
    (   SELECT
        null as input_topiaid,
        0 as topiaversion,
        (SELECT topiacreatedate from  _12106_script_date) as topiacreatedate,
        'MM' as key,
        'Eau' as inputname,
        'IRRIGATION' as inputtype,
        d.topiaid as domain,
        null as inputprice,
        d.code as code,
        'MM'as usageunit,
        aa.waterquantitymin as qtmin,
        aa.waterquantitymax as qtmax,
        aa.waterquantityaverage as qtavg,
        aa.waterquantitymedian as qtmed,
        aa.topiaid as action,
        concat('fr.inra.agrosyst.api.entities.action.IrrigationInputUsage_', split_part(aa.topiaid, '_', 2)) as usage_id
        FROM abstractaction aa
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        inner join domain d on gp.domain = d.topiaid
        where aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.IrrigationActionImpl'
        and aa.irrigationinputusage is null
    )
UNION ALL
    (
        SELECT
        null as input_topiaid,
        0 as topiaversion,
        (SELECT topiacreatedate from  _12106_script_date) as topiacreatedate,
        'MM' as key,
        'Eau' as inputname,
        'IRRIGATION' as inputtype,
        d.topiaid as domain,
        null as inputprice,
        d.code as code,
        'MM'as usageunit,
        aa.waterquantitymin as qtmin,
        aa.waterquantitymax as qtmax,
        aa.waterquantityaverage as qtavg,
        aa.waterquantitymedian as qtmed,
        aa.topiaid as action,
        concat('fr.inra.agrosyst.api.entities.action.IrrigationInputUsage_', split_part(aa.topiaid, '_', 2)) as usage_id
        FROM abstractaction aa
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        inner join domain d on plot.domain = d.topiaid
        where aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.IrrigationActionImpl'
        and aa.irrigationinputusage is null
    )
UNION ALL
    (
        SELECT
        null as input_topiaid,
        0 as topiaversion,
        (SELECT topiacreatedate from  _12106_script_date) as topiacreatedate,
        'MM' as key,
        'Eau' as inputname,
        'IRRIGATION' as inputtype,
        d.topiaid as domain,
        null as inputprice,
        d.code as code,
        'MM'as usageunit,
        aa.waterquantitymin as qtmin,
        aa.waterquantitymax as qtmax,
        aa.waterquantityaverage as qtavg,
        aa.waterquantitymedian as qtmed,
        aa.topiaid as action,
        concat('fr.inra.agrosyst.api.entities.action.IrrigationInputUsage_', split_part(aa.topiaid, '_', 2)) as usage_id
        FROM abstractaction aa
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        inner join domain d on plot.domain = d.topiaid
        where aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.IrrigationActionImpl'
        and aa.irrigationinputusage is null
    )
);

create table _12106_abstractdomaininputstockunit as (
select distinct
  concat('fr.inra.agrosyst.api.entities.DomainIrrigationInput', uuid_in(md5(random()::text || random()::text)::cstring)) as topiaid,
  topiaversion,
  topiacreatedate,
  key, inputname,
  inputtype,
  domain,
  inputprice,
  code,
  usageunit
  from _12106_missing_irrigation group by input_topiaid, topiaversion, topiacreatedate, key, inputname, inputtype, domain, inputprice, code, usageunit);

update _12106_missing_irrigation mi set input_topiaid = adisu.topiaid
from (select distinct adi.topiaid, adi.code from _12106_abstractdomaininputstockunit adi) as adisu
where mi.code = adisu.code;

insert into abstractdomaininputstockunit (topiaid, topiaversion, topiacreatedate, key, inputname, inputtype, domain, inputprice, code)
(
  SELECT topiaid, topiaversion, topiacreatedate, key, inputname, inputtype, domain, inputprice, code FROM _12106_abstractdomaininputstockunit
);

insert into domainirrigationinput (topiaid, usageunit)
(
  SELECT topiaid, usageunit FROM _12106_abstractdomaininputstockunit
);

insert into abstractinputusage (topiaid, topiaversion, topiacreatedate, productname, qtmin, qtavg, qtmed, qtmax, inputtype, code)
(
 SELECT
   usage_id as topiaid,
   topiaversion,
   topiacreatedate,
   mi.inputname as productname,
   qtmin,
   qtavg,
   qtmed,
   qtmax,
   inputtype,
   uuid_in(md5(random()::text || random()::text)::cstring) as code
 from _12106_missing_irrigation mi
);

insert into irrigationinputusage (topiaid, domainirrigationinput)
(
 SELECT
   usage_id as topiaid,
   input_topiaid as domainirrigationinput
 from _12106_missing_irrigation mi
);

update abstractaction aa set irrigationinputusage = mi.usage_id
from (select mi0.usage_id, mi0.action from _12106_missing_irrigation mi0) as mi
where mi.action = aa.topiaid;









