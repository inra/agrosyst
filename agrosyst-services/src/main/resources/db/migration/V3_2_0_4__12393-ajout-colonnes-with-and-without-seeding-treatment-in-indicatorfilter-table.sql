---
-- %%Ignore-License
---

ALTER TABLE indicatorfilter ADD COLUMN withseedingtreatment boolean NULL;

ALTER TABLE indicatorfilter ADD COLUMN withoutseedingtreatment boolean NULL;
