---
-- %%Ignore-License
---

-- id_produit = a_AAAOX
update domainphytoproductinput
set refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_3d827001-27cc-44a9-af93-52d2450a8d6f'
where refinput in (
  'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_f0aeafd3-bf00-4653-8fd8-78499bb58360',
  'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_04eaf94e-1111-4fb0-882a-39649d7cb2fc',
  'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_912d8395-d8dd-44c4-b89d-bebbf87e8976'
);

-- id_produit = a_AAAOY
update domainphytoproductinput
set refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_2b7ec7a1-8a7f-45ec-ad4f-40b4c45126ca'
where refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_3c364d81-e19a-430a-b060-89003035eec4';

-- id_produit = a_AAAPB
update domainphytoproductinput
set refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_06c62689-5f89-4ed4-a166-dc86917dd77a'
where refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_8381b37f-dfdc-44b0-9eb4-d3b9223d1e83';

-- id_produit = a_AAAPC
update domainphytoproductinput
set refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_f18e7828-9a85-4242-b20c-7d47472a8649'
where refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_9a5b6d00-10c7-4a58-852a-3ce9139c739a';

-- id_produit = a_AAAPE
update domainphytoproductinput
set refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_0e128bf7-1f2c-4f25-88a8-cb761d87b383'
where refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_4c88e7f6-95d0-406d-8296-060f26a4a7c2';

-- id_produit = maaf_1190082 ou maaf_1190082_A
update domainphytoproductinput
set refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_d59bcc4b-e1c5-4eab-bd47-35ab23c327c7'
where refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_d6c8d19e-536d-412a-9ed5-c857485ece57';

-- id_produit = maaf_2170220 ou maaf_2170220_A
update domainphytoproductinput
set refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_fd1a8975-18b1-401a-8aae-c230ddb6c9b2'
where refinput = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_e05baa77-7e29-4da8-93b7-37f78a8b7cff';


-- Suppression des lignes de doublons qui ne sont désormais plus utilisées
delete from refactatraitementsproduit
where topiaid in (
  'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_f0aeafd3-bf00-4653-8fd8-78499bb58360',
  'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_04eaf94e-1111-4fb0-882a-39649d7cb2fc',
  'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_912d8395-d8dd-44c4-b89d-bebbf87e8976',
  'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_3c364d81-e19a-430a-b060-89003035eec4',
  'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_8381b37f-dfdc-44b0-9eb4-d3b9223d1e83',
  'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_9a5b6d00-10c7-4a58-852a-3ce9139c739a',
  'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_4c88e7f6-95d0-406d-8296-060f26a4a7c2',
  'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_d6c8d19e-536d-412a-9ed5-c857485ece57',
  'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_85922ec2-da4e-4adb-956f-128d069154ce',
  'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_7de426ce-09ab-4a77-a65f-74fb02cffe40',
  'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_e05baa77-7e29-4da8-93b7-37f78a8b7cff'
);
