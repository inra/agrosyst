---
-- %%Ignore-License
---

CREATE TEMPORARY TABLE v3_0_5_0__12019_radd_missing_irrigation_domain_input_script_dat AS (SELECT CURRENT_TIMESTAMP(3)::timestamp without time zone AS topiacreatedate);

CREATE TEMPORARY TABLE irrig_action_missing_usage AS (
  SELECT topiaid, waterquantitymin, waterquantityaverage, waterquantitymedian, waterquantitymax, practicedintervention, effectiveintervention
  FROM abstractaction aa
  WHERE COALESCE(aa.irrigationinputusage, '###'::text) = '###' and aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.IrrigationActionImpl'
);

CREATE TEMPORARY TABLE irrig_action_existing_usage AS (
  SELECT topiaid, waterquantitymin, waterquantityaverage, waterquantitymedian, waterquantitymax, practicedintervention, effectiveintervention
  FROM abstractaction aa
  WHERE COALESCE(aa.irrigationinputusage, '###'::text) != '###' and aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.IrrigationActionImpl'
  LIMIT 10
);


CREATE TEMPORARY TABLE missing_domain_irrig AS (
  (
    SELECT
      0 topiaversion,
      (SELECT topiacreatedate FROM v3_0_5_0__12019_radd_missing_irrigation_domain_input_script_dat) AS topiacreatedate,
      'MM' AS key,
      'Eau d’irrigation' AS inputname,
      'IRRIGATION' AS inputtype,
      uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring) AS code,
      gp.domain AS domain,
      CONCAT('fr.inra.agrosyst.api.entities.DomainIrrigationInput_', uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring))  AS topiaid
        FROM irrig_action_missing_usage aa
        --FROM irrig_action_existing_usage aa
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where not exists (select 1 from abstractdomaininputstockunit dis where dis.domain = gp.domain and dis.inputtype = 'IRRIGATION')
  )
  UNION ALL
  (
    SELECT
      0 topiaversion,
      (SELECT topiacreatedate FROM v3_0_5_0__12019_radd_missing_irrigation_domain_input_script_dat) AS topiacreatedate,
      'MM' AS key,
      'Eau d’irrigation' AS inputname,
      'IRRIGATION' AS inputtype,
      uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring) AS code,
      gp.domain AS domain,
      CONCAT('fr.inra.agrosyst.api.entities.DomainIrrigationInput_', uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring))  AS topiaid
        FROM irrig_action_missing_usage aa
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        where not exists (select 1 from abstractdomaininputstockunit dis where dis.domain = gp.domain and dis.inputtype = 'IRRIGATION')
  )
  UNION ALL
  (
    SELECT
      0 topiaversion,
      (SELECT topiacreatedate FROM v3_0_5_0__12019_radd_missing_irrigation_domain_input_script_dat) AS topiacreatedate,
      'MM' AS key,
      'Eau d’irrigation' AS inputname,
      'IRRIGATION' AS inputtype,
      uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring) AS code,
      plot.domain AS domain,
      CONCAT('fr.inra.agrosyst.api.entities.DomainIrrigationInput_', uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring))  AS topiaid
        FROM irrig_action_missing_usage aa
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where not exists (select 1 from abstractdomaininputstockunit dis where dis.domain = plot.domain and dis.inputtype = 'IRRIGATION')
  )
  UNION ALL
  (
    SELECT
      0 topiaversion,
      (SELECT topiacreatedate FROM v3_0_5_0__12019_radd_missing_irrigation_domain_input_script_dat) AS topiacreatedate,
      'MM' AS key,
      'Eau d’irrigation' AS inputname,
      'IRRIGATION' AS inputtype,
      uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring) AS code,
      plot.domain AS domain,
      CONCAT('fr.inra.agrosyst.api.entities.DomainIrrigationInput_', uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring))  AS topiaid
        FROM irrig_action_missing_usage aa
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        where not exists (select 1 from abstractdomaininputstockunit dis where dis.domain = plot.domain and dis.inputtype = 'IRRIGATION')
  )
);

DELETE FROM missing_domain_irrig etc0 WHERE topiaid IN (
SELECT topiaid FROM missing_domain_irrig
  WHERE topiaid IN
      (SELECT topiaid
       FROM
          (SELECT topiaid,
           ROW_NUMBER() OVER( PARTITION BY domain
          ORDER BY domain) AS row_num
          FROM missing_domain_irrig ) t
          WHERE t.row_num > 1));

INSERT INTO abstractdomaininputstockunit (topiaversion, topiacreatedate, key, inputname, inputtype, code, domain, topiaid)
  SELECT topiaversion, topiacreatedate, key, inputname, inputtype, code, domain, topiaid FROM missing_domain_irrig;

INSERT INTO domainirrigationinput (topiaid , usageunit)
  SELECT topiaid, key FROM missing_domain_irrig;

CREATE TEMPORARY TABLE action_with_missing_irrig_usage AS (
  (
    SELECT
      aa.topiaid AS irrig_action,
      0 AS topiaversion,
      (SELECT topiacreatedate FROM v3_0_5_0__12019_radd_missing_irrigation_domain_input_script_dat) AS topiacreatedate,
      gp.domain AS domain,
      CONCAT('fr.inra.agrosyst.api.entities.action.IrrigationInputUsage_', uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring))  AS topiaid,
      dis.topiaid as domainirrigationinput,
      aa.waterquantitymin AS qtmin,
      aa.waterquantityaverage AS qtavg,
      aa.waterquantitymedian AS qtmed,
      aa.waterquantitymax AS qtmax,
      'IRRIGATION' AS inputtype,
      'Eau d’irrigation' AS productname,
      uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring) AS code
        FROM irrig_action_missing_usage aa
        --FROM irrig_action_existing_usage aa
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
        INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        LEFT JOIN abstractdomaininputstockunit dis ON dis.domain = gp.domain and dis.inputtype = 'IRRIGATION'
  )
  UNION ALL
  (
    SELECT
      aa.topiaid AS irrig_action,
      0 AS topiaversion,
      (SELECT topiacreatedate FROM v3_0_5_0__12019_radd_missing_irrigation_domain_input_script_dat) AS topiacreatedate,
      gp.domain AS domain,
      CONCAT('fr.inra.agrosyst.api.entities.action.IrrigationInputUsage_', uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring))  AS topiaid,
      dis.topiaid as domainirrigationinput,
      aa.waterquantitymin AS qtmin,
      aa.waterquantityaverage AS qtavg,
      aa.waterquantitymedian AS qtmed,
      aa.waterquantitymax AS qtmax,
      'IRRIGATION' AS inputtype,
      'Eau d’irrigation' AS productname,
      uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring) AS code
        FROM irrig_action_missing_usage aa
        INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
        INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
        INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
        INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
        INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
        LEFT JOIN abstractdomaininputstockunit dis ON dis.domain = gp.domain and dis.inputtype = 'IRRIGATION'
  )
  UNION ALL
  (
    SELECT
      aa.topiaid AS irrig_action,
      0 AS topiaversion,
      (SELECT topiacreatedate FROM v3_0_5_0__12019_radd_missing_irrigation_domain_input_script_dat) AS topiacreatedate,
      plot.domain AS domain,
      CONCAT('fr.inra.agrosyst.api.entities.action.IrrigationInputUsage_', uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring))  AS topiaid,
      dis.topiaid as domainirrigationinput,
      aa.waterquantitymin AS qtmin,
      aa.waterquantityaverage AS qtavg,
      aa.waterquantitymedian AS qtmed,
      aa.waterquantitymax AS qtmax,
      'IRRIGATION' AS inputtype,
      'Eau d’irrigation' AS productname,
      uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring) AS code
        FROM irrig_action_missing_usage aa
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
        INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
        INNER JOIN zone zone ON zone.topiaid = epcc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        LEFT JOIN abstractdomaininputstockunit dis ON dis.domain = plot.domain and dis.inputtype = 'IRRIGATION'
  )
  UNION ALL
  (
    SELECT
      aa.topiaid AS irrig_action,
      0 AS topiaversion,
      (SELECT topiacreatedate FROM v3_0_5_0__12019_radd_missing_irrigation_domain_input_script_dat) AS topiacreatedate,
      plot.domain AS domain,
      CONCAT('fr.inra.agrosyst.api.entities.action.IrrigationInputUsage_', uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring))  AS topiaid,
      dis.topiaid as domainirrigationinput,
      aa.waterquantitymin AS qtmin,
      aa.waterquantityaverage AS qtavg,
      aa.waterquantitymedian AS qtmed,
      aa.waterquantitymax AS qtmax,
      'IRRIGATION' AS inputtype,
      'Eau d’irrigation' AS productname,
      uuid_in(md5(CONCAT(aa.topiaid::text, now()::text))::cstring) AS code
        FROM irrig_action_missing_usage aa
        INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
        INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
        INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
        INNER JOIN zone zone ON zone.topiaid = escc.zone
        INNER JOIN plot plot ON zone.plot = plot.topiaid
        LEFT JOIN abstractdomaininputstockunit dis ON dis.domain = plot.domain and dis.inputtype = 'IRRIGATION'
  )
);

INSERT INTO abstractinputusage (topiaid, topiaversion, topiacreatedate, productname, qtmin, qtavg, qtmed, qtmax, inputtype, code)
  SELECT topiaid, topiaversion, topiacreatedate, productname, qtmin, qtavg, qtmed, qtmax, inputtype, code FROM action_with_missing_irrig_usage;

INSERT INTO irrigationinputusage (topiaid, domainirrigationinput)
  SELECT topiaid, domainirrigationinput FROM action_with_missing_irrig_usage;

UPDATE abstractaction aa SET irrigationinputusage = (SELECT awmiu.topiaid FROM action_with_missing_irrig_usage awmiu WHERE aa.topiaid = awmiu.irrig_action limit 1)
  WHERE exists (select 1 from action_with_missing_irrig_usage awmiu WHERE awmiu.irrig_action = aa.topiaid);