---
-- %%Ignore-License
---

drop table if exists _11924_all_duplicate_valorisations_context;
drop table if exists _11924_valid_duplicate_valorisations;
drop table if exists _11924_non_valid_duplicate_valorisations;
drop table if exists _11924_valid_duplicate_valorisation_to_remove;
drop table if exists _11924_removed_qualitycriteria;
drop table if exists _11924_removed_price;

create table _11924_all_duplicate_valorisations_context as (
SELECT
COUNT(*) AS nbr_doublon,
concat (string_agg(DISTINCT(escc.zone), '__'),  string_agg(DISTINCT(epcc.zone), '__'),  string_agg(DISTINCT(pcc1.practicedsystem), '__'),  string_agg(DISTINCT(pcc2.practicedsystem), '__')) as zone_or_synth,
hav.harvestingaction,
concat (string_agg(DISTINCT(CAST(pi.topiaid  as VARCHAR(200))), '__'),  string_agg(DISTINCT(CAST(ei.topiaid  as VARCHAR(200))), '__')) as intervention,
string_agg(CAST(hav.yealdaverage as VARCHAR(200)), '__') as rendements,
string_agg(hav.yealdunit, '__') as yealdunit,
string_agg(hav.speciescode, '__') as species_code,
string_agg(CAST(hav.topiacreatedate as VARCHAR(200)), '__') as dates_de_creation,
string_agg(CAST(hav.topiaversion as VARCHAR(200)), '__') as version,
string_agg(CAST(hp.price as VARCHAR(200)), '__') as price,
string_agg(hp.priceunit, '__') as priceunit
FROM harvestingactionvalorisation hav
inner join harvestingprice hp on hp.harvestingactionvalorisation = hav.topiaId
inner join abstractaction a on hav.harvestingaction = a.topiaid
left join practicedintervention pi on a.practicedintervention = pi.topiaid
left join effectiveintervention ei on a.effectiveintervention = ei.topiaid
left JOIN practicedcropcyclephase phase0 ON pi.practicedcropcyclephase = phase0.topiaid
left JOIN practicedcropcycle pcc1 ON pcc1.topiaid = phase0.practicedperennialcropcycle
left JOIN practicedcropcycleconnection connection ON pi.practicedcropcycleconnection = connection.topiaid
left JOIN practicedcropcyclenode node0 ON connection.target = node0.topiaid
left JOIN practicedcropcycle pcc2 ON pcc2.topiaid = node0.practicedseasonalcropcycle
left JOIN effectivecropcyclephase phase1 ON ei.effectivecropcyclephase = phase1.topiaid
left JOIN effectiveperennialcropcycle epcc ON phase1.topiaid = epcc.phase
left JOIN effectivecropcyclenode node1 ON ei.effectivecropcyclenode = node1.topiaid
left JOIN effectiveseasonalcropcycle escc ON node1.effectiveseasonalcropcycle = escc.topiaid
GROUP BY hav.harvestingaction, speciescode, destination
HAVING COUNT(*) > 1
and COUNT(*) < 2000
order by COUNT(*) desc);

-- à partir de la liste des doublons, on concerve uniquement les valorisations qui ont la même espèce que celle des espèces sélectionnées sur l'intervention
create table _11924_valid_duplicate_valorisations as (
	(select hav.topiaid, hav.harvestingaction, hav.speciescode, hav.destination, hav.yealdunit,hav.harvestingaction_idx, hav.topiaversion, hav.topiacreatedate, hav.salespercent, hav.selfconsumedpersent, hav.novalorisationpercent, hav.yealdmin, hav.yealdmax, hav.yealdaverage, hav.yealdmedian, hav.beginmarketingperiod, hav.beginmarketingperioddecade, hav.beginmarketingperiodcampaign, hav.endingmarketingperiod, hav.endingmarketingperioddecade, hav.endingmarketingperiodcampaign, hav.isorganiccrop, hp.price, hp.priceunit
	  from harvestingactionvalorisation hav
	  inner join harvestingprice hp on hp.harvestingactionvalorisation = hav.topiaId
	  inner join _11924_all_duplicate_valorisations_context dvi on dvi.harvestingaction = hav.harvestingaction
	  inner join abstractaction aa on hav.harvestingaction = aa.topiaid
	  inner join effectiveintervention ei on ei.topiaid = aa.effectiveintervention
	  inner join effectivespeciesstade ess on ess.effectiveintervention = ei.topiaid
	  inner join croppingplanspecies cps on ess.croppingplanspecies = cps.code
	  where hav.speciescode = cps.code
	  order by hav.harvestingaction, speciescode, destination, hp.price desc, hav.topiacreatedate desc)
  union
    (select hav.topiaid, hav.harvestingaction, hav.speciescode, hav.destination,hav.yealdunit, hav.harvestingaction_idx, hav.topiaversion, hav.topiacreatedate, hav.salespercent, hav.selfconsumedpersent, hav.novalorisationpercent, hav.yealdmin, hav.yealdmax, hav.yealdaverage, hav.yealdmedian, hav.beginmarketingperiod, hav.beginmarketingperioddecade, hav.beginmarketingperiodcampaign, hav.endingmarketingperiod, hav.endingmarketingperioddecade, hav.endingmarketingperiodcampaign, hav.isorganiccrop, hp.price, hp.priceunit
      from harvestingactionvalorisation hav
      inner join harvestingprice hp on hp.harvestingactionvalorisation = hav.topiaId
	  inner join _11924_all_duplicate_valorisations_context dvi on dvi.harvestingaction = hav.harvestingaction
	  inner join abstractaction aa on hav.harvestingaction = aa.topiaid
	  inner join practicedintervention pi on pi.topiaid = aa.practicedintervention
	  inner join practicedspeciesstade pss on pss.practicedintervention = pi.topiaid
	  where hav.speciescode = pss.speciescode
	  order by hav.harvestingaction, speciescode, destination, hp.price desc, hav.topiacreatedate desc)
);

-- à partir de la liste des doublons, toutes les valorisations qui n'ont pas la même espèce que celle des espèces sélectionnées sur l'intervention
create table _11924_non_valid_duplicate_valorisations as (
    select hav.topiaid, hav.harvestingaction, hav.speciescode, hav.destination,hav.yealdunit, hav.harvestingaction_idx, hav.topiaversion, hav.topiacreatedate, hav.salespercent, hav.selfconsumedpersent, hav.novalorisationpercent, hav.yealdmin, hav.yealdmax, hav.yealdaverage, hav.yealdmedian, hav.beginmarketingperiod, hav.beginmarketingperioddecade, hav.beginmarketingperiodcampaign, hav.endingmarketingperiod, hav.endingmarketingperioddecade, hav.endingmarketingperiodcampaign, hav.isorganiccrop, hp.price, hp.priceunit
      from harvestingactionvalorisation hav
      inner join harvestingprice hp on hp.harvestingactionvalorisation = hav.topiaId
	  inner join _11924_all_duplicate_valorisations_context dvi on dvi.harvestingaction = hav.harvestingaction
	  where not exists (
	    select 1 from _11924_valid_duplicate_valorisations vdv
	    where vdv.topiaid = hav.topiaid
	  )
);

-- parmis les doublons, on garde s'il existe le rendement > 0, un prix ou le plus récent en priorité
create table _11924_valid_duplicate_valorisation_to_remove as
SELECT * FROM _11924_valid_duplicate_valorisations
  WHERE topiaid IN
      (SELECT topiaid
       FROM
          (SELECT topiaid,
           ROW_NUMBER() OVER( PARTITION BY harvestingaction, speciescode, destination
          ORDER BY yealdaverage desc, price desc, topiaversion, topiacreatedate desc) AS row_num
          FROM _11924_valid_duplicate_valorisations ) t
          WHERE t.row_num > 1);

 -- on concerve une copie des critères de qualité à supprimer
create table _11924_removed_qualitycriteria as (
select * from qualitycriteria q
where exists ( select 1 from _11924_valid_duplicate_valorisation_to_remove v where v.topiaId = q.harvestingactionvalorisation )
union
select * from qualitycriteria q
where exists ( select 1 from _11924_non_valid_duplicate_valorisations v where v.topiaId = q.harvestingactionvalorisation )
);

 -- on concerve une copie des prix à supprimer
create table _11924_removed_price as (
select * from harvestingprice hp
where exists ( select 1 from _11924_valid_duplicate_valorisation_to_remove v where v.topiaId = hp.harvestingactionvalorisation )
union
select * from harvestingprice hp
where exists ( select 1 from _11924_non_valid_duplicate_valorisations v where v.topiaId = hp.harvestingactionvalorisation )
);

delete from qualitycriteria qc where exists (select 1 from _11924_removed_qualitycriteria rqq where rqq.topiaId = qc.topiaId);
delete from harvestingprice hp where exists (select 1 from _11924_removed_price rp where rp.topiaId = hp.topiaId);

delete from harvestingactionvalorisation hav where exists (select 1 from _11924_valid_duplicate_valorisation_to_remove v where v.topiaid = hav.topiaid);
delete from harvestingactionvalorisation hav where exists (select 1 from _11924_non_valid_duplicate_valorisations v where v.topiaid = hav.topiaid);
