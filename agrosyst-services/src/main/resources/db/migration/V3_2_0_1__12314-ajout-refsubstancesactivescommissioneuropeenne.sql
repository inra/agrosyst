---
-- %%Ignore-License
---

-- rollback
-- ALTER TABLE refsubstancesactivescommissioneuropeenne DROP COLUMN cuivre;
-- ALTER TABLE refsubstancesactivescommissioneuropeenne DROP COLUMN soufre;

ALTER TABLE refsubstancesactivescommissioneuropeenne ALTER COLUMN sa_candidates_substitution SET DEFAULT false;
ALTER TABLE refsubstancesactivescommissioneuropeenne ALTER COLUMN substances_de_base SET DEFAULT false;
ALTER TABLE refsubstancesactivescommissioneuropeenne ALTER COLUMN sa_faible_risque SET DEFAULT false;
ALTER TABLE refsubstancesactivescommissioneuropeenne ALTER COLUMN sa_candidates_substitution SET not null;
ALTER TABLE refsubstancesactivescommissioneuropeenne ALTER COLUMN substances_de_base SET not null;
ALTER TABLE refsubstancesactivescommissioneuropeenne ALTER COLUMN sa_faible_risque SET not null;

ALTER TABLE refsubstancesactivescommissioneuropeenne ADD COLUMN cuivre boolean DEFAULT false;
ALTER TABLE refsubstancesactivescommissioneuropeenne ADD COLUMN soufre boolean DEFAULT false;

UPDATE refsubstancesactivescommissioneuropeenne SET cuivre = true,  soufre = false WHERE id_sa = '276';
UPDATE refsubstancesactivescommissioneuropeenne SET cuivre = true,  soufre = false WHERE id_sa = '570';
UPDATE refsubstancesactivescommissioneuropeenne SET cuivre = true,  soufre = false WHERE id_sa = '571';
UPDATE refsubstancesactivescommissioneuropeenne SET cuivre = true,  soufre = false WHERE id_sa = '572';
UPDATE refsubstancesactivescommissioneuropeenne SET cuivre = true,  soufre = false WHERE id_sa = '580';
UPDATE refsubstancesactivescommissioneuropeenne SET cuivre = true,  soufre = false WHERE id_sa = '932';
UPDATE refsubstancesactivescommissioneuropeenne SET cuivre = false, soufre = true WHERE id_sa = '1028';
UPDATE refsubstancesactivescommissioneuropeenne SET cuivre = true,  soufre = false WHERE id_sa = '1221';
UPDATE refsubstancesactivescommissioneuropeenne SET cuivre = true,  soufre = true WHERE id_sa = '1222';
UPDATE refsubstancesactivescommissioneuropeenne SET cuivre = false, soufre = true WHERE id_sa = 'NA_0102';