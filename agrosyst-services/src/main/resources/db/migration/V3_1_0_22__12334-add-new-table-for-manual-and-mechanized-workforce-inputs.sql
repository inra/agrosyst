---
-- %%Ignore-License
---

CREATE TABLE public.domainmanualworkforceinput (
  topiaid varchar(255) NOT NULL,
  CONSTRAINT domainmanualworkforceinput_pkey PRIMARY KEY (topiaid)
);

ALTER TABLE public.domainmanualworkforceinput ADD CONSTRAINT dmawi_fk_adisu_topiaid FOREIGN KEY (topiaid) REFERENCES public.abstractdomaininputstockunit(topiaid);

CREATE TABLE public.domainmechanizedworkforceinput (
  topiaid varchar(255) NOT NULL,
  CONSTRAINT domainmechanizedworkforceinput_pkey PRIMARY KEY (topiaid)
);

ALTER TABLE public.domainmechanizedworkforceinput ADD CONSTRAINT dmewi_fk_adisu_topiaid FOREIGN KEY (topiaid) REFERENCES public.abstractdomaininputstockunit(topiaid);
