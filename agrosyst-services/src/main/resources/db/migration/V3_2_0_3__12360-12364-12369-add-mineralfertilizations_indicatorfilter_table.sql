---
-- %%Ignore-License
---

CREATE TABLE public.indicatorfilter_mineralfertilizations (
	"owner" varchar(255) NULL,
	mineralfertilizations varchar(255) NULL
);

CREATE TABLE public.indicatorfilter_organicfertilizations (
	"owner" varchar(255) NULL,
	organicfertilizations varchar(255) NULL
);
