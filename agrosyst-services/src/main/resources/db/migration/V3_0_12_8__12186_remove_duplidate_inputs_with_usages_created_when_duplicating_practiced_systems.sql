---
-- %%Ignore-License
---

drop table if exists _12186_inputs_used_to_remove;
drop table if exists _12186_inputs_used_to_remove_b;

-- supprime un lot de semence qui n'aurait pas du être créé
delete from domainseedlotinput where topiaid = 'fr.inra.agrosyst.api.entities.DomainSeedLotInput_c59ce66f-4887-48f2-b4a4-d7c41210f5dd';

-- Supprime les doublons d'intrants ayant des usages introduits par un bug dans la copie des systèmes synthétisés.

create table _12186_inputs_used_to_remove AS (
  select adis1.code as input_code, adis1.domain as domain_id, adis1.topiacreatedate as input_create_date, adis1.topiaid as input_topiaid, adis1.inputtype as adis_input_type, appiu.topiaid as usage_id, aiu.qtavg as usage_qtavg from
  (select adis.code, adis.domain
  from abstractdomaininputstockunit  adis
  group by adis.code, adis.domain
  having count(*) > 1) as sub_res
  inner join abstractdomaininputstockunit adis1 on adis1.code = sub_res.code and adis1.domain = sub_res.domain
  left join abstractphytoproductinputusage appiu on appiu.domainphytoproductinput = adis1.topiaid
  left join abstractinputusage aiu on aiu.topiaid = appiu.topiaid
  order by adis1.code, adis1.domain, adis1.topiacreatedate
);

create index _12186_inputs_used_to_remove_usage_id_idx on _12186_inputs_used_to_remove(usage_id);

alter table _12186_inputs_used_to_remove add column new_input_id varchar;

-- recherche d'un unique intrant correspondant
update _12186_inputs_used_to_remove appiu set new_input_id = (
  select iutr.input_topiaid
  from (select iutr.* from abstractphytoproductinputusage ppiu
     inner join _12186_inputs_used_to_remove iutr on iutr.usage_id = ppiu.topiaid
     inner join abstractdomaininputstockunit adis on adis.code = iutr.input_code and iutr.domain_id = adis.domain
     where ppiu.topiaid = appiu.usage_id
     order by iutr.input_create_date
     limit 1) as sub
  inner join _12186_inputs_used_to_remove iutr on iutr.input_code = sub.input_code and iutr.domain_id = sub.domain_id
  order by iutr.input_create_date
  limit 1
) where appiu.usage_id is not null;

-- mise à jour des usages pour qu'il pointe vers un unique intrant correspondant
update abstractphytoproductinputusage appiu set domainphytoproductinput = (
  select new_input_id
  from _12186_inputs_used_to_remove iutr
  where iutr.usage_id = appiu.topiaid
) where exists (
  select 1
  from _12186_inputs_used_to_remove iutr
  where iutr.usage_id = appiu.topiaid
  and new_input_id IS NOT NULL
);

-- maintenant on prépare la suppréssion des doublons
CREATE TABLE _12186_inputs_used_to_remove_b AS (
    select a.*
    from abstractdomaininputstockunit a
        join abstractdomaininputstockunit a2 on a.code = a2.code and a.domain = a2.domain and a.topiaid != a2.topiaid
        left join abstractphytoproductinputusage appiu on a.topiaid = appiu.domainphytoproductinput
    where
        appiu.domainphytoproductinput is null and
        (
            a.inputtype = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES' or
            a.inputtype = 'TRAITEMENT_SEMENCE' or
            a.inputtype = 'LUTTE_BIOLOGIQUE'
        )
);

-- on applique les supressions
delete from inputprice where topiaid in (select r.inputprice from _12186_inputs_used_to_remove_b r);

delete from domainphytoproductinput where topiaid in (select topiaid from _12186_inputs_used_to_remove_b);
delete from abstractdomaininputstockunit where topiaid in (select topiaid from _12186_inputs_used_to_remove_b);
