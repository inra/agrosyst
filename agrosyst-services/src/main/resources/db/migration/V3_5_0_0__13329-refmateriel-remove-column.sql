---
-- %%Ignore-License
---
UPDATE refmateriel SET donneestauxdechargemoteur = donnesstauxdechargemoteur
WHERE donneestauxdechargemoteur IS NULL AND donnesstauxdechargemoteur IS NOT NULL;

ALTER TABLE refmateriel DROP COLUMN donnesstauxdechargemoteur;
