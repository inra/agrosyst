---
-- %%Ignore-License
--
--

ALTER TABLE RefSolTextureGeppa ADD COLUMN onlyForFrance boolean DEFAULT true;
UPDATE RefSolTextureGeppa SET onlyForFrance = false
  WHERE abbreviation_classes_texturales_GEPAA IN ('La', 'L', 'Sl', 'A', 'S', 'Ls');

DELETE FROM trad_ref_sol WHERE key LIKE 'fr.inra.agrosyst.api.entities.Depth%';
DELETE FROM trad_ref_sol WHERE key LIKE 'fr.inra.agrosyst.api.entities.Limestone%';
DELETE FROM trad_ref_sol WHERE key LIKE 'fr.inra.agrosyst.api.entities.Stoniness%';
DELETE FROM trad_ref_sol WHERE key LIKE 'fr.inra.agrosyst.api.entities.Hydromorphic%';

insert into trad_ref_sol values ('fra', 'fr.inra.agrosyst.api.entities.Stoniness.NO_STONY', 'Non caillouteux (<5%)');
insert into trad_ref_sol values ('fra', 'fr.inra.agrosyst.api.entities.Stoniness.LITTLE_STONY', 'Peu caillouteux (5-15%)');
insert into trad_ref_sol values ('fra', 'fr.inra.agrosyst.api.entities.Stoniness.STONY', 'Caillouteux (>15%)');

insert into trad_ref_sol values ('eng', 'fr.inra.agrosyst.api.entities.Stoniness.NO_STONY', 'No stony (<5%)');
insert into trad_ref_sol values ('eng', 'fr.inra.agrosyst.api.entities.Stoniness.LITTLE_STONY', 'Little stony (5-15%)');
insert into trad_ref_sol values ('eng', 'fr.inra.agrosyst.api.entities.Stoniness.STONY', 'Stony (>15%)');
