---
-- %%Ignore-License
---

ALTER TABLE refotherinput ADD COLUMN unit varchar(255);
CREATE UNIQUE INDEX refotherinput_uniqueness ON refotherinput (inputtype_c0, COALESCE(caracteristic1, '###'::text), COALESCE(caracteristic2, '###'::text), COALESCE(caracteristic3, '###'::text));
