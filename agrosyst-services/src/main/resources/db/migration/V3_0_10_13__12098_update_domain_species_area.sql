---
-- %%Ignore-License
---

create table _12098_update_domain_species_area AS
  select distinct d0.topiaid                                                                                                                         as domainId,
  LOWER(unaccent(d0.speciestoarea))                                                                                                                  as original_speciestoarea,
  LOWER(unaccent(concat(re.libelle_espece_botanique , '_' , re.libelle_qualifiant_aee , '_' , re.libelle_type_saisonnier_aee)))                      as from_speciestoarea_part,
  LOWER(unaccent(concat(re.code_espece_botanique  , '_' , re.code_qualifiant_aee  , '_' , re.code_type_saisonnier_aee, '_', code_destination_aee)))  as new_speciestoarea_part,
  json_speciesarea.value                                                                                                                             as new_speciestoarea_part_value
  from
  domain d0
  inner join croppingplanentry cpe on cpe.domain = d0.topiaid
  inner join croppingplanspecies cps on cps.croppingplanentry = cpe.topiaid
  inner join refespece re on re.topiaid = cps.species,
  json_each_text((select d.speciestoarea from domain d where d0.topiaid = d.topiaid)::json) as json_speciesarea
  where NULLIF(concat (d0.speciestoarea), '') is not null
  and d0.speciestoarea != '{}'
  and (LOWER(unaccent(d0.speciestoarea)) like concat('%', LOWER(unaccent(concat(re.libelle_espece_botanique , '_' , re.libelle_qualifiant_aee , '_' , re.libelle_type_saisonnier_aee))) ,'%')
  or
    LOWER(unaccent(d0.speciestoarea)) like LOWER(unaccent(concat((select trv.traduction from trad_ref_vivant trv where  trv.key = to_complex_i18n_key(re.libelle_espece_botanique) and trv.lang = 'fra'),
  '_' ,
  (select trv.traduction from trad_ref_vivant trv where  trv.key = to_complex_i18n_key(re.libelle_qualifiant_aee) and trv.lang = 'fra'),
  '_' ,
  (select trv.traduction from trad_ref_vivant trv where  trv.key = to_complex_i18n_key(re.libelle_type_saisonnier_aee) and trv.lang = 'fra'))))
  )
  and LOWER(unaccent(concat(re.libelle_espece_botanique , '_' , re.libelle_qualifiant_aee , '_' , re.libelle_type_saisonnier_aee))) = LOWER(unaccent(json_speciesarea.key));

CREATE index IF NOT EXISTS _12098_update_domain_species_area_idx ON _12098_update_domain_species_area(domainid);

update domain d set speciestoarea = (
  select concat ('{', string_agg(DISTINCT(concat('"',new_speciestoarea_part,'":',new_speciestoarea_part_value)), ','), '}')
  from _12098_update_domain_species_area
  where domainId = d.topiaid
)
where d.topiaid in (select domainId from _12098_update_domain_species_area);