---
-- %%Ignore-License
---

update inputprice ip set price = price / 100, priceunit = 'EURO_KG'
where category in ('SEEDING_INPUT', 'SEEDING_PLAN_COMPAGNE_INPUT') and priceunit = 'EURO_Q';

update inputprice set price = price / 1000, priceunit = 'EURO_KG'
where category in ('SEEDING_INPUT', 'SEEDING_PLAN_COMPAGNE_INPUT') and priceunit = 'EURO_T';

update inputprice set price = price * 1000, priceunit = 'EURO_KG'
where category in ('SEEDING_INPUT', 'SEEDING_PLAN_COMPAGNE_INPUT') and priceunit = 'EURO_G';

update inputprice set price = price * 1000000, priceunit = 'EURO_KG'
where category in ('SEEDING_INPUT', 'SEEDING_PLAN_COMPAGNE_INPUT') and priceunit = 'EURO_MG';
