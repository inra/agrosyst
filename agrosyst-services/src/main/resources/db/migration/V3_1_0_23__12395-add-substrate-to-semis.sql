---
-- %%Ignore-License
---

ALTER TABLE substrateInputUsage
ADD COLUMN seedingActionUsage varchar(255);

ALTER TABLE substrateInputUsage
ADD CONSTRAINT substrateinputusage_seedingactionusage_fkey
FOREIGN KEY (seedingActionUsage) REFERENCES abstractaction(topiaid);
