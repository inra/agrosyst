---
-- %%Ignore-License
---

-- link price with input MINERAL_INPUT
update abstractdomaininputstockunit adis set inputprice = orphan_input_price.price_id
from (
  select adis.topiaid as topiaid, ip.topiaid as price_id
  from inputprice ip
  inner join abstractdomaininputstockunit adis
        on split_part(adis."key",';', 1) = ip.objectid
        and split_part(adis."key",';', 3) = ip.sourceunit
        and adis."domain" = ip."domain"
        and adis.inputprice is null
  and ip.category = 'MINERAL_INPUT'
  and not exists (select 1 from abstractdomaininputstockunit where inputprice = ip.topiaid)
) as orphan_input_price
where adis.topiaid  = orphan_input_price.topiaid;

-- link price with input MINERAL_INPUT ignore sourceunit
update abstractdomaininputstockunit adis set inputprice = orphan_input_price.price_id
from (
  select adis.topiaid as topiaid, ip.topiaid as price_id
  from inputprice ip
  inner join abstractdomaininputstockunit adis
        on split_part(adis."key",';', 1) = ip.objectid
        and adis."domain" = ip."domain"
        and adis.inputprice is null
  and ip.category = 'MINERAL_INPUT'
  and not exists (select 1 from abstractdomaininputstockunit where inputprice = ip.topiaid)
) as orphan_input_price
where adis.topiaid  = orphan_input_price.topiaid;

-- link price with input APPLICATION_DE_PRODUITS_PHYTOSANITAIRES
update abstractdomaininputstockunit adis set inputprice = orphan_input_price.price_id
from (
  select adis.topiaid as topiaid, ip.topiaid as price_id
  from inputprice ip
  inner join abstractdomaininputstockunit adis
    on  ip.objectid ilike concat('%', split_part(adis."key",';', 2),'%')
    and substr(ip.objectid, 0, (length(ip.objectid) - position('_' in reverse(ip.objectid)) + 1)) ilike split_part(adis."key",';', 1)
    and substr(ip.objectid, (length(ip.objectid) - position('_' in reverse(ip.objectid)) + 2)) ilike split_part(adis."key",';', 2)
    and split_part(adis."key",';', 3) = ip.sourceunit
    and adis."domain" = ip."domain"
    and adis.inputtype = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
    and adis.inputprice is null
  where ip.category = 'PHYTO_TRAITMENT_INPUT'
  and not exists (select 1 from abstractdomaininputstockunit where inputprice = ip.topiaid)
) as orphan_input_price
where adis.topiaid  = orphan_input_price.topiaid;

-- link price with input APPLICATION_DE_PRODUITS_PHYTOSANITAIRES ignore sourceunit
update abstractdomaininputstockunit adis set inputprice = orphan_input_price.price_id
from (
  select adis.topiaid as topiaid, ip.topiaid as price_id
  from inputprice ip
  inner join abstractdomaininputstockunit adis
    on  ip.objectid ilike concat('%', split_part(adis."key",';', 2),'%')
    and substr(ip.objectid, 0, (length(ip.objectid) - position('_' in reverse(ip.objectid)) + 1)) ilike split_part(adis."key",';', 1)
    and substr(ip.objectid, (length(ip.objectid) - position('_' in reverse(ip.objectid)) + 2)) ilike split_part(adis."key",';', 2)
    and adis."domain" = ip."domain"
    and adis.inputtype = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
    and adis.inputprice is null
  where ip.category = 'PHYTO_TRAITMENT_INPUT'
  and not exists (select 1 from abstractdomaininputstockunit where inputprice = ip.topiaid)
) as orphan_input_price
where adis.topiaid  = orphan_input_price.topiaid;

-- link price with input EPANDAGES_ORGANIQUES
update abstractdomaininputstockunit adis set inputprice = orphan_input_price.price_id
from (
  select adis.topiaid as topiaid, ip.topiaid as price_id
  from inputprice ip
  inner join domainorganicproductinput pi
  inner join abstractdomaininputstockunit adis on pi.topiaid = adis.topiaid
    on adis."domain" = ip."domain"
    and split_part(split_part(adis."key",';', 1),'=', 2) = ip.objectid
    and adis.inputprice is null
    and adis.inputtype = 'EPANDAGES_ORGANIQUES'
    and ip.sourceunit = pi.usageunit
    and LOWER(unaccent(adis.inputname)) = LOWER(unaccent(ip.displayname))
    and coalesce (ip.organic, false) = coalesce (pi.organic, false)
  where ip.category = 'ORGANIQUES_INPUT'
  and pi.usageunit = ip.sourceunit
  and not exists (
    select 1 from abstractdomaininputstockunit
    where inputprice = ip.topiaid
  )
) as orphan_input_price
where adis.topiaid  = orphan_input_price.topiaid;


update abstractdomaininputstockunit adis set inputprice = orphan_input_price.price_id
from (
  select adis.topiaid as topiaid, ip.topiaid as price_id
  from inputprice ip
  inner join domainorganicproductinput pi
  inner join abstractdomaininputstockunit adis on pi.topiaid = adis.topiaid
    on adis."domain" = ip."domain"
    and split_part(split_part(adis."key",';', 1),'=', 2) = ip.objectid
    and adis.inputprice is null
    and adis.inputtype = 'EPANDAGES_ORGANIQUES'
    and ip.sourceunit = pi.usageunit
    and LOWER(unaccent(adis.inputname)) = LOWER(unaccent(ip.displayname))
    and coalesce (ip.organic, false) = coalesce (pi.organic, false)
  where ip.category = 'ORGANIQUES_INPUT'
  and pi.usageunit = ip.sourceunit
  and not exists (
    select 1 from abstractdomaininputstockunit
    where inputprice = ip.topiaid
  )
) as orphan_input_price
where adis.topiaid  = orphan_input_price.topiaid;

-- link price with input EPANDAGES_ORGANIQUES ignore sourceunit

-- function that remove article from sentence
CREATE OR REPLACE function replace_article(word text) returns text
LANGUAGE sql immutable as
$$
  select
  LOWER(unaccent(
         replace(
         replace(
         replace(
         replace(
         replace(
         replace(
         replace(
         replace(
         replace(word, ' de ', ' '), ' la ', ' '), ' les ', ' '), ' à ', ' '), ' l'' ', ' '), ' un ', ' '), ' une ', ' '), ' des ', ' '), ' le ', ' ')))
$$;

-- function that identify a term ins a sentence and replace it with 'term_match'
CREATE OR REPLACE FUNCTION identify_word(previous text, current_text text, terme text) RETURNS text
LANGUAGE sql VOLATILE as
$$
  --call raise_notice(previous, current_text, terme, replace(replace_article(COALESCE(previous,current_text)), LOWER(unaccent(terme)),'term_match'));
  SELECT replace(replace_article(LOWER(COALESCE(previous,current_text))), LOWER(unaccent(terme)), 'term_match')
$$;

-- aggregate function to analyse recursively
CREATE AGGREGATE identify_word_agg (text, text)
(
  sfunc = identify_word
, stype = text
);

-- try match price and input that don't have exactly same name
-- ex:
--     price displayname            |          input name
-- Fumier de bovins	Rumi.          - Fumier de bovins, logettes paillée, stockage < 2 mois
-- Fumier de dindes	Avi.           - Fumier de dindes (après stockage)
-- Fumier de poulets de chair	Avi. - Fumier de poulets de chair ( après stockage )
update abstractdomaininputstockunit adis set inputprice = orphan_input_price.price_id
from (
  select adis.topiaid as topiaid, ip.topiaid as price_id
  from inputprice ip
  inner join domainorganicproductinput pi
  inner join abstractdomaininputstockunit adis on pi.topiaid = adis.topiaid
    on adis."domain" = ip."domain"
    and split_part(split_part(adis."key",';', 1),'=', 2) = ip.objectid
    and adis.inputprice is null
    and adis.inputtype = 'EPANDAGES_ORGANIQUES'
    and ip.sourceunit = pi.usageunit
    and coalesce (ip.organic, false) = coalesce (pi.organic, false)
  where ip.category = 'ORGANIQUES_INPUT'
  and pi.usageunit = ip.sourceunit
  and (select (count(*) - 1) from (
    SELECT unnest(string_to_array(identify_word_agg (adis.inputname, k.term), 'term_match'))
    FROM (select regexp_split_to_table(ip.displayname, '\s+') as term) as k
  ) as sub)  > 1
  and not exists (
    select 1 from abstractdomaininputstockunit
    where inputprice = ip.topiaid)
) as orphan_input_price
where adis.topiaid  = orphan_input_price.topiaid;

update abstractdomaininputstockunit adis0 set inputprice = null
from (
  select adis.topiaid as topiaid
  from domainseedlotinput pi
  inner join abstractdomaininputstockunit adis on pi.topiaid = adis.topiaid
    and adis.inputprice is not null
    and (adis.inputtype = 'PLAN_COMPAGNE' or adis.inputtype = 'SEMIS')
) as orphan_input_price
where adis0.topiaid = orphan_input_price.topiaid;

update abstractdomaininputstockunit adis0 set inputprice = null
from (
  select adis.topiaid as topiaid
  from domainseedspeciesinput dssi
  inner join abstractdomaininputstockunit adis on adis.topiaid = dssi.topiaid
    and adis.inputprice is not null
    and (adis.inputtype = 'PLAN_COMPAGNE' or adis.inputtype = 'SEMIS')
) as orphan_input_price
where adis0.topiaid = orphan_input_price.topiaid;

-- update crop price
update domainseedlotinput adis0 set seedprice = orphan_input_price.price_id
from (
  select adis.topiaid as topiaid, ip.topiaid as price_id
  from inputprice ip
  inner join domainseedlotinput pi
  inner join abstractdomaininputstockunit adis on pi.topiaid = adis.topiaid
    on adis."domain" = ip."domain"
    and pi.seedprice is null
    and (adis.inputtype = 'PLAN_COMPAGNE' or adis.inputtype = 'SEMIS')
  where (ip.category = 'SEEDING_PLAN_COMPAGNE_INPUT' or ip.category = 'SEEDING_INPUT')
  and pi.cropseed = split_part(ip.objectid, ' - ', 1)
  and coalesce (pi.organic, false) = CASE WHEN split_part(ip.objectid, ' - ', 2)
                                     IS NOT NULL
                                     AND (split_part(ip.objectid, ' - ', 2) = 'true' or split_part(ip.objectid, ' - ', 2) = 'false')
                                     THEN coalesce (NULLIF(split_part(ip.objectid, ' - ', 2), '')::bool, false)
                                     ELSE NULL end
  and not exists (
    select 1 from domainseedlotinput dsl
    where dsl.seedprice = ip.topiaid)
) as orphan_input_price
where adis0.topiaid = orphan_input_price.topiaid;

-- update species price
update domainseedspeciesinput adis0 set seedprice = orphan_input_price.price_id
from (
  select adis.topiaid as topiaid, ip.topiaid as price_id
  from inputprice ip
  inner join domainseedlotinput pi
  inner join domainseedspeciesinput dsls on dsls.domainseedlotinput = pi.topiaid
  inner join croppingplanspecies cps on dsls.speciesseed = cps.topiaid
  inner join refespece re on cps.species = re.topiaid
  left join refvarietegeves rvg on cps.variety = rvg.topiaid
  left join refvarieteplantgrape rvpg on cps.variety = rvpg.topiaid
  inner join abstractdomaininputstockunit adis on pi.topiaid = adis.topiaid
    on adis."domain" = ip."domain"
    and adis.inputprice is null
    and (adis.inputtype = 'PLAN_COMPAGNE' or adis.inputtype = 'SEMIS')
  where (ip.category = 'SEEDING_PLAN_COMPAGNE_INPUT' or ip.category = 'SEEDING_INPUT')
  and pi.seedprice is null
  and dsls.seedprice is null
  and concat(coalesce(re.code_espece_botanique, ''), ' - ',
       coalesce(re.code_qualifiant_aee, ''), ' - ',
       coalesce(re.code_destination_aee , ''), ' - ',
       coalesce(rvg.denomination, coalesce(rvpg.variete , ''))) = ip.objectid
  and not exists (
    select 1 from domainseedspeciesinput dss
    where dss.seedprice = ip.topiaid)
) as orphan_input_price
where adis0.topiaid = orphan_input_price.topiaid;

-- suprime les doublons
update abstractdomaininputstockunit set inputprice = null where topiaid in (
  SELECT input_id
   FROM
      (SELECT adis."domain",
              adis.topiaid as input_id,
              adis.inputprice as price_id,
              ROW_NUMBER() OVER( PARTITION BY adis."domain", adis.inputprice) AS row_num
      from abstractdomaininputstockunit adis
   ) t
   WHERE t.row_num > 1);

-- backup inputprice that will be removed
create table _12058_orphan_inputprice as (
  select *
  from inputprice ip
  where not exists (
    select 1 from abstractdomaininputstockunit where inputprice = ip.topiaid
  )
  and category != 'SEEDING_INPUT'
  and category != 'SEEDING_PLAN_COMPAGNE_INPUT'
);

delete from inputprice ip
where not exists (
  select 1 from abstractdomaininputstockunit where inputprice = ip.topiaid
)
and category != 'SEEDING_INPUT'
and category != 'SEEDING_PLAN_COMPAGNE_INPUT';




