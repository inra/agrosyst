---
-- %%Ignore-License
---

update abstractinputusage aiu0 set topiacreatedate = (select topiacreatedate from refotherinput where topiaid like '%_0000000-0000-0000-0000-000000000000') where exists
(
  select 1 from abstractinputusage aiu
  where aiu.topiacreatedate >= (date_trunc('day', (select topiacreatedate from refotherinput where topiaid like '%_0000000-0000-0000-0000-000000000000')))
  and aiu.topiacreatedate < (date_trunc('day', (select topiacreatedate from refotherinput where topiaid like '%_0000000-0000-0000-0000-000000000000')) + interval '1 day')
  and aiu.topiaid = aiu0.topiaid
);

alter table abstractinputusage add column deprecatedqtavg double precision;
alter table abstractinputusage add column deprecatedunit text;
alter table abstractinputusage add column deprecatedid text;

update abstractinputusage aiu set deprecatedid = (select au.id_input from _12084_input_to_usage au where aiu.topiaid = au.id_usage order by aiu.topiaid limit 1 )
where exists (select 1 from _12084_input_to_usage au where aiu.topiaid = au.id_usage);

CREATE INDEX IF NOT EXISTS abstractinputusage_deprecated_id_idx ON abstractinputusage(deprecatedid);

update abstractinputusage aiu set deprecatedqtavg = (
  select ai.qtavg
  from abstractinput ai where ai.topiaid = aiu.deprecatedid)
where aiu.deprecatedid is not null;

update abstractinputusage aiu set deprecatedunit = (
  select NULLIF(concat(ai.phytoproductunit, ai.organicproductunit, ai.mineralproductunit, ai.otherproductinputunit, ai.substrateinputunit, ai.potinputunit), '')
  from abstractinput ai where ai.topiaid = aiu.deprecatedid)
where aiu.deprecatedid is not null;


