---
-- %%Ignore-License
---

update domain set speciestoarea = replace(speciestoarea, ':}', ':""}') where speciestoarea is not null and speciestoarea like '%:}';

create table _12164_speciestoarea_fix as (
select distinct
topiaid,
speciestoarea,
replace(replace(replace(regexp_matches(speciestoarea, '\d+,\d+', 'g')::text, '{', ''), '}', ''), '"', '') as to_replace
from domain);
