---
-- %%Ignore-License
---

ALTER TABLE echelle_intrant DROP COLUMN IF EXISTS dose;

ALTER TABLE echelle_intrant RENAME COLUMN intrant to intrant_nom;

ALTER TABLE echelle_intrant ADD COLUMN dose_appliquee text NULL;

ALTER TABLE echelle_intrant ADD COLUMN dose_appliquee_unite text NULL;

ALTER TABLE echelle_intrant ADD COLUMN ift_a_l_ancienne_dose_reference_unite text NULL;

ALTER TABLE echelle_intrant ADD COLUMN ift_a_la_cible_non_mil_dose_reference_unite text NULL;

ALTER TABLE echelle_intrant ADD COLUMN ift_a_la_cible_mil_dose_reference_unite text NULL;

ALTER TABLE echelle_intrant ADD COLUMN ift_a_la_culture_non_mil_dose_reference_unite text NULL;

ALTER TABLE echelle_intrant ADD COLUMN ift_a_la_culture_mil_dose_reference_unite text NULL;
