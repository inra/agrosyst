---
-- %%Ignore-License
---

with newline as (
SELECT *
FROM _12123_refciblesagrosystgroupesciblesmaa_2_72 r272
WHERE r272.topiaid not in (SELECT topiaid FROM _12123_refciblesagrosystgroupesciblesmaa_avril_22))
INSERT INTO _12123_refciblesagrosystgroupesciblesmaa_avril_22
SELECT * FROM newline;

with newline as (
SELECT *
FROM _12123_refdestination_2_72 r272
WHERE r272.topiaid not in (SELECT topiaid FROM _12123_refdestination_avril_22))
INSERT INTO _12123_refdestination_avril_22
SELECT * FROM newline;

with newline as (
SELECT *
FROM _12123_refelementvoisinage_avril_22 r272
WHERE r272.topiaid not in (SELECT topiaid FROM _12123_refelementvoisinage_avril_22))
INSERT INTO _12123_refelementvoisinage_avril_22
SELECT * FROM newline;

with newline as (
SELECT *
FROM _12123_refespece_2_72 r272
WHERE r272.topiaid not in (SELECT topiaid FROM _12123_refespece_avril_22))
INSERT INTO _12123_refespece_avril_22
SELECT * FROM newline;

with newline as (
SELECT *
FROM _12123_refinterventionagrosysttravailedi_2_72 r272
WHERE r272.topiaid not in (SELECT topiaid FROM _12123_refinterventionagrosysttravailedi_avril_22))
INSERT INTO _12123_refinterventionagrosysttravailedi_avril_22
SELECT * FROM newline;

with newline as (
SELECT *
FROM _12123_refmateriel_2_72 r272
WHERE r272.topiaid not in (SELECT topiaid FROM _12123_refmateriel_avril_22))
INSERT INTO _12123_refmateriel_avril_22
SELECT * FROM newline;

with newline as (
SELECT *
FROM _12123_refqualitycriteria_2_72 r272
WHERE r272.topiaid not in (SELECT topiaid FROM _12123_refqualitycriteria_avril_22))
INSERT INTO _12123_refqualitycriteria_avril_22
SELECT * FROM newline;

with newline as (
SELECT *
FROM _12123_refsolarvalis_2_72 r272
WHERE r272.topiaid not in (SELECT topiaid FROM _12123_refsolarvalis_avril_22))
INSERT INTO _12123_refsolarvalis_avril_22
SELECT * FROM newline;

CREATE TABLE _12123_refciblesagrosystgroupesciblesmaa_diff AS (
  SELECT
  r_avril_22.topiaid,
  r_avril_22.cible_edi_ref_label cible_edi_ref_label_2_72,  r3.cible_edi_ref_label cible_edi_ref_label_3,
  r_avril_22.groupe_cible_maa    groupe_cible_maa_2_72,     r3.groupe_cible_maa    groupe_cible_maa_3
  FROM refciblesagrosystgroupesciblesmaa r3
  INNER JOIN _12123_refciblesagrosystgroupesciblesmaa_avril_22 r_avril_22 on r3.topiaid = r_avril_22.topiaid
  WHERE r_avril_22.cible_edi_ref_label != r3.cible_edi_ref_label
  or r_avril_22.groupe_cible_maa != r3.groupe_cible_maa
);

CREATE TABLE _12123_refdestination_diff AS (
  SELECT
  r_avril_22.topiaid,
  r_avril_22.destination destination_2_72,  r3.destination destination_3
  FROM refdestination r3
  INNER JOIN _12123_refdestination_avril_22 r_avril_22 on r3.topiaid = r_avril_22.topiaid
  WHERE r_avril_22.destination != r3.destination
);

CREATE TABLE _12123_refelementvoisinage_diff AS (
  SELECT
  r_avril_22.topiaid,
  r_avril_22.iae_nom iae_nom_2_72,  r3.iae_nom iae_nom_3
  FROM refelementvoisinage r3
  INNER JOIN _12123_refelementvoisinage_avril_22 r_avril_22 on r3.topiaid = r_avril_22.topiaid
  WHERE r_avril_22.iae_nom != r3.iae_nom
);

CREATE TABLE _12123_refespece_diff AS (
  SELECT
  r_avril_22.topiaid,
  r_avril_22.libelle_espece_botanique    libelle_espece_botanique_2_72,    r3.libelle_espece_botanique    libelle_espece_botanique_3,
  r_avril_22.libelle_qualifiant_aee      libelle_qualifiant_aee_2_72,      r3.libelle_qualifiant_aee      libelle_qualifiant_aee_3,
  r_avril_22.libelle_type_saisonnier_aee libelle_type_saisonnier_aee_2_72, r3.libelle_type_saisonnier_aee libelle_type_saisonnier_aee_3,
  r_avril_22.libelle_destination_aee     libelle_destination_aee_2_72,     r3.libelle_destination_aee     libelle_destination_aee_3
  FROM refespece r3
  INNER JOIN _12123_refespece_avril_22 r_avril_22 on r3.topiaid = r_avril_22.topiaid
  WHERE r_avril_22.libelle_espece_botanique != r3.libelle_espece_botanique
  or r_avril_22.libelle_qualifiant_aee      != r3.libelle_qualifiant_aee
  or r_avril_22.libelle_type_saisonnier_aee != r3.libelle_type_saisonnier_aee
  or r_avril_22.libelle_destination_aee     != r3.libelle_destination_aee
);

CREATE TABLE _12123_refinterventionagrosysttravailedi_diff AS (
  SELECT
  r_avril_22.topiaid,
  r_avril_22.reference_label cible_edi_ref_label_2_72,  r3.reference_label cible_edi_ref_label_3
  FROM refinterventionagrosysttravailedi r3
  INNER JOIN _12123_refinterventionagrosysttravailedi_avril_22 r_avril_22 on r3.topiaid = r_avril_22.topiaid
  WHERE r_avril_22.reference_label != r3.reference_label
);

CREATE TABLE _12123_refmateriel_diff AS (
  SELECT
  r_avril_22.topiaid,
  r_avril_22.typeMateriel1 typeMateriel1_2_72, r3.typeMateriel1 typeMateriel1_3,
  r_avril_22.typeMateriel2 typeMateriel2_2_72, r3.typeMateriel2 typeMateriel2_3,
  r_avril_22.typeMateriel3 typeMateriel3_2_72, r3.typeMateriel3 typeMateriel3_3,
  r_avril_22.typeMateriel4 typeMateriel4_2_72, r3.typeMateriel4 typeMateriel4_3,
  r_avril_22.unite         unite_2_72,         r3.unite         unite_3
  FROM refmateriel r3
  INNER JOIN _12123_refmateriel_avril_22 r_avril_22 on r3.topiaid = r_avril_22.topiaid
  WHERE r_avril_22.typeMateriel1 != r3.typeMateriel1
  or r_avril_22.typeMateriel2      != r3.typeMateriel2
  or r_avril_22.typeMateriel3 != r3.typeMateriel3
  or r_avril_22.typeMateriel4     != r3.typeMateriel4
  or r_avril_22.unite             != r3.unite
);

CREATE TABLE _12123_refqualitycriteria_diff AS (
  SELECT
  r_avril_22.topiaid,
  r_avril_22.qualityCriteriaLabel qualityCriteriaLabel_2_72,  r3.qualityCriteriaLabel qualityCriteriaLabel_3
  FROM refqualitycriteria r3
  INNER JOIN _12123_refqualitycriteria_avril_22 r_avril_22 on r3.topiaid = r_avril_22.topiaid
  WHERE r_avril_22.qualityCriteriaLabel != r3.qualityCriteriaLabel
);

CREATE TABLE _12123_refsolarvalis_diff AS (
  SELECT
  r_avril_22.topiaid,
  r_avril_22.sol_nom sol_nom_2_72,                   r3.sol_nom          sol_nom_3,
  r_avril_22.sol_texture sol_texture_2_72,           r3.sol_texture      sol_texture_3,
  r_avril_22.sol_calcaire sol_calcaire_2_72,         r3.sol_calcaire     sol_calcaire_3,
  r_avril_22.sol_profondeur sol_profondeur_2_72,     r3.sol_profondeur   sol_profondeur_3,
  r_avril_22.sol_hydromorphie sol_hydromorphie_2_72, r3.sol_hydromorphie sol_hydromorphie_3,
  r_avril_22.sol_pierrosite sol_pierrosite_2_72,     r3.sol_pierrosite   sol_pierrosite_3
  FROM refsolarvalis r3
  INNER JOIN _12123_refsolarvalis_avril_22 r_avril_22 on r3.topiaid = r_avril_22.topiaid
  WHERE r_avril_22.sol_nom != r3.sol_nom
  or r_avril_22.sol_texture != r3.sol_texture
  or r_avril_22.sol_calcaire != r3.sol_calcaire
  or r_avril_22.sol_profondeur != r3.sol_profondeur
  or r_avril_22.sol_hydromorphie != r3.sol_hydromorphie
  or r_avril_22.sol_pierrosite != r3.sol_pierrosite
);

UPDATE refciblesagrosystgroupesciblesmaa ref3 set cible_edi_ref_label = ref2.cible_edi_ref_label, groupe_cible_maa = ref2.groupe_cible_maa from _12123_refciblesagrosystgroupesciblesmaa_avril_22 ref2 where ref2.topiaid = ref3.topiaid;
UPDATE refdestination ref3 set destination = ref2.destination from _12123_refdestination_avril_22 ref2 where ref2.topiaid = ref3.topiaid;
UPDATE refelementvoisinage ref3 set iae_nom = ref2.iae_nom from _12123_refelementvoisinage_avril_22 ref2 where ref2.topiaid = ref3.topiaid;
UPDATE refespece ref3 set libelle_espece_botanique = ref2.libelle_espece_botanique, libelle_qualifiant_aee = ref2.libelle_qualifiant_aee, libelle_type_saisonnier_aee = ref2.libelle_type_saisonnier_aee, libelle_destination_aee = ref2.libelle_destination_aee from _12123_refespece_avril_22 ref2 where ref2.topiaid = ref3.topiaid;
UPDATE refinterventionagrosysttravailedi ref3 set reference_label = ref2.reference_label from _12123_refinterventionagrosysttravailedi_avril_22 ref2 where ref2.topiaid = ref3.topiaid;
UPDATE refmateriel ref3 set typeMateriel1 = ref2.typeMateriel1, typeMateriel2 = ref2.typeMateriel2, typeMateriel3 = ref2.typeMateriel3, typeMateriel4 = ref2.typeMateriel4, unite = ref2.unite from _12123_refmateriel_avril_22 ref2 where ref2.topiaid = ref3.topiaid;
UPDATE refqualitycriteria ref3 set qualityCriteriaLabel = ref2.qualityCriteriaLabel from _12123_refqualitycriteria_avril_22 ref2 where ref2.topiaid = ref3.topiaid;
UPDATE refsolarvalis ref3 set sol_nom = ref2.sol_nom, sol_texture = ref2.sol_texture, sol_calcaire = ref2.sol_calcaire, sol_profondeur = ref2.sol_profondeur, sol_hydromorphie = ref2.sol_hydromorphie, sol_pierrosite = ref2.sol_pierrosite from _12123_refsolarvalis_avril_22 ref2 where ref2.topiaid = ref3.topiaid;

CREATE TABLE _12123_refciblesagrosystgroupesciblesmaa_3 AS (SELECT * FROM refciblesagrosystgroupesciblesmaa where topiaid not in (SELECT topiaid FROM _12123_refciblesagrosystgroupesciblesmaa_avril_22));
CREATE TABLE _12123_refdestination_3 AS (SELECT * FROM refdestination where topiaid not in (SELECT topiaid FROM _12123_refdestination_avril_22));
CREATE TABLE _12123_refelementvoisinage_3 AS (SELECT * FROM refelementvoisinage where topiaid not in (SELECT topiaid FROM _12123_refelementvoisinage_avril_22));
CREATE TABLE _12123_refespece_3 AS (SELECT * FROM refespece where topiaid not in (SELECT topiaid FROM _12123_refespece_avril_22));
CREATE TABLE _12123_refinterventionagrosysttravailedi_3 AS (SELECT * FROM refinterventionagrosysttravailedi where topiaid not in (SELECT topiaid FROM _12123_refinterventionagrosysttravailedi_avril_22));
CREATE TABLE _12123_refmateriel_3 AS (SELECT * FROM refmateriel where topiaid not in (SELECT topiaid FROM _12123_refmateriel_avril_22));
CREATE TABLE _12123_refqualitycriteria_3 AS (SELECT * FROM refqualitycriteria where topiaid not in (SELECT topiaid FROM _12123_refqualitycriteria_avril_22));
CREATE TABLE _12123_refsolarvalis_3 AS (SELECT * FROM refsolarvalis where topiaid not in (SELECT topiaid FROM _12123_refsolarvalis_avril_22));
