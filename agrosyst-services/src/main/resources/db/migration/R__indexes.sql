---
-- %%Ignore-License
---

CREATE INDEX IF NOT EXISTS crops_strategy_by_strategy_idx ON crops_strategy(strategy);

CREATE INDEX IF NOT EXISTS plots_by_growingsystem_idx ON plot(growingsystem);

CREATE INDEX IF NOT EXISTS marketingdestinationobjectives_by_growingsystem_idx ON marketingdestinationobjective(growingsystem);

CREATE INDEX IF NOT EXISTS toolscoupling_by_code_idx ON toolscoupling(code);
CREATE INDEX IF NOT EXISTS practicedintervention_toolscouplingcodes_by_owner_idx ON practicedintervention_toolscouplingcodes(owner);
CREATE INDEX IF NOT EXISTS croppingplanentry_by_domain_idx ON croppingplanentry(domain);

CREATE INDEX IF NOT EXISTS refHarvestingPrice_full_idx ON refHarvestingPrice(active, code_espece_botanique, code_qualifiant_AEE, code_destination_A, marketingPeriod, marketingPeriodDecade);

CREATE INDEX IF NOT EXISTS adventice_by_identifiant_idx ON RefAdventice(identifiant);
CREATE INDEX IF NOT EXISTS nuisible_edi_by_reference_code_idx ON RefNuisibleEDI(reference_code);
CREATE INDEX IF NOT EXISTS groupe_cible_by_code_groupe_cible_maa_idx ON RefCiblesAgrosystGroupesCiblesMAA(code_groupe_cible_maa);

CREATE INDEX IF NOT EXISTS decision_rules_domain_code_idx ON DecisionRule(domainCode);
CREATE INDEX IF NOT EXISTS domain_code_idx ON Domain(code);

CREATE INDEX IF NOT EXISTS RefMAADosesRefParGroupeCible_campagne_idx ON RefMAADosesRefParGroupeCible(campagne);
CREATE INDEX IF NOT EXISTS RefMAABiocontrole_campagne_idx ON RefMAABiocontrole(campagne);

CREATE INDEX IF NOT EXISTS equipments_toolsCoupling_equipments_idx ON equipments_toolsCoupling(equipments);
CREATE INDEX IF NOT EXISTS toolsCoupling_tractor_idx ON toolsCoupling(tractor);

CREATE INDEX IF NOT EXISTS practicedSystem_practicedPlot_idx ON practicedSystem(practicedPlot);

CREATE INDEX if not exists abstractphytoproductinputusage_domainphytoproductinput ON abstractphytoproductinputusage(domainphytoproductinput);
CREATE INDEX if not exists seedspeciesinputusage_domainseedspeciesinput ON seedspeciesinputusage(domainseedspeciesinput);
CREATE INDEX if not exists seedlotinputusage_domainseedlotinput ON seedlotinputusage(domainseedlotinput);
CREATE INDEX if not exists organicproductinputusage_domainorganicproductinput ON organicproductinputusage(domainorganicproductinput);
CREATE INDEX if not exists substrateinputusage_domainsubstrateinput ON substrateinputusage(domainsubstrateinput);
CREATE INDEX if not exists potinputusage_domainpotinput ON potinputusage(domainpotinput);
CREATE INDEX if not exists mineralproductinputusage_domainmineralproductinput ON mineralproductinputusage(domainmineralproductinput);
CREATE INDEX if not exists otherproductinputusage_domainotherinput ON otherproductinputusage(domainotherinput);
CREATE INDEX if not exists irrigationinputusage_domainirrigationinput ON irrigationinputusage(domainirrigationinput);

CREATE INDEX if not exists domainseedlotinput_cropseed ON domainseedlotinput(cropseed);
CREATE INDEX if not exists effectiveperennialcropcycle_croppingplanentry ON effectiveperennialcropcycle(croppingplanentry);
CREATE INDEX if not exists effectivecropcyclenode_croppingplanentry ON effectivecropcyclenode(croppingplanentry);
CREATE INDEX if not exists effectivecropcycleconnection_intermediatecroppingplanentry ON effectivecropcycleconnection(intermediatecroppingplanentry);
CREATE INDEX if not exists measurementsession_croppingplanentry ON measurementsession(croppingplanentry);
CREATE INDEX if not exists crops_strategy_crops ON crops_strategy(crops);
CREATE INDEX if not exists croppestmaster_crops ON croppestmaster_crops(crops);
CREATE INDEX if not exists crops_versemaster ON crops_versemaster(crops);
CREATE INDEX if not exists crops_foodmaster ON crops_versemaster(crops);
CREATE INDEX if not exists crops_yieldloss ON crops_yieldloss(crops);
CREATE INDEX if not exists arbocropadventicemaster_crops ON arbocropadventicemaster_crops(crops);
CREATE INDEX if not exists arbocroppestmaster_crops ON arbocropadventicemaster_crops(crops);

CREATE INDEX if not exists domainseedspeciesinput_speciesseed ON domainseedspeciesinput(speciesseed);
CREATE INDEX if not exists effectivecropcyclespecies_croppingplanspecies ON effectivecropcyclespecies(croppingplanspecies);
CREATE INDEX if not exists effectivespeciesstade_croppingplanspecies ON effectivespeciesstade(croppingplanspecies);
CREATE INDEX if not exists croppestmaster_species_species ON croppestmaster_species(species);
CREATE INDEX if not exists species_versemaster_species ON species_versemaster(species);
CREATE INDEX if not exists foodmaster_species_species ON foodmaster_species(species);
CREATE INDEX if not exists species_yieldloss_species ON species_yieldloss(species);
CREATE INDEX if not exists arbocropadventicemaster_species_species ON arbocropadventicemaster_species(species);
CREATE INDEX if not exists arbocroppestmaster_species_species ON arbocroppestmaster_species(species);
CREATE INDEX if not exists measure_croppingplanspecies ON measure(croppingplanspecies);

CREATE index IF NOT EXISTS ref_harvesting_price_scena_full_idx ON refharvestingprice(active, code_scenario, organic, code_destination_A, code_espece_botanique, code_qualifiant_AEE, marketingPeriod, marketingPeriodDecade) WHERE code_scenario <> ''::text;

CREATE index IF NOT EXISTS refactagroupecultures_idx ON refactagroupecultures(active, id_culture, id_groupe_culture);
CREATE index IF NOT EXISTS refactadosagespc_id_produit_id_traitement_idx ON refactadosagespc (active, id_produit, id_traitement, id_culture);
CREATE index IF NOT EXISTS refactadosagespc_amm_id_traitement_idx ON refactadosagespc (active, code_amm, id_traitement, id_culture);