---
-- %%Ignore-License
--
--

ALTER TABLE PracticedPlot ADD COLUMN importance double precision;
ALTER TABLE PracticedPlot ADD COLUMN domain varchar(255);
alter table PracticedPlot
    add constraint FK_7o49kfk9gj8j5w8ngdcohfc42
    foreign key (domain)
    references Domain;

ALTER TABLE PracticedSystem ADD COLUMN practicedPlot varchar(255);
alter table PracticedSystem
    add constraint FK_7o49kfk9gj8j5w8ngdcohfc84
    foreign key (practicedPlot)
    references PracticedPlot;

UPDATE PracticedSystem ps SET practicedPlot = (SELECT pp.topiaId FROM PracticedPlot pp WHERE pp.practicedSystem = ps.topiaId);
ALTER TABLE PracticedPlot DROP COLUMN practicedSystem;