---
-- %%Ignore-License
---

----------------------------------
--------     REALISE   -----------
----------------------------------

create table _11209_effectiveintervention_toolcouplings as (
  select * from effectiveintervention_toolcouplings
);

create table _11209_practicedintervention_toolscouplingcodes as (
  select * from practicedintervention_toolscouplingcodes
);

create temporary table interventions_with_more_tan_one_co as (
  select effectiveintervention from effectiveintervention_toolcouplings eitc1 group by effectiveintervention having count(effectiveintervention) > 1
);

-- on supprime ces doublons
create temporary table effectiveintervention_toolcouplings_duplicated_lines as(
select -- faux doublons dans la table effectiveintervention_toolcouplings 2 lignes dupliquées
  effectiveintervention, toolcouplings
  from effectiveintervention_toolcouplings eitc where effectiveintervention in (select effectiveintervention from interventions_with_more_tan_one_co)
  group by effectiveintervention, toolcouplings
  having count(toolcouplings) > 1);

delete from effectiveintervention_toolcouplings eitc where concat(eitc.effectiveintervention,eitc.toolcouplings) in (select concat(eitcdl.effectiveintervention,eitcdl.toolcouplings) from effectiveintervention_toolcouplings_duplicated_lines eitcdl);
insert into effectiveintervention_toolcouplings (effectiveintervention, toolcouplings) select effectiveintervention, toolcouplings from effectiveintervention_toolcouplings_duplicated_lines;

create temporary table croisement_action as (
  select distinct eitc.effectiveintervention, tc.topiaid tc_id, tc.topiacreatedate
  -- combinaison
  from interventions_with_more_tan_one_co eimto
  join effectiveintervention_toolcouplings eitc on eitc.effectiveintervention = eimto.effectiveintervention
  join toolscoupling tc on tc.topiaid = eitc.toolcouplings
  join mainsactions_toolscoupling mtc on mtc.toolscoupling = tc.topiaid -- action de la combinaison
  -- intervention
  join effectiveintervention ei on eitc.effectiveintervention = ei.topiaid
  join abstractaction aa on ei.topiaid = aa.effectiveintervention -- action de l'intervention
  -- jointure avec le referentiel des actions selon la double cle : action intervention = action referentiel = action de la combinaison
  join refinterventionagrosysttravailedi refint on refint.topiaid = aa.mainaction and mtc.mainsactions = refint.topiaid
  order by effectiveintervention, tc.topiacreatedate desc
);

create temporary table realise_comb_agarder as (
  -- remonte toutes les lignes dont l'intervention cible qu'une seule combinaison d'outils associé à une action principale
  -- les autres lignes (combinaisons d'outils) assosiées à cette intervention ne sont pas associées à une action
  select effectiveintervention, tc_id
  from croisement_action
  where effectiveintervention not in (select effectiveintervention from croisement_action group by effectiveintervention having count(effectiveintervention)>1)
  union
  select ca.effectiveintervention,ca.tc_id
  from croisement_action ca
  -- jointure avec croisement_action de ceux encore en double, pour leur attribuer un numero , on ne gardera que le 1 puisque ils ont ete tries selon leur date de creation
  join (select effectiveintervention, tc_id, topiacreatedate,
      row_number() OVER (PARTITION BY ca.effectiveintervention) as rownb -- numero de ligne par rapport au groupe effectiveintervention
    from croisement_action ca
    where effectiveintervention in (select effectiveintervention from croisement_action group by effectiveintervention having count(effectiveintervention)>1)
    ) ca2 on ca2.effectiveintervention = ca.effectiveintervention and ca2.tc_id = ca.tc_id
  where rownb=1
);

-- on conserve un lien entre intervention et combinaisons d'outils pour
-- ne retourne rien en date du 27-09-23
insert into realise_comb_agarder(effectiveintervention, tc_id) (
  select eitc0.effectiveintervention, eitc0.toolcouplings
  from effectiveintervention_toolcouplings eitc0
  join interventions_with_more_tan_one_co eiwmto0 on eitc0.effectiveintervention = eiwmto0.effectiveintervention
  where eitc0.effectiveintervention not in (select effectiveintervention from realise_comb_agarder)
  and concat(eitc0.effectiveintervention, eitc0.toolcouplings) not in (select concat(effectiveintervention, tc_id) from croisement_action)
  and concat(eitc0.effectiveintervention, eitc0.toolcouplings) in (
    select concat(res.effectiveintervention, res.tc_id) from (
      select eitc.effectiveintervention, tc.topiaid tc_id, tc.topiacreatedate, row_number() OVER (PARTITION BY eitc.effectiveintervention) as rownb -- numero de ligne par rapport au groupe effectiveintervention
      from effectiveintervention_toolcouplings eitc
      join interventions_with_more_tan_one_co eiwmto on eitc.effectiveintervention = eiwmto.effectiveintervention
      join toolscoupling tc on tc.topiaid = eitc.toolcouplings
      order by tc.topiacreatedate desc) as res
    where res.rownb=1)
);

-- on supprime toutes les interventions ou l'on a choisi une combinaison d'outil assiciée a une action principale
DELETE FROM effectiveintervention_toolcouplings
where effectiveintervention in (
  select effectiveintervention from realise_comb_agarder
);

insert into effectiveintervention_toolcouplings(effectiveintervention, toolcouplings)
  select effectiveintervention, tc_id from realise_comb_agarder;

-- ne supprime rien en date du 27-09-23
DELETE FROM effectiveintervention_toolcouplings where concat(effectiveintervention, toolcouplings) IN (
  select concat(effectiveintervention, tc_id)
  from realise_comb_agarder
  where effectiveintervention in (
    select effectiveintervention
    from realise_comb_agarder eitc1
    group by effectiveintervention having count(effectiveintervention) > 1)
  and concat(effectiveintervention, tc_id) not in(
    select concat(effectiveintervention, tc_id) from croisement_action
  )
);

----------------------------------
--------   SYNTHETISE  -----------
----------------------------------

create temporary table practiced_interventions_with_more_tan_one_co as ( -- faux doublons dans la table practicedintervention_toolscouplingcodes 13 lignes dupliquées
  select distinct owner
  from practicedintervention_toolscouplingcodes
  group by owner having count(owner) > 1
);

-- on supprime ces doublons
create temporary table practicedintervention__toolcouplings_duplicated_lines as(
select -- faux doublons dans la table effectiveintervention_toolcouplings 2 lignes dupliquées
  owner, toolscouplingcodes
  from practicedintervention_toolscouplingcodes where owner in (select owner from practiced_interventions_with_more_tan_one_co)
  group by owner, toolscouplingcodes
  having count(toolscouplingcodes) > 1);

delete from practicedintervention_toolscouplingcodes where concat(owner, toolscouplingcodes) in (select concat(owner, toolscouplingcodes) from practicedintervention__toolcouplings_duplicated_lines);
insert into practicedintervention_toolscouplingcodes (owner, toolscouplingcodes) select owner, toolscouplingcodes from practicedintervention__toolcouplings_duplicated_lines;

create temporary table croisement_combinaison_domaine as (
  select distinct pitc."owner", tc.topiacreatedate, pitc.toolscouplingcodes,tc.topiaid tc_id,aa.mainaction action_interv,mtc.mainsactions action_tc,
  row_number() OVER (PARTITION BY pitc.owner) as rownb -- numero de ligne selon owner, l'intervention , ordoné par la date de creation
  from practiced_interventions_with_more_tan_one_co dup
  join practicedintervention_toolscouplingcodes pitc on dup.owner = pitc."owner"
  join practicedintervention pi on pitc."owner" = pi.topiaid
  join abstractaction aa on pi.topiaid = aa.practicedintervention -- action de l'intervention
  -- combinaison outils. On remonte au domaine pour joindre toolscoupling et ne selectionner que la ligne de la campagne de l'intervention puisque on a que le code de la combinaison
  left join practicedcropcycleconnection pccc on pi.practicedcropcycleconnection = pccc.topiaid -- culture saisonnieres
  left join practicedcropcyclenode pcn on pcn.topiaid = pccc.target
  left join practicedcropcyclephase pp on pp.topiaid = pi.practicedcropcyclephase -- cultures perennes
  join practicedcropcycle pcy on (pcy.topiaid = pcn.practicedseasonalcropcycle or pp.practicedperennialcropcycle = pcy.topiaid)
  join practicedsystem ps on pcy.practicedsystem = ps.topiaid
  join growingsystem gs on gs.topiaid = ps.growingsystem
  join growingplan gp on gp.topiaid = gs.growingplan
  join toolscoupling tc on tc.code = pitc.toolscouplingcodes and gp."domain" = tc."domain"
  join mainsactions_toolscoupling mtc on mtc.toolscoupling = tc.topiaid -- action de la combinaison
  order by owner, tc.topiacreatedate desc
);

create temporary table synthetise_comb_agarder as (
  with croisement_action as (
    select owner,tc_id,toolscouplingcodes
    from croisement_combinaison_domaine
    -- jointure avec le referentiel des actions selon la double cle : action intervention = action referentiel = action de la combinaison
    join refinterventionagrosysttravailedi refint on refint.topiaid = action_interv and action_tc = refint.topiaid
  )
  -- Selection avec le croisement des actions : ne regle le problème que pour 89 interventions
  select owner,tc_id,toolscouplingcodes, 'croisement action' as motif
  from croisement_action
  where owner not in (select owner from croisement_action group by owner having count(owner)>1)
  union
  -- Selection la date la plus recente et sinon une ligne au hasard grace au numero de ligne attitré plus haut: regle les 334 interventions qui restent
  select owner,tc_id,toolscouplingcodes, 'plus recente ou ligne au hasard' as motif
  from croisement_combinaison_domaine cd
  where owner not in (select owner from croisement_action group by owner having count(owner)=1)
  and rownb = 1
 );

-- DELETION de la table practicedintervention_toolscouplingcodes
DELETE FROM practicedintervention_toolscouplingcodes
where (owner,toolscouplingcodes) in (
  select pitc.owner, pitc.toolscouplingcodes from practicedintervention_toolscouplingcodes pitc
  left join synthetise_comb_agarder keep on keep.owner = pitc.owner and keep.toolscouplingcodes = pitc.toolscouplingcodes
  where keep.owner is null
  and pitc.owner in (select owner from practiced_interventions_with_more_tan_one_co)
);






