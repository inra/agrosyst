---
-- %%Ignore-License
---

ALTER TABLE synthetise_echelle_culture ADD COLUMN marge_directe_reelle_avec_autoconso float8 NULL;
ALTER TABLE synthetise_echelle_culture ADD COLUMN marge_directe_reelle_sans_autoconso float8 NULL;
ALTER TABLE synthetise_echelle_culture ADD COLUMN marge_directe_reelle_taux_de_completion float8 NULL;
ALTER TABLE synthetise_echelle_culture ADD COLUMN marge_directe_reelle_detail_champs_non_renseig text NULL;

ALTER TABLE synthetise_echelle_synthetise ADD COLUMN marge_directe_reelle_avec_autoconso float8 NULL;
ALTER TABLE synthetise_echelle_synthetise ADD COLUMN marge_directe_reelle_sans_autoconso float8 NULL;
ALTER TABLE synthetise_echelle_synthetise ADD COLUMN marge_directe_reelle_taux_de_completion float8 NULL;
ALTER TABLE synthetise_echelle_synthetise ADD COLUMN marge_directe_reelle_detail_champs_non_renseig text NULL;

ALTER TABLE realise_echelle_culture ADD COLUMN marge_directe_reelle_avec_autoconso float8 NULL;
ALTER TABLE realise_echelle_culture ADD COLUMN marge_directe_reelle_sans_autoconso float8 NULL;
ALTER TABLE realise_echelle_culture ADD COLUMN marge_directe_reelle_taux_de_completion float8 NULL;
ALTER TABLE realise_echelle_culture ADD COLUMN marge_directe_reelle_detail_champs_non_renseig text NULL;

ALTER TABLE realise_echelle_itk ADD COLUMN marge_directe_reelle_avec_autoconso float8 NULL;
ALTER TABLE realise_echelle_itk ADD COLUMN marge_directe_reelle_sans_autoconso float8 NULL;
ALTER TABLE realise_echelle_itk ADD COLUMN marge_directe_reelle_taux_de_completion float8 NULL;
ALTER TABLE realise_echelle_itk ADD COLUMN marge_directe_reelle_detail_champs_non_renseig text NULL;

ALTER TABLE realise_echelle_parcelle ADD COLUMN marge_directe_reelle_avec_autoconso float8 NULL;
ALTER TABLE realise_echelle_parcelle ADD COLUMN marge_directe_reelle_sans_autoconso float8 NULL;
ALTER TABLE realise_echelle_parcelle ADD COLUMN marge_directe_reelle_taux_de_completion float8 NULL;
ALTER TABLE realise_echelle_parcelle ADD COLUMN marge_directe_reelle_detail_champs_non_renseig text NULL;

ALTER TABLE realise_echelle_sdc ADD COLUMN marge_directe_reelle_avec_autoconso float8 NULL;
ALTER TABLE realise_echelle_sdc ADD COLUMN marge_directe_reelle_sans_autoconso float8 NULL;
ALTER TABLE realise_echelle_sdc ADD COLUMN marge_directe_reelle_taux_de_completion float8 NULL;
ALTER TABLE realise_echelle_sdc ADD COLUMN marge_directe_reelle_detail_champs_non_renseig text NULL;

ALTER TABLE realise_echelle_zone ADD COLUMN marge_directe_reelle_avec_autoconso float8 NULL;
ALTER TABLE realise_echelle_zone ADD COLUMN marge_directe_reelle_sans_autoconso float8 NULL;
ALTER TABLE realise_echelle_zone ADD COLUMN marge_directe_reelle_taux_de_completion float8 NULL;
ALTER TABLE realise_echelle_zone ADD COLUMN marge_directe_reelle_detail_champs_non_renseig text NULL;
