---
-- %%Ignore-License
---

-- to avoid conflict
update refactatraitementsproduit set id_produit = CONCAT(id_produit, '_', 'SUPP') where active is false ;
-- 166

-- 999909 S3 -> 147 H1
update refactatraitementsproduit set id_traitement = 147, code_traitement = 'H1'
where topiaid in (
  select ratp.topiaid
  from refactatraitementsproduit ratp
  where ratp.id_traitement = 999909
  and code_traitement = 'S3'
  and not exists (
    select 1
    from refactatraitementsproduit ratp0
    where ratp0.id_traitement = 147
    and ratp0.code_traitement = 'H1'
    and ratp0.id_produit = ratp.id_produit
    and ratp0.code_traitement_maa = ratp.code_traitement_maa
    and ratp0.refcountry = ratp.refcountry
    and ratp0.nodu = ratp.nodu
    and ratp0.code_amm = ratp.code_amm)
);
-- about 870 products

update domainphytoproductinput dppi set refinput = (
select ratp1.topiaid from refactatraitementsproduit ratp, refactatraitementsproduit ratp1
  where ratp1.id_traitement = 147
  and ratp1.code_traitement = 'H1'
  and ratp.refcountry = ratp1.refcountry
  and ratp.id_produit = ratp1.id_produit
  and ratp.nodu = ratp1.nodu
  and ratp.code_amm = ratp1.code_amm
  and ratp.code_traitement_maa = ratp1.code_traitement_maa
  and ratp.id_traitement  = 999909
  and ratp.code_traitement = 'S3'
  and ratp.topiaid = dppi.refinput
  )
where dppi.refinput = (
select ratp.topiaid from refactatraitementsproduit ratp, refactatraitementsproduit ratp1
  where ratp1.id_traitement = 147
  and ratp1.code_traitement = 'H1'
  and ratp.refcountry = ratp1.refcountry
  and ratp.id_produit = ratp1.id_produit
  and ratp.nodu = ratp1.nodu
  and ratp.code_amm = ratp1.code_amm
  and ratp.code_traitement_maa = ratp1.code_traitement_maa
  and ratp.id_traitement  = 999909
  and ratp.code_traitement = 'S3'
  and ratp.topiaid = dppi.refinput
);
-- 24

-- disable remaining refactatraitementsproduit that have historic ones existing
update refactatraitementsproduit set id_produit = CONCAT(id_produit, '_', 'SUPP'), active = false
where topiaid in (
select ratp.topiaid from refactatraitementsproduit ratp, refactatraitementsproduit ratp1
  where ratp1.id_traitement = 147
  and ratp1.code_traitement = 'H1'
  and ratp.refcountry = ratp1.refcountry
  and ratp.id_produit = ratp1.id_produit
  and ratp.nodu = ratp1.nodu
  and ratp.code_amm = ratp1.code_amm
  and ratp.code_traitement_maa = ratp1.code_traitement_maa
  and ratp.id_traitement  = 999909
  and ratp.code_traitement = 'S3'
);
-- 2

-- update all other to historic ones
update refactatraitementsproduit set id_traitement = 147, code_traitement = 'H1'
where id_traitement  = 999909
and code_traitement = 'S3'
and id_produit not like '%_SUPP'
;
-- 0

-- 999908 S4 -> 153 I2
update refactatraitementsproduit set id_traitement = 153, code_traitement = 'I2'
where topiaid in (
select ratp.topiaid
from refactatraitementsproduit ratp
where ratp.id_traitement = 999908
and ratp.code_traitement = 'S4'
and not exists (
  select 1
  from refactatraitementsproduit ratp0
  where ratp0.id_traitement = 153
  and ratp0.code_traitement = 'I2'
  and ratp0.id_produit = ratp.id_produit
  --and ratp0.code_traitement_maa = ratp.code_traitement_maa
  and ratp0.refcountry = ratp.refcountry
  and ratp0.nodu = ratp.nodu
  and ratp0.code_amm = ratp.code_amm
));
-- about 279 products

-- migrate domainphytoproductinput using deprecated refactatraitementsproduit whith id_traitement = 999908 and code_traitement = S4 to ones with id_traitement = 153 and code_traitement = 'I2'
update domainphytoproductinput dppi set refinput = (
select ratp1.topiaid from refactatraitementsproduit ratp, refactatraitementsproduit ratp1
  where ratp1.id_traitement = 153
  and ratp1.code_traitement = 'I2'
  and ratp.refcountry = ratp1.refcountry
  and ratp.id_produit = ratp1.id_produit
  and ratp.nodu = ratp1.nodu
  and ratp.code_amm = ratp1.code_amm
  and ratp.code_traitement_maa = ratp1.code_traitement_maa
  and ratp.id_traitement  = 999908
  and ratp.code_traitement = 'S4'
  and ratp.topiaid = dppi.refinput)
where dppi.refinput = (
select ratp.topiaid from refactatraitementsproduit ratp, refactatraitementsproduit ratp1
  where ratp1.id_traitement = 153
  and ratp1.code_traitement = 'I2'
  and ratp.refcountry = ratp1.refcountry
  and ratp.id_produit = ratp1.id_produit
  and ratp.nodu = ratp1.nodu
  and ratp.code_amm = ratp1.code_amm
  and ratp.code_traitement_maa = ratp1.code_traitement_maa
  and ratp.id_traitement  = 999908
  and ratp.code_traitement = 'S4'
  and ratp.topiaid = dppi.refinput
);
-- 12

-- to avoid conflict isolate duplicated entries
update refactatraitementsproduit set id_produit = CONCAT(id_produit, '_', 'SUPP'), active = false
where topiaid in(
select ratp.topiaid from refactatraitementsproduit ratp, refactatraitementsproduit ratp1
  where ratp1.id_traitement = 153
  and ratp1.code_traitement = 'I2'
  and ratp.refcountry = ratp1.refcountry
  and ratp.id_produit = ratp1.id_produit
  and ratp.nodu = ratp1.nodu
  and ratp.code_amm = ratp1.code_amm
  --and ratp.code_traitement_maa = ratp1.code_traitement_maa
  and ratp.id_traitement  = 999908
  and ratp.code_traitement = 'S4'
);
-- 3

-- migrate all other refactatraitementsproduit with id_traitement = 153 and code_traitement = 'I2' to id_traitement = 999908 and code_traitement = 'S4'
update refactatraitementsproduit set id_traitement = 153, code_traitement = 'I2'
where id_traitement  = 999908
and code_traitement = 'S4';
-- 12

-- 999909, 'S3' -> 147, 'H1'

update refactatraitementsproduit set id_traitement = 147, code_traitement = 'H1'
where id_traitement  = 999909
and code_traitement = 'S3';
-- 31

-- 999907 S5 -> 140 F1
update refactatraitementsproduit set id_traitement  = 140, code_traitement = 'F1'
where topiaid in (
select ratp.topiaid
from refactatraitementsproduit ratp
where ratp.id_traitement= 999907
and ratp.code_traitement = 'S5'
and not exists (
  select 1
  from refactatraitementsproduit ratp0
  where ratp0.id_traitement = 140
  and ratp0.code_traitement = 'F1'
  and ratp0.id_produit = ratp.id_produit
  and ratp0.code_traitement_maa = ratp.code_traitement_maa
  and ratp0.refcountry = ratp.refcountry
  and ratp0.nodu = ratp.nodu
  and ratp0.code_amm = ratp.code_amm
));
-- about 825 products

update domainphytoproductinput dppi set refinput = (
select ratp1.topiaid from refactatraitementsproduit ratp, refactatraitementsproduit ratp1
  where ratp1.id_traitement = 140
  and ratp1.code_traitement = 'F1'
  and ratp.refcountry = ratp1.refcountry
  and ratp.id_produit = ratp1.id_produit
  and ratp.nodu = ratp1.nodu
  and ratp.code_amm = ratp1.code_amm
  and ratp.code_traitement_maa = ratp1.code_traitement_maa
  and ratp.id_traitement  = 999907
  and ratp.code_traitement = 'S5'
  and ratp.topiaid = dppi.refinput
  )
where dppi.refinput = (
select ratp.topiaid from refactatraitementsproduit ratp, refactatraitementsproduit ratp1
  where ratp1.id_traitement = 140
  and ratp1.code_traitement = 'F1'
  and ratp.refcountry = ratp1.refcountry
  and ratp.id_produit = ratp1.id_produit
  and ratp.nodu = ratp1.nodu
  and ratp.code_amm = ratp1.code_amm
  and ratp.code_traitement_maa = ratp1.code_traitement_maa
  and ratp.id_traitement  = 999907
  and ratp.code_traitement = 'S5'
  and ratp.topiaid = dppi.refinput
);
-- 69

-- to avoid conflict isolate duplicated entries
update refactatraitementsproduit set id_produit = CONCAT(id_produit, '_', 'SUPP'), active = false
where topiaid in(
select ratp.topiaid from refactatraitementsproduit ratp, refactatraitementsproduit ratp1
  where ratp1.id_traitement = 140
  and ratp1.code_traitement = 'F1'
  and ratp.refcountry = ratp1.refcountry
  and ratp.id_produit = ratp1.id_produit
  and ratp.nodu = ratp1.nodu
  and ratp.code_amm = ratp1.code_amm
  and ratp.code_traitement_maa = ratp1.code_traitement_maa
  and ratp.id_traitement  = 999907
  and ratp.code_traitement = 'S5'
);
-- 5

update refactatraitementsproduit set id_traitement = 140, code_traitement = 'F1'
where id_traitement  = 999907
and code_traitement = 'S5';
-- 23

delete from refactatraitementsproduit where id_traitement = 999909 and code_traitement = 'S3';
delete from refactatraitementsproduit where id_traitement = 999908 and code_traitement = 'S4';
delete from refactatraitementsproduit where id_traitement = 999907 and code_traitement = 'S5';

-- 23437 entrées dans la table refactatraitementsproduit

-- table refactatraitementsproduitscateg
delete from refactatraitementsproduitscateg where id_traitement = 999909 and code_traitement = 'S3';
delete from refactatraitementsproduitscateg where id_traitement = 999908 and code_traitement = 'S4';
delete from refactatraitementsproduitscateg where id_traitement = 999907 and code_traitement = 'S5';
delete from refactatraitementsproduitscateg where code_traitement = 'S6' and active is false;

update refactatraitementsproduitscateg set id_traitement = 147 where code_traitement = 'S3';
update refactatraitementsproduitscateg set id_traitement = 153 where code_traitement = 'S4';
update refactatraitementsproduitscateg set id_traitement = 140 where code_traitement = 'S5';

-- faire la mise à jour des refactadosagespc
-- table refactadosagespc
update refactadosagespc set id_traitement = 147, code_traitement = 'H1' where id_traitement = 999909 and code_traitement = 'S3';
-- 1136
update refactadosagespc set id_traitement = 153, code_traitement = 'I2' where id_traitement = 999908 and code_traitement = 'S4';
-- 15
update refactadosagespc set id_traitement = 140, code_traitement = 'F1' where id_traitement = 999907 and code_traitement = 'S5';
-- 87

-- faire la mise à jour des refprixphyto
update refprixphyto set id_traitement = 147 where id_traitement = 999909;
-- 6370
update refprixphyto set id_traitement = 153 where id_traitement = 999908;
-- 2106
update refprixphyto set id_traitement = 140 where id_traitement = 999907;
-- 66630

insert into refgroupecibletraitement (topiaid, topiaversion, topiacreatedate, code_traitement_maa, id_traitement, code_traitement, nom_traitement, code_groupe_cible_maa, active) values
(CONCAT('fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitement_', uuid_in(md5(random()::text || random()::text)::cstring)), 1, '2024-12-05 00:00:00.000', 'S3', 147, 'H1', 'Herbicides - Désherbants sélectifs et non sélectifs', '', true),
(CONCAT('fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitement_', uuid_in(md5(random()::text || random()::text)::cstring)), 1, '2024-12-05 00:00:00.000', 'S4', 153, 'I2', 'Insecticides - Traitement des parties aériennes', '', true),
(CONCAT('fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitement_', uuid_in(md5(random()::text || random()::text)::cstring)), 1, '2024-12-05 00:00:00.000', 'S5', 140, 'F1', 'Fongicides - Traitement des parties aériennes', '', true);
