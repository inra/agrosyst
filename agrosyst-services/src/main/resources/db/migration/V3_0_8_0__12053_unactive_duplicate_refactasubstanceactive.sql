---
-- %%Ignore-License
---

UPDATE refactasubstanceactive set active = false
  WHERE topiaid IN
      (SELECT topiaid
       FROM
          (SELECT topiaid,
                  ROW_NUMBER() OVER( PARTITION BY id_produit, LOWER(unaccent(nom_commun_sa)), active) AS row_num
          FROM refactasubstanceactive where active is true
          order by topiaversion DESC) t
          WHERE t.row_num > 1 );
