<#ftl output_format="XHTML">
<#--
 #%L
 Agrosyst :: Services
 %%
 Copyright (C) 2017 INRA, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<h3>Maîtrise de la verse</h3>
<#if verseMastersSplitted??>
<#list verseMastersSplitted as cropObjects>
  <table class="gridTable">
    <tr>
      <th scope="col" width="19%"></th>
      <#list cropObjects as cropObject>
        <th scope="col" class="crop" width="27%">
          <#list cropObject.crops as crop>
            ${crop.name}<#sep>, </#sep>
          </#list>
        </th>
      </#list>
    </tr>
    <tr class="header">
      <td class="secondary" colspan="${cropObjects.size() + 1}">
        Niveau de risque&#160;:
      </td>
    </tr>
    <tr>
      <td class="secondary">
        - Échelle de risque
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(enum.of(cropObject, "riskScale"))!"-"}
        </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        - Expression de l’agriculteur
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(cropObject.riskFarmerComment)!"-"}
        </td>
      </#list>
    </tr>
    <tr class="header">
      <td class="secondary" colspan="${cropObjects.size() + 1}">
        Résultats obtenus, niveau de maîtrise finale&#160;:
      </td>
    </tr>
    <tr>
      <td class="secondary">
        - Échelle de maîtrise
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(enum.of(cropObject, "masterScale", "Verse"))!"-"}
        </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        - Qualification du niveau de maîtrise
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(enum.of(cropObject, "qualifier"))!"-"}
        </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        - Expression de l'agriculteur
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(cropObject.resultFarmerComment)!"-"}
        </td>
      </#list>
    </tr>
    <tr class="header">
      <td class="secondary" colspan="${cropObjects.size() + 1}">
        IFT&#160;:
      </td>
    </tr>
    <tr>
      <td class="secondary">
        - IFT-Régulateur
      </td>
      <#list cropObjects as cropObject>
          <td>
            ${(cropObject.iftMain)!"-"}
          </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
       - Commentaires du conseiller
      </td>
      <#list cropObjects as cropObject>
          <td>
            ${(cropObject.adviserComments)!"-"}
          </td>
      </#list>
    </tr>
  </table>
  <#sep>
  <div class="newPage"></div>
  </#sep>
</#list>
</#if>
