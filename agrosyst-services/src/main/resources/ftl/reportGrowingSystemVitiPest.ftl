<#ftl output_format="XHTML">
<#--
 #%L
 Agrosyst :: Services
 %%
 Copyright (C) 2017 INRA, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<h3>Maîtrise des ravageurs</h3>
<#if vitiPestMastersSplitted??>
    <#list vitiPestMastersSplitted as vitiPestMasters>
      <table class="gridTable">
        <tr>
          <th scope="col" width="19%"></th>
          <#list vitiPestMasters as vitiPestMaster>
            <th scope="col" class="agressor" width="27%">
              ${(groupesCiblesParCode[vitiPestMaster.object.codeGroupeCibleMaa])!"-"} / ${(vitiPestMaster.object.agressor.reference_label)!"-"}
            </th>
          </#list>
        </tr>
        <tr class="header">
          <td class="secondary" colspan="${vitiPestMasters.size() + 1}">
            Pression sur le système de culture&#160;:
          </td>
        </tr>
        <tr>
          <td class="secondary">
            - Échelle de pression
          </td>
          <#list vitiPestMasters as vitiPestMaster>
            <td>
              ${(enum.of(vitiPestMaster, "pressureScale"))!"-"}
            </td>
          </#list>
        </tr>
        <tr>
          <td class="secondary">
            - Evolution par rapport à l’année précédente
          </td>
          <#list vitiPestMasters as vitiPestMaster>
            <td>
              ${(enum.of(vitiPestMaster, "pressureEvolution"))!"-"}
            </td>
          </#list>
        </tr>
        <tr>
          <td class="secondary">
            - Expression de l’agriculteur
          </td>
          <#list vitiPestMasters as vitiPestMaster>
            <td>
              ${(vitiPestMaster.pressureFarmerComment)!"-"}
            </td>
          </#list>
        </tr>
        <tr class="header">
          <td class="secondary" colspan="${vitiPestMasters.size() + 1}">
            Résultats obtenus, niveau de maîtrise finale&#160;:
          </td>
        </tr>
        <tr>
          <td class="secondary">
            - Échelle de maîtrise
          </td>
          <#list vitiPestMasters as vitiPestMaster>
            <td>
              ${(enum.of(vitiPestMaster, "masterScale", "VitiPest"))!"-"}
            </td>
          </#list>
        </tr>
        <tr>
          <td class="secondary">
            - Note globale d’attaque sur feuilles
          </td>
          <#list vitiPestMasters as vitiPestMaster>
            <td>
              ${(enum.of(vitiPestMaster, "leafPestAttackRate"))!"-"}
            </td>
          </#list>
        </tr>
        <#if dephyExpe>
          <tr>
            <td class="secondary">
              - Nombre de perforations pour 100 grappes
            </td>
            <td>
              ${(vitiPestMaster.nbPestGrapePerforation)!"-"}
            </td>
          </tr>
        <#else>
          <tr>
            <td class="secondary">
              - Note globale d’attaque sur grappes
            </td>
            <#list vitiPestMasters as vitiPestMaster>
              <td>
                ${(enum.of(vitiPestMaster, "grapePestAttackRate"))!"-"}
              </td>
            </#list>
          </tr>
        </#if>
        <tr>
          <td class="secondary">
            - Qualification du niveau de maîtrise
          </td>
          <#list vitiPestMasters as vitiPestMaster>
            <td>
              ${(enum.of(vitiPestMaster, "qualifier"))!"-"}
            </td>
          </#list>
        </tr>
        <tr>
          <td class="secondary">
            - Expression de l’agriculteur
          </td>
          <#list vitiPestMasters as vitiPestMaster>
            <td>
              ${(vitiPestMaster.resultFarmerComment)!"-"}
            </td>
          </#list>
        </tr>
        <tr>
          <td class="secondary">
            - Commentaires du conseiller
          </td>
          <#list vitiPestMasters as vitiPestMaster>
            <td>
              ${(vitiPestMaster.adviserComments)!"-"}
            </td>
          </#list>
        </tr>
        <tr class="header">
          <td class="secondary" colspan="${vitiPestMasters.size() + 1}">
            IFT&#160;:
          </td>
        </tr>
        <tr>
          <td class="secondary">
            - Nombre de traitements
          </td>
          <#list vitiPestMasters as vitiPestMaster>
              <td>
                ${(vitiPestMaster.treatmentCount)!"-"}
              </td>
          </#list>
        </tr>
        <#if dephyExpe>
          <tr>
            <td class="secondary">
              - dont nombre de traitements obligatoires
            </td>
            <#list vitiPestMasters as vitiPestMaster>
                <td>
                  ${(vitiPestMaster.nbPestTraitementRequired)!"-"}
                </td>
            </#list>
          </tr>
        </#if>
        <tr>
          <td class="secondary">
           - IFT-Fongicide (chimique)
          </td>
          <#list vitiPestMasters as vitiPestMaster>
              <td>
                ${(vitiPestMaster.chemicalFungicideIFT)!"-"}
              </td>
          </#list>
        </tr>
        <tr>
          <td class="secondary">
           - IFT-Fongicide biocontrôle
          </td>
          <#list vitiPestMasters as vitiPestMaster>
              <td>
                ${(vitiPestMaster.bioControlFungicideIFT)!"-"}
              </td>
          </#list>
        </tr>
      </table>
      <#sep>
      <div class="newPage"></div>
      </#sep>
    </#list>
</#if>

<ul>
  <li>IFT-ravageurs du système de culture (chimique) : ${(reportGrowingSystem.vitiPestChemicalPestIFT)!"-"}</li>
  <li>IFT-ravageurs biocontrôle du système de culture : ${(reportGrowingSystem.vitiPestBioControlPestIFT)!"-"}</li>
  <li>Niveau global de maîtrise des ravageurs : ${(enum.of(reportGrowingSystem, "vitiPestQualifier"))!"-"}</li>
</ul>
