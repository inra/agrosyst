<#ftl output_format="XHTML">
<#--
 #%L
 Agrosyst :: Services
 %%
 Copyright (C) 2017 INRA, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<#if sectionRavageurs??>
  <h3>Maîtrise des ravageurs</h3>
  <table class="data">
   <tr>
    <th scope="col">Bio-agresseur considéré</th>
    <th scope="col">Objectifs agronomiques</th>
    <th scope="col">Résultats attendus par l'agriculteur</th>
    <th scope="col">Objectifs de maîtrise / niveau de tolérance</th>
    <th scope="col">Stratégies de gestion pluriannuelles ou extraparcellaires</th>
    <th scope="col">Tactiques annuelles à l'échelle de la parcelle</th>
   </tr>
       <#list sectionRavageurs as section>
         <tr>
           <td><#if section.bioAgressor?has_content>${section.bioAgressor.reference_label}<#else>"-"</#if></td>
           <td>${(section.agronomicObjective)!"-"}</td>
           <td>${(section.expectedResult)!"-"}</td>
           <td>${(enum.of(section, "categoryObjective", "RAVAGEURS"))!"-"}</td>
           <#include "strategiesSection.ftl">
         </tr>
       </#list>
  </table>
</#if>
