<#ftl output_format="XHTML">
<#--
 #%L
 Agrosyst :: Services
 %%
 Copyright (C) 2017 INRA, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
  <!-- header for growing system only -->
  <div id="customHeader" style="position: running(customHeader);" class="customHeader">
     <#list reportGrowingSystem.sectors as sector>${enum.tr("fr.inra.agrosyst.api.entities.Sector." + sector)!"-"}<#sep>, </#sep></#list>,
     ${reportGrowingSystem.growingSystem.growingPlan.domain.name},
     ${reportGrowingSystem.growingSystem.growingPlan.domain.campaign?c},
     ${reportGrowingSystem.growingSystem.name}
  </div>

  <h1>Bilan de campagne / échelle regional - ${reportRegional.name}</h1>

  <h2>Faits marquants de l'année:</h2>

  <h3>Conditions climatiques de l’année:</h3>
  <ul>
    <li>Pluviométrie : nombre de mm du 1er mars au 30 juin : ${(reportRegional.rainfallMarchJune)!"-"}</li>
    <li>Nombre de jours de pluie du 1er mars au 30 juin : ${(reportRegional.rainyDaysMarchJune)!"-"}</li>
    <li>Pluviométrie : nombre de mm du 1er juillet au 31 octobre : ${(reportRegional.rainfallJulyOctober)!"-"}</li>
  </ul>

  <#if reportRegional.sectors?? && reportRegional.sectorSpecies?? &&
       reportRegional.sectors?seq_contains("ARBORICULTURE") && reportRegional.sectorSpecies?seq_contains("POMMIER_POIRIER")>
    <h3>Pression Tavelure de l’année</h3>

    <ul>
      <li>INOKI : nombre de contaminations primaires : ${(reportRegional.primaryContaminations)!"-"}</li>
      <li>INOKI : nombre de jours avec contaminations primaires : ${(reportRegional.numberOfDaysWithPrimaryContaminations)!"-"}</li>
      <li>RIM Pro : nombre de contaminations secondaires (fruit) : ${(reportRegional.secondaryContaminations)!"-"}</li>
      <li>RIM Pro : somme des RIM sur l’année : ${(reportRegional.secondaryContaminations)!"-"}</li>
    </ul>
  </#if>

  <#if reportRegional.diseasePressures??>
  <h3>Pression en maladie</h3>
  <table class="data">
   <tr>
    <th scope="col" width="9%">Groupe cible</th>
    <th scope="col" width="9%">Maladies</th>
    <th scope="col" width="9%">Cultures concernées</th>
    <th scope="col" width="24%">Pression de l’année (échelle régionale)</th>
    <th scope="col" width="24%">Evolution de la pression par rapport à la campagne précédente</th>
    <th scope="col" width="25%">Commentaires</th>
   </tr>
   <#list reportRegional.diseasePressures as diseasePressure>
     <tr>
       <td>${(groupesCiblesParCode[diseasePressure.codeGroupeCibleMaa])!"-"}</td>
       <td>
         <#if diseasePressure.agressors??>
           <#list diseasePressure.agressors as agressor>
             ${agressor.reference_label}<#sep>, </#sep>
           </#list>
         <#else>"-"</#if>
       </td>
       <td>${(diseasePressure.crops)!"-"}</td>
       <td>${(enum.of(diseasePressure, "pressureScale", "RegionPest"))!"-"}</td>
       <td>${(enum.of(diseasePressure, "pressureEvolution"))!"-"}</td>
       <td>${(diseasePressure.comment)!"-"}</td>
     </tr>
    </#list>
  </table>
  </#if>

  <#if reportRegional.pestPressures??>
  <h3>Pression en ravageur</h3>
  <table class="data">
    <tr>
      <th scope="col" width="9%">Groupe cible</th>
      <th scope="col" width="9%">Maladies</th>
      <th scope="col" width="9%">Cultures concernées</th>
      <th scope="col" width="24%">Pression de l’année (échelle régionale)</th>
      <th scope="col" width="24%">Evolution de la pression par rapport à la campagne précédente</th>
      <th scope="col" width="25%">Commentaires</th>
    </tr>
    <#list reportRegional.pestPressures as pestPressure>
      <tr>
        <td>${(groupesCiblesParCode[pestPressure.codeGroupeCibleMaa])!"-"}</td>
        <td>
          <#if pestPressure.agressors??>
            <#list pestPressure.agressors as agressor>
              ${agressor.reference_label}<#sep>, </#sep>
            </#list>
          <#else>"-"</#if>
        </td>
        <td>${(pestPressure.crops)!"-"}</td>
        <td>${(enum.of(pestPressure, "pressureScale", "RegionPest"))!"-"}</td>
        <td>${(enum.of(pestPressure, "pressureEvolution"))!"-"}</td>
        <td>${(pestPressure.comment)!"-"}</td>
      </tr>
    </#list>
  </table>
  </#if>
