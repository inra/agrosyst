<#ftl output_format="XHTML">
<#--
 #%L
 Agrosyst :: Services
 %%
 Copyright (C) 2017 INRA, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<h3>Maîtrise de l'alimentation hydrique et minérale</h3>
<#if foodMastersSplitted??>
<#list foodMastersSplitted as cropObjects>
  <table class="gridTable">
    <tr>
      <th scope="col" width="19%"></th>
      <#list cropObjects as cropObject>
        <th scope="col" class="crop" width="27%">
          <#list cropObject.crops as crop>
            ${crop.name}<#sep>, </#sep>
          </#list>
        </th>
      </#list>
    </tr>
    <tr class="header">
      <td class="secondary" colspan="${cropObjects.size() + 1}">
        Alimentation hydrique&#160;:
      </td>
    </tr>
    <tr>
      <td class="secondary">
        - Irrigation
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(enum.of(cropObject, "foodIrrigation"))!"-"}
        </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        - Stress hydrique
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(cropObject.hydriqueStressInt)!"-"}
        </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        Alimentation minérale&#160;:
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(cropObject.azoteStressInt)!"-"}
        </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        Alimentation minérale (commentaire)&#160;:
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(cropObject.mineralFood)!"-"}
        </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        Température et rayonnement&#160;:
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(cropObject.tempStressInt)!"-"}
        </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        Commentaires – Résultats obtenus et faits marquants&#160;:
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(cropObject.comment)!"-"}
        </td>
      </#list>
    </tr>
  </table>
  <#sep>
  <div class="newPage"></div>
  </#sep>
</#list>
</#if>
