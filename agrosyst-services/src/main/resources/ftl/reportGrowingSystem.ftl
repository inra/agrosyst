<#ftl output_format="XHTML">
<#--
 #%L
 Agrosyst :: Services
 %%
 Copyright (C) 2017 - 2019 INRA, CodeLutin
 Copyright (C) 2020 INRAE, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
 <head>
  <title>Bilan de campagne / échelle système de culture : ${reportGrowingSystem.name}</title>

  <style>
    /* page params */
    @page {
        @top-left {
            content: element(customHeader);
        }
        @bottom-right {
            content: element(normalFooter);
        }
    }
    @page land { size:landscape; }
    @page port { size:portrait; }

    /** others */
    body {
      page: land;
      width: 26.9cm; /* y compris les marges en plus */
    }
    div.newPage {
      page-break-after:always
    }
    h1 {
      color: #3B7DBA;
      text-align: center;
      font-size: 22px;
    }
    h2 {
      font-size: 18px;
      color: #3B7DBA;
      border-bottom: 1px dotted black;
    }
    h3 {
      font-size: 14px;
      color: #3B7DBA;
      margin-left: 15px;
    }

    table.data {
      border: 1px solid #F0EFE3;
    }
    table.data th {
      background-color: #016680;
      color: white;
    }
    table.data tr td {
      padding: 2px;
    }
    table.data tr:nth-child(even) td {
      background-color: #F7F7F7;
    }
    table.gridTable {
      border: 2px solid #F0EFE3;
      border-spacing: 0;
      margin-bottom: 5px;
    }
    table.gridTable th.crop {
      background-color: #44546A;
      color: white;
      text-align: center;
      border: 1px solid white;
      padding: 1px;
    }
    table.gridTable th.agressor {
      background-color: #8497B0;
      color: #FFFFFF;
      text-align: center;
      border: 1px solid white;
      padding: 1px;
    }
    table.gridTable td {
      padding: 3px;
      border-right: 1px solid #F0EFE3;
      font-size: 75%;
    }
    table.gridTable tr:nth-child(even) td {
      background-color: #F7F7F7;
    }
    table.gridTable td:last-child {
      border-right: none;
    }
    table.gridTable .secondary {
      color: #44546A;
      font-size: 100%;
    }
    table.gridTable tr.header td {
      font-weight: bold;
    }
    table.gridTable tr .secondary, table.gridTable tr:nth-child(even) .secondary, table.gridTable tr.header td {
      background-color: #E0E0E0;
    }

    /** Header anf footer */
    .customHeader {
        border-bottom: 1px solid black;
    }
    .normalFooter {
        text-align: right;
    }
    .pagenumber:before {
        content: counter(page)
    }
    .pagecount:before {
        content: counter(pages)
    }
  </style>
 </head>

 <body>
  <div id="normalFooter" style="position: running(normalFooter);" class="normalFooter">
      Page <span class="pagenumber"/> sur <span class="pagecount"/>
  </div>

  <#if reportRegional??>
    <#include "reportRegional.ftl">
    <div class="newPage"></div>
  </#if>

  <h1>Bilan de campagne / échelle système de culture - ${reportGrowingSystem.name}</h1>

  <!-- header for growing system only -->
  <div id="customHeader" style="position: running(customHeader);" class="customHeader">
     ${reportGrowingSystem.growingSystem.growingPlan.domain.name},
     ${reportGrowingSystem.growingSystem.name},
     ${reportGrowingSystem.name},
     ${reportGrowingSystem.growingSystem.growingPlan.domain.campaign?c}
  </div>

  <h2>Faits marquants de l'année:</h2>
    <ul>
      <li><b>Méthode d’estimation des IFT déclarés</b> : ${(enum.of(reportGrowingSystem, "iftEstimationMethod"))!"-"}</li>
    </ul>

    <ul>
      <li><b>Quelles sont les principales évolutions du système de culture depuis le début du suivi ?</b><br/>
       ${(reportGrowingSystem.highlightsEvolutions)!"-"}</li>
      <li><b>Quelles mesures spécifiques aux conditions de l’année ont été mises en place sur le système de culture ?</b><br />
       ${(reportGrowingSystem.highlightsMeasures)!"-"}</li>
      <li><b>Quels sont les faits marquants de l’année en terme de conduite des cultures et de performances techniques (rendement, qualité…) ?</b><br />
       ${(reportGrowingSystem.highlightsPerformances)!"-"}</li>
      <li><b>Quels enseignements pour améliorer le système de culture ?</b><br />
       ${(reportGrowingSystem.highlightsTeachings)!"-"}</li>
    </ul>

  <#if reportGrowingSystemSections??>

    <#list reportGrowingSystemSections as sector, sections>
      <#if reportGrowingSystem.sectors?? && sections??>
        <#if sector == "ARBORICULTURE" && reportGrowingSystem.sectors?seq_contains("ARBORICULTURE")>
          <div class="newPage"></div>
          <#assign addPageBreak = false>
          <h2>Bilan des pratiques et État sanitaire des cultures (Arboriculture)</h2>

          <#if sections?seq_contains("MASTER_DISEASE")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemArboDisease.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_PEST")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemArboPest.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_ADVENTICE")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemArboAdventice.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_FOOD")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemArboFood.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_YIELD")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemArboYield.ftl">
            <#assign addPageBreak = true>
          </#if>

        <#elseif sector == "VITICULTURE" && reportGrowingSystem.sectors?seq_contains("VITICULTURE")>
          <div class="newPage"></div>
          <#assign addPageBreak = false>
          <h2>Bilan des pratiques et État sanitaire des cultures (Viticulture)</h2>

          <#if sections?seq_contains("MASTER_DISEASE")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemVitiDisease.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_PEST")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemVitiPest.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_ADVENTICE")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemVitiAdventice.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_FOOD")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemVitiFood.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_YIELD")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemVitiYield.ftl">
            <#assign addPageBreak = true>
          </#if>

        <#elseif sector == "MARAICHAGE">
          <div class="newPage"></div>
          <#assign addPageBreak = false>
          <h2>Bilan des pratiques et État sanitaire des cultures (${enum.tr("fr.inra.agrosyst.api.entities.Sector." + sector)})</h2>

          <#if sections?seq_contains("MASTER_ADVENTICE")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemMaraichageAdventice.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_DISEASE")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemMaraichageDisease.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_PEST")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemMaraichagePest.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_FOOD")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemMaraichageFood.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_YIELD")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemMaraichageYield.ftl">
            <#assign addPageBreak = true>
          </#if>

        <#elseif sector == "HORTICULTURE">
          <div class="newPage"></div>
          <#assign addPageBreak = false>
          <h2>Bilan des pratiques et État sanitaire des cultures (${enum.tr("fr.inra.agrosyst.api.entities.Sector." + sector)})</h2>

          <#if sections?seq_contains("MASTER_ADVENTICE")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemHorticultureAdventice.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_DISEASE")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemHorticultureDisease.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_PEST")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemHorticulturePest.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_FOOD")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemHorticultureFood.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_YIELD")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemHorticultureYield.ftl">
            <#assign addPageBreak = true>
          </#if>

        <#elseif sector == "null" && (
              reportGrowingSystem.sectors?seq_contains("GRANDES_CULTURES") ||
              reportGrowingSystem.sectors?seq_contains("POLYCULTURE_ELEVAGE") ||
              reportGrowingSystem.sectors?seq_contains("MARAICHAGE") ||
              reportGrowingSystem.sectors?seq_contains("HORTICULTURE") ||
              reportGrowingSystem.sectors?seq_contains("CULTURES_TROPICALES")
            ) ||
            sector != "VITICULTURE" && sector != "ARBORICULTURE" && sector != "MARAICHAGE" && reportGrowingSystem.sectors?seq_contains(sector)>

          <div class="newPage"></div>
          <#assign addPageBreak = false>
          <h2>Bilan des pratiques et État sanitaire des cultures
            <#if sector == "null">
              (Cultures assolées)
            <#else>
              (${enum.tr("fr.inra.agrosyst.api.entities.Sector." + sector)})
            </#if>
          </h2>

          <#if sections?seq_contains("MASTER_ADVENTICE")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemAllAdventice.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_DISEASE")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemAllDisease.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_PEST")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemAllPest.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_VERSE")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemAllVerse.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_FOOD")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemAllFood.ftl">
            <#assign addPageBreak = true>
          </#if>
          <#if sections?seq_contains("MASTER_YIELD")>
            <#if addPageBreak><div class="newPage"></div></#if>
            <#include "reportGrowingSystemAllYield.ftl">
            <#assign addPageBreak = true>
          </#if>
        </#if>
      </#if>
    </#list>
  </#if>

  <#if managementSections?? && managementSections?size gt 0>

    <!-- header for management mode only -->
    <div id="customHeader" style="position: running(customHeader);" class="customHeader">
       ${reportGrowingSystem.growingSystem.growingPlan.domain.name},
       ${reportGrowingSystem.growingSystem.name},
       Modèle décisionnel,
       ${reportGrowingSystem.growingSystem.growingPlan.domain.campaign?c}
    </div>

    <div class="newPage"></div>
    <#assign addPageBreak = false>
    <h2>Modèle décisionnel</h2>

    <#if managementSections?seq_contains("CYCLE_PLURIANNUEL_DE_CULTURE")>
      <#if addPageBreak><div class="newPage"></div></#if>
      <#include "managementModeCycleCultures.ftl">
      <#assign addPageBreak = true>
    </#if>
    <#if managementSections?seq_contains("TRAVAIL_DU_SOL")>
      <#if addPageBreak><div class="newPage"></div></#if>
      <#include "managementModeTravailSols.ftl">
      <#assign addPageBreak = true>
    </#if>
    <#if managementSections?seq_contains("ADVENTICES")>
      <#if addPageBreak><div class="newPage"></div></#if>
      <#include "managementModeAdventices.ftl">
      <#assign addPageBreak = true>
    </#if>
    <#if managementSections?seq_contains("MALADIES")>
      <#if addPageBreak><div class="newPage"></div></#if>
      <#include "managementModeMaladies.ftl">
      <#assign addPageBreak = true>
    </#if>
    <#if managementSections?seq_contains("RAVAGEURS")>
      <#if addPageBreak><div class="newPage"></div></#if>
      <#include "managementModeRavageurs.ftl">
      <#assign addPageBreak = true>
    </#if>
    <#if managementSections?seq_contains("MAITRISE_DES_DOMMAGES_PHYSIQUES")>
      <#if addPageBreak><div class="newPage"></div></#if>
      <#include "managementModeDommages.ftl">
      <#assign addPageBreak = true>
    </#if>
    <#if managementSections?seq_contains("FERTILITE_SOL_CULTURES")>
      <#if addPageBreak><div class="newPage"></div></#if>
      <#include "managementModeFertiliteSols.ftl">
      <#assign addPageBreak = true>
    </#if>
    <#if managementSections?seq_contains("PRODUCTION")>
      <#if addPageBreak><div class="newPage"></div></#if>
      <#include "managementModeProductions.ftl">
      <#assign addPageBreak = true>
    </#if>
  </#if>
 </body>
</html>
