<#ftl output_format="XHTML">
<#--
 #%L
 Agrosyst :: Services
 %%
 Copyright (C) 2017 INRA, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
 <!-- Pluriannuel ou extraparcellaire -->
 <td>
   <#list section.strategies as strategy>
     <#if strategy??>
       <#if "MULTI_ANNUAL_STRATEGY" == strategy.implementationType!"">
         <div>
           <#list strategy.crops as crop>
             ${crop.name}<#sep>, </#sep>
           </#list>
           - ${strategy.explanation!"-"}
         </div>
       </#if>
     </#if>
   </#list>
 </td>

 <!-- Annuel ou parcellaire -->
 <td>
   <#list section.strategies as strategy>
     <#if strategy??>
       <#if "MULTI_ANNUAL_STRATEGY" != strategy.implementationType!"">
         <div>
           <#list strategy.crops as crop>
             ${crop.name}<#sep>, </#sep>
           </#list>
           - ${strategy.explanation!"-"}
         </div>
       </#if>
     </#if>
   </#list>
 </td>
