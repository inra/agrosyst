<#ftl output_format="XHTML">
<#--
 #%L
 Agrosyst :: Services
 %%
 Copyright (C) 2017 INRA, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<h3>Maîtrise des maladies</h3>
<#if withCropDiseaseMastersSplitted?? && withCropDiseaseMastersSplitted[sector]??>

<#list withCropDiseaseMastersSplitted[sector] as withCropMasters>
  <table class="gridTable">
    <tr>
      <th scope="col"></th>
      <#list withCropMasters as withCropMaster>
        <#if withCropMaster.span gt 0>
          <th scope="col" colspan="${withCropMaster.span}" class="crop">
            <#list withCropMaster.crop.crops as crop>
              ${crop.name}<#sep>, </#sep>
            </#list>
          </th>
        </#if>
      </#list>
    </tr>
    <tr>
      <th scope="col" width="19%"></th>
      <#list withCropMasters as withCropMaster>
        <th scope="col" class="agressor" width="27%">
          ${(groupesCiblesParCode[withCropMaster.object.codeGroupeCibleMaa])!"-"} / ${(withCropMaster.object.agressor.reference_label)!"-"}
        </th>
      </#list>
    </tr>
    <tr class="header">
      <td class="secondary" colspan="${withCropMasters.size() + 1}">
        Pression avant intervention&#160;:
      </td>
    </tr>
    <tr>
      <td class="secondary">
        - Échelle de pression
      </td>
      <#list withCropMasters as withCropMaster>
        <td>
          ${(enum.of(withCropMaster.object, "pressureScale"))!"-"}
        </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        - Expression de l’agriculteur
      </td>
      <#list withCropMasters as withCropMaster>
        <td>
          ${(withCropMaster.object.pressureFarmerComment)!"-"}
        </td>
      </#list>
    </tr>
    <tr class="header">
      <td class="secondary" colspan="${withCropMasters.size() + 1}">
        Résultats obtenus, niveau de maîtrise finale&#160;:
      </td>
    </tr>
    <tr>
      <td class="secondary">
        - Échelle de maîtrise
      </td>
      <#list withCropMasters as withCropMaster>
        <td>
          ${(enum.of(withCropMaster.object, "masterScale", "Pest"))!"-"}
        </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        - Qualification du niveau de maîtrise
      </td>
      <#list withCropMasters as withCropMaster>
        <td>
          ${(enum.of(withCropMaster.object, "qualifier"))!"-"}
        </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        - Expression de l’agriculteur
      </td>
      <#list withCropMasters as withCropMaster>
        <td>
          ${(withCropMaster.object.resultFarmerComment)!"-"}
        </td>
      </#list>
    </tr>
    <tr class="header">
      <td class="secondary" colspan="${withCropMasters.size() + 1}">
        IFT&#160;:
      </td>
    </tr>
    <tr>
      <td class="secondary">
        - IFT-Fongicide
      </td>
      <#list withCropMasters as withCropMaster>
        <#if withCropMaster.span gt 0>
          <td colspan="${withCropMaster.span}">
            ${(withCropMaster.crop.iftMain)!"-"}
          </td>
        </#if>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
       - Commentaires du conseiller
      </td>
      <#list withCropMasters as withCropMaster>
        <#if withCropMaster.span gt 0>
          <td colspan="${withCropMaster.span}">
            ${(withCropMaster.crop.adviserComments)!"-"}
          </td>
        </#if>
      </#list>
    </tr>
  </table>
  <#sep>
  <div class="newPage"></div>
  </#sep>
</#list>
</#if>
