<#ftl output_format="XHTML">
<#--
 #%L
 Agrosyst :: Services
 %%
 Copyright (C) 2017 INRA, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<h3>Rendement et qualité</h3>
<#if yieldLossesSplitted??>
<#list yieldLossesSplitted as cropObjects>
  <table class="gridTable">
    <tr>
      <th scope="col" width="19%"></th>
      <#list cropObjects as cropObject>
        <th scope="col" class="crop" width="27%">
          <#list cropObject.crops as crop>
            ${crop.name}<#sep>, </#sep>
          </#list>
        </th>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        L’objectif de rendement est-il atteint ?
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(cropObject.yieldObjectiveInt)!"-"}
        </td>
      </#list>
    </tr>
    <tr class="header">
      <td class="secondary" colspan="${cropObjects.size() + 1}">
        Si les objectifs de rendement ne sont pas atteints, causes (par ordre d’importance)&#160;:
      </td>
    </tr>
    <tr>
      <td class="secondary">
        - Cause 1
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(enum.of(cropObject, "cause1"))!"-"}
        </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        - Cause 2
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(enum.of(cropObject, "cause2"))!"-"}
        </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        - Cause 3
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(enum.of(cropObject, "cause3"))!"-"}
        </td>
      </#list>
    </tr>
    <tr>
      <td class="secondary">
        Commentaires sur la qualité
      </td>
      <#list cropObjects as cropObject>
        <td>
          ${(cropObject.comment)!"-"}
        </td>
      </#list>
    </tr>
  </table>
  <#sep>
  <div class="newPage"></div>
  </#sep>
</#list>
</#if>
<#if reportGrowingSystem.yieldInfos??>
  <ul>
    <li>Commentaires – Résultats obtenus et faits marquants :
      <#list reportGrowingSystem.yieldInfos as yieldInfo>
        <#if yieldInfo.sector?default("null") == sector>
        ${(yieldInfo.comment)!"-"}
        </#if>
      </#list>
    </li>
  </ul>
</#if>
