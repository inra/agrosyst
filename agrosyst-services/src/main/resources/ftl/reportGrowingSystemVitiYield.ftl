<#ftl output_format="XHTML">
<#--
 #%L
 Agrosyst :: Services
 %%
 Copyright (C) 2017 INRA, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<h3>Rendement et qualité</h3>

<ul>
  <li>L’objectif de rendement est-il atteint ? ${(enum.of(reportGrowingSystem, "vitiYieldObjective"))!"-"}</li>
  <li>Si les objectifs de rendement ne sont pas atteints, causes (par ordre d’importance)
    <ul>
      <li>Cause 1 : ${(enum.of(reportGrowingSystem, "vitiLossCause1"))!"-"}</li>
      <li>Cause 2 : ${(enum.of(reportGrowingSystem, "vitiLossCause2"))!"-"}</li>
      <li>Cause 3 : ${(enum.of(reportGrowingSystem, "vitiLossCause3"))!"-"}</li>
    </ul>
  </li>
  <li>Commentaires sur la qualité : ${(reportGrowingSystem.vitiYieldQuality)!"-"}</li>
  <#if reportGrowingSystem.yieldInfos??>
    <li>Commentaires – Résultats obtenus et faits marquants :
      <#list reportGrowingSystem.yieldInfos as yieldInfo>
        <#if yieldInfo.sector?default("null") == "VITICULTURE">
        ${(yieldInfo.comment)!"-"}
        </#if>
      </#list>
    </li>
  </#if>
</ul>
