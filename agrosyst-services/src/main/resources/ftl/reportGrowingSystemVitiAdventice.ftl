<#ftl output_format="XHTML">
<#--
 #%L
 Agrosyst :: Services
 %%
 Copyright (C) 2017 INRA, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<h3>Maîtrise des adventices</h3>

<ul>
  <li>Échelle de pression : ${(enum.of(reportGrowingSystem, "vitiAdventicePressureScale", "Adventice"))!"-"}</li>
  <li>Pression des adventices  - Expression de l’agriculteur : ${(reportGrowingSystem.vitiAdventicePressureFarmerComment)!"-"}</li>
  <li>Résultats obtenus, niveau de maîtrise finale : ${(enum.of(reportGrowingSystem, "vitiAdventiceQualifier"))!"-"}</li>
  <li>Niveau de maîtrise  - Expression de l’agriculteur : ${(reportGrowingSystem.vitiAdventiceResultFarmerComment)!"-"}</li>
</ul>

<table class="data">
   <tr>
    <th scope="col"></th>
    <th scope="col" colspan="2">Nombre de traitements</th>
    <th scope="col" colspan="2">IFT</th>
   </tr>
   <tr>
    <th scope="col"></th>
    <th scope="col">Chimique</th>
    <th scope="col">Biologique</th>
    <th scope="col">Chimique</th>
    <th scope="col">Biologique</th>
   </tr>
   <tr>
     <th scope="col">Traitements herbicides (hors épamprage)</th>
     <td>${(reportGrowingSystem.vitiHerboTreatmentChemical)!"-"}</td>
     <td>${(reportGrowingSystem.vitiHerboTreatmentBioControl)!"-"}</td>
     <td>${(reportGrowingSystem.vitiHerboTreatmentChemicalIFT)!"-"}</td>
     <td>${(reportGrowingSystem.vitiHerboTreatmentBioControlIFT)!"-"}</td>
   </tr>
   <tr>
     <th scope="col">Epamprage</th>
     <td>${(reportGrowingSystem.vitiSuckeringChemical)!"-"}</td>
     <td>${(reportGrowingSystem.vitiSuckeringBioControl)!"-"}</td>
     <td>${(reportGrowingSystem.vitiSuckeringChemicalIFT)!"-"}</td>
     <td>${(reportGrowingSystem.vitiSuckeringBioControlIFT)!"-"}</td>
   </tr>
</table>
